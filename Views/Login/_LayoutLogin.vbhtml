﻿@Imports Martins.AcoesMercadologicas.Core
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>@ViewBag.Title</title>

    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="~/assets/css/bootstrap.css" />
    <link rel="stylesheet" href="~/assets/css/font-awesome.css" />

    <!-- page specific plugin styles -->
    <!-- text fonts -->
    <link rel="stylesheet" href="~/assets/css/ace-fonts.css" />

    <link rel="stylesheet" href="~/assets/css/chosen.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="~/assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

    <!--[if lte IE 9]>
        <link rel="stylesheet" href="~/assets/css/ace-part2.css" class="ace-main-stylesheet" />
    <![endif]-->
    <!--[if lte IE 9]>
      <link rel="stylesheet" href="~/assets/css/ace-ie.css" />
    <![endif]-->
    <!-- inline styles related to this page -->
    <!-- ace settings handler -->
    <script src="~/assets/js/ace-extra.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
    <!--[if lte IE 8]>
    <script src="~/assets/js/html5shiv.js"></script>
    <script src="~/assets/js/respond.js"></script>
    <![endif]-->
    <!-- custom styles -->

    <link rel="stylesheet" href="~/assets/custom/css/custom.css" />
    <link rel="shortcut icon" href="~/assets/custom/images/martins_favicon.ico" />
    <link rel="stylesheet" href="~/assets/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="~/assets/css/bootstrap-timepicker.css" />
    <link rel="stylesheet" href="~/assets/css/daterangepicker.css" />
    <link rel="stylesheet" href="~/assets/css/bootstrap-datetimepicker.css" />
    <script src="~/assets/js/jquery.js"></script>
    <script src="~/assets/js/bootstrap.js"></script>
    <!-- /.main-container -->
    <!-- basic scripts -->
    <!--[if !IE]> -->
    <!-- <![endif]-->
    <!--[if IE]>
        <script src="~/assets/js/jquery1x.js"></script>
    <![endif]-->
    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.js'>" + "<" + "/script>");
    </script>

</head>

<body>
    <div class="main-container" id="main-container">
        <div class="main-content">
            <div class="main-content-inner">
                @RenderBody()
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

    <script src="@Url.Content("~/Scripts/jquery.unobtrusive-ajax.js")" type="text/javascript"></script>


    <!-- page specific plugin scripts -->
    <!-- ace scripts -->
    <script src="~/assets/js/ace/elements.scroller.js"></script>
    <script src="~/assets/js/ace/elements.colorpicker.js"></script>
    <script src="~/assets/js/ace/elements.fileinput.js"></script>
    <script src="~/assets/js/ace/elements.typeahead.js"></script>
    <script src="~/assets/js/ace/elements.wysiwyg.js"></script>
    <script src="~/assets/js/ace/elements.spinner.js"></script>
    <script src="~/assets/js/ace/elements.treeview.js"></script>
    <script src="~/assets/js/ace/elements.wizard.js"></script>
    <script src="~/assets/js/ace/elements.aside.js"></script>
    <script src="~/assets/js/ace/ace.js"></script>
    <script src="~/assets/js/ace/ace.ajax-content.js"></script>
    <script src="~/assets/js/ace/ace.touch-drag.js"></script>
    <script src="~/assets/js/ace/ace.sidebar.js"></script>
    <script src="~/assets/js/ace/ace.sidebar-scroll-1.js"></script>
    <script src="~/assets/js/ace/ace.submenu-hover.js"></script>
    <script src="~/assets/js/ace/ace.widget-box.js"></script>
    <script src="~/assets/js/ace/ace.settings.js"></script>
    <script src="~/assets/js/ace/ace.settings-rtl.js"></script>
    <script src="~/assets/js/ace/ace.settings-skin.js"></script>
    <script src="~/assets/js/ace/ace.widget-on-reload.js"></script>
    <script src="~/assets/js/ace/ace.searchbox-autocomplete.js"></script>
    <script src="~/assets/js/chosen.jquery.js"></script>

    <!-- inline scripts related to this page -->
    <!-- the following scripts are used in demo only for onpage help and you don't need them -->
    <link rel="stylesheet" href="~/assets/css/ace.onpage-help.css" />
    @*<link rel="stylesheet" href="~/docs/assets/js/themes/sunburst.css" />*@

    <script type="text/javascript"> ace.vars['base'] = '..'; </script>
    <script src="~/assets/js/ace/elements.onpage-help.js"></script>
    <script src="~/assets/js/ace/ace.onpage-help.js"></script>
    <script src="~/assets/js/date-time/bootstrap-datepicker.js"></script>
    <script src="~/assets/custom/js/bootstrap-datepicker.pt-BR.js"></script>
    <script src="~/assets/js/date-time/bootstrap-timepicker.js"></script>
    <script src="~/assets/js/date-time/moment.js"></script>
    <script src="~/assets/js/date-time/daterangepicker.js"></script>
    <script src="~/assets/js/date-time/bootstrap-datetimepicker.js"></script>
    <script src="~/assets/js/jquery.meio.mask.min.js"></script>

    <script src="~/Scripts/custom.js"></script>

</body>
</html>
