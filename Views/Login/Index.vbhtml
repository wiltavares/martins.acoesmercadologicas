﻿@ModelType LoginViewModel
@Code
    ViewData("Title") = "Ações"
    Layout = "~/Views/Login/_LayoutLogin.vbhtml"
End Code
<body class="login-layout">
    <div class="main-container">
        <div class="main-content">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="login-container">
                        <div class="center login-header">
                            <img class="login-logo-martins" src="~/assets/custom/images/logo_martins.svg">
                            <div class="login-font-logo">Ações Mercadológicas</div>
                        </div>

                        <div class="space-6"></div>

                        <div class="position-relative">
                            <div id="login-box" class="login-box visible widget-box no-border">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <h4 class="header blue lighter bigger">
                                            <i class="ace-icon fa fa-coffee green"></i>
                                            Favor entrar com seus dados
                                        </h4>

                                        <div class="space-6"></div>

                                        @Using (Html.BeginForm(FormMethod.Post))
                                            @Html.AntiForgeryToken()

                                            @<div class="form-horizontal">

                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        @Html.TextBoxFor(Function(model) model.Usuario, New With {.Class = "form-control", .placeholder = "Usuário"})
                                                        @Html.ValidationMessageFor(Function(model) model.Usuario, "", New With {.class = "text-danger-message-login"})
                                                        <i class="ace-icon fa fa-user"></i>
                                                    </span>
                                                </label>

                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        @Html.TextBox("senha", Model.Senha, New With {.Class = "form-control", .type = "password", .placeholder = "Senha"})
                                                        @Html.ValidationMessageFor(Function(model) model.Senha, "", New With {.class = "text-danger-message-login"})
                                                        <i class="ace-icon fa fa-lock"></i>
                                                    </span>
                                                </label>
                                                @Html.ValidationMessage("LoginInvalido", "", New With {.class = "text-danger-message-login"})

                                                <div class="space"></div>

                                                <div class="clearfix">
                                                    @*<label class="inline">
                                                        @Html.CheckBox("checkBox", Model.Lembrar, New With {.class = "checkbox-ace"})
                                                        @Html.LabelFor(Function(model) model.Lembrar)
                                                    </label>*@

                                                    <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                        <i class="ace-icon fa fa-key pull-left"></i>
                                                        <span class="bigger-110">Entrar</span>
                                                    </button>
                                                </div>
                                            </div>
                                        End Using

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
