﻿@ModelType GridSettings(Of AprovadoresExtraViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Html.Partial(Constantes.VIEW_MODAL_CONFIRMACAO, New Tuple(Of String, String, Object)("Delete", "AprovadoresExtra", Model))

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 250px;">
                    <thead>
                        <tr role="row">
                            <th class="center">Ações</th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMTIPEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMTIPEVTACOCMC", Model.Page.OrderBy)">
                                Tipo Evento
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMUSREDEAPVEXT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMUSREDEAPVEXT", Model.Page.OrderBy)">
                                Tipo Aprovador
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMUSREDEAPV", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMUSREDEAPV", Model.Page.OrderBy)">
                                Apv. Substítuido
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMFNC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFNC", Model.Page.OrderBy)">
                                Apv. Extra
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td class="center">
                                    <div class="hidden-sm hidden-xs btn-group">
                                        <a data-rel="tooltip" title="Deletar" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#confirmationDelete" data-id="@(item.TIPEVTACOCMC.Value.ToString + ":" + item.CODTIPAPV.Value.ToString + ":" + item.CODEDEAPV.Value.ToString)">
                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    @Html.Label(If(CStr(item.NOMTIPEVTACOCMC) = "", "", item.NOMTIPEVTACOCMC.Trim()))
                                </td>
                                <td>
                                    @Html.Label(If(CStr(item.DESTIPAPV) = "", "", item.DESTIPAPV.Trim()))
                                </td>
                                <td>
                                    @Html.Label(If(CStr(item.NOMUSREDEAPV) = "", "", item.NOMUSREDEAPV.Trim()))
                                </td>
                                <td>
                                    @Html.Label(If(CStr(item.NOMFNC) = "", "", item.NOMFNC.Trim()))
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>