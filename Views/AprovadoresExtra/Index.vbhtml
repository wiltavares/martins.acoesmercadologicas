﻿@ModelType GridSettings(Of AprovadoresExtraViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Aprovadores Extras"
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Aprovadores Extras</li>
    </ul>
</div>
<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modalFuncionarios"></div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "AprovadoresExtra", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-12">
                            @Html.Label("lblTipoEvento", "Tipo de Evento (*):")
                            @Html.DropDownListFor(Function(model) model.Filter.TIPEVTACOCMC, New SelectList(ViewBag.ListaTipoEvento, "TIPEVTACOCMC", "NOMTIPEVTACOCMC", Model.Filter.TIPEVTACOCMC), "Selecione...", New With {.Class = "chosen-select form-control", .id = "codTipoEvento"})
                            @Html.Label("lblValidTipoEvento", "Preencha o campo Tipo de Evento", New With {.id = "validTipoEvento", .class = "text-danger-message-login", .style = "display:none;"})
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            @Html.Label("lblTipoAprovador", "Tipo de Aprovador (*):")
                            @Html.DropDownListFor(Function(model) model.Filter.CODTIPAPV, New SelectList(ViewBag.ListaTipoAprovador, "CODTIPAPV", "DESTIPAPV", Model.Filter.CODTIPAPV), "Selecione...", New With {.Class = "chosen-select form-control", .id = "codTipoAprovador", .onChange = "CarregaSubstituido()"})
                            @Html.Label("lblValidTipoAprovador", "Preencha o campo Tipo de Aprovador", New With {.id = "ValidTipoAprovador", .class = "text-danger-message-login", .style = "display:none;"})
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            @Html.Label("lblAprovadorSubstituido", "Aprovador Substítuido (*):")
                            @Html.DropDownListFor(Function(model) model.Filter.CODFNC, New SelectList(ViewBag.ListaTipoAprovadorSubstituido, "CODFNC", "NOMFNC", Model.Filter.CODFNC), "Selecione...", New With {.Class = "chosen-select form-control", .id = "codTipoAprovadorSubst"})
                            @Html.Label("lblValidAprovadorSubstituido", "Preencha o campo Aprovador Substítuido", New With {.id = "ValidAprovadorSubstituido", .class = "text-danger-message-login", .style = "display:none;"})
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" id="modalFunc">
                            @Html.HiddenFor(Function(model) model.Filter.CODEDEAPVEXA, New With {.Class = "form-control", .id = "idCodFuncionario"})
                            @Html.Label("lblNomFunc", "Funcionário (*):")
                            <div class="input-group" id="dividItem">
                                @Html.TextBoxFor(Function(model) model.Filter.NOMFNC, New With {.Class = "form-control", .id = "nomFuncionario"})
                                <span class="input-group-btn">
                                    <button class="btn btn-white" id="btBuscaFunc" type="button" onclick="modalBuscaFuncionariosAprovadores()" style="display: block">
                                        <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                    </button>
                                    <button class="btn btn-white" id="btTiraFunc" type="button" onclick="tiraFuncionarioAprovadores()" style="display: none">
                                        <i class="ace-icon fa fa-close bigger-120 red"></i>
                                    </button>
                                </span>
                            </div>
                            @Html.Label("lblValidNomFunc", "Preencha o campo Funcionário", New With {.id = "ValidNomFunc", .class = "text-danger-message-login", .style = "display:none;"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </button>
                                <a class="btn btn-white btn-success" onclick="IncluirAprovadoresExtra()">
                                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                                    Incluir
                                </a>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger-120"></i>
                                    Limpar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<br class="clearfix" />
<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/AprovadoresExtra/_ResultadosView.vbhtml", Model)
    End Code
</div>

<script src="~/Scripts/AprovadoresExtra.js"></script>