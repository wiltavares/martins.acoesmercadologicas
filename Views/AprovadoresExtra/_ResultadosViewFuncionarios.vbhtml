﻿@ModelType GridSettings(Of FuncionarioViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 250px;">
                    <thead>
                        <tr role="row">
                            <th class="center">
                                Selecionar
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODFNC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFNC", Model.Page.OrderBy)">
                                Matricula
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMFNC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFNC", Model.Page.OrderBy)">
                                Funcionário
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td class="center">
                                    <div>
                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" onclick="selecionarFuncionarioAprovadoresExtra(@item.CODFNC, '@item.NOMFNC')">
                                            <i class="ace-icon fa fa-check bigger-120"></i>
                                        </button>
                                    </div>
                                </td>
                                <td>
                                    @Html.Label(CInt(item.CODFNC))
                                </td>
                                <td>
                                    @Html.Label(If(CStr(item.NOMFNC) = "", "", item.NOMFNC.Trim()))
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>