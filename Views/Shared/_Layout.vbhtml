﻿@Imports Martins.AcoesMercadologicas.Core
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>@ViewBag.Title</title>

    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="~/assets/css/bootstrap.css" />
    <link rel="stylesheet" href="~/assets/css/font-awesome.css" />

    <!-- page specific plugin styles -->
    <!-- text fonts -->
    <link rel="stylesheet" href="~/assets/css/ace-fonts.css" />

    <link rel="stylesheet" href="~/assets/css/chosen.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="~/assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

    <!--[if lte IE 9]>
        <link rel="stylesheet" href="~/assets/css/ace-part2.css" class="ace-main-stylesheet" />
    <![endif]-->
    <!--[if lte IE 9]>
      <link rel="stylesheet" href="~/assets/css/ace-ie.css" />
    <![endif]-->
    <!-- inline styles related to this page -->
    <!-- ace settings handler -->
    <script src="~/assets/js/ace-extra.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
    <!--[if lte IE 8]>
    <script src="~/assets/js/html5shiv.js"></script>
    <script src="~/assets/js/respond.js"></script>
    <![endif]-->
    <!-- custom styles -->

    <link rel="stylesheet" href="~/assets/custom/css/custom.css" />
    <link rel="shortcut icon" href="~/assets/custom/images/martins_favicon.ico" />
    <link rel="stylesheet" href="~/assets/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="~/assets/css/bootstrap-timepicker.css" />
    <link rel="stylesheet" href="~/assets/css/daterangepicker.css" />
    <link rel="stylesheet" href="~/assets/css/bootstrap-datetimepicker.css" />
    <script src="~/assets/js/jquery.js"></script>
    <script src="~/assets/js/bootstrap.js"></script>
    <!-- /.main-container -->
    <!-- basic scripts -->
    <!--[if !IE]> -->
    <!-- <![endif]-->
    <!--[if IE]>
        <script src="~/assets/js/jquery1x.js"></script>
    <![endif]-->
    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.js'>" + "<" + "/script>");
    </script>

</head>

<body class="no-skin martins-color">
    <!-- #section:basics/navbar.layout -->
    <div id="navbar" class="navbar navbar-default ace-save-state">
        <div class="navbar-container ace-save-state" id="navbar-container">
            <!-- #section:basics/sidebar.mobile.toggle -->
            <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                <span class="sr-only">Toggle sidebar</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>
            </button>

            <!-- /section:basics/sidebar.mobile.toggle -->
            <div class="navbar-header">
                <!-- #section:basics/navbar.layout.brand -->
                <a href="#">
                    <small>
                        <img class="logo-martins" src="~/assets/custom/images/logo_martins.svg">
                        @Html.ActionLink("Ações Mercadológicas", "Index", "Home", New With {.area = ""}, New With {.class = "navbar-brand"})
                    </small>
                </a>

                <!-- /section:basics/navbar.layout.brand -->
                <!-- #section:basics/navbar.toggle -->
                <!-- /section:basics/navbar.toggle -->
            </div>

            <!-- #section:basics/navbar.dropdown -->
            <div class="navbar-buttons pull-right" role="navigation">

                <ul class="nav ace-nav pull-right">
                    <li class="hidden-xs">
                        <a href="#">
                            Versao:
                            @System.Configuration.ConfigurationManager.AppSettings("AppVersion")
                            <i class="ace-icon fa fa-info fa-2x"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            @System.Environment.MachineName
                            <i class="ace-icon fa fa-server fa-2x"></i>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            @HttpContext.Current.User.Identity.Name
                            <i class="ace-icon fa fa-user fa-2x"></i>
                        </a>
                    </li>
                    @if (Session("IsMobile")) Then
                        @:<li>
                            @:<a href="https://ww3.martins.com.br/Mobile.AprovaSim/">
                                @:Aprova SIM
                                @:<i class="fa fa-reply"></i>
                                @:</a>
                            @:</li>
                    Else
                        @:<li>
                            @:<a href="@Url.Action("LogOff", "Login")">
                                @:Sair
                                @:<i Class="ace-icon fa fa-sign-out fa-2x"></i>
                                @:</a>
                            @:</li>
                    End If

                </ul>
            </div>

            <!-- /section:basics/navbar.dropdown -->
        </div><!-- /.navbar-container -->
    </div>

    <!-- /section:basics/navbar.layout -->
    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
            try { ace.settings.loadState('main-container') } catch (e) { }
        </script>

        <!-- #section:basics/sidebar -->
        @*@Html.MvcSiteMap().Menu("Menu")*@


        <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
            <script type="text/javascript">
                try { ace.settings.loadState('sidebar') } catch (e) { }
            </script>
            @Code
                Html.RenderPartial("~/Views/Shared/_MenuD.vbhtml", ControleAcesso.Menus())
            End Code

            <!-- /.nav-list -->
            <!-- #section:basics/sidebar.layout.minimize -->

            <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
            </div>

            <!-- /section:basics/sidebar.layout.minimize -->
        </div>


        <!-- /section:basics/sidebar -->
        <div class="main-content">
            <div class="main-content-inner">
                <!-- #section:basics/content.breadcrumbs -->
                @*<div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <ul class="breadcrumb">*@
                @*@Html.MvcSiteMap().SiteMapPath()*@
                @*</ul>*@<!-- /.breadcrumb -->
                <!-- #section:basics/content.searchbox -->
                @*</div>*@

                <!-- /section:basics/content.breadcrumbs -->
                <div class="page-content">
                    @Html.Partial(Constantes.VIEW_LOADING)
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            @RenderBody()


                            <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>
        </div><!-- /.main-content -->
        <div class="footer">
            <div class="footer-inner">
                <!-- #section:basics/footer -->
                <div class="footer-content">
                    <div class="col-sm-6">
                        <span class="bigger-120 pull-right padding-top-footer">

                            <span class="pull-right" style="color:#1C4D86; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;">Ações Mercadológicas </span>
                            <br />
                            <small class="cinza-fonte pull-right"> Versão: @System.Configuration.ConfigurationManager.AppSettings("AppVersion") - © @DateTime.Now.Year</small>

                        </span>
                    </div>
                    <div class="col-sm-6 ">
                        <span class="bigger-120 pull-left">
                            <img class="" style="height:50px" src="~/assets/custom/images/logo_martins_colorida.svg">
                        </span>
                    </div>
                </div>
                <!-- /section:basics/footer -->
            </div>
        </div>

        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
            <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
        </a>
    </div>

    <script src="~/Scripts/acaoMercadologica.js"></script>
    <script src="~/Scripts/pedidoBonificacao.js"></script>
    <script src="~/Scripts/premiacaoExclusiva.js"></script>
    <script src="~/Scripts/carimbo.js"></script>
    <script src="~/Scripts/fornecedorAcaoMercadologica.js"></script>
    <script src="~/Scripts/modalFuncionarios.js"></script>
    <script src="~/Scripts/itensOrcamento.js"></script>

    <script src="@Url.Content("~/Scripts/jquery.unobtrusive-ajax.js")" type="text/javascript"></script>

    <!-- page specific plugin scripts -->
    <!-- ace scripts -->
    <script src="~/assets/js/ace/elements.scroller.js"></script>
    <script src="~/assets/js/ace/elements.colorpicker.js"></script>
    <script src="~/assets/js/ace/elements.fileinput.js"></script>
    <script src="~/assets/js/ace/elements.typeahead.js"></script>
    <script src="~/assets/js/ace/elements.wysiwyg.js"></script>
    <script src="~/assets/js/ace/elements.spinner.js"></script>
    <script src="~/assets/js/ace/elements.treeview.js"></script>
    <script src="~/assets/js/ace/elements.wizard.js"></script>
    <script src="~/assets/js/ace/elements.aside.js"></script>
    <script src="~/assets/js/ace/ace.js"></script>
    <script src="~/assets/js/ace/ace.ajax-content.js"></script>
    <script src="~/assets/js/ace/ace.touch-drag.js"></script>
    <script src="~/assets/js/ace/ace.sidebar.js"></script>
    <script src="~/assets/js/ace/ace.sidebar-scroll-1.js"></script>
    <script src="~/assets/js/ace/ace.submenu-hover.js"></script>
    <script src="~/assets/js/ace/ace.widget-box.js"></script>
    <script src="~/assets/js/ace/ace.settings.js"></script>
    <script src="~/assets/js/ace/ace.settings-rtl.js"></script>
    <script src="~/assets/js/ace/ace.settings-skin.js"></script>
    <script src="~/assets/js/ace/ace.widget-on-reload.js"></script>
    <script src="~/assets/js/ace/ace.searchbox-autocomplete.js"></script>
    <script src="~/assets/js/chosen.jquery.js"></script>

    <!-- inline scripts related to this page -->
    <!-- the following scripts are used in demo only for onpage help and you don't need them -->
    <link rel="stylesheet" href="~/assets/css/ace.onpage-help.css" />
    @*<link rel="stylesheet" href="~/docs/assets/js/themes/sunburst.css" />*@

    <script type="text/javascript">    ace.vars['base'] = '..';</script>
    <script src="~/assets/js/ace/elements.onpage-help.js"></script>
    <script src="~/assets/js/ace/ace.onpage-help.js"></script>
    <script src="~/assets/js/date-time/bootstrap-datepicker.js"></script>
    <script src="~/assets/custom/js/bootstrap-datepicker.pt-BR.js"></script>
    <script src="~/assets/js/date-time/bootstrap-timepicker.js"></script>
    <script src="~/assets/js/date-time/moment.js"></script>
    <script src="~/assets/js/date-time/daterangepicker.js"></script>
    <script src="~/assets/js/date-time/bootstrap-datetimepicker.js"></script>
    <script src="~/assets/js/jquery.meio.mask.min.js"></script>
    <script src="~/Scripts/custom.js"></script>
    <script src="~/Scripts/planoMarketing.js"></script>


    @*<script src="~/docs/assets/js/rainbow.js"></script>
        <script src="~/docs/assets/js/language/generic.js"></script>
        <script src="~/docs/assets/js/language/html.js"></script>
        <script src="~/docs/assets/js/language/css.js"></script>
        <script src="~/docs/assets/js/language/javascript.js"></script>*@
    @RenderSection("scripts", required:=False)
    <script>

        $('.open-modal').click(function (e) {
            e.preventDefault();
            $('#modal').toggle();
            scrollTo = $('body').scrollTop();
            $('body').css("position", "fixed");
        });

        $('.close-modal').click(function (e) {
            e.preventDefault();
            $('#modal').toggle();
            $('body').css("position", "static");
            $('body').animate({ scrollTop: scrollTo }, 0);
        });

        jQuery(function ($) {
            /********************************/
            //add tooltip for small view action buttons in dropdown menu
            $('[data-rel="tooltip"]').tooltip({ placement: tooltip_placement });

            //tooltip placement on right or left
            function tooltip_placement(context, source) {
                var $source = $(source);
                var $parent = $source.closest('table')
                var off1 = $parent.offset();

                var w1 = $parent.width();

                var off2 = $source.offset();
                //var w2 = $source.width();

                if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                    return 'right';
            }
        });

        $('.date-picker').datepicker({
            autoclose: true,
            language: 'pt-BR',
            todayHighlight: true
        })
            //show datepicker when clicking on the icon
            .next().on(ace.click_event, function () {
                $(this).prev().focus();
            });

        //or change it into a date range picker
        $('.input-daterange').datepicker({ autoclose: true, language: 'pt-BR' });


        //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
        $('input[name=date-range-picker]').daterangepicker({
            'applyClass': 'btn-sm btn-success',
            'cancelClass': 'btn-sm btn-default',
            locale: {
                applyLabel: 'Apply',
                cancelLabel: 'Cancel',
            }
        })

        ChosenConfig();

    </script>

</body>
</html>
