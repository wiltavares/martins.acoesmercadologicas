﻿@ModelType Tuple(Of String, String, Object)
@Imports Martins.AcoesMercadologicas.Core

<div class="modal modal-confirmation fade" id="confirmationDelete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
                &nbsp;Confirmação de Exclusão
            </div>
            <div class="modal-body">Tem certeza que deseja excluir o registro?</div>
            <div class="modal-footer clearfix">
                <div class="btn-group pull-right">
                    <a href="javascript:confirmarAcao()" class="btn btn-success btn-white">
                        <i class="ace-icon fa fa-check-square-o bigger-120 green"></i>
                        Sim
                    </a>
                    <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                        <i class="ace-icon fa fa-square-o bigger-120 red"></i>
                        Não
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var idModal;
    var divMsg;

    $('#confirmationDelete').on('shown.bs.modal', function (e) {
        idModal = $(e.relatedTarget).attr('data-id');
        divMsg = $(e.relatedTarget).attr('data-msg');
        if(!divMsg){
            divMsg = 'msgGeral';
        }
    })

    function confirmarAcao() {
        $('#confirmationDelete').modal('hide');
        var dadosPesquisa = {Filter: @Html.Raw(Json.Encode(Model.Item3.Filter)), Page: @Html.Raw(Json.Encode(Model.Item3.Page))}
        LoadingOn();
        $.ajax({
            url: '@Url.Action(Model.Item1, Model.Item2)',
            type: 'POST',
            data: { id: idModal, dadosPesquisa: dadosPesquisa},
            success: function (data) {
                $('#' + '@Model.Item3.ResultID').html(data);
                atualizaMensagem(divMsg);
                LoadingOff();
            },
            error : function (){
                atualizaMensagem(divMsg);
                LoadingOff();
            }
        });
    }
</script>