﻿@*<ul class="nav nav-list">
    @Code
        Dim authCookie As HttpCookie = Context.Request.Cookies(FormsAuthentication.FormsCookieName)
        If authCookie Is Nothing OrElse authCookie.Value = "" Then
            Return
        End If

        Dim authTicket As FormsAuthenticationTicket
        Try
            authTicket = FormsAuthentication.Decrypt(authCookie.Value)
        Catch
            Return
        End Try

        ' retrieve roles from UserData
        Dim roles As String() = authTicket.UserData.Split("|")(1).Split(";"c)
    End Code
    @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "1")) Then
        @<li class="" name="Cadastros">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-folder-open-o"></i>
                <span class="menu-text"> Cadastros </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "5")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "Evento")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Evento
                        </a>
                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "6")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "TipoEvento")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Tipo de Evento
                        </a>

                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "7")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "TipoFluxo")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Tipo de Fluxo
                        </a>

                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "8")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "TipoOperacao")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Tipo de Operação
                        </a>

                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "9")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "PlanoDeMarketing")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Plano de Marketing
                        </a>

                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "10")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "Parametros")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Parâmetros
                        </a>

                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "11")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "PoliticasAcaoComercial")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Políticas
                        </a>

                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "16")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "AprovadoresExtra")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Aprovadores Extras
                        </a>

                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "18")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "RelacaoFuncionarioGerenteMarketing")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Funcionário x Ger. Marketing
                        </a>

                        <b class="arrow"></b>
                    </li>
                End If

            </ul>

        </li>
    End If

    @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "2")) Then
        @<li class="open">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-line-chart"></i>
                <span class="menu-text"> Ação Mercadológica </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "12")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "AcaoComercial")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Consulta Ação Mercadológica
                        </a>

                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "14")) Then
                    @<li class="">
                        <a href="@Url.Action("CreateEdit", "AcaoComercial", New With {.id = Nothing})">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Nova Ação Mercadológica
                        </a>
                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "17")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "AcaoComercial", New With {.mhnApr = 1})">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Aprovações
                        </a>
                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "24")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "ConsultaSaldoAcao")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Consulta Saldo Ação
                        </a>

                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "25")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "ConsultaGastoAcao")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Consulta Gasto Ação
                        </a>

                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "26")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "ConsultaSaldoAcaoFornecedor")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Consulta Saldo Ação X Fornecedor
                        </a>

                        <b class="arrow"></b>
                    </li>

                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "27")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "ConsultaSaldoCarimbo")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Consulta Saldo Carimbo
                        </a>
                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "28")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "ConsultaExtratoCarimboEvento")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Consulta Extrato Carimbo por Evento
                        </a>
                        <b class="arrow"></b>
                    </li>
                End If
                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "35")) Then
                    @<li class="">
                        <a href="@Url.Action("Index", "ConsultaAcompanhamentoPreAcordo")">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Consulta Acompanhamento Pré-Acordo
                        </a>
                        <b class="arrow"></b>
                    </li>
                End If

                 @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "37")) Then
                    @<li class="">
                         <a href="@Url.Action("Index", "ConsultaExtratoVerba")">
                             <i class="menu-icon fa fa-caret-right"></i>
                             Extrato De Verbas
                         </a>
                        <b class="arrow"></b>
                    </li>
                 End If
            </ul>
        </li>
    End If

    @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = "3")) Then
        @<li class="">
            <a href="@Url.Action("Index", "ControleAcesso")">
                <i class="menu-icon fa fa-gears"></i>
                <span class="menu-text"> Controle de Acesso </span>
            </a>
            <b class="arrow"></b>
        </li>
    End If


</ul>*@