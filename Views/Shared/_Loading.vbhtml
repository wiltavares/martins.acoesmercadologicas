﻿<div class="modal fade loading-back" id="loadingDialog" data-backdrop="true" data-keyboard="false" style="z-index:1053">
    <div class="modal-dialog modal-loading">
        <div class="modal-content">
            <div class="modal-header ">
                Carregando ...
            </div>
            <div class="modal-body">
                <div id="ajax_loader">
                    <img src="~/assets/custom/images/loading_bar.gif" style="display: block; margin-left: auto; margin-right: auto; ">
                </div>
            </div>
        </div>
    </div>
</div>
