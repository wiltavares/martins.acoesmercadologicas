﻿@ModelType List(Of Menu)
@Imports Martins.AcoesMercadologicas.Core
<ul class="nav nav-list">
    @Code
        Dim authCookie As HttpCookie = Context.Request.Cookies(FormsAuthentication.FormsCookieName)
        If authCookie Is Nothing OrElse authCookie.Value = "" Then
            Return
        End If

        Dim authTicket As FormsAuthenticationTicket
        Try
            authTicket = FormsAuthentication.Decrypt(authCookie.Value)
        Catch
            Return
        End Try

        ' retrieve roles from UserData
        Dim roles As String() = authTicket.UserData.Split("|")(1).Split(";"c)
        Dim RUrl As String = Request.RawUrl
        Dim classli As String = ""
    End Code
    @For Each MenuPai As Menu In (From item In Model Where item.cdMenuPai Is Nothing Order By item.NumOrdem)
        @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = MenuPai.cdMenu)) Then
            @code
            Dim Filhos = (From item In Model Where item.cdMenuPai = MenuPai.cdMenu Order By item.NumOrdem)
            Dim FilhoSelecionado = (From item In Filhos Where RUrl.Contains(Url.Action(item.Metodo, item.Controler)))
            End Code
            @If Filhos.Count > 0 Then
                @If FilhoSelecionado.Count > 0 Then
                    @<li class="open">
                        <a href="#" class="dropdown-toggle">
                            @Html.Raw(String.Format("<i class='{0}'></i>", MenuPai.ClassIcone))
                            <span class="menu-text"> @MenuPai.NomMenu </span>

                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class="submenu">
                            @For Each MenuFilho As Menu In Filhos
                                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = MenuFilho.cdMenu)) Then
                                    @If (MenuFilho.cdMenu <> "14" And MenuFilho.cdMenu <> "24" And RUrl.Contains(Url.Action(MenuFilho.Metodo, MenuFilho.Controler, MenuFilho.Parametro))) _
                                        Or ((MenuFilho.cdMenu = "14" Or MenuFilho.cdMenu = "24") And RUrl = (Url.Action(MenuFilho.Metodo, MenuFilho.Controler, MenuFilho.Parametro))) _
                                        Or (MenuFilho.cdMenu = "12" And (RUrl.Contains("/AcaoComercial/CreateEdit/"))
                                            ) Then
                                    classli = "active"
                                    Else
                                    classli = ""
                                    End If
                                If (MenuFilho.cdMenu = 32) Then
                                    @<li class="open">
                                        <a href="#" class="dropdown-toggle">
                                            @Html.Raw(String.Format("<i class='{0}'></i>", MenuFilho.ClassIcone))
                                            <span class="menu-text"> @MenuFilho.NomMenu </span>

                                            <b class="arrow fa fa-angle-down"></b>
                                        </a>
                                        <b class="arrow"></b>
                                        <ul class="submenu">
                                            @If (classli = "active") Then
                                            If Not RUrl.Contains("/AcaoComercial/PreAcordoCreateEditVigencia") Then
                                            classli = ""
                                            End If
                                            End If

                                            @Html.Raw(String.Format("<li class='{2}'><a href='{0}'><i class='menu-icon fa fa-caret-right'></i>{1}</a><b class='arrow'></b></li>", Url.Action("PreAcordoCreateEditVigencia", MenuFilho.Controler, MenuFilho.Parametro), "Limitado por Vigência", classli))
                                            @If RUrl.Contains("/AcaoComercial/PreAcordoCreateEditValor") Then
                                            classli = "active"
                                            Else
                                            classli = ""
                                            End If

                                            @Html.Raw(String.Format("<li class='{2}'><a href='{0}'><i class='menu-icon fa fa-caret-right'></i>{1}</a><b class='arrow'></b></li>", Url.Action("PreAcordoCreateEditValor", MenuFilho.Controler, MenuFilho.Parametro), "Limitado por Valor", classli))
                                        </ul>
                                    </li>
                                Else
                                    @Html.Raw(String.Format("<li class='{2}'><a href='{0}'><i class='menu-icon fa fa-caret-right'></i>{1}</a><b class='arrow'></b></li>", Url.Action(MenuFilho.Metodo, MenuFilho.Controler, MenuFilho.Parametro), MenuFilho.NomMenu, classli))
                                End If
                                End If
                            Next
                        </ul>
                    </li>
                Else
                    @<li>
                        <a href="#" class="dropdown-toggle">
                            @Html.Raw(String.Format("<i class='{0}'></i>", MenuPai.ClassIcone))
                            <span class="menu-text"> @MenuPai.NomMenu </span>

                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class="submenu">
                            @For Each MenuFilho As Menu In Filhos
                                @If Not String.IsNullOrEmpty(Array.Find(roles, Function(s) s = MenuFilho.cdMenu)) Then
                                classli = ""
                                If (MenuFilho.cdMenu = 32) Then
                                    @<li class="open">
                                        <a href="#" class="dropdown-toggle">
                                            @Html.Raw(String.Format("<i class='{0}'></i>", MenuFilho.ClassIcone))
                                            <span class="menu-text"> @MenuFilho.NomMenu </span>

                                            <b class="arrow fa fa-angle-down"></b>
                                        </a>
                                        <b class="arrow"></b>
                                        <ul class="submenu">
                                            @Html.Raw(String.Format("<li class='{2}'><a href='{0}'><i class='menu-icon fa fa-caret-right'></i>{1}</a><b class='arrow'></b></li>", Url.Action("PreAcordoCreateEditVigencia", MenuFilho.Controler, MenuFilho.Parametro), "Limitado por Vigência", classli))
                                            @Html.Raw(String.Format("<li class='{2}'><a href='{0}'><i class='menu-icon fa fa-caret-right'></i>{1}</a><b class='arrow'></b></li>", Url.Action("PreAcordoCreateEditValor", MenuFilho.Controler, MenuFilho.Parametro), "Limitado por Valor", classli))
                                        </ul>
                                    </li>
                                Else
                                    @Html.Raw(String.Format("<li class='{2}'><a href='{0}'><i class='menu-icon fa fa-caret-right'></i>{1}</a><b class='arrow'></b></li>", Url.Action(MenuFilho.Metodo, MenuFilho.Controler, MenuFilho.Parametro), MenuFilho.NomMenu, classli))
                                End If
                                End If
                            Next
                        </ul>
                    </li>
                End If
            End If

        End If

    Next
</ul>