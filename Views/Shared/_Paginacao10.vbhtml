﻿@Imports PagedList.Mvc
@Imports Martins.AcoesMercadologicas.Core
@Code

    Dim ordena = If(Model.Page.OrderBy, "")
    Dim pageNumber = If(Model.Page.PageNumber, Constantes.PAGE_NUMBER_DEFAULT)
    Dim pageSize = If(Model.Page.PageSize, Constantes.PAGE_SIZE_DEFAULT)
    Dim totalCount = If(Model.TotalCount, 0)
    Dim urlAction = Url.Action(Model.ActionName)

    If Model.ControllerName IsNot Nothing Then
        urlAction = Url.Action(Model.ActionName, Model.ControllerName)
    End If

    Dim atualizarPageSize = "atualizarPageSize_" & Model.ResultID & "()"
    Dim mudarDePagina = "mudarDePagina_" & Model.ResultID

    Dim fnAtualizarGrid As String = "function atualizarGrid_" & Model.ResultID & "(pageConf) { var dadosPesquisa = {'Filter' : " &
     Newtonsoft.Json.JsonConvert.SerializeObject(Model.Filter) & ", 'ResultID' : '" & Model.ResultID &
    "', 'PaginationID' : '" & Model.PaginationID & "', 'ActionName': '" & Model.ActionName &
    "', 'Page' : pageConf, 'TotalCount' : " & Model.TotalCount & "}; " & " atualizarGrid(pageConf, dadosPesquisa,'" & urlAction & "');}"

    Dim fnOrdenarGrid As String = "function ordenarGrid_" & Model.ResultID & "(orderBy) { var pageConf = {'pageSize' : " &
pageSize & ", 'pageNumber' : " & Constantes.PAGE_NUMBER_DEFAULT & ", 'orderBy' : orderBy}; atualizarGrid_" & Model.ResultID & "(pageConf);}"

    Dim fnAtualizarPageSize As String = "function atualizarPageSize_" & Model.ResultID & "() { var pageConf = {'pageSize' : " &
"$('#" & Model.PaginationID & " label.active input').val(), 'pageNumber' : " & Constantes.PAGE_NUMBER_DEFAULT &
", 'orderBy' : '" & ordena & "'}; atualizarGrid_" & Model.ResultID & "(pageConf);}"

    Dim fnMudarDePagina As String = "function mudarDePagina_" & Model.ResultID & "(currPage) { var pageConf = {'pageSize' : " &
    pageSize & ", 'pageNumber' : currPage, 'orderBy' : '" & ordena & "'}; atualizarGrid_" & Model.ResultID & "(pageConf);}"

End Code

<div class="row paginacao">
    <div class="col-sm-4">
        <div data-toggle="buttons" id="@Model.PaginationID" class="btn-group" onchange="@Html.Raw(atualizarPageSize)">
            <Label Class="btn btn-info btn-xs @Grid.AtivarPageSize(pageSize, 10)">
                @Html.RadioButton("10", 10, If(pageSize = 10, True, False))
                10
            </Label>
            <label class="btn btn-info btn-xs @Grid.AtivarPageSize(pageSize, 25)">
                @Html.RadioButton("25", 25, If(pageSize = 25, True, False))
                25
            </label>
            <Label Class="btn btn-info btn-xs @Grid.AtivarPageSize(pageSize, 50)">
                @Html.RadioButton("50" & "Radio", 50, If(pageSize = 50, True, False))
                50
            </Label>
            <Label Class="btn btn-info btn-xs @Grid.AtivarPageSize(pageSize, 100)">
                @Html.RadioButton("100", 100, If(pageSize = 100, True, False))
                100
            </Label>
        </div>
        <p>Exibindo @Model.ListPaged.Count de @totalCount</p>
    </div>
    <div class="col-sm-8">
        <div class="text-right">
            @Html.PagedListPager(Model.ListPaged, Function(page) "javascript:" + mudarDePagina + "(" + page.ToString() + ")")
        </div>
    </div>
</div>
<Script>
    $('th[data-href]').on("click", function () {
        document.location = $(this).data('href');
    });

    @Html.Raw(fnAtualizarGrid)
    @Html.Raw(fnOrdenarGrid)
    @Html.Raw(fnAtualizarPageSize)
    @Html.Raw(fnMudarDePagina)

</Script>
