﻿@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim alerts = If(TempData.ContainsKey(Core.Alert.TempDataKey), DirectCast(TempData(Core.Alert.TempDataKey), List(Of Core.Alert)), New List(Of Core.Alert)())
    Dim scrollToMsg = If(alerts.Count > 0, "$(""body"").scrollTo(""targetMsgScroll"");", "")
    Dim id = 0
End Code
@For i = 0 To alerts.Count - 1
    Dim alert = alerts(i)
    Dim alertDismiss = If(alert.Dismissable, "alert-dismissible", "")

    Dim autoClose = IIf(alert.AutoClose, "auto-close", "")
    If (Not alert.Exception Is Nothing) Then
        autoClose = ""
    End If

    Dim idCollapse = "collapse" & i
    Dim targedCollapse = "#" & idCollapse

    @<div Class="alert @autoClose alert-@alert.AlertStyle fade in @alertDismiss">
        @If alert.Dismissable Then
            @<a href="#" Class="close" data-dismiss="alert" aria-label="close">&times;</a>
        End If
        @If alert.Exception Is Nothing Then
            @Html.Raw(alert.Message)
        Else
            @<div class="accordion-toggle collapsed smaller @alert.AlertStyle" data-toggle="collapse" data-parent="#accordion" href="@targedCollapse" aria-expanded="false" style="cursor:pointer">
                <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                @Html.Raw(alert.Exception.Message)
            </div>
                @<div Class="panel-collapse collapse smaller @alert.AlertStyle" id="@idCollapse" aria-expanded="false" style="height: 0px;">
                    <br />
                    Tipo:
                    @HttpContext.Current.Server.HtmlDecode(alert.Exception.GetType().ToString())
                    <br />
                    Trace:
                    @HttpContext.Current.Server.HtmlDecode(alert.Exception.StackTrace)
                </div>
        End If
    </div>
Next

<script>

    window.setTimeout(function () {
        $(".alert.auto-close").alert('close')
    }, 8000);

    if (@IIf(alerts.Count > 0, "true", "false") == true){
        $('html, body').scrollTop(0);
        $('html, body .modal').scrollTop(0);
    }

</script>
