﻿@ModelType Tuple(Of String, String)

<div class="modal modal-confirmation fade" id="confirmationDelete1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
                &nbsp;Confirmação de Exclusão
            </div>
            <div class="modal-body">Tem certeza que deseja excluir o registro?</div>
            <div class="modal-footer clearfix">
                <div class="btn-group pull-right">
                    <a href="@Url.Action(Model.Item1, Model.Item2)" class="btn btn-success btn-white">
                        <i class="ace-icon fa fa-check-square-o bigger-120 green"></i>
                        Sim
                    </a>
                    <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                        <i class="ace-icon fa fa-square-o bigger-120 red"></i>
                        Não
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>