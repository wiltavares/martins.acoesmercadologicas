﻿@ModelType ModalConfirmacaoViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim metodoConfirma = Model.BtnSimAcao & "(" & Json.Encode(Model.Objeto) & ")"
    Dim metodoNaoConfirma = Model.BtnNaoAcao & "()"
End Code
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
            &nbsp; @Html.Raw(Model.Titulo)
        </div>
        <div class="modal-body">@Html.Raw(Model.Mensagem)</div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <a onclick="fechaModalConfirmacao(); @metodoConfirma" class="btn btn-success btn-white">
                    <i class="ace-icon fa fa-check-square-o bigger-120 green"></i>
                    @Html.Raw(Model.BtnSimNome)
                </a>
                <a onclick="@metodoNaoConfirma" class="btn btn-danger btn-white">
                    <i class="ace-icon fa fa-square-o bigger-120 red"></i>
                    @Html.Raw(Model.BtnNaoNome)
                </a>
            </div>
        </div>
    </div>
</div>

<script>

    function fechaModalConfirmacao() {
        var div = @Model.IdDiv
        $(div).modal('hide');
    }
</script>