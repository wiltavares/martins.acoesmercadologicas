﻿@ModelType GridSettings(Of PoliticasAcaoComercialViewModel)
@Imports Martins.AcoesMercadologicas.Core

@Html.Partial(Constantes.VIEW_MODAL_CONFIRMACAO, New Tuple(Of String, String, Object)("Deletar", "PoliticasAcaoComercial", Model))

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 250px;">
                    <thead>
                        <tr role="row">
                            <th class="center">Ações</th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODPOLACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODPOLACOCMC", Model.Page.OrderBy)">
                                Código
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESPOLACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESPOLACOCMC", Model.Page.OrderBy)">
                                Tipo de Fluxo
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td Class="center">
                                    <div Class="hidden-sm hidden-xs btn-group">
                                        <a data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" href="@Url.Action("CreateEdit", New With {.id = item.CODPOLACOCMC})">
                                            <i class="ace-icon fa fa-pencil bigger-119"></i>
                                        </a>
                                        @*<a Class="btn btn-xs btn-danger" data-toggle="modal" data-target="#confirmationDelete" data-id="@item.CODPOLACOCMC">
                                            <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                        </a>*@
                                    </div>

                                    <div Class="hidden-md hidden-lg">
                                        <div Class="inline pos-rel">
                                            <button class="btn btn-minier btn-md btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                                <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                            </button>

                                            <ul Class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                <li>
                                                    <a href="@Url.Action("Edit", New With {.id = item.CODPOLACOCMC})" Class="tooltip-info" data-rel="tooltip" title="" data-original-title="Editar">
                                                        <span Class="blue">
                                                            <i Class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" Class="tooltip-info " data-rel="tooltip" title="" data-original-title="Excluir"
                                                       data-toggle="modal" data-target="#confirmationDelete" data-id="@item.CODPOLACOCMC">
                                                        <span Class="red">
                                                            <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    @Html.Label(CInt(item.CODPOLACOCMC))
                                </td>
                                <td>
                                    @Html.Label(item.DESPOLACOCMC.Trim())
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>