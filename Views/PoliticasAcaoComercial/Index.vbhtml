﻿@ModelType GridSettings(Of PoliticasAcaoComercialViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Consultar Politicas Ação Comercial"
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Políticas</li>
    </ul>
</div>
<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div> 

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "PoliticasAcaoComercial", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-3">
                            @Html.Label("lblCodigo", "Código:")
                            @Html.TextBoxFor(Function(m) m.Filter.CODPOLACOCMC, New With {.Class = "form-control maskNumber"})
                          
                        </div>
                        <div Class="col-sm-6">
                            @Html.Label("lblTipoDeFluxo", "Descrição:")
                            @Html.TextBoxFor(Function(m) m.Filter.DESPOLACOCMC, New With {.Class = "form-control"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white ">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger-120"></i>
                                    Limpar
                                </a>
                                <a href="@Url.Action("CreateEdit")" class="btn btn-success btn-white">
                                    <i class="ace-icon fa fa-plus bigger-120"></i>
                                    Cadastrar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<br class="clearfix" />

<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/PoliticasAcaoComercial/_resultadosView.vbhtml", Model)
    End Code
</div>
