﻿@ModelType GridSettings(Of RelacaoFuncionarioGerenteMarketingViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code

    ViewData("Title") = "Funcionário x Gerente de Marketing"

End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Funcionário x Gerente de Marketing</li>
    </ul>
</div>
<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modalFuncionarios"></div>
<div class="modal fade" id="modal"></div>
<div class="modal fade" id="modalResultImport"></div>
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "RelacaoFuncionarioGerenteMarketing", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-12" id="modalFuncGerente">
                            @Html.HiddenFor(Function(Model) Model.Filter.CODFUNC_GER_MTK, New With {.Class = "form-control", .id = "idCodFuncionarioGerente"})
                            @Html.Label("lblNomFunc", "Usuário Gerente de Marketing:")
                            <div class="input-group" id="dividItem">
                                @Html.TextBoxFor(Function(Model) Model.Filter.NOMUNC_GER_MTK, New With {.Class = "form-control", .id = "nomFuncionarioGerente"})
                                <span class="input-group-btn">
                                    <button class="btn btn-white" id="btBuscaFunc" type="button" onclick="modalBuscaFuncionarios()" style="display: block">
                                        <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                    </button>
                                    <button class="btn btn-white" id="btTiraFunc" type="button" onclick="tiraFuncionarioGerente()" style="display: none">
                                        <i class="ace-icon fa fa-close bigger-120 red"></i>
                                    </button>
                                </span>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12" id="modalFuncEmitente">
                            @Html.HiddenFor(Function(Model) Model.Filter.CODFUNC_EMITENTE, New With {.Class = "form-control", .id = "idCodFuncionarioEmitente"})
                            @Html.Label("lblNomFunc", "Funcionário Emitente:")
                            <div class="input-group" id="dividItem">
                                @Html.TextBoxFor(Function(Model) Model.Filter.NOMFUNC_EMITENTE, New With {.Class = "form-control", .id = "nomFuncionarioEmitente"})
                                <span class="input-group-btn">
                                    <button class="btn btn-white" id="btBuscaFuncEmitente" type="button" onclick="modalBuscaFuncionariosEmitente()" style="display: block">
                                        <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                    </button>
                                    <button class="btn btn-white" id="btTiraFuncEmitente" type="button" onclick="tiraFuncionarioEmitente()" style="display: none">
                                        <i class="ace-icon fa fa-close bigger-120 red"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull left">
                                @*<button type="submit" class="btn btn-white btn-success">
                                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                                    Adicionar\Alterar
                                </button>*@
                                @*<a href="@Url.Action("Adicionar")" class="btn btn-success btn-white">
                                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                                    Adicionar\Alterar
                                </a>*@


                                <a class="btn btn-info btn-white" onclick="adicionaRelacaoFuncGerMtk()">
                                    <i class="ace-icon fa fa-search bigger"></i>
                                    Adicionar\Alterar
                                </a>

                                <button type="submit" class="btn btn-info btn-white ">
                                    <i class="ace-icon fa fa-search bigger"></i>
                                    Pesquisar
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            End Using

        </div>
    </div>
</div>
<br class="clearfix" />

<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/RelacaoFuncionarioGerenteMarketing/_ResultadosView.vbhtml", Model)
    End Code
</div>



<script src="~/Scripts/modalFuncionarios.js"></script>