﻿@ModelType GridSettings(Of RelacaoFuncionarioGerenteMarketingViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Relação Funcionario x Gerente Marketing
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width:auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th>
                                Ações
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODFUNC_EMITENTE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFUNC_EMITENTE", Model.Page.OrderBy)">
                                Código Funcionário
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMFUNC_EMITENTE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFUNC_EMITENTE", Model.Page.OrderBy)">
                                Nome Funcionário
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODFUNC_GER_MTK", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFUNC_GER_MTK", Model.Page.OrderBy)">
                                Código Gerente Marketing
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMUNC_GER_MTK", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMUNC_GER_MTK", Model.Page.OrderBy)">
                                Nome Gerente Marketing
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each Item In Model.ListPaged
                            @<tr>
                                <td class="center">
                                    <div class="hidden-sm hidden-xs btn-group left">

                                        <a class="btn btn-xs btn-danger" title="Deletar" onclick="apagaRelacaoFuncGerMtk(@item.CODFUNC_EMITENTE, '@item.CODFUNC_GER_MTK')">
                                            <i class="ace-icon fa fa-trash-o bigger-120">

                                            </i>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    @Html.Label(item.CODFUNC_EMITENTE)
                                </td>
                                <td>
                                    @Html.Label(item.NOMFUNC_EMITENTE)
                                </td>
                                <td>
                                    @Html.Label(item.CODFUNC_GER_MTK)
                                </td>
                                <td>
                                    @Html.Label(item.NOMUNC_GER_MTK)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>

@*<script src="~/Scripts/modalFuncionarios.js"></script>*@