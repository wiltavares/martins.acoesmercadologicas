﻿@ModelType GridSettings(Of FuncionarioViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Funcionários
        </div>
        <div class="modal-body">
            <div id="msgBuscaFuncionarios">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            @Using (Ajax.BeginForm("PesquisarFuncionarios", "RelacaoFuncionarioGerenteMarketing", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="row">
                    <div class="col-sm-6">
                        @Html.Hidden("idFilto", "idFiltro:", New With {.id = "idFiltroFunc"})
                        @Html.Label("lblMatricula", "Matrícula:")
                        @Html.TextBoxFor(Function(m) m.Filter.CODFNC, New With {.Class = "form-control", .id = "FilterCodFuncionario"})
                    </div>
                    <div class="col-sm-6">
                            @Html.Label("lblMatricula", "Funcionário:")
                        <div class="input-group">
                            @Html.TextBoxFor(Function(m) m.Filter.NOMFNC, New With {.Class = "form-control", .id = "FilternomeFuncionario"})
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-white" type="button" onclick="BuscaFuncionarios()">
                                    <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            End Using
            <div id="resultadosFuncionarios">
                @Code
                    Html.RenderPartial("~/Views/RelacaoFuncionarioGerenteMarketing/_ResultadosViewFuncionarios.vbhtml", Model)
                End Code
            </div>
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <button class="btn btn-danger btn-white" onclick="btnCancelar()">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<script src="~/Scripts/modalFuncionarios.js"></script>