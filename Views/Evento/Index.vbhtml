﻿@ModelType GridSettings(Of EventoViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Eventos"
End Code
<div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Evento</li>
    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modal"></div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "Evento", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-2 col-sm-6">
                            @Html.Label("acaoID", "Evento:")
                            @Html.TextBoxFor(Function(Model) Model.Filter.CODEVTACOCMC, New With {.Class = "form-control"})
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("nomeEvento", "Nome do Evento:")
                            @Html.TextBoxFor(Function(Model) Model.Filter.NOMEVTACOCMC, New With {.Class = "form-control"})
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("status", "Tipo Evento:")
                            @Html.DropDownListFor(Function(m) m.Filter.TIPEVTACOCMC, New SelectList(ViewBag.tipoEvento, "TIPEVTACOCMC", "NOMTIPEVTACOCMC", Model.Filter.TIPEVTACOCMC), "Selecione...", New With {.Class = "form-control chosen-select"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white ">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger-120"></i>
                                    Limpar
                                </a>
                                <a href="@Url.Action("CreateEdit")" class="btn btn-success btn-white">
                                    <i class="ace-icon fa fa-plus bigger-120"></i>
                                    Cadastrar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<br class="clearfix" />

<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/Evento/_ResultadosView.vbhtml", Model)
    End Code
</div>
