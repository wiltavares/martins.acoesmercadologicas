﻿@ModelType EventoViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim tituloTela = IIf(Model Is Nothing OrElse Model.CODEVTACOCMC Is Nothing, "Incluir Evento", "Editar Evento")
    ViewData("Title") = tituloTela
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Evento</li>
        @Code
            Dim campo = IIf(ViewBag.acao = "Save", "Incluir", "Editar ")
        End Code
        <li class="active">@campo</li>
    </ul>
</div>
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-file-text-o bigger-110"></i>
            Dados
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Html.BeginForm(ViewBag.acao, "Evento"))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-2 col-sm-6">
                            @Html.Label("acaoID", "Evento:")
                            @Html.TextBox("CODEVTACOCMC", Model.CODEVTACOCMC, Model.CODEVTACOCMC.ToString, New With {.Class = "form-control", .readonly = "readonly"})
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("comeEvento", "Nome do Evento (*):")
                            @Html.TextBox("NOMEVTACOCMC", Model.NOMEVTACOCMC, New With {.Class = "form-control"})
                            @Html.ValidationMessageFor(Function(m) m.NOMEVTACOCMC, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("status", "Tipo Evento (*):")
                            @Html.DropDownListFor(Function(m) m.TIPEVTACOCMC, New SelectList(ViewBag.tipoEvento, "TIPEVTACOCMC", "NOMTIPEVTACOCMC", Model.TIPEVTACOCMC), "Selecione...", New With {.Class = "form-control chosen-select"})
                            @Html.ValidationMessageFor(Function(m) m.TIPEVTACOCMC, "", New With {.class = "text-danger-message-login"})
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("empenho", "Empenho (*):")
                            @Html.DropDownListFor(Function(m) m.TIPDSNDSCBNF, New SelectList(ViewBag.empenho, "TIPDSNDSCBNF", "DESDSNDSCBNF", Model.TIPDSNDSCBNF), "Selecione...", New With {.Class = "form-control chosen-select"})
                            @Html.ValidationMessageFor(Function(m) m.TIPDSNDSCBNF, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("eventoAdicional", "Evento Adicional:")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDEVTADIACOCMCDES, "Sim")
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDEVTADIACOCMCDES, "Não")
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("politica", "Política Divergente:")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDEVTMTDPOLDVRDES, "Sim")
                                    @Html.Label("sim2", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDEVTMTDPOLDVRDES, "Não")
                                    @Html.Label("nao2", "Não")
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("VinculoPromocao", "Vinculação Promoção:")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDVCLPMCDES, "Sim")
                                    @Html.Label("sim3", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDVCLPMCDES, "Não")
                                    @Html.Label("nao3", "Não")
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-white btn-success ">
                                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                                    Salvar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-white btn-danger">
                                    <i class="ace-icon fa fa-times bigger-120 red"></i>
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>