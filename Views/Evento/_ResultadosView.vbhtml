﻿@ModelType GridSettings(Of EventoViewModel)
@Imports Martins.AcoesMercadologicas.Core

@Html.Partial(Constantes.VIEW_MODAL_CONFIRMACAO, New Tuple(Of String, String, Object)("Delete", "Evento", Model))

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div>
                <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="center">Ações</th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODEVTACOCMC", Model.Page.OrderBy)">
                                Código
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMEVTACOCMC", Model.Page.OrderBy)">
                                Nome
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("TIPEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("TIPEVTACOCMC", Model.Page.OrderBy)">
                                Tipo Evento
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("TIPDSNDSCBNF", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("TIPDSNDSCBNF", Model.Page.OrderBy)">
                                Empenho
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDEVTADIACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDEVTADIACOCMC", Model.Page.OrderBy)">
                                Adicional
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                 <td Class="center">
                                     <div Class="hidden-sm hidden-xs btn-group">
                                         <a data-rel="tooltip" title="Editar" Class="btn btn-xs btn-info" href="@Url.Action("CreateEdit", New With {.id = item.CODEVTACOCMC})">
                                             <i Class="ace-icon fa fa-pencil bigger-119"></i>
                                         </a>
                                         @*<a data-rel="tooltip" title="Deletar" Class="btn btn-xs btn-danger" data-toggle="modal" data-target="#confirmationDelete" data-id="@item.CODEVTACOCMC">
                                             <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                         </a>*@
                                     </div>

                                     <div Class="hidden-md hidden-lg">
                                         <div Class="inline pos-rel">
                                             <Button Class="btn btn-minier btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                                 <i Class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                             </Button>

                                             <ul Class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                 <li>
                                                     <a href="@Url.Action("CreateEdit", New With {.id = item.CODEVTACOCMC})" Class="tooltip-info" data-rel="tooltip" title="" data-original-title="Editar">
                                                         <span Class="blue">
                                                             <i Class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                                         </span>
                                                     </a>
                                                 </li>
                                                 <li>
                                                     <a Class="tooltip-info" data-toggle="modal" data-target="#confirmationDelete" data-id="@item.CODEVTACOCMC">
                                                         <span class="red">
                                                             <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                                         </span>
                                                     </a>
                                                 </li>
                                             </ul>
                                         </div>
                                     </div>
                                 </td>
                                <td>
                                    @Html.Label(CInt(item.CODEVTACOCMC))
                                </td>
                                <td>
                                    @Html.Label(item.NOMEVTACOCMC)
                                </td>
                                <td>
                                    @Html.Label(item.NOMTIPEVTACOCMC)
                                </td>
                                <td>
                                    @Html.Label(item.DESDSNDSCBNF)
                                </td>
                                <td>
                                    @Html.Label(item.INDEVTADIACOCMCDES)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>
