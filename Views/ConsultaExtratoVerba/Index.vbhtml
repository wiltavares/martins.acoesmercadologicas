﻿@ModelType GridSettings(Of ExtratoVerbaViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Consulta Extrato de Verba"
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-bar-chart"></i>
            Relatórios
        </li>
        <li class="active"> Extrato de Verba</li>
    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modal"></div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "ConsultaExtratoVerba", Constantes.AJAX_OBJECT_POST_DEFAULT, New With {.id = "ConsultaExtratoVerba"}))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-4">
                            <br />
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(m) m.Filter.TipoOperacao, "EXTRATO", New With {.id = "TipoOperacaoExtrato", .onchange = "rblTipoOperacaoExtratoExtrato_checkedChange();"})
                                    @Html.Label("TipoOperacaoExtrato", "Extrato")
                                </div>
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(m) m.Filter.TipoOperacao, "SALDO", New With {.id = "TipoOperacaoSaldo", .onchange = "rblTipoOperacaoExtratoSaldo_checkedChange();"})
                                    @Html.Label("TipoOperacaoSaldo", "Saldo")
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4  visible-extrato">
                            @Html.Label("data", "Período:")
                            @*@Html.HiddenFor(Function(m) m.Filter.DataIniAux, New With {.id = "dataIniAux"})
                                @Html.HiddenFor(Function(m) m.Filter.DataFimAux, New With {.id = "dataFimAux"})*@
                            <div class="input-daterange input-group">
                                @Html.TextBoxFor(Function(m) m.Filter.DataIni, "{0:dd/MM/yyyy}", New With {.class = "input-sm date-interval form-control", .id = "dataIni", .autocomplete = "off"})
                                <span class="input-group-addon">
                                    à
                                </span>
                                @Html.TextBoxFor(Function(m) m.Filter.DataFim, "{0:dd/MM/yyyy}", New With {.class = "input-sm date-interval form-control", .id = "dataFim", .autocomplete = "off"})
                            </div>
                        </div>
                        <div class="col-sm-4 visible-extrato">
                            <br />
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(m) m.Filter.TipoVisao, "FORNECEDOR", New With {.id = "TipoVisaoFornecedor", .onchange = "rblTipoVisaoFornecedor_checkedChange();"})
                                    @Html.Label("TipoVisaoFornecedor", "Fornecedor")
                                </div>
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(m) m.Filter.TipoVisao, "EVENTO", New With {.id = "TipoVisaoEvento", .onchange = "rblTipoVisaoEvento_checkedChange();"})
                                    @Html.Label("TipoVisaoEvento", "Evento")
                                </div>
                            </div>
                        </div>
                    </div>
                    <div Class="row">
                        <div Class="col-sm-4">
                            @Html.Label("lblNomFornecedor", "Fornecedor:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODFRN, New SelectList(ViewBag.FiltroFornecedor, "CODFRN", "CodNomFrn", Model.Filter.CODFRN), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFornecedores"})
                        </div>
                        <div Class="col-sm-4">
                            @Html.Label("Comprador", "Comprador:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODCPR, New SelectList(ViewBag.FiltroComprador, "CODCPR", "CODNOMCPR", Model.Filter.CODCPR), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFiltroComprador"})
                        </div>
                        <div class="col-sm-4">
                            @Html.Label("Celula", "Célula:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODDIVCMP, New SelectList(ViewBag.FiltroCelula, "CODDIVCMP", "CODDESDIVCMP", Model.Filter.CODDIVCMP), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFiltroCelula"})
                        </div>

                    </div>
                    <br />

                    <div Class="row">
                        <div class="col-sm-4">
                            @Html.Label("Diretoria", "Diretoria:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODDRTCMP, New SelectList(ViewBag.FiltroDiretoria, "CODDRTCMP", "CODDESDRTCMP", Model.Filter.CODDRTCMP), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFiltroDiretoria"})
                        </div>
                        <div Class="col-sm-4 visible-evento" style="display:none">
                            @Html.Label("lblTipoEvento", "Tipo de Evento:")
                            @Html.DropDownListFor(Function(m) m.Filter.TIPEVTACOCMC, New SelectList(ViewBag.FiltroTipoEvento, "TIPEVTACOCMC", "CODIGO_NOME", Model.Filter.CODFRN), "Selecione...", New With {.Class = "chosen-select form-control", .id = "drpFiltroTipoEvento"})
                        </div>
                        <div Class="col-sm-4 visible-evento" style="display:none">
                            @Html.Label("lblEvento", "Evento:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODEVTACOCMC, New SelectList(ViewBag.FiltroEvento, "CODEVTACOCMC", "CODIGO_NOME", Model.Filter.CODCPR), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFiltroEvento"})
                        </div>
                    </div>
                    <br />
                    <div Class="row">
                        <div Class="col-sm-12">
                            <div Class="btn-group pull-left">
                                <Button type="submit" Class="btn btn-info btn-white ">
                                    <i Class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </Button>
                                <a href="@Url.Action("Index")" Class="btn btn-grey btn-white">
                                    <i Class="ace-icon fa fa-eraser bigger"></i>
                                    Limpar
                                </a>
                                <a onclick="btnExportarExtratoVerba()" href="#" Class="btn btn-info btn-white" id="btnImprimirRel">
                                    <i Class="ace-icon fa fa-file-excel-o bigger-120"></i>
                                    Exportar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


            End Using

        </div>
    </div>
</div>
<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/ConsultaExtratoVerba/_ResultadosView.vbhtml", Model)
    End Code
</div>

<script src="~/Scripts/consultaExtratoVerba.js"></script>

