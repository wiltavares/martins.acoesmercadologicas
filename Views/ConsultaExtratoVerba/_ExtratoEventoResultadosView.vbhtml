﻿@ModelType GridSettings(Of ExtratoVerbaViewModel)
@Imports Martins.AcoesMercadologicas.Core



<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, " hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            <div>
                <div style="width: auto; overflow-x: scroll;">
                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 600px;">
                        <thead>
                            <tr role="row">
                                <th class="@Grid.AtribuirIconeOrdenacao("CODDIVCMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODDIVCMP", Model.Page.OrderBy)">
                                    Cod Celula
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("DESDIVCMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESDIVCMP", Model.Page.OrderBy)">
                                    Celula
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("CODDRTCMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODDRTCMP", Model.Page.OrderBy)">
                                    Cod Diretoria                        
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("DESDRTCMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESDRTCMP", Model.Page.OrderBy)">
                                    Diretoria
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy)">
                                    Cod Fornecedor
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy)">
                                    Fornecedor
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("CODACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODACOCMC", Model.Page.OrderBy)">
                                    Cod Ação
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("NOMACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMACOCMC", Model.Page.OrderBy)">
                                    Ação
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("DATAPVACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATAPVACOCMC", Model.Page.OrderBy)">
                                    Dt. Ação
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("TIPEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("TIPEVTACOCMC", Model.Page.OrderBy)">
                                    Cod. Tipo do Evento
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("NOMTIPEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMTIPEVTACOCMC", Model.Page.OrderBy)">
                                    Tipo do Evento
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("CODEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODEVTACOCMC", Model.Page.OrderBy)">
                                    Cod. Evento
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("NOMEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMEVTACOCMC", Model.Page.OrderBy)">
                                    Evento
                                </th>
                                @*<th class="@Grid.AtribuirIconeOrdenacao("TIPDSNDSCBNF", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("TIPDSNDSCBNF", Model.Page.OrderBy)">
            Cod. Empenho
        </th>
        <th class="@Grid.AtribuirIconeOrdenacao("DESDSNDSCBNF", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESDSNDSCBNF", Model.Page.OrderBy)">
            Empenho
        </th>*@
                                <th class="@Grid.AtribuirIconeOrdenacao("VLRPTCFRNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRPTCFRNACOCMC", Model.Page.OrderBy)">
                                    Vlr. Participação
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @For Each item In Model.ListPaged
                                @<tr>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.CODDIVCMP)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.DESDIVCMP)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.CODDRTCMP)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.DESDRTCMP)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.CODFRN)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.NOMFRN)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.CODACOCMC)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.NOMACOCMC)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.DATAPVACOCMC)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.TIPEVTACOCMC)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.NOMTIPEVTACOCMC)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.CODEVTACOCMC)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.NOMEVTACOCMC)
                                    </td>
                                    @*<td>
                                            @Html.DisplayFor(Function(model) item.TIPDSNDSCBNF)
                                        </td>
                                        <td>
                                            @Html.DisplayFor(Function(model) item.DESDSNDSCBNF)
                                        </td>*@
                                    <td>
                                        @Html.DisplayFor(Function(model) item.VLRPTCFRNACOCMC)
                                    </td>
                                </tr>
                            Next
                        </tbody>
                    </table>
                </div>
            </div>
            <br Class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>
