﻿@ModelType GridSettings(Of ExtratoVerbaViewModel)
@Imports Martins.AcoesMercadologicas.Core



<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, " hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            <div>
                <div style="width: auto; overflow-x: scroll;">
                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 600px;">
                        <thead>
                            <tr role="row">
                                <th style="max-width:100px;">&nbsp;&nbsp;&nbsp;</th>
                                <th style="max-width:100px;">&nbsp;&nbsp;&nbsp;</th>
                                <th style="max-width:100px;">&nbsp;&nbsp;&nbsp;</th>
                                <th>
                                    Data
                                </th>
                                <th>
                                    Ocorrencia
                                </th>
                                <th>
                                    Valor
                                </th>
                                <th>
                                    Identificador
                                </th>
                            </tr>
                        </thead>
                        <tbody style="font-weight: bold;">

                            @code
                                Dim CODFRN As Decimal = 0
                                Dim CODDRTCMP As Decimal = 0
                                Dim CODDIVCMP As Decimal = 0

                            End Code
                            @For Each item In Model.ListPaged
                                @code
                                    Dim cor As String = "grey"

                                    If item.TIPO_MOV = "SALDO INICIAL" Then
                                        cor = "dark"
                                    ElseIf item.TIPO_MOV = "VERBA RECEBIDA" Then
                                        cor = "green"
                                    ElseIf item.TIPO_MOV = "VERBA A RECEBER" Then
                                        cor = "blue"
                                    ElseIf item.TIPO_MOV = "AÇÃO APROVADA" Then
                                        cor = "red"
                                    ElseIf item.TIPO_MOV = "AÇÃO EM APROVAÇÃO" Then
                                        cor = "grey"
                                    ElseIf item.TIPO_MOV = "SALDO FINAL" Then
                                        cor = "dark"
                                    End If


                                    If (CODDIVCMP <> item.CODDIVCMP) Then
                                        CODDIVCMP = item.CODDIVCMP
                                        @<tr>
                                            <td colspan="7" style="background-color: #e8e8e8;">
                                                Celula: @Html.DisplayFor(Function(model) item.CODDIVCMP)
                                                -
                                                @Html.DisplayFor(Function(model) item.DESDIVCMP)
                                            </td>
                                        </tr>
                                    End If

                                    If (CODDRTCMP <> item.CODDRTCMP) Then
                                        CODDRTCMP = item.CODDRTCMP
                                        @<tr>
                                            <td style="background-color: #d9d5d5;"></td>
                                            <td colspan="6" style="background-color: #d9d5d5;">
                                                Diretoria:
                                                @Html.DisplayFor(Function(model) item.CODDRTCMP)
                                                -
                                                @Html.DisplayFor(Function(model) item.DESDRTCMP)
                                            </td>
                                        </tr>
                                    End If

                                    If (CODFRN <> item.CODFRN) Then
                                        CODFRN = item.CODFRN
                                        @<tr>
                                            <td colspan="2" style="background-color: #b2b1b1;"></td>
                                            <td colspan="5" style="background-color: #b2b1b1;">
                                                Fornecedor:
                                                @Html.DisplayFor(Function(model) item.CODFRN)
                                                -
                                                @Html.DisplayFor(Function(model) item.NOMFRN)
                                            </td>
                                        </tr>
                                    End If

                                End Code
                                @<tr class="@cor">
                                    <td colspan="3" style="background-color: #fff;"></td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.DT_OCORRENCIA)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.TIPO_MOV)
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.VLRMOV)

                                        @If (item.VLRSLDRSV > 0) Then
                                            @<p>
                                                RESERVADO:
                                                @Html.DisplayFor(Function(model) item.VLRSLDRSV)
                                            </p>
                                        End If
                                    </td>
                                    <td>
                                        @Html.DisplayFor(Function(model) item.IDTMOV)
                                    </td>
                                </tr>
                                    Next
                        </tbody>
                    </table>
                </div>
            </div>
            <br Class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>
