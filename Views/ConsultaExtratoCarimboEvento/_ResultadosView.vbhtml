﻿@ModelType GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)
@Imports Martins.AcoesMercadologicas.Core

@Html.Partial(Constantes.VIEW_MODAL_CONFIRMACAO, New Tuple(Of String, String, Object)("Delete", "Carimbo por Evento", Model))


<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, " hide", "" )">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            <div>
                <div style="width: auto; overflow-x: scroll;">
                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 2000px;">
                        <thead>
                            <tr role="row">

                                <th class="@Grid.AtribuirIconeOrdenacao("NOMTIPEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMTIPEVTACOCMC", Model.Page.OrderBy)">
                                    Tipo Evento
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("NOMEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMEVTACOCMC", Model.Page.OrderBy)">
                                    Evento
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("OBJETIVO", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("OBJETIVO", Model.Page.OrderBy)">
                                    Objetivo
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("DATGRCMCOVBAFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATGRCMCOVBAFRN", Model.Page.OrderBy)">
                                    Data Geração
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("DATAPVACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATAPVACOCMC", Model.Page.OrderBy)">
                                    Data Aprovação
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("FORNECEDOR", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("FORNECEDOR", Model.Page.OrderBy)">
                                    Fornecedor
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("CODMCOVBAFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMCOVBAFRN", Model.Page.OrderBy)">
                                    Carimbo
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("ACAO", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("ACAO", Model.Page.OrderBy)">
                                    Ação Cormercial
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("DESOBSMCOVBAFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESOBSMCOVBAFRN", Model.Page.OrderBy)">
                                    Obs.
                                </th>
                                <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZACOCMC", Model.Page.OrderBy)">
                                    Vlr. Utilizado
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            @For Each item In Model.ListPaged
                                @<tr>
                                    <td>
                                        @item.NOMTIPEVTACOCMC
                                    </td>
                                    <td>
                                        @item.NOMEVTACOCMC
                                    </td>
                                    <td>
                                        @item.OBJETIVO
                                        @*@item.TIPOBJMCOVBAFRN - @item.DESOBJMCOVBAFRN*@
                                    </td>
                                    <td>
                                        @item.DATGRCMCOVBAFRN.ToShortDateString()
                                    </td>
                                    <td>
                                        @item.DATAPVACOCMC.ToShortDateString()
                                    </td>
                                    <td>
                                        @item.FORNECEDOR
                                        @*@item.CODFRN - @item.NOMFRN*@
                                    </td>
                                    <td>
                                        @item.CODMCOVBAFRN
                                    </td>
                                    <td>
                                        @item.ACAO
                                    </td>

                                    <td>
                                        @item.DESOBSMCOVBAFRN

                                    </td>
                                     <td class="text-right">
                                         @item.VLRUTZACOCMC.ToString("#,##0.00")

                                     </td>
                                </tr>
                            Next
                        </tbody>
                    </table>
                </div>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>
