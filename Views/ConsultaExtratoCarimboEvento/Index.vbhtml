﻿@ModelType GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Carimbo por Evento"
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-bar-chart"></i>
            Relatórios
        </li>
        <li class="active">Consulta Extrato Carimbo por Evento</li>
    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modal"></div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "ConsultaExtratoCarimboEvento", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("carimboID", "Carimbo:")
                            @Html.TextBoxFor(Function(Model) Model.Filter.CODMCOVBAFRN, New With {.Class = "form-control maskNumber"})
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("objetivo", "Objetivo:")
                            @Html.DropDownListFor(Function(m) m.Filter.DESOBJMCOVBAFRN, New SelectList(ViewBag.objetivo, "DESOBJMCOVBAFRN", "DescDrop", Model.Filter.DESOBJMCOVBAFRN), "Selecione...", New With {.Class = "form-control"})

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("evento", "Evento:")
                            @Html.DropDownListFor(Function(m) m.Filter.NOMEVTACOCMC, New SelectList(ViewBag.evento, "NOMEVTACOCMC", "CODIGO_NOME", Model.Filter.NOMEVTACOCMC), "Selecione...", New With {.Class = "form-control"})


                        </div>
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("tipoEvento", "Tipo de Evento:")
                            @Html.DropDownListFor(Function(m) m.Filter.NOMTIPEVTACOCMC, New SelectList(ViewBag.tipoEvento, "NOMTIPEVTACOCMC", "DescDropTipeve", Model.Filter.NOMTIPEVTACOCMC), "Selecione...", New With {.Class = "form-control"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white ">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger"></i>
                                    Limpar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/ConsultaExtratoCarimboEvento/_ResultadosView.vbhtml", Model)
    End Code
</div>



