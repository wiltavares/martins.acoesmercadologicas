﻿@ModelType GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Consulta Saldo Disponível"
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-bar-chart"></i>
            Relatórios
        </li>
        <li class="active">Consulta Saldo Carimbo</li>
    </ul>
</div>


<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            Buscar Carimbos
            @Using (Ajax.BeginForm("Pesquisar", "ConsultaSaldoDisponivel", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-3">
                            @Html.DropDownListFor(Function(m) m.Filter.COD_FILTRO, New SelectList(ViewBag.filtroCarimbos, "VALUE", "TEXT", Model.Filter.COD_FILTRO), New With {.Class = "form-control", .id = "dropFiltroCarimbos", .onchange = "dropFiltroCarimbos_onChange($(this).val())"})
                        </div>

                        <div class="col-sm-7" style="display:none; " id="divFiltroFornecedor">
                            <div class="row">
                                <div class="col-xs-3">
                                    @Html.TextBoxFor(Function(m) m.Filter.CODFRN, New With {.Class = "form-control", .id = "filtroCodFrn"})
                                </div>
                                <div class="col-xs-7">
                                    <div class="row">
                                        @Html.DropDownListFor(Function(m) m.Filter.CODFRN, New SelectList(ViewBag.filtroFornecedorCarimbo, "NOMFRN", "CODFRN", Model.Filter.CODFRN), New With {.Class = "form-control", .id = "dropFiltroFornecedorCarimbo", .onchange = "dropFornecedorCarimbo_onChange()"})
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <div class="btn-group pull-right">
                                        <a onclick="buscarFornecedores()" class="btn btn-sm btn-info" style="height:34px">
                                            <i class="ace-icon fa fa-search" style="vertical-align: middle"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7" style="display:none; " id="divFiltroCelula">
                            <div class="row">
                                <div class="col-sm-12">
                                    @Html.DropDownListFor(Function(m) m.Filter.CODDIVCMP, New SelectList(ViewBag.filtroCelulaCarimbo, "CODDIVCMP", "DESDIVCMP", Model.Filter.CODDIVCMP), New With {.Class = "form-control", .id = "dropFiltroCelula"})
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7" id="divFiltroCarimbo">
                            <div class="row">
                                <div class="col-sm-8">
                                    @Html.TextBoxFor(Function(m) m.Filter.CODMCOVBAFRN, New With {.Class = "form-control", .id = "filtroCodCarimbo"})
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-2">

                            <input type="checkbox" id="filtroSaldoDisponivel" />
                            <label for="filtroSaldoDisponivel">
                                Saldo Disponível
                            </label>
                            </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <a onclick="filtrarCarimbos()" class="btn btn-info btn-white">
                                    <i class="ace-icon fa fa-search bigger"></i>
                                    Pesquisar
                                </a>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger-120"></i>
                                    Limpar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>


<div id="consultaSaldo">
    @Code
        Html.RenderPartial("~/Views/ConsultaSaldoCarimbo/_ResultadoConsultaSaldo.vbhtml")
    End Code
</div>




