﻿@ModelType GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    
        <div class="widget-header widget-header-small widget-light-blue">
            <h4 class="widget-title smaller">
                Carimbos
            </h4>
        </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:1500px">
                    <thead>
                        <tr role="row">
                            @*<th class="center">
                                    Ações
                                </th>*@
                            <th class="@Grid.AtribuirIconeOrdenacao("CODMCOVBAFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMCOVBAFRN", Model.Page.OrderBy)">
                                Carimbo
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CELULA", Model.Page.OrderBy)" data= href="@Grid.OrdenarColuna("CELULA", Model.Page.OrderBy)">
                                Célula
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DATGRCMCO", Model.Page.OrderBy)" data= href="@Grid.OrdenarColuna("DATGRCMCO", Model.Page.OrderBy)">
                                Data
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("FORNECEDOR", Model.Page.OrderBy)" data= href="@Grid.OrdenarColuna("FORNECEDOR", Model.Page.OrderBy)">
                                Fornecedor
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLRMCO", Model.Page.OrderBy)" data= href="@Grid.OrdenarColuna("VLRMCO", Model.Page.OrderBy)">
                                Valor Carimbo
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZ", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZ", Model.Page.OrderBy)">
                                Valor Utilizado
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("SALDO_CARIMBO", Model.Page.OrderBy)" data= href="@Grid.OrdenarColuna("SALDO_ACAO", Model.Page.OrderBy)">
                                Saldo
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESOBSMCOVBAFRN", Model.Page.OrderBy)" data= href="@Grid.OrdenarColuna("DESOBSMCOVBAFRN", Model.Page.OrderBy)">
                                Obs. Carimbo
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            'Dim idCheckInc = "ckbIncCar" & item.CODMCOVBAFRN
                            'Dim idTextValor = "txtValor" & item.CODMCOVBAFRN
                            'Dim idHiddenNum = "hdnCodCar" & item.CODMCOVBAFRN
                            @<tr>
                                @*<td class="center">
                                    <div id="extratcarimbo"></div>
                                    <div class="hidden-sm hidden-xs btn-group">
                                        <a data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" href="@Url.Action("ModalExtratoCarimbo")">
                                            <i class="ace-icon fa fa-pencil bigger-119"></i>

                                        </a>
                                    </div>
                                    </td>*@
                                @* New With {.TIPEVTACOCMC = item.TIPEVTACOCMC, .CODEVTACOCMC = item.CODEVTACOCMC, .ANOMESREF = item.ANOMESREF, .CODUNDESRNGC = item.CODUNDESRNGC}*@
                                <td>
                                    @item.CODMCOVBAFRN
                                </td>
                                <td>
                                    @item.CELULA
                                </td>
                                <td>
                                    @item.DATGRCMCO.ToShortDateString()
                                </td>
                                <td>
                                    @item.FORNECEDOR 
                                </td>
                                 <td class="text-right">
                                     @item.VLRMCO.ToString("#,##0.00")
                                 </td>
                                 <td class="text-right">
                                     @item.VLRUTZ.ToString("#,##0.00")
                                 </td>

                                 <td class="text-right">
                                     @item.SALDO_CARIMBO.ToString("#,##0.00")
                                 </td>
                                <td>
                                    @item.DESOBSMCOVBAFRN
                                </td>
                            </tr>

                        Next

                    </tbody>

                </table>
            </div>

            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>

















