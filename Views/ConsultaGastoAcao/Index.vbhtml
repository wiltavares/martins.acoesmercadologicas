﻿@ModelType GridSettings(Of AcaoComercialViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Gasto por Evento"

End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-bar-chart"></i>
            Relatórios
        </li>
        <li class="active">Consulta Gasto Ação</li>
    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modal"></div>
<div class="modal fade" id="modalResultImport"></div>
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "ConsultaGastoAcao", Constantes.AJAX_OBJECT_POST_DEFAULT, New With {.id = "ConsultaGasto"}))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">                       
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("evento", "Evento:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODEVTACOCMC, New SelectList(ViewBag.evento, "CODEVTACOCMC", "DROPDESC", Model.Filter.CODEVTACOCMC), "Selecione...", New With {.Class = "form-control chosen-select", .id = "CODEVTACOCMC"})
                        </div> 
                    </div>                                     
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white " name="Pesquisar" value="Pesquisar">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white" >
                                    <i class="ace-icon fa fa-eraser bigger-120"></i>
                                    Limpar
                                </a>
                                <a onclick="Imprimirpdf()" href="#" class="btn btn-info btn-white">
                                    <i class="ace-icon fa fa-file-excel-o bigger-120"></i>
                                    Exportar
                                </a>
                                @*<a onclick="fnImprimir()" href="#" class="btn btn-info btn-white">
                                    <i class="ace-icon fa fa-print bigger-120"></i>
                                    Imprimir
                                </a>*@
                            </div>
                        </div>
                    </div>
                </div>
            End Using

        </div>
    </div>
</div>
<div id="resultadosView">
   @Code
       Html.RenderPartial("~/Views/ConsultaGastoAcao/_ResultadosView.vbhtml", Model)
    End Code
</div>

<script src="~/Scripts/saldoAcaoFornecedor.js"></script>