﻿@ModelType GridSettings(Of AcaoComercialViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                <thead>
                    <tr role="row">
                        <th class="@Grid.AtribuirIconeOrdenacao("CODACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODACOCMC", Model.Page.OrderBy)">
                            Ação
                        </th>
                        <th class="@Grid.AtribuirIconeOrdenacao("DESOPEFSCDSNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESOPEFSCDSNACOCMC", Model.Page.OrderBy)">
                            Tipo de Operação
                        </th>
                        <th class="@Grid.AtribuirIconeOrdenacao("CODITE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODITE", Model.Page.OrderBy)">
                            Item
                        </th>
                        <th class="@Grid.AtribuirIconeOrdenacao("VLRORC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRORC", Model.Page.OrderBy)">
                            Valor Orçado
                        </th>
                        <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZ", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZ", Model.Page.OrderBy)">
                            Valor Utilizado
                        </th>
                        <th class="@Grid.AtribuirIconeOrdenacao("VLRSLD", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRSLD", Model.Page.OrderBy)">
                            Valor Saldo
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @For Each item In Model.ListPaged
                        @<tr>
                            <td>
                                @item.CODACOCMC
                            </td>
                            <td>
                                @item.CODOPE - @item.NOMOPE
                            </td>
                            <td>
                                @item.CODITE
                            </td>
                             <td class="text-right">
                                 @code
                                 Dim valor As New Decimal
                                 valor = item.VLRORC
                                 End Code
                                 @valor.ToString("#,##0.00")
                             </td>

                             <td class="text-right">
                                 @code
                                 valor = item.VLRUTZ
                                 End Code
                                 @valor.ToString("#,##0.00")
                             </td>

                             <td class="text-right">
                                 @code valor = item.VLRSLD End Code
                                 @valor.ToString("#,##0.00")
                             </td>

                        </tr>
                        Next
                </tbody>
            </table>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>
