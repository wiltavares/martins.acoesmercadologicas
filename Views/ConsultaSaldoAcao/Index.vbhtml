﻿@ModelType GridSettings(Of AcaoComercialViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Consulta Saldo Ação x Evento"

End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-bar-chart"></i>
            Relatórios
        </li>
        <li class="active">Consulta Saldo Ação</li>
    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modal"></div>
<div class="modal fade" id="modalResultImport"></div>
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "ConsultaSaldoAcao", Constantes.AJAX_OBJECT_POST_DEFAULT, New With {.id = "ConsultaSaldo"}))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        @*@Html.HiddenFor(Function(model) model.Filter.NOMFNC, New With {.Class = "form-control", .id = "NOMFNC"})*@
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("data", "Data Início:")
                            @Html.HiddenFor(Function(m) m.Filter.DataIniAux, New With {.id = "dataIniAux"})
                            @Html.HiddenFor(Function(m) m.Filter.DataFimAux, New With {.id = "dataFimAux"})
                            <div class="input-daterange input-group">
                                @Html.TextBoxFor(Function(m) m.Filter.DATINIACOCMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataIni()", .id = "dataIni", .autocomplete = "off"})
                                <span class="input-group-addon">
                                    à
                                </span>
                                @Html.TextBoxFor(Function(m) m.Filter.DATFIMACOCMC, CDate(Model.Filter.DATFIMACOCMC).ToShortDateString, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataFim()", .id = "dataFim", .autocomplete = "off"})
                                @* @Html.TextBoxFor(Function(m) m.Filter.DATFIMACOCMC, New With {.class = "input-sm date-interval form-control", .id = "dataFim"})*@
                                @*@Html.TextBoxFor(Function(m) m.Filter.DATFIMACOCMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataFim()", .id = "dataFim"})*@
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("evento", "Evento:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODEVTACOCMC, New SelectList(ViewBag.evento, "CODEVTACOCMC", "DROPDESC", Model.Filter.CODEVTACOCMC), "Selecione...", New With {.Class = "form-control chosen-select", .id = "CODEVTACOCMC"})
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("operacoes", "Operações:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODOPEFSCDSNACOCMC, New SelectList(ViewBag.operacoes, "CODOPEFSCDSNACOCMC", "DROPDESC", Model.Filter.CODOPEFSCDSNACOCMC), "Selecione...", New With {.Class = "form-control chosen-select", .id = "CODOPEFSCDSNACOCMC"})
                        </div>
                        <div class="col-lg-1 col-sm-6">
                            @Html.Label("acaoID", "Ação:")
                            @Html.TextBoxFor(Function(m) m.Filter.CODACOCMC, New With {.Class = "form-control", .min = 0, .id = "CODACOCMC", .alt = "inteiro"})
                        </div>
                        <div class="col-lg-5 col-sm-6">
                            @Html.Label("acao", "Nome da Ação:")
                            @Html.TextBoxFor(Function(m) m.Filter.NOMACOCMC, New With {.class = "input-sm form-control", .id = "NOMACOCMC"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white " name="Pesquisar" value="Pesquisar">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger-120"></i>
                                    Limpar
                                </a>
                                <a onclick="fnImprimir()" href="#" class="btn btn-info btn-white">
                                    <i class="ace-icon fa fa-file-excel-o bigger-120"></i>
                                    Exportar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/ConsultaSaldoAcao/_ResultadosView.vbhtml", Model)
    End Code
</div>

<script src="~/Scripts/saldoAcaoFornecedor.js"></script>