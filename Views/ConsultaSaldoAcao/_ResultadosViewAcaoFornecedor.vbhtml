﻿@ModelType GridSettings(Of AcaoComercialViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1500px;">
                    <thead>
                        <tr role="row">                           
                            <th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy)">
                                Fornecedor
                            </th>                            
                            <th class="@Grid.AtribuirIconeOrdenacao("CODACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODACOCMC", Model.Page.OrderBy)">
                                Ação.
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DATAPVACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATAPVACOCMC", Model.Page.OrderBy)">
                                Aprovação
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODOPEFSCDSNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODOPEFSCDSNACOCMC", Model.Page.OrderBy)">
                                Operação
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMER", Model.Page.OrderBy)">
                                Mercadoria
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("saldo", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("saldo", Model.Page.OrderBy)">
                                TotFrn
                            </th>  
                            <th class="@Grid.AtribuirIconeOrdenacao("ptcfrn", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("ptcfrn", Model.Page.OrderBy)">
                                PtcFrn
                            </th>  
                            <th class="@Grid.AtribuirIconeOrdenacao("saldo_fornecedor", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("saldo_fornecedor", Model.Page.OrderBy)">
                                SldFrn
                            </th>  
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                               
                                <td>
                                    @item.CODFRN - @item.NOMFRN
                                </td>
                                 <td>
                                    @item.CODACOCMC - @item.NOMACOCMC
                                 </td>
                                <td>        
                                    @IIf(Not IsNothing(item.DATAPVACOCMC), item.DATAPVACOCMC.Value.ToShortDateString, "")
                                </td>
                                <td>
                                    @item.CODOPEFSCDSNACOCMC - @item.DESOPEFSCDSNACOCMC
                                </td>   
                                <td>                                                                
                                    @IIF(item.CODMER = 0, item.DESOPEFSCDSNACOCMC, item.CODMER & " - " & item.CODITEOCDACOCMC)                                                                    
                                </td>
                                 <td class="text-right">
                                     @code
                                     Dim valor As New Decimal
                                     valor = item.saldo
                                     End Code
                                     @valor.ToString("#,##0.00")
                                 </td>
                                 <td class="text-right">
                                     @code
                                     valor = item.PtcFrn
                                     End Code
                                     @valor.ToString("#,##0.00")
                                 </td>
                                 
                                 <td class="text-right">
                                     @code
                            valor = item.saldo_fornecedor
                                     End Code
                                     @valor.ToString("#,##0.00")
                                 </td>                               
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>
