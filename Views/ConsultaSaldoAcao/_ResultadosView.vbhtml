﻿@ModelType GridSettings(Of AcaoComercialViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1500px;">
                    <thead>
                        <tr role="row">
                            <th class="center">Ações</th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODACOCMC", Model.Page.OrderBy)">
                                Ação
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMACOCMC", Model.Page.OrderBy)">
                                Nome Ação.
                            </th>                            
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMOPE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMOPE", Model.Page.OrderBy)">
                                Tipo de Operação.
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODITE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODITE", Model.Page.OrderBy)">
                                Item
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLRORC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRORC", Model.Page.OrderBy)">
                                Valor Orçado
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZ", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZ", Model.Page.OrderBy)">
                                Valor Utilizado
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLRSLD", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRSLD", Model.Page.OrderBy)">
                                Valor Saldo
                            </th>      
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td Class="center">
                                    <div Class="hidden-sm hidden-xs btn-group left">     
                                                                                                                                                             
                                        <a class="btn btn-xs btn-purple tooltip-info" href="javascript:redirectLoading('@Url.Action("../ConsultaSaldoAcaoFornecedor/Index", New With {.CODACOCMC = item.CODACOCMC})')" data-rel="tooltip" title="Ação Fornecedor">
                                            <i class="ace-icon glyphicon glyphicon-random  bigger-110"></i>
                                        </a>
                                    </div>

                                    <div Class="hidden-md hidden-lg">
                                        <div Class="inline pos-rel">
                                            <Button Class="btn btn-minier btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                                <i Class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                            </Button>

                                            <ul Class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                <li>
                                                    <a href="javascript:redirectLoading('@Url.Action("Edit", New With {.id = item.CODACOCMC})')" Class="tooltip-info" data-rel="tooltip" title="" data-original-title="Editar">
                                                        <span Class="blue">
                                                            <i Class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a onclick="abrirModal('AcaoComercial', 'VisualizarFluxo', @Json.Encode(item.CODACOCMC))" Class="tooltip-info" data-rel="tooltip" title="Fluxo" data-original-title="Fluxo">
                                                        <span class="purple">
                                                            <i class="ace-icon glyphicon glyphicon-random bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a onclick="abrirModal('AcaoComercial', 'RealizarEstorno', @Json.Encode(item.CODACOCMC))" Class="tooltip-error" data-rel="tooltip" title="Estorno" data-original-title="Estorno">
                                                        <span class="orange">
                                                            <i class="ace-icon glyphicon glyphicon-export bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    @item.CODACOCMC
                                </td>
                                 <td>
                                     @item.NOMACOCMC
                                 </td>
                                <td>
                                    @item.CODOPE - @item.NOMOPE
                                    @item.DESOPEFSCDSNACOCMC
                                </td>
                                <td>
                                    @item.CODITE
                                </td>   
                                 <td class="text-right">
                                     @code
                                     Dim valor As New Decimal
                                     valor = item.VLRORC
                                     End Code
                                     @valor.ToString("#,##0.00")
                                 </td>
                                 <td class="text-right">
                                     @code
                                     valor = item.VLRUTZ
                                     End Code
                                     @valor.ToString("#,##0.00")
                                 </td>
                                 <td class="text-right">
                                     @code valor = item.VLRSLD End Code
                                     @valor.ToString("#,##0.00")
                                 </td>                               
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>
