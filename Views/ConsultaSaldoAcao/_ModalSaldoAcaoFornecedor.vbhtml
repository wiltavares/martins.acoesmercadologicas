﻿@ModelType  GridSettings(Of AcaoComercialViewModel) 
@Imports Martins.AcoesMercadologicas.Core

<div class="modal fade" id="modalBuscaItem"></div>
<div class="modal fade" id="modalSuprimentos"></div>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            Consulta Saldo Ação x Fornecedor
        </div>

        <div class="modal-body">

            <div id="msgGeral">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>

            @Using (Ajax.BeginForm("PesquisarSaldoAcaoFornecedor", "ConsultaSaldoAcao", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()     
            @<div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title smaller">
                        Pesquisa
                    </h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <form action="/" id="MercadoriaCompleta">                           
                            <div class="row">

                                <div class="col-lg-3 col-sm-6">
                                    @Html.Label("acaoID", "Ação:")
                                    @Html.TextBoxFor(Function(m) m.Filter.CODACOCMC, New With {.Class = "form-control", .id = "txtCodAcao"})                                                               
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    @Html.Label("acao", "Nome da Ação:")
                                    @Html.TextBoxFor(Function(m) m.Filter.NOMACOCMC, New With {.class = "input-sm form-control", .id = "txtNomAcao"})
                                </div>    
                            </div>         
                                  
                             <br />  
                            <div class="row">
                                        <div class="col-sm-3">
                                            @Html.Label("opcoesID", "Opções:")
                                            @Html.DropDownListFor(Function(m) m.Filter.COD_FILTRO, New SelectList(ViewBag.filtroSaldoAcaoFrn, "VALUE", "TEXT", Model.Filter.COD_FILTRO), "Selecione...", New With {.Class = "chosen-select form-control ", .id = "dropFiltroCarimbos", .onchange = "dropFiltros_onChange($(this).val())"})
                                        </div>
                                        <div class="col-sm-7" id="divFiltroFornecedor" style="display:none;">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    @Html.Label("FornID", "Cód:")
                                                    @Html.TextBoxFor(Function(m) m.Filter.CODFRN, New With {.Class = "form-control", .id = "filtroCodFrn"})
                                                </div>
                                                <div class="col-xs-7">
                                                    <div class="row">
                                                        @Html.Label("Forn2ID", "Nome")
                                                        @Html.DropDownListFor(Function(m) m.Filter.CODFRN, New SelectList(ViewBag.fornecedorAcao, "NOMFRN", "CODFRN", Model.Filter.CODFRN), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFiltroFornecedorCarimbo", .onchange = "dropFornecedorCarimbo_onChange()"})
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="btn-group pull-right">
                                                        @Html.Label("Forn3ID", "Filtrar")
                                                        <a onclick="buscarFornecedores()" class="btn btn-sm btn-info" style="height:34px">
                                                            <i class="ace-icon fa fa-search" style="vertical-align: middle"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-7" style="display:none; " id="divFiltroCelula">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    @Html.Label("CelID", "Célula:")
                                                    @Html.DropDownListFor(Function(m) m.Filter.CODDIVCMP, New SelectList(ViewBag.filtroCelula, "CODDIVCMP", "DESDIVCMP", Model.Filter.CODDIVCMP), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFiltroCelula"})
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-7" id="divFiltroComprador">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    @Html.Label("CompID", "Comprador:")
                                                    @Html.DropDownListFor(Function(m) m.Filter.CODCPR, New SelectList(ViewBag.filtroComprador, "CODCPR", "NOMCPR", Model.Filter.CODCPR), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFiltroComprador"})                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                 
                        </form>
                    </div>
                </div>

                 <div class="modal-footer clearfix">
                     <div class="btn-group pull-right">
                         <button  type="submit" class="btn btn-info btn-white ">
                             <i class="ace-icon fa fa-search bigger-120"></i>
                             Pesquisar
                         </button>
                         <a onclick="LimparCamposModal()"  class="btn btn-grey btn-white">
                             <i class="ace-icon fa fa-eraser bigger-120"></i>
                             Limpar
                         </a>
                     </div>
                 </div>
                
            </div>                  
        End Using  
            <div id="resultadosView">
                @Html.Partial("~/Views/ConsultaSaldoAcao/_ResultadosViewAcaoFornecedor.vbhtml", Model)
            </div>
        </div>
       
    </div>
</div>