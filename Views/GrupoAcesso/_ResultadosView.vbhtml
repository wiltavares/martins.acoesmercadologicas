﻿@ModelType GridSettings(Of GrupoAcessoViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 250px;">
                    <thead>
                        <tr role="row">
                            <th class="center">Ações</th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODGRPACS", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODGRPACS", Model.Page.OrderBy)">
                              Grupo de Acesso
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESGRPACS", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESGRPACS", Model.Page.OrderBy)">
                              Nome do Grupo de Acesso
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESGRPRDE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESGRPRDE", Model.Page.OrderBy)">
                             Nome do Grupo AD
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td class="center">
                                    <div class="hidden-sm hidden-xs btn-group">
                                        <a data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" href="@Url.Action("CreateEdit", New With {.id = item.CODGRPACS})">
                                            <i class="ace-icon fa fa-pencil bigger-119"></i>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    @item.CODGRPACS
                                </td>
                                <td>
                                    @item.DESGRPACS.Trim()
                                </td>
                                <td>
                                   @item.DESGRPRDE.Trim()
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code
        </div>
    </div>
</div>