﻿@ModelType GrupoAcessoViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim tituloTela = IIf(Model Is Nothing OrElse Model.CODGRPACS Is Nothing, "Incluir Grupo Acesso", "Editar Grupo Acesso")
    ViewData("Title") = tituloTela
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Controle de Acesso
        </li>
        <li class="active">Grupo de Acesso</li>
        @Code
            Dim campo = IIf(ViewBag.acao = "Save", "Incluir", "Editar ")
        End Code
        <li class="active">@campo</li>
    </ul>
</div>
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-file-text-o bigger-110"></i>
            Dados
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Html.BeginForm(ViewBag.acao, "GrupoAcesso"))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-2 col-sm-6">
                            @Html.Label("acaoID", "Grupo de Acesso:")
                            @Html.TextBox("CODGRPACS", Model.CODGRPACS, Model.CODGRPACS.ToString, New With {.Class = "form-control", .readonly = "readonly"})
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("nomGrupoAcesso", "Nome do Grupo de Acesso (*):")
                            @Html.TextBox("DESGRPACS", Model.DESGRPACS, New With {.Class = "form-control"})
                            @Html.ValidationMessageFor(Function(m) m.DESGRPACS, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("DESGRPRDE", "Nome do Grupo AD (*):")
                            @Html.TextBox("DESGRPRDE", Model.DESGRPRDE, New With {.Class = "form-control"})
                            @Html.ValidationMessageFor(Function(m) m.DESGRPRDE, "", New With {.class = "text-danger-message-login"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-white btn-success ">
                                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                                    Salvar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-white btn-danger">
                                    <i class="ace-icon fa fa-times bigger-120 red"></i>
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>