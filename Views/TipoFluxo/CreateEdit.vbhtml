﻿@ModelType TipoFluxoViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim tituloTela = IIf(ViewBag.acao = "Salvar", "Incluir Tipo de Fluxo", "Editar Tipo de Fluxo")
    ViewData("Title") = tituloTela
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Tipo de Fluxo</li>
        @Code
            Dim campo = IIf(ViewBag.acao = "Salvar", "Incluir", "Editar ")
        End Code
        <li class="active">@campo</li>
    </ul>
</div>
<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div Class="widget-box widget-color-blue2">
    <div Class="widget-header widget-header-small">
        <h4 Class="widget-title smaller white lighter">
            <i Class="ace-icon fa fa-file-text-o bigger-110"></i>
            Dados
        </h4>
    </div>

    <div Class="widget-body">
        <div Class="widget-main">
            @Using (Html.BeginForm(ViewBag.acao, "TipoFluxo"))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-3">
                            @Html.Label("lblCodigo", "Código:")
                            @Html.EditorFor(Function(model) model.TIPFLUAPVACOCMC, New With {.htmlAttributes = New With {.Class = "form-control", .disabled = "true"}})
                            @Html.HiddenFor(Function(model) model.TIPFLUAPVACOCMC)
                        </div>
                        <div class="col-sm-6">
                            @Html.Label("lblTipoDeFluxo", "Tipo de Fluxo (*):")
                            @Html.EditorFor(Function(model) model.DESFLUAPVACOCMC, New With {.htmlAttributes = New With {.class = "form-control"}})
                            @Html.ValidationMessageFor(Function(model) model.DESFLUAPVACOCMC, "", New With {.class = "text-danger-message-login"})
                        </div>
                    </div>
                    <br>

                                <div Class="row">
                        <div class="col-sm-12">
                            <div Class="btn-group pull-left">
                                <Button type="submit" Class="btn btn-white btn-success">
                                    <i Class="ace-icon fa fa-save bigger-120 green"></i>
                                    Salvar
                                </Button>
                                <a href="@Url.Action("Index")" Class="btn btn-white btn-danger">
                                    <i Class="ace-icon fa fa-times bigger-120 red"></i>
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
