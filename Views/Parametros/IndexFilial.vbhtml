﻿@ModelType FilialEmpresaCorporativoViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code

    ViewData("Title") = "Alterar Código da Cidade"

End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Parâmetros</li>
        @Code
            Dim campo = IIf(Url.Action.Split("=").Length = 1 OrElse Url.Action.Split("=")(1) = "0", "Incluir", "Editar")
        End Code
        <li class="active">@campo</li>
    </ul>
</div>
<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modal"></div>
<div class="modal fade" id="modalResultImport"></div>
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Altera Código da Cidade
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("EditFilial", "Parametros", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">

                        <div class="col-lg-1 col-sm-6">
                            @Html.Label("cd", "Código:")
                            @Html.TextBoxFor(Function(m) m.CODFILEMP, New With {.Class = "form-control", .disabled = "true", .alt = "inteiro"})
                            @Html.HiddenFor(Function(m) m.CODFILEMP)
                        </div>
                        <div class="col-lg-5 col-sm-6">
                            @Html.Label("fil", "Filial:")
                            @Html.TextBoxFor(Function(m) m.NOMFILEMP, New With {.Class = "form-control", .disabled = "true"})
                        </div>
                        <div class="col-lg-1 col-sm-6">
                            @Html.Label("CidID", "Cidade:")
                            @Html.TextBoxFor(Function(m) m.CODCIDPCOACOCMC, New With {.Class = "form-control", .id = "filtroCodCid", .ReadyOnly = "True", .alt = "inteiro"})
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("cidade", "Nome:")
                            @Html.DropDownListFor(Function(m) m.CODCID, New SelectList(ViewBag.cidades, "CODCID", "NOMCID", Model.CODCID), "Selecione...", New With {.Class = "form-control chosen-select", .id = "dropFiltroCidade", .onchange = "dropFiltroCidade_onChange()"})
                        </div>
                        <div class="col-sm-1">
                            <div class="btn-group pull-right">
                                @Html.Label("Forn3ID", "Filtrar")
                                <a onclick="buscarCidades()" class="btn btn-sm btn-info" style="height:34px">
                                    <i class="ace-icon fa fa-search" style="vertical-align: middle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull left">
                                <button type="submit" class="btn btn-white btn-success">
                                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                                    Alterar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-white btn-danger">
                                    <i class="ace-icon fa fa-times bigger-120 red"></i>
                                    Cancelar
                                </a>
                                @*<a href="@Url.Action("Index")" class="btn btn-white btn-warning">
                                        <i class="ace-icon fa fa-backward bigger-120 orange"></i>
                                        Voltar
                                    </a>*@
                            </div>
                        </div>
                    </div>
                </div>
            End Using

        </div>
    </div>
</div>

<script src="~/Scripts/cidade.js"></script>