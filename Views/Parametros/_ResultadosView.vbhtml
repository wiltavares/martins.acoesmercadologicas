﻿@ModelType GridSettings(Of FilialEmpresaCorporativoViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Relação Filial x Código da Cidade
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 250px;">
                    <thead>
                        <tr role="row">
                            <th class="center">Editar </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODFILEMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFILEMP", Model.Page.OrderBy)">
                                Código Filial
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMFILEMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFILEMP", Model.Page.OrderBy)">
                                Nome Filial
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODCIDPCOACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODCIDPCOACOCMC", Model.Page.OrderBy)">
                                Código da Cidade
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each Item In Model.ListPaged
                            @<tr>
                                <td class="center">
                                    <div class="hidden-sm hidden-xs btn-group">
                                        <a data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" href="@Url.Action("IndexFilial", New With {.CodFilEmp = item.CODFILEMP})">
                                            <i class="ace-icon fa fa-pencil bigger-119"></i>
                                        </a>
                                    </div>
                                    <div class="hidden-md hidden-lg">
                                        <div class="inline pos-rel">
                                            <button class="btn btn-minier btn-md btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                                <i class="ace-icon fa fa-cog icon-only"></i>
                                            </button>

                                            <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                <li>
                                                    <a class="tooltip-info" data-rel="tooltip" href="@Url.Action("IndexFilial", New With {.CodFilEmp = item.CODFILEMP})">
                                                        <i class="ace-icon fa fa-pencil-square-o blue"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="tooltip-info" data-rel="tooltip" data-toggle="modal" data-target="#confirmationDelete" data-id="@item.CODFILEMP">
                                                        <i class="ace-icon fa fa-trash-o red"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    @Item.CODFILEMP
                                </td>
                                <td>
                                    @Item.NOMFILEMP
                                </td>
                                <td>
                                    @Item.CODCIDPCOACOCMC - @Item.NOMCID
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code
        </div>
    </div>
</div>

