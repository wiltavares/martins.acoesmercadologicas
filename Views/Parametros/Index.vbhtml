﻿@ModelType ParamentroSistemaDeGestaoDeAcaoComercialViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code

    ViewData("Title") = "Parâmetros"

End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Parâmetros</li>
    </ul>
</div>
<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modal"></div>
<div class="modal fade" id="modalResultImport"></div>
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Parâmetros
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Edit", "Parametros", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div style="display:none;">
                            @Html.TextBoxFor(Function(m) m.CODEMP, New With {.Class = "form-control"})
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            @Html.Label("FornID", "Cód:")
                            @Html.TextBoxFor(Function(m) m.CODFRNMKT, New With {.Class = "form-control", .id = "filtroCodFrn", .alt = "inteiro"})
                        </div>

                        <div class="col-lg-5 col-sm-6">
                            @Html.Label("fornecedorEvento", "Fornecedor de Marketing:")
                            @Html.DropDownListFor(Function(m) m.CODFRNMKT, New SelectList(ViewBag.fornecedorAcao, "CODFRN", "NOMFRN", Model.CODFRNMKT), New With {.Class = "form-control", .id = "dropFiltroFornecedorCarimbo", .onchange = "dropFornecedorCarimbo_onChange()"})
                        </div>
                        <div class="col-sm-1">
                            <div class="btn-group pull-right">
                                @Html.Label("Forn3ID", "Filtrar")
                                <a onclick="buscarFornecedores()" class="btn btn-sm btn-info" style="height:34px">
                                    <i class="ace-icon fa fa-search" style="vertical-align: middle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("vlrMaximo", "Valor Máximo da Ação Comercial")
                            @Html.TextBoxFor(Function(m) m.VLRMAXACOCMC, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("vlrMin", "Valor Mínimo Item:")
                            @Html.TextBoxFor(Function(m) m.VLRMNMITERSU, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("txAdmin", "Taxa de Administração(%):")
                            @Html.TextBoxFor(Function(m) m.VLRTXAADMFRNACOCMC, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("calcEncargo", "Cálculo de Encargos(%):")
                            @Html.TextBoxFor(Function(m) m.PERCALENCFOLPGT, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("txBrinde", "Taxa de Brinde(%):")
                            @Html.TextBoxFor(Function(m) m.VLRTXAADMBDEACOCMC, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                        </div>

                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("txtPercMargem", "Percentual Margem:")
                            @Html.TextBoxFor(Function(m) m.PERMRGADIBDEPMC, New With {.Class = "form-control", .id = "filtroPercMargem", .alt = "decimal_positivo_2"})
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <br />
                            <div class="btn-group pull-right">
                                <button type="submit" class="btn btn-white btn-success ">
                                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                                    Alterar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<br class="clearfix" />
<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/Parametros/_ResultadosView.vbhtml", Model.list)
    End Code
</div>



