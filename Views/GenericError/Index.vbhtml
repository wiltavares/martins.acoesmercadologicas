﻿@ModelType Exception
@Code
    ViewData("Title") = "Ops! :("
    Dim mensagem = "Mensagem de exceção não encontrada."
    Dim stackTrace = ""
    Dim tipo = ""
    If Model IsNot Nothing Then
        mensagem = Model.Message
        stackTrace = Model.StackTrace
        tipo = Model.GetType().ToString()
    End If
End Code

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
</head>
<body>
    <div class="widget-box widget-color-red">
        <div class="widget-header widget-header-small">
            <h4 class="widget-title smaller white lighter">
                <span class="bigger-100">
                    <i class="ace-icon fa fa-exclamation-circle"></i>
                </span>
                Ocorreu um erro inesperado
            </h4>
        </div>

        <div class="widget-body">
            <div class="widget-main">
                <h4 class="lighter smaller">Para mais informações entre em contato com os administradores do sistema. </h4>

                <hr class="widget-red-border-opacity" />

                <h5 class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#idCollapse" aria-expanded="false" style="cursor:pointer; color: #e2755f !important">
                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                    Detalhes do Erro
                </h5>
                <div Class="panel-collapse collapse" id="idCollapse" aria-expanded="false" style="height: 0px; color: #5c5a5a !important; font-weight: normal !important;">
                    <div class="widget-main" style="background-color: rgba(226, 117, 95, 0.20) !important">
                        <h5>
                            Mensagem: @Html.Raw(mensagem)
                            <br />
                            <br />
                            Tipo: @Html.Raw(tipo)
                            <br />
                            <br />
                            Trace: @HttpContext.Current.Server.HtmlDecode(stackTrace)
                            <br />
                        </h5>
                    </div>
                </div>

            </div>
            <div class="widget-toolbox  widget-custom padding-8 clearfix">
                <div class="btn-group pull-right">
                    <a href="javascript:history.back()" class="btn btn-white btn-danger">
                        <i class="ace-icon fa fa-arrow-left"></i>
                        Voltar
                    </a>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
