﻿@ModelType GridSettings(Of SimulaFundingViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Simula Funding"
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-bar-chart"></i>
            Simula Funding
        </li>
        <li class="active">Simula Funding</li>
    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modalItensOrcamento"></div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "SimulaFunding", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("lblNomFornecedor", "Fornecedor:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODFRN, New SelectList(ViewBag.ListaDropFornecedor, "CODFRN", "CodNomFrn", Model.Filter.CODFRN), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFornecedores"})
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("lblFilial", "Filial:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODFILEMP, New SelectList(ViewBag.filiais, "CODFILEMP", "DROPDESC", Model.Filter.CODFILEMP), New With {.Class = "form-control", .id = "dropFilial"})
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            @Html.Label("item", "Mercadorias:")
                        </div>
                        <div id="dividItem">
                            <div class="col-xs-3 col-lg-3 col-sm-3">
                                @Html.TextBoxFor(Function(f) f.Filter.CODMER, New With {.Class = "form-control", .id = "idItemMerc", .alt = "inteiro_positivo"})
                            </div>
                            <div class="col-xs-9 col-lg-9 col-sm-9">
                                <div class="input-group ">
                                    @Html.TextBoxFor(Function(f) f.Filter.DESMER, Nothing, New With {.Class = "form-control", .id = "NomItemMerc"})
                                    <span class="input-group-btn">
                                        <button class="btn btn-white" id="btnBuscaItens" type="button" onclick="modalBuscaItensOrcamentoSimulaFunding()" style="display: block">
                                            <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                        </button>
                                        <button class="btn btn-white" id="btnTiraItens" type="button" onclick="tirarItensOrcamento()" style="display: none">
                                            <i class="ace-icon fa fa-close bigger-120 red"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-3">
                            @Html.Label("lblQuantidade", "Quantidade:")
                            @Html.TextBoxFor(Function(f) f.Filter.QUANTIDADE, New With {.Class = "form-control", .id = "txtquantidade", .alt = "inteiro_positivo"})
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            @Html.Label("lvlParticipacao", "Participação Cliente(%):")
                            @Html.TextBoxFor(Function(f) f.Filter.PARTICIPACAO, New With {.Class = "form-control", .id = "txtParticipacao", .alt = "inteiro_positivo"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white ">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                    Simula
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger"></i>
                                    Limpar
                                </a>
                                <a onclick="ExportarFunding()" href="#" class="btn btn-info btn-white" id="btnImprimirRel">
                                    <i class="ace-icon fa fa-file-excel-o bigger-120"></i>
                                    Exportar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/SimulaFunding/_ResultadosView.vbhtml", Model)
    End Code
</div>



<script>
    function ExportarFunding() {

        LoadingOn();

        var fornecedor = parseInt($('#dropFornecedores').val());
        var filial = parseInt($('#dropFilial').val());
        var codmer = parseInt($('#idItemMerc').val());
        var desmer = $('#NomItemMerc').val();
        var qt = parseInt($('#txtquantidade').val());
        var participacao = parseInt($('#txtParticipacao').val());


        data = {
            CODFRN: fornecedor,
            CODFILEMP: filial,
            CODMER: codmer,
            DESMER: desmer,
            PARTICIPACAO: participacao,
            QUANTIDADE: qt
        };

        $.ajax({
            cache: false,
            url: SiteUrl + "SimulaFunding/Exportar",
            data: data,
            success: function (data) {
                var response = data;

                window.location = SiteUrl + 'SimulaFunding/Download?fileGuid=' + response.FileGuid
                            + '&filename=' + response.FileName;
                LoadingOff();
            },
            error: function () {
                LoadingOff();
            }
        })
    };
</script>