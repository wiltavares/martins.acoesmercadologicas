﻿@ModelType GridSettings(Of SimulaFundingViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, " hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div>
                <div style="width: auto; overflow-x: scroll;">
                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1500px">
                        <thead>
                            <tr role="row">
                                @code
                                    @<th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy)">
                                        Cod. Fornecedor
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("DESFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESFRN", Model.Page.OrderBy)">
                                        Fornecedor
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("DESFILEMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESFILEMP", Model.Page.OrderBy)">
                                        Filial
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("CODMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMER", Model.Page.OrderBy)">
                                        Código Mercadoria
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("DESMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESMER", Model.Page.OrderBy)">
                                        Mercadoria
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("UnitarioCliente", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("UnitarioCliente", Model.Page.OrderBy)">
                                        Participação Cliente
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("UnitarioCalculado", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("UnitarioCalculado", Model.Page.OrderBy)">
                                        Valor Unitário
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("QUANTIDADE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("QUANTIDADE", Model.Page.OrderBy)">
                                        Quantidade
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("TotalCalculado", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("TotalCalculado", Model.Page.OrderBy)">
                                        Total Funding
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("TotalCliente", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("TotalCliente", Model.Page.OrderBy)">
                                        Total Cliente
                                    </th>
                                End Code
                            </tr>
                        </thead>
                        <tbody>
                            @For Each item In Model.ListPaged
                                @<tr>
                                    <td>
                                        @item.CODFRN
                                    </td>
                                    <td>
                                        @item.DESFRN
                                    </td>
                                     <td>
                                         @item.DESFILEMP
                                     </td>
                                    <td>
                                        @item.CODMER
                                    </td>
                                    <td>
                                        @item.DESMER
                                    </td>
                                    <td>
                                        @item.UnitarioCliente
                                    </td>
                                    <td>
                                        @item.UnitarioCalculado
                                    </td>
                                    <td>
                                        @item.QUANTIDADE
                                    </td>
                                    <td>
                                        @code
                                            If (Not IsNothing(item.TotalCalculado)) Then
                                                @Math.Round(item.TotalCalculado.Value, 2)
                                            End If
                                        End Code
                                    </td>
                                    <td>
                                        @code
                                            If (Not IsNothing(item.TotalCliente)) Then
                                                @Math.Round(item.TotalCliente.Value, 2)
                                            End If
                                        End Code
                                    </td>
                                </tr>
                                            Next
                        </tbody>
                    </table>
                </div>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code
        </div>
    </div>
</div>
