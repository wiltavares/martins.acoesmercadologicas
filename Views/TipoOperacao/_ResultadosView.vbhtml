﻿@ModelType GridSettings(Of TipoOperacaoViewModel)
@Imports Martins.AcoesMercadologicas.Core

@Html.Partial(Constantes.VIEW_MODAL_CONFIRMACAO, New Tuple(Of String, String, Object)("Delete", "TipoOperacao", Model))

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 2500px;">
                    <thead>
                        <tr role="row">
                            <th class="center">Ações</th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODOPEFSCDSNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODOPEFSCDSNACOCMC", Model.Page.OrderBy)">
                                Código
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESOPEFSCDSNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESOPEFSCDSNACOCMC", Model.Page.OrderBy)">
                                Tipo de Operação
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDTIPOPEDSNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDTIPOPEDSNACOCMC", Model.Page.OrderBy)">
                                Busca Estoque Martins
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDUNDESRNGCACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDUNDESRNGCACOCMC", Model.Page.OrderBy)">
                                Operação Brinde
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDBDEOPEDSNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDBDEOPEDSNACOCMC", Model.Page.OrderBy)">
                                Informar BU
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDTIPOPEICT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDTIPOPEICT", Model.Page.OrderBy)">
                                Operação de Incentivo
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDTIPOPEVLRMON", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDTIPOPEVLRMON", Model.Page.OrderBy)">
                                Operação Paga em Dinheiro
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDGRCITEENCFOLPGT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDGRCITEENCFOLPGT", Model.Page.OrderBy)">
                                Gera encargo de Folha
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDGRCREQAUTITERSU", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDGRCREQAUTITERSU", Model.Page.OrderBy)">
                                Requisição Auto
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDTIPOPEACOCMCAUT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDTIPOPEACOCMCAUT", Model.Page.OrderBy)">
                                Operação Gerada Automáticamente
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDTIPOPEREQVGM", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDTIPOPEREQVGM", Model.Page.OrderBy)">
                                Requisição de Viagem
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDTIPOPEREQEVT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDTIPOPEREQEVT", Model.Page.OrderBy)">
                                Requisição de Eventos
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDTIPOPESLCPGT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDTIPOPESLCPGT", Model.Page.OrderBy)">
                                Solicitação de Pagamento
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDPTCCLIOPEDSN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDPTCCLIOPEDSN", Model.Page.OrderBy)">
                                Participação de Cliente
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td Class="center">
                                    <div Class="hidden-sm hidden-xs btn-group">
                                        <a data-rel="tooltip" title="Editar" Class="btn btn-xs btn-info" href="@Url.Action("CreateEdit", New With {.id = item.CODOPEFSCDSNACOCMC})">
                                            <i Class="ace-icon fa fa-pencil bigger-119"></i>
                                        </a>
                                        @*<a data-rel="tooltip" title="Deletar" Class="btn btn-xs btn-danger" data-toggle="modal" data-target="#confirmationDelete" data-id="@item.CODOPEFSCDSNACOCMC">
                                            <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                        </a>*@
                                    </div>

                                    <div Class="hidden-md hidden-lg">
                                        <div Class="inline pos-rel">
                                            <Button Class="btn btn-minier btn-md btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                                <i Class="ace-icon fa fa-cog icon-only"></i>
                                            </Button>

                                            <ul Class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                <li>
                                                    <a Class="tooltip-info" data-rel="tooltip" href="@Url.Action("CreateEdit", New With {.id = item.CODOPEFSCDSNACOCMC})">
                                                        <i Class="ace-icon fa fa-pencil-square-o blue"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a Class="tooltip-info" data-rel="tooltip" data-toggle="modal" data-target="#confirmationDelete" data-id="@item.CODOPEFSCDSNACOCMC">
                                                        <i Class="ace-icon fa fa-trash-o red"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    @Html.Label(CInt(item.CODOPEFSCDSNACOCMC))
                                </td>
                                <td>
                                    @Html.Label(CStr(item.DESOPEFSCDSNACOCMC))
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDTIPOPEDSNACOCMC)=0,"Não", "Sim"))
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDUNDESRNGCACOCMC)=0,"Não", "Sim"))

                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDBDEOPEDSNACOCMC) = 0, "Não", "Sim"))
                                    
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDTIPOPEICT) = 0, "Não", "Sim"))
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDTIPOPEVLRMON) = 0, "Não", "Sim"))
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDGRCITEENCFOLPGT) = 0, "Não", "Sim"))
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDGRCREQAUTITERSU) = 0, "Não", "Sim"))
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDTIPOPEACOCMCAUT) = 0, "Não", "Sim"))
                                </td>
                                <td>
                                   @Html.Label(If(CInt(item.INDTIPOPEREQVGM)=0,"Não", "Sim"))
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDTIPOPEREQEVT) = 0, "Não", "Sim"))
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDTIPOPESLCPGT) = 0, "Não", "Sim"))
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDPTCCLIOPEDSN) = 0, "Não", "Sim"))
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>