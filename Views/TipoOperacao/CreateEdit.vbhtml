﻿@ModelType TipoOperacaoViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim tituloTela = IIf(ViewBag.acao = "Save", "Incluir Tipo Operação", "Editar Tipo Operação")
    ViewData("Title") = tituloTela
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Tipo Operação</li>
        @Code
            Dim campo = IIf(ViewBag.acao = "Save", "Incluir", "Editar ")
        End Code
        <li class="active">@campo</li>
    </ul>
</div>
<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-file-text-o bigger-110"></i>
            Dados
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Html.BeginForm(ViewBag.acao, "TipoOperacao"))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("lblCodigo", "Código:")
                            @Html.EditorFor(Function(model) model.CODOPEFSCDSNACOCMC, IIf(IsNothing(Model.CODOPEFSCDSNACOCMC) OrElse Model.CODOPEFSCDSNACOCMC=0, "", Model.CODOPEFSCDSNACOCMC), New With {.htmlAttributes = New With {.class = "form-control", .disabled = "true"}})
                            @Html.HiddenFor(Function(model) model.CODOPEFSCDSNACOCMC)
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("lblTipoDeOperacao", "Tipo de Operação (*):")
                            @Html.EditorFor(Function(model) model.DESOPEFSCDSNACOCMC, New With {.htmlAttributes = New With {.class = "form-control"}})
                            @Html.ValidationMessageFor(Function(model) model.DESOPEFSCDSNACOCMC, "", New With {.class = "text-danger-message-login"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Busca Estoque Martins (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEDSNACOCMC, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @*@Html.RadioButtonFor(Function(Model) Model.INDTIPOPEDSNACOCMC, 0, New With {.checked = "checked"})*@
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEDSNACOCMC, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDTIPOPEDSNACOCMC, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Operação Brinde (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDUNDESRNGCACOCMC, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @*@Html.RadioButtonFor(Function(Model) Model.INDBDEOPEDSNACOCMC, 0, New With {.checked = "checked"})*@
                                    @Html.RadioButtonFor(Function(Model) Model.INDUNDESRNGCACOCMC, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDUNDESRNGCACOCMC, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Informar BU (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDBDEOPEDSNACOCMC, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @*@Html.RadioButtonFor(Function(Model) Model.INDUNDESRNGCACAOCMC, 0, New With {.checked = "checked"})*@
                                    @Html.RadioButtonFor(Function(Model) Model.INDBDEOPEDSNACOCMC, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDBDEOPEDSNACOCMC, "", New With {.class = "text-danger-message-login"})
                        </div>
                        @*@code
                        Dim tipoFrn As Decimal
                        If (Model.INDTIPOPEICT IsNot Nothing andlso Model.INDTIPOPEICT==1) Then
                            tipoFrn = Model.INDTIPOPEICT
                        Else
                            tipoFrn = 1
                        End If
                        End Code*@
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Operação de Incentivo (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEICT, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEICT, 0)
                                    @*<input @IIf(ViewBag.pnlTipoPatrocinadorEnable, "", "disabled = 'disabled'") @IIf(tipoFrn = 1, "checked", "") id="rdbFornecedor" name="patrocinadorRdb" onchange="configuraPatrocinador(1)" type="radio" value="1">*@
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDTIPOPEICT, "", New With {.class = "text-danger-message-login"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Operação Paga em Dinheiro (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEVLRMON, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @*@Html.RadioButtonFor(Function(Model) Model.INDGRCITEENCFOLPGT, 0, New With {.checked = "checked"})*@
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEVLRMON, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDTIPOPEVLRMON, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Gera Item de encargo de Folha (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDGRCITEENCFOLPGT, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @*@Html.RadioButtonFor(Function(Model) Model.INDTIPOPEACOCMCAUT, 0, New With {.checked = "checked"})*@
                                    @Html.RadioButtonFor(Function(Model) Model.INDGRCITEENCFOLPGT, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDGRCITEENCFOLPGT, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Requisição Automática Suprimentos (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDGRCREQAUTITERSU, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @*@Html.RadioButtonFor(Function(Model) Model.INDTIPOPEICT, 0, New With {.checked = "checked"})*@
                                    @Html.RadioButtonFor(Function(Model) Model.INDGRCREQAUTITERSU, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDGRCREQAUTITERSU, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Operação Gerada Automáticamente (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEACOCMCAUT, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @*@Html.RadioButtonFor(Function(Model) Model.INDTIPOPEVLRMON, 0, New With {.checked = "checked"})*@
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEACOCMCAUT, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDTIPOPEACOCMCAUT, "", New With {.class = "text-danger-message-login"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Operação Requisição de Viagem (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEREQVGM, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @*@Html.RadioButtonFor(Function(Model) Model.INDTIPOPEREQVGM, 0, New With {.checked = "checked"})*@
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEREQVGM, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDTIPOPEREQVGM, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Operação Requisição de Eventos (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEREQEVT, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @*@Html.RadioButtonFor(Function(Model) Model.INDTIPOPEREQEVT, 0, New With {.checked = "checked"})*@
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPEREQEVT, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDTIPOPEREQEVT, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Operação Solicitação de Pagamento (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPESLCPGT, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @*@Html.RadioButtonFor(Function(Model) Model.INDTIPOPESLCPGT, 0, New With {.checked = "checked"})*@
                                    @Html.RadioButtonFor(Function(Model) Model.INDTIPOPESLCPGT, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDTIPOPESLCPGT, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("BscEstqMartins", "Operação Participação de Cliente (*):")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(Model) Model.INDPTCCLIOPEDSN, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-sm-6">
                                    @*@Html.RadioButtonFor(Function(Model) Model.INDPTCCLIOPEDSN, 0, New With {.checked = "checked"})*@
                                    @Html.RadioButtonFor(Function(Model) Model.INDPTCCLIOPEDSN, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                            @Html.ValidationMessageFor(Function(model) model.INDPTCCLIOPEDSN, "", New With {.class = "text-danger-message-login"})
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-white btn-success">
                                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                                    Salvar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-white btn-danger">
                                    <i class="ace-icon fa fa-times bigger-120 red"></i>
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
