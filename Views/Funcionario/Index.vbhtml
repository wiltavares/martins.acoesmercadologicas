﻿@ModelType IEnumerable(Of FuncionarioViewModel)
@Code
ViewData("Title") = "Index"
End Code

<h2>Index</h2>

<p>
    @Html.ActionLink("Create New", "Create")
</p>
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.NOMFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODDPT)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODSEC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODEMP)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.DATADSFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.DATDEMFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.ENDFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODCID)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODBAIFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODCEPFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.NUMTLFFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODFILFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODSEXFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODDRT)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODDIV)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODCENCST)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODGRPPGTFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODSETFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.NUMCNTCRRFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODAGEBCOFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODBCOFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODCGRFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.NUMCPFFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODSNDFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.DATNSC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODUNDNGC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODFILEMP)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODUNDREG)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODSGMMCDCTB)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.NUMRMLTLF)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.DESPSWFNCPAD)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.DESPSWCDF)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.DESENDCREETNFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODNIVFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.INDTRBFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.NUMCARCCP)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.NUMCARRLP)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.DATVLDCARCCP)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.DATVLDCARRLP)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.DATPRVDEMFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.NUMCPF)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.DATALTCADFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.TIPFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.NOMFNCEXD)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.INDEQIFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.NUMENDFNC)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CODEDEOCDRDF)
        </th>
        <th></th>
    </tr>

@For Each item In Model
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.NOMFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODDPT)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODSEC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODEMP)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.DATADSFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.DATDEMFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.ENDFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODCID)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODBAIFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODCEPFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.NUMTLFFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODFILFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODSEXFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODDRT)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODDIV)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODCENCST)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODGRPPGTFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODSETFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.NUMCNTCRRFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODAGEBCOFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODBCOFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODCGRFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.NUMCPFFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODSNDFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.DATNSC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODUNDNGC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODFILEMP)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODUNDREG)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODSGMMCDCTB)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.NUMRMLTLF)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.DESPSWFNCPAD)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.DESPSWCDF)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.DESENDCREETNFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODNIVFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.INDTRBFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.NUMCARCCP)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.NUMCARRLP)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.DATVLDCARCCP)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.DATVLDCARRLP)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.DATPRVDEMFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.NUMCPF)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.DATALTCADFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.TIPFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.NOMFNCEXD)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.INDEQIFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.NUMENDFNC)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CODEDEOCDRDF)
        </td>
        <td>
            @*@Html.ActionLink("Edit", "Edit", New With {.id = item.PrimaryKey}) |
            @Html.ActionLink("Details", "Details", New With {.id = item.PrimaryKey}) |
            @Html.ActionLink("Delete", "Delete", New With {.id = item.PrimaryKey})*@
        </td>
    </tr>
Next

</table>
