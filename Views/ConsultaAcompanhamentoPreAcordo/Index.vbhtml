﻿@ModelType GridSettings(Of AcompanhamentoPreAcordoViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Consulta Acompanhamento Pré-Acordo"
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-bar-chart"></i>
            Relatórios
        </li>
        <li class="active">Consulta Acompanhamento Pré-Acordo</li>
    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modalItensOrcamento"></div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">   
            @Using (Ajax.BeginForm("Pesquisar", "ConsultaAcompanhamentoPreAcordo", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3" id="txtCodFornecedor">
                            @Html.Label("lblCodPreAcordo", "Pré-Acordo:")
                            @Html.TextBoxFor(Function(m) m.Filter.CODACOCMC, New With {.Class = "form-control", .id = "CodPreAcordo", .alt = "inteiro"})
                        </div>

                        <div class="col-lg-5 col-sm-5">
                            @Html.Label("lblPeriodo", "Período(*):")
                            @Html.HiddenFor(Function(m) m.Filter.DataIniAux, New With {.id = "dataIniAux"})
                            @Html.HiddenFor(Function(m) m.Filter.DataFimAux, New With {.id = "dataFimAux"})
                            <div class="input-daterange input-group">
                                @Html.TextBoxFor(Function(m) m.Filter.DATINIACOCMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataIni()", .id = "dataIni", .autocomplete = "off"})
                                @Html.ValidationMessageFor(Function(m) m.Filter.DATINIACOCMC, "", New With {.class = "text-danger-message-login"})
                                <span class="input-group-addon">
                                    à
                                </span>
                                @Html.TextBoxFor(Function(m) m.Filter.DATFIMACOCMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataFim()", .id = "dataFim", .autocomplete = "off"})
                                @Html.ValidationMessageFor(Function(m) m.Filter.DATFIMACOCMC, "", New With {.class = "text-danger-message-login"})
                            </div>
                        </div>
                        <div class="col-sm-4">
                            @Html.Label("lblCelula", "Célula:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODDIVCMP, New SelectList(ViewBag.filtroCelulaCarimbo, "CODDIVCMP", "DESDIVCMP", Model.Filter.CODDIVCMP), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFiltroCelula"})
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("lblComprador", "Comprador:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODCPR, New SelectList(ViewBag.filtroComprador, "CODCPR", "NOMCPR", Model.Filter.CODCPR), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFiltroComprador"})
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("lblPromocao", "Promoção:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODPMC, New SelectList(ViewBag.filtroPromocao, "CODPMC", "CodNomProm", Model.Filter.CODPMC), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFiltroPromocao"})
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-sm-4">
                            @Html.Label("lblNomFornecedor", "Fornecedor:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODFRN, New SelectList(ViewBag.ListaDropFornecedor, "CODFRN", "CodNomFrn", Model.Filter.CODFRN), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFornecedores"})
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            @Html.Label("lblNomFornecedorPatrocinador", "Fornecedor Patrocinador:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODFRNPTR, New SelectList(ViewBag.ListaDropFornecedorPatrocinador, "CODFRN", "CodNomFrn", Model.Filter.CODFRNPTR), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFornecedoresPatrocinador"})
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            @Html.Label("filial", "Filial:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODFILEMP, New SelectList(ViewBag.listaFilial, "CODFILEMP", "CODFILIAL_NOMFILIAL", Model.Filter.CODFILEMP), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFilial"})
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            @Html.Label("item", "Mercadorias:")
                        </div>
                        <div id="dividItem">
                            <div class="col-xs-3 col-lg-3 col-sm-3">
                                @Html.TextBoxFor(Function(f) f.Filter.CODMER, New With {.Class = "form-control", .id = "idItemMerc"})
                            </div>
                            <div class="col-xs-9 col-lg-9 col-sm-9">
                                <div class="input-group ">
                                    @Html.TextBoxFor(Function(f) f.Filter.DESMER, Nothing, New With {.Class = "form-control", .id = "NomItemMerc"})
                                    <span class="input-group-btn">
                                        <button class="btn btn-white" id="btnBuscaItens" type="button" onclick="modalBuscaItensOrcamento()" style="display: block">
                                            <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                        </button>
                                        <button class="btn btn-white" id="btnTiraItens" type="button" onclick="tirarItensOrcamento()" style="display: none">
                                            <i class="ace-icon fa fa-close bigger-120 red"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            @Html.Label("lblTipoOperacao", "Tipo Operação:")
                            <div class="row">
                                <div class="col-sm-4">
                                    @Html.RadioButtonFor(Function(m) m.Filter.TIPOPEINIGRCHST, "V", New With {.id = "rdbTipoOperacaoVend", .checked = "checked"})
                                    @Html.Label("venda", "Venda")
                                </div>
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(m) m.Filter.TIPOPEINIGRCHST, "F", New With {.id = "rdbTipoOperacaoFat"})
                                    @Html.Label("faturamento", "Faturamento")
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            @Html.Label("lblVisao", "Visão:")
                            <div class="row">
                                <div class="col-sm-4">
                                    @Html.RadioButtonFor(Function(m) m.Filter.IndVisao, 0, New With {.id = "rdbFornecedor"})
                                    @Html.Label("fornecedor", "Fornecedor")
                                </div>
                                <div class="col-sm-4">
                                    @Html.RadioButtonFor(Function(m) m.Filter.IndVisao, 1, New With {.id = "rdbMercadoria", .checked = "checked"})
                                    @Html.Label("mercadoria", "Mercadoria")
                                </div>
                                <div class="col-sm-4">
                                    @Html.RadioButtonFor(Function(m) m.Filter.IndVisao, 2, New With {.id = "rdbCliente"})
                                    @Html.Label("cliente", "Cliente")
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            @Html.Label("lblVisao", "Tipo Campanha:")
                            <div class="row">
                                <div class="col-sm-3">
                                    @Html.RadioButtonFor(Function(m) m.Filter.INDCPHBTB, 0, New With {.id = "rdbMartins", .checked = "checked"})
                                    @Html.Label("martins", "Martins")
                                </div>
                                <div class="col-sm-3">
                                    @Html.RadioButtonFor(Function(m) m.Filter.INDCPHBTB, 1, New With {.id = "rdbB2B"})
                                    @Html.Label("b2b", "B2B")
                                </div>
                                <div class="col-sm-3">
                                    @Html.RadioButtonFor(Function(m) m.Filter.INDCPHBTB, -1, New With {.id = "rdbAmbos"})
                                    @Html.Label("ambos", "Ambos")
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white ">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger"></i>
                                    Limpar
                                </a>
                                <a onclick="btnImprimirRel()" href="#" class="btn btn-info btn-white" id="btnImprimirRel">
                                    <i class="ace-icon fa fa-file-excel-o bigger-120"></i>
                                    Exportar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/ConsultaAcompanhamentoPreAcordo/_ResultadosView.vbhtml", Model)
    End Code
</div>



