﻿@ModelType GridSettings(Of AcompanhamentoPreAcordoViewModel)
@Imports Martins.AcoesMercadologicas.Core
@code
    ' Dim QtdMerc As Decimal?
    'QtdMerc = Model.Filter.QuantidadeMerc
    'Dim TotalGeral As Decimal?
    'TotalGeral = Model.Filter.TotalGeral
    Dim visao As Decimal?
    Dim PedidoNF As String = ""
    Dim tipOp As String = ""
    visao = Model.Filter.IndVisao

    If (Model.Filter.TIPOPEINIGRCHST = "V") Then
        PedidoNF = "Pedido"
        tipOp = Model.Filter.TIPOPEINIGRCHST
    Else
        PedidoNF = "Nota Fiscal"
        tipOp = Model.Filter.TIPOPEINIGRCHST
    End If
End Code

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, " hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">

            <div style="width: auto; overflow-x: scroll;">
                @code
                    Dim tamanhoGrid = "2000px;"
                    If (visao = 1) Then
                        tamanhoGrid = "2200px;"
                    ElseIf (visao = 2) Then
                        tamanhoGrid = "2800px;"
                        If (tipOp = "F") Then
                            tamanhoGrid = "2900px;"
                        End If
                    End If
                End Code
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: @tamanhoGrid">
                    <thead>
                        <tr role="row">
                            @code
                                @<th class="@Grid.AtribuirIconeOrdenacao("DATREFOPE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATREFOPE", Model.Page.OrderBy)">
                                    Data Movimento
                                </th>
                                @<th class="@Grid.AtribuirIconeOrdenacao("CODFILEMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFILEMP", Model.Page.OrderBy)">
                                    Código Filial
                                </th>
                                @<th class="@Grid.AtribuirIconeOrdenacao("NOMFILEMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFILEMP", Model.Page.OrderBy)">
                                    Filial
                                </th>

                                @<th class="@Grid.AtribuirIconeOrdenacao("CODFRNPTR", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRNPTR", Model.Page.OrderBy)">
                                    Código Frn Patrocinador
                                </th>
                                @<th class="@Grid.AtribuirIconeOrdenacao("NOMFRNPTR", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFRNPTR", Model.Page.OrderBy)">
                                    Fornecedor Patrocinador
                                </th>

                                @<th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy)">
                                    Código Fornecedor
                                </th>
                                @<th class="@Grid.AtribuirIconeOrdenacao("NOMFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFRN", Model.Page.OrderBy)">
                                    Fornecedor
                                </th>
                                @<th class="@Grid.AtribuirIconeOrdenacao("INDCPHBTB", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDCPHBTB", Model.Page.OrderBy)">
                                    Tipo Campanha
                                </th>

                                'Cliente e Mercadoria
                                If (visao = 1 Or visao = 2) Then
                                    @<th class="@Grid.AtribuirIconeOrdenacao("CODMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMER", Model.Page.OrderBy)">
                                        Código Mercadoria
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("DESMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESMER", Model.Page.OrderBy)">
                                        Mercadoria
                                    </th>
                                End If

                                @<th class="@Grid.AtribuirIconeOrdenacao("CODACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODACOCMC", Model.Page.OrderBy)">
                                    Código Pré-Acordo
                                </th>
                                @<th class="@Grid.AtribuirIconeOrdenacao("NOMACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMACOCMC", Model.Page.OrderBy)">
                                    Pré-Acordo
                                </th>

                                @<th class="@Grid.AtribuirIconeOrdenacao("CODPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODPMC", Model.Page.OrderBy)">
                                    Código Promoção
                                </th>
                                @<th class="@Grid.AtribuirIconeOrdenacao("DESPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESPMC", Model.Page.OrderBy)">
                                    Promoção
                                </th>

                                'Cliente
                                If (visao = 2) Then
                                    @<th class="@Grid.AtribuirIconeOrdenacao("CODCLI", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODCLI", Model.Page.OrderBy)">
                                        Código Cliente
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("NOMCLI", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMCLI", Model.Page.OrderBy)">
                                        Cliente
                                    </th>
                                    @<th class="@Grid.AtribuirIconeOrdenacao("NUMITEORI", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NUMITEORI", Model.Page.OrderBy)">
                                        Nº Item
                                    </th>

                                    @<th class="@Grid.AtribuirIconeOrdenacao("NUMDOCOPE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NUMDOCOPE", Model.Page.OrderBy)">
                                        @PedidoNF
                                    </th>

                                    If (tipOp = "F") Then
                                        @<th class="@Grid.AtribuirIconeOrdenacao("DATIPRNOTFSC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATIPRNOTFSC", Model.Page.OrderBy)">
                                            Data da Impressão
                                        </th>
                                    End If
                                End If

                                @<th class="@Grid.AtribuirIconeOrdenacao("QDEITEPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("QDEITEPMC", Model.Page.OrderBy)">
                                    Quantidade
                                </th>
                                @<th class="@Grid.AtribuirIconeOrdenacao("VLRUNTFNDMERPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUNTFNDMERPMC", Model.Page.OrderBy)">
                                    Valor Unitário
                                </th>
                                @<th class="@Grid.AtribuirIconeOrdenacao("Total", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("Total", Model.Page.OrderBy)">
                                    Total
                                </th>
                            End Code
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td>
                                    @IIf(Not IsNothing(item.DATREFOPE), item.DATREFOPE.Value.ToShortDateString, "")
                                </td>
                                <td>
                                    @item.CODFILEMP
                                </td>
                                <td>
                                    @item.NOMFILEMP
                                </td>

                                <td>
                                    @item.CODFRNPTR
                                </td>
                                <td>
                                    @item.NOMFRNPTR
                                </td>

                                <td>
                                    @item.CODFRN
                                </td>
                                <td>
                                    @item.NOMFRN
                                </td>
                                <td>
                                    @If item.INDCPHBTB = 1 Then
                                        @<span>B2B</span>
                                    Else
                                        @<span>Martins</span>
                                    End If
                                </td>
                                @If (visao = 1 Or visao = 2) Then
                                    @<td>
                                        @item.CODMER
                                    </td>
                                    @<td>
                                        @item.DESMER
                                    </td>
                                End If
                                <td>
                                    @item.CODACOCMC
                                </td>
                                <td>
                                    @item.NOMACOCMC
                                </td>
                                <td>
                                    @item.CODPMC
                                </td>
                                <td>
                                    @item.DESPMC
                                </td>
                                @If (visao = 2) Then
                                    @<td>
                                        @item.CODCLI
                                    </td>
                                    @<td>
                                        @item.NOMCLI
                                    </td>
                                    @<td>
                                        @item.NUMITEORI
                                    </td>
                                    @<td>
                                        @item.NUMDOCOPE
                                    </td>
                                    If (tipOp = "F") Then
                                    @<td>
                                        @IIf(Not IsNothing(item.DATIPRNOTFSC), item.DATIPRNOTFSC.Value.ToShortDateString, "")
                                    </td>
                                    End If
                                End If
                                <td>
                                    @item.QDEITEPMC
                                </td>
                                <td>
                                    @item.VLRUNTFNDMERPMC
                                </td>
                                <td>
                                    @item.Total
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                </div>
                <div class="col-sm-2">
                    Quantidade: @Model.Filter.QuantidadeMerc
                </div>
                <div class="col-sm-3">
                    Total Geral: @Model.Filter.TotalGeral.GetValueOrDefault.ToString("N2")
                </div>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code
        </div>
    </div>
</div>