﻿@ModelType GridSettings(Of PlanoDeMarketingViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Html.Partial(Constantes.VIEW_MODAL_CONFIRMACAO, New Tuple(Of String, String, Object)("Delete", "PlanoDeMarketing", Model))


<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1500px;">
                    <thead>
                        <tr role="row">
                            <th class="center">Ações</th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESUNDESRNGC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESUNDESRNGC", Model.Page.OrderBy)">
                                BU
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMTIPEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMTIPEVTACOCMC", Model.Page.OrderBy)">
                                Tipo de Evento
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMEVTACOCMC", Model.Page.OrderBy)">
                                Evento
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("ANOMESREFDAT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("ANOMESREFDAT", Model.Page.OrderBy)">
                                Data de Referência
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLROCDPLNMKT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLROCDPLNMKT", Model.Page.OrderBy)">
                                Vr. Orçamento
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZPLNMKT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZPLNMKT", Model.Page.OrderBy)">
                                Vr. Utilizado
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLRRSVPLNMKT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRRSVPLNMKT", Model.Page.OrderBy)">
                                Vr. Reservado
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLROCDCTTPLNMKT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLROCDCTTPLNMKT", Model.Page.OrderBy)">
                                Vr. Orçado Contrato
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLROCDEXACTTPLNMKT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLROCDEXACTTPLNMKT", Model.Page.OrderBy)">
                                Vr. Orçado Extra Ctt
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td class="center">
                                    <div class="hidden-sm hidden-xs btn-group">
                                        <a data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" href="@Url.Action("CreateEdit", New With {.TIPEVTACOCMC = item.TIPEVTACOCMC, .CODEVTACOCMC = item.CODEVTACOCMC, .ANOMESREF = item.ANOMESREF, .CODUNDESRNGC = item.CODUNDESRNGC})">
                                            <i class="ace-icon fa fa-pencil bigger-119"></i>
                                        </a>
                                        @*<a data-rel="tooltip" title="Deletar" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#confirmationDelete" data-id="@(item.TIPEVTACOCMC.Value.ToString + ":" + item.CODEVTACOCMC.Value.ToString + ":" + item.ANOMESREF.Value.ToString + ":" + item.CODUNDESRNGC.Value.ToString)">
                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                        </a>*@
                                    </div>

                                    <div class="hidden-md hidden-lg">
                                        <div class="inline pos-rel">
                                            <button class="btn btn-minier btn-md btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                                <i class="ace-icon fa fa-cog icon-only"></i>
                                            </button>

                                            <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                <li>
                                                    <a href="@Url.Action("CreateEdit", New With {.TIPEVTACOCMC = item.TIPEVTACOCMC, .CODEVTACOCMC = item.CODEVTACOCMC, .ANOMESREF = item.ANOMESREF, .CODUNDESRNGC = item.CODUNDESRNGC})" class="tooltip-info" data-rel="tooltip" title="" data-original-title="Editar">
                                                        <i class="ace-icon fa fa-pencil-square-o blue"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="tooltip-info" data-rel="tooltip" data-toggle="modal" data-target="#confirmationDelete" data-id="@(item.TIPEVTACOCMC.Value.ToString + ":" + item.CODEVTACOCMC.Value.ToString + ":" + item.ANOMESREF.Value.ToString + ":" + item.CODUNDESRNGC.Value.ToString)">
                                                        <i class="ace-icon fa fa-trash-o red"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    @Html.Label(IIf(Not IsNothing(item.DESUNDESRNGC), item.DESUNDESRNGC, ""))
                                </td>
                                <td>
                                    @Html.Label(IIf(Not IsNothing(item.NOMTIPEVTACOCMC), item.NOMTIPEVTACOCMC, ""))
                                </td>
                                <td>
                                    @Html.Label(IIf(Not IsNothing(item.NOMEVTACOCMC), item.NOMEVTACOCMC, ""))
                                </td>
                                <td>
                                    @Html.Label(item.ANOMESREFDAT.Value.Month.ToString & "/" & item.ANOMESREFDAT.Value.Year.ToString)
                                </td>
                                 <td class="text-right">
                                     @code
                                     Dim valor As New Decimal
                                     valor = item.VLROCDPLNMKT
                                     End Code
                                     @valor.ToString("#,##0.00")
                                 </td>
                                 <td class="text-right">
                                     @code
                                     valor = item.VLRUTZPLNMKT
                                     End Code
                                     @valor.ToString("#,##0.00")
                                 </td>
                                 <td class="text-right">
                                     @code
                                     valor = item.VLRRSVPLNMKT
                                     End Code
                                     @valor.ToString("#,##0.00")
                                 </td>
                                 <td class="text-right">
                                     @code
                                     valor = item.VLROCDCTTPLNMKT
                                     End Code
                                     @valor.ToString("#,##0.00")
                                 </td>
                                 <td class="text-right">
                                     @code
                                     valor = item.VLROCDEXACTTPLNMKT
                                     End Code
                                     @valor.ToString("#,##0.00")
                                 </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>
