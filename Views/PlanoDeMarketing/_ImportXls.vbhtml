﻿@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim AJAX_OBJECT_POST_PLANO As New AjaxOptions() With {
        .UpdateTargetId = "modalResultImport",
        .InsertionMode = InsertionMode.Replace,
        .OnComplete = "AbreModalImport",
        .HttpMethod = "POST"
    }
End Code
<br />
@Using (Ajax.BeginForm("importarPlanilha", "PlanoDeMarketing", AJAX_OBJECT_POST_PLANO, New With {.enctype = "multipart/form-data", .action = "#", .id = "arquivo"}))
    @<div class="row">
        <div class="col-lg-5">
            <label class="ace-file-input">
                <input type="file" name="file_upload" id="file_upload" class="ace-file-container" data-title="Choose" />
                <span Class="ace-file-container" data-title="Choose">
                    <span Class="ace-file-name" data-title="Sem arquivo ...">
                        <i Class=" ace-icon fa fa-upload">
                        </i>
                    </span>
                </span>
                <a Class="remove" href="#">
                    <i Class=" ace-icon fa fa-times">
                    </i>
                </a>
            </label>

            <button type="submit" name="Submit" value="Importar" class="btn btn-info btn-white">
                <i class="ace-icon fa fa-cloud-upload bigger-120"></i>
                Importar
            </button>
        </div>
    </div>

End Using