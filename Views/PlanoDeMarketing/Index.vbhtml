﻿@ModelType GridSettings(Of PlanoDeMarketingViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Plano de Marketing"

End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Plano de Marketing</li>
    </ul>
</div>
<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modal"></div>
<div class="modal fade" id="modalResultImport"></div>
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "PlanoDeMarketing", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("status", "Tipo de Evento:")
                            @Html.DropDownListFor(Function(m) m.Filter.TIPEVTACOCMC, New SelectList(ViewBag.tipoEvento, "TIPEVTACOCMC", "DROPDESC", Model.Filter.TIPEVTACOCMC), "Selecione...", New With {.Class = "form-control chosen-select"})
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("status", "Evento:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODEVTACOCMC, New SelectList(ViewBag.evento, "CODEVTACOCMC", "DROPDESC", Model.Filter.CODEVTACOCMC), "Selecione...", New With {.Class = "form-control chosen-select"})
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            @Html.Label("unidade", "Unidade de Negócio (BU):")
                            @Html.DropDownListFor(Function(m) m.Filter.CODUNDESRNGC, New SelectList(ViewBag.unidade, "CODUNDESRNGC", "DROPDESC", Model.Filter.TIPEVTACOCMC), "Selecione...", New With {.Class = "form-control chosen-select"})
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            @Html.Label("data", "Data de Referência:")
                            <div class="input-group">
                                @Html.TextBoxFor(Function(m) m.Filter.ANOMESREFDAT, CDate(Model.Filter.ANOMESREFDAT).ToShortDateString, New With {.Class = "form-control date-picker"})
                            </div>
                        </div>
                    </div>
                    <br />
                    <div Class="row">
                        <div Class="col-sm-12">
                            <div Class="btn-group pull-left">
                                <Button type="submit" Class="btn btn-info btn-white ">
                                    <i Class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </Button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger-120"></i>
                                    Limpar
                                </a>
                                <a href="@Url.Action("CreateEdit", New With {.TIPEVTACOCMC = 0})" class="btn btn-success btn-white">
                                    <i Class="ace-icon fa fa-plus bigger-120"></i>
                                    Cadastrar
                                </a>
                                <a Class="btn btn-white btn-yellow" id="btnImportXls" onclick="abrirModal('PlanoDeMarketing', 'AbrirModarImport', undefined, undefined, ModalImportXlsPlanoDeMarketingSuccess)">
                                    <i Class="ace-icon fa fa-file-excel-o bigger-120"></i>
                                    Importar Planilha
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using


        </div>
    </div>
</div>
<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/PlanoDeMarketing/_ResultadosView.vbhtml", Model)
    End Code
</div>
