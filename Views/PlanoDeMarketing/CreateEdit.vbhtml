﻿@ModelType PlanoDeMarketingViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim tituloTela = IIf(Url.Action.Split("=").Length = 1 OrElse Url.Action.Split("=")(1) = "0", "Incluir Plano De Marketing", "Editar Plano De Marketing")
    ViewData("Title") = tituloTela
    Dim hidden As String = IIf(Url.Action.Split("=").Length = 1 OrElse Url.Action.Split("=")(1) = "0", "hidden", "")
    Dim saveMethod = IIf(Url.Action.Split("=").Length = 1 OrElse Url.Action.Split("=")(1) = "0", "Save", "Alterar")
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Plano de Marketing</li>
        @Code
            Dim campo = IIf(Url.Action.Split("=").Length = 1 OrElse Url.Action.Split("=")(1) = "0", "Incluir", "Editar")
        End Code
        <li class="active">@campo</li>
    </ul>
</div>
<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>


<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-file-text-o bigger-110"></i>
            Dados
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Html.BeginForm(saveMethod, "PlanoDeMarketing"))
            @Html.AntiForgeryToken()
            @<div class="form-horizontal">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        @Html.Label("status", "Tipo de Evento (*):")
                        @Html.DropDownListFor(Function(m) m.TIPEVTACOCMC, New SelectList(ViewBag.tipoEvento, "TIPEVTACOCMC", "DROPDESC", Model.TIPEVTACOCMC), "Selecione...", New With {.Class = "form-control chosen-select", .onchange = "carregarEventosPorTipoEventoPlanoMarketing()", .id = "dropTipoEvento"})
                        @Html.ValidationMessageFor(Function(m) m.TIPEVTACOCMC, "", New With {.class = "text-danger-message-login"})
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        @Html.Label("status", "Evento (*):")
                        @Html.DropDownListFor(Function(m) m.CODEVTACOCMC, New SelectList(ViewBag.evento, "CODEVTACOCMC", "DROPDESC", Model.CODEVTACOCMC), "Selecione...", New With {.Class = "form-control chosen-select", .id = "dropEvento"})
                        @Html.ValidationMessageFor(Function(m) m.CODEVTACOCMC, "", New With {.class = "text-danger-message-login"})
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        @Html.Label("unidade", "Unidade de Negócio (*):")
                        @Html.DropDownListFor(Function(m) m.CODUNDESRNGC, New SelectList(ViewBag.unidade, "CODUNDESRNGC", "DROPDESC", Model.TIPEVTACOCMC), "Selecione...", New With {.Class = "form-control chosen-select"})
                        @Html.ValidationMessageFor(Function(m) m.CODUNDESRNGC, "", New With {.class = "text-danger-message-login"})
                    </div>
                    <div Class="col-lg-6 col-sm-6">
                        @Html.Label("data", "Data de Referência (*):")
                        <div class="input-daterange input-group">
                            @Html.TextBoxFor(Function(m) m.ANOMESREFDAT, If(Not IsNothing(Model.ANOMESREFDAT), CDate(Model.ANOMESREFDAT).ToShortDateString, Nothing), New With {.Class = "input-sm form-control"})
                            @Html.ValidationMessageFor(Function(m) m.ANOMESREF, "", New With {.class = "text-danger-message-login"})
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-4">
                        @Html.Label("orcadoContrato", "Valor Orçado no Contrato:")
                        @Html.TextBoxFor(Function(m) m.VLROCDCTTPLNMKT, "{0:0.00}", New With {.Class = "form-control", .onChange = "CalculaTotal()", .alt = "decimal_positivo_2"})
                        @Html.ValidationMessageFor(Function(m) m.VLROCDCTTPLNMKT, "", New With {.class = "text-danger-message-login"})
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        @Html.Label("orcadoContratoextra", "Valor Orçado Extra Contrato:")
                        @Html.TextBoxFor(Function(m) m.VLROCDEXACTTPLNMKT, "{0:0.00}", New With {.Class = "form-control", .onChange = "CalculaTotal()", .alt = "decimal_positivo_2"})
                        @Html.ValidationMessageFor(Function(m) m.VLROCDEXACTTPLNMKT, "", New With {.class = "text-danger-message-login"})
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        @Html.Label("orcado", "Valor Orçado:")
                        @Html.TextBoxFor(Function(m) m.VLROCDPLNMKT, "{0:0.00}", New With {.Class = "form-control", .readonly = "readOnly", .alt = "decimal_positivo_2"})
                    </div>
                </div>
                <div class="@hidden row">
                    <div class="col-lg-4 col-sm-4">
                        @Html.Label("orcadoContrato", "Valor Utilizado:")
                        @Html.TextBoxFor(Function(m) m.VLRUTZPLNMKT, "{0:0.00}", New With {.Class = "form-control", .readOnly = "readonly", .alt = "decimal_positivo_2"})
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        @Html.Label("orcadoContrato", "Valor Reservado:")
                        @Html.TextBoxFor(Function(m) m.VLRRSVPLNMKT, "{0:0.00}", New With {.Class = "form-control", .readOnly = "readonly", .alt = "decimal_positivo_2"})
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-left">
                            <button type="submit" class="btn btn-white btn-success">
                                <i class="ace-icon fa fa-save bigger-120 green"></i>
                                Salvar
                            </button>
                            <a href="@Url.Action("Index")" class="btn btn-white btn-danger">
                                <i class="ace-icon fa fa-times bigger-120 red"></i>
                                Cancelar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            End Using
        </div>
    </div>
</div>