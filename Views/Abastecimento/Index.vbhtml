﻿@ModelType GridSettings(Of AbastecimentoViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Promoção"
End Code

<div id="divFiltroAbastecimento">
    <div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-line-chart"></i>
                Abastecimento
            </li>
            <li class="active">
                Consulta
            </li>
        </ul>
    </div>

    <div id="msgGeral">
        @Html.Partial(Constantes.VIEW_ALERTAS)
    </div>
    <div id="divModalAbastecerPromocaoAlert"></div>
    <div id="divModalHistoricoAbastecimento"></div>


    @Using (Ajax.BeginForm("Pesquisar", "Abastecimento", Constantes.AJAX_OBJECT_POST_DEFAULT))
        @Html.AntiForgeryToken()
        @<div class="form-horizontal">

            <div Class="widget-box widget-color-blue2 ">
                <div Class="widget-header widget-header-small">
                    <h4 Class="widget-title smaller white lighter">
                        <i Class="ace-icon fa fa-sliders bigger-110"></i>
                        Filtros de Pesquisa
                    </h4>
                    <div Class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i Class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                        </a>
                    </div>
                </div>
                <div Class="widget-body">
                    <div Class="widget-main">
                        <div Class="form-horizontal">
                            <div Class="row">
                                <div Class="col-sm-2">
                                    @Html.Label("lblCodPromocao", "Cod. da Promoção:")
                                    @Html.TextBoxFor(Function(Model) Model.Filter.CODPMC, New With {.Class = "form-control"})
                                </div>

                                <div Class="col-sm-6">
                                    @Html.Label("lblNomPromocao", "Nome da Promoção:")
                                    @Html.TextBoxFor(Function(Model) Model.Filter.NOMPMC, New With {.Class = "form-control"})
                                </div>
                                <div Class="col-sm-4">
                                    @Html.Label("lblCODACOCMC", "Ação Comercial:")
                                    @Html.TextBoxFor(Function(Model) Model.Filter.CODACOCMC, New With {.Class = "form-control"})
                                </div>
                            </div>
                            <div Class="row">
                                <div class="col-lg-4 col-sm-4">
                                    @Html.Label("data", "Intervalo de vigência:")
                                    @Html.HiddenFor(Function(Model) Model.Filter.DATINIPMC, New With {.id = "dataIniAux"})
                                    @Html.HiddenFor(Function(Model) Model.Filter.DATFIMPMC, New With {.id = "dataFimAux"})
                                    <div class="input-daterange input-group">
                                        @Html.TextBoxFor(Function(Model) Model.Filter.DATINIPMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataIni()", .id = "dataIni", .autocomplete = "off"})
                                        <span class="input-group-addon">
                                            à
                                        </span>
                                        @Html.TextBoxFor(Function(Model) Model.Filter.DATFIMPMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataFim()", .id = "dataFim", .autocomplete = "off"})
                                    </div>
                                </div>
                                <div Class="col-lg-4 col-sm-4">
                                    @Html.Label("filial", "Filial (*):")
                                    @Html.DropDownListFor(Function(Model) Model.Filter.CODFILEMP, New SelectList(ViewBag.listaFilial, "CODFILEMP", "CODFILIAL_NOMFILIAL", Model.Filter.CODFILEMP), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFilial"})
                                </div>

                                <div Class="col-sm-4">
                                    @Html.Label("BU", "BU:")
                                    @Html.DropDownListFor(Function(Model) Model.Filter.CODUNDESRNGC, New SelectList(ViewBag.listaBU, "CODUNDESRNGC", "DESUNDESRNGC", Model.Filter.CODFILEMPORIVBA), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropBU"})
                                </div>

                            </div>
                            <div Class="row">
                                <div Class="col-sm-4">
                                    @Html.Label("QDEDIACOB", "Dias Cobertura:")
                                    @Html.TextBoxFor(Function(Model) Model.Filter.QDEDIACOB, New With {.Class = "form-control"})
                                </div>
                                <div Class="col-sm-4">
                                    @Html.Label("QDEDIARES", "Dias Restante Abastecimento:")
                                    @Html.TextBoxFor(Function(Model) Model.Filter.QDEDIARES, New With {.Class = "form-control"})
                                </div>
                                <div class="col-sm-4">
                                    @Html.Label("autor", "Autor:")
                                    @Html.DropDownListFor(Function(m) m.Filter.CODFNCGRCPMC, New SelectList(ViewBag.listaFuncionarios, "CODFNC", "CODIGO_NOME", Model.Filter.CODFNCGRCPMC), "Selecione...", New With {.Class = "chosen-select form-control"})
                                </div>
                            </div>

                            <div Class="row">
                                <div Class="col-sm-6">
                                    @Html.Label("CODFRNMER", "Cod. Fornecedor:")
                                    @Html.DropDownListFor(Function(m) m.Filter.CODFRNMER, New SelectList(ViewBag.ListaDropFornecedor, "CODFRN", "CodNomFrn", Model.Filter.CODFRNMER), "Selecione...", New With {.Class = "chosen-select form-control"})

                                    @*@Html.TextBoxFor(Function(Model) Model.Filter.CODFRNMER, New With {.Class = "form-control"})*@
                                </div>
                                <div Class="col-sm-2">
                                    @Html.Label("CODPRMPMC", "Cod. da Mercadoria:")
                                    @Html.TextBoxFor(Function(Model) Model.Filter.CODPRMPMC, New With {.Class = "form-control"})
                                </div>
                                <div Class="col-sm-4">
                                    @Html.Label("DESMER", "Nome da Mercadoria:")
                                    @Html.TextBoxFor(Function(Model) Model.Filter.DESMER, New With {.Class = "form-control"})
                                </div>
                                @*<div Class="col-sm-4">
            @Html.Label("NOMFRNMER", "Nom Fornecedor:")
            @Html.TextBoxFor(Function(Model) Model.Filter.NOMFRNMER, New With {.Class = "form-control"})
        </div>*@
                            </div>
                            <br>
                            <div Class="row">
                                <div Class="col-sm-6">
                                    <div Class="btn-group pull-left">
                                        <Button type="submit" Class="btn btn-info btn-white ">
                                            <i Class="ace-icon fa fa-search bigger"></i>
                                            Pesquisar
                                        </Button>
                                        <a href="@Url.Action("Index", "Abastecimento")" class="btn btn-grey btn-white">
                                            <i class="ace-icon fa fa-eraser bigger-120"></i>
                                            Limpar
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-6" style="float: right;">
                                    <button type="button" class="fa fa-chevron-down btn btn-xs" style="float: right;" onclick="expandAll();"></button>
                                    <button type="button" class="fa fa-chevron-up btn btn-xs" style="float: right;" onclick="collapsAll();"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    End Using
    <br class="clearfix" />
    <div id="resultadosView">
        @Code
            Html.RenderPartial("~/Views/Abastecimento/Abastecimento.vbhtml", Model.ListPaged)
        End Code
    </div>
</div>

@*<script>
        var showModal = true;
    </script>*@

@section Scripts
    <script type="text/javascript" src="~/Scripts/Abastecimento.js"></script>
    <script type="text/javascript" src="~/Scripts/AbastecerPromocao.js"></script>
End Section