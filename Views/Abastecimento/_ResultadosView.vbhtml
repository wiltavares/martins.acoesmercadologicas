﻿@ModelType List(Of AbastecimentoViewModel)

@code
    Dim compradores = Model.GroupBy(Function(f) f.CODCPR).Select(Function(g) New AbastecimentoViewModel() With {.CODCPR = g.Key, .NOMCPR = g.First().NOMCPR}).ToList()
End Code

@For Each comprador In compradores
    @<div Class="well well-sm divComp">
        <div class="msgAbastecimento"></div>
        <p>
            GTE COMPRAS:&nbsp;
            <b>@Html.DisplayFor(Function(m) comprador.CODCPR)</b>
            &nbsp;-&nbsp;
            <b>@Html.DisplayFor(Function(m) comprador.NOMCPR)</b>

            <i Class="fa fa-chevron-down botaoAbastecimento" style="float: right;" onclick="toggleTreeview(this);"></i>
        </p>


        @code
            Dim filiais = Model.Where(Function(f) f.CODCPR = comprador.CODCPR).GroupBy(Function(f) f.CODFILEMP).Select(Function(g) New AbastecimentoViewModel() With {.CODFILEMP = g.Key, .NOMFILEMP = g.First().NOMFILEMP}).ToList()
        End Code

        @For Each filial In filiais
            @<div class="well well-sm" style="background-color: #fbf6b8">
                <p>
                    FILIAL:&nbsp;
                    <b>@Html.DisplayFor(Function(m) filial.CODFILEMP)</b>
                    &nbsp;-&nbsp;
                    <b>@Html.DisplayFor(Function(m) filial.NOMFILEMP)</b>

                    <i class="fa fa-chevron-down botaoAbastecimento" style="float: right;" onclick="toggleTreeview(this);"></i>
                </p>
                @code
                    Dim promocoes = Model.Where(Function(f) f.CODCPR = comprador.CODCPR And f.CODFILEMP = filial.CODFILEMP).GroupBy(Function(f) f.CODPMC).Select(Function(g) New AbastecimentoViewModel() With {.CODPMC = g.Key, .NOMPMC = g.First.NOMPMC, .DESPMC = g.First.DESPMC, .DATINIPMC = g.First.DATINIPMC, .DATFIMPMC = g.First.DATFIMPMC, .CODACOCMC = g.First.CODACOCMC})
                End Code

                @For Each promocao In promocoes
                    @<div class="well">
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    PROMOÇÃO:&nbsp;
                                    <b>
                                        @Html.DisplayFor(Function(m) promocao.CODPMC)
                                    </b>
                                    &nbsp;-&nbsp;
                                    <b>
                                        @Html.DisplayFor(Function(m) promocao.NOMPMC)
                                    </b>

                                    &nbsp;Vigência&nbsp;
                                    <b>
                                        @Html.DisplayFor(Function(m) promocao.DATINIPMC)
                                    </b>
                                    até
                                    <b>
                                        @Html.DisplayFor(Function(m) promocao.DATFIMPMC)
                                    </b>
                                    &nbsp;-&nbsp;
                                    <b>
                                        @Html.DisplayFor(Function(m) promocao.DESPMC)
                                    </b>
                                </p>
                                @*<button type="button" class="fa fa-minus btn btn-xs" style="float: right;" onclick="jsPostAbastecimentoVarios(this, 1, @promocao.CODPMC)" title="Excluir para todos da promoção."></button>
                                    <button type="button" class="fa fa-plus btn btn-xs" style="float: right;" onclick="jsPostAbastecimentoVarios(this, 0, @promocao.CODPMC)" title="Inserir para todos da promoção."></button>*@
                            </div>
                        </div>

                        @code
                            Dim brindes = Model.Where(Function(f) f.CODCPR = comprador.CODCPR And f.CODFILEMP = filial.CODFILEMP And f.CODPMC = promocao.CODPMC)
                        End Code

                        <table id="@String.Format("tb_promocao_{0}", promocao.CODPMC)" Class="table table-bordered table-hover dataTable" style="background-color:white" role="grid">
                            <thead>
                                <tr role="row">
                                    <th>
                                        Qde. Bonific.
                                    </th>
                                    <th>
                                        Qde. Máxima
                                    </th>
                                    <th>
                                        Estoque Brinde
                                    </th>
                                    <th>
                                        Estoque Total
                                    </th>
                                    <th>
                                        Cobertura
                                    </th>
                                    <th>
                                        Dias Rest.
                                    </th>
                                    <th>
                                        Valor Brinde
                                    </th>
                                    <th>
                                        Media Saída
                                    </th>
                                    <th style="border-color: #dddddd;">
                                        <div Class="input-group" style="min-width: 100%;  max-width: 200px; min-width: 175px;">
                                            <input type="text" Class="form-control input-sm" alt="inteiro_positivo" placeholder="Ação Comercial" value="@promocao.CODACOCMC" onchange="txtAcaoComercialAbastecer_change(this);">
                                            <input type="text" Class="form-control input-sm" alt="inteiro_positivo" placeholder="Quantidade" onchange="txtQtdAbastecer_change(this);">
                                            <span Class="input-group-addon" style="padding-left: 6px; padding-right: 6px; border-left: none;" onclick="AbastecerMultiplo(@promocao.CODPMC, 0);">
                                                <i Class="ace-icon fa fa-plus" style="background:transparent;border:none;color:#04b004;"></i>
                                            </span>
                                            <span Class="input-group-addon" style="padding-left: 6px; padding-right: 6px;" onclick="AbastecerMultiplo(@promocao.CODPMC, 1);">
                                                <i Class="ace-icon fa fa-minus" style="background:transparent;border:none;color: #d00c0c;"></i>
                                            </span>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @For Each brinde In brindes
                                    @<tr class="linhaBrinde" id="@String.Format("row_{0}_{1}_desc", promocao.CODPMC, brinde.CODMER)">
                                        <td colspan="8">
                                            Brinde:<b class="CODMER">@Html.DisplayFor(Function(r) brinde.CODMER)</b>
                                            &nbsp;- &nbsp;
                                            <b class="DESMER">@Html.DisplayFor(Function(r) brinde.DESMER)</b>
                                            <input type="hidden" name="TIPEDENIVPRMPMC" value="@Html.DisplayFor(Function(r) brinde.TIPEDENIVPRMPMC)" />
                                            <button type="button" Class="btn btn-xs" style="float:right" onclick="HistoricoAbastecimento(@promocao.CODPMC, @brinde.CODMER);"><i Class="ace-icon fa fa-book bigger-120"></i> </button>
                                        </td>
                                        <td rowspan="2">
                                            <div Class="input-group" style="min-width: 100%;  max-width: 200px; min-width: 175px;">
                                                <input type="text" Class="form-control input-sm CODACOCMC" alt="inteiro_positivo" placeholder="Ação Comercial" value="@brinde.CODACOCMC">
                                                <input type="text" Class="form-control input-sm QDEMERISRPMC" alt="inteiro_positivo" placeholder="Quantidade">
                                                @if brinde.DATECSPRD Is Nothing Then
                                                    @<span Class="input-group-addon" style="padding-left: 6px; padding-right: 6px; border-left: none; " onclick="AbastecerUnico(@promocao.CODPMC, @brinde.CODMER, 0);">
                                                        <i Class="ace-icon fa fa-plus" style="background:transparent;border:none;color:#04b004;"></i>
                                                    </span>
                                                Else
                                                    @<span Class="input-group-addon" style="padding-left: 6px; padding-right: 6px; border-left: none; cursor:unset;">
                                                        <i Class="ace-icon fa fa-plus" style="background:transparent;border:none;color:#abcaab;"></i>
                                                    </span>
                                                End If


                                                <span Class="input-group-addon" style="padding-left: 6px; padding-right: 6px;" onclick="AbastecerUnico(@promocao.CODPMC, @brinde.CODMER, 1);">
                                                    <i Class="ace-icon fa fa-minus" style="background:transparent;border:none;color: #d00c0c;"></i>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    @<tr Class="linhaBrinde" id="@String.Format("row_{0}_{1}_qtds", promocao.CODPMC, brinde.CODMER)">
                                        <td Class="center QDEMERVNDPRMPMC">
                                            @Html.DisplayFor(Function(r) brinde.QDEMERVNDPRMPMC)
                                        </td>
                                        <td Class="center QDEMERMAXPRMPMC">
                                            @Html.DisplayFor(Function(r) brinde.QDEMERMAXPRMPMC)
                                        </td>
                                        <td Class="center ETQBND">
                                            @Html.DisplayFor(Function(r) brinde.ETQBND)
                                        </td>
                                        <td Class="center ETQSLD">
                                            @Html.DisplayFor(Function(r) brinde.ETQSLD)
                                        </td>
                                        <td Class="center QDEDIACOB">
                                            @Html.DisplayFor(Function(r) brinde.QDEDIACOB)
                                        </td>
                                        <td Class="center QDEDIARES">
                                            @Html.DisplayFor(Function(r) brinde.QDEDIARES)
                                        </td>
                                        <td Class="center VALOR_BRINDE">
                                            @Html.DisplayFor(Function(r) brinde.VALOR_BRINDE)
                                        </td>
                                        <td Class="center MEDIA_SAIDA">
                                            @Html.DisplayFor(Function(r) brinde.MEDIA_SAIDA)
                                        </td>
                                    </tr>
                                Next
                            </tbody>
                        </table>
                    </div>Next
            </div>
        Next
    </div>
Next

<style>
    .input-group-addon {
        cursor: pointer;
    }

        .input-group-addon: hover {
            background-color: #cdcccc;
        }
</style>

@*<script>
        $('.numberInput').keyup(function () {
            if ($(this).val().replace(/\s/g, '').length) {
                $(this).removeClass('classError');
            }

            //Verifica se codAcoCmc e Quantidade estao preenchidos
            if ($(this).parents('.input-group').find('.qdeMerIsrPmc').val().replace(/\s/g, '').length && $(this).parents('.input-group').find('.codAcoCmc').val().replace(/\s/g, '').length) {
                $(this).parents('td').removeClass('classErrorInside');
            }

        });
    </script>*@