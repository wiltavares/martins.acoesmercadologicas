﻿@ModelType List(Of AbastecimentoViewModel)


<div class="modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header ">
                <i class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Histórico
            </div>
            <div class="modal-body">

                <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th>Seq</th>
                            <th>Tipo</th>
                            <th>Ação Com.</th>
                            <th>Qtd</th>
                            <th>Geração</th>
                            <th>Valor</th>
                            <th>Funcionário</th>
                            <th>Nome do Funcionário</th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @<tr>
                                <td>
                                    @Html.DisplayFor(Function(m) item.NUMSEQISRQDEMERPMC)
                                </td>
                                <td>
                                    @Select Case item.INDTIPISRMERPMC
                                        Case 0
                                            @<label>INSERIDO</label>
                                        Case 1
                                            @<label>EXCLUÍDO</label>
                                    End Select
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.CODACOCMC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.qdeMerIsrPmc)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.DATGRCRLC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.VLRTOTMERISRPMC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.CODFNCGRCPMC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.NOMFNCGRCPMC)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>

            </div>
            <div class="modal-footer clearfix">
                <div class="btn-group pull-right">
                    <button class="btn btn-danger btn-white" onclick="btnFecharHistoricoAbastecimento()">
                        <i class="ace-icon fa fa-close bigger-120 red"></i>
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>