﻿@ModelType GridSettings(Of AcaoComercialViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Consulta Saldo Ação x Fornecedor"

End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-bar-chart"></i>
            Relatórios
        </li>
        <li class="active">Consulta Saldo Ação x Fornecedor</li>
    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modal"></div>
<div class="modal fade" id="modalResultImport"></div>
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "ConsultaSaldoAcaoFornecedor", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-2">

                            @Html.Label("acaoID", "Ação:")
                            @Html.TextBoxFor(Function(m) m.Filter.CODACOCMC, New With {.Class = "form-control", .id = "txtCodAcao"})

                        </div>
                        <div class="col-sm-10">
                            @Html.Label("acao", "Nome da Ação:")
                            @Html.TextBoxFor(Function(m) m.Filter.NOMACOCMC, New With {.class = "input-sm form-control", .id = "txtNomAcao"})
                        </div>
                    </div>
                     <div class="row">

                         <div class="col-sm-2">
                             @Html.Label("opcoesID", "Opções:")
                             @Html.DropDownListFor(Function(m) m.Filter.COD_FILTRO, New SelectList(ViewBag.filtroSaldoAcaoFrn, "VALUE", "TEXT", Model.Filter.COD_FILTRO), New With {.Class = "form-control ", .id = "dropFiltroCarimbos", .onchange = "dropFiltros_onChange($(this).val())"})
                         </div>

                         <div class="col-sm-10" style="display:none; " id="divFiltroCelula">
                             @Html.Label("CelID", "Célula:")
                             @Html.DropDownListFor(Function(m) m.Filter.CODDIVCMP, New SelectList(ViewBag.filtroCelula, "CODDIVCMP", "DESDIVCMP", Model.Filter.CODDIVCMP), "Selecione...", New With {.Class = "form-control", .id = "dropFiltroCelula"})
                         </div>
                         <div class="col-sm-10" id="divFiltroComprador">
                             @Html.Label("CompID", "Comprador:")
                             @Html.DropDownListFor(Function(m) m.Filter.CODCPR, New SelectList(ViewBag.filtroComprador, "CODCPR", "NOMCPR", Model.Filter.CODCPR), "Selecione...", New With {.Class = "form-control", .id = "dropFiltroComprador"})
                         </div>

                         <div id="divFiltroFornecedor" style="display:none;">

                             <div class="col-sm-4">
                                 @Html.Label("FornID", "Cód:")
                                 @Html.TextBoxFor(Function(m) m.Filter.CODFRN, New With {.Class = "form-control", .id = "filtroCodFrn"})
                             </div>

                             <div class="col-sm-5" >
                                 @Html.Label("Forn2ID", "Nome")
                                 @Html.DropDownListFor(Function(m) m.Filter.CODFRN, New SelectList(ViewBag.fornecedorAcao, "NOMFRN", "CODFRN", Model.Filter.CODFRN), New With {.Class = "form-control", .id = "dropFiltroFornecedorCarimbo", .onchange = "dropFornecedorCarimbo_onChange()"})
                             </div>

                             <div class="col-sm-1" >
                                 <div class="btn-group pull-right">
                                     @Html.Label("Forn3ID", "Filtrar")
                                     <a onclick="buscarFornecedores()" class="btn btn-sm btn-info" style="height:34px">
                                         <i class="ace-icon fa fa-search" style="vertical-align: middle"></i>
                                     </a>
                                 </div>
                             </div>

                        </div>

                    </div> 
             
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white ">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                Pesquisar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger-120"></i>
                                Limpar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using

        </div>
    </div>
</div>
<div id="resultadosView">
   @Code
       Html.RenderPartial("~/Views/ConsultaSaldoAcaoFornecedor/_ResultadosView.vbhtml", Model)
    End Code
</div>

<script src="~/Scripts/saldoAcaoFornecedor.js"></script>