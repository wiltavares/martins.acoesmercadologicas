﻿@ModelType GridSettings(Of RelacaoAcaoComercialxFluxoAprovacaoViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div style="width: auto; overflow-x: scroll">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1500px">
        <thead>
            <tr role="row">
                <th class="@Grid.AtribuirIconeOrdenacao("CODACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Cod. Ação
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NUMFLUAPV", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NUMFLUAPV", Model.Page.OrderBy, Model.ResultID)">
                    Fluxo
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NOMTIPSTAFLUAPV", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMTIPSTAFLUAPV", Model.Page.OrderBy, Model.ResultID)">
                    Estado
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DATHRAAPVFLU", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATHRAAPVFLU", Model.Page.OrderBy, Model.ResultID)">
                    Data
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODFNC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFNC", Model.Page.OrderBy, Model.ResultID)">
                    Funcionário
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESOBSAPV", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESOBSAPV", Model.Page.OrderBy, Model.ResultID)">
                    Observação
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESMTVAPVFLUACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESMTVAPVFLUACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Motivo
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODFNCARZ", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFNCARZ", Model.Page.OrderBy, Model.ResultID)">
                    Aprovador Delegado
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr class="@Constantes.GetColorStatusFluxoAprovacao(item.TIPSTAFLUAPV)">
                    <td>
                        @Model.Filter.CODACOCMC
                    </td>
                    <td>
                        @item.NUMFLUAPV
                    </td>
                    <td>
                        @item.NOMTIPSTAFLUAPV
                    </td>
                    <td>
                        @item.DATHRAAPVFLU
                    </td>
                    <td>
                        @item.CODFNC - @item.NOMFNC
                    </td>
                    <td>
                        @item.DESOBSAPV
                    </td>
                    <td>
                        @item.DESMTVAPVFLUACOCMC
                    </td>
                    <td>
                        @IIf(item.CODFNCARZ <> 0, item.CODFNCARZ & " - " & item.NOMFNCARZ, "")
                    </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />

@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code

