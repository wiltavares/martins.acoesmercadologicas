﻿@ModelType GridSettings(Of RelacaoAcaoComercialxFluxoAprovacaoViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="modal-dialog modal-lg">
    <div class="modal-content">

        <div class="modal-header ">
            <i class="ace-icon fa fa-random bigger-120"></i>
            &nbsp;Fluxo de Aprovações
        </div>

        <div class="modal-body">
            <div class="legend-group-box">
                <div class="row">
                    <div class="col-sm-3" style="padding-bottom:3px">
                        Legenda:
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="legend-box green-bkg">
                        </div>
                        Aprovada
                    </div>
                    <div class="col-sm-3">
                        <div class="legend-box red-bkg">
                        </div>
                        Rejeitada
                    </div>
                    <div class="col-sm-3">
                        <div class="legend-box blue-bkg">
                        </div>
                        Em Aprovação
                    </div>
                    <div class="col-sm-3">
                        <div class="legend-box orange-bkg">
                        </div>
                        Espera p/ Aprovação
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="legend-box pink-bkg">
                        </div>
                        Pedido Parecer
                    </div>
                    <div class="col-sm-3">
                        <div class="legend-box pink-bkg">
                        </div>
                        Resposta Parecer
                    </div>
                    <div class="col-sm-3">
                        <div class="legend-box darkblue-bkg">
                        </div>
                        Notificação
                    </div>
                </div>

            </div>
            <br class="clearfix" />

            <div id="gridFluxoAprovacoes">
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Fluxo/_GridFluxo.vbhtml", Model)
                End Code
            </div>

        </div>

        <div class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                @*<a href="javascript:redirectLoading('@Url.Action("CreateEdit", New With {.id = Model.Filter.CODACOCMC})')" Class="btn btn-white btn-primary">
                    <i Class="ace-icon fa fa-pencil-square-o bigger-120 blue"></i>
                    Ação Mercadológica
                </a>*@
                <a href="" data-dismiss="modal" class="btn btn-danger btn-white pull-right">
                    <i class="ace-icon fa fa-times bigger"></i>
                    Cancelar
                </a>
            </div>
        </div>
    </div>
</div>
