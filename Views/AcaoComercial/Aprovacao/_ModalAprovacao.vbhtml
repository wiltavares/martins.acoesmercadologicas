﻿@ModelType DadosFluxoAcaoComercial
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim AJAX_OBJECT_POST_APROVACAO As New AjaxOptions() With {
        .UpdateTargetId = "resultadosView",
        .OnBegin = "LoadingOn",
        .InsertionMode = InsertionMode.Replace,
        .OnComplete = "fimModal()",
        .HttpMethod = "POST"
    }

End Code

<div class="modal fade" id="modalFuncionarios"></div>

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Aprovação de fluxo
        </div>
        <div class="modal-body">
            <div id="msgAprovacaoFluxo">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            @Using (Ajax.BeginForm("SalvarAprovacao", "Acaocomercial", AJAX_OBJECT_POST_APROVACAO))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal" id="modalAprovacao">
                    <div class="row">
                        <div class="col-xs-12">
                            @Html.Label("item", "Ação:")
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            @Html.RadioButtonFor(Function(f) f.radio, 1, New With {.id = "rdbAprovacaoAprovar", .onChange = "aprovacaoRadiochange(1)"}.EnableIf(ViewBag.parecer))
                            @*<input @IIf(ViewBag.parecer, "", "disabled = 'disabled'") @IIf(tipoFrn = 1, "checked", "") id="rdbAprovacaoAprovar" name="aprovacaoRdb" onchange="configuraPatrocinador(1)" type="radio" value="1">*@
                            @Html.Label("lblrdbAprovacaoAprovar", "Aprovar")
                        </div>
                        <div class="col-sm-3">
                            @Html.RadioButtonFor(Function(f) f.radio, 2, New With {.id = "aprovacaoRdb", .onChange = "aprovacaoRadiochange(2)"}.EnableIf(ViewBag.parecer))
                            @*<input @IIf(ViewBag.parecer, "", "disabled = 'disabled'") @IIf(tipoFrn = 2, "checked", "") id="rdbAprovacaoRejeitar" name="aprovacaoRdb" onchange="configuraPatrocinador(2)" type="radio" value="2">*@
                            @Html.Label("lblrdbAprovacaoRejeitar", "Rejeitar")
                        </div>
                        <div class="col-sm-3">
                            @Html.RadioButtonFor(Function(f) f.radio, 3, New With {.id = "darParecerRdb", .onChange = "aprovacaoRadiochange(3)"}.EnableIf(Not ViewBag.parecer))
                            @*<input @IIf(not ViewBag.parecer, "", "disabled = 'disabled'") @IIf(tipoFrn = 3, "checked", "") id="rdbAprovacaoParecer" name="aprovacaoRdb" onchange="configuraPatrocinador(3)" type="radio" value="3">*@
                            @Html.Label("lblrdbAprovacaoParecer", "Parecer")
                        </div>
                        <div class="col-sm-3">
                            @Html.RadioButtonFor(Function(f) f.radio, 4, New With {.id = "parecerRdb", .onChange = "aprovacaoRadiochange(4)"}.EnableIf(ViewBag.parecer))
                            @*<input @IIf(ViewBag.parecer, "", "disabled = 'disabled'") @IIf(tipoFrn = 4, "checked", "") id="rdbAprovacaoPedirParecer" name="aprovacaoRdb" onchange="configuraPatrocinador(4)" type="radio" value="4">*@
                            @Html.Label("lblrdbAprovacaoPedirParecer", "Pedir Parecer")
                        </div>
                    </div>

                    <div class="row" id="divPreAcordo">
                        <div class="col-sm-8">
                            @code
                            If (Model.IndPreAcordo = True And Model.NUMSEQNIVAPV = 101) Then
                            @Html.Label("lblFormaPagamento", "Forma Pagamento (*):")
                            @Html.DropDownListFor(Function(m) m.TIPFRMDSCBNF, New SelectList(ViewBag.listaFormaPagamento, "TIPFRMDSCBNF", "DESFRMDSCBNF", Model.TIPFRMDSCBNF), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFormaPagamento"})
                            @Html.Label("lblCampObrigFormPag", "Preencha o campo Forma Pagamento", New With {.id = "lblCampObrigFormPag", .style = "display: none;", .class = "text-danger-message-login"})
                            End If
                            End code
                        </div>
                        <div class="col-sm-4">
                            @code
                            If (Model.IndPreAcordo = True And Model.NUMSEQNIVAPV = 101) Then
                            @Html.Label("lblNumDias", "Nro. Dias (*):")
                            @Html.TextBoxFor(Function(m) m.QDEDIARCBPMS, New With {.Class = "form-control", .id = "txtNumDias", .alt = "inteiro_positivo"})
                            @Html.Label("lblCampObrigNumdias", "Preencha o campo Nro. Dias", New With {.id = "lblCampObrigNumdias", .style = "display: none;", .class = "text-danger-message-login"})
                            End If
                            End code
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12" id="modalFunc" style="display: none;">
                            @Html.HiddenFor(Function(model) model.funcionario, New With {.Class = "form-control", .id = "idCodFuncionario"})
                            @Html.Label("lblNomFunc", "Funcionário (*):")
                            <div class="input-group" id="dividItem">
                                @Html.TextBoxFor(Function(model) model.nomFuncionario, New With {.Class = "form-control", .id = "nomFuncionario"})
                                <span class="input-group-btn">
                                    <button class="btn btn-white" id="btBuscaFunc" type="button" onclick="modalBuscaFuncionariosAprovacao()" style="display: block">
                                        <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                    </button>
                                    <button class="btn btn-white" id="btTiraFunc" type="button" onclick="tiraFuncionarioAprovacao()" style="display: none">
                                        <i class="ace-icon fa fa-close bigger-120 red"></i>
                                    </button>
                                </span>
                            </div>
                            @Html.Label("lblValidNomFunc", "Preencha o campo Funcionário", New With {.id = "ValidNomFunc", .class = "text-danger-message-login", .style = "display:none;"})
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            @Html.Label("obsAprovacaoFluxo", "Observação (*):", New With {.id = "obsAprovacaoFluxo"})
                            @Html.TextAreaFor(Function(f) f.observacao, 4, 75, New With {.id = "AprovacaoFluxoObs", .style = "width:100%"})
                            @Html.Label("lblDescricaoAprovacao", "Preencha o campo Observação", New With {.id = "lblDescricaoAprovacao", .style = "display: none;", .class = "text-danger-message-login"})
                            @*@Html.TextArea("DESFRMUTZITEACOCMC", "", 4, 100, New With {.id = "AprovacaoFluxoObs"})*@
                        </div>
                    </div>
                    <div class="modal-footer clearfix">
                        <div class="btn-group pull-right">
                            <button type="submit" @*data-dismiss="modal"*@ class="btn btn-success btn-white" onclick="if (!confirmaAprovacao()) return false;">
                                <i class="ace-icon fa fa-check-square-o bigger-120 green"></i>
                                OK
                            </button>
                            <button class="btn btn-danger btn-white" data-dismiss="modal">
                                <i class="ace-icon fa fa-close bigger-120 red"></i>
                                Cancelar
                            </button>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
