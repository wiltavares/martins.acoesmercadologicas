﻿@ModelType GridSettings(Of ParametrosGeraisDoIncentivoDeVendasViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
            <tr role="row">
                <th class="center">
                    Selecionar
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODICT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODICT", Model.Page.OrderBy, Model.ResultID)">
                    Código
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESICT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESICT", Model.Page.OrderBy, Model.ResultID)">
                    Incentivo
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                Dim idCheckExc = "ckbExInc" & item.CODICT

                @<tr>
                    <td Class="center">
                        <input id="@idCheckExc" type="checkbox" value="false"/>
                    </td>
                     <td>
                         @item.CODICT
                     </td>
                     <td>
                         @item.DESICT
                     </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />

@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO, Model)
End Code
