﻿@ModelType ParametrosGeraisDoIncentivoDeVendasViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim AJAX_OBJECT_POST_Incentivo As New AjaxOptions() With {
        .UpdateTargetId = "gridIncenivoModal",
        .InsertionMode = InsertionMode.Replace,
        .HttpMethod = "POST",
        .OnSuccess = "filtroIncentivoSuccess",
        .OnFailure = "filtroIncentivoFailure"
    }
End Code
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Inserir Incentivo
        </div>
        <div class="modal-body">
            <div id="msgModalIncentivo">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            Buscar Incentivos
            @Using (Ajax.BeginForm("FiltrarIncentivos", "Incentivo", AJAX_OBJECT_POST_Incentivo))
                @<div Class="legend-group-box form-horizontal">
                    <div Class="row">
                        <div Class="col-xs-3">
                            @Html.TextBoxFor(Function(m) m.CODICT, New With {.Class = "form-control"})
                        </div>
                        <div class="col-xs-7">
                            <div class="row">
                                @Html.TextBoxFor(Function(m) m.DESICT, New With {.Class = "form-control"})
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div Class="btn-group pull-right">
                                <button type="submit" class="btn btn-info btn-white pull-right">
                                    <i class="ace-icon fa fa-sliders bigger"></i>
                                    Filtrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
            <br />

            <div Class="widget-box widget-color-blue2">
                <div Class="widget-header widget-header-small widget-light-blue">
                    <h4 Class="widget-title smaller">
                        Incentivos
                    </h4>
                </div>

                <div Class="widget-body">
                    <div Class="widget-main">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="gridIncentivoModal">
                                    @Code
                                        Html.RenderPartial("~/Views/AcaoComercial/Incentivo/_GridIncentivoModal.vbhtml", ViewBag.GridIncentivoModal)
                                    End Code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br />

            <div class="row">
                <div class="col-sm-12">
                    <div Class="btn-group pull-right">
                        <a onclick="selecionarIncentivos()" class="btn btn-success btn-white">
                            <i class="ace-icon fa fa-plus bigger"></i>
                            Selecionar Incentivos
                        </a>
                        <a onclick="removerIncentivos()" class="btn btn-danger btn-white">
                            <i class="ace-icon fa fa-times bigger"></i>
                            Remover Incentivos
                        </a>
                    </div>
                </div>
            </div>
            <br />


            <div Class="widget-box widget-color-blue2">
                <div Class="widget-header widget-header-small widget-light-blue">
                    <h4 Class="widget-title smaller">
                        Incentivos Selecionados
                    </h4>
                </div>

                <div Class="widget-body">
                    <div Class="widget-main">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="gridIncentivosSelecionadosModal">
                                    @Code
                                        Html.RenderPartial("~/Views/AcaoComercial/Incentivo/_GridIncentivosSelecionadosModal.vbhtml", ViewBag.GridIncentivosSelecionados)
                                    End Code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <a onclick="concluirInsercaoIncentivos()" class="btn btn-success btn-white">
                    <i class="ace-icon fa fa-check-square-o bigger"></i>
                    Concluir
                </a>
                <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                    <i class="ace-icon fa fa-times bigger"></i>
                    Sair
                </a>
            </div>
        </div>
    </div>
</div>

<script src="~/Scripts/Incentivo.js"></script>