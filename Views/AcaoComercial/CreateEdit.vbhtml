﻿@ModelType AcaoComercialViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim IndPreAcordo As Boolean = False
    Dim NomeTipo As String = "Ação"
    Dim nomeTela As String = "Acao"
    Dim ActionNameSalvar As String = "Salvar"
    Dim ActionNameSair As String = "ConsultaAcao"
    Dim ajaxSalvarAcao As New AjaxOptions() With {
            .OnBegin = "LoadingOn",
            .OnComplete = "salvarAcaoComplete",
            .HttpMethod = "POST"
    }
    Dim action As String = ViewContext.RouteData.Values("action")
    If action.Contains("PreAcordo") Or action.Contains("PreAcordoCreateEditVigencia") Then
        IndPreAcordo = True
        NomeTipo = "Pré-Acordo"
        nomeTela = "PreAcordo"
        ActionNameSair = "PreAcordoConsultar"
        If action.Contains("Vigencia") Then
            ActionNameSalvar = "PreAcordoSalvarVigencia"
        Else
            ActionNameSalvar = "PreAcordoSalvarValor"
        End If
    End If

    If (Session("IsMobile") = True) Then
        ActionNameSair = "Aprovacoes"
    End If

    ViewData("Title") = NomeTipo


End Code
<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade" id="modal"></div>

<div id="modalMecanicaPromocaoSubModal"></div>
<div class="modal fade hideScroll" id="modalMecanicaPromocao"></div>


@Using (Ajax.BeginForm(ActionNameSalvar, "AcaoComercial", ajaxSalvarAcao))

    @Html.AntiForgeryToken()
    @<div class="form-horizontal" id="formAcao">
        <div class="widget-box widget-color-blue2">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller white lighter">
                    <i class="ace-icon fa fa-file-text-o bigger-110"></i>
                    @NomeTipo
                </h4>
            </div>

            <div class="widget-body">
                <div class="widget-main">

                    <div class="widget-box widget-color-blue2">
                        <div class="widget-header widget-header-small widget-light-blue">
                            <h4 class="widget-title smaller">
                                Dados
                            </h4>
                            <div class="widget-toolbar">
                                <a href="#" data-action="collapse">
                                    <i class="ace-icon fa fa-chevron-up blue"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div id="divValidacaoDados">
                                    @Html.Partial("~/Views/AcaoComercial/_DadosAcao.vbhtml")
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-sm-9">
                                        <a class="btn btn-primary btn-white @ViewHelper.Enable(ViewBag.btnPlanoMarketingEnable)" onclick="abrirModalPlanoMarketing()">
                                            <i class="ace-icon fa fa-book bigger-120 primary"></i>
                                            Plano Marketing
                                        </a>
                                        <a class="btn btn-primary btn-white" onclick="AbrirModalEditMecanicaAcao();">
                                            <i class="ace-icon fa fa-gears bigger-120 primary"></i>
                                            Mecânica
                                        </a>
                                        <a class="btn btn-white btn-purple" style="@ViewHelper.Visible(ViewBag.btnPoliticaVisible)" onclick="abrirModal('AcaoComercial', 'ExibirPoliticas',  @Json.Encode(Model.CODACOCMC))">
                                            <i class="ace-icon fa fa-pencil-square-o bigger-120 pink"></i>
                                            Políticas
                                        </a>
                                        <a href="@Url.Action("Index")" class="btn btn-white btn-danger" style="@ViewHelper.Visible(ViewBag.btnRejeitarVisible)">
                                            <i class="ace-icon fa fa-times-circle bigger-120 red"></i>
                                            Rejeição
                                        </a>
                                        @If (Model.TIPFRMDSCBNF <> 0) Then
                                            Dim texto As String = String.Format("Forma de Pagamento: <br/> {0} <br/> Número de Dias: {1}", Model.DESFRMDSCBNF, Model.QDEDIARCBPMS)
                                            @<span class="btn btn-white btn-danger popover-error" data-rel="popover" data-placement="top" data-original-title="Informações GAC" data-content="@texto">
                                                <i class="ace-icon fa fa-dollar bigger-120"></i>
                                                GAC
                                            </span>
                                        End If
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="btn-group pull-right">
                                            <a id="btnSlcIncentivo" onclick="abrirModal('Incentivo', 'InserirIncentivo', @Json.Encode(New RelacaoIncentivoxAcaoComercialViewModel))" class="btn btn-white btn-success" style="@ViewHelper.Visible(ViewBag.btnSlcIncentivoVisible)">
                                                <i class="ace-icon fa fa-check-circle bigger-120 success"></i>
                                                Selecionar Incentivo
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="widgetPatrocinados">
                        @Code
                            Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/_WidgetFornecedor.vbhtml")
                        End Code
                    </div>
                    <div id="divWidgetOrcamento">
                        @Code
                            Html.RenderPartial("~/Views/AcaoComercial/Orcamento/_WidgetOrcamento.vbhtml")
                        End Code
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <a class="btn btn-white btn-purple" style="@ViewHelper.Visible(ViewBag.btnDarParecerVisible)" onclick="abrirModal('AcaoComercial', 'abrirModalAprovacao', 'parecer|@nomeTela|@Model.CODACOCMC')">
                                <i class="ace-icon fa fa-comments-o bigger-120 purple">
                                </i>
                                Dar Parecer
                            </a>
                            <a class="btn btn-white btn-pink" style="@ViewHelper.Visible(ViewBag.btnAprovacaoVisible)" onclick="abrirModal('AcaoComercial', 'abrirModalAprovacao' , 'aprovacao|@nomeTela|@Model.CODACOCMC')">
                                <i class="ace-icon fa fa-gavel bigger-120 pink"></i>
                                Aprovação
                            </a>
                            <button id="btnSalvar" value="Salvar" type="submit" class="btn btn-white btn-success" onsubmit="LoadingOn()" onclick="if (!confirmSalvar()) return false;" style="@ViewHelper.Visible(ViewBag.btnGravarVisible)">
                                <i class="ace-icon fa fa-save bigger-120 green"></i>
                                Salvar
                            </button>
                            @Html.HiddenFor(Function(f) f.Enviar, New With {.id = "seEnviar"})
                            @Html.HiddenFor(Function(f) f.confirmaEnviar1, New With {.id = "confirmaEnviar1"})
                            @Html.HiddenFor(Function(f) f.confirmaEnviar2, New With {.id = "confirmaEnviar2"})
                            @Html.HiddenFor(Function(f) f.confirmaEnviarGerente, New With {.id = "confirmaEnviarGerente"})

                            <button id="btnEnviar" value="Enviar" class="btn btn-white btn-primary" onclick="if (!btnEnviarFuncaoClick()) return false;" style="@ViewHelper.Visible(ViewBag.btnEnviarVisible)">
                                <i class="ace-icon fa fa-send-o bigger-120 blue"></i>
                                Enviar
                            </button>
                            <a href="@Url.Action(ActionNameSair)" class="btn btn-white btn-danger">
                                <i class="ace-icon fa fa-times bigger-120 red"></i>
                                Sair
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

End Using

<script type="text/javascript" src="~/Scripts/mecanicaPromocao.js"></script>

<script>

    function abrirModalPlanoMarketing() {
        if ($('#dropTipoEvento').val() == "" || $('#dropEvento').val() == "") {
            $("#msgGeral").load(SiteUrl + 'Alert/_Alerts?msgWarning=PLANO_NAO_DEFINIDO', function () { });
        } else {
            abrirModal('AcaoComercial', 'ExibePlanoMarketing', listForPlanoMkt());
        }
    };

    function listForPlanoMkt() {

        var anoref = 0;
        if ($('#txtDataValidadeInicial').val().length === 10)
            var anoref = $('#txtDataValidadeInicial').val().substring(6, 10);

        return [$('#dropTipoEvento').val(), $('#dropEvento').val(), anoref];
    };

</script>
