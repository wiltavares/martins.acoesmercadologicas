﻿@ModelType FuncionarioViewModel
@Imports Martins.AcoesMercadologicas.Core

<div class="modal fade" id="modalBuscaGerente"></div>

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Gerente
        </div>
        <div class="modal-body">
            <div id="msgBuscaGerente">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
                @Html.Label("gerente", "Gerente:")
            <div class="row">
                <div class="col-xs-3">
                    @Html.TextBoxFor(Function(f) f.CODFNC, New With {.Class = "form-control", .id = "idGerente"})
                </div>
                <div class="col-xs-9 input-group" id="dividItem">
                    @Html.TextBoxFor(Function(f) f.NOMFNC, Nothing, New With {.Class = "form-control", .id = "Gerente"})
                    <span class="input-group-btn">
                        <button class="btn btn-white" id="btBscGer" type="button" onclick="modalBuscarGerente()" style="display: block">
                            <i class="ace-icon fa fa-search bigger-120 blue"></i>
                        </button>
                        <button class="btn btn-white" id="btTiraGer" type="button" onclick="tirarGerente()" style="display: none">
                            <i class="ace-icon fa fa-close bigger-120 red"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <a href="javascript:enviarGerenteSelecionado()" class="btn btn-success btn-white">
                    <i class="ace-icon fa fa-check-square-o bigger-120 green"></i>
                    Enviar
                </a>
                <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </a>
            </div>
        </div>
    </div>
</div>
