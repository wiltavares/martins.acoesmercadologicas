﻿@ModelType GridSettings(Of FuncionarioViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Selecionar Gerente
        </div>
        <div class="modal-body">
            <div id="msgBuscaMercadorias">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            @Using (Ajax.BeginForm("PesquisarSubstitutos", "AcaoComercial", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="row">
                    <div class="col-xs-12">
                        @Html.Label("item", "Gerente:")
                    </div>
                </div>
                @<div class="row">
                    <div class="col-lg-4 col-sm-6">
                        @Html.TextBoxFor(Function(m) m.Filter.CODFNC, New With {.Class = "form-control", .id = "idGerenteM"})
                    </div>
                    <div class="col-lg-8 col-sm-6">
                        <div class="input-group">
                            @Html.TextBoxFor(Function(m) m.Filter.NOMFNC, New With {.Class = "form-control", .id = "nomFuncionarioM"})
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-white" type="button" onclick="buscarGerente()">
                                    <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            End Using
            <div id="resultadosGerente">
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/ModalSelecionarGerente/_ResultadoGerentes.vbhtml", Model)
                End Code
            </div>
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <button class="btn btn-danger btn-white" onclick="cancelarBuscaGerente()">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
