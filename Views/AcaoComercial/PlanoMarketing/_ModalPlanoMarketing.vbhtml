﻿@ModelType List(Of PlanoDeMarketingViewModel)
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Acompanhamento do Plano de Marketing
        </div>
        <div class="modal-body modal-lg">
            <div class="row">
                <div class="col-sm-6">
                    @Html.Label("TipoEventoID", "Tipo Evento:")
                    @Html.Label("TipoEvento", String.Format("{0} - {1}", Model.FirstOrDefault().TIPEVTACOCMC.ToString(), Model.FirstOrDefault().NOMTIPEVTACOCMC))
                </div>
                <div class="col-sm-6">
                    @Html.Label("EventoID", "Evento:")
                    @Html.Label("Evento", String.Format("{0} - {1}", Model.FirstOrDefault().CODEVTACOCMC.ToString(), Model.FirstOrDefault().NOMEVTACOCMC))
                </div>
            </div>
            @For Each item In From m In Model Group m By m.CODUNDESRNGC, m.DESUNDESRNGC Into ListPlano = Group Order By CODUNDESRNGC
                @<div class="widget-box widget-color-blue2">
                    <div class="widget-header widget-header-small widget-light-blue">
                        <h4 class="widget-title smaller">
                            @String.Format("{0} - {1}", item.ListPlano.FirstOrDefault().CODUNDESRNGC.ToString(), item.DESUNDESRNGC)
                        </h4>
                        <div class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up blue"></i>
                            </a>
                        </div>
                        <div class="widget-toolbar">
                            @Html.Label("lblSaldo", String.Format("Saldo: {0:N2}", (From i In item.ListPlano Select i.VLROCDPLNMKT.GetValueOrDefault() - i.VLRUTZPLNMKT.GetValueOrDefault() - i.VLRRSVPLNMKT.GetValueOrDefault()).Sum()))
                        </div>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            <div style="width: auto; overflow-x: scroll;">
                                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1000px;">
                                    <thead>
                                        <tr role="row">
                                            <th></th>
                                            <th>Jan</th>
                                            <th>Fev</th>
                                            <th>Mar</th>
                                            <th>Abr</th>
                                            <th>Mai</th>
                                            <th>Jun</th>
                                            <th>Jul</th>
                                            <th>Ago</th>
                                            <th>Set</th>
                                            <th>Out</th>
                                            <th>Nov</th>
                                            <th>Dez</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Vr. Orçado</td>
                                            @For index = 1 To 12
                                            @<td class="text-right">
                                                @code
                                                    Dim i As String = Date.Now.Year.ToString() + index.ToString().PadLeft(2, "0")
                                                    Dim vlr As String = (From a In item.ListPlano Where a.ANOMESREF.GetValueOrDefault().ToString() = i Select a.VLROCDPLNMKT.GetValueOrDefault().ToString("N2")).ToList().FirstOrDefault()
                                                    If Not vlr Is Nothing Then
                                            @vlr
                                                    End If
                                                End Code
                                            </td>
                                            Next
                                        </tr>
                                        <tr>
                                            <td>Vr. Reservado</td>
                                            @For index = 1 To 12
                                            @<td class="text-right">
                                                @code
                                                    Dim i As String = Date.Now.Year.ToString() + index.ToString().PadLeft(2, "0")
                                                    Dim vlr As String = (From a In item.ListPlano Where a.ANOMESREF.GetValueOrDefault().ToString() = i Select a.VLRRSVPLNMKT.GetValueOrDefault().ToString("N2")).ToList().FirstOrDefault()
                                                    If Not vlr Is Nothing Then
                                            @vlr
                                                    End If
                                                End Code
                                            </td>
                                            Next
                                        </tr>
                                        <tr>
                                            <td>Vr. Utilizado</td>
                                            @For index = 1 To 12
                                            @<td class="text-right">
                                                @code
                                                    Dim i As String = Date.Now.Year.ToString() + index.ToString().PadLeft(2, "0")
                                                    Dim vlr As String = (From a In item.ListPlano Where a.ANOMESREF.GetValueOrDefault().ToString() = i Select a.VLRUTZPLNMKT.GetValueOrDefault().ToString("N2")).ToList().FirstOrDefault()
                                                    If Not vlr Is Nothing Then
                                            @vlr
                                                    End If
                                                End Code
                                            </td>
                                            Next
                                        </tr>
                                        <tr>
                                            <td>Saldo</td>
                                            @For index = 1 To 12
                                            @<td class="text-right">
                                                @code
                                                    Dim i As String = Date.Now.Year.ToString() + index.ToString().PadLeft(2, "0")
                                                    Dim vlr As String = (From a In item.ListPlano Where a.ANOMESREF.GetValueOrDefault().ToString() = i Select (a.VLROCDPLNMKT - a.VLRUTZPLNMKT - a.VLRRSVPLNMKT).GetValueOrDefault().ToString("N2")).ToList().FirstOrDefault()
                                                    If Not vlr Is Nothing Then
                                            @vlr
                                                    End If
                                                End Code
                                            </td>
                                            Next
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            Next
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <a href="" data-dismiss="modal" class="btn btn-danger btn-white pull-right">
                    <i class="ace-icon fa fa-times bigger"></i>
                    Cancelar
                </a>
            </div>
        </div>
    </div>
</div>
