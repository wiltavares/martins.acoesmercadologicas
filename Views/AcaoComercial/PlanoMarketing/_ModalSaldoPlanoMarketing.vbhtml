﻿@ModelType List(Of RelacaoAcaoComercialxPlanoMarketingViewModel)
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Saldo Plano de Marketing
        </div>
        <div class="modal-body">
            <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                <thead>
                    <tr role="row">
                        <th>BU</th>
                        <th>Saldo Mensal</th>
                        <th>Saldo Atual</th>
                        <th>Saldo Anual</th>
                        <th>Valor Ação</th>
                        <th>Resultado Mensal</th>
                        <th>Resultado Atual</th>
                        <th>Resultado Anual</th>
                    </tr>
                </thead>
                <tbody>
                    @For Each item In Model
                        @<tr>
                            <td>
                                @Html.Label("DESUNDESRNGC", String.Format("{0} - {1}", item.CODUNDESRNGC, item.DESUNDESRNGC))
                            </td>
                            <td class="text-right">
                                @Html.Label("VLRSLDMNSPLNMKT", item.VLRSLDMNSPLNMKT.GetValueOrDefault.ToString("N2"))
                            </td>
                            <td class="text-right">
                                @Html.Label("VLRSLDATUPLNMKT", item.VLRSLDATUPLNMKT.GetValueOrDefault.ToString("N2"))
                            </td>
                            <td class="text-right">
                                @Html.Label("VLRSLDANOPLNMKT", item.VLRSLDANOPLNMKT.GetValueOrDefault.ToString("N2"))
                            </td>
                            <td class="text-right @IIf(item.VLRACAO < 0.0, "red", "")">
                                @Html.Label("VLRACAO", item.VLRACAO.GetValueOrDefault.ToString("N2"))
                            </td>
                            <td class="text-right @IIf(item.VLRESULTADOMENSAL < 0.0, "red", "")">
                                @Html.Label("VLRESULTADOMENSAL", item.VLRESULTADOMENSAL.GetValueOrDefault.ToString("N2"))
                            </td>
                            <td class="text-right @IIf(item.VLRESULTADOATUAL < 0.0, "red", "") ">
                                @Html.Label("VLRESULTADOATUAL", item.VLRESULTADOATUAL.GetValueOrDefault.ToString("N2"))
                            </td>
                            <td class="text-right @IIf(item.VLRESULTADOANUAL < 0.0, "red", "") ">
                                @Html.Label("VLRESULTADOANUAL", item.VLRESULTADOANUAL.GetValueOrDefault.ToString("N2"))
                            </td>
                        </tr>
                    Next
                </tbody>
            </table>
        </div>
        <div class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <a href="" data-dismiss="modal" class="btn btn-danger btn-white pull-right">
                    <i class="ace-icon fa fa-times bigger"></i>
                    Cancelar
                </a>
            </div>
        </div>
    </div>
</div>
