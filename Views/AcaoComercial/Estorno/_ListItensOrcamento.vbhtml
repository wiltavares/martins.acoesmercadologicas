﻿@ModelType List(Of RelacaoAcaoComercialPorItemOrcamentoViewModel)

<div style="width: auto; overflow-x: scroll">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
        </thead>
        <tbody>
            @If Model IsNot Nothing AndAlso Model.Count > 0 Then
                @<tr>
                    <td>
                        @Html.CheckBox("ckbTodos", New With {.onclick = "marcarOuDesmarcarTodos()"}) (*) - Todos
                    </td>
                </tr>
                @For Each item In Model
                    Dim idCheck = "ckbEst" & item.CODOPEFSCDSNACOCMC
                    @<tr>
                        <td>
                            <input id="@idCheck" type="checkbox" value="false" onclick="atualizarValoresEstorno()" /> @item.CODOPEFSCDSNACOCMC - @item.DESOPEFSCDSNACOCMC Saldo: @item.SALDO.ToString("#,##0.00")
                        </td>
                    </tr>
                Next
                Else
                @<tr>
                    <td class="red-color">
                        Nenhum Item Disponível para Estorno
                    </td>
                </tr>
            End If
        </tbody>
    </table>
</div>

<script>


</script>


