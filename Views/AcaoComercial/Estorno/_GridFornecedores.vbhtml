﻿@ModelType GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div style="width: auto; overflow-x: scroll">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 500px">
        <thead>
            <tr role="row">
                <th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy, Model.ResultID)">
                    Código
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NOMFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFRN", Model.Page.OrderBy, Model.ResultID)">
                    Nome do Fornecedor
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Valor Participação
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("PERCENTUAL", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("PERCENTUAL", Model.Page.OrderBy, Model.ResultID)">
                    Percentual Participação
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("EstornoAcordo", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("EstornoAcordo", Model.Page.OrderBy, Model.ResultID)">
                    Estorno Ação
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr>
                    <td>
                        @item.CODFRN
                    </td>
                    <td>
                        @item.NOMFRN
                    </td>
                    <td class="text-right">
                        @IIf(item.VLRUTZACOCMC Is Nothing, 0, String.Format("{0:N}", item.VLRUTZACOCMC.GetValueOrDefault()))
                    </td>
                    <td class="text-right">
                        @IIf(item.PERCENTUAL Is Nothing, 0, String.Format("{0:N}", item.PERCENTUAL.GetValueOrDefault()))
                    </td>
                    <td class="green-color text-right">
                        @IIf(item.EstornoAcordo Is Nothing, "", String.Format("{0:N}", item.EstornoAcordo.GetValueOrDefault()))
                    </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />

@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code

