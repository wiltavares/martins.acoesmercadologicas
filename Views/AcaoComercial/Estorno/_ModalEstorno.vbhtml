﻿@ModelType EstornoAcaoMercadologicaViewModel
@Imports Martins.AcoesMercadologicas.Core
<div class="modal-dialog modal-lg">
    <div class="modal-content">

        <div class="modal-header ">
            <i class="ace-icon glyphicon glyphicon-export bigger-120"></i>
            &nbsp;Estorno Manual
        </div>

        <div class="modal-body">

            <div class="row">
                <div id="msgModalEstorno">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div class="col-sm-5">
                    @Html.HiddenFor(Function(m) m.CODACOCMC, New With {.Class = "form-control", .id = "idCodAcaoComercial"})
                    @Html.Label("lblAcao", "Ação:")
                    @Html.TextBoxFor(Function(m) m.CODIGO_NOME, New With {.Class = "form-control", .readonly = "readonly"})
                </div>
                <div class="col-sm-5">
                    @Html.Label("lblEmpenho", "Emp. Exa Acordo:")
                    @Html.TextBoxFor(Function(m) m.EMPEXAACORDO, "{0:0.00}", New With {.Class = "form-control", .readonly = "readonly"})
                </div>
                <div class="col-sm-2">
                    @Html.Label("lblSaldo", "Valor do Saldo:")
                    @Html.TextBoxFor(Function(m) m.VALOR_SALDO, New With {.Class = "form-control", .readonly = "readonly", .alt = "decimal_positivo_2"})
                </div>
            </div>
            <br class="clearfix" />

            <div id="listEstornoItens">
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Estorno/_ListItensOrcamento.vbhtml", Model.ItensOrcamento)
                End Code
            </div>

            <br class="clearfix" />

            <div id="gridEstornoFornecedores">
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Estorno/_GridFornecedores.vbhtml", Model.StateGridFornecedores)
                End Code
            </div>

        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">

                @Code
                    If Not (Model.VALOR_SALDO Is Nothing Or (Model.VALOR_SALDO >= 0 AndAlso Model.VALOR_SALDO < 0.1)) Then
                        @<a class="btn btn-white btn-primary" onclick="EstornarItensAcao()">
                            <i class=" ace-icon glyphicon glyphicon-export bigger blue">
                            </i>
                            Estornar
                        </a>
                    End If
                End Code
                <a href="" data-dismiss="modal" class="btn btn-danger btn-white pull-right">
                    <i class="ace-icon fa fa-times bigger"></i>
                    Cancelar
                </a>
            </div>
        </div>
    </div>
</div>
