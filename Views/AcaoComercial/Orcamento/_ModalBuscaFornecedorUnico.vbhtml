﻿@ModelType GridSettings(Of PoVendorSitesAllViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim AJAX_OBJECT_POST_FornecedorUnico As New AjaxOptions() With {
        .UpdateTargetId = "resultadosFrnUnico",
        .InsertionMode = InsertionMode.Replace,
        .HttpMethod = "POST",
        .OnBegin = "LoadingOn",
        .OnComplete = "LoadingOff_UpdateMessage",
        .OnSuccess = "atualizaMensagem('msgBuscaFornecedorUnico')"
    }
End Code


<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Fornecedor Único
        </div>
        <div class="modal-body">
            <div id="msgBuscaFornecedorUnico">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            @Using (Ajax.BeginForm("CsnFrnIteRsu", "ItensOrcamento", AJAX_OBJECT_POST_FornecedorUnico))
                @Html.AntiForgeryToken()
                @<div class="row">
                    <div class="col-xs-12">
                        @Html.Label("item", "Contrato/Fornecedor:")
                    </div>
                </div>
                @<div class="row">
                    <div class="col-lg-4 col-sm-6">
                        @Html.TextBoxFor(Function(m) m.Filter.NUMCGCEMPFRN, New With {.Class = "form-control", .id = "numFrnUnq"})
                    </div>
                    <div class="col-lg-8 col-sm-6">
                        <div class="input-group">
                            @Html.TextBoxFor(Function(m) m.Filter.RAZSOCEMPFRN, New With {.Class = "form-control", .id = "razFrnUnq"})
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-white" type="submit">
                                    <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            End Using
            <div id="resultadosFrnUnico">
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Orcamento/_ResultadosFornecedorUnico.vbhtml", Model)
                End Code
            </div>
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <button class="btn btn-danger btn-white" onclick="cancelarFornecedorUnico()">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
