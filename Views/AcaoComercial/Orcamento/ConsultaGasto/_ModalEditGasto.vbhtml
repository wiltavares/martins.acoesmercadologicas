﻿@ModelType RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel
@Imports Martins.AcoesMercadologicas.Core


<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-edit bigger-120"></i>
            &nbsp;Forma de utilização
        </div>
        <div class="modal-body">
            <div id="msgModalEditGasto">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>


            @Html.HiddenFor(Function(f) f.CODACOCMC, New With {.id = "hdfEditGasto_CODACOCMC"})
            @Html.HiddenFor(Function(f) f.NUMLNHITEOCDACOCMC, New With {.id = "hdfEditGasto_NUMLNHITEOCDACOCMC"})
            @Html.HiddenFor(Function(f) f.NUMSEQUTZITEOCD, New With {.id = "hdfEditGasto_NUMSEQUTZITEOCD"})
            @Html.HiddenFor(Function(f) f.CODFNC, New With {.id = "hdfEditGasto_CODFNC"})
            @Html.HiddenFor(Function(f) f.DATGRCRLC, New With {.id = "hdfEditGasto_DATGRCRLC"})

            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    @Html.Label("txtEditGasto_CODUTZITEOCDACOCMC", "Código:")
                    @Html.TextBoxFor(Function(f) f.CODUTZITEOCDACOCMC, New With {.Class = "form-control", .id = "txtEditGasto_CODUTZITEOCDACOCMC", .alt = "inteiro"})
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    @Html.Label("txtEditGasto_DESUTZITEOCDACOCMC", "Descrição:")
                    @Html.TextAreaFor(Function(f) f.DESUTZITEOCDACOCMC, New With {.Class = "form-control", .id = "txtEditGasto_DESUTZITEOCDACOCMC"})
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    @Html.Label("txtEditGasto_QDEUTZITEOCDACOCMC", "Quantidade:")
                    @Html.TextBoxFor(Function(f) f.QDEUTZITEOCDACOCMC, New With {.Class = "form-control", .id = "txtEditGasto_QDEUTZITEOCDACOCMC", .alt = "inteiro"})
                </div>

                <div class="col-lg-4 col-sm-6">
                    @Html.Label("txtEditGasto_VLRUTZITEOCDACOCMC", "Valor:")
                    @Html.TextBoxFor(Function(f) f.VLRUTZITEOCDACOCMC, New With {.Class = "form-control", .id = "txtEditGasto_VLRUTZITEOCDACOCMC", .alt = "decimal_positivo_2"})
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    @Html.Label("txtEditGasto_INDTIPISRUTZITEOCD", "Tipo:")
                    <br />
                    @Html.RadioButtonFor(Function(f) f.INDTIPISRUTZITEOCD, 0, New With {.id = "rblEditGasto_INDTIPISRUTZITEOCD_Gasto"})
                    @Html.Label("rblEditGasto_INDTIPISRUTZITEOCD_Gasto", "Gasto")
                    @Html.RadioButtonFor(Function(f) f.INDTIPISRUTZITEOCD, 1, New With {.id = "rblEditGasto_INDTIPISRUTZITEOCD_Estorno"})
                    @Html.Label("rblEditGasto_INDTIPISRUTZITEOCD_Estorno", "Estorno")
                </div>
            </div>
            @Html.HiddenFor(Function(f) f.INDTIPISRUTZITEOCD, New With {.id = "hdfEditGasto_INDTIPISRUTZITEOCD"})

        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-success btn-white" onclick="salvarUtilizacaoItem();">
                    <i class="ace-icon fa fa-save bigger-120 success"></i>
                    Salvar
                </button>
                <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Fechar
                </a>
            </div>
        </div>
    </div>
</div>
