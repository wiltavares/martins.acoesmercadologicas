﻿@ModelType GridSettings(Of RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 2000px;">
        <thead>
            <tr role="row">
                <th class="@Grid.AtribuirIconeOrdenacao("NUMSEQUTZITEOCD", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NUMSEQUTZITEOCD", Model.Page.OrderBy, Model.ResultID)">
                    Seq
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("TIPUTLZ", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("TIPUTLZ", Model.Page.OrderBy, Model.ResultID)">
                    Tipo
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODUTZITEOCDACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODUTZITEOCDACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Código
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESUTZITEOCDACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESUTZITEOCDACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Descrição da Utilização
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZITEOCDACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZITEOCDACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Vlr. Unitário
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("QDEUTZITEOCDACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("QDEUTZITEOCDACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Qtd.
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VlrUtz", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VlrUtz", Model.Page.OrderBy, Model.ResultID)">
                    Vlr.Utilizado
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NOMFNC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFNC", Model.Page.OrderBy, Model.ResultID)">
                    funcionário
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DATGRCRLC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATGRCRLC", Model.Page.OrderBy, Model.ResultID)">
                    Data
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr>
                    <td>
                        @item.NUMSEQUTZITEOCD
                    </td>
                    <td>
                        @item.TIPUTLZ
                    </td>
                    <td>
                        @item.CODUTZITEOCDACOCMC
                    </td>
                    <td>
                        @item.DESUTZITEOCDACOCMC
                    </td>
                     <td class="text-right">
                         @CDec(item.VLRUTZITEOCDACOCMC).ToString("#,##0.00")
                     </td>
                    <td>
                        @item.QDEUTZITEOCDACOCMC
                    </td>
                     <td class="text-right">
                         @CDec(item.VlrUtz).ToString("#,##0.00")
                     </td>
                    <td>
                        @item.NOMFNC
                    </td>
                    <td>
                        @CDate(item.DATGRCRLC).ToShortDateString()
                    </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />
@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code