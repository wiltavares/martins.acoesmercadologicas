﻿@ModelType GridSettings(Of RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small widget-light-blue">
        <h4 class="widget-title smaller">
            Itens
        </h4>
        <div class="widget-toolbar">
            <div class="btn-group">
                <a href="#" data-action="collapse">
                    <i class="ace-icon fa fa-chevron-up blue"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div id="gridItensGastos">
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Orcamento/ConsultaGasto/_ResultConsultaGasto.vbhtml", Model)
                End Code
            </div>
            <div class="row">
                <div class="col-sm-3">
                    Total Gasto: @ViewBag.TotalGasto
                </div>
                <div class="col-sm-3">
                    Total Estornado: @ViewBag.TotalEstorno
                </div>
                <div class="col-sm-3">
                    Total Utilizado: @ViewBag.TotalUtz
                </div>
            </div>
        </div>
    </div>
</div>

