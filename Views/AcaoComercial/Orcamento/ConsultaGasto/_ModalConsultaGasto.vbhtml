﻿@ModelType GridSettings(Of RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel)
@Imports Martins.AcoesMercadologicas.Core


<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Relação Item do Orçamento x Utilização
        </div>
        <div class="modal-body">
            <div id="msgModalConsultaGasto">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    @Html.Label("acao", "Ação:")
                    @Html.TextBoxFor(Function(f) f.Filter.CODACOCMC, New With {.Class = "form-control", .id = "idItem", .readOnly = "true"})
                </div>
                <div class="col-lg-4 col-sm-6">
                    @Html.Label("nome", "Nome:")
                    @Html.TextBox("NomAcao", ViewBag.NomAcaoGasto, New With {.Class = "form-control", .id = "idItem", .readOnly = "true"})
                </div>
                <div class="col-lg-4 col-sm-6">
                    @Html.Label("valor", "Valor Total Item:")
                    @Html.TextBox("valorTotal", ViewBag.ValorTotal, New With {.Class = "form-control", .id = "idItem", .readOnly = "true"})
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    @Html.Label("item", "Item:")
                    @Html.TextBox("item", ViewBag.NomItemGasto, New With {.Class = "form-control", .id = "idItem", .readOnly = "true"})
                </div>
                <div class="col-lg-6 col-sm-6">
                    @Html.Label("operacao", "Tipo de Operação:")
                    @Html.TextBox("operacao", ViewBag.TipoOperacaoGasto, New With {.Class = "form-control", .id = "idItem", .readOnly = "true"})
                </div>
            </div>
            <div id="resultadosConsultaGasto">
                @Code
                    Html.RenderPartial("~\Views\AcaoComercial\Orcamento\ConsultaGasto\_WidgetConsultaGasto.vbhtml", Model)
                End Code
            </div>
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                @if (ViewBag.CodTipoOperacao = 13) Then
                    @<button type="button" class="btn btn-primary btn-white" onclick="adicionarUtilizacaoItem(@ViewBag.CodItemGasto);">
                        <i class="ace-icon fa fa-plus bigger-120 blue"></i>
                        Adicionar
                    </button>
                End If


                <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Fechar
                </a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalEditGasto"></div>