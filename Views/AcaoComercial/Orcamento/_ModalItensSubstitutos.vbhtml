﻿@ModelType Tuple(Of GridSettings(Of MercadoriaViewModel), RelacaoAcaoComercialPorItemOrcamentoViewModel, GridSettings(Of MercadoriaViewModel))
@Imports Martins.AcoesMercadologicas.Core

<div class="modal fade" id="ModalImportXLS"></div>
<div class="modal fade" id="modalResultImport"></div>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Inserir Item Substituto
        </div>
        <div class="modal-body">
            <div id="msgItensSubstitutos">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div id="msgModalBonificacao">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            @code
                Html.RenderPartial("~/Views/AcaoComercial/Orcamento/_DadosRelacaoModalItensSubstitutos.vbhtml", Model.Item2)
            End Code
            <div class="legend-group-box">
                @Using (Ajax.BeginForm("PesquisarSubstitutos", "ItensOrcamento", Constantes.AJAX_OBJECT_POST_DEFAULT))
                    @Html.AntiForgeryToken()
                    @<div class="form-horizontal">
                        <div class="row">
                            <div class="col-sm-12" id="divFiltroItensSubstitutos">
                                <div class="row">
                                    <div class="col-xs-3">
                                        @Html.TextBoxFor(Function(m) m.item1.Filter.CODMER, New With {.Class = "form-control", .id = "filtroCodMerSubstituta"})
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="row">
                                            @Html.TextBoxFor(Function(m) m.item1.Filter.DESMER, New With {.Class = "form-control", .id = "dropMercadoriaSubstituta"})
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="btn-group pull-right">
                                            <button class="btn btn-info btn-white" type="button" onclick="btnBuscaItensSubstitutos()">
                                                <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                End Using
            </div>

            <div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title smaller">
                        Itens Substitutos
                    </h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="resultadosBuscaItensSubstitutos">
                                    @Code
                                        Html.RenderPartial("~/Views/AcaoComercial/Orcamento/_ResultadoItensSubstitutos.vbhtml", Model.Item1)
                                    End Code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @*<div class="row">
                <div class="col-sm-12">
                    <div class="btn-group pull-right">
                        <a onclick="selecionarItensSubstitutos()" class="btn btn-success btn-white">
                            <i class="ace-icon fa fa-plus bigger"></i>
                            Selecionar Itens Substitutos
                        </a>
                        <a onclick="removerItensSubstitutos()" class="btn btn-danger btn-white">
                            <i class="ace-icon fa fa-times bigger"></i>
                            Remover Itens Substitutos
                        </a>
                    </div>
                </div>
            </div>*@


            <div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title smaller">
                        Itens Substitutos Selecionados
                    </h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="gridItensSubstitutosSelecionados">
                                    @Code
                                        Html.RenderPartial("~/Views/AcaoComercial/Orcamento/_ResultadoItensSubstitutosSelecionados.vbhtml", Model.Item3)
                                    End Code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <a class="btn btn-grey btn-white" onclick="limparItensSubs()">
                    <i class="ace-icon fa fa-eraser bigger"></i>
                    Limpar
                </a>
                <a class="btn btn-white btn-yellow" id="btnImportXls" onclick="abrirModal('ItensOrcamento', 'AbrirModarImport', undefined, '#ModalImportXLS', ModalImportXlsItensSubstitutosSuccess)">
                    <i class="ace-icon fa fa-file-excel-o bigger-120"></i>
                    Importar Planilha
                </a>
                <a onclick="SalvarCacheItensSub()" class="btn btn-success btn-white">
                    <i class="ace-icon fa fa-check-square-o bigger"></i>
                    Concluir
                </a>
                <a onclick="modalSubstitutos()" class="btn btn-danger btn-white">
                    <i class="ace-icon fa fa-times bigger"></i>
                    Cancelar
                </a>
            </div>
        </div>
    </div>
</div>
