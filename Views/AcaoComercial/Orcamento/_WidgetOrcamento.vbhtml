﻿@ModelType AcaoComercialViewModel
@Imports Martins.AcoesMercadologicas.Core
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small widget-light-blue">
        <h4 class="widget-title smaller">
            Orçamento
            @If (Model.TIPACOMCD = TipoAcao.PreAcordoVigencia) Then
                @<big class="green">Limitado por Vigência</big>
            ElseIf Model.TIPACOMCD = TipoAcao.PreAcordoValor Then
                @<big class="red">Limitado por Valor</big>
            End If
        </h4>
        <div class="widget-toolbar">
            <div class="btn-group">
                <a class="btn btn-md btn-success btn-white" style="@ViewHelper.Visible(ViewBag.btnIsrItemVisible)" id="btnIsrItem" onclick="btnInserirItemAcaoComercial()">
                    <i class="ace-icon fa fa-plus bigger-120"></i>
                    Incluir Item
                </a>
            </div>
            <div class="btn-group">
                <a class="btn btn-md btn-primary btn-white" style="@ViewHelper.Visible(ViewBag.btnIsrItemVisible And Model.INDACOCMCBTBVLR <> 3)" id="btnIsrItem" onclick="btnImportItemOrcamento()">
                    <i class="ace-icon fa fa-file-excel-o bigger-120"></i>
                    Importar Item
                </a>
            </div>
            <div class="btn-group">
                <a href="#" data-action="collapse">
                    <i class="ace-icon fa fa-chevron-up blue"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div id="gridItensOrcamento">
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Orcamento/_GridOrcamento.vbhtml", Model.StateGridItensOrcamento)
                End Code
            </div>
            <div class="row">
                <div class="col-sm-3">
                    Total Valor Orçamento: @Model.ValorTotal.ToString("#,##0.00")
                </div>
                <div class="col-sm-3">
                    Total Vlr. Brinde: @Model.ValorTotalBrinde.ToString("#,##0.00")
                </div>
                @If Not (Model.TIPACOMCD = TipoAcao.PreAcordoVigencia) Then
                    @<div class="col-sm-3">
                        Total Saldo Orçamento: @Model.SaldoOrcamento.ToString("#,##0.00")
                    </div>
                End If
                <div class="col-sm-3">
                    Total Vlr. Cliente: @Model.ValorCliente.ToString("#,##0.00")
                </div>
            </div>
        </div>
    </div>
</div>
