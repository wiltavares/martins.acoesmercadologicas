﻿@ModelType RelacaoAcaoComercialPorItemOrcamentoViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim AJAX_OBJECT_POST_ItensSubs As New AjaxOptions() With {
        .UpdateTargetId = "modal",
        .InsertionMode = InsertionMode.Replace,
        .OnComplete = "RecarregaImportados",
        .HttpMethod = "POST"
    }
End Code

<div class="modal-dialog modal-md" id="modalErros">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-file-excel-o bigger-120"></i>
            Importar Planilha
        </div>
      
        @Using (Ajax.BeginForm("ImportarPlanilhaItemOrcamento", "ItensOrcamento", AJAX_OBJECT_POST_ItensSubs, New With {.enctype = "multipart/form-data", .id = "arquivo"}))
            @<div class="modal-body">
                 <div class="form-horizontal">
                     <div class="row">
                         <div class="col-lg-6 col-sm-6">
                             @Html.Label("tipoOP", "Tipo de Operação:")
                             @Html.DropDownListFor(Function(m) m.INDTIPOPEDSNACOCMC, New SelectList(ViewBag.operacao, "CODOPEFSCDSNACOCMC", "DROPDESC", Model.INDTIPOPEDSNACOCMC), "Selecione...", New With {.Class = "form-control", .id = "dropOperacao", .onchange = "carregaDropFilialItensOrcamento()"})
                             @Html.Label("lblErroOperacao", "Favor Selecionar uma Operação.", New With {.id = "lblErroOperacao", .style = "display: none;", .class = "text-danger-message-login"})
                         </div>
                         @*<div class="col-lg-4 col-sm-6">
                             @Html.Label("filial", "Filial:")
                             @Html.DropDownListFor(Function(m) m.CODFILEMP, New SelectList(ViewBag.filiais, "CODFILEMP", "DROPDESC", Model.CODFILEMP), "Selecione...", New With {.Class = "form-control", .id = "dropFilial"})
                         </div>*@
                         <div class="col-lg-6 col-sm-6" id="DropBUImportacao">
                             @Html.Label("bu", "BU:")
                             @Html.DropDownListFor(Function(m) m.CODUNDESRNGCOPEDSN, New SelectList(ViewBag.unidadeNegocio, "CODUNDESRNGC", "DROPDESC", Model.CODUNDESRNGCOPEDSN), "Selecione...", New With {.Class = "form-control chosen-select", .id = "drupBU"})
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12">
                             @Html.Label("centroCustoID", "Centro de Custo:")
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-3 col-lg-3 col-sm-4">
                             @Html.TextBoxFor(Function(f) f.CODCENCST, Nothing, New With {.Class = "form-control", .id = "centroCustoID"})
                         </div>
                         <div class="col-xs-9 col-lg-9 col-sm-8">
                             <div class="input-group">
                                 @Html.DropDownListFor(Function(f) f.DESCENCST, New SelectList(ViewBag.dropCentroCusto, "CODCENCST", "DESCENCST", Model.DESCENCST), New With {.Class = "form-control", .id = "centroCustoDES", .onchange = "centroCustoDES_onchange()"})
                                 <span class="input-group-btn">
                                     <button class="btn btn-info btn-white" onclick="btnBuscaCentroCusto()" id="btnCentroCusto" type="button">
                                         <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                     </button>
                                 </span>
                             </div>
                         </div>
                     </div>
                     <div class="row">
                         <br />
                         <div class="col-lg-12 col-sm-12">
                             <label class="ace-file-input">
                                 <input type="file" accept=".xls" name="file_upload" id="file_upload" class="ace-file-container" data-title="Choose" />
                                 <span Class="ace-file-container" data-title="Choose">
                                     <span Class="ace-file-name" data-title="Sem arquivo ...">
                                         <i Class=" ace-icon fa fa-upload">
                                         </i>
                                     </span>
                                 </span>
                                 <a Class="remove" href="#">
                                     <i Class=" ace-icon fa fa-times">
                                     </i>
                                 </a>
                             </label>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-lg-12">
                             @Html.Label("txtFormaUtlz", "Forma de Utilização:", New With {.id = "txtFormaUtlz"})
                             @Html.TextAreaFor(Function(f) f.DESFRMUTZITEACOCMC, 4, 100, New With {.id = "FormaUtlz", .class = "form-control"})
                             @Html.Label("lblErroForma", "Favor informar a forma de utilização.", New With {.id = "lblErroFormaUtlz", .style = "display: none;", .class = "text-danger-message-login"})
                         </div>
                     </div>
                 </div>
            </div>

            @<div Class="modal-footer clearfix">
                 <div Class="btn-group pull-right">
                     <a href="/martins.acoesmercadologicas/TemplateXLS/ItemOrcamento.xls" class="btn btn-grey btn-white">
                         <i class="ace-icon fa fa-cloud-download bigger-120"></i>
                         Template
                     </a>

                     <Button type="submit" name="arquivo" id="arquivo" value="Importar" Class="btn btn-info btn-white">
                         <i Class="ace-icon fa fa-cloud-upload bigger-120"></i>
                         Importar
                     </Button>
                     <a onclick="cancelarImportacaoItensOrcamento()" class="btn btn-danger btn-white">
                         <i class="ace-icon fa fa-close bigger-120 red"></i>
                         Sair
                     </a>
                 </div>
            </div>
        End Using

    </div>
</div>
