﻿@ModelType GridSettings(Of MtsPorRequisicaoVViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Pedido
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:1500px">
                    <thead>
                        <tr role="row">
                            <th class="@Grid.AtribuirIconeOrdenacao("PURCHASE_NUM", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("PURCHASE_NUM", Model.Page.OrderBy, Model.ResultID)">
                                Nro. Compra
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("PURCHASE_TYPE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("PURCHASE_TYPE", Model.Page.OrderBy, Model.ResultID)">
                                Tipo Compra
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("PURCHASE_STATUS", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("PURCHASE_STATUS", Model.Page.OrderBy, Model.ResultID)">
                                Status Compra
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VENDOR_NAME", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VENDOR_NAME", Model.Page.OrderBy, Model.ResultID)">
                                Nome Vendedor
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("PURCHASE_UNIT_PRICE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("PURCHASE_UNIT_PRICE", Model.Page.OrderBy, Model.ResultID)">
                                Preço Unt.
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("PURCHASE_QUANTITY", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("PURCHASE_QUANTITY", Model.Page.OrderBy, Model.ResultID)">
                                Qde.
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("PURCHASE_TOTAL", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("PURCHASE_TOTAL", Model.Page.OrderBy, Model.ResultID)">
                                Valor Total
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("PURCHASE_QUANTITY_RECEIVED", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("PURCHASE_QUANTITY_RECEIVED", Model.Page.OrderBy, Model.ResultID)">
                                Qde. Recebida
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("PURCHASE_QUANTITY_CANCELLED", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("PURCHASE_QUANTITY_CANCELLED", Model.Page.OrderBy, Model.ResultID)">
                                Qde. Cancelada
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td>
                                    @item.PURCHASE_NUM
                                </td>
                                <td>
                                    @item.PURCHASE_TYPE
                                </td>
                                <td>
                                    @item.PURCHASE_STATUS
                                </td>
                                <td>
                                    @item.VENDOR_NAME
                                </td>
                                <td>
                                    @item.PURCHASE_UNIT_PRICE
                                </td>
                                <td>
                                    @item.PURCHASE_QUANTITY
                                </td>
                                <td>
                                    @item.PURCHASE_TOTAL
                                </td>
                                <td>
                                    @item.PURCHASE_QUANTITY_RECEIVED
                                </td>
                                <td>
                                    @item.PURCHASE_QUANTITY_CANCELLED
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code
        </div>
    </div>
</div>



