﻿@ModelType Tuple(Of GridSettings(Of MtsPorRequisicaoVViewModel), GridSettings(Of MtsPorRequisicaoVViewModel))
@Imports Martins.AcoesMercadologicas.Core

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Item no Suprimentos
        </div>
        <div class="modal-body">
            <div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title smaller">
                        Item no Suprimento
                    </h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="ace-icon fa fa-chevron-up blue"></i>
                        </a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            <div class="col-lg-2 col-sm-6">
                                @Html.Label("req", "Requisição:")
                                @Html.TextBoxFor(Function(m) m.Item1.ListPaged.Item(0).REQUISITION_NUM, New With {.Class = "form-control", .readonly = "readonly", .id = "req"})
                            </div>
                            <div class="col-lg-5 col-sm-6">
                                @Html.Label("tipoReq", "Tipo Requisição:")
                                @Html.TextBoxFor(Function(m) m.Item1.ListPaged.Item(0).REQUISITION_TYPE, New With {.Class = "form-control", .readonly = "readonly", .id = "tipoReq"})
                            </div>
                            <div class="col-lg-5 col-sm-6">
                                @Html.Label("statusReq", "Status Requisição:")
                                @Html.TextBoxFor(Function(m) m.Item1.ListPaged.Item(0).REQUISITION_APPROVED_STATUS, New With {.Class = "form-control", .readonly = "readonly", .id = "statusReq"})
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div id="GridPedido">@* primeira tabela *@
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Orcamento/_ResultadoPedidoItem.vbhtml", Model.Item1)
                End Code
            </div>
            <div id="GridNotaFiscal">@* segunda tabela *@
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Orcamento/_ResultadoNotaFiscal.vbhtml", Model.Item2)
                End Code
            </div>
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <button class="btn btn-danger btn-white" onclick="sairModalSuprimentos()">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Sair
                </button>
            </div>
        </div>
    </div>
</div>
