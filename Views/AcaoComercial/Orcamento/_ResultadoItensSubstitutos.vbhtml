﻿@ModelType GridSettings(Of MercadoriaViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 800px;">
        <thead>
            <tr role="row">
                <th class="center">
                    Selecionar
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESMER", Model.Page.OrderBy, Model.ResultID)">
                    Item Substituto
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("ValorUnitario", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("ValorUnitario", Model.Page.OrderBy, Model.ResultID)">
                    Valor Unitário
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("quantdadeSubstitutos", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("quantdadeSubstitutos", Model.Page.OrderBy, Model.ResultID)">
                    Quantidade
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("valorTotalSubs", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("valorTotalSubs", Model.Page.OrderBy, Model.ResultID)">
                    Valor Total
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                Dim idCheckInc = "ckbIncPed" & item.CODMER

                @<tr>
                    <td class="center">
                        <button data-rel="tooltip" title="Selecionar" class="btn btn-xs btn-success" onclick="selecionarItensSubstitutos(@item.CODMER)">
                            <i class="ace-icon fa fa-plus bigger-120"></i>
                        </button>
                        @*<input id="@idCheckInc" type="checkbox" value="false" />*@
                    </td>
                    <td>
                        @item.DESMER
                    </td>
                    <td>
                        @Format(item.ValorUnitario, ("#,##0.00"))
                    </td>
                    <td>
                        @item.quantdadeSubstitutos
                    </td>
                    <td>
                        @Format(item.valorTotalSubs, ("#,##0.00"))
                    </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />

@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code
