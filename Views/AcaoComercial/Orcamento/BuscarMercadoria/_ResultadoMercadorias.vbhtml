﻿@ModelType GridSettings(Of MercadoriaViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="center">Selecionar</th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMER", Model.Page.OrderBy, Model.ResultID)">
                                Código
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESMER", Model.Page.OrderBy, Model.ResultID)">
                                Nome
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @code
                            Dim idt As String
                            Dim vlr As String
                            If (Not IsNothing(item.IDTITE)) Then
                                idt = "," + item.IDTITE.ToString
                            Else
                                idt = ""
                            End If
                            If (Not IsNothing(item.VLRPCO)) Then
                                vlr = "," + item.VLRPCO.ToString.Replace(",", ".")
                            Else
                                vlr = ""
                            End If
                            
                            End Code
                            @<tr>
                                <td class="center">
                                    <div >
                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" id="merc_@item.CODMER" onclick="selecionarMercadoria(@item.CODMER,'@item.DESMER' @idt @vlr)">
                                            <i class="ace-icon fa fa-check bigger-120"></i>
                                        </button>
                                    </div>
                                </td>
                                <td>
                                    @item.CODMER
                                </td>
                                <td>
                                    @item.DESMER
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code
        </div>
    </div>
</div>


