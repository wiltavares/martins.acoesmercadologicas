﻿@ModelType List(Of MtsPorRequisicaoVViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2  @IIf(Model.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Pedido
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:1500px">
                    <thead>
                        <tr role="row">
                            <th >
                                Nro. Compra
                            </th>
                            <th>
                                Tipo Compra
                            </th>
                            <th>
                                Status Compra
                            </th>
                            <th>
                                Nome Vendedor
                            </th>
                            <th>
                                Preço Unt.
                            </th>
                            <th>
                                Qde.
                            </th>
                            <th>
                                Valor Total
                            </th>
                            <th>
                                Qde. Recebida
                            </th>
                            <th>
                                Qde. Cancelada
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @<tr>
                                <td>
                                    @item.PURCHASE_NUM
                                </td>
                                <td>
                                    @item.PURCHASE_TYPE
                                </td>
                                <td>
                                    @item.PURCHASE_STATUS
                                </td>
                                <td>
                                    @item.VENDOR_NAME
                                </td>
                                <td>
                                    @item.PURCHASE_UNIT_PRICE
                                </td>
                                <td>
                                    @item.PURCHASE_QUANTITY
                                </td>
                                <td>
                                    @item.PURCHASE_TOTAL
                                </td>
                                <td>
                                    @item.PURCHASE_QUANTITY_RECEIVED
                                </td>
                                <td>
                                    @item.PURCHASE_QUANTITY_CANCELLED
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
        </div>
    </div>
</div>



