﻿@ModelType GridSettings(Of MtsPorRequisicaoVViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Nota Fiscal
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:800px">
                    <thead>
                        <tr role="row">
                            <th class="@Grid.AtribuirIconeOrdenacao("INVOICE_NUM", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("PURCHASE_NUM", Model.Page.OrderBy, Model.ResultID)">
                                Nro. Nota Fiscal
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("OPERATION_STATUS", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("OPERATION_STATUS", Model.Page.OrderBy, Model.ResultID)">
                                Status
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INVOICE_DATE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INVOICE_DATE", Model.Page.OrderBy, Model.ResultID)">
                                Data
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INVOICE_UNIT_PRICE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INVOICE_UNIT_PRICE", Model.Page.OrderBy, Model.ResultID)">
                                Preço Unt.
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INVOICE_QUANTITY", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INVOICE_QUANTITY", Model.Page.OrderBy, Model.ResultID)">
                                Quantidade
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INVOICE_TOTAL", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INVOICE_TOTAL", Model.Page.OrderBy, Model.ResultID)">
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td>
                                    @item.INVOICE_NUM
                                </td>
                                <td>
                                    @item.OPERATION_STATUS
                                </td>
                                <td>
                                    @CDate(item.INVOICE_DATE).toShortDateString()
                                </td>
                                <td>
                                    @item.INVOICE_UNIT_PRICE
                                </td>
                                <td>
                                    @item.INVOICE_QUANTITY
                                </td>
                                <td>
                                    @item.INVOICE_TOTAL
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code
        </div>
    </div>
</div>




