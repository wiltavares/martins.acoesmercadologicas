﻿@ModelType GridSettings(Of MercadoriaViewModel)
@Imports Martins.AcoesMercadologicas.Core


<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Mercadoria
        </div>
        <div class="modal-body">
            <div id="msgBuscaMercadorias">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            @Using (Ajax.BeginForm("PesquisarSubstitutos", "ItensOrcamento", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="row">
                    <div class="col-xs-12">
                        @Html.Label("item", "Item Orçamento:")
                    </div>
                </div>
                @<div class="row">
                    <div class="col-lg-4 col-sm-6">
                        @Html.TextBoxFor(Function(m) m.Filter.CODMER, New With {.Class = "form-control", .id = "idItem"})
                        @Html.Label("lblErroTamanhoIdMercadoria", "Código do item pode ter até 10 digitos.", New With {.id = "lblErroTamanhoIdMercadoria", .style = "display: none;", .class = "text-danger-message-login"})
                    </div>
                    <div class="col-lg-8 col-sm-6">
                        <div class="input-group">
                            @Html.TextBoxFor(Function(m) m.Filter.DESMER, New With {.Class = "form-control", .id = "DesIte"})
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-white" type="button" onclick="btnBuscaItens()">
                                    <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            End Using
            <div id="resultadosBuscaItens">
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Orcamento/_ResultadoMercadorias.vbhtml", Model)
                End Code
            </div>
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <button class="btn btn-danger btn-white" onclick="limparCacheFiltroItens()">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>