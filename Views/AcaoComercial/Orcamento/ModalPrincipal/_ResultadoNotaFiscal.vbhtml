﻿@ModelType List(Of MtsPorRequisicaoVViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2  @IIf(Model.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Nota Fiscal
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:800px">
                    <thead>
                        <tr role="row">
                            <th>
                                Nro. Nota Fiscal
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                Data
                            </th>
                            <th>
                                Preço Unt.
                            </th>
                            <th>
                                Quantidade
                            </th>
                            <th>
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @<tr>
                                <td>
                                    @item.INVOICE_NUM
                                </td>
                                <td>
                                    @item.OPERATION_STATUS
                                </td>
                                <td>
                                    @CDate(item.INVOICE_DATE).ToShortDateString()
                                </td>
                                <td>
                                    @item.INVOICE_UNIT_PRICE
                                </td>
                                <td>
                                    @item.INVOICE_QUANTITY
                                </td>
                                <td>
                                    @item.INVOICE_TOTAL
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
        </div>
    </div>
</div>




