﻿@ModelType RelacaoAcaoComercialPorItemOrcamentoViewModel
@Imports Martins.AcoesMercadologicas.Core

<div class="modal fade" id="modalBuscaItem"></div>
<div class="modal fade" id="modalSuprimentos"></div>
<div class="modal fade" id="modalSubstitutos"></div>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Item da Ação
        </div>
        <div class="modal-body">
            <div id="msgModalOrcamento">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            @Code
                Dim tupla As New Tuple(Of AcaoComercialViewModel, RelacaoAcaoComercialPorItemOrcamentoViewModel)(Model.AcaoComercial, Model)
                Html.RenderPartial("~/Views/AcaoComercial/Orcamento/ModalPrincipal/_DadosAcaoModalOrcamento.vbhtml", tupla)
            End Code
            <div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title smaller">
                        Item
                    </h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <form action="/" id="MercadoriaCompleta">
                            @Html.Label("lblErroTemItensSubs", "Para alterar os campos: Tipo de Operação, Filial, Item Orçamento e/ou Quantidade, favor retirar os itens substitutos.", New With {.id = "lblErroTemItensSubs", .style = "display: none;", .class = "text-danger-message-login"})

                            @Html.HiddenFor(Function(f) f.NUMLNHITEOCDACOCMC, New With {.Class = "form-control", .id = "numLinha"})
                            @Html.HiddenFor(Function(f) f.IDTITERSU, New With {.id = "IDTITERSU"})
                            @Html.HiddenFor(Function(f) f.INDGRCREQAUTITERSU, New With {.id = "INDGRCREQAUTITERSU"})
                            <div class="row">
                                <div class="col-lg-4 col-sm-6">
                                    @Html.Label("tipoOP", "Tipo de Operação:")
                                    @Html.DropDownListFor(Function(m) m.INDTIPOPEDSNACOCMC, New SelectList(ViewBag.operacao, "CODOPEFSCDSNACOCMC", "DROPDESC", Model.INDTIPOPEDSNACOCMC), "Selecione...", New With {.Class = "form-control", .id = "dropOperacao", .onchange = "carregaDropFilial()"})
                                    @Html.Label("lblErroOperacao", "Favor Selecionar uma Operação.", New With {.id = "lblErroOperacao", .style = "display: none;", .class = "text-danger-message-login"})
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    @Html.Label("filial", "Filial:")
                                    @Html.DropDownListFor(Function(m) m.CODFILEMP, New SelectList(ViewBag.filiais, "CODFILEMP", "DROPDESC", Model.CODFILEMP), "Selecione...", New With {.Class = "form-control", .id = "dropFilial"})
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    @Html.Label("bu", "BU:")
                                    @Html.DropDownListFor(Function(m) m.CODUNDESRNGCOPEDSN, New SelectList(ViewBag.unidadeNegocio, "CODUNDESRNGC", "DROPDESC", Model.CODUNDESRNGCOPEDSN), "Selecione...", New With {.Class = "form-control chosen-select", .id = "drupBU"})
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    @Html.Label("centroCustoID", "Centro de Custo:")
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 col-lg-3 col-sm-4">
                                    @Html.TextBoxFor(Function(f) f.CODCENCST, Nothing, New With {.Class = "form-control", .id = "centroCustoID"})
                                </div>
                                <div class="col-xs-9 col-lg-9 col-sm-8">
                                    <div class="input-group">
                                        @Html.DropDownListFor(Function(f) f.DESCENCST, New SelectList(ViewBag.dropCentroCusto, "CODCENCST", "DESCENCST", Model.DESCENCST), New With {.Class = "form-control", .id = "centroCustoDES", .onchange = "centroCustoDES_onchange()"})
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-white" onclick="btnBuscaCentroCusto()" id="btnCentroCusto" type="button">
                                                <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                @*@Html.HiddenFor(Function(f) f.CODMER, New With {.Class = "form-control", .id = "idItemCodMer"})*@
                                <div class="col-xs-12">
                                    @Html.Label("item", "Item Orçamento:")
                                </div>
                                <div id="dividItem" style="display: none;">
                                    <div class="col-xs-3 col-lg-3 col-sm-4">
                                        @Html.TextBoxFor(Function(f) f.CODMER, New With {.Class = "form-control", .id = "idItemCodMer", .onChange = "modalBuscarItens()"})
                                    </div>
                                    <div class="col-xs-9 col-lg-9 col-sm-8">
                                        <div class="input-group ">
                                            @Html.TextBoxFor(Function(f) f.CODITEOCDACOCMC, Nothing, New With {.Class = "form-control", .id = "itemBusca"})
                                            <span class="input-group-btn">
                                                <button class="btn btn-white" id="btBscIte" type="button" onclick="modalBuscarItens()" style="display: block">
                                                    <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                                </button>
                                                <button class="btn btn-white" id="btTiraIte" type="button" onclick="tirarItem()" style="display: none">
                                                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12" id="divItem" style="display: block;">
                                    @Html.TextBoxFor(Function(f) f.NOMITEMORC, Nothing, New With {.Class = "form-control", .id = "idDesMerc"})
                                </div>
                                <div class="col-xs-12" id="divdropItem" style="display: none;">
                                    @Html.DropDownListFor(Function(f) f.DescNomItemOrcDrop, New SelectList(ViewBag.dropItemOrcamento, "DescNomItemOrcDrop", "DescNomItemOrcDrop", Model.DescNomItemOrcDrop), New With {.Class = "form-control", .id = "nomItemOrcDrop"})
                                </div>
                                @Html.Label("lblErroMecadoria", "Favor informar uma Mercadoria!", New With {.id = "lblErroMercadoria", .style = "display: none;", .class = "text-danger-message-login"})
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    @Html.Label("VlrUnt", "Valor Unitário do Item:")
                                    @Html.TextBoxFor(Function(f) f.VLRUNTITEOCDACOCMC, New With {.Class = "form-control", .id = "VlrUntario2", .onChange = "calculaTotalItem()", .style = "display: none;", .disabled = "true", .alt = "decimal_positivo_2"})
                                    @Html.TextBoxFor(Function(f) f.VLRUNTITEOCDACOCMC, New With {.Class = "form-control", .id = "VlrUntario", .readonly = "true", .alt = "decimal_positivo_4"})
                                    @*<button class="btn btn-info btn-white" id="btCalcPrec" style="display: none;">
                                            <i class="ace-icon fa fa-search bigger"></i>
                                            Calcular preço
                                        </button>*@
                                    <button class="btn btn-info btn-white" id="btCalcPrec" style="display: none;">
                                        <i class="ace-icon fa fa-search bigger"></i>
                                    </button>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    @Html.Label("QantUti", "Quantidade a ser utilizada:")
                                    @Html.TextBoxFor(Function(f) f.QDEITEOCDACOCMC, "0", New With {.Class = "form-control", .id = "QtdUtz", .onChange = "calculaTotalItem()", .min = "0", .alt = "inteiro"})
                                    @Html.Label("lblErroQuantidade", "Favor colocar quantidade maior que 0!", New With {.id = "lblErroQuantidade", .style = "display: none;", .class = "text-danger-message-login"})
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    @Html.Label("VlrIte", "Valor Total do item:")
                                    @Html.TextBox("valorTotal", Model.ValorTotal, New With {.Class = "form-control", .id = "TotIte", .readonly = "true", .alt = "decimal_positivo_4"})
                                    @Html.Label("lblErroTotalItem", "Valor Total do item não pode ser 0!", New With {.id = "lblErroTotalItem", .style = "display: none;", .class = "text-danger-message-login"})
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    @Html.Label("DatUtlzIte", "Dt. de Utilização do Item:")
                                    <div class="input-daterange input-group">
                                        @Html.TextBoxFor(Function(f) f.DATUTZITEACOCMC, Date.Now.ToShortDateString, New With {.class = "input-sm date-interval form-control", .id = "dtUtilizacao", .autocomplete = "off"})
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    @Html.Label("VlrpgoCliente", "Valor pago pelo cliente:", New With {.id = "VlrpgoCliente", .style = "display: none;"})
                                    @Html.TextBoxFor(Function(f) f.VLRPTCCLIITEACOCMC, "{0:0.00}", New With {.Class = "form-control", .id = "VlrpagoCliente", .style = "display: none;", .min = "0", .onChange = "calculaPagoPeloFornecedor()", .alt = "decimal_positivo_4"})
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    @Html.Label("VlrpgoForn", "Valor pago pelo fornecedor:", New With {.id = "VlrpgoForn", .style = "display: none;"})
                                    @Html.TextBoxFor(Function(f) f.VLRPTCFRNITEACOCMC, "{0:0.00}", New With {.Class = "form-control", .id = "VlrpagoForn", .readonly = "true", .style = "display: none;", .alt = "decimal_positivo_4"})
                                </div>

                                <div class="col-lg-3 col-sm-6">
                                    @Html.Label("txtDiasParaComprar", "Qt. de dias para comprar:", New With {.id = "txtDiasParaComprar", .style = "display: none;", .min = 0})
                                    @Html.TextBoxFor(Function(f) f.QDEDIACMPITEACOCMC, "", New With {.Class = "form-control", .id = "diasParaComprar", .style = "display: none;", .min = "0", .onChange = "verificaQtDiasPositivo()", .alt = "inteiro"})
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="col-sm-6" id="divfornecedorunico" style="display: none;">
                                        @*@Html.HiddenFor(Function(f) f.INDITERSUFRNUNC, New With {.Class = "form-control", .id = "INDITERSUFRNUNC"})*@
                                        @Html.Label("lblFornecedorUnico", "Fornecedor único:")
                                        <div class="col-sm-6">

                                            @Html.CheckBox("fornecedorUnico", False, New With {.id = "fornecedorUnico", .onClick = "ViaAppFornecedorUnicoCheked()"})
                                            @*@Html.CheckBoxFor(Function(f) (f.INDITERSUFRNUNC = "1"), New With {.id = "fornecedorUnico", .onClick = "ViaAppFornecedorUnicoCheked()"})*@
                                        </div>
                                    </div>
                                    <div class="col-sm-6" id="divviaApp" style="display: none;">
                                        @*@Html.HiddenFor(Function(f) f.INDITERSUARZPRVPGT, New With {.Class = "form-control", .id = "INDITERSUARZPRVPGT"})*@
                                        @Html.Label("lblApp", "Via APP.:", New With {.id = "txtviaApp"})
                                        <div class="col-sm-6">
                                            @Html.CheckBox("viaAppNome", False, New With {.id = "viaApp", .onClick = "ViaAppFornecedorUnicoCheked()"})
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="formaPgtDiv" style="display: none;">
                                @Html.HiddenFor(Function(f) f.IDTTERPGTITERSU, New With {.Class = "form-control", .id = "FormaPgt"})
                                <div class="col-xs-12">
                                    @Html.Label("formaPgt", "Forma de Pagamento:")
                                    <div class="input-group" id="dividItem">
                                        @Html.TextBox("IDTTERPGTITERSU_TXT", "", New With {.Class = "form-control", .id = "dropFormPgt"})
                                        <span class="input-group-btn">
                                            <button class="btn btn-white" id="btFormPgt" type="button" onclick="modalFormPgt()" style="display: block">
                                                <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                            </button>
                                            <button class="btn btn-white" id="btTiraFormPgt" type="button" onclick="tirarFormPgt()" style="display: none">
                                                <i class="ace-icon fa fa-close bigger-120 red"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="contratoFornecedorDiv" style="display: none;">
                                @Html.HiddenFor(Function(f) f.IDTLCLEMPFRNITERSU, New With {.Class = "form-control", .id = "FornUnic"})
                                <div class="col-lg-12">
                                    @Html.Label("contratoFornecedor", "Fornecedor:")
                                </div>
                                <div class="col-lg-4">
                                    @Html.HiddenFor(Function(f) f.NUMCGCEMPFRN, New With {.id = "numCgcEmpFrnc"})
                                    @Html.HiddenFor(Function(f) f.IDTEMPFRNITERSU, New With {.id = "idtEmpFrnIIteRsu"})
                                    @Html.TextBoxFor(Function(f) f.IDTCONCGCEMPFRN, "", New With {.Class = "form-control", .readonly = True, .id = "cgcFornecedor", .placeholder = "CNPJ"})
                                </div>
                                <div class="col-lg-8">
                                    <div class="input-group" id="dividItem">
                                        @Html.TextBoxFor(Function(f) f.NOMRAZSOCEMPFRN, "", New With {.Class = "form-control", .id = "dropFornUnic", .placeholder = "Razão Social"})
                                        <span class="input-group-btn">
                                            <button class="btn btn-white" id="btFrnUnq" type="button" onclick="modalFornecedorUnico()" style="display: block">
                                                <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                            </button>
                                            <button class="btn btn-white" id="btTiraFrnUnq" type="button" onclick="tirarFornecedorUnico()" style="display: none">
                                                <i class="ace-icon fa fa-close bigger-120 red"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    @Html.Label("txtFormaUtlz", "Forma de Utilização:", New With {.id = "txtFormaUtlz"})
                                    @Html.TextAreaFor(Function(f) f.DESFRMUTZITEACOCMC, 4, 100, New With {.id = "FormaUtlz", .class = "form-control"})
                                    @Html.Label("lblErroForma", "Favor informar a forma de utilização.", New With {.id = "lblErroFormaUtlz", .style = "display: none;", .class = "text-danger-message-login"})
                                </div>
                            </div>
                            <div class="row" id="ObsComprador" style="display: none;" }>
                                <div class="col-lg-12">
                                    @Html.Label("txtObsComprador", "Observação ao comprador de suprimentos:", New With {.id = "txtObsComprador"})
                                    @Html.TextAreaFor(Function(f) f.DESOBSCPR, 4, 100, New With {.id = "formaUtilizacao", .Class = "form-control"})
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <button class="btn btn-info btn-white" id="btItensSubstitutos" style="display: none;" onclick="ItensSubstitutos(@Json.Encode(Model),@Model.AcaoComercial.CODACOCMC)">
                    <i class="ace-icon fa fa-plus bigger-120"></i>
                    Itens Substitutos
                </button>
                <button class="btn btn-info btn-white" id="btGasto" style="display: none;">
                    <i class="ace-icon fa fa-plus bigger-120"></i>
                    Gasto
                </button>
                <button id="btnSalvar" class="btn btn-success btn-white" onclick="Salvar()" style="display: block">
                    <i class="ace-icon fa fa-save bigger-120"></i>
                    Salvar
                </button>
                <button id="btnAlterar" class="btn btn-success btn-white" onclick="SalvarAlteracao()" style="display: none">
                    <i class="ace-icon fa fa-save bigger-120"></i>
                    Alterar
                </button>
                <a href="" data-dismiss="modal" class="btn btn-danger btn-white" onclick="cancelarIncluirItem()" @*onclick=" fecharmodalitens()"*@>
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Sair
                </a>
            </div>
        </div>
    </div>
</div>
@*<script src="~/Scripts/itensOrcamento.js"></script>*@


@section Scripts
    <script src="~/Scripts/itensOrcamento.js"></script>
End Section



