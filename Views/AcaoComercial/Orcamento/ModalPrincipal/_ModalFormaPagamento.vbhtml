﻿@ModelType GridSettings(Of ApTermsViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim AJAX_OBJECT_POST_FormaPagamento As New AjaxOptions() With {
        .UpdateTargetId = "resultadosFormPgt",
        .InsertionMode = InsertionMode.Replace,
        .HttpMethod = "POST",
        .OnBegin = "LoadingOn",
        .OnComplete = "LoadingOff_UpdateMessage",
        .OnSuccess = "atualizaMensagem('msgModalFormaPagamento')"
    }
End Code

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Forma de Pagamento
        </div>
        <div class="modal-body">
            <div id="msgModalFormaPagamento">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            @Using (Ajax.BeginForm("AtuCbTerPgt", "ItensOrcamento", AJAX_OBJECT_POST_FormaPagamento))
                @Html.AntiForgeryToken()
                @<div class="row">
                    <div class="col-xs-12">
                        @Html.Label("item", "Forma de Pagamento:")

                        <div class="input-group">
                            @Html.TextBoxFor(Function(m) m.Filter.DESCRIPTION, New With {.Class = "form-control", .id = "formPgt"})
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-white" type="submit" id="btnBuscFormPgt">
                                    <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            End Using
            <div id="resultadosFormPgt">
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Orcamento/ModalPrincipal/_ResultadoFormaPagamento.vbhtml", Model)
                End Code
            </div>
        </div>
        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" onclick="cancelarFormPgt()">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Sair
                </Button>
            </div>
        </div>
    </div>
</div>

