﻿@ModelType Tuple(Of AcaoComercialViewModel, RelacaoAcaoComercialPorItemOrcamentoViewModel)
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small widget-light-blue">
        <h4 class="widget-title smaller">
            Dados Da Ação
        </h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="ace-icon fa fa-chevron-up blue"></i>
            </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div class="row">
                <div class="col-lg-2 col-sm-6">
                    @Html.Label("acaoID", "Ação:")
                    @Html.TextBoxFor(Function(m) m.Item1.CODACOCMC, New With {.Class = "form-control", .readonly = "readonly", .id = "codAcao"})
                </div>
                <div class="col-lg-5 col-sm-6 ">
                    @Html.Label("acaoNome", "Nome da Ação:")
                    @Html.TextBoxFor(Function(m) m.Item1.NOMACOCMC, New With {.Class = "form-control", .readonly = "readonly"})
                </div>
                <div class="col-lg-5 col-sm-6">
                    @Html.Label("data", "Validade:")
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-daterange input-group">
                                @Html.TextBoxFor(Function(m) m.Item1.DATINIACOCMC, CDate(Model.Item1.DATINIACOCMC).ToShortDateString, New With {.class = "input-sm date-interval form-control", .readonly = "true", .autocomplete = "off"})
                                <span class="input-group-addon">
                                    à
                                </span>
                                @Html.TextBoxFor(Function(m) m.Item1.DATFIMACOCMC, CDate(Model.Item1.DATFIMACOCMC).ToShortDateString, New With {.class = "input-sm date-interval form-control", .readonly = "true", .autocomplete = "off"})
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9 col-sm-6">
                    @Html.Label("suprimentos", "Status Requisição de Suprimentos:")
                    <div class="input-group">
                        @Html.TextBoxFor(Function(m) m.Item2.DescDropSuprimentos, New With {.Class = "form-control", .readonly = "readonly", .id = "DescDropSuprimentos"})
                        <span class="input-group-btn">
                            <button class="btn btn-white btn-primary" id="btSuprimetntos" type="button" onclick="suprimentos()">
                                <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                Suprimentos
                            </button>
                        </span>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    @Html.Label("saldo", "Saldo da participação:")
                    @Html.TextBoxFor(Function(f) f.Item1.SaldoParticipacao, Nothing, New With {.Class = "form-control", .readonly = "readonly", .alt = "decimal_positivo_2"})
                </div>
            </div>
        </div>
    </div>
</div>