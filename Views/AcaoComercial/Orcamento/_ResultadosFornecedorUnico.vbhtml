﻿@ModelType GridSettings(Of PoVendorSitesAllViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 800px;">
                    <thead>
                        <tr role="row">
                            <th class="center">
                                Selecionar
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NUMCGCEMPFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NUMCGCEMPFRN", Model.Page.OrderBy, Model.ResultID)">
                                CGC
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("RAZSOCEMPFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("RAZSOCEMPFRN", Model.Page.OrderBy, Model.ResultID)">
                                Razão Social
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @code
                            For Each item In Model.ListPaged
                        End Code
                        <tr>
                            <td class="center">
                                <div>
                                    <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" onclick="selecionarFornecedorUnico(@item.NUMCGCEMPFRN,'@item.RAZSOCEMPFRN',@item.IDTLCLEMPFRN, @item.IDTEMPFRN)">
                                        <i class="ace-icon fa fa-check bigger-120"></i>
                                    </button>
                                </div>
                            </td>
                            <td>
                                @item.NUMCGCEMPFRN.ToString
                            </td>
                            <td>
                                @item.RAZSOCEMPFRN.ToString
                            </td>
                        </tr>
                        @code
                            Next
                        End Code
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />

            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code
        </div>
    </div>
</div>