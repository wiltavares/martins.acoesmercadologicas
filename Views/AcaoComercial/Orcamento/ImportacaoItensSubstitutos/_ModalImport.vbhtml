﻿@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim AJAX_OBJECT_POST_ItensSubs As New AjaxOptions() With {
        .UpdateTargetId = "modalResultImport",
        .InsertionMode = InsertionMode.Replace,
        .OnComplete = "RecarregaImportados",
        .HttpMethod = "POST"
    }
End Code

<div class="modal-dialog modal-md" id="modalErros">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-file-excel-o bigger-120"></i>
            Importar Planilha
        </div>
        @Using (Ajax.BeginForm("importarPlanilha", "ItensOrcamento", AJAX_OBJECT_POST_ItensSubs, New With {.enctype = "multipart/form-data", .action = "#", .id = "arquivo"}))
            @<div class="modal-body">
                <div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <label class="ace-file-input">
                                <input type="file" accept=".xls,.xlsx" name="file_upload" id="file_upload" class="ace-file-container" data-title="Choose" />
                                <span Class="ace-file-container" data-title="Choose">
                                    <span Class="ace-file-name" data-title="Sem arquivo ...">
                                        <i Class=" ace-icon fa fa-upload">
                                        </i>
                                    </span>
                                </span>
                                <a Class="remove" href="#">
                                    <i Class=" ace-icon fa fa-times">
                                    </i>
                                </a>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

                @<div Class="modal-footer clearfix">
                     <div Class="btn-group pull-right">
                         <Button type="submit" name="arquivo" id="arquivo" value="Importar" Class="btn btn-info btn-white">
                             <i Class="ace-icon fa fa-cloud-upload bigger-120"></i>
                             Importar
                         </Button>
                         <a href="@Url.Action("ItemSubstituto.xls", "TemplateXLS")" Class="btn btn-grey btn-white">
                             <i Class="ace-icon fa fa-cloud-download bigger-120"></i>
                             Template
                         </a>
                         <a onclick="cancelarImportacao()" class="btn btn-danger btn-white">
                             <i class="ace-icon fa fa-close bigger-120 red"></i>
                             Cancelar
                         </a>
                     </div>
                </div>
        End Using

    </div>
</div>
