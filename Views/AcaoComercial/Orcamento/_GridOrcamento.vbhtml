﻿@ModelType GridSettings(Of RelacaoAcaoComercialPorItemOrcamentoViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Html.Partial(Constantes.VIEW_MODAL_CONFIRMACAO, New Tuple(Of String, String, Object)("Delete", "ItensOrcamento", Model))
@code
    Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
    Dim IndPreAcordoVigencia As Boolean = (acaoCache.TIPACOMCD = TipoAcao.PreAcordoVigencia)
End Code
<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 3000px;">
        <thead>
            <tr role="row">
                <th>
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMER", Model.Page.OrderBy, Model.ResultID)">
                    Item de Orçamento
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODOPEFSCDSNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODOPEFSCDSNACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Tipo Operação
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VLRUNTITEOCDACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUNTITEOCDACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Vlr.Unt.
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("QDEITEOCDACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("QDEITEOCDACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Qnt.
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("ValorTotal", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("ValorTotal", Model.Page.OrderBy, Model.ResultID)">
                    Vlr.Tot.
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZOPEDSNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZOPEDSNACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Vlr. Utlz.
                </th>
                @If Not IndPreAcordoVigencia Then
                    @<th class="@Grid.AtribuirIconeOrdenacao("SaldoItem", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("SaldoItem", Model.Page.OrderBy, Model.ResultID)">
                        Saldo
                    </th>
                End If
                <th class="@Grid.AtribuirIconeOrdenacao("VLRPTCCLIITEACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRPTCCLIITEACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Vlr. Cliente
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VLRPTCFRNITEACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRPTCFRNITEACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Vlr. Fornecedor
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("QDEUTZOPEDSNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("QDEUTZOPEDSNACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Qnt. Utlz.
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DATUTZITEACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATUTZITEACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Dat.Utlz.
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODUNDESRNGCOPEDSN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODUNDESRNGCOPEDSN", Model.Page.OrderBy, Model.ResultID)">
                    BU
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODFILEMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFILEMP", Model.Page.OrderBy, Model.ResultID)">
                    Filial
                </th>
                <th>
                    Item Usado
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODFRNPCPMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRNPCPMER", Model.Page.OrderBy, Model.ResultID)">
                    Cód. Fornecedor
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODCENCST", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODCENCST", Model.Page.OrderBy, Model.ResultID)">
                    Centro de Custo
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("QDEDIACMPITEACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("QDEDIACMPITEACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Dias para a compra
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODIDTTSC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODIDTTSC", Model.Page.OrderBy, Model.ResultID)">
                    Cod Fil Cgc Cpf
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESCTTRSU", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESCTTRSU", Model.Page.OrderBy, Model.ResultID)">
                    Contrato de Fornecimento
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NUMLNHCTTRSU", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NUMLNHCTTRSU", Model.Page.OrderBy, Model.ResultID)">
                    Linha de Contrato
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESFRMUTZITEACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESFRMUTZITEACOCMC", Model.Page.OrderBy, Model.ResultID)" style="max-width: 200px;">
                    Forma de Utilizacação
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESOBSCPR", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESOBSCPR", Model.Page.OrderBy, Model.ResultID)">
                    Observação do Comprador
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr>
                    <td class="center">
                        <div class="hidden-sm hidden-xs btn-group">
                            @code
                            Dim visEditar = IIf(ViewBag.btnGravarVisible Is Nothing, True, ViewBag.btnGravarVisible)
                            End Code
                            <a data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="@ViewHelper.Visible(visEditar)" onclick="abrirModal('ItensOrcamento', 'EditarItem', @Json.Encode(item), undefined, SucessoAlterar, undefined, @Json.Encode(item))">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </a>
                            <a data-rel="tooltip" title="Deletar" class="btn btn-xs btn-danger" style="@ViewHelper.Visible(IIf(ViewBag.btnGravarVisible Is Nothing, True, ViewBag.btnGravarVisible))" data-toggle="modal" data-target="#confirmationDelete" data-id="@(item.CODACOCMC.ToString + ":" + item.NUMLNHITEOCDACOCMC.ToString)">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </a>
                            <a data-rel="tooltip" title="Visualizar" class="btn btn-xs btn-warning" style="@ViewHelper.Visible(Not visEditar)" onclick="abrirModal('ItensOrcamento', 'EditarItem', @Json.Encode(item), undefined, SucessoAlterar, undefined, @Json.Encode(item))">
                                <i class="ace-icon fa fa-eye bigger-120"></i>
                            </a>
                            <a data-rel="tooltip" title="Consulta Gasto" class="btn btn-xs btn-info" style="@ViewHelper.Visible(IIf(ViewBag.btnGasto, True, ViewBag.btnGasto))" onclick="abrirModal('ItensOrcamento', 'ModalConsultaGasto', @Json.Encode(item))">
                                <i class="ace-icon fa fa-usd bigger-120"></i>
                            </a>
                        </div>

                        <div class="hidden-md hidden-lg">
                            <div class="inline pos-rel">
                                <button class="btn btn-minier btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                    <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                </button>

                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                    <li>
                                        <a onclick="abrirModal('ItensOrcamento', 'EditarItem', @Json.Encode(item), undefined, SucessoAlterar, undefined, @Json.Encode(item))" style="@ViewHelper.Visible(IIf(ViewBag.btnGravarVisible Is Nothing, True, ViewBag.btnGravarVisible))" class="tooltip-info" data-rel="tooltip" title="" data-original-title="Editar">
                                            <span class="blue">
                                                <i class="ace-icon fa fa-pencil-square-o "></i>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-rel="tooltip" title="Deletar" class="tooltip-info" style="@ViewHelper.Visible(IIf(ViewBag.btnGravarVisible Is Nothing, True, ViewBag.btnGravarVisible))" data-toggle="modal" data-target="#confirmationDelete" data-id="@(item.CODACOCMC.ToString + ":" + item.NUMLNHITEOCDACOCMC.ToString)">
                                            <span class="red">
                                                <i class="ace-icon fa fa-trash-o"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-rel="tooltip" title="Visualizar" class="tooltip-info" style="@ViewHelper.Visible(IIf(Not ViewBag.btnGravarVisible Is Nothing, True, ViewBag.btnGravarVisible))" onclick="abrirModal('ItensOrcamento', 'EditarItem', @Json.Encode(item), undefined, SucessoAlterar, undefined, @Json.Encode(item))">
                                            <span class="yellow-color">
                                                <i class="ace-icon fa fa-eye"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-rel="tooltip" title="Consulta Gasto" class="tooltip-info" style="@ViewHelper.Visible(IIf(ViewBag.btnGasto, True, ViewBag.btnGasto))" onclick="abrirModal('ItensOrcamento', 'ModalConsultaGasto', @Json.Encode(item))">
                                            <span class="blue">
                                                <i class="ace-icon fa fa-usd"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                    <td>
                        @item.CODMER - @item.CODITEOCDACOCMC
                    </td>
                    <td>
                        @code
                            Dim operacao As String
                            operacao = item.CODOPEFSCDSNACOCMC.ToString + " - " + item.DESOPEFSCDSNACOCMC.ToString
                        End Code
                        @operacao
                    </td>
                    <td class="text-right">
                        @item.VLRUNTITEOCDACOCMC.ToString("#,##0.0000")
                    </td>
                    <td class="text-right">
                        @item.QDEITEOCDACOCMC
                    </td>
                    <td class="text-right">
                        @item.ValorTotal.ToString("#,##0.0000")
                    </td>                  
                    <td class="text-right">
                        @item.VLRUTZOPEDSNACOCMC.ToString("#,##0.0000")
                    </td>
                     @If Not IndPreAcordoVigencia Then
                         @<td class="text-right">
                             @item.SaldoItem.ToString("#,##0.0000")
                         </td>
                     End If
                    <td class="text-right">
                        @item.VLRPTCCLIITEACOCMC.ToString("#,##0.0000")
                    </td>
                    <td class="text-right">
                        @item.VLRPTCFRNITEACOCMC.ToString("#,##0.0000")
                    </td>
                    <td class="text-right">
                        @item.QDEUTZOPEDSNACOCMC
                    </td>
                    <td>
                        @code
                            Dim data As String

                            If (Not IsNothing(item.DATUTZITEACOCMC)) Then
                                data = CDate(item.DATUTZITEACOCMC).ToShortDateString()
                            Else
                                data = ""
                            End If
                        End Code
                        @data
                    </td>
                    <td>
                        @code
                            Dim bu As String = ""
                            If (Not IsNothing(item.CODUNDESRNGCOPEDSN) AndAlso item.CODUNDESRNGCOPEDSN <> 0) Then
                                bu = item.CODUNDESRNGCOPEDSN.ToString
                                If Not IsNothing(item.DESUNDESRNGC) Then
                                    bu = bu + " - " + item.DESUNDESRNGC.ToString
                                End If
                            End If
                        End Code
                        @bu
                    </td>
                    <td>
                        @code
                            Dim filial As String = ""
                            If (Not IsNothing(item.CODFILEMP) AndAlso item.CODFILEMP <> 0 AndAlso Not IsNothing(item.NOMFILEMP)) Then
                                filial = item.CODFILEMP.ToString + " - " + item.NOMFILEMP.ToString
                            End If
                        End Code
                        @filial
                    </td>
                    <td>
                        0
                    </td>
                    <td>
                        @item.CODFRNPCPMER
                    </td>
                    <td>
                        @item.CODCENCST
                    </td>
                    <td>
                        @item.QDEDIACMPITEACOCMC
                    </td>
                    <td>
                        @IIf(item.INDGRCREQAUTITERSU IsNot Nothing AndAlso item.INDGRCREQAUTITERSU = 1, item.NUMCGCEMPFRN, 0)
                    </td>
                    <td>
                        @IIf(Not IsNothing(item.DESCTTRSU), item.DESCTTRSU, "")
                    </td>
                    <td>
                        @item.NUMLNHCTTRSU
                    </td>
                    <td style="max-width: 700px;">
                        @item.DESFRMUTZITEACOCMC
                    </td>
                    <td>
                        @item.DESOBSCPR
                    </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />
@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code

<script>
    if (typeof AtualizarCanalDeVenda == 'function') {
        AtualizarCanalDeVenda();
    }
    if (typeof AtualizaValorFornecedorPreAcordo == 'function') {
        AtualizaValorFornecedorPreAcordo();
    }
    
</script>