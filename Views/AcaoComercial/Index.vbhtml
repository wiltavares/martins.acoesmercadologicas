﻿@ModelType GridSettings(Of AcaoComercialViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim action As String = ViewContext.RouteData.Values("action")
    Dim actionPesquisa As String = "Pesquisar"
    Dim ClassIcone As String = "fa fa-ban"
    Dim Title1 As String = String.Empty
    Dim Title2 As String = String.Empty
    Select Case action
        Case "Consulta", "ConsultaAcao"
            Title1 = "Ação"
            Title2 = "Consulta"
            ClassIcone = Constantes.Icone_Acao
            actionPesquisa = "Pesquisar"
        Case "MinhasAprovacoes"
            Title1 = "Ação"
            ClassIcone = Constantes.Icone_Acao
            Title2 = "Minhas Aprovações"
            actionPesquisa = "Pesquisar"
        Case "PreAcordoConsulta", "PreAcordoConsultar"
            Title1 = "Pré-Acordo"
            ClassIcone = Constantes.Icone_PreAcordo
            Title2 = "Consulta"
            actionPesquisa = "PreAcordoPesquisar"
        Case "PreAcordoMinhasAprovacoes"
            Title1 = "Pré-Acordo"
            ClassIcone = Constantes.Icone_PreAcordo
            Title2 = "Minhas Aprovações"
            actionPesquisa = "PreAcordoPesquisar"
        Case "Aprovacoes"
            ClassIcone = "fa fa-check"
            Title1 = "Ação/Pré-Acordo"
            Title2 = "Minhas Aprovações"
            actionPesquisa = "AprovacoesPesquisar"
    End Select
    ViewData("Title") = String.Format("{0} - {1}", Title1, Title2)
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            @Html.Raw(String.Format("<i class='{0}'></i>", ClassIcone))
            @Title1
        </li>
        <li class="active">
            @Title2
        </li>
    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

@* Div para modais *@
<div class="modal fade" id="modal"></div>

<div class="widget-box widget-color-blue2 @IIf(actionPesquisa = "AprovacoesPesquisar", "collapsed", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm(actionPesquisa, "AcaoComercial", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-1 col-sm-6">
                            @Html.Label("acaoID", String.Format("{0}:", Title1))
                            @Html.TextBoxFor(Function(m) m.Filter.CODACOCMC, New With {.Class = "form-control", .alt = "inteiro"})
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            @Html.Label("acaoNome", "Nome:")
                            @Html.TextBoxFor(Function(m) m.Filter.NOMACOCMC, New With {.Class = "form-control"})
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("status", "Status:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODSTAAPVACOCMC, New SelectList(ViewBag.listaStatus, "Value", "Text", Model.Filter.CODSTAAPVACOCMC), "Selecione...", New With {.Class = "chosen-select form-control"})
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("data", "Validade:")
                            @Html.HiddenFor(Function(f) f.Filter.DataIniAux, New With {.id = "dataIniAux"})
                            @Html.HiddenFor(Function(f) f.Filter.DataFimAux, New With {.id = "dataFimAux"})
                            <div class="input-daterange input-group">
                                @Html.TextBoxFor(Function(m) m.Filter.DATINIACOCMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataIni()", .id = "dataIni", .autocomplete = "off"})
                                <span class="input-group-addon">
                                    à
                                </span>
                                @Html.TextBoxFor(Function(m) m.Filter.DATFIMACOCMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataFim()", .id = "dataFim", .autocomplete = "off"})
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            @Html.Label("tipoEvento", "Tipo de Evento:")
                            @Html.DropDownListFor(Function(m) m.Filter.TIPEVTACOCMC, New SelectList(ViewBag.listaTipoEvento, "TIPEVTACOCMC", "CODIGO_NOME", Model.Filter.TIPEVTACOCMC), "Selecione...", New With {.Class = "chosen-select form-control"})
                        </div>
                        <div class="col-sm-4">
                            @Html.Label("evento", "Evento:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODEVTACOCMC, New SelectList(ViewBag.listaEventos, "CODEVTACOCMC", "CODIGO_NOME", Model.Filter.CODEVTACOCMC), "Selecione...", New With {.Class = "chosen-select form-control"})
                        </div>
                        <div class="col-sm-4">
                            @Html.Label("autor", "Autor:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODFNCCRIACOCMC, New SelectList(ViewBag.listaFuncionarios, "CODFNC", "CODIGO_NOME", Model.Filter.CODFNCCRIACOCMC), "Selecione...", New With {.Class = "chosen-select form-control"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white ">
                                    <i class="ace-icon fa fa-search bigger"></i>
                                    Pesquisar
                                </button>
                                <a href="@Request.Url.AbsoluteUri" class="btn btn-grey btn-white">
                                    <i Class="ace-icon fa fa-eraser bigger"></i>
                                    Limpar
                                </a>
                                <a Class="btn btn-white btn-warning" href="@Url.Action("Clonar", "Clone")">
                                    <i Class="ace-icon fa fa-clone bigger-120 light-orange"></i>
                                    Clonar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<br Class="clearfix" />

<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/AcaoComercial/_ResultadosView.vbhtml", Model)
    End Code
</div>
