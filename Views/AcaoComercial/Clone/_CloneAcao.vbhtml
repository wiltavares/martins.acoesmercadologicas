﻿@ModelType CloneViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Clonar"

    Dim ajaxClonar As New AjaxOptions() With {
            .UpdateTargetId = "msgGeral",
            .InsertionMode = InsertionMode.Replace,
            .OnBegin = "LoadingOn",
            .OnComplete = "SalvaCloneComplete",
            .HttpMethod = "POST"
    }

    
End Code

<script type="text/javascript">
    function SalvaCloneComplete(data) {
        atualizaMensagem();
        if (data.responseText.indexOf('window.location') === -1) {
            LoadingOff();
        }
    }
</script>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

@Using (Ajax.BeginForm("CriarClone", "Clone", ajaxClonar))
    @*@Using (Html.BeginForm("CriarClone", "Clone"))*@
    @Html.AntiForgeryToken()
    @<div class="form-horizontal" id="formAcao">
        <div class="widget-box widget-color-blue2">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller white lighter">
                    <i class="ace-icon fa fa-file-text-o bigger-110"></i>
                    Clonar
                </h4>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <div class="row">
                        <div class="col-sm-4">
                            @Html.Label("acaoID", "Cód. da Ação: (*)")
                            @Html.TextBoxFor(Function(Model) Model.CODACOCMC, New With {.Class = "form-control", .alt = "inteiro"})
                            @Html.ValidationMessageFor(Function(m) m.CODACOCMC, "", New With {.class = "text-danger-message-login"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-4">
                            @Html.CheckBoxFor(Function(Model) Model.INDITEMORCAMENTO)
                            @Html.Label("Itens de Orçamento", "Itens de Orçamento")
                        </div>
                        <div class="col-sm-4">
                            @Html.CheckBoxFor(Function(Model) Model.INDMECANICA)
                            @Html.Label("Mecânica", "Mecânica")
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button id="btnSalvar" value="CriarClone" type="submit" class="btn btn-success btn-white" onsubmit="LoadingOn()">
                                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                                    Salvar
                                </button>
                                <a></a>
                                <a href="@Url.Action("Index", "AcaoComercial")" class="btn btn-white btn-danger">
                                    <i class="ace-icon fa fa-times bigger-120 red"></i>
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

End Using
