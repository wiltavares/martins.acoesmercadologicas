﻿@ModelType List(Of PoliticasAcaoComercialViewModel)
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Políticas
        </div>
        <div class="modal-body">
            <Table Class="table table-striped table-bordered table-hover dataTable" role="grid">
                <thead>
                    <tr role="row">
                        <th class="center">@Html.Label("Políticas Divergentes")</th>
                    </tr>
                </thead>
                <tbody>
                    @For Each item In Model
                        @<tr>
                            <td>
                                @Html.Label("DESPOLACOCMC", item.DESPOLACOCMC)
                            </td>
                        </tr>
                    Next
                </tbody>
            </Table>
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <a href="" data-dismiss="modal" class="btn btn-success btn-white">
                    <i class="ace-icon fa fa-check-square-o bigger-120 green"></i>
                    OK
                </a>
            </div>
        </div>
    </div>
</div>
