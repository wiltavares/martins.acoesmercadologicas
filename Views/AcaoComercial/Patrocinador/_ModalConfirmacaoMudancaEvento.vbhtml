﻿@ModelType Tuple(Of String, String, Object)
@Imports Martins.AcoesMercadologicas.Core

<div class="modal modal-confirmation fade" id="confirmaMudancaEvento">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
                &nbsp;Confirmação de Mudança de Evento
            </div>
            <div class="modal-body">Ao mudar o evento todos os fornecedores e itens existentes na ação serão excuídos, deseja excluí-los?</div>
            <div class="modal-footer clearfix">
                <div class="btn-group pull-right">
                    <a href="javascript:confirmarMudancaEvento()" class="btn btn-success btn-white">
                        <i class="ace-icon fa fa-check-square-o bigger-120 green"></i>
                        Sim
                    </a>
                    <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                        <i class="ace-icon fa fa-square-o bigger-120 red"></i>
                        Não
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var idModal;

    $('#confirmationDelete').on('shown.bs.modal', function (e) {
        idModal = $(e.relatedTarget).attr('data-id');
    })

    function confirmarMudancaEvento() {
        $('#confirmationDelete').modal('hide');
        var dadosPesquisa = {Filter: @Html.Raw(Json.Encode(Model.Item3.Filter)), Page: @Html.Raw(Json.Encode(Model.Item3.Page))}
        LoadingOn();
        $.ajax({
            url: '@Url.Action(Model.Item1, Model.Item2)',
            type: 'POST',
            data: { id: idModal, dadosPesquisa: dadosPesquisa},
            success: function (data) {
                $('#' + '@Model.Item3.ResultID').html(data);
                LoadingOff();
            },
            error : function (){
                LoadingOff();
            }
        });
    }
</script>