﻿@ModelType AcaoComercialViewModel
@Imports Martins.AcoesMercadologicas.Core
@code
'Dim action As String = ViewContext.RouteData.Values("action")
'Dim IndPreAcordo As Boolean = False
'If action.Contains("PreAcordo") Then
'    IndPreAcordo = True
'End If
'Dim IndPreAcordo As Boolean = False
'IndPreAcordo = (Model.TIPACOMCD = 5 or Model.TIPACOMCD =6)
End Code
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small widget-light-blue">
        <h4 class="widget-title smaller">
            @IIf(Model.IndPreAcordo, "Fornecedor", "Patrocinador")
        </h4>
        <div class="widget-toolbar" >

            <div class="btn-group ">
                <a class="btn btn-md btn-success btn-white @ViewHelper.Enable(ViewBag.btnIsrPatrocinadorEnable)" style="@ViewHelper.Visible(ViewBag.btnIsrCarimboVisible)" id="btnCarimbo" onclick="abrirModal('Carimbo', 'InserirCarimbo', @Json.Encode(New RelacaoAcaoxFornecedorxCarimboViewModel))">
                    <i class="ace-icon fa fa-plus bigger-120"></i>
                    Incluir Carimbo
                </a>
                <a class="btn-md  btn btn-success btn-white @ViewHelper.Enable(ViewBag.btnIsrPatrocinadorEnable)" style="@ViewHelper.Visible(ViewBag.btnIsrFornecedorVisible)" id="btnFornecedor" onclick="abrirModalInserirFornecedor(@Json.Encode(New RelacaoAcaoComercialxFornecedoresViewModel))">
                    <div id="btnFrnIncluirFornecedor">
                        <i class="ace-icon fa fa-plus bigger-120"></i>
                        Incluir Fornecedor
                    </div>
                    <div id="btnFrnDespesasMartins" class="hide">
                        <i class="ace-icon fa fa-plus bigger-120"></i>
                        Despesas Martins
                    </div>
                </a>
                <a class="btn-md  btn btn-success btn-white @ViewHelper.Enable(ViewBag.btnIsrPatrocinadorEnable)" style="@ViewHelper.Visible(ViewBag.btnIsrBonificacaoVisible)" id="btnBonificacao" onclick="abrirModal('PedidoBonificacao', 'InserirPedidoBonificacao', @Json.Encode(Model.CODACOCMC))">
                    <i class="ace-icon fa fa-plus bigger-120"></i>
                    Incluir Pedido
                </a>
                <a class="btn-md  btn btn-success btn-white @ViewHelper.Enable(ViewBag.btnIsrPatrocinadorEnable)" style="@ViewHelper.Visible(ViewBag.btnIsrPremiacaoVisible)" id="btnPremiacao" onclick="abrirModal('PremiacaoExclusiva', 'InserirPremiacao', @Json.Encode(Model.CODACOCMC))">
                    <i class="ace-icon fa fa-plus bigger-120"></i>
                    Incluir Premiação
                </a>
                <a class="btn-md  btn btn-primary btn-white" style="@ViewHelper.Visible(ViewBag.btnIsrFornecedorVisible)" id="btnSaldoPlanoMkt" onclick="abrirModalSaldoPlanoMarketing()">
                    <i class="ace-icon fa fa-line-chart bigger-120"></i>
                    Saldo Plano Marketing
                </a>
            </div>
            <div class="btn-group ">
                <a href="#" data-action="collapse">
                    <i class="ace-icon fa fa-chevron-up blue"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @*@If (Not IndPreAcordo) Then*@
                
            <div class="row" style=@IIf(Not Model.IndPreAcordo, "display:block;", "display:none;")>
                <div class="col-sm-12">
                    <div class="row" id="pnlTipoPatrocinador">
                        @code
                            Dim tipoFrn As Decimal
                            If (Model.TIPACOMCD IsNot Nothing) Then
                                tipoFrn = Model.TIPACOMCD
                                'Model.IndPreAcordo = True
                            Else
                                tipoFrn = 1
                            End If
                        End Code
                        @Html.HiddenFor(Function(f) f.TIPACOMCD, New With {.Class = "form-control", .id = "valorTIPACOMCD"})
                        <div class="col-sm-3">

                            <input @IIf(ViewBag.pnlTipoPatrocinadorEnable, "", "disabled = 'disabled'") @IIf(tipoFrn = 1, "checked", "") id="rdbFornecedor" name="patrocinadorRdb" onchange="configuraPatrocinador(1)" type="radio" value="1">
                            @Html.Label("lblFornecedor", "Fornecedor")
                        </div>
                        <div class="col-sm-3">
                            <input @*@IIf(ViewBag.pnlTipoPatrocinadorEnable, "", "disabled = 'disabled'")*@ disabled="disabled" @IIf(tipoFrn = 2, "checked", "") id="rdbCarimbo" name="patrocinadorRdb" onchange="configuraPatrocinador(2)" type="radio" value="2">
                            @Html.Label("lblCarimbo", "Carimbo")
                        </div>
                        <div class="col-sm-3">
                            <input @IIf(ViewBag.pnlTipoPatrocinadorEnable, "", "disabled = 'disabled'") @IIf(tipoFrn = 3, "checked", "") id="rdbBonificacao" name="patrocinadorRdb" onchange="configuraPatrocinador(3)" type="radio" value="3">
                            @Html.Label("lblBonificacao", "Pedido Bnf.")
                        </div>
                        <div class="col-sm-3">
                            <input @IIf(ViewBag.pnlTipoPatrocinadorEnable, "", "disabled = 'disabled'") @IIf(tipoFrn = 4, "checked", "") id="rdbPremiacao" name="patrocinadorRdb" onchange="configuraPatrocinador(4)" type="radio" value="4">
                            @Html.Label("lblPremiacai", "Premiação Exclusiva")
                        </div>
                    </div>
                </div>
            </div>
            <br />
            @*End If*@
            <div id="gridFornecedores" style="@ViewHelper.Visible(ViewBag.btnIsrFornecedorVisible)">
                    @Code
                        Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridFornecedor.vbhtml", Model.StateGridFornecedores)
                    End Code
            </div>
            <div id="gridCarimbos" style="@ViewHelper.Visible(ViewBag.btnIsrCarimboVisible)">
                    @Code
                        Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimbo.vbhtml", Model.StateGridCarimbos)
                    End Code
            </div>
            <div id = "gridBonificacoes" style="@ViewHelper.Visible(ViewBag.btnIsrBonificacaoVisible)">
                    @Code
                        Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridBonificacao.vbhtml", Model.StateGridBonificacoes)
                    End Code
            </div>
            <div id="gridPremiacoes" style="@ViewHelper.Visible(ViewBag.btnIsrPremiacaoVisible)">
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/PremiacaoExclusiva/_GridPremiacao.vbhtml", Model.StateGridPremiacao)
                End Code
            </div>

        </div>
    </div>
</div>

<script>
    function abrirModalSaldoPlanoMarketing() {
        if ($('#dropTipoEvento').val() == "" || $('#dropEvento').val() == "") {
            $("#msgGeral").load(SiteUrl + 'Alert/_Alerts?msgWarning=DEFINA_PLANO_MARKETING', function () { });
        } else {
            abrirModal('AcaoComercial', 'ExibeSaldoPlanoMarketing', listForPlanoMkt());
        }
    };

    function abrirModalInserirFornecedor(objeto) {

        if ($('#dropTipoEvento').val() == "" || $('#dropEvento').val() == "") {
            $("#msgGeral").load(SiteUrl + 'Alert/_Alerts?msgWarning=PLANO_NAO_DEFINIDO', function () { });
        } else {
            abrirModal('Fornecedor', 'Inserirfornecedor', objeto);
        }
    };
</script>