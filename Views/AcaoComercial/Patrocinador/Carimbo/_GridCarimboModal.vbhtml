﻿@ModelType GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:1200px">
        <thead>
            <tr role="row">
                <th class="center">
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODMCOVBAFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMCOVBAFRN", Model.Page.OrderBy, Model.ResultID)">
                    Carimbo
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DATGRCMCOVBAFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATGRCMCOVBAFRN", Model.Page.OrderBy, Model.ResultID)">
                    Data
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy, Model.ResultID)">
                    Fornecedor
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VALOR_DISPONIVEL", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VALOR_DISPONIVEL", Model.Page.OrderBy, Model.ResultID)">
                    Vlr Disponível
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("OBSERVACAO", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("OBSERVACAO", Model.Page.OrderBy, Model.ResultID)">
                    Obs. Carimbo
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                Dim idCheckInc = "ckbIncCar" & item.CODMCOVBAFRN
                @<tr>
                    <td Class="center">
                        <input id="@idCheckInc" type="checkbox" value="false" />
                    </td>
                    <td>
                        @item.CODMCOVBAFRN
                    </td>
                    <td>
                        @item.DATGRCMCOVBAFRN.ToShortDateString()
                    </td>
                    <td>
                        @item.CODFRN - @item.NOMFRN
                    </td>
                     <td class="text-right">
                         @item.VALOR_DISPONIVEL.ToString("#,##0.00")
                     </td>
                    <td>
                        @item.OBSERVACAO
                    </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />

@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO5)
End Code
