﻿@ModelType GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)
@Imports Martins.AcoesMercadologicas.Core
@code
    Dim vlrTOTOCDACOCMC As Decimal = 0
End Code
<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:1800px">
        <thead>
            <tr role="row">
                <th>
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NOMFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFRN", Model.Page.OrderBy, Model.ResultID)">
                    Fornecedor
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODMCOVBAFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMCOVBAFRN", Model.Page.OrderBy, Model.ResultID)">
                    Carimbo
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Valor
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("OBSERVACAO", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("OBSERVACAO", Model.Page.OrderBy, Model.ResultID)">
                    Obs. Carimbo
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr>
                    <td class="center">
                        <div class="hidden-sm hidden-xs btn-group">
                            <a class="btn btn-xs btn-info" style="@ViewHelper.Visible(Iif(ViewBag.btnGravarVisible Is Nothing, true, ViewBag.btnGravarVisible))" onclick="abrirModal('Carimbo', 'EditarCarimbo', @item.CODMCOVBAFRN)">
                                <i class="ace-icon fa fa-pencil bigger-119"></i>
                            </a>
                            <a class="btn btn-xs btn-danger" style="@ViewHelper.Visible(Iif(ViewBag.btnGravarVisible is Nothing, true, ViewBag.btnGravarVisible ))" onclick="abrirModalSemLoading('Carimbo', 'ConfirmaRemoverCarimbo', @item.CODMCOVBAFRN)">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </a>
                        </div>

                        <div class="hidden-md hidden-lg">
                            <div class="inline pos-rel">
                                <button class="btn btn-minier btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                    <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                </button>

                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                    <li>
                                        <a onclick="abrirModal('Carimbo', 'EditarCarimbo', @item.CODMCOVBAFRN)" class="tooltip-info" data-rel="tooltip" title="Editar" data-original-title="Editar">
                                            <span class="blue">
                                                <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="abrirModalSemLoading('Carimbo', 'ConfirmaRemoverCarimbo', @item.CODMCOVBAFRN)" class="tooltip-info" data-rel="tooltip" title="Excluir" data-original-title="Excluir">
                                            <span class="red">
                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                    <td>
                        @item.CODFRN - @item.NOMFRN
                    </td>
                    <td>
                        @item.CODMCOVBAFRN
                    </td>
                    <td class="text-right">
                        @item.VLRUTZACOCMC.ToString("#,##0.00")
                        @code
                        vlrTOTOCDACOCMC += item.VLRUTZACOCMC
                        End Code
                    </td>
                    <td>
                        @item.OBSERVACAO
                    </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />
<div class="row">
    <div class="col-sm-3">
        Participação: @vlrTOTOCDACOCMC.ToString("#,##0.00")
    </div>
</div>
