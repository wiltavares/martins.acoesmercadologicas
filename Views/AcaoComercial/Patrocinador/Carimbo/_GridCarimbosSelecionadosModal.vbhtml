﻿@ModelType GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:800px">
        <thead>
            <tr role="row">
                <th class="center">
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODMCOVBAFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMCOVBAFRN", Model.Page.OrderBy, Model.ResultID)">
                    Carimbo
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NOMFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFRN", Model.Page.OrderBy, Model.ResultID)">
                    Fornecedor
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VALOR_DISPONIVEL", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VALOR_DISPONIVEL", Model.Page.OrderBy, Model.ResultID)">
                    Vlr Disponível
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Vlr Utilizado
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("OBSERVACAO", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("OBSERVACAO", Model.Page.OrderBy, Model.ResultID)">
                    Obs. Carimbo
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                Dim idCheckExc = "ckbExCar" & item.CODMCOVBAFRN
                Dim idTextValor = "txtValor" & item.CODMCOVBAFRN
                Dim idHiddenNum = "hdnCodCar" & item.CODMCOVBAFRN

                @<tr>
                    <td class="center">
                        <input id="@idCheckExc" type="checkbox" value="false" />
                    </td>
                    <td>
                        @item.CODMCOVBAFRN
                    </td>
                    <td>
                        @item.CODFRN - @item.NOMFRN
                    </td>
                    <td class="text-right">
                        @item.VALOR_DISPONIVEL.ToString("#,##0.00")
                    </td>
                    <td class="text-right">
                        @If item.CODACOCMC Is Nothing Then
                        Dim atualizaValorUtilizado = "atualizaValorUlitizadoCampoCarimbo('" & idHiddenNum & "','" & idTextValor & "')"
                            @Html.HiddenFor(Function(m) item.CODMCOVBAFRN, New With {.Class = "form-control", .id = idHiddenNum})
                            @Html.TextBoxFor(Function(m) item.VLRUTZACOCMC, "{0:0.00}", New With {.Class = "form-control", .id = idTextValor, .onchange = atualizaValorUtilizado, .alt = "decimal_positivo_2"})
                        Else
                            @*@item.VLRUTZACOCMC.ToString("#,##0.00")*@
                        Dim atualizaValorUtilizado = "atualizaValorUlitizadoCampoCarimbo('" & idHiddenNum & "','" & idTextValor & "')"
                            @Html.HiddenFor(Function(m) item.CODMCOVBAFRN, New With {.Class = "form-control", .id = idHiddenNum})
                            @Html.TextBoxFor(Function(m) item.VLRUTZACOCMC, "{0:0.00}", New With {.Class = "form-control", .id = idTextValor, .onchange = atualizaValorUtilizado, .alt = "decimal_positivo_2"})
                        End If
                    </td>
                    <td>
                        @item.OBSERVACAO
                    </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />

@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO5)
End Code
