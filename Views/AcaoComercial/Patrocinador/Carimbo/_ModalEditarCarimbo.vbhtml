﻿@ModelType RelacaoAcaoxFornecedorxCarimboViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim ajaxObject As New AjaxOptions() With {
            .UpdateTargetId = "gridCarimbos",
            .InsertionMode = InsertionMode.Replace,
            .OnBegin = "LoadingOn",
            .OnSuccess = "AlterarCarimboSuccess",
            .HttpMethod = "POST"
    }
End Code

@Using (Ajax.BeginForm("AlterarCarimbo", "Carimbo", ajaxObject))
    @Html.AntiForgeryToken()
    @<div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header ">
                <i class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Editar Carimbo
            </div>

            <div class="modal-body">

                <div id="msgModalCarimbo">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        @Html.Label("lblID", "Carimbo:")
                        @Html.TextBoxFor(Function(Model) Model.CODMCOVBAFRN, New With {.Class = "form-control", .readonly = "readonly"})
                    </div>
                    <div class="col-sm-2">
                        @Html.Label("lblID", "Cód.:")
                        @Html.TextBoxFor(Function(Model) Model.CODFRN, New With {.Class = "form-control", .readonly = "readonly"})
                    </div>
                    <div class="col-sm-4">
                        @Html.Label("lblNome", "Fornecedor:")
                        @Html.TextBoxFor(Function(Model) Model.NOMFRN, New With {.Class = "form-control", .readonly = "readonly"})
                    </div>
                    <div class="col-sm-2">
                        @Html.Label("lblValorDisp", "Valor Disponível:")
                        @Html.TextBoxFor(Function(Model) Model.VALOR_DISPONIVEL, "{0:0.00}", New With {.Class = "form-control", .readonly = "readonly", .alt = "decimal_positivo_2"})
                    </div>
                    <div class="col-sm-2">
                        @Html.Label("lblValor", "Valor Utilizado:")
                        @Html.TextBoxFor(Function(Model) Model.VLRUTZACOCMC, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        @Html.Label("lblObservacao", "Obs. Carimbo:")
                        @Html.TextAreaFor(Function(Model) Model.OBSERVACAO, New With {.Class = "form-control", .Row = "5", .readonly = "readonly"})
                    </div>
                </div>

                <div class="modal-footer clearfix">
                    <div class="btn-group pull-right">
                        <button type="submit" class="btn btn-success btn-white">
                            <i class="ace-icon fa fa-save bigger-120"></i>
                            Salvar
                        </button>
                        <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                            <i class="ace-icon fa fa-close bigger-120 red"></i>
                            Cancelar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
End Using
