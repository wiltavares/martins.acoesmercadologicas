﻿@ModelType RelacaoAcaoxFornecedorxCarimboViewModel
@Imports Martins.AcoesMercadologicas.Core

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Inserir Carimbo
        </div>
        <div class="modal-body">
            <div id="msgModalCarimbo">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            Buscar Carimbos
            <div class="legend-group-box form-horizontal">
                <div class="row">
                    <div class="col-sm-3">
                        @Html.DropDownListFor(Function(m) m.COD_FILTRO, New SelectList(ViewBag.filtroCarimbos, "VALUE", "TEXT", Model.COD_FILTRO), New With {.Class = "chosen-select form-control ", .id = "dropFiltroCarimbos", .onchange = "dropFiltroCarimbos_onChange($(this).val())"})
                    </div>
                    <div class="col-sm-7" id="divFiltroFornecedor" style="display:none;">
                        <div class="row">
                            <div class="col-xs-3">
                                @Html.TextBoxFor(Function(m) m.CODFRN, New With {.Class = "form-control", .id = "filtroCodFrn"})
                            </div>
                            <div class="col-xs-8">
                                <div class="row">
                                    @Html.DropDownListFor(Function(m) m.CODFRN, New SelectList(ViewBag.filtroFornecedorCarimbo, "CODFRN", "CodNomFrn", Model.CODFRN), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFiltroFornecedorCarimbo", .onchange = "dropFornecedorCarimbo_onChange()"})
                                </div>
                            </div>
                            <div class="col-xs-1">
                                <div class="btn-group pull-right">
                                    <a onclick="buscarFornecedores()" class="btn btn-sm btn-info" style="height:34px">
                                        <i class="ace-icon fa fa-search" style="vertical-align: middle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7" style="display:none; " id="divFiltroCelula">
                        <div class="row">
                            <div class="col-sm-12">
                                @Html.DropDownListFor(Function(m) m.CODDIVCMP, New SelectList(ViewBag.filtroCelulaCarimbo, "CODDIVCMP", "DESDIVCMP", Model.CODDIVCMP), New With {.Class = "chosen-select form-control", .id = "dropFiltroCelula"})
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7" id="divFiltroCarimbo">
                        <div class="row">
                            <div class="col-sm-6">
                                @Html.TextBoxFor(Function(m) m.CODMCOVBAFRN, New With {.Class = "form-control", .id = "filtroCodCarimbo"})
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="btn-group pull-right">
                            <a onclick="filtrarCarimbosInserir()" class="btn btn-info btn-white pull-right">
                                <i class="ace-icon fa fa-sliders bigger"></i>
                                Filtrar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title smaller">
                        Carimbos
                    </h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="gridCarimboModal">
                                    @Code
                                        Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimboModal.vbhtml", ViewBag.GridCarimboModal)
                                    End Code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title smaller">
                        Carimbos Selecionados
                    </h4>
                    <div class="widget-toolbar">
                        <div class="btn-group">
                            <a onclick="selecionarCarimbos()" class="btn btn-success btn-white">
                                <i class="ace-icon fa fa-plus bigger"></i>
                                Selecionar Carimbos
                            </a>
                            <a onclick="removerCarimbos()" class="btn btn-danger btn-white">
                                <i class="ace-icon fa fa-times bigger"></i>
                                Remover Carimbos
                            </a>
                        </div>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="gridCarimbosSelecionadosModal">
                                    @Code
                                        Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimbosSelecionadosModal.vbhtml", ViewBag.GridCarimbosSelecionados)
                                    End Code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <a onclick="concluirInsercaoCarimbos()" class="btn btn-success btn-white">
                    <i class="ace-icon fa fa-check-square-o bigger"></i>
                    Concluir
                </a>
                <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                    <i class="ace-icon fa fa-times bigger"></i>
                    Cancelar
                </a>
            </div>
        </div>
    </div>
</div>
