﻿@ModelType GridSettings(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel)
@Imports Martins.AcoesMercadologicas.Core
@code
    Dim vlrTOTOCDACOCMC As Decimal = 0
End Code
<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
            <tr role="row">
                <th>
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy, Model.ResultID)">
                    Fornecedor
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Valor
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESPRMEXV", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESPRMEXV", Model.Page.OrderBy, Model.ResultID)">
                    Descrição
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr>
                    <td class="center">
                        <div class="hidden-sm hidden-xs btn-group">
                            <a class="btn btn-xs btn-info" style="@ViewHelper.Visible(Iif(ViewBag.btnGravarVisible is Nothing, true, ViewBag.btnGravarVisible ))" onclick="abrirModal('PremiacaoExclusiva', 'EditarPremiacao', @item.CODFRN)">
                                <i class="ace-icon fa fa-pencil bigger-119"></i>
                            </a>
                            <a class="btn btn-xs btn-danger" style="@ViewHelper.Visible(Iif(ViewBag.btnGravarVisible is Nothing, true, ViewBag.btnGravarVisible ))" onclick="abrirModal('PremiacaoExclusiva', 'ConfirmaRemoverPremiacao', @item.CODFRN)">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </a>
                        </div>

                        <div class="hidden-md hidden-lg">
                            <div class="inline pos-rel">
                                <button class="btn btn-minier btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                    <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                </button>

                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                    <li>
                                        <a onclick="abrirModal('PremiacaoExclusiva', 'InserirPremiacao', @Json.Encode(item))" class="tooltip-info" data-rel="tooltip" title="" data-original-title="Editar">
                                            <span class="blue">
                                                <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                    <td>
                        @item.CODFRN - @item.NOMFRN
                    </td>
                    <td class="text-right">
                        @code
                        Dim valor As New Decimal
                        valor = item.VLRUTZACOCMC.Value
                        vlrTOTOCDACOCMC += item.VLRUTZACOCMC
                        End Code
                        @valor.ToString("#,##0.00")
                    </td>
                    <td>
                        @item.DESPRMEXV
                    </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />
<div class="row">
    <div class="col-sm-3">
        Participação: @vlrTOTOCDACOCMC.ToString("#,##0.00")
    </div>
</div>