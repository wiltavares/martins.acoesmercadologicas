﻿@ModelType  RelacaoAcaoComercialxPremiacaoExclusivaViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim AJAX_OBJECT_POST_PremiacaoExclusiva As New AjaxOptions() With {
        .UpdateTargetId = "gridPremiacoes",
        .InsertionMode = InsertionMode.Replace,
        .HttpMethod = "POST",
        .OnSuccess = "concluirInsercaoPremiacao",
        .OnFailure = "atualizaModalPremiacaoExclusiva"
    }
End Code

<div class="modal-dialog modal-md">
    @Using (Ajax.BeginForm("ValidarPremiacao", "PremiacaoExclusiva", AJAX_OBJECT_POST_PremiacaoExclusiva))
        @<div class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;@IIf(Model.Novo.GetValueOrDefault(), "Incluir Premiação Exclusiva", "Alterar Premiação Exclusiva")
            </div>
            <div Class="modal-body">
                <div id="msgModalPremiacao">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div Class="row">
                    @Html.HiddenFor(Function(m) m.Novo)
                    <div Class="col-lg-12 col-sm-12" id="divFiltroFornecedor">
                        @Html.Label("fornecedor", "Fornecedor (*):")
                        <div Class="row">
                            <div Class="col-xs-3">
                                @Html.TextBox("txtCodFrn", Model.CODFRN, New With {.Class = "form-control", .id = "filtroCodFrn"}.EnableIf(Model.Novo))
                            </div>
                            <div Class="col-xs-7">
                                <div Class="row">
                                    @Html.DropDownList("drp", New SelectList(ViewBag.Fornecedor, "CODFRN", "CodNomFrn", Model.CODFRN), "Selecione...", New With {.Class = "chosen-select form-control ", .id = "dropFiltroFornecedores", .onchange = "dropFornecedorPremiacao_onChange()"}.EnableIf(Model.Novo))
                                    @Html.TextBoxFor(Function(m) m.CODFRN, New With {.id = "hdfCodFornecedor", .hidden = "hidden"})
                                </div>
                            </div>
                            <div Class="col-xs-2">
                                <div Class="btn-group pull-right">
                                    <a onclick="buscarFornecedoresPremiacaoExclusiva()" Class="btn btn-sm btn-info" style="height:34px">
                                        <i Class="ace-icon fa fa-search" style="vertical-align: middle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div Class="row">
                            <div Class="col-xs-12">
                                @Html.ValidationMessageFor(Function(m) m.CODFRN, "", New With {.class = "text-danger-message-login"})
                                @Html.ValidationMessageFor(Function(m) m.NOMFRN, "", New With {.class = "text-danger-message-login"})
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div Class="col-lg-3 col-sm-12">
                        @Html.Label("valor", "Valor (*):")
                        @Html.TextBoxFor(Function(f) f.VLRUTZACOCMC, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                    </div>
                </div>
                <div class="row">
                    <div Class="col-sm-12">
                        @Html.ValidationMessageFor(Function(m) m.VLRUTZACOCMC, "", New With {.class = "text-danger-message-login"})
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        @Html.Label("descpremiacao", "Descrição da Premiação (*):")
                        @Html.TextAreaFor(Function(f) f.DESPRMEXV, New With {.Class = "form-control"})
                        @Html.ValidationMessageFor(Function(m) m.DESPRMEXV, "", New With {.class = "text-danger-message-login"})
                    </div>
                </div>
            </div>
            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    <button type="submit" id="btnIncluirPremiacao" class="btn btn-success btn-white">
                        <i class="ace-icon fa @IIf(Model.Novo, "fa-plus", "fa-save") bigger"></i>
                        @IIf(Model.Novo, "Incluir", "Alterar")
                    </button>
                    <a href="" data-dismiss="modal" Class="btn btn-danger btn-white">
                        <i Class="ace-icon fa fa-times bigger"></i>
                        Cancelar
                    </a>
                </div>
            </div>
        </div>
    End Using
</div>
