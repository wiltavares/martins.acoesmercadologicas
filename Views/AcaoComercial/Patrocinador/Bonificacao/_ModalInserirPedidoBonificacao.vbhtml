﻿@ModelType RelacaoAcaoComercialXPedidoBonificadoViewModel
@Imports Martins.AcoesMercadologicas.Core

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Inserir Pedido
        </div>
        <div class="modal-body">
            <div id="msgModalBonificacao">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div class="legend-group-box">
                <div class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-3">
                            @Html.DropDownListFor(Function(m) m.COD_FILTRO, New SelectList(ViewBag.filtroPedidos, "VALUE", "TEXT", Model.COD_FILTRO), New With {.Class = "chosen-select form-control ", .id = "dropFiltroPedidos", .onchange = "dropFiltroPedidos_onChange($(this).val())"})
                        </div>
                        <div class="col-sm-7" id="divFiltroFornecedor">
                            <div class="row">
                                <div class="col-xs-3">
                                    @Html.TextBoxFor(Function(m) m.CODFRN, New With {.Class = "form-control", .id = "filtroCodFrn"})
                                </div>
                                <div class="col-xs-8">
                                    <div class="row">
                                        @Html.DropDownListFor(Function(m) m.CODFRN, New SelectList(ViewBag.filtroFornecedorBonificacao, "CODFRN", "CodNomFrn", Model.CODFRN), "Selecione...", New With {.Class = "chosen-select form-control ", .id = "dropFiltroFornecedores", .onchange = "dropFornecedorBonificacao_onChange()"})
                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <div class="btn-group pull-right">
                                        <a onclick="buscarFornecedoresPedidoBonificacao()" class="btn btn-sm btn-info" style="height:34px">
                                            <i class="ace-icon fa fa-search" style="vertical-align: middle"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7" style="display:none; " id="divFiltroPedido">
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.TextBoxFor(Function(m) m.NUMPEDCMP, New With {.Class = "form-control", .id = "filtroNumPed"})
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="btn-group pull-right">
                                <a onclick="filtrarPedidosDeCompra()" class="btn btn-info btn-white pull-right">
                                    <i class="ace-icon fa fa-sliders bigger"></i>
                                    Filtrar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title smaller">
                        Pedidos de Compra
                    </h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="gridPedidosCompraModal">
                                    @Code
                                        Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridPedidosCompraModal.vbhtml", ViewBag.GridPedidosCompra)
                                    End Code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title smaller">
                        Pedidos Selecionados
                    </h4>
                    <div class="widget-toolbar">
                        <div class="btn-group">
                            <a onclick="selecionarPedidos()" class="btn btn-success btn-white">
                                <i class="ace-icon fa fa-plus bigger"></i>
                                Selecionar Pedidos
                            </a>
                            <a onclick="removerPedidos()" class="btn btn-danger btn-white">
                                <i class="ace-icon fa fa-times bigger"></i>
                                Remover Pedidos
                            </a>
                        </div>
                    </div>
                </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="gridPedidosBonificacaoModal">
                                @Code
                                    Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridBonificacaoModal.vbhtml", ViewBag.GridPedidosBonificacao)
                                End Code
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal-footer clearfix">
        <div class="btn-group pull-right">
            <a onclick="concluirInsercaoPedidos()" class="btn btn-success btn-white">
                <i class="ace-icon fa fa-check-square-o bigger"></i>
                Concluir
            </a>
            <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                <i class="ace-icon fa fa-times bigger"></i>
                Cancelar
            </a>
        </div>
    </div>
</div>
</div>
