﻿@ModelType GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)
@Imports Martins.AcoesMercadologicas.Core
@code
    Dim vlrTOTOCDACOCMC As Decimal = 0
End Code
<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
            <tr role="row">
                <th class="center">
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NUMPEDCMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NUMPEDCMP", Model.Page.OrderBy, Model.ResultID)">
                    Pedido
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy, Model.ResultID)">
                    Fornecedor
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESOBSPEDCMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESOBSPEDCMP", Model.Page.OrderBy, Model.ResultID)">
                    Obs. Pedido
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Valor Utilizado
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr>
                    <td Class="center">
                        <div Class="btn-group">
                            <a class="btn btn-xs btn-danger" style="@ViewHelper.Visible(Iif(ViewBag.btnGravarVisible is Nothing, true, ViewBag.btnGravarVisible ))" onclick="abrirModalSemLoading('PedidoBonificacao', 'ConfirmaRemoverPedido', @item.NUMPEDCMP)">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </a>
                        </div>
                    </td>
                    <td>
                        @item.NUMPEDCMP
                    </td>
                    <td>
                        @item.CODFRN - @item.NOMFRN
                    </td>
                    <td>
                        @item.DESOBSPEDCMP
                    </td>
                     <td class="text-right">
                         @item.VLRUTZACOCMC.ToString("#,##0.00")
                         @code
                         vlrTOTOCDACOCMC += item.VLRUTZACOCMC
                         End Code
                     </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />
<div class="row">
    <div class="col-sm-3">
        Participação: @vlrTOTOCDACOCMC.ToString("#,##0.00")
    </div>
</div>
