﻿@ModelType GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1000px;">
        <thead>
            <tr role="row">
                <th class="center">
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NUMPEDCMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NUMPEDCMP", Model.Page.OrderBy, Model.ResultID)">
                    Pedido
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy, Model.ResultID)">
                    Fornecedor
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VERBA_DISPONIVEL", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VERBA_DISPONIVEL", Model.Page.OrderBy, Model.ResultID)">
                    Vlr Disponível
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Vlr Utilizado
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESOBSPEDCMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESOBSPEDCMP", Model.Page.OrderBy, Model.ResultID)">
                    Obs. Pedido
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                Dim idCheckExc = "ckbExPed" & item.NUMPEDCMP
                Dim idTextValor = "txtValor" & item.NUMPEDCMP
                Dim idHiddenNum = "hdnNumPed" & item.NUMPEDCMP

                @<tr>
                    <td Class="center">
                        <input id="@idCheckExc" type="checkbox" value="false"/>
                    </td>
                    <td>
                        @item.NUMPEDCMP
                    </td>
                    <td>
                        @item.CODFRN - @item.NOMFRN
                    </td>
                     <td class="text-right">
                         @item.VERBA_DISPONIVEL.ToString("#,##0.00")
                     </td>
                     <td class="text-right">
                         @If item.CODACOCMC Is Nothing Then
                         Dim atualizaValorUtilizado = "atualizaValorUlitizadoCampo('" & idHiddenNum & "','" & idTextValor & "')"
                             @Html.HiddenFor(Function(m) item.NUMPEDCMP, New With {.Class = "form-control", .id = idHiddenNum})
                             @Html.TextBoxFor(Function(m) item.VLRUTZACOCMC, "{0:0.00}", New With {.Class = "form-control", .id = idTextValor, .onchange = atualizaValorUtilizado, .alt = "decimal_positivo_2"})
                         Else
                             @item.VLRUTZACOCMC.ToString("#,##0.00")
                         End If
                     </td>

                     <td>
                         @item.DESOBSPEDCMP
                     </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />

@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO5)
End Code
