﻿@Code

End Code


<div class="modal-dialog" role="document">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header ">
                <i class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Rateio de Saldo
            </div>


            <div class="modal-body">
                <div id="msgModalRateioMsg"></div>
                <div class="row">
                    <div class="col-sm-12">
                        @Html.Label("lblCriterioParaRateio", "Critério para rateio")
                        <div class="row">
                            <div class="col-sm-12" id="divRatVlrNotFix">
                                <input  type="radio" id="rbtProporcional" name="rbtRatVlr" />
                                @Html.Label("rbtProporcional", "Proporcional ao saldo do fornecedor")
                            </div>
                            <div class="col-sm-12" id="divRatVlrFix">
                                <input type="radio" id="rbtFixo"  name="rbtRatVlr" />
                                @Html.Label("rbtFixo", "Este valor para cada fornecedor")
                            </div>
                            <div class="col-sm-12" id="divRatDivQuant">
                                <input type="radio" id="rbtDivQuant" checked="checked"  name="rbtRatVlr" />
                                @Html.Label("rbtDivQuant", "Dividido o valor entre os fornecedores")
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        @Html.Label("CalRateio", "Valor para rateio:")
                        <input type="text" id="CalRateio" class="form-control" alt="decimal_positivo_2" />
                    </div>
                </div>

            </div>

            <div Class="modal-footer clearfix">

                <div Class="btn-group pull-right">
                    <Button Class="btn btn-success btn-white" onclick="CalculaRateio()">
                        <i Class="ace-icon fa fa-save bigger-120"></i>
                        Salvar
                    </Button>
                    <Button Class="btn btn-danger btn-white" onclick="FecharRateio()">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Cancelar
                    </Button>

                </div>
            </div>
        </div>
    </div>
</div>
