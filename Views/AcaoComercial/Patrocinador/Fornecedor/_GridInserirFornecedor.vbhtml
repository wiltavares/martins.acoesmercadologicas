﻿@ModelType GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1000px;">
        <thead>
            <tr role="row">
                <th class="center">
                    Selecionar
                </th>
                <th class="center">
                    Ação
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODUNDESRNGC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODUNDESRNGC", Model.Page.OrderBy, Model.ResultID)">
                    BU
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy, Model.ResultID)">
                    Fornecedor
                </th>
                @*<th class="@Grid.AtribuirIconeOrdenacao("TIPDSNDSCBNF", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("TIPDSNDSCBNF", Model.Page.OrderBy, Model.ResultID)">
                        Empenho Acordo
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("VLRSLDDSNCTTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRSLDDSNCTTACOCMC", Model.Page.OrderBy, Model.ResultID)">
                        Vr Saldo Acordo
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZCTTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZCTTACOCMC", Model.Page.OrderBy, Model.ResultID)">
                        Vr Acordo
                    </th>*@
                <th class="@Grid.AtribuirIconeOrdenacao("VLRSLDDSNDSCACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRSLDDSNDSCACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Saldo Exa Acordo
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZEXACTTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZEXACTTACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Vr Exa Acordo
                </th>
                @*<th class="@Grid.AtribuirIconeOrdenacao("VLRSLDDSNACR", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRSLDDSNACR", Model.Page.OrderBy, Model.ResultID)">
                        Saldo Receita CMV
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("VLRUTZRCTCSTMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZRCTCSTMER", Model.Page.OrderBy, Model.ResultID)">
                        Vr Receita CMV
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("VLRPTCFRNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRPTCFRNACOCMC", Model.Page.OrderBy, Model.ResultID)">
                        Vr Participação
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("VLRTXAADMFRNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRTXAADMFRNACOCMC", Model.Page.OrderBy, Model.ResultID)">
                        Taxa Adm
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("VLRTXAADMBDEACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRTXAADMBDEACOCMC", Model.Page.OrderBy, Model.ResultID)">
                        Taxa Brinde
                    </th>*@
                <th class="@Grid.AtribuirIconeOrdenacao("DESBFCFRNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESBFCFRNACOCMC", Model.Page.OrderBy, Model.ResultID)">
                    Benefício
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                Dim idCheck = "ckbFornecedor" & item.CODFRN
                @<tr class="@item.CorTipoSaldoFrn">
                    <td class="center">
                        @*<input id="@idCheck" type="checkbox" value="false" />*@
                        <input type="checkbox" id="chkSelecionar" name="Selecionar" value="@Json.Encode(item.CODFRN)">
                    </td>
                    <td class="center">
                        <div class="hidden-sm hidden-xs btn-group">
                            <a class="btn btn-xs btn-info" onclick="exibeDadosFornecedor(@Json.Encode(item.CODFRN), @Json.Encode(item.NOMFRN))">
                                <i class="ace-icon fa fa-pencil bigger-119"></i>
                            </a>
                        </div>
                        @*<div class="hidden-sm hidden-xs btn-group">
                                <button class="btn btn-blue" id="btBscIte" type="button" onclick="modalAssociaBeneficioFornecedor()" style="display: block">
                                    <i class="ace-icon glyphicon glyphicon-modal-window bigger-119"></i>
                                </button>
                            </div>*@
                        <div class="hidden-md hidden-lg">
                            <div class="inline pos-rel">
                                <button class="btn btn-minier btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                    <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                </button>

                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                    <li>
                                        <a onclick="abrirModal('AcaoComercial', 'EditarFornecedor', @Json.Encode(item.CODFRN))" class="tooltip-info" data-rel="tooltip" title="" data-original-title="Editar">
                                            <span class="blue">
                                                <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                    <td>
                        @item.CODUNDESRNGC - @item.DESUNDESRNGC
                    </td>
                    <td>
                        @item.CODFRN - @item.NOMFRN
                    </td>
                    @*
                        <td>
                            @item.TIPDSNDSCBNF - @item.DESDSNDSCBNF
                        </td>
                        <td>
                            @item.VLRSLDDSNCTTACOCMC
                        </td>
                        <td>
                            @item.VLRUTZCTTACOCMC
                        </td>
                    *@
                    <td>
                        @item.VLRSLDDSNDSCACOCMC.GetValueOrDefault().ToString("#,##0.00")
                    </td>
                    <td>
                        @item.VLRUTZEXACTTACOCMC.GetValueOrDefault().ToString("#,##0.00")
                    </td>
                    @*
                        <td>
                            @item.VLRSLDDSNACR
                        </td>
                        <td>
                            @item.VLRUTZRCTCSTMER
                        </td>
                        <td>
                            @item.VLRPTCFRNACOCMC
                        </td>
                        <td>
                            @item.VLRTXAADMFRNACOCMC
                        </td>
                        <td>
                            @item.VLRTXAADMBDEACOCMC
                        </td>
                    *@
                    <td>
                        @item.DESBFCFRNACOCMC
                    </td>
                </tr>
            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />

@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code
