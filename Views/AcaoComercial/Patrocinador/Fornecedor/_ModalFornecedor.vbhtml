﻿@ModelType RelacaoAcaoComercialxFornecedoresViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim ajaxObject As New AjaxOptions() With {
            .UpdateTargetId = "gridFornecedores",
            .InsertionMode = InsertionMode.Replace,
            .OnBegin = "LoadingOn",
            .OnSuccess = "AlterarFornecedorSuccess",
            .HttpMethod = "POST"
    }
End Code

@Using (Ajax.BeginForm("AlterarFornecedor", "Fornecedor", ajaxObject))
    @Html.AntiForgeryToken()
    @<div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header ">
                <i class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Editar Fornecedor
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-3 col-sm-3" style="@ViewHelper.Visible(ViewBag.INDEVTSMA)">
                        @code
                            Dim htmlAtr As New Dictionary(Of String, Object)
                            htmlAtr.Add("class", "form-control chosen-select")
                            htmlAtr.Add("id", "dropTipoFornecedor")
                            htmlAtr.Add("readonly", "readonly")
                            htmlAtr.Add("disabled", "disabled")
                        End Code

                        @Html.Label("txtTipoFornecedor", "Tipo Fornecedor:")
                        @Html.DropDownListFor(Function(Model) Model.TIPFRNACOCMC, New SelectList(ViewBag.ListaTipoFornecedor, "VALUE", "TEXT", Model.TIPFRNACOCMC), htmlAtr)
                    </div>

                    <div class="col-lg-3 col-sm-3" style="@ViewHelper.Visible(ViewBag.INDEVTSMA AndAlso Model.IndPreAcordo)">
                        @code
                            If (Model.IndPreAcordo = True AndAlso IsNothing(Model.PERPTCFRNACOCMC)) Then
                                Model.PERPTCFRNACOCMC = 10000
                            End If
                        End Code

                        @Html.Label("txtPercentualParticipacao", "Percentual Participação (%):")
                        @Html.TextBoxFor(Function(Model) Model.PERPTCFRNACOCMC, "{0:0.00}", New With {.Class = "form-control", .id = "textPercentualParticipacao", .alt = "porcentagem"})
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        @Html.Label("fornecedorID", "Cód.:")
                        @Html.TextBoxFor(Function(Model) Model.CODFRN, New With {.Class = "form-control", .readonly = "readonly"})
                    </div>
                    <div class="col-sm-6">
                        @Html.Label("fornecedorNome", "Nome Fornecedor:")
                        @Html.TextBoxFor(Function(Model) Model.NOMFRN, New With {.Class = "form-control", .readonly = "readonly"})
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        @Html.Label("txUnddeNegocioBU", "Und. de Negócio (BU):")
                        @code
                            Dim htmlAtt As New Dictionary(Of String, Object)
                            htmlAtt.Add("class", "form-control chosen-select")
                            htmlAtt.Add("id", "dropBus")
                            If (Model.CODFRN.GetValueOrDefault() <> Constantes.DESPESAS_MARTINS) Then
                                htmlAtt.Add("disabled", "disabled")
                            Else
                                htmlAtt.Add("onchange", "bu_onchange()")
                            End If
                        End Code
                        @Html.DropDownListFor(Function(model) model.CODUNDESRNGC, New SelectList(ViewBag.listaBU, "CODUNDESRNGC", "DESUNDESRNGC", Model.CODUNDESRNGC), htmlAtt)
                        @Html.HiddenFor(Function(model) model.DESUNDESRNGC, New With {.id = "hdfDESUNDESRNGC"})
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        @Html.Label("vrSaldoExtra", "Saldo Extra Acordo:")
                        @Html.TextBoxFor(Function(Model) Model.VLRSLDDSNDSCACOCMC, "{0:0.00}", New With {.Class = "form-control", .readonly = "readonly", .alt = "decimal_positivo_2"})
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        @Html.Label("vrExtraAcordo", "Valor Extra Acordo:")
                        @Html.TextBoxFor(Function(Model) Model.VLRUTZEXACTTACOCMC, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                    </div>


                </div>

                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        @Html.Label("empenhoExtraAcordo", "Empenho Extra Acordo:")
                        @Html.TextBoxFor(Function(Model) Model.TIPDES, New With {.Class = "form-control", .readonly = "readonly"})
                    </div>
                    @*<div class="col-lg-4 col-sm-6">
                            @Html.Label("vrSaldoReceitaCMV", "Saldo Receita CMV:")
                            @Html.TextBoxFor(Function(Model) Model.VLRSLDDSNACR, "{0:0.00}", New With {.Class = "form-control", .readonly = "readonly", .alt = "decimal_positivo_2"})
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            @Html.Label("vrReceitaCMV", "Valor Receita CMV:")
                            @Html.TextBoxFor(Function(Model) Model.VLRUTZRCTCSTMER, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                        </div>*@
                </div>

                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        @Html.Label("txAdministracao", "Benefício do Fornecedor (*):")
                        @Html.TextAreaFor(Function(model) model.DESBFCFRNACOCMC, New With {.Class = "form-control"})
                        @Html.ValidationMessageFor(Function(m) m.DESBFCFRNACOCMC, "", New With {.class = "text-danger-message-login"})
                    </div>
                </div>
                <br />
            </div>
            <div class="modal-footer clearfix">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-success btn-white">
                        <i class="ace-icon fa fa-save bigger-120"></i>
                        Salvar
                    </button>
                    <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                        <i class="ace-icon fa fa-close bigger-120 red"></i>
                        Cancelar
                    </a>
                </div>
            </div>
        </div>
    </div>
                            End Using

<script>
    $('#textPercentualParticipacao').on('keyup', function (event) {
        
        if (event.target.value.replace(',', '.') > 100)
            return event.target.value = '100,00';
    });
</script>
