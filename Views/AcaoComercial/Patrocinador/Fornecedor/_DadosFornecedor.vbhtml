﻿@ModelType RelacaoAcaoComercialxFornecedoresViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim ajaxObject As New AjaxOptions() With {
            .UpdateTargetId = "gridInserirFornecedor",
            .InsertionMode = InsertionMode.Replace,
            .OnBegin = "LoadingOn",
            .OnSuccess = "AlterarFornecedorModalSuccess",
            .HttpMethod = "POST"
    }
End Code

@Using (Ajax.BeginForm("AlterarFornecedorModal", "Fornecedor", ajaxObject))
    @Html.AntiForgeryToken()
    @<div Class="row">
        <div Class="col-sm-12">
            <div Class="row">
                <div Class="col-sm-4">
                    @Html.Label("codFnr", "Cod.:")
                    @Html.TextBoxFor(Function(Model) Model.CODFRN, New With {.Class = "form-control", .readonly = "readonly", .alt = "inteiro"})
                </div>
                <div Class="col-sm-6">
                    @Html.Label("nmFrn", "Nome do Fornecedor:")
                    @Html.TextBoxFor(Function(Model) Model.NOMFRN, New With {.Class = "form-control", .readonly = "readonly"})
                </div>
                <div Class="col-sm-2">
                    <br />
                    <Button type="submit" Class="btn btn-success btn-white pull-right">
                        <i Class="ace-icon fa fa-save bigger-120"></i>
                        Alterar
                    </Button>
                </div>
            </div>
            <div Class="row">
                <div class="col-lg-4 col-sm-6">
                    @Html.Label("txUnddeNegocioBU", "Und. de Negócio (BU):")
                    @code
                        Dim htmlAtt As New Dictionary(Of String, Object)
                        htmlAtt.Add("class", "form-control chosen-select")
                        htmlAtt.Add("id", "dropBus")
                        If (Model.CODFRN.GetValueOrDefault() <> Constantes.DESPESAS_MARTINS) Then
                            htmlAtt.Add("disabled", "disabled")
                        Else
                            htmlAtt.Add("onchange", "bu_onchange()")
                        End If
                    End Code
                    @Html.DropDownListFor(Function(model) model.CODUNDESRNGC, New SelectList(ViewBag.listaBU, "CODUNDESRNGC", "DESUNDESRNGC", Model.CODUNDESRNGC), htmlAtt)
                    @Html.HiddenFor(Function(model) model.DESUNDESRNGC, New With {.id = "hdfDESUNDESRNGC"})
                </div>
                <div Class="col-lg-4 col-sm-6">
                    @Html.Label("vrSaldoExtra", "Saldo Extra Acordo:")
                    @Html.TextBoxFor(Function(Model) Model.VLRSLDDSNDSCACOCMC, "{0:0.00}", New With {.Class = "form-control", .readonly = "readonly", .alt = "decimal_positivo_2"})
                </div>
                <div Class="col-lg-4 col-sm-6">
                    @Html.Label("vrExtraAcordo", "Valor Extra Acordo:")
                    @Html.TextBoxFor(Function(Model) Model.VLRUTZEXACTTACOCMC, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                </div>
                @*<div Class="col-lg-4 col-sm-6">
                        @Html.Label("SaldoAcordo", "Saldo Acordo:")
                        @Html.TextBoxFor(Function(Model) Model.VLRSLDCNTCRRFRN, "{0:0.00}", New With {.Class = "form-control", .readonly = "readonly", .alt = "decimal_positivo_2"})
                    </div>
                    <div Class="col-lg-4 col-sm-6">
                        @Html.Label("vrAcordo", "Valor Acordo:")
                        @Html.TextBoxFor(Function(Model) Model.VLRUTZCTTACOCMC, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                    </div>*@
            </div>
            <div Class="row">
                @*<div class="col-lg-4 col-sm-6">
                        @Html.Label("EmpenhodeAcordo", "Empenho de Acordo:")
                        @Html.DropDownListFor(Function(model) model.TIPDSNDSCBNF, New SelectList(ViewBag.listaEmpenho, "TIPDSNDSCBNF", "DESDSNDSCBNF", Model.TIPDSNDSCBNF), "Selecione...", New With {.Class = "form-control chosen-select", .disabled = "disabled"})
                    </div>*@

            </div>
            <div Class="row">
                <div class="col-lg-4 col-sm-6">
                    @Html.Label("empenhoExtraAcordo", "Empenho Extra Acordo:")
                    @Html.TextBoxFor(Function(Model) Model.TIPDES, New With {.Class = "form-control", .readonly = "readonly"})
                </div>
                @*<div Class="col-lg-4 col-sm-6">
            @Html.Label("vrSaldoReceitaCMV", "Saldo Receita CMV:")
            @Html.TextBoxFor(Function(Model) Model.VLRSLDDSNACR, "{0:0.00}", New With {.Class = "form-control", .readonly = "readonly", .alt = "decimal_positivo_2"})
        </div>
        <div Class="col-lg-4 col-sm-6">
            @Html.Label("vrReceitaCMV", "Valor Receita CMV")
            @Html.TextBoxFor(Function(Model) Model.VLRUTZRCTCSTMER, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
        </div>*@
            </div>
            <div Class="row">
                @*<div Class="col-lg-4 col-sm-6">
                        @Html.Label("txVlrParticipacao:", "Valor da Participação:")
                        @Html.TextBoxFor(Function(Model) Model.VLRPTCFRNACOCMC, "{0:0.00}", New With {.Class = "form-control", .readonly = "readonly", .alt = "decimal_positivo_2"})
                    </div>
                    <div Class="col-lg-4 col-sm-6">
                        @Html.Label("txAdministracao", "Taxa de Administração:")
                        @Html.TextBoxFor(Function(Model) Model.VLRTXAADMFRNACOCMC, "{0:0.00}", New With {.Class = "form-control", .readonly = "readonly", .alt = "decimal_positivo_2"})
                    </div>*@
                @*<div Class="col-lg-4 col-sm-6">
                        @Html.Label("txBrinde", "Taxa de Brinde:")
                        @Html.TextBoxFor(Function(Model) Model.VLRTXAADMBDEACOCMC, "{0:0.00}", New With {.Class = "form-control", .alt = "decimal_positivo_2"})
                    </div>*@
            </div>
            <div Class="row">
                <div Class="col-lg-12 col-sm-12">
                    @Html.Label("txAdministracao", "Benefício do Fornecedor:")
                    @Html.TextAreaFor(Function(model) model.DESBFCFRNACOCMC, New With {.Class = "form-control", .rows = 2, .id = "BeneficioDadosFornecedor"})
                </div>
            </div>
        </div>
    </div>
                        End Using
