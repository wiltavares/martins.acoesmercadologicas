﻿@ModelType RelacaoAcaoComercialxFornecedoresViewModel
@Imports Martins.AcoesMercadologicas.Core

@Code

End Code
<div class="modal fade" id="modal"></div>

<div id="msgModalInserirFornecedor">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div Class="modal-dialog modal-lg">
    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Associa Beneficio Fornecedor
        </div>
        <div Class="modal-body">
            <div Class="form-horizontal">
                Associa Beneficio Fornecedor
                <div Class="legend-group-box form-horizontal">
                    <div Class="row">
                        <div class="row">
                            <div class="col-xs-3">
                                @Html.Label("lblFornecedor", "Fornecedor :")
                            </div>
                            <div class="col-xs-3">
                                @Html.TextBoxFor(Function(Model) Model.CODFRN, New With {.Class = "form-control", .id = "CodFornecedor"})
                            </div>
                            <div class="col-xs-3">
                                @Html.TextBoxFor(Function(Model) Model.NOMFRN, New With {.Class = "form-control", .id = "NomFornecedor"})
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="gridInserirFornecedor">
                                    @Code
                                        Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridAssociaBeneficioFornecedor.vbhtml", ViewBag.gridAssociaBeneficioFornecedores)
                                    End Code
                                </div>
                            </div>
                        </div>
                    </div>
                    <div Class="row">
                        <div class="row">
                            <div class="col-xs-3">
                                @Html.Label("lblVlrTotal", "Valor Total :")
                            </div>
                            <div class="col-xs-3">
                                @Html.Label("lblResultado", "0,00")
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
    </div>
    <div Class="modal-footer clearfix">
        <div Class="btn-group pull-right">
            <Button Class="btn btn-success btn-white">
                <i Class="ace-icon fa fa-save bigger-120"></i>
                Incluir
            </Button>
            <a href="" data-dismiss="modal" Class="btn btn-danger btn-white">
                <i Class="ace-icon fa fa-close bigger-120 red"></i>
                Fechar
            </a>
        </div>
    </div>
</div>
