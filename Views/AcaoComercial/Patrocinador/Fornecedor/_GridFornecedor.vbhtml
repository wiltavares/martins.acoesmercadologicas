﻿@ModelType GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel)
@Imports Martins.AcoesMercadologicas.Core
@code
    Dim vlrPTCFRNACOCMC As Decimal = 0
    Dim vlrSaldoReceitaCMV As Decimal = 0
    Dim vlrTOTTXAADMACOCMC As Decimal = 0
    Dim vlrTOTBDEACOCMC As Decimal = 0

End Code
<div style="width: auto; overflow-x: scroll;">
    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:1100px">
        <thead>
            <tr role="row">
                <th>
                </th>
                <th>
                    BU
                </th>
                <th>
                    Fornecedor
                </th>

                @*<th class="@Grid.AtribuirIconeOrdenacao("TIPDSNDSCBNF", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("TIPDSNDSCBNF", Model.Page.OrderBy, Model.ResultID)">
            Empenho Acordo
        </th>*@

                @*<th class="@Grid.AtribuirIconeOrdenacao("VLRSLDDSNCTTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRSLDDSNCTTACOCMC", Model.Page.OrderBy, Model.ResultID)">
            Vr Saldo Acordo
        </th>*@

                @*<th class="@Grid.AtribuirIconeOrdenacao("VLRUTZCTTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZCTTACOCMC", Model.Page.OrderBy, Model.ResultID)">
            Vr Acordo
        </th>*@

                <th>
                    Saldo Exa Acordo
                </th>
                <th>
                    Vr Exa Acordo
                </th>
                <th>
                    Tipo Fornecedor
                </th>
                <th>
                    Percentual Participação
                </th>

                @*<th class="@Grid.AtribuirIconeOrdenacao("VLRSLDDSNACR", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRSLDDSNACR", Model.Page.OrderBy, Model.ResultID)">
            Saldo Receita CMV
        </th>*@

                @*<th class="@Grid.AtribuirIconeOrdenacao("VLRUTZRCTCSTMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRUTZRCTCSTMER", Model.Page.OrderBy, Model.ResultID)">
            Vr Receita CMV
        </th>*@

                @*<th class="@Grid.AtribuirIconeOrdenacao("VLRPTCFRNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRPTCFRNACOCMC", Model.Page.OrderBy, Model.ResultID)">
            Vr Participação
        </th>*@

                @*<th class="@Grid.AtribuirIconeOrdenacao("VLRTXAADMFRNACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRTXAADMFRNACOCMC", Model.Page.OrderBy, Model.ResultID)">
            Taxa Adm
        </th>*@

                @*<th class="@Grid.AtribuirIconeOrdenacao("VLRTXAADMBDEACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRTXAADMBDEACOCMC", Model.Page.OrderBy, Model.ResultID)">
            Taxa Brinde
        </th>*@

                <th>
                    Benefício
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @code
                    vlrPTCFRNACOCMC += item.VLRPTCFRNACOCMC

                    If (item.VLRUTZRCTCSTMER <> 0) Then
                        vlrSaldoReceitaCMV += item.VLRUTZRCTCSTMER
                    End If

                    If (item.VLRTXAADMFRNACOCMC <> 0) Then
                        vlrTOTTXAADMACOCMC += item.VLRTXAADMFRNACOCMC
                    End If

                End Code
                @<tr>
                    <td class="center">
                        <div class="hidden-sm hidden-xs btn-group">
                            <a class="btn btn-xs btn-info" style="@ViewHelper.Visible(IIf(ViewBag.btnGravarVisible Is Nothing, True, ViewBag.btnGravarVisible))" onclick="abrirModal('Fornecedor', 'EditarFornecedor', @Json.Encode(item.CODFRN))">
                                <i class="ace-icon fa fa-pencil bigger-119"></i>
                            </a>
                            <a class="btn btn-xs btn-danger" style="@ViewHelper.Visible(IIf(ViewBag.btnGravarVisible Is Nothing, True, ViewBag.btnGravarVisible))" onclick="abrirModalSemLoading('Fornecedor', 'ConfirmaRemoverFornecedor', @Json.Encode(item.CODFRN))">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </a>
                        </div>

                        <div class="hidden-md hidden-lg">
                            <div class="inline pos-rel">
                                <button class="btn btn-minier btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                    <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                </button>

                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                    <li>
                                        <a onclick="abrirModal('Fornecedor', 'EditarFornecedor', @Json.Encode(item.CODFRN))" class="tooltip-info" data-rel="tooltip" title="Editar" data-original-title="Editar">
                                            <span class="blue">
                                                <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="abrirModalSemLoading('Fornecedor', 'ConfirmaRemoverFornecedor', @Json.Encode(item.CODFRN))" class="tooltip-info" data-rel="tooltip" title="Excluir" data-original-title="Excluir">
                                            <span class="red">
                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                    <td>
                        @item.CODUNDESRNGC - @item.DESUNDESRNGC
                    </td>
                    <td>
                        @item.CODFRN - @item.NOMFRN
                    </td>

                    @code
                        Dim valor As New Decimal
                        valor = 0
                    End Code


                    @*<td>
                            @item.TIPDSNDSCBNF - @item.DESDSNDSCBNF
                        </td>
                        <td class="text-right">
                            @code
                            Dim valor As New Decimal
                            If (IsNothing(item.VLRSLDDSNCTTACOCMC)) Then
                            valor = 0
                            Else
                            valor = item.VLRSLDDSNCTTACOCMC
                            End If
                            End Code
                            @valor.ToString("#,##0.00")
                        </td>
                        <td class="text-right">
                            @code
                            If (IsNothing(item.VLRUTZCTTACOCMC)) Then
                            valor = 0
                            Else
                            valor = item.VLRUTZCTTACOCMC
                            End If
                            End Code
                            @valor.ToString("#,##0.00")
                        </td>*@

                    <td class="text-right">
                        @code
                            If (IsNothing(item.VLRSLDDSNDSCACOCMC)) Then
                                valor = 0
                            Else
                                valor = item.VLRSLDDSNDSCACOCMC
                            End If
                        End Code
                        @valor.ToString("#,##0.00")
                    </td>
                    <td class="text-right">
                        @code
                            If (IsNothing(item.VLRUTZEXACTTACOCMC)) Then
                                valor = 0
                            Else
                                valor = item.VLRUTZEXACTTACOCMC
                            End If
                        End Code
                        @valor.ToString("#,##0.00")
                    </td>

                    <td class="text-right">
                        @code
                            Dim TipFornecedor As String
                            If (IsNothing(item.TIPFRNACOCMC)) Then
                                TipFornecedor = ""
                            ElseIf (item.TIPFRNACOCMC = 0) Then
                                TipFornecedor = "Martins"
                            ElseIf (item.TIPFRNACOCMC = 1) Then
                                TipFornecedor = "EDL"
                            End If
                        End Code
                        @TipFornecedor
                    </td>
                    <td class="text-right">
                        @item.PERPTCFRNACOCMC%
                    </td>

                    @*<td class="text-right">
                            @code
                            If (IsNothing(item.VLRSLDDSNACR)) Then
                            valor = 0
                            Else
                            valor = item.VLRSLDDSNACR
                            End If
                            End Code
                            @valor.ToString("#,##0.00")
                        </td>
                        <td class="text-right">
                            @code
                            If (IsNothing(item.VLRUTZRCTCSTMER)) Then
                            valor = 0
                            Else
                            valor = item.VLRUTZRCTCSTMER
                            vlrSaldoReceitaCMV += item.VLRUTZRCTCSTMER
                            End If
                            End Code
                            @valor.ToString("#,##0.00")
                        </td>

                        <td class="text-right">
                            @code
                            If (IsNothing(item.VLRPTCFRNACOCMC)) Then
                            valor = 0
                            Else
                            valor = item.VLRPTCFRNACOCMC
                            vlrPTCFRNACOCMC += item.VLRPTCFRNACOCMC
                            End If
                            End Code
                            @valor.ToString("#,##0.00")
                        </td>*@

                    @*<td class="text-right">
                            @code
                            If (IsNothing(item.VLRTXAADMFRNACOCMC)) Then
                            valor = 0
                            Else
                            valor = item.VLRTXAADMFRNACOCMC
                            vlrTOTTXAADMACOCMC += item.VLRTXAADMFRNACOCMC
                            End If
                            End Code
                            @valor.ToString("#,##0.00")
                        </td>*@

                    @*<td class="text-right">
                            @code
                            If (IsNothing(item.VLRTXAADMBDEACOCMC)) Then
                            valor = 0
                            Else
                            valor = item.VLRTXAADMBDEACOCMC
                            vlrTOTBDEACOCMC += item.VLRTXAADMBDEACOCMC
                            End If
                            End Code
                            @valor.ToString("#,##0.00")
                        </td>*@

                    <td>
                        @item.DESBFCFRNACOCMC
                    </td>
                </tr>
                            Next
        </tbody>
    </table>
</div>
<br class="clearfix" />
<div class="row">
    <div class="col-sm-3">
        Participação: @vlrPTCFRNACOCMC.ToString("#,##0.00")
    </div>
    @*<div id="lblTaxasESaldosFornecedor">
            <div class="col-sm-3">
                Sla. Receita CMV: @vlrSaldoReceitaCMV.ToString("#,##0.00")
            </div>
            <div class="col-sm-3">
                Tx. Administração: @vlrTOTTXAADMACOCMC.ToString("#,##0.00")
            </div>
             <div class="col-sm-3">
                    Tx. Brinde: @vlrTOTBDEACOCMC.ToString("#,##0.00")
                </div>
        </div>*@
</div>

@*@Code
        Html.RenderPartial(Constantes.VIEW_PAGINACAO)
    End Code*@
