﻿@ModelType RelacaoAcaoComercialxFornecedoresViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim EsconderGridsVisible = ("display: none;")
    Dim EscondeMsgRateioisible = ("display: none;")
    Dim EsconderDropFornecedoresEdl = ("display: none;")
End Code

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Inserir Fornecedor
        </div>
        <div id="msgModalInserirFornecedor">
            @Html.Partial(Constantes.VIEW_ALERTAS)
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-lg-3 col-sm-3" style="@ViewHelper.Visible(ViewBag.INDEVTSMA)">
                    @code
                        Dim htmlAtt As New Dictionary(Of String, Object)
                        htmlAtt.Add("class", "form-control chosen-select")
                        htmlAtt.Add("id", "dropTipoFornecedor")
                        htmlAtt.Add("onchange", "changeTipoFornecedor(this)")
                        If (Model.IndPreAcordo = True) Then
                            htmlAtt.Add("disabled", "disabled")
                        End If
                    End Code

                    @Html.Label("txtTipoFornecedor", "Tipo Fornecedor:")
                    @Html.DropDownListFor(Function(Model) Model.TIPFRNACOCMC, New SelectList(ViewBag.ListaTipoFornecedor, "VALUE", "TEXT", Model.TIPFRNACOCMC), htmlAtt)
                </div>

                <div class="col-lg-3 col-sm-3" style="@ViewHelper.Visible(ViewBag.INDEVTSMA AndAlso Model.IndPreAcordo)">
                    @code
                        If (Model.IndPreAcordo = True) Then
                            Model.PERPTCFRNACOCMC = 10000
                        End If
                    End Code

                    @Html.Label("txtPercentualParticipacao", "Percentual Participação (%):")
                    @Html.TextBoxFor(Function(Model) Model.PERPTCFRNACOCMC, New With {.Class = "form-control", .id = "textPercentualParticipacao", .alt = "porcentagem"})
                </div>
            </div>
            <br />
            <div class="form-horizontal">
                Buscar Fornecedores
                <div class="legend-group-box form-horizontal">
                    @Html.HiddenFor(Function(Model) Model.EmpenhoExaFrn, New With {.Class = "form-control", .id = "sEmpenhoExaFrn"})

                    <div class="row">
                        <div class="col-xs-3 col-sm-3" id="testeDropFornecedor">
                            @Html.DropDownList("dropFornecedor", DirectCast(ViewBag.listaFornecedor, IEnumerable(Of SelectListItem)), New With {.Class = "form-control chosen-select", .id = "dropFornecedor", .onchange = "carregarDescricaoDivisaoDeComprar_DiretoriaCompraPorFornecedor($(this).val())"})
                        </div>
                        <div class="col-xs-8 col-sm-8" id="divFiltroFornecedor">
                            <div class="row">
                                <div class="col-xs-3" id="txtCodFornecedor">
                                    @Html.TextBoxFor(Function(Model) Model.CODFRN, New With {.Class = "form-control", .id = "CodFornecedor"})
                                </div>
                                <div class="col-xs-8 col-sm-8" id="txtNmFornecedor" >
                                    @Html.DropDownListFor(Function(Model) Model.CODFRN, New SelectList(ViewBag.ListaDropFornecedor, "CODFRN", "CodNomFrn", Model.CODFRN), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFornecedores", .onchange = "fornecedores_onchange()"})
                                </div>
                                <div class="col-xs-8 col-sm-8" id="txtNmFornecedorEdl" style="@EsconderDropFornecedoresEdl">
                                    @Html.DropDownListFor(Function(Model) Model.CODFRN, New SelectList(ViewBag.ListaDropFornecedorEdl, "CODFRN", "CodNomFrn", Model.CODFRN), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFornecedoresEdl", .onchange = "fornecedoresEdl_onchange()"})
                                </div>
                                <div class="col-xs-1">
                                    <div class="btn-group" id="divPesquisarFornecedor">
                                        <a class="btn btn-info btn-sm" onclick="pesquisarFornecedor()" style="height:34px">
                                            <i class="ace-icon fa fa-search bigger"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-8" style="@EsconderGridsVisible" id="DivDescricaoDivisaoDeCompra">
                            @Html.DropDownListFor(Function(Model) Model.CODDIVCMP, New SelectList(ViewBag.ListaRelacaoEquipeDeComprasxGerenteNegocioServCompras, "CODDIVCMP", "DESDIVCMP", Model.CODDIVCMP), New With {.Class = "chosen-select form-control", .id = "codCelula"})
                        </div>
                        <div class="col-xs-8 col-sm-8" style="@EsconderGridsVisible" id="DivDiretoriaCompra">
                            @Html.DropDownListFor(Function(Model) Model.CODDRTCMP, New SelectList(ViewBag.ListaDiretoriaCompra, "CODDRTCMP", "DESDRTCMP", Model.CODDRTCMP), New With {.Class = "chosen-select form-control", .id = "codDiretoria"})
                        </div>
                        <div class="col-sm-1">
                            <div class="btn-group pull-right">
                                <a class="btn btn-sm btn-success" onclick="adicionarFornecedorGridView()" style="height:34px">
                                    <span class="ace-icon fa fa-plus icon-only" style="padding-top:4px"></span>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title smaller">
                        Fornecedores Selecionados
                    </h4>
                    <div class="widget-toolbar">
                        <div class="btn-group">
                            <a class="btn btn-sm btn-info" onclick="modalDefTipoRateio()" style="height:34px">
                                <span class="ace-icon fa fa-plus bigger-120" style="padding-top:4px"></span>
                                Definir Rateio
                            </a>
                            <a class="btn btn-sm btn-danger" onclick="removerFornecedoresModal()" style="height:34px">
                                <span class="ace-icon fa fa-times icon-only" style="padding-top:4px"></span>
                                Excluir Selecionados
                            </a>
                            @*<button data-toggle="dropdown" class="btn btn-danger dropdown-toggle" aria-expanded="false" style="height:34px">
                            <span class="ace-icon fa fa-caret-down icon-only"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-info dropdown-menu-left">
                            <li>
                                <a onclick="confirmaRemocao('semDados')">Sem Dados</a>
                            </li>
                            <li>
                                <a onclick="confirmaRemocao('semSaldoAcordo')">Sem Saldo de Acordo</a>
                            </li>
                            <li>
                                <a onclick="confirmaRemocao('semSaldoExtraAcordo')">Sem Saldo de Extra Acordo</a>
                            </li>
                            <li>
                                <a onclick="confirmaRemocao('semSaldoAcordoExtra')">Sem Saldo de Acordo e Extra Acordo</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a onclick="confirmaRemocao('selecionados')">Selecionados</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a onclick="confirmaRemocao('todos')">Todos</a>
                            </li>
                        </ul>*@
                        </div>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="legend-group-box">
                            <div class="row">
                                <div class="col-sm-3" style="padding-bottom:3px">
                                    Legenda:
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="legend-box gray-bkg">
                                    </div>
                                    <div style="padding-top:3px">
                                        Sem Dados
                                    </div>
                                </div>
                                @*<div class="col-sm-3">
                                <div class="legend-box green-bkg">
                                </div>
                                <div style="padding-top:3px">
                                    Sem Saldo Acordo
                                </div>
                            </div>*@

                                <div class="col-sm-3">
                                    <div class="legend-box green-bkg">
                                    </div>
                                    <div style="padding-top:3px">
                                        Com Saldo Extra Acordo
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="legend-box red-bkg">
                                    </div>
                                    <div style="padding-top:3px">
                                        Sem Saldo Extra Acordo
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="legend-box orange-bkg">
                                    </div>
                                    <div style="padding-top:3px">
                                        Saldo Insuficiente
                                    </div>
                                </div>
                                @*<div class="col-sm-4">
                                <div class="legend-box red-bkg">
                                </div>
                                <div style="padding-top:3px">
                                    Sem Saldo Acordo e Extra Acordo
                                </div>
                            </div>*@
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="gridInserirFornecedor">
                                    @Code
                                        Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridInserirFornecedor.vbhtml", ViewBag.gridFornecedores)
                                    End Code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box widget-color-blue2" id="widget-box-13">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title lighter"></h4>

                    <div class="widget-toolbar" style="float: left !important">
                        <ul class="nav nav-tabs" id="myTab2">
                            <li class="active">
                                <a data-toggle="tab" href="#home2" aria-expanded="true">Dados Do Fornecedor</a>
                            </li>

                            @*<li class="">
                            <a data-toggle="tab" href="#profile2" aria-expanded="false">Dados para rateio</a>
                        </li>*@

                            <li class="">
                                <a data-toggle="tab" href="#info2" aria-expanded="false">Insere Beneficio Genérico</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="tab-content no-padding">
                            <div id="home2" class="tab-pane active">
                                <div id="DadosFornecedor" class="scroll-content">
                                    @Code
                                        Html.RenderPartial("~/Views/AcaoComercial/Patrocinador/Fornecedor/_DadosFornecedor.vbhtml", Model)
                                    End Code
                                </div>
                            </div>
                            <div id="info2" class="tab-pane">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right">
                                            <button class="btn btn-success btn-white" onclick="inserirBeneficioParaOsfornecedores()">
                                                <i class="ace-icon fa fa-plus bigger-120"></i>
                                                Inserir Beneficio
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="scroll-content">
                                            @Html.TextAreaFor(Function(model) model.DESBFCFRNACOCMC, New With {.Class = "form-control", .id = "BeneficioGenerico"})
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer clearfix">

            <div class="btn-group pull-right">
                <button class="btn btn-success btn-white" onclick="concluirInsercaoFornecedor()">
                    <i class="ace-icon fa fa-save bigger-120"></i>
                    Salvar
                </button>
                <a href="" data-dismiss="modal" class="btn btn-danger btn-white">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </a>
            </div>
        </div>
    </div>
</div>

<div  id="frmRateio" class="modal fade" tabindex="-1" role="dialog">
</div>
