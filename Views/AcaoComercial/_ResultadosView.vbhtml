﻿@ModelType GridSettings(Of AcaoComercialViewModel)
@Imports Martins.AcoesMercadologicas.Core
@code
    Dim ActionNameCreateEdit As String = "CreateEdit"
End Code
<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1500px;">
                    <thead>
                        <tr role="row">
                            <th class="center"></th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODACOCMC", Model.Page.OrderBy)">
                                Código
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMACOCMC", Model.Page.OrderBy)">
                                Nome
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODSTAAPVACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODSTAAPVACOCMC", Model.Page.OrderBy)">
                                Status
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DATGRCACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATGRCACOCMC", Model.Page.OrderBy)">
                                Geração
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DATINIACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATINIACOCMC", Model.Page.OrderBy)">
                                Início
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DATFIMACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATFIMACOCMC", Model.Page.OrderBy)">
                                Fim
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLRTOTOCDACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRTOTOCDACOCMC", Model.Page.OrderBy)">
                                Valor Orçado
                            </th>
                            @*<th class="@Grid.AtribuirIconeOrdenacao("VLRTOTTXAADMACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRTOTTXAADMACOCMC", Model.Page.OrderBy)">
                                Vl. Tx. Adm.
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLRTOTBDEACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRTOTBDEACOCMC", Model.Page.OrderBy)">
                                Vl. Tx. Brinde
                            </th>*@
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMFNC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFNC", Model.Page.OrderBy)">
                                Autor
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DATAPVACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATAPVACOCMC", Model.Page.OrderBy)">
                                Aprovação
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODACOCMCORI", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODACOCMCORI", Model.Page.OrderBy)">
                                Original
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @code
                            Select Case item.TIPACOMCD
                                Case TipoAcao.PreAcordoVigencia
                                    ActionNameCreateEdit = "PreAcordoCreateEditVigencia"
                                Case TipoAcao.PreAcordoValor
                                    ActionNameCreateEdit = "PreAcordoCreateEditValor"
                            End Select
                            End Code
                            @<tr>
                                <td Class="center">
                                    <div Class="btn-group left">
                                        <a Class="btn btn-xs btn-info tooltip-info" href="javascript:redirectLoading('@Url.Action(ActionNameCreateEdit, New With {.id = item.CODACOCMC})')" data-rel="tooltip" title="Editar">
                                            <i Class="ace-icon fa fa-pencil bigger-119"></i>
                                        </a>
                                        <a Class="btn btn-xs btn-purple tooltip-info" onclick="abrirModal('AcaoComercial', 'VisualizarFluxo', @Json.Encode(item.CODACOCMC))" data-rel="tooltip" title="Fluxo">
                                            <i Class="ace-icon glyphicon glyphicon-random  bigger-110"></i>
                                        </a>
                                        <a Class="btn btn-xs btn-warning tooltip-info @IIf(item.CODSTAAPVACOCMC = Constantes.Status.APROVADA AndAlso ViewBag.usuarioPodeEstornar, "", "hide")" data-rel="tooltip" title="Estorno Manual" 
                                           onclick="abrirModal('AcaoComercial', 'RealizarEstorno', @Json.Encode(item.CODACOCMC))">
                                            <i Class="ace-icon glyphicon glyphicon-export  bigger-110"></i>
                                        </a>
                                    </div>

                                    @*<div Class="hidden-md hidden-lg">
                                        <div Class="inline pos-rel">
                                            <Button Class="btn btn-minier btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                                <i Class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                            </Button>

                                            <ul Class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                <li>
                                                    <a href="javascript:redirectLoading('@Url.Action(ActionNameCreateEdit, New With {.id = item.CODACOCMC})')" Class="tooltip-info" data-rel="tooltip" title="" data-original-title="Editar">
                                                        <span Class="blue">
                                                            <i Class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a onclick="abrirModal('AcaoComercial', 'VisualizarFluxo', @Json.Encode(item.CODACOCMC))" Class="tooltip-info" data-rel="tooltip" title="Fluxo" data-original-title="Fluxo">
                                                        <span class="purple">
                                                            <i class="ace-icon glyphicon glyphicon-random bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a onclick="abrirModal('AcaoComercial', 'RealizarEstorno', @Json.Encode(item.CODACOCMC))" Class="tooltip-error" data-rel="tooltip" title="Estorno" data-original-title="Estorno">
                                                        <span class="orange">
                                                            <i class="ace-icon glyphicon glyphicon-export bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>*@
                                </td>
                                <td>
                                    @item.CODACOCMC
                                </td>
                                <td>
                                    @item.NOMACOCMC
                                </td>
                                <td>
                                    @Constantes.ConvertStatusAcaoComercial(item.CODSTAAPVACOCMC)
                                </td>
                                <td>
                                    @IIf(Not IsNothing(item.DATGRCACOCMC), item.DATGRCACOCMC.Value.ToShortDateString, "")
                                </td>
                                <td>
                                    @IIf(Not IsNothing(item.DATINIACOCMC), item.DATINIACOCMC.Value.ToShortDateString, "")
                                </td>
                                <td>
                                    @IIf(Not IsNothing(item.DATFIMACOCMC), item.DATFIMACOCMC.Value.ToShortDateString, "")
                                </td>
                                 <td class="text-right">
                                     @item.VLRTOTOCDACOCMC.ToString("#,##0.00")
                                 </td>
                                 @*<td class="text-right">
                                     @item.VLRTOTTXAADMACOCMC.ToString("#,##0.00")
                                 </td>
                                 <td class="text-right">
                                     @item.VLRTOTBDEACOCMC.ToString("#,##0.00")
                                 </td>*@
                                <td>
                                    @item.NOMFNC
                                </td>
                                <td>
                                    @IIf(Not IsNothing(item.DATAPVACOCMC) AndAlso item.DATAPVACOCMC.Value <> Date.MinValue, item.DATAPVACOCMC.Value.ToShortDateString, "")
                                </td>
                                <td>
                                    @IIf(item.CODACOCMCORI is Nothing OrElse item.CODACOCMCORI = 0, "", item.CODACOCMCORI)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>
