﻿@ModelType AcaoComercialViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim IndPreAcordo As Boolean = False
    Dim TipoPreAcordo As Decimal = 0
    Dim action As String = ViewContext.RouteData.Values("action")
    If action.Contains("PreAcordo") Then
        IndPreAcordo = True
        If action.Contains("Vigencia") Then
            TipoPreAcordo = 5
        Else
            TipoPreAcordo = 6
        End If
    End If

End Code

<div class="row">
    <div class="col-sm-2">
        @Html.Label("acaoID", "Código:")
        @Html.TextBox("CODACOCMC", Model.CODACOCMC, New With {.Class = "form-control", .readonly = "readonly"})
        @Html.HiddenFor(Function(m) m.CODACOCMCCPI)
    </div>
    <div class="col-sm-4">
        @Html.Label("acaoNome", "Nome (*):")
        @Html.TextBox("NOMACOCMC", Model.NOMACOCMC, New With {.Class = "form-control"}.EnableIf(ViewBag.txtNomeAcaoEnable))
        @Html.ValidationMessageFor(Function(m) m.NOMACOCMC, "", New With {.class = "text-danger-message-login"})
    </div>

    <div class="col-sm-2">
        @Html.Label("lblComplementar", "Complementar")
        <div class="row">
            <div class="col-sm-6">
                @Html.RadioButtonFor(Function(m) m.INDACOCMCCPL, Constantes.SIM, New With {.onchange = "configuraAcaoOriginal(true)", .id = "radioComplementarSim"}.EnableIf(ViewBag.complementarEnable))
                @Html.Label("sim", "Sim")
            </div>
            <div class="col-sm-6">
                @Html.RadioButtonFor(Function(m) m.INDACOCMCCPL, Constantes.NAO, New With {.onchange = "configuraAcaoOriginal(false)", .id = "radioComplementarNao"}.EnableIf(ViewBag.complementarEnable))
                @Html.Label("nao", "Não")
            </div>
        </div>
    </div>
    <div class="col-sm-4" id="divAcaoPrincipal" style="@ViewHelper.Visible(ViewBag.txtAcaoOriginalVisible)">
        @Html.Label("lblAcaoPrincipal", "Principal (*):")
        @Html.TextBoxFor(Function(model) model.CODACOCMCORI, New With {.Class = "form-control", .id = "codAcaoPrincipal", .onChange = "changeAcaoPrincipal()", .alt = "inteiro_positivo"}.EnableIf(ViewBag.txtAcaoOriginalEnable))
        @Html.Label("lblAcaoComplementar", "Favor Selecionar uma Ação Complementar.", New With {.id = "lblAcaoComplementar", .style = "display: none;", .class = "text-danger-message-login"})
        @Html.ValidationMessageFor(Function(m) m.CODACOCMCORI, "", New With {.class = "text-danger-message-login"})
        @Html.HiddenFor(Function(m) m.IndPreAcordo, New With {.id = "IndPreAcordo"})
        @*@Html.HiddenFor(Function(m) m.TipoPreAcordo, New With {.id = "TipoPreAcordo"})*@
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        @Html.Label("codEmitente", "Cód. do Emitente:")
        @Html.TextBox("CODFNCCRIACOCMC", Model.CODFNCCRIACOCMC, New With {.Class = "form-control", .readonly = "readonly"})
    </div>
    <div class="col-sm-4">
        @Html.Label("nmEmitente", "Nome do Emitente:")
        @Html.TextBoxFor(Function(model) model.NOMFNC, New With {.Class = "form-control", .readonly = "readonly"})
    </div>
    <div class="col-sm-2" id="divCodAprovador" style="@ViewHelper.Visible(ViewBag.aprovadorVisible)">
        @Html.Label("codAprovador", "Cód. do Aprovador:")
        @Html.TextBoxFor(Function(model) model.CODFNCAPVATUACOCMC, New With {.Class = "form-control", .readonly = "readonly"})
    </div>
    <div class="col-sm-4" id="divNomeAprovador" style="@ViewHelper.Visible(ViewBag.aprovadorVisible)">
        @Html.Label("lblNmAprovador", "Nome do Aprovador Atual:")
        @Html.TextBoxFor(Function(model) model.NOMFNCAPVATU, New With {.Class = "form-control", .readonly = "readonly"})
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        @Html.Label("status", "Status:")
        @Html.DropDownListFor(Function(m) m.CODSTAAPVACOCMC, New SelectList(ViewBag.listaStatus, "Value", "Text", Model.CODSTAAPVACOCMC), New With {.Class = "chosen-select form-control", .disabled = "disabled"})
    </div>
    <div class="col-sm-4">
        @Html.Label("data", "Validade (*):")
        <div class="row">
            <div class="col-sm-12">
                <div class="input-daterange input-group">
                    @Html.TextBoxFor(Function(m) m.DATINIACOCMC, If(Model.DATINIACOCMC.HasValue, CDate(Model.DATINIACOCMC).ToShortDateString, ""), New With {.id = "txtDataValidadeInicial", .class = "input-sm date-interval form-control", .autocomplete = "off"}.EnableIf(ViewBag.dataValidadeEnable))
                    <span class="input-group-addon">
                        à
                    </span>
                    @Html.TextBoxFor(Function(m) m.DATFIMACOCMC, If(Model.DATFIMACOCMC.HasValue, CDate(Model.DATFIMACOCMC).ToShortDateString, ""), New With {.id = "txtDataValidadeFinal", .class = "input-sm date-interval form-control", .autocomplete = "off"}.EnableIf(ViewBag.dataValidadeEnable))
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                @Html.ValidationMessageFor(Function(m) m.DATINIACOCMC, "", New With {.class = "text-danger-message-login"})
            </div>
            <div class="col-sm-6">
                @Html.ValidationMessageFor(Function(m) m.DATFIMACOCMC, "", New With {.class = "text-danger-message-login"})
            </div>
        </div>
    </div>
    <div class="col-sm-4" id="divCanalDeVenda" style="@ViewHelper.Visible(ViewBag.canalVendaVisible)">
        @Html.Label("evento", "Canal de Venda (*):")
        @Html.DropDownListFor(Function(m) m.CODCNLVNDINI, New SelectList(ViewBag.listaCanaisVenda, "CODCNLVNDINI", "DESCNLVND", Model.CODCNLVNDINI), "Selecione...", New With {.Class = "form-control chosen-select", .id = "dropCanalDeVenda", .onchange = "document.getElementById(""lblCanaldeVenda"").style.display = ""none"";"}.EnableIf(ViewBag.canalVendaEnable))
        @Html.Label("lblCanaldeVenda", "Favor Selecionar um Canal de Venda.", New With {.id = "lblCanaldeVenda", .style = "display: none;", .class = "text-danger-message-login"})
        @Html.ValidationMessageFor(Function(m) m.CODCNLVNDINI, "", New With {.class = "text-danger-message-login"})
        @Html.HiddenFor(Function(m) m.CanalVendaVisible, New With {.id = "IndCanalVendaVisible"})

    </div>
</div>


<div class="row">
    <div class="col-sm-3">
        @*@Code
                Dim carregaDropEventos = "carregarEventosPorTipoEvento()"
            End Code*@
        @Html.Label("tipoEvento", "Tipo de Evento:")
        @Html.HiddenFor(Function(m) m.INDVLDRGRICT)
        @Html.DropDownListFor(Function(m) m.TIPEVTACOCMC, New SelectList(ViewBag.listaTipoEvento, "TIPEVTACOCMC", "CODIGO_NOME", Model.TIPEVTACOCMC), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropTipoEvento", .onchange = "carregarEventosPorTipoEvento()"}.EnableIf(ViewBag.dropTipoEventoEnable))
    </div>
    <div class="col-sm-3">
        @Html.Label("evento", "Evento:")
        @Html.DropDownListFor(Function(m) m.CODEVTACOCMC, New SelectList(ViewBag.listaEventos, "CODEVTACOCMC", "CODIGO_NOME", Model.CODEVTACOCMC), "Selecione...", New With {.Class = "chosen-select form-control ", .id = "dropEvento", .onchange = "dropEvento_onChange(false, $(this).val())"}.EnableIf(ViewBag.dropEventoEnable))
    </div>
    <div class="col-sm-3">
        @Html.Label("empenhoExa", "Empenho Exa Acordo:")
        @Html.TextBoxFor(Function(model) model.EmpenhoExa, New With {.Class = "form-control", .id = "txtEmpenhoExa", .readonly = "readonly"})
    </div>
    <div class="col-sm-3">
        @code
'If (IndPreAcordo = True) Then
            @Html.Label("filial", "Filial (*):")
            @Html.DropDownListFor(Function(m) m.CODFILEMPORIVBA, New SelectList(ViewBag.listaFilial, "CODFILEMP", "CODFILIAL_NOMFILIAL", Model.CODFILEMPORIVBA), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFilial"}.EnableIf(ViewBag.dropFilial))
            @Html.ValidationMessageFor(Function(m) m.CODFILEMPORIVBA, "", New With {.class = "text-danger-message-login"})
            'Else
            @*@Html.HiddenFor(Function(m) m.CODFILEMPORIVBA, New With {.Value = "0"})*@
            'End If
        End Code
    </div>
</div>

@code
    Dim visibleINDVCLPMC = "none"
    If ((ViewBag.INDVCLPMC = 1)) Then
        visibleINDVCLPMC = ""
    End If
End Code
<div class="row">
    <div class="col-sm-3" id="divPromocaoVinculada" style="display: @visibleINDVCLPMC;">
        <br />
        @Html.Label("lblINDVCLPMC", "Promoção vinculada")
        @Html.HiddenFor(Function(m) m.INDVCLPMCBool)
        <input class="ace ace-switch ace-switch-5" @IIf(Model.INDVCLPMCBool(), "checked=""checked""", "") type="checkbox" onchange="$('#INDVCLPMCBool').val(this.checked);">
        <span class="lbl middle"></span>
    </div>
</div>

@*<input id="id-button-blders" checked="checked" type="checkbox" class="ace ace-switch ace-switch-5">*@
@code
    Dim visibleINDCPHBTB = "none"
    If ((ViewBag.INDCPHBTB = 1)) Then
        visibleINDCPHBTB = ""
    End If
End Code

<div class="row">
    <div class="col-sm-12" id="divDESCPHBTB" style="display: @visibleINDCPHBTB;">
        @Html.Label("CampanhaB2B", "Campanha B2B (*):")
        @Html.EditorFor(Function(f) Model.DESCPHBTB, New With {.htmlAttributes = New With {.class = "form-control", .style = "text-transform: uppercase;"}.EnableIf(ViewBag.txtNomeAcaoEnable)})
        @Html.ValidationMessageFor(Function(m) m.DESCPHBTB, "", New With {.class = "text-danger-message-login"})
    </div>
</div>
<div class="row">
    <div class="col-sm-4" id="divDESCPHBTXB" style="display: @visibleINDCPHBTB;">
        @Html.Label("Tipo B2B", "Tipo B2B:")
        @Html.DropDownListFor(Function(m) m.INDACOCMCBTBVLR, New SelectList(ViewBag.listarB2B, "Value", "Text", Model.INDACOCMCBTBVLR), New With {.Class = "chosen-select form-control", .id = "dropFilial"}.EnableIf(ViewBag.txtNomeAcaoEnable))
        @Html.ValidationMessageFor(Function(m) m.INDACOCMCBTBVLR, "", New With {.class = "text-danger-message-login"})
    </div>
</div>

<br />
<div class="row">
    @code
        If (IndPreAcordo = False AndAlso Model.INDCASHBACK) Then
            @<div class="col-sm-3">
                @Html.Label("Destino Despesas Cash", "Destino Despesas Cash:")
                @Html.DropDownListFor(Function(m) m.TIPDSNDSPCBK, New SelectList(ViewBag.listarDestinoDespesasCash, "Value", "Text", Model.TIPDSNDSPCBK), New With {.Class = "chosen-select form-control", .id = "dropDestinoDespesasCash"})
                @Html.ValidationMessageFor(Function(m) m.TIPDSNDSPCBK, "", New With {.class = "text-danger-message-login"})
            </div>
        End If
    End Code

    <div class="col-sm-3">
        @Html.Label("lblEventoPromocional", "Evento Promocional:")
        @Html.DropDownListFor(Function(m) m.CODEVTBTB, New SelectList(ViewBag.listarEventoPromocional, "CODEVTBTB", "CODDES", Model.CODEVTBTB), "0 - Selecione", New With {.Class = "chosen-select form-control", .id = "dropEventoPromocional"})
    </div>
</div>
