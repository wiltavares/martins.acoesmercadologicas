﻿@ModelType  List(Of RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalListSegmentoCliente" style="z-index: 1051;">

    <div Class="modal-dialog modal-lg">

        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Segmento Clientes
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">
                <div id="msgModalSegmentoCliente">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div>
                    <Table id="tblModalSegmentoCliente" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="center">
                                    @Html.CheckBox("chkSegmentoCliente", New With {.onclick = "marcarOuDesmarcarTodosSegmentoCliente()", .Id = "chkTodosSegmentoCliente", .title = "Selecionar Todos"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoSeguimentoCliente))
                                </th>
                                <th>
                                    Código
                                </th>
                                <th>
                                    Nome
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @For Each item In Model
                                @Html.HiddenFor(Function(r) item.CODACOCMC, New With {.id = "hdfCODACOCMC"})
                                @Html.HiddenFor(Function(r) item.NUMSEQRGRACOCMC, New With {.id = "hdfNUMSEQRGRACOCMC"})
                                @<tr Class="trResultado">

                                    @If item.CODACOCMC IsNot Nothing AndAlso item.NUMSEQRGRACOCMC IsNot Nothing Then
                                        @<td class="center">
                                            @Html.HiddenFor(Function(r) item.CODSGMNGCCLI, New With {.class = "hdfSegmentoCliente"})
                                            @Html.CheckBox("SelecioneSegmentoCliente", New With {.class = "marcaSegmentoCliente", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoSeguimentoCliente))
                                        </td>
                                    Else
                                        @<td class="center">
                                            @Html.HiddenFor(Function(r) item.CODSGMNGCCLI, New With {.class = "hdfSegmentoCliente"})
                                            @Html.CheckBox("SelecioneSegmentoCliente", New With {.class = "marcaSegmentoCliente"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoSeguimentoCliente))
                                        </td>
                                    End If


                                    <td>
                                        @Html.Label(item.CODSGMNGCCLI)
                                    </td>
                                    <td>
                                        @Html.Label("NomeSegmentoCliente", item.DESSGMNGCCLI)
                                    </td>
                                </tr>
                            Next
                        </tbody>
                    </Table>
                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoSeguimentoCliente) Then
                        @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosGridSegmentoCliente();">
                            <i Class="ace-icon fa fa-save bigger-120 green"></i>
                            Salvar
                        </Button>
                    End If
                    <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalSegmentoCliente();">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Sair
                    </Button>
                </div>
            </div>
        </div>
    </div>
</div>
