﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaViewModel)
@Imports Martins.AcoesMercadologicas.Core

@Code
    Dim ajaxopt As New AjaxOptions() With {
    .UpdateTargetId = "resultadosView",
    .InsertionMode = InsertionMode.Replace,
    .OnBegin = "LoadingOn",
    .OnComplete = "ModalClonarMecanicaOnComplete",
    .HttpMethod = "POST"
}
End Code

<div class="modal-dialog modal-lg modalListMecanicaPromocao" id="modalListMecanicaPromocao">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Mecânica
        </div>
        <div Class="modal-body modal-lg" style="overflow-y: scroll !important; right: 2px;">
            <div class="data-msg" id="msgModalPromoção">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>


            <div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small">
                    <h4 class="widget-title smaller white lighter">
                        <i class="ace-icon fa fa-sliders bigger-110"></i>
                        Filtros de Pesquisa
                    </h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        @Using (Ajax.BeginForm("ClonarMecanicaResultados", "RelacaoAcaoMecanica", ajaxopt))
                            @Html.AntiForgeryToken()
                            @<div class="form-horizontal">
                                <div class="row">
                                    <div class="col-lg-3 col-sm-6">
                                        @Html.Label("acaoID", "Código Ação:")
                                        @Html.TextBoxFor(Function(m) m.Filter.CODACOCMC, New With {.Class = "form-control", .alt = "inteiro"})
                                    </div>
                                    <div class="col-lg-6 col-sm-6">
                                        @Html.Label("acaoNome", "Nome Ação:")
                                        @Html.TextBoxFor(Function(m) m.Filter.NOMACOCMC, New With {.Class = "form-control"})
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-6">
                                        @Html.Label("TIPPMC", "Tipo Mecânica:")
                                        @Html.DropDownListFor(Function(m) m.Filter.TIPPMC, New SelectList(ViewBag.ListaTipoPromoca, "CodTipoPromo", "NomTipoPromo", Model.Filter.TIPPMC), New With {.class = "chosen-select form-control"})
                                    </div>
                                    <div class="col-lg-6 col-sm-6">
                                        @Html.Label("DESRGRACOCMC", "Mecânica:")
                                        @Html.TextBoxFor(Function(m) m.Filter.DESRGRACOCMC, New With {.Class = "form-control"})
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-left">
                                            <button type="submit" class="btn btn-info btn-white ">
                                                <i class="ace-icon fa fa-search bigger"></i>
                                                Pesquisar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        End Using
                    </div>
                </div>
            </div>
            <br Class="clearfix" />

            <div id="resultadosView">
                @Code
                    Html.RenderPartial("~/Views/AcaoComercial/Mecanica/RelacaoAcaoMecanica/_ClonarMecanicaResultadosView.vbhtml", Model)
                End Code
            </div>
        </div>

        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                @if (ViewBag.btnAdicinarMecanica) Then
                    @<button type="button" Class="btn btn-white btn-info" onclick="modalCriarMecanicaPromocao();">
                        <i Class="ace-icon fa fa-plus bigger-120 info"></i>
                        Adicionar Mecânica
                    </button>
                End If

                <Button Class="btn btn-danger btn-white" type="button" onclick="$('#modalMecanicaPromocao').modal('hide');">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Sair
                </Button>
            </div>
        </div>
    </div>
</div>