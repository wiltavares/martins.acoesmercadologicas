﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1500px;">
                    <thead>
                        <tr role="row">
                            <th class="center"></th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODACOCMC", Model.Page.OrderBy)">
                                Código Ação
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMACOCMC", Model.Page.OrderBy)">
                                Nome Ação
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESRGRACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESRGRACOCMC", Model.Page.OrderBy)">
                                Mecânica
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("TIPPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("TIPPMC", Model.Page.OrderBy)">
                                Tipo Mecânica
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td Class="center">
                                    <div Class="btn-group left">
                                        <a Class="btn btn-xs btn-primary" onclick="ClonarMecanicaPromocao('@Json.Encode(item.CODACOCMC)', '@Json.Encode(item.NUMSEQRGRACOCMC)');" data-rel="tooltip" title="Clonar">
                                            <i Class="ace-icon fa fa-clone bigger-120"></i>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(r) item.CODACOCMC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(r) item.NOMACOCMC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(r) item.DESRGRACOCMC)
                                </td>
                                <td>
                                    @Select Case item.TIPPMC
                                        Case 0
                                             @<label>Normal</label>
                                        Case 1
                                            @<label>
                                                Combinada: @Html.DisplayFor(Function(r) item.QDEGRPMIXPMC)
                                            </label>
                                        Case 2
                                            @<label>
                                                Faixas Multiplas: @Html.DisplayFor(Function(r) item.QDEFXAPMC)
                                            </label>
                                    End Select
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code
        </div>
    </div>
</div>
