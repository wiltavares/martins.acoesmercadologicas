﻿@ModelType RelacaoAcaoMecanicaMixViewModel
@Imports Martins.AcoesMercadologicas.Core


<div class="row">
    <div class="col-xs-12">
        <div class="widget-box widget-color-blue2">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller white lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Mix
                </h4>
                <div class="widget-toolbar">

                    @If (ViewBag.ParticipacaoMixTotal = 0) Then
                        @<button class="btn btn-xs btn-white btn-info" type="button" onclick="abrirModalPreVisualizarMixMecanicaAcao(id='#modalListPreVisualizarMixMecanicaAcao')">
                            <i Class="ace-icon fa fa-eye bigger-119"></i>
                            Pré Visualizar Mix
                        </button>
                    End If


                    <a href="#" data-action="collapse">
                        <i class="1 ace-icon fa bigger-125 fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">

                @If (ViewBag.ParticipacaoMixTotal = 0) Then

                    @<div Class="widget-main">
                        <div id="msgModalMix">
                            @Html.Partial(Constantes.VIEW_ALERTAS)
                        </div>
                        <div style="width: auto;">
                            <table class="table table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                <tbody>

                                    @If (ViewBag.INDEVTSMA = 0) Then
                                        @<tr>
                                            <td Class="center">
                                                @if (ViewBag.TipoPromocao <> 1) Then
                                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                                        @if (Model.INDGRUPOMERCADORIA = 1) Then
                                                            @<Button Class="btn btn-xs btn-white btn-success" onclick="abrirModalMixGrupoMercadoria(id='#modalListMixGrupoMercadoria')">
                                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                                Configurar
                                                            </Button>
                                                        Else
                                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalMixGrupoMercadoria(id='#modalListMixGrupoMercadoria')">
                                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                                Selecionar
                                                            </Button>
                                                        End If
                                                Else
                                                        @if (Model.INDGRUPOMERCADORIA = 1) Then
                                                            @<Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalMixGrupoMercadoria(id='#modalListMixGrupoMercadoria')">
                                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                                Configurado
                                                            </Button>
                                                        End If
                                                    End If
                                                 End If
                                            </td>
                                            <td>
                                                @Html.DisplayNameFor(Function(p) p.CODGRPMER)
                                            </td>
                                        </tr>

                                        @<tr>
                                            <td Class="center">
                                                @If (ViewBag.TipoPromocao <> 1) Then
                                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                                        @if (Model.INDFAMILIAMERCADORIA = 1) Then
                                                            @<Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalMixFamiliaMercadoria(id='#modalMecanicaMixFamiliaMercadoria')">
                                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                                Configurar
                                                            </Button>
                                                        Else
                                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalMixFamiliaMercadoria(id='#modalMecanicaMixFamiliaMercadoria')">
                                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                                Selecionar
                                                            </Button>
                                                        End If
                                                    Else
                                                        @if (Model.INDFAMILIAMERCADORIA = 1) Then
                                                            @<Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalMixFamiliaMercadoria(id='#modalMecanicaMixFamiliaMercadoria')">
                                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                                Configurado
                                                            </Button>
                                                        End If
                                                    End If
                                                End If
                                            </td>
                                            <td>
                                                @Html.DisplayNameFor(Function(p) p.CODFMLMER)
                                            </td>
                                        </tr>

                                        @<tr>
                                            <td Class="center">
                                                @If (ViewBag.TipoPromocao <> 1) Then
                                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                                        @if (Model.INDCLASSEMERCADORIA = 1) Then
                                                            @<Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalMixClasseMercadoria(id='#modalMecanicaMixClasseMercadoria')">
                                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                                Configurar
                                                            </Button>
                                                        Else
                                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalMixClasseMercadoria(id='#modalMecanicaMixClasseMercadoria')">
                                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                                Selecionar
                                                            </Button>
                                                        End If
                                                    Else
                                                        @if (Model.INDCLASSEMERCADORIA = 1) Then
                                                            @<Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalMixClasseMercadoria(id='#modalMecanicaMixClasseMercadoria')">
                                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                                Configurado
                                                            </Button>
                                                        End If
                                                    End If
                                                End If
                                            </td>
                                            <td>
                                                @Html.DisplayNameFor(Function(p) p.CODCLSMER)
                                            </td>
                                        </tr>
                                    End If

                                    <tr>
                                        <td Class="center">
                                            @if (ViewBag.PermiteEdicaoMecanica) Then
                                                @if (Model.INDMERCADORIA = 1) Then
                                                    @<Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalMixMercadoria(id='#modalMecanicaMixMercadoria')">
                                                        <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </Button>
                                                Else
                                                    @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalMixMercadoria(id='#modalMecanicaMixMercadoria')">
                                                        <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                        Selecionar
                                                    </Button>
                                                End If
                                            Else
                                                @if (Model.INDMERCADORIA = 1) Then
                                                    @<Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalMixMercadoria(id='#modalMecanicaMixMercadoria')">
                                                        <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurado
                                                    </Button>
                                                End If
                                            End If
                                        </td>
                                        <td>
                                            @Html.DisplayNameFor(Function(p) p.CODMER)
                                        </td>
                                    </tr>
                                    
                                    @If (ViewBag.INDEVTSMA = 0) Then
                                        @<tr>
                                            <td Class="center">
                                                @if (ViewBag.TipoPromocao <> 1) Then
                                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                                        @if (Model.INDFORNECEDOR = 1) Then
                                                            @<Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalMixFornecedor(id='#modalMecanicaMixFornecedor')">
                                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                                Configurar
                                                            </Button>
                                                        Else
                                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalMixFornecedor(id='#modalMecanicaMixFornecedor')">
                                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                                Selecionar
                                                            </Button>
                                                        End If
                                                    Else
                                                        @if (Model.INDFORNECEDOR = 1) Then
                                                            @<Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalMixFornecedor(id='#modalMecanicaMixFornecedor')">
                                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                                Configurado
                                                            </Button>
                                                        End If
                                                    End If
                                                End If
                                            </td>
                                            <td>
                                                @Html.DisplayNameFor(Function(p) p.CODFRN)
                                            </td>
                                        </tr>
                                    End If
                                </tbody>
                            </table>
                        </div>
                    </div>
                Else
                    @<div class="alert alert-info">
                        Tipo de Participação: Mix Total
                    </div>
                End If
            </div>
        </div>
    </div>
</div>

@section Scripts
    <script type="text/javascript" src="~/Scripts/mecanicaPromocao.js"></script>
End Section



