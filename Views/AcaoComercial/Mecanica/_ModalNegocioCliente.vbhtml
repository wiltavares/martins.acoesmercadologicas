﻿@ModelType  List(Of RelacaoAcaoMecanicaPublicoAlvoNegocioClienteViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalListNegocioCliente" style="z-index: 1051;">

    <div Class="modal-dialog modal-lg">

        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Negocio Clientes
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">
                <div id="msgModalNegocioCliente">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div>
                    <Table id="tblModalNegocioCliente" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="center">
                                    @Html.CheckBox("chkNegocioCliente", New With {.onclick = "marcarOuDesmarcarTodosNegocioCliente()", .Id = "chkTodosNegocioCliente", .title = "Selecionar Todos"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoNegocioCliente))
                                </th>
                                <th>
                                    Código
                                </th>
                                <th>
                                    Nome
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @For Each item In Model
                                @Html.HiddenFor(Function(r) item.CODACOCMC, New With {.id = "hdfCODACOCMC"})
                                @Html.HiddenFor(Function(r) item.NUMSEQRGRACOCMC, New With {.id = "hdfNUMSEQRGRACOCMC"})
                                @<tr Class="trResultado">
                                    @If item.CODACOCMC IsNot Nothing AndAlso item.NUMSEQRGRACOCMC IsNot Nothing Then
                                        @<td class="center">
                                            @Html.HiddenFor(Function(r) item.CODUNDESRNGC, New With {.class = "hdfNegocioCliente"})
                                            @Html.CheckBox("SelecioneNegocioCliente", New With {.class = "marcaNegocioCliente", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoNegocioCliente))
                                        </td>
                                    Else
                                        @<td class="center">
                                            @Html.HiddenFor(Function(r) item.CODUNDESRNGC, New With {.class = "hdfNegocioCliente"})
                                            @Html.CheckBox("SelecioneNegocioCliente", New With {.class = "marcaNegocioCliente"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoNegocioCliente))
                                        </td>
                                    End If

                                    <td>
                                        @Html.Label(item.CODUNDESRNGC)
                                    </td>
                                    <td>
                                        @Html.Label("NomeNegocioCliente", item.DESUNDESRNGC)
                                    </td>
                                </tr>
                            Next
                        </tbody>
                    </Table>
                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoNegocioCliente) Then
                        @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosGridNegocioCliente();">
                            <i Class="ace-icon fa fa-save bigger-120 green"></i>
                            Salvar
                        </Button>
                    End If
                    <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalNegocioCliente();">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Sair
                    </Button>
                </div>
            </div>
        </div>
    </div>
</div>
