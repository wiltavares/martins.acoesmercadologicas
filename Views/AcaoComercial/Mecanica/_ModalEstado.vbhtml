﻿@ModelType  List(Of RelacaoAcaoMecanicaPublicoAlvoEstadoViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalListEstadoUniao" style="z-index: 1051;">
    <div Class="modal-dialog modal-lg">
        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Estado
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">
                <div id="msgmodalListEstadoUniao">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div>
                    <Table id="tblModalEstadoUniao" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="center">
                                    @Html.CheckBox("chkEstados", New With {.onclick = "marcarOuDesmarcarTodosEstados()", .Id = "chkTodosEstados"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoEstado))
                                </th>
                                <th>
                                    Sigla
                                </th>
                                <th>
                                    Nome
                                </th>
                                <th>
                                    Reg.
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @for Each item In Model
                                @Html.HiddenFor(Function(r) item.CODACOCMC, New With {.id = "hdfCODACOCMC"})
                                @Html.HiddenFor(Function(r) item.NUMSEQRGRACOCMC, New With {.id = "hdfNUMSEQRGRACOCMC"})
                                @Html.HiddenFor(Function(r) item.CODESTUNI, New With {.id = "hdfCODESTUNI"})
                                @<tr Class="trResultado">
                                    @if item.CODACOCMC IsNot Nothing AndAlso item.NUMSEQRGRACOCMC IsNot Nothing Then
                                        @<td Class="center">
                                            @Html.CheckBox("SelecioneEstadoUniao", New With {.class = "marcaEstado", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoEstado))
                                        </td>
                                    Else
                                        @<td Class="center">
                                            @Html.CheckBox("SelecioneEstadoUniao", New With {.class = "marcaEstado"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoEstado))
                                        </td>
                                    End If
                                    <td>
                                        @Html.Label(item.CODESTUNI)
                                    </td>
                                    <td>
                                        @Html.Label(item.NOMESTUNI)
                                    </td>
                                    <td>
                                        @Html.Label(item.CODDIVREG)
                                    </td>
                                </tr>
                            Next
                        </tbody>
                    </Table>
                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoEstado) Then
                        @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosGridEstadoUniao();">
                            <i Class="ace-icon fa fa-save bigger-120 green"></i>
                            Salvar
                        </Button>
                    End If
                    <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalEstadoUni();">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Sair
                    </Button>
                </div>
            </div>
        </div>
    </div>
</div>
