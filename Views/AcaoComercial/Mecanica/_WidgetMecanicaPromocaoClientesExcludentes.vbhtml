﻿@ModelType List(Of RelacaoAcaoMecanicaClientesExcludentesViewModel)
@Imports Martins.AcoesMercadologicas.Core


<div class="row">
    <div class="col-xs-12">
        <div class="widget-box widget-color-blue2">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller white lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Clientes Excludentes
                </h4>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="1 ace-icon fa bigger-125 fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div id="msgWidgetClientesExcludentes">
                        @Html.Partial(Constantes.VIEW_ALERTAS)
                    </div>
                    <div style="width: auto; overflow-x: scroll;">
                        <table class="table table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                            <thead>

                            </thead>
                            <tbody>
                                
                                <tr>
                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                        @if (ViewBag.NenhumClienteExcludenteSelecionado) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalClientesExcludentes(id='#modalListClientesExcludentes')">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            </td>
                                        Else
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalClientesExcludentes(id='#modalListClientesExcludentes')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurar
                                                </Button>
                                            </td>
                                        End If
                                    Else
                                        @if (Not ViewBag.NenhumClienteExcludenteSelecionado) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalClientesExcludentes(id='#modalListClientesExcludentes')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurado
                                                </Button>
                                            </td>
                                        Else
                                            @<td Class="center"></td>
                                        End If

                                    End If
                                    <td>
                                        Clientes Excludentes
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section Scripts
    <script type="text/javascript" src="~/Scripts/mecanicaPromocao.js"></script>
End Section



