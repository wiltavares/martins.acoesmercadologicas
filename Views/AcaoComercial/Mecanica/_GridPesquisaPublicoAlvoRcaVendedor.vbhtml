﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div style="width: auto; overflow-x: scroll;">
    <Table id="tblResultadoPesquisaRcaVendedor" Class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
            <tr role="row">
                <th class="center">
                    <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="addPesquisaPublicoAlvoRcaVendedor(@Newtonsoft.Json.JsonConvert.SerializeObject(Model.Filter))">
                        &nbsp;<i class="fa fa-plus bigger-120"></i>&nbsp;
                    </button>
                </th>
                <th>
                    Código
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NOMREP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMREP", Model.Page.OrderBy, Model.ResultID)">
                    Nome
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr Class="trResultado">
                    <td class="center">
                        <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="salvarPublicoAlvoRcaVendedor(this)">
                            <i class="ace-icon fa fa-plus bigger-120 info"></i>
                        </button>
                    </td>
                    <td>
                        @item.CODREP
                    </td>
                    <td>
                        @item.NOMREP
                    </td>
                </tr>
            Next
        </tbody>
    </Table>
</div>
<br class="clearfix" />
@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code