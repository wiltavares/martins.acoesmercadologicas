﻿@ModelType List(Of RelacaoAcaoMecanicaMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Classe Mercadoria Selecionadas
                </h4>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divClasseMercadoriaSelecionadas" style="width: auto; overflow-x: scroll;">
                        <Table id="tblClasseMercadoriaSelecionada" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                            <thead>
                                <tr role="row">
                                    @if (ViewBag.PermiteEdicaoMecanicaMixClasseMercadoria) Then
                                        @<th Class="center">
                                            <button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteAllMixClasseMercadoria()" title="Excluir todos">
                                                <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </button>
                                        </th>
                                    End If
                                    <th>
                                        Cód.
                                    </th>
                                    <th>
                                        Classe Mercadoria
                                    </th>
                                    <th>
                                        Grupo Mercadoria
                                    </th>
                                    <th>
                                        Família Mercadoria
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @for Each item In Model
                                    @<tr Class="trResultado">
                                        @Html.HiddenFor(Function(r) item.NUMSEQMIXACOCMC)
                                        @Html.HiddenFor(Function(r) item.CODGRPMER)
                                        @if (ViewBag.PermiteEdicaoMecanicaMixClasseMercadoria) Then
                                            @<td class="center">
                                                <button type="button" class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteMixClasseMercadoria(this)">
                                                    <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </button>
                                            </td>
                                        End If
                                        <td>
                                            @item.CODCLSMER
                                        </td>
                                        <td>
                                            @item.DESCLSMER
                                        </td>
                                        <td>
                                            @item.CODGRPMER - @item.DESGRPMER
                                        </td>
                                        <td>
                                            @item.CODFMLMER - @item.DESFMLMER
                                        </td>
                                    </tr>
                                Next
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
