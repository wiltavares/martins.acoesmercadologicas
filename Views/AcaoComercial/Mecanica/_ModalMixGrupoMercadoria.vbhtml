﻿@ModelType  List(Of RelacaoAcaoMecanicaMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalListMixGrupoMercadoria" style="z-index: 1051;">
    <div Class="modal-dialog modal-lg">
        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Grupo Mercadoria
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">
                <div id="msgmodalListMixGrupoMercadoria">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div>
                    <Table id="tblModalGrupoMercadoria" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="center">
                                    @Html.CheckBox("chkGruposMercadorias", New With {.onclick = "marcarOuDesmarcarTodosGruposMercadorias()", .Id = "chkTodosGruposMercadorias"}.EnableIf(ViewBag.PermiteEdicaoMecanicaMixGrupoMercadoria))
                                </th>
                                <th>
                                    Código
                                </th>
                                <th>
                                    Nome
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @for Each item In Model
                                @Html.HiddenFor(Function(r) item.CODACOCMC, New With {.id = "hdfCODACOCMC"})
                                @Html.HiddenFor(Function(r) item.NUMSEQRGRACOCMC, New With {.id = "hdfNUMSEQRGRACOCMC"})
                                @Html.HiddenFor(Function(r) item.CODGRPMER, New With {.id = "hdfCODGRPMER"})
                                @<tr Class="trResultado">
                                    @if item.CODACOCMC IsNot Nothing AndAlso item.NUMSEQRGRACOCMC IsNot Nothing AndAlso item.NUMSEQMIXACOCMC IsNot Nothing Then
                                        @<td Class="center">
                                            @Html.CheckBox("SelecioneGrupoMercadoria", New With {.class = "marcaGrupoMercadoria", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoMecanicaMixGrupoMercadoria))
                                        </td>
                                    Else
                                        @<td Class="center">
                                            @Html.CheckBox("SelecioneGrupoMercadoria", New With {.class = "marcaGrupoMercadoria"}.EnableIf(ViewBag.PermiteEdicaoMecanicaMixGrupoMercadoria))
                                        </td>
                                    End If
                                    <td>
                                        @Html.Label(item.CODGRPMER)
                                    </td>
                                    <td>
                                        @Html.Label(item.DESGRPMER)
                                    </td>
                                </tr>
                            Next
                        </tbody>
                    </Table>
                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    @if (ViewBag.PermiteEdicaoMecanicaMixGrupoMercadoria) Then
                        @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosMixGrupoMercadoria();">
                            <i Class="ace-icon fa fa-save bigger-120 green"></i>
                            Salvar
                        </Button>
                    End If
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalMixGrupoMercadoria();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Sair
                </Button>
                </div>
            </div>
        </div>
    </div>
</div>
