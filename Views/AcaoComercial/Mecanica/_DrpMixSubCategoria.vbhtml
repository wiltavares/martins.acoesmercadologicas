﻿@ModelType RelacaoAcaoMecanicaMixViewModel
@Imports Martins.AcoesMercadologicas.Core

<div class="col-sm-12">
    @Html.Label("Sub Categoria:")
    @Html.DropDownListFor(Function(r) r.CODCLSMER, New SelectList(ViewBag.itensDropSubCategoria, "CODCLSMER", "DESCLSMER", Model.CODCLSMER), New With {.class = "chosen-select form-control", .id = "drpMixSubCategoria"}.EnableIf(ViewBag.PermiteEdicaoMecanicaMixGrupoMercadoria))
</div>