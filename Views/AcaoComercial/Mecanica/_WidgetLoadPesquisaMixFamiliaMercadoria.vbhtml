﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

@code
    Dim ajaxOptions As New AjaxOptions() With {
        .UpdateTargetId = "gridItensPesquisaMixFamiliaMercadoria",
        .InsertionMode = InsertionMode.Replace,
        .OnBegin = "LoadingOn",
        .OnComplete = "LoadingOff_UpdateMessage",
        .HttpMethod = "POST"
        }

End Code

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Pesquisa Família Mercadoria
                </h4>
            </div>
            <div Class="widget-body">
                <div id="msgWidgetMixFamiliaMercadoria">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div Class="widget-main">
                    <div id="divResultadoPesquisaMixFamiliaMercadoria">
                        @Using (Ajax.BeginForm("PesquisaMixFamiliaMercadoria", "RelacaoAcaoMecanicaMix", ajaxOptions, New With {.id = "frmPesquisaRelacaoAcaoMecanicaMix"}))
                            @Html.AntiForgeryToken()

                            @Html.HiddenFor(Function(r) Model.Filter.CODACOCMC)
                            @Html.HiddenFor(Function(r) Model.Filter.NUMSEQRGRACOCMC)

                            @<div class="row">
                                 <div class="col-sm-6">
                                     @Html.Label("Família de Mercadoria:")
                                     <div>
                                         @Html.TextBoxFor(Function(r) Model.Filter.CODFMLMER, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                         @Html.TextBoxFor(Function(r) Model.Filter.DESFMLMER, New With {.class = "form-control", .style = "width: 80%;"})
                                     </div>
                                 </div>
                                 <div class="col-sm-6">
                                     @Html.Label("Grupo de Mercadoria:")
                                     <div>
                                         @Html.TextBoxFor(Function(r) Model.Filter.CODGRPMER, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                         @Html.TextBoxFor(Function(r) Model.Filter.DESGRPMER, New With {.class = "form-control", .style = "width: 80%;"})
                                     </div>
                                 </div>
                            </div>
                            @<br />
                            @<div Class="row">
                                <div Class="col-sm-3">
                                    <Button type="submit" Class="btn btn-white btn-info">
                                        <i Class="ace-icon fa fa-search bigger-120 info"></i>
                                        Pesquisar
                                    </Button>
                                </div>
                            </div>
                        End Using

                        <br />
                        <div id="gridItensPesquisaMixFamiliaMercadoria">
                            @Code
                                Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_GridPesquisaMixFamiliaMercadoria.vbhtml", Model)
                            End Code
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
