﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div style="width: auto; overflow-x: scroll;">
    <Table id="tblResultadoPesquisaGerenteMercado" Class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
            <tr role="row">
                <th class="center">
                    <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="addPesquisaPublicoAlvoGerenteMercado(@Newtonsoft.Json.JsonConvert.SerializeObject(Model.Filter))">
                        &nbsp;<i class="fa fa-plus bigger-120"></i>&nbsp;
                    </button>
                </th>
                <th>
                    Código
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NOMSUP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMSUP", Model.Page.OrderBy, Model.ResultID)">
                    Nome
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr Class="trResultado">
                    <td class="center">
                        <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="salvarPublicoAlvoGerenteMercado(this)">
                            <i class="ace-icon fa fa-plus bigger-120 info"></i>
                        </button>
                    </td>
                    <td>
                        @item.CODSUP
                    </td>
                    <td>
                        @item.NOMSUP
                    </td>
                </tr>
            Next
        </tbody>
    </Table>
</div>
<br class="clearfix" />
@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code