﻿@ModelType List(Of RelacaoAcaoMecanicaFilialViewModel)
@Imports Martins.AcoesMercadologicas.Core

@Using (Ajax.BeginForm("Save", "RelacaoAcaoMecanica", Constantes.AJAX_OBJECT_POST_DEFAULT, New With {.id = "frmWidgetMecanicaPromocaoFiliais"}))
    @Html.AntiForgeryToken()
    @<div Class="row">
        <div Class="col-xs-12">
            <div Class="widget-box widget-color-blue2">
                <div Class="widget-header widget-header-small">
                    <h4 Class="widget-title smaller white lighter">
                        <i Class="ace-icon fa fa-sliders bigger-110"></i>
                        Filiais
                    </h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="1 ace-icon fa bigger-125 fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div Class="widget-body">
                    <div Class="widget-main">

                        <div style="width: auto; overflow-x: scroll;">
                            <table id="tabelaFiliais" class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px;">
                                <thead>
                                    <tr role="row">
                                        <th class="center">
                                            @Html.CheckBox("chkTodos", New With {.onclick = "marcarOuDesmarcarTodasFiliais()", .Id = "chkTodasFiliais"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                                        </th>
                                        <th>
                                            Codigo
                                        </th>
                                        <th>
                                            Filial
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for Each item In Model
                                        @<tr class="trResultado">
                                            @if item.CODACOCMC IsNot Nothing AndAlso item.NUMSEQRGRACOCMC IsNot Nothing Then
                                                @<td Class="center">
                                                    @Html.CheckBox("SelecioneFilial", New With {.class = "marca", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                                                </td>
                                            Else
                                                @<td Class="center">
                                                    @Html.CheckBox("SelecioneFilial", New With {.class = "marca"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                                                </td>
                                            End If
                                            <td Class="tdFilial">
                                                <span Class="codFilialValue" value="@item.CODFILEMP">@item.CODFILEMP</span>
                                            </td>
                                            <td>
                                                @item.DESABVFILEMP
                                            </td>
                                        </tr>
                                    Next
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>End Using
