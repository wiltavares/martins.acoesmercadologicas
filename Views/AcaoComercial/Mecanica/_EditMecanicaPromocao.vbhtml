﻿@ModelType RelacaoAcaoMecanicaViewModel
@Imports Martins.AcoesMercadologicas.Core

<div class="modal-dialog modal-lg" id="modalEditMecanicaPromocao">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="fa fa-pencil bigger-120"></i>
            &nbsp;Mecânica
        </div>
        <div id="divBodyEditMecanicaPromocao" class="modal-body" style="overflow-y: scroll !important;">
            <div class="widget-main">
                @Html.AntiForgeryToken()
                <div id="WidgetMecanicaPromocaoGerais">
                    <div id="msgModalEditMecanicaPromocao">
                        @Html.Partial(Constantes.VIEW_ALERTAS)
                    </div>
                    @code
                        Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetMecanicaPromocaoGerais.vbhtml", Model)
                    End Code
                    <div id="WidgetMecanicaPromocaoFiliais">

                    </div>
                    @If (Model.NUMSEQRGRACOCMC <> 0) Then
                        @code
                            Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetMecanicaPromocaoCriterioDeApuracao.vbhtml", Model)
                        End Code
                    End If

                    <div id="WidgetMecanicaPromocaoPublicoAlvo">

                    </div>

                    @*<div id="WidgetMecanicaPromocaoClientesExcludentes">

                    </div>*@

                    <div id="WidgetMecanicaPromocaoMix">

                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                @if (ViewBag.PermiteEdicaoMecanica) Then
                    @<button type="submit" Class="btn btn-white btn-success" onclick="SalvarMecanica()">
                        <i Class="ace-icon fa fa-save bigger-120 green"></i>
                        Salvar Mecânica
                    </button>
                End If
            <Button Class="btn btn-danger btn-white" type="button" onclick="AbrirModalEditMecanicaAcao();">
                <i Class="ace-icon fa fa-close bigger-120 red"></i>
                Sair
            </Button>
            </div>
        </div>
    </div>
</div>

<div id="divModalExtra" class="modal fade" tabindex="-1" role="dialog"></div>