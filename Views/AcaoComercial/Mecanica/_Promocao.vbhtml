﻿@ModelType  List(Of RelacaoAcaoMecanicaViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="modal-dialog modal-lg modalListMecanicaPromocao" id="modalListMecanicaPromocao">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Mecânica
        </div>
        <div Class="modal-body modal-lg" style="overflow-y: scroll !important; right: 2px;">
            <div class="data-msg" id="msgModalPromoção">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            @For Each item In Model
                @<div Class="row">
                    <div Class="col-xs-12">
                        <div Class="well">
                            <div Class="row">
                                <div Class="col-sm-12">
                                    @*<Button Class="btn btn-xs btn-info" type="button" onclick="$('#modalEditMecanicaPromocao_prot').show();ChosenResize(); $('#modalListMecanicaPromocao').hide();">
                                            <i Class="ace-icon fa fa-pencil bigger-119">Prot</i>
                                        </Button>*@

                                    @if (ViewBag.btnEditarMecanica) Then
                                        @<a Class="btn btn-xs btn-info" onclick="modalEditMecanicaPromocao(@Json.Encode(item))">
                                            <i Class="ace-icon fa fa-pencil bigger-119"></i>
                                        </a>
                                    Else
                                        @<a Class="btn btn-xs btn-warning" onclick="modalEditMecanicaPromocao(@Json.Encode(item))">
                                            <i Class="ace-icon fa fa-eye bigger-120"></i>
                                        </a>
                                    End If
                                    @if (ViewBag.btnEditarMecanica) Then
                                        @<Button Class="btn btn-xs btn-danger" onclick="DeleteRelacaoAcaoMecanica(@item.CODACOCMC,  @item.NUMSEQRGRACOCMC);" )">
                                            <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                        </Button>
                                    End If
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    @item.DESRGRACOCMC
                                </div>
                            </div>
                            <div Class="row">
                                <div Class="col-sm-12">


                                    <Label> Filiais :</Label>
                                    <br />
                                    @for Each itemFilial In item.RelacaoAcaoMecanicaFilialList
                                        @<span Class="badge gadge-primary">@itemFilial.CODFILEMP - @itemFilial.DESABVFILEMP </span>
                                    Next
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            Next
        </div>

        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                @if (ViewBag.btnAdicinarMecanica) Then
                    @<button type="button" Class="btn btn-white btn-warning" onclick="modalClonarMecanicaPromocao();">
                        <i Class="ace-icon fa fa-clone bigger-120 light-orange"></i>
                        Clonar Mecânica
                    </button>

                    @<button type="button" Class="btn btn-white btn-info" onclick="modalCriarMecanicaPromocao();">
                        <i Class="ace-icon fa fa-plus bigger-120 info"></i>
                        Adicionar Mecânica
                    </button>
                End If

                <Button Class="btn btn-danger btn-white" type="button" onclick="FecharModalMecanicaAcao()">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Sair
                </Button>
            </div>
        </div>
    </div>
</div>
@*<div id="frmEditMecanicaPromocao" class="modal fade" tabindex="-1" role="dialog"></div>*@

@*@Html.Partial("~/Views/AcaoComercial/Mecanica/_EditMecanicaPromocao_Prot.vbhtml")*@
