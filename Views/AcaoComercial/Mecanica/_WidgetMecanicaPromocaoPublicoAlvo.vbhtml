﻿@ModelType RelacaoAcaoMecanicaPublicoAlvoViewModel
@Imports Martins.AcoesMercadologicas.Core


<div class="row">
    <div class="col-xs-12">
        <div class="widget-box widget-color-blue2">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller white lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Publico Alvo
                </h4>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="1 ace-icon fa bigger-125 fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div id="msgModalPublicoAlvo">
                        @Html.Partial(Constantes.VIEW_ALERTAS)
                    </div>
                    <div style="width: auto; overflow-x: scroll;">
                        <table class="table table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                            <thead>

                            </thead>
                            <tbody>
                                <tr style="background-color: #f5f5f5;">
                                    <td colspan="2">
                                        <b>Região</b>
                                    </td>
                                </tr>
                                <tr>
                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                        @if (Model.INDESTADO = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-success" onclick="abrirModalEstadoUniao(id='#modalListEstadoUniao')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurar
                                                </Button>
                                            </td>
                                        Else
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalEstadoUniao(id='#modalListEstadoUniao')">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            </td>
                                        End If
                                    Else
                                        @if (Model.INDESTADO = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalEstadoUniao(id='#modalListEstadoUniao')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurado
                                                </Button>
                                                @*<a Class="btn btn-xs btn-warning" onclick="abrirModalEstadoUniao(id='#modalListEstadoUniao')">
                                                        <i Class="ace-icon fa fa-eye bigger-120"></i>
                                                    </a>*@
                                            </td>
                                        Else
                                            @<td Class="center"></td>
                                        End If
                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.INDESTADO)
                                    </td>
                                </tr>
                                <tr style="background-color: #f5f5f5;">
                                    <td colspan="2">
                                        <b>Clientes</b>
                                    </td>
                                </tr>
                                <tr>
                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                        @if (Model.INDATIVIDADE = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalAtividade(id='#modalListAtividade')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurar
                                                </Button>
                                            </td>
                                        Else
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalAtividade(id='#modalListAtividade')">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            </td>
                                        End If
                                    Else
                                        @if (Model.INDATIVIDADE = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalAtividade(id='#modalListAtividade')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurado
                                                </Button>
                                                @*<a Class="btn btn-xs btn-warning" onclick="abrirModalAtividade(id='#modalListAtividade')">
                                                        <i Class="ace-icon fa fa-eye bigger-120"></i>
                                                    </a>*@
                                            </td>
                                        Else
                                            @<td Class="center"></td>
                                        End If

                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.INDATIVIDADE)
                                    </td>
                                </tr>
                                <tr>
                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                        @if (Model.INDGRUPAMENTOCLIENTE = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalGrupamentoCliente(id='#modalListGrupamentoCliente')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurar
                                                </Button>
                                            </td>
                                        Else
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalGrupamentoCliente(id='#modalListGrupamentoCliente')">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            </td>
                                        End If
                                    Else
                                        @if (Model.INDGRUPAMENTOCLIENTE = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalGrupamentoCliente(id='#modalListGrupamentoCliente')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurado
                                                </Button>
                                                @*<a Class="btn btn-xs btn-warning" onclick="abrirModalGrupamentoCliente(id='#modalListGrupamentoCliente')">
                                                        <i Class="ace-icon fa fa-eye bigger-120"></i>
                                                    </a>*@
                                            </td>
                                        Else
                                            @<td Class="center"></td>
                                        End If

                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.INDGRUPAMENTOCLIENTE)
                                    </td>
                                </tr>
                                <tr>
                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                        @if (Model.INDNEGOCIOCLIENTE = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalNegocioCliente(id='#modalListNegocioCliente')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurar
                                                </Button>
                                            </td>
                                        Else
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalNegocioCliente(id='#modalListNegocioCliente')">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            </td>
                                        End If
                                    Else
                                        @if (Model.INDNEGOCIOCLIENTE = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalNegocioCliente(id='#modalListNegocioCliente')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurado
                                                </Button>
                                                @*<a Class="btn btn-xs btn-warning" onclick="abrirModalNegocioCliente(id='#modalListNegocioCliente')">
                                                        <i Class="ace-icon fa fa-eye bigger-120"></i>
                                                    </a>*@
                                            </td>
                                        Else
                                            @<td Class="center"></td>
                                        End If

                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.INDNEGOCIOCLIENTE)
                                    </td>
                                </tr>
                                <tr>
                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                        @if (Model.INDSEGMENTOCLIENTE = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalSegmentoCliente(id='#modalListSegmentoCliente')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurar
                                                </Button>
                                            </td>
                                        Else
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalSegmentoCliente(id='#modalListSegmentoCliente')">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            </td>
                                        End If
                                    Else
                                        @if (Model.INDSEGMENTOCLIENTE = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalSegmentoCliente(id='#modalListSegmentoCliente')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurado
                                                </Button>
                                                @*<a Class="btn btn-xs btn-warning" onclick="abrirModalSegmentoCliente(id='#modalListSegmentoCliente')">
                                                    <i Class="ace-icon fa fa-eye bigger-120"></i>
                                                </a>*@
                                            </td>
                                        Else
                                            @<td Class="center"></td>
                                        End If

                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.INDSEGMENTOCLIENTE)
                                    </td>
                                </tr>
                                <tr>
                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                        @if (Model.INDCLIENTE = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalCliente(id='#modalListCliente')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurar
                                                </Button>
                                            </td>
                                        Else
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalCliente(id='#modalListCliente')">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            </td>
                                        End If
                                    Else
                                        @if (Model.INDCLIENTE = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalCliente(id='#modalListCliente')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurado
                                                </Button>
                                                @*<a Class="btn btn-xs btn-warning" onclick="abrirModalCliente(id='#modalListCliente')">
                                                        <i Class="ace-icon fa fa-eye bigger-120"></i>
                                                    </a>*@
                                            </td>
                                        Else
                                            @<td Class="center"></td>
                                        End If

                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.INDCLIENTE)
                                    </td>
                                </tr>
                                <tr style="background-color: #f5f5f5;">
                                    <td colspan="2">
                                        <b>Força de Vendas</b>
                                    </td>
                                </tr>
                                <tr>
                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                        @if (Model.INDRCAVENDEDOR = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalRcaVendedor(id='#modalListRcaVendedor')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurar
                                                </Button>
                                            </td>
                                        Else
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalRcaVendedor(id='#modalListRcaVendedor')">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            </td>
                                        End If
                                    Else
                                        @if (Model.INDRCAVENDEDOR = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalRcaVendedor(id='#modalListRcaVendedor')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurado
                                                </Button>
                                                @*<a Class="btn btn-xs btn-warning" onclick="abrirModalRcaVendedor(id='#modalListRcaVendedor')">
                                                        <i Class="ace-icon fa fa-eye bigger-120"></i>
                                                    </a>*@
                                            </td>
                                        Else
                                            @<td Class="center"></td>
                                        End If

                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.INDRCAVENDEDOR)
                                    </td>
                                </tr>
                                <tr>
                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                        @If (Model.INDGERENTEVENDAS = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalGerenteVendas(id='#modalListGerenteVendas')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurar
                                                </Button>
                                            </td>
                                        Else
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalGerenteVendas(id='#modalListGerenteVendas')">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            </td>
                                        End If
                                    Else
                                        @If (Model.INDGERENTEVENDAS = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalGerenteVendas(id='#modalListGerenteVendas')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurado
                                                </Button>
                                                @*<a Class="btn btn-xs btn-warning" onclick="abrirModalGerenteVendas(id='#modalListGerenteVendas')">
                                                    <i Class="ace-icon fa fa-eye bigger-120"></i>
                                                </a>*@
                                            </td>
                                        Else
                                            @<td Class="center"></td>
                                        End If

                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.INDGERENTEVENDAS)
                                    </td>
                                </tr>
                                <tr>
                                    @if (ViewBag.PermiteEdicaoMecanica) Then
                                        @If (Model.INDGERENTEMERCADO = 1) Then
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-success" type="button" onclick="abrirModalGerenteMercado(id='#modalListGerenteMercado')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurar
                                                </Button>
                                            </td>
                                        Else
                                            @<td Class="center">
                                                <Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalGerenteMercado(id='#modalListGerenteMercado')">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            </td>
                                        End If
                                    Else
                                        @If (Model.INDGERENTEMERCADO = 1) Then
                                            @<td Class="center">

                                                <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalGerenteMercado(id='#modalListGerenteMercado')">
                                                    <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                    Configurado
                                                </Button>
                                                @*<a Class="btn btn-xs btn-warning" onclick="abrirModalGerenteMercado(id='#modalListGerenteMercado')">
                                                        <i Class="ace-icon fa fa-eye bigger-120"></i>
                                                    </a>*@
                                            </td>
                                        Else
                                            @<td Class="center"></td>
                                        End If

                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.INDGERENTEMERCADO)
                                    </td>
                                </tr>
                                <tr style="background-color: #f5f5f5;">
                                    <td colspan="2">
                                        <b>Nacional</b>
                                    </td>
                                </tr>
                                <tr>
                                    @if (Model.INDNACIONAL) Then
                                        @<td Class="center">
                                            <Button Class="btn btn-xs btn-white btn-success" type="button">
                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                Selecionado
                                            </Button>
                                        </td>
                                    Else
                                        @<td></td>
                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.INDNACIONAL)
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section Scripts
    <script type="text/javascript" src="~/Scripts/mecanicaPromocao.js"></script>
End Section



