﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaMixViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div style="width: auto; overflow-x: scroll;">
    <Table id="tblResultadoPesquisaMixFamiliaMercadoria" Class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
            <tr role="row">
                <th class="center">
                    <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="addPesquisaMixFamiliaMercadoria(@Newtonsoft.Json.JsonConvert.SerializeObject(Model.Filter))">
                        &nbsp;<i class="fa fa-plus bigger-120"></i>&nbsp;
                    </button>
                </th>
                <th>
                    Cód.
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESFMLMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESFMLMER", Model.Page.OrderBy, Model.ResultID)">
                    Nome Família Mercadoria
                </th>
                <th>
                    Grupo Mercadoria
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr Class="trResultado">
                    @Html.HiddenFor(Function(r) item.CODGRPMER)
                    <td class="center">
                        <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="salvarMixFamiliaMercadoria(this)">
                            <i class="ace-icon fa fa-plus bigger-120 info"></i>
                        </button>
                    </td>
                    <td>
                        @item.CODFMLMER
                    </td>
                    <td>
                        @item.DESFMLMER
                    </td>
                    <td>
                        @item.CODGRPMER - @item.DESGRPMER
                    </td>
                </tr>
            Next
        </tbody>
    </Table>
</div>
<br class="clearfix" />
@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code