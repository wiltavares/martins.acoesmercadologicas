﻿@ModelType  List(Of RelacaoAcaoMecanicaPublicoAlvoGerenteVendasViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalListGerenteVendas" style="z-index: 1051;">

    <div Class="modal-dialog modal-lg">

        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Gerente Vendas
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">
                <div id="msgModalGerenteVendas">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div>
                    <Table id="tblModalGerenteVendas" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="center">
                                    @Html.CheckBox("chkGerenteVendas", New With {.onclick = "marcarOuDesmarcarTodosGerenteVendas()", .Id = "chkTodosGerenteVendas", .title = "Selecionar Todos"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoGerenteVendas))
                                </th>
                                <th>
                                    Código
                                </th>
                                <th>
                                    Nome
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @For Each item In Model
                                @Html.HiddenFor(Function(r) item.CODACOCMC, New With {.id = "hdfCODACOCMC"})
                                @Html.HiddenFor(Function(r) item.NUMSEQRGRACOCMC, New With {.id = "hdfNUMSEQRGRACOCMC"})
                                @<tr Class="trResultado">
                                    @if item.CODACOCMC IsNot Nothing Then
                                        @<td Class="center">
                                            @Html.CheckBox("SelecioneGerenteVendas", New With {.class = "marcaGerenteVendas", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoGerenteVendas))
                                        </td>
                                    Else
                                        @<td Class="center">
                                            @Html.CheckBox("SelecioneGerenteVendas", New With {.class = "marcaGerenteVendas"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoGerenteVendas))
                                        </td>
                                    End If
                                    <td>
                                        @Html.Label(item.CODGER)
                                    </td>
                                    <td>
                                        @Html.Label(item.NOMGER)
                                    </td>
                                </tr>
                            Next
                        </tbody>
                    </Table>
                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoGerenteVendas) Then
                        @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosGridGerenteVendas();">
                            <i Class="ace-icon fa fa-save bigger-120 green"></i>
                            Salvar
                        </Button>
                    End If
                    <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalGerenteVendas();">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Sair
                    </Button>
                </div>
            </div>
        </div>
    </div>
</div>
