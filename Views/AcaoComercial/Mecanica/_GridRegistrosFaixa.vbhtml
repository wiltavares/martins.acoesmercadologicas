﻿@ModelType RelacaoAcaoMecanicaViewModel
@Imports Martins.AcoesMercadologicas.Core
<div Class="row">
    <div Class="col-sm-6">
        @Html.Label("Forma de calculo:")
        @Html.DropDownListFor(Function(r) r.TIPEQCCALCTRPMC, New SelectList(ViewBag.formaDeCalculo, "tipeqccalctrpmc", "deseqccalctrpmc", Model.TIPEQCCALCTRPMC), New With {.class = "form-control", .disabled = "disabled"})
    </div>


    @If (Model.TIPPMC = 2) Then
        @<div id="divCriterio" Class="col-sm-6">
            @Html.Label("Critério:")
            @Html.DropDownListFor(Function(r) r.CODCTRPMC, New SelectList(ViewBag.criterio, "CODCTRPMC", "DESCTRPMC", Model.CODCTRPMC), New With {.class = "chosen-select form-control", .id = "drpCriterioPromocaoFaixa", .onChange = "GridRegistrosFaixaCODCTRPMCChange(this);"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
        </div>
    End If
</div>
<div class="row">
    <div class="col-sm-6">
        @Html.Label(" Parametros de Cálculo:")
    </div>
</div>
@code
    Dim DisplayCashBack As String = "display: none"
    Dim DisplayCashBackBrinde As String = ""
    Dim DisplayVlrMax As String = "display: none"
    Dim DisplayHeader As String = "Percentual Cash Back por Atingimento "
    If (Model.INDTIPPTOPMC <> 0) Then
        DisplayCashBack = ""
        DisplayCashBackBrinde = "display: none"
    End If
    If (Model.INDTIPPTOPMC = 2) Then
        DisplayHeader = "Valor Cash Back por Atingimento "
    End If
    If (Model.INDTIPPTOPMC = 1) Then
        DisplayVlrMax = ""
    End If

End Code
<table id="tblGridRegistrosFaixa" class="table table-striped table-bordered table-hover dataTable" role="grid">
    <thead>
        <tr role="row">
            <th>
                Faixa
            </th>
            <th>
                Início
            </th>
            <th>

            </th>
            <th>
                Fim
            </th>
            <th id="cashback" style="@DisplayCashBack">
                @DisplayHeader
            </th>
        </tr>
    </thead>
    <tbody>
        @If Model.RELACAOACAOMECANICAREGISTROSFAIXALIST IsNot Nothing Then
            @For Each item In Model.RELACAOACAOMECANICAREGISTROSFAIXALIST
                @<tr>
                    <td Class="center">
                        @Html.DisplayFor(Function(r) item.TIPEDENIVPRMPMC)
                    </td>
                    <td Class="center">
                        @If (item.CODCTRPMC = 2) Then
                            @Html.TextBoxFor(Function(r) item.VLRLIMMNMFXACTRPMC, "{0:0}", New With {.alt = "inteiro"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                        ElseIf (item.CODCTRPMC = 5) Then
                            @Html.TextBoxFor(Function(r) item.VLRLIMMNMFXACTRPMC, "{0:0.00}", New With {.alt = "decimal_positivo_2"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                        End If
                    </td>
                    <td Class="center">
                        <b> a</b>
                    </td>
                    <td Class="center">
                        @If (item.VLRLIMMAXFXACTRPMC >= 9999999999) Then
                            @Html.TextBox("Acima", "Acima", New With {.readonly = "readonly"})
                        Else
                            @If (item.CODCTRPMC = 2) Then
                                @Html.TextBoxFor(Function(r) item.VLRLIMMAXFXACTRPMC, "{0:0}", New With {.alt = "inteiro"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                            ElseIf (item.CODCTRPMC = 5) Then
                                @Html.TextBoxFor(Function(r) item.VLRLIMMAXFXACTRPMC, "{0:0.00}", New With {.alt = "decimal_positivo_2"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                            End If
                        End If
                    </td>
                    <td Class="center cashbackCampo" style="@DisplayCashBack">
                        @Html.TextBoxFor(Function(r) item.QDEPTOFXACTRPMC, "{0:0.00}", New With {.Class = "form-control type_dec", .alt = "decimal_positivo_2", .maxlength = "15"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                    </td>
                </tr>
            Next
        End If
    </tbody>
</table>
<br />
<div Class="row tipoPremio">
    <div Class="col-sm-4">
        @Html.Label("Tipo Prêmio:", New With {.id = "lblTipoPremio"})
        @Html.DropDownListFor(Function(r) r.INDTIPPTOPMC, New SelectList(ViewBag.tipoPremio, "Value", "Text", Model.INDTIPPTOPMC), New With {.Class = "form-control", .onchange = "trocarMascaraFaixaCashBack();"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
    </div>
    <div Class="col-sm-4" style="@DisplayCashBackBrinde" id="primeiraIteracao">
        @Html.Label("Qtd de Prêmio por Atingimento (*):", New With {.id = "lblBrindes"})
        @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, New With {.class = "form-control", .id = "txtQtdPremioAtingimentoCriterioFaixa", .alt = "inteiro_positivo", .maxlength = "15"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
    </div>
</div>
<div class="row tipoMaxFxa" style="@DisplayVlrMax">
    <div class="col-sm-6">
        @Html.Label("Limitado a:", New With {.id = "lblMaxFxa"})
        @Html.TextBoxFor(Function(r) r.VLRLIMMAXCTR, "{0:0.00}", New With {.Class = "form-control", .id = "txtVLRLIMMAXCTR", .alt = "decimal_positivo_2", .maxlength = "15"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
    </div>
</div>
