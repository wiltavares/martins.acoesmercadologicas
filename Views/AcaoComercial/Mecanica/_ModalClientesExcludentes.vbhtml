﻿@ModelType  RelacaoAcaoMecanicaClientesExcludentesViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalListClientesExcludentes" style="z-index: 1051;">

    <div Class="modal-dialog modal-full">

        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp; Clientes Excludentes
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">
                <div id="msgModalClientesExcludentes">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        @if (ViewBag.PermiteEdicaoMecanicaClientesExcludentes) Then
                            @<div id="divLoadPesquisaClientesExcludentes">
                                Carregar o grid de resultado
                            </div>

                            Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsClientesExcludentes", .Controller = "RelacaoAcaoMecanicaClientesExcludentes", .EnderecoTemplate = "ClientesExcludentes.xls"})

                        End If

                    </div>

                    @if (ViewBag.PermiteEdicaoMecanicaClientesExcludentes) Then
                        @<div Class="col-xs-6" id="divLoadClientesExcludentes">

                        </div>
                    Else
                        @<div Class="col-xs-12" id="divLoadClientesExcludentes">

                        </div>
                    End If

                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalClientesExcludentes();">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Sair
                    </Button>
                </div>
            </div>
        </div>
    </div>
</div>
