﻿@ModelType List(Of RelacaoAcaoMecanicaMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Mercadorias Selecionadas
                </h4>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divMercadoriaSelecionadas" style="width: auto; overflow-x: scroll;">
                        <Table id="tblMercadoriaSelecionada" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                            <thead>
                                <tr role="row">
                                    @if (ViewBag.PermiteEdicaoMecanicaMixMercadoria) Then
                                        @<th Class="center">
                                            <button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteAllMixMercadoria()" title="Excluir todos">
                                                <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </button>
                                        </th>
                                    End If
                                    @If (Model.Any(Function(fx) fx.CODGRPMIXMERPMC > 0)) Then
                                        @<th>
                                            Grupo
                                        </th>
                                    End If
                                    <th>
                                        Cód.
                                    </th>
                                    <th>
                                        Nome Mercadoria
                                    </th>
                                    <th>
                                        Grupo Mercadoria
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @for Each item In Model
                                    @<tr Class="trResultado">
                                        @Html.HiddenFor(Function(r) item.NUMSEQMIXACOCMC)
                                        @Html.HiddenFor(Function(r) item.CODGRPMER)

                                        @If (ViewBag.PermiteEdicaoMecanicaMixMercadoria) Then
                                            @<td Class="center">
                                                <button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteMixMercadoria(this)">
                                                    &nbsp;<i Class="fa fa-trash-o bigger-120"></i>&nbsp;
                                                </button>
                                            </td>
                                        End If
                                        @If (Model.Any(Function(fx) fx.CODGRPMIXMERPMC > 0)) Then
                                            @<td>
                                                @item.CODGRPMIXMERPMC
                                            </td>
                                        End If

                                        <td>
                                            @item.CODMER
                                        </td>
                                        <td>
                                            @item.DESMER
                                        </td>
                                        <td>
                                            @item.CODGRPMER - @item.DESGRPMER
                                        </td>
                                    </tr>
                                Next
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
