﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaMixViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div style="width: auto; overflow-x: scroll; ">
    <div style="min-width: 1200px;">
        <Table id="tblResultadoPesquisaMixMercadoria" Class="table table-striped table-bordered table-hover dataTable" role="grid">
            <thead>
                <tr role="row">
                    <th class="center">
                        <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="addPesquisaMixMercadoria(@Newtonsoft.Json.JsonConvert.SerializeObject(Model.Filter))">
                            &nbsp;<i class="fa fa-plus bigger-120"></i>&nbsp;
                        </button>
                    </th>
                    <th>
                        Cód.
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("DESMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESMER", Model.Page.OrderBy, Model.ResultID)">
                        Nome Mercadoria
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("CODGRPMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODGRPMER", Model.Page.OrderBy, Model.ResultID)">
                        Grupo Mercadoria
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("CODFMLMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFMLMER", Model.Page.OrderBy, Model.ResultID)">
                        Família Mercadoria
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("CODCLSMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODCLSMER", Model.Page.OrderBy, Model.ResultID)">
                        Classe Mercadoria
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy, Model.ResultID)">
                        Fornecedor
                    </th>
                </tr>
            </thead>
            <tbody>
                @For Each item In Model.ListPaged
                    @<tr Class="trResultado">
                        @Html.HiddenFor(Function(r) item.CODGRPMER)
                        <td class="center">
                            <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="salvarMixMercadoria(this)">
                                &nbsp;<i class="fa fa-plus bigger-120"></i>&nbsp;
                            </button>
                        </td>
                        <td>
                            @item.CODMER
                        </td>
                        <td>
                            @item.DESMER
                        </td>
                        <td>
                            @item.CODGRPMER - @item.DESGRPMER
                        </td>
                        <td>
                            @item.CODFMLMER - @item.DESFMLMER
                        </td>
                        <td>
                            @item.CODCLSMER - @item.DESCLSMER
                        </td>
                        <td>
                            @item.CODFRN - @item.NOMFRN
                        </td>
                    </tr>
                Next
            </tbody>
        </Table>
    </div>
</div>
<br class="clearfix" />
@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code