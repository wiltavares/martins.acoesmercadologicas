﻿@ModelType  RelacaoAcaoMecanicaMixViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalMecanicaMixMercadoria" style="z-index: 1051;">

    <div Class="modal-dialog modal-full">

        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp; Mercadoria
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">
                <div id="msgModalMecanicaMixMercadoria">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        @if (ViewBag.PermiteEdicaoMecanicaMixMercadoria) Then
                            @<div id="divLoadPesquisaMixMercadoria">

                            </div>
                            @<div id="divLoadPesquisaMixMercadoriaPorPromocao">

                            </div>

                            Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsMixMercadoria", .Controller = "RelacaoAcaoMecanicaMix", .EnderecoTemplate = "MixMercadoria.xls"})

                        End If
                    </div>

                    @if (ViewBag.PermiteEdicaoMecanicaMixMercadoria) Then
                        @<div class="col-xs-6" id="divMixMercadoriaAdicionados">

                        </div>
                    Else
                        @<div class="col-xs-12" id="divMixMercadoriaAdicionados">

                        </div>
                    End If

                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalMixMercadoria();">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Sair
                    </Button>
                </div>
            </div>
        </div>
    </div>
</div>
