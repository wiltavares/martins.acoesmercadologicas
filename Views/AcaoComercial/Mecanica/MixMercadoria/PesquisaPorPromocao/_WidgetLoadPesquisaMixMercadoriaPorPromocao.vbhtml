﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

@code
    Dim ajaxOptions As New AjaxOptions() With {
        .UpdateTargetId = "gridItensPesquisaMixMercadoriaPorPromocao",
        .InsertionMode = InsertionMode.Replace,
        .OnBegin = "LoadingOn",
        .OnComplete = "MixMercadoriaPorPromocaoLoadingOff_UpdateMessage",
        .HttpMethod = "POST"
        }

End Code

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Pesquisa Mercadoria
                </h4>
                <div class="widget-toolbar">
                    <div class="btn-group ">
                        <a class="btn-md  btn btn-primary btn-white" style="" id="btnTogglePesquisaMixMercadoria" onclick="togglePesquisaMixMercadoria('ParaPesquisaPadrao')">
                            <i class="ace-icon fa fa-exchange bigger-120"></i>
                            Voltar
                        </a>
                    </div>
                </div>
            </div>
            <div Class="widget-body">
                <div id="msgWidgetMixMercadoria">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div Class="widget-main">
                    <div id="divResultadoPesquisaMixMercadoria">
                        @Using (Ajax.BeginForm("PesquisaMixMercadoriaPorPromocao", "RelacaoAcaoMecanicaMix", ajaxOptions, New With {.id = "frmPesquisaMixMercadoriaPorPromocao"}))
                            @Html.AntiForgeryToken()

                            @Html.HiddenFor(Function(r) Model.Filter.CODACOCMC)
                            @Html.HiddenFor(Function(r) Model.Filter.NUMSEQRGRACOCMC)

                            @<div class="row">
                                <div class="col-sm-6">
                                    @Html.Label("Código Promoção:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODPMC, New With {.class = "form-control"})
                                    </div>
                                </div>
                                <div Class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <br />
                                    <Button type="submit" Class="btn btn-white btn-info">
                                        <i Class="ace-icon fa fa-search bigger-120 info"></i>
                                        Pesquisar
                                    </Button>
                                </div>
                                @if (ViewBag.QTDFXAGRP > 0) Then
                                    @<div Class="col-sm-3">
                                        @code
                                            Dim grupos = New List(Of SelectListItem)()
                                            For index = 1 To ViewBag.QTDFXAGRP
                                                grupos.Add(New SelectListItem() With {.Text = index, .Value = index})
                                            Next
                                        End Code
                                        @if (ViewBag.TIPPMC = 1) Then
                                            @Html.Label("Grupo:")
                                            @<br />
                                            @Html.DropDownListFor(Function(Model) Model.Filter.CODGRPMIXMERPMC, New SelectList(grupos, "VALUE", "TEXT", Model.Filter.CODGRPMIXMERPMC), New With {.id = "drpAddRelacaoAcaoMecanicaMixFaixaPorPromocao", .Class = "form-control"})
                                        End If
                                    </div>
                                End If
                            </div>
                        End Using

                        <br />
                        <div id="gridItensPesquisaMixMercadoriaPorPromocao">
                            @Code
                                Html.RenderPartial("~/Views/AcaoComercial/Mecanica/MixMercadoria/PesquisaPorPromocao/_GridPesquisaMixMercadoriaPorPromocao.vbhtml", Model)
                            End Code
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
