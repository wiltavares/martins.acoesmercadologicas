﻿
<div class="modal-dialog modal-lg" id="modalEditMecanicaPromocao_prot">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="fa fa-pencil bigger-120"></i>
            &nbsp;Mecânica
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-box widget-color-blue2">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title smaller white lighter">
                                <i class="ace-icon fa fa-sliders bigger-110"></i>
                                Geral
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Descrição da mecânica</label>
                                        <textarea class="form-control" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Tipo:</label>
                                        <select class="chosen-select form-control">
                                            <option value="">Selecione... </option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Quantidade de Faixa</label>
                                        <text class="form-control"></text>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Participação Mix Total:</label>
                                        <br />
                                        <input id="rblParticipacaoMixTotalSim" type="radio" value="1" />
                                        <label for="rblParticipacaoMixTotalSim">Sim</label>
                                        <input id="rblParticipacaoMixTotalNao" type="radio" value="2" />
                                        <label for="rblParticipacaoMixTotalNao">Não</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-box widget-color-blue2">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title smaller white lighter">
                                <i class="ace-icon fa fa-sliders bigger-110"></i>
                                Filiais
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">

                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th class="center">
                                                    <input type="checkbox" title="Selecionar Todos" />
                                                </th>
                                                <th>
                                                    Codigo
                                                </th>
                                                <th>
                                                    Filial
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" checked />
                                                </td>
                                                <td>
                                                    1
                                                </td>
                                                <td>
                                                    Nome da filial 1
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" checked />
                                                </td>
                                                <td>
                                                    2
                                                </td>
                                                <td>
                                                    Nome da filial 2
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" />
                                                </td>
                                                <td>
                                                    3
                                                </td>
                                                <td>
                                                    Nome da filial 3
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" />
                                                </td>
                                                <td>
                                                    3
                                                </td>
                                                <td>
                                                    Nome da filial 3
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-box widget-color-blue2">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title smaller white lighter">
                                <i class="ace-icon fa fa-sliders bigger-110"></i>
                                Critério de apuração
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Forma de calculo:</label>
                                        <select class="chosen-select form-control">
                                            <option value="">Selecione... </option>
                                            <option value="1">ESCALA PROGRESSIVA</option>
                                            <option value="2">FAIXAS FINITAS</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Critério:</label>
                                        <select class="chosen-select form-control">
                                            <option value="">Selecione... </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Parametros de Cálculo:</label>
                                        <div style="width: auto; overflow-x: scroll; overflow-y:hidden;">
                                            <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                                                <thead>
                                                    <tr role="row">
                                                        <th style="width:33.33%">
                                                            MIX
                                                        </th>
                                                        <th style="width:33.33%">
                                                            Tipo Critério
                                                        </th>
                                                        <th style="width:33.33%">
                                                            Critério
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="center">
                                                            nome do critério
                                                        </td>
                                                        <td>
                                                            <select class="chosen-select form-control" style="width:300px;">
                                                                <option value="">Selecione... </option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="center">
                                                            nome do critério
                                                        </td>
                                                        <td>
                                                            <select class="chosen-select form-control" style="width:300px;">
                                                                <option value="">Selecione... </option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="center">
                                                            nome do critério
                                                        </td>
                                                        <td>
                                                            <select class="chosen-select form-control" style="width:300px;">
                                                                <option value="">Selecione... </option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-10">
                                        <label style="float:right; margin-top:8px;">Qde de brindes por atingimento:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-box widget-color-blue2">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title smaller white lighter">
                                <i class="ace-icon fa fa-sliders bigger-110"></i>
                                Publico Alvo
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">

                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th class="center">

                                                </th>
                                                <th>
                                                    Publico alvo
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr style="background-color: #f5f5f5;">
                                                <td colspan="2">
                                                    <b>Região</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    Estado
                                                </td>
                                            </tr>
                                            <tr style="background-color: #f5f5f5;">
                                                <td colspan="2">
                                                    <b>Clientes</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-warning" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-square-o bigger-119"></i>
                                                        Selecionar
                                                    </button>
                                                </td>
                                                <td>
                                                    Atividade
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    Grupamento de Clientes
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    Negocio Clientes
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    Segmento Clientes
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    Cliente
                                                </td>
                                            </tr>
                                            <tr style="background-color: #f5f5f5;">
                                                <td colspan="2">
                                                    <b>Força de Vendas</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    RCA/Vendedor
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    Gerente Regional
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    Gerente de Vendas
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    Nacional
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-box widget-color-blue2">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title smaller white lighter">
                                <i class="ace-icon fa fa-sliders bigger-110"></i>
                                Mix
                            </h4>
                            <button class="btn btn-xs btn-white btn-yellow" style="float: right; color: white; margin-top: 2.5px; margin-right: 3px;" type="button" onclick=" $('#modalListTestarMercadoria').modal('show'); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                Testar
                            </button>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>

                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    Grupo Mercadoria
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-warning" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-square-o bigger-119"></i>
                                                        Selecionar
                                                    </button>
                                                </td>
                                                <td>
                                                    Família Mercadoria
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-warning" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-square-o bigger-119"></i>
                                                        Selecionar
                                                    </button>
                                                </td>
                                                <td>
                                                    Classe Mercadoria
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    Mercadoria
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-white btn-success" type="button" onclick=" $('#modalListPublicoAlvoExemplo').show(); ChosenResize(); $('#modalEditMecanicaPromocao').hide();">
                                                        <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                        Configurar
                                                    </button>
                                                </td>
                                                <td>
                                                    Fornecedor
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <button class="btn btn-white btn-success" type="button" onclick="$('#modalEditMecanicaPromocao_prot').hide(); $('#modalListMecanicaPromocao').show();">
                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                    Salvar Mecânica
                </button>
                <button class="btn btn-danger btn-white" type="button" onclick="$('#modalEditMecanicaPromocao_prot').hide(); $('#modalListMecanicaPromocao').show();">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Sair
                </button>
            </div>
        </div>
    </div>
</div>



<div class="modal-dialog modal-lg" id="modalListPublicoAlvoExemplo" style="display:none;">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Selecionar [Tipo de publico alvo]
        </div>
        <div class="modal-body">
            <div class="modal-body">

                <div class="row">
                    <div class="col-xs-12">
                        <label for="item">Item Orçamento:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <input class="form-control" data-val="true" data-val-number="The field CODMER must be a number." id="idItem" name="Filter.CODMER" type="text" value="">
                        <label class="text-danger-message-login" for="lblErroTamanhoIdMercadoria" id="lblErroTamanhoIdMercadoria" style="display: none;">Código do item pode ter até 10 digitos.</label>
                    </div>
                    <div class="col-lg-8 col-sm-6">
                        <div class="input-group">
                            <input class="form-control" id="DesIte" name="Filter.DESMER" type="text" value="">
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-white" type="button" onclick="btnBuscaItens()">
                                    <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div id="resultadosBuscaItens">
                    <div class="widget-box widget-color-blue2 ">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title smaller white lighter">
                                <i class="ace-icon fa fa-list bigger-110"></i>
                                Resultados
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                                        <thead>
                                            <tr role="row">
                                                <th class="center">Selecionar</th>
                                                <th class="sorting" data-href="javascript:ordenarGrid_resultadosBuscaItens('CODMERC')">
                                                    Código
                                                </th>
                                                <th class="sorting" data-href="javascript:ordenarGrid_resultadosBuscaItens('DESMERC')">
                                                    Nome
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center">
                                                    <div>
                                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                            <i class="ace-icon fa fa-check bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                                <td>
                                                    4787
                                                </td>
                                                <td>
                                                    Nome do item
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <div>
                                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-default" style="width: 32px;">
                                                            <i class="ace-icon fa fa-square-o bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                                <td>
                                                    4787
                                                </td>
                                                <td>
                                                    Nome do item
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <div>
                                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-default" style="width: 32px;">
                                                            <i class="ace-icon fa fa-square-o bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                                <td>
                                                    4787
                                                </td>
                                                <td>
                                                    Nome do item
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <div>
                                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-default" style="width: 32px;">
                                                            <i class="ace-icon fa fa-square-o bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                                <td>
                                                    4787
                                                </td>
                                                <td>
                                                    Nome do item
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <div>
                                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                            <i class="ace-icon fa fa-check bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                                <td>
                                                    4787
                                                </td>
                                                <td>
                                                    Nome do item
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <div>
                                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                            <i class="ace-icon fa fa-check bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                                <td>
                                                    4787
                                                </td>
                                                <td>
                                                    Nome do item
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <div>
                                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                            <i class="ace-icon fa fa-check bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                                <td>
                                                    4787
                                                </td>
                                                <td>
                                                    Nome do item
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <div>
                                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                            <i class="ace-icon fa fa-check bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                                <td>
                                                    4787
                                                </td>
                                                <td>
                                                    Nome do item
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <div>
                                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                            <i class="ace-icon fa fa-check bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                                <td>
                                                    4787
                                                </td>
                                                <td>
                                                    Nome do item
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <div>
                                                        <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                            <i class="ace-icon fa fa-check bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                                <td>
                                                    4787
                                                </td>
                                                <td>
                                                    Nome do item
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br class="clearfix">


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <button class="btn btn-success btn-white " type="button">
                    <i class="ace-icon fa fa-file-excel-o bigger-120 success"></i>
                    Importar CSV
                </button>
                <button class="btn btn-info btn-white" type="button" onclick="$('#modalEditMecanicaPromocao').show();ChosenResize(); $('#modalListPublicoAlvoExemplo').hide();">
                    <i class="ace-icon fa fa-check bigger-120 blue"></i>
                    Concluir
                </button>
                <button class="btn btn-danger btn-white" type="button" onclick="$('#modalEditMecanicaPromocao').show();ChosenResize(); $('#modalListPublicoAlvoExemplo').hide();">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Sair
                </button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modalListTestarMercadoria" tabindex="-1" role="dialog" aria-labelledby="modalListTestarMercadoriaLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="widget-title smaller white lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Mercadorias
                </h4>
            </div>
            <div class="modal-body">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Informações:</label>
                            <p><b>Exibindo 10 mercadoria do GRUPO DE MERCADORIA.</b></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div id="gridItensOrcamento">

                            <div style="width: auto; overflow-x: scroll;">
                                <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" data-href="javascript:ordenarGrid_gridItensOrcamento('CODMERC')">
                                                Cod. Mercadoria
                                            </th>
                                            <th class="sorting" data-href="javascript:ordenarGrid_gridItensOrcamento('NOMMERC')">
                                                Nome Mercadoria
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1001
                                            </td>
                                            <td>
                                                Bicicleta
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1002
                                            </td>
                                            <td>
                                                Sabão em pó
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1003
                                            </td>
                                            <td>
                                                Bolacha
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1004
                                            </td>
                                            <td>
                                                Macarrão
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1005
                                            </td>
                                            <td>
                                                Óleo
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1006
                                            </td>
                                            <td>
                                                Feijão
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1007
                                            </td>
                                            <td>
                                                Extrato Elefante
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1008
                                            </td>
                                            <td>
                                                Sal
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1009
                                            </td>
                                            <td>
                                                Papel A4
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1010
                                            </td>
                                            <td>
                                                CD
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br class="clearfix">


                            <div class="row paginacao">
                                <div class="col-sm-4">
                                    <div data-toggle="buttons" id="paginacaoView" class="btn-group" onchange="atualizarPageSize_resultadosView()">
                                        <label class="btn btn-info btn-xs active">
                                            <input checked="checked" name="10" type="radio" value="10">
                                            10
                                        </label>
                                        <label class="btn btn-info btn-xs ">
                                            <input name="25" type="radio" value="25">
                                            25
                                        </label>
                                        <label class="btn btn-info btn-xs ">
                                            <input name="50Radio" type="radio" value="50">
                                            50
                                        </label>
                                        <label class="btn btn-info btn-xs ">
                                            <input name="100" type="radio" value="100">
                                            100
                                        </label>
                                    </div>
                                    <p>Exibindo 10 de 23</p>
                                </div>
                                <div class="col-sm-8">
                                    <div class="text-right">
                                        <div class="pagination-container"><ul class="pagination"><li class="active"><a>1</a></li><li><a href="javascript:mudarDePagina_resultadosView(2)">2</a></li><li><a href="javascript:mudarDePagina_resultadosView(3)">3</a></li><li class="PagedList-skipToNext"><a href="javascript:mudarDePagina_resultadosView(2)" rel="next">»</a></li></ul></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-white" type="button" onclick=" $('#modalEditMecanicaPromocao').show(); ChosenResize(); $('#modalListTestarMercadoria').modal('hide');">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Sair
                </button>
            </div>
        </div>
    </div>
</div>

