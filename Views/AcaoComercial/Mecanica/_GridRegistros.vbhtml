﻿@ModelType RelacaoAcaoMecanicaViewModel
@Imports Martins.AcoesMercadologicas.Core
<div Class="row">
    <div Class="col-sm-6">
        @Html.Label("Forma de calculo:")
        @Html.DropDownListFor(Function(r) r.TIPEQCCALCTRPMC, New SelectList(ViewBag.formaDeCalculo, "tipeqccalctrpmc", "deseqccalctrpmc", Model.TIPEQCCALCTRPMC), New With {.class = "chosen-select form-control", .onchange = "formaCalculoChange(this);"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
    </div>
    @If (Model.TIPPMC = 2) Then
        @<div id="divCriterio" Class="col-sm-6">
            @Html.Label("Critério:")
            @Html.DropDownListFor(Function(r) r.CODCTRPMC, New SelectList(ViewBag.criterio, "CODCTRPMC", "DESCTRPMC", Model.CODCTRPMC), New With {.class = "chosen-select form-control"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
        </div>
    End If
</div>
<div class="row">
    <div class="col-sm-6">
        @Html.Label(" Parametros de Cálculo:")
    </div>
</div>
<table id="tblGridRegistros" class="table table-striped table-bordered table-hover dataTable" role="grid">
    <thead>
        <tr role="row">
            <th>
                MIX
            </th>
            <th>
                Tipo Critério
            </th>
            <th>
                Critério
            </th>
        </tr>
    </thead>
    <tbody id="tbodyTeste">
        @If (Model.RELACAOACAOMECANICAREGISTROSLIST IsNot Nothing) Then
            @For Each item In Model.RELACAOACAOMECANICAREGISTROSLIST
                @<tr>
                    <td class="center">
                        @Html.Label("Único", New With {.class = "form-control", .id = "lblMixSimples"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                    </td>
                    <td>
                        @Html.DropDownListFor(Function(r) item.CODCTRPMC, New SelectList(ViewBag.criterio, "CODCTRPMC", "DESCTRPMC", item.CODCTRPMC), New With {.Class = "form-control", .id = "drpTipoCriterioSimples", .onchange = "tipoCriterioChange(this);"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                    </td>
                    <td>
                        @code
                            Dim styleVLRLIMMNMFXACTRPMC As String = ""
                            Dim styleTAMIVLFXAEQCCALCTR As String = ""
                            If (item.TIPEQCCALCTRPMC = 2) Then '2 = FAIXAS FINITAS
                                styleTAMIVLFXAEQCCALCTR = "display: none;"
                            Else '1 = ESCALA PROGRESSIVA
                                styleVLRLIMMNMFXACTRPMC = "display: none;"
                            End If
                        End Code

                        @If item.CODCTRPMC = 5 Then
                            @Html.TextBoxFor(Function(r) item.VLRLIMMNMFXACTRPMC, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .style = styleVLRLIMMNMFXACTRPMC}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                            @Html.TextBoxFor(Function(r) item.TAMIVLFXAEQCCALCTR, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .style = styleTAMIVLFXAEQCCALCTR}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                        Else
                            @Html.TextBoxFor(Function(r) item.VLRLIMMNMFXACTRPMC, New With {.class = "form-control", .alt = "inteiro_positivo", .maxlength = "10", .style = styleVLRLIMMNMFXACTRPMC}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                            @Html.TextBoxFor(Function(r) item.TAMIVLFXAEQCCALCTR, New With {.class = "form-control", .alt = "inteiro_positivo", .maxlength = "10", .style = styleTAMIVLFXAEQCCALCTR}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                        End If
                    </td>
                </tr>
            Next
        End If
    </tbody>
</table>
<br />
<div Class="row tipoPremio">
    <div Class="col-sm-6">
        @Html.Label("Tipo Prêmio:", New With {.id = "lblTipoPremio"})
        @Html.DropDownListFor(Function(r) r.INDTIPPTOPMC, New SelectList(ViewBag.tipoPremio, "Value", "Text", Model.INDTIPPTOPMC), New With {.Class = "form-control", .onchange = "trocarMascaraCashBack();"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
    </div>
    @If (Model.INDTIPPTOPMC = 0) Then
        @<div Class="col-sm-6">
            @Html.Label("Qtd de Prêmio por Atingimento:", New With {.id = "lblBrindes"})
            @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, New With {.class = "form-control", .id = "txtQtdPremioAtingimentoCriterioNormal", .alt = "inteiro_positivo", .maxlength = "15"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
        </div>
    Else
        @<div Class="col-sm-6">
            @Html.Label("Qtd de Prêmio por Atingimento:", New With {.id = "lblBrindes"})
            @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, "{0:0.00}", New With {.class = "form-control", .id = "txtQtdPremioAtingimentoCriterioNormal", .alt = "inteiro_positivo", .maxlength = "15"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
        </div>
    End If
</div>
<div class="row tipoMax">
    <div class="col-sm-6">
        @Html.Label("Limitado a:", New With {.id = "lblMax"})
        @Html.TextBoxFor(Function(r) r.VLRLIMMAXCTR, "{0:0.00}", New With {.Class = "form-control", .id = "txtVLRLIMMAXCTR", .alt = "decimal_positivo_2", .maxlength = "15"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
    </div>
</div>
