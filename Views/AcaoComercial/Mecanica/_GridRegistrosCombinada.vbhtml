﻿@ModelType RelacaoAcaoMecanicaViewModel
@Imports Martins.AcoesMercadologicas.Core
<div Class="row">
    <div Class="col-sm-6">
        @Html.Label("Forma de calculo:")
        @Html.DropDownListFor(Function(r) r.TIPEQCCALCTRPMC, New SelectList(ViewBag.formaDeCalculo, "tipeqccalctrpmc", "deseqccalctrpmc", Model.TIPEQCCALCTRPMC), New With {.class = "chosen-select form-control", .onchange = "formaCalculoCombinadaChange(this);"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
    </div>
    @If (Model.TIPPMC = 2) Then
        @<div id="divCriterio" Class="col-sm-6">
            @Html.Label("Critério:")
            @Html.DropDownListFor(Function(r) r.CODCTRPMC, New SelectList(ViewBag.criterio, "CODCTRPMC", "DESCTRPMC", Model.CODCTRPMC), New With {.class = "chosen-select form-control"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
        </div>
    End If
</div>
<div class="row">
    <div class="col-sm-6">
        @Html.Label(" Parametros de Cálculo:")
    </div>
</div>
<table id="tblGridRegistrosCombinada" class="table table-striped table-bordered table-hover dataTable" role="grid">
    <thead>
        <tr role="row">
            <th>
                MIX
            </th>
            <th>
                Tipo Critério
            </th>
            <th>
                Critério
            </th>
            <th>
                Descrição Mecânica
            </th>
        </tr>
    </thead>
    <tbody>
        @If Model.RELACAOACAOMECANICAREGISTROSCOMBINADALIST IsNot Nothing Then
            @For Each item In Model.RELACAOACAOMECANICAREGISTROSCOMBINADALIST
                @<tr>
                    <td Class="center">
                        @Html.DisplayFor(Function(r) item.TIPEDENIVPRMPMC)
                    </td>
                    <td>
                        @Html.DropDownListFor(Function(r) item.CODCTRPMC, New SelectList(ViewBag.criterio, "CODCTRPMC", "DESCTRPMC", item.CODCTRPMC), New With {.Class = "form-control", .onchange = "tipoCriterioChange(this);"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                    </td>
                    <td>
                        @code
                            Dim styleVLRLIMMNMFXACTRPMC As String = ""
                            Dim styleTAMIVLFXAEQCCALCTR As String = ""
                            If (item.TIPEQCCALCTRPMC = 2) Then '2 = FAIXAS FINITAS
                                styleTAMIVLFXAEQCCALCTR = "display: none;"
                            Else '1 = ESCALA PROGRESSIVA
                                styleVLRLIMMNMFXACTRPMC = "display: none;"
                            End If
                        End Code

                        @If item.CODCTRPMC = 5 Then
                            @Html.TextBoxFor(Function(r) item.VLRLIMMNMFXACTRPMC, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .style = styleVLRLIMMNMFXACTRPMC}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                            @Html.TextBoxFor(Function(r) item.TAMIVLFXAEQCCALCTR, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .style = styleTAMIVLFXAEQCCALCTR}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                        Else
                            @Html.TextBoxFor(Function(r) item.VLRLIMMNMFXACTRPMC, New With {.class = "form-control", .alt = "inteiro_positivo", .maxlength = "10", .style = styleVLRLIMMNMFXACTRPMC}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                            @Html.TextBoxFor(Function(r) item.TAMIVLFXAEQCCALCTR, New With {.class = "form-control", .alt = "inteiro_positivo", .maxlength = "10", .style = styleTAMIVLFXAEQCCALCTR}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                        End If
                    </td>
                    <td>
                        @Html.TextBoxFor(Function(r) item.DESCTRMIXPMC, New With {.class = "form-control desc"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                    </td>
                </tr>
            Next
        End If
    </tbody>
</table>
<br />
<div Class="row tipoPremio">
    <div Class="col-sm-4">
        @Html.Label("Tipo Prêmio:", New With {.id = "lblTipoPremio"})
        @Html.DropDownListFor(Function(r) r.INDTIPPTOPMC, New SelectList(ViewBag.tipoPremio, "Value", "Text", Model.INDTIPPTOPMC), New With {.Class = "form-control", .onchange = "trocarMascaraCashBack();"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
    </div>
    @If (Model.INDTIPPTOPMC = 0) Then
        @<div Class="col-sm-6">
            @Html.Label("Qtd de Prêmio por Atingimento:", New With {.id = "lblBrindes"})
            @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, New With {.class = "form-control", .id = "txtQtdPremioAtingimentoCriterioCombinada", .alt = "inteiro_positivo", .maxlength = "15"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
        </div>
    Else
        @<div Class="col-sm-6">
            @Html.Label("Qtd de Prêmio por Atingimento:", New With {.id = "lblBrindes"})
            @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, "{0:0.00}", New With {.class = "form-control", .id = "txtQtdPremioAtingimentoCriterioCombinada", .alt = "inteiro_positivo", .maxlength = "15"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
        </div>
    End If
</div>