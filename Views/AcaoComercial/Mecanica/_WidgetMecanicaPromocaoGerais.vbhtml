﻿@ModelType RelacaoAcaoMecanicaViewModel
@Imports Martins.AcoesMercadologicas.Core

@Using (Ajax.BeginForm("Save", "RelacaoAcaoMecanica", Constantes.AJAX_OBJECT_POST_DEFAULT, New With {.id = "frmWidgetMecanicaPromocaoGerais"}))
    @Html.AntiForgeryToken()
    @Html.HiddenFor(Function(r) r.CODACOCMC)
    @Html.HiddenFor(Function(r) r.NUMSEQRGRACOCMC)


    @<div Class="row">
        <div Class="col-xs-12">
            <div Class="widget-box widget-color-blue2">
                <div Class="widget-header widget-header-small">
                    <h4 Class="widget-title smaller white lighter">
                        <i Class="ace-icon fa fa-sliders bigger-110"></i>
                        Geral
                    </h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="1 ace-icon fa bigger-125 fa-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div Class="widget-body" id="divBodyMecanicaPromocaoWidgetGerais">
                    <div id="msgWidgetMecanicaPromocao">

                    </div>
                    <div Class="widget-main">
                        <div>
                            <div Class="row">
                                <div Class="col-xs-12">
                                    @Html.Label("DesMecanica", "Descrição Da Mecanica:")
                                    @Html.TextAreaFor(Function(r) r.DESRGRACOCMC, New With {.class = "form-control desc", .id = "DesMecanica", .maxlength = "180"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                                </div>
                            </div>
                            <div Class="row">
                                <div Class="col-sm-4">
                                    @Html.Label("TipoPromo", "Tipo:")
                                    @Html.DropDownListFor(Function(r) r.TIPPMC, New SelectList(ViewBag.ListaTipoPromoca, "CodTipoPromo", "NomTipoPromo", Model.TIPPMC), New With {.class = "chosen-select form-control", .id = "drpTipoPromocao", .onchange = "drpTipoPromocao_Change()"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                                </div>
                                <div id="divQtdGrupos" Class="col-sm-4">
                                    @Html.Label("QtdGrupos", "Quantidade Grupos:")
                                    @Html.TextBoxFor(Function(r) r.QDEGRPMIXPMC, New With {.class = "form-control numberInput", .id = "txtQtdGrupos", .onchange = "drpTipoPromocao_Change()", .alt = "inteiro_positivo", .maxlength = "2"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                                </div>
                                <div id="divQtdFaixas" Class="col-sm-4">
                                    @Html.Label("QtdFaixas", "Quantidade Faixas:")
                                    @Html.TextBoxFor(Function(r) r.QDEFXAPMC, New With {.class = "form-control numberInput", .id = "txtQtdFaixas", .onchange = "drpTipoPromocao_Change()", .alt = "inteiro_positivo", .maxlength = "2"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                                </div>
                                @*@If (ViewBag.PermiteEdicaoMecanica) Then
                                    End If*@
                                <div id="divrdbPartMixTotal" Class="col-sm-4">
                                    @Html.Label("PartMixTotal", "Participação Mix Total:")
                                    <br />
                                    @Html.RadioButtonFor(Function(Model) Model.INDPTCTOTMIXPMC, "1", New With {.onchange = "changeParticipacaoMixTotal(1)"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                                    <Label for="rblParticipacaoMixTotalSim"> Sim</Label>
                                    @Html.RadioButtonFor(Function(Model) Model.INDPTCTOTMIXPMC, "0", New With {.onchange = "changeParticipacaoMixTotal(0)"}.EnableIf(ViewBag.PermiteEdicaoMecanica))
                                    <Label for="rblParticipacaoMixTotalNao"> Não</Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
End Using