﻿@ModelType List(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    RCA / Vendedor Selecionados
                </h4>
                <div class="widget-toolbar">
                    <span>Total: @Model.Count()</span>
                </div>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divResultadoPesquisaRcaVendedor">
                        <Table id="tblResultadoPesquisaRcaVendedor" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                            <thead>
                                <tr role="row">
                                    @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoRcaVendedor) Then
                                        @<th Class="center">

                                            <button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteAllPublicoAlvoRcaVendedor()" title="Excluir todos">
                                                <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </button>
                                        </th>
                                    End If
                                    <th>
                                        Código
                                    </th>
                                    <th>
                                        Nome
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @for Each item In Model
                                    @<tr Class="trResultado">

                                        @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoRcaVendedor) Then
                                            @<td Class="center">
                                                <Button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeletePublicoAlvoRcaVendedor(this)">
                                                    <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </Button>
                                            </td>
                                        End If
                                        <td>
                                            @item.CODREP
                                        </td>
                                        <td>
                                            @item.NOMREP
                                        </td>
                                    </tr>
                                Next
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
