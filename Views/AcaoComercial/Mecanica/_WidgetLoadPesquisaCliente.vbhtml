﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoClienteViewModel)
@Imports Martins.AcoesMercadologicas.Core

@code
    Dim ajaxOptions As New AjaxOptions() With {
        .UpdateTargetId = "divGridItensPesquisaPublicoAlvoCliente",
        .InsertionMode = InsertionMode.Replace,
        .OnBegin = "LoadingOn",
        .OnComplete = "publicoAlvoClienteAjaxComplete",
        .HttpMethod = "POST"
        }

End Code

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Pesquisa Cliente
                </h4>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divResultadoPesquisaCliente">
                        @Using (Ajax.BeginForm("PesquisaMecanicaPromocaoWidgetLoadPesquisaCliente", "RelacaoAcaoMecanicaPublicoAlvoCliente", ajaxOptions, New With {.id = "frmPesquisaPublicoAlvoCliente"}))
                            @Html.AntiForgeryToken()

                            @Html.HiddenFor(Function(r) Model.Filter.CODACOCMC)
                            @Html.HiddenFor(Function(r) Model.Filter.NUMSEQRGRACOCMC)

                            @<div class="row">
                                <div Class="col-sm-6">
                                    @Html.Label("Cliente:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODCLI, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.NOMCLI, New With {.class = "form-control", .style = "width: 80%;"})
                                    </div>
                                </div>
                                <div Class="col-sm-6">
                                    @Html.Label("Representante Vendas:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODREP, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.NOMREP, New With {.class = "form-control", .style = "width: 80%;"})
                                    </div>
                                </div>
                            </div>
                            @<div Class="row">
                                @*<div Class="col-sm-6">
                                    @Html.Label("Gerente Vendas:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODGER, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.NOMGER, New With {.class = "form-control", .style = "width: 80%;"})
                                    </div>
                                </div>*@
                                <div Class="col-sm-6">
                                    @Html.Label("Gerente Mercado:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODSUP, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.NOMSUP, New With {.class = "form-control", .style = "width: 80%;"})
                                    </div>
                                </div>
                            </div>
                            @<br />
                            @<div Class="row">
                                <div Class="col-sm-12">
                                    <Button type="submit" Class="btn btn-white btn-info">
                                        <i Class="ace-icon fa fa-search bigger-120 info"></i>
                                        Pesquisar
                                    </Button>
                                </div>
                            </div>
                        End Using
                        <br />
                        <div id="divGridItensPesquisaPublicoAlvoCliente">
                            @code
                                Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_GridPesquisaPublicoAlvoCliente.vbhtml", Model)
                            End Code
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
