﻿@ModelType ImportacaoXLSViewModelo
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim AJAX_OBJECT_POST_PLANO As New AjaxOptions() With {
.UpdateTargetId = "modalResultImport",
.InsertionMode = InsertionMode.Replace,
.OnComplete = "AbreModalImport",
.HttpMethod = "POST"
}
End Code

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Importar Planilha
                </h4>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divResultadoPesquisaCliente">

                        @Code
                            Dim rota = Model.Controller + "/" + Model.Action
                        End Code
                        @Using (Ajax.BeginForm(Model.Action, Model.Controller, AJAX_OBJECT_POST_PLANO, New With {.enctype = "multipart/form-data", .action = "#", .id = "arquivo", .value = rota}))
                            @<div class="row">
                                <div class="col-lg-12 col-sm-12">
                                    <label class="ace-file-input">
                                        <input type="file" accept=".xls" name="file_upload" id="file_upload" class="ace-file-container" data-title="Choose" />
                                        <span Class="ace-file-container" data-title="Choose">
                                            <span Class="ace-file-name" data-title="Sem arquivo ...">
                                                <i Class=" ace-icon fa fa-upload">
                                                </i>
                                            </span>
                                        </span>
                                        <a Class="remove" href="#">
                                            <i Class=" ace-icon fa fa-times">
                                            </i>
                                        </a>
                                    </label>
                                </div>
                            </div>
                            @<div class="row">
                                <div class="col-sm-12">
                                    <Button type="submit" name="arquivo" id="btnImportarXLS" value="Importar" Class="btn btn-info btn-white">
                                        <i Class="ace-icon fa fa-cloud-upload bigger-120"></i>
                                        Importar
                                    </Button>
                                    <a href="@Url.Action(Model.EnderecoTemplate, "TemplateXLS")" Class="btn btn-grey btn-white">
                                        <i Class="ace-icon fa fa-cloud-download bigger-120"></i>
                                        Template
                                    </a>
                                </div>
                            </div>
                        End Using
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    ModalImportXls();
</script>