﻿@ModelType RelacaoAcaoMecanicaMixViewModel
@Imports Martins.AcoesMercadologicas.Core

<div class="col-sm-12">
    @Html.Label("Categoria:")
    @Html.DropDownListFor(Function(r) r.CODFMLMER, New SelectList(ViewBag.itensDropGrupoMercadoriaCategoria, "CODFMLMER", "DESFMLMER", Model.CODFMLMER), New With {.class = "chosen-select form-control", .id = "drpMixCategoria", .onchange = "drpMixCategoria_Change()"}.EnableIf(ViewBag.PermiteEdicaoMecanicaMixGrupoMercadoria))
</div>