﻿@ModelType  RelacaoAcaoMecanicaPublicoAlvoClienteViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalListCliente" style="z-index: 1051;">

    <div Class="modal-dialog modal-full">

        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp; Clientes
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">
                <div id="msgModalCliente">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoCliente) Then
                            @<div id="divLoadPesquisaCliente">
                                Carregar o grid de resultado
                            </div>

                            Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsCliente", .Controller = "RelacaoAcaoMecanicaPublicoAlvoCliente", .EnderecoTemplate = "PublicoAlvoCliente.xls"})

                        End If

                    </div>

                    @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoCliente) Then
                        @<div Class="col-xs-6" id="divLoadPublicoAlvoCliente">

                        </div>
                    Else
                        @<div Class="col-xs-12" id="divLoadPublicoAlvoCliente">

                        </div>
                    End If

                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalCliente();">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Sair
                    </Button>
                </div>
            </div>
        </div>
    </div>
</div>
