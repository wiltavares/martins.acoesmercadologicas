﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalListGrupamentoCliente" style="z-index: 1051;">
    <div Class="modal-dialog modal-lg">
        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Grupamento Clientes
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">

                <div id="msgmodalListGrupamentoCliente">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoGrupamentoCliente) Then
                    @<div class="widget-box widget-color-blue collapsed">
                        <div class="widget-header widget-header-small">
                            <h4 Class="widget-title smaller white lighter">
                                <i Class="ace-icon fa fa-check-square-o bigger-110"></i>
                                Selecionar Por BU
                            </h4>
                            <div class="widget-toolbar">
                                <a href="#" data-action="collapse">
                                    <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div Class="row">
                                    @For Each item In ViewBag.TipoGrupoCliente
                                        @<div Class="col-sm-4">
                                            <input id="idCheckteste" class="chkBUGrupamentoCliente" type="checkbox" value="@item.CODUNDESRNGC" />
                                            @Html.Label(item.DESUNDESRNGC)
                                        </div>
                                    Next
                                    <button type="button" class="btn btn-xs btn-primary  btn-primary " style="float: right; margin-right:12px; " title="Aplicar" onclick="CarregarBuSelecionadoGrupamentoCliente()">
                                        <i class="ace-icon fa fa-check-square-o bigger-120 info"></i>
                                        Aplicar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                End If
                <br />
                <div id="divGridModalGrupamentoCliente">
                    @Code
                        Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_GridGrupamentoCliente.vbhtml", Model)
                    End Code
                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoGrupamentoCliente) Then
                        @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosGridGrupamentoCliente();">
                            <i Class="ace-icon fa fa-save bigger-120 green"></i>
                            Salvar
                        </Button>
                    End If
                    <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalGrupamentoCliente();">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Sair
                    </Button>
                </div>
            </div>
        </div>
    </div>
</div>
