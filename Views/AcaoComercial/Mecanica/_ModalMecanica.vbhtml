﻿@ModelType AcaoComercialViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim AJAX_OBJECT_POST_MECANICA As New AjaxOptions() With {
        .UpdateTargetId = "msgGeral",
        .OnBegin = "LoadingOn",
        .InsertionMode = InsertionMode.Replace,
        .OnComplete = "fimModal()",
        .HttpMethod = "POST"
    }
End Code
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Mecânica
        </div>
@Using (Ajax.BeginForm("AtualizarMecanica", "AcaoComercial", AJAX_OBJECT_POST_MECANICA))
    @Html.AntiForgeryToken()

        @<div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    @Html.TextAreaFor(Function(m) m.Mecanica.DESRGRACOCMC, New With {.Class = "form-control", .rows = "5"})
                </div>
            </div>
        </div>
        @<div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <button id="btnConcluir" type="submit" class="btn btn-white btn-success" onsubmit="LoadingOn()">
                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                    Concluir
                </button>
                <button class="btn btn-danger btn-white" type="button" onclick="FecharModalMecanicaAcao()">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Sair
                </button>
            </div>
        </div>
End Using
    </div>
</div>