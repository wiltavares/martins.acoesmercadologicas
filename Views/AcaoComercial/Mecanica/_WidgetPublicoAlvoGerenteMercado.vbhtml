﻿@ModelType List(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Gerente Mercado Selecionados
                </h4>
                <div class="widget-toolbar">
                    <span>Total: @Model.Count()</span>
                </div>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divResultadoPesquisaGerenteMercado">
                        <Table id="tblResultadoPesquisaGerenteMercado" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                            <thead>
                                <tr role="row">
                                    @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoGerenteMercado) Then
                                        @<th Class="center">
                                            <button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteAllPublicoAlvoGerenteMercado()" title="Excluir todos">
                                                <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </button>
                                        </th>
                                    End If
                                    <th>
                                        Código
                                    </th>
                                    <th>
                                        Nome
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @for Each item In Model
                                    @<tr Class="trResultado">

                                        @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoGerenteMercado) Then
                                            @<td class="center">
                                                <button type="button" class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeletePublicoAlvoGerenteMercado(this)">
                                                    <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </button>
                                            </td>
                                        End If
                                        <td>
                                            @item.CODSUP
                                        </td>
                                        <td>
                                            @item.NOMSUP
                                        </td>
                                    </tr>
                                Next
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
