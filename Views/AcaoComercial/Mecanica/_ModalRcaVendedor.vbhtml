﻿@ModelType  RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalListRcaVendedor" style="z-index: 1051;">

    <div Class="modal-dialog modal-full">

        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp; Rca/Vendedor
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">
                <div id="msgModalRcaVendedor">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoRcaVendedor) Then
                            @<div id="divLoadPesquisaRcaVendedor">

                            </div>

                            Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsRcaVendedor", .Controller = "RelacaoAcaoMecanicaPublicoAlvoRcaVendedor", .EnderecoTemplate = "PublicoAlvoRcaVendedor.xls"})

                        End If
                    </div>

                    @If (ViewBag.PermiteEdicaoMecanicaPublicoAlvoRcaVendedor) Then
                        @<div class="col-xs-6" id="divLoadPublicoAlvoRcaVendedor">

                        </div>
                    Else
                        @<div class="col-xs-12" id="divLoadPublicoAlvoRcaVendedor">

                        </div>
                    End If
                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalRcaVendedor();">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Sair
                    </Button>
                </div>
            </div>
        </div>
    </div>
</div>
