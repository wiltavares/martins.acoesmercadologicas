﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaClientesExcludentesViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div style="width: auto; overflow-x: scroll;">
    <Table id="tblResultadoPesquisaClientesExcludentes" Class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
            <tr role="row">
                @if (ViewBag.PermiteEdicaoMecanicaClientesExcludentes) Then
                    @<th Class="center">
                        <button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteAllClientesExcludentes()" title="Excluir todos">
                            <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                        </button>
                    </th>
                End If
                <th class="@Grid.AtribuirIconeOrdenacao("CODCLI", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODCLI", Model.Page.OrderBy, Model.ResultID)">
                    Código
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NOMCLI", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMCLI", Model.Page.OrderBy, Model.ResultID)">
                    Nome
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr Class="trResultado">
                    @if (ViewBag.PermiteEdicaoMecanicaClientesExcludentes) Then
                        @<td Class="center">
                            <button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteClientesExcludentes(this)">
                                <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                            </button>
                        </td>
                    End If
                    <td>
                        @Html.DisplayFor(Function(r) item.CODCLI)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(r) item.NOMCLI)
                    </td>
                </tr>
            Next
        </tbody>
    </Table>
</div>
<br class="clearfix" />
@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code

