﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

@code
    Dim ajaxOptions As New AjaxOptions() With {
        .UpdateTargetId = "gridItensPesquisaMixFornecedor",
        .InsertionMode = InsertionMode.Replace,
        .OnBegin = "LoadingOn",
        .OnComplete = "LoadingOff_UpdateMessage",
        .HttpMethod = "POST"
        }

End Code

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Pesquisa Fornecedor
                </h4>
            </div>
            <div Class="widget-body">
                <div id="msgWidgetMixFornecedor">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div Class="widget-main">
                    <div id="divResultadoPesquisaMixFornecedor">
                        @Using (Ajax.BeginForm("PesquisaMixFornecedor", "RelacaoAcaoMecanicaMix", ajaxOptions, New With {.id = "frmPesquisaMixFornecedor"}))
                            @Html.AntiForgeryToken()

                            @Html.HiddenFor(Function(r) Model.Filter.CODACOCMC)
                            @Html.HiddenFor(Function(r) Model.Filter.NUMSEQRGRACOCMC)

                            @<div class="row">
                                <div class="col-sm-12">
                                    @Html.Label("Fornecedor:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODFRN, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.NOMFRN, New With {.class = "form-control", .style = "width: 80%;"})
                                    </div>
                                    @*@Html.TextBoxFor(Function(r) Model.Filter.NOMFRN, New With {.class = "form-control", .id = "txtDesFornecedor"})*@
                                </div>
                            </div>
                            @<div class="row">
                                <div class="col-sm-6">
                                    @Html.Label("Diretoria:")
                                    <div>
                                        @Html.DropDownListFor(Function(Model) Model.Filter.CODDRTCMP, New SelectList(ViewBag.ListaDiretoriaCompra, "CODDRTCMP", "DESDRTCMP", Model.Filter.CODDRTCMP), "Selecione...", New With {.Class = "chosen-select form-control"})
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    @Html.Label("Celula:")
                                    <div>
                                        @Html.DropDownListFor(Function(Model) Model.Filter.CODDIVCMP, New SelectList(ViewBag.ListaRelacaoEquipeDeComprasxGerenteNegocioServCompras, "CODDIVCMP", "DESDIVCMP", Model.Filter.CODDIVCMP), "Selecione...", New With {.Class = "chosen-select form-control"})
                                    </div>
                                </div>
                            </div>
                            @<br />
                            @<div Class="row">
                                <div Class="col-sm-3">
                                    <Button type="submit" Class="btn btn-white btn-info">
                                        <i Class="ace-icon fa fa-search bigger-120 info"></i>
                                        Pesquisar
                                    </Button>
                                </div>
                            </div>
                        End Using

                        <br />
                        <div id="gridItensPesquisaMixFornecedor">
                            @Code
                                Html.RenderPartial("~/Views/AcaoComercial/Mecanica/MixFornecedor/_GridPesquisaMixFornecedor.vbhtml", Model)
                            End Code
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
