﻿@ModelType List(Of RelacaoAcaoMecanicaMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Fornecedores Selecionados
                </h4>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divFornecedorSelecionadas" style="width: auto; overflow-x: scroll;">
                        <Table id="tblFornecedorSelecionada" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                            <thead>
                                <tr role="row">
                                    @if (ViewBag.PermiteEdicaoMecanicaMixFornecedor) Then
                                        @<th Class="center">
                                            <button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteAllMixFornecedor()" title="Excluir todos">
                                                <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </button>
                                        </th>
                                    End If
                                    <th>
                                        Cód. Fornecedor
                                    </th>
                                    <th>
                                        Nome Fornecedor
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @for Each item In Model
                                    @<tr Class="trResultado">
                                        @Html.HiddenFor(Function(r) item.NUMSEQMIXACOCMC)

                                        @If (ViewBag.PermiteEdicaoMecanicaMixFornecedor) Then
                                            @<td Class="center">
                                                <Button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteMixFornecedor(this)">
                                                    &nbsp;<i Class="fa fa-trash-o bigger-120"></i>&nbsp;
                                                </Button>
                                            </td>
                                        End If
                                        <td>
                                            @item.CODFRN
                                        </td>
                                        <td>
                                            @item.NOMFRN
                                        </td>
                                    </tr>
                                Next
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
