﻿@ModelType  RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalListGerenteMercado" style="z-index: 1051;">

    <div Class="modal-dialog modal-full">

        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp; Gerente Mercado
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">
                <div id="msgModalGerenteMercado">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoGerenteMercado) Then
                            @<div id="divLoadPesquisaGerenteMercado">

                            </div>

                            Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsGerenteMercado", .Controller = "RelacaoAcaoMecanicaPublicoAlvoGerenteMercado", .EnderecoTemplate = "PublicoAlvoGerenteMercado.xls"})

                        End If
                    </div>

                    @if (ViewBag.PermiteEdicaoMecanicaPublicoAlvoGerenteMercado) Then
                        @<div class="col-xs-6" id="divLoadPublicoAlvoGerenteMercado">

                        </div>
                    Else
                        @<div class="col-xs-12" id="divLoadPublicoAlvoGerenteMercado">

                        </div>
                    End If

                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalGerenteMercado();">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Sair
                    </Button>
                </div>
            </div>
        </div>
    </div>
</div>
