﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaClientesExcludentesViewModel)
@Imports Martins.AcoesMercadologicas.Core

@code
    Dim ajaxOptions As New AjaxOptions() With {
        .UpdateTargetId = "divGridClientesExcludentes",
        .InsertionMode = InsertionMode.Replace,
        .OnBegin = "LoadingOn",
        .OnComplete = "publicoAlvoClienteAjaxComplete",
        .HttpMethod = "POST"
        }

End Code

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Clientes Excludentes Selecionado
                </h4>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div Class="widget-main">
                        <div id="divResultadoPesquisaClientesExcludentes">
                            @Using (Ajax.BeginForm("PesquisaClientesExcludentes", "RelacaoAcaoMecanicaClientesExcludentes", ajaxOptions, New With {.id = "frmPesquisaClientesExcludentes"}))
                                @Html.AntiForgeryToken()

                                @Html.HiddenFor(Function(r) Model.Filter.CODACOCMC)
                                @Html.HiddenFor(Function(r) Model.Filter.NUMSEQRGRACOCMC)

                                @<div class="row">
                                    <div Class="col-sm-9">
                                        @Html.Label("Cliente:")
                                        <div>
                                            @Html.TextBoxFor(Function(r) Model.Filter.CODCLI, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                            @Html.TextBoxFor(Function(r) Model.Filter.NOMCLI, New With {.class = "form-control", .style = "width: 80%;", .id = "txtNomCli"})
                                        </div>
                                    </div>
                                    <div Class="col-sm-3">
                                        <label>&nbsp;</label>
                                        <br />
                                        <Button type="submit" Class="btn btn-white btn-info">
                                            <i Class="ace-icon fa fa-search bigger-120 info"></i>
                                            Pesquisar
                                        </Button>
                                    </div>
                                </div>
                            End Using
                            <br />
                            <div id="divGridClientesExcludentes">
                                @code
                                    Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_GridClientesExcludentes.vbhtml", Model)
                                End Code
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
