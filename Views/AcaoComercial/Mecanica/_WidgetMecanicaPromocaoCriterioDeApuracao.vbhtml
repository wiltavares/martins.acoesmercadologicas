﻿@ModelType RelacaoAcaoMecanicaViewModel
@Imports Martins.AcoesMercadologicas.Core
@*@Using (Ajax.BeginForm("Save", "RelacaoAcaoMecanica", Constantes.AJAX_OBJECT_POST_DEFAULT, New With {.id = "frmWidgetMecanicaPromocaoGerais"}))
    @Html.AntiForgeryToken()*@

<div Class="row">
    <div Class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Critério de apuração
                </h4>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="1 ace-icon fa bigger-125 fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    
                    <div id="msgParametrosCalculoSimples">

                    </div>
                    @*<div Class="row">
                        <div Class="col-sm-6">
                            @Html.Label("Forma de calculo:")
                            @Html.DropDownListFor(Function(r) r.CodFormaCalculo, New SelectList(ViewBag.formaDeCalculo, "tipeqccalctrpmc", "deseqccalctrpmc", Model.CodFormaCalculo), New With {.class = "chosen-select form-control"})
                        </div>
                        @If (Model.TIPPMC = 2) Then
                            @<div id="divCriterio" Class="col-sm-6">
                                @Html.Label("Critério:")
                                @Html.DropDownListFor(Function(r) r.CODCTRPMC, New SelectList(ViewBag.criterio, "CODCTRPMC", "DESCTRPMC", Model.CODCTRPMC), New With {.class = "chosen-select form-control"})
                            </div>
                        End If
                    </div>*@
                    <div Class="row">
                        <div Class="col-sm-12">
                            @*<Label> Parametros de Cálculo:</Label>*@
                            <div id="divParametrosCalculoSimples" style="width: auto; overflow-x: scroll; overflow-y:hidden;">
                                
                            </div>
                        </div>
                    </div>

                    @*<div Class="row">
                        <div Class="col-sm-10">
                            @Html.Label("Qtd de Prêmio por Atingimento:", New With {.id = "lblBrindes", .Style = "float:right; margin-top:8px;"})
                        </div>
                        <div Class="col-sm-2">
                            @Html.TextBoxFor(Function(r) r.QtdBrindesAting, New With {.class = "form-control"})
                        </div>
                    </div>*@
                </div>
            </div>
        </div>
    </div>
</div>

