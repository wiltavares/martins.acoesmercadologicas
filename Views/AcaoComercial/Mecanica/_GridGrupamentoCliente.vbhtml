﻿@ModelType GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel)
@Imports Martins.AcoesMercadologicas.Core

<br class="clearfix" />
<div class="row">
    <div Class="btn-group pull-left">
        <Button Class="btn btn-white btn-info" onclick="marcarOuDesmarcarTodosGrupamentoCliente();">
            <i Class="ace-icon fa fa-check-square-o bigger-120 blue"></i>
            Selecionar Todos
        </Button>
    </div>
</div>
<div class="row">
    <div style="width: auto; overflow-x: scroll;">
        <Table id="tblModalGrupamentoCliente" Class="table table-striped table-bordered table-hover dataTable" role="grid">
            <thead>
                <tr role="row">
                    <th class="center @Grid.AtribuirIconeOrdenacao("CODACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODACOCMC", Model.Page.OrderBy, Model.ResultID)">
                        @*@Html.CheckBox("chkGrupamentoCliente", New With {.onclick = "marcarOuDesmarcarTodosGrupamentoCliente()", .Id = "chkTodosGrupamentoCliente", .title = "Selecionar Todos"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoGrupamentoCliente))*@
                        <input id="chkTodosGrupamentoCliente" name="chkTodosGrupamentoCliente" type="hidden" value="0">
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("CODGRPCLI", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODGRPCLI", Model.Page.OrderBy, Model.ResultID)">
                        Código
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("DESGRPCLI", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESGRPCLI", Model.Page.OrderBy, Model.ResultID)">
                        Nome
                    </th>
                </tr>
            </thead>
            <tbody>
                @For Each item In Model.ListPaged
                    @<tr Class="trResultado">
                        <td Class="center">
                            @Html.HiddenFor(Function(r) item.CODGRPCLI, New With {.class = "CODGRPCLI"})
                            @If item.CODACOCMC IsNot Nothing AndAlso item.NUMSEQRGRACOCMC IsNot Nothing Then
                                @Html.CheckBox("SelecioneGrupamentoCliente", New With {.class = "marcaGrupamentoCliente", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoGrupamentoCliente))
                            Else
                                @Html.CheckBox("SelecioneGrupamentoCliente", New With {.class = "marcaGrupamentoCliente"}.EnableIf(ViewBag.PermiteEdicaoMecanicaPublicoAlvoGrupamentoCliente))
                            End If
                        </td>
                        <td>
                            @item.CODGRPCLI
                        </td>
                        <td>
                            @Html.Label("NomeGrupamentoCliente", item.DESGRPCLI)
                        </td>
                    </tr>
                Next
            </tbody>
        </Table>
    </div>
</div>

<br class="clearfix" />
<div style="display:none">
    @Code
        Html.RenderPartial(Constantes.VIEW_PAGINACAO)
    End Code
</div>

