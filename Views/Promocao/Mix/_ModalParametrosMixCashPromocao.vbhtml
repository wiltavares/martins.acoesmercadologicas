﻿@ModelType GridSettings(Of PromocaoMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

@Code
    Dim ajaxObject As New AjaxOptions() With {
    .OnBegin = "LoadingOn",
    .HttpMethod = "POST",
    .OnSuccess = "editPromocao_Success",
    .OnFailure = "LoadingOff_UpdateMessage"
        }
End Code

@Html.HiddenFor(Function(Model) Model.Filter.INDCPTFCH, New With {.id = "hdfIndicadorComboFechado"})
@Html.HiddenFor(Function(Model) Model.Filter.CODPMC, New With {.id = "hdfCodPmc"})
@Html.HiddenFor(Function(Model) Model.Filter.TIPFRNACOCMC, New With {.id = "hdfTipFrnAcoCmc"})

<div Class="modal-dialog modal-full hideScroll" id="modalParametrosMixCashPromocao" style="z-index: 1051; margin-top: 4px;">
    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Parâmetros Mix Promoção
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="divAlertaParametrosMixCashPromocao">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>

            @Code
                Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsParametrosCash", .Controller = "PromocaoParametrosMix", .EnderecoTemplate = "ParametroMixCash.xls"})
            End Code

            <div class="form-horizontal">
                <div class="widget-box widget-color-blue2">

                    <div Class="widget-header widget-header-small">
                        <h4 Class="widget-title smaller white lighter">
                            <i Class="ace-icon fa fa-sliders bigger-110"></i>
                        </h4>
                        <div Class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i Class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                            </a>
                        </div>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            <div>
                                <div class="row">
                                    <div Class="col-lg-3 col-sm-3">
                                        @Html.Label("lblQtdMinima", "Quantidade Minima (*):")
                                        <br />
                                        @Code
                                            Model.Filter.QDEMNMMERMIX = 0
                                        End Code
                                        @Html.TextBoxFor(Function(Model) Model.Filter.QDEMNMMERMIX, New With {.Class = "input-sm date-interval form-control", .id = "txtQtdMinima", .autocomplete = "off", .alt = "inteiro_positivo", .onblur = "defaultZero($(this))"})
                                    </div>

                                    <div class="col-lg-2 col-sm-2">
                                        @Html.Label("lblItemObrigatorio", "Item Obrigatório:")
                                        <br />
                                        <div class="row">
                                            @Code
                                                Dim htmlAttSim As New Dictionary(Of String, Object)
                                                htmlAttSim.Add("id", "radioItemObrigatorioSim")

                                                Dim htmlAttNao As New Dictionary(Of String, Object)
                                                htmlAttNao.Add("id", "radioItemObrigatorioNao")

                                                If (Model.Filter.INDCPTFCH = 1) Then
                                                    Model.Filter.INDITEOBR = 1
                                                    htmlAttSim.Add("disabled", "disabled")
                                                    htmlAttNao.Add("disabled", "disabled")
                                                Else
                                                    Model.Filter.INDITEOBR = 0
                                                End If
                                            End Code

                                            @Code
                                            End Code
                                            <div class="col-sm-3">
                                                @Html.RadioButtonFor(Function(Model) Model.Filter.INDITEOBR, Constantes.SIM, htmlAttSim)
                                                @Html.Label("sim", "Sim")
                                            </div>
                                            <div class="col-sm-3">
                                                @Html.RadioButtonFor(Function(Model) Model.Filter.INDITEOBR, Constantes.NAO, htmlAttNao)
                                                @Html.Label("nao", "Não")
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div Class="col-lg-2 col-sm-2">
                                        <div Class="btn-group pull-right">
                                            <Button type="submit" Class="btn btn-info btn-white" onclick="aplicarParametrosCash()">
                                                <i Class="ace-icon fa fa-check bigger"></i>
                                                Aplicar
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div Class="row">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br />
            <div>
                <div id="gridItensPesquisaParametrosMixCashPromocao">
                    @Code
                        Html.RenderPartial("~/Views/Promocao/Mix/_GridParametrosMixCashPromocao.vbhtml", Model)
                    End Code
                </div>
            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalParametrosMixCashPromocao();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Sair
                </Button>
            </div>
        </div>
    </div>
</div>

<script>
    function defaultZero(e) {
        if (e.val() == "")
            e.val("0")
    }
</script>