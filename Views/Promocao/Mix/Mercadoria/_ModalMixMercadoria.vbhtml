﻿@ModelType PromocaoMixViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-full hideScroll" id="modalPromocaoMixMercadoria" style="z-index: 1051; margin-top: 4px;">

    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp; Mercadoria
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalPromocaoMixMercadoria">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div class="row">
                <div class="col-xs-6">

                    @if (ViewBag.PermiteEdicaoPromocaoMixMercadoria) Then
                        @<div id="divLoadPesquisaPromocaoMixMercadoria">
                        </div>
                        @<div id="divLoadPesquisaPromocaoMixMercadoriaPorPromocao">
                        </div>
                        Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsMixMercadoria", .Controller = "PromocaoMix", .EnderecoTemplate = "MixMercadoria.xls"})
                    End If
                </div>

                @if (ViewBag.PermiteEdicaoPromocaoMixMercadoria) Then
                    @<div class="col-xs-6" id="divPromocaoMixMercadoriaAdicionados">

                    </div>
                Else
                    @<div class="col-xs-12" id="divPromocaoMixMercadoriaAdicionados">

                    </div>
                End If

            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalPromocaoMixMercadoria();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>