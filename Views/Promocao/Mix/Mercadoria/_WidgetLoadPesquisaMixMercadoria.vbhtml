﻿@ModelType GridSettings(Of PromocaoMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

@code
    Dim ajaxOptions As New AjaxOptions() With {
        .UpdateTargetId = "gridItensPesquisaPromocaoMixMercadoria",
        .InsertionMode = InsertionMode.Replace,
        .OnBegin = "LoadingOn",
        .OnComplete = "MixMercadoriaClienteLoadingOff_UpdateMessage",
        .HttpMethod = "POST"
        }

End Code

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Pesquisa Mercadoria
                </h4>
                <div class="widget-toolbar">
                    <div class="btn-group ">
                        <a class="btn-md  btn btn-primary btn-white" style="" id="btnPromocaoTogglePesquisaMixMercadoria" onclick="togglePromocaoPesquisaMixMercadoria('ParaPesquisaPorPromocao')">
                            <i class="ace-icon fa fa-exchange bigger-120"></i>
                            Pesquisar por Promoção
                        </a>
                    </div>
                </div>
            </div>
            <div Class="widget-body">
                <div id="msgWidgetMixMercadoria">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div Class="widget-main">
                    <div id="divResultadoPesquisaPromocaoMixMercadoria">
                        @Using (Ajax.BeginForm("PesquisaMixMercadoria", "PromocaoMix", ajaxOptions, New With {.id = "frmPesquisaPromocaoMixMercadoria"}))
                            @Html.AntiForgeryToken()

                            @Html.HiddenFor(Function(r) Model.Filter.CODPMC)
                            @Html.HiddenFor(Function(r) Model.Filter.NUMSEQMIXACOCMC)

                            @<div class="row">
                                <div class="col-sm-6">
                                    @Html.Label("Mercadoria:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODMER, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.DESMER, New With {.class = "form-control", .id = "txtDesMercadoria", .style = "width: 80%;"})
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    @Html.Label("Grupo Mercadoria:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODGRPMER, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.DESGRPMER, New With {.class = "form-control", .id = "txtDesGrupoMercadoria", .style = "width: 80%;"})
                                    </div>
                                </div>
                            </div>
                            @<div class="row">
                                <div class="col-sm-6">
                                    @Html.Label("Família Mercadoria:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODFMLMER, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.DESFMLMER, New With {.class = "form-control", .id = "txtDesFamiliaMercadoria", .style = "width: 80%;"})
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    @Html.Label("Classe Mercadoria:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODCLSMER, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.DESCLSMER, New With {.class = "form-control", .id = "txtDesClasseMercadoria", .style = "width: 80%;"})
                                    </div>
                                </div>
                            </div>
                            @<div Class="row">
                                <div class="col-sm-6">
                                    @Html.Label("Fornecedor:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODFRN, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.NOMFRN, New With {.class = "form-control", .id = "txtDesFornecedor", .style = "width: 80%;"})
                                    </div>
                                </div>
                                <div Class="col-sm-6">
                                    <label>&nbsp;</label>
                                    <br />
                                    <Button type="submit" Class="btn btn-white btn-info">
                                        <i Class="ace-icon fa fa-search bigger-120 info"></i>
                                        Pesquisar
                                    </Button>
                                </div>

                                @if (ViewBag.QDEGRPMIXPMC > 0) Then
                                    @<div Class="col-sm-3">
                                        @if (ViewBag.TIPPMC = 1) Then
                                            @Html.Label("Grupo:")
                                        End If
                                        <br />
                                        <select id="drpAddPromocaoMixFaixa" Class="form-control">
                                            @For index = 1 To ViewBag.QDEGRPMIXPMC
                                                @<option value="@index"> @index</option>
                                            Next
                                        </select>
                                    </div>
                                End If

                            </div>
                        End Using

                        <br />
                        <div id="gridItensPesquisaPromocaoMixMercadoria">
                            @Code
                                Html.RenderPartial("~/Views/Promocao/Mix/Mercadoria/_GridPesquisaMixMercadoria.vbhtml", Model)
                            End Code
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
