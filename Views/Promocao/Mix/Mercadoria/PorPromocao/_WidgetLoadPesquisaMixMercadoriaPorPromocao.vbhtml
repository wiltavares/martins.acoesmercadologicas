﻿@ModelType GridSettings(Of PromocaoMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

@code
    Dim ajaxOptions As New AjaxOptions() With {
        .UpdateTargetId = "gridItensPesquisaPromocaoMixMercadoriaPorPromocao",
        .InsertionMode = InsertionMode.Replace,
        .OnBegin = "LoadingOn",
        .OnComplete = "MixMercadoriaPorPromocaoLoadingOff_UpdateMessage",
        .HttpMethod = "POST"
        }

End Code

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Pesquisa Mercadoria
                </h4>
                <div class="widget-toolbar">
                    <div class="btn-group ">
                        <a class="btn-md  btn btn-primary btn-white" style="" id="btnPromocaoTogglePesquisaMixMercadoria" onclick="togglePromocaoPesquisaMixMercadoria('ParaPesquisaPadrao')">
                            <i class="ace-icon fa fa-exchange bigger-120"></i>
                            Voltar
                        </a>
                    </div>
                </div>
            </div>
            <div Class="widget-body">
                <div id="msgWidgetMixMercadoria">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div Class="widget-main">
                    <div id="divResultadoPesquisaPromocaoMixMercadoriaPorPromocao">
                        @Using (Ajax.BeginForm("PesquisaMixMercadoriaPorPromocao", "PromocaoMix", ajaxOptions, New With {.id = "frmPesquisaPromocaoMixMercadoriaPorPromocao"}))
                            @Html.AntiForgeryToken()

                            @Html.HiddenFor(Function(r) Model.Filter.CODPMC)
                            @Html.HiddenFor(Function(r) Model.Filter.NUMSEQMIXACOCMC)

                            @<div Class="row">
                                <div class="col-sm-6">
                                    @Html.Label("Código Promoção:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODPMCtoFind, New With {.class = "form-control"})
                                    </div>
                                </div>
                                <div Class="col-sm-6">
                                    <label>&nbsp;</label>
                                    <br />
                                    <Button type="submit" Class="btn btn-white btn-info">
                                        <i Class="ace-icon fa fa-search bigger-120 info"></i>
                                        Pesquisar
                                    </Button>
                                </div>

                                @if (ViewBag.QDEGRPMIXPMC > 0) Then
                                    @<div Class="col-sm-3">
                                        @if (ViewBag.TIPPMC = 1) Then
                                            @Html.Label("Grupo:")
                                        End If
                                        <br />
                                        <select id="drpAddPromocaoMixFaixaPorPromocao" Class="form-control">
                                            @For index = 1 To ViewBag.QDEGRPMIXPMC
                                                @<option value="@index"> @index</option>
                                            Next
                                        </select>
                                    </div>
                                End If

                            </div>
                        End Using

                        <br />
                        <div id="gridItensPesquisaPromocaoMixMercadoriaPorPromocao">
                            @Code
                                Html.RenderPartial("~/Views/Promocao/Mix/Mercadoria/_GridPesquisaMixMercadoria.vbhtml", Model)
                            End Code
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
