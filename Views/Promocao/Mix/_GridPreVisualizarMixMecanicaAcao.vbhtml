﻿@ModelType GridSettings(Of PromocaoMixVisualizarVigenteViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div style="width: auto; overflow-x: scroll;">
    <Table id="tblModalPreVisualizarMixMecanicaAcao" Class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
            <tr role="row">
                @If (Model.ListPaged.Any(Function(f) f.CODGRPMIXMERPMC > 0)) Then
                    @<th class="@Grid.AtribuirIconeOrdenacao("CODGRPMIXMERPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODGRPMIXMERPMC", Model.Page.OrderBy, Model.ResultID)">
                        Grupo
                    </th>
                End If
                <th class="@Grid.AtribuirIconeOrdenacao("CODMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMER", Model.Page.OrderBy, Model.ResultID)">
                    Código
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESMER", Model.Page.OrderBy, Model.ResultID)">
                    Descrição Mercadoria
                </th>
                <th>
                    Grupo Mercadoria
                </th>
                <th>
                    Família Mercadoria
                </th>
                <th>
                    Classe Mercadoria
                </th>
                <th>
                    Fornecedor
                </th>
            </tr>
        </thead>
        <tbody>
            @Html.HiddenFor(Function(r) Model.Filter.CODACOCMC)
            @Html.HiddenFor(Function(r) Model.Filter.NUMSEQRGRACOCMC)

            @For Each item In Model.ListPaged
                @<tr role="row">
                    @If (Model.ListPaged.Any(Function(f) f.CODGRPMIXMERPMC > 0)) Then
                        @<td>
                            @item.CODGRPMIXMERPMC
                        </td>
                    End If
                    <td>
                        @item.CODMER
                    </td>
                    <td>
                        @item.DESMER
                    </td>
                    <td>
                        @item.CODGRPMER - @item.DESGRPMER
                    </td>
                    <td>
                        @item.CODFMLMER - @item.DESFMLMER
                    </td>
                    <td>
                        @item.CODCLSMER - @item.DESCLSMER
                    </td>
                    <td>
                        @item.CODFRNPCPMER - @item.NOMFRN
                    </td>
                </tr>
            Next
        </tbody>
    </Table>
</div>
<br class="clearfix" />
@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code