﻿@ModelType GridSettings(Of PromocaoMixVisualizarVigenteViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-lg hideScroll" id="modalListPromocaoVisualizarMixVigente" style="z-index: 1051; margin-top: 4px;">
    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Visualização Mix Vigente
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgmodalListPromocaoVisualizarMixVigente">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div>
                <div id="gridItensPesquisaVisualizarMixVigente">
                    @Code
                        Html.RenderPartial("~/Views/Promocao/Mix/_GridVisualizarMixVigente.vbhtml", Model)
                    End Code
                </div>
            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalVisualizarMixVigente();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>
