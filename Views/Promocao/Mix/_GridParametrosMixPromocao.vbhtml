﻿@ModelType GridSettings(Of PromocaoMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" id="tableModalParametrosMixPromocao" role="grid" style="min-width: 100%;">
                    <thead>
                        <tr role="row">
                            <th style="width: 5%" class="center">
                                @Html.CheckBox("chkTodasMercadorias", New With {.onclick = "marcarOuDesmarcarTodasMercadorias()", .Id = "chkTodasMercadorias"})
                            </th>
                            <th style="width: 10%" class="@Grid.AtribuirIconeOrdenacao("CODMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMER", Model.Page.OrderBy, Model.ResultID)">
                                Mercadoria
                            </th>
                            <th style="width: 30%" class="@Grid.AtribuirIconeOrdenacao("DESMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESMER", Model.Page.OrderBy, Model.ResultID)">
                                Nome Mercadoria
                            </th>
                            <th style="width: 15%" class="@Grid.AtribuirIconeOrdenacao("QDEMNMMERMIX", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("QDEMNMMERMIX", Model.Page.OrderBy, Model.ResultID)">
                                Quantidade Minima
                            </th>
                            <th style="width: 15%" class="@Grid.AtribuirIconeOrdenacao("QDEMAXMERMIX", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("QDEMAXMERMIX", Model.Page.OrderBy, Model.ResultID)">
                                Quantidade Máxima
                            </th>
                            <th style="width: 10%" class="@Grid.AtribuirIconeOrdenacao("INDITEOBR", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDITEOBR", Model.Page.OrderBy, Model.ResultID)">
                                Item Obrigatório
                            </th>
                            <th style="width: 10%" class="@Grid.AtribuirIconeOrdenacao("INDJRNSMA", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDJRNSMA", Model.Page.OrderBy, Model.ResultID)">
                                Jornal SMART
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td Class="center">
                                    @Html.CheckBox("SelecioneMercadoria", New With {.class = "SelecioneMercadoria"})
                                </td>
                                <td class="CodigoMercadoria">
                                    @item.CODMER
                                </td>
                                <td>
                                    @item.DESMER
                                </td>
                                <td>
                                    @IIf(IsNothing(item.QDEMNMMERMIX), 0, item.QDEMNMMERMIX)
                                </td>
                                <td>
                                    @IIf(IsNothing(item.QDEMAXMERMIX), 0, item.QDEMAXMERMIX)
                                </td>
                                <td>
                                    @IIf(item.INDITEOBR Is Nothing OrElse item.INDITEOBR <> 1, "Não", "Sim")
                                </td>
                                <td>
                                    @IIf(item.INDJRNSMA Is Nothing OrElse item.INDJRNSMA <> 1, "Não", "Sim")
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br Class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>
