﻿@ModelType GridSettings(Of PromocaoMixVisualizarVigenteViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal fade hideScroll" id="modalListPreVisualizarMixMecanicaAcao" style="z-index: 1051;">
    <div Class="modal-dialog modal-full">
        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Pré Visualização do Mix
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">
                <div id="msgmodalListPreVisualizarMixMecanicaAcao">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div>
                    <div id="gridItensPesquisaPreVisualizarMixMecanicaAcao">
                        @Code
                            Html.RenderPartial("~/Views/Promocao/Mix/_GridPreVisualizarMixMecanicaAcao.vbhtml", Model)
                        End Code
                    </div>
                </div>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalPreVisualizarMixMecanicaAcao();">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Cancelar
                    </Button>
                </div>
            </div>
        </div>
    </div>
</div>