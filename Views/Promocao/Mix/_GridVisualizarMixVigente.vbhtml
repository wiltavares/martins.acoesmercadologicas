﻿@ModelType GridSettings(Of PromocaoMixVisualizarVigenteViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div style="width: auto; overflow-x: scroll;">
    <Table id="tblModalPromocaoVisualizarMixVigente" Class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
            <tr role="row">
                @If (ViewBag.TIPPMC = 1) Then
                    @<th Class="@Grid.AtribuirIconeOrdenacao("CODGRPMIXMERPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODGRPMIXMERPMC", Model.Page.OrderBy, Model.ResultID)">
                        Grupo
                    </th>
                End If
                <th class="@Grid.AtribuirIconeOrdenacao("CODMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMER", Model.Page.OrderBy, Model.ResultID)">
                    Código
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESMER", Model.Page.OrderBy, Model.ResultID)">
                    Descrição Mercadoria
                </th>
            </tr>
        </thead>
        <tbody>
            @Html.HiddenFor(Function(r) Model.Filter.CODPMC)
            @Html.HiddenFor(Function(r) Model.Filter.TIPEDEPUBALVPMC)

            @For Each item In Model.ListPaged
                @<tr role="row">
                    @If (ViewBag.TIPPMC = 1) Then
                        @<td>
                            @item.CODGRPMIXMERPMC
                        </td>
                    End If
                    <td>
                        @item.CODMER
                    </td>
                    <td>
                        @item.DESMER
                    </td>
                </tr>
            Next
        </tbody>
    </Table>
</div>
<br class="clearfix" />
@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code