﻿@ModelType List(Of PromocaoMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Fornecedores Selecionados
                </h4>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divFornecedorSelecionadas" style="width: auto; overflow-x: scroll;">
                        <Table id="tblFornecedorSelecionada" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                            <thead>
                                <tr role="row">
                                    @If (ViewBag.PermiteEdicaoPromocaoMixFornecedor) Then
                                        @<th Class="center">
                                            <button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteAllPromocaoMixFornecedor()" title="Excluir todos">
                                                <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </button>
                                        </th>
                                    End If
                                    <th>
                                        Cód.
                                    </th>
                                    <th>
                                        Nome Fornecedor
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @for Each item In Model
                                    @<tr Class="trResultado">
                                        @Html.HiddenFor(Function(r) item.NUMSEQMIXACOCMC)
                                        <td class="center">
                                            @If (ViewBag.PermiteEdicaoPromocaoMixFornecedor AndAlso item.INDVLDRGRMIXPMC = 0) Then
                                                @<button type="button" class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeletePromocaoMixFornecedor(this)">
                                                    &nbsp;<i Class="fa fa-trash-o bigger-120"></i>&nbsp;
                                                </button>
                                            Else
                                                @<button type="button" class="btn btn-danger btn-info" style="padding: 0px;" disabled="disabled" >
                                                    &nbsp;<i Class="fa fa-trash-o bigger-120"></i>&nbsp;
                                                </button>
                                            End If
                                        </td>
                                        <td>
                                            @Html.DisplayFor(Function(r) item.CODFRN)
                                        </td>
                                        <td>
                                            @Html.DisplayFor(Function(r) item.NOMFRN)
                                        </td>
                                    </tr>
                                Next
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
