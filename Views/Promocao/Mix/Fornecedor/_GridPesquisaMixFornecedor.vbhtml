﻿@ModelType GridSettings(Of PromocaoMixViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div style="width: auto; overflow-x: scroll;">
    <Table id="tblResultadoPesquisaPromocaoMixFornecedor" Class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
            <tr role="row">
                <th class="center">
                    <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="addPesquisaMixFornecedor(@Newtonsoft.Json.JsonConvert.SerializeObject(Model.Filter))">
                        &nbsp;<i class="fa fa-plus bigger-120"></i>&nbsp;
                    </button>
                </th>
                <th>
                    Cód.
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("NOMFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFRN", Model.Page.OrderBy, Model.ResultID)">
                    Nome Fornecedor
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr Class="trResultado">
                    <td class="center">
                        <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="salvarPromocaoMixFornecedor(this)">
                            &nbsp;<i class="fa fa-plus bigger-120"></i>&nbsp;
                        </button>
                    </td>
                    <td>
                        @Html.DisplayFor(Function(r) item.CODFRN)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(r) item.NOMFRN)
                    </td>
                </tr>
            Next
        </tbody>
    </Table>
</div>
<br class="clearfix" />
@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code
