﻿@ModelType PromocaoMixViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-full hideScroll" id="modalPromocaoMixFornecedor" style="z-index: 1051; margin-top: 4px;">

    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp; Fornecedor
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalPromocaoMixFornecedor">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div class="row">
                <div class="col-xs-6">
                    @if (ViewBag.PermiteEdicaoPromocaoMixFornecedor) Then
                        @<div id="divLoadPesquisaPromocaoMixFornecedor">
                        </div>

                        Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsMixFornecedor", .Controller = "PromocaoMix", .EnderecoTemplate = "MixFornecedor.xls"})
                    End If
                </div>
                @if (ViewBag.PermiteEdicaoPromocaoMixFornecedor) Then
                    @<div class="col-xs-6" id="divPromocaoMixFornecedorAdicionados">

                    </div>
                Else
                    @<div class="col-xs-12" id="divPromocaoMixFornecedorAdicionados">

                    </div>
                End If

            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalPromocaoMixFornecedor();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>