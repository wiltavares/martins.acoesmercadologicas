﻿@ModelType  List(Of PromocaoMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-lg hideScroll" id="modalListPromocaoMixGrupoMercadoria" style="z-index: 1051; margin-top: 4px;">
    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Grupo Mercadoria
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgmodalListPromocaoMixGrupoMercadoria">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div>
                <Table id="tblModalPromocaoMixGrupoMercadoria" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="center">
                                @Html.CheckBox("chkGruposMercadorias", New With {.onclick = "marcarOuDesmarcarTodosGruposMercadorias()", .Id = "chkTodosGruposMercadorias"}.EnableIf(ViewBag.PermiteEdicaoPromocaoMixGrupoMercadoria))
                            </th>
                            <th>
                                Código
                            </th>
                            <th>
                                Nome
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        @for Each item In Model

                            @<tr Class="trResultado">
                                @if item.CODPMC IsNot Nothing AndAlso item.NUMSEQMIXACOCMC IsNot Nothing Then
                                    @<td Class="center">
                                        @Html.CheckBox("SelecioneGrupoMercadoria", New With {.class = "marcaGrupoMercadoria", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoPromocaoMixGrupoMercadoria AndAlso item.INDVLDRGRMIXPMC = 0))
                                    </td>
                                Else
                                    @<td Class="center">
                                        @Html.CheckBox("SelecioneGrupoMercadoria", New With {.class = "marcaGrupoMercadoria"}.EnableIf(ViewBag.PermiteEdicaoPromocaoMixGrupoMercadoria))
                                    </td>
                                End If
                                     <td>
                                         @Html.HiddenFor(Function(model) item.NUMSEQMIXACOCMC)
                                         @Html.HiddenFor(Function(r) item.CODGRPMER)

                                         @Html.DisplayFor(Function(model) item.CODGRPMER)
                                     </td>
                                     <td>
                                         @Html.DisplayFor(Function(model) item.DESGRPMER)
                                     </td>
                                 </tr>
                        Next
                         </tbody>
                     </Table>
                 </div>
             </div>

             <div Class="modal-footer clearfix">
                 <div Class="btn-group pull-right">
                     @if (ViewBag.PermiteEdicaoPromocaoMixGrupoMercadoria) Then
                         @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosPromocaoMixGrupoMercadoria();">
                             <i Class="ace-icon fa fa-save bigger-120 green"></i>
                             Salvar
                         </Button>
                     End If
                     <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalMixGrupoMercadoria();">
                         <i Class="ace-icon fa fa-close bigger-120 red"></i>
                         Cancelar
                     </Button>
                 </div>
             </div>
         </div>
     </div>
