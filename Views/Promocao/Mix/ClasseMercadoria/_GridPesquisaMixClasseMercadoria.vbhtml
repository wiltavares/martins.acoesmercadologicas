﻿@ModelType GridSettings(Of PromocaoMixViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div style="width: auto; overflow-x: scroll;">
    <Table id="tblResultadoPesquisaPromocaoMixClasseMercadoria" Class="table table-striped table-bordered table-hover dataTable" role="grid">
        <thead>
            <tr role="row">
                <th class="center">
                    <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="addPesquisaMixClasseMercadoria(@Newtonsoft.Json.JsonConvert.SerializeObject(Model.Filter))">
                        &nbsp;<i class="fa fa-plus bigger-120"></i>&nbsp;
                    </button>
                </th>
                <th>
                    Cód.
                </th>
                <th class="@Grid.AtribuirIconeOrdenacao("DESCLSMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESCLSMER", Model.Page.OrderBy, Model.ResultID)">
                    Nome Classe Mercadoria
                </th>
                <th>
                    Grupo Mercadoria
                </th>
                <th>
                    Família Mercadoria
                </th>
            </tr>
        </thead>
        <tbody>
            @For Each item In Model.ListPaged
                @<tr Class="trResultado">
                    @Html.HiddenFor(Function(r) item.CODGRPMER)
                    @Html.HiddenFor(Function(r) item.CODFMLMER)
                    <td class="center">
                        <button type="button" class="btn btn-success btn-info" style="padding: 0px;" onclick="salvarPromocaoMixClasseMercadoria(this)">
                            <i class="ace-icon fa fa-plus bigger-120 info"></i>
                        </button>
                    </td>
                    <td>
                        @Html.DisplayFor(Function(r) item.CODCLSMER)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(r) item.DESCLSMER)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(r) item.CODGRPMER) - @Html.DisplayFor(Function(r) item.DESGRPMER)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(r) item.CODFMLMER) -  @Html.DisplayFor(Function(r) item.DESFMLMER)
                    </td>
                </tr>
            Next
        </tbody>
    </Table>
</div>
<br class="clearfix" />
@Code
    Html.RenderPartial(Constantes.VIEW_PAGINACAO)
End Code
