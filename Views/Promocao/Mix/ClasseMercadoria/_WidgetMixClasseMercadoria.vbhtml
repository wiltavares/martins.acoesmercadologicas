﻿@ModelType List(Of PromocaoMixViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Classe Mercadoria Selecionadas
                </h4>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divPromocaoMixClasseMercadoriaSelecionadas" style="width: auto; overflow-x: scroll;">
                        <Table id="tblPromocaoMixClasseMercadoriaSelecionada" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                            <thead>
                                <tr role="row">
                                    @If (ViewBag.PermiteEdicaoPromocaoMixClasseMercadoria) Then
                                        @<th Class="center">
                                            <button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteAllPromocaoMixClasseMercadoria()" title="Excluir todos">
                                                <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </button>
                                        </th>
                                    End If
                                    <th>
                                        Cód.
                                    </th>
                                    <th>
                                        Nome Classe Mercadoria
                                    </th>
                                    <th>
                                        Grupo Mercadoria
                                    </th>
                                    <th>
                                        Família Mercadoria
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @for Each item In Model
                                    @<tr Class="trResultado">
                                        @Html.HiddenFor(Function(r) item.NUMSEQMIXACOCMC)
                                        @Html.HiddenFor(Function(r) item.CODGRPMER)
                                        @Html.HiddenFor(Function(r) item.CODFMLMER)
                                        <td class="center">
                                            @if (ViewBag.PermiteEdicaoPromocaoMixClasseMercadoria AndAlso item.INDVLDRGRMIXPMC = 0) Then
                                                @<button type="button" class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeletePromocaoMixClasseMercadoria(this)">
                                                    <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </button>
                                            Else
                                                @<button type="button" class="btn btn-danger btn-info" style="padding: 0px;" disabled="disabled">
                                                    <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </button>
                                            End If
                                        </td>
                                        <td>
                                            @Html.DisplayFor(Function(r) item.CODCLSMER)
                                        </td>
                                        <td>
                                            @Html.DisplayFor(Function(r) item.DESCLSMER)
                                        </td>
                                        <td>
                                            @Html.DisplayFor(Function(r) item.CODGRPMER) - @Html.DisplayFor(Function(r) item.DESGRPMER)
                                        </td>
                                        <td>
                                            @Html.DisplayFor(Function(r) item.CODFMLMER) - @Html.DisplayFor(Function(r) item.DESFMLMER)
                                        </td>
                                    </tr>
                                Next
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
