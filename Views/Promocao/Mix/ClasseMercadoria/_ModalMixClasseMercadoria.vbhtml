﻿@ModelType PromocaoMixViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-full hideScroll" id="modalPromocaoMixClasseMercadoria" style="z-index: 1051; margin-top: 4px;">

    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Classe Mercadoria
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalPromocaoMixClasseMercadoria">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div class="row">
                <div class="col-xs-6">
                    @if (ViewBag.PermiteEdicaoPromocaoMixClasseMercadoria) Then
                        @<div id="divLoadPesquisaPromocaoMixClasseMercadoria">
                        </div>

                        Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsMixClasseMercadoria", .Controller = "PromocaoMix", .EnderecoTemplate = "MixClasseMercadoria.xls"})
                    End If
                </div>

                @if (ViewBag.PermiteEdicaoPromocaoMixClasseMercadoria) Then
                    @<div class="col-xs-6" id="divPromocaoMixClasseMercadoriaAdicionados">

                    </div>
                Else
                    @<div class="col-xs-12" id="divPromocaoMixClasseMercadoriaAdicionados">

                    </div>
                End If

            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalPromocaoMixClasseMercadoria();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>