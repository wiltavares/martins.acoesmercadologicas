﻿@ModelType PromocaoMixViewModel
@Imports Martins.AcoesMercadologicas.Core


<div class="widget-box widget-color-wi">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller blue lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Mix
        </h4>
        <div class="widget-toolbar">
            <div class="btn-group">

                @If (ViewBag.ParticipacaoMixTotal = 0) Then
                    @If (ViewBag.SituacaoPromocao = "Liberada") Then
                        @<Button class="btn btn-md btn-white btn-success" type="button" onclick="ProcessarAtualizacoesMixPromocao()">
                            <i class="ace-icon fa fa-check-square-o bigger-125"></i>
                            Processar Alterações
                        </Button>
                    End If

                    @If (ViewBag.SituacaoPromocao = "Liberada" Or ViewBag.SituacaoPromocao = "Encerrada" Or ViewBag.SituacaoPromocao = "") Then
                        @<button class="btn btn-md btn-white btn-info" type="button" onclick="abrirModalVisualizarMixVigente()">
                            <i class="ace-icon fa fa-eye bigger-125"></i>
                            Visualizar MIX Vigente
                        </button>
                    End If

                    If (ViewBag.INDEVTSMA = 1) Then
                        @<Button Class="btn btn-md btn-white btn-info" type="button" onclick="abrirModalParametrosMixPromocao()">
                            <i Class="ace-icon fa fa-sliders bigger-125"></i>
                            Parâmetros Mix
                        </Button>
                    ElseIf (ViewBag.INDCASHBACK) Then
                        @<Button Class="btn btn-md btn-white btn-info" type="button" onclick="abrirModalParametrosMixCashPromocao()">
                            <i Class="ace-icon fa fa-sliders bigger-125"></i>
                            Parâmetros Mix
                        </Button>
                    End If

                    @If (ViewBag.SituacaoPromocao = "Liberada" Or ViewBag.SituacaoPromocao = "À Liberar") Then

                        @<Button Class="btn btn-md btn-white btn-info" type="button" onclick="abrirModalPreVisualizarMixPromocao()">
                            <i Class="ace-icon fa fa-eye bigger-125"></i>
                            Pré Visualização Mix Promoção
                        </Button>
                    End If
                End If
            </div>
            <a href="#" data-action="collapse">
                <i Class="ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>
    </div>
    <div Class="widget-body">
        @If (ViewBag.ParticipacaoMixTotal = 0) Then
            @<div Class="widget-main">
                <div id="msgModalMix">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div style="width: auto;">
                    <table class="table table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                        <thead>

                        </thead>
                        <tbody>
                            @If (ViewBag.INDEVTSMA = 0) Then
                                @<tr>
                                    @If (Model.INDGRUPOMERCADORIA = 1) Then
                                        @<td Class="center">
                                            <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalMixGrupoMercadoria()">
                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                Configurado
                                            </Button>
                                        </td>
                                    Else
                                        @<td Class="center">
                                            @if (ViewBag.PermiteEdicaoPromocaoMixGrupoMercadoria And ViewBag.SituacaoPromocao = "À Liberar") Then
                                                @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalMixGrupoMercadoria()">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            End If
                                        </td>
                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.CODGRPMER)
                                    </td>
                                </tr>

                                @<tr>

                                    @If (Model.INDFAMILIAMERCADORIA = 1) Then
                                        @<td Class="center">
                                            <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalMixFamiliaMercadoria()">
                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                Configurado
                                            </Button>
                                        </td>
                                    Else
                                        @<td Class="center">
                                            @if (ViewBag.PermiteEdicaoPromocaoMixFamiliaMercadoria And ViewBag.SituacaoPromocao = "À Liberar") Then
                                                @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalMixFamiliaMercadoria()">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            End If
                                        </td>
                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.CODFMLMER)
                                    </td>
                                </tr>

                                @<tr>
                                    @If (Model.INDCLASSEMERCADORIA = 1) Then
                                        @<td Class="center">
                                            <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalMixClasseMercadoria()">
                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                Configurado
                                            </Button>
                                        </td>
                                    Else
                                        @<td Class="center">
                                            @if (ViewBag.PermiteEdicaoPromocaoMixClasseMercadoria And ViewBag.SituacaoPromocao = "À Liberar") Then
                                                @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalMixClasseMercadoria()">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            End If
                                        </td>
                                    End If
                                    <td>
                                        @Html.DisplayNameFor(Function(p) p.CODCLSMER)
                                    </td>
                                </tr>
                            End If
                            
                            <tr>
                                @If (Model.INDMERCADORIA = 1) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalMixMercadoria()">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurado
                                        </Button>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @if (ViewBag.PermiteEdicaoPromocaoMixMercadoria And ViewBag.SituacaoPromocao = "À Liberar") Then
                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalMixMercadoria()">
                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                Selecionar
                                            </Button>
                                        End If
                                    </td>
                                End If
                                <td>
                                    @Html.DisplayNameFor(Function(p) p.CODMER)
                                </td>
                            </tr>

                            @If (ViewBag.INDEVTSMA = 0) Then
                                @<tr>
                                    @If (Model.INDFORNECEDOR = 1) Then
                                        @<td Class="center">
                                            <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalMixFornecedor()">
                                                <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                                Configurado
                                            </Button>
                                        </td>
                                    Else
                                        @<td Class="center">
                                            @if (ViewBag.PermiteEdicaoPromocaoMixFornecedor And ViewBag.SituacaoPromocao = "À Liberar") Then
                                                @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalMixFornecedor()">
                                                    <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                    Selecionar
                                                </Button>
                                            End If
                                        </td>
                                    End If
                                        <td>
                                        @Html.DisplayNameFor(Function(p) p.CODFRN)
                                    </td>
                                </tr>
                            End If
                        </tbody>
                    </table>
                </div>
            </div>
        Else
            @<div class="alert alert-info">
                Tipo de Participação: Mix Total
            </div>
        End If
    </div>
</div>

