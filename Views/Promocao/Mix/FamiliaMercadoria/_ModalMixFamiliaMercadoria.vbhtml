﻿@ModelType  PromocaoMixViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-full hideScroll" id="modalPromocaoMixFamiliaMercadoria" style="z-index: 1051; margin-top: 4px;">

    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Família Mercadoria
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalPromocaoMixFamiliaMercadoria">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div class="row">
                <div class="col-xs-6">
                    @if (ViewBag.PermiteEdicaoPromocaoMixFamiliaMercadoria) Then
                        @<div id="divLoadPesquisaPromocaoMixFamiliaMercadoria">
                        </div>
                        Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsMixFamiliaMercadoria", .Controller = "PromocaoMix", .EnderecoTemplate = "MixFamiliaMercadoria.xls"})
                    End If
                </div>

                @if (ViewBag.PermiteEdicaoPromocaoMixFamiliaMercadoria) Then
                    @<div class="col-xs-6" id="divPromocaoMixFamiliaMercadoriaAdicionados">

                    </div>
                Else
                    @<div class="col-xs-12" id="divPromocaoMixFamiliaMercadoriaAdicionados">

                    </div>
                End If

            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalPromocaoMixFamiliaMercadoria();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>