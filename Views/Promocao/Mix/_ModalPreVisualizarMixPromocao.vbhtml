﻿@ModelType GridSettings(Of PromocaoMixVisualizarVigenteViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-full hideScroll" id="modalListPreVisualizarMixPromocao" style="z-index: 1051; margin-top: 4px;">
    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Pré Visualização Mix Promoção
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgmodalListPreVisualizarMixPromocao">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div>
                <div id="gridItensPesquisaPreVisualizarMixPromocao">
                    @Code
                        Html.RenderPartial("~/Views/Promocao/Mix/_GridPreVisualizarMixPromocao.vbhtml", Model)
                    End Code
                </div>
            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalPreVisualizarMixPromocao();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>