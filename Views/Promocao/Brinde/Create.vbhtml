﻿@ModelType PromocaoBrindeViewModel
@Imports Martins.AcoesMercadologicas.Core

<div class="modal fade" id="modalBuscaItem"></div>

@code
    Dim ajaxObjectPost As New AjaxOptions() With {
        .UpdateTargetId = "divCreateBrinde",
        .InsertionMode = InsertionMode.Replace,
        .OnBegin = "LoadingOn",
        .OnComplete = "CreateBrinde_Complete",
        .HttpMethod = "POST"
        }
    Dim isCob = ViewBag.isCob
End Code

<div id="msgModalBrinde">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>


<div class="widget-box" style="border-color: green;">
    <div class="widget-header widget-header-small" style=" border-color: green;">
        <h4 class="widget-title smaller green lighter">
            <i class="ace-icon fa fa-pencil bigger-110"></i>
            Incluir Brinde
        </h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Create", "PromocaoBrinde", Nothing, ajaxObjectPost, New With {.id = "frmCreateBrinde"}))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="widget-body">
                        <div class="widget-main">
                            <div>
                                <div class="row">
                                    @Html.HiddenFor(Function(Model) Model.CODPMC)
                                    @Html.HiddenFor(Function(Model) Model.CODACOCMC, New With {.id = "CODACAOBRINDE"})
                                    @Html.HiddenFor(Function(Model) Model.PERMERRSVPRMPMC, New With {.id = "IdBrindeReserva", .Value = 100, .alt = "inteiro_positivo"})

                                    <div class="col-sm-2">
                                        @Html.LabelFor(Function(Model) Model.PERMERRSVPRMPMC, "Cod.:")
                                        <br />
                                        @Html.TextBoxFor(Function(Model) Model.CODPRMPMC, New With {.Class = "form-control", .id = "idItemCodMer", .onChange = "modalBuscarItens()"})

                                    </div>
                                    <div class="col-sm-4">
                                        @Html.LabelFor(Function(Model) Model.PERMERRSVPRMPMC, "Brinde:")
                                        <br />
                                        <div class="input-group ">
                                            @Html.TextBoxFor(Function(f) f.DESPRMPMC, Nothing, New With {.Class = "form-control", .id = "itemBusca"})
                                            <span class="input-group-btn">
                                                <button class="btn btn-white" id="btBscIte" type="button" onclick="modalBuscarItens()" style="display: block">
                                                    <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                                </button>
                                                <button class="btn btn-white" id="btTiraIte" type="button" onclick="tirarItem()" style="display: none">
                                                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>


                                    <div class="col-sm-2">
                                        @Html.LabelFor(Function(Model) Model.QDEBDEMPLCTRPMC, "Múltiplo Brinde:")
                                        <br />
                                        @Html.TextBoxFor(Function(Model) Model.QDEBDEMPLCTRPMC, New With {.Class = "form-control", .id = "IdMultBrinde", .Value = 1, .alt = "inteiro_positivo"})
                                    </div>

                                    @If (ViewBag.listaFaixa.Count >= 1) Then
                                        @<div Class="col-sm-4">
                                            @Html.LabelFor(Function(Model) Model.TIPEDENIVPRMPMC, "Faixa:")
                                            <br />
                                            @Html.DropDownListFor(Function(Model) Model.TIPEDENIVPRMPMC, New SelectList(ViewBag.listaFaixa, "VALUE", "TEXT", Model.TIPEDENIVPRMPMC), New With {.Class = "form-control", .id = "dropStatus"})
                                        </div>
                                    End If

                                    <div class="col-sm-4" id="divTipoBrinde">
                                        @Html.LabelFor(Function(Model) Model.INDISRBDEADI, "Tipo Brinde:")
                                        <br />
                                        <div class="row">
                                            <div class="col-sm-4">
                                                @Html.RadioButtonFor(Function(Model) Model.INDISRBDEADI, Constantes.NAO, New With {.checked = True})
                                                @Html.Label("normal", "Normal")
                                            </div>
                                            <div class="col-sm-4">
                                                @Html.RadioButtonFor(Function(Model) Model.INDISRBDEADI, Constantes.SIM)
                                                @Html.Label("adicional", "Adicional")
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if (isCob) Then
                                    @<div class="row">
                                        <div class="col-sm-3">
                                            <label>Custo:</label>
                                            <br>
                                            <input class="input-sm form-control" alt="currency" readonly="readonly" data-val="true" id="custoIncluirBrinde" type="text">
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Preço Médio Venda:</label>
                                            <br>
                                            <input class="input-sm form-control" alt="currency" readonly="readonly" data-val="true" id="precoIncluirBrinde" type="text">
                                        </div>
                                        <div class="col-sm-2">
                                            @Html.LabelFor(Function(Model) Model.PERBFCBDE, "% Benefício:")
                                            <br />
                                            @Html.TextBoxFor(Function(Model) Model.PERBFCBDE, New With {.Class = "form-control", .id = "percIncluirBrinde", .onkeyup = "validaPerc();", .alt = "currency_positivo", .maxlength = "6", .size = "1"})
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Valor:</label>
                                            <br>
                                            @*<input class="input-sm form-control" alt="currency" readonly="readonly" data-val="true" id="valorIncluirBrinde" type="text">*@
                                            @Html.TextBoxFor(Function(Model) Model.VLRUNTLIQMER, New With {.Class = "input-sm form-control", .id = "valorIncluirBrinde", .alt = "currency_positivo"})
                                        </div>
                                        <div class="col-sm-1">
                                            <label>&nbsp;</label>
                                            <br />
                                            <label>
                                                @Html.CheckBoxFor(Function(Model) Model.RATEAR)
                                                Ratear
                                            </label>
                                        </div>
                                    </div>
                                End If

                                <div class="row">                             
                                    <div Class="col-sm-12">
                                        <label>&nbsp;</label>
                                        <br />
                                        <div Class="btn-group pull-left">
                                            <Button type="button" Class="btn btn-white btn-success " onclick="adicionarBrinde()">
                                                <i Class="ace-icon fa fa-save bigger-120 green"></i>
                                                Adicionar
                                            </Button>
                                            <a Class="btn btn-white btn-yellow" id="btnImportXls" onclick="abrirModal('PromocaoBrinde', 'AbrirModalImport', undefined, '#modalPromocao', ModalImportXlsBrindesPromocaoSuccess)">
                                                <i Class="ace-icon fa fa-file-excel-o bigger-120"></i>
                                                Importar Planilha
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {
        chbBrindeAdicional_OnClick();
        tirarItem();
        if ($('#idItemCodMer').val() != '') {
            $('#idItemCodMer').prop('readonly', 'readonly');
        }
        if ($('[name="DESPRMPMC"]').val()) {
            $('[name="DESPRMPMC"]').prop('readonly', 'readonly');
            $('#btBscIte').hide();
            $('#btTiraIte').show();
        }
    });

</script>
