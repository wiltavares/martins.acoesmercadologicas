﻿@ModelType List(Of PromocaoBrindeHistoricoViewModel)


<div class="modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header ">
                <i class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Histórico
            </div>
            <div class="modal-body">

                <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th>Seq</th>
                            <th>Tipo</th>
                            <th>Brinde</th>
                            <th>Descrição</th>
                            <th>Data</th>
                            <th>Matrícula</th>
                            <th>Nome do Funcionário</th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @<tr>
                                <td>
                                    @Html.DisplayFor(Function(m) item.NUMSEQ)
                                </td>
                                <td>
                                    @Select Case item.INDSITMER
                                        Case 0
                                            @<label>INSERIDO</label>
                                        Case 1
                                            @<label>EXCLUÍDO</label>
                                    End Select
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.CODMER)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.DESPRMPMC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.DATGRCHST)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.CODFNC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.NOMFNC)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>

            </div>
            <div class="modal-footer clearfix">
                <div class="btn-group pull-right">
                    <button class="btn btn-danger btn-white" onclick="btnFecharHistoricoBrindePromocaoLiberada()">
                        <i class="ace-icon fa fa-close bigger-120 red"></i>
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>