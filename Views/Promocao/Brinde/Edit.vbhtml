﻿@ModelType PromocaoBrindeViewModel
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-wi">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller blue lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Brindes
        </h4>
        <div class="widget-toolbar">
            <div class="btn-group">
                <a id="editBrindes" onclick="editCancelBlock('Brindes', this)" class="btn btn-md btn-white btn-info">
                    <i class="ace-icon fa fa-edit bigger-125"></i>
                    Editar
                </a>
                @code
                    If (ViewBag.encerrada IsNot Nothing AndAlso ViewBag.encerrada = False) Then
                        @<a href="#" title="Abastecer Promoção" onclick="AbastecerPromocao(@Model.CODPMC)" class="btn btn-md btn-white btn-info">
                            <i id="btnAbastecerPromocao" class="ace-icon fa fa-plus-circle bigger-125"></i>
                            Abastecer
                        </a>
                    End If
                End Code
            </div>
            <a href="#" data-action="collapse">
                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-main" id="widgetBrindes">
            <div id="divCreateBrinde">
                @Code
                    Html.RenderPartial("~/Views/Promocao/Brinde/Create.vbhtml", Model)
                End Code
            </div>

            <div id="divListBrindeResultadosView">

            </div>

        </div>
    </div>

</div>
<div id="divModalAbastecerPromocao"></div>
<div id="divModalAbastecerPromocaoAlert"></div>
<div id="divModalHistoricoAbastecimento"></div>


<script type="text/javascript" src="~/Scripts/AbastecerPromocao.js"></script>
