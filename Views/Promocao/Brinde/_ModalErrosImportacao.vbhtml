﻿@ModelType List(Of String)
@Imports Martins.AcoesMercadologicas.Core

<div class="modal-dialog modal-lg" id="modalErros" style="z-index: 1051; margin-top: 4px;">
    <div class="modal-content">
        <div class="modal-header ">
            <i class="ace-icon fa fa-print bigger-120"></i>
            Resultado Importação Planos de Marketing
        </div>
        <div class="modal-body" style="overflow-y: scroll !important;">
            @Code
                If (Model.Count > 0) Then
                    @<div>
                        <Table Class="table table-striped table-bordered table-hover dataTable" role="grid">
                            <thead>
                                <tr role="row">
                                    <th class="center">ERRO</th>
                                </tr>
                            </thead>
                            <tbody>
                                @For Each item In Model
                                    @<tr>
                                        <td>
                                            @item
                                        </td>
                                    </tr>
                                Next
                            </tbody>
                        </Table>
                    </div>
                Else
                    @<div>
                        A importação foi concluida sem Erros!
                    </div>
                End If
            End Code
        </div>
        <div class="modal-footer clearfix">
            <div class="btn-group pull-right">
                <a href="" data-dismiss="modal" class="btn btn-success btn-white">
                    <i class="ace-icon fa fa-check-square-o bigger-120 green"></i>
                    OK
                </a>
            </div>
        </div>
    </div>
</div>