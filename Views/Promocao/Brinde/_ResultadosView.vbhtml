﻿@ModelType List(Of PromocaoBrindeViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim isCob = ViewBag.isCob
End Code
<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small widget-light-blue">
        <h4 class="widget-title smaller blue lighter">
            Brinde Normal
        </h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>

    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:800px">
                    <thead>
                        <tr role="row">

                            @*@if (ViewBag.EncerradaOuLiberada = False) Then*@
                            <th Class="center"></th>
                            @*End If*@

                            @If (ViewBag.listaFaixa = True) Then
                                @<th>
                                    Faixa
                                </th>
                            End If

                            <th>
                                Brinde
                            </th>
                            <th>
                                Descrição
                            </th>
                            <th>
                                Múltiplo Brinde
                            </th>
                            <th>
                                Qtde. Venda
                            </th>
                            <th>
                                Estoque Máximo
                            </th>
                            <th>
                                Saldo
                            </th>
                            <th>
                                Média
                            </th>
                            <th>
                                Valor Prêmio
                            </th>
                            @If (Model.Where(Function(fx) fx.DATECSPRD IsNot Nothing).Count > 0) Then
                                @<th>
                                    Data Exclusão
                                </th>
                                @<th>
                                    Funcionario Exclusão
                                </th>
                            End If

                            @if (isCob) Then

                                @<th>
                                    Valor
                                </th>
                                @<th>
                                    Custo
                                </th>
                                @<th>
                                    Preço Médio Venda
                                </th>
                                @<th>
                                    Ratear
                                </th>
                                @<th>
                                    % Benefício
                                </th>
                            End If
                        </tr>
                    </thead>
                    <tbody>

                        @For Each item In Model.Where(Function(f) f.INDISRBDEADI = 0).ToList()
                            @<tr>

                                @if (ViewBag.EncerradaOuLiberada = False) Then
                                    @<td Class="center">
                                        <div Class="hidden-sm hidden-xs btn-group" onclick="deleteBrinde('@item.CODPRMPMC','@item.CODPMC', '@ViewBag.CodFuncionarioAutorPromocao', '@ViewBag.CodFuncionarioLogado')">
                                            <a data-rel="tooltip" title="Delete" Class="btn btn-xs btn-danger">
                                                <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </a>
                                        </div>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @If (item.DATECSPRD Is Nothing) Then
                                            @<div Class="hidden-sm hidden-xs btn-group" onclick="removerBrindePromocaoLiberada('@item.CODPRMPMC','@item.CODPMC', @item.SALDO, '@ViewBag.CodFuncionarioAutorPromocao', '@ViewBag.CodFuncionarioLogado')">
                                                <a data-rel="tooltip" title="Excluir o brinde da promoção" Class="btn btn-xs btn-danger">
                                                    <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </a>
                                            </div>
                                        Else
                                            @<div Class="hidden-sm hidden-xs btn-group" onclick="retornaBrindePromocaoLiberada('@item.CODPRMPMC','@item.CODPMC', @item.SALDO, '@ViewBag.CodFuncionarioAutorPromocao', '@ViewBag.CodFuncionarioLogado')">
                                                <a data-rel="tooltip" title="Desfazer exclusão do brinde" Class="btn btn-xs btn-primary">
                                                    <i Class="ace-icon fa fa-recycle bigger-120"></i>
                                                </a>
                                            </div>
                                        End If
                                        <div Class="hidden-sm hidden-xs btn-group" onclick="historicoBrindePromocaoLiberada('@item.CODPRMPMC','@item.CODPMC', '@item.TIPEDENIVPRMPMC')">
                                            <a data-rel="tooltip" title="Histórico" Class="btn btn-xs btn-default">
                                                <i Class="ace-icon fa fa-book bigger-120"></i>
                                            </a>
                                        </div>
                                    </td>
                                End If

                                @If (ViewBag.listaFaixa = True) Then
                                    @<td>
                                        @Html.DisplayFor(Function(modelItem) item.TIPEDENIVPRMPMC)
                                    </td>
                                End If

                                <td>
                                    @Html.DisplayFor(Function(modelItem) item.CODPRMPMC)
                                </td>

                                <td>
                                    @Html.DisplayFor(Function(modelItem) item.DESPRMPMC)
                                </td>

                                <td>
                                    @Html.DisplayFor(Function(modelItem) item.QDEBDEMPLCTRPMC)
                                </td>

                                <td>
                                    @Html.DisplayFor(Function(modelItem) item.QDEMERVNDPRMPMC)
                                </td>

                                <td>
                                    @Html.DisplayFor(Function(modelItem) item.QDEMERMAXPRMPMC)
                                </td>


                                @*Novos*@
                                <td>
                                    @Html.DisplayFor(Function(modelItem) item.SALDO)
                                </td>

                                <td>
                                    @Html.DisplayFor(Function(modelItem) item.MEDIA)
                                </td>


                                <td>
                                    @Html.DisplayFor(Function(modelItem) item.VLRPRMPMC)
                                </td>
                                @If (Model.Where(Function(fx) fx.DATECSPRD IsNot Nothing).Count > 0) Then
                                    @<td>
                                        @Html.DisplayFor(Function(modelItem) item.DATECSPRD)
                                    </td>
                                    @<td>
                                        @Html.DisplayFor(Function(modelItem) item.CODFNCECS) @Html.DisplayFor(Function(modelItem) item.NOMFNCECS)
                                    </td>
                                End If
                                @If (isCob) Then

                                    @<td>
                                        @Html.DisplayFor(Function(modelItem) item.VLRUNTLIQMER)
                                    </td>

                                    @<td>
                                        R$@Html.DisplayFor(Function(modelItem) item.CUSTO)
                                    </td>

                                    @<td>
                                        @Html.DisplayFor(Function(modelItem) item.VLRUNTPCOBRT)
                                    </td>
                                    @<td>
                                        @Code
                                            If (item.RATEAR = True) Then
                                                @<span>Sim</span>
                                            Else
                                                @<span>Não</span>
                                            End If
                                        End Code
                                    </td>
                                    @<td>
                                        @Html.DisplayFor(Function(modelItem) item.PERBFCBDE)
                                    </td>

                                End If
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</div>




@Code
    If (Model.Where(Function(f) f.INDISRBDEADI = 1).ToList().Count > 0) Then
        @<div class="widget-box widget-color-blue2">
            <div class="widget-header widget-header-small widget-light-blue">
                <h4 class="widget-title smaller blue lighter">
                    Brinde Adicional
                </h4>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div style="width: auto; overflow-x: scroll;">
                        <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:800px">
                            <thead>
                                <tr role="row">

                                    @if (ViewBag.EncerradaOuLiberada = False) Then
                                        @<th Class="center"></th>

                                    End If

                                    @If (ViewBag.listaFaixa = True) Then
                                        @<th>
                                            Faixa
                                        </th>
                                    End If

                                    <th>
                                        Brinde
                                    </th>
                                    <th>
                                        Descrição
                                    </th>
                                    <th>
                                        Múltiplo Brinde
                                    </th>
                                    <th>
                                        Qtde. Venda
                                    </th>
                                    <th>
                                        Estoque Máximo
                                    </th>
                                    <th>
                                        Saldo
                                    </th>
                                    <th>
                                        Média
                                    </th>
                                    <th>
                                        Valor Prêmio
                                    </th>


                                    @if (isCob) Then

                                        @<th>
                                            Valor
                                        </th>
                                        @<th>
                                            Custo
                                        </th>
                                        @<th>
                                            Preço Médio Venda
                                        </th>
                                        @<th>
                                            Ratear
                                        </th>
                                        @<th>
                                            % Benefício
                                        </th>
                                    End If
                                </tr>
                            </thead>
                            <tbody>

                                @For Each item In Model.Where(Function(f) f.INDISRBDEADI = 1).ToList()
                                    @<tr>
                                        @if (ViewBag.EncerradaOuLiberada = False) Then
                                            @<td Class="center">
                                                <div Class="hidden-sm hidden-xs btn-group" onclick="deleteBrinde('@item.CODPRMPMC','@item.CODPMC')">
                                                    <a data-rel="tooltip" title="Delete" Class="btn btn-xs btn-danger">
                                                        <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </a>
                                                </div>
                                            </td>

                                        End If





                                        @If (ViewBag.listaFaixa = True) Then
                                            @<td>
                                                @Html.DisplayFor(Function(modelItem) item.TIPEDENIVPRMPMC)
                                            </td>
                                        End If
                                        <td>
                                            @Html.DisplayFor(Function(modelItem) item.CODPRMPMC)
                                        </td>

                                        <td>
                                            @Html.DisplayFor(Function(modelItem) item.DESPRMPMC)
                                        </td>

                                        <td>
                                            @Html.DisplayFor(Function(modelItem) item.QDEBDEMPLCTRPMC)
                                        </td>

                                        <td>
                                            @Html.DisplayFor(Function(modelItem) item.QDEMERVNDPRMPMC)
                                        </td>

                                        <td>
                                            @Html.DisplayFor(Function(modelItem) item.QDEMERMAXPRMPMC)
                                        </td>
                                        @*Novos*@
                                        <td>
                                            @Html.DisplayFor(Function(modelItem) item.SALDO)
                                        </td>

                                        <td>
                                            @Html.DisplayFor(Function(modelItem) item.MEDIA)
                                        </td>

                                        <td>
                                            @Html.DisplayFor(Function(modelItem) item.VLRPRMPMC)
                                        </td>


                                        @if (isCob) Then
                                            @Code
                                                Dim valor As Decimal

                                                valor = (100 - item.PERBFCBDE) / 100.0 * item.CUSTO
                                            End Code


                                            @<td>
                                                R$@Math.Round(valor, 2)
                                            </td>

                                            @<td>
                                                R$@Html.DisplayFor(Function(modelItem) item.CUSTO)
                                            </td>

                                            @<td>
                                                @Html.DisplayFor(Function(modelItem) item.VLRUNTPCOBRT)
                                            </td>
                                            @<td>
                                                @Html.CheckBoxFor(Function(modelItem) item.RATEAR)
                                            </td>
                                            @<td>
                                                @Html.DisplayFor(Function(modelItem) item.PERBFCBDE)
                                            </td>

                                        End If
                                    </tr>
                                Next
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>





    End If
End Code

@section Scripts
    <script type="text/javascript" src="~/Scripts/Promocao.js"></script>
End Section
