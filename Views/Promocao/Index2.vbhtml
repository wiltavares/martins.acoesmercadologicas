﻿
@Code
    ViewData("Title") = "Promoção"
End Code

<div id="divFiltroPromocao">
    <div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-line-chart"></i>
                Promoção
            </li>
            <li class="active">
                Consulta
            </li>
        </ul>
    </div>
    <div class="widget-box widget-color-blue2 ">
        <div class="widget-header widget-header-small">
            <h4 class="widget-title smaller white lighter">
                <i class="ace-icon fa fa-sliders bigger-110"></i>
                Filtros de Pesquisa
            </h4>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-main">
                <form action="/martins.acoesmercadologicas/AcaoComercial/Pesquisar" data-ajax="true" data-ajax-begin="LoadingOn" data-ajax-complete="LoadingOff_UpdateMessage" data-ajax-method="POST" data-ajax-mode="replace" data-ajax-update="#resultadosView" id="form0" method="post">
                    <input name="__RequestVerificationToken" type="hidden" value="sJbZ15k2-W44HUMX5b5B7CXDzdwnVTvZBC08J_1xUHR3ZQutnGpIGN4wvyKS0puIBEC4N9Gnqa8JfM84cqY17ay3hvoFM-Gl4CVg0EcH9tU1">                <div class="form-horizontal">
                        <div class="row">
                            <div class="col-lg-4 col-sm-4">
                                <label for="status">Filial:</label>
                                <select class="chosen-select form-control" data-val="true" data-val-number="The field CODSTAAPVACOCMC must be a number." id="Filter_CODSTAAPVACOCMC" name="Filter.CODSTAAPVACOCMC" style="display: none;">
                                    <option value="">Selecione...</option>
                                    <option value="1">Filial 1</option>
                                    <option value="2">Filial 2</option>
                                    <option value="3">Filial 3</option>
                                    <option value="4">Filial 4</option>
                                    <option value="5">Filial 5</option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <label for="status">Pesquisar Por:</label>
                                <select class="chosen-select form-control" data-val="true" data-val-number="The field CODSTAAPVACOCMC must be a number." id="Filter_CODSTAAPVACOCMC" name="Filter.CODSTAAPVACOCMC" style="display: none;">
                                    <option value="">Selecione...</option>
                                    <option value="1">Codigo Promocao</option>
                                    <option value="2">Nome Promocao</option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <label for="data">Validade:</label>
                                <input id="dataIniAux" name="Filter.DataIniAux" type="hidden" value="">
                                <input id="dataFimAux" name="Filter.DataFimAux" type="hidden" value="">
                                <div class="input-daterange input-group">
                                    <input class="input-sm date-interval form-control" data-val="true" data-val-date="The field DATINIACOCMC must be a date." data-val-required="Preencha o campo Data Inicial" id="dataIni" name="Filter.DATINIACOCMC" onchange="copiaDataIni()" type="text" value="">
                                    <span class="input-group-addon">
                                        à
                                    </span>
                                    <input class="input-sm date-interval form-control" data-val="true" data-val-date="The field DATFIMACOCMC must be a date." data-val-required="Preencha o campo Data Final" id="dataFim" name="Filter.DATFIMACOCMC" onchange="copiaDataFim()" type="text" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="tipoEvento">Situação:</label>
                                <select class="chosen-select form-control" data-val="true" data-val-number="The field TIPEVTACOCMC must be a number." id="Filter_TIPEVTACOCMC" name="Filter.TIPEVTACOCMC" style="display: none;">
                                    <option value="">Selecione...</option>
                                    <option value="1">Todos   </option>
                                    <option value="2">Liberar      </option>
                                    <option value="3">Encerrada  </option>
                                    <option value="4">Liberada            </option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label for="evento">BU:</label>
                                <select class="chosen-select form-control" data-val="true" data-val-number="The field CODEVTACOCMC must be a number." id="Filter_CODEVTACOCMC" name="Filter.CODEVTACOCMC" style="display: none;">
                                    <option value="">Selecione...</option>
                                    <option value="1">Todos        </option>
                                    <option value="1">Nenhum        </option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group pull-left">
                                    <button type="submit" class="btn btn-info btn-white ">
                                        <i class="ace-icon fa fa-search bigger"></i>
                                        Pesquisar
                                    </button>
                                    <a href="http://localhost/martins.acoesmercadologicas/AcaoComercial/Consulta" class="btn btn-grey btn-white">
                                        <i class="ace-icon fa fa-eraser bigger"></i>
                                        Limpar
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="divResultadoPesquisaPromocao" class="widget-box widget-color-blue2 ">
        <div class="widget-header widget-header-small">
            <h4 class="widget-title smaller white lighter">
                <i class="ace-icon fa fa-list bigger-110"></i>
                Resultados
            </h4>
        </div>
        <div class="widget-body">
            <div class="widget-main">
                <div style="width: auto; overflow-x: scroll;">
                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1500px;">
                        <thead>
                            <tr role="row">
                                <th class="center"></th>
                                <th class="sorting" data-href="javascript:ordenarGrid_resultadosView('DATGRCACOCMCC')">
                                    Filial
                                </th>
                                <th class="selected-column sorting_desc" data-href="javascript:ordenarGrid_resultadosView('CODACOCMCC')">
                                    Código
                                </th>
                                <th class="sorting" data-href="javascript:ordenarGrid_resultadosView('NOMACOCMCC')">
                                    Nome Promoção
                                </th>
                                <th class="sorting" data-href="javascript:ordenarGrid_resultadosView('CODSTAAPVACOCMCC')">
                                    Situação
                                </th>
                                <th class="sorting" data-href="javascript:ordenarGrid_resultadosView('DATINIACOCMCC')">
                                    Dt. Inicio Promoção
                                </th>
                                <th class="sorting" data-href="javascript:ordenarGrid_resultadosView('DATFIMACOCMCC')">
                                    Dt. Fim Promoção
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="center">
                                    <div class="btn-group left">
                                        <button class="btn btn-xs btn-info" type="button" onclick="$('#divEditPromocaoGeral').show(); $('#divEditPromocao1').show(); $('#divEditPromocao2').show(); $('#divEditPromocao3').show(); $('#divEditPromocao4').show(); $('#divEditPromocao5').show(); $('#divEditPromocao6').show(); $('#divFiltroPromocao').hide(); $('#divResultadoPesquisaPromocao').hide();">
                                            <i class="ace-icon fa fa-pencil bigger-119"></i>
                                        </button>
                                    </div>


                                </td>
                                <td>
                                    Filial Uberlândia
                                </td>
                                <td>
                                    1
                                </td>
                                <td>
                                    Promoção Dezembro 2018
                                </td>
                                <td>
                                    Encerrada
                                </td>
                                <td>
                                    01/12/2018
                                </td>
                                <td>
                                    31/12/2018
                                </td>
                            </tr>
                            <tr>
                                <td class="center">
                                    <div class="btn-group left">
                                        <button class="btn btn-xs btn-info" type="button" onclick="$('#divEditPromocaoGeral').show(); $('#divEditPromocao1').show(); $('#divEditPromocao2').show(); $('#divEditPromocao3').show(); $('#divEditPromocao4').show(); $('#divEditPromocao5').show(); $('#divEditPromocao6').show(); $('#divFiltroPromocao').hide(); $('#divResultadoPesquisaPromocao').hide();">
                                            <i class="ace-icon fa fa-pencil bigger-119"></i>
                                        </button>
                                    </div>


                                </td>
                                <td>
                                    Filial Araguari
                                </td>
                                <td>
                                    2
                                </td>
                                <td>
                                    Promoção Dezembro 2018
                                </td>
                                <td>
                                    Liberar
                                </td>
                                <td>
                                    01/12/2018
                                </td>
                                <td>
                                    26/12/2018
                                </td>
                            </tr>
                            <tr>
                                <td class="center">
                                    <div class="btn-group left">
                                        <button class="btn btn-xs btn-info" type="button" onclick="$('#divEditPromocaoGeral').show(); $('#divEditPromocao1').show(); $('#divEditPromocao2').show(); $('#divEditPromocao3').show(); $('#divEditPromocao4').show(); $('#divEditPromocao5').show(); $('#divEditPromocao6').show(); $('#divFiltroPromocao').hide(); $('#divResultadoPesquisaPromocao').hide();">
                                            <i class="ace-icon fa fa-pencil bigger-119"></i>
                                        </button>
                                    </div>


                                </td>
                                <td>
                                    Filial Uberaba
                                </td>
                                <td>
                                    3
                                </td>
                                <td>
                                    Promoção Dezembro 2018
                                </td>
                                <td>
                                    Liberada
                                </td>
                                <td>
                                    20/12/2018
                                </td>
                                <td>
                                    02/01/2018
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br class="clearfix">


                <div class="row paginacao">
                    <div class="col-sm-4">
                        <div data-toggle="buttons" id="paginacaoView" class="btn-group" onchange="atualizarPageSize_resultadosView()">
                            <label class="btn btn-info btn-xs active">
                                <input checked="checked" name="10" type="radio" value="10">
                                10
                            </label>
                            <label class="btn btn-info btn-xs ">
                                <input name="25" type="radio" value="25">
                                25
                            </label>
                            <label class="btn btn-info btn-xs ">
                                <input name="50Radio" type="radio" value="50">
                                50
                            </label>
                            <label class="btn btn-info btn-xs ">
                                <input name="100" type="radio" value="100">
                                100
                            </label>
                        </div>
                        <p>Exibindo 4 de 4</p>
                    </div>
                    <div class="col-sm-8">
                        <div class="text-right">
                            <div class="pagination-container"><ul class="pagination"><li class="active"><a>1</a></li></ul></div>
                        </div>
                    </div>
                </div>
                <script>
                    $('th[data-href]').on("click", function () {
                        document.location = $(this).data('href');
                    });

                    function atualizarGrid_resultadosView(pageConf) { var dadosPesquisa = { 'Filter': { "CODACOCMC": null, "NOMACOCMC": null, "TIPDSNDSCBNF": null, "DATINIACOCMC": null, "DESOBJACOCMC": null, "INDACOCMCCPL": 0.0, "VLRTOTACOCMC": 0.0, "VLRTOTOCDACOCMC": 0.0, "VLRTOTTXAADMACOCMC": 0.0, "VLRTOTBDEACOCMC": 0.0, "CODFNCCRIACOCMC": 1299.0, "DATAPVACOCMC": null, "TIPFLUAPVACOCMC": null, "CODSTAAPVACOCMC": null, "CODFNCAPVATUACOCMC": null, "CODACOCMCORI": null, "DATFIMACOCMC": null, "DATGRCACOCMC": null, "INDALTFRNACOCMC": null, "CODCNLVNDINI": null, "TIPACOMCD": 1.0, "VALOR_SALDO": null, "CODFILEMPORIVBA": null, "DESCSTA": null, "NOMFNC": null, "NOMFNCAPVATU": null, "NOMUSRRCF": null, "CODEVTACOCMC": null, "NOMEVTACOCMC": null, "TIPEVTACOCMC": null, "NOMTIPEVTACOCMC": null, "CODFRN": null, "NOMFRN": null, "saldo": null, "ptcfrn": null, "SALDO_FORNECEDOR": null, "CODITEOCDACOCMC": null, "CODMER": null, "COD_FILTRO": null, "CODDIVCMP": null, "DESDIVCMP": null, "CODCPR": null, "NOMCPR": null, "CODOPE": null, "NOMOPE": null, "DataIniAux": null, "DataFimAux": null, "CODFILEMP": null, "CODOPEFSCDSNACOCMC": null, "VLRMAXACOCMC": 0.0, "VLRTXAADM": 0.0, "VLRTXABDE": 0.0, "EmpenhoExa": null, "DESOPEFSCDSNACOCMC": null, "CODITE": null, "VLRORC": null, "VLRUTZ": null, "VLRSLD": null, "INDVLDRGRICT": 0.0, "CanalVendaVisible": false, "Fornecedores": [], "StateGridFornecedores": null, "ItensOrcamento": null, "StateGridItensOrcamento": null, "Carimbos": [], "StateGridCarimbos": null, "PlanosDeMarketing": null, "ItensSubstituidos": null, "FluxosAprovacao": null, "Incentivos": null, "Mecanica": null, "Bonificacoes": [], "StateGridBonificacoes": null, "StateGridPremiacao": null, "Premiacao": [], "ValorTotal": 0.0, "SaldoOrcamento": 0.0, "ValorTotalBrinde": 0.0, "TaxaBrinde": 0.0, "ValorCliente": 0.0, "ValorParticipacao": 0.0, "SaldoReceitaCMV": 0.0, "TaxaAdmin": 0.0, "SaldoParticipacao": 0.0, "Enviar": false, "confirmaEnviar2": null, "confirmaEnviar1": null, "confirmaEnviarGerente": null, "IndPreAcordo": false, "DESFRMDSCBNF": null, "QDEDIARCBPMS": 0, "TIPFRMDSCBNF": 0 }, 'ResultID': 'resultadosView', 'PaginationID': 'paginacaoView', 'ActionName': 'Pesquisar', 'Page': pageConf, 'TotalCount': 4 }; atualizarGrid(pageConf, dadosPesquisa, '/martins.acoesmercadologicas/AcaoComercial/Pesquisar'); }
                    function ordenarGrid_resultadosView(orderBy) { var pageConf = { 'pageSize': 10, 'pageNumber': 1, 'orderBy': orderBy }; atualizarGrid_resultadosView(pageConf); }
                    function atualizarPageSize_resultadosView() { var pageConf = { 'pageSize': $('#paginacaoView label.active input').val(), 'pageNumber': 1, 'orderBy': 'CODACOCMCD' }; atualizarGrid_resultadosView(pageConf); }
                    function mudarDePagina_resultadosView(currPage) { var pageConf = { 'pageSize': 10, 'pageNumber': currPage, 'orderBy': 'CODACOCMCD' }; atualizarGrid_resultadosView(pageConf); }

                </script>

            </div>
        </div>
    </div>

</div>


<div id="divEditPromocaoGeral" class="widget-box widget-color-blue " style="display:none;">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Gerais
        </h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <br />
        <div id="divEditPromocao1" class="widget-box widget-color-wi">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller blue lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Promoção
                </h4>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                    </a>
                </div>

            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div>
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Ação:</label>
                                    <br />
                                    <span><b>123123</b></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Tipo:</label>
                                    <br />
                                    <span><b>Faixas Multiplas</b></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Quantidade de Faixa</label>
                                    <br />
                                    <span><b>8</b></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Participação Mix Total:</label>
                                    <br />
                                    <span><b>123123</b></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Mecanica:</label>
                                    <p><b>Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....</b></p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <label>BU:</label>
                                <select class="chosen-drop form-control">
                                    <option value="">Selecione...</option>
                                </select>
                            </div>
                            <div class="col-sm-8">
                                <label>Nome Promoção:</label>
                                <text class="form-control"></text>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Dt. Inicio:</label>
                                <datalist class="form-control"></datalist>
                            </div>
                            <div class="col-sm-4">
                                <label>Dt. Fím:</label>
                                <datalist class="form-control"></datalist>
                            </div>
                            <div class="col-sm-4">
                                <label>Status Envio:</label>
                                <select class="chosen-drop form-control">
                                    <option value="">Selecione...</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Visualizar Livro Simplificado:</label>
                                <br />
                                <input id="rblParticipacaoMixTotalSim" type="radio" value="1" />
                                <label for="rblParticipacaoMixTotalSim">Sim</label>
                                <input id="rblParticipacaoMixTotalNao" type="radio" value="2" />
                                <label for="rblParticipacaoMixTotalNao">Não</label>
                            </div>
                            <div class="col-sm-4">
                                <label>Publicar Promoção Aplicativo:</label>
                                <br />
                                <input id="rblParticipacaoMixTotalSim" type="radio" value="1" />
                                <label for="rblParticipacaoMixTotalSim">Sim</label>
                                <input id="rblParticipacaoMixTotalNao" type="radio" value="2" />
                                <label for="rblParticipacaoMixTotalNao">Não</label>
                            </div>
                            <div class="col-sm-4">
                                <br />
                                <label>Critério Adicional:</label>

                                <input id="rblParticipacaoMixTotalSim" type="checkbox" value="1" />
                                <label for="rblParticipacaoMixTotalSim"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Quantidade Grupo:</label>
                                <text class="form-control"></text>
                            </div>
                            <div class="col-sm-4">
                                <label>Quantidade Faixa:</label>
                                <text class="form-control"></text>
                            </div>
                            <div class="col-sm-4">
                                <label>Quantidade Máx. de brinde por cliente:</label>
                                <text class="form-control"></text>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Venda Avaliação Promoção Fator Cresc.:</label>
                                <text class="form-control"></text>
                            </div>
                            <div class="col-sm-4">
                                <label>valor Limite:</label>
                                <text class="form-control"></text>
                            </div>
                            <div class="col-sm-4">
                                <label>Quantidade Mínima:</label>
                                <text class="form-control"></text>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <br />
                                <label>Cobrança de Brinde:</label>

                                <input id="rblParticipacaoMixTotalSim" type="checkbox" value="1" />
                                <label for="rblParticipacaoMixTotalSim"></label>
                            </div>
                            <div class="col-sm-4">
                                <br />
                                <label>Brinde Adicional:</label>

                                <input id="rblParticipacaoMixTotalSim" type="checkbox" value="1" />
                                <label for="rblParticipacaoMixTotalSim"></label>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br />
        <div id="divEditPromocao2" class="widget-box widget-color-wi " style="display:none;">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller blue lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Brindes
                </h4>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                    </a>
                </div>

            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div>
                        <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 1500px;"></table>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="widget-box" style="    border-color: green;">
                                    <div class="widget-header widget-header-small" style=" border-color: green;">
                                        <h4 class="widget-title smaller green lighter">
                                            <i class="ace-icon fa fa-pencil bigger-110"></i>
                                            Dados
                                        </h4>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Brinde:</label>
                                                    <div class="input-group" id="adv-search">
                                                        <input type="text" class="form-control" />
                                                        <div class="input-group-btn">
                                                            <div class="btn-group" role="group">
                                                                <button type="button" class="btn btn-primary btn-light" style="border-radius: 3px; padding: 4px; margin-left: 1px;"><span class="glyphicon glyphicon-search" aria-hidden="true" style="color: #0000ffb8;"></span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Brinde Reserva:</label>
                                                    <text class="form-control"></text>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Múltiplo Brinde:</label>
                                                    <text class="form-control"></text>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Tipo Brinde:</label>
                                                    <br />
                                                    <input id="rblParticipacaoMixTotalSim" type="radio" value="1" />
                                                    <label for="rblParticipacaoMixTotalSim">Normal</label>
                                                    <input id="rblParticipacaoMixTotalNao" type="radio" value="2" />
                                                    <label for="rblParticipacaoMixTotalNao">Adicional</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Faixa:</label>
                                                    <select class="chosen-drop form-control">
                                                        <option value="">Selecione...</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>&nbsp;</label>
                                                    <br />
                                                    <button class="btn btn-success btn-white btn-adicionar">
                                                        Adicionar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="widget-box widget-color-blue2">
                                    <div class="widget-header widget-header-small widget-light-blue">
                                        <h4 class="widget-title smaller blue lighter">
                                            Brinde Normal
                                        </h4>
                                    </div>
                                    <div style="width: auto; overflow-x: scroll;">
                                        <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                            <thead>
                                                <tr role="row">
                                                    <th class="center">

                                                    </th>
                                                    <th>
                                                        Brinde
                                                    </th>
                                                    <th>
                                                        Brinde Reserva
                                                    </th>
                                                    <th>
                                                        Múltiplo Brinde
                                                    </th>
                                                    <th>
                                                        Faixa
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="center">
                                                        <button class="btn btn-xs btn-danger" title="Excluir">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        Bicicleta
                                                    </td>
                                                    <td>
                                                        10%
                                                    </td>
                                                    <td>
                                                        Não
                                                    </td>
                                                    <td>
                                                        Faixa 1
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center">
                                                        <button class="btn btn-xs btn-danger" title="Excluir">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        Monitor
                                                    </td>
                                                    <td>
                                                        15%
                                                    </td>
                                                    <td>
                                                        Não
                                                    </td>
                                                    <td>
                                                        Faixa 2
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="widget-box widget-color-blue2">
                                    <div class="widget-header widget-header-small widget-light-blue">
                                        <h4 class="widget-title smaller blue lighter">
                                            Brinde Adicional
                                        </h4>
                                    </div>
                                    <div style="width: auto; overflow-x: scroll;">
                                        <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                            <thead>
                                                <tr role="row">
                                                    <th class="center">

                                                    </th>
                                                    <th>
                                                        Brinde
                                                    </th>
                                                    <th>
                                                        Brinde Reserva
                                                    </th>
                                                    <th>
                                                        Múltiplo Brinde
                                                    </th>
                                                    <th>
                                                        Faixa
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="center">
                                                        <button class="btn btn-xs btn-danger" title="Excluir">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        TV Led 32''
                                                    </td>
                                                    <td>
                                                        25%
                                                    </td>
                                                    <td>
                                                        Não
                                                    </td>
                                                    <td>
                                                        Faixa 5
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div id="divEditPromocao3" class="widget-box widget-color-wi " style="display:none;">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller blue lighter">
                    <i class="ace-icon fa fa-comments bigger-110"></i>
                    Comunicações
                </h4>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                    </a>
                </div>

            </div>
            <div class="widget-body">
                <div class="widget-main">

                    <div class="row">
                        <div class="col-sm-12">

                            <div style="width: auto; overflow-x: scroll;">
                                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                    <thead>
                                        <tr role="row">
                                            <th class="center">
                                                Sel
                                            </th>
                                            <th>
                                                Código
                                            </th>
                                            <th>
                                                Comunicações
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="center">
                                                <input type="checkbox" title="Selecionar Todos" />
                                            </td>
                                            <td>
                                                1001
                                            </td>
                                            <td>
                                                <text class="form-control"></text>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="center">
                                                <input type="checkbox" title="Selecionar Todos" />
                                            </td>
                                            <td>
                                                1002
                                            </td>
                                            <td>
                                                <text class="form-control"></text>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="center">
                                                <input type="checkbox" title="Selecionar Todos" />
                                            </td>
                                            <td>
                                                1003
                                            </td>
                                            <td>
                                                <text class="form-control"></text>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <br />
        <div id="divEditPromocao4" class="widget-box widget-color-wi" style="display: none;">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller blue lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Critério de apuração
                </h4>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Forma de calculo:</label>
                            <select class="chosen-drop form-control">
                                <option value="">Selecione... </option>
                                <option value="1">ESCALA PROGRESSIVA</option>
                                <option value="2">FAIXAS FINITAS</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Critério:</label>
                            <select class="chosen-drop form-control">
                                <option value="">Selecione... </option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Parametros de Cálculo:</label>
                            <div style="width: auto; overflow-x: scroll; overflow-y:hidden;">
                                <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th style="width:33.33%">
                                                MIX
                                            </th>
                                            <th style="width:33.33%">
                                                Tipo Critério
                                            </th>
                                            <th style="width:33.33%">
                                                Critério
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="center">
                                                nome do critério
                                            </td>
                                            <td>
                                                <select class="chosen-drop form-control" style="width:300px;">
                                                    <option value="">Selecione... </option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="center">
                                                nome do critério
                                            </td>
                                            <td>
                                                <select class="chosen-drop form-control" style="width:300px;">
                                                    <option value="">Selecione... </option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="center">
                                                nome do critério
                                            </td>
                                            <td>
                                                <select class="chosen-drop form-control" style="width:300px;">
                                                    <option value="">Selecione... </option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label>Qde de brinde por atingimento:</label>
                            <input type="text" class="form-control" />
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br />
        <div id="divEditPromocao5" class="widget-box widget-color-wi" style="display: none;">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller blue lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Publico Alvo
                </h4>
            </div>
            <div class="widget-body">
                <div class="widget-main">

                    <div style="width: auto; overflow-x: scroll;">
                        <table class="table table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                            <thead>
                                <tr role="row">
                                    <th class="center">

                                    </th>
                                    <th>
                                        Publico alvo
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style="background-color: #f5f5f5;">
                                    <td colspan="2">
                                        <b>Região</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        Estado
                                    </td>
                                </tr>
                                <tr style="background-color: #f5f5f5;">
                                    <td colspan="2">
                                        <b>Clientes</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-warning" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-square-o bigger-119"></i>
                                            Selecionar
                                        </button>
                                    </td>
                                    <td>
                                        Atividade
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        Grupamento de Clientes
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        Negocio Clientes
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        Segmento Clientes
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        Cliente
                                    </td>
                                </tr>
                                <tr style="background-color: #f5f5f5;">
                                    <td colspan="2">
                                        <b>Força de Vendas</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        RCA/Vendedor
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        Gerente Regional
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        Gerente de Vendas
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        Nacional
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div id="divEditPromocao6" class="widget-box widget-color-wi ">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller blue lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Mix
                </h4>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                    </a>
                </div>
                <button class="btn btn-xs btn-white btn-success" style="float: right; color: white; margin-top: 2.5px; margin-right: 3px;" type="button">
                    <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                    Processar Alterações
                </button>
                <button class="btn btn-xs btn-white btn-yellow" style="float: right; color: white; margin-top: 2.5px; margin-right: 3px;" type="button" onclick=" $('#modalListTestarMercadoria').modal('show'); ChosenResize();">
                    <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                    Testar Alterações
                </button>
                <button class="btn btn-xs btn-white btn-info" style="float: right; color: white; margin-top: 2.5px; margin-right: 3px;" type="button" onclick=" $('#modalListTestarMercadoria').modal('show'); ChosenResize();">
                    <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                    Visualizar MIX Vigente
                </button>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div style="width: auto; overflow-x: scroll;">
                        <table class="table table-striped table-bordered table-hover dataTable">
                            <thead>

                            </thead>
                            <tbody>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        Grupo Mercadoria
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-warning" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-square-o bigger-119"></i>
                                            Selecionar
                                        </button>
                                    </td>
                                    <td>
                                        Família Mercadoria
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-warning" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-square-o bigger-119"></i>
                                            Selecionar
                                        </button>
                                    </td>
                                    <td>
                                        Classe Mercadoria
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        Mercadoria
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <button class="btn btn-xs btn-white btn-success" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal(); ChosenResize();">
                                            <i class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurar
                                        </button>
                                    </td>
                                    <td>
                                        Fornecedor
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalListTestarMercadoria" tabindex="-1" role="dialog" aria-labelledby="modalListTestarMercadoriaLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="widget-title smaller white lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Mercadorias
                </h4>
            </div>
            <div class="modal-body">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Informações:</label>
                            <p><b>Exibindo 10 mercadoria do GRUPO DE MERCADORIA.</b></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div id="gridItensOrcamento">

                            <div style="width: auto; overflow-x: scroll;">
                                <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" data-href="javascript:ordenarGrid_gridItensOrcamento('CODMERC')">
                                                Cod. Mercadoria
                                            </th>
                                            <th class="sorting" data-href="javascript:ordenarGrid_gridItensOrcamento('NOMMERC')">
                                                Nome Mercadoria
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1001
                                            </td>
                                            <td>
                                                Bicicleta
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1002
                                            </td>
                                            <td>
                                                Sabão em pó
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1003
                                            </td>
                                            <td>
                                                Bolacha
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1004
                                            </td>
                                            <td>
                                                Macarrão
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1005
                                            </td>
                                            <td>
                                                Óleo
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1006
                                            </td>
                                            <td>
                                                Feijão
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1007
                                            </td>
                                            <td>
                                                Extrato Elefante
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1008
                                            </td>
                                            <td>
                                                Sal
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1009
                                            </td>
                                            <td>
                                                Papel A4
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1010
                                            </td>
                                            <td>
                                                CD
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br class="clearfix">


                            <div class="row paginacao">
                                <div class="col-sm-4">
                                    <div data-toggle="buttons" id="paginacaoView" class="btn-group" onchange="atualizarPageSize_resultadosView()">
                                        <label class="btn btn-info btn-xs active">
                                            <input checked="checked" name="10" type="radio" value="10">
                                            10
                                        </label>
                                        <label class="btn btn-info btn-xs ">
                                            <input name="25" type="radio" value="25">
                                            25
                                        </label>
                                        <label class="btn btn-info btn-xs ">
                                            <input name="50Radio" type="radio" value="50">
                                            50
                                        </label>
                                        <label class="btn btn-info btn-xs ">
                                            <input name="100" type="radio" value="100">
                                            100
                                        </label>
                                    </div>
                                    <p>Exibindo 10 de 23</p>
                                </div>
                                <div class="col-sm-8">
                                    <div class="text-right">
                                        <div class="pagination-container"><ul class="pagination"><li class="active"><a>1</a></li><li><a href="javascript:mudarDePagina_resultadosView(2)">2</a></li><li><a href="javascript:mudarDePagina_resultadosView(3)">3</a></li><li class="PagedList-skipToNext"><a href="javascript:mudarDePagina_resultadosView(2)" rel="next">»</a></li></ul></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-white" type="button" onclick="$('#modalListTestarMercadoria').modal('hide');">
                    <i class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalListPublicoAlvoExemplo" tabindex="-1" role="dialog" aria-labelledby="modalListTestarMercadoriaLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header ">
                <i class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Selecionar [Tipo de publico alvo]
            </div>
            <div class="modal-body">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-xs-12">
                            <label for="item">Item Orçamento:</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <input class="form-control" data-val="true" data-val-number="The field CODMER must be a number." id="idItem" name="Filter.CODMER" type="text" value="">
                            <label class="text-danger-message-login" for="lblErroTamanhoIdMercadoria" id="lblErroTamanhoIdMercadoria" style="display: none;">Código do item pode ter até 10 digitos.</label>
                        </div>
                        <div class="col-lg-8 col-sm-6">
                            <div class="input-group">
                                <input class="form-control" id="DesIte" name="Filter.DESMER" type="text" value="">
                                <span class="input-group-btn">
                                    <button class="btn btn-info btn-white" type="button" onclick="btnBuscaItens()">
                                        <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="resultadosBuscaItens">
                        <div class="widget-box widget-color-blue2 ">
                            <div class="widget-header widget-header-small">
                                <h4 class="widget-title smaller white lighter">
                                    <i class="ace-icon fa fa-list bigger-110"></i>
                                    Resultados
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div style="width: auto; overflow-x: scroll;">
                                        <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                                            <thead>
                                                <tr role="row">
                                                    <th class="center">Selecionar</th>
                                                    <th class="sorting" data-href="javascript:ordenarGrid_resultadosBuscaItens('CODMERC')">
                                                        Código
                                                    </th>
                                                    <th class="sorting" data-href="javascript:ordenarGrid_resultadosBuscaItens('DESMERC')">
                                                        Nome
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="center">
                                                        <div>
                                                            <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                                <i class="ace-icon fa fa-check bigger-120"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        4787
                                                    </td>
                                                    <td>
                                                        Nome do item
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center">
                                                        <div>
                                                            <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-default" style="width: 32px;">
                                                                <i class="ace-icon fa fa-square-o bigger-120"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        4787
                                                    </td>
                                                    <td>
                                                        Nome do item
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center">
                                                        <div>
                                                            <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-default" style="width: 32px;">
                                                                <i class="ace-icon fa fa-square-o bigger-120"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        4787
                                                    </td>
                                                    <td>
                                                        Nome do item
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center">
                                                        <div>
                                                            <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-default" style="width: 32px;">
                                                                <i class="ace-icon fa fa-square-o bigger-120"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        4787
                                                    </td>
                                                    <td>
                                                        Nome do item
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center">
                                                        <div>
                                                            <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                                <i class="ace-icon fa fa-check bigger-120"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        4787
                                                    </td>
                                                    <td>
                                                        Nome do item
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center">
                                                        <div>
                                                            <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                                <i class="ace-icon fa fa-check bigger-120"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        4787
                                                    </td>
                                                    <td>
                                                        Nome do item
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center">
                                                        <div>
                                                            <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                                <i class="ace-icon fa fa-check bigger-120"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        4787
                                                    </td>
                                                    <td>
                                                        Nome do item
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center">
                                                        <div>
                                                            <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                                <i class="ace-icon fa fa-check bigger-120"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        4787
                                                    </td>
                                                    <td>
                                                        Nome do item
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center">
                                                        <div>
                                                            <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                                <i class="ace-icon fa fa-check bigger-120"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        4787
                                                    </td>
                                                    <td>
                                                        Nome do item
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center">
                                                        <div>
                                                            <button data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" style="width: 32px;">
                                                                <i class="ace-icon fa fa-check bigger-120"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        4787
                                                    </td>
                                                    <td>
                                                        Nome do item
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <br class="clearfix">


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <div class="btn-group pull-right">
                    <button class="btn btn-success btn-white " type="button">
                        <i class="ace-icon fa fa-file-excel-o bigger-120 success"></i>
                        Importar CSV
                    </button>
                    <button class="btn btn-info btn-white" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal('hide');">
                        <i class="ace-icon fa fa-check bigger-120 blue"></i>
                        Concluir
                    </button>
                    <button class="btn btn-danger btn-white" type="button" onclick="$('#modalListPublicoAlvoExemplo').modal('hide');">
                        <i class="ace-icon fa fa-close bigger-120 red"></i>
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>

</div>