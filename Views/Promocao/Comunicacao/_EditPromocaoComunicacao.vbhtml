﻿@ModelType  PromocaoComunicacaoSaveViewModel

@Code
    Dim OpcoesComunicacao As List(Of PromocaoComunicacaoViewModel) = ViewBag.OpcoesComunicacao

    @<div class="row">
        <div class="col-md-6">
            <table id="tblGridRegistrosComunicacao" Class="table table-bordered dataTable" role="grid">
                <thead>
                    <tr role="row">
                        <th>
                            Sel
                        </th>
                        <th>
                            Código
                        </th>
                        <th>
                            Comunicações
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @For Each item In Model.RelacaoComunicacao
                        @Code
                            Dim checked = ""
                            If (item.CODPMC IsNot Nothing) Then
                                checked = "checked"
                            End If
                        End Code
                        @<tr>
                            <td Class="center">
                                <input id="chbTipCmnPmc_@item.TIPCMNPMC" type="checkbox" @checked>
                            </td>
                            <td>
                                @Html.HiddenFor(Function(model) item.TIPCMNPMC)
                                @Html.DisplayFor(Function(model) item.TIPCMNPMC)
                            </td>
                            <td>
                                @Html.DisplayFor(Function(model) item.DESTIPCMNPMC)
                            </td>
                        </tr>
                    Next
                </tbody>
            </table>

            <button type="submit" class="btn btn-white btn-success" onclick="salvarComunicacoes();" style="display: block; margin-top: 12px;">
                <i class="ace-icon fa fa-save bigger-120 green"></i>
                Salvar
            </button>
        </div>

        <div class="col-md-6">
            @Code
                Dim itemLivro = Model.RelacaoComunicacao.Where(Function(f) f.TIPCMNPMC = 3).FirstOrDefault()
            End Code
            <table id="tblGridLivro" Class="@IIf(itemLivro.CODPMC Is Nothing, "hide", "") table table-striped table-bordered dataTable" role="grid">
                <tbody>
                    <tr>
                        <td>
                            Código Simbolo (A-Z):
                        </td>
                        <td>
                            @Html.TextBoxFor(Function(Model) itemLivro.CODSMBDVGPMCLIVPCO, New With {.Class = "form-control", .id = "codigoSimbolo", .maxlength = "1"})
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Forma de divulgação:
                        </td>
                        <td style="padding-top: 0; padding-bottom: 0;">
                            @For Each item In OpcoesComunicacao
                                @<div Class="radio">
                                    <label>
                                        @Html.RadioButtonFor(Function(Model) itemLivro.TIPDVGPMCLIVPCO, item.TIPDVGPMCLIVPCO)
                                        @Html.DisplayFor(Function(Model) item.DESTIPDVGPMCLIVPCO)
                                    </label>
                                </div>
                            Next
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
End Code
