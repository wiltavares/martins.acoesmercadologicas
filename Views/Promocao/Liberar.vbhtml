﻿@ModelType RetornoValidaQtd
@Imports Martins.AcoesMercadologicas.Core






<div Class="modal-dialog modal-lg hideScroll" id="modalLiberarPromocaoResultado" style="z-index: 1051; margin-top: 4px;">
    <div Class="modal-content">
        @If (Model.Erro = "SUCESSO") Then
            @<div Class="modal-header ">
                <i Class="ace-icon fa fa-check bigger-120"></i>
                &nbsp;Sucesso
            </div>
        Else
            @<div Class="modal-header ">
                <i Class="ace-icon fa fa-minus-square bigger-120"></i>
                &nbsp;Não foi possível liberar a Promoção
            </div>
        End If

        <div Class="modal-body" style="overflow-y: scroll !important;">
            @If (Model.Erro = "SUCESSO") Then
                @<h3>
                    Promoção liberada com sucesso!
                </h3>
            Else
               @Html.Raw(Model.Erro)

                @If (Model.lstMercadoriaErros IsNot Nothing AndAlso Model.lstMercadoriaErros.Count > 0) Then
                    @<div>
                        <Table Class="table table-striped">
                            <thead>
                                <tr>
                                    <td>
                                        Código
                                    </td>
                                    <td>
                                        Descrição
                                    </td>
                                    <td>
                                        ERRO
                                    </td>
                                    <td>
                                        TIPO
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                @Code
                                    For Each item In Model.lstMercadoriaErros
                                        @<tr>
                                            <td>@item.CODPRMPMC</td>
                                            <td>@Html.DisplayFor(Function(model) item.DESMER)</td>
                                            <td>@Html.DisplayFor(Function(model) item.ERRO)</td>
                                            <td>@Html.DisplayFor(Function(model) item.TIPO)</td>
                                        </tr>
                                    Next
                                End Code
                            </tbody>
                        </Table>
                    </div>
                End If
            End If

        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalLiberarPromocao();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Fechar
                </Button>
            </div>
        </div>
    </div>
</div>
