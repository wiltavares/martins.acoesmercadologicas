﻿@ModelType PromocaoBrindeViewModel
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-wi">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller blue lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Brindes
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main" id="widgetBrindes">
            <div id="divCreateBrinde">

                <div class="widget-box" style="border-color: green;">
                    <div class="widget-header widget-header-small" style=" border-color: green;">
                        <h4 class="widget-title smaller green lighter">
                            <i class="ace-icon fa fa-pencil bigger-110"></i>
                            Incluir Brinde
                        </h4>
                        <div class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                            </a>
                        </div>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="form-horizontal">
                                <div class="widget-body">
                                    <div class="widget-main">

                                        <form id="frmAddStepBrinde">
                                            <div class="row">
                                                @Html.HiddenFor(Function(Model) Model.CODPMC)
                                                @Html.HiddenFor(Function(Model) Model.CODACOCMC, New With {.id = "CODACAOBRINDE"})
                                                @Html.HiddenFor(Function(Model) Model.PERMERRSVPRMPMC)

                                                <div class="col-sm-2">
                                                    @Html.LabelFor(Function(Model) Model.CODPRMPMC, "Cod.:")
                                                    <br />
                                                    @Html.TextBoxFor(Function(Model) Model.CODPRMPMC, New With {.Class = "form-control", .id = "idItemCodMer", .onChange = "modalBuscarItens()"})
                                                </div>
                                                <div class="col-sm-4">
                                                    @Html.LabelFor(Function(Model) Model.DESPRMPMC, "Brinde:")
                                                    <br />
                                                    <div class="input-group ">
                                                        @Html.TextBoxFor(Function(f) f.DESPRMPMC, Nothing, New With {.Class = "form-control", .id = "itemBusca"})
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-white" id="btBscIte" type="button" onclick="modalBuscarItens()" style="display: block">
                                                                <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                                            </button>
                                                            <button class="btn btn-white" id="btTiraIte" type="button" onclick="tirarItem()" style="display: none">
                                                                <i class="ace-icon fa fa-close bigger-120 red"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    @Html.LabelFor(Function(Model) Model.QDEBDEMPLCTRPMC, "Múltiplo Brinde:")
                                                    <br />
                                                    @Html.TextBoxFor(Function(Model) Model.QDEBDEMPLCTRPMC, New With {.Class = "input-sm form-control", .id = "IdMultBrinde", .Value = 1, .alt = "inteiro_positivo"})
                                                </div>
                                                @If (ViewBag.listaFaixa.Count > 0) Then
                                                    @<div Class="col-sm-3">
                                                        @Html.LabelFor(Function(Model) Model.TIPEDENIVPRMPMC, "Faixa:")
                                                        <br />
                                                        @Html.DropDownListFor(Function(Model) Model.TIPEDENIVPRMPMC, New SelectList(ViewBag.listaFaixa, "VALUE", "TEXT", Model.TIPEDENIVPRMPMC), New With {.Class = "form-control", .id = "dropStatus"})
                                                    </div>
                                                End If

                                                <div class="col-sm-3" id="divTipoBrinde">
                                                    @Html.LabelFor(Function(Model) Model.INDISRBDEADI, "Tipo Brinde:")
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>
                                                                @Html.RadioButtonFor(Function(Model) Model.INDISRBDEADI, Constantes.NAO, New With {.checked = True})
                                                                Normal
                                                            </label>
                                                        </div>
                                                        @If (ViewBag.BrindeAdicional) Then
                                                            @<div Class="col-xs-6">
                                                                <label>
                                                                    @Html.RadioButtonFor(Function(Model) Model.INDISRBDEADI, Constantes.SIM)
                                                                    Adicional
                                                                </label>
                                                            </div>
                                                        End If
                                                    </div>
                                                </div>
                                            </div>
                                            @if (ViewBag.IndCobrancaBrinde) Then
                                                @<div class="row">
                                                    <div class="col-sm-3">
                                                        <label>Custo:</label>
                                                        @Html.LabelFor(Function(Model) Model.CUSTO, "Custo:")
                                                        <br>
                                                        @Html.TextBoxFor(Function(Model) Model.CUSTO, New With {.Class = "form-control", .id = "custoIncluirBrinde", .alt = "currency", .readonly = "readonly"})
                                                    </div>
                                                    <div class="col-sm-3">
                                                        @Html.LabelFor(Function(Model) Model.VLRUNTPCOBRT, "Preço Médio Venda:")
                                                        <br>
                                                        @Html.TextBoxFor(Function(Model) Model.VLRUNTPCOBRT, New With {.Class = "form-control", .id = "precoIncluirBrinde", .alt = "currency", .readonly = "readonly"})
                                                    </div>
                                                    <div class="col-sm-2">
                                                        @Html.LabelFor(Function(Model) Model.PERBFCBDE, "% Benefício:")
                                                        <br />
                                                        @Html.TextBoxFor(Function(Model) Model.PERBFCBDE, New With {.Class = "form-control", .id = "percIncluirBrinde", .alt = "currency_positivo", .maxlength = "6", .size = "1"})
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Valor:</label>
                                                        <br>
                                                        @Html.TextBoxFor(Function(Model) Model.VLRUNTLIQMER, New With {.Class = "input-sm form-control", .id = "valorIncluirBrinde", .alt = "currency_positivo"})
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <label>&nbsp;</label>
                                                        <br />
                                                        <label>
                                                            @Html.CheckBoxFor(Function(Model) Model.RATEAR)
                                                            Ratear
                                                        </label>
                                                    </div>
                                                </div>
                                            End If

                                            <div class="row">
                                                <div Class="col-sm-12">
                                                    <label>&nbsp;</label>
                                                    <br />
                                                    <div Class="btn-group pull-left">
                                                        <Button type="button" Class="btn btn-white btn-success " onclick="AddBrinde()">
                                                            <i Class="ace-icon fa fa-save bigger-120 green"></i>
                                                            Adicionar
                                                        </Button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="wdgStepBrindeResultNormal" class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small widget-light-blue">
                    <h4 class="widget-title smaller blue lighter">
                        Brinde Normal
                    </h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                        </a>
                    </div>

                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div style="width: auto; overflow-x: scroll;">
                            <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:800px">
                                <thead>
                                    <tr role="row">
                                        <th class="center"></th>
                                        @If (ViewBag.listaFaixa.Count > 0) Then
                                            @<th>Faixa</th>
                                        End If
                                        <th>Brinde</th>
                                        <th>Descrição</th>
                                        <th>Múltiplo Brinde</th>
                                        @if (ViewBag.IndCobrancaBrinde) Then
                                            @<th>Custo</th>
                                            @<th>Preço Médio Venda</th>
                                            @<th>% Benefício</th>
                                            @<th>Valor</th>
                                            @<th>Ratear</th>
                                        End If
                                        @*<th>Qtde. Venda</th>
                                            <th>Estoque Máximo</th>
                                            <th>Saldo</th>
                                            <th>Média</th>
                                            <th>Per. Reserva</th>
                                            <th>Valor Do Brinde</th>*@
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>




            @If (ViewBag.BrindeAdicional) Then
                @<div id="wdgStepBrindeResultAdicional" Class="widget-box widget-color-blue2">
                    <div Class="widget-header widget-header-small widget-light-blue">
                        <h4 Class="widget-title smaller blue lighter">
                            Brinde Adicional
                        </h4>
                        <div Class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i Class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                            </a>
                        </div>
                    </div>
                    <div Class="widget-body">
                        <div Class="widget-main">
                            <div style="width: auto; overflow-x: scroll;">
                                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:800px">
                                    <thead>
                                        <tr role="row">
                                            <th class="center"></th>
                                            @If (ViewBag.listaFaixa.Count > 0) Then
                                                @<th>Faixa</th>
                                            End If
                                            <th>Brinde</th>
                                            <th>Descrição</th>
                                            <th>Múltiplo Brinde</th>
                                            @if (ViewBag.IndCobrancaBrinde) Then
                                                @<th>Custo</th>
                                                @<th>Preço Médio Venda</th>
                                                @<th>% Benefício</th>
                                                @<th>Valor</th>
                                                @<th>Ratear</th>
                                            End If
                                            @*<th>Qtde. Venda</th>
                                                <th>Estoque Máximo</th>
                                                <th>Saldo</th>
                                                <th>Média</th>
                                                <th>Per. Reserva</th>
                                                <th>Valor do Brinde</th>*@
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            End If
        </div>
    </div>
</div>



<div class="row">
    <div class="col-sm-12">
        <hr />
        <div class="btn-group" style="float:right;">
            <Button Class="btn btn-white btn-prev" onclick="VoltarStepBrindes();">
                <i Class="ace-icon fa fa-arrow-left"></i>
                Voltar
            </button>
            <button class="btn btn-success btn-white btn-next" onclick="AvancarStepBrindes();">
                Avançar
                <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
            </button>
            <a class="btn btn-danger btn-white" href="@Url.Action("", "PromocaoWizard")">
                <i class="ace-icon fa fa-close bigger-120 red"></i>
                Sair
            </a>
        </div>
    </div>
</div>


<div id="modalBuscaItem" class="modal fade"></div>
