﻿@ModelType  GridSettings(Of RelacaoAcaoMecanicaViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 ">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Pendentes de geração da promoção
        </h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div id="divwidgetMecanica" class="widget-main">

            <div class="data-msg" id="msgModalPromoção">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            @For Each item In Model.ListPaged
                @<div class="well well-sm">
                    <div class="row">
                        <div class="col-sm-1">
                            <a class="btn btn-xs btn-success" href="@Url.Action("CreateWizard", "PromocaoWizard")/@item.CODACOCMC/@item.NUMSEQRGRACOCMC">
                                <i class="ace-icon fa fa-cogs bigger-119"></i>
                            </a>
                            @If (item.CODACOCMCCPI <> 0) Then
                                @<button class="btn btn-xs btn-primary" onclick="ClonarPromocoesDaAcao('@item.CODACOCMC');" title="Clonar as promoções da ação original">
                                    <i Class="ace-icon fa fa-clone bigger-119"></i>
                                </button>
                            End If
                        </div>
                        <div Class="col-sm-7">
                            <Label>
                                @If (item.TIPACOMCD = TipoAcao.PreAcordoVigencia) Then
                                    @<span style="color:limegreen">Pré Acordo Limitado por Vigência:</span>
                                ElseIf item.TIPACOMCD = TipoAcao.PreAcordoValor Then
                                    @<span style="color:red">Pré Acordo Limitado por Valor:</span>
                                Else
                                    @<span>Ação Mercadológica:</span>
                                End If
                                <b>
                                    @Html.DisplayFor(Function(model) item.CODACOCMC) - @Html.DisplayFor(Function(model) item.NOMACOCMC)
                                </b>
                            </Label>
                        </div>
                        <div class="col-sm-4">
                            <label>Autor: <b>@Html.DisplayFor(Function(model) item.NOMFNC)</b></label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-3">
                            <label>Geração: <b>@Html.DisplayFor(Function(model) item.DATGRCACOCMC)</b></label>
                        </div>
                        <div class="col-sm-4">
                            <label>Vigência: <b>@Html.DisplayFor(Function(model) item.DATINIACOCMC) até: @Html.DisplayFor(Function(model) item.DATFIMACOCMC)</b></label>
                        </div>
                        <div class="col-sm-4">
                            <label>Valor Orçado: <b>@Html.DisplayFor(Function(model) item.VLRTOTACOCMC)</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1">

                        </div>
                        <div class="col-sm-11">
                            <label>Mecânica: <b>@Html.DisplayFor(Function(model) item.DESRGRACOCMC)</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1">

                        </div>
                        <div class="col-sm-3">
                            @Select Case item.TIPPMC
                                Case 0
            @<label>Tipo: <b>Normal</b></label>
                                Case 1
            @<label>Tipo: <b>Combinada</b></label>
                                Case 2
                                    @<label>Tipo: <b>Faixas Multiplas</b></label>
                            End Select
                        </div>
                        <div class="col-sm-4">
                            @Select Case item.TIPPMC
                                Case 1
                                    @<label>Quantidade de Grupo:<b>@Html.DisplayFor(Function(model) item.QDEGRPMIXPMC)</b></label>
                                                        Case 2
                                                            @<label>Quantidade de Faixa:<b>@Html.DisplayFor(Function(model) item.QDEFXAPMC)</b></label>
                                                    End Select
                        </div>
                        <div class="col-sm-4">
                            @Select Case item.INDPTCTOTMIXPMC
                                Case 1
            @<label>Participação Mix Total: <b>Sim</b></label>
                                Case 0
                                    @<label>Participação Mix Total: <b>Não</b></label>
                            End Select

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-11">
                            <label>Filiais:</label>
                            @For Each filial In item.RelacaoAcaoMecanicaFilialList
                                If (Not filial.CODPMC Is Nothing) Then
                                    @<span Class="badge badge-success">@Html.DisplayFor(Function(model) filial.DESABVFILEMP)</span>
                                ElseIf (Not filial.DATECSUSR Is Nothing) Then
                                    @<span Class="badge badge-danger">@Html.DisplayFor(Function(model) filial.DESABVFILEMP)</span>
                                Else
                                    @<span Class="badge badge-primary">@Html.DisplayFor(Function(model) filial.DESABVFILEMP)</span>
                                End If
                            Next
                        </div>
                    </div>
                </div>
            Next

            <br Class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code
        </div>
    </div>
</div>




