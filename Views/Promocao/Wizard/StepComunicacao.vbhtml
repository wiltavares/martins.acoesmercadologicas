﻿@ModelType  PromocaoComunicacaoSaveViewModel


<div class="row">
    <div class="col-sm-12">
        <div class="widget-box widget-color-blue2">
            <div class="widget-header widget-header-small widget-light-blue">
                <h4 class="widget-title smaller blue lighter">
                    <i class="ace-icon fa fa-comments bigger-110"></i>
                    Comunicações
                </h4>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row">
                        <div class="col-md-6">
                            <table id="tblGridRegistrosComunicacao" Class="table table-bordered dataTable" role="grid">
                                <thead>
                                    <tr role="row">
                                        <th>
                                            Sel
                                        </th>
                                        <th>
                                            Código
                                        </th>
                                        <th>
                                            Comunicações
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @For Each item In Model.RelacaoComunicacao
                                        @<tr>
                                            <td Class="center">
                                                <input id="chbTipCmnPmc_@item.TIPCMNPMC" type="checkbox" checked>
                                            </td>
                                            <td>
                                                @Html.HiddenFor(Function(model) item.TIPCMNPMC)
                                                @Html.DisplayFor(Function(model) item.TIPCMNPMC)
                                            </td>
                                            <td>
                                                @Html.DisplayFor(Function(model) item.DESTIPCMNPMC)
                                            </td>
                                        </tr>
                                    Next
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-6">
                            <table id="tblGridLivro" Class="table table-striped table-bordered dataTable" role="grid">
                                <tbody>
                                    <tr>
                                        <td>
                                            Código Simbolo (A-Z):
                                        </td>
                                        <td>
                                            <input Class="input-sm form-control" id="codigoSimbolo" type="text" maxlength="1" value="P">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Forma de divulgação:
                                        </td>
                                        <td style="padding-top: 0; padding-bottom: 0;">
                                            @For Each item In ViewBag.OpcoesComunicacao
                                                @<div Class="radio">
                                                    <Label> <input type="radio" name="formaDivulgRadio" value="@item.TIPDVGPMCLIVPCO">@item.DESTIPDVGPMCLIVPCO</Label>
                                                </div>
                                            Next
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <hr />
        <div class="btn-group" style="float:right;">
            @If (Not (ViewBag.IsCashBack)) Then
                @<Button Class="btn btn-white btn-prev" onclick="VoltarStepComunicacao();">
                    <i Class="ace-icon fa fa-arrow-left"></i>
                    Voltar
                </Button>
            Else
                @<Button Class="btn btn-white btn-prev" onclick="VoltarStepBrindes();">
                    <i Class="ace-icon fa fa-arrow-left"></i>
                    Voltar
                </Button>
            End If
            <button class="btn btn-success btn-white btn-next" onclick="AvancarStepComunicacao();">
                Criar
                <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
            </button>
            <a class="btn btn-danger btn-white" href="@Url.Action("", "PromocaoWizard")">
                <i class="ace-icon fa fa-close bigger-120 red"></i>
                Sair
            </a>
        </div>
    </div>
</div>