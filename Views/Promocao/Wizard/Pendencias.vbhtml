﻿@ModelType  GridSettings(Of RelacaoAcaoMecanicaViewModel)
@Imports Martins.AcoesMercadologicas.Core


@Code
    ViewData("Title") = "Promoções - Pendências"
    Dim ajaxopt As New AjaxOptions() With {
            .UpdateTargetId = "resultadosView",
            .InsertionMode = InsertionMode.Replace,
            .OnBegin = "LoadingOn",
            .OnComplete = "LoadingOff_UpdateMessage",
            .HttpMethod = "POST"
    }

End Code
<div id="divMecanica">

    <div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-line-chart"></i>
                Promoção
            </li>
            <li class="active">
                Pendentes de geração
            </li>
        </ul>
    </div>

    <div class="widget-box widget-color-blue2">
        <div class="widget-header widget-header-small">
            <h4 class="widget-title smaller white lighter">
                <i class="ace-icon fa fa-sliders bigger-110"></i>
                Filtros de Pesquisa
            </h4>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-main">
                @Using (Ajax.BeginForm("Pesquisar", "PromocaoWizard", ajaxopt))
                    @Html.AntiForgeryToken()
                    @<div class="form-horizontal">
                        <div class="row">
                            <div class="col-lg-1 col-sm-6">
                                @Html.Label("acaoID", String.Format("{0}:", "Cod Acão"))
                                @Html.TextBoxFor(Function(m) m.Filter.CODACOCMC, New With {.Class = "form-control", .alt = "inteiro"})
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                @Html.Label("acaoNome", "Nome:")
                                @Html.TextBoxFor(Function(m) m.Filter.NOMACOCMC, New With {.Class = "form-control"})
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                @Html.Label("data", "Validade:")
                                <div class="input-daterange input-group">
                                    @Html.TextBoxFor(Function(m) m.Filter.DATINIACOCMC, New With {.class = "input-sm date-picker form-control", .autocomplete = "off"})
                                    <span class="input-group-addon">
                                        à
                                    </span>
                                    @Html.TextBoxFor(Function(m) m.Filter.DATFIMACOCMC, New With {.class = "input-sm date-picker form-control", .autocomplete = "off"})
                                </div>
                            </div>
                            <div class="col-sm-4">
                                @Html.Label("autor", "Autor:")
                                @Html.DropDownListFor(Function(m) m.Filter.CODFNCCRIACOCMC, New SelectList(ViewBag.listaFuncionarios, "CODFNC", "CODIGO_NOME", Model.Filter.CODFNCCRIACOCMC), "Selecione...", New With {.Class = "chosen-select form-control"})
                            </div>
                        </div>

                        <br />
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group pull-left">
                                    <button type="submit" class="btn btn-info btn-white ">
                                        <i class="ace-icon fa fa-search bigger"></i>
                                        Pesquisar
                                    </button>
                                    <a href="@Request.Url.AbsoluteUri" class="btn btn-grey btn-white">
                                        <i Class="ace-icon fa fa-eraser bigger"></i>
                                        Limpar
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                End Using
            </div>
        </div>
    </div>
    <br class="clearfix" />
    <div id="resultadosView">
        @Code
            Html.RenderPartial("~/Views/Promocao/Wizard/PendenciasResultados.vbhtml", Model)
        End Code
    </div>
</div>


<div id="modalResumoCloneProcoesDaAcao"></div>


@section  scripts
    <script src="~/Scripts/PromocaoCreateWizard.js"></script>
End Section

@*<script>
        DatePickerConfig();
    </script>*@