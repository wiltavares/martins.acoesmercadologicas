﻿@ModelType PromocaoViewModel

<form id="frmStepGeral">
    <div class="row">
        <div class="col-sm-4">
            @Html.Label("lblCODUNDESRNGC", "BU (*):")
            @Html.DropDownListFor(Function(Model) Model.CODUNDESRNGC, New SelectList(ViewBag.listaBU, "CODUNDESRNGC", "DESUNDESRNGC", Model.CODFILEMPORIVBA), "Selecione...", New With {.Class = "form-control chosen-select"})
            @Html.ValidationMessageFor(Function(m) m.CODUNDESRNGC, "", New With {.class = "text-danger-message-login"})
        </div>
        <div Class="col-sm-8">
            @Html.Label("lblNOMPMC", "Nome da Promoção (*):")

            @Html.TextBoxFor(Function(Model) Model.NOMPMC, New With {.class = "input-md form-control", .id = "NonPromocao", .maxlength = "55"})
            @Html.ValidationMessageFor(Function(m) m.NOMPMC, "", New With {.class = "text-danger-message-login"})

        </div>
        
    </div>
    <br />
    <div Class="row">
        @If (ViewBag.IsCashBack) Then
            @<div Class="col-sm-4">
                <label style="transform: translateY(105%);">
                    @Html.CheckBoxFor(Function(Model) Model.INDPMCIMDBool, New With {.Id = "ckbPromocaoRelampago", .onClick = "ckbPromocaoRelampago_OnClick(this.checked)"})
                    Promoção Relâmpago
                </label>
            </div>
        End If

        <div Class="col-sm-4" id="divDataInicio">
            @Html.Label("lblDATINIPMC", "Dt. de Início(*):")
            <div Class="input-daterange input-group">
                @Html.TextBoxFor(Function(Model) Model.DATINIPMC, New With {.class = "input-sm date-interval form-control date-picker", .alt = "datepicker", .autocomplete = "off"})
                @Html.ValidationMessageFor(Function(m) m.DATINIPMC, "", New With {.class = "text-danger-message-login"})
            </div>
        </div>

        <div Class="col-sm-1 horas" style="display:none">
            @Html.Label("lblDATINIPMC", "Hr. Inicio(*):")
            @Html.TextBoxFor(Function(Model) Model.HRAINIPMC, "{0:HH:mm}", New With {.Class = "input-md form-control", .id = "horaIni", .alt = "time_hh_mm", .autocomplete = "off", .maxlength = "10"})
            @Html.ValidationMessageFor(Function(m) m.HRAINIPMC, "", New With {.class = "text-danger-message-login"})
        </div>

        <div Class="col-sm-4" id="divDataFim">
            @Html.Label("lblDATFIMPMC", "Dt. de Fim (*):")
            <div class="input-daterange input-group">
                @Html.TextBoxFor(Function(Model) Model.DATFIMPMC, New With {.class = "input-sm date-interval form-control date-picker", .alt = "datepicker", .autocomplete = "off"})
                @Html.ValidationMessageFor(Function(m) m.DATFIMPMC, "", New With {.class = "text-danger-message-login"})
            </div>
        </div>

        <div Class="col-sm-1 horas" style="display:none">
            @Html.Label("lblDATINIPMC", "Hr. Fim(*):")
            @Html.TextBoxFor(Function(Model) Model.HRAFIMPMC, "{0:HH:mm}", New With {.Class = "input-md form-control", .id = "horaFim", .pattern = "([01][0-9]|2[0-3]):[0-5][0-9]", .alt = "time_hh_mm", .autocomplete = "off", .maxlength = "10"})
            @Html.ValidationMessageFor(Function(m) m.HRAFIMPMC, "", New With {.class = "text-danger-message-login"})
        </div>

    </div>
    
    <br />
    <div class="row">
        <div class="col-sm-4">
            @code
                Dim statusEnvioLookup = New List(Of SelectListItem)()
                If (Not (ViewBag.IsCashBack)) Then
                    statusEnvioLookup.Add(New SelectListItem() With {.Text = "Selecione...", .Value = ""})
                    statusEnvioLookup.Add(New SelectListItem() With {.Text = "Enviado", .Value = " "})
                    statusEnvioLookup.Add(New SelectListItem() With {.Text = "A Enviar", .Value = "S"})
                    statusEnvioLookup.Add(New SelectListItem() With {.Text = "Não Enviar", .Value = "N"})
                    statusEnvioLookup.Add(New SelectListItem() With {.Text = "Comércio Eletrônico", .Value = "I"})
                    statusEnvioLookup.Add(New SelectListItem() With {.Text = "Telemarketing", .Value = "T"})
                    statusEnvioLookup.Add(New SelectListItem() With {.Text = "Presencial", .Value = "P"})
                Else
                    statusEnvioLookup.Add(New SelectListItem() With {.Text = "Comércio Eletrônico", .Value = "I"})
                End If

            End Code
            @Html.Label("lblFLGENVPMC", "Status de Envio:")
            @Html.DropDownListFor(Function(Model) Model.FLGENVPMC, New SelectList(statusEnvioLookup, "VALUE", "TEXT", Model.FLGENVPMC), New With {.Class = "form-control", .id = "dropStatus"})
        </div>
        <div class="col-sm-4">
            @code
                Dim tipoEquipeVendas = New List(Of SelectListItem)()
                tipoEquipeVendas.Add(New SelectListItem() With {.Text = "Selecione...", .Value = ""})
                tipoEquipeVendas.Add(New SelectListItem() With {.Text = "Ambos", .Value = "0"})
                tipoEquipeVendas.Add(New SelectListItem() With {.Text = "Martins", .Value = "1"})
                tipoEquipeVendas.Add(New SelectListItem() With {.Text = "Almais", .Value = "2"})
            End Code
            @Html.Label("TIPEQIVND", "Equipe de Vendas (*):")
            @Html.DropDownListFor(Function(Model) Model.TIPEQIVND, New SelectList(tipoEquipeVendas, "VALUE", "TEXT", Model.TIPEQIVND), New With {.Class = "form-control"})
        </div>
        <div class="col-sm-4">
            @Html.Label("TIPNATVND", "Natureza da Venda (*):")
            @Html.DropDownListFor(Function(Model) Model.TIPNATVND, New SelectList(ViewBag.listaNaturezaVenda, "VALUE", "TEXT", Model.TIPNATVND), New With {.Class = "form-control"}.EnableIf(ViewBag.IsCashBack))
        </div>
    </div>

    <br />
    <div class="row">
        @If (Model.TIPPMC <> 1) Then
            @<div Class="col-sm-4">
                <div Class="well well-sm">
                    <div Class="row">
                        <div Class="col-sm-12">
                            <label>
                                @Html.CheckBoxFor(Function(Model) Model.INDCTRADIAVLPMCBool, New With {.Id = "ckbCrterioAdicional", .onClick = "ckbCrterioAdicional_OnClick()"})
                                Critério Adicional
                            </label>
                        </div>
                        <div Class="col-sm-12" id="divVendaAvaliacaoPromocaoCres">
                            @Html.Label("vendaAvaliacaoPmc", "Venda Avaliação Promoção Fator Cresc.:")
                            @Html.TextBoxFor(Function(Model) Model.FTRACRCTRAVLPMC, New With {.class = "input-sm form-control", .alt = "inteiro_positivo", .maxlength = 6})
                        </div>
                        <div Class="col-sm-12" id="divValorLimite">
                            @Html.Label("lblValorLimite", "Valor Limite:")
                            @Html.TextBoxFor(Function(Model) Model.VLRLIMMAXCTRAVLPMC, New With {.class = "input-sm form-control", .alt = "inteiro_positivo", .maxlength = 6})
                        </div>
                        <div Class="col-sm-12" id="divQtdMin">
                            @Html.Label("qtdMin", "Quantidade Mínima:")
                            @Html.TextBoxFor(Function(Model) Model.QDEMNMITEPEDPMC, New With {.class = "input-sm form-control", .alt = "inteiro_positivo", .maxlength = 6})
                        </div>
                    </div>
                </div>
            </div>
        End If
        <div Class="col-sm-4">
            <div Class="well well-sm">
                <div Class="row">
                    <div Class="col-sm-12">
                        <Label>
                            @Html.CheckBoxFor(Function(Model) Model.INDCOBBDEPMCBool, New With {.id = "chbCobrancaBrinde", .onClick = "AlertaCobrancaBrinde()"})
                            Cobrança de Brinde
                        </Label>
                    </div>
                    <div Class="col-sm-12">
                        <label>
                            @Html.CheckBoxFor(Function(Model) Model.INDISRBDEADIBool, New With {.id = "chbBrindeAdicional"})
                            Brinde Adicional
                        </label>
                    </div>
                    <div Class="col-sm-12">
                        @Html.Label("lblQDEMAXBDECLI", "Quantidade Máx. de brinde por cliente:")
                        @Html.TextBoxFor(Function(Model) Model.QDEMAXBDECLI, New With {.class = "input-sm form-control", .alt = "inteiro_positivo", .maxlength = 6})
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-12">
                        @Html.Label("lblINDPMCVNDSMP", "Visualizar Livro Simplificado:")
                        <div class="row">
                            <div class="col-sm-4 col-xs-6">
                                <label>
                                    @Html.RadioButtonFor(Function(Model) Model.INDPMCVNDSMP, Constantes.SIM, New With {.id = "rbtINDPMCVNDSMPSim"})
                                    Sim
                                </label>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                                <label>
                                    @Html.RadioButtonFor(Function(Model) Model.INDPMCVNDSMP, Constantes.NAO, New With {.id = "rbtINDPMCVNDSMPNao"})
                                    Não
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        @Html.Label("lblINDPMCVLDAPLMVLCLI", "Publicar Promoção no Aplicativo:")
                        <div class="row">
                            <div class="col-sm-4 col-xs-6">
                                <label>
                                    @Html.RadioButtonFor(Function(Model) Model.INDPMCVLDAPLMVLCLI, Constantes.SIM, New With {.id = "rbtINDPMCVLDAPLMVLCLISim"})
                                    Sim
                                </label>
                            </div>
                            <div class="col-sm-4  col-xs-6">
                                <label>
                                    @Html.RadioButtonFor(Function(Model) Model.INDPMCVLDAPLMVLCLI, Constantes.NAO, New With {.id = "rbtINDPMCVLDAPLMVLCLINao"})
                                    Não
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @If (Model.INDEVTSMA = 1) Then
        @<div Class="row">
            <div Class="col-sm-12">
                <div Class="well well-sm">
                    <div Class="row">
                        <div Class="col-sm-12">
                            Evento SMART :     
                        </div>
                    </div>
                    <br />
                    <div Class="row">
                        <div Class="col-sm-4">
                            <div Class="col-sm-12">
                                @Html.Label("lblComboFechado", "Combo:")
                                @Code
                                    If (IsNothing(Model.INDCPTFCH)) Then
                                        Model.INDCPTFCH = 0
                                    End If
                                End Code
                                <div Class="row">
                                    <div Class="col-sm-4 col-xs-6">
                                        <Label>
                                            @Html.RadioButtonFor(Function(Model) Model.INDCPTFCH, Constantes.NAO, New With {.id = "rbtINDCPTFCHSim"})
                                            Aberto
                                        </label>
                                    </div>
                                    <div Class="col-sm-4 col-xs-6">
                                        <Label>
                                            @Html.RadioButtonFor(Function(Model) Model.INDCPTFCH, Constantes.SIM, New With {.id = "rbtINDCPTFCHNao"})
                                            Fechado
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div Class="col-sm-4">
                            @Html.Label("dropClassificacaoCombo", "Tipo Classificação Combo:")
                            @Html.DropDownListFor(Function(Model) Model.TIPNIVCLFCPT, New SelectList(ViewBag.listaTipoClassificacaoCombo, "TIPNIVCLFCPT", "DESTIPNIVCLFCPT", Model.TIPNIVCLFCPT), "Selecione...", New With {.Class = "form-control chosen-select"})
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                    End If
</form>


<div class="row">
    <div class="col-sm-12">
        <hr />
        <div class="btn-group" style="float:right;">
            <button class="btn btn-white btn-prev" onclick="VoltarStepGeral();">
                <i class="ace-icon fa fa-arrow-left"></i>
                Voltar
            </button>
            @If (Not (ViewBag.IsCashBack)) Then
                @<Button Class="btn btn-success btn-white btn-next" onclick="AvancarStepGeral();">
                    Avançar
                    <i Class="ace-icon fa fa-arrow-right icon-on-right"></i>
                </Button>
            Else
                @<Button Class="btn btn-success btn-white btn-next" onclick="AvancarStepGeralCashback();">
                    Avançar
                    <i Class="ace-icon fa fa-arrow-right icon-on-right"></i>
                </Button>
            End If
            <a class="btn btn-danger btn-white" href="@Url.Action("", "PromocaoWizard")">
                <i class="ace-icon fa fa-close bigger-120 red"></i>
                Sair
            </a>
        </div>
    </div>
</div>