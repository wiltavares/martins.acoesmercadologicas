﻿@ModelType List(Of PromocaoViewModel)
@Imports Martins.AcoesMercadologicas.Core


<div class="modal fade hideScroll" id="modalResumoCloneProcoesDaAcaoInner">

    <div Class="modal-dialog modal-lg hideScroll" style="z-index: 1051; margin-top: 4px;">
        <div Class="modal-content">
            <div Class="modal-header ">
                <i Class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Clonagem de promoção
            </div>
            <div Class="modal-body" style="overflow-y: scroll !important;">


                <div id="msgGeralResumoCloneProcoesDaAcao"></div>
                @If Not ViewBag.Mensagem Is Nothing Then
                    @<div Class="alert  alert-warning">
                        @Html.Raw(ViewBag.Mensagem)
                    </div>
                End If

                <Table Class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                    <thead>
                        <tr role="row">
                            <th>
                                Filial
                            </th>
                            <th>
                                Promoção
                            </th>
                            <th>
                                Situação
                            </th>
                            <th>
                                Dt. Inicio Promoção
                            </th>
                            <th>
                                Dt. Fim Promoção
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @<tr>
                                <td>
                                    @Html.DisplayFor(Function(x) item.CODFILEMP) -
                                    @Html.DisplayFor(Function(x) item.NOMFILEMP)
                                </td>
                                <td>
                                    @If (item.CODPMC IsNot Nothing) Then
                                        @<a data-rel="tooltip" title="ir para a promoção" target="_blank" href="@Url.Action("Edit", "Promocao", New With {.id = item.CODPMC})">
                                            @Html.DisplayFor(Function(x) item.CODPMC)&nbsp;
                                            @Html.DisplayFor(Function(x) item.NOMPMC)
                                            <i class="ace-icon fa fa-external-link"></i>
                                        </a>
                                    End If
                                </td>
                                <td>
                                    @item.DESSITPMC
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(x) item.DATINIPMC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(x) item.DATFIMPMC)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </Table>
            </div>

            <div Class="modal-footer clearfix">
                <div Class="btn-group pull-right">
                    <Button Class="btn btn-danger btn-white" type="button" onclick="FecharModalResumoCloneProcoesDaAcao()">
                        <i Class="ace-icon fa fa-close bigger-120 red"></i>
                        Fechar
                    </Button>
                </div>
            </div>
        </div>
    </div>

</div>