﻿@ModelType List(Of PromocaoViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="row">
    <div class="col-sm-12">
        <div class="widget-box widget-color-blue2">
            <div class="widget-header widget-header-small widget-light-blue">
                <h4 class="widget-title smaller blue lighter">
                    <i class="ace-icon fa fa-book bigger-110"></i>
                    Resumo
                </h4>
            </div>
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                    <thead>
                        <tr role="row">
                            <th>
                                Filial
                            </th>
                            <th>
                                Promoção
                            </th>
                            <th>
                                Situação
                            </th>
                            <th>
                                Dt. Inicio Promoção
                            </th>
                            <th>
                                Dt. Fim Promoção
                            </th>
                            <th>
                                Ações
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @<tr>
                                <td>
                                    @Html.DisplayFor(Function(x) item.CODFILEMP) -
                                    @Html.DisplayFor(Function(x) item.NOMFILEMP)
                                </td>
                                <td>
                                    @If (item.CODPMC IsNot Nothing) Then
                                        @<a data-rel="tooltip" title="ir para a promoção" target="_blank" href="@Url.Action("Edit", "Promocao", New With {.id = item.CODPMC})">
                                            @Html.DisplayFor(Function(x) item.CODPMC)&nbsp;
                                            @Html.DisplayFor(Function(x) item.NOMPMC)
                                            <i class="ace-icon fa fa-external-link"></i>
                                        </a>
                                    End If
                                </td>
                                <td>
                                    @item.DESSITPMC
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(x) item.DATINIPMC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(x) item.DATFIMPMC)
                                </td>
                                <td>
                                    @Code
                                        If (item.DESSITPMC.Contains("À Liberar")) Then
                                            @<button data-rel="tooltip" title="Liberar" Class="btn btn-xs btn-success" onclick="LiberarPromocao(@item.CODPMC, liberarCallbackWizard)">
                                                <i Class="ace-icon fa fa-paper-plane bigger-119"></i>
                                            </button>
                                        End If
                                    End Code
                                    <button data-rel="tooltip" title="Abastecer" class="btn btn-xs btn-primary" onclick="AbastecerPromocao(@item.CODPMC)">
                                        <i class="ace-icon fa fa-download bigger-119"></i>
                                    </button>
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <hr />
        <div class="btn-group" style="float:right;">
            @If (ViewBag.FiliaisPendentes) Then
                @<Button Class="btn btn-success btn-white btn-next" onclick="IniciarStepFilial();">
                    Avançar
                    <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                </Button>
            Else
                @<a class="btn btn-success btn-white" href="@Url.Action("", "PromocaoWizard")">
                    Lista de Pendências
                    <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                </a>
            End If
        </div>
    </div>
</div>


<div id="divModalAbastecerPromocao"></div>
<div id="divModalAbastecerPromocaoAlert"></div>
<div id="divModalHistoricoAbastecimento"></div>