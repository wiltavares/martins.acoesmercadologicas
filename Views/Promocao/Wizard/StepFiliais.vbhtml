﻿@ModelType List(Of RelacaoAcaoMecanicaFilialViewModel)


<div class="row">
    <div class="col-sm-12">
        <div class="widget-box widget-color-blue2">
            <div class="widget-header widget-header-small widget-light-blue">
                <h4 class="widget-title smaller blue lighter">
                    <i class="ace-icon fa fa-building bigger-110"></i>
                    Filiais
                </h4>
            </div>
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                    <thead>
                        <tr role="row">
                            <th class="center">
                                Aplicar Promoção Para
                            </th>
                            <th>
                                Código
                            </th>
                            <th>
                                Filial
                            </th>
                            <th>
                                Promoção
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @<tr Class="@IIf((item.CODPMC IsNot Nothing), "text-muted", "")">
                                <td Class="center">
                                    <input type="checkbox" @IIf((item.CODPMC IsNot Nothing), "checked", "") @IIf((item.CODPMC IsNot Nothing Or item.DATECSUSR IsNot Nothing), "disabled", "") value="@item.CODFILEMP" />
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(x) item.CODFILEMP)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(x) item.DESABVFILEMP)
                                </td>
                                <td>
                                    @If (item.CODPMC IsNot Nothing) Then
                                        @<a data-rel="tooltip" title="ir para a promoção" target="_blank" href="@Url.Action("Edit", "Promocao", New With {.id = item.CODPMC})">
                                            @Html.DisplayFor(Function(x) item.CODPMC)&nbsp;
                                            <i class="ace-icon fa fa-external-link"></i>
                                        </a>
                                    ElseIf (item.DATECSUSR IsNot Nothing) Then
                                        @<span Class="badge badge-danger">
                                            Excluido @Html.DisplayFor(Function(x) item.DATECSUSR)
                                        </span>
                                    End If
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <hr />
        <div class="btn-group" style="float:right;">

            @*<button class="btn btn-white btn-prev">
                    <i class="ace-icon fa fa-arrow-left"></i>
                    Voltar
                </button>*@
            <button class="btn btn-danger btn-white btn-next" onclick="ExcluirFiliaisStepFilial();" title="Excluir Filiais Selecionadas">
                <i class="ace-icon fa fa-trash-o bigger-120 red"></i>
                Excluir
            </button>

            <button class="btn btn-success btn-white btn-next" onclick="AvancarStepFilial();" title=" Avançar para criar promoção">
                Avançar
                <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
            </button>
            <a class="btn btn-danger btn-white" href="@Url.Action("", "PromocaoWizard")" title="Voltar para a lista de pendencias">
                <i class="ace-icon fa fa-close bigger-120 red"></i>
                sair
            </a>

        </div>
    </div>
</div>
