﻿@ModelType RelacaoAcaoMecanicaViewModel
@Code
    ViewData("Title") = "Promoção - Criar"
End Code

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade hideScroll" id="modalPromocao"></div>

<div class="widget-box widget-color-blue2 ">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-cogs bigger-110"></i>
            Promoção
        </h4>
    </div>
    <div class="widget-body">
        <div class="well well-sm">
            <div class="row">
                <div class="col-sm-8">
                    <label>
                        @If (ViewBag.AcaoComercial.TIPACOMCD = TipoAcao.PreAcordoVigencia) Then
                            @<span style="color:limegreen">Pré Acordo Limitado por Vigência:</span>
                            @<a data-rel="tooltip" title="ir para a Ação Mercadologica" target="_blank" href="@Url.Action("PreAcordoCreateEditVigencia", "AcaoComercial", New With {.id = Model.CODACOCMC})">
                                <b>@Html.DisplayFor(Function(model) model.CODACOCMC) - @Html.DisplayFor(Function(model) model.NOMACOCMC)</b>
                                &nbsp;<i class="ace-icon fa fa-external-link"></i>
                            </a>
                        ElseIf ViewBag.AcaoComercial.TIPACOMCD = TipoAcao.PreAcordoValor Then
                            @<span style="color:red">Pré Acordo Limitado por Valor:</span>
                            @<a data-rel="tooltip" title="ir para a Ação Mercadologica" target="_blank" href="@Url.Action("PreAcordoCreateEditValor", "AcaoComercial", New With {.id = Model.CODACOCMC})">
                                <b>@Html.DisplayFor(Function(model) model.CODACOCMC) - @Html.DisplayFor(Function(model) model.NOMACOCMC)</b>
                                &nbsp;<i class="ace-icon fa fa-external-link"></i>
                            </a>
                        Else
                            @<span>Ação Mercadológica:</span>
                            @<a data-rel="tooltip" title="ir para a Ação Mercadológica" target="_blank" href="@Url.Action("CreateEdit", "AcaoComercial", New With {.id = Model.CODACOCMC})">
                                <b>@Html.DisplayFor(Function(model) model.CODACOCMC) - @Html.DisplayFor(Function(model) model.NOMACOCMC)</b>
                                &nbsp;<i class="ace-icon fa fa-external-link"></i>
                            </a>
                        End If
                    </label>
                </div>
                <div class="col-sm-4">
                    <label>Autor: <b>@Html.DisplayFor(Function(model) model.NOMFNC)</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label>Geração: <b>@Html.DisplayFor(Function(model) model.DATGRCACOCMC)</b></label>
                </div>
                <div class="col-sm-4">
                    <label>Vigência: <b>@Html.DisplayFor(Function(model) model.DATINIACOCMC) até: @Html.DisplayFor(Function(model) model.DATFIMACOCMC)</b></label>
                </div>
                <div class="col-sm-2">
                    <label>Valor Orçado: <b>@Html.DisplayFor(Function(model) model.VLRTOTACOCMC)</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p>Mecanica: <b>@Html.DisplayFor(Function(model) model.DESRGRACOCMC)</b></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    @Select Case Model.TIPPMC
                        Case 0
        @<label>Tipo: <b>Normal</b></label>
                        Case 1
        @<label>Tipo: <b>Combinada</b></label>
                        Case 2
                            @<label>Tipo: <b>Faixas Multiplas</b></label>
                    End Select
                </div>
                <div class="col-sm-4">
                    @Select Case Model.TIPPMC
                        Case 1
                            @<label>Quantidade de Grupo: <b>@Html.DisplayFor(Function(model) model.QDEGRPMIXPMC)</b></label>
                                            Case 2
                                                @<label>Quantidade de Faixa: <b>@Html.DisplayFor(Function(model) model.QDEFXAPMC)</b></label>
                                        End Select
                </div>
                <div class="col-sm-2">
                    @Select Case Model.INDPTCTOTMIXPMC
                        Case 1
        @<label>Participação Mix Total: <b>Sim</b></label>
                        Case 0
                            @<label>Participação Mix Total: <b>Não</b></label>
                    End Select

                </div>
            </div>
        </div>

        <div id="fuelux-wizard-container">
            <div>
                <ul class="steps">
                    <li data-step="1" class="active">
                        <span class="step"></span>
                        <span class="title">Filiais</span>
                    </li>
                    <li data-step="2">
                        <span class="step"></span>
                        <span class="title">Geral</span>
                    </li>
                    <li data-step="3">
                        <span class="step"></span>
                        <span class="title">Brindes</span>
                    </li>
                    <li data-step="4">
                        <span class="step"></span>
                        <span class="title">Comunicações</span>
                    </li>
                    <li data-step="5">
                        <span class="step"></span>
                        <span class="title">Resumo</span>
                    </li>
                </ul>
            </div>

            <hr />

            <div class="step-content pos-rel">

                <div id="divStepPane1" class="step-pane" data-step="1"></div>

                <div id="divStepPane2" class="step-pane" data-step="2"></div>

                <div id="divStepPane3" class="step-pane" data-step="3"></div>

                <div id="divStepPane4" class="step-pane" data-step="4"></div>

                <div id="divStepPane5" class="step-pane" data-step="5">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box widget-color-blue2">
                                <div class="widget-header widget-header-small widget-light-blue">
                                    <h4 class="widget-title smaller blue lighter">
                                        <i class="ace-icon fa fa-book bigger-110"></i>
                                        Resumo
                                    </h4>
                                </div>
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th>
                                                    Filial
                                                </th>
                                                <th>
                                                    Promoção
                                                </th>
                                                <th>
                                                    Dt. Inicio Promoção
                                                </th>
                                                <th>
                                                    Dt. Fim Promoção
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Filial 1
                                                </td>
                                                <td>
                                                    1002
                                                </td>
                                                <td>
                                                    15/12/2018
                                                </td>
                                                <td>
                                                    01/01/2019
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Filial 5
                                                </td>
                                                <td>
                                                    1003
                                                </td>
                                                <td>
                                                    20/12/2018
                                                </td>
                                                <td>
                                                    10/01/2019
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section  scripts
    <script src="~/Scripts/PromocaoCreateWizard.js"></script>
    <script src="~/Scripts/LiberarPromocao.js"></script>
    <script src="~/Scripts/AbastecerPromocao.js"></script>
End Section


