﻿@ModelType PromocaoViewModel
@Imports Martins.AcoesMercadologicas.Core

@Code
    Dim ajaxObject As New AjaxOptions() With {
    .OnBegin = "LoadingOn",
    .HttpMethod = "POST",
    .OnSuccess = "editPromocao_Success",
    .OnFailure = "LoadingOff_UpdateMessage"
        }
End Code

<div id="divEditPromocao" class="widget-box widget-color-wi">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller blue lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Promoção - @Model.DESSITPMC
        </h4>
        @Code
            Dim classEdit = ""

            If (Model.TIPSITPMC = "E") Then
                classEdit = "hideEdit"
            End If

        End Code

        <input type="hidden" id="classEdit" value="@classEdit">

        <div class="widget-toolbar">
            <div class="btn-group">
                <a href="@Url.Action("index", "Promocao")" class="btn btn-md btn-white btn-danger" title="Sair da promoção">
                    <i class="ace-icon fa fa-times bigger-125"></i>
                    Sair
                </a>
                <a id="editCancelBlockButton" href="#" onclick="editCancelBlock('Promocao', this)" class="btn btn-md btn-white btn-info @classEdit" title="Editar Promoção">
                    <i class="ace-icon fa fa-edit bigger-125"></i>
                    Editar
                </a>
                @code
                    If (Model.DESSITPMC.Contains("À Liberar")) Then
                        @<a href="#" onclick="LiberarPromocao(@Model.CODPMC)" class="btn btn-md btn-white btn-info" title="Liberar Promoção">
                            <i id="btnLiberarPromocao" class="ace-icon fa fa-paper-plane bigger-125"></i>
                            Liberar
                        </a>
                    End If
                End Code
                @code
                    If Model.TIPSITPMC.Trim = String.Empty Then
                        @<a href="#" onclick="AbrirPromocaoConfirm(@Model.CODPMC)" class="btn btn-md btn-white btn-warning" title="Reabrir Promoção">
                            <i id="btnAbrirPromocao" class="ace-icon fa fa-folder-open bigger-125"></i>
                            Reabrir
                        </a>
                    End If
                End Code
                @code
                    Dim authCookie As HttpCookie = Context.Request.Cookies(FormsAuthentication.FormsCookieName)
                    If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                        Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)

                        Dim roles As String() = authTicket.UserData.Split("|")(1).Split(";"c)
                        If (Not roles.Where(Function(f) f = "46").FirstOrDefault() Is Nothing) Then
                            @<a href="#" onclick="ClonarPromocao(@Model.CODPMC);" class="btn btn-md btn-white btn-warning" title="Clonar Promoção">
                                <i class="ace-icon fa fa-clone bigger-125"></i>
                                Clonar
                            </a>
                        End If
                    End If
                End Code
            </div>
            <a href="#" data-action="collapse">
                <i class="ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div class="widget-main" id="widgetPromocao">
            @Using (Ajax.BeginForm("Alterar", "Promocao", ajaxObject, New With {.id = "formEditarPromocao"}))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">

                    <div class="widget-body">
                        <div class="widget-main">
                            <div>
                                <div class="well well-sm">

                                    @*HiddenFor para todos os campos da promoção*@
                                    @*@Html.HiddenFor(Function(Model) Model.CODPMC, New With {.Class = "input-sm date-interval form-control"})*@
                                    @Html.HiddenFor(Function(Model) Model.TIPEDEPUBALVPMC)
                                    @Html.HiddenFor(Function(Model) Model.DESPMC)
                                    @Html.HiddenFor(Function(Model) Model.DATHRAGRCPMC)
                                    @Html.HiddenFor(Function(Model) Model.CODFNCGRCPMC)
                                    @Html.HiddenFor(Function(Model) Model.FLGSLCLSTPMC)
                                    @Html.HiddenFor(Function(Model) Model.QDEPTOCOPVND)
                                    @Html.HiddenFor(Function(Model) Model.TIPSITPMC, New With {.id = "hdfSituacaoPromocao"})
                                    @Html.HiddenFor(Function(Model) Model.CODFNCALTSITPMC)
                                    @Html.HiddenFor(Function(Model) Model.DATHRAALTSITPMC)
                                    @Html.HiddenFor(Function(Model) Model.DATPCSPMC)
                                    @Html.HiddenFor(Function(Model) Model.TIPAPUPMC)
                                    @Html.HiddenFor(Function(Model) Model.DATHRAALTPMC)
                                    @Html.HiddenFor(Function(Model) Model.CODEMP)
                                    @Html.HiddenFor(Function(Model) Model.CODPMCPCP)
                                    @Html.HiddenFor(Function(Model) Model.QDEBNFPMCCAA)
                                    @Html.HiddenFor(Function(Model) Model.QDEVNDPMCCAA)
                                    @Html.HiddenFor(Function(Model) Model.DATINIPMCCAA)
                                    @Html.HiddenFor(Function(Model) Model.DATFIMPMCCAA)
                                    @Html.HiddenFor(Function(Model) Model.FLGCONPMCCAA)
                                    @Html.HiddenFor(Function(Model) Model.NOMUSRSIS)
                                    @Html.HiddenFor(Function(Model) Model.INDPTCPMCCMCETN)
                                    @Html.HiddenFor(Function(Model) Model.INDPTCPMCTMK)
                                    @Html.HiddenFor(Function(Model) Model.INDAPUICTQDEBDE)
                                    @Html.HiddenFor(Function(Model) Model.INDECSITEPMCPEDVND)
                                    @Html.HiddenFor(Function(Model) Model.INDPRMUNCVGRPMC)
                                    @Html.HiddenFor(Function(Model) Model.VLRBSECALGRPMIXPMC)
                                    @Html.HiddenFor(Function(Model) Model.PERVARGRPMIXPMC)
                                    @Html.HiddenFor(Function(Model) Model.INDBDEFRC)
                                    @Html.HiddenFor(Function(Model) Model.INDSTAPMCREL)
                                    @Html.HiddenFor(Function(Model) Model.VLRVBAPMC)
                                    @Html.HiddenFor(Function(Model) Model.CODFNCALTPMC)
                                    @Html.HiddenFor(Function(Model) Model.QDEGRPMIXPMC, New With {.id = "IdQDEGRPMIXPMC"})
                                    @Html.HiddenFor(Function(Model) Model.INDPMCSISNVO)
                                    @Html.HiddenFor(Function(Model) Model.INDISRBDEADI)
                                    @Html.HiddenFor(Function(Model) Model.CODFILEMP, New With {.id = "idFilialPromocao"})
                                    @Html.HiddenFor(Function(Model) Model.TIPPMC, New With {.id = "idTipoPromocao"})
                                    @Html.HiddenFor(Function(Model) Model.TIPFRNACOCMC, New With {.id = "idTipoFornecedor"})

                                    <div class="row">
                                        <div Class="col-lg-4 col-sm-4">
                                            @Html.Label("filial", "Filial (*):")
                                            <br />
                                            @Html.TextBoxFor(Function(Model) Model.CodNomFil, New With {.Class = "input-sm date-interval form-control", .id = "IDfilial", .readonly = "readonly", .autocomplete = "off"})
                                        </div>
                                        <div Class="col-lg-2 col-sm-2">
                                            @Html.Label("lbCodPmc", "Código:")
                                            <br />
                                            @Html.TextBoxFor(Function(Model) Model.CODPMC, New With {.Class = "input-sm date-interval form-control", .id = "IDCodPmc", .readonly = "readonly", .autocomplete = "off"})
                                        </div>
                                        <div class="col-lg-3 col-sm-3">
                                            @If (ViewBag.AcaoComercial.TIPACOMCD = TipoAcao.PreAcordoVigencia) Then
                                                @Html.Label("lblCODACOCMC", "Pré Acordo Limitado por Vigência:", New With {.Style = "color:limegreen"})
                                            ElseIf ViewBag.AcaoComercial.TIPACOMCD = TipoAcao.PreAcordoValor Then
                                                @Html.Label("lblCODACOCMC", "Pré Acordo Limitado por Valor:", New With {.Style = "color:red"})
                                            Else
                                                @Html.Label("lblCODACOCMC", "Ação Mercadológica:", New With {.Style = "color:red"})
                                            End If
                                            <br />
                                            @Html.TextBoxFor(Function(Model) Model.CODACOCMC, New With {.Class = "input-sm form-control", .id = "idCodAcaoPromocao", .readonly = "readonly"})
                                        </div>

                                        <div class="col-lg-3 col-sm-3">
                                            @Html.Label("lblCODACOCMC", "Tipo Fornecedor:")
                                            <br />
                                            @Html.TextBoxFor(Function(Model) Model.NOMTIPFRNACOCMC, New With {.Class = "input-sm form-control", .id = "tipFornecedor", .readonly = "readonly"})
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            @Html.Label("lbTIPPMC", "Tipo:")
                                            <br />
                                            @Html.TextBoxFor(Function(Model) Model.DesTIPPMC, New With {.Class = "input-sm form-control", .id = "IdTipo", .readonly = "readonly"})
                                        </div>
                                        <div Class="col-sm-2">

                                            @If Model.TIPPMC = 1 Then
                                                @Html.Label("lblQDEGRPMIXPMC", "Quantidade de Grupo:")
                                                @<br />
                                                @Html.TextBoxFor(Function(Model) Model.QDEGRPMIXPMC, New With {.class = "input-sm form-control", .id = "IdQtdGrupo", .readonly = "readonly"})
                                            Else
                                                @Html.HiddenFor(Function(Model) Model.QDEGRPMIXPMC)
                                            End If

                                            @If Model.TIPPMC = 2 Then
                                                @Html.Label("lblQDEFXAPMC", "Quantidade de Faixa:")
                                                @<br />
                                                @Html.TextBoxFor(Function(Model) Model.QDEFXAPMC, New With {.class = "input-sm form-control", .id = "IdQtdFaixa", .readonly = "readonly"})
                                            Else
                                                @Html.HiddenFor(Function(Model) Model.QDEFXAPMC)
                                            End If
                                        </div>

                                        <div class="col-sm-3">
                                            @Html.Label("lblINDPTCTOTMIXPMC", "Participação Mix Total:")
                                            <br />
                                            @*@Html.TextBoxFor(Function(Model) Model.INDPTCTOTMIXPMC, New With {.Class = "input-sm date-interval form-control", .readonly = "readonly"})*@
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    @Html.RadioButtonFor(Function(Model) Model.INDPTCTOTMIXPMC, Constantes.SIM, New With {.id = "radioParticipacaoMixSim", .readonly = "readonly"})
                                                    @Html.Label("sim", "Sim")
                                                </div>
                                                <div class="col-sm-3">
                                                    @Html.RadioButtonFor(Function(Model) Model.INDPTCTOTMIXPMC, Constantes.NAO, New With {.id = "radioParticipacaoMixSim", .readonly = "readonly"})
                                                    @Html.Label("nao", "Não")
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            @Html.Label("lblEventoPromocional", "Evento Promocional:")
                                            <br />
                                            @Html.TextBoxFor(Function(Model) Model.NOMEVTBTB, New With {.Class = "input-sm form-control", .id = "txtEventoPromocional", .readonly = "readonly"})
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            @Html.Label("lblDESCMCPMC:", "Mecanica:")
                                            <br />
                                            @Html.TextAreaFor(Function(Model) Model.DESCMCPMC, New With {.Class = "input-sm form-control", .TextMode = "MultiLine", .Rows = "3", .id = "mecanica"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                            @Html.ValidationMessageFor(Function(m) m.DESCMCPMC, "", New With {.class = "text-danger-message-login"})
                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-4">
                                    @Html.Label("lblCODUNDESRNGC", "BU (*):")
                                    @Html.DropDownListFor(Function(Model) Model.CODUNDESRNGC, New SelectList(ViewBag.listaBU, "CODUNDESRNGC", "DESUNDESRNGC", Model.CODFILEMPORIVBA), "Selecione...", New With {.Class = "form-control chosen-select"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                    @Html.ValidationMessageFor(Function(m) m.CODUNDESRNGC, "", New With {.class = "text-danger-message-login"})
                                    @*@Html.HiddenFor(Function(Model) Model.CODUNDESRNGC)*@
                                </div>
                                <div Class="col-sm-8">
                                    @Html.Label("lblNOMPMC", "Nome Promoção (*):")

                                    @Html.TextBoxFor(Function(Model) Model.NOMPMC, New With {.class = "input-md form-control", .id = "NonPromocao", .maxlength = "55"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                    @Html.ValidationMessageFor(Function(m) m.NOMPMC, "", New With {.class = "text-danger-message-login"})

                                </div>
                            </div>
                            <br />
                            <div Class="row">
                                @If (Model.FLGENVPMC = "I") Then
                                    @<div Class="col-sm-4">
                                        <div Class="form-check" style="transform: translateY(105%);">
                                            @Html.CheckBoxFor(Function(Model) Model.INDPMCIMDBool, New With {.Id = "ckbPromocaoRelampago", .class = "form-check-input", .onClick = "ckbPromocaoRelampago_OnClick(this.checked)"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                            <Label Class="form-check-label" for="INDPMCIMDBool">
                                                Promoção Relâmpago
                                            </Label>
                                        </div>
                                    </div>
                                End If

                                <div Class="col-sm-4" id="divDataInicio">
                                    @Html.Label("lblDATINIPMC", "Dt. de Início(*):")
                                    <div Class="input-daterange input-group">
                                        @Html.TextBoxFor(Function(Model) Model.DATINIPMC, New With {.class = "input-sm date-interval form-control date-picker", .alt = "datepicker", .autocomplete = "off"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                        @Html.ValidationMessageFor(Function(m) m.DATINIPMC, "", New With {.class = "text-danger-message-login"})
                                    </div>
                                </div>

                                <div Class="col-sm-1 horas" style="display:none">
                                    @Html.Label("lblDATINIPMC", "Hr. Inicio(*):")
                                    @Html.TextBoxFor(Function(Model) Model.HRAINIPMC, "{0:HH:mm}", New With {.Class = "input-md form-control", .id = "horaIni", .alt = "time_hh_mm", .autocomplete = "off", .maxlength = "10"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                    @Html.ValidationMessageFor(Function(m) m.HRAINIPMC, "", New With {.class = "text-danger-message-login"})
                                </div>

                                <div Class="col-sm-4" id="divDataFim">
                                    @Html.Label("lblDATFIMPMC", "Dt. de Fim (*):")
                                    <div class="input-daterange input-group">
                                        @Html.TextBoxFor(Function(Model) Model.DATFIMPMC, New With {.class = "input-sm date-interval form-control date-picker", .alt = "datepicker", .autocomplete = "off"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                        @Html.ValidationMessageFor(Function(m) m.DATFIMPMC, "", New With {.class = "text-danger-message-login"})
                                    </div>
                                </div>

                                <div Class="col-sm-1 horas" style="display:none">
                                    @Html.Label("lblDATINIPMC", "Hr. Fim(*):")
                                    @Html.TextBoxFor(Function(Model) Model.HRAFIMPMC, "{0:HH:mm}", New With {.Class = "input-md form-control", .id = "horaFim", .alt = "time_hh_mm", .autocomplete = "off", .maxlength = "10"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                    @Html.ValidationMessageFor(Function(m) m.HRAFIMPMC, "", New With {.class = "text-danger-message-login"})
                                </div>
                            </div>
                            
                            <br />
                            <div Class="row">
                                <div class="col-sm-4">
                                    @code
                                        Dim statusEnvioLookup = New List(Of SelectListItem)()
                                        If (Not (ViewBag.IsCashBack)) Then
                                            statusEnvioLookup.Add(New SelectListItem() With {.Text = "Selecione...", .Value = ""})
                                            statusEnvioLookup.Add(New SelectListItem() With {.Text = "Enviado", .Value = " "})
                                            statusEnvioLookup.Add(New SelectListItem() With {.Text = "A Enviar", .Value = "S"})
                                            statusEnvioLookup.Add(New SelectListItem() With {.Text = "Não Enviar", .Value = "N"})
                                            statusEnvioLookup.Add(New SelectListItem() With {.Text = "Comércio Eletrônico", .Value = "I"})
                                            statusEnvioLookup.Add(New SelectListItem() With {.Text = "Telemarketing", .Value = "T"})
                                            statusEnvioLookup.Add(New SelectListItem() With {.Text = "Presencial", .Value = "P"})
                                        Else
                                            statusEnvioLookup.Add(New SelectListItem() With {.Text = "Comércio Eletrônico", .Value = "I"})
                                        End If

                                    End Code
                                    @Html.Label("lblFLGENVPMC", "Status de Envio:")
                                    @Html.DropDownListFor(Function(Model) Model.FLGENVPMC, New SelectList(statusEnvioLookup, "VALUE", "TEXT", Model.FLGENVPMC), New With {.Class = "form-control", .id = "dropStatus"})
                                </div>
                                <div class="col-sm-4">
                                    @code
                                        Dim tipoEquipeVendas = New List(Of SelectListItem)()
                                        tipoEquipeVendas.Add(New SelectListItem() With {.Text = "Ambos", .Value = "0"})
                                        tipoEquipeVendas.Add(New SelectListItem() With {.Text = "Martins", .Value = "1"})
                                        tipoEquipeVendas.Add(New SelectListItem() With {.Text = "Almais", .Value = "2"})
                                    End Code
                                    @Html.Label("TIPEQIVND", "Equipe de Vendas (*):")
                                    @Html.DropDownListFor(Function(Model) Model.TIPEQIVND, New SelectList(tipoEquipeVendas, "VALUE", "TEXT", Model.TIPEQIVND), New With {.Class = "form-control"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                </div>
                                <div Class="col-sm-4">
                                    @Html.Label("TIPNATVND", "Natureza da Venda (*):")
                                    @Html.DropDownListFor(Function(Model) Model.TIPNATVND, New SelectList(ViewBag.listaNaturezaVenda, "VALUE", "TEXT", Model.TIPNATVND), New With {.Class = "form-control"}.ReadonlyIf(Model.TIPSITPMCBoll AndAlso Model.INDTIPPTOPMC))
                                </div>
                            </div>
                            
                            <br />
                            <div class="row">
                                @If (Model.TIPPMC <> 1) Then
                                    @<div Class="col-sm-4">
                                        <div Class="well well-sm">
                                            <div Class="row">
                                                <div Class="col-sm-12">
                                                    <label>
                                                        @Html.CheckBoxFor(Function(Model) Model.INDCTRADIAVLPMCBool, New With {.Id = "ckbCrterioAdicional", .onClick = "ckbCrterioAdicional_OnClick()"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                                        Critério Adicional
                                                    </label>
                                                </div>
                                                <div Class="col-sm-12" id="divVendaAvaliacaoPromocaoCres">
                                                    @Html.Label("vendaAvaliacaoPmc", "Venda Avaliação Promoção Fator Cresc.:")
                                                    @Html.TextBoxFor(Function(Model) Model.FTRACRCTRAVLPMC, New With {.class = "input-sm form-control", .id = "qtdMaxBrindPromocao", .alt = "inteiro_positivo", .maxlength = 6}.ReadonlyIf(Model.TIPSITPMCBoll))
                                                </div>
                                                <div Class="col-sm-12" id="divValorLimite">
                                                    @Html.Label("lblValorLimite", "Valor Limite:")
                                                    @Html.TextBoxFor(Function(Model) Model.VLRLIMMAXCTRAVLPMC, New With {.class = "input-sm form-control", .id = "qtdMaxBrindPromocao", .alt = "inteiro_positivo", .maxlength = 6}.ReadonlyIf(Model.TIPSITPMCBoll))
                                                </div>
                                                <div Class="col-sm-12" id="divQtdMin">
                                                    @Html.Label("qtdMin", "Quantidade Mínima:")
                                                    @Html.TextBoxFor(Function(Model) Model.QDEMNMITEPEDPMC, New With {.class = "input-sm form-control", .id = "qqtdMinPromocao", .alt = "inteiro_positivo", .maxlength = 6}.ReadonlyIf(Model.TIPSITPMCBoll))
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                End If
                                <div Class="col-sm-4">
                                    <div Class="well well-sm">
                                        <div Class="row">
                                            <div Class="col-sm-12">
                                                <Label>
                                                    @Html.CheckBoxFor(Function(Model) Model.INDCOBBDEPMCBool, New With {.id = "chbCobrancaBrinde", .onClick = "AlertaCobrancaBrinde()"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                                    Cobrança de Brinde
                                                </Label>
                                            </div>
                                            <div Class="col-sm-12">
                                                <label>
                                                    @Html.CheckBoxFor(Function(Model) Model.INDISRBDEADIBool, New With {.id = "chbBrindeAdicional", .onClick = "chbBrindeAdicional_OnClick()"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                                    Brinde Adicional
                                                </label>
                                            </div>
                                            <div Class="col-sm-12">
                                                @Html.Label("lblQDEMAXBDECLI", "Quantidade Máx. de brinde por cliente:")
                                                @Html.TextBoxFor(Function(Model) Model.QDEMAXBDECLI, New With {.class = "input-sm form-control", .id = "qtdMaxBrindPromocao", .alt = "inteiro_positivo", .maxlength = 6}.ReadonlyIf(Model.TIPSITPMCBoll))
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div Class="col-sm-4">
                                    <div Class="well well-sm">
                                        <div Class="row">
                                            <div Class="col-sm-12">
                                                @Html.Label("lblINDPMCVNDSMP", "Visualizar Livro Simplificado:")
                                                <div Class="row">
                                                    <div class="col-sm-4 col-xs-6">
                                                        <label>
                                                            @Html.RadioButtonFor(Function(Model) Model.INDPMCVNDSMP, Constantes.SIM, New With {.id = "radioComplementarSim"})
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-4 col-xs-6">
                                                        <label>
                                                            @Html.RadioButtonFor(Function(Model) Model.INDPMCVNDSMP, Constantes.NAO, New With {.id = "radioComplementarSim"})
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div Class="col-sm-12">
                                                @Html.Label("lblINDPMCVLDAPLMVLCLI", "Publicar Promoção Aplicativo:")
                                                <div class="row">
                                                    <div class="col-sm-4 col-xs-6">
                                                        <label>
                                                            @Html.RadioButtonFor(Function(Model) Model.INDPMCVLDAPLMVLCLI, Constantes.SIM, New With {.id = "radioComplementarSim"})
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-4  col-xs-6">
                                                        <label>
                                                            @Html.RadioButtonFor(Function(Model) Model.INDPMCVLDAPLMVLCLI, Constantes.NAO, New With {.id = "radioComplementarSim"})
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @If (Model.INDEVTSMA = 1) Then
                                @<div Class="row">
                                    <div Class="col-sm-12">
                                        <div Class="well well-sm">
                                            <div Class="row">
                                                <div Class="col-sm-12">
                                                    Evento SMART :
                                                </div>
                                            </div>
                                            <br />
                                            <div Class="row">
                                                <div Class="col-sm-4">
                                                    <div Class="col-sm-12">
                                                        @Html.Label("lblComboFechado", "Combo:")
                                                        @Code
                                                            If (IsNothing(Model.INDCPTFCH)) Then
                                                                Model.INDCPTFCH = 0
                                                            End If
                                                        End Code
                                                        <div Class="row">
                                                            <div Class="col-sm-4 col-xs-6">
                                                                <Label>
                                                                    @Html.RadioButtonFor(Function(Model) Model.INDCPTFCH, Constantes.NAO, New With {.id = "rbtINDCPTFCHSim"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                                                    Aberto
                                                                </Label>
                                                            </div>
                                                            <div Class="col-sm-4 col-xs-6">
                                                                <Label>
                                                                    @Html.RadioButtonFor(Function(Model) Model.INDCPTFCH, Constantes.SIM, New With {.id = "rbtINDCPTFCHNao"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                                                    Fechado
                                                                </Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div Class="col-sm-4">
                                                    @Html.Label("dropClassificacaoCombo", "Tipo Classificação Combo:")
                                                    @Html.DropDownListFor(Function(Model) Model.TIPNIVCLFCPT, New SelectList(ViewBag.listaTipoClassificacaoCombo, "TIPNIVCLFCPT", "DESTIPNIVCLFCPT", Model.TIPNIVCLFCPT), "Selecione...", New With {.Class = "form-control chosen-select"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                                            End If

                            @*<div Class="row">
            @If (Model.TIPPMC = 2) Then
                @<div Class="col-sm-4">
                    @Html.Label("lblQDEGRPMIXPMC", "Quantidade Faixa:")
                    @Html.TextBoxFor(Function(Model) Model.QDEFXAPMC, New With {.class = "input-sm date-interval form-control", .id = "qtdGrupoMixpromocao"}.ReadonlyIf(Model.TIPSITPMCBoll))
                </div>

            End If
            @If (Model.TIPPMC = 1) Then
                @<div Class="col-sm-4">
                    @Html.Label("lblQDEFXAPMC", "Quantidade Grupo:")
                    @Html.TextBoxFor(Function(Model) Model.QDEGRPMIXPMC, New With {.class = "input-sm form-control", .id = "IdQtdGrupoMiPromocao"}.ReadonlyIf(Model.TIPSITPMCBoll))
                </div>
            End If
        </div>*@



                            @Html.ValidationSummary(False, "", New With {.class = "text-danger"})

                            <div Class="row">
                                <div Class="col-sm-3">
                                    <br />
                                    <div Class="btn-group pull-left">
                                        <Button type="submit" Class="btn btn-white btn-success ">
                                            <i Class="ace-icon fa fa-save bigger-120 green"></i>
                                            Salvar
                                        </Button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            End Using
        </div>
    </div>
</div>

@section Scripts
    <script type="text/javascript" src="~/Scripts/Promocao.js"></script>
End Section

