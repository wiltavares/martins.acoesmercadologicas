﻿@ModelType GridSettings(Of PromocaoViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Promoção"
End Code

<div id="divFiltroPromocao">
    <div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-line-chart"></i>
                Promoção
            </li>
            <li class="active">
                Consulta
            </li>
        </ul>
    </div>

    <div id="msgGeral">
        @Html.Partial(Constantes.VIEW_ALERTAS)
    </div>

    <div class="modal fade hideScroll" id="modalPromocao"></div>

    @Using (Ajax.BeginForm("Pesquisar", "Promocao", Constantes.AJAX_OBJECT_POST_DEFAULT))
        @Html.AntiForgeryToken()
        @<div class="form-horizontal">

            <div Class="widget-box widget-color-blue2 ">
                <div Class="widget-header widget-header-small">
                    <h4 Class="widget-title smaller white lighter">
                        <i Class="ace-icon fa fa-sliders bigger-110"></i>
                        Filtros de Pesquisa
                    </h4>
                    <div Class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i Class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                        </a>
                    </div>
                </div>

                <div Class="widget-body">
                    <div Class="widget-main">
                        <form action="/martins.acoesmercadologicas/AcaoComercial/Pesquisar" data-ajax="true" data-ajax-begin="LoadingOn" data-ajax-complete="LoadingOff_UpdateMessage" data-ajax-method="POST" data-ajax-mode="replace" data-ajax-update="#resultadosView" id="form0" method="post">
                            <input name="__RequestVerificationToken" type="hidden" value="sJbZ15k2-W44HUMX5b5B7CXDzdwnVTvZBC08J_1xUHR3ZQutnGpIGN4wvyKS0puIBEC4N9Gnqa8JfM84cqY17ay3hvoFM-Gl4CVg0EcH9tU1">                <div Class="form-horizontal">
                                <div Class="row">
                                    <div Class="col-lg-2 col-sm-2">
                                        @Html.Label("lblCodPromocao", "Cod. da Promoção:")
                                        @Html.TextBoxFor(Function(Model) Model.Filter.CODPMC, New With {.Class = "form-control"})
                                    </div>

                                    <div Class="col-lg-4 col-sm-4">
                                        @Html.Label("lblNomPromocao", "Nome da Promoção:")
                                        @Html.TextBoxFor(Function(Model) Model.Filter.NOMPMC, New With {.Class = "form-control"})
                                    </div>

                                    <div Class="col-lg-3 col-sm-3">
                                        @Html.Label("filial", "Filial:")
                                        @Html.DropDownListFor(Function(Model) Model.Filter.CODFILEMP, New SelectList(ViewBag.listaFilial, "CODFILEMP", "CODFILIAL_NOMFILIAL", Model.Filter.CODFILEMP), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropFilial"})
                                    </div>
                                    <div class="col-sm-3">
                                        @Html.Label("autor", "Autor:")
                                        @Html.DropDownListFor(Function(m) Model.Filter.CODFNCGRCPMC, New SelectList(ViewBag.listaFuncionarios, "CODFNC", "CODIGO_NOME", Model.Filter.CODFNCGRCPMC), "Selecione...", New With {.Class = "chosen-select form-control"})
                                    </div>
                                </div>
                                <div Class="row">
                                    <div class="col-lg-4 col-sm-4">
                                        @Html.Label("data", "Intervalo de vigência:")
                                        @Html.HiddenFor(Function(Model) Model.Filter.DATINIPMC, New With {.id = "dataIniAux"})
                                        @Html.HiddenFor(Function(Model) Model.Filter.DATFIMPMC, New With {.id = "dataFimAux"})
                                        <div class="input-daterange input-group">
                                            @Html.TextBoxFor(Function(Model) Model.Filter.DATINIPMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataIni()", .id = "dataIni", .autocomplete = "off"})
                                            <span class="input-group-addon">
                                                à
                                            </span>
                                            @Html.TextBoxFor(Function(Model) Model.Filter.DATFIMPMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataFim()", .id = "dataFim", .autocomplete = "off"})
                                        </div>
                                    </div>
                                    <div Class="col-sm-2">
                                        @Html.Label("CODACOCMC", "Ação Comercial:")
                                        @Html.TextBoxFor(Function(Model) Model.Filter.CODACOCMC, New With {.Class = "form-control"})
                                    </div>
                                    <div Class="col-sm-3">
                                        @Html.Label("situação", "Situação:")
                                        @Html.DropDownListFor(Function(Model) Model.Filter.TIPSITPMC, New SelectList(ViewBag.listaSituacao, "VALUE", "TEXT", Model.Filter.TIPSITPMC), "Selecione...", New With {.Class = "form-control", .id = "dropSituacao"})
                                    </div>
                                    <div Class="col-sm-3">
                                        @Html.Label("BU", "BU:")
                                        @Html.DropDownListFor(Function(Model) Model.Filter.CODUNDESRNGC, New SelectList(ViewBag.listaBU, "CODUNDESRNGC", "DESUNDESRNGC", Model.Filter.CODFILEMPORIVBA), "Selecione...", New With {.Class = "chosen-select form-control", .id = "dropBU"})
                                    </div>

                                </div>
                                <br>
                                <div Class="row">
                                    <div Class="col-sm-12">
                                        <div Class="btn-group pull-left">
                                            <Button type="submit" Class="btn btn-info btn-white ">
                                                <i Class="ace-icon fa fa-search bigger"></i>
                                                Pesquisar
                                            </Button>
                                            <a href="@Url.Action("", "Promocao")" class="btn btn-grey btn-white">
                                                <i class="ace-icon fa fa-eraser bigger-120"></i>
                                                Limpar
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    End Using
    <br class="clearfix" />
    <div id="resultadosView">
        @Code
            Html.RenderPartial("~/Views/Promocao/_ResultadosView.vbhtml", Model)
        End Code
    </div>
</div>
<div class="modal fade hideScroll" id="modalIndexPromocao"></div>


@section Scripts
    <script type="text/javascript" src="~/Scripts/PromocaoPesquisa.js"></script>
    <script type="text/javascript" src="~/Scripts/LiberarPromocao.js"></script>
End Section