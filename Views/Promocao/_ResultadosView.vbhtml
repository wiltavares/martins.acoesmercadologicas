﻿@ModelType GridSettings(Of PromocaoViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
        <Button Class="btn btn-xs btn-white btn-info" style="float: right; color: white; margin-top: 2.5px; margin-right: 3px;" type="button" onclick="alterarDataFimPromocao()">
            <i Class="ace-icon fa fa-pencil-square-o bigger-119"></i>
            Alterar Data Fim
        </Button>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" id="tblResultadosPesquisaPromocao" role="grid" style="min-width: 1500px;">
                    <thead>
                        <tr role="row">
                            <th class="center">
                                @Html.CheckBox("chkTodos", New With {.onclick = "marcarOuDesmarcarTodasPromocao()", .Id = "chkTodasPromocao"})
                            </th>
                            <th class="center"></th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODPMC", Model.Page.OrderBy)">
                                Código
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMPMC", Model.Page.OrderBy)">
                                Descrição da Promoção
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESSITPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESSITPMC", Model.Page.OrderBy)">
                                Situação
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DATINIPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATINIPMC", Model.Page.OrderBy)">
                                Dt. Inicio Promoção
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DATFIMPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATFIMPMC", Model.Page.OrderBy)">
                                Dt. Fim Promoção
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMFILIAL", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFILIAL", Model.Page.OrderBy)">
                                Filial
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESUNDESRNGC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESUNDESRNGC", Model.Page.OrderBy)">
                                BU
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td Class="center">
                                    @Html.CheckBox("SelecionePromocao", New With {.class = "selecionePromocao"})
                                </td>
                                <td Class="center">
                                    <div class="btn-group left">
                                        <a data-rel="tooltip" title="Editar" class="btn btn-xs btn-info tooltip-info" href="@Url.Action("Edit", New With {.id = item.CODPMC})">
                                            <i class="ace-icon fa fa-pencil bigger-110"></i>
                                        </a>

                                        @Code
                                            If (item.DESSITPMC.Contains("À Liberar")) Then
                                                @<a data-rel="tooltip" title="Liberar" Class="btn btn-xs btn-success tooltip-info" onclick="LiberarPromocao('@item.CODPMC', liberarCallbackList)">
                                                    <i Class="ace-icon fa fa-paper-plane bigger-110"></i>
                                                </a>
                                            End If

                                            If item.TIPSITPMC.Trim = String.Empty Then
                                                @<a data-rel="tooltip" title="Reabrir" Class="btn btn-xs btn-warning tooltip-info" onclick="AbrirPromocaoConfirm('@item.CODPMC')">
                                                    <i Class="ace-icon fa fa-folder-open bigger-110"></i>
                                                </a>
                                            End If

                                            If item.INDTIPPTOPMC AndAlso item.DATINIPMC.Value.Date <= DateTime.Now.Date AndAlso item.DATFIMPMC.Value.Date >= DateTime.Now.Date Then
                                                If item.TIPSITPMC.Trim = String.Empty Then
                                                    @<a data-rel="tooltip" title="Suspender" Class="btn btn-xs btn-danger tooltip-info" onclick="SuspenderPromocaoCashbackConfirm('@item.CODPMC')">
                                                        <i Class="ace-icon fa fa-ban bigger-110"></i>
                                                    </a>
                                                End If

                                                If item.TIPSITPMC = "S" Then
                                                    @<a data-rel="tooltip" title="Retirar suspensão" Class="btn btn-xs btn-primary tooltip-info" onclick="RetirarSuspensaoConfirm('@item.CODPMC')">
                                                        <i Class="ace-icon fa fa-recycle bigger-110"></i>
                                                    </a>
                                                End If

                                            End If

                                            If (item.INDHSTSITPMC = 1) Then
                                                @<a data-rel="tooltip" title="Histórico de Suspensão/Liberação" Class="btn btn-xs btn-default tooltip-info" onclick="ExibirHistorico('@item.CODPMC')">
                                                    <i Class="ace-icon fa fa-book bigger-120"></i>
                                                </a>
                                            End If
                                        End Code
                                    </div>
                                </td>
                                <td class="CodigoPromocao">
                                    @item.CODPMC
                                </td>
                                <td>
                                    @item.NOMPMC
                                </td>
                                <td>
                                    @item.DESSITPMC
                                </td>
                                <td class="DataInicioPromocao">
                                    @IIf(Not IsNothing(item.DATINIPMC), item.DATINIPMC.Value.ToShortDateString, "")
                                </td>
                                <td class="DataFimPromocao">
                                    @IIf(Not IsNothing(item.DATFIMPMC), item.DATFIMPMC.Value.ToShortDateString, "")
                                </td>
                                <td>
                                    @item.CODFILEMP -
                                    @item.NOMFILEMP
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.DESUNDESRNGC)
                                </td>
                            </tr>
                                            Next
                    </tbody>
                </table>
            </div>
            <br Class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>


    <div id="divModalHistoricoSituacao"></div>
</div>
