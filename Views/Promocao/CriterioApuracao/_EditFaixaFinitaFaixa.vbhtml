﻿@ModelType PromocaoCriterioApuracaoViewModel
@Imports Martins.AcoesMercadologicas.Core


<div class="widget-body">
    <div class="widget-main faixa" id="widgetCriterio">
        <div class="form-horizontal">

            <div class="widget-body">
                <div class="widget-main">


                    <div class="row">
                        @Html.HiddenFor(Function(Model) Model.CODPMC, New With {.id = "hdfCodPmcFaixaFinitaFaixa"})
                        <div Class="col-sm-6">
                            @Html.Label("Forma de calculo:")
                            @Html.DropDownListFor(Function(r) r.CodFormaCalculo, New SelectList(ViewBag.formaDeCalculo, "tipeqccalctrpmc", "deseqccalctrpmc", Model.CodFormaCalculo), New With {.class = "cursorNotAllowed disabledEdit form-control", .id = "dropFormaCalculoFaixaFinitaFaixa"})
                        </div>
                        <div id="divCriterio" Class="col-sm-6">
                            @Html.Label("Critério:")
                            @Html.DropDownListFor(Function(r) r.CODCTRPMC, New SelectList(ViewBag.criterio, "CODCTRPMC", "DESCTRPMC", Model.CODCTRPMC), New With {.class = "chosen-select form-control", .id = "dropCriterioFaixaFinitaFaixa"})
                        </div>
                    </div>
                    <br />
                    <table id="tblGridFaixaFinitaFaixa" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th>
                                    Faixa
                                </th>
                                <th>
                                    Início
                                </th>
                                <th>

                                </th>
                                <th>
                                    Fim
                                </th>
                                @If (Model.INDTIPPTOPMC = 1) Then
                                    @<th>
                                        Percentual Cash Back por Atingimento
                                    </th>
                                ElseIf (Model.INDTIPPTOPMC = 2) Then
                                    @<th>
                                        Valor Cash Back por Atingimento
                                    </th>
                                End If

                            </tr>
                        </thead>
                        <tbody>
                            @*@For Each item In Model.ParametrosCalculo
                    @<tr>
                        <td Class="center">
                            @Html.Label("Único", New With {.class = "form-control", .id = "lblMixSimples"})
                        </td>
                        <td>
                            @Html.DropDownListFor(Function(r) item.CODCTRPMC, New SelectList(ViewBag.criterio, "CODCTRPMC", "DESCTRPMC", item.CODCTRPMC), "SELECIONE...", New With {.Class = "form-control", .id = "drpTipoCriterioSimples"})
                        </td>
                        <td>
                            @Html.TextBoxFor(Function(r) item.TAMIVLFXAEQCCALCTR, New With {.class = "form-control"})
                        </td>
                    </tr>
                Next*@
                            @For Each item In Model.ParametrosCalculo.ToList()
                                @<tr>
                                    <td Class="center">
                                        @If (item.TIPEDENIVPRMPMC <> 0) Then
                                            @Html.Label(item.TIPEDENIVPRMPMC)
                                        End If
                                    </td>
                                    <td Class="center">
                                        @If (item.CODCTRPMC = 2) Then
                                            @Html.TextBoxFor(Function(r) item.VLRLIMMNMFXACTRPMC, "{0:0}", New With {.alt = "inteiro"})
                                        ElseIf (item.CODCTRPMC = 5) Then
                                            @Html.TextBoxFor(Function(r) item.VLRLIMMNMFXACTRPMC, "{0:0.00}", New With {.alt = "decimal_positivo_2"})
                                        End If
                                    </td>
                                    <td Class="center">
                                        <b> a</b>
                                    </td>

                                    <td Class="center">
                                        @If (item.VLRLIMMAXFXACTRPMC >= 9999999999) Then
                                            @Html.TextBox("Acima", "Acima", New With {.readonly = "readonly"})
                                        Else
                                            @If (item.CODCTRPMC = 2) Then
                                                @Html.TextBoxFor(Function(r) item.VLRLIMMAXFXACTRPMC, "{0:0}", New With {.alt = "inteiro"})
                                            ElseIf (item.CODCTRPMC = 5) Then
                                                @Html.TextBoxFor(Function(r) item.VLRLIMMAXFXACTRPMC, "{0:0.00}", New With {.alt = "decimal_positivo_2"})
                                            End If
                                        End If
                                    </td>
                                    @If (Model.INDTIPPTOPMC = 1) Then
                                        @<td class="center">
                                            @If (Model.QDEPTOFXAEQCCALCTR <> 0) Then
                                                @Html.TextBoxFor(Function(r) item.QDEPTOFXAEQCCALCTR, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada"})
                                            ElseIf (Model.QDEPTOFXACTRPMC <> 0) Then
                                                @Html.TextBoxFor(Function(r) item.QDEPTOFXACTRPMC, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada"})
                                            End If
                                        </td>
                                    ElseIf (Model.INDTIPPTOPMC = 2) Then
                                        @<td class="center">
                                            @If (Model.QDEPTOFXAEQCCALCTR <> 0) Then
                                                @Html.TextBoxFor(Function(r) item.QDEPTOFXAEQCCALCTR, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada"})
                                            ElseIf (Model.QDEPTOFXACTRPMC <> 0) Then
                                                @Html.TextBoxFor(Function(r) item.QDEPTOFXACTRPMC, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada"})
                                            End If
                                        </td>
                                    End If
                                    <td Class="hide">
                                        @Html.TextBoxFor(Function(r) item.TIPEDENIVPRMPMC, New With {.id = "tipe"})
                                    </td>
                                    <td Class="center hide">
                                        @Html.TextBoxFor(Function(r) item.VLRLIMMAXFXACTRPMC, New With {.id = "txtFimHide"})
                                    </td>
                                </tr>
                            Next
                        </tbody>
                    </table>

                    <div Class="row">
                        <div Class="col-sm-6">
                            @Html.Label("Tipo de Prêmio:", New With {.id = "lblTipoBrinde"})
                            <br />
                            @Html.TextBoxFor(Function(r) r.INDTIPPTOPMC_DESC, New With {.class = "form-control"})
                        </div>
                        <div Class="col-sm-6">
                            @If (Model.INDTIPPTOPMC = 0) Then
                                @Html.Label("Qtd de Prêmio por Atingimento:", New With {.id = "lblBrindes"})
                                @<br />
                                If (Model.QDEPTOFXAEQCCALCTR <> 0) Then
                                    @Html.TextBoxFor(Function(r) r.QDEPTOFXAEQCCALCTR, New With {.class = "form-control", .alt = "inteiro_positivo", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                                ElseIf (Model.QDEPTOFXACTRPMC <> 0) Then
                                    @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, New With {.class = "form-control", .alt = "inteiro_positivo", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                                End If
                            End If
                        </div>
                        <div Class="col-sm-0">
                            <label>&nbsp;</label>
                            <br />
                            <div Class="btn-group pull-right">
                                <Button type="button" Class="btn btn-white btn-success" onclick='jsListaGridCriterioApuracaoFaixaFinitaFaixa()'>
                                    <i Class="ace-icon fa fa-save bigger-120 green"></i>
                                    Salvar
                                </Button>
                            </div>
                        </div>
                    </div>
                    @If (Model.INDTIPPTOPMC = 1) Then
                        @<div class="row">
                            <div Class="col-sm-6">
                                @Html.Label("Limitado a:", New With {.id = "lblTipoBrinde"})
                                <br />
                                @Html.TextBoxFor(Function(r) r.VLRLIMMAXCTR, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                            </div>
                        </div>
                    End If
                </div>
            </div>
        </div>
    </div>
</div>



