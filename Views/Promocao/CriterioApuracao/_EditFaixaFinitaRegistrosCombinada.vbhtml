﻿@ModelType PromocaoCriterioApuracaoViewModel
@Imports Martins.AcoesMercadologicas.Core


<div class="widget-body">
    <div class="widget-main" id="widgetCriterio">
        <div class="form-horizontal">

            <div class="widget-body">
                <div class="widget-main">


                    <div class="row">
                        @Html.HiddenFor(Function(Model) Model.CODPMC, New With {.id = "hdfCodPmcFaixaFinitaCombinada"})
                        <div Class="col-sm-6">
                            @Html.Label("Forma de calculo:")
                            @Html.DropDownListFor(Function(r) r.CodFormaCalculo, New SelectList(ViewBag.formaDeCalculo, "tipeqccalctrpmc", "deseqccalctrpmc", Model.CodFormaCalculo), New With {.class = "form-control", .id = "dropFormaCalculoFaixaFinitaCombinada"})
                        </div>
                    </div>
                    <br />

                    <table id="tblGridRegistrosCombinada" class="table table-striped table-bordered table-hover dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th>
                                    MIX
                                </th>
                                <th>
                                    Tipo Critério
                                </th>
                                <th>
                                    Critério
                                </th>
                                <th>
                                    Descrição Mecânica
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @For Each item In Model.ParametrosCalculo.ToList()
                                @<tr>
                                    <td Class="center">
                                        @Html.Label(item.CODGRPMIXMERPMC)

                                    </td>
                                    <td>
                                        @Html.DropDownListFor(Function(r) item.CODCTRPMC, New SelectList(ViewBag.criterio, "CODCTRPMC", "DESCTRPMC", item.CODCTRPMC), New With {.Class = "form-control"})

                                        @*@Html.DropDownListFor(Function(r) item.CODCTRPMC, New SelectList(ViewBag.criterio, "CODCTRPMC", "DESCTRPMC", item.CODCTRPMC), "SELECIONE...", New With {.Class = "form-control"})*@
                                    </td>
                                    <td>
                                        @Html.TextBoxFor(Function(r) item.VLRLIMMNMFXACTRPMC, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2"})
                                    </td>
                                    <td>
                                        @code
                                            Dim desc = item.DESCTRMIXPMC.Trim()
                                        End Code
                                        @Html.TextBoxFor(Function(r) desc, New With {.class = "form-control desc", .maxlength = "70"})
                                    </td>
                                    <td Class="hide">
                                        @Html.TextBoxFor(Function(r) item.TIPEDENIVPRMPMC, New With {.id = "tipe"})
                                    </td>
                                </tr>
                            Next
                        </tbody>
                    </table>
                    <br />
                    <div Class="row">
                        <div Class="col-sm-6">
                            @Html.Label("Tipo de Prêmio:", New With {.id = "lblTipoBrinde"})
                            <br />
                            @Html.TextBoxFor(Function(r) r.INDTIPPTOPMC_DESC, New With {.class = "form-control"})
                        </div>
                        <div Class="col-sm-6">
                            @If (Model.INDTIPPTOPMC = 0) Then
                                @Html.Label("Qtd de Prêmio por Atingimento:", New With {.id = "lblBrindes"})
                                @<br />
                                If (Model.QDEPTOFXAEQCCALCTR <> 0) Then
                                    @Html.TextBoxFor(Function(r) r.QDEPTOFXAEQCCALCTR, New With {.class = "form-control", .alt = "inteiro_positivo", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                                ElseIf (Model.QDEPTOFXACTRPMC <> 0) Then
                                    @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, New With {.class = "form-control", .alt = "inteiro_positivo", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                                End If
                            ElseIf (Model.INDTIPPTOPMC = 1) Then
                                @Html.Label("Percentual Cash Back por Atingimento:", New With {.id = "lblBrindes"})
                                @<br />
                                If (Model.QDEPTOFXAEQCCALCTR <> 0) Then
                                    @Html.TextBoxFor(Function(r) r.QDEPTOFXAEQCCALCTR, New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                                ElseIf (Model.QDEPTOFXACTRPMC <> 0) Then
                                    @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                                End If
                            ElseIf (Model.INDTIPPTOPMC = 2) Then
                                @Html.Label("Valor Cash Back por Atingimento:", New With {.id = "lblBrindes"})
                                @<br />
                                If (Model.QDEPTOFXAEQCCALCTR <> 0) Then
                                    @Html.TextBoxFor(Function(r) r.QDEPTOFXAEQCCALCTR, New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                                ElseIf (Model.QDEPTOFXACTRPMC <> 0) Then
                                    @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                                End If
                            End If
                        </div>
                        <div Class="col-sm-0">
                            <label>&nbsp;</label>
                            <br />
                            <div Class="btn-group pull-right">
                                <Button type="button" Class="btn btn-white btn-success " onclick='jsListaGridCriterioApuracaoFaixaFinitaCombinada()'>
                                    <i Class="ace-icon fa fa-save bigger-120 green"></i>
                                    Salvar
                                </Button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




