﻿@ModelType PromocaoBrindeViewModel
@Imports Martins.AcoesMercadologicas.Core


<div class="widget-box widget-color-wi">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller blue lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Critério de apuração
        </h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>
        @*<div class="widget-toolbar" id="editCriterio">
                <a onclick="editCancelBlock('Criterio', this)">
                    <i class="1 ace-icon fa fa-edit bigger-125"></i>
                </a>
        </div>*@
    </div>

    <div id="divCriterioApuracaoEditView"></div>

</div>


