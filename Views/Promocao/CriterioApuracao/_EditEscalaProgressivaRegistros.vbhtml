﻿@ModelType PromocaoCriterioApuracaoViewModel
@Imports Martins.AcoesMercadologicas.Core



<div class="widget-body">
    <div class="widget-main" id="widgetCriterio">

        <div class="widget-body">
            <div class="widget-main">


                <div class="row">
                    @Html.HiddenFor(Function(Model) Model.CODPMC, New With {.id = "hdfCodPmcEscalaProgressivaResgistros"})
                    <div Class="col-sm-6">
                        @Html.Label("Forma de calculo:")
                        @Html.DropDownListFor(Function(r) r.CodFormaCalculo, New SelectList(ViewBag.formaDeCalculo, "tipeqccalctrpmc", "deseqccalctrpmc", Model.CodFormaCalculo), New With {.class = "form-control", .id = "dropFormaCalculoEscalaProgressivaRegistros"})
                    </div>
                </div>
                <br />
                <table id="tblGridEscalaProgressivaRegistros" class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th>
                                MIX
                            </th>
                            <th>
                                Tipo Critério
                            </th>
                            <th>
                                Critério
                            </th>
                        </tr>
                    </thead>
                    <tbody id="tbodyTeste">
                        @For Each item In Model.ParametrosCalculo.ToList()
                            @<tr>
                                <td Class="center">
                                    @Html.Label("Único", New With {.class = "form-control", .id = "lblMixSimples"})
                                </td>
                                <td>
                                    @Html.DropDownListFor(Function(r) item.CODCTRPMC, New SelectList(ViewBag.criterio, "CODCTRPMC", "DESCTRPMC", item.CODCTRPMC), New With {.Class = "form-control", .id = "drpTipoCriterioSimples"})
                                </td>
                                <td>
                                    @If (item.CODCTRPMC = 2) Then
                                        @Html.TextBoxFor(Function(r) item.TAMIVLFXAEQCCALCTR, "{0:0}", New With {.class = "form-control", .alt = "inteiro"})
                                    End If
                                    @If (item.CODCTRPMC = 5) Then
                                        @Html.TextBoxFor(Function(r) item.TAMIVLFXAEQCCALCTR, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2"})
                                    End If
                                </td>
                                <td Class="hide">
                                    @Html.TextBoxFor(Function(r) item.TIPEDENIVPRMPMC, New With {.id = "tipe"})
                                </td>
                            </tr>
                        Next

                    </tbody>
                </table>


                <div Class="row">
                    <div Class="col-sm-6">
                        @Html.Label("Tipo de Prêmio:", New With {.id = "lblTipoBrinde"})
                        <br />
                        @Html.TextBoxFor(Function(r) r.INDTIPPTOPMC_DESC, New With {.class = "form-control"})
                    </div>
                    <div Class="col-sm-6">
                        @If (Model.INDTIPPTOPMC = 0) Then
                            @Html.Label("Qtd de Prêmio por Atingimento:", New With {.id = "lblBrindes"})
                            @<br />
                            If (Model.QDEPTOFXAEQCCALCTR <> 0) Then
                                @Html.TextBoxFor(Function(r) r.QDEPTOFXAEQCCALCTR, New With {.class = "form-control", .alt = "inteiro_positivo", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                            ElseIf (Model.QDEPTOFXACTRPMC <> 0) Then
                                @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, New With {.class = "form-control", .alt = "inteiro_positivo", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                            End If
                        ElseIf (Model.INDTIPPTOPMC = 1) Then
                            @Html.Label("Percentual Cash Back por Atingimento:", New With {.id = "lblBrindes"})
                            @<br />
                            If (Model.QDEPTOFXAEQCCALCTR <> 0) Then
                                @Html.TextBoxFor(Function(r) r.QDEPTOFXAEQCCALCTR, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                            ElseIf (Model.QDEPTOFXACTRPMC <> 0) Then
                                @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                            End If
                        ElseIf (Model.INDTIPPTOPMC = 2) Then
                            @Html.Label("Valor Cash Back por Atingimento:", New With {.id = "lblBrindes"})
                            @<br />
                            If (Model.QDEPTOFXAEQCCALCTR <> 0) Then
                                @Html.TextBoxFor(Function(r) r.QDEPTOFXAEQCCALCTR, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                            ElseIf (Model.QDEPTOFXACTRPMC <> 0) Then
                                @Html.TextBoxFor(Function(r) r.QDEPTOFXACTRPMC, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                            End If
                        End If
                    </div>
                    <div Class="col-sm-0">
                        <label>&nbsp;</label>
                        <br />
                        <div Class="btn-group pull-right">
                            <Button type="submit" Class="btn btn-white btn-success" onclick="jsListaGridCriterioApuracaoEscalaProgressivaRegistros()">
                                <i Class="ace-icon fa fa-save bigger-120 green"></i>
                                Salvar
                            </Button>
                        </div>
                    </div>
                </div>
                @If (Model.INDTIPPTOPMC <> 0) Then
                    @<div class="row">
                         <div Class="col-sm-6">
                             @Html.Label("Limitado a:", New With {.id = "lblTipoBrinde"})
                             <br />
                             @Html.TextBoxFor(Function(r) r.VLRLIMMAXCTR, "{0:0.00}", New With {.class = "form-control", .alt = "decimal_positivo_2", .id = "TxtQtdPremioAtingidoEscalaProgressivoCombinada", .style = "width: 250px;"})
                         </div>
                    </div>
                End If
            </div>
        </div>
    </div>
</div>