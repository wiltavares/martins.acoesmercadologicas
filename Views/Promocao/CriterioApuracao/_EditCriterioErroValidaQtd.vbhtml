﻿@ModelType List(Of PromocaoViewModel)
@Imports Martins.AcoesMercadologicas.Core
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title center" id="myModalLabel">Motivo</h4>
        </div>
        <div class="modal-body">

            <h4>Os brindes abaixo cadastrados na promoção possuem qde minima de venda maior que a qde da mecanica da promoção</h4>

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td>
                            Qtd Minima
                        </td>
                        <td>
                            Qtd Mecanica
                        </td>
                        <td>
                            Mercadoria
                        </td>
                    </tr>
                </thead>
                <tbody>
                    @Code
                        For Each item In Model
                            @<tr>
                                <td>@item.QDEMIN</td>
                                <td>@item.QDEPREMIOATINGIMENTO</td>
                                <td>@item.CODMER - @item.DESMER</td>
                            </tr>
                        Next
                    End Code
                </tbody>
            </table>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-warning" onclick="$('#modalPromocao').modal('hide');">Cancelar</button>
        </div>
    </div>
</div>

