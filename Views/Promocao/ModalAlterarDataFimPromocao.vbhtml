﻿@ModelType PromocaoViewModel
@Imports Martins.AcoesMercadologicas.Core


<div Class="modal-dialog modal-lg hideScroll" id="modalAlterarDataFimPromocao" style="z-index: 1051; margin-top: 4px;">
    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Alterar Data Fim
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalAlterarDataFimPromocao">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div Class="row">
                <div Class="col-sm-4">
                    @Html.Label("lblDATFIMPMC", "Data Fim (*):")
                    <div class="input-daterange input-group">
                        @Html.TextBoxFor(Function(Model) Model.DATFIMPMC, New With {.class = "input-sm form-control date-picker", .alt = "datepicker", .id = "txtNovaDataFimPromocao", .autocomplete = "off"})
                        @*@Html.ValidationMessageFor(Function(m) m.DATFIMPMC, "", New With {.class = "text-danger-message-login"})*@
                    </div>
                </div>
            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button type="button" Class="btn btn-white btn-success" onclick="SalvarAlteração()">
                    <i Class="ace-icon fa fa-save bigger-120 green"></i>
                    Salvar
                </Button>
                <Button Class="btn btn-danger btn-white" type="button" onclick="FecharModalAlterarDataFim()">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>



