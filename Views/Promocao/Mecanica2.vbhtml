﻿
@Code
    ViewData("Title") = "Gerar Promoção"
End Code
<div id="divMecanica">

    <div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-line-chart"></i>
                Promoção
            </li>
            <li class="active">
                Pendentes de geração
            </li>
        </ul>
    </div>
    <div class="widget-box widget-color-blue2 ">
        <div class="widget-header widget-header-small">
            <h4 class="widget-title smaller white lighter">
                <i class="ace-icon fa fa-sliders bigger-110"></i>
                Pendentes de geração da promoção
            </h4>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div id="divwidgetMecanica" class="widget-main">
                <form action="/martins.acoesmercadologicas/AcaoComercial/Pesquisar" data-ajax="true" data-ajax-begin="LoadingOn" data-ajax-complete="LoadingOff_UpdateMessage" data-ajax-method="POST" data-ajax-mode="replace" data-ajax-update="#resultadosView" id="form0" method="post">
                    <input name="__RequestVerificationToken" type="hidden" value="sJbZ15k2-W44HUMX5b5B7CXDzdwnVTvZBC08J_1xUHR3ZQutnGpIGN4wvyKS0puIBEC4N9Gnqa8JfM84cqY17ay3hvoFM-Gl4CVg0EcH9tU1">                <div class="form-horizontal">

                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-1">
                                    <button class="btn btn-xs btn-success" type="button" onclick="$('#divWidgetWizar').show(); ChosenResize(); $('#divMecanica').hide();">
                                        <i class="ace-icon fa fa-cogs bigger-119"></i>
                                    </button>
                                </div>
                                <div class="col-sm-2">
                                    <label>Ação </label>
                                    <br />
                                    <span><b>266865</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Nome</label>
                                    <br />
                                    <span><b>Promo cl 3175134</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Status</label>
                                    <br />
                                    <span><b>EM APROVAÇÃO</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Autor</label>
                                    <br />
                                    <span><b>Renata Mara Pereira</b></span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-1">
                                </div>
                                <div class="col-sm-2">
                                    <label>Geração</label>
                                    <br />
                                    <span><b>17/05/2017</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Início</label>
                                    <br />
                                    <span><b>17/05/2017</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Fim</label>
                                    <br />
                                    <span><b>20/05/2017</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Valor Orçado</label>
                                    <br />
                                    <span><b>0,00</b></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-2">
                                    <label>Tipo:</label>
                                    <br />
                                    <span><b>Faixas Multiplas</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Quantidade de Faixa:</label>
                                    <br />
                                    <span><b>8</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Participação Mix Total:</label>
                                    <br />
                                    <span><b>Sim</b></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-11">
                                    <p>Mecanica: <b>Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica...</b></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-11">
                                    <label>Filiais:</label>
                                    <br />
                                    <span class="badge gadge-primary">Nome da filial 1</span>
                                    <span class="badge gadge-primary">Nome da filial 2</span>
                                    <span class="badge gadge-primary">Nome da filial 3</span>
                                    <span class="badge gadge-primary">Nome da filial 4</span>
                                    <span class="badge gadge-primary">Nome da filial 5</span>
                                </div>
                            </div>
                        </div>


                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-1">
                                    <button class="btn btn-xs btn-success" type="button" onclick="$('#divWidgetWizar').show(); ChosenResize(); $('#divMecanica').hide();">
                                        <i class="ace-icon fa fa-cogs bigger-119"></i>
                                    </button>
                                </div>
                                <div class="col-sm-2">
                                    <label>Ação </label>
                                    <br />
                                    <span><b>266999</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Nome</label>
                                    <br />
                                    <span><b>Promo natal 4323567</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Status</label>
                                    <br />
                                    <span><b>EM APROVAÇÃO</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Autor</label>
                                    <br />
                                    <span><b>João Paulo</b></span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-2">
                                    <label>Geração</label>
                                    <br />
                                    <span><b>10/11/2018</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Início</label>
                                    <br />
                                    <span><b>20/12/2018</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Fim</label>
                                    <br />
                                    <span><b>26/12/2018</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Valor Orçado</label>
                                    <br />
                                    <span><b>0,00</b></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-2">
                                    <label>Tipo:</label>
                                    <br />
                                    <span><b>Faixas Multiplas</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Quantidade de Faixa:</label>
                                    <br />
                                    <span><b>8</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Participação Mix Total:</label>
                                    <br />
                                    <span><b>Sim</b></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-11">
                                    <label>Mecanica</label>
                                    <p>Mecanica: <b>Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica...</b></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-11">
                                    <label>Filiais:</label>
                                    <br />
                                    <span class="badge gadge-primary">Nome da filial 1</span>
                                    <span class="badge gadge-primary">Nome da filial 2</span>
                                    <span class="badge gadge-primary">Nome da filial 3</span>
                                    <span class="badge gadge-primary">Nome da filial 4</span>
                                    <span class="badge gadge-primary">Nome da filial 5</span>
                                </div>
                            </div>
                        </div>

                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-1">
                                    <button class="btn btn-xs btn-success" type="button" onclick="$('#divWidgetWizar').show(); ChosenResize(); $('#divMecanica').hide();">
                                        <i class="ace-icon fa fa-cogs bigger-119"></i>
                                    </button>
                                </div>
                                <div class="col-sm-2">
                                    <label>Ação </label>
                                    <br />
                                    <span><b>356865</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Nome</label>
                                    <br />
                                    <span><b>Promo Réveillon 3175134</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Status</label>
                                    <br />
                                    <span><b>EM APROVAÇÃO</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Autor</label>
                                    <br />
                                    <span><b>Douglas Rocha</b></span>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-2">
                                    <label>Geração</label>
                                    <br />
                                    <span><b>01/11/2018</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Início</label>
                                    <br />
                                    <span><b>28/12/2018</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Fim</label>
                                    <br />
                                    <span><b>02/01/2019</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Valor Orçado</label>
                                    <br />
                                    <span><b>0,00</b></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-2">
                                    <label>Tipo:</label>
                                    <br />
                                    <span><b>Faixas Multiplas</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Quantidade de Faixa:</label>
                                    <br />
                                    <span><b>8</b></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>Participação Mix Total:</label>
                                    <br />
                                    <span><b>Sim</b></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-11">
                                    <p>Mecanica: <b>Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica... Descrição da mecânica...</b></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-11">
                                    <label>Filiais:</label>
                                    <br />
                                    <span class="badge gadge-primary">Nome da filial 1</span>
                                    <span class="badge gadge-primary">Nome da filial 2</span>
                                    <span class="badge gadge-primary">Nome da filial 3</span>
                                    <span class="badge gadge-primary">Nome da filial 4</span>
                                    <span class="badge gadge-primary">Nome da filial 5</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>
<div id="divWidgetWizar" class="widget-box widget-color-blue2 " style="display: none;">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-cogs bigger-110"></i>
            Promoção
        </h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Ação:</label>
                            <br />
                            <span><b>123123</b></span>
                        </div>
                        <div class="col-sm-3">
                            <label>Tipo:</label>
                            <br />
                            <span><b>Faixas Multiplas</b></span>
                        </div>
                        <div class="col-sm-3">
                            <label>Quantidade de Faixa</label>
                            <br />
                            <span><b>8</b></span>
                        </div>
                        <div class="col-sm-3">
                            <label>Participação Mix Total:</label>
                            <br />
                            <span><b>123123</b></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Mecanica:</label>
                            <p><b>Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....Texto da mecanica....</b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="fuelux-wizard-container">
            <div>
                <!-- #section:plugins/fuelux.wizard.steps -->
                <ul class="steps">
                    <li data-step="1" class="active">
                        <span class="step"></span>
                        <span class="title">Filiais</span>
                    </li>

                    <li data-step="2">
                        <span class="step"></span>
                        <span class="title">Geral</span>
                    </li>

                    <li data-step="3">
                        <span class="step"></span>
                        <span class="title">Prêmios</span>
                    </li>

                    <li data-step="4">
                        <span class="step"></span>
                        <span class="title">Comunicações</span>
                    </li>
                    <li data-step="5">
                        <span class="step"></span>
                        <span class="title">Filiais</span>
                    </li>
                    <li data-step="6">
                        <span class="step"></span>
                        <span class="title">Geral</span>
                    </li>

                    <li data-step="7">
                        <span class="step"></span>
                        <span class="title">Prêmios</span>
                    </li>
                    <li data-step="8">
                        <span class="step"></span>
                        <span class="title">Comunicações</span>
                    </li>
                    <li data-step="9">
                        <span class="step"></span>
                        <span class="title">Resumo</span>
                    </li>
                </ul>

                <!-- /section:plugins/fuelux.wizard.steps -->
            </div>

            <hr />

            <!-- #section:plugins/fuelux.wizard.container -->
            <div class="step-content pos-rel">

                <div class="step-pane active" data-step="1">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box widget-color-blue2">
                                <div class="widget-header widget-header-small widget-light-blue">
                                    <h4 class="widget-title smaller blue lighter">
                                        <i class="ace-icon fa fa-building bigger-110"></i>
                                        Filiais
                                    </h4>
                                </div>
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th class="center">
                                                    Aplicar Promoção Para
                                                </th>
                                                <th>
                                                    Código
                                                </th>
                                                <th>
                                                    Filial
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" checked="checked" />
                                                </td>
                                                <td>
                                                    1
                                                </td>
                                                <td>
                                                    Filial 1
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" />
                                                </td>
                                                <td>
                                                    2
                                                </td>
                                                <td>
                                                    Filial 2
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="step-pane active" data-step="2">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>BU:</label>
                            <select class="chosen-drop form-control">
                                <option value="">Selecione...</option>
                            </select>
                        </div>
                        <div class="col-sm-8">
                            <label>Nome Promoção:</label>
                            <text class="form-control"></text>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Dt. Inicio:</label>
                            <datalist class="form-control"></datalist>
                        </div>
                        <div class="col-sm-4">
                            <label>Dt. Fím:</label>
                            <datalist class="form-control"></datalist>
                        </div>
                        <div class="col-sm-4">
                            <label>Status Envio:</label>
                            <select class="chosen-drop form-control">
                                <option value="">Selecione...</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Visualizar Livro Simplificado:</label>
                            <br />
                            <input id="rblParticipacaoMixTotalSim" type="radio" value="1" />
                            <label for="rblParticipacaoMixTotalSim">Sim</label>
                            <input id="rblParticipacaoMixTotalNao" type="radio" value="2" />
                            <label for="rblParticipacaoMixTotalNao">Não</label>
                        </div>
                        <div class="col-sm-4">
                            <label>Publicar Promoção Aplicativo:</label>
                            <br />
                            <input id="rblParticipacaoMixTotalSim" type="radio" value="1" />
                            <label for="rblParticipacaoMixTotalSim">Sim</label>
                            <input id="rblParticipacaoMixTotalNao" type="radio" value="2" />
                            <label for="rblParticipacaoMixTotalNao">Não</label>
                        </div>
                        <div class="col-sm-4">
                            <br />
                            <label>Critério Adicional:</label>

                            <input id="rblParticipacaoMixTotalSim" type="checkbox" value="1" />
                            <label for="rblParticipacaoMixTotalSim"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Quantidade Grupo:</label>
                            <text class="form-control"></text>
                        </div>
                        <div class="col-sm-4">
                            <label>Quantidade Faixa:</label>
                            <text class="form-control"></text>
                        </div>
                        <div class="col-sm-4">
                            <label>Quantidade Máx. de brinde por cliente:</label>
                            <text class="form-control"></text>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Venda Avaliação Promoção Fator Cresc.:</label>
                            <text class="form-control"></text>
                        </div>
                        <div class="col-sm-4">
                            <label>valor Limite:</label>
                            <text class="form-control"></text>
                        </div>
                        <div class="col-sm-4">
                            <label>Quantidade Mínima:</label>
                            <text class="form-control"></text>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <br />
                            <label>Cobrança de Brinde:</label>

                            <input id="rblParticipacaoMixTotalSim" type="checkbox" value="1" />
                            <label for="rblParticipacaoMixTotalSim"></label>
                        </div>
                        <div class="col-sm-4">
                            <br />
                            <label>Brinde Adicional:</label>

                            <input id="rblParticipacaoMixTotalSim" type="checkbox" value="1" />
                            <label for="rblParticipacaoMixTotalSim"></label>
                        </div>

                    </div>
                </div>


                <div class="step-pane" data-step="3">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box" style="    border-color: green;">
                                <div class="widget-header widget-header-small" style=" border-color: green;">
                                    <h4 class="widget-title smaller green lighter">
                                        <i class="ace-icon fa fa-pencil bigger-110"></i>
                                        Dados
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Prêmio:</label>
                                                <div class="input-group" id="adv-search">
                                                    <input type="text" class="form-control" />
                                                    <div class="input-group-btn">
                                                        <div class="btn-group" role="group">
                                                            <button type="button" class="btn btn-primary btn-light" style="border-radius: 3px; padding: 4px; margin-left: 1px;"><span class="glyphicon glyphicon-search" aria-hidden="true" style="color: #0000ffb8;"></span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Per. Reserva:</label>
                                                <text class="form-control"></text>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Múltiplo Brinde:</label>
                                                <text class="form-control"></text>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Tipo Brinde:</label>
                                                <br />
                                                <input id="rblParticipacaoMixTotalSim" type="radio" value="1" />
                                                <label for="rblParticipacaoMixTotalSim">Normal</label>
                                                <input id="rblParticipacaoMixTotalNao" type="radio" value="2" />
                                                <label for="rblParticipacaoMixTotalNao">Adicional</label>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Faixa:</label>
                                                <select class="chosen-drop form-control">
                                                    <option value="">Selecione...</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>&nbsp;</label>
                                                <br />
                                                <button class="btn btn-success btn-white btn-adicionar">
                                                    Adicionar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box widget-color-blue2">
                                <div class="widget-header widget-header-small widget-light-blue">
                                    <h4 class="widget-title smaller blue lighter">
                                        Prêmio Normal
                                    </h4>
                                </div>
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th class="center">

                                                </th>
                                                <th>
                                                    Prêmio
                                                </th>
                                                <th>
                                                    Per. Reserva
                                                </th>
                                                <th>
                                                    Múltiplo Brinde
                                                </th>
                                                <th>
                                                    Faixa
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-danger" title="Excluir">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    Bicicleta
                                                </td>
                                                <td>
                                                    10%
                                                </td>
                                                <td>
                                                    Não
                                                </td>
                                                <td>
                                                    Faixa 1
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-danger" title="Excluir">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    Monitor
                                                </td>
                                                <td>
                                                    15%
                                                </td>
                                                <td>
                                                    Não
                                                </td>
                                                <td>
                                                    Faixa 2
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box widget-color-blue2">
                                <div class="widget-header widget-header-small widget-light-blue">
                                    <h4 class="widget-title smaller blue lighter">
                                        Prêmio Adicional
                                    </h4>
                                </div>
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th class="center">

                                                </th>
                                                <th>
                                                    Prêmio
                                                </th>
                                                <th>
                                                    Per. Reserva
                                                </th>
                                                <th>
                                                    Múltiplo Brinde
                                                </th>
                                                <th>
                                                    Faixa
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-danger" title="Excluir">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    TV Led 32''
                                                </td>
                                                <td>
                                                    25%
                                                </td>
                                                <td>
                                                    Não
                                                </td>
                                                <td>
                                                    Faixa 5
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="step-pane" data-step="4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box widget-color-blue2">
                                <div class="widget-header widget-header-small widget-light-blue">
                                    <h4 class="widget-title smaller blue lighter">
                                        <i class="ace-icon fa fa-comments bigger-110"></i>
                                        Comunicações
                                    </h4>
                                </div>
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th class="center">
                                                    Sel
                                                </th>
                                                <th>
                                                    Código
                                                </th>
                                                <th>
                                                    Comunicações
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" title="Selecionar Todos" />
                                                </td>
                                                <td>
                                                    1001
                                                </td>
                                                <td>
                                                    <text class="form-control"></text>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" title="Selecionar Todos" />
                                                </td>
                                                <td>
                                                    1002
                                                </td>
                                                <td>
                                                    <text class="form-control"></text>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" title="Selecionar Todos" />
                                                </td>
                                                <td>
                                                    1003
                                                </td>
                                                <td>
                                                    <text class="form-control"></text>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="step-pane active" data-step="5">


                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box widget-color-blue2">
                                <div class="widget-header widget-header-small widget-light-blue">
                                    <h4 class="widget-title smaller blue lighter">
                                        <i class="ace-icon fa fa-building bigger-110"></i>
                                        Filiais
                                    </h4>
                                </div>
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th class="center">
                                                    Aplicar Promoção Para
                                                </th>
                                                <th>
                                                    Código
                                                </th>
                                                <th>
                                                    Filial
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="text-muted">
                                                <td class="center">
                                                    <input type="checkbox" checked="checked" readonly="readonly" />
                                                </td>
                                                <td>
                                                    1
                                                </td>
                                                <td>
                                                    Filial 1
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" />
                                                </td>
                                                <td>
                                                    2
                                                </td>
                                                <td>
                                                    Filial 2
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="step-pane active" data-step="6">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>BU:</label>
                            <select class="chosen-drop form-control">
                                <option value="">Selecione...</option>
                            </select>
                        </div>
                        <div class="col-sm-8">
                            <label>Nome Promoção:</label>
                            <text class="form-control"></text>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Dt. Inicio:</label>
                            <datalist class="form-control"></datalist>
                        </div>
                        <div class="col-sm-4">
                            <label>Dt. Fím:</label>
                            <datalist class="form-control"></datalist>
                        </div>
                        <div class="col-sm-4">
                            <label>Status Envio:</label>
                            <select class="chosen-drop form-control">
                                <option value="">Selecione...</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Visualizar Livro Simplificado:</label>
                            <br />
                            <input id="rblParticipacaoMixTotalSim" type="radio" value="1" />
                            <label for="rblParticipacaoMixTotalSim">Sim</label>
                            <input id="rblParticipacaoMixTotalNao" type="radio" value="2" />
                            <label for="rblParticipacaoMixTotalNao">Não</label>
                        </div>
                        <div class="col-sm-4">
                            <label>Publicar Promoção Aplicativo:</label>
                            <br />
                            <input id="rblParticipacaoMixTotalSim" type="radio" value="1" />
                            <label for="rblParticipacaoMixTotalSim">Sim</label>
                            <input id="rblParticipacaoMixTotalNao" type="radio" value="2" />
                            <label for="rblParticipacaoMixTotalNao">Não</label>
                        </div>
                        <div class="col-sm-4">
                            <br />
                            <label>Critério Adicional:</label>

                            <input id="rblParticipacaoMixTotalSim" type="checkbox" value="1" />
                            <label for="rblParticipacaoMixTotalSim"></label>
                        </div>
                    </div>
                    <div class="row">
                        @*<div class="col-sm-4">
                                <label>Quantidade Grupo:</label>
                                <text class="form-control"></text>
                            </div>
                            <div class="col-sm-4">
                                <label>Quantidade Faixa:</label>
                                <text class="form-control"></text>
                            </div>*@

                        <div class="col-sm-4">
                            <label>Quantidade Máx. de brinde por cliente:</label>
                            <text class="form-control"></text>
                        </div>

                        <div class="col-sm-4">
                            <label>Venda Avaliação Promoção Fator Cresc.:</label>
                            <text class="form-control"></text>
                        </div>
                        <div class="col-sm-4">
                            <label>valor Limite:</label>
                            <text class="form-control"></text>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Quantidade Mínima:</label>
                            <text class="form-control"></text>
                        </div>

                        <div class="col-sm-4">
                            <br />
                            <label>Cobrança de Brinde:</label>

                            <input id="rblParticipacaoMixTotalSim" type="checkbox" value="1" />
                            <label for="rblParticipacaoMixTotalSim"></label>
                        </div>
                        <div class="col-sm-4">
                            <br />
                            <label>Brinde Adicional:</label>

                            <input id="rblParticipacaoMixTotalSim" type="checkbox" value="1" />
                            <label for="rblParticipacaoMixTotalSim"></label>
                        </div>

                    </div>
                </div>

                <div class="step-pane" data-step="7">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box" style="    border-color: green;">
                                <div class="widget-header widget-header-small" style=" border-color: green;">
                                    <h4 class="widget-title smaller green lighter">
                                        <i class="ace-icon fa fa-pencil bigger-110"></i>
                                        Dados
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Prêmio:</label>
                                                <div class="input-group" id="adv-search">
                                                    <input type="text" class="form-control" />
                                                    <div class="input-group-btn">
                                                        <div class="btn-group" role="group">
                                                            <button type="button" class="btn btn-primary btn-light" style="border-radius: 3px; padding: 4px; margin-left: 1px;"><span class="glyphicon glyphicon-search" aria-hidden="true" style="color: #0000ffb8;"></span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Per. Reserva:</label>
                                                <text class="form-control"></text>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Múltiplo Brinde:</label>
                                                <text class="form-control"></text>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Tipo Brinde:</label>
                                                <br />
                                                <input id="rblParticipacaoMixTotalSim" type="radio" value="1" />
                                                <label for="rblParticipacaoMixTotalSim">Normal</label>
                                                <input id="rblParticipacaoMixTotalNao" type="radio" value="2" />
                                                <label for="rblParticipacaoMixTotalNao">Adicional</label>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Faixa:</label>
                                                <select class="chosen-drop form-control">
                                                    <option value="">Selecione...</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>&nbsp;</label>
                                                <br />
                                                <button class="btn btn-success btn-white btn-adicionar">
                                                    Adicionar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box widget-color-blue2">
                                <div class="widget-header widget-header-small widget-light-blue">
                                    <h4 class="widget-title smaller blue lighter">
                                        Prêmio Normal
                                    </h4>
                                </div>
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th class="center">

                                                </th>
                                                <th>
                                                    Prêmio
                                                </th>
                                                <th>
                                                    Per. Reserva
                                                </th>
                                                <th>
                                                    Múltiplo Brinde
                                                </th>
                                                <th>
                                                    Faixa
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-danger" title="Excluir">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    Bicicleta
                                                </td>
                                                <td>
                                                    10%
                                                </td>
                                                <td>
                                                    Não
                                                </td>
                                                <td>
                                                    Faixa 1
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-danger" title="Excluir">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    Monitor
                                                </td>
                                                <td>
                                                    15%
                                                </td>
                                                <td>
                                                    Não
                                                </td>
                                                <td>
                                                    Faixa 2
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box widget-color-blue2">
                                <div class="widget-header widget-header-small widget-light-blue">
                                    <h4 class="widget-title smaller blue lighter">
                                        Prêmio Adicional
                                    </h4>
                                </div>
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th class="center">

                                                </th>
                                                <th>
                                                    Prêmio
                                                </th>
                                                <th>
                                                    Per. Reserva
                                                </th>
                                                <th>
                                                    Múltiplo Brinde
                                                </th>
                                                <th>
                                                    Faixa
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center">
                                                    <button class="btn btn-xs btn-danger" title="Excluir">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    TV Led 32''
                                                </td>
                                                <td>
                                                    25%
                                                </td>
                                                <td>
                                                    Não
                                                </td>
                                                <td>
                                                    Faixa 5
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="step-pane" data-step="8">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box widget-color-blue2">
                                <div class="widget-header widget-header-small widget-light-blue">
                                    <h4 class="widget-title smaller blue lighter">
                                        <i class="ace-icon fa fa-comments bigger-110"></i>
                                        Comunicações
                                    </h4>
                                </div>
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th class="center">
                                                    Sel
                                                </th>
                                                <th>
                                                    Código
                                                </th>
                                                <th>
                                                    Comunicações
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" title="Selecionar Todos" />
                                                </td>
                                                <td>
                                                    1001
                                                </td>
                                                <td>
                                                    <text class="form-control"></text>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" title="Selecionar Todos" />
                                                </td>
                                                <td>
                                                    1002
                                                </td>
                                                <td>
                                                    <text class="form-control"></text>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="center">
                                                    <input type="checkbox" title="Selecionar Todos" />
                                                </td>
                                                <td>
                                                    1003
                                                </td>
                                                <td>
                                                    <text class="form-control"></text>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="step-pane" data-step="9">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box widget-color-blue2">
                                <div class="widget-header widget-header-small widget-light-blue">
                                    <h4 class="widget-title smaller blue lighter">
                                        <i class="ace-icon fa fa-book bigger-110"></i>
                                        Resumo
                                    </h4>
                                </div>
                                <div style="width: auto; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                                        <thead>
                                            <tr role="row">
                                                <th>
                                                    Filial
                                                </th>
                                                <th>
                                                    Promoção
                                                </th>
                                                <th>
                                                    Dt. Inicio Promoção
                                                </th>
                                                <th>
                                                    Dt. Fim Promoção
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Filial 1
                                                </td>
                                                <td>
                                                    1002
                                                </td>
                                                <td>
                                                    15/12/2018
                                                </td>
                                                <td>
                                                    01/01/2019
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Filial 5
                                                </td>
                                                <td>
                                                    1003
                                                </td>
                                                <td>
                                                    20/12/2018
                                                </td>
                                                <td>
                                                    10/01/2019
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>




            <!-- /section:plugins/fuelux.wizard.container -->
        </div>

        <hr />
        <div class="wizard-actions">
            <!-- #section:plugins/fuelux.wizard.buttons -->
            <button class="btn btn-white btn-prev">
                <i class="ace-icon fa fa-arrow-left"></i>
                Voltar
            </button>

            <button class="btn btn-success btn-white btn-next" data-last="Concluir">
                Avançar
                <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
            </button>

            <button class="btn btn-danger btn-white" type="button" onclick="$('#divWidgetWizar').hide(); $('#divMecanica').show();">
                <i class="ace-icon fa fa-close bigger-120 red"></i>
                Cancelar
            </button>
            <!-- /section:plugins/fuelux.wizard.buttons -->
        </div>
    </div>
</div>

@Html.Partial("~/Views/AcaoComercial/Mecanica/_Wizard.vbhtml")
