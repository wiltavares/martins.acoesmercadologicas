﻿@ModelType List(Of PromocaoHistoricoSituacaoViewModel)


<div class="modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header ">
                <i class="ace-icon fa fa-book bigger-120"></i>
                &nbsp;Histórico
            </div>
            <div class="modal-body">

                <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th>Seq</th>
                            <th>Tipo</th>
                            <th>Data</th>
                            <th>Matrícula</th>
                            <th>Nome do Funcionário</th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @<tr>
                                <td>
                                    @Html.DisplayFor(Function(m) item.NUMSEQ)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.DESSITPMC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.DATGRCHST)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.CODFNC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(m) item.NOMFNC)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>

            </div>
            <div class="modal-footer clearfix">
                <div class="btn-group pull-right">
                    <button class="btn btn-danger btn-white" onclick="btnFecharExibirHistorico()">
                        <i class="ace-icon fa fa-close bigger-120 red"></i>
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>