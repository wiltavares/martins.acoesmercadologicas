﻿@ModelType PromocaoViewModel
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-main disabledEdit" id="widgetPromocao" style="padding: 0">
    <div class="form-horizontal">
        <div class="widget-body">
            <div class="widget-main">
                <div>
                    <div class="well well-sm ">

                        @*HiddenFor para todos os campos da promoção*@
                        @*@Html.HiddenFor(Function(Model) Model.CODPMC, New With {.Class = "input-sm date-interval form-control"})*@
                        @Html.HiddenFor(Function(Model) Model.TIPEDEPUBALVPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.DESPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.DATHRAGRCPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.CODFNCGRCPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.FLGSLCLSTPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.QDEPTOCOPVND, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.TIPSITPMC, New With {.Class = "input-sm date-interval form-control", .id = "hdfSituacaoPromocao"})
                        @Html.HiddenFor(Function(Model) Model.CODFNCALTSITPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.DATHRAALTSITPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.DATPCSPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.TIPAPUPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.DATHRAALTPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.CODEMP, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.CODPMCPCP, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.QDEBNFPMCCAA, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.QDEVNDPMCCAA, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.DATINIPMCCAA, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.DATFIMPMCCAA, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.FLGCONPMCCAA, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.NOMUSRSIS, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.INDPTCPMCCMCETN, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.INDPTCPMCTMK, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.INDAPUICTQDEBDE, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.INDECSITEPMCPEDVND, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.INDPRMUNCVGRPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.VLRBSECALGRPMIXPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.PERVARGRPMIXPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.INDBDEFRC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.INDSTAPMCREL, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.TIPEQIVND, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.VLRVBAPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.CODFNCALTPMC, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.QDEGRPMIXPMC, New With {.Class = "input-sm date-interval form-control", .id = "IdQDEGRPMIXPMC"})
                        @Html.HiddenFor(Function(Model) Model.INDPMCSISNVO, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.INDISRBDEADI, New With {.Class = "input-sm date-interval form-control"})
                        @Html.HiddenFor(Function(Model) Model.CODFILEMP, New With {.Class = "input-sm date-interval form-control", .id = "idFilialPromocao"})
                        @Html.HiddenFor(Function(Model) Model.TIPPMC, New With {.Class = "input-sm date-interval form-control", .id = "idTipoPromocao"})

                        <div class="row">
                            <div Class="col-sm-4">
                                @Html.Label("lbCodPmc", "Código:")
                                <br />
                                @Html.TextBoxFor(Function(Model) Model.CODPMC, New With {.Class = "nput-sm date-interval form-control", .id = "IDCodPmc", .readonly = "readonly"})
                            </div>
                            <div Class="col-lg-4 col-sm-4">
                                @Html.Label("filial", "Filial (*):")
                                <br />
                                @Html.TextBoxFor(Function(Model) Model.CodNomFil, New With {.Class = "nput-sm date-interval form-control", .id = "IDfilial", .readonly = "readonly"})
                            </div>
                            <div class="col-sm-4">
                                @Html.Label("lblCODACOCMC", "Ação:")
                                <br />
                                @Html.TextBoxFor(Function(Model) Model.CODACOCMC, New With {.Class = "nput-sm date-interval form-control", .id = "idCodAcaoPromocao", .readonly = "readonly"})
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                @Html.Label("lbTIPPMC", "Tipo:")
                                <br />
                                @Html.TextBoxFor(Function(Model) Model.DesTIPPMC, New With {.Class = "nput-sm date-interval form-control", .id = "IdTipo", .readonly = "readonly"})
                            </div>
                            @Select Case Model.TIPPMC
                                Case 1
                                    @<div Class="col-sm-4">
                                        @Html.Label("lblQDEFXAPMC", "Quantidade de Grupo:")
                                        <br />
                                        @Html.TextBoxFor(Function(Model) Model.QDEGRPMIXPMC, New With {.class = "nput-sm date-interval form-control", .id = "IdQtdFaixa", .readonly = "readonly"})
                                    </div>
                                                    Case 2
                                                        @<div Class="col-sm-4">
                                                            @Html.Label("lblQDEFXAPMC", "Quantidade de Faixa:")
                                                            <br />
                                                            @Html.TextBoxFor(Function(Model) Model.QDEFXAPMC, New With {.class = "nput-sm date-interval form-control", .id = "IdQtdFaixa", .readonly = "readonly"})
                                                        </div>
                                                End Select
                            <div class="col-sm-4">
                                @Html.Label("lblINDPTCTOTMIXPMC", "Participação Mix Total:")
                                <br />
                                @*@Html.TextBoxFor(Function(Model) Model.INDPTCTOTMIXPMC, New With {.Class = "input-sm date-interval form-control", .readonly = "readonly"})*@
                                <div class="row">
                                    <div class="col-sm-3">
                                        @Html.RadioButtonFor(Function(Model) Model.INDPTCTOTMIXPMC, Constantes.SIM, New With {.id = "radioParticipacaoMixSim", .readonly = "readonly"})
                                        @Html.Label("sim", "Sim")
                                    </div>
                                    <div class="col-sm-3">
                                        @Html.RadioButtonFor(Function(Model) Model.INDPTCTOTMIXPMC, Constantes.NAO, New With {.id = "radioParticipacaoMixSim", .readonly = "readonly"})
                                        @Html.Label("nao", "Não")
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                @Html.Label("lblDESCMCPMC:", "Mecanica:")
                                <br />
                                @Html.TextAreaFor(Function(Model) Model.DESCMCPMC, New With {.Class = "input-sm Date-interval form-control", .TextMode = "MultiLine", .Rows = "3", .readonly = "readonly"})
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        @Html.Label("lblCODUNDESRNGC", "BU (*):")
                        @Html.DropDownListFor(Function(Model) Model.CODUNDESRNGC, New SelectList(ViewBag.listaBU, "CODUNDESRNGC", "DESUNDESRNGC", Model.CODFILEMPORIVBA), "Selecione...", New With {.Class = "form-control chosen-select"}.EnableIf(Model.TIPSITPMCBoll))
                        @Html.ValidationMessageFor(Function(m) m.CODUNDESRNGC, "", New With {.class = "text-danger-message-login"})
                        @Html.HiddenFor(Function(Model) Model.CODUNDESRNGC, New With {.Class = "input-sm date-interval form-control"})
                    </div>
                    <div Class="col-sm-8">
                        @Html.Label("lblNOMPMC", "Nome Promoção (*):")
                        @Html.TextBoxFor(Function(Model) Model.NOMPMC, New With {.class = "input-sm date-interval form-control", .id = "NonPromocao"}.ReadonlyIf(Model.TIPSITPMCBoll))
                        @Html.ValidationMessageFor(Function(m) m.NOMPMC, "", New With {.class = "text-danger-message-login"})
                    </div>
                </div>
                <div Class="row">
                    <div class="col-sm-4">
                        <div Class="form-check" style="transform: translateY(105%);">
                            @Html.CheckBoxFor(Function(Model) Model.INDPMCIMDBool, New With {.Id = "ckbPromocaoRelampago", .class = "form-check-input", .disabled = "disabled"}.ReadonlyIf(Model.TIPSITPMCBoll))
                            <Label Class="form-check-label" for="INDPMCIMDBool">
                                Promoção Relâmpago
                            </Label>
                        </div>
                    </div>

                    @Code
                        If (Model.INDPMCIMD = 1) Then
                            @<div Class="col-sm-4">
                                @Html.Label("lblDATINIPMC", "Dt. Inicio( *):")
                                <div Class="input-daterange input-group">
                                    @Html.TextBoxFor(Function(Model) Model.HRAINIPMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataIni()", .id = "dataIni"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                    @Html.ValidationMessageFor(Function(m) m.HRAINIPMC, "", New With {.class = "text-danger-message-login"})
                                </div>
                            </div>

                            @<div Class="col-sm-4">
                                @Html.Label("lblDATFIMPMC", "Dt. Fim (*):")
                                <div Class="input-daterange input-group">
                                    @Html.TextBoxFor(Function(Model) Model.HRAFIMPMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataIni()", .id = "dataIni"})
                                    @Html.ValidationMessageFor(Function(m) m.HRAFIMPMC, "", New With {.class = "text-danger-message-login"})
                                </div>
                            </div>
                        Else
                            @<div Class="col-sm-4">
                                @Html.Label("lblDATINIPMC", "Dt. Inicio( *):")
                                <div Class="input-daterange input-group">
                                    @Html.TextBoxFor(Function(Model) Model.DATINIPMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataIni()", .id = "dataIni"}.ReadonlyIf(Model.TIPSITPMCBoll))
                                    @Html.ValidationMessageFor(Function(m) m.DATINIPMC, "", New With {.class = "text-danger-message-login"})
                                </div>
                            </div>
                            @<div Class="col-sm-4">
                                @Html.Label("lblDATFIMPMC", "Dt. Fim (*):")
                                <div Class="input-daterange input-group">
                                    @Html.TextBoxFor(Function(Model) Model.DATFIMPMC, New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataIni()", .id = "dataIni"})
                                    @Html.ValidationMessageFor(Function(m) m.DATFIMPMC, "", New With {.class = "text-danger-message-login"})
                                </div>
                            </div>
                        End If
                    End Code
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        @Html.Label("lblFLGENVPMC", "Status Envio:")
                        @Html.DropDownListFor(Function(Model) Model.FLGENVPMC, New SelectList(ViewBag.listaStatusEnvio, "VALUE", "TEXT", Model.FLGENVPMC), New With {.Class = "form-control", .id = "dropStatus"})
                    </div>

                    <div class="col-sm-4">
                        @code
                            Dim tipoEquipeVendas = New List(Of SelectListItem)()
                            tipoEquipeVendas.Add(New SelectListItem() With {.Text = "Ambos", .Value = "0"})
                            tipoEquipeVendas.Add(New SelectListItem() With {.Text = "Martins", .Value = "1"})
                            tipoEquipeVendas.Add(New SelectListItem() With {.Text = "Almais", .Value = "2"})
                        End Code
                        @Html.Label("TIPEQIVND", "Equipe de Vendas (*):")
                        @Html.DropDownListFor(Function(Model) Model.TIPEQIVND, New SelectList(tipoEquipeVendas, "VALUE", "TEXT", Model.TIPEQIVND), New With {.Class = "form-control"}.EnableIf(Model.TIPSITPMCBoll))

                    </div>
                    <div Class="col-sm-4">
                        @Html.Label("lblQDEMAXBDECLI", "Quant. Máx. de brinde por cliente:")
                        @Html.TextBoxFor(Function(Model) Model.QDEMAXBDECLI, New With {.class = "input-sm form-control", .id = "qtdMaxBrindPromocao", .alt = "inteiro_positivo", .maxlength = 6, .readonly = "readonly"})
                    </div>
                </div>
                @If (Model.TIPPMC <> 1) Then
                    @<div class="row">
                        @*<div Class="col-sm-3">
                                <Label>
                                    @Html.CheckBoxFor(Function(Model) Model.INDCTRADIAVLPMCBool, New With {.Id = "ckbCrterioAdicional"}.EnableIf(Model.TIPSITPMCBoll))
                                    Critério Adicional
                                </Label>
                            </div>*@
                        <div Class="col-sm-6">
                            @Html.Label("vendaAvaliacaoPmc", "Venda Avaliação Promoção Fator Cresc.:")
                            @Html.TextBoxFor(Function(Model) Model.FTRACRCTRAVLPMC, New With {.class = "input-sm form-control", .id = "qtdMaxBrindPromocao", .alt = "inteiro_positivo", .maxlength = 6}.ReadonlyIf(Model.TIPSITPMCBoll))
                        </div>
                        <div Class="col-sm-3">
                            @Html.Label("lblValorLimite", "Valor Limite:")
                            @Html.TextBoxFor(Function(Model) Model.VLRLIMMAXCTRAVLPMC, New With {.class = "input-sm form-control", .id = "qtdMaxBrindPromocao", .alt = "inteiro_positivo", .maxlength = 6}.ReadonlyIf(Model.TIPSITPMCBoll))
                        </div>
                        <div Class="col-sm-3">
                            @Html.Label("qtdMin", "Quantidade Mínima:")
                            @Html.TextBoxFor(Function(Model) Model.QDEMNMITEPEDPMC, New With {.class = "input-sm form-control", .id = "qqtdMinPromocao", .alt = "inteiro_positivo", .maxlength = 6}.ReadonlyIf(Model.TIPSITPMCBoll))
                        </div>
                    </div>
                End If
                <br />
                <div Class="widget-box widget-color-wi">
                    <div Class="widget-header widget-header-small">
                        <h4 Class="widget-title smaller blue lighter">
                            Publico Alvo
                        </h4>
                    </div>
                    <div Class="widget-body">
                        <div Class="widget-main">
                            <div Class="row">
                                <div Class="col-sm-12">
                                    <span style="font-weight:bold"> Estado :     </span>
                                    @For Each item In ViewBag.promocaoPublicoAlvoEstadoNomes
                                        @<span>
                                            @item;
                                        </span>
                                    Next
                                </div>
                            </div>
                            <br />
                            <div Class="row">
                                <div Class="col-sm-12">
                                    <span style="font-weight:bold"> Atividade :    </span>
                                    @For Each item In ViewBag.promocaoPublicoAlvoAtividadeNomes
                                        @<span>
                                            @item;
                                        </span>
                                    Next
                                </div>
                            </div>
                            <br />
                            <div Class="row">
                                <div Class="col-sm-12">
                                    <span style="font-weight:bold"> Grupamento de Clientes:</span>
                                    @For Each item In ViewBag.promocaoPublicoAlvoGrupamentoClienteNomes
                                        @<span>
                                            @item;
                                        </span>
                                    Next
                                </div>
                            </div>
                            <br />
                            <div Class="row">
                                <div Class="col-sm-12">
                                    <span style="font-weight:bold"> Negocio de Clientes:</span>
                                    @For Each item In ViewBag.promocaoPublicoAlvoNegocioClienteNomes
                                        @<span>
                                            @item;
                                        </span>
                                    Next
                                </div>
                            </div>
                            <br />
                            <div Class="row">
                                <div Class="col-sm-12">
                                    <span style="font-weight:bold"> Segmento de Clientes:</span>
                                    @For Each item In ViewBag.promocaoPublicoAlvoSegmentoClienteNomes
                                        @<span>
                                            @item;
                                        </span>
                                    Next
                                </div>
                            </div>
                            <br />
                            <div Class="row">
                                <div Class="col-sm-12">
                                    <span style="font-weight:bold"> Gerente de Vendas:</span>
                                    @For Each item In ViewBag.promocaoPublicoAlvoGerenteVendasNomes
                                        @<span>
                                            @item;
                                        </span>
                                    Next
                                </div>
                            </div>
                            <br />
                            <div Class="row">
                                <div Class="col-sm-4">
                                    <span style="font-weight:bold">Quantidade Cliente :    </span>
                                    @ViewBag.publicoAlvoClienteQuantidade
                                </div>
                                <div Class="col-sm-4">
                                    <span style="font-weight:bold">Quantidade RCA / Vendedor :    </span>
                                    @ViewBag.publicoAlvoRcaVendedorQuantidade
                                </div>
                                <div Class="col-sm-4">
                                    <span style="font-weight:bold">Quantidade Gerente Mercado:</span>
                                    @ViewBag.publicoAlvoGerenteMercadoQuantidade
                                </div>
                            </div>
                            @If (ViewBag.isNacional) Then
                                @<br />
                                @<div Class="row">
                                    <div Class="col-sm-12">
                                        <span style="font-weight:bold"> Nacional :        </span>
                                        <span>sim</span>
                                    </div>
                                </div>
                            End If

                        </div>
                    </div>
                </div>
                <br />
                <div Class="widget-box widget-color-wi">
                    <div Class="widget-body">
                        <div Class="widget-main">

                            <div Class="row">
                                <div Class="col-sm-12">
                                    <span style="font-weight:bold"> Quantidade Mercadorias no MIX: </span> @ViewBag.TotalMix
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


