﻿@ModelType  List(Of PromocaoPublicoAlvoEstadoViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-lg hideScroll" id="modalListPromocaoEstadoUniao" style="z-index: 1051; margin-top: 4px;">
    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Estado
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgmodalListPromocaoEstadoUniao">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div>
                <Table id="tblModalPromocaoEstadoUniao" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="center">
                                @Html.CheckBox("chkEstados", New With {.onclick = "marcarOuDesmarcarTodosEstados()", .Id = "chkTodosEstados"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoEstadoUni))
                            </th>
                            <th>
                                Sigla
                            </th>
                            <th>
                                Nome
                            </th>
                            <th>
                                Reg.
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @for Each item In Model
                            @Html.HiddenFor(Function(r) item.CODPMC, New With {.id = "hdfCODACOCMC"})
                            @Html.HiddenFor(Function(r) item.TIPEDEPUBALVPMC, New With {.id = "hdfNUMSEQRGRACOCMC"})
                            @<tr Class="trResultado">
                                @if item.CODPMC IsNot Nothing AndAlso item.TIPEDEPUBALVPMC IsNot Nothing Then
                                    @<td Class="center">
                                        @If (ViewBag.SituacaoPromocao = "Liberada" Or ViewBag.SituacaoPromocao = "Encerrada") Then
                                            @Html.CheckBox("SelecioneEstadoUniao", New With {.class = "marcaEstado", .checked = "checked"}.EnableIf(False))
                                        Else
                                            @Html.CheckBox("SelecioneEstadoUniao", New With {.class = "marcaEstado", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoEstadoUni))
                                        End If
                                    </td>
                                Else
                                    @<td Class="center">
                                        @Html.CheckBox("SelecioneEstadoUniao", New With {.class = "marcaEstado"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoEstadoUni))
                                    </td>
                                End If
                                <td>
                                    @Html.Label(item.CODESTUNI)
                                </td>
                                <td>
                                    @Html.Label(item.NOMESTUNI)
                                </td>
                                <td>
                                    @Html.Label(item.CODDIVREG)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </Table>
            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                @if (ViewBag.PermiteEdicaoPublicoAlvoPromocaoEstadoUni) Then
                    @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosGridEstadoUniao();">
                        <i Class="ace-icon fa fa-save bigger-120 green"></i>
                        Salvar
                    </Button>
                End If
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalPromocaoEstadoUni();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>

