﻿@ModelType PromocaoPublicoAlvoGerenteMercadoViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-full hideScroll" id="modalListPromocaoGerenteMercado" style="z-index: 1051; margin-top: 4px;">

    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp; Gerente Mercado
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalPromocaoGerenteMercado">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div class="row">
                <div class="col-xs-6">
                    @if (ViewBag.PermiteEdicaoPublicoAlvoPromocaoGerenteMercado) Then
                        @<div id="divLoadPesquisaPromocaoGerenteMercado">
                        </div>
                        Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsGerenteMercado", .Controller = "PromocaoPublicoAlvoGerenteMercado", .EnderecoTemplate = "PublicoAlvoGerenteMercado.xls"})
                    End If
                </div>

                @if (ViewBag.PermiteEdicaoPublicoAlvoPromocaoGerenteMercado) Then
                    @<div class="col-xs-6" id="divLoadPromocaoPublicoAlvoGerenteMercado">

                    </div>
                Else
                    @<div class="col-xs-12" id="divLoadPromocaoPublicoAlvoGerenteMercado">

                    </div>
                End If

            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalGerenteMercado();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>