﻿@ModelType GridSettings(Of PromocaoPublicoAlvoGerenteMercadoViewModel)
@Imports Martins.AcoesMercadologicas.Core

@code
    Dim ajaxOptions As New AjaxOptions() With {
        .UpdateTargetId = "divGridPesquisaPromocaoPublicoAlvoGerenteMercado",
        .InsertionMode = InsertionMode.Replace,
        .OnBegin = "LoadingOn",
        .OnComplete = "LoadingOff_UpdateMessage",
        .HttpMethod = "POST"
        }

End Code

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Pesquisa Gerente Mercado
                </h4>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divResultadoPromocaoPesquisaGerenteMercado">

                        @Using (Ajax.BeginForm("PesquisaMecanicaPromocaoWidgetLoadPesquisaGerenteMercado", "PromocaoPublicoAlvoGerenteMercado", ajaxOptions, New With {.id = "frmPesquisaPromocaoPublicoAlvoGerenteMercado"}))
                            @Html.AntiForgeryToken()

                            @Html.HiddenFor(Function(r) Model.Filter.CODPMC)
                            @Html.HiddenFor(Function(r) Model.Filter.TIPEDEPUBALVPMC)

                            @<div class="row">
                                 <div class="col-sm-6">
                                     @Html.Label("Gerente Vendas:")
                                     <div>
                                         @Html.TextBoxFor(Function(r) Model.Filter.CODGER, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                         @Html.TextBoxFor(Function(r) Model.Filter.NOMGER, New With {.class = "form-control", .style = "width: 80%;", .id = "txtNomGer"})
                                     </div>
                                 </div>
                                 <div class="col-sm-6">
                                     @Html.Label("Gerente Mercado:")
                                     <div>
                                         @Html.TextBoxFor(Function(r) Model.Filter.CODSUP, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                         @Html.TextBoxFor(Function(r) Model.Filter.NOMSUP, New With {.class = "form-control", .style = "width: 80%;", .id = "txtNomSup"})
                                     </div>
                                 </div>
                            </div>
                            @<br />
                            @<div class="row">
                                <div class="col-sm-3">
                                    <button type="submit" class="btn btn-white btn-info">
                                        <i class="ace-icon fa fa-search bigger-120 info"></i>
                                        Pesquisar
                                    </button>
                                </div>
                            </div>


                        End Using

                        <br />

                        <div id="divGridPesquisaPromocaoPublicoAlvoGerenteMercado">
                            @code
                                Html.RenderPartial("~/Views/Promocao/PublicoAlvo/GerenteMercado/_GridPesquisaPromocaoPublicoAlvoGerenteMercado.vbhtml", Model)
                            End Code
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
