﻿@ModelType List(Of PromocaoPublicoAlvoGerenteMercadoViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Gerente Mercado Selecionados
                </h4>
                <div class="widget-toolbar">
                    <span>Total: @Model.Count()</span>
                </div>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divResultadoPesquisaPromocaoGerenteMercado">
                        <Table id="tblResultadoPesquisaPromocaoGerenteMercado" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                            <thead>
                                <tr role="row">
                                    <th Class="center">
                                        @if (ViewBag.PermiteEdicaoPublicoAlvoPromocaoGerenteMercado And (ViewBag.SituacaoPromocao = "À Liberar" Or ViewBag.SituacaoPromocao = "")) Then
                                            @<button type="button" Class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeleteAllPromocaoPublicoAlvoGerenteMercado()" title="Excluir todos">
                                                <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </button>
                                        End If
                                    </th>
                                    <th>
                                        Código
                                    </th>
                                    <th>
                                        Nome
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @for Each item In Model
                                    @<tr Class="trResultado">
                                        <td class="center">
                                            @if (ViewBag.PermiteEdicaoPublicoAlvoPromocaoGerenteMercado And (ViewBag.SituacaoPromocao = "À Liberar" Or ViewBag.SituacaoPromocao = "")) Then
                                                @<button type="button" class="btn btn-danger btn-info" style="padding: 0px;" onclick="DeletePromocaoPublicoAlvoGerenteMercado(this)">
                                                    <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </button>
                                            Else
                                                @<button type="button" class="btn btn-danger btn-info" style="padding: 0px;" disabled="disabled" onclick="DeletePromocaoPublicoAlvoGerenteMercado(this)">
                                                    <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </button>
                                            End If
                                        </td>
                                        <td>
                                            @Html.DisplayFor(Function(r) item.CODSUP)
                                        </td>
                                        <td>
                                            @Html.DisplayFor(Function(r) item.NOMSUP)
                                        </td>
                                    </tr>
                                Next
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
