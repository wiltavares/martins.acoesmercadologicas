﻿@ModelType  List(Of PromocaoPublicoAlvoGerenteVendasViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-lg hideScroll" id="modalPromocaoListGerenteVendas" style="z-index: 1051; margin-top: 4px;">
    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Gerente Vendas
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalPromocaoGerenteVendas">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div>
                <Table id="tblModalPromocaoGerenteVendas" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="center">
                                @Html.CheckBox("chkGerenteVendas", New With {.onclick = "marcarOuDesmarcarTodasGerenteVendas()", .Id = "chkTodosGerenteVendas", .title = "Selecionar Todos"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoGerenteVendas))
                            </th>
                            <th>
                                Código
                            </th>
                            <th>
                                Nome
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @Html.HiddenFor(Function(r) item.CODPMC, New With {.id = "hdfCODPMC"})
                            @Html.HiddenFor(Function(r) item.TIPEDEPUBALVPMC, New With {.id = "hdfTIPEDEPUBALVPMC"})
                            @<tr Class="trResultado">
                                @if item.CODPMC IsNot Nothing Then
                                    @<td Class="center">
                                        @If (ViewBag.SituacaoPromocao = "Liberada" Or ViewBag.SituacaoPromocao = "Encerrada") Then
                                            @Html.CheckBox("SelecioneGerenteVendas", New With {.class = "marcaGerenteVendas", .checked = "checked"}.EnableIf(False))
                                        Else
                                            @Html.CheckBox("SelecioneGerenteVendas", New With {.class = "marcaGerenteVendas", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoGerenteVendas))
                                        End If
                                    </td>
                                Else
                                    @<td Class="center">
                                        @Html.CheckBox("SelecioneGerenteVendas", New With {.class = "marcaGerenteVendas"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoGerenteVendas))
                                    </td>
                                End If
                                <td>
                                    @Html.Label(item.CODGER)
                                </td>
                                <td>
                                    @Html.Label(item.NOMGER)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </Table>
            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                @if (ViewBag.PermiteEdicaoPublicoAlvoPromocaoGerenteVendas) Then
                    @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosGridPromocaoGerenteVendas();">
                        <i Class="ace-icon fa fa-save bigger-120 green"></i>
                        Salvar
                    </Button>
                End If
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalPromocaoGerenteVendas();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>

