﻿@ModelType  List(Of PromocaoPublicoAlvoAtividadeViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-lg hideScroll" id="modalListPromocaoAtividade" style="z-index: 1051; margin-top: 4px;">
    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Atividade
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">

            <div id="msgmodalListPromocaoAtividade">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>

            @If (ViewBag.PermiteEdicaoPublicoAlvoPromocaoAtividade) Then

                @<div class="widget-box widget-color-blue collapsed">
                    <div class="widget-header widget-header-small">
                        <h4 Class="widget-title smaller white lighter">
                            <i Class="ace-icon fa fa-check-square-o bigger-110"></i>
                            Selecionar Por BU
                        </h4>

                        <div class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                            </a>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div Class="row">
                                @for Each item In ViewBag.TipoAtividade
                                    @<div Class="col-sm-4">
                                        <input id="idCheckteste" class="chkTipoAtividade" type="checkbox" value="@item.CODUNDESRNGC" />
                                        @Html.Label(item.DESUNDESRNGC)
                                    </div>
                                Next
                                <button type="button" class="btn btn-xs btn-primary  btn-primary " style="float: right; margin-right:12px; " title="Aplicar" onclick="PromocaoCarregarBuSelecionadoAtividade()">
                                    <i class="ace-icon fa fa-check-square-o bigger-120 info"></i>
                                    Aplicar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            End If


            <div>
                <Table id="tblModalPromocaoAtividade" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="center">
                                @Html.CheckBox("chkAtividades", New With {.onclick = "marcarOuDesmarcarTodasAtividades()", .Id = "chkTodasAtividades", .title = "Selecionar Todos"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoAtividade))
                            </th>
                            <th>
                                Código
                            </th>
                            <th>
                                Nome
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @Html.HiddenFor(Function(r) item.CODPMC, New With {.id = "hdfCODACOCMC"})
                            @Html.HiddenFor(Function(r) item.TIPEDEPUBALVPMC, New With {.id = "hdfNUMSEQRGRACOCMC"})
                            @<tr Class="trResultado">
                                @if item.CODPMC IsNot Nothing AndAlso item.TIPEDEPUBALVPMC IsNot Nothing Then
                                    @<td class="center">
                                        @Html.HiddenFor(Function(r) item.CODUNDESRNGC, New With {.class = "hdfAtividade"})
                                        @If (ViewBag.SituacaoPromocao = "Liberada" Or ViewBag.SituacaoPromocao = "Encerrada") Then
                                            @Html.CheckBox("SelecioneAtividade", New With {.class = "marcaAtividade", .checked = "checked"}.EnableIf(False))
                                        Else
                                            @Html.CheckBox("SelecioneAtividade", New With {.class = "marcaAtividade", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoAtividade))
                                        End If
                                    </td>
                                Else
                                    @<td class="center">
                                        @Html.HiddenFor(Function(r) item.CODUNDESRNGC, New With {.class = "hdfAtividade"})
                                        @Html.CheckBox("SelecioneAtividade", New With {.class = "marcaAtividade"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoAtividade))
                                    </td>
                                End If


                                <td class="CodAti">
                                    @Html.Label(item.CODATI)
                                </td>
                                <td>
                                    @Html.Label("NomeAtividade", item.NOMATI)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </Table>
            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                @If (ViewBag.PermiteEdicaoPublicoAlvoPromocaoAtividade) Then
                    @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosGridPromocaoAtividade();">
                        <i Class="ace-icon fa fa-save bigger-120 green"></i>
                        Salvar
                    </Button>
                End If
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalPromocaoAtividade();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>
