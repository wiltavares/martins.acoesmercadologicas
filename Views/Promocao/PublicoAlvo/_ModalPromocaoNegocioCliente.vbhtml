﻿@ModelType  List(Of PromocaoPublicoAlvoNegocioClienteViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-lg hideScroll" id="modalListPromocaoNegocioCliente" style="z-index: 1051; margin-top: 4px;">

    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Negocio Clientes
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalPromocaoNegocioCliente">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div>
                <Table id="tblModalPromocaoNegocioCliente" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="center">
                                @Html.CheckBox("chkNegocioCliente", New With {.onclick = "marcarOuDesmarcarTodasNegocioCliente()", .Id = "chkTodosNegocioCliente", .title = "Selecionar Todos"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoNegocioCliente))
                            </th>
                            <th>
                                Código
                            </th>
                            <th>
                                Nome
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @Html.HiddenFor(Function(r) item.CODPMC, New With {.id = "hdfCODPMC"})
                            @Html.HiddenFor(Function(r) item.TIPEDEPUBALVPMC, New With {.id = "hdfTIPEDEPUBALVPMC"})
                            @<tr Class="trResultado">
                                @If item.CODPMC IsNot Nothing AndAlso item.TIPEDEPUBALVPMC IsNot Nothing Then
                                    @<td class="center">
                                        @Html.HiddenFor(Function(r) item.CODUNDESRNGC, New With {.class = "hdfNegocioCliente"})
                                        @If (ViewBag.SituacaoPromocao = "Liberada" Or ViewBag.SituacaoPromocao = "Encerrada") Then
                                            @Html.CheckBox("SelecioneNegocioCliente", New With {.class = "marcaNegocioCliente", .checked = "checked"}.EnableIf(False))
                                        Else
                                            @Html.CheckBox("SelecioneNegocioCliente", New With {.class = "marcaNegocioCliente", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoNegocioCliente))
                                        End If
                                    </td>
                                Else
                                    @<td class="center">
                                        @Html.HiddenFor(Function(r) item.CODUNDESRNGC, New With {.class = "hdfNegocioCliente"})
                                        @Html.CheckBox("SelecioneNegocioCliente", New With {.class = "marcaNegocioCliente"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoNegocioCliente))
                                    </td>
                                End If

                                <td>
                                    @Html.Label(item.CODUNDESRNGC)
                                </td>
                                <td>
                                    @Html.Label("NomeNegocioCliente", item.DESUNDESRNGC)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </Table>
            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                @if (ViewBag.PermiteEdicaoPublicoAlvoPromocaoNegocioCliente) Then
                    @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosGridPromocaoNegocioCliente();">
                        <i Class="ace-icon fa fa-save bigger-120 green"></i>
                        Salvar
                    </Button>
                End If
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalPromocaoNegocioCliente();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>
