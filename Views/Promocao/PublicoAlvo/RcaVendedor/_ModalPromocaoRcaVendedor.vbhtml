﻿@ModelType PromocaoPublicoAlvoRcaVendedorViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-full hideScroll" id="modalListPromocaoRcaVendedor" style="z-index: 1051; margin-top: 4px;">

    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp; Rca/Vendedor
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalPromocaoRcaVendedor">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div class="row">
                <div class="col-xs-6">
                    @if (ViewBag.PermiteEdicaoPublicoAlvoPromocaoRcaVendedor) Then
                        @<div id="divLoadPesquisaPromocaoRcaVendedor">
                        </div>
                        Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsRcaVendedor", .Controller = "PromocaoPublicoAlvoRcaVendedor", .EnderecoTemplate = "PublicoAlvoRcaVendedor.xls"})
                    End If
                </div>

                @If (ViewBag.PermiteEdicaoPublicoAlvoPromocaoRcaVendedor) Then
                    @<div class="col-xs-6" id="divLoadPromocaoPublicoAlvoRcaVendedor">

                    </div>
                Else
                    @<div class="col-xs-12" id="divLoadPromocaoPublicoAlvoRcaVendedor">

                    </div>
                End If
            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalRcaVendedor();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>