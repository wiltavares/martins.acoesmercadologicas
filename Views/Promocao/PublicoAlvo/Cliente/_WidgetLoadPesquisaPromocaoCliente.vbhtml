﻿@ModelType GridSettings(Of PromocaoPublicoAlvoClienteViewModel)
@Imports Martins.AcoesMercadologicas.Core

@code
    Dim ajaxOptions As New AjaxOptions() With {
        .UpdateTargetId = "divGridItensPesquisaPromocaoPublicoAlvoCliente",
        .InsertionMode = InsertionMode.Replace,
        .OnBegin = "LoadingOn",
        .OnComplete = "ModalClientesLoadingOff_UpdateMessage",
        .HttpMethod = "POST"
        }

End Code

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Pesquisa Cliente
                </h4>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divResultadoPesquisaPromocaoCliente">
                        @Using (Ajax.BeginForm("PesquisaMecanicaPromocaoWidgetLoadPesquisaCliente", "PromocaoPublicoAlvoCliente", ajaxOptions, New With {.id = "frmPesquisaPromocaoPublicoAlvoCliente"}))
                            @Html.AntiForgeryToken()

                            @Html.HiddenFor(Function(r) Model.Filter.CODPMC)
                            @Html.HiddenFor(Function(r) Model.Filter.TIPEDEPUBALVPMC)

                            @<div class="row">
                                <div Class="col-sm-6">
                                    @Html.Label("Cliente:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODCLI, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.NOMCLI, New With {.class = "form-control", .style = "width: 80%;", .id = "txtNomCli"})
                                    </div>
                                </div>
                                <div Class="col-sm-6">
                                    @Html.Label("Representante Vendas:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODREP, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.NOMREP, New With {.class = "form-control", .style = "width: 80%;", .id = "txtNomRep"})
                                    </div>
                                </div>
                            </div>
                            @<div Class="row">
                                @*<div Class="col-sm-6">
                                    @Html.Label("Gerente Vendas:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODGER, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.NOMGER, New With {.class = "form-control", .style = "width: 80%;", .id = "txtNomGer"})
                                    </div>
                                </div>*@
                                <div Class="col-sm-6">
                                    @Html.Label("Gerente Mercado:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODSUP, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.NOMSUP, New With {.class = "form-control", .style = "width: 80%;", .id = "txtNomSup"})
                                    </div>
                                </div>
                            </div>
                            @<br />
                            @<div Class="row">
                                <div Class="col-sm-3">
                                    <Button type="submit" Class="btn btn-white btn-info">
                                        <i Class="ace-icon fa fa-search bigger-120 info"></i>
                                        Pesquisar
                                    </Button>
                                </div>
                            </div>
                        End Using
                        <br />
                        <div id="divGridItensPesquisaPromocaoPublicoAlvoCliente">
                            @code
                                Html.RenderPartial("~/Views/Promocao/PublicoAlvo/Cliente/_GridPesquisaPromocaoPublicoAlvoCliente.vbhtml", Model)
                            End Code
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
