﻿@ModelType  PromocaoPublicoAlvoClienteViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-full hideScroll" id="modalListPromocaoCliente" style="z-index: 1051; margin-top: 4px;">

    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp; Clientes
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalPromocaoCliente">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div class="row">
                <div class="col-xs-6">
                    @if (ViewBag.PermiteEdicaoPublicoAlvoPromocaoCliente) Then
                        @<div id="divLoadPesquisaPromocaoCliente">
                        </div>
                        Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsCliente", .Controller = "PromocaoPublicoAlvoCliente", .EnderecoTemplate = "PublicoAlvoCliente.xls"})
                    End If

                </div>

                @if (ViewBag.PermiteEdicaoPublicoAlvoPromocaoCliente) Then
                    @<div Class="col-xs-6" id="divLoadPromocaoPublicoAlvoCliente">

                    </div>
                Else
                    @<div Class="col-xs-12" id="divLoadPromocaoPublicoAlvoCliente">

                    </div>
                End If

            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalCliente();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>
