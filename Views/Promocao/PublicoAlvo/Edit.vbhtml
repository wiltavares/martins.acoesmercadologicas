﻿@ModelType List(Of PromocaoPublicoAlvoSelecaoViewModel)
@Imports Martins.AcoesMercadologicas.Core


@code
    Dim PermiteEdicao As Boolean = False
    Dim authCookie As HttpCookie = Context.Request.Cookies(FormsAuthentication.FormsCookieName)
    If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
        Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)

        Dim roles As String() = authTicket.UserData.Split("|")(1).Split(";"c)
        If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
            PermiteEdicao = True
        End If
    End If


    If (PermiteEdicao) Then
        PermiteEdicao = ViewBag.PermiteEdicaoPublicoAlvoPromocao
    End If

End Code
<div class="widget-box widget-color-wi">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller blue lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Publico Alvo
        </h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>
        @*<div class="widget-toolbar" id="editBrindes">
                <a onclick="editCancelBlock('Brindes', this)">
                    <i class="1 ace-icon fa fa-edit bigger-125"></i>
                </a>
            </div>*@
    </div>
    <div class="widget-body">
        <div class="widget-main" id="widgetPublicoAlvo">
            <div id="divCreatePublicoAlvo">
                <div id="msgModalPublicoAlvo">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div style="width: auto; overflow-x: scroll;">
                    <table class="table table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                        <thead>

                        </thead>
                        <tbody>
                            <tr style="background-color: #f5f5f5;">
                                <td colspan="2">
                                    <b>Região</b>
                                </td>
                            </tr>
                            <tr>
                                @If (Model.Where(Function(f) f.CODCTRSELPUBALVPMC = 1).Count() = 1) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalPromocaoEstadoUniao()">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurado
                                        </Button>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @if (PermiteEdicao) Then
                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalPromocaoEstadoUniao()">
                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                Selecionar
                                            </Button>
                                        End If
                                    </td>
                                End If

                            <td>
                                Estado
                            </td>
                            </tr>
                            <tr style="background-color: #f5f5f5;">
                                <td colspan="2">
                                    <b>Clientes</b>
                                </td>
                            </tr>
                            <tr>
                                @If (Model.Where(Function(f) f.CODCTRSELPUBALVPMC = 4).Count() = 1) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalPromocaoAtividade()">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurado
                                        </Button>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @if (PermiteEdicao) Then
                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalPromocaoAtividade()">
                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                Selecionar
                                            </Button>
                                        End If
                                    </td>
                                End If

                            <td>
                                Atividade
                            </td>
                            </tr>
                            <tr>
                                @If (Model.Where(Function(f) f.CODCTRSELPUBALVPMC = 5).Count() = 1) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalPromocaoGrupamentoCliente()">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurado
                                        </Button>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @if (PermiteEdicao) Then
                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalPromocaoGrupamentoCliente()">
                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                Selecionar
                                            </Button>
                                        End If
                                    </td>
                                End If
                            <td>
                                Grupamento de Clientes
                            </td>
                            </tr>
                            <tr>

                                @If (Model.Where(Function(f) f.CODCTRSELPUBALVPMC = 7).Count() = 1) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalPromocaoNegocioCliente()">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurado
                                        </Button>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @if (PermiteEdicao) Then
                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalPromocaoNegocioCliente()">
                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                Selecionar
                                            </Button>
                                        End If
                                    </td>
                                End If
                            <td>
                                Negocio de Clientes
                            </td>
                            </tr>
                            <tr>
                                @If (Model.Where(Function(f) f.CODCTRSELPUBALVPMC = 8).Count() = 1) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalPromocaoSegmentoCliente()">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurado
                                        </Button>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @if (PermiteEdicao) Then
                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalPromocaoSegmentoCliente()">
                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                Selecionar
                                            </Button>
                                        End If
                                    </td>
                                End If
                            <td>
                                Segmento de Clientes
                            </td>
                            </tr>
                            <tr>
                                @If (Model.Where(Function(f) f.CODCTRSELPUBALVPMC = 9).Count() = 1) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalPromocaoCliente()">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurado
                                        </Button>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @if (PermiteEdicao) Then
                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalPromocaoCliente()">
                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                Selecionar
                                            </Button>
                                        End If
                                    </td>
                                End If
                            <td>
                                Cliente
                            </td>
                            </tr>
                            <tr style="background-color: #f5f5f5;">
                                <td colspan="2">
                                    <b>Força de Vendas</b>
                                </td>
                            </tr>
                            <tr>
                                @If (Model.Where(Function(f) f.CODCTRSELPUBALVPMC = 10).Count() = 1) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalPromocaoRcaVendedor()">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurado
                                        </Button>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @if (PermiteEdicao) Then
                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalPromocaoRcaVendedor()">
                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                Selecionar
                                            </Button>
                                        End If
                                    </td>
                                End If
                            <td>
                                RCA/Vendedor
                            </td>
                            </tr>
                            <tr>
                                @If (Model.Where(Function(f) f.CODCTRSELPUBALVPMC = 2).Count() = 1) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalPromocaoGerenteVendas()">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurado
                                        </Button>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @if (PermiteEdicao) Then
                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalPromocaoGerenteVendas()">
                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                Selecionar
                                            </Button>
                                        End If
                                    </td>
                                End If
                            <td>
                                Gerente de Vendas
                            </td>
                            </tr>
                            <tr>
                                @If (Model.Where(Function(f) f.CODCTRSELPUBALVPMC = 3).Count() = 1) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalPromocaoGerenteMercado()">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurado
                                        </Button>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @if (PermiteEdicao) Then
                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalPromocaoGerenteMercado()">
                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                Selecionar
                                            </Button>
                                        End If
                                    </td>
                                End If
                                <td>
                                    Gerente Mercado
                                </td>
                            </tr>
                            <tr style="background-color: #f5f5f5;">
                                <td colspan="2">
                                    <b>Nacional</b>
                                </td>
                            </tr>
                            <tr>
                                @if (Model.Count = 0) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-success" type="button">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Selecionado
                                        </Button>
                                    </td>
                                Else
                                    @<td></td>
                                End If
                                <td>
                                    Nacional
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div id="divListPublicoAlvoResultadosView">

            </div>

        </div>
    </div>

</div>

