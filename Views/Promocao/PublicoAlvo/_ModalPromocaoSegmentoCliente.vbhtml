﻿@ModelType  List(Of PromocaoPublicoAlvoSegmentoClienteViewModel)
@Imports Martins.AcoesMercadologicas.Core


<div Class="modal-dialog modal-lg hideScroll" id="modalPromocaoListSegmentoCliente" style="z-index: 1051; margin-top: 4px;">

    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp;Segmento Clientes
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalPromocaoSegmentoCliente">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div>
                <Table id="tblModalPromocaoSegmentoCliente" Class="table table-striped table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="center">
                                @Html.CheckBox("chkSegmentoCliente", New With {.onclick = "marcarOuDesmarcarTodasSeguimentoCliente()", .Id = "chkTodosSegmentoCliente", .title = "Selecionar Todos"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoSegmentoCliente))
                            </th>
                            <th>
                                Código
                            </th>
                            <th>
                                Nome
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model
                            @Html.HiddenFor(Function(r) item.CODPMC, New With {.id = "hdfCODPMC"})
                            @Html.HiddenFor(Function(r) item.TIPEDEPUBALVPMC, New With {.id = "hdfTIPEDEPUBALVPMC"})
                            @<tr Class="trResultado">

                                @If item.CODPMC IsNot Nothing AndAlso item.TIPEDEPUBALVPMC IsNot Nothing Then
                                    @<td class="center">
                                        @Html.HiddenFor(Function(r) item.CODSGMNGCCLI, New With {.class = "hdfSegmentoCliente"})
                                        @If (ViewBag.SituacaoPromocao = "Liberada" Or ViewBag.SituacaoPromocao = "Encerrada") Then
                                            @Html.CheckBox("SelecioneSegmentoCliente", New With {.class = "marcaSegmentoCliente", .checked = "checked"}.EnableIf(False))
                                        Else
                                            @Html.CheckBox("SelecioneSegmentoCliente", New With {.class = "marcaSegmentoCliente", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoSegmentoCliente))
                                        End If
                                    </td>
                                Else
                                    @<td class="center">
                                        @Html.HiddenFor(Function(r) item.CODSGMNGCCLI, New With {.class = "hdfSegmentoCliente"})
                                        @Html.CheckBox("SelecioneSegmentoCliente", New With {.class = "marcaSegmentoCliente"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoSegmentoCliente))
                                    </td>
                                End If


                                <td>
                                    @Html.Label(item.CODSGMNGCCLI)
                                </td>
                                <td>
                                    @Html.Label("NomeSegmentoCliente", item.DESSGMNGCCLI)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </Table>
            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                @if (ViewBag.PermiteEdicaoPublicoAlvoPromocaoSegmentoCliente) Then
                    @<Button type="submit" Class="btn btn-white btn-success" onclick="salvarDadosGridPromocaoSegmentoCliente();">
                        <i Class="ace-icon fa fa-save bigger-120 green"></i>
                        Salvar
                    </Button>
                End If
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalPromocaoSegmentoCliente();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>

