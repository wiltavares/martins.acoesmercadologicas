﻿@ModelType  GridSettings(Of PromocaoPublicoAlvoGrupamentoClienteViewModel)
@Imports Martins.AcoesMercadologicas.Core

<br class="clearfix" />
<div class="row">
    <div Class="btn-group pull-left">
        <Button Class="btn btn-white btn-info" onclick="marcarOuDesmarcarTodasGrupamentoCliente();">
            <i Class="ace-icon fa fa-check-square-o bigger-120 blue"></i>
            Selecionar Todos
        </Button>
    </div>
</div>
<div class="row">
    <div style="width: auto; overflow-x: scroll;">
        <Table id="tblModalPromocaoGrupamentoCliente" Class="table table-striped table-bordered table-hover dataTable" role="grid">
            <thead>
                <tr role="row">
                    <th class="center @Grid.AtribuirIconeOrdenacao("CODPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODPMC", Model.Page.OrderBy, Model.ResultID)">
                        @*@Html.CheckBox("chkGrupamentoCliente", New With {.onclick = "marcarOuDesmarcarTodasGrupamentoCliente()", .Id = "chkTodosGrupamentoCliente", .title = "Selecionar Todos"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoGrupamentoCliente))*@
                        <input id="chkTodosGrupamentoCliente" name="chkTodosGrupamentoCliente" type="hidden" value="0">
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("CODGRPCLI", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODGRPCLI", Model.Page.OrderBy, Model.ResultID)">
                        Código
                    </th>
                    <th class="@Grid.AtribuirIconeOrdenacao("DESGRPCLI", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESGRPCLI", Model.Page.OrderBy, Model.ResultID)">
                        Nome
                    </th>
                </tr>
            </thead>
            <tbody>
                @For Each item In Model.ListPaged
                    @Html.HiddenFor(Function(r) item.CODPMC, New With {.id = "hdfCODPMC"})
                    @Html.HiddenFor(Function(r) item.TIPEDEPUBALVPMC, New With {.id = "hdfTIPEDEPUBALVPMC"})
                    @<tr Class="trResultado">
                        @if item.CODPMC IsNot Nothing AndAlso item.TIPEDEPUBALVPMC IsNot Nothing Then
                            @<td Class="center">
                                @Html.HiddenFor(Function(r) item.CODUNDESRNGC, New With {.class = "hdfGrupamentoCliente"})
                                @If (ViewBag.SituacaoPromocao = "Liberada" Or ViewBag.SituacaoPromocao = "Encerrada") Then
                                    @Html.CheckBox("SelecioneGrupamentoCliente", New With {.class = "marcaGrupamentoCliente", .checked = "checked"}.EnableIf(False))
                                Else
                                    @Html.CheckBox("SelecioneGrupamentoCliente", New With {.class = "marcaGrupamentoCliente", .checked = "checked"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoGrupamentoCliente))
                                End If
                            </td>
                        Else
                            @<td Class="center">
                                @Html.HiddenFor(Function(r) item.CODUNDESRNGC, New With {.class = "hdfGrupamentoCliente"})
                                @Html.CheckBox("SelecioneGrupamentoCliente", New With {.class = "marcaGrupamentoCliente"}.EnableIf(ViewBag.PermiteEdicaoPublicoAlvoPromocaoGrupamentoCliente))
                            </td>
                        End If

                        <td class="CodGrpCli">
                            @Html.Label(item.CODGRPCLI)
                        </td>
                        <td>
                            @Html.Label("NomeGrupamentoCliente", item.DESGRPCLI)
                        </td>
                    </tr>
                Next
            </tbody>
        </Table>
    </div>
</div>
<br class="clearfix" />
<div style="display:none">
    @Code
        Html.RenderPartial(Constantes.VIEW_PAGINACAO)
    End Code
</div>