﻿@ModelType List(Of PromocaoClientesExcludentesViewModel)
@Imports Martins.AcoesMercadologicas.Core


@code
    Dim PermiteEdicao As Boolean = False
    Dim authCookie As HttpCookie = Context.Request.Cookies(FormsAuthentication.FormsCookieName)
    If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
        Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)

        Dim roles As String() = authTicket.UserData.Split("|")(1).Split(";"c)
        If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
            PermiteEdicao = True
        End If
    End If


    If (PermiteEdicao) Then
        PermiteEdicao = ViewBag.PermiteEdicaoClientesExcludentes
    End If

End Code
<div class="widget-box widget-color-wi">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller blue lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Clientes Excludentes
        </h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
            </a>
        </div>
        @*<div class="widget-toolbar" id="editBrindes">
                <a onclick="editCancelBlock('Brindes', this)">
                    <i class="1 ace-icon fa fa-edit bigger-125"></i>
                </a>
            </div>*@
    </div>
    <div class="widget-body">
        <div class="widget-main" id="widgetClientesExcludentes">
            <div id="divCreateClientesExcludentes">
                <div id="msgModalClientesExcludentes">
                    @Html.Partial(Constantes.VIEW_ALERTAS)
                </div>
                <div style="width: auto; overflow-x: scroll;">
                    <table class="table table-bordered table-hover dataTable" role="grid" style="min-width:500px">
                        <thead>

                        </thead>
                        <tbody>
                            <tr>
                                @If (Model.Any()) Then
                                    @<td Class="center">
                                        <Button Class="btn btn-xs btn-white btn-primary" type="button" onclick="abrirModalPromocaoClientesExcludentes()">
                                            <i Class="ace-icon fa fa-check-square-o bigger-119"></i>
                                            Configurado
                                        </Button>
                                    </td>
                                Else
                                    @<td Class="center">
                                        @if (PermiteEdicao) Then
                                            @<Button Class="btn btn-xs btn-white btn-warning" type="button" onclick="abrirModalPromocaoClientesExcludentes()">
                                                <i Class="ace-icon fa fa-square-o bigger-119"></i>
                                                Selecionar
                                            </Button>
                                        End If
                                    </td>
                                End If
                                <td>
                                    Clientes Excludentes
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div id="divListClientesExcludentesResultadosView">

            </div>

        </div>
    </div>

</div>

