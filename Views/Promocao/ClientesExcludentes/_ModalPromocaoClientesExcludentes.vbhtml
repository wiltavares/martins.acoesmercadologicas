﻿@ModelType  PromocaoClientesExcludentesViewModel
@Imports Martins.AcoesMercadologicas.Core

<div Class="modal-dialog modal-full hideScroll" id="modalListPromocaoClientesExcludentes" style="z-index: 1051; margin-top: 4px;">

    <div Class="modal-content">
        <div Class="modal-header ">
            <i Class="ace-icon fa fa-book bigger-120"></i>
            &nbsp; Clientes Excludentes
        </div>
        <div Class="modal-body" style="overflow-y: scroll !important;">
            <div id="msgModalPromocaoClientesExcludentes">
                @Html.Partial(Constantes.VIEW_ALERTAS)
            </div>
            <div class="row">
                <div class="col-xs-6">
                    @if (ViewBag.PermiteEdicaoPromocaoClientesExcludentes) Then
                        @<div id="divLoadPesquisaPromocaoClientesExcludentes">
                        </div>
                        Html.RenderPartial("~/Views/AcaoComercial/Mecanica/_WidgetImport.vbhtml", New ImportacaoXLSViewModelo With {.Action = "ImportarXlsClientesExcludentes", .Controller = "PromocaoClientesExcludentes", .EnderecoTemplate = "ClientesExcludentes.xls"})
                    End If

                </div>

                @if (ViewBag.PermiteEdicaoPromocaoClientesExcludentes) Then
                    @<div Class="col-xs-6" id="divLoadPromocaoClientesExcludentes">

                    </div>
                Else
                    @<div Class="col-xs-12" id="divLoadPromocaoClientesExcludentes">

                    </div>
                End If

            </div>
        </div>

        <div Class="modal-footer clearfix">
            <div Class="btn-group pull-right">
                <Button Class="btn btn-danger btn-white" type="button" onclick="fecharModalClientesExcludentes();">
                    <i Class="ace-icon fa fa-close bigger-120 red"></i>
                    Cancelar
                </Button>
            </div>
        </div>
    </div>
</div>
