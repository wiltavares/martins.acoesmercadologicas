﻿@ModelType GridSettings(Of PromocaoClientesExcludentesViewModel)
@Imports Martins.AcoesMercadologicas.Core

@code
    Dim ajaxOptions As New AjaxOptions() With {
        .UpdateTargetId = "divGridItensPesquisaPromocaoClientesExcludentes",
        .InsertionMode = InsertionMode.Replace,
        .OnBegin = "LoadingOn",
        .OnComplete = "ModalClientesEsxludentesLoadingOff_UpdateMessage",
        .HttpMethod = "POST"
        }

End Code

<div class="row">
    <div class="col-sm-12">
        <div Class="widget-box widget-color-blue2">
            <div Class="widget-header widget-header-small">
                <h4 Class="widget-title smaller white lighter">
                    <i Class="ace-icon fa fa-sliders bigger-110"></i>
                    Pesquisa Clientes Excludentes
                </h4>
            </div>
            <div Class="widget-body">
                <div Class="widget-main">
                    <div id="divResultadoPesquisaPromocaoClientesExcludentes">
                        @Using (Ajax.BeginForm("PesquisaMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes", "PromocaoClientesExcludentes", ajaxOptions, New With {.id = "frmPesquisaPromocaoClientesExcludentes"}))
                            @Html.AntiForgeryToken()

                            @Html.HiddenFor(Function(r) Model.Filter.CODPMC)

                            @<div class="row">
                                <div Class="col-sm-6">
                                    @Html.Label("Cliente:")
                                    <div>
                                        @Html.TextBoxFor(Function(r) Model.Filter.CODCLI, New With {.class = "form-control", .style = "width: 20%; float: left;"})
                                        @Html.TextBoxFor(Function(r) Model.Filter.NOMCLI, New With {.class = "form-control", .style = "width: 80%;", .id = "txtNomCli"})
                                    </div>
                                </div>
                            </div>
                            @<br />
                            @<div Class="row">
                                <div Class="col-sm-3">
                                    <Button type="submit" Class="btn btn-white btn-info">
                                        <i Class="ace-icon fa fa-search bigger-120 info"></i>
                                        Pesquisar
                                    </Button>
                                </div>
                            </div>
                        End Using
                        <br />
                        <div id="divGridItensPesquisaPromocaoClientesExcludentes">
                            @code
                                Html.RenderPartial("~/Views/Promocao/ClientesExcludentes/_GridPesquisaPromocaoClientesExcludentes.vbhtml", Model)
                            End Code
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
