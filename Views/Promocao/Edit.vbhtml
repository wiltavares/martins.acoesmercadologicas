﻿@ModelType PromocaoViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim tituloTela = IIf(Model Is Nothing OrElse Model.CODPMC Is Nothing, "Promoção", "Editar Promoção")
    ViewData("Title") = tituloTela
End Code

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="modal fade hideScroll" id="modalPromocao"></div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-file-text-o bigger-110"></i>
            Dados
        </h4>
    </div>

    <div id="EditBrindesView">
        @Code
            Html.RenderPartial("~/Views/Promocao/EditPromocao.vbhtml", Model)
        End Code
    </div>

    @Code
        Html.RenderPartial("~/Views/Promocao/CriterioApuracao/Edit.vbhtml", New PromocaoBrindeViewModel With {.CODPMC = Model.CODPMC})
    End Code

    @*<br class="clearfix" />*@
    @If (ViewBag.INDTIPPTOPMC = 0) Then
        Html.RenderPartial("~/Views/Promocao/Brinde/Edit.vbhtml", New PromocaoBrindeViewModel With {.CODPMC = Model.CODPMC})
    End If

    <div id="divWidgetPromocaoPublicoAlvo">

    </div>

    @*<div id="divWidgetPromocaoClientesExcludentes">

    </div>*@

    <div id="divWidgetPromocaoMix">

    </div>

    <div id="divWidgetPromocaoComunicacao">
        <div class="widget-box widget-color-wi">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller blue lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Comunicação
                </h4>
                <div class="widget-toolbar">
                    <div id="editComunicacao" class="btn-group">
                        <a onclick="editCancelBlock('Comunicacao', this)" class="btn btn-md btn-white btn-info">
                            <i class="ace-icon fa fa-edit bigger-125"></i>
                            Editar
                        </a>
                    </div>
                    <a href="#" data-action="collapse">
                        <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                    </a>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main" id="widgetComunicacao">
                </div>
            </div>
        </div>
    </div>



</div>

@section Scripts
    <script type="text/javascript" src="~/Scripts/Promocao.js"></script>
    <script type="text/javascript" src="~/Scripts/LiberarPromocao.js"></script>
End Section
