﻿@ModelType TipoEventoViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    Dim tituloTela = IIf(ViewBag.acao = "Save", "Incluir Tipo de Evento", "Editar Tipo de Evento")
    ViewData("Title") = tituloTela
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Tipo de Evento</li>
        @Code
            Dim campo = IIf(ViewBag.acao = "Save", "Incluir", "Editar ")
        End Code
        <li class="active">@campo</li>
    </ul>
</div>
<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-file-text-o bigger-110"></i>
            Dados
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Html.BeginForm(ViewBag.acao, "TipoEvento"))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3">
                            @Html.Label("lblCodigo", "Código:")
                            @Html.EditorFor(Function(model) model.TIPEVTACOCMC, New With {.htmlAttributes = New With {.class = "form-control", .disabled = "true"}})
                            @Html.HiddenFor(Function(model) model.TIPEVTACOCMC)
                        </div>
                        <div class="col-lg-9 col-sm-9">
                            @Html.Label("lblTipoDeEvento", "Tipo de Evento (*):")
                            @Html.EditorFor(Function(model) model.NOMTIPEVTACOCMC, New With {.htmlAttributes = New With {.class = "form-control"}})
                            @Html.ValidationMessageFor(Function(model) model.NOMTIPEVTACOCMC, "", New With {.class = "text-danger-message-login"})
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-3">
                            @Html.Label("contacontabil", "Conta Contábil (*):")
                            @Html.DropDownListFor(Function(model) model.CODCNTCTB, New SelectList(ViewBag.ListaContaContabil, "CODCNTCTB", "CODCNTCTB", Model.CODCNTCTB), "Selecione...", New With {.Class = "form-control chosen-select"})
                            @Html.ValidationMessageFor(Function(model) model.CODCNTCTB, "", New With {.class = "text-danger-message-login"})
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            @Html.Label("indValRgrInc", "Validação Rgr. Incentivo:")
                            <div class="row">
                                <div class="col-xs-4">
                                    @Html.RadioButtonFor(Function(m) m.INDVLDRGRICT, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-xs-4">
                                    @Html.RadioButtonFor(Function(m) m.INDVLDRGRICT, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2">
                            @Html.Label("indPreAcordo", "Pré Acordo:")
                            <div class="row">
                                <div class="col-xs-4">
                                    @Html.RadioButtonFor(Function(m) m.INDPREARDPMC, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-xs-4">
                                    @Html.RadioButtonFor(Function(m) m.INDPREARDPMC, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2">
                            @Html.Label("indCampanhaB2B", "Campanha B2B:")
                            <div class="row">
                                <div class="col-xs-4">
                                    @Html.RadioButtonFor(Function(m) m.INDCPHBTB, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-xs-4">
                                    @Html.RadioButtonFor(Function(m) m.INDCPHBTB, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2">
                            @Html.Label("indEventoSmart", "Evento SMART:")
                            <div class="row">
                                <div class="col-xs-4">
                                    @Html.RadioButtonFor(Function(m) m.INDEVTSMA, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-xs-4">
                                    @Html.RadioButtonFor(Function(m) m.INDEVTSMA, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
            
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-white btn-success">
                                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                                    Salvar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-white btn-danger">
                                    <i class="ace-icon fa fa-times bigger-120 red"></i>
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
