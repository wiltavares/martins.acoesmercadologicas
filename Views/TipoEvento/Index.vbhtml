﻿@ModelType GridSettings(Of TipoEventoViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Consulta Tipo Evento"
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-folder-open-o"></i>
            Cadastros
        </li>
        <li class="active">Tipo Evento</li>
    </ul>
</div>
<div class="modal fade" id="modalFuncionarios"></div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "TipoEvento", Constantes.AJAX_OBJECT_POST_DEFAULT))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-lg-2 col-sm-2">
                            @Html.Label("lblCodigo", "Código:")
                            @Html.TextBoxFor(Function(Model) Model.Filter.TIPEVTACOCMC, New With {.Class = "form-control maskNumber"})
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            @Html.Label("lblTipoDeEvento", "Nome:")
                            @Html.TextBoxFor(Function(Model) Model.Filter.NOMTIPEVTACOCMC, New With {.Class = "form-control"})
                        </div>
                        <div class="col-lg-4 col-sm-3">
                            @Html.Label("contacontabil", "Conta Corrente:")
                            @Html.DropDownListFor(Function(model) model.Filter.CODCNTCTB, New SelectList(ViewBag.ListaContaContabil, "CODCNTCTB", "CODCNTCTB", Model.Filter.CODCNTCTB), "Selecione...", New With {.Class = "form-control chosen-select"})
                        </div>
                        <div class="col-lg-2 col-sm-3">
                            @Html.Label("indPreAcordo", "Pré Acordo:")
                            <div class="row">
                                <div class="col-xs-4">
                                    @Html.RadioButtonFor(Function(model) model.Filter.INDPREARDPMC, 1)
                                    @Html.Label("sim", "Sim")
                                </div>
                                <div class="col-xs-4">
                                    @Html.RadioButtonFor(Function(model) model.Filter.INDPREARDPMC, 0)
                                    @Html.Label("nao", "Não")
                                </div>
                                <div class="col-xs-4">
                                    @Html.RadioButtonFor(Function(model) model.Filter.INDPREARDPMC, 2, New With {.checked = True})
                                    @Html.Label("todos", "Todos")
                                </div>
                            </div>
                        </div>

                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white ">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger-120"></i>
                                    Limpar
                                </a>
                                <a href="@Url.Action("CreateEdit")" class="btn btn-success btn-white">
                                    <i class="ace-icon fa fa-plus bigger-120"></i>
                                    Cadastrar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<br class="clearfix" />

<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/TipoEvento/_ResultadosView.vbhtml", Model)
    End Code
</div>
