﻿@ModelType GridSettings(Of TipoEventoViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Html.Partial(Constantes.VIEW_MODAL_CONFIRMACAO, New Tuple(Of String, String, Object)("Delete", "TipoEvento", Model))

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, "hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div style="width: auto; overflow-x: scroll;">
                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width: 250px;">
                    <thead>
                        <tr role="row">
                            <th class="center">Ações</th>
                            <th class="@Grid.AtribuirIconeOrdenacao("TIPEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("TIPEVTACOCMC", Model.Page.OrderBy)">
                                Código
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMTIPEVTACOCMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMTIPEVTACOCMC", Model.Page.OrderBy)">
                                Nome
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODCNTCTB", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODCNTCTB", Model.Page.OrderBy)">
                                Conta Corrente
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDVLDRGRICT", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDVLDRGRICT", Model.Page.OrderBy)">
                                Valida Incentivo
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDPREARDPMC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDPREARDPMC", Model.Page.OrderBy)">
                                Pré Acordo
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDCPHBTB", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDCPHBTB", Model.Page.OrderBy)">
                                Campanha B2B
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("INDEVTSMA", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("INDEVTSMA", Model.Page.OrderBy)">
                                Evento SMART
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td class="center">
                                    <div class="hidden-sm hidden-xs btn-group">
                                        <a data-rel="tooltip" title="Editar" class="btn btn-xs btn-info" href="@Url.Action("CreateEdit", New With {.id = item.TIPEVTACOCMC})">
                                            <i class="ace-icon fa fa-pencil bigger-119"></i>
                                        </a>
                                        @*<a data-rel="tooltip" title="Deletar" Class="btn btn-xs btn-danger" data-toggle="modal" data-target="#confirmationDelete" data-id="@item.TIPEVTACOCMC">
                                                <i Class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </a>*@
                                    </div>

                                    <div class="hidden-md hidden-lg">
                                        <div class="inline pos-rel">
                                            <button class="btn btn-minier btn-warning dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                                <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                            </button>

                                            <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                <li>
                                                    <a class="tooltip-info" href="@Url.Action("CreateEdit", New With {.id = item.TIPEVTACOCMC})">
                                                        <span class="blue">
                                                            <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="tooltip-info" data-rel="tooltip" data-toggle="modal" data-target="#confirmationDelete" data-id="@item.TIPEVTACOCMC">
                                                        <span class="red">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    @Html.Label(CInt(item.TIPEVTACOCMC))
                                </td>
                                <td>
                                    @item.NOMTIPEVTACOCMC
                                </td>
                                <td>
                                    @Html.Label(CInt(item.CODCNTCTB))
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDVLDRGRICT) = 0, "Não", "Sim"))
                                </td>
                                <td>
                                    @Html.Label(If(CInt(item.INDPREARDPMC) = 0, "Não", "Sim"))
                                </td>
                                <td>
                                    @Html.Label(If(item.INDCPHBTB = 1, "Sim", "Não"))
                                </td>
                                <td>
                                    @Html.Label(If(item.INDEVTSMA = 1, "Sim", "Não"))
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code

        </div>
    </div>
</div>

