﻿@ModelType GridSettings(Of GrupoAcessoViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Controle de Acesso"
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-gears"></i>
           Controle de Acesso
        </li>
    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

@Using (Ajax.BeginForm("Pesquisar", "ControleAcesso", Constantes.AJAX_OBJECT_POST_DEFAULT))
    @Html.AntiForgeryToken()
    @<div class="form-horizontal">
        <div class="widget-box widget-color-blue2">
            <div class="widget-header widget-header-small">
                <h4 class="widget-title smaller white lighter">
                    <i class="ace-icon fa fa-sliders bigger-110"></i>
                    Filtros de Pesquisa
                </h4>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row">
                        <div class="col-sm-2">
                            @Html.Label("lblCodGrupoAcesso", "Grupo Acesso:")
                            @Html.TextBoxFor(Function(Model) Model.Filter.CODGRPACS)
                        </div>
                        <div class="col-sm-10">
                            @Html.Label("lblNomGrupoAcesso", "Nome do Grupo Acesso:")
                            @Html.TextBoxFor(Function(Model) Model.Filter.DESGRPACS, New With {.Class = "form-control"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white ">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger-120"></i>
                                    Limpar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
End Using
<br class="clearfix" />
<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/ControleAcesso/_ResultadosView.vbhtml", Model)
    End Code
</div>
