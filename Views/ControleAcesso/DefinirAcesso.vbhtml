﻿@ModelType GrupoAcessoViewModel
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Definir Acesso"
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-gears"></i>
            Controle de Acesso
        </li>
        <li class="active">Definir Acesso</li>

    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>

<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-file-text-o bigger-110"></i>
            Definir Acesso
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Html.BeginForm())
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-2">
                            @Html.Label("lblGrupoAcesso", "Grupo de Acesso:")
                            @Html.EditorFor(Function(model) model.CODGRPACS, New With {.htmlAttributes = New With {.class = "form-control", .disabled = "true", .ReadOnly = "true"}})
                            @*@Html.HiddenFor(Function(model) model.CODGRPACS)*@
                        </div>
                        <div class="col-sm-5">
                            @Html.Label("lblNomeGrupoAcesso", "Nome Grupo de Acesso:")
                            @Html.EditorFor(Function(model) model.DESGRPACS, New With {.htmlAttributes = New With {.class = "form-control", .disabled = "true", .ReadOnly = "true"}})
                            @*@Html.HiddenFor(Function(model) model.DESGRPACS)*@
                        </div>
                        <div class="col-sm-5">
                            @Html.Label("lblNomGrupoAD", "Nome Grupo AD:")
                            @Html.EditorFor(Function(model) model.DESGRPRDE, New With {.htmlAttributes = New With {.class = "form-control", .disabled = "true", .ReadOnly = "true"}})
                            @*@Html.HiddenFor(Function(model) model.DESGRPRDE)*@
                        </div>
                    </div>
                    <br>
                    <div id="divCheckMenu" class="arvore">
                        @Html.Partial("_TreeMenu", Model)
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button onclick="SalvarControleAcesso(@Model.CODGRPACS)" class="btn btn-white btn-success">
                                    <i class="ace-icon fa fa-save bigger-120 green"></i>
                                    Salvar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-white btn-danger">
                                    <i class="ace-icon fa fa-times bigger-120 red"></i>
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<script src="~/Scripts/controleAcesso.js"></script>