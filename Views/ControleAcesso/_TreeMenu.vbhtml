﻿@ModelType GrupoAcessoViewModel
@Imports System.Linq
@If (Model.ControleAcessos.Where(Function(s) s.CODMNUPAI = Model.CodMenuCorrente).Any()) Then
    @<div class="col-xs-12 arvore-node">
        @For Each node As ControleAcessoViewModel In Model.ControleAcessos
            If node.CODMNUPAI = Model.CodMenuCorrente Then
                Dim inner As New GrupoAcessoViewModel() With {.CodMenuCorrente = node.CODMNU, .ControleAcessos = Model.ControleAcessos}
            @<div class="content">
                @If node.CODMNUPAI = 0 Then
                    @Html.CheckBox(String.Format("{0}.{1}", node.CODMNUPAI.GetValueOrDefault(), node.CODMNU), (node.INDASSOCIADO = 1).Value, New With {.onclick = "marcarOuDesmarcarFilhos(this);"})
                    @Html.Label("NomeMenu", node.DESMNU.Replace("&", ""))
                Else
                    @Html.CheckBox(String.Format("{0}.{1}", node.CODMNUPAI.GetValueOrDefault(), node.CODMNU), (node.INDASSOCIADO = 1).Value)
                    @Html.Label(node.DESMNU, node.DESMNU.Replace("&", ""))
                End If
                @Html.Partial("_TreeMenu", inner)
            </div>
            End If
        Next
    </div>
End If
