﻿@ModelType GridSettings(Of ConsultaAcompanhamentoBcComFundingViewModel)
@Imports Martins.AcoesMercadologicas.Core
@Code
    ViewData("Title") = "Relatorio de Acompanhamento BC com Funding"
End Code
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-bar-chart"></i>
            Relatórios
        </li>
        <li class="active">Relatorio de Acompanhamento BC com Funding</li>
    </ul>
</div>

<div id="msgGeral">
    @Html.Partial(Constantes.VIEW_ALERTAS)
</div>



<div class="widget-box widget-color-blue2">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-sliders bigger-110"></i>
            Filtros de Pesquisa
        </h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @Using (Ajax.BeginForm("Pesquisar", "ConsultaAcompanhamentoBcComFunding", Constantes.AJAX_OBJECT_POST_DEFAULT, New With {.id = "frmFiltrosConsultaAcompanhamentoBcComFunding"}))
                @Html.AntiForgeryToken()
                @<div class="form-horizontal">


                    <div class="row">
                        <div class="col-lg-4">
                            @Html.Label("lblPeriodo", "Período(*):")
                            <div class="input-daterange input-group">
                                @Html.TextBoxFor(Function(m) m.Filter.DATREFOPEINI, "{0:dd/MM/yyyy}", New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataIni()", .id = "dataIni", .autocomplete = "off"})
                                @Html.ValidationMessageFor(Function(m) m.Filter.DATREFOPEINI, "", New With {.class = "text-danger-message-login"})
                                <span class="input-group-addon">
                                    à
                                </span>
                                @Html.TextBoxFor(Function(m) m.Filter.DATREFOPEFIM, "{0:dd/MM/yyyy}", New With {.class = "input-sm date-interval form-control", .onchange = "copiaDataFim()", .id = "dataFim", .autocomplete = "off"})
                                @Html.ValidationMessageFor(Function(m) m.Filter.DATREFOPEFIM, "", New With {.class = "text-danger-message-login"})
                            </div>
                        </div>

                        <div class="col-lg-8">
                            @Html.Label("lblTIPOPEINIGRCHST", "Tipo de Operação:")
                            <div class="row">
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(m) m.Filter.TIPOPEINIGRCHST, "F", New With {.id = "rbd_TIPOPEINIGRCHST_F"})
                                    @Html.Label("rbd_TIPOPEINIGRCHST_F", "Faturamento")
                                </div>
                                <div class="col-sm-6">
                                    @Html.RadioButtonFor(Function(m) m.Filter.TIPOPEINIGRCHST, "V", New With {.id = "rbd_TIPOPEINIGRCHST_V"})
                                    @Html.Label("rbd_TIPOPEINIGRCHST_V", "Venda")
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            @Html.Label("drpFiltroCODBFC", "Benefício:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODBFC, New SelectList(ViewBag.ListaDropBeneficiario, "CODBFC", "CODDESBFC", Model.Filter.CODBFC), "Selecione...", New With {.Class = "chosen-select form-control", .id = "drpFiltroCODBFC"})
                        </div>
                        <div class="col-lg-4">
                                @Html.Label("drpFiltroCODGRPFRN", "Grupo de Fornecedor:")
                                @Html.DropDownListFor(Function(m) m.Filter.CODGRPFRN, New SelectList(ViewBag.ListaDropGrupoFornecedor, "CODGRPFRN", "CODNOMGRPFRN", Model.Filter.CODGRPFRN), "Selecione...", New With {.Class = "chosen-select form-control", .id = "drpFiltroCODGRPFRN"})
                            </div>
                       

                        <div class="col-lg-4">
                            @Html.Label("drpFiltroCODFRN", "Fornecedor:")
                            @Html.DropDownListFor(Function(m) m.Filter.CODFRN, New SelectList(ViewBag.ListaDropFornecedor, "CODFRN", "CodNomFrn", Model.Filter.CODFRN), "Selecione...", New With {.Class = "chosen-select form-control", .id = "drpFiltroCODFRN"})
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info btn-white ">
                                    <i class="ace-icon fa fa-search bigger-120"></i>
                                    Pesquisar
                                </button>
                                <a href="@Url.Action("Index")" class="btn btn-grey btn-white">
                                    <i class="ace-icon fa fa-eraser bigger"></i>
                                    Limpar
                                </a>
                                <a onclick="btnExportarAcompanhamentoBC()" href="#" class="btn btn-info btn-white" id="btnImprimirRel">
                                    <i class="ace-icon fa fa-file-excel-o bigger-120"></i>
                                    Exportar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            End Using
        </div>
    </div>
</div>
<div id="resultadosView">
    @Code
        Html.RenderPartial("~/Views/ConsultaAcompanhamentoBcComFunding/_ResultadosView.vbhtml", Model)
    End Code
</div>


<script src="~/Scripts/consultaAcompanhamentoBcComFunding.js"></script>
