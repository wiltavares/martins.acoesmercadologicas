﻿@ModelType GridSettings(Of ConsultaAcompanhamentoBcComFundingViewModel)
@Imports Martins.AcoesMercadologicas.Core

<div class="widget-box widget-color-blue2 @IIf(Model.ListPaged.Count = 0, " hide", "")">
    <div class="widget-header widget-header-small">
        <h4 class="widget-title smaller white lighter">
            <i class="ace-icon fa fa-list bigger-110"></i>
            Resultados
        </h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">

            <div style="width: auto; overflow-x: scroll;">

                <table class="table table-striped table-bordered table-hover dataTable" role="grid" style="min-width:2200px">
                    <thead>
                        <tr role="row">
                            <th class="@Grid.AtribuirIconeOrdenacao("CODGRPFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODGRPFRN", Model.Page.OrderBy)">
                                Cod. Grupo Fornecedor
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMGRPFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMGRPFRN", Model.Page.OrderBy)">
                                Grupo Fornecedor
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DATREFOPE", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATREFOPE", Model.Page.OrderBy)">
                                Data Referência
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODBFC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODBFC", Model.Page.OrderBy)">
                                Cod. Benefício
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESBFC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESBFC", Model.Page.OrderBy)">
                                Benefício
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DATINIVLDBFC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATINIVLDBFC", Model.Page.OrderBy)">
                                Início Benefício
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DATFIMVLDBFC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DATFIMVLDBFC", Model.Page.OrderBy)">
                                Fim Benefício
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODFILEMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFILEMP", Model.Page.OrderBy)">
                                Cod Filial
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMFILEMP", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFILEMP", Model.Page.OrderBy)">
                                Filial
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODFRN", Model.Page.OrderBy)">
                                Cod. Fornecedor
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("NOMFRN", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("NOMFRN", Model.Page.OrderBy)">
                                Fornecedor
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("CODMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("CODMER", Model.Page.OrderBy)">
                                Cod. Mer.
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("DESMER", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("DESMER", Model.Page.OrderBy)">
                                Mercadoria
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("PERBFC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("PERBFC", Model.Page.OrderBy)">
                                Perc Bfc
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("QDEITEBFC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("QDEITEBFC", Model.Page.OrderBy)">
                                Qtde Itens
                            </th>
                            <th class="@Grid.AtribuirIconeOrdenacao("VLRTOTFNDBFC", Model.Page.OrderBy)" data-href="@Grid.OrdenarColuna("VLRTOTFNDBFC", Model.Page.OrderBy)">
                                Valor Descto Bfc
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @For Each item In Model.ListPaged
                            @<tr>
                                <td>
                                    @Html.DisplayFor(Function(model) item.CODGRPFRN)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.NOMGRPFRN)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.DATREFOPE)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.CODBFC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.DESBFC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.DATINIVLDBFC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.DATFIMVLDBFC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.CODFILEMP)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.NOMFILEMP)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.CODFRN)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.NOMFRN)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.CODMER)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.DESMER)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.PERBFC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.QDEITEBFC)
                                </td>
                                <td>
                                    @Html.DisplayFor(Function(model) item.VLRTOTFNDBFC)
                                </td>
                            </tr>
                        Next
                    </tbody>
                </table>
            </div>
            <br class="clearfix" />
            @Code
                Html.RenderPartial(Constantes.VIEW_PAGINACAO)
            End Code
        </div>
    </div>
</div>