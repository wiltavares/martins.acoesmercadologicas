﻿<%@ Application Language="VB" %> 
<%@ Import Namespace="System.ConditionalValidation.Validation" %>
<%@ Import Namespace="System.Security.Principal" %>
<%@ Import Namespace="Controllers" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System.Web.Mvc" %>

<script runat="server">
    Public Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        'Para validacoes condicionais no view model
        DataAnnotationsModelValidatorProvider.RegisterAdapter(GetType(RequiredIfAttribute), GetType(RequiredIfValidator))
        AreaRegistration.RegisterAllAreas()

        ModelBinders.Binders.Add(GetType(Decimal), New DecimalModelBinder())
        ModelBinders.Binders.Add(GetType(Decimal?), New DecimalModelBinder())

        RegisterRoutes(RouteTable.Routes)
        RegisterMappings()
        BuscaFornecedores()
    End Sub
    
    Protected Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Dim httpContext = DirectCast(sender, HttpApplication).Context
        Dim ex = Server.GetLastError()
        Dim controller = New GenericErrorController()
        Dim routeData = New RouteData()


        httpContext.ClearError()
        httpContext.Response.Clear()
        httpContext.Response.StatusCode = If(TypeOf ex Is HttpException, DirectCast(ex, HttpException).GetHttpCode(), 500)
        httpContext.Response.TrySkipIisCustomErrors = True


        routeData.Values("controller") = "GenericError"
        routeData.Values("action") = "Index"

        controller.ViewData.Model = ex
        DirectCast(controller, IController).Execute(New RequestContext(New HttpContextWrapper(httpContext), routeData))



    End Sub

    Protected Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        Dim authCookie As HttpCookie = Context.Request.Cookies(FormsAuthentication.FormsCookieName)
        If authCookie Is Nothing OrElse authCookie.Value = "" Then
            Return
        End If

        Dim authTicket As FormsAuthenticationTicket
        Try
            authTicket = FormsAuthentication.Decrypt(authCookie.Value)
        Catch
            Return
        End Try

        ' retrieve roles from UserData
        Dim roles As String() = authTicket.UserData.Split("|")(1).Split(";"c)

        If Context.User IsNot Nothing Then
            Context.User = New GenericPrincipal(Context.User.Identity, roles)
        End If
    End Sub
       
</script>