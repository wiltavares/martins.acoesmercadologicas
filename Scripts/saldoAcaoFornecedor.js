﻿// Mostra ou esconde os campos de filtro de celula/fornecedor/comprador
function dropFiltros_onChange(idFiltro) {
    if (idFiltro == 3) {
        $('#divFiltroFornecedor').show();
        $('#divFiltroCelula').hide();
        $('#divFiltroComprador').hide();
    } else if (idFiltro == 2) {
        $('#divFiltroCelula').show();
        $('#divFiltroFornecedor').hide();
        $('#divFiltroComprador').hide();
    } else {
        $('#divFiltroComprador').show();
        $('#divFiltroFornecedor').hide();
        $('#divFiltroCelula').hide();
    }
    ChosenResize();
}

// Evento change do drop de Fornecedor para atribuir o codigo ao textBox correspondente
function dropFornecedorCarimbo_onChange() {
    $('#filtroCodFrn').val($('#dropFiltroFornecedorCarimbo').val());
}

// Atualiza a mensagem do modal vinda do controller ou da propria tela
function atualizaMensagemModalCarimbo(mensagem, tipo) {
    if (!!mensagem) {
        mostraMensagem(mensagem, tipo, "atualizaMensagemModalCarimbo");
    } else {
        atualizaMensagem("atualizaMensagemModalCarimbo");
    }
}

// Busca fornecedores e atualiza o dropdown com os valores da busca
function buscarFornecedores() {
    var codFornecedor = $('#filtroCodFrn').val();
    postAjax("ConsultaSaldoAcao", "listaRelacaoFornecedoresAcao", { codFrn: codFornecedor },
        function (data) {
            carregarDropDinamicamente("dropFiltroFornecedorCarimbo", "CODFRN", "NOMFRN", data, true);
            $('#filtroCodFrn').val($('#dropFiltroFornecedorCarimbo').val());
            atualizaMensagemModalCarimbo();
            LoadingOff();
        }, function () {
            atualizaMensagemModalCarimbo();
            LoadingOff();
        }
    );
}

// chama o metodo Index do Modal
function LimparCamposModal() {

    $('#txtCodAcao').val('');
    $('#txtNomAcao').val('');

    var dropFiltroCarimbos = document.getElementById("dropFiltroCarimbos");
    dropFiltroCarimbos.selectedIndex = 0;

    var dropFiltroFornecedorCarimbo = document.getElementById("dropFiltroFornecedorCarimbo");
    dropFiltroFornecedorCarimbo.selectedIndex = 0;

    var dropFiltroCelula = document.getElementById("dropFiltroCelula");
    dropFiltroCelula.selectedIndex = 0;

    var dropFiltroComprador = document.getElementById("dropFiltroComprador");
    dropFiltroComprador.selectedIndex = 0;

    dropFiltros_onChange(1); //inicia combo
}

function fnImprimir() {

    LoadingOn();

    data = {
           CODEVTACOCMC: ToDecimal('CODEVTACOCMC')
         , CODACOCMC: ToDecimal('CODACOCMC')
         , DATINIACOCMC:  $('#dataIni').val()   
         , DATFIMACOCMC: $('#dataFim').val() 
         , CODOPEFSCDSNACOCMC: ToDecimal('CODOPEFSCDSNACOCMC')
         , NOMACOCMC: $('#NOMACOCMC').val()
    };

    $.ajax({
        cache: false,
        url: SiteUrl + "ConsultaSaldoAcao/Imprimir",
        data: data,
        success: function (data) {
            var response = data;
            if (data != "")
            {
                window.location = SiteUrl + 'ConsultaSaldoAcao/Download?fileGuid=' + response.FileGuid
                             + '&filename=' + response.FileName;
            }
            else
            {
                atualizaMensagem("msgGeral");
            }

            LoadingOff();
        },
        error: function () {
            LoadingOff();
        }
    })
};

// data: { acao.DATINIACOCMC: DATINIACOCMC, acao.DATFIMACOCMC : DATFIMACOCMC, acao.CODEVTACOCMC : CODEVTACOCMC, acao.CODOPEFSCDSNACOCMC : CODOPEFSCDSNACOCMC, acao.CODACOCMC : CODACOCMC,acao.NOMACOCMC : NOMACOCMC },

function ToDate(id) {
    var valor = $('#' + id).val();

    if (valor === '') {
        return null;
    }

    if (valor.length == 10) {
        var dia = parseInt(valor.substring(0, 2));
        var mes = parseInt(valor.substring(3, 5));
        var ano = parseInt(valor.substring(6, 10));
        var datacompleta = ano + "/" + mes + "/" + dia;
        return new Date(datacompleta);
    }
    else {
        return null;
    }
}

function ToDecimal(id) {
    var valor = $('#' + id).val();

    if (valor === '') {
        return null;
    }

    valor = valor.replace('.', ',');
    return parseFloat(valor);
}


function Imprimirpdf() {

    LoadingOn();

    data = {

        CODEVTACOCMC: ToDecimal('CODEVTACOCMC')


    };

    $.ajax({
        cache: false,
        url: SiteUrl + "ConsultaGastoAcao/Imprimir",
        data: data,
        success: function (data) {
            var response = data;
            window.location = SiteUrl + 'ConsultaGastoAcao/Download?fileGuid=' + response.FileGuid
                              + '&filename=' + response.FileName;
            LoadingOff();
        },
        error: function () {
            LoadingOff();
        }
    })
};

// data: { acao.DATINIACOCMC: DATINIACOCMC, acao.DATFIMACOCMC : DATFIMACOCMC, acao.CODEVTACOCMC : CODEVTACOCMC, acao.CODOPEFSCDSNACOCMC : CODOPEFSCDSNACOCMC, acao.CODACOCMC : CODACOCMC,acao.NOMACOCMC : NOMACOCMC },

//function ToDate(id) {
//    var valor = $('#' + id).val();

//    if (valor === '') {
//        return null;
//    }

//    return new Date(valor);
//}

function ToDecimal(id) {
    var valor = $('#' + id).val();

    if (valor === '') {
        return null;
    }

    valor = valor.replace('.', ',');
    return parseFloat(valor);
}





//var data = $('#dataIni');
//var data = $('#dataFim');
//var evento = $('#dropTipoEvento');
//var evento = $('#dropEvento');
//var btnFrnIncluirFornecedor = $('#btnFrnIncluirFornecedor');
//var btnFrnDespesasMartins = $('#btnFrnDespesasMartins');
//var cmdDelFrn = $('#divAcaoPrincipal');

//var form = $("#ConsultaSaldo").val();

//$.fn.serializeObject = function () {
//    var o = {};
//    var a = this.serializeArray();
//    $.each(a, function () {
//        if (o[this.name] !== undefined) {
//            if (!o[this.name].push) {
//                o[this.name] = [o[this.id]];
//            }
//            o[this.name].push(this.value || '');
//        } else {
//            o[this.name] = this.value || '';
//        }
//    });
//    return o;
//}

//var data = form.serializeObject();




