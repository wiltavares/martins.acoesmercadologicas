﻿
function LiberarPromocao(idPmc, callback) {
    getAjax("Promocao", "ModalLiberarPromocao" + '/' + idPmc,
        function (data) {
            ConfirmarPromocao(data, idPmc, callback);
            LoadingOff();
        },
        function (data) {
            LoadingOff();
            ShowModal({ mensagem: "Não é possível liberar essa promoção.", titulo: "Atenção" })
        }
    );
}

function ConfirmarPromocao(html, idPmc, callback) {
    var obj = {
        idPmc: idPmc,
        callback: callback
    };

    ShowModalConfirmar({ titulo: 'Confirmar liberação', mensagem: html, callback: LiberarPromocaoCore, parametros: obj, width: 800 });
}

function LiberarPromocaoCore(obj) {
    var idPmc = obj.idPmc;
    var callback = obj.callback;

    LoadingOn();



    $.ajax({
        url: SiteUrl + "Promocao/Liberar/" + idPmc,
        type: 'POST',
        success: function (data) {


            $("#modalPromocao").html(data);
            $("#modalPromocao").modal({ show: true, backdrop: 'static' }).on('shown.bs.modal', function () {
                ChosenResize();
            });

            resizeModalPromocao($('#modalLiberarPromocaoResultado'));

            LoadingOff();
        },
        function() {
            LoadingOff();
            ShowModal({ mensagem: "Não foi possível liberar essa promoção.", titulo: "Erro" })
        }
    });

}


function fecharModalLiberarPromocao() {
    $("#modalPromocao").modal("hide");

    if (window.location.href.toLowerCase().indexOf("/edit/") != -1) { //Edição da promocao
        location.reload();
    } else if (window.location.href.toLowerCase().indexOf("/promocaowizard/createwizard/") != -1) { //Wizard
        RefreshStepResumo();
    }
    else if ($('#divFiltroPromocao').length == 1) { // pagina de pesquisa
        $('#divFiltroPromocao').find('button[type="submit"] ').submit()
    }
}

function ModalLiberarPromocaoExluirMercadoriaPromocao() {

}

function liberarCallbackList() {
    //Atualiza pesquisa
    $('.modal[aria-labelledby="myModalLabel"]').modal().on('hidden.bs.modal', function () {
        $('#form0').submit();
    });
}

function liberarCallbackEditPromocao() {
    //Atualiza a tela
    $('.modal[aria-labelledby="myModalLabel"]').modal().on('hidden.bs.modal', function () {
        location.reload();
    });
}