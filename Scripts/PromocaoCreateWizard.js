﻿
$(document).ready(function () {
    if (window.location.pathname.split('/')[window.location.pathname.split('/').length - 3] == "CreateWizard") {
        IniciarStepFilial();
    }
});


function IniciarStepFilial() {

    LoadingOn();

    var CODACOCMC = GetCODACOCMC();
    var NUMSEQRGRACOCMC = GetNUMSEQRGRACOCMC();

    getAjax('PromocaoWizard', 'WizardCreateFilial/' + CODACOCMC + '/' + NUMSEQRGRACOCMC,
        function (data) {
            $('#divStepPane1').html(data);
            $('.step-pane.active').removeClass("active");
            $('#fuelux-wizard-container .steps >li[data-step]').removeClass('complete');
            $('#fuelux-wizard-container .steps >li[data-step]').removeClass('active');

            $('#fuelux-wizard-container .steps >li[data-step="1"]').addClass('active');
            $('#divStepPane1').addClass("active");
            atualizaMensagem();
            LoadingOff();
        },
        function () {
            atualizaMensagem();
            LoadingOff();
        }
    );
}
function AvancarStepFilial() {

    var CODACOCMC = GetCODACOCMC();
    var NUMSEQRGRACOCMC = GetNUMSEQRGRACOCMC();

    //verifica se selecionou pelo menos uma filial.
    if ($('#divStepPane1 input[type="checkbox"]:checked').not("[disabled]").length == 0) {
        mostraMensagem("SELECIONE_PELO_MENOS_UMA_FILIAL", "msgWarning");
        return;
    }

    LoadingOn();
    getAjax('PromocaoWizard', 'WizardCreateGeral/' + CODACOCMC + '/' + NUMSEQRGRACOCMC,
        function (data) {
            $('#divStepPane2').html(data);
            $('.step-pane.active').removeClass("active");
            $('#fuelux-wizard-container .steps >li[data-step="1"]').addClass('complete');
            $('#fuelux-wizard-container .steps >li[data-step="2"]').addClass('active');
            $('#divStepPane2').addClass("active");

            ckbCrterioAdicional_OnClick();
            DatePickerConfig();
            atualizaMensagem();
            LoadingOff();
        },
        function () {

            atualizaMensagem();
            LoadingOff();
        }
    );
}


function ExcluirFiliaisStepFilial() {



    //verifica se selecionou pelo menos uma filial.
    if ($('#divStepPane1 input[type="checkbox"]:checked').not("[disabled]").length == 0) {
        mostraMensagem("SELECIONE_PELO_MENOS_UMA_FILIAL", "msgWarning");
        return;
    }



    ShowModalConfirmar({
        titulo: 'Confirmação', mensagem: "Deseja realmente excluir estas filiais Selecionadas?",
        callback: ConfirmarExcluirFiliais
    });


}
function ConfirmarExcluirFiliais() {

    LoadingOn();

    var CODACOCMC = GetCODACOCMC();
    var NUMSEQRGRACOCMC = GetNUMSEQRGRACOCMC();


    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        filiais: ReadEditStepFiliais()
    };

    postAjax('PromocaoWizard', 'WizardExcluirFiliais', data,
        function (data) {
            if (data == "True") {
                IniciarStepFilial();
            }
            atualizaMensagem();
            LoadingOff();
        },
        function () {
            atualizaMensagem();
            LoadingOff();
        }
    );
}



function VoltarStepGeral() {
    $('.step-pane.active').removeClass("active");
    $('#fuelux-wizard-container .steps >li[data-step="1"]').removeClass('complete');
    $('#fuelux-wizard-container .steps >li[data-step="2"]').removeClass('active');
    $('#divStepPane1').addClass("active");
}
function AvancarStepGeral() {
    var CODACOCMC = GetCODACOCMC();
    var NUMSEQRGRACOCMC = GetNUMSEQRGRACOCMC();
    //var INDISRBDEADI = ($('#chbBrindeAdicional:checked').length === 1);

    //if (validarHoras() == false)
    //    return;

    LoadingOn();

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        Promocao: ReadEditStepGeral()
    };

    postAjax('PromocaoWizard', 'WizardCreateBrinde', data,
        function (data) {
            if (data != "") {
                $('#divStepPane3').html(data);
                $('.step-pane.active').removeClass("active");
                $('#fuelux-wizard-container .steps >li[data-step="2"]').addClass('complete');
                $('#fuelux-wizard-container .steps >li[data-step="3"]').addClass('active');
                $('#divStepPane3').addClass("active");

                brindes = [];
            }

            atualizaMensagem();
            LoadingOff();
        },
        function (error) {
            atualizaMensagem();
            LoadingOff();
        }
    );
}

function AvancarStepGeralCashback() {
    var promocao = ReadEditStepGeral()
    if (promocao.CODUNDESRNGC == '') {
        ShowModal({ mensagem: "Favor preencher o campo BU.", titulo: "Atenção" });
        return;
    }

    if (promocao.NOMPMC == '') {
        ShowModal({ mensagem: "Favor preencher o campo Nome da Promoção.", titulo: "Atenção" });
        return;
    }

    if (promocao.FLGENVPMC == '') {
        ShowModal({ mensagem: "Favor preencher o campo Status de Envio.", titulo: "Atenção" });
        return;
    }

    if (promocao.DATINIPMC == '') {
        ShowModal({ mensagem: "Favor preencher o campo Dt. de Início.", titulo: "Atenção" });
        return;
    }

    if (promocao.DATFIMPMC == '') {
        ShowModal({ mensagem: "Favor preencher o campo Dt. de Fim.", titulo: "Atenção" });
        return;
    }

    if (promocao.TIPEQIVND == '') {
        ShowModal({ mensagem: "Favor preencher o campo Equipe de Vendas.", titulo: "Atenção" });
        return;
    }

    //if (validarHoras() == false)
    //    return;

    AvancarStepBrindes()
}

function VoltarStepBrindes() {
    $('.step-pane.active').removeClass("active");
    $('#fuelux-wizard-container .steps >li[data-step="2"]').removeClass('complete');
    $('#fuelux-wizard-container .steps >li[data-step="3"]').removeClass('active');
    $('#fuelux-wizard-container .steps >li[data-step="3"]').removeClass('complete');
    $('#fuelux-wizard-container .steps >li[data-step="4"]').removeClass('active');
    $('#divStepPane2').addClass("active");
    ChosenResize();
}
function AvancarStepBrindes() {
    var CODACOCMC = GetCODACOCMC();
    var NUMSEQRGRACOCMC = GetNUMSEQRGRACOCMC();

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        Promocao: ReadEditStepGeral()
    };

    LoadingOn();
    postAjax('PromocaoWizard', 'WizardCreateComunicacao', data,
        function (data) {
            if (data != "") {
                $('#divStepPane4').html(data);
                $('#fuelux-wizard-container .steps >li[data-step="2"]').addClass('complete');
                $('.step-pane.active').removeClass("active");
                $('#fuelux-wizard-container .steps >li[data-step="3"]').addClass('complete');
                $('#fuelux-wizard-container .steps >li[data-step="4"]').addClass('active');
                $('#divStepPane4').addClass("active");

                $('[name="formaDivulgRadio"]').last().prop('checked', true)

                //configura evento na lista de preço
                $('#chbTipCmnPmc_3').change(function () {
                    if ($(this).is(":checked")) {
                        $('#tblGridLivro').show()
                    } else {
                        $('#tblGridLivro').hide();
                    }
                });

                $('#codigoSimbolo').on("keyup", function (event) {
                    if ($('#codigoSimbolo').val().length > 1)
                        $('#codigoSimbolo').val($('#codigoSimbolo').val().substring(0, 1))
                    var arr = [];
                    for (var i = 65; i <= 90; i++) {
                        arr.push(i);
                    }
                    for (var i = 97; i <= 122; i++) {
                        arr.push(i);
                    }

                    if (jQuery.inArray($('#codigoSimbolo').val().charCodeAt(0), arr) === -1) {
                        $('#codigoSimbolo').val('');
                    }
                });
            }

            atualizaMensagem();
            LoadingOff();
        },
        function () {

            atualizaMensagem();
            LoadingOff();
        }
    );
}


function VoltarStepComunicacao() {
    $('.step-pane.active').removeClass("active");
    $('#fuelux-wizard-container .steps >li[data-step="3"]').removeClass('complete');
    $('#fuelux-wizard-container .steps >li[data-step="4"]').removeClass('active');
    $('#divStepPane3').addClass("active");
}
function AvancarStepComunicacao() {

    if ($('#chbTipCmnPmc_3').prop('checked') === true && $('#codigoSimbolo').val() === '') {
        ShowModal({ mensagem: "Favor preencher o campo Código Simbolo.", titulo: "Atenção" });
        return;
    }


    var CODACOCMC = GetCODACOCMC();
    var NUMSEQRGRACOCMC = GetNUMSEQRGRACOCMC();

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        filiais: ReadEditStepFiliais(),
        promocao: ReadEditStepGeral(),
        brindes: ReadEditStepBrindes(),
        comunicacao: ReadEditStepComunicacao()
    };
    LoadingOn();
    postAjax('PromocaoWizard', 'WizardCreatePromocao', data,
        function (data) {
            if (data != "") {
                $('#divStepPane5').html(data);
                $('.step-pane.active').removeClass("active");
                $('#fuelux-wizard-container .steps >li[data-step="4"]').addClass('complete');
                $('#fuelux-wizard-container .steps >li[data-step="5"]').addClass('active');
                $('#divStepPane5').addClass("active");
            }
            atualizaMensagem();
            LoadingOff();
        },
        function (error) {
            //Window.html(error.responseText);
            atualizaMensagem();
            LoadingOff();
        }
    );
}

function ReadEditStepGeral() {
    var form = $("#frmStepGeral");
    var promocao = form.serializeObject();
    return promocao;
}
function ReadEditStepFiliais() {
    var filiais = [];
    $('#divStepPane1 input[type="checkbox"]:checked').not("[disabled]").each(function fx(index, value) {
        filiais[filiais.length] = parseFloat($(value).val());
    });
    return filiais;
}

var brindes = [];

function AddBrinde() {

    var brinde = ReadAddStepBrinde();

    var data = {
        CODACOCMC: GetCODACOCMC(),
        NUMSEQRGRACOCMC: GetNUMSEQRGRACOCMC(),
        Brinde: brinde
    };

    if ($('#chbCobrancaBrinde').is(':checked')) {
        if (data.Brinde.PERBFCBDE === null || data.Brinde.PERBFCBDE == '') {
            ShowModal({ mensagem: "É necessário preencher o campo % Benefício antes de inserir o brinde.", titulo: "Atenção" });
            return;
        }
    }

    let brindeExiste = brindes.find(p => p.CODPRMPMC == brinde.CODPRMPMC);
    if (brindeExiste !== undefined && brindeExiste != null) {
        ShowModal({ mensagem: "Este brinde ja foi adicionado.", titulo: "Atenção" });
        return;
    }

    postAjax('PromocaoWizard', 'WizardAddBrinde', data,
        function (data) {
            if (data != "") {

                if (data.CUSTO != null)
                    data.CUSTO = (data.CUSTO.toFixed(2) + '').replace('.', ',');

                if (data.VLRUNTLIQMER != null)
                    data.VLRUNTLIQMER = (data.VLRUNTLIQMER.toFixed(2) + '').replace('.', ',');

                if (data.VLRUNTPCOBRT != null)
                    data.VLRUNTPCOBRT = (data.VLRUNTPCOBRT.toFixed(2) + '').replace('.', ',');

                if (data.PERBFCBDE != null)
                    data.PERBFCBDE = (data.PERBFCBDE.toFixed(2) + '').replace('.', ',');


                brindes[brindes.length] = data;
                BindBrindeList();
                tirarItem();
                $('#IdBrindeReserva').val('100');
                $('#IdMultBrinde').val('1');
            }
            atualizaMensagem();
            LoadingOff();
        },
        function (error) {
            atualizaMensagem();
            LoadingOff();
        }
    );
}
function DeleteBrinde(index) {

    var arrayAux = [];
    for (var i = 0; i < brindes.length; i++) {
        if (i != index) {
            arrayAux[arrayAux.length] = brindes[i];
        }
    }
    brindes = arrayAux;
    BindBrindeList();
}

function ReadAddStepBrinde() {
    var form = $("#frmAddStepBrinde");
    var brinde = form.serializeObject();
    return brinde;
}
function BindBrindeList() {
    $('#wdgStepBrindeResultAdicional table > tbody').html('');
    $('#wdgStepBrindeResultNormal table > tbody').html('');

    for (var i = 0; i < brindes.length; i++) {


        var htmlRow = '<tr>';
        htmlRow += '    <td Class="center">';
        htmlRow += '        <a data-rel="tooltip" title="Delete" class="btn btn-xs btn-danger" onclick="DeleteBrinde(' + i + ');">';
        htmlRow += '            <i class="ace-icon fa fa-trash-o bigger-120"></i>';
        htmlRow += '        </a>';
        htmlRow += '    </td>';
        if (brindes[i].TIPEDENIVPRMPMC != 0) {
            htmlRow += '    <td>' + brindes[i].TIPEDENIVPRMPMC + '</td>';
        }
        htmlRow += '    <td>' + brindes[i].CODPRMPMC + '</td>';
        htmlRow += '    <td>' + brindes[i].DESPRMPMC + '</td>';
        htmlRow += '    <td>' + brindes[i].QDEBDEMPLCTRPMC + '</td>';
        //htmlRow += '    <td>' + brindes[i].QDEMERVNDPRMPMC + '</td>';

        if (brindes[i].VLRUNTLIQMER != null) {
            htmlRow += '    <td>' + brindes[i].CUSTO + '</td>';
            htmlRow += '    <td>' + brindes[i].VLRUNTPCOBRT + '</td>';
            htmlRow += '    <td>' + brindes[i].PERBFCBDE + '</td>';
            htmlRow += '    <td>' + brindes[i].VLRUNTLIQMER + '</td>';
            htmlRow += '    <td>' + (brindes[i].RATEAR == 1 ? "Sim" : "Não") + '</td>';
        }
        //htmlRow += '    <td>' + brindes[i].QDEMERMAXPRMPMC + '</td>';
        //htmlRow += '    <td>' + brindes[i].SALDO + '</td>';
        //htmlRow += '    <td>' + brindes[i].MEDIA + '</td>';
        //htmlRow += '    <td>' + brindes[i].PERMERRSVPRMPMC + '</td>';
        //htmlRow += '    <td>' + brindes[i].VLRPRMPMC + '</td>';
        htmlRow += '</tr>'

        if (brindes[i].INDISRBDEADI == 0) { //Normal
            $('#wdgStepBrindeResultNormal table > tbody').append(htmlRow);
        } else {
            $('#wdgStepBrindeResultAdicional table > tbody').append(htmlRow);
        }
    }
}

function ReadEditStepBrindes() {
    return brindes;
}
function ReadEditStepComunicacao() {

    var comunicacao = [];
    $('#tblGridRegistrosComunicacao tr:has(td)').each(function (index, value) {
        if ($(value).eq(0).find('input').prop('checked') === true) {
            var newitem = { TIPCMNPMC: ($(value).eq(0).find('input[type="Hidden"][name$="TIPCMNPMC"]').val()) }
            if (newitem.TIPCMNPMC == 3) {
                newitem.CODSMBDVGPMCLIVPCO = $('#codigoSimbolo').val();
                newitem.TIPDVGPMCLIVPCO = $('[name="formaDivulgRadio"]:checked').val();
            }
            comunicacao[comunicacao.length] = newitem
        }
    });

    return comunicacao;
}

function AlertaCobrancaBrinde() {
    var CobrancaBrinde = $('#chbCobrancaBrinde').is(":checked");
    if (CobrancaBrinde === true) {
        ShowModal({ mensagem: "Brinde terá custo para o cliente", titulo: "Atenção" })
        //alert("Brinde terá custo para o cliente")
    }
}

function GetCODACOCMC() {

    var CODACOCMC = window.location.pathname.split('/')[window.location.pathname.split('/').length - 2];
    return CODACOCMC;
}

function GetNUMSEQRGRACOCMC() {

    var NUMSEQRGRACOCMC = window.location.pathname.split('/')[window.location.pathname.split('/').length - 1]
    return NUMSEQRGRACOCMC;
}


function modalBuscarItens() {
    var desMerc = $('#itemBusca').val();
    var idMerc = $('#idItemCodMer').val();
    var objeto = { CODMER: idMerc, DESMER: desMerc };

    abrirModal('ItensOrcamento', 'BuscaMercadorias', objeto, "#modalBuscaItem", terminarModalBuscarMercadoria, 'msgModalBrinde', objeto);
    //btnBuscaItens(desMerc);
}
function terminarModalBuscarMercadoria(objeto) {
    $("#idItem").val(objeto.CODMER);
    $("#DesIte").val(objeto.DESMER);
    btnBuscaItens();
}
function btnBuscaItens(mercadoria) {
    LoadingOn();
    var descItem = $('#DesIte').val();
    if (mercadoria) {
        descItem = mercadoria
    }
    var idItem = $('#idItem').val();
    if (idItem.length > 10) {
        document.getElementById("lblErroTamanhoIdMercadoria").style.display = "block";
        LoadingOff();
        return;
    }
    else {
        document.getElementById("lblErroTamanhoIdMercadoria").style.display = "none";
    }
    if (!idItem) {
        idItem = 0
    }
    $.ajax({
        url: SiteUrl + "ItensOrcamento/BuscaGridMercadoriasBrinde",
        type: 'POST',
        data: { idMerc: idItem, desMerc: descItem },
        success: function (data) {
            if (data !== "") {
                $("#resultadosBuscaItens").html(data);
                atualizaMensagem('msgBuscaMercadorias');
                if (idItem != 0) {
                    var botao = $("#merc_".concat(idItem));
                    if (botao != undefined) {
                        botao.click();
                    }
                    else {
                        LoadingOff();
                    }
                }
                else {
                    LoadingOff();
                }
            } else {
                atualizaMensagem();
                LoadingOff();
            }
        },
        error: function () {
            atualizaMensagem('msgBuscaMercadorias');
            LoadingOff();
        }
    });
}

function selecionarMercadoria(codMercadoria, desMercadoria, idtite, vlrpco) {
    $('#idItemCodMer').val(codMercadoria);
    $('#itemBusca').val(desMercadoria.substring(0, 50));

    document.getElementById("btBscIte").style.display = "none";
    document.getElementById("btTiraIte").style = "";
    document.getElementById("itemBusca").readOnly = true;
    document.getElementById("idItemCodMer").readOnly = true;
    limparCacheFiltroItens();

    $('#modalBuscaItem').modal('hide');

    var CODACOCMC = GetCODACOCMC();
    if ($('#chbCobrancaBrinde').is(':checked')) {
        $.ajax({
            url: SiteUrl + "PromocaoWizard" + "/" + "ConsultaValoreBrinde" + '/' + CODACOCMC + '/' + codMercadoria,
            type: 'GET',
            success: function (data) {
                if (data.Custo >= 0) {
                    $('#custoIncluirBrinde').val(data.Custo.toFixed(2));
                    $('#valorIncluirBrinde').val(data.Custo.toFixed(2));
                    $('#precoIncluirBrinde').val(data.ValorMedio.toFixed(2));

                    //validaPerc();
                } else {
                    $('#custoIncluirBrinde').val(0.00);
                    $('#valorIncluirBrinde').val(0.00);
                    $('#precoIncluirBrinde').val(0.00);
                    ShowModal({ mensagem: "O item não é principal nem substituto nesta ação comercial.", titulo: "Atenção" })
                    tirarItem();
                }
                LoadingOff();
                MaskConfig();
            },
            error: function (data) {
                tirarItem();
                LoadingOff();
                ShowModal({ mensagem: "Não foi possível carregar os valores do brinde.", titulo: "Atenção" })
            }
        });
    }
}

function liberarCallbackWizard() {
    atualizaMensagem();
}



function RefreshStepResumo() {

    var CODACOCMC = GetCODACOCMC();
    var NUMSEQRGRACOCMC = GetNUMSEQRGRACOCMC();

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    };
    LoadingOn();
    postAjax('PromocaoWizard', 'RefreshStepResumo', data,
        function (data) {
            if (data != "") {
                $('#divStepPane5').html(data);
            }
            atualizaMensagem();
            LoadingOff();
        },
        function (error) {
            //Window.html(error.responseText);
            atualizaMensagem();
            LoadingOff();
        }
    );
}



function ckbCrterioAdicional_OnClick() {

    var criterioAdicional = $('#ckbCrterioAdicional').is(":checked");

    $('#divVendaAvaliacaoPromocaoCres').hide();
    $('#divValorLimite').hide();
    $('#divQtdMin').hide();
    if (criterioAdicional === true) {
        $('#divVendaAvaliacaoPromocaoCres').show();
        $('#divValorLimite').show();
        $('#divQtdMin').show();
    }
    else {
        $('#divVendaAvaliacaoPromocaoCres').hide();
        $('#divValorLimite').hide();
        $('#divQtdMin').hide();
    }
}


// Configurações de serialização de dados de formularios
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};



function resizeModalPromocao(modal) {
    var screenHeight = window.innerHeight;

    modal.height(screenHeight - 10);
    modal.find('.modal-body').height(screenHeight - 140);
}




function ClonarPromocoesDaAcao(CODACOCMC) {

  



    ShowModalConfirmar({
        titulo: 'Confirmar',
        mensagem: "Você quer clonar as promoções com base na ação original",
        appendElement: "msgModalPromoção",
        callback: function () {

            LoadingOn();
            $.ajax({
                url: SiteUrl + "PromocaoWizard/ClonarPromocoesDaAcao",
                type: 'POST',
                data: { CODACOCMC: CODACOCMC },
                success: function (data) {

                    $('#modalResumoCloneProcoesDaAcao').html(data);
                    $('#modalResumoCloneProcoesDaAcaoInner').modal({ show: true, backdrop: 'static' }).on('shown.bs.modal', function () {
                        ChosenResize();
                    });

                    atualizaMensagem('msgGeralResumoCloneProcoesDaAcao');

                    LoadingOff();                    
                },
                error: function () {
                    atualizaMensagem('msgModalPromoção');
                    LoadingOff();
                }
            });
        },
        parametros: null
    });
}
function FecharModalResumoCloneProcoesDaAcao() {
    window.location.reload();
}

function ckbPromocaoRelampago_OnClick(e) {
    if (e == true) {
        $('.horas').show();
        $('#divDataInicio').removeClass('col-sm-4').addClass('col-sm-3')
        $('#divDataFim').removeClass('col-sm-4').addClass('col-sm-3')
    }
    else {
        $('.horas').hide();
        $('#divDataInicio').removeClass('col-sm-3').addClass('col-sm-4')
        $('#divDataFim').removeClass('col-sm-3').addClass('col-sm-4')
    }
}

function validarHoras() {
    var promocao = ReadEditStepGeral()

    if ($('#ckbPromocaoRelampago').is(':checked')) {
        var isValidHrIni = /^([0-1][0-9]|2[0-3]):([0-5][0-9])$/.test(promocao.HRAINIPMC)
        if (!isValidHrIni) {
            ShowModal({ mensagem: "Favor preencher o campo Hr. Inicio com um valor válido.", titulo: "Atenção" });
            return false;
        }

        var isValidHrFim = /^([0-1][0-9]|2[0-3]):([0-5][0-9])$/.test(promocao.HRAFIMPMC)
        if (!isValidHrFim) {
            ShowModal({ mensagem: "Favor preencher o campo Hr. Fim com um valor válido.", titulo: "Atenção" });
            return false;
        }

        var diffDays = moment.duration(moment(promocao.DATFIMPMC, "DD/MM/YYYY").diff(moment(promocao.DATINIPMC, "DD/MM/YYYY"))).asDays()
        if (diffDays > 3) {
            ShowModal({ mensagem: "Para Promoção Relâmpago, o campo Dt. de Fim não pode ter mais de 3 dias de diferença para o campo Dt. de Início.", titulo: "Atenção" });
            return false;
        }
    }
    return true;
}