﻿// Mostra ou esconde os campos de filtro de fornecedor/pedido
function dropFiltroPedidos_onChange(idFiltro) {
    if (idFiltro == 2) {
        $('#divFiltroPedido').show();
        $('#divFiltroFornecedor').hide();
    } else {
        $('#divFiltroPedido').hide();
        $('#divFiltroFornecedor').show();
    }
    ChosenResize();
}
// Evento change do drop de Fornecedor para atribuir o codigo ao textBox correspondente
function dropFornecedorBonificacao_onChange() {
    $('#filtroCodFrn').val($('#dropFiltroFornecedores').val());
}

// Atualiza a mensagem do modal vinda do controller ou da propria tela
function atualizaMensagemModalBonificacao(mensagem, tipo) {
    if (!!mensagem) {
        mostraMensagem(mensagem, tipo, "msgModalBonificacao");
    } else {
        atualizaMensagem("msgModalBonificacao");
    }
}

// Busca fornecedores e atualiza o dropdown com os valores da busca
function buscarFornecedoresPedidoBonificacao() {
    var codFornecedor = $('#filtroCodFrn').val();

    postAjax("Fornecedor", "BuscarFornecedores", { codFrn: codFornecedor },
        function (data) {
            carregarDropDinamicamente("dropFiltroFornecedores", "CODFRN", "CodNomFrn", data, true);
            $('#filtroCodFrn').val($('#dropFiltroFornecedores').val());
            atualizaMensagemModalBonificacao();
            LoadingOff();
        }, function () {
            atualizaMensagemModalBonificacao();
            LoadingOff();
        }
    );
}

// Busca fornecedores e atualiza o dropdown com os valores da busca
function filtrarPedidosDeCompra() {
    var codFiltro = "";
    var existeFiltro = false;

    if ($('#dropFiltroPedidos').val() == 2) {
        codFiltro = "numPed=" + $('#filtroNumPed').val();
        existeFiltro = $('#filtroNumPed').val() != "";
    } else {
        codFiltro = "codFrn=" + $('#filtroCodFrn').val();
        existeFiltro = $('#filtroCodFrn').val() != "";
    }

    if (!existeFiltro) {
        atualizaMensagemModalBonificacao("INFORME_FILTRO", "msgWarning");
    } else {
        postAjaxUrlData("PedidoBonificacao", "FiltrarPedidosCompra", codFiltro,
            function (data) {
                if (data !== "") {
                    $('#gridPedidosCompraModal').html(data);
                }
                atualizaMensagemModalBonificacao();
                LoadingOff();
            }, function () {
                atualizaMensagemModalBonificacao();
                LoadingOff();
            }
        );
    }
}

// Retorna a lista dos ids de pedidos checkados de um tipo de checkBox
function retornaListaDeValoresCheckados(idCheck) {

    var itensChecados = [];
    var size = idCheck.length;

    $('[id^="' + idCheck + '"]').each(function () {
        var $this = $(this);

        if ($this.is(":checked")) {
            itensChecados.push($this.attr("id").substr(size));
        }
    });

    return itensChecados;
}

// Pega a lista de itens checkados e coloca na lista de pedidos selecionados
function selecionarPedidos() {
    var itens = retornaListaDeValoresCheckados("ckbIncPed");

    if (itens.length == 0) {
        atualizaMensagemModalBonificacao("SELECIONAR_PEDIDO", "msgWarning");
    } else {

        $('[id^="ckbIncPed"]').prop("checked", false);

        postAjax("PedidoBonificacao", "SelecionarPedidos", { itensCheckados: itens },
            function (data) {
                if (data !== "") {
                    $('#gridPedidosBonificacaoModal').html(data);
                }
                atualizaMensagemModalBonificacao();
                LoadingOff();
            }, function () {
                atualizaMensagemModalBonificacao();
                LoadingOff();
            }
        );
    }
}

// Pega a lista de itens selecionados checkados e remove do grid
function removerPedidos() {
    var itens = retornaListaDeValoresCheckados("ckbExPed");

    if (itens.length == 0) {
        atualizaMensagemModalBonificacao("SELECIONAR_PEDIDO", "msgWarning");
    } else {
        $('[id^="ckbExPed"]').prop("checked", false);

        postAjax("PedidoBonificacao", "RemoverPedidos", { itensCheckados: itens },
            function (data) {
                if (data !== "") {
                    $('#gridPedidosBonificacaoModal').html(data);
                }
                atualizaMensagemModalBonificacao();
                LoadingOff();
            }, function () {
                atualizaMensagemModalBonificacao();
                LoadingOff();
            }
        );
    }
}

// Atualiza o valor utilizado da linha do grid apos edicao
function atualizaValorUlitizadoCampo(idHid, idText) {
    var valorUtilizado = $('#' + idText).val();
    valorUtilizado = valorUtilizado.replace(/\./g, '');
    var numPedido = $('#' + idHid).val();

    postAjax("PedidoBonificacao", "AtualizaValorUlitizadoCampo", { valores: [numPedido, valorUtilizado] },
        function (data) {
        }, function () {
            atualizaMensagemModalBonificacao();
        },
        true
    );
}

// Acao do botao de concluir a insercao de pedidos
function concluirInsercaoPedidos() {

    postAjax("PedidoBonificacao", "ValidarPedidosBonificacao", {},
        function (data) {
            if (data == 0) {
                atualizaMensagemModalBonificacao();
            } else {
                $('#modal').modal('hide');
                $('#gridBonificacoes').html(data);
                atualizaMensagem();
            }
            LoadingOff();
        }, function () {
            atualizaMensagemModalBonificacao();
            LoadingOff();
        }
    );

}

// Remove pedido da lista de pedidos na tela de acao
function removePedido(idPedido) {

    postAjax("PedidoBonificacao", "RemoverPedido", { numPed: idPedido },
        function (data) {
            if (data == 0) {
                atualizaMensagemModalBonificacao();
            } else {
                $('#modal').modal('hide');
                $('#gridBonificacoes').html(data);
                atualizaMensagem();
            }
            LoadingOff();
        }, function () {
            atualizaMensagemModalBonificacao();
            LoadingOff();
        }
    );

}
