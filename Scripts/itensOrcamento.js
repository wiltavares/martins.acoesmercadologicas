﻿function AtualizaDropBU() {
    $.ajax({
        url: SiteUrl + "ItensOrcamento/AtualizaDropBU",
        type: 'POST',
        success: function (data) {
            carregarDropDinamicamente("drupBU", "CODUNDESRNGC", "DROPDESC", data, true);
        },
        error: function () {
            atualizaMensagem();
        }
    });
}

function ListarParametrosAcaoComercial() {
    $.ajax({
        url: SiteUrl + "ItensOrcamento/AtualizaDropBU",
        type: 'POST',
        success: function (data) {
            var ParVlrMinIte = data.STRVLRMNMITERSU;//variavel que deve ser utilizada durante clonagem de orçamento
        },
        error: function () {
            atualizaMensagem();
        }
    });
}

function btnBuscaCentroCusto() {
    var codCentroCusto = $('#centroCustoID').val();
    if (codCentroCusto == "") {
        codCentroCusto = 0;
    }
    var desCentroCusto = '';// $('#centroCustoDES').val();
    $.ajax({
        url: SiteUrl + "ItensOrcamento/BuscaCentroCusto",
        type: 'POST',
        data: { codCentroCusto: codCentroCusto, desCentroCusto: desCentroCusto },
        success: function (data) {
            carregarDropDinamicamente("centroCustoDES", "CODCENCST", "DESCENCST", data, true);
            if (data.length > 0) {
                $('#centroCustoID').val(data[0].CODCENCST);
                $('#centroCustoDES').val(data[0].DESCENCST);
            }
        },
        error: function () {
            atualizaMensagem();
        }
    });
}


function centroCustoDES_onchange() {
    $('#centroCustoID').val($("#centroCustoDES").val());
}

function btnBuscaItens(mercadoria) {
    LoadingOn();
    var descItem = $('#DesIte').val();
    if (mercadoria) {
        descItem = mercadoria
    }
    var idItem = $('#idItem').val();
    if (idItem.length > 10) {
        document.getElementById("lblErroTamanhoIdMercadoria").style.display = "block";
        LoadingOff();
        return;
    }
    else {
        document.getElementById("lblErroTamanhoIdMercadoria").style.display = "none";
    }
    var codOperacao = document.getElementById("dropOperacao").value;
    var patrociador = 1;
    if (document.getElementById("rdbFornecedor").checked) {
        patrociador = 1
    }
    if (document.getElementById("rdbCarimbo").checked) {
        patrociador = 2
    }
    if (document.getElementById("rdbBonificacao").checked) {
        patrociador = 3
    }
    if (document.getElementById("rdbPremiacao").checked) {
        patrociador = 4
    }
    var filial = document.getElementById("dropFilial").value;

    if (!idItem) {
        idItem = 0
    }

    $.ajax({
        url: SiteUrl + "ItensOrcamento/buscaGridMercadorias",
        type: 'POST',
        data: { idMerc: idItem, desMerc: descItem, codOperacao: codOperacao, codFilial: filial, patrocinador: patrociador },
        success: function (data) {
            if (data !== "") {
                $("#resultadosBuscaItens").html(data);
                atualizaMensagem('msgBuscaMercadorias');
                if (idItem != 0) {
                    var botao = $("#merc_".concat(idItem));
                    if (botao != undefined) {
                        botao.click();
                    }
                }
            } else {
                atualizaMensagem();
            }
            LoadingOff();
        },
        error: function () {
            atualizaMensagem('msgBuscaMercadorias');
            LoadingOff();
        }
    });
}

function limparCacheFiltroItens() {
    $.ajax({
        url: SiteUrl + "ItensOrcamento/LimparCacheFiltro",
        type: 'POST',
        data: {},
        success: function (data) {
            $('#modalBuscaItem').modal('hide');
        },
        error: function () {
            atualizaMensagem(divMsg);
        }
    });
}

function tirarItem(INDACOCMCBTBVLR) {

    if (INDACOCMCBTBVLR == 3) { //B2B BUDGET UNICO
        document.getElementById("idItemCodMer").readOnly = true;
        $("#idItemCodMer").val('999999999');

        document.getElementById("itemBusca").readOnly = true;
        $("#itemBusca").val('BUDGET UNICO');

        document.getElementById("btBscIte").disabled = true;


        document.getElementById("VlrUntario").style.display = "none";
        document.getElementById("VlrUntario").disabled = true;
        document.getElementById("VlrUntario2").style.display = "block";
        document.getElementById("VlrUntario2").disabled = false;

        $("#QtdUtz").val('1');
        document.getElementById("QtdUtz").readOnly = true;

        document.getElementById("VlrpagoCliente").readOnly = true;
    }
    else {
        $('#idItemCodMer').val("");
        $('#itemBusca').val("");

        document.getElementById("btBscIte").style = "";
        document.getElementById("btTiraIte").style.display = "none";
        document.getElementById("itemBusca").readOnly = false;
        document.getElementById("idItemCodMer").readOnly = false;

        $('#VlrUntario2').val("0,00");
        $('#VlrUntario').val("0,0000");
        $('#VlrpagoForn').val("0,0000");
        $('#TotIte').val("0,0000");
        $('#QtdUtz').val("0");


        $('#custoIncluirBrinde').val(0);
        $('#valorIncluirBrinde').val(0);
        $('#precoIncluirBrinde').val(0);
    }
}

function modalBuscarItens() {
    var desMerc = $('#itemBusca').val();
    var idMerc = $('#idItemCodMer').val();
    var objeto = { CODMER: idMerc, DESMER: desMerc };

    abrirModal('ItensOrcamento', 'BuscaMercadorias', objeto, "#modalBuscaItem", terminarModalBuscarMercadoria, 'msgModalOrcamento', objeto);
    //btnBuscaItens(desMerc);
}

function terminarModalBuscarMercadoria(objeto) {
    $("#idItem").val(objeto.CODMER);
    $("#DesIte").val(objeto.DESMER);
    btnBuscaItens();
}

function verificaQtDiasPositivo() {
    if (parseInt($('#diasParaComprar').val()) < 0) {
        $('#diasParaComprar').val("0");
    }
}

function suprimentos() {
    var codAcao = $('#codAcao').val();
    var codMercadoria = $('#idItemCodMer').val();
    LoadingOn();

    //codAcao = '203205'//valores de teste
    //codMercadoria = '2295'//valores de teste
    if (document.getElementById("DescDropSuprimentos").readOnly) {
        $.ajax({
            url: SiteUrl + "ItensOrcamento/Suprimentos",
            type: 'POST',
            data: { codMercadoria: codMercadoria, codAcao: codAcao },
            success: function (data) {
                if (data !== "") {
                    $("#modalSuprimentos").html(data);
                    $("#modalSuprimentos").modal();
                    LoadingOff();
                } else {
                    atualizaMensagem("msgModalOrcamento");
                    LoadingOff();
                }
            },
            error: function () {
                atualizaMensagem("msgModalOrcamento");
                LoadingOff();
            }
        });
    }
}

function sairModalSuprimentos() {
    $('#modalSuprimentos').modal('hide');
}


function AtuCbIteCsm(CodIte, DesIte) {
    var codOperacao = $('#dropOperacao').val();
    var filial = $('#dropFilial').val();
    var codInteiro = 0
    codInteiro = CodIte

    $.ajax({
        url: SiteUrl + "ItensOrcamento/BuscaMercadorias",
        type: 'POST',
        data: { idMerc: codInteiro, desMerc: DesIte, codOperacao: codOperacao, codFilial: filial, patrocinador: 0 },
        success: function (data) {
            carregarDropDinamicamente("IteOcd", "COD", "DROPDESC", data, true);
        },
        error: function () {
            atualizaMensagem();
        }
    });
}


function fecharModalItens() {

    var patrocinador
    if (document.getElementById("rdbFornecedor").checked) {
        patrocinador = 1
    }
    if (document.getElementById("rdbCarimbo").checked) {
        patrocinador = 2
    }
    if (document.getElementById("rdbBonificacao").checked) {
        patrocinador = 3
    }
    if (document.getElementById("rdbPremiacao").checked) {
        patrocinador = 4
    }
    var filial = document.getElementById("dropFilial").value;

    var form = $("#MercadoriaCompleta");

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    var data = form.serializeObject();

    $.ajax({
        url: SiteUrl + "ItensOrcamento/fecharModalItens",
        type: 'POST',
        data: { mercadoria: data, patrocinador: patrocinador },
        success: function (data) {
            if (data) {
                document.getElementById("#divCanalDeVenda").style = "";
            }
            else {
                document.getElementById("#divCanalDeVenda").style.display = "none";
            }
        },
        error: function () {
            atualizaMensagem();
        }
    });
}

function Salvar() {

    if (validaDadosSalvar()) {
        var patrocinador = 1;
        if (document.getElementById("rdbFornecedor").checked) {
            patrocinador = 1
        }
        if (document.getElementById("rdbCarimbo").checked) {
            patrocinador = 2
        }
        if (document.getElementById("rdbBonificacao").checked) {
            patrocinador = 3
        }
        if (document.getElementById("rdbPremiacao").checked) {
            patrocinador = 4
        }
        var filial = document.getElementById("dropFilial").value;

        var dtUtilizacao = document.getElementById("dtUtilizacao").value

        var form = $("#MercadoriaCompleta");

        $.fn.serializeObject = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };

        var viapp = false
        if (document.getElementById("viaApp").checked == true) {
            viapp = true;
        }
        var fornecedorunico = false
        if (document.getElementById("fornecedorUnico").checked == true) {
            fornecedorunico = true
        }
        var data = form.serializeObject();

        if ($('#VlrUntario2').is(":visible")) {
            data.VLRUNTITEOCDACOCMC = $('#VlrUntario2').val();
        } else if ($('#VlrUntario').is(":visible")) {
            data.VLRUNTITEOCDACOCMC = $('#VlrUntario').val();
        }
        //data.VLRPTCFRNITEACOCMC = data.VLRPTCFRNITEACOCMC.replace('.', ',')
        //data.VLRUNTITEOCDACOCMC = data.VLRUNTITEOCDACOCMC.replace('.', ',')

        if (document.getElementById("dtUtilizacao").readOnly || document.getElementById("divItem").style.display == "" || document.getElementById("divdropItem").style.display == "") {
            data.DATUTZITEACOCMC = dtUtilizacao
        }

        if (document.getElementById("fornecedorUnico").checked) {
            data.INDITERSUFRNUNC = 0
        }
        else {
            data.INDITERSUFRNUNC = 0
        }

        if (document.getElementById("viaApp").checked) {
            data.INDITERSUARZPRVPGT = 0
        }
        else {
            data.INDITERSUARZPRVPGT = 0
        }

        if ($('#dropOperacao').prop("disabled") == true || document.getElementById("divItem").style.display == "" || document.getElementById("divdropItem").style.display == "") {
            data.INDTIPOPEDSNACOCMC = $('#dropOperacao').val();
        }
        if ($('#dropFilial').prop("disabled") == true || document.getElementById("divItem").style.display == "" || document.getElementById("divdropItem").style.display == "") {
            data.CODFILEMP = $('#dropFilial').val();
        }

        if (document.getElementById("divdropItem").style.display == "") {
            data.NOMITEMORCDROP = $('#nomItemOrcDrop').val();
        }

        if (data.INDTIPOPEDSNACOCMC == 6 || data.INDTIPOPEDSNACOCMC == 7 || data.INDTIPOPEDSNACOCMC == 10 || data.INDTIPOPEDSNACOCMC == 13 ||
            data.INDTIPOPEDSNACOCMC == 15 || data.INDTIPOPEDSNACOCMC == 16 || data.INDTIPOPEDSNACOCMC == 17) {
            data.VLRPTCFRNITEACOCMC = data.VLRUNTITEOCDACOCMC
        }

        LoadingOn();
        $.ajax({
            url: SiteUrl + "ItensOrcamento/Salvar",
            type: 'POST',
            data: { mercadoria: data, fornecedorUnico: fornecedorunico, viaApp: viapp, patrocinador: patrocinador },
            success: function (data) {
                if (data) {

                    $('#divWidgetOrcamento').html(data);
                    $('#modal').modal('hide');
                    atualizaMensagem();
                }
                else {
                    atualizaMensagem("msgModalOrcamento");

                    $('#dropOperacao').prop("disabled", false);
                    $('#dropFilial').prop("disabled", false);
                    $('#drupBU').prop("disabled", false);
                    $('#drupBU').trigger("chosen:updated");
                    $('#dropOperacao').trigger("chosen:updated");
                    $('#dropFilial').trigger("chosen:updated");
                    document.getElementById("QtdUtz").readOnly = false;
                    document.getElementById("btTiraIte").disabled = false;
                    document.getElementById("VlrpagoCliente").readOnly = false;
                    document.getElementById("FormaUtlz").readOnly = false;

                    //  document.getElementById("FormaUtlz").disabled = false;
                    document.getElementById("btBscIte").disabled = false;
                    $('#btnAlterar').prop("disabled", false).trigger("chosen:updated");
                    document.getElementById("lblErroTemItensSubs").style.display = "none";

                }
                //AtualizarCanalDeVenda();
                //$('#modal').modal('hide');

                //LoadingOff();
            },
            error: function () {
                atualizaMensagem();
                LoadingOff();
            }
        });
    }
    LoadingOff();
}


function validaDadosSalvar() {

    var validade = true;

    if ($('#dropOperacao').val() == 0 || $('#dropOperacao').val() == 0) {
        document.getElementById("lblErroOperacao").style.display = "block";
        validade = false;
    }
    else {
        document.getElementById("lblErroOperacao").style.display = "none";
    }
    if ($('#idItemCodMer').val() == 0 && document.getElementById("dividItem").style.display == "" || $('#idItemCodMer').val() == '' && document.getElementById("dividItem").style.display == "" || document.getElementById("divItem").style.display == "" && $('#idDesMerc').val() == '' || document.getElementById("divdropItem").style.display == "" && $('#nomItemOrcDrop').val() == '') {
        document.getElementById("lblErroMercadoria").style.display = "block";
        validade = false;
    }
    else {
        document.getElementById("lblErroMercadoria").style.display = "none";
    }
    if ($('#QtdUtz').val() == '' || $('#QtdUtz').val() == 0) {
        document.getElementById("lblErroQuantidade").style.display = "block";
        validade = false;
    }
    else {
        document.getElementById("lblErroQuantidade").style.display = "none";
    }
    if ($('#FormaUtlz').val() == '') {
        document.getElementById("lblErroFormaUtlz").style.display = "block";
        validade = false;
    }
    else {
        document.getElementById("lblErroFormaUtlz").style.display = "none";
    }
    if ($('#QtdUtz').val() == '' || $('#QtdUtz').val() > '9999999999') {
        validade = false;
        atualizaMensagemModalItens("VALOR_TOTAL_MAXIMO_ITENS_ORCAMENTO", "msgWarning");
    }

    if ($('#VlrUntario2').is(":visible")) {
        var vlrUntario = parseFloat($('#VlrUntario2').val().replace(/\./g, "").replace(',', '.'));
    } else {
        var vlrUntario = parseFloat($('#VlrUntario').val().replace(/\./g, "").replace(',', '.'));
    }

    if (vlrUntario * $('#QtdUtz').val() == 0) {
        document.getElementById("lblErroTotalItem").style.display = "block";
        validade = false;
    }
    else {
        document.getElementById("lblErroTotalItem").style.display = "none";
    }

    return validade;
}

function mostraCamposOrcamento(operacao, gridOrcamento, tipoEvento, INDACOCMCBTBVLR) {

    // $('#QtdUtz').val("0");
    // $('#VlrUntario').val("0,00");
    // //$('#TotIte').val("0,00");
    // $('#ObsComprador').val("");
    // var ParVlrMinIte;

    //document.getElementById("divfornecedorunico").style.display = "none";
    //document.getElementById("fornecedorUnico").checked = false;
    //document.getElementById("divviaApp").style.display = "none";
    //document.getElementById("viaApp").checked = false;

    document.getElementById("formaPgtDiv").style.display = "none";
    //tirarFormPgt();
    document.getElementById("contratoFornecedorDiv").style.display = "none";
    //tirarFornecedorUnico();
    //document.getElementById("cgcFornecedorUnicoDiv").style.display = "none";
    $('#numCgcEmpFrnc').val("");
    $('#idtEmpFrnIIteRsu').val("");

    document.getElementById("ObsComprador").style.display = "none";
    $('#ObsComprador').val("");
    document.getElementById("txtObsComprador").style.display = "block";

    $('#idItemCodMer').val("");
    $('#DesIte').val("");


    if (operacao.INDPTCCLIOPEDSN == 1) {
        document.getElementById("VlrpagoForn").style.display = "block";
        document.getElementById("VlrpgoForn").style.display = "block";
        document.getElementById("VlrpagoCliente").style.display = "block";
        document.getElementById("VlrpgoCliente").style.display = "block";
    }
    else {
        document.getElementById("VlrpagoForn").style.display = "none";
        document.getElementById("VlrpgoForn").style.display = "none";
        document.getElementById("VlrpagoCliente").style.display = "none";
        document.getElementById("VlrpgoCliente").style.display = "none";
        $('#VlrpagoForn').val("");
        $('#VlrpgoForn').val("");
        $('#VlrpagoCliente').val("");
        $('#VlrpgoCliente').val("");
    }
    if (operacao.INDTIPOPEDSNACOCMC == 1) {

        //document.getElementById("btCalcPrec").style.display = "block";
        $('#VlrUntario').val("0,00");
        if (document.getElementById("VlrUntario") != null) {
            document.getElementById("VlrUntario").style.display = "block";
            document.getElementById("VlrUntario").disabled = false;
        }
        if (document.getElementById("VlrUntario2") != null) {
            document.getElementById("VlrUntario2").style.display = "none";
            document.getElementById("VlrUntario2").disabled = true;
        }
        //document.getElementById("VlrUntario").readOnly = true;
        document.getElementById("divItem").style.display = "none";
        $('#IteOcd').val("");

        $('#DesIte').val("0");
        $('#idItemCodMer').val("");
        document.getElementById("dividItem").style = "";

        //$('#TotIte').val("0,00");
        $('#formaUtilizacao').val("");
        document.getElementById("dtUtilizacao").readOnly = true;

        $('#diasParaComprar').val("");
        document.getElementById("diasParaComprar").style.display = "none";
        document.getElementById("txtDiasParaComprar").style.display = "none";
        $('#txtDiasParaComprar').val("");
    }
    else {
        //Botão calcular é desabilitado. O calculo é feito no lost focus
        //btCalPcoUntIte.Visible = False

        carregarDropDinamicamente("DesIte", "", "", {}, true);//deve limpar o drop

        //Habilita campo de Valor Unitario para usuário alterar.
        document.getElementById("VlrUntario").style.display = "none";
        document.getElementById("VlrUntario").disabled = true;
        document.getElementById("VlrUntario2").style.display = "block";
        document.getElementById("VlrUntario2").disabled = false;

        document.getElementById("dtUtilizacao").readOnly = false;  //habilita o campo da data de utilização

        if (operacao.INDGRCREQAUTITERSU == 1 || operacao.INDTIPOPEREQVGM == 1 || operacao.INDTIPOPEREQEVT == 1 || operacao.INDTIPOPESLCPGT == 1) {//parte para aumentar tamanhodo campo de texto, provavelmente sera reitrada
            //if (operacao.INDGRCREQAUTITERSU == 1) {
            //    //TxtFrmUtz.Height = 90;
            //}
            //else {
            //    //TxtFrmUtz.Height = 156
            //}

            if (operacao.INDGRCREQAUTITERSU == 1) { //Obtem parametro de valor minimo
                //ListarParametrosAcaoComercial();//on sucess seta variavle utilizada na função de clonar orçamento

                document.getElementById("divfornecedorunico").style.display = "block";
                //document.getElementById("fornecedorUnico").checked = false;
                document.getElementById("divviaApp").style.display = "block";
                //document.getElementById("viaApp").checked = false;
            }

            document.getElementById("ObsComprador").style.display = ((operacao.INDGRCREQAUTITERSU == 1) ? "block" : "none");
            document.getElementById("txtObsComprador").style.display = ((operacao.INDGRCREQAUTITERSU == 1) ? "block" : "none");
            document.getElementById("txtDiasParaComprar").style.display = ((operacao.INDGRCREQAUTITERSU == 1) ? "block" : "none");
            document.getElementById("diasParaComprar").style.display = ((operacao.INDGRCREQAUTITERSU == 1) ? "block" : "none");
            document.getElementById("divItem").style.display = "none";
            //$('#IteOcd').val("");
        }
        else {
            //$('#diasParaComprar').val("");
            document.getElementById("diasParaComprar").style.display = "none";
            document.getElementById("txtDiasParaComprar").style.display = "none";
            //$('#txtDiasParaComprar').val("");

        }

        // $('#FormaUtlz').val("");
        // $('#VlrUntario').val("0,00");
        // //$('#TotIte').val("0,00");
        // $('#idDesMerc').val("");
    }

    //Validação dos tipos de operações 10, 13, 15, 16 e 18 vão exibir um campo texto.
    if (operacao.CODOPEFSCDSNACOCMC == 10 || operacao.CODOPEFSCDSNACOCMC == 13 || operacao.CODOPEFSCDSNACOCMC == 15 ||
        operacao.CODOPEFSCDSNACOCMC == 16 || operacao.CODOPEFSCDSNACOCMC == 18) {
        document.getElementById("dividItem").style.display = "none";
        document.getElementById("divdropItem").style.display = "none";
        document.getElementById("divItem").style = "";

    } else if (operacao.CODOPEFSCDSNACOCMC == 6 || operacao.CODOPEFSCDSNACOCMC == 7 ||
        operacao.CODOPEFSCDSNACOCMC == 17) { //Validação dos tipos de operações 6, 7 e 17 vão exibir um DropDownList.
        document.getElementById("dividItem").style.display = "none";
        document.getElementById("divItem").style.display = "none";
        document.getElementById("divdropItem").style = "";

    }
    else //As outras operações deve exibir o código do item, nome do item e botão para busca do item.
    {
        document.getElementById("divItem").style.display = "none";
        document.getElementById("divdropItem").style.display = "none";
        document.getElementById("dividItem").style = "";
        document.getElementById("VlrUntario2").disabled = true;
        //$('#idDesMerc').val("");
    }

    if (INDACOCMCBTBVLR == 3) { //B2B BUDGET UNICO
        document.getElementById("idItemCodMer").readOnly = true;
        document.getElementById("itemBusca").readOnly = true;
        document.getElementById("btBscIte").disabled = true;
        document.getElementById("btTiraIte").disabled = true;
        document.getElementById("VlrUntario").style.display = "none";
        document.getElementById("VlrUntario").disabled = true;
        document.getElementById("VlrUntario2").style.display = "block";
        document.getElementById("VlrUntario2").disabled = false;
        document.getElementById("QtdUtz").readOnly = true;
        document.getElementById("VlrpagoCliente").readOnly = true;
    }

    if (operacao.INDUNDESRNGCACOCMC == 1) {

        if (gridOrcamento.TotalCount > 0) { // verifica se existe algum item incluído
            var orcamento;
            for (orcamento in gridOrcamento.ListPaged) { //percorre todos os itens já inseridos
                //verifica se o há algum tipo de operação igual ao tipo de operação escolhido
                var codOperacao = $('#dropOperacao').val();
                if (codOperacao == orcamento.CODOPEFSCDSNACOCMC) {
                    //mantem o mesmo BU para tipos de operação iguais 
                    $('#dropOperacao').val(orcamento.CODUNDESRNGCOPEDSN);
                    break;
                }
            }

            if (!$('#drupBU').val()) {
                //se não houver nenhum  tipo de operação escolhido igual ao escolhido atual
                AtualizaDropBU(); // preenche o combo de unidade de negócio
            }
        }
        else {
            AtualizaDropBU();
        }
    }
    else {
        carregarDropDinamicamente("drupBU", "CODUNDESRNGC", "DROPDESC", undefined, true);
    }

    if ($('#dropOperacao').val() > -1) {
        var skillsSelect = document.getElementById("dropOperacao");
        var selectedText = skillsSelect.options[skillsSelect.selectedIndex].text;
        var valor = selectedText.split("-")[0].trim();
        if ((valor == 2 || valor == 4) && tipoEvento.INDCPHBTB !== 1) {
            document.getElementById("btItensSubstitutos").style.display = "block";
        }
        else {
            document.getElementById("btItensSubstitutos").style.display = "none";
        }
    }
    else {
        document.getElementById("btItensSubstitutos").style.display = "none";
    }

    //parte para habilitar botao gasto
    if ((operacao == 0 || operacao == 14) && CODSTAAPVACOCMC_chosen == "APROVADA" && $('#IteOcd').val().split("-")[0].trim() == "0") {
        document.getElementById("btGasto").style.display = "block";
    }
    else {
        document.getElementById("btGasto").style.display = "none";
    }

    if ($('#fornecedorUnico').is(":checked") || $('#viaApp').is(":checked")) { // se via fornecedor unico ou app
        document.getElementById("contratoFornecedorDiv").style.display = "block";
        document.getElementById("formaPgtDiv").style.display = "block";
        GetInfoFornecedorUnicoItemOrcamento();
        GetInfoFormaPagamentoItemOrcamento();
    }
    else {
        document.getElementById("formaPgtDiv").style.display = "none";
        document.getElementById("contratoFornecedorDiv").style.display = "none";
    }
}

function carregaDropFilial() {
    var codOperacao = $('#dropOperacao').val();

    if (codOperacao != "") {
        LoadingOn();

        $.ajax({
            url: SiteUrl + "ItensOrcamento/TipoOperacaoChange",
            type: 'POST',
            data: { codOperacao: codOperacao },
            success: function (data) {
                carregarDropDinamicamente("dropFilial", "CODFILEMP", "DROPDESC", data.filiais, true);
                mostraCamposOrcamento(data.operacao, data.gridOrcamento, data.tipoEvento, data.INDACOCMCBTBVLR);
                tirarItem(data.INDACOCMCBTBVLR);
                LoadingOff();
            },
            error: function () {
                tirarItem();
                LoadingOff(0);
                atualizaMensagem();
            }
        });
    }
}

function carregaDropFilialItensOrcamento() {
    var codOperacao = $('#dropOperacao').val();

    if (codOperacao != "") {
        LoadingOn();

        $.ajax({
            url: SiteUrl + "ItensOrcamento/TipoOperacaoChangeItensOrcamento",
            type: 'POST',
            data: { codOperacao: codOperacao },
            success: function (data) {
                //carregarDropDinamicamente("dropFilial", "CODFILEMP", "DROPDESC", data.filiais, true);
                LoadingOff();
                if (data.operacao.INDUNDESRNGCACOCMC == 1) {
                    $('#DropBUImportacao').show();
                    ChosenResize();
                    AtualizaDropBU();
                }
                else {
                    $('#DropBUImportacao').hide();
                    $('#DropBUImportacao').val('');
                }
            },
            error: function () {
                LoadingOff();
                atualizaMensagem();
            }
        });
    }
}
function calculaTotalItem() {
    var vlrUnidade;
    if (document.getElementById("VlrUntario").disabled) {
        vlrUnidade = parseFloat($('#VlrUntario2').val().replace(/\./g, "").replace(',', '.'));
        $('#VlrUntario2').val(vlrUnidade.toFixed(2));
    }
    else {
        vlrUnidade = parseFloat($('#VlrUntario').val().replace(/\./g, "").replace(',', '.'));
    }
    var qtUtilizado = parseFloat($('#QtdUtz').val());
    if (qtUtilizado > 0) {
        $('#TotIte').val((vlrUnidade * qtUtilizado).toFixed(4));
        vlrpagoCliente = parseFloat($('#VlrpagoCliente').val().replace(/\./g, "").replace(',', '.'));
        if(isNaN(vlrpagoCliente))
            vlrpagoCliente = 0;
        $('#VlrpagoForn').val((vlrUnidade - vlrpagoCliente).toFixed(4));
    }
    else {
        $('#QtdUtz').val("0");
    }
    LoadingOff();
}

function SalvarAlteracao() {
    var patrocinador = 1;
    if (document.getElementById("rdbFornecedor").checked) {
        patrocinador = 1
    }
    if (document.getElementById("rdbCarimbo").checked) {
        patrocinador = 2
    }
    if (document.getElementById("rdbBonificacao").checked) {
        patrocinador = 3
    }
    if (document.getElementById("rdbPremiacao").checked) {
        patrocinador = 4
    }

    var form = $("#MercadoriaCompleta");

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    var data = form.serializeObject();

    if ($('#VlrUntario2').is(":visible")) {
        data.VLRUNTITEOCDACOCMC = $('#VlrUntario2').val();
    } else if ($('#VlrUntario').is(":visible")) {
        data.VLRUNTITEOCDACOCMC = $('#VlrUntario').val();
    }

    //data.VLRPTCFRNITEACOCMC = data.VLRPTCFRNITEACOCMC.replace('.', ',')
    //data.VLRUNTITEOCDACOCMC = data.VLRUNTITEOCDACOCMC.replace('.', ',')

    //Pega a data de ultilização
    if (document.getElementById("dtUtilizacao").readOnly) {
        data.DATUTZITEACOCMC = undefined
    }
    else if (document.getElementById("dtUtilizacao").readOnly == false) {
        data.DATUTZITEACOCMC = document.getElementById("dtUtilizacao").value
    }

    if (document.getElementById("FormaUtlz").readOnly) {
        data.DESFRMUTZITEACOCMC = $('#FormaUtlz').val();
    }

    if (document.getElementById("fornecedorUnico").checked) {
        data.INDITERSUFRNUNC = 1
    }
    else {
        data.INDITERSUFRNUNC = 0
    }

    if (document.getElementById("viaApp").checked) {
        data.INDITERSUARZPRVPGT = 1
    }
    else {
        data.INDITERSUARZPRVPGT = 0
    }

    if (data.INDTIPOPEDSNACOCMC != $('#dropOperacao').val()) {
        data.INDTIPOPEDSNACOCMC = $('#dropOperacao').val();
    }
    if (data.CODFILEMP != $('#dropFilial').val()) {
        data.CODFILEMP = $('#dropFilial').val();
    }

    if ($('#QtdUtz').val() == '' || $('#QtdUtz').val() > '9999999999') {
        atualizaMensagemModalItens("VALOR_TOTAL_MAXIMO_ITENS_ORCAMENTO", "msgWarning");
        return
    }
    if (!ValidaEntradaItemSubs(data)) {
        atualizaMensagemModalItens("VALOR_TOTAL_MINIMO_ITENS_ORCAMENTO", "msgWarning");
        return
    }

    if (document.getElementById("divdropItem").style.display == "") {
        data.NOMITEMORCDROP = $('#nomItemOrcDrop').val();
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "ItensOrcamento/Alterar",
        type: 'POST',
        data: { mercadoria: data, patrocinador: patrocinador },
        success: function (data) {
            if (data) {
                $('#divWidgetOrcamento').html(data);
                cancelarIncluirItem();

                //AtualizarCanalDeVenda();
                $('#modal').modal('hide');
                atualizaMensagem();
            }
            else {
                atualizaMensagem("msgModalOrcamento");
            }


            LoadingOff();
        },
        error: function () {
            atualizaMensagem();
            LoadingOff();
        }
    });
}


// Atualiza a mensagem do modal vinda do controller ou da propria tela
function atualizaMensagemModalItens(mensagem, tipo) {
    if (!!mensagem) {
        mostraMensagem(mensagem, tipo, "msgModalOrcamento");
    } else {
        atualizaMensagem("msgModalOrcamento");
    }
}

function ItensSubstitutos(mercadoria, codAcao) {

    if ($('#QtdUtz').val() == '' || $('#QtdUtz').val() > '9999999999') {
        atualizaMensagemModalItens("VALOR_TOTAL_MAXIMO_ITENS_ORCAMENTO", "msgWarning");
        return
    }
    if (ValidaEntradaItemSubs(mercadoria)) {

        if ($('#codAcao').val() == '') {
            atualizaMensagemModalItens("ESPERAR_SALVAR_ACAO", "msgWarning");
            return
        }
    }
    else {
        atualizaMensagemModalItens("VALOR_TOTAL_MINIMO_ITENS_ORCAMENTO", "msgWarning");
        return
    }

    var form = $("#MercadoriaCompleta");

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    var data = form.serializeObject();
    data.CODACOCMC = codAcao;
    if (data.DESCENCST === undefined) {
        data.DESCENCST = $('#itemBusca').val();
    }
    if (data.ValorTotal === undefined) {
        data.ValorTotal = $('#TotIte').val();
    }
    if (data.CODFILEMP === undefined) {
        data.CODFILEMP = $('#dropFilial').val();
    }
    if (data.INDTIPOPEDSNACOCMC === undefined) {
        data.INDTIPOPEDSNACOCMC = $('#dropOperacao').val();
    }

    abrirModal("ItensOrcamento", "ItensSubstitutos", data, "#modalSubstitutos");

}



function ValidaEntradaItemSubs(mercadoria) {
    if ($('#TotIte').val().replace('.', '').replace(',', '.') > "0") {
        return true
    }
    else {
        return false
    }
}


function selecionarItensSubstitutos(cod) {
    var itens = retornaListaDeValoresCheckados("ckbIncPed");

    //if (itens.length == 0) {
    //    mostraMensagem('Favor selecionar algum item.', 'msgWarning', 'msgItensSubstitutos');
    //} else {

    $('[id^="ckbIncPed"]').prop("checked", false);

    postAjax("ItensOrcamento", "SelecionarItensSubstitutos", { itensCheckados: cod },
        function (data) {
            if (data !== "") {
                $('#gridItensSubstitutosSelecionados').html(data);
            }
            // atualizaMensagem("msgItensSubstitutos");
            LoadingOff();
        }, function () {
            atualizaMensagem("msgItensSubstitutos");
            LoadingOff();
        }
    );
    //}
}


function removerItensSubstitutos(cod) {
    //var itens = retornaListaDeValoresCheckados("ckbExPed");


    postAjax("ItensOrcamento", "RemoverItensSubstitutos", { itensCheckados: cod },
        function (data) {
            if (data !== "") {
                $('#gridItensSubstitutosSelecionados').html(data);
            }
            atualizaMensagem("msgItensSubstitutos");
            LoadingOff();
        }, function () {
            atualizaMensagem("msgItensSubstitutos");
            LoadingOff();
        }
    );

}

function btnBuscaItensSubstitutos(mercadoria) {
    LoadingOn();
    var descItem = $('#DesIte').val();
    if (mercadoria) {
        descItem = mercadoria
    }
    var idItem = $('#idItem').val();
    var codOperacao = document.getElementById("dropOperacao").value;
    var patrociador = 0
    if (document.getElementById("rdbFornecedor").checked) {
        patrociador = 1
    }
    if (document.getElementById("rdbCarimbo").checked) {
        patrociador = 2
    }
    if (document.getElementById("rdbBonificacao").checked) {
        patrociador = 3
    }
    if (document.getElementById("rdbPremiacao").checked) {
        patrociador = 4
    }
    var filial = document.getElementById("dropFilial").value;

    if (!idItem) {
        idItem = 0
    }

    var dataEnviar = {
        DESMER: $('#dropMercadoriaSubstituta').val(),
        CODMER: $('#filtroCodMerSubstituta').val()
    }
    //var dataEnviar = null;

    var valorItemOriginal = $('#valorItensSubstitutos').val()

    $.ajax({
        url: SiteUrl + "ItensOrcamento/PesquisarSubstitutos",
        type: 'POST',
        data: { dadosPesquisa: dataEnviar, codOperacao: codOperacao, codFilial: filial, patrocinador: patrociador, valorTotalItemOriginal: valorItemOriginal },
        success: function (data) {
            if (data !== "") {
                if (dataEnviar.CODMER != undefined && dataEnviar.CODMER != '' && dataEnviar.CODMER != 0) {
                    selecionarItensSubstitutos(dataEnviar.CODMER);
                }
                $("#resultadosBuscaItensSubstitutos").html(data);
            }
            atualizaMensagem("msgItensSubstitutos");

            LoadingOff();
        },
        error: function () {
            atualizaMensagem("msgItensSubstitutos");
            LoadingOff();
        }
    });
}

function retornaListaDeValoresCheckados(idCheck) {

    var itensChecados = [];
    var size = idCheck.length;

    $('[id^="' + idCheck + '"]').each(function () {
        var $this = $(this);

        if ($this.is(":checked")) {
            itensChecados.push($this.attr("id").substr(size));
        }
    });

    return itensChecados;
}



function ViaAppFornecedorUnicoCheked() {
    var viaApp = $('#viaApp').is(":checked");
    var fornecedor = $('#fornecedorUnico').is(":checked");

    if (!(viaApp || fornecedor)) {
        document.getElementById("formaPgtDiv").style.display = "none";
        document.getElementById("contratoFornecedorDiv").style.display = "none";
        //document.getElementById("cgcFornecedorUnicoDiv").style.display = "none";
        document.getElementById("diasParaComprar").style.display = "none";
        document.getElementById("txtDiasParaComprar").style.display = "none";
        $('#diasParaComprar').val('');

        tirarFormPgt();
        tirarFornecedorUnico();
    }
    else {
        document.getElementById("contratoFornecedorDiv").style.display = "block";
        document.getElementById("formaPgtDiv").style.display = "block";
        //document.getElementById("cgcFornecedorUnicoDiv").style.display = "block";
    }
}

function modalFornecedorUnico() {
    var desFrn = $('#dropFornUnic').val();
    abrirModal('ItensOrcamento', 'abrirModalFornecedorUnico', desFrn, "#modalBuscaItem");
}

function selecionarFornecedorUnico(NUMCGCEMPFRN, RAZSOCEMPFRN, IDTLCLEMPFRN, IDTEMPFRN) {
    $('#cgcFornecedor').val("0/0 - " + NUMCGCEMPFRN);
    $('#dropFornUnic').val(RAZSOCEMPFRN);
    $('#FornUnic').val(IDTLCLEMPFRN);
    $('#numCgcEmpFrnc').val(NUMCGCEMPFRN);
    $('#idtEmpFrnIIteRsu').val(IDTEMPFRN);
    document.getElementById("dropFornUnic").readOnly = true;
    document.getElementById("btFrnUnq").style.display = "none";
    document.getElementById("btTiraFrnUnq").style = "";
    $('#modalBuscaItem').modal('hide');
}

function cancelarFornecedorUnico() {
    $('#modalBuscaItem').modal('hide');
}

function tirarFornecedorUnico() {
    $('#dropFornUnic').val("");
    $('#FornUnic').val("");

    document.getElementById("btFrnUnq").style = "";
    document.getElementById("btTiraFrnUnq").style.display = "none";
    document.getElementById("dropFornUnic").readOnly = false;
}



function modalFormPgt() {
    var desFrm = $('#dropFormPgt').val();
    abrirModal('ItensOrcamento', 'abrirModalFormaPgt', desFrm, "#modalBuscaItem", undefined, 'msgModalFormaPagamento');
}

function selecionarFrmPgt(codigo, descricao) {
    $('#dropFormPgt').val(descricao);
    $('#FormaPgt').val(codigo);
    document.getElementById("dropFormPgt").readOnly = true;
    document.getElementById("btFormPgt").style.display = "none";
    document.getElementById("btTiraFormPgt").style = "";
    $('#modalBuscaItem').modal('hide');
}

function cancelarFormPgt() {
    $('#modalBuscaItem').modal('hide');
}

function tirarFormPgt() {
    $('#dropFormPgt').val("");
    $('#FormaPgt').val("");

    document.getElementById("btFormPgt").style = "";
    document.getElementById("btTiraFormPgt").style.display = "none";
    document.getElementById("dropFormPgt").readOnly = false;
}

function modalSubstitutos() {
    $('#modalSubstitutos').modal('hide');
    $.ajax({
        url: SiteUrl + "ItensOrcamento/limparCacheFiltroAuxItensSub",
        type: 'POST',
        data: {}
    });
}

function selecionarMercadoria(codMercadoria, desMercadoria, idtite, vlrpco) {
    var filial = $('#dropFilial').val();
    $.ajax({
        url: SiteUrl + "ItensOrcamento/ConsultaPrecoUnitarioItem",
        type: 'POST',
        data: { codMercadoria: codMercadoria, codFilial: filial },
        success: function (data) {
            if (idtite) {
                $('#IDTITERSU').val(idtite);
            }
            else {
                $('#IDTITERSU').val('0');
            }
            $('#idItemCodMer').val(codMercadoria);
            $('#itemBusca').val(desMercadoria.substring(0, 50));
            if ($('#VlrUntario2').is(":visible"))
                $('#VlrUntario2').val(parseFloat(data).toFixed(2).replace(".", ","));
            else
                $('#VlrUntario').val(parseFloat(data).toFixed(4).replace(".", ","));
            if (vlrpco) {
                $('#VlrpagoForn').val(parseFloat(vlrpco).toFixed(4).replace(".", ","));
                if ($('#VlrUntario2').is(":visible"))
                    $('#VlrUntario2').val(parseFloat(vlrpco).toFixed(2).replace(".", ","));
                else
                    $('#VlrUntario').val(parseFloat(vlrpco).toFixed(4).replace(".", ","));
            }
            else {
                $('#VlrpagoForn').val(parseFloat(data).toFixed(4).replace(".", ","));
            }

            document.getElementById("btBscIte").style.display = "none";
            document.getElementById("btTiraIte").style = "";
            document.getElementById("itemBusca").readOnly = true;
            document.getElementById("idItemCodMer").readOnly = true;
            limparCacheFiltroItens();
            $('#modalBuscaItem').modal('hide');
        },
        error: function () {
            atualizaMensagem(divMsg);
        }
    });
}

function SalvarCacheItensSub() {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "ItensOrcamento/SalvarCacheItensSub",
        type: 'POST',
        data: {},
        success: function (data) {
            if (data !== "") {
                $("#resultadosBuscaItensSubstitutos").html(data);
            } else {
                atualizaMensagem("msgBuscaMercadorias");
            }
            $('#modalSubstitutos').modal('hide');
            $.ajax({
                url: SiteUrl + "ItensOrcamento/validaTrocarOperacaoFilial",
                type: 'POST',
                data: { objeto: undefined },
                success: function (data) {
                    if (data == "False") {
                        $('#dropOperacao').prop("disabled", true);
                        $('#dropFilial').prop("disabled", true);
                        $('#drupBU').prop("disabled", true);
                        $('#drupBU').trigger("chosen:updated");
                        $('#dropOperacao').trigger("chosen:updated");
                        $('#dropFilial').trigger("chosen:updated");
                        //document.getElementById("dropOperacao").readOnly = true;
                        //document.getElementById("dropFilial").readOnly = true;
                        document.getElementById("QtdUtz").readOnly = true;
                        document.getElementById("VlrpagoCliente").readOnly = true;
                        document.getElementById("FormaUtlz").readOnly = true;

                        document.getElementById("btTiraIte").disabled = true;

                        //$('#btnAlterar').prop("disabled", true).trigger("chosen:updated");
                        document.getElementById("lblErroTemItensSubs").style.display = "block";

                    }
                    else {
                        $('#dropOperacao').prop("disabled", false);
                        $('#dropFilial').prop("disabled", false);
                        $('#drupBU').prop("disabled", false);
                        $('#drupBU').trigger("chosen:updated");
                        $('#dropOperacao').trigger("chosen:updated");
                        $('#dropFilial').trigger("chosen:updated");
                        document.getElementById("QtdUtz").readOnly = false;
                        document.getElementById("btTiraIte").disabled = false;
                        document.getElementById("VlrpagoCliente").readOnly = false;
                        document.getElementById("FormaUtlz").readOnly = false;

                        //  document.getElementById("FormaUtlz").disabled = false;
                        document.getElementById("btBscIte").disabled = false;
                        $('#btnAlterar').prop("disabled", false).trigger("chosen:updated");
                        document.getElementById("lblErroTemItensSubs").style.display = "none";
                    }
                }
            });
            LoadingOff();
        },
        error: function () {
            atualizaMensagem("msgBuscaMercadorias");
            $('#modalSubstitutos').modal('hide');
            LoadingOff();
        }
    });
    $.ajax({
        url: SiteUrl + "ItensOrcamento/limparCacheFiltroAuxItensSub",
        type: 'POST',
        data: {}
    });

}

function ModalImportXlsItensSubstitutosSuccess() {
    // variavel para salvar o arquivo
    var files;

    // evento ao selecionar arquivo
    $('input[type=file]').on('change', prepareUpload);

    // salva o arquivo na variavel
    function prepareUpload(event) {
        files = event.target.files;
    }

    $('#arquivo').on('submit', uploadFiles);

    // ao submeter o formulario
    function uploadFiles(event) {
        event.stopPropagation(); // para execução
        event.preventDefault(); // para completamente a execução

        var data = new FormData();
        $.each(files, function (key, value) {
            data.append(key, value);
        });

        var codOperacao = document.getElementById("dropOperacao").value;
        data.append("codOperacao", codOperacao);

        var codFilial = document.getElementById("dropFilial").value;
        data.append("codFilial", codFilial);

        var valorTotalItemOriginal = $('#valorItensSubstitutos').val()
        data.append("valorTotalItemOriginal", valorTotalItemOriginal);

        $.ajax({
            url: SiteUrl + 'ItensOrcamento/ImportarPlanilha',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'text',
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    LoadingOn();
                    $('#modalResultImport').html(data);
                    $('#modalResultImport').modal();
                    $('#ModalImportXLS').modal('toggle');
                    LoadingOff();
                }
                else {
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
            }
        });
    }

    $('.ace-file-container').ace_file_input({
        no_file: 'Sem Arquivo ...',
        btn_choose: 'Selecione',
        btn_change: 'Mudar',
        droppable: false,
        onchange: null,
        thumbnail: false
    });
}

function okModalResultadoImport() {
    $('#modalResultImport').modal('hide');
    RecarregaImportados();
}

function limparItensSubs() {
    postAjax("ItensOrcamento", "LimpaItensSubs", {},
        function (data) {
            if (data !== "") {
                $('#gridItensSubstitutosSelecionados').html(data);
            }
            atualizaMensagem("msgItensSubstitutos");
            LoadingOff();
        }, function () {
            atualizaMensagem("msgItensSubstitutos");
            LoadingOff();
        }
    );
}

function RecarregaImportados() {
    var valorItemOriginal = $('#valorItensSubstitutos').val()
    var filial = document.getElementById("dropFilial").value;

    postAjax("ItensOrcamento", "RecarregaImportados", { codFilial: filial, valorTotalItemOriginal: valorItemOriginal },
        function (data) {
            if (data !== "") {
                $('#gridItensSubstitutosSelecionados').html(data);
            }
            atualizaMensagem("msgItensSubstitutos");
            LoadingOff();
        }, function () {
            atualizaMensagem("msgItensSubstitutos");
            LoadingOff();
        }
    );
}

function calculaPagoPeloFornecedor() {
    if ($('#VlrUntario2').is(":visible")) {
        var vlrUnidade = parseFloat($('#VlrUntario2').val().replace(/\./g, "").replace(',', '.'));
    } else {
        var vlrUnidade = parseFloat($('#VlrUntario').val().replace(/\./g, "").replace(',', '.'));
    }
    var vlrCliente = parseFloat($('#VlrpagoCliente').val().replace(/\./g, "").replace(',', '.').replace('-', ''));
    var result = vlrUnidade - vlrCliente;
    $('#VlrpagoCliente').val(vlrCliente.toFixed(4));
    if (result < 0) {
        $('#VlrpagoForn').val("0,0000");
    }
    else {
        $('#VlrpagoForn').val(result.toFixed(4));
    }
    LoadingOff();
}

function cancelarIncluirItem() {
    postAjax("ItensOrcamento", "limparCacheItensSub", {},
        function (data) {
            atualizaMensagem("msgItensSubstitutos");
            LoadingOff();
        }, function () {
            atualizaMensagem("msgItensSubstitutos");
            LoadingOff();
        }
    );
}

function cancelarImportacao() {
    $('#ModalImportXLS').modal('hide');
}

$(document).on('keydown', '#idItemCodMer', function (e) {
    var keyCode = e.keyCode || e.which;

    //if (keyCode == 9) {
    //    e.preventDefault();
    //    // call custom function here
    //    modalBuscarItens();
    //}
});

function modalBuscaItensOrcamento() {
    var objeto = $('#idItemMerc').val() + "," + $('#NomItemMerc').val()
    abrirModal('ConsultaAcompanhamentoPreAcordo', 'AbrirModalItensOrcamento', objeto, '#modalItensOrcamento');
    atualizaMensagem("msgModalBuscaItensOrcamento");

}

function modalBuscaItensOrcamentoSimulaFunding() {
    var objeto = $('#idItemMerc').val() + "," + $('#NomItemMerc').val()
    abrirModal('SimulaFunding', 'AbrirModalItensOrcamento', objeto, '#modalItensOrcamento');
    atualizaMensagem("msgModalBuscaItensOrcamento");

}

function btnCancelarItensOrcamento() {
    $('#modalItensOrcamento').modal('hide');
}

function selecionarItensOrcamento(CODMER, DESMER) {
    $('#idItemMerc').val(CODMER);
    $('#NomItemMerc').val(DESMER);

    document.getElementById("btnBuscaItens").style.display = "none";
    document.getElementById("btnTiraItens").style = "";
    $('#modalItensOrcamento').modal('hide');
}

function tirarItensOrcamento() {
    $('#idItemMerc').val("");
    $('#NomItemMerc').val("");

    document.getElementById("btnBuscaItens").style = "";
    document.getElementById("btnTiraItens").style.display = "none";
}


function btnBuscaItensOrcamento() {
    var codItem = $('#FilterCodItem').val();

    if (codItem == "") {
        codItem = 0
    }

    var desItem = $('#FilterDesItem').val();

    $.ajax({
        url: SiteUrl + "ConsultaAcompanhamentoPreAcordo/BuscaItensOrcamentoModal",
        type: 'POST',
        data: { codItem: codItem, desItem: desItem },
        success: function (data) {
            if (data !== "") {
                $("#resultadosBuscaItensOrcamento").html(data);
            }
            atualizaMensagem("msgModalBuscaItensOrcamento");
        },
        error: function () {
            atualizaMensagem("msgModalBuscaItensOrcamento");
        }
    });
}


function btnImprimirRel() {

    LoadingOn();

    var opcaoVisao = 0
    var tipoOperacao = ""
    var rdbTipoOperacaoVend = document.getElementById('rdbTipoOperacaoVend').checked;
    var rdbTipoOperacaoFat = document.getElementById('rdbTipoOperacaoFat').checked;
    var rdbFornecedor = document.getElementById('rdbFornecedor').checked;
    var rdbMercadoria = document.getElementById('rdbMercadoria').checked;
    var rdbCliente = document.getElementById('rdbCliente').checked;

    if (rdbTipoOperacaoVend == true) {

        tipoOperacao = $('#rdbTipoOperacaoVend').val()
    }
    else if (rdbTipoOperacaoFat == true) {

        tipoOperacao = $('#rdbTipoOperacaoFat').val()
    }

    if (rdbFornecedor == true) {

        opcaoVisao = $('#rdbFornecedor').val()
    }
    else if (rdbMercadoria == true) {

        opcaoVisao = $('#rdbMercadoria').val()
    }
    else if (rdbCliente == true) {
        opcaoVisao = $('#rdbCliente').val()
    }

    data = {
        codPreAcordo: $('#CodPreAcordo').val()
        , tipoOperacao: tipoOperacao
        , dataIni: $('#dataIni').val()
        , dataFim: $('#dataFim').val()
        , codCelula: $('#dropFiltroCelula').val()
        , codComprador: $('#dropFiltroComprador').val()
        , codFornecedor: $('#dropFornecedores').val()
        , codFornecedorPatrocinador: $('#dropFornecedoresPatrocinador').val()
        , codMercadoria: $('#idItemMerc').val()
        , opcaoVisao: opcaoVisao
        , desMercadoria: $('#NomItemMerc').val()
        , codPromocao: $('#dropFiltroPromocao').val()
        , codFilial: $('#dropFilial').val()
        , codFilial: $('#dropFilial').val()
        , indCampanhaB2B: $('input[type=radio][name="Filter.INDCPHBTB"]:checked').val()
    };

    $.ajax({
        cache: false,
        url: SiteUrl + "ConsultaAcompanhamentoPreAcordo/Imprimir",
        data: data,
        success: function (data) {
            var response = data;

            if ($('#dataIni').val() !== "" && $('#dataFim').val() !== "" && data !== "") {
                window.location = SiteUrl + 'ConsultaAcompanhamentoPreAcordo/Download?fileGuid=' + response.FileGuid
                    + '&filename=' + response.FileName;
            }
            else {
                atualizaMensagem("msgGeral");
            }
            LoadingOff();
        },
        error: function () {
            LoadingOff();
        }
    })
};


function SucessoAlterar(obj) {
    LoadingOn();

    //var casasDepois = obj.VLRPTCFRNITEACOCMC.toString().length - (obj.VLRPTCFRNITEACOCMC.toString().indexOf('.') + 1);
    //if (obj.VLRPTCFRNITEACOCMC.toString().indexOf('.') == -1) {
    //    casasDepois = 0
    //}
    //for (i = casasDepois; i < 4 ; i++) {
    //    obj.VLRPTCFRNITEACOCMC = obj.VLRPTCFRNITEACOCMC.toString().concat('0')
    //}
    //obj.VLRPTCFRNITEACOCMC = obj.VLRPTCFRNITEACOCMC.toString().replace('.', '')

    //var casasDepois = obj.VLRUNTITEOCDACOCMC.toString().length - (obj.VLRUNTITEOCDACOCMC.toString().indexOf('.') + 1);
    //if (obj.VLRUNTITEOCDACOCMC.toString().indexOf('.') == -1) {
    //    casasDepois = 0
    //}
    //for (i = casasDepois; i < 4 ; i++) {
    //    obj.VLRUNTITEOCDACOCMC = obj.VLRUNTITEOCDACOCMC.toString().concat('0')
    //}
    //obj.VLRUNTITEOCDACOCMC = obj.VLRUNTITEOCDACOCMC.toString().replace('.', '')

    //var casasDepois = obj.VLRPTCCLIITEACOCMC.toString().length - (obj.VLRPTCCLIITEACOCMC.toString().indexOf('.') + 1);
    //if (obj.VLRPTCCLIITEACOCMC.toString().indexOf('.') == -1) {
    //    casasDepois = 0
    //}
    //for (i = casasDepois; i < 4 ; i++) {
    //    obj.VLRPTCCLIITEACOCMC = obj.VLRPTCCLIITEACOCMC.toString().concat('0')
    //}
    //obj.VLRPTCCLIITEACOCMC = obj.VLRPTCCLIITEACOCMC.toString().replace('.', '')




    var codOperacao = $('#dropOperacao').val();
    $.ajax({
        url: SiteUrl + "ItensOrcamento/TipoOperacaoChange",
        type: 'POST',
        data: { codOperacao: codOperacao },
        success: function (data) {
            if (obj.INDITERSUARZPRVPGT == 1)
                $('#viaApp').prop("checked", "checked");
            else
                $('#viaApp').prop("checked", "");
            if (obj.INDITERSUFRNUNC == 1)
                $('#fornecedorUnico').prop("checked", "checked");
            else
                $('#fornecedorUnico').prop("checked", "");

            mostraCamposOrcamento(data.operacao, data.gridOrcamento, data.tipoEvento, data.INDACOCMCBTBVLR);
            $('#QtdUtz').val(obj.QDEITEOCDACOCMC);
            $('#VlrUntario').val(obj.VLRUNTITEOCDACOCMC.toFixed(4));
            $('#VlrUntario2').val(obj.VLRUNTITEOCDACOCMC.toFixed(2));

            $('#TotIte').val(obj.ValorTotal.toFixed(4).replace(".", ","));
            $('#VlrpagoForn').val(obj.VLRPTCFRNITEACOCMC.toFixed(4).replace(".", ","));

            $('#VlrpagoCliente').val(obj.VLRPTCCLIITEACOCMC.toFixed(4));
            $('#idItemCodMer').val(obj.CODMER);
            $('#formaUtilizacao').val(obj.DESOBSCPR);
            $('#diasParaComprar').val(obj.QDEDIACMPITEACOCMC);
            $('#FormaUtlz').val(obj.DESFRMUTZITEACOCMC);
            $('#idDesMerc').val(obj.CODITEOCDACOCMC);
            //$('#dtUtilizacao').val(obj.DATUTZITEACOCMC);



            document.getElementById("btnAlterar").style = "";
            document.getElementById("btnSalvar").style.display = "none";

            var opcao = document.getElementById("dropOperacao").value;

            $.ajax({
                url: SiteUrl + "ItensOrcamento/validaTrocarOperacaoFilial",
                type: 'POST',
                data: { objeto: undefined },
                success: function (data) {

                    //if ($('#CODSTAAPVACOCMC').val() != 1) {
                    //($('#CODSTAAPVACOCMC').val() == 1 && opcao == 2 || opcao == 4)
                    if ($('#CODSTAAPVACOCMC').val() == 1 && data == "False" && (opcao == 2 || opcao == 4)) {
                        $('#dropOperacao').prop("disabled", true).trigger("chosen:updated");
                        $('#centroCustoDES').prop("disabled", true).trigger("chosen:updated");
                        $('#dropFilial').prop("disabled", true).trigger("chosen:updated");
                        $('#dropFilial').val(obj.CODFILEMP);
                        $('#drupBU').prop("disabled", true).trigger("chosen:updated");
                        $('#centroCustoDES').prop("disabled", true).trigger("chosen:updated");

                        document.getElementById("dtUtilizacao").readOnly = true
                        document.getElementById("VlrUntario").readOnly = true
                        document.getElementById("VlrpagoForn").readOnly = true
                        document.getElementById("VlrpagoCliente").readOnly = true
                        document.getElementById("diasParaComprar").readOnly = true
                        document.getElementById("dropFormPgt").readOnly = true
                        document.getElementById("FornUnic").readOnly = true
                        document.getElementById("btFormPgt").disabled = true;
                        document.getElementById("btFrnUnq").disabled = true;
                        document.getElementById("numCgcEmpFrnc").readOnly = true
                        document.getElementById("idtEmpFrnIIteRsu").readOnly = true
                        document.getElementById("formaUtilizacao").readOnly = true
                        document.getElementById("QtdUtz").readOnly = true
                        document.getElementById("fornecedorUnico").disabled = true;
                        document.getElementById("viaApp").disabled = true;

                        document.getElementById("QtdUtz").readOnly = true
                        document.getElementById("idItemCodMer").readOnly = true
                        document.getElementById("centroCustoID").readOnly = true
                        document.getElementById("itemBusca").readOnly = true
                        document.getElementById("btBscIte").disabled = true;
                        document.getElementById("btnCentroCusto").disabled = true;
                        document.getElementById("FormaUtlz").readOnly = true;
                        document.getElementById("btnAlterar").style.display = "block";
                        $('#btnAlterar').prop("disabled", false).trigger("chosen:updated");
                        $('#btSuprimetntos').prop("disabled", true).trigger("chosen:updated");

                        document.getElementById("lblErroTemItensSubs").style.display = "block";
                    }
                    else {
                        document.getElementById("itemBusca").readOnly = true
                        document.getElementById("idItemCodMer").readOnly = true
                        document.getElementById("btBscIte").style.display = "none";
                        document.getElementById("btTiraIte").style.display = "block";

                        document.getElementById("lblErroTemItensSubs").style.display = "none";

                        //var vlrUnidade = parseFloat($('#VlrUntario').val().replace(",", "."));



                        //var valor = new Number($('#VlrUntario').val());
                        //var vlrUnidade = valor.toFixed(4);


                        // var vlrUnidade = parseFloat($('#VlrUntario').val().toFixed(4));
                        //var qtUtilizado = parseFloat($('#QtdUtz').val().replace(",", "."));
                        //if (qtUtilizado > 0) {
                        $('#TotIte').val(obj.ValorTotal.toFixed(4).replace(".", ","));
                        $('#VlrpagoForn').val(obj.VLRPTCFRNITEACOCMC.toFixed(4).replace(".", ","));
                        //}
                        //else {
                        //    $('#QtdUtz').val("0");
                        //}
                        //var vlrCliente = parseFloat($('#VlrpagoCliente').val());
                        //var result = vlrUnidade - vlrCliente;
                        //if (result < 0) {
                        //    $('#VlrpagoForn').val('0,0000');
                        //}
                        //else {
                        //    result = Math.round(result * 10000) / 10000;
                        //    $('#VlrpagoForn').val(result);
                        //}
                        if ($('#CODSTAAPVACOCMC').val() != 1) {
                            $('#dropOperacao').prop("disabled", true).trigger("chosen:updated");
                            $('#centroCustoDES').prop("disabled", true).trigger("chosen:updated");
                            $('#dropFilial').prop("disabled", true).trigger("chosen:updated");
                            $('#dropFilial').val(obj.CODFILEMP);
                            $('#drupBU').prop("disabled", true).trigger("chosen:updated");
                            $('#centroCustoDES').prop("disabled", true).trigger("chosen:updated");
                            $('#btTiraIte').prop("disabled", true).trigger("chosen:updated");
                            $('#btnCentroCusto').prop("disabled", true).trigger("chosen:updated");

                            document.getElementById("QtdUtz").readOnly = true;
                            document.getElementById("VlrpagoCliente").readOnly = true;
                            document.getElementById("centroCustoID").readOnly = true;
                            document.getElementById("FormaUtlz").readOnly = true;
                        }
                    }
                }
            });

            LoadingOff();

        },
        error: function () {
            atualizaMensagem();
            LoadingOff();

        }
    });
    //$.ajax({
    //    url: SiteUrl + "ItensOrcamento/validaTrocarOperacaoFilial",
    //    type: 'POST',
    //    data: { objeto: obj },
    //    success: function (data) {
    //        if (data == "False") {
    //            $('#dropOperacao').prop("disabled", true)
    //            $('#dropFilial').prop("disabled", true)
    //            $('#drupBU').prop("disabled", true);
    //            $('#drupBU').trigger("chosen:updated");
    //            $('#dropOperacao').trigger("chosen:updated");
    //            $('#dropFilial').trigger("chosen:updated");
    //            //document.getElementById("dropOperacao").readOnly = true;
    //            //document.getElementById("dropFilial").readOnly = true;
    //            //document.getElementById("QtdUtz").readOnly = true;
    //            document.getElementById("btTiraIte").disabled = true;


    //            document.getElementById("lblErroTemItensSubs").style.display = "block";

    //        }
    //        else {
    //            $('#dropOperacao').prop("disabled", false);
    //            $('#dropFilial').prop("disabled", false);
    //            $('#drupBU').prop("disabled", false);
    //            $('#drupBU').trigger("chosen:updated");
    //            $('#dropOperacao').trigger("chosen:updated");
    //            $('#dropFilial').trigger("chosen:updated");
    //            document.getElementById("QtdUtz").readOnly = false;
    //            document.getElementById("btTiraIte").disabled = false;

    //            document.getElementById("lblErroTemItensSubs").style.display = "none";
    //        }
    //    }
    //});
}



function adicionarUtilizacaoItem(CodItemGasto) {
    abrirModal('ItensOrcamento', 'ModalAdicionarGasto', { NUMLNHITEOCDACOCMC: CodItemGasto }, "#modalEditGasto", undefined, 'msgModalEditGasto');
}


function salvarUtilizacaoItem() {
    LoadingOn();

    var data = {
        CODACOCMC: $('#CODACOCMC').val(),
        NUMLNHITEOCDACOCMC: $('#hdfEditGasto_NUMLNHITEOCDACOCMC').val(),
        CODUTZITEOCDACOCMC: $('#txtEditGasto_CODUTZITEOCDACOCMC').val(),
        DESUTZITEOCDACOCMC: $('#txtEditGasto_DESUTZITEOCDACOCMC').val(),
        QDEUTZITEOCDACOCMC: $('#txtEditGasto_QDEUTZITEOCDACOCMC').val(),
        VLRUTZITEOCDACOCMC: $('#txtEditGasto_VLRUTZITEOCDACOCMC').val(),
        INDTIPISRUTZITEOCD: $('input[type="radio"][name="INDTIPISRUTZITEOCD"]:checked').val()
    };

    postAjax("ItensOrcamento", "SalvarUtilizacaoGasto", data,
        function (data) {

            $('.modal-backdrop').remove();
            $('#modal').html(data);
            $('#modal').modal().on('shown.bs.modal', function () {
                ChosenResize();
            });

            atualizaMensagem("msgModalConsultaGasto");

            AtualizaWidgetOrcamento();
            LoadingOff();
        }, function () {

            $('.modal-backdrop').remove();
            $('#modal').html(data);
            $('#modal').modal().on('shown.bs.modal', function () {
                ChosenResize();
            });

            atualizaMensagem("msgModalEditGasto");
            LoadingOff();
        }
    );
}


function AtualizaWidgetOrcamento() {

    $.ajax({
        url: SiteUrl + "ItensOrcamento/AtualizaWidgetOrcamento",
        type: 'GET',
        success: function (data) {
            if (data) {
                $('#divWidgetOrcamento').html(data);
                atualizaMensagem();
            }
            else {
                atualizaMensagem("msgModalOrcamento");
            }
            LoadingOff();
        },
        error: function () {
            atualizaMensagem();
            LoadingOff();
        }
    });
}

function GetInfoFormaPagamentoItemOrcamento() {
    if ($('#FormaPgt').val() !== undefined && $('#FormaPgt').val() !== "") {
        $.ajax({
            url: SiteUrl + "ItensOrcamento/GetDescricaoFormaPagamento/" + $('#FormaPgt').val(),
            type: 'GET',
            success: function (data) {
                if (data) {
                    $('#dropFormPgt').val(data);
                    document.getElementById("dropFormPgt").readOnly = true;
                    atualizaMensagem();
                }
                else {
                    atualizaMensagem();
                }
                LoadingOff();
            },
            error: function () {
                atualizaMensagem();
                LoadingOff();
            }
        });
    }
}

function GetInfoFornecedorUnicoItemOrcamento() {
    if ($('#FornUnic').val() !== undefined && $('#FornUnic').val() !== "") {
        $.ajax({
            url: SiteUrl + "ItensOrcamento/GetInfoFornecedorUnico/" + $('#FornUnic').val(),
            type: 'GET',
            success: function (data) {
                if (data) {
                    $('#cgcFornecedor').val("0/0 - " + data.NUMCGCEMPFRN);
                    document.getElementById("cgcFornecedor").readOnly = true;

                    $('#dropFornUnic').val(data.RAZSOCEMPFRN);
                    document.getElementById("dropFornUnic").readOnly = true;

                    atualizaMensagem();
                }
                else {
                    atualizaMensagem();
                }
                LoadingOff();
            },
            error: function () {
                atualizaMensagem();
                LoadingOff();
            }
        });
    }
}
