﻿function retornaListaCheckados() {
    var selecteds = $('#divCheckMenu input:checked');
    var values = new Array(selecteds.length)
    for (var i = 0; i < selecteds.length; i++) {
        values[i] =  $(selecteds[i]).attr('name').split(".")[1];
    } 
    return values
}

// Pega a lista de itens checkados e coloca na lista de carimbos selecionados
function SalvarControleAcesso(idAcao) {
    var itens = retornaListaCheckados();
    postAjax("ControleAcesso", "Salvar", { MenusSelecionados: itens, CodGrup: idAcao },
    function (data) {
        atualizaMensagem();
        LoadingOff();
    }, function () {
        atualizaMensagem();
        LoadingOff();
    }
    );
}

function marcarOuDesmarcarFilhos(sender) {
    if ($(sender).is(":checked")) {
        $(sender).parent().find('input[type="checkbox"]').prop("checked", true);
    } else {
        $(sender).parent().find('input[type="checkbox"]').prop("checked", false);
    }
}