﻿
function AbastecerPromocao(idPmc) {
    getAjax("Abastecimento", "ModalAbastecerPromocao" + '/' + idPmc,
        function (data) {
            if (data !== "")
                ShowModal({ mensagem: data, titulo: "Abastecimento", widthS: 1050, appendElement: "divModalAbastecerPromocao" });
            else
                ShowModal({ mensagem: data, titulo: "Atenção", appendElement: "divModalAbastecerPromocaoAlert" });
            LoadingOff();
        },
        function (data) {
            LoadingOff();
            ShowModal({ mensagem: "Não é possível abastecer essa promoção.", titulo: "Atenção", appendElement: "divModalAbastecerPromocaoAlert" })
        }
    );
}

function AbastecerMultiplo(CODPMC, TIPO) {

    var brindes = [];

    $('#tb_promocao_' + CODPMC + ' tr b.CODMER').each(function (index, element) {

        var CODMER = $(element).text();
        var rowid = "#row_" + CODPMC + "_" + CODMER;
        var QDEMERISRPMC = $(rowid + '_desc').find('input[type="text"].QDEMERISRPMC').val();
        if (QDEMERISRPMC != '') {
            var data = {
                CODPMC: CODPMC,
                CODMER: CODMER,
                INDTIPISRMERPMC: TIPO,

                DESMER: $(rowid + '_desc').find('b.DESMER').html(),
                CODACOCMC: $(rowid + '_desc').find('input[type="text"].CODACOCMC').val(),
                TIPEDENIVPRMPMC: $(rowid + '_desc').find('input[type="hidden"][name="TIPEDENIVPRMPMC"]').val(),
                QDEMERISRPMC: QDEMERISRPMC,
                ETQSLD: $(rowid + '_qtds').find('td.ETQSLD').text().trim(),
                ETQBND: $(rowid + '_qtds').find('td.ETQBND').text().trim(),
                QDEMERVNDPRMPMC: $(rowid + '_qtds').find('td.QDEMERVNDPRMPMC').text().trim(),
                QDEMERMAXPRMPMC: $(rowid + '_qtds').find('td.QDEMERMAXPRMPMC').text().trim()
            }
            if (data.CODACOCMC == '') {
                ShowModal({ mensagem: "Favor preencher o campo Ação Comercial antes de salvar.", titulo: "Atenção", appendElement: "divModalAbastecerPromocaoAlert" });
                return;
            }
            if (data.QDEMERISRPMC == '') {
                ShowModal({ mensagem: "Favor preencher o campo Quantidade antes de salvar.", titulo: "Atenção", appendElement: "divModalAbastecerPromocaoAlert" })
                return;
            }

            brindes[brindes.length] = data;
        }
    });



    if (brindes.length > 0) {
        Abastecer(brindes);
    }
}


function AbastecerUnico(CODPMC, CODMER, TIPO) {

    var rowid = "#row_" + CODPMC + "_" + CODMER;


    var data = {
        CODPMC: CODPMC,
        CODMER: CODMER,
        INDTIPISRMERPMC: TIPO,

        DESMER: $(rowid + '_desc').find('b.DESMER').html(),
        CODACOCMC: $(rowid + '_desc').find('input[type="text"].CODACOCMC').val(),
        TIPEDENIVPRMPMC: $(rowid + '_desc').find('input[type="hidden"][name="TIPEDENIVPRMPMC"]').val(),
        QDEMERISRPMC: $(rowid + '_desc').find('input[type="text"].QDEMERISRPMC').val(),
        ETQSLD: $(rowid + '_qtds').find('td.ETQSLD').text().trim(),
        ETQBND: $(rowid + '_qtds').find('td.ETQBND').text().trim(),
        QDEMERVNDPRMPMC: $(rowid + '_qtds').find('td.QDEMERVNDPRMPMC').text().trim(),
        QDEMERMAXPRMPMC: $(rowid + '_qtds').find('td.QDEMERMAXPRMPMC').text().trim()
    }


    if (data.CODACOCMC == '') {
        ShowModal({ mensagem: "Favor preencher o campo Ação Comercial antes de salvar.", titulo: "Atenção", appendElement: "divModalAbastecerPromocaoAlert" });
        return;
    }
    if (data.QDEMERISRPMC == '') {
        ShowModal({ mensagem: "Favor preencher o campo Quantidade antes de salvar.", titulo: "Atenção", appendElement: "divModalAbastecerPromocaoAlert" })
        return;
    }

    var brindes = [data];

    Abastecer(brindes);
}


function Abastecer(brindes) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "Abastecimento/Abastecer",
        type: 'POST',
        data: { brindes: brindes },
        success: function (data) {
            //ATUALIZA VALORES NA ROW 
            if (data.erro !== undefined) {
                ShowModal({ mensagem: data.erro, titulo: "Atenção", appendElement: "divModalAbastecerPromocaoAlert" });
            } else {
                for (var i = 0; i < data.length; i++) {
                    var rowid = "#row_" + data[i].CODPMC + "_" + data[i].CODMER;

                    $(rowid + '_qtds').find('td.QDEMERVNDPRMPMC').text(data[i].QDEMERVNDPRMPMC);
                    $(rowid + '_qtds').find('td.QDEMERMAXPRMPMC').text(data[i].QDEMERMAXPRMPMC);
                    $(rowid + '_qtds').find('td.ETQBND').text(data[i].ETQBND);
                    $(rowid + '_qtds').find('td.ETQSLD').text(data[i].ETQSLD);
                    $(rowid + '_qtds').find('td.QDEDIACOB').text(data[i].QDEDIACOB);
                    $(rowid + '_qtds').find('td.QDEDIARES').text(data[i].QDEDIARES);
                    //$(rowid + '_qtds').find('td.VALOR_BRINDE').text(data[i].VALOR_BRINDE);
                    //$(rowid + '_qtds').find('td.MEDIA_SAIDA').text(data[i].MEDIA_SAIDA);

                    $(rowid + '_desc').find('input[type="text"].CODACOCMC').val(data[i].CODACOCMC);
                    $(rowid + '_desc').find('input[type="text"].QDEMERISRPMC').val('');

                    if (i == 0) {
                        $(rowid + '_desc').parent().prepend('<tr id="msgAbastecimentoSucesso"><td colspan="9"><div class="alert alert-success fade in">Abastecimento realizado com sucesso</div></td></tr>');

                        window.setTimeout(function () { $('#msgAbastecimentoSucesso').remove(); }, 5000);
                    }
                }


                if (typeof BuscarListaBrindes === 'function')
                    BuscarListaBrindes();
            }
            LoadingOff();
        },
        error: function () {
            ShowModal({ mensagem: "Não foi possível salvar as alterações.", titulo: "Atenção", appendElement: "divModalAbastecerPromocaoAlert" })
            LoadingOff();
        }
    });
}

function HistoricoAbastecimento(CODPMC, CODMER) {
    getAjax("Abastecimento", "Historico" + '/' + CODPMC + '/' + CODMER,
        function (data) {
            $('#divModalHistoricoAbastecimento').html(data);
            $('#divModalHistoricoAbastecimento > .modal').modal();
            LoadingOff();
        }
    );
}
function btnFecharHistoricoAbastecimento() {
    $('#divModalHistoricoAbastecimento > .modal').modal('hide');
}

function txtAcaoComercialAbastecer_change(sender) {
    $(sender).parents('table').find('input[type="text"].CODACOCMC').val($(sender).val());
}

function txtQtdAbastecer_change(sender) {
    $(sender).parents('table').find('input[type="text"].QDEMERISRPMC').val($(sender).val());
}

function toggleTreeview(sender) {
    $(sender).parent().parent().find('> div.well').toggle();
}

function collapsAll() {
    $('#resultadosView div.well div.well').hide();
}

function expandAll() {
    $('#resultadosView div.well div.well').show();
}

