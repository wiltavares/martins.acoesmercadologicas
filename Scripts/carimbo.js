﻿// Mostra ou esconde os campos de filtro de carimbo/celula/fornecedor
function dropFiltroCarimbos_onChange(idFiltro) {
    if (idFiltro == 3) {
        $('#divFiltroFornecedor').show();
        $('#divFiltroCelula').hide();
        $('#divFiltroCarimbo').hide();
    } else if (idFiltro == 2) {
        $('#divFiltroCelula').show();
        $('#divFiltroFornecedor').hide();
        $('#divFiltroCarimbo').hide();
    } else {
        $('#divFiltroCarimbo').show();
        $('#divFiltroFornecedor').hide();
        $('#divFiltroCelula').hide();
    }
    ChosenResize();
}

// Evento change do drop de Fornecedor para atribuir o codigo ao textBox correspondente
function dropFornecedorCarimbo_onChange() {
    $('#filtroCodFrn').val($('#dropFiltroFornecedorCarimbo').val());
}

// Atualiza a mensagem do modal vinda do controller ou da propria tela
function atualizaMensagemModalCarimbo(mensagem, tipo) {
    if (!!mensagem) {
        mostraMensagem(mensagem, tipo, "msgModalCarimbo");
    } else {
        atualizaMensagem("msgModalCarimbo");
    }
}

// Busca fornecedores e atualiza o dropdown com os valores da busca
function buscarFornecedores() {
    var codFornecedor = $('#filtroCodFrn').val();
    postAjax("Fornecedor", "BuscarFornecedores", { codFrn: codFornecedor },
        function (data) {
            carregarDropDinamicamente("dropFiltroFornecedorCarimbo", "CODFRN", "CodNomFrn", data, true);
            $('#filtroCodFrn').val($('#dropFiltroFornecedorCarimbo').val());
            atualizaMensagemModalCarimbo();
            LoadingOff();
        }, function () {
            atualizaMensagemModalCarimbo();
            LoadingOff();
        }
    );
}

// Busca carimbos de acordo com o filtro
function filtrarCarimbosInserir() {
    var codFiltro = ""
    var existeFiltro = false
    if ($('#dropFiltroCarimbos').val() == 3) {
        codFiltro = "codFrn=" + $('#filtroCodFrn').val();
        existeFiltro = $('#filtroCodFrn').val() != "";
    } else if ($('#dropFiltroCarimbos').val() == 2) {
        codFiltro = "codCelula=" + $('#dropFiltroCelula').val();
        existeFiltro = $('#dropFiltroCelula').val() != "";
    } else {
        codFiltro = "codCarimbo=" + $('#filtroCodCarimbo').val();
        existeFiltro = $('#filtroCodCarimbo').val() != "";
    }

    if (!existeFiltro) {
        atualizaMensagemModalCarimbo("INFORME_FILTRO", "msgWarning");
    } else {
        postAjaxUrlData("Carimbo", "FiltrarCarimbos", codFiltro,
            function (data) {
                if (data !== "") {
                    $('#gridCarimboModal').html(data);
                }
                atualizaMensagemModalCarimbo();
                LoadingOff();
            }, function () {
                atualizaMensagemModalCarimbo();
                LoadingOff();
            }
        );
    }
}


// Retorna a lista dos ids de pedidos checkados de um tipo de checkBox
function retornaListaDeValoresCheckados(idCheck) {

    var itensChecados = [];
    var size = idCheck.length;

    $('[id^="' + idCheck + '"]').each(function () {
        var $this = $(this);

        if ($this.is(":checked")) {
            itensChecados.push($this.attr("id").substr(size));
        }
    });

    return itensChecados;
}

// Pega a lista de itens checkados e coloca na lista de carimbos selecionados
function selecionarCarimbos() {
    var itens = retornaListaDeValoresCheckados("ckbIncCar");

    if (itens.length == 0) {
        atualizaMensagemModalCarimbo("SELECIONAR_CARIMBO", "msgWarning");
    } else {
        $('[id^="ckbIncCar"]').prop("checked", false);

        postAjax("Carimbo", "SelecionarCarimbos", { itensCheckados: itens },
            function (data) {
                if (data !== "") {
                    $('#gridCarimbosSelecionadosModal').html(data);
                }
                atualizaMensagemModalCarimbo();
                LoadingOff();
            }, function () {
                atualizaMensagemModalCarimbo();
                LoadingOff();
            }
        );
    }
}

// Pega a lista de itens selecionados checkados e remove do grid
function removerCarimbos() {
    var itens = retornaListaDeValoresCheckados("ckbExCar");

    if (itens.length == 0) {
        atualizaMensagemModalCarimbo("SELECIONAR_CARIMBO", "msgWarning");
    } else {
        $('[id^="ckbExCar"]').prop("checked", false);

        postAjax("Carimbo", "RemoverCarimbos", { itensCheckados: itens },
            function (data) {
                if (data !== "") {
                    $('#gridCarimbosSelecionadosModal').html(data);
                }
                atualizaMensagemModalCarimbo();
                LoadingOff();
            }, function () {
                atualizaMensagemModalCarimbo();
                LoadingOff();
            }
        );
    }
}

// Atualiza o valor utilizado da linha do grid apos edicao
function atualizaValorUlitizadoCampoCarimbo(idHid, idText) {
    var valorUtilizado = $('#' + idText).val();
    valorUtilizado = valorUtilizado.replace(/\./g, '');
    var codCarimbo = parseFloat($('#' + idHid).val());

    postAjax("Carimbo", "AtualizaValorUlitizadoCampoCarimbo", { valores: [codCarimbo, valorUtilizado] },
        function (data) {
        }, function () {
            atualizaMensagemModalCarimbo();
        },
        true
    );

}

// Acao do botao de concluir a insercao de carimbos
function concluirInsercaoCarimbos() {
    postAjax("Carimbo", "ValidarCarimbos", {},
        function (data) {
            if (data == "") {
                atualizaMensagemModalCarimbo();
            } else {
                $('#modal').modal('hide');
                $('#gridCarimbos').html(data);
                atualizaMensagem();
            }
            LoadingOff();
        }, function () {
            atualizaMensagemModalCarimbo();
            LoadingOff();
        }
    );
}

// Acao do botao de concluir apos excluir carimbo do grid da tela de acao
function removeCarimbo(codCarimbo) {
    postAjax("Carimbo", "RemoverCarimbo", { codCrb: codCarimbo },
        function (data) {
            if (data == "") {
                atualizaMensagemModalCarimbo();
            } else {
                $('#modal').modal('hide');
                $('#gridCarimbos').html(data);
                atualizaMensagem();
            }
            LoadingOff();
        }, function () {
            atualizaMensagemModalCarimbo();
            LoadingOff();
        }
    );
}
//CONSULTA SALDO CARIMBO

// Evento change do drop de Fornecedor para atribuir o codigo ao textBox correspondente
function dropFornecedorCarimbo_onChange() {
    $('#filtroCodFrn').val($('#dropFiltroFornecedorCarimbo').val());
}

// Busca carimbos de acordo com o filtro
function filtrarCarimbos() {
    var codFiltro = ""
    var existeFiltro = false
    if ($('#dropFiltroCarimbos').val() == 3) {
        codFiltro = "codFrn=" + $('#filtroCodFrn').val();
        existeFiltro = $('#filtroCodFrn').val() != "";
    } else if ($('#dropFiltroCarimbos').val() == 2) {
        codFiltro = "codCelula=" + $('#dropFiltroCelula').val();
        existeFiltro = $('#dropFiltroCelula').val() != "";
    } else {
        codFiltro = "codCarimbo=" + $('#filtroCodCarimbo').val();
        existeFiltro = $('#filtroCodCarimbo').val() != "";
    }

    codFiltro += "&flSaldo=" + ($('#filtroSaldoDisponivel:checked').length != 0)

    if (!existeFiltro) {
        atualizaMensagemResultadoConsultaSaldo("INFORME_FILTRO", "msgWarning");
    } else {
        postAjaxUrlData("ConsultaSaldoCarimbo", "FiltrarCarimbos", codFiltro,
            function (data) {
                if (data !== "") {
                    $('#consultaSaldo').html(data);
                }
                atualizaMensagemResultadoConsultaSaldo();
                LoadingOff();
            }, function () {
                atualizaMensagemResultadoConsultaSaldo();
                LoadingOff();
            }
        );
    }
}

// Atualiza a mensagem da index vinda do controller ou da propria tela
function atualizaMensagemResultadoConsultaSaldo(mensagem, tipo) {
    if (!!mensagem) {
        mostraMensagem(mensagem, tipo, "msgGeral");
    } else {
        atualizaMensagem("msgGeral");
    }
}

// Retorna a lista dos ids de pedidos checkados de um tipo de checkBox
function retornaListaDeValoresCheckados(idCheck) {

    var itensChecados = [];
    var size = idCheck.length;

    $('[id^="' + idCheck + '"]').each(function () {
        var $this = $(this);

        if ($this.is(":checked")) {
            itensChecados.push($this.attr("id").substr(size));
        }
    });

    return itensChecados;
}

// Sucesso ao alterar um carimbo pelo grid
function AlterarCarimboSuccess(content, success , obj) {

    if (content == "")
    {
        atualizaMensagem("msgModalCarimbo");
    }
    else
    {
        $('#modal').modal('hide');
    }
        
    LoadingOff();
}


/*
Abrir modal chamando uma acao e passando um objeto
*/
//function abrirModalExtratoCarimbo(ConsultaSaldoCarimbo, abrirModalExtratoCarimbo, ModalExtratocarimbo) {
//    if (div === abrirModalExtratoCarimbo) {
//        div = "#modalExtratoCarimbo"
//    }
//}


