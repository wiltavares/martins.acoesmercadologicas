﻿// Configura acao do radio button de acao complementar
function configuraAcaoOriginal(param) {
    var acaoOriginal = $('#divAcaoPrincipal');
    var tipoEvento = $('#dropTipoEvento');
    var evento = $('#dropEvento');
    var filial = $('#dropFilial');
    var btnFrnIncluirFornecedor = $('#btnFrnIncluirFornecedor');
    var btnFrnDespesasMartins = $('#btnFrnDespesasMartins');
    var cmdDelFrn = $('#divAcaoPrincipal');

    if (param) {
        acaoOriginal.show()
        tipoEvento.prop("disabled", true).trigger("chosen:updated");
        evento.prop("disabled", true).trigger("chosen:updated");
        //filial.prop("disabled", true).trigger("chosen:updated");
        btnFrnIncluirFornecedor.hide();
        btnFrnDespesasMartins.removeClass("hide");
        btnFrnDespesasMartins.show();

    } else {
        acaoOriginal.hide()
        $('#codAcaoPrincipal').val("");
        tipoEvento.prop("disabled", false).trigger("chosen:updated");
        evento.prop("disabled", false).trigger("chosen:updated");
        filial.prop("disabled", false).trigger("chosen:updated");
        btnFrnIncluirFornecedor.show();
        btnFrnDespesasMartins.hide();
    }
}

//carrega dados da acao principal após ela ter sido digitada
function changeAcaoPrincipal() {
    var tipoEvento = $('#dropTipoEvento');
    var evento = $('#dropEvento');

    var preAcordo = $('#IndPreAcordo').val();
    var filial = $('#dropFilial');

    var acaoPrincipal = $('#codAcaoPrincipal').val();
    if (!(acaoPrincipal == "")) {
        $.ajax({
            url: SiteUrl + "AcaoComercial/buscarAcaoPrincipal",
            type: 'POST',
            data: { codAcaoPrincipal: parseInt(acaoPrincipal), preAcordo: preAcordo },
            success: function (data) {
                if (data) {
                    tipoEvento.val(data.TIPEVTACOCMC);
                    tipoEvento.trigger("chosen:updated");

                    evento.val(data.CODEVTACOCMC);
                    evento.trigger("chosen:updated");

                    filial.val(data.CODFILEMPORIVBA);
                    filial.trigger("chosen:updated");

                    carregarEventosPorTipoEvento(atualizaEventosAutomatico, data.CODEVTACOCMC);
                    copiaFornecedor(acaoPrincipal);
                }
                else {
                    document.getElementById('codAcaoPrincipal').value = '';
                }
                atualizaMensagem();

            },
            error: function () {
                atualizaMensagem();
                document.getElementById('codAcaoPrincipal').value = '';
            }
        });
    }
}

//copia patrocinador da acao principal
function copiaFornecedor(acaoPrincipal) {
    $.ajax({
        url: SiteUrl + "AcaoComercial/atualizarFornecedoresAcaoPrincipal",
        type: 'POST',
        data: { codAcaoPrincipal: parseInt(acaoPrincipal) },
        success: function (data) {
            if (data) {
                $('#widgetPatrocinados').html(data);
                var selecionado = 0
                if ($('#rdbFornecedor').prop("checked")) {
                    configuraPatrocinador(1);
                }
                if ($('#rdbCarimbo').prop("checked")) {
                    configuraPatrocinador(2);
                }
                if ($('#rdbBonificacao').prop("checked")) {
                    configuraPatrocinador(3);
                }
                if ($('#rdbPremiacao').prop("checked")) {
                    configuraPatrocinador(4);
                }
            }
            atualizaMensagem();
            document.getElementById("lblAcaoComplementar").style.display = "none";
        },
        error: function () {
            atualizaMensagem();
        }
    });
}

//atualizar eventos após tipo evento ser atualizado
function atualizaEventosAutomatico(valor) {
    var evento = $('#dropEvento');
    evento.val(valor);
    evento.trigger("chosen:updated");
    dropEvento_onChange(false, valor)
}

// Configura acao do radio button de carimbo x fornecedor
function configuraPatrocinador(param) {
    var btnCarimbo = $('#btnCarimbo');
    var gridCarimbos = $('#gridCarimbos');
    var btnFornecedor = $('#btnFornecedor');
    var btnSaldoPlanoMkt = $('#btnSaldoPlanoMkt');
    var gridFornecedores = $('#gridFornecedores');
    var btnBonificacao = $('#btnBonificacao');
    var gridBonificacoes = $('#gridBonificacoes');
    var btnPremiacao = $('#btnPremiacao');
    var gridPremiacoes = $('#gridPremiacoes');

    btnCarimbo.hide();
    gridCarimbos.hide();
    btnFornecedor.hide();
    btnSaldoPlanoMkt.hide();
    gridFornecedores.hide();
    btnBonificacao.hide();
    gridBonificacoes.hide();
    btnPremiacao.hide();
    gridPremiacoes.hide();

    switch (param) {
        case 2:
            btnCarimbo.show();
            gridCarimbos.show();
            break;
        case 3:
            btnBonificacao.show();
            gridBonificacoes.show();
            break;
        case 4:
            btnPremiacao.show();
            gridPremiacoes.show();
            break;
        default:
            btnFornecedor.show();
            btnSaldoPlanoMkt.show();
            gridFornecedores.show();
            break;
    }
    $('#valorTIPACOMCD').val(param);
}

/*
* Carrega Drop de eventos de acordo com tipo evento
*/
function carregarEventosPorTipoEvento(onComplete, valor) {
    carregarDropPorFiltro('dropTipoEvento', 'dropEvento', 'CODEVTACOCMC', 'CODIGO_NOME', 'AcaoComercial', 'ListarEventosDoTipoEventoAno');

    $.ajax({
        url: SiteUrl + "AcaoComercial/BuscarTipoEvento/" + $('#dropTipoEvento').val(),
        type: 'POST',
        success: function (data) {
            if (data.INDVLDRGRICT == "1") {
                $('#btnSlcIncentivo').show()
            }
            else {
                $('#btnSlcIncentivo').hide()
            }
            if (data.INDCPHBTB == "1") {
                $('#divDESCPHBTB').show()
            }
            else {
                $('#divDESCPHBTB').hide()
            }

            if (data.INDCPHBTB == "1") {
                $('#divDESCPHBTXB').show()
            }
            else {
                $('#divDESCPHBTXB').hide()
            }

            if (!(onComplete === undefined)) {
                onComplete(valor);
            }
            ChosenResize();
        },
        error: function (error) { }
    });

    $('#txtEmpenhoExa').val("");
    $('#txtEmpenhoExa').change();
    configuraTelaEmpenhoExa("");
}

/*
* Configura comportamento da tela de acordo com o empenho exa selecionado
*/
function configuraTelaEmpenhoExa(empenho, idIncentivo) {
    var rdbFornecedor = $('#rdbFornecedor');
    var rdbCarimbo = $('#rdbCarimbo');
    var rdbPremiacao = $('#rdbPremiacao');
    var rdbBonificacao = $('#rdbBonificacao');
    var rdbBonificacao = $('#rdbBonificacao');
    var btnSlcIncentivo = $('#btnSlcIncentivo');
    var lblTaxasESaldosFornecedor = $('#lblTaxasESaldosFornecedor');

    rdbCarimbo.attr('disabled', 'disabled');

    if (empenho === "99 - ACOES COMERCIAIS") {
        //   rdbCarimbo.removeAttr("disabled");
        rdbFornecedor.removeAttr("disabled");
        rdbPremiacao.removeAttr("disabled");
        rdbBonificacao.removeAttr("disabled");

        //rdbFornecedor.attr('checked', 'checked');
        //rdbCarimbo.removeAttr('checked');
        //rdbPremiacao.removeAttr('checked');
        //rdbBonificacao.removeAttr('checked');
        lblTaxasESaldosFornecedor.hide();
    } else {
        rdbFornecedor.prop('checked', true);
        rdbFornecedor.attr('disabled', 'disabled');
        //rdbCarimbo.attr('disabled', 'disabled');
        rdbPremiacao.attr('disabled', 'disabled');
        rdbBonificacao.attr('disabled', 'disabled');
        lblTaxasESaldosFornecedor.show();
        //configuraPatrocinador(true);

        configuraPatrocinador(1)
    }
}

// Busca os dados de empenho exa acordo a partir do evento
function buscaEmpenhoExaAcordo(idEvento) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + 'AcaoComercial/BuscaEmpenhoExaAcordo?idEvento=' + idEvento,
        type: 'GET',
        success: function (data) {
            $('#txtEmpenhoExa').val(data.TIPDSNDSCBNF + ' - ' + data.DESDSNDSCBNF);
            $('#txtEmpenhoExa').change();
            configuraTelaEmpenhoExa(data.TIPDSNDSCBNF + ' - ' + data.DESDSNDSCBNF);
            if (data.INDVCLPMC == 1) {
                $('#divPromocaoVinculada').show();
            } else {
                $('#divPromocaoVinculada').hide();
            }


            LoadingOff();
        },
        error: function () {
            LoadingOff();
        }
    });
}

function dropEvento_onChange(existeFuncionarios, idEvento) {
    if (existeFuncionarios) {
        $('#confirmaMudancaEvento').modal();
    } else {
        if ($('#dropEvento').val() == "") {
            $('#txtEmpenhoExa').val("");
            $('#txtEmpenhoExa').change();
            configuraTelaEmpenhoExa("");
            $('#divPromocaoVinculada').hide();
        } else {
            buscaEmpenhoExaAcordo(idEvento);
        }
    }
}

function atualizarValoresEstorno() {

    LoadingOn();

    var itensChecados = [];

    $('[id^="ckbEst"]').each(function () {
        var $this = $(this);

        if ($this.is(":checked")) {
            itensChecados.push($this.attr("id").substr(6));
        }
    });

    $.ajax({
        url: SiteUrl + 'AcaoComercial/RecalcularEstorno',
        type: 'POST',
        data: { itensChecados: itensChecados },
        success: function (data) {
            $('#gridEstornoFornecedores').html(data);
            LoadingOff();
        },
        error: function () {
            LoadingOff();
        }
    });
}


// Marcar ou desmarcar todos os checkbox
function marcarOuDesmarcarTodos() {
    if ($('#ckbTodos').is(":checked")) {
        $('[id^="ckbEst"]').prop("checked", true);
    } else {
        $('[id^="ckbEst"]').prop("checked", false);
    }
    atualizarValoresEstorno();
}

// Acao do botao de concluir apos excluir Premiacao do grid da tela de acao
function removePremiacao(index) {
    postAjax("PremiacaoExclusiva", "RemoverPremiacao", { indexPremiacao: index },
        function (data) {
            $('#modal').modal('hide');
            $('#gridPremiacoes').html(data);
            atualizaMensagem();
            LoadingOff();

        }, function () {
            LoadingOff();
        }
    );
}
// Finalizar o botao de salvar de acao
function salvarAcaoComplete(data) {
    atualizaMensagem();
    $('#seEnviar').val(false);

    if (data.responseText.indexOf('window.location') === -1 && data.responseText.indexOf('abrirModal') === -1) {
        if (data.responseText != "") {
            $('#divValidacaoDados').html(data.responseText);
        }
        $('.input-daterange').datepicker({ autoclose: true, language: 'pt-BR' });
        LoadingOff();
    }
}

//verifica se a data inicial da validade é menor que a de criação
//function validaDataValidade(dataCriada) {
//    if (dataCriada == "") {
//        return 0;
//    }
//    var codTipoAprovador = $('#txtDataValidadeInicial').val();
//    if (!codTipoAprovador == "") {
//        var dataIni = $('#txtDataValidadeInicial').val().split('/');
//        var datafin = $('#txtDataValidadeFinal').val().split('/');
//        var dataC = dataCriada.split('/');

//        atualizaMensagem();

//        if (dataIni[2] > dataC[2]) {
//            mostraMensagem("VALIDADE_ACAOCOMERCIAL_ANTERIOR", "msgWarning");
//        } else if (dataIni[1] == dataC[1] && dataIni[0] < dataC[0]) {
//            mostraMensagem("VALIDADE_ACAOCOMERCIAL_ANTERIOR", "msgWarning");
//        } else if (dataIni[1] == dataC[1] && dataIni[2] == dataC[2] && dataIni[1] > dataC[1]) {
//            mostraMensagem("VALIDADE_ACAOCOMERCIAL_ANTERIOR", "msgWarning");
//        } else if (dataIni[2] == dataC[2] && dataIni[1] == dataC[1] && dataIni[0] > datafin[0]) {
//            mostraMensagem("VALIDADE_ACAOCOMERCIAL_ANTERIOR", "msgWarning");
//        }
//    }
//}

function EnviarAcaoMercadologica(controller, action, obj, div, onSuccess) {
    LoadingOn()

    //verificar se abre modal
    //$.ajax({
    //    url: SiteUrl + controller + "/" + action,
    //    type: 'POST',
    //    data: { "objeto": obj },
    //    success: function (data) { onSuccess(data) },
    //    error: function (error) { onError(error) }

    //});

    $.ajax({
        url: SiteUrl + "AcaoComercial/Enviar",
        type: 'POST',
        data: { acaoViewModel: obj },
        success: function (data) {
            if (data) {
                $('#modal').html(data);
            }
            //$('#modal').modal('hide');
            //atualizaMensagem();
            LoadingOff();

        },
        error: function () {
            atualizaMensagem();
            LoadingOff();

        }
    });
}

function btnInserirItemAcaoComercial() {
    if ($('#CODACOCMC').val() === undefined) {
        mostraMensagem("SALVAR_ACAO_PARA_INSERIR_ITEM", "msgWarning", "msgGeral");
        return
    }
    if ($('#CODACOCMC').val() == '') {
        mostraMensagem("SALVAR_ACAO_PARA_INSERIR_ITEM", "msgWarning", "msgGeral");
        return
    }
    abrirModal('ItensOrcamento', 'IncluirItem', '')
}

function btnImportItemOrcamento() {
    if ($('#CODACOCMC').val() === undefined) {
        mostraMensagem("SALVAR_ACAO_PARA_INSERIR_ITEM", "msgWarning", "msgGeral");
        return
    }
    if ($('#CODACOCMC').val() == '') {
        mostraMensagem("SALVAR_ACAO_PARA_INSERIR_ITEM", "msgWarning", "msgGeral");
        return
    }
    abrirModal('ItensOrcamento', 'AbrirModarImportItemOrcamento', undefined, '#modal', ModalImportXlsItensOrcamentoSuccess);
}


function ModalImportXlsItensOrcamentoSuccess() {
    // variavel para salvar o arquivo
    var files;

    // evento ao selecionar arquivo
    $('input[type=file]').on('change', prepareUpload);

    // salva o arquivo na variavel
    function prepareUpload(event) {
        files = event.target.files;
    }

    $('#arquivo').on('submit', uploadFiles);

    // ao submeter o formulario
    function uploadFiles(event) {
        event.stopPropagation(); // para execução
        event.preventDefault(); // para completamente a execução

        var data = new FormData();

        if (files == undefined) {
            return false;
        }
        $.each(files, function (key, value) {
            data.append(key, value);
        });

        data.append("INDTIPOPEDSNACOCMC", document.getElementById("dropOperacao").value);
        //data.append("CODFILEMP", document.getElementById("dropFilial").value);
        data.append("CODUNDESRNGCOPEDSN", document.getElementById("drupBU").value);
        data.append("CODCENCST", document.getElementById("centroCustoID").value);
        data.append("DESFRMUTZITEACOCMC", document.getElementById("FormaUtlz").value);



        $.ajax({
            url: SiteUrl + 'ItensOrcamento/importarPlanilhaItemOrcamento',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'text',
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR) {
               // LoadingOn();
                if (data != null) {
                    if (typeof data.error !== 'undefined') {
                        console.log('ERRORS: ' + data.error);
                    //} else if (data.indexOf("<div id=\"gridItensOrcamento\">") !== -1) {
                    //    $('#divWidgetOrcamento').html(data);
                    //    $('#modal').modal("hide");
                    }
                    else if (data === "") {
                        $('#modal').modal("hide");
                        atualizaMensagem();
                    } else {
                        //$('#modal').modal("hide");
                        $('#modal').html(data);
                        $('#modal').modal();
                        AtualizaWidgetOrcamento();
                    }
                }
                //LoadingOff();
                //atualizaMensagem();
              
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
                atualizaMensagem();
                AtualizaWidgetOrcamento();
            }
        });

        //atualizaMensagem();
    }

    $('.ace-file-container').ace_file_input({
        no_file: 'Sem Arquivo ...',
        btn_choose: 'Selecione',
        btn_change: 'Mudar',
        droppable: false,
        onchange: null,
        thumbnail: false
    });
}

function cancelarImportacaoItensOrcamento() {
    $('#modal').modal('hide');
}


function btnEnviarFuncaoClick() {
    if (!confirmSalvar()) {
        console.log("Fim - If " + new Date());
        return false;
    }
    else {
        $('#seEnviar').val(true);
        return true;
    }

    //document.getElementById("btnSalvar").click();
}

function confirmaEnvio(atributo) {
    if (atributo == 1) {
        $('#confirmaEnviar1').val(true);
    }
    if (atributo == 2) {
        $('#confirmaEnviar2').val(true);
    }
    $('#seEnviar').val(true);
    document.getElementById("btnSalvar").click();
    $('#modal').modal('hide');
}

function fechaModalEnvio() {
    $('#modal').modal('hide');
    $('#seEnviar').val(false);
}

function selecionarGerente() {

    LoadingOn();
    $.ajax({
        url: SiteUrl + "ItensOrcamento/ConsultaPrecoUnitarioItem",
        type: 'POST',
        data: { codMercadoria: codMercadoria, codFilial: filial },
        success: function (data) {
            if (idtite) {
                $('#IDTITERSU').val(idtite);
            }
            else {
                $('#IDTITERSU').val('0');
            }
            $('#idItemCodMer').val(codMercadoria);
            $('#itemBusca').val(desMercadoria);
            $('#VlrUntario').val(parseFloat(data).toFixed(4));
            if (vlrpco) {
                $('#VlrpagoForn').val(parseFloat(vlrpco).toFixed(4));
                $('#VlrUntario').val(parseFloat(vlrpco).toFixed(4));

            }
            else {
                $('#VlrpagoForn').val(parseFloat(data).toFixed(4));
            }

            document.getElementById("btBscIte").style.display = "none";
            document.getElementById("btTiraIte").style = "";
            document.getElementById("itemBusca").readOnly = true;
            limparCacheFiltroItens();
            $('#modalBuscaItem').modal('hide');
            LoadingOff();
        },
        error: function () {
            atualizaMensagem(divMsg);
            LoadingOff();
        }
    });
}

function modalBuscarGerente() {
    var idGerente = $('#idGerente').val();
    var nomGerente = $('#Gerente').val();

    if (idGerente == "") {
        idGerente = 0
    }

    $.ajax({
        url: SiteUrl + "AcaoComercial/BuscarFuncionariosModal",
        type: 'POST',
        data: { idGerente: parseInt(idGerente), nomGerente: nomGerente },
        success: function (data) {
            if (data) {
                $('#modalBuscaGerente').html(data);
                $('#modalBuscaGerente').modal();
            }
        },
        error: function () {
            atualizaMensagem();
        }
    });
}


function buscarGerente() {
    var idGerente = $('#idGerenteM').val();
    var nomGerente = $('#nomFuncionarioM').val();

    if (idGerente == "") {
        idGerente = 0
    }

    $.ajax({
        url: SiteUrl + "AcaoComercial/BuscarFuncionariosModalResult",
        type: 'POST',
        data: { idGerente: parseInt(idGerente), nomGerente: nomGerente },
        success: function (data) {
            if (data) {
                $('#resultadosGerente').html(data);
            }
            //atualizaMensagem();
        },
        error: function () {
            atualizaMensagem();
        }
    });
}

function selecionarGerente(idGerente, nomGerente) {
    LoadingOn();
    $('#idGerente').val(idGerente);
    $('#Gerente').val(nomGerente);

    document.getElementById("btBscGer").style.display = "none";
    document.getElementById("btTiraGer").style = "";
    document.getElementById("idGerente").readOnly = true;
    document.getElementById("Gerente").readOnly = true;
    $('#modalBuscaGerente').modal('hide');
    LoadingOff();
}

function tirarGerente() {
    $('#idGerente').val("");
    $('#Gerente').val("");

    document.getElementById("btBscGer").style = "";
    document.getElementById("btTiraGer").style.display = "none";
    document.getElementById("idGerente").readOnly = false;
    document.getElementById("Gerente").readOnly = false;
}

function cancelarBuscaGerente() {
    $('#modalBuscaGerente').modal('hide');
}

function enviarGerenteSelecionado() {
    $('#confirmaEnviarGerente').val($('#idGerente').val());
    $('#seEnviar').val(true);
    document.getElementById("btnSalvar").click();
    $('#modal').modal('hide');
}

function copiaDataIni() {
    $('#dataIniAux').val($('#dataIni').val());
}

function copiaDataFim() {
    $('#dataFimAux').val($('#dataFim').val());
}

function confirmSalvar() {
    var canalVenda = document.getElementById('IndCanalVendaVisible').value
    var dataIni = $('#txtDataValidadeInicial').val().split('/');
    var CODACOCMC = $('#CODACOCMC').val();
    var dtIni = new Date(dataIni[2], dataIni[1], dataIni[0]);
    var dataCriada = new Date().toLocaleDateString().split('/');
    var dtCriada = new Date(dataCriada[2], dataCriada[1], dataCriada[0]);

    if ($('#radioComplementarSim').prop("checked")) {
        if ($('#codAcaoPrincipal').val() == 0 || $('#codAcaoPrincipal').val() == '') {
            document.getElementById("lblAcaoComplementar").style.display = "block";
            atualizaMensagem();
            return false;
        }
        else {
            document.getElementById("lblAcaoComplementar").style.display = "none";
            atualizaMensagem();
        }
    }

    //Verifica se o Canal foi preenchido
    if (canalVenda == "True") {
        if ($('#dropCanalDeVenda').val() == "") {
            document.getElementById("lblCanaldeVenda").style.display = "block";
            atualizaMensagem();
            return false;
        }
        else {
            document.getElementById("lblCanaldeVenda").style.display = "none";
            atualizaMensagem();
        }
    }


    if (CODACOCMC == "" && dtIni < dtCriada) {
        mostraMensagem("VALIDADE_ACAOCOMERCIAL_ANTERIOR", "msgWarning");
        return false;
    }
    return true;
}

function selecionarFuncionarioAprovacao(CODFNC, NOMFNC) {
    $('#idCodFuncionario').val(CODFNC);
    //$('#nomFuncionario').val(CODFNC + ' - ' + NOMFNC);
    $('#nomFuncionario').val(NOMFNC);

    document.getElementById("nomFuncionario").readOnly = true;
    document.getElementById("btBuscaFunc").style.display = "none";
    document.getElementById("btTiraFunc").style = "";
    $('#modalFuncionarios').modal('hide');
}

function modalBuscaFuncionariosAprovacao() {
    abrirModal('Acaocomercial', 'AbrirModalFuncionarioAprovacao', $('#nomFuncionario').val(), '#modalFuncionarios');
    atualizaMensagem("msgBuscaFuncionarios");

}

function tiraFuncionarioAprovacao() {
    $('#idCodFuncionario').val("");
    $('#nomFuncionario').val("");

    document.getElementById("btBuscaFunc").style = "";
    document.getElementById("btTiraFunc").style.display = "none";
    document.getElementById("nomFuncionario").readOnly = false;
}



function BuscaFuncionariosAprovacao() {
    var codfuncionario = $('#FilterCodFuncionario').val();
    var nomfuncionario = $('#FilternomeFuncionario').val();

    $.ajax({
        url: SiteUrl + "AcaoComercial/BuscaFuncionariosAprovacao",
        type: 'POST',
        data: { codfuncionario: codfuncionario, nomfuncionario: nomfuncionario },
        success: function (data) {
            if (data !== "") {
                $("#resultadosFuncionarios").html(data);
            }
            atualizaMensagem("msgBuscaFuncionarios");
        },
        error: function () {
            atualizaMensagem("msgBuscaFuncionarios");
        }
    });
}

function aprovacaoRadiochange(valor) {
    var divPreAcordo = $('#divPreAcordo');

    if (valor == 1) {
        divPreAcordo.show()
    }
    else {
        divPreAcordo.hide()
        $('#dropFormaPagamento').val('').trigger('chosen:updated');
        $('#txtNumDias').val("");
    }

    if (valor == 4) {
        document.getElementById("modalFunc").style = "";
    }
    else {
        document.getElementById("modalFunc").style.display = "none";
        tiraFuncionarioAprovacao();
    }
}

function fimModal() {
    atualizaMensagem();
    $('#modal').modal('hide');
    LoadingOff();
}
function fimModalCancel() {
    $('#modal').modal('hide');
}

function confirmaAprovacao() {
    var erros = 0
    var rdbAprovacaoAprovar = document.getElementById('rdbAprovacaoAprovar').checked;

    if ($('#parecerRdb').prop("checked")) {
        if ($('#idCodFuncionario').val() == 0 || $('#idCodFuncionario').val() == '') {
            document.getElementById("ValidNomFunc").style.display = "block";
            atualizaMensagem();
            erros = 1
        }
        else {
            document.getElementById("ValidNomFunc").style.display = "none";
            atualizaMensagem();
        }
    }

    if ($('#AprovacaoFluxoObs').val() === undefined || $('#AprovacaoFluxoObs').val() == '') {
        document.getElementById("lblDescricaoAprovacao").style.display = "block";
        atualizaMensagem();
        erros = 1
    }
    else {
        document.getElementById("lblDescricaoAprovacao").style.display = "none";
        atualizaMensagem();
    }

    if (rdbAprovacaoAprovar == true) {
        if ($('#dropFormaPagamento').val() === undefined || $('#dropFormaPagamento').val() == '') {
            document.getElementById("lblCampObrigFormPag").style.display = "block";
            atualizaMensagem();
            erros = 1
        }
        else {
            document.getElementById("lblCampObrigFormPag").style.display = "none";
            atualizaMensagem();
        }

        if ($('#txtNumDias').val() === undefined || $('#txtNumDias').val() == '') {
            document.getElementById("lblCampObrigNumdias").style.display = "block";
            atualizaMensagem();
            erros = 1
        }
        else {
            document.getElementById("lblCampObrigNumdias").style.display = "none";
            atualizaMensagem();
        }
    }

    if (erros != 0) {
        return false
    }
    return true;
}


function AtualizarCanalDeVenda() {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "ItensOrcamento/AtualizarCanalDeVenda",
        type: 'POST',
        data: {},
        success: function (data) {
            if (data == "True") {
                document.getElementById("divCanalDeVenda").style.display = "block"
                document.getElementById('IndCanalVendaVisible').value = true
            }
            else {
                document.getElementById("divCanalDeVenda").style.display = "none"
                document.getElementById('IndCanalVendaVisible').value = false
            }
            atualizaMensagem();
            LoadingOff();
        },
        error: function () {
            atualizaMensagem();
            LoadingOff();
        }
    });
}

function EstornarItensAcao() {
    var codAcaoComercial = $('#idCodAcaoComercial').val();
    var itensChecados = [];

    $('[id^="ckbEst"]').each(function () {
        var $this = $(this);

        if ($this.is(":checked")) {
            itensChecados.push($this.attr("id").substr(6));
        }
    });


    $.ajax({
        url: SiteUrl + "AcaoComercial/EstornarItensAcao",
        type: 'POST',
        data: { itensChecados: itensChecados, codAcaoComercial: codAcaoComercial },
        success: function (data) {
            if (data === 'True') {
                //$('#modal').html('');
                $('#modal').modal('hide');
                atualizaMensagem();
            } else
                atualizaMensagem("msgModalEstorno");
        },
        error: function () {
            atualizaMensagem("msgModalEstorno");
        }
    });
}

function AtualizaValorFornecedorPreAcordo() {
    var acao = $('#CODACOCMC').val();

    $.ajax({
        url: SiteUrl + "AcaoComercial/AtualizaValorFornecedorPreAcordo",
        type: 'POST',
        data: { acao: acao },
        success: function (data) {
            $('#widgetPatrocinados').html(data);
            atualizaMensagem();
        },
        error: function () {
            atualizaMensagem();
        }
    });
}




function AbrirModalEditMecanicaAcao() {
    if ($('#CODACOCMC').val() === undefined || $('#CODACOCMC').val() == '') {
        mostraMensagem("SALVAR_ACAO_PARA_INSERIR_MECANICA", "msgWarning", "msgGeral");
        return
    }
    abrirModal('acaocomercial'
        , 'ExibirMecanica'
        , undefined
        , "#modalMecanicaPromocao"
        , function (data) {
            resizeModal($('#modalMecanicaPromocao'));
        }
    );
}

function FecharModalMecanicaAcao() {
    $('#modalMecanicaPromocao').modal('hide');
    location.reload();
}