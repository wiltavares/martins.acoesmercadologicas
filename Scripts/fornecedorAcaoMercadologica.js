﻿// Atualiza a mensagem do modal vinda do controller ou da propria tela
function atualizaMensagemModalFornecedor(mensagem, tipo) {
    if (!!mensagem) {
        mostraMensagem(mensagem, tipo, "msgModalInserirFornecedor");
    } else {
        atualizaMensagem("msgModalInserirFornecedor");
    }
}

// Sucesso ao alterar um fornecedor pelo grid
function AlterarFornecedorSuccess() {
    $('#modal').modal('hide');
    atualizaMensagem();
    LoadingOff();
}

// Sucesso ao alterar um fornecedor pelo grid
function AlterarFornecedorModalSuccess() {
    atualizaMensagemModalFornecedor();
    LoadingOff();
}

function carregarDescricaoDivisaoDeComprar_DiretoriaCompraPorFornecedor(id) {
    HabilitaDesabilitaDrop(id)
}

function HabilitaDesabilitaDrop(id) {
    var DescricaoDivisaoDeCompra = $('#DivDescricaoDivisaoDeCompra');
    var DivDiretoriaCompra = $('#DivDiretoriaCompra');
    var CodFornecedor = $('#txtCodFornecedor');
    var NmFornecedor = $('#txtNmFornecedor');
    var PesquisarFornecedor = $('#divPesquisarFornecedor');

    if (id == "0") {
        DescricaoDivisaoDeCompra.hide().trigger("chosen:updated");
        DivDiretoriaCompra.show().trigger("chosen:updated");
        CodFornecedor.hide().trigger("chosen:updated");
        NmFornecedor.hide().trigger("chosen:updated");
        PesquisarFornecedor.hide().trigger("chosen:updated");
    }
    if (id == "1") {
        DescricaoDivisaoDeCompra.show().trigger("chosen:updated");
        DivDiretoriaCompra.hide().trigger("chosen:updated");
        CodFornecedor.hide().trigger("chosen:updated");
        NmFornecedor.hide().trigger("chosen:updated");
        PesquisarFornecedor.hide().trigger("chosen:updated");
    }
    if (id == "2") {
        DescricaoDivisaoDeCompra.hide().trigger("chosen:updated");
        DivDiretoriaCompra.hide().trigger("chosen:updated");
        CodFornecedor.show().trigger("chosen:updated");
        NmFornecedor.show().trigger("chosen:updated");
        PesquisarFornecedor.show().trigger("chosen:updated");
    }
    ChosenResize();
}

function fornecedores_onchange() {
    $('#CodFornecedor').val($("#dropFornecedores").val());
}

function fornecedoresEdl_onchange() {
    $('#CodFornecedor').val($("#dropFornecedoresEdl").val());
}

function bu_onchange() {
    $('#hdfDESUNDESRNGC').val($("#dropBus option:selected").text());
}

// Pesquisa o fornecedor pelo codigo e carrega o dropdown
function pesquisarFornecedor() {
    var id = $('#CodFornecedor').val();
    var tipoFornecedor = $('#dropTipoFornecedor').val();

    postAjax("Fornecedor", "BuscarFornecedores", { codFrn: id, tipofornecedor: tipoFornecedor },
        function (data) {
            if (tipoFornecedor == 1) {
                carregarDropDinamicamente("dropFornecedoresEdl", "CODFRN", "CodNomFrn", data, true);
                $('#CodFornecedor').val($("#dropFornecedoresEdl").val());
            }
            else {
                carregarDropDinamicamente("dropFornecedores", "CODFRN", "CodNomFrn", data, true);
                $('#CodFornecedor').val($("#dropFornecedores").val());
            }
            atualizaMensagemModalFornecedor();
            LoadingOff();
        }, function () {
            atualizaMensagemModalFornecedor();
            $('#CodFornecedor').val("");
            $('#CodFornecedor').change();
            LoadingOff();
        }
    );
}

// Atribui o mesmo beneficio para todos os fornecedores
function inserirBeneficioParaOsfornecedores() {
    var sBeneficio = $('#BeneficioGenerico').val();

    ("Fornecedor", "InserirBeneficioParaOsfornecedores", { sBeneficio: sBeneficio },
        function (data) {
            if (data !== "") {
                $('#BeneficioDadosFornecedor').val(sBeneficio);
                $('#BeneficioDadosFornecedor').change();
                $('#BeneficioGenerico').val(sBeneficio);
                $('#BeneficioGenerico').change();
                $('#gridInserirFornecedores').html(data);
            }
            atualizaMensagemModalFornecedor();
            LoadingOff();

        }, function () {
            atualizaMensagemModalFornecedor();
            $('#BeneficioDadosFornecedor').val("");
            $('#BeneficioGenerico').val("");
            LoadingOff();
        }
    );
}

// Calcula rateio e distribui aos fornecedores
function CalculaRateio(controller, action) {
    LoadingOn();
    var vlrRateio = parseFloat($('#CalRateio').val().replace(/\./g, "").replace(/\,/g, "."));

    // verificar qual radio button está marcado
    if (document.getElementById("rbtFixo").checked == true) {
        tipo = "VALOR_FIXO";
    }
    else if (document.getElementById("rbtDivQuant").checked) {
        tipo = "VALOR_DIVIDO_QTD";
    }
    else {
        tipo = "PROPORCIONAL_SALDO";
    }

    if (vlrRateio == "" || isNaN(vlrRateio) || vlrRateio === 0) {
        mostraMensagem('INFORME_RATEIO', 'msgWarning', "msgModalRateioMsg");
        LoadingOff();
        return;
    }
    else {
        $.ajax({
            url: SiteUrl + "Fornecedor/CalcularRateio",
            type: 'POST', //'GET',
            data: JSON.stringify({ tipo: tipo, vlrRateio: vlrRateio }), contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data !== "") {
                    $('#gridInserirFornecedor').html(data);
                    $('#CalRateio').val("");
                }
                atualizaMensagemModalFornecedor();
                FecharRateio();
                LoadingOff();
            },
            error: function () {
                atualizaMensagemModalFornecedor();
                FecharRateio();
                LoadingOff();
            }
        });
    }
}

// Adiciona fornecedores ao grid de fornecedores do modal
function adicionarFornecedorGridView() {
    var tipoFornecedor = $('#dropTipoFornecedor').val();
    var id = $('#dropFornecedor').val();
    var codFornecedor = $('#CodFornecedor').val();
    var codCelula = $('#codCelula').val();
    var codDiretoria = $('#codDiretoria').val();
    var sUrl = "";
    var existeFiltro = false;

    if (id == "2") {
        sUrl = "codFrn=" + codFornecedor + "&tipoFornecedor=" + tipoFornecedor
        existeFiltro = codFornecedor != "";
        $('#CodFornecedor').val('');
    }
    if (id == "1") {
        sUrl = "codCelula=" + codCelula
        existeFiltro = codCelula != "";
    }
    if (id == "0") {
        sUrl = "codDiretoria=" + codDiretoria
        existeFiltro = codDiretoria != "";
    }

    if (!existeFiltro) {
        atualizaMensagemModalFornecedor("INFORME_FILTRO", "msgWarning");
    } else {
        postAjaxUrlData("Fornecedor", "AdicionarFornecedorGrid", sUrl,
            function (data) {
                if (data !== "") {
                    $('#gridInserirFornecedor').html(data);
                }
                atualizaMensagemModalFornecedor();
                LoadingOff();
            }, function () {
                atualizaMensagemModalFornecedor();
                LoadingOff();
            });
    }
}

// Exibe dados do fornecedor da tab de dados
function exibeDadosFornecedor(codFornecedor, nomeFornecedor) {
    LoadingOn()
    postAjax("Fornecedor", "ExibeDadosFornecedor", { codFrn: codFornecedor },
        function (data) {
            if (data !== "") {
                $("#DadosFornecedor").html(data);
                //$('#dropFornecedores').html("").trigger("chosen:updated");
                //$('#dropFornecedores').append($('<option></option>').val(codFornecedor).html(nomeFornecedor));
                //$('#CodFornecedor').val(codFornecedor);
            } else {
                atualizaMensagemModalFornecedor();
            }
            LoadingOff()
        }, function () {
            atualizaMensagemModalFornecedor();
            LoadingOff()
        }, true
    );
}

// Retorna a lista dos ids de pedidos checkados de um tipo de checkBox
function retornaListaDeFornecedoresCheckados() {
    var idCheck = "ckbFornecedor"
    var itensChecados = [];
    var size = idCheck.length;

    $('[id^="' + idCheck + '"]').each(function () {
        var $this = $(this);

        if ($this.is(":checked")) {
            itensChecados.push($this.attr("id").substr(size));
        }
    });

    return itensChecados;
}

// Acao do botao de concluir a insercao de pedidos
function concluirInsercaoFornecedor() {
    var tipoFornecedor = $('#dropTipoFornecedor').val();
    var percentualParticipacao = $('#textPercentualParticipacao').val();

    LoadingOn()
    $.ajax({
        url: SiteUrl + "Fornecedor/ValidaInserirFornecedores/",
        type: 'POST',
        data: { tipoFornecedor: tipoFornecedor, percentualParticipacao: percentualParticipacao },
        success: function (data) {
            if (data != "") {
                $('#modal').modal('hide');
                $('#gridFornecedores').html(data);
            }
            atualizaMensagemModalFornecedor();
            LoadingOff();
        },
        error: function (e) {
            atualizaMensagemModalFornecedor();
            LoadingOff();
        }
    });
}

// Acao do botao de seleção de proporção de saldo
function defineProporcaoSaldo() {
    LoadingOn()
    $.ajax({
        url: SiteUrl + "Fornecedor/ValidaInserirFornecedores/", // precisa voltar para modal de inclusão de fornecedor
        type: 'POST',
        success: function (data) {
            if (data != "") {
                $('#modal').modal('hide');
                $('#gridFornecedores').html(data);
            }
            atualizaMensagemModalFornecedor();
            LoadingOff();
        },
        error: function () {
            atualizaMensagemModalFornecedor();
            LoadingOff();
        }
    });
}

// Acao do botao de concluir a insercao de pedidos
function concluirEditarFornecedor() {
    LoadingOn()
    $.ajax({
        url: SiteUrl + "Fornecedor/ValidaEditarFornecedores/",
        type: 'POST',
        success: function (data) {
            if (data == 0) {
                atualizaMensagemModalFornecedor();
            } else {
                $('#modal').modal('hide');
                $('#GetGridForneedoresModal').html(data);
            }
            LoadingOff();
        },
        error: function () {
            atualizaMensagemModalFornecedor();
            LoadingOff();
        }
    });
}

// Acao do botao de confirmar exclusao do fornecedor da tela de acao
function removeFornecedor(codFornecedor) {
    postAjax("Fornecedor", "RemoverFornecedor", { codFrn: codFornecedor },
        function (data) {
            if (data == "") {
                atualizaMensagemModalFornecedor();
            } else {
                $('#gridFornecedores').html(data);
                atualizaMensagem();
            }
            LoadingOff();
        }, function () {
            atualizaMensagemModalFornecedor();
            LoadingOff();
        }
    );
}

// Remove fornecedores de acordo com o saldo ou se nao tem dados
function removerFornecedoresPorSaldo(codTpSaldo) {
    postAjax("Fornecedor", "RemoverFornecedorPorSaldo", { codTpSaldo: codTpSaldo },
        function (data) {
            if (data == "") {
                atualizaMensagemModalFornecedor();
            } else {
                $('#gridInserirFornecedor').html(data);
                atualizaMensagemModalFornecedor();
            }
            LoadingOff();
        }, function () {
            atualizaMensagemModalFornecedor();
            LoadingOff();
        }
    );
}

// Remove todos os fornecedores selecionados
function removerFornecedoresSelecionados() {

    var itens = retornaListaDeFornecedoresCheckados();

    if (itens.length == 0) {
        atualizaMensagemModalFornecedor("SELECIONAR_FORNECEDOR", "msgWarning");
    } else {
        $('[id^="ckbFornecedor"]').prop("checked", false);

        postAjax("Fornecedor", "RemoverFornecedoresSelecionados", { itensCheckados: itens },
            function (data) {
                if (data != "") {
                    $('#gridInserirFornecedor').html(data);
                }
                atualizaMensagemModalFornecedor();
                LoadingOff();
            }, function () {
                atualizaMensagemModalFornecedor();
                LoadingOff();
            }
        );
    }
}

// Remove todos os fornecedores do grid do modal
function removerTodosFornecedores() {
    postAjax("Fornecedor", "RemoverTodosFornecedores", {},
        function (data) {
            if (data == "") {
                atualizaMensagemModalFornecedor();
            } else {
                $('#gridInserirFornecedor').html(data);
                atualizaMensagemModalFornecedor();
            }
            LoadingOff();
        }, function () {
            atualizaMensagemModalFornecedor();
            LoadingOff();
        }
    );
}

// Remove os fornecedores do grid do modal por filtro
function removerFornecedoresModal() {

    var id = $('#dropFornecedor').val();
    var codFornecedor = $('#CodFornecedor').val();
    var codCelula = $('#codCelula').val();
    var codDiretoria = $('#codDiretoria').val();
    var sUrl = "";
    //var existeFiltro = false;

    //var selecionar = $('#idCheck').val();
    var selecionar = $('[name=Selecionar]:checked')

    var listcods = "";
    if (selecionar.length != 0) {
        for (var i = 0; i < selecionar.length; i++) {
            var codFornecedor = $(selecionar[i]).val()

            if (listcods != "") {
                listcods += "|"
            }
            listcods += codFornecedor;
        }
        sUrl = "ListCodigos=" + listcods
    }

    if (selecionar.length == 0) {
        atualizaMensagemModalFornecedor("SELECIONAR_FORNECEDOR", "msgWarning");
    } else {
        postAjaxUrlData("Fornecedor", "RemoverFornecedores", sUrl,
            function (data) {
                if (data !== "") {
                    $('#gridInserirFornecedor').html(data);
                }
                atualizaMensagemModalFornecedor();
                LoadingOff();
            }, function () {
                atualizaMensagemModalFornecedor();
                LoadingOff();
            });
    }
}

// Abre o modal de confirmacao da exclusao, passa como parametros os metodos e mensagem de exclusao correspondente
function confirmaRemocao(codExc) {

    postAjax("Fornecedor", "ConfirmaRemoverFornecedorModal", { tipo: codExc },
        function (data) {
            if (data != "") {
                $('#modalFornecedor').html(data);
                $('#modalFornecedor').modal();
            }
            atualizaMensagemModalFornecedor();
        }, function () {
            atualizaMensagemModalFornecedor();
        }, true
    );
}

function modalAssociaBeneficioFornecedor() {
    abrirModalSemLoading('Fornecedor', 'AssociaBeneficioFornecedor', '', "#modal");
}


function modalDefTipoRateio() {
    abrirModalSemLoading('Fornecedor', 'DefinirTipoRateio', '', "#frmRateio");
}

function configuraTipoRateio(param) {
    if (param) {
        document.getElementById("radioProporcional").checked = true;
        document.getElementById("radioFixo").checked = false;
    } else {
        document.getElementById("radioProporcional").checked = false;
        document.getElementById("radioFixo").checked = true;
    }
}

function FecharRateio() {
    $('#frmRateio').modal('hide');

}

function changeTipoFornecedor(tipoFornecedor) {
    var dropFornecedor = $("#dropFornecedor");
    var txtNmFornecedor = $("#txtNmFornecedor");
    var txtNmFornecedorEdl = $("#txtNmFornecedorEdl");
    var txtCodFornecedor = $("#txtCodFornecedor");
    var DivDescricaoDivisaoDeCompra = $("#DivDescricaoDivisaoDeCompra");
    var DivDiretoriaCompra = $("#DivDiretoriaCompra");
    var PesquisarFornecedor = $('#divPesquisarFornecedor');

    $('#CodFornecedor').val('').trigger("chosen:updated");;
    $('#dropFornecedores').val('').trigger("chosen:updated");;
    $('#dropFornecedoresEdl').val('').trigger("chosen:updated");;

    if (tipoFornecedor.value == "0") { //Martins
        txtNmFornecedorEdl.hide().trigger("chosen:updated");
        txtNmFornecedor.show().trigger("chosen:updated");
        txtCodFornecedor.show().trigger("chosen:update");

        $("#dropFornecedor").prop("disabled", false).trigger("chosen:updated");

    }
    if (tipoFornecedor.value == "1") { //EDL
        txtNmFornecedor.hide().trigger("chosen:updated");
        txtNmFornecedorEdl.show().trigger("chosen:updated");
        txtCodFornecedor.show().trigger("chosen:update");

        DivDescricaoDivisaoDeCompra.hide().trigger("chosen:update");
        DivDiretoriaCompra.hide().trigger("chosen:update");

        $("#dropFornecedor").prop("disabled", true).trigger("chosen:updated");
        $("#dropFornecedor").val('2').trigger("chosen:updated");
        PesquisarFornecedor.show().trigger("chosen:updated");
    }
    ChosenResize();
}
