﻿window.onresize = function () {
    ChosenResize();    
}

$(document).on('show.bs.modal', '.modal', function () {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function () {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

function ShowModal(options) {

    if (options.titulo === undefined)
        options.titulo = "Confirme!";

    if (options.mensagem === undefined)
        options.mensagem = "Deseja confirmar?";


    if (options.appendElement === undefined)
        options.appendElement = "msgGeral";

    var htmlMensagem = '';
    htmlMensagem += '<div class="modal-generico modal-big modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >';
    var width = options.width != undefined ? options.width : 500;
    var widthS = options.widthS != undefined ? 'width: ' + options.widthS + 'px;' : '';

    if (options.widthS != undefined)
        htmlMensagem += '<div class="modal-dialog" style="' + widthS + '">';
    else
        htmlMensagem += '<div class="modal-dialog" style="max-width:' + width + 'px;">';

    htmlMensagem += '<div class="modal-content">';
    htmlMensagem += '<div class="modal-header">';
    htmlMensagem += '<h4 class="modal-title center">' + options.titulo + '</h4>';
    //htmlMensagem += '<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-left: 0px; margin-top: -31px;">';
    //htmlMensagem += '<span aria-hidden="true">&times;</span>';
    //htmlMensagem += '</button>';
    htmlMensagem += '</div>';
    htmlMensagem += '<div class="modal-body">';
    if(options.semPara != undefined)
        htmlMensagem += options.mensagem;
    else
        htmlMensagem += '<p>' + options.mensagem + '</p>';

    htmlMensagem += '</div>';

    htmlMensagem += '<div class="modal-footer">';
    htmlMensagem += '<button type="button" class="btn btn-warning btn-fechar">Fechar</button>';
    htmlMensagem += '</div>';
    htmlMensagem += '</div>';
    htmlMensagem += '</div>';
    htmlMensagem += '</div>';

    $('#' + options.appendElement).append(htmlMensagem);

    $('#' + options.appendElement + ' .modal-generico').modal('show').on('hidden.bs.modal', function () {
        $('#' + options.appendElement + ' .modal-generico').remove();
    });

    $('#' + options.appendElement + ' button.btn-fechar').click(function () {
        $('#' + options.appendElement + ' .modal-generico').modal('hide');
        //$('.modal-generico').remove();
    });
    
}

function ShowModalConfirmar(options) {

    if (options.titulo === undefined)
        options.titulo = "Confirme!";

    if (options.mensagem === undefined)
        options.mensagem = "Deseja confirmar?";


    if (options.appendElement === undefined)
        options.appendElement = "msgGeral";

    var htmlMensagem = '';
    htmlMensagem += '<div class="modal-confirmar modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >';

    if (options.width === undefined)
        htmlMensagem += '<div class="modal-dialog">';
    else
        htmlMensagem += '<div class="modal-dialog" style="width: ' + options.width + 'px">';


    htmlMensagem += '<div class="modal-content">';
    htmlMensagem += '<div class="modal-header">';
    htmlMensagem += '<h4 class="modal-title center" id="myModalLabel">' + options.titulo + '</h4>';
    //htmlMensagem += '<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-left: 0px;">';
    //htmlMensagem += '<span aria-hidden="true">&times;</span>';
    //htmlMensagem += '</button>';
    htmlMensagem += '</div>';
    htmlMensagem += '<div class="modal-body">';
    htmlMensagem += '<p>' + options.mensagem + '</p>';
    htmlMensagem += '</div>';

    htmlMensagem += '<div class="modal-footer">';
    htmlMensagem += '<button id="btnModalConfirmar" type="button" class="btn btn-success">Confirmar</button>';
    htmlMensagem += '<button id="btnModalCancelarConfirmacao" type="button" class="btn btn-warning">Cancelar</button>';
    htmlMensagem += '</div>';
    htmlMensagem += '</div>';
    htmlMensagem += '</div>';
    htmlMensagem += '</div>';

    $('#' + options.appendElement).append(htmlMensagem);
    $('.modal-confirmar').modal('show').on('hidden.bs.modal', function () {
        $('.modal-confirmar').remove();
    });

    $("#btnModalConfirmar").click(function () {
        $('.modal-confirmar').modal('hide');
        $('.modal-confirmar').remove();

        if (options.callback) {
            if (options.parametros)
                options.callback(options.parametros);
            else
                options.callback();

        }
    });

    $("#btnModalCancelarConfirmacao").click(function () {
        $('.modal-confirmar').modal('hide');
        $('.modal-confirmar').remove();
    });
}


function fnUrlSite() {
    
    var a = location.pathname.split("/");

    if (location.hostname.toLowerCase() == "localhost")
        return location.protocol + "//" + location.host + "/";
    else
        return location.protocol + "//" + location.host + "/" + a[1] + "/";
}

var SiteUrl = fnUrlSite();


// Atualiza mensagens vindas do controller na tela na div do parametro
function atualizaMensagem(div) {
    if (!div) {
        div = "msgGeral"
    }
    $("#" + div).load(SiteUrl + 'Alert/_Alerts', function () { });
}

// Mostra mensagem na tela na div do parametro
// tipo: msgInfo, msgDanger, msgWarning, msgSuccess
function mostraMensagem(msg, tipo, div) {
    if (!div) {
        div = "msgGeral"
    }
    $("#" + div).load(SiteUrl + 'Alert/_Alerts/?' + tipo + "=" + msg, function () { });
}

// Mostra modal de carregando
function LoadingOn() {
    $('#loadingDialog').modal({ show: true, backdrop: 'static' });
}

// Esconde o modal de carregando
function LoadingOff() {
    $('#loadingDialog').modal('hide');
    ChosenConfig();
    ChosenResize();
    MaskConfig();
    PopoverConfig();
    WidgetConfig();
    DatePickerConfig();
}

// Esconde o modal de carregando e atualiza a div de mensagens
function LoadingOff_UpdateMessage() {
    $('#loadingDialog').modal('hide');
    atualizaMensagem();
    ChosenConfig();
    ChosenResize();
    MaskConfig();
    PopoverConfig();
    WidgetConfig();
}

//Atualizar paginacao e ordenacao de grids
function atualizarGrid(pageConf, dadosPesquisa, urlAction, div) {

    LoadingOn();
    $.ajax({
        url: urlAction,
        type: 'POST',
        data: { "dadosPesquisa": dadosPesquisa },
        success: function (data) {
            $('#' + dadosPesquisa.ResultID).html(data);
            atualizaMensagem(div);
            LoadingOff();
        },
        error: function () {
            atualizaMensagem(div);
            LoadingOff();
        }
    });
}

//Redirecionar para acao usando janela de loading
function redirectLoading(url) {
    LoadingOn();
    document.location = url;
}

/*
Abrir modal chamando uma acao e passando um objeto
*/
function abrirModal(controller, action, obj, div, onSuccess, divMsg, dado) {
    if (div === undefined) {
        div = "#modal"
    }
    LoadingOn();
    $.ajax({
        url: SiteUrl + controller + "/" + action,
        type: 'POST',
        data: { "objeto": obj },
        success: function (data) {
            if (data !== "") {
                $(div).html(data);
                $(div).modal({ show: true, backdrop: 'static' }).on('shown.bs.modal', function () {
                    ChosenResize();
                });
                if (onSuccess) {
                    if (dado) {
                        onSuccess(dado);
                    }
                    else {
                        onSuccess();
                    }
                }
            } else {
                atualizaMensagem(divMsg);
            }
            LoadingOff();
        },
        error: function () {
            atualizaMensagem(divMsg);
            LoadingOff();
        }
    });
}

/*
Abrir modal chamando uma acao e passando um objeto sem loadinf
*/
function abrirModalSemLoading(controller, action, obj, div, divMsg) {
    if (div === undefined) {
        div = "#modal"
    }
    $.ajax({
        url: SiteUrl + controller + "/" + action,
        type: 'POST',
        data: { "objeto": obj },
        success: function (data) {
            if (data !== "") {
                $(div).html(data);
                $(div).modal({ show: true, backdrop: 'static' });
            } else {
                atualizaMensagem(divMsg);
            }
            ChosenConfig();
            ChosenResize();
            MaskConfig();
            PopoverConfig();
        },
        error: function () {
            ChosenConfig();
            ChosenResize();
            MaskConfig();
            PopoverConfig();
            atualizaMensagem(divMsg);
        }
    });
}

/*
Chama metodo post do controller passando dados campo data
*/
function postAjax(controller, action, obj, onSuccess, onError, loadingOff) {
    if (!loadingOff) {
        LoadingOn();
    }
    $.ajax({
        url: SiteUrl + controller + "/" + action,
        type: 'POST',
        data: obj,
        success: function (data) {
            onSuccess(data)
        },
        error: function (error) {
            onError(error)
        }
    });
}

/*
Chama metodo post do controller passando dados na url
*/
function postAjaxUrlData(controller, action, urlData, onSuccess, onError) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + controller + "/" + action + "/?" + urlData,
        type: 'POST',
        success: function (data) { onSuccess(data) },
        error: function (error) { onError(error) }
    });
}

/*
Chama metodo get do controller 
*/
function getAjax(controller, action, onSuccess, onError) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + controller + "/" + action,
        type: 'GET',
        success: function (data) { onSuccess(data) },
        error: function (error) { onError(error) }
    });
}

/*
Chama metodo get do controller passando dados na url
*/
function getAjaxUrlData(controller, action, urlData, onSuccess, onError) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + controller + "/" + action + "/?" + urlData,
        type: 'GET',
        success: function (data) { onSuccess(data) },
        error: function (error) { onError(error) }
    });
}

/*Deixa um componente visivel ou nao de acordo com o parametro*/
function hideOrShow(param, id) {
    var component = $(this);
    if (id) {
        component = $('#' + id);
    }
    if (param) {
        component.removeAttr("hide")
        component.show();
    } else {
        component.hide();
    }
}

/*
 * Carrega dropDow de acordo com a lista de saida. Parametros:
 *.idValor = campo onde tera o valor de pesquisa
 *.idDropFinal = dropdown que sera carregado
 *.campoValor = atributo do objeto que é o valor do drop
 *.campoNome = atributo do objeto que é o nome do drop
 *.controller = controller que contem a acao chamada
 *.action = acao que sera chamada para carregar o drop
*/
function carregarDropPorFiltro(idValor, idDropFinal, campoValor, campoNome, controller, action) {
    idAction = $('#' + idValor).val()
    if (idAction == "") {
        $('#' + idDropFinal).html("").trigger("chosen:updated");
        $('#' + idDropFinal).append(
                $('<option></option>').val("").html("Selecione ..."));
        $('#' + idDropFinal).trigger("chosen:updated");
    } else {
        LoadingOn();
        $.ajax({
            url: SiteUrl + controller + '/' + action + '?id=' + idAction,
            type: 'GET',
            success: function (lista) {
                $('#' + idDropFinal).html("").trigger("chosen:updated");
                $('#' + idDropFinal).append(
                        $('<option></option>').val("").html("Selecione ..."));
                $.each(lista, function (i, item) {
                    $('#' + idDropFinal).append(
                        $('<option></option>').val(item[campoValor]).html(item[campoNome]));
                });
                $('#' + idDropFinal).trigger("chosen:updated");
                LoadingOff();
            },
            error: function () {
                LoadingOff();
            }
        });
    }
}

/*
 * Carrega dropDow de acordo com a lista. Parametros:
 *.idDropDown = dropdown que sera carregado
 *.campoValor = atributo do objeto que é o valor do drop
 *.campoNome = atributo do objeto que é o nome do drop
 *.listaItens = lista dos itens para atribuir ao dropdown
 *.delegate = funcao a ser chamada depois da execucao do metodo
*/
function carregarDropDinamicamente(idDropDown, campoValor, campoNome, listaItens, semSelecione) {
    if (listaItens == undefined || listaItens.length == 0) {
        $('#' + idDropDown).html("").trigger("chosen:updated");
        if (!semSelecione) {
            $('#' + idDropDown).append(
                    $('<option></option>').val("").html("Selecione..."));
        }
        $('#' + idDropDown).trigger("chosen:updated");
    } else {
        $('#' + idDropDown).html("").trigger("chosen:updated");
        if (!semSelecione) {
            $('#' + idDropDown).append($('<option></option>').val("").html("Selecione..."));
        }
        $.each(listaItens, function (i, item) {
            $('#' + idDropDown).append(
                $('<option></option>').val(item[campoValor]).html(item[campoNome]));
        });
        $('#' + idDropDown).trigger("chosen:updated");
    }
}

function PopoverConfig() {
    $('[data-rel=popover]').popover({ html: true });
}

//Somente digitar numeros
function Numero(num) {
    if (document.all)
        var tecla = event.keyCode;
    else if (document.layers)
        var tecla = num.which;
    if (tecla > 47 && tecla < 58)
        return true;
    else {
        if (tecla == 44) {
            return true;
        }
        else {
            if (tecla != 8)
                event.keyCode = 0;
            else
                return true;
        }
    }
}
function ChosenConfig() {
    if (!IsMobile()) {
        $('.chosen-select').chosen({ allow_single_deselect: true });
        //resize the chosen on window resize

        $(window)
        .off('resize.chosen')
        .on('resize.chosen', function () {
            $('.chosen-select').each(function () {
                var $this = $(this);
                $this.next().css({ 'width': $this.parent().width() });
            })
        }).trigger('resize.chosen');
        //resize chosen on sidebar collapse/expand
        $(document).on('settings.ace.chosen', function (e, event_name, event_val) {
            if (event_name != 'sidebar_collapsed') return;
            $('.chosen-select').each(function () {
                var $this = $(this);
                $this.next().css({ 'width': $this.parent().width() });
            })
        });


        $('#chosen-multiple-style .btn').on('click', function (e) {
            var target = $(this).find('input[type=radio]');
            var which = parseInt(target.val());
            if (which == 2) $('#form-field-select-4').addClass('tag-input-style');
            else $('#form-field-select-4').removeClass('tag-input-style');
        });

    }

}

// Atualiza os drops com autocomplete
function chosenSelectUpdate() {
    if (!IsMobile()) {
        $('.chosen-select').chosen({ allow_single_deselect: true });
        //resize the chosen on window resize

        $(window)
            .off('resize.chosen')
            .on('resize.chosen', function () {
                $('.chosen-select').each(function () {
                    var $this = $(this);
                    $this.next().css({ 'width': $this.parent().width() });
                })
            }).trigger('resize.chosen');
        //resize chosen on sidebar collapse/expand
        $(document).on('settings.ace.chosen', function (e, event_name, event_val) {
            if (event_name != 'sidebar_collapsed') return;
            $('.chosen-select').each(function () {
                var $this = $(this);
                $this.next().css({ 'width': $this.parent().width() });
            })
        });
    }
}

function IsMobile() {
    return $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
}

function ChosenResize() {
    $("body").find('.chosen-container').each(function () {
        var size = parseInt($(this).prev().css('width').replace('px', ''));
        size = size - 24;

        $(this).css('width', size + 'px');
        $(this).find('a:first-child').css('width', size + 'px');
        $(this).find('.chosen-drop').css('width', size + 'px');
        $(this).find('.chosen-search input').css('width', (size - 10) + 'px');
    });
}


//function MaskConfig()
//{
$.mask.masks = $.extend($.mask.masks, {
    decimal_1: { mask: '9,999.999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },
    decimal_2: { mask: '99,999.999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },
    decimal_3: { mask: '999,999.999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },
    decimal_4: { mask: '9999,999.999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },
    decimal_5: { mask: '99999,999.999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },
    decimal_6: { mask: '999999,999.999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },
    decimal_7: { mask: '9999999,999.999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },
    decimal_8: { mask: '99999999,999.999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },
    decimal_9: { mask: '999999999,999.999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },

    decimal_positivo_1: { mask: '9,999.999.999.999', type: 'reverse', autoTab: false },
    decimal_positivo_2: { mask: '99,999.999.999.999', type: 'reverse', autoTab: false },
    decimal_positivo_3: { mask: '999,999.999.999.999', type: 'reverse', autoTab: false },
    decimal_positivo_4: { mask: '9999,999.999.999.999', type: 'reverse', autoTab: false },
    decimal_positivo_5: { mask: '99999,999.999.999.999', type: 'reverse', autoTab: false },
    decimal_positivo_6: { mask: '999999,999.999.999.999', type: 'reverse', autoTab: false },
    decimal_positivo_7: { mask: '9999999,999.999.999.999', type: 'reverse', autoTab: false },
    decimal_positivo_8: { mask: '99999999,999.999.999.999', type: 'reverse', autoTab: false },
    decimal_positivo_9: { mask: '999999999,999.999.999.999', type: 'reverse', autoTab: false },

    inteiro: { mask: '999999999999', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_positivo: { mask: '999999999999', type: 'repeat', autoTab: false },

    inteiro_sep_negativo_1: { mask: '9', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_2: { mask: '99', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_3: { mask: '999', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_4: { mask: '999.9', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_5: { mask: '999.99', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_6: { mask: '999.999', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_7: { mask: '999.999.9', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_8: { mask: '999.999.99', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_9: { mask: '999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_10: { mask: '999.999.999.9', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_11: { mask: '999.999.999.99', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_12: { mask: '999.999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_13: { mask: '999.999.999.999.9', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_14: { mask: '999.999.999.999.99', type: 'reverse', defaultValue: '+', autoTab: false },
    inteiro_sep_negativo_15: { mask: '999.999.999.999.999', type: 'reverse', defaultValue: '+', autoTab: false },

    inteiro_sep_positivo_1: { mask: '9', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_2: { mask: '99', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_3: { mask: '999', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_4: { mask: '999.9', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_5: { mask: '999.99', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_6: { mask: '999.999', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_7: { mask: '999.999.9', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_8: { mask: '999.999.99', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_9: { mask: '999.999.999', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_10: { mask: '999.999.999.9', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_11: { mask: '999.999.999.99', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_12: { mask: '999.999.999.999', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_13: { mask: '999.999.999.999.9', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_14: { mask: '999.999.999.999.99', type: 'reverse', autoTab: false },
    inteiro_sep_positivo_15: { mask: '999.999.999.999.999', type: 'reverse', autoTab: false },

    currency: { mask: '99,999.999.999', type: 'reverse', defaultValue: '+' },
    currency_positivo: { mask: '99,999.999.999', type: 'reverse', defaultValue: '' },
    datepicker: { mask: '99/99/9999', autoTab: false },
    datetime: { mask: '99/99/9999 99:99:99', autoTab: false },
    time_hh_mm_ss: { mask: '99:99:99', autoTab: false },
    time_hh_mm: { mask: '99:99', autoTab: false },
    phone: { mask: '999999999?9', autoTab: false },
    phone9: { mask: '999999999?9', autoTab: false },
    phone_with_ddd: { mask: '(99) 99999-99999?9', autoTab: false },
    placaVeiculo: { mask: 'aaa9999', autoTab: false },
    cep: { mask: '99999-999', autoTab: false },
    cpf: { mask: '999.999.999-99', autoTab: false },
    cnpj: { mask: '99.999.999/9999-99', autoTab: false },
    cnae: { mask: '9999-9/99', autoTab: false },

    porcentagem: { mask: '99,999%', type: 'reverse', autoTab: false }
});
//}

function MaskConfig() {
    $('input[type="text"]').setMask();

    //Telefone 9 e 8 digitos
    $("[alt=phone9]").each(function () {
        if (this.value.length == 9) {
            $(this).setMask("99999-99999?9");
        } else {
            $(this).setMask("9999-99999?9");
        }
    });
    $("[alt=phone9]").on('keypress', function (event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unsetMask();
        if (phone.length < 8) {
            element.setMask("9999-99999?9");
        } else {
            element.setMask("99999-99999?9");
        }
    });
    $("[alt=phone9]").on('keyup', function (event) {
        if (event.keyCode == 8) { //backspace
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target);
            element.unsetMask();
            if (phone.length = 8) {
                element.setMask("9999-99999?9");
            }
        }
    });

    //Telefone com DDD e 9 ou 8 digitos
    $("[alt=phone_with_ddd]").each(function () {
        if (this.value.length == 15) {
            $(this).setMask("(99) 99999-99999?9");
        } else {
            $(this).setMask("(99) 9999-99999?9");
        }
    });
    $("[alt=phone_with_ddd]").on('keypress', function (event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unsetMask();
        if (phone.length < 10) {
            element.setMask("(99) 9999-99999?9");
        } else {
            element.setMask("(99) 99999-99999?9");
        }
    });
    $("[alt=phone_with_ddd]").on('keyup', function (event) {
        if (event.keyCode == 8) { //backspace
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target);
            element.unsetMask();
            if (phone.length = 10) {
                element.setMask("(99) 9999-99999?9");
            }
        }
    });
}

function WidgetConfig() {
    $(document).on('shown.ace.widget hidden.ace.widget closed.ace.widget', '.widget-box', function (event) {
        ChosenResize();
    });
}

MaskConfig();
PopoverConfig();
WidgetConfig();

//Função responsável por alterar todas as ocorrências de uma Substring em uma string
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};



function DatePickerConfig() {
    $('.date-picker').datepicker({
        autoclose: true,
        language: 'pt-BR',
        todayHighlight: true
    })
        //show datepicker when clicking on the icon
        .next().on(ace.click_event, function () {
            $(this).prev().focus();
        });
}