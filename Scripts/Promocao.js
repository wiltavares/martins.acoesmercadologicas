﻿
$(document).ready(function () {
    BuscarListaBrindes();
    ControleCriterioApuracao()
    ckbCrterioAdicional_OnClick();
    chbBrindeAdicional_OnClick();
    BuscarComunicacao();

    //
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    widgetPromocaoPublicoAlvo(CODPMC, TIPEDEPUBALVPMC);
    //

    //
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    widgetPromocaoMix(CODPMC, TIPEDEPUBALVPMC);
    //

    var CODPMC = $('#IDCodPmc').val();
    //widgetPromocaoClientesExcludentes(CODPMC);

    updateBlockDisabled();
    //$('input[type="text"]').setMask();

    ckbPromocaoRelampago_OnClick($('#ckbPromocaoRelampago').is(':checked'))
    $('#ckbPromocaoRelampago').prop('disabled', true);

});

function editCancelBlock(divType, el) {
    if ($(el).children('i')[0].className.indexOf('fa-times') >= 0)
        location.reload();
    else {
        if (lstAllowedWidgets.length > 0) {

            var nomeBloco = '';
            switch (lstAllowedWidgets[0]) {
                case "Brindes":
                    nomeBloco = "Brindes";
                    break;
                case "Promocao":
                    nomeBloco = "Promoção";
                    break;
                case "Comunicacao":
                    nomeBloco = "Comunicação";
                    break;
                default:
            }

            ShowModal({ mensagem: "Não é possível editar dois blocos simultaneamente. Salve as alterações do bloco <b>\"" + nomeBloco + "\"</b> (em edição) e tente novamente.", titulo: "Atenção" })
            return;
        } else {
            findAndRemove(lstBlockedWidgets, divType);
            lstAllowedWidgets.push(divType);

            updateBlockDisabled();

            setBlockedPmcImd();

            $(el).children('i')[0].className = $(el).children('i')[0].className.replace('edit', 'times');

        }
    }


}

function editPromocao_Complete(data) {
    LoadingOff_UpdateMessage()
}

function editPromocao_Success(data) {
    editCancelBlock('Promocao', $('#editCancelBlockButton'))
}

function findAndRemove(array, obj) {
    if (array == null)
        return true;

    array.forEach(function (result, index) {
        if (JSON.stringify(result) === JSON.stringify(obj)) {
            //Remove from array
            array.splice(index, 1);
        }
    });
}

var lstBlockedWidgets = ["Promocao", "Criterio", "Brindes", "Comunicacao"];
var lstAllowedWidgets = [];

function updateBlockDisabled() {
    var situacao = $('#hdfSituacaoPromocao').val();

    if (situacao === 'E') {
        lstBlockedWidgets.forEach(function (result, index) {
            $('#widget' + result).addClass('disabledEdit');
            $('#widget' + result).parent().addClass('cursorNotAllowed');
            $('#widget' + result).find('.btn[type="submit"]').hide();
            $('#widget' + result).find('.button[type="submit"]').hide();
            $('#widget' + result).find('button.btn-success').hide();
        });


        mostraMensagem('Promocao%20encerrada,%20não%20é%20possivel%20editar.', 'msgInfo');
    }
    else {
        lstBlockedWidgets.forEach(function (result, index) {
            $('#widget' + result).addClass('disabledEdit');
            $('#widget' + result).parent().addClass('cursorNotAllowed');
            $('#widget' + result).find('.btn[type="submit"]').hide();
            $('#widget' + result).find('.button[type="submit"]').hide();
            $('#widget' + result).find('button.btn-success').hide();
        });

        lstAllowedWidgets.forEach(function (result, index) {
            $('#widget' + result).removeClass('disabledEdit');
            $('#widget' + result).parent().removeClass('cursorNotAllowed');
            $('#widget' + result).find('.btn[type="submit"]').show();
            $('#widget' + result).find('.button[type="submit"]').show();
            $('#widget' + result).find('button.btn-success').show();
        });
    }

    $('#editPromocao').addClass($('#classEdit').val());
    $('#editCriterio').addClass($('#classEdit').val());
    $('#editBrindes').addClass($('#classEdit').val());
    $('#editComunicacao').addClass($('#classEdit').val());

    setBlockedInput();

}

function setBlockedInput() {
    $('input[readonly="readonly"]').css('pointer-events', 'none')
    var inputsReadOnly = $('input.date-interval[readonly="readonly"]');

    inputsReadOnly.each(function (i, item) {
        if ($(item).parent().hasClass('input-group'))
            $(item).parent().addClass('cursorNotAllowed');
        else
            $(item).addClass('cursorNotAllowed');
    });
}

function setBlockedPmcImd() {
    var situacao = $('#hdfSituacaoPromocao').val();
    if (situacao == "I") {
        $('#ckbPromocaoRelampago').removeAttr('disabled');
        $('#DATINIPMC').removeAttr('disabled');
        $('#horaIni').removeAttr('disabled');
    }
    else {
        $('#ckbPromocaoRelampago').prop('disabled', true);
        $('#DATINIPMC').prop('disabled', true);
        $('#horaIni').prop('disabled', true);
    }

    //remover classe cursorNotAllowed
    //remover style
    $('#DATFIMPMC').removeAttr('readonly');
    $('#DATFIMPMC').removeAttr('style');
    $('#horaFim').removeAttr('readonly');
    $('#horaFim').removeAttr('style');
}


function BuscarComunicacao() {

    var codPmc = $('#IDCodPmc').val();
    getAjax("PromocaoComunicacao", "Edit" + '/' + codPmc,
        function (data) {
            $("#divWidgetPromocaoComunicacao .widget-main").html(data);

            if ($('#tblGridLivro input[type="radio"][name$="TIPDVGPMCLIVPCO"]:checked').val() == undefined)
                $('#tblGridLivro input[type="radio"][name$="TIPDVGPMCLIVPCO"]').first().prop('checked', true)

            $('#chbTipCmnPmc_3').change(function () {
                if ($(this).is(":checked")) {
                    $('#tblGridLivro').removeClass('hide');
                } else {
                    $('#tblGridLivro').addClass('hide');
                }
            });

            $('#codigoSimbolo').on("keyup", function (event) {
                if ($('#codigoSimbolo').val().length > 1)
                    $('#codigoSimbolo').val($('#codigoSimbolo').val().substring(0, 1))
                var arr = [];
                for (var i = 65; i <= 90; i++) {
                    arr.push(i);
                }
                for (var i = 97; i <= 122; i++) {
                    arr.push(i);
                }

                if (jQuery.inArray($('#codigoSimbolo').val().charCodeAt(0), arr) === -1) {
                    $('#codigoSimbolo').val('');
                }
            });
            updateBlockDisabled();
            LoadingOff();
        },
        function (data) {
            $("#divWidgetPromocaoComunicacao .widget-main").html('<div class="alert auto-close alert-info fade in">Erro ao carregar comunicação</div>');
            LoadingOff();
        }
    );
}

function salvarComunicacoes() {

    if ($('#chbTipCmnPmc_3').prop('checked') === true && $('#codigoSimbolo').val() === '') {
        ShowModal({ mensagem: "Favor preencher o campo Código Simbolo.", titulo: "Atenção" });
        return;
    }

    var codPmc = $('#IDCodPmc').val();
    var comunicacao = [];
    $('#tblGridRegistrosComunicacao tr:has(td)').each(function (index, value) {
        if ($(value).eq(0).find('input').prop('checked') === true) {
            var newitem = { TIPCMNPMC: ($(value).eq(0).find('input[type="Hidden"][name$="TIPCMNPMC"]').val()) }
            if (newitem.TIPCMNPMC == 3) {
                newitem.CODSMBDVGPMCLIVPCO = $('#codigoSimbolo').val();
                newitem.TIPDVGPMCLIVPCO = $('#tblGridLivro input[type="radio"][name$="TIPDVGPMCLIVPCO"]:checked').val();
            }
            comunicacao[comunicacao.length] = newitem
        }
    });



    var data = {
        CODPMC: codPmc,
        RelacaoComunicacao: comunicacao,
    }


    //var RelComunicacao = $('#tblGridRegistrosComunicacao tr:has(td)').map(function (i, v) {
    //    var $td = $('td', this);
    //    return {
    //        TIPCMNPMC: $td.eq(1).find('label').text(),
    //        CODPMC: $td.eq(0).find('input').prop('checked') === true ? codPmc : 0,
    //    }
    //}).get();

    //var ParamComunicacao = {
    //    CODSMBDVGPMCLIVPCO: $('#codigoSimbolo').val(),
    //    TIPDVGPMCLIVPCO: $('[name="formaDivulgRadio"]:checked').val(),
    //    CODPMC: codPmc
    //};

    //var data = {
    //    CODPMC: codPmc,
    //    ParamComunicacao: ParamComunicacao,
    //    RelComunicacao: RelComunicacao
    //}

    LoadingOn();
    postAjax("PromocaoComunicacao", "Alterar", data, function (data) {
        if (data !== undefined && data !== null) {
            if (data !== 'Salvar')
                ShowModal({ mensagem: data, titulo: "Atenção", width: 750 });
        }
        LoadingOff();
    }, function () { LoadingOff(); }, false);
}

function BuscarListaBrindes() {

    var codPmc = $('#IDCodPmc').val();
    getAjax("PromocaoBrinde", "List" + '/' + codPmc,
        function (data) {
            $("#divListBrindeResultadosView").html(data);
            updateBlockDisabled();
            chbBrindeAdicional_OnClick()
            $('#CODACAOBRINDE').val($('#idCodAcaoPromocao').val());

            if ($('[name="TIPEDENIVPRMPMC"] option').first().val() == '')
                $('[name="TIPEDENIVPRMPMC"] option').first().remove();
            LoadingOff();
        },
        function (data) {
            $("#divListBrindeResultadosView").html('<div class="alert auto-close alert-info fade in">Erro ao carregar brindes</div>');
            updateBlockDisabled();
            LoadingOff();
        }
    );
}

//Função Tipo_Promoção e QtdGrpFaixa
function ControleCriterioApuracao() {

    var codPmc = $('#IDCodPmc').val();
    var tipo = $('#idTipoPromocao').val();

    getAjax("CriterioApuracao", "Edit" + '/' + codPmc + '/' + tipo,
        function (data) {
            $("#divCriterioApuracaoEditView").html(data);
            updateBlockDisabled();
            LoadingOff();
            $('.numberInput, .desc').keyup(function () {
                if ($(this).val().replace(/\s/g, '').length) {
                    $(this).removeClass('classError')
                }
            });
        },
        function (data) {
            $("#divCriterioApuracaoEditView").html('<div class="alert auto-close alert-info fade in">Erro ao carregar critério de apuração</div>');
            updateBlockDisabled();
            LoadingOff();
        }
    );
}

function CreateBrinde_Complete() {
    atualizaMensagem();
    BuscarListaBrindes();
    $('body > div.modal-backdrop.fade').remove();

    var scrollTopBrindes = $('#widgetBrindes').parents('.widget-box.widget-color-wi')[0].offsetTop + 50;
    $('html').scrollTop(scrollTopBrindes);
}

function ckbCrterioAdicional_OnClick() {

    var criterioAdicional = $('#ckbCrterioAdicional').is(":checked");

    $('#divVendaAvaliacaoPromocaoCres').hide();
    $('#divValorLimite').hide();
    $('#divQtdMin').hide();
    if (criterioAdicional === true) {
        $('#divVendaAvaliacaoPromocaoCres').show();
        $('#divValorLimite').show();
        $('#divQtdMin').show();
    }
    else {
        $('#divVendaAvaliacaoPromocaoCres').hide();
        $('#divValorLimite').hide();
        $('#divQtdMin').hide();
    }
}

function AlertaCobrancaBrinde() {
    var CobrancaBrinde = $('#chbCobrancaBrinde').is(":checked");
    if (CobrancaBrinde === true) {
        ShowModal({ mensagem: "Brinde terá custo para o cliente", titulo: "Atenção" })
        //alert("Brinde terá custo para o cliente")
    }
}

function modalBuscarItens() {
    var desMerc = $('#itemBusca').val();
    var idMerc = $('#idItemCodMer').val();
    var objeto = { CODMER: idMerc, DESMER: desMerc };

    abrirModal('ItensOrcamento', 'BuscaMercadorias', objeto, "#modalBuscaItem", terminarModalBuscarMercadoria, 'msgModalBrinde', objeto);
    //btnBuscaItens(desMerc);
}

function terminarModalBuscarMercadoria(objeto) {
    $("#idItem").val(objeto.CODMER);
    $("#DesIte").val(objeto.DESMER);
    btnBuscaItens();
}

function btnBuscaItens(mercadoria) {
    LoadingOn();
    var descItem = $('#DesIte').val();
    if (mercadoria) {
        descItem = mercadoria
    }
    var idItem = $('#idItem').val();
    if (idItem.length > 10) {
        document.getElementById("lblErroTamanhoIdMercadoria").style.display = "block";
        LoadingOff();
        return;
    }
    else {
        document.getElementById("lblErroTamanhoIdMercadoria").style.display = "none";
    }
    if (!idItem) {
        idItem = 0
    }
    $.ajax({
        url: SiteUrl + "ItensOrcamento/BuscaGridMercadoriasBrinde",
        type: 'POST',
        data: { idMerc: idItem, desMerc: descItem },
        success: function (data) {
            if (data !== "") {
                $("#resultadosBuscaItens").html(data);
                atualizaMensagem('msgBuscaMercadorias');
                if (idItem != 0) {
                    var botao = $("#merc_".concat(idItem));
                    if (botao != undefined) {
                        botao.click();
                    } else {
                        LoadingOff();
                    }
                } else {
                    LoadingOff();
                }
            } else {
                atualizaMensagem();
                LoadingOff();
            }
        },
        error: function () {
            atualizaMensagem('msgBuscaMercadorias');
            LoadingOff();
        }
    });
}

function selecionarMercadoria(codMercadoria, desMercadoria, idtite, vlrpco) {
    $('#idItemCodMer').val(codMercadoria);
    $('#itemBusca').val(desMercadoria.substring(0, 50));

    document.getElementById("btBscIte").style.display = "none";
    document.getElementById("btTiraIte").style = "";
    document.getElementById("itemBusca").readOnly = true;
    document.getElementById("idItemCodMer").readOnly = true;
    limparCacheFiltroItens();
    $('#modalBuscaItem').modal('hide');

    //

    var CODPMC = $('#IDCodPmc').val();

    if ($('#chbCobrancaBrinde').is(':checked')) {
        $.ajax({
            url: SiteUrl + "PromocaoBrinde" + "/" + "ValoresBrinde" + '/' + CODPMC + '/' + codMercadoria,
            type: 'GET',
            success: function (data) {
                if (data.Custo >= 0) {
                    $('#custoIncluirBrinde').val(data.Custo)
                    $('#valorIncluirBrinde').val(data.Custo)
                    $('#precoIncluirBrinde').val(data.ValorMedio)

                    validaPerc();
                } else {
                    $('#custoIncluirBrinde').val(0)
                    $('#valorIncluirBrinde').val(0)
                    $('#precoIncluirBrinde').val(0)
                    ShowModal({ mensagem: "O item não é principal nem substituto nesta ação comercial.", titulo: "Atenção" })
                    tirarItem();
                }
                LoadingOff();
            },
            error: function (data) {
                tirarItem();
                LoadingOff();
                ShowModal({ mensagem: "Não foi possível carregar os valores do brinde.", titulo: "Atenção" })
            }
        });
    }
}

function validaPerc() {
    MaskConfig();
    if ($('#percIncluirBrinde').val() === '') {
        $('#percIncluirBrinde').addClass('classError');
    } else {
        $('#percIncluirBrinde').removeClass('classError');

        if (isNaN($('#percIncluirBrinde').val().replace(',', '.')) || $('#percIncluirBrinde').val().replace(',', '.') < 0) {
            $('#percIncluirBrinde').addClass('classError');
            $('#percIncluirBrinde').val('');
            return;
        } else if ($('#percIncluirBrinde').val().replace(',', '.') > 100) {
            $('#percIncluirBrinde').val('100,00');
            return;
        }

        MaskConfig();
    }
}

function chbBrindeAdicional_OnClick() {
    var adicional = $('#chbBrindeAdicional').is(":checked");
    if (adicional === true) {
        $("#divTipoBrinde").show();
        $("#divResultadoGripAdicional").show();

    }
    else {
        $("#divTipoBrinde").hide();
        $("#divResultadoGripAdicional").hide();
    }
}

//Buscar todos os campos do grid RelacaoPromocaoCriterioApuracaoEscalaProgressiva  T0107960 Normal
function jsListaGridCriterioApuracaoEscalaProgressivaRegistros(continua) {
    if (continua === undefined) {
        verificaCriterio(jsListaGridCriterioApuracaoEscalaProgressivaRegistros);
        return;
    }

    var tblGridRegistros = $('#tblGridEscalaProgressivaRegistros tr:has(td)').map(function (i, v) {
        var $td = $('td', this);
        return {
            CODCTRPMC: $td.eq(1).find('select option:selected').val(),
            TAMIVLFXAEQCCALCTR: $td.eq(2).find('input').val(),
            VLRLIMMNMFXACTRPMC: $td.eq(2).find('input').val(),
            TIPEDENIVPRMPMC: $td.eq(3).find('input').val()
        }
    }).get();

    var CodFormaCalculo = $('#dropFormaCalculoEscalaProgressivaRegistros').val();
    var QDEPTOFXACTRPMC = $('#TxtQtdPremioAtingidoEscalaProgressivoRegistro').val();
    var codPmc = $('#hdfCodPmcEscalaProgressivaResgistros').val();
    var situacao = $('#hdfSituacaoPromocao').val();

    var data = {
        ParametrosCalculo: tblGridRegistros,
        CodFormaCalculo: CodFormaCalculo,
        QDEPTOFXAEQCCALCTR: QDEPTOFXACTRPMC,
        QDEPTOFXACTRPMC: QDEPTOFXACTRPMC,
        CODPMC: codPmc,
        TIPSITPMC: situacao,
        Combinada: false
    };

    LoadingOn();
    //data.CODPMC
    postAjax("CriterioApuracao", "Alterar", data, function (data) {
        if (data !== undefined && data !== null) {
            if (data === 'Salvar')
                location.reload();
            else
                ShowModal({ mensagem: data, titulo: "Atenção", width: 750 });
        }
        LoadingOff();
    }, function () { LoadingOff(); }, false);
    //LoadingOff();
}

function jsListaGridCriterioApuracaoEscalaProgressivaCombinada(continua) {

    if (continua === undefined) {
        verificaCriterio(jsListaGridCriterioApuracaoEscalaProgressivaCombinada);
        return;
    }

    var tblGridRegistros = $('#tblGridRegistrosCombinada tr:has(td)').map(function (i, v) {
        var $td = $('td', this);
        return {
            CODGRPMIXMERPMC: $td.eq(0).find('label').text(),
            CODCTRPMC: $td.eq(1).find('select option:selected').val(),
            TAMIVLFXAEQCCALCTR: $td.eq(2).find('input').val(),
            VLRLIMMNMFXACTRPMC: $td.eq(2).find('input').val(),
            DESCTRMIXPMC: $td.eq(3).find('input').val(),
            TIPEDENIVPRMPMC: $td.eq(4).find('input').val()
        }
    }).get();

    var CodFormaCalculo = $('#dropFormaCalculoEscalaProgressivaCombinada').val();
    var QDEPTOFXACTRPMC = $('#TxtQtdPremioAtingidoEscalaProgressivoCombinada').val();
    var codPmc = $('#hdfCodPmcEscalaProgressivaCombinada').val();
    var situacao = $('#hdfSituacaoPromocao').val();

    var data = {
        ParametrosCalculo: tblGridRegistros,
        CodFormaCalculo: CodFormaCalculo,
        QDEPTOFXAEQCCALCTR: QDEPTOFXACTRPMC,
        QDEPTOFXACTRPMC: QDEPTOFXACTRPMC,
        CODPMC: codPmc,
        TIPSITPMC: situacao,
        Combinada: true
    };

    LoadingOn();
    //data.CODPMC
    postAjax("CriterioApuracao", "Alterar", data, function (data) {
        if (data !== undefined && data !== null) {
            if (data === 'Salvar')
                location.reload();
            else
                ShowModal({ mensagem: data, titulo: "Atenção", width: 750 });
        }
        LoadingOff();
    }, function () { LoadingOff(); }, false);
    //LoadingOff();
}

function jsListaGridCriterioApuracaoFaixaFinitaFaixa(continua) {

    if (continua === undefined) {
        verificaFaixas();
        return;
    }

    var tblGridRegistros = $('#tblGridFaixaFinitaFaixa tr:has(td)').map(function (i, v) {
        var $td = $('td', this);
        var valorMax = $td.eq(3).find('input').val() === 'Acima' ? $td.eq(5).find('input').val().replace(',00', '').replaceAll('.', '') : $td.eq(3).find('input').val().replace(',00', '').replaceAll('.', '')
        return {
            VLRLIMMNMFXACTRPMC: $td.eq(1).find('input').val().replace(',00', '').replaceAll('.', ''),
            VLRLIMMAXFXACTRPMC: valorMax,
            TIPEDENIVPRMPMC: $td.eq(4).find('input').val()
        }
    }).get();

    var CodFormaCalculo = $('#dropFormaCalculoFaixaFinitaFaixa').val();
    var CodCriterio = $('#dropCriterioFaixaFinitaFaixa').val();
    var QDEPTOFXACTRPMC = $('#TxtQtdPremioAtingidoFaixaFinitaFaixa').val();
    var codPmc = $('#hdfCodPmcFaixaFinitaFaixa').val();
    var situacao = $('#hdfSituacaoPromocao').val();

    var data = {
        ParametrosCalculo: tblGridRegistros,
        CodFormaCalculo: CodFormaCalculo,
        QDEPTOFXACTRPMC: QDEPTOFXACTRPMC,
        CODPMC: codPmc,
        TIPSITPMC: situacao,
        Combinada: false,
        CODCTRPMC: CodCriterio
    };

    LoadingOn();
    //data.CODPMC
    postAjax("CriterioApuracao", "Alterar", data, function (data) {
        if (data !== undefined && data !== null) {
            if (data === 'Salvar')
                location.reload();
            else
                ShowModal({ mensagem: data, titulo: "Atenção", width: 750 });
        }
        LoadingOff();
    }, function () { LoadingOff(); }, false);
}

function jsListaGridCriterioApuracaoFaixaFinitaRegistros(continua) {

    if (continua === undefined) {
        verificaCriterio(jsListaGridCriterioApuracaoFaixaFinitaRegistros);
        return;
    }

    var tblGridRegistros = $('#tblGridFaixaFinitaRegistros tr:has(td)').map(function (i, v) {
        var $td = $('td', this);
        return {
            CODCTRPMC: $td.eq(1).find('select option:selected').val(),
            VLRLIMMNMFXACTRPMC: $td.eq(2).find('input').val(),
            TAMIVLFXAEQCCALCTR: $td.eq(2).find('input').val(),
            TIPEDENIVPRMPMC: $td.eq(3).find('input').val()
        }
    }).get();

    var CodFormaCalculo = $('#dropFormaCalculoFaixaFinitaRegistros').val();
    var QDEPTOFXACTRPMC = $('#TxtQtdPremioAtingidoFaixaFinitaRegistros').val();
    var codPmc = $('#hdfCodPmcFaixaFinitaRegistros').val();
    var situacao = $('#hdfSituacaoPromocao').val();

    var data = {
        ParametrosCalculo: tblGridRegistros,
        CodFormaCalculo: CodFormaCalculo,
        QDEPTOFXACTRPMC: QDEPTOFXACTRPMC,
        QDEPTOFXAEQCCALCTR: QDEPTOFXACTRPMC,
        CODPMC: codPmc,
        TIPSITPMC: situacao,
        Combinada: false
    };

    LoadingOn();
    //data.CODPMC
    postAjax("CriterioApuracao", "Alterar", data, function (data) {
        if (data !== undefined && data !== null) {
            if (data === 'Salvar')
                location.reload();
            else
                ShowModal({ mensagem: data, titulo: "Atenção", width: 750 });
        }
        LoadingOff();
    }, function () { LoadingOff(); }, false);
    //LoadingOff();
}

function jsListaGridCriterioApuracaoFaixaFinitaCombinada(continua) {

    if (continua === undefined) {
        verificaCriterio(jsListaGridCriterioApuracaoFaixaFinitaCombinada);
        return;
    }

    var tblGridRegistros = $('#tblGridRegistrosCombinada tr:has(td)').map(function (i, v) {
        var $td = $('td', this);
        return {
            CODGRPMIXMERPMC: $td.eq(0).find('label').text(),
            CODCTRPMC: $td.eq(1).find('select option:selected').val(),
            VLRLIMMNMFXACTRPMC: $td.eq(2).find('input').val(),
            TAMIVLFXAEQCCALCTR: $td.eq(2).find('input').val(),
            DESCTRMIXPMC: $td.eq(3).find('input').val(),
            TIPEDENIVPRMPMC: $td.eq(4).find('input').val()
        }
    }).get();

    var CodFormaCalculo = $('#dropFormaCalculoFaixaFinitaCombinada').val();
    var QDEPTOFXACTRPMC = $('#TxtQtdPremioAtingidoFaixaFinitaCombinada').val();
    var codPmc = $('#hdfCodPmcFaixaFinitaCombinada').val();
    var situacao = $('#hdfSituacaoPromocao').val();

    var data = {
        ParametrosCalculo: tblGridRegistros,
        CodFormaCalculo: CodFormaCalculo,
        QDEPTOFXACTRPMC: QDEPTOFXACTRPMC,
        QDEPTOFXAEQCCALCTR: QDEPTOFXACTRPMC,
        CODPMC: codPmc,
        TIPSITPMC: situacao,
        Combinada: true
    };

    LoadingOn();
    //data.CODPMC
    postAjax("CriterioApuracao", "Alterar", data, function (data) {
        if (data !== undefined && data !== null) {
            if (data === 'Salvar')
                location.reload();
            else
                ShowModal({ mensagem: data, titulo: "Atenção", width: 750 });
        }
        LoadingOff();
    }, function () { LoadingOff(); }, false);
    //LoadingOff();
}

function verificaFaixas() {

    var validacaoTable = $('#tblGridFaixaFinitaFaixa tr:has(td)').map(function (i, v) {
        var $td = $('td', this);
        var min = parseFloat($td.eq(1).find('input').val().replace(',00', '').replaceAll('.', '').replaceAll(',', '.'))
        var max = $td.eq(3).find('input').val() === 'Acima' ? 9999999999 : parseFloat($td.eq(3).find('input').val().replace(',00', '').replaceAll('.', '').replaceAll(',', '.'))
        var isFulfilled = min === null || min === 0 || isNaN(min) || max === null || max === 0 || isNaN(max) ? false : true;
        var isAscending = isFulfilled === true && max > min ? true : false;

        return {
            min: min,
            max: max,
            isFulfilled: isFulfilled,
            isAscending: isAscending,
            td: $td
        }
    }).get();

    $('.classError').removeClass('classError');

    if (validacaoTable.filter(x => x.isFulfilled === false).length > 0) {
        validacaoTable.filter(x => x.isFulfilled === false).forEach(function (result, index) {
            var td = result.td;

            if (result.min === null || result.min === 0 || isNaN(result.min)) {
                td.eq(1).find('input').addClass('classError');
            } else {
                td.eq(3).find('input').addClass('classError');
            }
        });
        ShowModal({ mensagem: "É necessário preencher todos os campos.", titulo: "Atenção", width: 750 })
        return;
    }

    if (validacaoTable.filter(x => x.isAscending === false).length > 0) {
        validacaoTable.filter(x => x.isAscending === false).forEach(function (result, index) {
            var td = result.td;

            td.eq(1).find('input').addClass('classError');
            td.eq(3).find('input').addClass('classError');

        });
        ShowModal({ mensagem: "O valor do campo Início deve ser menor que o do campo Fim", titulo: "Atenção", width: 750 })
        return;
    }

    if (validacaoTable.length > 0) {

        var prevRow;
        var valueExpected = null;
        validacaoTable.forEach(function (result, index) {
            if (index > 0 && valueExpected === null) {
                var td = result.td;

                if (parseFloat((prevRow.max + 0.01).toFixed(2)) !== result.min) {
                    td.eq(1).find('input').addClass('classError');
                    valueExpected = (prevRow.max + 0.01).toFixed(2);
                }
            }

            prevRow = result;
        });

        if (valueExpected !== null) {
            ShowModal({ mensagem: "O valor do campo destacado deve ser: " + $.mask.string(valueExpected, $.mask.masks.currency), titulo: "Atenção", width: 750 })
            return;
        }
    }

    if (!$('#TxtQtdPremioAtingidoFaixaFinitaFaixa').val().replace(/\s/g, '').length) {
        $('#TxtQtdPremioAtingidoFaixaFinitaFaixa').addClass('classError')
        ShowModal({ mensagem: "É necessário preencher todos os campos.", titulo: "Atenção", width: 750 })
        return;
    } else if (isNaN(parseInt($('#TxtQtdPremioAtingidoFaixaFinitaFaixa').val())) || parseInt($('#TxtQtdPremioAtingidoFaixaFinitaFaixa').val()) === 0) {
        $('#TxtQtdPremioAtingidoFaixaFinitaFaixa').addClass('classError')
        ShowModal({ mensagem: "O campo Qde de Prêmio não pode ser zero.", titulo: "Atenção", width: 750 })
        return;
    }

    //Sucesso
    jsListaGridCriterioApuracaoFaixaFinitaFaixa(true);
}

function verificaCriterio(callback) {

    $('.classError').removeClass('classError');

    var error = 0;
    $('#widgetCriterio input.numberInput:visible').each(function (i, v) {
        var el = $(this);
        if (el.val() === '') {
            el.addClass('classError');
            error++;
        } else if (isNaN(parseInt(el.val())) || parseInt(el.val()) === 0) {
            el.addClass('classError');
            error++;
        }
    });

    $('#widgetCriterio input.desc:visible').each(function (i, v) {
        var el = $(this);
        if (!el.val().replace(/\s/g, '').length) {
            el.addClass('classError');
            error++;
        }
    });

    if (error !== 0) {
        ShowModal({ mensagem: "É necessário preencher todos os campos.", titulo: "Atenção", width: 750 })
        return;
    }

    //Sucesso
    return callback(true);
}

function adicionarBrinde(fracionado) {
    if (fracionado === undefined) {

        //Valida campos

        if ($('#valorIncluirBrinde').length > 0) {
            if ($('#valorIncluirBrinde').val() === '' || isNaN($('#valorIncluirBrinde').val().replaceAll('.', '').replace(',', '.')) || parseFloat($('#valorIncluirBrinde').val().replaceAll('.', '').replace(',', '.')) < 0) {
                ShowModal({ mensagem: "É necessário preencher o campo Valor antes de inserir o brinde.", titulo: "Atenção" })
                return;
            }
        }

        if ($('#percIncluirBrinde').length > 0) {
            if ($('#percIncluirBrinde').val() === '' || isNaN($('#percIncluirBrinde').val().replace(',', '.')) || parseFloat($('#percIncluirBrinde').val().replace(',', '.')) < 0 || parseFloat($('#percIncluirBrinde').val().replace(',', '.')) > 100) {
                ShowModal({ mensagem: "É necessário preencher o campo % Benefício antes de inserir o brinde.", titulo: "Atenção" })
                return;
            }
        }

        var filial = $('#idFilialPromocao').val();
        var mercadoria = $('#idItemCodMer').val();

        //VerificaProduto
        $.ajax({
            url: SiteUrl + "PromocaoBrinde/VerificaProdutoVendidoFracionado/" + mercadoria + "/" + filial,
            type: 'GET',
            success: function (data) {
                if (data === "True" || data === "False") {
                    if (data === "False") {
                        adicionarBrinde(false);
                    } else {
                        adicionarBrinde(true);
                    }

                } else {
                    atualizaMensagem();
                }
                LoadingOff();
            },
            error: function () {
                atualizaMensagem('msgBuscaMercadorias');
                LoadingOff();
            }
        });



    } else if (fracionado == true) {
        //Exibir confirmação produto fracionado
        ShowModalConfirmar({ titulo: 'Confirmação', mensagem: "A mercadoria que está sendo inserida é fracionada e pode ser fornecida pelo CAD há regiões atendidas por um CDR. Caso isto aconteça as regioes atendidas pelo CDR não enxergarão a promoção pelo CAD que abastece o CDR. Deseja continuar?", callback: adicionarBrinde, parametros: false });

    } else if (fracionado == false) {
        //Salvar
        $('#frmCreateBrinde').submit();
    }

}

function resizeModalPromocao(modal) {
    var screenHeight = window.innerHeight;

    modal.height(screenHeight - 10);
    modal.find('.modal-body').height(screenHeight - 140);
}

var mercadoria = 0;
var promocao = 0;

function deleteBrinde(mer, pmc, codFuncionarioAutorPromocao, codFuncionarioLogado) {
    mercadoria = mer;
    promocao = pmc;

    let mensagem = ''
    if (codFuncionarioAutorPromocao !== codFuncionarioLogado) {
        mensagem = "Você não é o autor dessa Promoção. <br/> Deseja realmente excluir esse brinde?"
    }
    else {
        mensagem = "Deseja realmente excluir esse brinde?";
    }

    ShowModalConfirmar({ titulo: 'Confirmação', mensagem: mensagem, callback: deleteBrindeAjax });
}

function deleteBrindeAjax() {
    $.ajax({
        url: SiteUrl + "PromocaoBrinde/Delete",
        type: 'POST',
        data: { CODPRMPMC: mercadoria, CODPMC: promocao },
        success: function (data) {
            //divListBrindeResultadosView
            $('#divListBrindeResultadosView').html(data);
            atualizaMensagem('msgGeral');
        },
        error: function () {
            atualizaMensagem('msgGeral');
            LoadingOff();
        }
    });
}



function removerBrindePromocaoLiberada(mer, pmc, saldo, codFuncionarioAutorPromocao, codFuncionarioLogado) {
    mercadoria = mer;
    promocao = pmc;

    //var mensagem = "Deseja realmente remover este brinde da promoção?";

    let mensagem = ''
    if (codFuncionarioAutorPromocao !== codFuncionarioLogado) {
        mensagem = "Você não é o autor dessa Promoção. <br/> Deseja realmente remover este brinde da promoção?"
    }
    else
    {
        mensagem = "Deseja realmente remover este brinde da promoção?";
    }
    if (saldo > 0) {
        mensagem += "<br/><b>Este brinde tem um saldo de: " + saldo + "</b>";
    }

    ShowModalConfirmar({ titulo: 'Confirmação', mensagem: mensagem, callback: removerBrindePromocaoLiberadaAjax });
}

function removerBrindePromocaoLiberadaAjax() {
    $.ajax({
        url: SiteUrl + "PromocaoBrinde/RemoverBrindePromocaoLiberada",
        type: 'POST',
        data: { CODPRMPMC: mercadoria, CODPMC: promocao },
        success: function (data) {
            $('#divListBrindeResultadosView').html(data);
            atualizaMensagem('msgGeral');
        },
        error: function () {
            atualizaMensagem('msgGeral');
            LoadingOff();
        }
    });
}


function retornaBrindePromocaoLiberada(mer, pmc, saldo, codFuncionarioAutorPromocao, codFuncionarioLogado) {
    mercadoria = mer;
    promocao = pmc;

    let mensagem = ''
    if (codFuncionarioAutorPromocao !== codFuncionarioLogado) {
        mensagem = "Você não é o autor dessa Promoção. <br/> Deseja realmente recolocar este brinde da promoção?"
    }
    else {
        mensagem = "Deseja realmente recolocar este brinde na promoção?";
    }
    if (saldo > 0) {
        mensagem += "<br/><b>Este brinde tem um saldo de: " + saldo + "</b>";
    }

    ShowModalConfirmar({ titulo: 'Confirmação', mensagem: mensagem, callback: retornaBrindePromocaoLiberadaAjax });
}

function retornaBrindePromocaoLiberadaAjax() {
    $.ajax({
        url: SiteUrl + "PromocaoBrinde/RetornaBrindePromocaoLiberada",
        type: 'POST',
        data: { CODPRMPMC: mercadoria, CODPMC: promocao },
        success: function (data) {
            $('#divListBrindeResultadosView').html(data);
            atualizaMensagem('msgGeral');
        },
        error: function () {
            atualizaMensagem('msgGeral');
            LoadingOff();
        }
    });
}

function historicoBrindePromocaoLiberada(CODPRMPMC, CODPMC, TIPEDENIVPRMPMC) {
    $.ajax({
        url: SiteUrl + "PromocaoBrinde/Historico",
        type: 'POST',
        data: { CODPRMPMC: CODPRMPMC, CODPMC: CODPMC, TIPEDENIVPRMPMC: TIPEDENIVPRMPMC},
        success: function (data) {
            $('#divModalHistoricoAbastecimento').html(data);
            $('#divModalHistoricoAbastecimento > .modal').modal();
            LoadingOff();
            atualizaMensagem('msgGeral');

        },
        error: function () {
            atualizaMensagem('msgGeral');
            LoadingOff();
        }
    });
}

function btnFecharHistoricoBrindePromocaoLiberada() {
    $('#divModalHistoricoAbastecimento > .modal').modal('hide');
}


function abrirModalPromocaoEstadoUniao() {
    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    abrirModal("PromocaoPublicoAlvoEstado", "EditPromocaoPublicoAlvoEstadoUniao", obj, "#modalPromocao", function () { resizeModalPromocao($('#modalListPromocaoEstadoUniao')); });
}

function fecharModalPromocaoEstadoUni() {
    $('#modalListPromocaoEstadoUniao').parent().modal('hide');
}

function marcarOuDesmarcarTodosEstados() {

    if ($("#chkTodosEstados").prop("checked"))
        $(".marcaEstado").prop("checked", true);
    else $(".marcaEstado").prop("checked", false);

}

//Salvar estados selecionados
function salvarDadosGridEstadoUniao() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var lstEstadoUniaoChecked = [];

    var lstEstadoUniao = $("#tblModalPromocaoEstadoUniao tbody input[name='SelecioneEstadoUniao'][type='checkbox']");


    lstEstadoUniao.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODESTUNI: check.parent().parent().find("label").eq(0).text(),
            }
            lstEstadoUniaoChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoEstado" + "/" + "Save/" + CODPMC + '/' + TIPEDEPUBALVPMC,
        type: 'POST',
        data: { obj: lstEstadoUniaoChecked },
        success: function (data) {
            var CODPMC = $('#IDCodPmc').val();
            var TIPEDEPUBALVPMC = 4;
            $('#modalListPromocaoEstadoUniao').parent().modal('hide');
            widgetPromocaoPublicoAlvo(CODPMC, TIPEDEPUBALVPMC);
            mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalPublicoAlvo");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgmodalListEstadoUniao");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function abrirModalPromocaoAtividade() {
    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    abrirModal("PromocaoPublicoAlvoAtividade", "EditMecanicaPromocaoPublicoAlvoAtividade", obj, "#modalPromocao", function () { resizeModalPromocao($('#modalListPromocaoAtividade')); });
}

function fecharModalPromocaoAtividade() {
    $('#modalListPromocaoAtividade').parent().modal('hide');
}

function marcarOuDesmarcarTodasAtividades() {

    if ($("#chkTodasAtividades").prop("checked"))
        $(".marcaAtividade").prop("checked", true);
    else $(".marcaAtividade").prop("checked", false);

}

//Salvar Atividades selecionados
function salvarDadosGridPromocaoAtividade() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var lstAtividadeChecked = [];

    var lstAtividade = $("#tblModalPromocaoAtividade tbody input[name='SelecioneAtividade'][type='checkbox']");


    lstAtividade.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODATI: check.parent().parent().find("label").eq(0).text()
            }
            lstAtividadeChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoAtividade" + "/" + "Save/" + CODPMC + '/' + TIPEDEPUBALVPMC,
        type: 'POST',
        data: { obj: lstAtividadeChecked },
        success: function (data) {
            var CODPMC = $('#IDCodPmc').val();
            var TIPEDEPUBALVPMC = 4;
            $('#modalListPromocaoAtividade').parent().modal('hide');
            widgetPromocaoPublicoAlvo(CODPMC, TIPEDEPUBALVPMC);
            mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalPublicoAlvo");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgmodalListPromocaoAtividade");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Promocao Grupamento Cliente

function abrirModalPromocaoGrupamentoCliente() {
    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    abrirModal("PromocaoPublicoAlvoGrupamentoCliente", "EditMecanicaPromocaoPublicoAlvoGrupamentoCliente", obj, "#modalPromocao", function () { resizeModalPromocao($('#modalListPromocaoGrupamentoCliente')); });
}

function fecharModalPromocaoGrupamentoCliente() {
    $('#modalListPromocaoGrupamentoCliente').parent().modal('hide');
}

function marcarOuDesmarcarTodasGrupamentoCliente() {

    if ($("#chkTodosGrupamentoCliente").prop("checked")) {
        $(".marcaGrupamentoCliente").prop("checked", false);
        $("#chkTodosGrupamentoCliente").prop("checked", false);
    }
    else {
        $(".marcaGrupamentoCliente").prop("checked", true);
        $("#chkTodosGrupamentoCliente").prop("checked", true);
    }

}

//Salvar Grupamento Cliente selecionados
function salvarDadosGridPromocaoGrupamentoCliente() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var lstGrupamentoClienteChecked = [];

    var lstGrupamentoCliente = $("#tblModalPromocaoGrupamentoCliente tbody input[name='SelecioneGrupamentoCliente'][type='checkbox']");


    lstGrupamentoCliente.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODGRPCLI: check.parent().parent().find("label").eq(0).text()
            }
            lstGrupamentoClienteChecked.push(obj);
        }
    }).get();


    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoGrupamentoCliente" + "/" + "Save/" + CODPMC + '/' + TIPEDEPUBALVPMC,
        type: 'POST',
        data: { obj: lstGrupamentoClienteChecked },
        success: function (data) {
            var CODPMC = $('#IDCodPmc').val();
            var TIPEDEPUBALVPMC = 4;
            $('#modalListPromocaoGrupamentoCliente').parent().modal('hide');
            widgetPromocaoPublicoAlvo(CODPMC, TIPEDEPUBALVPMC);
            mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalPublicoAlvo");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgmodalListPromocaoGrupamentoCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Promocao Negocio Cliente

function abrirModalPromocaoNegocioCliente() {
    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    abrirModal("PromocaoPublicoAlvoNegocioCliente", "EditMecanicaPromocaoPublicoAlvoNegocioCliente", obj, "#modalPromocao", function () { resizeModalPromocao($('#modalListPromocaoNegocioCliente')); });
}

function fecharModalPromocaoNegocioCliente() {
    $('#modalListPromocaoNegocioCliente').parent().modal('hide');
}

function marcarOuDesmarcarTodasNegocioCliente() {

    if ($("#chkTodosNegocioCliente").prop("checked"))
        $(".marcaNegocioCliente").prop("checked", true);
    else $(".marcaNegocioCliente").prop("checked", false);

}

//Salvar Negocio Cliente selecionados
function salvarDadosGridPromocaoNegocioCliente() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var lstNegocioClienteChecked = [];

    var lstNegocioCliente = $("#tblModalPromocaoNegocioCliente tbody input[name='SelecioneNegocioCliente'][type='checkbox']");


    lstNegocioCliente.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODUNDESRNGC: check.parent().parent().find("label").eq(0).text()
            }
            lstNegocioClienteChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoNegocioCliente" + "/" + "Save/" + CODPMC + '/' + TIPEDEPUBALVPMC,
        type: 'POST',
        data: { obj: lstNegocioClienteChecked },
        success: function (data) {
            var CODPMC = $('#IDCodPmc').val();
            var TIPEDEPUBALVPMC = 4;
            $('#modalListPromocaoNegocioCliente').parent().modal('hide');
            widgetPromocaoPublicoAlvo(CODPMC, TIPEDEPUBALVPMC);
            mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalPublicoAlvo");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgModalPromocaoNegocioCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Promocao Segmento Cliente

function abrirModalPromocaoSegmentoCliente() {
    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    abrirModal("PromocaoPublicoAlvoSegmentoCliente", "EditMecanicaPromocaoPublicoAlvoSegmentoCliente", obj, "#modalPromocao", function () { resizeModalPromocao($('#modalPromocaoListSegmentoCliente')); });
}

function fecharModalPromocaoSegmentoCliente() {
    $('#modalPromocaoListSegmentoCliente').parent().modal('hide');
}

function marcarOuDesmarcarTodasSeguimentoCliente() {

    if ($("#chkTodosSegmentoCliente").prop("checked"))
        $(".marcaSegmentoCliente").prop("checked", true);
    else $(".marcaSegmentoCliente").prop("checked", false);

}

//Salvar Segmento Cliente selecionados
function salvarDadosGridPromocaoSegmentoCliente() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var lstSegmentoClienteChecked = [];

    var lstSegmentoCliente = $("#tblModalPromocaoSegmentoCliente tbody input[name='SelecioneSegmentoCliente'][type='checkbox']");


    lstSegmentoCliente.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODSGMNGCCLI: check.parent().parent().find("label").eq(0).text()
            }
            lstSegmentoClienteChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoSegmentoCliente" + "/" + "Save/" + CODPMC + '/' + TIPEDEPUBALVPMC,
        type: 'POST',
        data: { obj: lstSegmentoClienteChecked },
        success: function (data) {
            var CODPMC = $('#IDCodPmc').val();
            var TIPEDEPUBALVPMC = 4;
            $('#modalPromocaoListSegmentoCliente').parent().modal('hide');
            widgetPromocaoPublicoAlvo(CODPMC, TIPEDEPUBALVPMC);
            mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalPublicoAlvo");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgModalPromocaoSegmentoCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function ModalClientesLoadingOff_UpdateMessage() {
    $('#loadingDialog').modal('hide');
    atualizaMensagem("msgModalPromocaoCliente");
    ChosenConfig();
    ChosenResize();
    MaskConfig();
    PopoverConfig();
    WidgetConfig();
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Promocao Gerente Vendas

function abrirModalPromocaoGerenteVendas() {
    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    abrirModal("PromocaoPublicoAlvoGerenteVendas", "EditMecanicaPromocaoPublicoAlvoGerenteVendas", obj, "#modalPromocao", function () { resizeModalPromocao($('#modalPromocaoListGerenteVendas')); });
}

function fecharModalPromocaoGerenteVendas() {
    $('#modalPromocaoListGerenteVendas').parent().modal('hide');
}

function marcarOuDesmarcarTodasGerenteVendas() {

    if ($("#chkTodosGerenteVendas").prop("checked"))
        $(".marcaGerenteVendas").prop("checked", true);
    else $(".marcaGerenteVendas").prop("checked", false);

}

//Salvar Gerente Vendas selecionados
function salvarDadosGridPromocaoGerenteVendas() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var lstGerenteVendasChecked = [];

    var lstGerenteVendas = $("#tblModalPromocaoGerenteVendas tbody input[name='SelecioneGerenteVendas'][type='checkbox']");


    lstGerenteVendas.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODGER: check.parent().parent().find("label").eq(0).text()
            }
            lstGerenteVendasChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoGerenteVendas" + "/" + "Save/" + CODPMC + '/' + TIPEDEPUBALVPMC,
        type: 'POST',
        data: { obj: lstGerenteVendasChecked },
        success: function (data) {
            var CODPMC = $('#IDCodPmc').val();
            var TIPEDEPUBALVPMC = 4;
            $('#modalPromocaoListGerenteVendas').parent().modal('hide');
            widgetPromocaoPublicoAlvo(CODPMC, TIPEDEPUBALVPMC);
            mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalPublicoAlvo");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgModalPromocaoGerenteVendas");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Promocao Clientes Excludentes
function abrirModalPromocaoClientesExcludentes() {
    var CODPMC = parseFloat($('#IDCodPmc').val());

    var obj = {
        CODPMC: CODPMC
    }

    abrirModal("PromocaoClientesExcludentes", "EditMecanicaPromocaoClientesExcludentes", obj, "#modalPromocao",
        function () {
            
            getAjax("PromocaoClientesExcludentes", "EditMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes/" + CODPMC, function (data) {
                $('#divLoadPesquisaPromocaoClientesExcludentes').html(data);
                LoadingOff();
            })
            abrirWidgetPromocaoClientesExcludentes();
            resizeModalPromocao($('#modalListPromocaoClientesExcludentes'));
        });

}

function abrirWidgetPromocaoClientesExcludentes() {
    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    postAjax("PromocaoClientesExcludentes", "ListarClientesExcludentes", obj, function (data) {
        $('#divLoadPromocaoClientesExcludentes').html(data);
        LoadingOff();
    });
}

function salvarPromocaoClientesExcludentes(e) {
    var CODPMC = $('#IDCodPmc').val();
    var CODCLI = e.parentElement.parentElement.children.item(1).innerText;
    var NOMCLI = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODPMC: CODPMC,
        CODCLI: CODCLI,
        NOMCLI: NOMCLI
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoClientesExcludentes" + "/" + "Save",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoClientesExcludentes();
            atualizaMensagem("msgModalPromocaoClientesExcludentes");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}

function addPesquisaPromocaoClientesExcludentes(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoClientesExcludentes" + "/" + "AddPesquisa",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPromocaoClientesExcludentes();
            $('#frmPesquisaPromocaoClientesExcludentes').submit();
            atualizaMensagem("msgModalPromocaoClientesExcludentes");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeletePromocaoClientesExcludentes(e) {
    var CODPMC = $('#IDCodPmc').val();
    var CODCLI = e.parentElement.parentElement.children.item(1).innerText;
    var NOMCLI = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODPMC: CODPMC,
        CODCLI: CODCLI,
        NOMCLI: NOMCLI
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoClientesExcludentes" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoClientesExcludentes();
            mostraMensagem("", "", "msgModalPromocaoClientesExcludentes");
            //e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalPromocaoClientesExcludentes");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllClientesExcludentes() {
    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoClientesExcludentes" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoClientesExcludentes();
            mostraMensagem("", "", "msgModalPromocaoClientesExcludentes");
            //e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalPromocaoClientesExcludentes");
        },
        complete: function () {
            LoadingOff();
        }
    });
}


function fecharModalClientesExcludentes() {
    $('#modalListPromocaoClientesExcludentes').parent().modal('hide');
    var CODPMC = $('#IDCodPmc').val();
    widgetPromocaoClientesExcludentes(CODPMC);
    LoadingOff();
}

function ModalClientesEsxludentesLoadingOff_UpdateMessage() {
    $('#loadingDialog').modal('hide');
    atualizaMensagem("msgModalPromocaoClientesExcludentes");
    ChosenConfig();
    ChosenResize();
    MaskConfig();
    PopoverConfig();
    WidgetConfig();
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Promocao Cliente

function abrirModalPromocaoCliente() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC
    }

    abrirModal("PromocaoPublicoAlvoCliente", "EditMecanicaPromocaoPublicoAlvoCliente", obj, "#modalPromocao",
        function () {
            getAjax("PromocaoPublicoAlvoCliente", "EditMecanicaPromocaoWidgetLoadPesquisaCliente/" + CODPMC + '/' + TIPEDEPUBALVPMC, function (data) {
                $('#divLoadPesquisaPromocaoCliente').html(data);
                LoadingOff();
            })
            abrirWidgetPromocaoPublicoAlvoCliente();
            resizeModalPromocao($('#modalListPromocaoCliente'));
        });

}

function abrirWidgetPromocaoPublicoAlvoCliente() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC
    }

    postAjax("PromocaoPublicoAlvoCliente", "ListarPublicoAlvoCliente", obj, function (data) {
        $('#divLoadPromocaoPublicoAlvoCliente').html(data);
        LoadingOff();
    });
}

function salvarPromocaoPublicoAlvoCliente(e) {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var CODCLI = e.parentElement.parentElement.children.item(1).innerText;
    var NOMCLI = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC,
        CODCLI: CODCLI,
        NOMCLI: NOMCLI
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoCliente" + "/" + "Save",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoCliente();
            atualizaMensagem("msgModalPromocaoCliente");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}

function addPesquisaPromocaoPublicoAlvoCliente(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoCliente" + "/" + "AddPesquisa",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoCliente();
            $('#frmPesquisaPromocaoPublicoAlvoCliente').submit();
            atualizaMensagem("msgModalPromocaoCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeletePromocaoPublicoAlvoCliente(e) {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var CODCLI = e.parentElement.parentElement.children.item(1).innerText;
    var NOMCLI = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC,
        CODCLI: CODCLI,
        NOMCLI: NOMCLI
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoCliente" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoCliente();
            mostraMensagem("", "", "msgModalPromocaoCliente");
            //e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalPromocaoCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllPromocaoPublicoAlvoCliente() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoCliente" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoCliente();
            mostraMensagem("", "", "msgModalPromocaoCliente");
            //e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalPromocaoCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function fecharModalCliente() {
    $('#modalListPromocaoCliente').parent().modal('hide');
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    widgetPromocaoPublicoAlvo(CODPMC, TIPEDEPUBALVPMC);
    LoadingOff();
}


// Esconde o modal de carregando e atualiza a div de mensagens
function ModalPromocaoClienteLoadingOff_UpdateMessage() {
    $('#loadingDialog').modal('hide');
    atualizaMensagem("msgModalPromocaoCliente");
    ChosenConfig();
    ChosenResize();
    MaskConfig();
    PopoverConfig();
    WidgetConfig();
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Promocao Rca Vendedor

function abrirModalPromocaoRcaVendedor() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC
    }

    abrirModal("PromocaoPublicoAlvoRcaVendedor", "EditMecanicaPromocaoPublicoAlvoRcaVendedor", obj, "#modalPromocao",
        function () {
            getAjax("PromocaoPublicoAlvoRcaVendedor", "EditMecanicaPromocaoWidgetLoadPesquisaRcaVendedor/" + CODPMC + '/' + TIPEDEPUBALVPMC, function (data) {
                $('#divLoadPesquisaPromocaoRcaVendedor').html(data);
                LoadingOff();
            })
            abrirWidgetPromocaoPublicoAlvoRcaVendedor();


            resizeModalPromocao($('#modalListPromocaoRcaVendedor'));
        });
}

function abrirWidgetPromocaoPublicoAlvoRcaVendedor() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC
    }

    postAjax("PromocaoPublicoAlvoRcaVendedor", "ListarPublicoAlvoRcaVendedor", obj, function (data) {
        $('#divLoadPromocaoPublicoAlvoRcaVendedor').html(data);
        LoadingOff();
    });
}

function salvarPromocaoPublicoAlvoRcaVendedor(e) {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var CODREP = e.parentElement.parentElement.children.item(1).innerText;
    var NOMREP = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC,
        CODREP: CODREP,
        NOMREP: NOMREP
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoRcaVendedor" + "/" + "Save",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoRcaVendedor();
            atualizaMensagem("msgModalPromocaoRcaVendedor");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}

function addPesquisaPromocaoPublicoAlvoRcaVendedor(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoRcaVendedor" + "/" + "AddPesquisa",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoRcaVendedor();
            $('#frmPesquisaPromocaoPublicoAlvoRcaVendedor').submit();
            atualizaMensagem("msgModalPromocaoRcaVendedor");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeletePromocaoPublicoAlvoRcaVendedor(e) {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var CODREP = e.parentElement.parentElement.children.item(1).innerText;
    var NOMREP = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC,
        CODREP: CODREP,
        NOMREP: NOMREP
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoRcaVendedor" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoRcaVendedor();
            mostraMensagem("", "", "msgModalPromocaoRcaVendedor");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalPromocaoRcaVendedor");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllPromocaoPublicoAlvoRcaVendedor() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoRcaVendedor" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoRcaVendedor();
            mostraMensagem("", "", "msgModalPromocaoRcaVendedor");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalPromocaoRcaVendedor");
        },
        complete: function () {
            LoadingOff();
        }
    });
}


function fecharModalRcaVendedor() {
    $('#modalListPromocaoRcaVendedor').parent().modal('hide');
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    widgetPromocaoPublicoAlvo(CODPMC, TIPEDEPUBALVPMC);
    LoadingOff();
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Promocao Gerente Mercado

function abrirModalPromocaoGerenteMercado() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC
    }

    abrirModal("PromocaoPublicoAlvoGerenteMercado", "EditMecanicaPromocaoPublicoAlvoGerenteMercado", obj, "#modalPromocao",
        function () {
            getAjax("PromocaoPublicoAlvoGerenteMercado", "EditMecanicaPromocaoWidgetLoadPesquisaGerenteMercado/" + CODPMC + '/' + TIPEDEPUBALVPMC, function (data) {
                $('#divLoadPesquisaPromocaoGerenteMercado').html(data);
                LoadingOff();
            })
            abrirWidgetPromocaoPublicoAlvoGerenteMercado();

            resizeModalPromocao($('#modalListPromocaoGerenteMercado'));
        });

}

function abrirWidgetPromocaoPublicoAlvoGerenteMercado() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC
    }

    postAjax("PromocaoPublicoAlvoGerenteMercado", "ListarPublicoAlvoGerenteMercado", obj, function (data) {
        $('#divLoadPromocaoPublicoAlvoGerenteMercado').html(data);
        LoadingOff();
    });
}

function salvarPromocaoPublicoAlvoGerenteMercado(e) {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var CODSUP = e.parentElement.parentElement.children.item(1).innerText;
    var NOMSUP = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC,
        CODSUP: CODSUP,
        NOMSUP: NOMSUP
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoGerenteMercado" + "/" + "Save",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoGerenteMercado();
            atualizaMensagem("msgModalPromocaoGerenteMercado");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}

function addPesquisaPromocaoPublicoAlvoGerenteMercado(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoGerenteMercado" + "/" + "AddPesquisa",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoGerenteMercado();
            $('#frmPesquisaPromocaoPublicoAlvoGerenteMercado').submit();
            atualizaMensagem("msgModalPromocaoGerenteMercado");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeletePromocaoPublicoAlvoGerenteMercado(e) {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var CODSUP = e.parentElement.parentElement.children.item(1).innerText;
    var NOMSUP = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC,
        CODSUP: CODSUP,
        NOMSUP: NOMSUP
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoGerenteMercado" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoGerenteMercado();
            mostraMensagem("", "", "msgModalPromocaoGerenteMercado");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalPromocaoGerenteMercado");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllPromocaoPublicoAlvoGerenteMercado() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoPublicoAlvoGerenteMercado" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoPublicoAlvoGerenteMercado();
            mostraMensagem("", "", "msgModalPromocaoGerenteMercado");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalPromocaoGerenteMercado");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function fecharModalGerenteMercado() {
    $('#modalListPromocaoGerenteMercado').parent().modal('hide');
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    widgetPromocaoPublicoAlvo(CODPMC, TIPEDEPUBALVPMC);
    LoadingOff();
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Publico Alvo - Carregar Widget

function widgetPromocaoPublicoAlvo(CODPMC, TIPEDEPUBALVPMC) {
    CODPMC = CODPMC;
    TIPEDEPUBALVPMC = TIPEDEPUBALVPMC;

    getAjax("Promocao", "CarregarWidgetPublicoAlvo/" + CODPMC + '/' + TIPEDEPUBALVPMC, function (data) {
        $('#divWidgetPromocaoPublicoAlvo').html(data);
    })
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Clientes Excludentes - Carregar Widget

function widgetPromocaoClientesExcludentes(CODPMC) {
    CODPMC = CODPMC;

    getAjax("Promocao", "CarregarWidgetClientesExcludentes/" + CODPMC , function (data) {
        $('#divWidgetPromocaoClientesExcludentes').html(data);
    })
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Mix - Carregar Widget

function widgetPromocaoMix(CODPMC, TIPEDEPUBALVPMC) {
    CODPMC = CODPMC;
    TIPEDEPUBALVPMC = TIPEDEPUBALVPMC;

    INDPTCTOTMIXPMC = $('#formEditarPromocao [name="INDPTCTOTMIXPMC"][checked="checked"]').val();

    getAjax("PromocaoMix", "CarregarWidgetPromocaoMix/" + CODPMC + '/' + TIPEDEPUBALVPMC + '/' + INDPTCTOTMIXPMC, function (data) {
        $('#divWidgetPromocaoMix').html(data);
    })
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Promocao Mix Grupo Mercadoria

function abrirModalMixGrupoMercadoria(id) {
    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    abrirModal("PromocaoMix", "EditPromocaoMixGrupoMercadoria", obj, "#modalPromocao", function () { resizeModalPromocao($('#modalListPromocaoMixGrupoMercadoria')); });

}

function marcarOuDesmarcarTodosGruposMercadorias() {
    if ($("#chkTodosGruposMercadorias").prop("checked"))
        $(".marcaGrupoMercadoria:not(:disabled)").prop("checked", true);
    else
        $(".marcaGrupoMercadoria:not(:disabled)").prop("checked", false);
}

function fecharModalMixGrupoMercadoria() {
    $('#modalListPromocaoMixGrupoMercadoria').parent().modal('hide');
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    widgetPromocaoMix(CODPMC, TIPEDEPUBALVPMC);
    LoadingOff();
}

function salvarDadosPromocaoMixGrupoMercadoria() {
    var CODPMC = $('#IDCodPmc').val();
    var lstGrupoMercadoriaChecked = [];

    var lstGrupoMercadoria = $("#tblModalPromocaoMixGrupoMercadoria tbody input[name='SelecioneGrupoMercadoria'][type='checkbox']");


    lstGrupoMercadoria.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODGRPMER: check.parent().parent().find('input[type="hidden"][name$="CODGRPMER"]').val(),
                NUMSEQMIXACOCMC: check.parent().parent().find('input[type="hidden"][name$="NUMSEQMIXACOCMC"]').val()
            }
            lstGrupoMercadoriaChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "SaveMixGrupoMercadoria/" + CODPMC,
        type: 'POST',
        data: { obj: lstGrupoMercadoriaChecked },
        success: function (data) {
            if (data === "True") {
                var CODPMC = $('#IDCodPmc').val();
                var TIPEDEPUBALVPMC = 4;
                $('#modalListPromocaoMixGrupoMercadoria').parent().modal('hide');
                widgetPromocaoMix(CODPMC, TIPEDEPUBALVPMC);
                atualizaMensagem("msgModalMix");
            } else {
                atualizaMensagem("msgmodalListPromocaoMixGrupoMercadoria");
            }
        },
        error: function (error) {
            atualizaMensagem("msgmodalListPromocaoMixGrupoMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Promocao Mix Familia Mercadoria

function abrirModalMixFamiliaMercadoria() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC
    }

    postAjax("PromocaoMix", "EditPromocaoMixFamiliaMercadoria", obj,
        function (data) {

            $("#modalPromocao").html(data);
            $("#modalPromocao").modal({ show: true, backdrop: 'static' }).on('shown.bs.modal', function () {
                ChosenResize();
            });

            resizeModalPromocao($('#modalPromocaoMixFamiliaMercadoria'));

            if ($('#divLoadPesquisaPromocaoMixFamiliaMercadoria').length > 0) {
                getAjax("PromocaoMix", "CarregarWidgetLoadPesquisaMixFamiliaMercadoria/" + CODPMC + '/' + TIPEDEPUBALVPMC,
                    function (data) {
                        $('#divLoadPesquisaPromocaoMixFamiliaMercadoria').html(data);
                        LoadingOff();
                    });
            }
            abrirWidgetPromocaoMixFamiliaMercadoriaAdicionados();
        });
}

function abrirWidgetPromocaoMixFamiliaMercadoriaAdicionados() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC
    }

    postAjax("PromocaoMix", "CarregarWidgetMixFamiliaMercadoriaAdicionados", obj, function (data) {
        $('#divPromocaoMixFamiliaMercadoriaAdicionados').html(data);
        LoadingOff();
    });
}

function fecharModalPromocaoMixFamiliaMercadoria() {
    $('#modalPromocaoMixFamiliaMercadoria').parent().modal('hide');
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    widgetPromocaoMix(CODPMC, TIPEDEPUBALVPMC);
    LoadingOff();
}


function addPesquisaMixFamiliaMercadoria(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "AddPesquisaMixFamiliaMercadoria",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPromocaoMixFamiliaMercadoriaAdicionados();
            $('#frmPesquisaPromocaoMixFamiliaMercadoria').submit();
            atualizaMensagem("msgModalPromocaoMixFamiliaMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function salvarPromocaoMixFamiliaMercadoria(e) {

    var CODPMC = $('#IDCodPmc').val();
    var CODGRPMER = e.parentElement.parentElement.children.item(0).value;
    var CODFMLMER = e.parentElement.parentElement.children.item(2).innerText;


    var obj = {
        CODPMC: CODPMC,
        CODFMLMER: CODFMLMER,
        CODGRPMER: CODGRPMER
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "SaveMixFamiliaMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixFamiliaMercadoriaAdicionados();
            mostraMensagem("", "", "msgModalPromocaoMixFamiliaMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}


function DeletePromocaoMixFamiliaMercadoria(e) {

    var CODPMC = $('#IDCodPmc').val();
    var CODFMLMER = e.parentElement.parentElement.children.item('3').innerText;
    var CODGRPMER = e.parentElement.parentElement.children.item('1').value;
    var NUMSEQMIXACOCMC = e.parentElement.parentElement.children.item('0').value;

    var obj = {
        CODPMC: CODPMC,
        NUMSEQMIXACOCMC: NUMSEQMIXACOCMC,
        CODFMLMER: CODFMLMER,
        CODGRPMER: CODGRPMER
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "DeleteMixFamiliaMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixFamiliaMercadoriaAdicionados();
            atualizaMensagem("msgModalPromocaoMixFamiliaMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixFamiliaMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllPromocaoMixFamiliaMercadoria() {

    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "DeleteMixFamiliaMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixFamiliaMercadoriaAdicionados();
            atualizaMensagem("msgModalPromocaoMixFamiliaMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixFamiliaMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}


//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Promocao Mix Classe Mercadoria

function abrirModalMixClasseMercadoria() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC
    }

    postAjax("PromocaoMix", "EditPromocaoMixClasseMercadoria", obj,
        function (data) {

            $("#modalPromocao").html(data);
            $("#modalPromocao").modal({ show: true, backdrop: 'static' }).on('shown.bs.modal', function () {
                ChosenResize();
            });

            resizeModalPromocao($('#modalPromocaoMixClasseMercadoria'));

            if ($('#divLoadPesquisaPromocaoMixClasseMercadoria').length > 0) {
                getAjax("PromocaoMix", "CarregarWidgetLoadPesquisaMixClasseMercadoria/" + CODPMC + '/' + TIPEDEPUBALVPMC, function (data) {
                    $('#divLoadPesquisaPromocaoMixClasseMercadoria').html(data);
                    LoadingOff();
                })
            }

            abrirWidgetPromocaoMixClasseMercadoriaAdicionados();
        });

}

function abrirWidgetPromocaoMixClasseMercadoriaAdicionados() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC
    }

    postAjax("PromocaoMix", "CarregarWidgetMixClasseMercadoriaAdicionados", obj, function (data) {
        $('#divPromocaoMixClasseMercadoriaAdicionados').html(data);
        LoadingOff();
    });
}

function fecharModalPromocaoMixClasseMercadoria() {
    $('#modalPromocaoMixClasseMercadoria').parent().modal('hide');
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    widgetPromocaoMix(CODPMC, TIPEDEPUBALVPMC);
    LoadingOff();
}

function addPesquisaMixClasseMercadoria(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "AddPesquisaMixClasseMercadoria",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPromocaoMixClasseMercadoriaAdicionados();
            $('#frmPesquisaPromocaoMixClasseMercadoria').submit();
            atualizaMensagem("msgModalPromocaoMixClasseMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function salvarPromocaoMixClasseMercadoria(e) {

    var CODPMC = $('#IDCodPmc').val();
    var CODFMLMER = e.parentElement.parentElement.children.item(1).value;
    var CODGRPMER = e.parentElement.parentElement.children.item(0).value;
    var CODCLSMER = e.parentElement.parentElement.children.item(3).innerText;


    var obj = {
        CODPMC: CODPMC,
        CODCLSMER: CODCLSMER,
        CODGRPMER: CODGRPMER,
        CODFMLMER: CODFMLMER
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "SaveMixClasseMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixClasseMercadoriaAdicionados();
            atualizaMensagem("msgModalPromocaoMixClasseMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeletePromocaoMixClasseMercadoria(e) {

    var CODPMC = $('#IDCodPmc').val();
    var CODFMLMER = e.parentElement.parentElement.children.item('2').value;
    var CODGRPMER = e.parentElement.parentElement.children.item('1').value;
    var NUMSEQMIXACOCMC = e.parentElement.parentElement.children.item('0').value;
    var CODCLSMER = e.parentElement.parentElement.children.item('4').innerText;

    var obj = {
        CODPMC: CODPMC,
        NUMSEQMIXACOCMC: NUMSEQMIXACOCMC,
        CODFMLMER: CODFMLMER,
        CODGRPMER: CODGRPMER,
        CODCLSMER: CODCLSMER
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "DeleteMixClasseMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixClasseMercadoriaAdicionados();
            atualizaMensagem("msgModalPromocaoMixClasseMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalPromocaoMixClasseMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllPromocaoMixClasseMercadoria() {

    var CODPMC = $('#IDCodPmc').val();
  
    var obj = {
        CODPMC: CODPMC     
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "DeleteMixClasseMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixClasseMercadoriaAdicionados();
            atualizaMensagem("msgModalPromocaoMixClasseMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalPromocaoMixClasseMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Promocao Mix Mercadoria

function abrirModalMixMercadoria() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC
    }

    postAjax("PromocaoMix", "EditPromocaoMixMercadoria", obj,
        function (data) {

            $("#modalPromocao").html(data);
            $("#modalPromocao").modal({ show: true, backdrop: 'static' }).on('shown.bs.modal', function () {
                ChosenResize();
            });

            resizeModalPromocao($('#modalPromocaoMixMercadoria'));

            if ($('#divLoadPesquisaPromocaoMixMercadoria').length > 0) {
                PromocaoCarregarWidgetLoadPesquisaMixMercadoria()
            }

            abrirWidgetPromocaoMixMercadoriaAdicionados();
        });

}

function PromocaoCarregarWidgetLoadPesquisaMixMercadoria() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    getAjax("PromocaoMix", "CarregarWidgetLoadPesquisaMixMercadoria/" + CODPMC + '/' + TIPEDEPUBALVPMC
        , function (data) {
        $('#divLoadPesquisaPromocaoMixMercadoria').html(data);
        LoadingOff();
    });

}

function abrirWidgetPromocaoMixMercadoriaAdicionados() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC
    }

    postAjax("PromocaoMix", "CarregarWidgetMixMercadoriaAdicionados", obj, function (data) {
        $('#divPromocaoMixMercadoriaAdicionados').html(data);
        LoadingOff();
    });
}

function fecharModalPromocaoMixMercadoria() {
    $('#modalPromocaoMixMercadoria').parent().modal('hide');
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    widgetPromocaoMix(CODPMC, TIPEDEPUBALVPMC);
    LoadingOff();
}

function addPesquisaMixMercadoria(data) {
    if (data) {
        data.CODGRPMIXMERPMC = $('#drpAddPromocaoMixFaixa').val()
    }
    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "AddPesquisaMixMercadoria",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPromocaoMixMercadoriaAdicionados();
            $('#frmPesquisaPromocaoMixMercadoria').submit();
            atualizaMensagem("msgModalPromocaoMixMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function salvarPromocaoMixMercadoria(e) {

    var CODPMC = $('#IDCodPmc').val();
    var CODMER = e.parentElement.parentElement.children.item(1).innerText;
    var CODGRPMIXMERPMC = $('#drpAddPromocaoMixFaixa').val();


    var obj = {
        CODPMC: CODPMC,
        CODMER: CODMER,
        CODGRPMIXMERPMC: CODGRPMIXMERPMC
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "SaveMixMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixMercadoriaAdicionados();
            atualizaMensagem("msgModalPromocaoMixMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}


function DeletePromocaoMixMercadoria(e) {

    var CODPMC = $('#IDCodPmc').val();
    var NUMSEQMIXACOCMC = e.parentElement.parentElement.children.item('0').value;
    var CODMER = e.parentElement.parentElement.children.item('2').innerText;

    var obj = {
        CODPMC: CODPMC,
        NUMSEQMIXACOCMC: NUMSEQMIXACOCMC,
        CODMER: CODMER
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "DeleteMixMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixMercadoriaAdicionados();
            atualizaMensagem("msgModalPromocaoMixMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllPromocaoMixMercadoria() {

    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "DeleteMixMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixMercadoriaAdicionados();
            atualizaMensagem("msgModalPromocaoMixMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}


// Esconde o modal de carregando e atualiza a div de mensagens
function MixMercadoriaClienteLoadingOff_UpdateMessage() {
    $('#loadingDialog').modal('hide');
    atualizaMensagem("msgModalPromocaoMixMercadoria");
    ChosenConfig();
    ChosenResize();
    MaskConfig();
    PopoverConfig();
    WidgetConfig();
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Promocao Mix Fornecedor

function abrirModalMixFornecedor() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC
    }

    postAjax("PromocaoMix", "EditPromocaoMixFornecedor", obj,
        function (data) {

            $("#modalPromocao").html(data);
            $("#modalPromocao").modal({ show: true, backdrop: 'static' }).on('shown.bs.modal', function () {
                ChosenResize();
            });

            resizeModalPromocao($('#modalPromocaoMixFornecedor'));

            if ($('#divLoadPesquisaPromocaoMixFornecedor').length > 0) {
                getAjax("PromocaoMix", "CarregarWidgetLoadPesquisaMixFornecedor/" + CODPMC + '/' + TIPEDEPUBALVPMC, function (data) {
                    $('#divLoadPesquisaPromocaoMixFornecedor').html(data);
                    LoadingOff();
                })
            }

            abrirWidgetPromocaoMixFornecedorAdicionados();

        });


}

function abrirWidgetPromocaoMixFornecedorAdicionados() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC
    }

    postAjax("PromocaoMix", "CarregarWidgetMixFornecedorAdicionados", obj, function (data) {
        $('#divPromocaoMixFornecedorAdicionados').html(data);
        LoadingOff();
    });
}

function fecharModalPromocaoMixFornecedor() {
    $('#modalPromocaoMixFornecedor').parent().modal('hide');
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    widgetPromocaoMix(CODPMC, TIPEDEPUBALVPMC);
    LoadingOff();
}

function addPesquisaMixFornecedor(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "AddPesquisaMixFornecedor",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPromocaoMixFornecedorAdicionados();
            $('#frmPesquisaPromocaoMixFornecedor').submit();
            atualizaMensagem("msgModalPromocaoMixFornecedor");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function salvarPromocaoMixFornecedor(e) {

    var CODPMC = $('#IDCodPmc').val();
    var CODFRN = e.parentElement.parentElement.children.item(1).innerText;


    var obj = {
        CODPMC: CODPMC,
        CODFRN: CODFRN
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "SaveMixFornecedor",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixFornecedorAdicionados();
            atualizaMensagem("msgModalPromocaoMixFornecedor");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}


function DeletePromocaoMixFornecedor(e) {

    var CODPMC = $('#IDCodPmc').val();
    var NUMSEQMIXACOCMC = e.parentElement.parentElement.children.item('0').value;
    var CODFRN = e.parentElement.parentElement.children.item('2').innerText;

    var obj = {
        CODPMC: CODPMC,
        CODFRN: CODFRN,
        NUMSEQMIXACOCMC: NUMSEQMIXACOCMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "DeleteMixFornecedor",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixFornecedorAdicionados();
            atualizaMensagem("msgModalPromocaoMixFornecedor");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixFornecedor");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllPromocaoMixFornecedor(e) {

    var CODPMC = $('#IDCodPmc').val();

    var obj = {
        CODPMC: CODPMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "DeleteMixFornecedor",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixFornecedorAdicionados();
            atualizaMensagem("msgModalPromocaoMixFornecedor");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixFornecedor");
        },
        complete: function () {
            LoadingOff();
        }
    });
}



//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Promoca Visualizar Mix Vigente

function abrirModalVisualizarMixVigente() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var TIPFRNACOCMC = $('#idTipoFornecedor').val();

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC,
        TIPFRNACOCMC: TIPFRNACOCMC
    }

    abrirModal("PromocaoMixVisualizarVigente", "CarregarModalItensVisualizarMixVigente", obj, "#modalPromocao", function () { resizeModalPromocao($('#modalListPromocaoVisualizarMixVigente')); })
}

function fecharModalVisualizarMixVigente() {
    $("#modalListPromocaoVisualizarMixVigente").parent().modal('hide');
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Promoca Visualizar Mix Vigente

function abrirModalPreVisualizarMixPromocao() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var TIPFRNACOCMC = $('#idTipoFornecedor').val();

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC,
        TIPFRNACOCMC: TIPFRNACOCMC
    }

    abrirModal("PromocaoMixVisualizarVigente", "CarregarModalPreVisualizarMixPromocao", obj, "#modalPromocao", function () { resizeModalPromocao($('#modalListPreVisualizarMixPromocao')); })
}

function fecharModalPreVisualizarMixPromocao() {
    $("#modalListPreVisualizarMixPromocao").parent().modal('hide');
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Processar Atualizações

function ProcessarAtualizacoesMixPromocao() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;
    var TIPFRNACOCMC = $('#idTipoFornecedor').val();

    var obj = {
        CODPMC: CODPMC,
        TIPEDEPUBALVPMC: TIPEDEPUBALVPMC,
        TIPFRNACOCMC: TIPFRNACOCMC
    }

    postAjax("PromocaoMixVisualizarVigente", "AtualizaMixPromocao", obj,
        function () {
            atualizaMensagem("msgModalMix");
            LoadingOff();
        },
        function () {
            atualizaMensagem("msgModalMix");
            LoadingOff();
        }
    );
}



function LiberarPromocao(idPmc) {
    getAjax("Promocao", "ModalLiberarPromocao" + '/' + idPmc,
        function (data) {
            ConfirmarPromocao(data, idPmc);
            LoadingOff();
        },
        function (data) {
            LoadingOff();
            ShowModal({ mensagem: "Não é possível liberar essa promoção.", titulo: "Atenção" })
        }
    );
}

function ConfirmarPromocao(html, idPmc) {
    ShowModalConfirmar({ titulo: 'Confirmar liberação', mensagem: html, callback: LiberarPromocaoCore, parametros: idPmc, width: 800 });


}

function LiberarPromocaoCore(idPmc, callback) {

    LoadingOn();
    $.ajax({
        url: SiteUrl + "Promocao/Liberar/" + idPmc,
        type: 'POST',
        success: function (data) {
            LoadingOff();
            if (data.indexOf('Sucesso') >= 0) {
                ShowModal({ mensagem: "SUCESSO", titulo: "Sucesso" })
            } else {
                ShowModal({ mensagem: data, titulo: "Não foi possível liberar" })
                callback();
            }
        },
        function() {
            LoadingOff();
            ShowModal({ mensagem: "Não foi possível liberar essa promoção.", titulo: "Erro" })
        }
    });

}

function dummy() {

}

function PromocaoCarregarBuSelecionadoAtividade() {
    var lstBUAtividade = $(".chkTipoAtividade");
    var lstBUAtividadeSelecionados = [];

    lstBUAtividade.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODUNDESRNGC: parseInt(check.val())
            }
            lstBUAtividadeSelecionados.push(obj);
        }
    });


    if (lstBUAtividadeSelecionados.length <= 0) {
        mostraMensagem("Favor%20escolher%20um%20BU!", "msgInfo", "msgmodalListPromocaoAtividade");
        LoadingOff();
    }
    else {

        postAjax("PromocaoPublicoAlvoAtividade", "BuscarAtividadePorBU", { obj: lstBUAtividadeSelecionados },
            function (data) {
                var lstAtividade = $(".hdfAtividade");

                //Desmarcar todos
                lstAtividade.each(function (element) {
                    var obj2 = $(this);
                    obj2.parent().children(".marcaAtividade").prop("checked", false);
                });

                for (var i = 0; i < data.length; i++) {
                    lstAtividade.each(function (element) {
                        var obj1 = $(this);
                        if (parseInt(obj1.parent().parent().children(".CodAti").text().trim()) === data[i].CODATI) {
                            obj1.parent().children(".marcaAtividade").prop("checked", true);
                        }
                    });
                }
                LoadingOff();
            },
            function (error) {
                mostraMensagem("Erro%20ao%20buscar%20por%20BU!", "msgDanger", "msgmodalListPromocaoAtividade");
            }
        )
    }
}

function PromocaoCarregarBuSelecionadoGrupamentoCliente() {
    var lstBUGrupamentoCliente = $(".chkTipoAtividade");
    var lstBUGrupamentoClienteSelecionados = [];

    lstBUGrupamentoCliente.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODUNDESRNGC: parseInt(check.val())
            }
            lstBUGrupamentoClienteSelecionados.push(obj);
        }
    });


    if (lstBUGrupamentoClienteSelecionados.length <= 0) {
        mostraMensagem("Favor%20escolher%20um%20BU!", "msgInfo", "msgmodalListPromocaoGrupamentoCliente");
        LoadingOff();
    }
    else {

        postAjax("PromocaoPublicoAlvoGrupamentoCliente", "BuscarGrupamentoClientePorBU", { obj: lstBUGrupamentoClienteSelecionados },
            function (data) {
                var lstGrupamentoCliente = $(".hdfGrupamentoCliente");

                //Desmarcar todos
                lstGrupamentoCliente.each(function (element) {
                    var obj2 = $(this);
                    obj2.parent().children(".marcaGrupamentoCliente").prop("checked", false);
                });

                for (var i = 0; i < data.length; i++) {
                    lstGrupamentoCliente.each(function (element) {
                        var obj1 = $(this);
                        if (parseInt(obj1.parent().parent().children(".CodGrpCli").text().trim()) === data[i].CODGRPCLI) {
                            obj1.parent().children(".marcaGrupamentoCliente").prop("checked", true);
                        }
                    });
                }
                LoadingOff();
            },
            function (error) {
                mostraMensagem("Erro%20ao%20buscar%20por%20BU!", "msgDanger", "msgmodalListGrupamentoCliente");
            }
        )
    }
}

function ModalImportXlsBrindesPromocaoSuccess() {
    // variavel para salvar o arquivo
    var files;

    // evento ao selecionar arquivo
    $('input[type=file]').on('change', prepareUpload);

    // salva o arquivo na variavel
    function prepareUpload(event) {
        files = event.target.files;
        if (files == undefined) {
            $(".btnImportarBrinde").attr("disabled", "disabled");
        }
        else {
            $(".btnImportarBrinde").removeAttr("disabled");
            $.each(files, function (index, value) {
                var nome = value.name;
                var extensao = nome.substring(nome.indexOf("."));
                if (extensao != ".xlsx" && extensao != ".xls") {
                    $(".btnImportarBrinde").attr("disabled", "disabled");
                    mostraMensagem("Importação%20de%20Arquivo%20não%20permitido!", "msgDanger", "msgModalImportBrinde");
                }
            });
        }
    }

    if (files == undefined) {
        $(".btnImportarBrinde").attr("disabled", "disabled");
    }
    else {
        $(".btnImportarBrinde").removeAttr("disabled");
    }

    $('#arquivo').on('submit', uploadFiles);

    // ao submeter o formulario
    function uploadFiles(event) {
        event.stopPropagation(); // para execução
        event.preventDefault(); // para completamente a execução

        var data = new FormData();
        $.each(files, function (key, value) {
            data.append(key, value);
        });

        var CODPMC = $('#IDCodPmc').val();

        LoadingOn();

        $.ajax({
            url: SiteUrl + 'PromocaoBrinde/ImportarXlsBrinde/' + CODPMC,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'text',
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    LoadingOn();
                    $('#modalPromocao').html(data);
                    $('#modalPromocao').modal();
                    $('#modal').modal('toggle');
                    BuscarListaBrindes();
                    LoadingOff();
                    resizeModalPromocao($('#modalErros'));
                }
                else {
                    console.log('ERRORS: ' + data.error);
                    resizeModalPromocao($('#modalErros'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
            }
        });
    }

    $('.ace-file-container').ace_file_input({
        no_file: 'Sem Arquivo ...',
        btn_choose: 'Selecione',
        btn_change: 'Mudar',
        droppable: false,
        onchange: null,
        thumbnail: false
    });
}


function ClonarPromocao(CODPMC) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "Promocao/Clonar",
        type: 'POST',
        data: { CODPMC: CODPMC },
        success: function (data) {
            LoadingOn();
            if (data != null && data != "")
                window.location = SiteUrl + 'Promocao/Edit/' + data.CODPMC;
            else
                atualizaMensagem();
        },
        error: function (error) {
            atualizaMensagem();
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function ModalImportXls() {
    // variavel para salvar o arquivo
    var files;

    // evento ao selecionar arquivo
    $('input[type=file]').on('change', prepareUpload);

    // salva o arquivo na variavel
    function prepareUpload(event) {
        files = event.target.files;
        if (files == undefined) {
            $("#btnImportarXLS").attr("disabled", "disabled");
        }
        else {
            $("#btnImportarXLS").removeAttr("disabled");
            $.each(files, function (index, value) {
                var nome = value.name;
                var extensao = nome.substring(nome.indexOf("."));
                if (extensao != ".xlsx" && extensao != ".xls") {
                    $("#btnImportarXLS").attr("disabled", "disabled");
                    //mostraMensagem("Importação%20de%20Arquivo%20não%20permitido!", "msgDanger", "msgModalCliente");
                }
            });
        }

    }

    if (files == undefined) {
        $("#btnImportarXLS").attr("disabled", "disabled");
    }
    else {
        $("#btnImportarXLS").removeAttr("disabled");
    }


    $('#arquivo').on('submit', uploadFiles);

    // ao submeter o formulario
    function uploadFiles(event) {
        event.stopPropagation(); // para execução
        event.preventDefault(); // para completamente a execução
        LoadingOn();
        var data = new FormData();
        $.each(files, function (key, value) {
            data.append(key, value);
        });
        var obj = {
            CODPMC: $('#IDCodPmc').val(),
            TIPEDEPUBALVPMC: 4
        }

        $.ajax({
            url: SiteUrl + $(event.target).attr('value') + '/' + obj.CODPMC + '/' + obj.TIPEDEPUBALVPMC,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'text',
            processData: false,
            contentType: false,
            success: function (data) {
                if (data == "TrueCliente") {
                    abrirWidgetPromocaoPublicoAlvoCliente();
                    atualizaMensagem("msgModalPromocaoCliente");
                }
                else if (data == "FalseCliente") {
                    abrirWidgetPromocaoPublicoAlvoCliente();
                    atualizaMensagem("msgModalPromocaoCliente");
                }
                else if (data == "TrueRcaVendedor") {
                    abrirWidgetPromocaoPublicoAlvoRcaVendedor();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalPromocaoRcaVendedor");
                }
                else if (data == "FalseRcaVendedor") {
                    atualizaMensagem("msgModalPromocaoRcaVendedor");
                    abrirWidgetPromocaoPublicoAlvoRcaVendedor();
                }
                else if (data == "TrueGerenteMercado") {
                    abrirWidgetPromocaoPublicoAlvoGerenteMercado();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalPromocaoGerenteMercado");
                }
                else if (data == "FalseGerenteMercado") {
                    atualizaMensagem("msgModalPromocaoGerenteMercado");
                    abrirWidgetPromocaoPublicoAlvoGerenteMercado();
                }
                else if (data == "TrueMixFornecedor") {
                    abrirWidgetPromocaoMixFornecedorAdicionados();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalPromocaoMixFornecedor");
                }
                else if (data == "FalseMixFornecedor") {
                    atualizaMensagem("msgModalPromocaoMixFornecedor");
                    abrirWidgetPromocaoMixFornecedorAdicionados();
                }
                else if (data == "TrueMixMercadoria") {
                    abrirWidgetPromocaoMixMercadoriaAdicionados();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalPromocaoMixMercadoria");
                }
                else if (data == "FalseMixMercadoria") {
                    atualizaMensagem("msgModalPromocaoMixMercadoria");
                    abrirWidgetPromocaoMixMercadoriaAdicionados();
                }
                else if (data == "TrueMixClasseMercadoria") {
                    abrirWidgetPromocaoMixClasseMercadoriaAdicionados();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalPromocaoMixClasseMercadoria");
                }
                else if (data == "FalseMixClasseMercadoria") {
                    atualizaMensagem("msgModalPromocaoMixClasseMercadoria");
                    abrirWidgetPromocaoMixClasseMercadoriaAdicionados();
                }
                else if (data == "TrueMixFamiliaMercadoria") {
                    abrirWidgetPromocaoMixFamiliaMercadoriaAdicionados();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalPromocaoMixFamiliaMercadoria");
                }
                else if (data == "FalseMixFamiliaMercadoria") {
                    atualizaMensagem("msgModalPromocaoMixFamiliaMercadoria");
                    abrirWidgetPromocaoMixFamiliaMercadoriaAdicionados();
                }
                else if (data == "TrueMixParametrosMercadoria") {
                    abrirModalParametrosMixPromocao();
                }
                else if (data == "FalseMixParametrosMercadoria") {
                    atualizaMensagem("divAlertaParametrosMixPromocao");
                }
                else if (data == "TrueMixCashParametrosMercadoria") {
                    abrirModalParametrosMixCashPromocao();
                }
                else if (data == "FalseMixCashParametrosMercadoria") {
                    atualizaMensagem("divAlertaParametrosMixCashPromocao");
                }
                LoadingOff();
            },
            error: function (error) {
                atualizaMensagem();
                //console.log('ERRORS: ' + textStatus);
                LoadingOff();
            }
        });
    }

    $('.ace-file-container').ace_file_input({
        no_file: 'Sem Arquivo ...',
        btn_choose: 'Selecione',
        btn_change: 'Mudar',
        droppable: false,
        onchange: null,
        thumbnail: false
    });
}


function AbrirPromocaoConfirm(idPmc) {
    ShowModalConfirmar({ titulo: 'Confirmar reabrir', mensagem: "Deseja reabrir esta promoção?", callback: AbrirPromocao, parametros: idPmc, width: 800 });
}
function AbrirPromocao(idPmc) {
    postAjax("Promocao", "Abrir" + '/' + idPmc,
        null,
        function (data) {
            LoadingOff();
            atualizaMensagem();
            if (data != "") {
                window.location.href = window.location.href;
                location.reload();
            }
        },
        function (data) {
            LoadingOff();
            atualizaMensagem();
        }
    );
}

function togglePromocaoPesquisaMixMercadoria(type) {
    if (type === 'ParaPesquisaPorPromocao') {
        PromocaoCarregarWidgetLoadPesquisaMixMercadoriaPorPromocao();
        $('#divLoadPesquisaPromocaoMixMercadoriaPorPromocao').show()
        $('#divLoadPesquisaPromocaoMixMercadoria').hide()
    }
    else if (type === 'ParaPesquisaPadrao') {
        PromocaoCarregarWidgetLoadPesquisaMixMercadoria();
        $('#divLoadPesquisaPromocaoMixMercadoriaPorPromocao').hide()
        $('#divLoadPesquisaPromocaoMixMercadoria').show()
    }
}

function PromocaoCarregarWidgetLoadPesquisaMixMercadoriaPorPromocao() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPEDEPUBALVPMC = 4;

    getAjax("PromocaoMix", "CarregarWidgetLoadPesquisaMixMercadoriaPorPromocao/" + CODPMC + '/' + TIPEDEPUBALVPMC
        , function (data) {
            $('#divLoadPesquisaPromocaoMixMercadoriaPorPromocao').html(data);
            LoadingOff();
        });
}

function MixMercadoriaPorPromocaoLoadingOff_UpdateMessage() {
    $('#loadingDialog').modal('hide');
    atualizaMensagem("msgModalPromocaoMixMercadoria");
    ChosenConfig();
    ChosenResize();
    MaskConfig();
    PopoverConfig();
    WidgetConfig();
}

function PromocaoAddPesquisaMixMercadoriaPorPromocao(data) {
    if (data) {
        data.CODGRPMIXMERPMC = $('#drpAddPromocaoMixFaixaPorPromocao').val()
    }
    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "AddPesquisaMixMercadoriaPorPromocao",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPromocaoMixMercadoriaAdicionados();
            $('#frmPesquisaPromocaoMixMercadoriaPorPromocao').submit();
            atualizaMensagem("msgModalPromocaoMixMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function salvarPromocaoMixMercadoriaPorPromocao(e) {

    var CODPMC = $('#IDCodPmc').val();
    var CODMER = e.parentElement.parentElement.children.item(1).innerText;
    var CODGRPMIXMERPMC = $('#drpAddPromocaoMixFaixaPorPromocao').val();


    var obj = {
        CODPMC: CODPMC,
        CODMER: CODMER,
        CODGRPMIXMERPMC: CODGRPMIXMERPMC
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "PromocaoMix" + "/" + "SaveMixMercadoriaPorPromocao",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPromocaoMixMercadoriaAdicionados();
            atualizaMensagem("msgModalPromocaoMixMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}


//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Modal Parâmetros Mix


function abrirModalParametrosMixPromocao() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPFRNACOCMC = $('#idTipoFornecedor').val();
    var INDCPTFCH = $('#rbtINDCPTFCHSim').prop('checked') ? $('#rbtINDCPTFCHSim').prop('value') : $('#rbtINDCPTFCHNao').prop('value')
    var TIPSITPMC = $('#hdfSituacaoPromocao').val();

    var obj = {
        CODPMC: CODPMC,
        TIPFRNACOCMC: TIPFRNACOCMC,
        INDCPTFCH: INDCPTFCH,
        TIPSITPMC: TIPSITPMC
    }

    abrirModal("PromocaoParametrosMix", "CarregarModalParametrosMixPromocao", obj, "#modalPromocao", function () { resizeModalPromocao($('#modalParametrosMixMixPromocao')); })
}

function fecharModalParametrosMixPromocao() {
    location.reload();
}

function marcarOuDesmarcarTodasMercadorias() {
    if ($("#chkTodasMercadorias").prop("checked"))
        $(".SelecioneMercadoria").prop("checked", true);
    else $(".SelecioneMercadoria").prop("checked", false);
}

function aplicarParametros() {
    LoadingOn();
    if (validarParametros()) {
        var data = montarObjParametro();

        $.ajax({
            url: SiteUrl + "PromocaoParametrosMix/Aplicar",
            type: 'POST',
            data: { list: data },
            success: function (data) {
                abrirModalParametrosMixPromocao();
                if (data != "") {
                    atualizaMensagem("divAlertaParametrosMixPromocao");
                }
                LoadingOff();
            },
            error: function (error) {
                atualizaMensagem("divAlertaParametrosMixPromocao");
                LoadingOff();
            }
        });
    }
    LoadingOff();
}

function validarParametros() {
    var indcadorComboFechado = ($('#hdfIndicadorComboFechado').val() == 1);
    var indicadorItemObrigatorio = ($('#radioItemObrigatorioSim').prop('checked') == 1);
    var qtdMinima = parseInt($('#txtQtdMinima').val());
    var qtdMaxima = parseInt($('#txtQtdMaxima').val());

    if ($('#txtQtdMinima').val() == "") {
        ShowModal({ mensagem: "Quantidade Minima deve ser preenchida." });
        return false;
    }

    if ($('#txtQtdMaxima').val() == "") {
        ShowModal({ mensagem: "Quantidade Máxima deve ser preenchida." });
        return false;
    }

    if (indcadorComboFechado && (qtdMaxima % qtdMinima != 0)) {
        ShowModal({ mensagem: "Quantidade Máxima deve ser múltipla de Quantidade Minima." });
        return false;
    }

    if (indicadorItemObrigatorio && qtdMinima < 1) {
        ShowModal({ mensagem: "Para Itens Obrigatórios, o campo Quantidade Minima deve ser maior que 0." });
        return false;
    }
    return true;
}

function changeIndJornalSmart() {
    $('#radioItemObrigatorioSim').prop('checked', true);
}

function montarObjParametro() {
    var codPmc = $('#hdfCodPmc').val();
    var tipFrnAcoCmc = $('#hdfTipFrnAcoCmc').val();
    var qtdMinima = parseInt($('#txtQtdMinima').val());
    var qtdMaxima = parseInt($('#txtQtdMaxima').val());
    var radioItemObrigatorio = $('#radioItemObrigatorioSim').prop('checked') ? 1 : 0;
    var radioJornalSmart = $('#radioJornalSmartSim').prop('checked') ? 1 : 0;
    var grid = $("#tableModalParametrosMixPromocao tbody input[name=SelecioneMercadoria][type=checkbox]");
    var lista = [];

    grid.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            lista.push({
                CODPMC: codPmc,
                TIPFRNACOCMC: tipFrnAcoCmc,
                CODMER: parseFloat(check.parent().parent().children('.CodigoMercadoria').html().trim()),
                QDEMNMMERMIX: qtdMinima,
                QDEMAXMERMIX: qtdMaxima,
                INDITEOBR: radioItemObrigatorio,
                INDJRNSMA: radioJornalSmart
            });
        }
    });

    if (lista.length < 0)
        ShowModal({ mensagem: "Selecione pelo menos uma mercadoria." });

    return lista;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Modal Parâmetros Mix Cash

function abrirModalParametrosMixCashPromocao() {
    var CODPMC = $('#IDCodPmc').val();
    var TIPFRNACOCMC = $('#idTipoFornecedor').val();
    var TIPSITPMC = $('#hdfSituacaoPromocao').val();

    var obj = {
        CODPMC: CODPMC,
        TIPFRNACOCMC: TIPFRNACOCMC,
        TIPSITPMC: TIPSITPMC
    }

    abrirModal("PromocaoParametrosMix", "CarregarModalParametrosMixCashPromocao", obj, "#modalPromocao", function () { resizeModalPromocao($('#modalParametrosMixCashPromocao')); })
}

function fecharModalParametrosMixCashPromocao() {
    location.reload();
}

function aplicarParametrosCash() {
    if (validarParametrosCash()) {
        var data = montarObjParametroCash();

        LoadingOn();
        $.ajax({
            url: SiteUrl + "PromocaoParametrosMix/AplicarCash",
            type: 'POST',
            data: { list: data },
            success: function (data) {
                LoadingOff();
                abrirModalParametrosMixCashPromocao();
                if (data != "") {
                    atualizaMensagem("divAlertaParametrosMixCashPromocao");
                }
            },
            error: function (error) {
                atualizaMensagem("divAlertaParametrosMixCashPromocao");
            }
        });
    }
}

function validarParametrosCash() {
    var indicadorItemObrigatorio = ($('#radioItemObrigatorioSim').prop('checked') == 1);
    var qtdMinima = parseInt($('#txtQtdMinima').val());

    if ($('#txtQtdMinima').val() == "") {
        ShowModal({ mensagem: "Quantidade Minima deve ser preenchida." });
        return false;
    }

    if (indicadorItemObrigatorio && qtdMinima < 1) {
        ShowModal({ mensagem: "Para Itens Obrigatórios, o campo Quantidade Minima deve ser maior que 0." });
        return false;
    }
    return true;
}

function montarObjParametroCash() {
    var codPmc = $('#hdfCodPmc').val();
    var tipFrnAcoCmc = $('#hdfTipFrnAcoCmc').val();
    var qtdMinima = parseInt($('#txtQtdMinima').val());
    var radioItemObrigatorio = $('#radioItemObrigatorioSim').prop('checked') ? 1 : 0;
    var grid = $("#tableModalParametrosMixCashPromocao tbody input[name=SelecioneMercadoria][type=checkbox]");
    var lista = [];

    grid.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            lista.push({
                CODPMC: codPmc,
                TIPFRNACOCMC: tipFrnAcoCmc,
                CODMER: parseFloat(check.parent().parent().children('.CodigoMercadoria').html().trim()),
                QDEMNMMERMIX: qtdMinima,
                INDITEOBR: radioItemObrigatorio,
            });
        }
    });

    if (lista.length < 0)
        ShowModal({ mensagem: "Selecione pelo menos uma mercadoria." });

    return lista;
}

function ckbPromocaoRelampago_OnClick(e) {
    if (e == true) {
        $('.horas').show();
        $('#divDataInicio').removeClass('col-sm-4').addClass('col-sm-3')
        $('#divDataFim').removeClass('col-sm-4').addClass('col-sm-3')
    }
    else {
        $('.horas').hide();
        $('#divDataInicio').removeClass('col-sm-3').addClass('col-sm-4')
        $('#divDataFim').removeClass('col-sm-3').addClass('col-sm-4')
    }
}