﻿//Função Editar Mecanica
function modalEditMecanicaPromocao(objeto) {

    var CODACOCMC = objeto.CODACOCMC;
    var NUMSEQRGRACOCMC = objeto.NUMSEQRGRACOCMC;


    abrirModal('RelacaoAcaoMecanica', 'EditMecanicaPromocao/' + CODACOCMC + '/' + NUMSEQRGRACOCMC,
        {},
        "#modalMecanicaPromocao",
        function (data) {
            drpTipoPromocao_Change();
            widgetFiliais(CODACOCMC, NUMSEQRGRACOCMC);
            widgetPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC);
            //widgetClientesExcludentes(CODACOCMC, NUMSEQRGRACOCMC);
            widgetMix(CODACOCMC, NUMSEQRGRACOCMC);

            resizeModal($('#modalEditMecanicaPromocao').parents('.modal'));

            //$('.modal-backdrop.fade:not(.in)').remove();
            $('.modal-backdrop.fade:not(:first)').remove();
        }
    );
}

function modalCriarMecanicaPromocao() {
    var CODACOCMC = $("#CODACOCMC").val();
    abrirModal('RelacaoAcaoMecanica', 'CriarMecanicaPromocao/' + CODACOCMC,
        {},
        "#modalMecanicaPromocao",
        function (data) {
            drpTipoPromocao_Change();
            //widgetFiliais(CODACOCMC, 0);
            //widgetPublicoAlvo(CODACOCMC, 0);
            resizeModal($('#modalEditMecanicaPromocao').parents('.modal'));
        }
    );
}

function modalClonarMecanicaPromocao() {
    var CODACOCMC = $("#CODACOCMC").val();
    abrirModal('RelacaoAcaoMecanica', 'ModalClonarMecanica/' + CODACOCMC,
        {},
        "#modalMecanicaPromocao",
        function (data) {
            resizeModal($('#modalEditMecanicaPromocao').parents('.modal'));
        }
    );
}

function ModalClonarMecanicaOnComplete() {
    LoadingOff_UpdateMessage()
    resizeModal($('#modalListMecanicaPromocao'));
}

function ClonarMecanicaPromocao(CODACOCMC_Copy, NUMSEQRGRACOCMC_Copy) {
    var CODACOCMC_Paste = $("#CODACOCMC").val();
    postAjax('RelacaoAcaoMecanica', 'ClonarMecanica',
        {
            CODACOCMC_Copy: CODACOCMC_Copy,
            NUMSEQRGRACOCMC_Copy: NUMSEQRGRACOCMC_Copy,
            CODACOCMC_Paste: CODACOCMC_Paste
        },
        function (data) {
            modalEditMecanicaPromocao(data)
        },
        function (data) {
            atualizaMensagem();
        },
        false
    );
}


//Função OnChange Campo DRP_Tipo_Promoção e QtdGrpFaixa
function drpTipoPromocao_Change() {

    var tipo = $('#drpTipoPromocao').val();
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    $('#divQtdGrupos').hide();
    $('#divQtdFaixas').hide();



    switch (tipo) {
        case "0":
            $('#divQtdGrupos').hide();
            $('#divQtdFaixas').hide();
            $('#divCriterio').hide();
            $('#divQtdGrupos input[name="QDEGRPMIXPMC"]').val('0');

            if (!$('#divQtdFaixas input[name="QDEFXAPMC"]').val()) {
                $('#divQtdFaixas input[name="QDEFXAPMC"]').val('0');
            }
           
            var TIPPMC = $('#drpTipoPromocao').val();
            getAjax("RelacaoAcaoMecanica", "EditMecanicaPromocaoGridRegistros/" + CODACOCMC + '/' + NUMSEQRGRACOCMC + '/' + TIPPMC, function (data) {
                $('#divParametrosCalculoSimples').html(data);
                LoadingOff();
                if ($('#TIPEQCCALCTRPMC').val() == '1') { //Se for Faixa Finita, qtdFaixa é igual a 1
                    $('#divQtdFaixas input[name="QDEFXAPMC"]').val('0');
                }
                else {
                    $('#divQtdFaixas input[name="QDEFXAPMC"]').val('1');
                }
                trocarMascaraCashBack();
            })
            break;
        case "1":
            $('#divQtdGrupos').show();
            $('#divCriterio').hide();
            $('#divQtdFaixas input[name="QDEFXAPMC"]').val('0');
            var QDEGRPMIXPMC = $('#txtQtdGrupos').val();
            var TIPPMC = $('#drpTipoPromocao').val();
            if (QDEGRPMIXPMC != "") {
                getAjax("RelacaoAcaoMecanica", "EditMecanicaPromocaoGridRegistrosCombinada/" + CODACOCMC + '/' + NUMSEQRGRACOCMC + '/' + QDEGRPMIXPMC + '/' + TIPPMC, function (data) {
                    $('#divParametrosCalculoSimples').html(data);
                    LoadingOff();
                    trocarMascaraCashBack();
                })
            }
            break;
        case "2":
            $('#divQtdFaixas').show();
            $('#divCriterio').show();
            $('#divQtdGrupos input[name="QDEGRPMIXPMC"]').val('0');
            var QDEFXAPMC = $('#txtQtdFaixas').val();
            var TIPPMC = $('#drpTipoPromocao').val();
            if (QDEFXAPMC != "") {
                getAjax("RelacaoAcaoMecanica", "EditMecanicaPromocaoGridRegistrosFaixa/" + CODACOCMC + '/' + NUMSEQRGRACOCMC + '/' + QDEFXAPMC + '/' + TIPPMC, function (data) {
                    $('#divParametrosCalculoSimples').html(data);
                    LoadingOff();
                    trocarMascaraCashBack();
                })
            }
            break;
    }
}

//Função Salvar Dados
function jsSalvarDados() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var form = $("#frmWidgetMecanicaPromocaoGerais");
    //var tbl = document.getElementById("tabelaFiliais").rows;
    var lstChecked = [];

    // Função para salvar dados do Widget dos dados Gerais
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };



    //Seleciona Filiais Marcadas
    var lstCheck = $('#tabelaFiliais tbody input[name="SelecioneFilial"][type="checkbox"]');

    lstCheck.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODFILEMP: parseFloat(check.parents('tr').children('.tdFilial').children('.codFilialValue').attr('value'))
            };

            lstChecked.push(obj);
        }
    });

    //Carrega dados Critério de Apuração

    var tipo = $('#drpTipoPromocao').val();

    switch (tipo) {
        case "0":
            var tblGridRegistros = $('#tblGridRegistros tr:has(td)').map(function (i, v) {
                var $td = $('td', this);

                if ($td.parent().parent().parent().parent().find('select option:selected').val() == 1) {  // 1 = ESCALA PROGRESSIVA
                    return {
                        TIPEDENIVPRMPMC: 0,
                        CODGRPMIXMERPMC: 0,
                        TIPPMC: $('#drpTipoPromocao').val(),
                        TIPEQCCALCTRPMC: 1,
                        CODCTRPMC: $td.eq(1).find('select option:selected').val(),
                        VLRLIMMNMFXACTRPMC: 0,
                        TAMIVLFXAEQCCALCTR: $td.eq(2).find('input[name$="TAMIVLFXAEQCCALCTR"]').val(),
                        QDEPTOFXACTRPMC: $('#txtQtdPremioAtingimentoCriterioNormal').val(),
                        INDTIPPTOPMC: $('div.tipoPremio select[name=INDTIPPTOPMC]').val(),
                        VLRLIMMAXCTR: $('#txtVLRLIMMAXCTR').val()
                    }
                }
                else {//2 = FAIXAS FINITAS
                    return {
                        TIPEDENIVPRMPMC: 1,
                        CODGRPMIXMERPMC: 0,
                        TIPPMC: $('#drpTipoPromocao').val(),
                        TIPEQCCALCTRPMC: 2,
                        CODCTRPMC: $td.eq(1).find('select option:selected').val(),
                        VLRLIMMNMFXACTRPMC: $td.eq(2).find('input[name$="VLRLIMMNMFXACTRPMC"]').val(),
                        TAMIVLFXAEQCCALCTR: 0,
                        VLRLIMMAXFXACTRPMC: ($td.eq(1).find('select option:selected').val() == 5 ? "9999999999,99" : "9999999999"),
                        QDEPTOFXACTRPMC: $('#txtQtdPremioAtingimentoCriterioNormal').val(),
                        INDTIPPTOPMC: $('div.tipoPremio select[name=INDTIPPTOPMC]').val(),
                        VLRLIMMAXCTR: $('#txtVLRLIMMAXCTR').val()
                    }
                }
            }).get();
            break;
        case "1":
            var tblGridRegistrosCombinada = $('#tblGridRegistrosCombinada tr:has(td)').map(function (i, v) {
                var $td = $('td', this);

                if ($td.parent().parent().parent().parent().find('select option:selected').val() == 1) {  // 1 = ESCALA PROGRESSIVA
                    return {
                        TIPEDENIVPRMPMC: $td.eq(0).text().trim(),
                        CODGRPMIXMERPMC: $td.eq(0).text().trim(),
                        TIPPMC: $('#drpTipoPromocao').val(),
                        CODCTRPMC: $td.eq(1).find('select option:selected').val(),
                        VLRLIMMNMFXACTRPMC: 0,
                        TAMIVLFXAEQCCALCTR: $td.eq(2).find('input[name$="TAMIVLFXAEQCCALCTR"]').val(),
                        DESCTRMIXPMC: $td.eq(3).find('input').val(),
                        QDEPTOFXACTRPMC: $('#txtQtdPremioAtingimentoCriterioCombinada').val(),
                        TIPEQCCALCTRPMC: 1,
                        INDTIPPTOPMC: $('div.tipoPremio select[name=INDTIPPTOPMC]').val(),
                        VLRLIMMAXCTR: $('#txtVLRLIMMAXCTR').val()
                    }
                } else {//2 = FAIXAS FINITAS
                    return {
                        TIPEDENIVPRMPMC: $td.eq(0).text().trim(),
                        CODGRPMIXMERPMC: $td.eq(0).text().trim(),
                        TIPPMC: $('#drpTipoPromocao').val(),
                        CODCTRPMC: $td.eq(1).find('select option:selected').val(),
                        VLRLIMMNMFXACTRPMC: $td.eq(2).find('input[name$="VLRLIMMNMFXACTRPMC"]').val(),
                        VLRLIMMAXFXACTRPMC: ($td.eq(1).find('select option:selected').val() == 5 ? "9999999999" : "9999999999"),// sistema antigo gera 9999999999 mesmo para quando e valor
                        DESCTRMIXPMC: $td.eq(3).find('input').val(),
                        QDEPTOFXACTRPMC: $('#txtQtdPremioAtingimentoCriterioCombinada').val(),
                        TIPEQCCALCTRPMC: 2,
                        INDTIPPTOPMC: $('div.tipoPremio select[name=INDTIPPTOPMC]').val(),
                        VLRLIMMAXCTR: $('#txtVLRLIMMAXCTR').val()
                    }
                }
            }).get();
            break;
        case "2":
            var tblGridRegistrosFaixa = $('#tblGridRegistrosFaixa tr:has(td)').map(function (i, v) {
                var $td = $('td', this);
                if ($td.eq(3).contents('input').val() == 'Acima') {
                    if ($('#drpCriterioPromocaoFaixa').val() == 5)
                        var VLRMAXCTRPMC = "9999999999,99";
                    else
                        var VLRMAXCTRPMC = "9999999999";
                }
                else {
                    var VLRMAXCTRPMC = $td.eq(3).contents('input').val().replace(',00', '').replaceAll('.', '');
                }
                let QDEPTOFXACTRPMC;
                if ($('div.tipoPremio select[name=INDTIPPTOPMC]').val() == 0)
                    QDEPTOFXACTRPMC = $('#txtQtdPremioAtingimentoCriterioFaixa').val();
                else
                    QDEPTOFXACTRPMC = $td.eq(4).find('input').val().replace(',00', '').replaceAll('.', '');
                return {
                    TIPEDENIVPRMPMC: $td.eq(0).text().trim(),
                    CODGRPMIXMERPMC: 0,
                    TIPPMC: $('#drpTipoPromocao').val(),
                    VLRLIMMNMFXACTRPMC: $td.eq(1).find('input').val().replace(',00', '').replaceAll('.', ''),
                    VLRLIMMAXFXACTRPMC: VLRMAXCTRPMC,
                    QDEPTOFXACTRPMC: QDEPTOFXACTRPMC,
                    TIPEQCCALCTRPMC: 2,
                    CODCTRPMC: $('#drpCriterioPromocaoFaixa').val(),
                    INDTIPPTOPMC: $('div.tipoPremio select[name=INDTIPPTOPMC]').val(),
                    VLRLIMMAXCTR: $('#txtVLRLIMMAXCTR').val()
                }
            }).get();
            break;
    }

    var obj = form.serializeObject();
    obj.RelacaoAcaoMecanicaFilialList = lstChecked;
    obj.RELACAOACAOMECANICAREGISTROSLIST = tblGridRegistros;
    obj.RELACAOACAOMECANICAREGISTROSCOMBINADALIST = tblGridRegistrosCombinada;
    obj.RELACAOACAOMECANICAREGISTROSFAIXALIST = tblGridRegistrosFaixa;

    //postAjax("RelacaoAcaoMecanica", "Save", data);

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanica/Save",
        type: 'POST',
        data: obj,
        success: function (data) {
            modalEditMecanicaPromocao(data);
            //$('#modalEditMecanicaPromocao').parent().modal('hide');
            mostraMensagem("Registros%20salvo%20com%20sucesso!", "msgSuccess", "msgModalEditMecanicaPromocao");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgModalEditMecanicaPromocao");
        },
        complete: function () {
            atualizaMensagem(divMsg);
            LoadingOff();
        }
    });

}

function tipoCriterioChange(sender) {
    if ($(sender).val() == '2') {
        $(sender).parent().parent().find('input[name$="TAMIVLFXAEQCCALCTR"]').attr('alt', 'inteiro')
        $(sender).parent().parent().find('input[name$="VLRLIMMNMFXACTRPMC"]').attr('alt', 'inteiro')
    } else {
        $(sender).parent().parent().find('input[name$="TAMIVLFXAEQCCALCTR"]').attr('alt', 'decimal_positivo_2')
        $(sender).parent().parent().find('input[name$="VLRLIMMNMFXACTRPMC"]').attr('alt', 'decimal_positivo_2')
    }
    $(sender).parent().parent().find('input[name$="TAMIVLFXAEQCCALCTR"]').val('');
    $(sender).parent().parent().find('input[name$="VLRLIMMNMFXACTRPMC"]').val('');

    MaskConfig();
}
function formaCalculoCombinadaChange(sender) {

    if ($(sender).val() == '1') {
        $("#tblGridRegistrosCombinada").find('input[name$="TAMIVLFXAEQCCALCTR"]').show();
        $("#tblGridRegistrosCombinada").find('input[name$="VLRLIMMNMFXACTRPMC"]').hide();
    } else {
        $("#tblGridRegistrosCombinada").find('input[name$="TAMIVLFXAEQCCALCTR"]').hide();
        $("#tblGridRegistrosCombinada").find('input[name$="VLRLIMMNMFXACTRPMC"]').show();
    }
    $("#tblGridRegistrosCombinada").find('input[name$="TAMIVLFXAEQCCALCTR"]').val('');
    $("#tblGridRegistrosCombinada").find('input[name$="VLRLIMMNMFXACTRPMC"]').val('');

    MaskConfig();
}

function formaCalculoChange(sender) {

    if ($(sender).val() == '1') { //ESCALA PROGRESSIVA  
        $("#tblGridRegistros").find('input[name$="TAMIVLFXAEQCCALCTR"]').show();
        $("#tblGridRegistros").find('input[name$="VLRLIMMNMFXACTRPMC"]').hide();
        $('#divQtdFaixas input[name="QDEFXAPMC"]').val('0');
    }
    else {
        $("#tblGridRegistros").find('input[name$="TAMIVLFXAEQCCALCTR"]').hide();
        $("#tblGridRegistros").find('input[name$="VLRLIMMNMFXACTRPMC"]').show();
        $('#divQtdFaixas input[name="QDEFXAPMC"]').val('1'); //preencher o campo qtd faixa com o valor 1 quando for normal e a mecanica for faixa finita
    }
    $("#tblGridRegistros").find('input[name$="TAMIVLFXAEQCCALCTR"]').val('');
    $("#tblGridRegistros").find('input[name$="VLRLIMMNMFXACTRPMC"]').val('');

    MaskConfig();
}

//Função CheckBox Marca e Desmarca Todas Filiais
function marcarOuDesmarcarTodasFiliais() {

    if ($("#chkTodasFiliais").prop("checked"))
        $(".marca").prop("checked", true);
    else $(".marca").prop("checked", false);

}

function marcarOuDesmarcarTodosEstados() {

    if ($("#chkTodosEstados").prop("checked"))
        $(".marcaEstado").prop("checked", true);
    else $(".marcaEstado").prop("checked", false);

}

function marcarOuDesmarcarTodasAtividades() {

    if ($("#chkTodasAtividades").prop("checked"))
        $(".marcaAtividade").prop("checked", true);
    else $(".marcaAtividade").prop("checked", false);

}

function marcarOuDesmarcarTodosGrupamentoCliente() {
    if ($("#chkTodosGrupamentoCliente").prop("checked")) {
        $(".marcaGrupamentoCliente").prop("checked", false);
        $("#chkTodosGrupamentoCliente").prop("checked", false);
    }
    else {
        $(".marcaGrupamentoCliente").prop("checked", true);
        $("#chkTodosGrupamentoCliente").prop("checked", true);
    }
}

function marcarOuDesmarcarTodosNegocioCliente() {
    if ($("#chkTodosNegocioCliente").prop("checked"))
        $(".marcaNegocioCliente").prop("checked", true);
    else $(".marcaNegocioCliente").prop("checked", false);
}

function marcarOuDesmarcarTodosSegmentoCliente() {
    if ($("#chkTodosSegmentoCliente").prop("checked"))
        $(".marcaSegmentoCliente").prop("checked", true);
    else $(".marcaSegmentoCliente").prop("checked", false);
}


function marcarOuDesmarcarTodosGerenteVendas() {
    if ($("#chkTodosGerenteVendas").prop("checked"))
        $(".marcaGerenteVendas").prop("checked", true);
    else $(".marcaGerenteVendas").prop("checked", false);
}

function marcarOuDesmarcarTodosGruposMercadorias() {
    if ($("#chkTodosGruposMercadorias").prop("checked"))
        $(".marcaGrupoMercadoria").prop("checked", true);
    else $(".marcaGrupoMercadoria").prop("checked", false);
}


function marcarOuDesmarcarTodasAtividadesDoMesmoSeguimento(CODUNDESRNGC) {

    getAjax("", "" + CODUNDESRNGC,
        function (data) {

            var lstAtividade = $(".hdfAtividade");

            for (var i = 0; i < data.length; i++) {
                lstAtividade.each(function (element) {
                    var obj = $(this);
                    if (obj.val() === data[i]) {
                        if (e.checked)
                            obj.parent().children(".marcaAtividade").prop("checked", true)
                        else obj.parent().children(".marcaAtividade").prop("checked", false);
                    }
                });
            }
        },
        function (error) {
            alert("Erro ao Buscar Por BU.")
        }
    )
}

function CarregarBuSelecionadoGrupamentoCliente() {
    var lstBUGrupamentoCliente = $(".chkBUGrupamentoCliente");
    var lstBUGrupamentoClienteSelecionados = [];

    lstBUGrupamentoCliente.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODUNDESRNGC: parseInt(check.val())
            }
            lstBUGrupamentoClienteSelecionados.push(obj);
        }
    });


    if (lstBUGrupamentoClienteSelecionados.length <= 0) {
        mostraMensagem("Favor%20escolher%20um%20BU!", "msgInfo", "msgmodalListGrupamentoCliente");
        LoadingOff();
    }
    else {

        postAjax("RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente", "BuscarGrupamentoClientePorBU", { obj: lstBUGrupamentoClienteSelecionados },
            function (data) {
                var lstGrupamentoCliente = $(".marcaGrupamentoCliente");

                //desmarca todos
                lstGrupamentoCliente.prop("checked", false);

                //percorre a lista marcando os itens de acordo com as BU's selecionadas
                for (var i = 0; i < data.length; i++) {
                    lstGrupamentoCliente.each(function (element) {
                        var obj1 = $(this);
                        if (obj1.parent().children("input[type='hidden'].CODGRPCLI").val() == data[i].CODGRPCLI) {
                            obj1.prop("checked", true);
                        }
                    });
                }
                LoadingOff();
                atualizaMensagem("msgmodalListGrupamentoCliente");
            },
            function (error) {
                atualizaMensagem("msgmodalListGrupamentoCliente");
            }
        )
    }
}

function CarregarBuSelecionadoAtividade() {
    var lstBUAtividade = $(".chkBUAtividade");
    var lstBUAtividadeSelecionados = [];

    lstBUAtividade.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODUNDESRNGC: parseInt(check.val())
            }
            lstBUAtividadeSelecionados.push(obj);
        }
    });


    if (lstBUAtividadeSelecionados.length <= 0) {
        mostraMensagem("Favor%20escolher%20um%20BU!", "msgInfo", "msgmodalListAtividade");
        LoadingOff();
    }
    else {

        postAjax("RelacaoAcaoMecanicaPublicoAlvoAtividade", "BuscarAtividadePorBU", { obj: lstBUAtividadeSelecionados },
            function (data) {
                var lstAtividade = $(".hdfAtividade");

                //Desmarcar todos
                lstAtividade.each(function (element) {
                    var obj2 = $(this);
                    obj2.parent().children(".marcaAtividade").prop("checked", false);
                });

                for (var i = 0; i < data.length; i++) {
                    lstAtividade.each(function (element) {
                        var obj1 = $(this);
                        if (parseInt(obj1.parent().parent().children(".CodAti").text().trim()) === data[i].CODATI) {
                            obj1.parent().children(".marcaAtividade").prop("checked", true);
                        }
                    });
                }
                LoadingOff();
            },
            function (error) {
                mostraMensagem("Erro%20ao%20buscar%20por%20BU!", "msgDanger", "msgmodalListAtividade");
            }
        )
    }
}

function changeParticipacaoMixTotal(INDPTCTOTMIXPMC) {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    TIPPMC = $('#frmWidgetMecanicaPromocaoGerais select[name="TIPPMC"]').val();

    getAjax("RelacaoAcaoMecanica", "EditMecanicaPromocaoWidgetMix/" + CODACOCMC + '/' + NUMSEQRGRACOCMC + '/' + TIPPMC + '/' + INDPTCTOTMIXPMC, function (data) {
        $('#WidgetMecanicaPromocaoMix').html(data);
        LoadingOff();
    })
}

//function changeParticipacaoMixTotalNao(e) {
//    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
//    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
//    INDPTCTOTMIXPMC = e;

//    getAjax("RelacaoAcaoMecanica", "EditMecanicaPromocaoWidgetMix/" + CODACOCMC + '/' + NUMSEQRGRACOCMC + '/' + INDPTCTOTMIXPMC, function (data) {
//        $('#WidgetMecanicaPromocaoMix').html(data);
//        LoadingOff();
//    })
//}

//Função Aceitar somente numero TextBox
function somenteNumeros(num) {
    var er = /[^0-9]/;
    er.lastIndex = 0;
    var campo = num;
    if (er.test(campo.value)) {
        campo.value = "";
    }
}

function widgetMix(CODACOCMC, NUMSEQRGRACOCMC) {
    CODACOCMC = CODACOCMC;
    NUMSEQRGRACOCMC = NUMSEQRGRACOCMC;
    INDPTCTOTMIXPMC = $('#frmWidgetMecanicaPromocaoGerais #divrdbPartMixTotal [checked="checked"]').val();
    TIPPMC = $('#frmWidgetMecanicaPromocaoGerais select[name="TIPPMC"]').val();


    getAjax("RelacaoAcaoMecanica", "EditMecanicaPromocaoWidgetMix/" + CODACOCMC + '/' + NUMSEQRGRACOCMC + '/' + TIPPMC + '/' + INDPTCTOTMIXPMC, function (data) {
        $('#WidgetMecanicaPromocaoMix').html(data);
    })
}

function widgetPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC) {
    CODACOCMC = CODACOCMC;
    NUMSEQRGRACOCMC = NUMSEQRGRACOCMC;

    getAjax("RelacaoAcaoMecanica", "EditMecanicaPromocaoWidgetPublicoAlvo/" + CODACOCMC + '/' + NUMSEQRGRACOCMC, function (data) {
        $('#WidgetMecanicaPromocaoPublicoAlvo').html(data);
    })
}

function widgetClientesExcludentes(CODACOCMC, NUMSEQRGRACOCMC) {
    CODACOCMC = CODACOCMC;
    NUMSEQRGRACOCMC = NUMSEQRGRACOCMC;

    getAjax("RelacaoAcaoMecanica", "EditMecanicaPromocaoWidgetClientesExcludentes/" + CODACOCMC + '/' + NUMSEQRGRACOCMC, function (data) {
        $('#WidgetMecanicaPromocaoClientesExcludentes').html(data);
    })
}

function widgetFiliais(CODACOCMC, NUMSEQRGRACOCMC) {
    CODACOCMC = CODACOCMC;
    NUMSEQRGRACOCMC = NUMSEQRGRACOCMC;

    getAjax("RelacaoAcaoMecanica", "EditMecanicaPromocaoWidgetFiliais/" + CODACOCMC + '/' + NUMSEQRGRACOCMC, function (data) {
        $('#WidgetMecanicaPromocaoFiliais').html(data);
    })
}

function abrirModalEstadoUniao(id) {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaPublicoAlvoEstado", "EditMecanicaPromocaoPublicoAlvoEstadoUniao", data, "#msgGeral", "", modalT);
}

function abrirModalAtividade(id) {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaPublicoAlvoAtividade", "EditMecanicaPromocaoPublicoAlvoAtividade", data, "#msgGeral", "", modalT);
}

function abrirModalGrupamentoCliente(id) {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente", "EditMecanicaPromocaoPublicoAlvoGrupamentoCliente", data, "#modalMecanicaPromocaoSubModal", "", modalT);
}

function abrirModalNegocioCliente(id) {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaPublicoAlvoNegocioCliente", "EditMecanicaPromocaoPublicoAlvoNegocioCliente", data, "#msgGeral", "", modalT);
}

function abrirModalSegmentoCliente(id) {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente", "EditMecanicaPromocaoPublicoAlvoSegmentoCliente", data, "#msgGeral", "", modalT);
}

function abrirModalCliente(id) {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaPublicoAlvoCliente"
        , "EditMecanicaPromocaoPublicoAlvoCliente"
        , data
        , "#modalMecanicaPromocaoSubModal"
        , ""
        , modalT
        , EditMecanicaPromocaoWidgetLoadPesquisaCliente
        , abrirWidgetPublicoAlvoCliente
    );

}

function abrirModalClientesExcludentes(id) {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }
    //abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaClientesExcludentes", "EditMecanicaPromocaoClientesExcludentes", data, "#msgGeral", "", modalT);
    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaClientesExcludentes"
        , "EditMecanicaPromocaoClientesExcludentes"
        , data
        , "#msgGeral"
        , ""
        , modalT
        , EditMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes
        , abrirWidgetClientesExcludentes
    );

}

function EditMecanicaPromocaoWidgetLoadPesquisaCliente() {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    getAjax("RelacaoAcaoMecanicaPublicoAlvoCliente"
        , "EditMecanicaPromocaoWidgetLoadPesquisaCliente/" + CODACOCMC + '/' + NUMSEQRGRACOCMC
        , function (data) {
            $('#divLoadPesquisaCliente').html(data);
            LoadingOff();
            atualizaMensagem("msgModalCliente");
        })
}

function EditMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes() {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    getAjax("RelacaoAcaoMecanicaClientesExcludentes"
        , "EditMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes/" + CODACOCMC + '/' + NUMSEQRGRACOCMC
        , function (data) {
            $('#divLoadPesquisaClientesExcludentes').html(data);
            LoadingOff();
            atualizaMensagem("msgModalClientesExcludentes");
        })
}

function publicoAlvoClienteAjaxComplete() {
    $('#loadingDialog').modal('hide');
    atualizaMensagem("msgModalCliente");
    ChosenConfig();
    ChosenResize();
    MaskConfig();
    PopoverConfig();
    WidgetConfig();
}


function abrirWidgetPublicoAlvoCliente() {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    postAjax("RelacaoAcaoMecanicaPublicoAlvoCliente", "ListarPublicoAlvoCliente", obj, function (data) {
        $('#divLoadPublicoAlvoCliente').html(data);
        LoadingOff();
    });
}

function abrirWidgetClientesExcludentes() {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    postAjax("RelacaoAcaoMecanicaClientesExcludentes", "ListarClientesExcludentes", obj, function (data) {
        $('#divLoadClientesExcludentes').html(data);
        LoadingOff();
    });
}

function abrirModalRcaVendedor(id) {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaPublicoAlvoRcaVendedor"
        , "EditMecanicaPromocaoPublicoAlvoRcaVendedor"
        , data
        , "#modalMecanicaPromocaoSubModal"
        , ""
        , modalT
        , EditMecanicaPromocaoWidgetLoadPesquisaRcaVendedor
        , abrirWidgetPublicoAlvoRcaVendedor
    );

}

function EditMecanicaPromocaoWidgetLoadPesquisaRcaVendedor() {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    getAjax("RelacaoAcaoMecanicaPublicoAlvoRcaVendedor"
        , "EditMecanicaPromocaoWidgetLoadPesquisaRcaVendedor/" + CODACOCMC + '/' + NUMSEQRGRACOCMC
        , function (data) {
            $('#divLoadPesquisaRcaVendedor').html(data);
            LoadingOff();
        })
}

function abrirModalGerenteVendas(id) {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaPublicoAlvoGerenteVendas", "EditMecanicaPromocaoPublicoAlvoGerenteVendas/" + CODACOCMC + '/' + NUMSEQRGRACOCMC, data, "#msgGeral", "", modalT);
}

function abrirWidgetPublicoAlvoRcaVendedor() {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    postAjax("RelacaoAcaoMecanicaPublicoAlvoRcaVendedor", "ListarPublicoAlvoRcaVendedor", obj, function (data) {
        $('#divLoadPublicoAlvoRcaVendedor').html(data);
        LoadingOff();
    });
}

function abrirModalGerenteMercado(id) {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaPublicoAlvoGerenteMercado"
        , "EditMecanicaPromocaoPublicoAlvoGerenteMercado"
        , data
        , "#modalMecanicaPromocaoSubModal"
        , ""
        , modalT
        , EditMecanicaPromocaoWidgetLoadPesquisaGerenteMercado
        , abrirWidgetPublicoAlvoGerenteMercado
    );
}

function EditMecanicaPromocaoWidgetLoadPesquisaGerenteMercado() {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    getAjax("RelacaoAcaoMecanicaPublicoAlvoGerenteMercado"
        , "EditMecanicaPromocaoWidgetLoadPesquisaGerenteMercado/" + CODACOCMC + '/' + NUMSEQRGRACOCMC
        , function (data) {
            $('#divLoadPesquisaGerenteMercado').html(data);
            LoadingOff();
        })
}

function abrirWidgetPublicoAlvoGerenteMercado() {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    postAjax("RelacaoAcaoMecanicaPublicoAlvoGerenteMercado", "ListarPublicoAlvoGerenteMercado", obj, function (data) {
        $('#divLoadPublicoAlvoGerenteMercado').html(data);
        LoadingOff();
    });
}

function abrirModalMixFamiliaMercadoria(modalT) {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaMix"
        , "EditMecanicaPromocaoMixFamiliaMercadoria"
        , data
        , "#modalMecanicaPromocaoSubModal"
        , ""
        , modalT
        , CarregarWidgetLoadPesquisaMixFamiliaMercadoria
        , abrirWidgetMixFamiliaMercadoriaAdicionados
    );

}

function CarregarWidgetLoadPesquisaMixFamiliaMercadoria() {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    getAjax("RelacaoAcaoMecanicaMix"
        , "CarregarWidgetLoadPesquisaMixFamiliaMercadoria/" + CODACOCMC + '/' + NUMSEQRGRACOCMC
        , function (data) {
            $('#divLoadPesquisaMixFamiliaMercadoria').html(data);
            LoadingOff();
        })
}

//Abrir Modal de Configuração Mix Classe Mercadoria
function abrirModalMixClasseMercadoria(modalT) {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaMix"
        , "EditMecanicaPromocaoMixClasseMercadoria"
        , data
        , "#modalMecanicaPromocaoSubModal"
        , ""
        , modalT
        , CarregarWidgetLoadPesquisaMixClasseMercadoria
        , abrirWidgetMixClasseMercadoriaAdicionados
    );

}

function CarregarWidgetLoadPesquisaMixClasseMercadoria() {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    getAjax("RelacaoAcaoMecanicaMix"
        , "CarregarWidgetLoadPesquisaMixClasseMercadoria/" + CODACOCMC + '/' + NUMSEQRGRACOCMC
        , function (data) {
            $('#divLoadPesquisaMixClasseMercadoria').html(data);
            LoadingOff();
        })
}
//Abrir Modal de Configuração Mix Mercadoria
function abrirModalMixMercadoria(id) {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaMix"
        , "EditMecanicaPromocaoMixMercadoria"
        , data
        , "#modalMecanicaPromocaoSubModal"
        , ""
        , modalT
        , CarregarWidgetLoadPesquisaMixMercadoria
        , abrirWidgetMixMercadoriaAdicionados
    );
}

function CarregarWidgetLoadPesquisaMixMercadoria() {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    let TIPPMC = $('#frmWidgetMecanicaPromocaoGerais select[name="TIPPMC"]').val();

    let QTDFXAGRP = "";
    if (TIPPMC == 1)
        QTDFXAGRP = $('#frmWidgetMecanicaPromocaoGerais input[type="text"][name="QDEGRPMIXPMC"]').val();
    else
        QTDFXAGRP = $('#frmWidgetMecanicaPromocaoGerais input[type="text"][name="QDEFXAPMC"]').val();

    getAjax("RelacaoAcaoMecanicaMix"
        , "CarregarWidgetLoadPesquisaMixMercadoria/" + CODACOCMC + '/' + NUMSEQRGRACOCMC + '/' + TIPPMC + '/' + QTDFXAGRP
        , function (data) {
            $('#divLoadPesquisaMixMercadoria').html(data);
            LoadingOff();
        })
}

function MixMercadoriaLoadingOff_UpdateMessage() {
    $('#loadingDialog').modal('hide');
    atualizaMensagem("msgModalMecanicaMixMercadoria");
    ChosenConfig();
    ChosenResize();
    MaskConfig();
    PopoverConfig();
    WidgetConfig();
}



//Abrir Modal de Configuração Mix Fornecedor
function abrirModalMixFornecedor(id) {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaMix"
        , "EditMecanicaPromocaoMixFornecedor"
        , data
        , "#modalMecanicaPromocaoSubModal"
        , ""
        , id
        , CarregarWidgetLoadPesquisaMixFornecedor
        , abrirWidgetMixFornecedorAdicionados
    );

}

function CarregarWidgetLoadPesquisaMixFornecedor() {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    getAjax("RelacaoAcaoMecanicaMix"
        , "CarregarWidgetLoadPesquisaMixFornecedor/" + CODACOCMC + '/' + NUMSEQRGRACOCMC
        , function (data) {
            $('#divLoadPesquisaMixFornecedor').html(data);
            LoadingOff();
        })
}

function abrirWidgetMixFamiliaMercadoriaAdicionados() {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    postAjax("RelacaoAcaoMecanicaMix", "CarregarWidgetMixFamiliaMercadoriaAdicionados", obj, function (data) {
        $('#divMixFamiliaMercadoriaAdicionados').html(data);
        LoadingOff();
    });
}

//Abrir Widget com itens adicionados
function abrirWidgetMixClasseMercadoriaAdicionados() {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    postAjax("RelacaoAcaoMecanicaMix", "CarregarWidgetMixClasseMercadoriaAdicionados", obj, function (data) {
        $('#divMixClasseMercadoriaAdicionados').html(data);
        LoadingOff();
    });
}

//Abrir Widget com itens adicionados
function abrirWidgetMixMercadoriaAdicionados() {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    postAjax("RelacaoAcaoMecanicaMix", "CarregarWidgetMixMercadoriaAdicionados", obj, function (data) {
        $('#divMixMercadoriaAdicionados').html(data);
        LoadingOff();
    });
}

//Abrir Widget com itens adicionados
function abrirWidgetMixFornecedorAdicionados() {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    postAjax("RelacaoAcaoMecanicaMix", "CarregarWidgetMixFornecedorAdicionados", obj, function (data) {
        $('#divMixFornecedorAdicionados').html(data);
        LoadingOff();
    });
}

function abrirModalMixGrupoMercadoria(id) {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("RelacaoAcaoMecanicaMix", "EditMecanicaPromocaoMixGrupoMercadoria", data, "#msgGeral", "", modalT);
}

function abrirWidgetMixGrupoMercadoria() {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    postAjax("RelacaoAcaoMecanicaPublicoAlvoGerenteMercado", "ListarPublicoAlvoGerenteMercado", obj, function (data) {
        $('#divLoadPublicoAlvoGerenteMercado').html(data);
        LoadingOff();
    });
}

/*
Abrir modal chamando uma acao e passando um objeto sem loadinf
*/
function abrirModalSemLoadingRelacaoAcaoMecanica(controller, action, obj, div, divMsg) {
    if (div === undefined) {
        div = "#modal"
    }
    $.ajax({
        url: SiteUrl + controller + "/" + action,
        type: 'POST',
        data: obj,
        success: function (data) {
            if (data !== "") {
                $(div).html(data);
                $('#modalEditMecanicaPromocao').parents('.modal').modal('hide');
                $('#modalListEstadoUniao').modal().on('hidden.bs.modal', function () {
                    $('#modalEditMecanicaPromocao').parents('.modal').modal('show');
                    resizeModal($('#modalEditMecanicaPromocao').parents('.modal'));

                });
                resizeModal($('#modalListEstadoUniao'));
                atualizaMensagem(divMsg);
            }
            ChosenConfig();
            ChosenResize();
            MaskConfig();
            PopoverConfig();
        },
        error: function () {
            ChosenConfig();
            ChosenResize();
            MaskConfig();
            PopoverConfig();
            atualizaMensagem(divMsg);
        }
    });
}

/*
Abrir modal chamando uma acao e passando um objeto sem loadinf
*/
function abrirModalSemLoadingPublicoAlvoConfiguracoes(controller, action, obj, div, divMsg, modalT, successFunction1, successFunction2) {
    if (div === undefined) {
        div = "#modal"
    }
    $.ajax({
        url: SiteUrl + controller + "/" + action,
        type: 'POST',
        data: obj,
        success: function (data) {
            if (data !== "") {
                $(div).html(data);
                $('#modalEditMecanicaPromocao').parents('.modal').modal('hide');
                if (modalT !== undefined) {
                    var $modalT = $(modalT);
                    $modalT.modal().on('hidden.bs.modal', function () {
                        $('#modalEditMecanicaPromocao').parents('.modal').modal('show');
                    });
                    $modalT.on('shown.bs.modal', function () {
                        ChosenResize();
                    });
                    resizeModal($modalT);
                }
            } else {
                atualizaMensagem(divMsg);
            }

            ChosenConfig();
            ChosenResize();
            MaskConfig();
            PopoverConfig();

            if (successFunction1 && (typeof successFunction1 == "function")) {
                successFunction1()
            }
            if (successFunction2 && (typeof successFunction2 == "function")) {
                successFunction2()
            }
        },
        error: function () {
            ChosenConfig();
            ChosenResize();
            MaskConfig();
            PopoverConfig();
            atualizaMensagem(divMsg);
        }
    });
}


function fecharModalEstadoUni() {
    $('#modalListEstadoUniao').modal('hide');
}

function fecharModalAtividade() {
    $('#modalListAtividade').modal('hide');
}

function fecharModalGrupamentoCliente() {
    $('#modalListGrupamentoCliente').modal('hide');
}

function fecharModalNegocioCliente() {
    $('#modalListNegocioCliente').modal('hide');
}

function fecharModalSegmentoCliente() {
    $('#modalListSegmentoCliente').modal('hide');
}

function fecharModalGerenteVendas() {
    $('#modalListGerenteVendas').modal('hide');
}

function fecharModalCliente() {
    $('#modalListCliente').modal('hide');
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    widgetPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC);
    LoadingOff();
}

function fecharModalClientesExcludentes() {
    $('#modalListClientesExcludentes').modal('hide');
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    widgetClientesExcludentes(CODACOCMC, NUMSEQRGRACOCMC);
    LoadingOff();
}

function fecharModalRcaVendedor() {
    $('#modalListRcaVendedor').modal('hide');
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    widgetPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC);
    LoadingOff();
}

function fecharModalGerenteMercado() {
    $('#modalListGerenteMercado').modal('hide');
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    widgetPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC);
    LoadingOff();
}

function fecharModalSelecaoCliente() {
    $('#modalListSelecaoCliente').modal('hide');
    //No lugar do .modal('show') vai ter que fazer um get na modalListCliente
    $('#modalListCliente').modal('show');
}

function fecharModalMixGrupoMercadoria() {
    $('#modalListMixGrupoMercadoria').modal('hide');
    widgetMix(CODACOCMC, NUMSEQRGRACOCMC);
    LoadingOff();
}

function fecharModalMixFamiliaMercadoria() {
    $('#modalMecanicaMixFamiliaMercadoria').modal('hide');
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    widgetMix(CODACOCMC, NUMSEQRGRACOCMC);
    LoadingOff();
}

function fecharModalMixClasseMercadoria() {
    $('#modalMecanicaMixClasseMercadoria').modal('hide');
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    widgetMix(CODACOCMC, NUMSEQRGRACOCMC);
    LoadingOff();
}

function fecharModalMixMercadoria() {
    $('#modalMecanicaMixMercadoria').modal('hide');
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    widgetMix(CODACOCMC, NUMSEQRGRACOCMC);
    LoadingOff();
}

function fecharModalMixFornecedor() {
    $('#modalMecanicaMixFornecedor').modal('hide');
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    widgetMix(CODACOCMC, NUMSEQRGRACOCMC);
    LoadingOff();
}

//Salvar estados selecionados
function salvarDadosGridEstadoUniao() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var lstEstadoUniaoChecked = [];

    var lstEstadoUniao = $("#tblModalEstadoUniao tbody input[name='SelecioneEstadoUniao'][type='checkbox']");


    lstEstadoUniao.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODESTUNI: check.parent().parent().find("label").eq(0).text(),
            }
            lstEstadoUniaoChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoEstado" + "/" + "Save/" + CODACOCMC + '/' + NUMSEQRGRACOCMC,
        type: 'POST',
        data: { obj: lstEstadoUniaoChecked },
        success: function (data) {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
            $('#modalListEstadoUniao').modal('hide');
            widgetPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC);
            //atualizaMensagem("msgModalPublicoAlvo");
            mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalPublicoAlvo");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgmodalListEstadoUniao");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//Salvar Atividades selecionados
function salvarDadosGridAtividade() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var lstAtividadeChecked = [];

    var lstAtividade = $("#tblModalAtividade tbody input[name='SelecioneAtividade'][type='checkbox']");


    lstAtividade.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODATI: check.parent().parent().find("label").eq(0).text()
            }
            lstAtividadeChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoAtividade" + "/" + "Save/" + CODACOCMC + '/' + NUMSEQRGRACOCMC,
        type: 'POST',
        data: { obj: lstAtividadeChecked },
        success: function (data) {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
            $('#modalListAtividade').modal('hide');
            widgetPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC);
            mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalEditMecanicaPromocao");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgmodalListAtividade");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//Salvar Grupamento Cliente selecionados
function salvarDadosGridGrupamentoCliente() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var lstGrupamentoClienteChecked = [];

    var lstGrupamentoCliente = $("#tblModalGrupamentoCliente tbody input[name='SelecioneGrupamentoCliente'][type='checkbox']:checked");


    lstGrupamentoCliente.each(function (element) {
        var check = $(this);
        var obj = {
            CODGRPCLI: check.parent().children("input[type='hidden'].CODGRPCLI").val()
        }
        lstGrupamentoClienteChecked.push(obj);
    }).get();


    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente" + "/" + "Save/" + CODACOCMC + '/' + NUMSEQRGRACOCMC,
        type: 'POST',
        data: { obj: lstGrupamentoClienteChecked },
        success: function (data) {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
            $('#modalListGrupamentoCliente').modal('hide');
            widgetPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC);

            //mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalEditMecanicaPromocao");
            atualizaMensagem("msgModalEditMecanicaPromocao");
        },
        error: function (error) {
            atualizaMensagem("msgModalEditMecanicaPromocao");
            //mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgmodalListGrupamentoCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//Salvar Negocio Cliente selecionados
function salvarDadosGridNegocioCliente() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var lstNegocioClienteChecked = [];

    var lstNegocioCliente = $("#tblModalNegocioCliente tbody input[name='SelecioneNegocioCliente'][type='checkbox']");


    lstNegocioCliente.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODUNDESRNGC: check.parent().parent().find("label").eq(0).text()
            }
            lstNegocioClienteChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoNegocioCliente" + "/" + "Save/" + CODACOCMC + '/' + NUMSEQRGRACOCMC,
        type: 'POST',
        data: { obj: lstNegocioClienteChecked },
        success: function (data) {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
            $('#modalListNegocioCliente').modal('hide');
            widgetPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC);
            mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalEditMecanicaPromocao");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgModalNegocioCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//Salvar Segmento Cliente selecionados
function salvarDadosGridSegmentoCliente() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var lstSegmentoClienteChecked = [];

    var lstSegmentoCliente = $("#tblModalSegmentoCliente tbody input[name='SelecioneSegmentoCliente'][type='checkbox']");


    lstSegmentoCliente.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODSGMNGCCLI: check.parent().parent().find("label").eq(0).text()
            }
            lstSegmentoClienteChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente" + "/" + "Save/" + CODACOCMC + '/' + NUMSEQRGRACOCMC,
        type: 'POST',
        data: { obj: lstSegmentoClienteChecked },
        success: function (data) {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
            $('#modalListSegmentoCliente').modal('hide');
            widgetPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC);
            mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalEditMecanicaPromocao");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgModalSegmentoCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//Salvar Gerente Vendas selecionados
function salvarDadosGridGerenteVendas() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var lstGerenteVendasChecked = [];

    var lstGerenteVendas = $("#tblModalGerenteVendas tbody input[name='SelecioneGerenteVendas'][type='checkbox']");


    lstGerenteVendas.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODGER: check.parent().parent().find("label").eq(0).text()
            }
            lstGerenteVendasChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoGerenteVendas" + "/" + "Save/" + CODACOCMC + '/' + NUMSEQRGRACOCMC,
        type: 'POST',
        data: { obj: lstGerenteVendasChecked },
        success: function (data) {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
            $('#modalListGerenteVendas').modal('hide');
            widgetPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC);
            mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalEditMecanicaPromocao");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgModalGerenteVendas");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function salvarPublicoAlvoCliente(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODCLI = e.parentElement.parentElement.children.item(1).innerText;
    var NOMCLI = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODCLI: CODCLI,
        NOMCLI: NOMCLI
    }

    //postAjax("RelacaoAcaoMecanicaPublicoAlvoCliente", "Save", obj);

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoCliente" + "/" + "Save",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPublicoAlvoCliente();
            atualizaMensagem("msgModalCliente");
            //mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalCliente");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            //mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "modalListCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function salvarClientesExcludentes(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODCLI = e.parentElement.parentElement.children.item(1).innerText;
    var NOMCLI = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODCLI: CODCLI,
        NOMCLI: NOMCLI
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaClientesExcludentes" + "/" + "Save",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetClientesExcludentes();
            atualizaMensagem("msgModalClientesExcludentes");
            //mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalCliente");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            //mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "modalListCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function addPesquisaClientesExcludentes(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaClientesExcludentes" + "/" + "AddPesquisa",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetClientesExcludentes();
            $('#frmPesquisaClientesExcludentes').submit();
            atualizaMensagem("msgModalClientesExcludentes");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function addPesquisaPublicoAlvoCliente(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoCliente" + "/" + "AddPesquisa",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPublicoAlvoCliente();
            $('#frmPesquisaPublicoAlvoCliente').submit();
            atualizaMensagem("msgModalCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}


function salvarPublicoAlvoRcaVendedor(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODREP = e.parentElement.parentElement.children.item(1).innerText;
    var NOMREP = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODREP: CODREP,
        NOMREP: NOMREP
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoRcaVendedor" + "/" + "Save",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPublicoAlvoRcaVendedor();
            atualizaMensagem("msgModalRcaVendedor");
            //mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalRcaVendedor");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            //mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "modalListRcaVendedor");
        },
        complete: function () {
            LoadingOff();
        }
    });
}
function addPesquisaPublicoAlvoRcaVendedor(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoRcaVendedor" + "/" + "AddPesquisa",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPublicoAlvoRcaVendedor();
            $('#frmPesquisaPublicoAlvoRcaVendedor').submit();
            atualizaMensagem("msgModalRcaVendedor");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function salvarPublicoAlvoGerenteMercado(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODSUP = e.parentElement.parentElement.children.item(1).innerText;
    var NOMSUP = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODSUP: CODSUP,
        NOMSUP: NOMSUP
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoGerenteMercado" + "/" + "Save",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPublicoAlvoGerenteMercado();
            atualizaMensagem("msgModalGerenteMercado");
            //mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalGerenteMercado");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            //mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "modalListGerenteMercado");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function addPesquisaPublicoAlvoGerenteMercado(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoGerenteMercado" + "/" + "AddPesquisa",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetPublicoAlvoGerenteMercado();
            $('#frmPesquisaPublicoAlvoGerenteMercado').submit();
            atualizaMensagem("msgModalGerenteMercado");
        },
        complete: function () {
            LoadingOff();
        }
    });
}


//Salvar Dados Mix Grupo Mercadoria
function salvarDadosMixGrupoMercadoria() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var lstGrupoMercadoriaChecked = [];

    var lstGrupoMercadoria = $("#tblModalGrupoMercadoria tbody input[name='SelecioneGrupoMercadoria'][type='checkbox']");


    lstGrupoMercadoria.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {
            var obj = {
                CODGRPMER: check.parent().parent().find("label").eq(0).text(),
            }
            lstGrupoMercadoriaChecked.push(obj);
        }
    }).get();

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "SaveMixGrupoMercadoria/" + CODACOCMC + '/' + NUMSEQRGRACOCMC,
        type: 'POST',
        data: { obj: lstGrupoMercadoriaChecked },
        success: function (data) {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
            $('#modalListMixGrupoMercadoria').modal('hide');
            widgetMix(CODACOCMC, NUMSEQRGRACOCMC);
            //mostraMensagem("Alterações%20realizadas%20com%20sucesso!", "msgSuccess", "msgModalMix");
            atualizaMensagem("msgModalMix");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20salvar%20configurações!", "msgDanger", "msgmodalListMixGrupoMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function salvarMixFamiliaMercadoria(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODGRPMER = e.parentElement.parentElement.children.item(0).value;
    var CODFMLMER = e.parentElement.parentElement.children.item(2).innerText;


    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODFMLMER: CODFMLMER,
        CODGRPMER: CODGRPMER
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "SaveMixFamiliaMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetMixFamiliaMercadoriaAdicionados();
            atualizaMensagem("msgModalMecanicaMixFamiliaMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
        },
        complete: function () {
            LoadingOff();
        }
    });
}
function addPesquisaMixFamiliaMercadoria(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "AddPesquisaMixFamiliaMercadoria",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetMixFamiliaMercadoriaAdicionados();
            $('#frmPesquisaRelacaoAcaoMecanicaMix').submit();
            atualizaMensagem("msgModalMecanicaMixFamiliaMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}


//Salvar dados Ação/Mecanica/ Mix Classe Mercadoria
function salvarMixClasseMercadoria(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODFMLMER = e.parentElement.parentElement.children.item(1).value;
    var CODGRPMER = e.parentElement.parentElement.children.item(0).value;
    var CODCLSMER = e.parentElement.parentElement.children.item(3).innerText;


    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODCLSMER: CODCLSMER,
        CODGRPMER: CODGRPMER,
        CODFMLMER: CODFMLMER
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "SaveMixClasseMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetMixClasseMercadoriaAdicionados();
            atualizaMensagem("msgModalMecanicaMixClasseMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}

function addPesquisaMixClasseMercadoria(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "AddPesquisaMixClasseMercadoria",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetMixClasseMercadoriaAdicionados();
            $('#frmPesquisaMixClasseMercadoria').submit();
            atualizaMensagem("msgModalMecanicaMixClasseMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}


//Salvar dados Ação/Mecanica/ Mix Mercadoria
function salvarMixMercadoria(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODMER = e.parentElement.parentElement.children.item(2).innerText;
    var CODGRPMIXMERPMC = $('#drpAddRelacaoAcaoMecanicaMixFaixa').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODMER: CODMER,
        CODGRPMIXMERPMC: CODGRPMIXMERPMC
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "SaveMixMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetMixMercadoriaAdicionados();
            atualizaMensagem("msgModalMecanicaMixMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}
function addPesquisaMixMercadoria(data) {
    if (data) {
        data.CODGRPMIXMERPMC = $('#drpAddRelacaoAcaoMecanicaMixFaixa').val()
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "AddPesquisaMixMercadoria",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetMixMercadoriaAdicionados();
            $('#frmPesquisaMixMercadoria').submit();
            atualizaMensagem("msgModalMecanicaMixMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

//Salvar dados Ação/Mecanica/ Mix Fornecedor
function salvarMixFornecedor(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODFRN = e.parentElement.parentElement.children.item(1).innerText;


    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODFRN: CODFRN
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "SaveMixFornecedor",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetMixFornecedorAdicionados();
            atualizaMensagem("msgModalMecanicaMixFornecedor");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}

function addPesquisaMixFornecedor(data) {
    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "AddPesquisaMixFornecedor",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetMixFornecedorAdicionados();
            $('#frmPesquisaMixFornecedor').submit();
            atualizaMensagem("msgModalMecanicaMixFornecedor");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeletePublicoAlvoCliente(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODCLI = e.parentElement.parentElement.children.item(1).innerText;
    var NOMCLI = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODCLI: CODCLI,
        NOMCLI: NOMCLI
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoCliente" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPublicoAlvoCliente();
            mostraMensagem("", "", "msgModalCliente");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalCliente");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllPublicoAlvoCliente() {


    ShowModalConfirmar({
        titulo: 'Confirmar Exclusão',
        mensagem: "Confirme a exclusão de todos os clientes desta mecanica",
        appendElement: "msgModalCliente",
        callback: function () {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

            var obj = {
                CODACOCMC: CODACOCMC,
                NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
            }

            LoadingOn();
            $.ajax({
                url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoCliente" + "/" + "Delete",
                type: 'POST',
                data: obj,
                success: function (data) {
                    abrirWidgetPublicoAlvoCliente();
                },
                error: function (error) {
                    mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalCliente");
                },
                complete: function () {
                    LoadingOff();
                }
            });
        },
        parametros: null
    });
}

function DeleteClientesExcludentes(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODCLI = e.parentElement.parentElement.children.item(1).innerText;
    var NOMCLI = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODCLI: CODCLI,
        NOMCLI: NOMCLI
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaClientesExcludentes" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetClientesExcludentes();
            mostraMensagem("", "", "msgModalClientesExcludentes");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalClientesExcludentes");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllClientesExcludentes() {


    ShowModalConfirmar({
        titulo: 'Confirmar Exclusão',
        mensagem: "Confirme a exclusão de todos os clientes excludentes desta mecanica",
        appendElement: "msgModalClientesExcludentes",
        callback: function () {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

            var obj = {
                CODACOCMC: CODACOCMC,
                NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
            }

            LoadingOn();
            $.ajax({
                url: SiteUrl + "RelacaoAcaoMecanicaClientesExcludentes" + "/" + "Delete",
                type: 'POST',
                data: obj,
                success: function (data) {
                    abrirWidgetClientesExcludentes();
                },
                error: function (error) {
                    mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalClientesExcludentes");
                },
                complete: function () {
                    LoadingOff();
                }
            });
        },
        parametros: null
    });
}

function DeletePublicoAlvoRcaVendedor(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODREP = e.parentElement.parentElement.children.item(1).innerText;
    var NOMREP = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODREP: CODREP,
        NOMREP: NOMREP
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoRcaVendedor" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPublicoAlvoRcaVendedor();
            mostraMensagem("", "", "msgModalRcaVendedor");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalRcaVendedor");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllPublicoAlvoRcaVendedor() {


    ShowModalConfirmar({
        titulo: 'Confirmar Exclusão',
        mensagem: "Confirme a exclusão de todos os RCA/Vendedores",
        appendElement: "msgModalRcaVendedor",
        callback: function () {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

            var obj = {
                CODACOCMC: CODACOCMC,
                NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
            }

            LoadingOn();
            $.ajax({
                url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoRcaVendedor" + "/" + "Delete",
                type: 'POST',
                data: obj,
                success: function (data) {
                    abrirWidgetPublicoAlvoRcaVendedor();
                },
                error: function (error) {
                    mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalRcaVendedor");
                },
                complete: function () {
                    LoadingOff();
                }
            });
        },
        parametros: null
    });
}

function DeletePublicoAlvoGerenteMercado(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODSUP = e.parentElement.parentElement.children.item(1).innerText;
    var NOMSUP = e.parentElement.parentElement.children.item(2).innerText;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODSUP: CODSUP,
        NOMSUP: NOMSUP
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoGerenteMercado" + "/" + "Delete",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetPublicoAlvoGerenteMercado();
            mostraMensagem("", "", "msgModalGerenteMercado");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalGerenteMercado");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllPublicoAlvoGerenteMercado() {
    ShowModalConfirmar({
        titulo: 'Confirmar Exclusão',
        mensagem: "Confirme a exclusão de todos os gerentes de mercado",
        appendElement: "msgModalGerenteMercado",
        callback: function () {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

            var obj = {
                CODACOCMC: CODACOCMC,
                NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
            }

            LoadingOn();
            $.ajax({
                url: SiteUrl + "RelacaoAcaoMecanicaPublicoAlvoGerenteMercado" + "/" + "Delete",
                type: 'POST',
                data: obj,
                success: function (data) {
                    abrirWidgetPublicoAlvoGerenteMercado();
                },
                error: function (error) {
                    mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalGerenteMercado");
                },
                complete: function () {
                    LoadingOff();
                }
            });
        },
        parametros: null
    });
}

function DeleteMixFamiliaMercadoria(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODFMLMER = e.parentElement.parentElement.children.item('3').innerText;
    var CODGRPMER = e.parentElement.parentElement.children.item('1').innerText;
    var NUMSEQMIXACOCMC = e.parentElement.parentElement.children.item('0').value;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODFMLMER: CODFMLMER,
        CODGRPMER: CODGRPMER,
        NUMSEQMIXACOCMC: NUMSEQMIXACOCMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "DeleteMixFamiliaMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetMixFamiliaMercadoriaAdicionados();
            mostraMensagem("", "", "msgModalMecanicaMixFamiliaMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixFamiliaMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function DeleteAllMixFamiliaMercadoria() {
    ShowModalConfirmar({
        titulo: 'Confirmar Exclusão',
        mensagem: "Confirme a exclusão do todos os itens",
        appendElement: "msgModalMecanicaMixFamiliaMercadoria",
        callback: function () {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

            var obj = {
                CODACOCMC: CODACOCMC,
                NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
            }

            LoadingOn();
            $.ajax({
                url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "DeleteMixFamiliaMercadoria",
                type: 'POST',
                data: obj,
                success: function (data) {
                    abrirWidgetMixFamiliaMercadoriaAdicionados();
                },
                error: function (error) {
                    mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixFamiliaMercadoria");
                },
                complete: function () {
                    LoadingOff();
                }
            });
        },
        parametros: null
    });
}

//Excluir Itens Mix Classe Mercadoria
function DeleteMixClasseMercadoria(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODCLSMER = e.parentElement.parentElement.children.item('3').innerText;
    var CODGRPMER = e.parentElement.parentElement.children.item('1').innerText;
    var NUMSEQMIXACOCMC = e.parentElement.parentElement.children.item('0').value;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODCLSMER: CODCLSMER,
        CODGRPMER: CODGRPMER,
        NUMSEQMIXACOCMC: NUMSEQMIXACOCMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "DeleteMixClasseMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetMixClasseMercadoriaAdicionados();
            mostraMensagem("", "", "msgModalMecanicaMixClasseMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixClasseMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}
function DeleteAllMixClasseMercadoria() {
    ShowModalConfirmar({
        titulo: 'Confirmar Exclusão',
        mensagem: "Confirme a exclusão do todos os itens",
        appendElement: "msgModalMecanicaMixClasseMercadoria",
        callback: function () {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

            var obj = {
                CODACOCMC: CODACOCMC,
                NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
            }

            LoadingOn();
            $.ajax({
                url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "DeleteMixClasseMercadoria",
                type: 'POST',
                data: obj,
                success: function (data) {
                    abrirWidgetMixClasseMercadoriaAdicionados();
                },
                error: function (error) {
                    mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixClasseMercadoria");
                },
                complete: function () {
                    LoadingOff();
                }
            });
        },
        parametros: null
    });
}

//Excluir Itens Mix Classe Mercadoria
function DeleteMixMercadoria(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var NUMSEQMIXACOCMC = e.parentElement.parentElement.children.item('0').value;
    var CODMER = e.parentElement.parentElement.children.item('4').innerText;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODMER: CODMER,
        NUMSEQMIXACOCMC: NUMSEQMIXACOCMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "DeleteMixMercadoria",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetMixMercadoriaAdicionados();
            mostraMensagem("", "", "msgModalMecanicaMixMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}
function DeleteAllMixMercadoria() {
    ShowModalConfirmar({
        titulo: 'Confirmar Exclusão',
        mensagem: "Confirme a exclusão do todos os itens",
        appendElement: "msgModalMecanicaMixMercadoria",
        callback: function () {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

            var obj = {
                CODACOCMC: CODACOCMC,
                NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
            }

            LoadingOn();
            $.ajax({
                url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "DeleteMixMercadoria",
                type: 'POST',
                data: obj,
                success: function (data) {
                    abrirWidgetMixMercadoriaAdicionados();
                },
                error: function (error) {
                    mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixMercadoria");
                },
                complete: function () {
                    LoadingOff();
                }
            });
        },
        parametros: null
    });
}


//Excluir Itens Mix Classe Fornecedor
function DeleteMixFornecedor(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var NUMSEQMIXACOCMC = e.parentElement.parentElement.children.item('0').value;
    var CODFRN = e.parentElement.parentElement.children.item('2').innerText;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODFRN: CODFRN,
        NUMSEQMIXACOCMC: NUMSEQMIXACOCMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "DeleteMixFornecedor",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetMixFornecedorAdicionados();
            mostraMensagem("", "", "msgModalMecanicaMixFornecedor");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixFornecedor");
        },
        complete: function () {
            LoadingOff();
        }
    });
}
function DeleteAllMixFornecedor() {
    ShowModalConfirmar({
        titulo: 'Confirmar Exclusão',
        mensagem: "Confirme a exclusão do todos os itens",
        appendElement: "msgModalMecanicaMixFornecedor",
        callback: function () {
            var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
            var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

            var obj = {
                CODACOCMC: CODACOCMC,
                NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
            }

            LoadingOn();
            $.ajax({
                url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "DeleteMixFornecedor",
                type: 'POST',
                data: obj,
                success: function (data) {
                    abrirWidgetMixFornecedorAdicionados();
                },
                error: function (error) {
                    mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalMecanicaMixFornecedor");
                },
                complete: function () {
                    LoadingOff();
                }
            });
        },
        parametros: null
    });
}

function DeleteRelacaoAcaoMecanica(CODACOCMC, NUMSEQRGRACOCMC) {

    ShowModalConfirmar({
        titulo: "Confirmação de Exclusão",
        appendElement: "msgModalPromoção",
        mensagem: "Tem certeza que deseja excluir o registro?",
        callback: DeleteRelacaoAcaoMecanicaConfirm,
        parametros: {
            CODACOCMC: CODACOCMC,
            NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
        }
    });

}

function DeleteRelacaoAcaoMecanicaConfirm(e) {

    var CODACOCMC = e.CODACOCMC;
    var NUMSEQRGRACOCMC = e.NUMSEQRGRACOCMC;

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanica" + "/" + "DeleteT",
        type: 'POST',
        data: obj,
        success: function (data) {
            var CODACOCMC = obj.CODACOCMC;
            $('#modalListMecanicaPromocao').hide();
            $('.modal-backdrop.fade:not(:first)').remove();
            AbrirModalEditMecanicaAcao();
            mostraMensagem("", "", "msgModalPromoção");
        },
        error: function (error) {
            mostraMensagem("Ocorreu%20um%20erro%20ao%20tentar%20deletar%20configurações!", "msgDanger", "msgModalPromoção");
        },
        complete: function () {
            LoadingOff();
        }
    });
}

function pesquisaCliente() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var NOMCLIENTE = $('#txtNomCli').val();
    var NOMREPRESENTANTE = $('#txtNomRep').val();
    var NOMGERENTE = $('#txtNomGer').val();
    var NOMSUPERVISOR = $('#txtNomSup').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        NOMCLI: NOMCLIENTE,
        NOMREP: NOMREPRESENTANTE,
        NOMGER: NOMGERENTE,
        NOMSUP: NOMSUPERVISOR
    }

    postAjax("RelacaoAcaoMecanicaPublicoAlvoCliente", "PesquisaMecanicaPromocaoWidgetLoadPesquisaCliente", obj, function (data) {
        $('#divLoadPesquisaCliente').html(data);
        LoadingOff();
    });
}

function pesquisaClientesExcludentes() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var NOMCLIENTE = $('#txtNomCli').val();
    var NOMREPRESENTANTE = $('#txtNomRep').val();
    var NOMGERENTE = $('#txtNomGer').val();
    var NOMSUPERVISOR = $('#txtNomSup').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        NOMCLI: NOMCLIENTE,
        NOMREP: NOMREPRESENTANTE,
        NOMGER: NOMGERENTE,
        NOMSUP: NOMSUPERVISOR
    }

    postAjax("RelacaoAcaoMecanicaClientesExcludentes", "PesquisaMecanicaPromocaoWidgetLoadPesquisaCliente", obj, function (data) {
        $('#divLoadPesquisaClientesExcludentes').html(data);
        LoadingOff();
    });
}

function pesquisaRcaVendedor() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var NOMREPRESENTANTE = $('#txtNomRep').val();
    var NOMGERENTE = $('#txtNomGer').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        NOMREP: NOMREPRESENTANTE,
        NOMSUP: NOMGERENTE,
    }

    postAjax("RelacaoAcaoMecanicaPublicoAlvoRcaVendedor", "PesquisaMecanicaPromocaoWidgetLoadPesquisaRcaVendedor", obj, function (data) {
        $('#divLoadPesquisaRcaVendedor').html(data);
        LoadingOff();
    });
}

function pesquisaGerenteMercado() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var NOMGERENTEVENDAS = $('#txtNomGer').val();
    var NOMGERENTEMERCADO = $('#txtNomSup').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        NOMGER: NOMGERENTEVENDAS,
        NOMSUP: NOMGERENTEMERCADO
    }

    postAjax("RelacaoAcaoMecanicaPublicoAlvoGerenteMercado", "PesquisaMecanicaPromocaoWidgetLoadPesquisaGerenteMercado", obj, function (data) {
        $('#divLoadPesquisaGerenteMercado').html(data);
        LoadingOff();
    });
}

function pesquisaMixFamiliaMercadoria() {
    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var DESFMLMER = $('#txtDesFamiliaMercadoria').val();

    obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        DESFMLMER: DESFMLMER
    }

    if (!$('#txtDesFamiliaMercadoria').val().replace(/\s/g, '').length) {
        $('#txtDesFamiliaMercadoria').addClass('classError');
        mostraMensagem("É%20necessário%20preencher%20todos%20os%20campos.", "msgDanger", "msgWidgetMixFamiliaMercadoria");
        return false;
    }
    else {

        postAjax("RelacaoAcaoMecanicaMix", "PesquisaMixFamiliaMercadoria", obj,
            function (data) {
                $('#divLoadPesquisaMixFamiliaMercadoria').html(data);
                LoadingOff();
            }
        )
    }
}

function resizeModal(modal) {

    var screenHeight = window.innerHeight;

    modal.height(screenHeight - 10);
    modal.find('.modal-body').height(screenHeight - 140);

}


function verificaInput(id, idError, callback, param) {


    var error = 0;
    $('#' + id + ' input.numberInput:visible').each(function (i, v) {
        var el = $(this);
        if (el.val() === '') {
            el.addClass('classError');
            error++;
        } else if (isNaN(parseInt(el.val())) || parseInt(el.val()) === 0 || parseInt(el.val()) < 0) {
            el.addClass('classError');
            error++;
        }
    });

    $('#' + id + ' textarea.desc:visible, ' + '#' + id + ' input.desc:visible').each(function (i, v) {
        var el = $(this);
        if (!el.val().replace(/\s/g, '').length) {
            el.addClass('classError');
            error++;
        }
    });

    if (error !== 0) {
        mostraMensagem("É%20necessário%20preencher%20todos%20os%20campos.", "msgDanger", idError);
        //ShowModal({ mensagem: "É necessário preencher todos os campos.", titulo: "Atenção", width: 750 })
        return false;
    }

    //Sucesso
    return callback(param);
}

$(document).ready(function () {

    $(document).on("keyup", ".numberInput, .desc", function () {
        if ($(this).val().replace(/\s/g, '').length) {
            $(this).removeClass('classError')
        }
    });


});


function SalvarMecanica() {
    
    $('.classError').removeClass('classError');
    if ($('#tblGridRegistrosFaixa').length) {
        //Tem as faixas
        if (verificaInput('divBodyMecanicaPromocaoWidgetGerais', 'msgWidgetMecanicaPromocao', dummy) === false)
            return;
        if (verificaInput('divParametrosCalculoSimples', 'msgParametrosCalculoSimples', dummy) === false)
            return;

        verificaFaixas('msgParametrosCalculoSimples');
    }
    else {
        //Não tem as faixas
        if (verificaInput('divBodyMecanicaPromocaoWidgetGerais', 'msgWidgetMecanicaPromocao', dummy) === false)
            return;

        if ($('#txtQtdPremioAtingimentoCriterioFaixa').length) {
            if (!$('#txtQtdPremioAtingimentoCriterioFaixa').val().replace(/\s/g, '').length) {
                $('#txtQtdPremioAtingimentoCriterioFaixa').addClass('classError')
                //ShowModal({ mensagem: "É necessário preencher todos os campos.", titulo: "Atenção", width: 750 })
                mostraMensagem("É%20necessário%20preencher%20todos%20os%20campos.", "msgDanger", 'msgParametrosCalculoSimples');
                return false;
            } else if ($("#INDTIPPTOPMC").val() == "0" && (isNaN(parseInt($('#txtQtdPremioAtingimentoCriterioFaixa').val())) || parseInt($('#txtQtdPremioAtingimentoCriterioFaixa').val()) === 0)) {
                $('#txtQtdPremioAtingimentoCriterioFaixa').addClass('classError')
                //ShowModal({ mensagem: "O campo Qde de Prêmio não pode ser zero.", titulo: "Atenção", width: 750 })
                mostraMensagem("O%20campo%20Qde%20de%20Prêmio%20não%20pode%20ser%20zero.", "msgDanger", 'msgParametrosCalculoSimples');
                return false;
            }
            else if ($("#INDTIPPTOPMC").val() != "0" && (isNaN(parseFloat($('#txtQtdPremioAtingimentoCriterioFaixa').val().replace(',', '.'))) || parseFloat($('#txtQtdPremioAtingimentoCriterioFaixa').val().replace(',', '.')) === 0)) {
                $('#txtQtdPremioAtingimentoCriterioFaixa').addClass('classError')
                //ShowModal({ mensagem: "O campo Qde de Prêmio não pode ser zero.", titulo: "Atenção", width: 750 })
                mostraMensagem("O%20campo%20Qde%20de%20Prêmio%20não%20pode%20ser%20zero.", "msgDanger", 'msgParametrosCalculoSimples');
                return false;
            }
        } else if ($('#txtQtdPremioAtingimentoCriterioCombinada').length) {

            if (!$('#txtQtdPremioAtingimentoCriterioCombinada').val().replace(/\s/g, '').length) {
                $('#txtQtdPremioAtingimentoCriterioCombinada').addClass('classError')
                //ShowModal({ mensagem: "É necessário preencher todos os campos.", titulo: "Atenção", width: 750 })
                mostraMensagem("É%20necessário%20preencher%20todos%20os%20campos.", "msgDanger", 'msgParametrosCalculoSimples');
                return false;
            } else if ($("#INDTIPPTOPMC").val() == "0" && (isNaN(parseInt($('#txtQtdPremioAtingimentoCriterioCombinada').val())) || parseInt($('#txtQtdPremioAtingimentoCriterioCombinada').val()) === 0)) {
                $('#txtQtdPremioAtingimentoCriterioCombinada').addClass('classError')
                //ShowModal({ mensagem: "O campo Qde de Prêmio não pode ser zero.", titulo: "Atenção", width: 750 })
                mostraMensagem("O%20campo%20Qde%20de%20Prêmio%20não%20pode%20ser%20zero.", "msgDanger", 'msgParametrosCalculoSimples');
                return false;
            }
            else if ($("#INDTIPPTOPMC").val() != "0" && (isNaN(parseFloat($('#txtQtdPremioAtingimentoCriterioCombinada').val().replace(',', '.'))) || parseFloat($('#txtQtdPremioAtingimentoCriterioCombinada').val().replace(',', '.')) === 0)) {
                $('#txtQtdPremioAtingimentoCriterioCombinada').addClass('classError')
                //ShowModal({ mensagem: "O campo Qde de Prêmio não pode ser zero.", titulo: "Atenção", width: 750 })
                mostraMensagem("O%20campo%20Qde%20de%20Prêmio%20não%20pode%20ser%20zero.", "msgDanger", 'msgParametrosCalculoSimples');
                return false;
            }
        } else if ($('#txtQtdPremioAtingimentoCriterioNormal').length) {

            if (!$('#txtQtdPremioAtingimentoCriterioNormal').val().replace(/\s/g, '').length) {
                $('#txtQtdPremioAtingimentoCriterioNormal').addClass('classError')
                //ShowModal({ mensagem: "É necessário preencher todos os campos.", titulo: "Atenção", width: 750 })
                mostraMensagem("É%20necessário%20preencher%20todos%20os%20campos.", "msgDanger", 'msgParametrosCalculoSimples');
                return false;
            } else if ($("#INDTIPPTOPMC").val() == "0" && (isNaN(parseInt($('#txtQtdPremioAtingimentoCriterioNormal').val())) || parseInt($('#txtQtdPremioAtingimentoCriterioNormal').val()) === 0)) {
                $('#txtQtdPremioAtingimentoCriterioNormal').addClass('classError')
                //ShowModal({ mensagem: "O campo Qde de Prêmio não pode ser zero.", titulo: "Atenção", width: 750 })
                mostraMensagem("O%20campo%20Qde%20de%20Prêmio%20não%20pode%20ser%20zero.", "msgDanger", 'msgParametrosCalculoSimples');
                return false;
            }
            else if ($("#INDTIPPTOPMC").val() != "0" && (isNaN(parseFloat($('#txtQtdPremioAtingimentoCriterioNormal').val().replace(',', '.'))) || parseFloat($('#txtQtdPremioAtingimentoCriterioNormal').val().replace(',','.')) === 0)) {
                $('#txtQtdPremioAtingimentoCriterioNormal').addClass('classError')
                //ShowModal({ mensagem: "O campo Qde de Prêmio não pode ser zero.", titulo: "Atenção", width: 750 })
                mostraMensagem("O%20campo%20Qde%20de%20Prêmio%20não%20pode%20ser%20zero.", "msgDanger", 'msgParametrosCalculoSimples');
                return false;
            }
        }

        verificaInput('divParametrosCalculoSimples', 'msgParametrosCalculoSimples', jsSalvarDados)
        return;
    }
}

function trocarMascaraCashBack() {
    let tipoPremio = $('div.tipoPremio select[name=INDTIPPTOPMC]').val();
    let max = $('#txtVLRLIMMAXCTR').val();
    //$('div.tipoPremio input[name=QDEPTOFXACTRPMC]').val('');
    if (tipoPremio == 0) {
        $('div.tipoPremio input[name=QDEPTOFXACTRPMC]').attr('alt', 'inteiro_positivo');
        $('div.tipoPremio input[name=QDEPTOFXACTRPMC]').attr('maxlength', '10');
        $('div.tipoPremio #lblBrindes').html('Qtd. de Prêmio por Atingimento');
        $('div.tipoMax').hide();
    }
    else if (tipoPremio == 1) {
        $('div.tipoPremio input[name=QDEPTOFXACTRPMC]').attr('alt', 'decimal_positivo_2');
        $('div.tipoPremio input[name=QDEPTOFXACTRPMC]').attr('maxlength', '6');
        $('div.tipoPremio #lblBrindes').html('Percentual Cash Back por Atingimento ');
        $('div.tipoMax').show();
    }
    else if (tipoPremio == 2) {
        $('div.tipoPremio input[name=QDEPTOFXACTRPMC]').attr('alt', 'decimal_positivo_2');
        $('div.tipoPremio input[name=QDEPTOFXACTRPMC]').attr('maxlength', '10');
        $('div.tipoPremio #lblBrindes').html('Valor Cash Back por Atingimento ');
        $('div.tipoMax').show();
    }

    MaskConfig();
}

function trocarMascaraFaixaCashBack() {
    let tipoPremio = $('div.tipoPremio select[name=INDTIPPTOPMC]').val();
    let max = $('#txtVLRLIMMAXCTR').val();
    //$('div.tipoPremio input[name=QDEPTOFXACTRPMC]').val('');
    if (tipoPremio == 0) {
        $('div.tipoPremio input[name=QDEPTOFXACTRPMC]').attr('alt', 'inteiro_positivo');
        $('div.tipoPremio input[name=QDEPTOFXACTRPMC]').attr('maxlength', '10');
        $('div.tipoPremio #lblBrindes').html('Qtd. de Prêmio por Atingimento');
        //$('div.tipoPremio #lblBrindes').show();
        $('#primeiraIteracao').show();
        //$('div.tipoPremio #txtQtdPremioAtingimentoCriterioFaixa').show();
        //$('div.tipoMax').hide();
        $('#cashback').hide();
        $('div.tipoMaxFxa').hide();
        //$('#cashbackEspaco').hide();
        $('.cashbackCampo').hide();
        $('#tipoMaxFxa').hide();
    }
    else if (tipoPremio == 1) {
        $('.cashbackCampo').show();
        $('.type_dec').attr('alt', 'decimal_positivo_2');
        $('.type_dec').attr('maxlength', '6');
        $('div.tipoPremio #lblBrindes').html('Percentual Cash Back por Atingimento ');
        //$('div.tipoPremio #lblBrindes').hide();
        //$('div.tipoPremio #txtQtdPremioAtingimentoCriterioFaixa').hide();
        $('#primeiraIteracao').hide();
        $('div.tipoMaxFxa').show();
        $('#cashbackEspaco').show();
        $('#cashback').show();
        $('#cashback').html('Percentual Cash Back por Atingimento');
    }
    else if (tipoPremio == 2) {
        $('.cashbackCampo').show();
        $('.type_dec').attr('alt', 'decimal_positivo_2');
        $('.type_dec').attr('maxlength', '15');
        $('div.tipoPremio #lblBrindes').html('Valor Cash Back por Atingimento ');
        //$('div.tipoPremio #lblBrindes').hide();
        //$('div.tipoPremio #txtQtdPremioAtingimentoCriterioFaixa').hide();
        $('#primeiraIteracao').hide();
        $('div.tipoMaxFxa').hide();
        $('#cashbackEspaco').show();
        $('#cashback').show();
        $('#cashback').html('Valor Cash Back por Atingimento');
    }

    MaskConfig();
}

function dummy() {

}


//Função responsável por alterar todas as ocorrências de uma Substring em uma string
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function verificaFaixas(idError) {

    var validacaoTable = $('#tblGridRegistrosFaixa tr:has(td)').map(function (i, v) {
        var $td = $('td', this);
        var min = parseFloat($td.eq(1).find('input').val().replace(',00', '').replaceAll('.', '').replaceAll(',', '.'))
        var max = $td.eq(3).find('input').val() === 'Acima' ? 9999999999 : parseFloat($td.eq(3).find('input').val().replace(',00', '').replaceAll('.', '').replaceAll(',', '.'))
        var isFulfilled = min === null || min === 0 || isNaN(min) || max === null || max === 0 || isNaN(max) ? false : true;
        var isAscending = isFulfilled === true && max > min ? true : false;

        return {
            min: min,
            max: max,
            isFulfilled: isFulfilled,
            isAscending: isAscending,
            td: $td
        }
    }).get();


    if (validacaoTable.filter(x => x.isFulfilled === false).length > 0) {
        validacaoTable.filter(x => x.isFulfilled === false).forEach(function (result, index) {
            var td = result.td;

            if ((result.min === null || result.min === 0 || isNaN(result.min)) && (result.max === null || result.max === 0 || isNaN(result.max))) {
                td.eq(1).find('input').addClass('classError');
                td.eq(3).find('input').addClass('classError');
            } else if (result.min === null || result.min === 0 || isNaN(result.min)) {
                td.eq(1).find('input').addClass('classError');
            } else {
                td.eq(3).find('input').addClass('classError');
            }
        });
        //ShowModal({ mensagem: "É necessário preencher todos os campos.", titulo: "Atenção", width: 750 })
        mostraMensagem("É%20necessário%20preencher%20todos%20os%20campos.", "msgDanger", idError);
        return false;
    }

    if (validacaoTable.filter(x => x.isAscending === false).length > 0) {
        validacaoTable.filter(x => x.isAscending === false).forEach(function (result, index) {
            var td = result.td;

            td.eq(1).find('input').addClass('classError');
            td.eq(3).find('input').addClass('classError');

        });
        //ShowModal({ mensagem: "O valor do campo Início deve ser maior que o do campo Fim", titulo: "Atenção", width: 750 })
        mostraMensagem("O%20valor%20do%20campo%20Início%20deve%20ser%20menor%20que%20o%20do%20campo%20Fim", "msgDanger", idError);
        return false;
    }

    if (validacaoTable.length > 0) {

        var prevRow;
        var valueExpected = null;
        validacaoTable.forEach(function (result, index) {
            if (index > 0 && valueExpected === null) {
                var td = result.td;

                if ($('#drpCriterioPromocaoFaixa').val() == 5) {
                    if (parseFloat((prevRow.max + 0.01).toFixed(2)) !== result.min) {
                        td.eq(1).find('input').addClass('classError');
                        valueExpected = (prevRow.max + 0.01).toFixed(2);
                    }
                } else if ($('#drpCriterioPromocaoFaixa').val() == 2) {
                    if (parseFloat((prevRow.max + 1).toFixed(0)) !== result.min) {
                        td.eq(1).find('input').addClass('classError');
                        valueExpected = (prevRow.max + 1).toFixed(0);
                    }
                }
            }

            prevRow = result;
        });

        if (valueExpected !== null) {
            //ShowModal({ mensagem: "O valor do campo destacado deve ser: " + $.mask.string(valueExpected, $.mask.masks.currency), titulo: "Atenção", width: 750 })
            mostraMensagem("O%20valor%20do%20campo%20destacado%20deve%20ser:%20" + $.mask.string(valueExpected, $.mask.masks.currency), "msgDanger", idError);
            return false;
        }
    }

    if ($('#txtQtdPremioAtingimentoCriterioFaixa').length) {
        if ($('div.tipoPremio select[name=INDTIPPTOPMC]').val() == 0) {
        if (!$('#txtQtdPremioAtingimentoCriterioFaixa').val().replace(/\s/g, '').length) {
            $('#txtQtdPremioAtingimentoCriterioFaixa').addClass('classError')
            //ShowModal({ mensagem: "É necessário preencher todos os campos.", titulo: "Atenção", width: 750 })
            mostraMensagem("É%20necessário%20preencher%20todos%20os%20campos.", "msgDanger", idError);
            return false;
        } else if (isNaN(parseInt($('#txtQtdPremioAtingimentoCriterioFaixa').val())) || parseInt($('#txtQtdPremioAtingimentoCriterioFaixa').val()) === 0) {
            $('#txtQtdPremioAtingimentoCriterioFaixa').addClass('classError')
            //ShowModal({ mensagem: "O campo Qde de Prêmio não pode ser zero.", titulo: "Atenção", width: 750 })
            mostraMensagem("O%20campo%20Qde%20de%20Prêmio%20não%20pode%20ser%20zero.", "msgDanger", idError);
            return false;
        }
        }
    } else if ($('#txtQtdPremioAtingimentoCriterioCombinada').length) {

        if (!$('#txtQtdPremioAtingimentoCriterioCombinada').val().replace(/\s/g, '').length) {
            $('#txtQtdPremioAtingimentoCriterioCombinada').addClass('classError')
            //ShowModal({ mensagem: "É necessário preencher todos os campos.", titulo: "Atenção", width: 750 })
            mostraMensagem("É%20necessário%20preencher%20todos%20os%20campos.", "msgDanger", idError);
            return false;
        } else if (isNaN(parseInt($('#txtQtdPremioAtingimentoCriterioCombinada').val())) || parseInt($('#txtQtdPremioAtingimentoCriterioCombinada').val()) === 0) {
            $('#txtQtdPremioAtingimentoCriterioCombinada').addClass('classError')
            //ShowModal({ mensagem: "O campo Qde de Prêmio não pode ser zero.", titulo: "Atenção", width: 750 })
            mostraMensagem("O%20campo%20Qde%20de%20Prêmio%20não%20pode%20ser%20zero.", "msgDanger", idError);
            return false;
        }
    } else if ($('#txtQtdPremioAtingimentoCriterioNormal').length) {

        if (!$('#txtQtdPremioAtingimentoCriterioNormal').val().replace(/\s/g, '').length) {
            $('#txtQtdPremioAtingimentoCriterioNormal').addClass('classError')
            //ShowModal({ mensagem: "É necessário preencher todos os campos.", titulo: "Atenção", width: 750 })
            mostraMensagem("É%20necessário%20preencher%20todos%20os%20campos.", "msgDanger", idError);
            return false;
        } else if (isNaN(parseInt($('#txtQtdPremioAtingimentoCriterioNormal').val())) || parseInt($('#txtQtdPremioAtingimentoCriterioNormal').val()) === 0) {
            $('#txtQtdPremioAtingimentoCriterioNormal').addClass('classError')
            //ShowModal({ mensagem: "O campo Qde de Prêmio não pode ser zero.", titulo: "Atenção", width: 750 })
            mostraMensagem("O%20campo%20Qde%20de%20Prêmio%20não%20pode%20ser%20zero.", "msgDanger", idError);
            return false;
        }
    }

    //Sucesso
    jsSalvarDados();
}


function drpMixGrupoMercadoria_Change() {

    var CODGRPMER = $('#drpMixGrupoMercadoria').val();

    getAjax("RelacaoAcaoMecanicaMix", "CarregarDropMixCategoria/" + CODGRPMER,
        function (data) {
            $('#rowMixCategoria').html(data);
            LoadingOff();
        });
}

function drpMixCategoria_Change() {

    var CODGRPMER = $('#drpMixGrupoMercadoria').val();
    var CODFMLMER = $('#drpMixCategoria').val();

    getAjax("RelacaoAcaoMecanicaMix", "CarregarDropMixSubCategoria/" + CODGRPMER + '/' + CODFMLMER,
        function (data) {
            $('#rowMixSubCategoria').html(data);
            LoadingOff();
        });
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Pré Visualização Mix Mecanica Ação

function abrirModalPreVisualizarMixMecanicaAcao(id) {
    CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();

    var modalT = id;

    var data = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC
    }

    abrirModalSemLoadingPublicoAlvoConfiguracoes("PromocaoMixVisualizarVigente", "CarregarModalPreVisualizarMixMecanicaAcao", data, "#modalMecanicaPromocaoSubModal", "", modalT);
}

function fecharModalVisualizarMixVigente() {
    $("#modalListPromocaoVisualizarMixVigente").parent().modal('hide');
}
function fecharModalPreVisualizarMixMecanicaAcao() {
    $('#modalListPreVisualizarMixMecanicaAcao').modal('hide');
}


function ModalImportXls() {
    // variavel para salvar o arquivo
    var files;

    // evento ao selecionar arquivo
    $('input[type=file]').on('change', prepareUpload);

    // salva o arquivo na variavel
    function prepareUpload(event) {
        files = event.target.files;
        if (files == undefined) {
            $("#btnImportarXLS").attr("disabled", "disabled");
        }
        else {
            $("#btnImportarXLS").removeAttr("disabled");
            $.each(files, function (index, value) {
                var nome = value.name;
                var extensao = nome.substring(nome.indexOf("."));
                if (extensao != ".xlsx" && extensao != ".xls") {
                    $("#btnImportarXLS").attr("disabled", "disabled");
                    mostraMensagem("Importação%20de%20Arquivo%20não%20permitido!", "msgDanger", "msgModalCliente");
                }
            });
        }

    }

    if (files == undefined) {
        $("#btnImportarXLS").attr("disabled", "disabled");
    }
    else {
        $("#btnImportarXLS").removeAttr("disabled");
    }


    $('#arquivo').on('submit', uploadFiles);

    // ao submeter o formulario
    function uploadFiles(event) {
        event.stopPropagation(); // para execução
        event.preventDefault(); // para completamente a execução
        LoadingOn();
        var data = new FormData();
        $.each(files, function (key, value) {
            data.append(key, value);
        });

        TIPPMC = $('#frmWidgetMecanicaPromocaoGerais select[name="TIPPMC"]').val();
        if (TIPPMC == 1)
            QDEGRPMIXPMC = $('#frmWidgetMecanicaPromocaoGerais input[type="text"][name="QDEGRPMIXPMC"]').val();
        else
            QDEGRPMIXPMC = 0;

        var obj = {
            CODACOCMC: $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val(),
            NUMSEQRGRACOCMC: $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val()
        }

        $.ajax({
            url: SiteUrl + $(event.target).attr('value') + '/' + obj.CODACOCMC + '/' + obj.NUMSEQRGRACOCMC + '/' + QDEGRPMIXPMC,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'text',
            processData: false,
            contentType: false,
            success: function (data) {
                if (data == "TrueCliente") {
                    abrirWidgetPublicoAlvoCliente();
                    atualizaMensagem("msgModalCliente");
                }
                else if (data == "FalseCliente") {
                    abrirWidgetPublicoAlvoCliente();
                    atualizaMensagem("msgModalCliente");
                }
                else if (data == "TrueRcaVendedor") {
                    abrirWidgetPublicoAlvoRcaVendedor();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalRcaVendedor");
                }
                else if (data == "FalseRcaVendedor") {
                    atualizaMensagem("msgModalRcaVendedor");
                    abrirWidgetPublicoAlvoRcaVendedor();
                }
                else if (data == "TrueGerenteMercado") {
                    abrirWidgetPublicoAlvoGerenteMercado();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalGerenteMercado");
                }
                else if (data == "FalseGerenteMercado") {
                    atualizaMensagem("msgModalGerenteMercado");
                    abrirWidgetPublicoAlvoGerenteMercado();
                }
                else if (data == "TrueMixFornecedor") {
                    abrirWidgetMixFornecedorAdicionados();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalMecanicaMixFornecedor");
                }
                else if (data == "FalseMixFornecedor") {
                    atualizaMensagem("msgModalMecanicaMixFornecedor");
                    abrirWidgetMixFornecedorAdicionados();
                }
                else if (data == "TrueMixMercadoria") {
                    abrirWidgetMixMercadoriaAdicionados();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalMecanicaMixMercadoria");
                }
                else if (data == "FalseMixMercadoria") {
                    atualizaMensagem("msgModalMecanicaMixMercadoria");
                    abrirWidgetMixMercadoriaAdicionados();
                }
                else if (data == "TrueMixClasseMercadoria") {
                    abrirWidgetMixClasseMercadoriaAdicionados();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalMecanicaMixClasseMercadoria");
                }
                else if (data == "FalseMixClasseMercadoria") {
                    atualizaMensagem("msgModalMecanicaMixClasseMercadoria");
                    abrirWidgetMixClasseMercadoriaAdicionados();
                }
                else if (data == "TrueMixFamiliaMercadoria") {
                    abrirWidgetMixFamiliaMercadoriaAdicionados();
                    mostraMensagem("Importação%20realizada%20com%20sucesso!", "msgSuccess", "msgModalMecanicaMixFamiliaMercadoria");
                }
                else if (data == "FalseMixFamiliaMercadoria") {
                    atualizaMensagem("msgModalMecanicaMixFamiliaMercadoria");
                    abrirWidgetMixFamiliaMercadoriaAdicionados();
                }
                LoadingOff();
            },
            error: function (error) {
                atualizaMensagem();
                //console.log('ERRORS: ' + textStatus);
                LoadingOff();
            }
        });
    }

    $('.ace-file-container').ace_file_input({
        no_file: 'Sem Arquivo ...',
        btn_choose: 'Selecione',
        btn_change: 'Mudar',
        droppable: false,
        onchange: null,
        thumbnail: false
    });
}





function GridRegistrosFaixaCODCTRPMCChange(sender) {
    if ($(sender).val() == 2)
        $('table#tblGridRegistrosFaixa input[alt="decimal_positivo_2"]').attr('alt', 'inteiro')
    else if ($(sender).val() == 5)
        $('table#tblGridRegistrosFaixa input[alt="inteiro"]').attr('alt', 'decimal_positivo_2')
    MaskConfig();
}

function togglePesquisaMixMercadoria(type) {
    if (type === 'ParaPesquisaPorPromocao') {
        CarregarWidgetLoadPesquisaMixMercadoriaPorPromocao();
        $('#divLoadPesquisaMixMercadoriaPorPromocao').show()
        $('#divLoadPesquisaMixMercadoria').hide()
    }
    else if (type === 'ParaPesquisaPadrao') {
        CarregarWidgetLoadPesquisaMixMercadoria();
        $('#divLoadPesquisaMixMercadoriaPorPromocao').hide()
        $('#divLoadPesquisaMixMercadoria').show()
    }
}

function CarregarWidgetLoadPesquisaMixMercadoriaPorPromocao() {
    let CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    let NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    let TIPPMC = $('#frmWidgetMecanicaPromocaoGerais select[name="TIPPMC"]').val();

    let QTDFXAGRP = "";
    if (TIPPMC == 1)
        QTDFXAGRP = $('#frmWidgetMecanicaPromocaoGerais input[type="text"][name="QDEGRPMIXPMC"]').val();
    else
        QTDFXAGRP = $('#frmWidgetMecanicaPromocaoGerais input[type="text"][name="QDEFXAPMC"]').val();

    getAjax("RelacaoAcaoMecanicaMix"
        , "CarregarWidgetLoadPesquisaMixMercadoriaPorPromocao/" + CODACOCMC + '/' + NUMSEQRGRACOCMC + '/' + TIPPMC + '/' + QTDFXAGRP
        , function (data) {
            $('#divLoadPesquisaMixMercadoriaPorPromocao').html(data);
            LoadingOff();
        })
}

function MixMercadoriaPorPromocaoLoadingOff_UpdateMessage() {
    $('#loadingDialog').modal('hide');
    atualizaMensagem("msgModalMecanicaMixMercadoria");
    ChosenConfig();
    ChosenResize();
    MaskConfig();
    PopoverConfig();
    WidgetConfig();
}


//Salvar dados Ação/Mecanica/ Mix Mercadoria
function salvarMixMercadoriaPorPromocao(e) {

    var CODACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="CODACOCMC"]').val();
    var NUMSEQRGRACOCMC = $('#frmWidgetMecanicaPromocaoGerais input[type="hidden"][name="NUMSEQRGRACOCMC"]').val();
    var CODMER = e.parentElement.parentElement.children.item(2).innerText;
    var CODGRPMIXMERPMC = $('#drpAddRelacaoAcaoMecanicaMixFaixaPorPromocao').val();

    var obj = {
        CODACOCMC: CODACOCMC,
        NUMSEQRGRACOCMC: NUMSEQRGRACOCMC,
        CODMER: CODMER,
        CODGRPMIXMERPMC: CODGRPMIXMERPMC
    }


    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "SaveMixMercadoriaPorPromocao",
        type: 'POST',
        data: obj,
        success: function (data) {
            abrirWidgetMixMercadoriaAdicionados();
            atualizaMensagem("msgModalMecanicaMixMercadoria");
            e.parentElement.parentElement.remove();
        },
        error: function (error) {

        },
        complete: function () {
            LoadingOff();
        }
    });
}

function addPesquisaMixMercadoriaPorPromocao(data) {
    if (data && data.CODGRPMIXMERPMC) {
        data.CODGRPMIXMERPMC = $('#drpAddRelacaoAcaoMecanicaMixFaixaPorPromocao').val()
    }
    LoadingOn();
    $.ajax({
        url: SiteUrl + "RelacaoAcaoMecanicaMix" + "/" + "AddPesquisaMixMercadoriaPorPromocao",
        type: 'POST',
        data: data,
        success: function (data) {
            abrirWidgetMixMercadoriaAdicionados();
            $('#frmPesquisaMixMercadoriaPorPromocao').submit();
            atualizaMensagem("msgModalMecanicaMixMercadoria");
        },
        complete: function () {
            LoadingOff();
        }
    });
}