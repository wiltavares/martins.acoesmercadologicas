﻿function alterarDataFimPromocao() {
    var objeto = "";
    var lstPromocao = $("#tblResultadosPesquisaPromocao tbody input[name=SelecionePromocao][type=checkbox]");
    var lstPromocaoSelecionadas = [];

    lstPromocao.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {

            var CODPMC = parseFloat(check.parent().parent().children('.CodigoPromocao').html().trim());

            lstPromocaoSelecionadas.push(CODPMC);
        }
    });

    if (lstPromocaoSelecionadas.length > 0) {
        abrirModal('Promocao', 'CarregarModalAlterarDataFimPromocao', objeto, "#modalIndexPromocao");
    }
    else {
        mostraMensagem("Favor,%20selecionar%20promoção!", "msgWarning")
    }

}

function SalvarAlteração() {
    var pDATFIMPMC = $("#txtNovaDataFimPromocao").val();
    var lstPromocao = $("#tblResultadosPesquisaPromocao tbody input[name=SelecionePromocao][type=checkbox]");
    var lstPromocaoSelecionadas = [];

    lstPromocao.each(function (element) {
        var check = $(this);
        if (check.prop('checked')) {

            var CODPMC = parseFloat(check.parent().parent().children('.CodigoPromocao').html().trim());

            lstPromocaoSelecionadas.push(CODPMC);
        }
    });
    LoadingOn();

    $.ajax({
        url: SiteUrl + 'Promocao/AlterarDataFimPromocao',
        type: 'POST',
        data: {
            DATFIMPMC: pDATFIMPMC,
            CODPMCList: lstPromocaoSelecionadas
        },
        success: function (data) {
            FecharModalAlterarDataFim();

            if (data == "True") {
                $('#form0').submit();
                mostraMensagem("Alterações realiza com sucesso!", msgSuccess);
            }
            LoadingOff();
            atualizaMensagem();
        },
        error: function (error) {
            $('#modalIndexPromocao').modal('hide')
            FecharModalAlterarDataFim();
            LoadingOff();
            atualizaMensagem();
        }
    });


}

function FecharModalAlterarDataFim() {
    $('#modalIndexPromocao').modal('hide')
}

function marcarOuDesmarcarTodasPromocao() {
    if ($("#chkTodasPromocao").prop("checked"))
        $(".selecionePromocao").prop("checked", true);
    else $(".selecionePromocao").prop("checked", false);
}

function resizeModalPromocao(modal) {
    var screenHeight = window.innerHeight;

    modal.height(screenHeight - 10);
    modal.find('.modal-body').height(screenHeight - 140);
}


function AbrirPromocaoConfirm(idPmc) {
    ShowModalConfirmar({ titulo: 'Confirmar reabrir', mensagem: "Deseja reabrir esta promoção?", callback: AbrirPromocao, parametros: idPmc, width: 800 });
}
function AbrirPromocao(idPmc) {
    postAjax("Promocao", "Abrir" + '/' + idPmc,
        null,
        function (data) {
            LoadingOff();
            atualizaMensagem();
            if (data != "") {
                window.location.href = window.location.href + '/edit/' + data.CODPMC;
            }
        },
        function (data) {
            LoadingOff();
            atualizaMensagem();
        }
    );
}

function SuspenderPromocaoCashbackConfirm(idPmc) {
    ShowModalConfirmar({ titulo: 'Confirmar suspender', mensagem: "Deseja suspender esta promoção?", callback: SuspenderPromocaoCashback, parametros: idPmc, width: 800 });
}
function SuspenderPromocaoCashback(idPmc) {
    postAjax("Promocao", "SuspenderPromocaoCashback", { CODPMC: idPmc},
        function (data) {
            $('#form0').submit();
            LoadingOff();
            atualizaMensagem();
        },
        function (data) {
            LoadingOff();
            atualizaMensagem();
        }
    );
}

function RetirarSuspensaoConfirm(idPmc) {
    ShowModalConfirmar({ titulo: 'Confirmar retirar suspensão', mensagem: "Deseja retirar a suspensão desta promoção?", callback: RetirarSuspensao, parametros: idPmc, width: 800 });
}
function RetirarSuspensao(idPmc) {
    postAjax("Promocao", "RetirarSuspensao", { CODPMC: idPmc },
        function (data) {
            $('#form0').submit();
            LoadingOff();
            atualizaMensagem();
        },
        function (data) {
            LoadingOff();
            atualizaMensagem();
        }
    );
}

function ExibirHistorico(idPmc) {
    $.ajax({
        url: "Promocao/PesquisarHistorico",
        type: 'POST',
        data: { CODPMC: idPmc },
        success: function (data) {
            $('#divModalHistoricoSituacao').html(data);
            $('#divModalHistoricoSituacao > .modal').modal();
            LoadingOff();
            atualizaMensagem('msgGeral');

        },
        error: function (e) {
            atualizaMensagem('msgGeral');
            LoadingOff();
        }
    });
}
function btnFecharExibirHistorico() {
    $('#divModalHistoricoSituacao > .modal').modal('hide');
}
