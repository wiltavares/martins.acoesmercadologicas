﻿
function IncluirAprovadoresExtra() {

    var codTipoEvento = $('#codTipoEvento').val();
    var codAprovSub = $('#codTipoAprovador').val();
    var codAprovExtra = $('#codTipoAprovadorSubst').val();
    var codFuncionario = $('#idCodFuncionario').val();

    if (codTipoEvento == "") {
        codTipoEvento = 0
    }
    if (codAprovSub == "") {
        codAprovSub = 0
    }
    if (codAprovExtra == "") {
        codAprovExtra = 0
    }
    if (codFuncionario == "") {
        codFuncionario = 0
    }


    if (ValidaCampos(codTipoEvento, codAprovSub, codAprovExtra, codFuncionario)) {
        $.ajax({
            url: SiteUrl + "AprovadoresExtra/IncluirRegistros",
            type: 'POST',
            data: { codTipoEvento: codTipoEvento, codAprovSub: codAprovSub, codAprovExtra: codAprovExtra, codFuncionario: codFuncionario },
            success: function (data) {
                if (data !== "") {
                    $("#resultadosView").html(data);
                    atualizaMensagem("msgGeral");
                } else {
                    atualizaMensagem("msgGeral");
                }
            },
            error: function () {
                atualizaMensagem("msgGeral");
            }
        });
    }
}

function BuscaFuncionariosAprovadores() {
    var codfuncionario = $('#FilterCodFuncionario').val();
    var nomfuncionario = $('#FilternomeFuncionario').val();

    $.ajax({
        url: SiteUrl + "AprovadoresExtra/BuscaFuncionariosAprovadores",
        type: 'POST',
        data: { codfuncionario: codfuncionario, nomfuncionario: nomfuncionario },
        success: function (data) {
            if (data !== "") {
                $("#resultadosFuncionarios").html(data);
            }
            atualizaMensagem("msgBuscaFuncionarios");
        },
        error: function () {
            atualizaMensagem("msgBuscaFuncionarios");
        }
    });
}

function modalBuscaFuncionariosAprovadores() {
    abrirModal('AprovadoresExtra', 'AbrirModalFuncionario', $('#nomFuncionario').val(), '#modalFuncionarios');
    atualizaMensagem("msgBuscaFuncionarios");

}

function selecionarFuncionarioAprovadoresExtra(CODFNC, NOMFNC) {
    $('#idCodFuncionario').val(CODFNC);
    //$('#nomFuncionario').val(CODFNC + ' - ' + NOMFNC);
    $('#nomFuncionario').val(NOMFNC);

    document.getElementById("nomFuncionario").readOnly = true;
    document.getElementById("btBuscaFunc").style.display = "none";
    document.getElementById("btTiraFunc").style = "";
    $('#modalFuncionarios').modal('hide');
}

function tiraFuncionarioAprovadores() {
    $('#idCodFuncionario').val("");
    $('#nomFuncionario').val("");

    document.getElementById("btBuscaFunc").style = "";
    document.getElementById("btTiraFunc").style.display = "none";
    document.getElementById("nomFuncionario").readOnly = false;
}

function btnCancelar() {
    $('#modalFuncionarios').modal('hide');
}

function CarregaSubstituido() {
    var codTipoAprovador = $('#codTipoAprovador').val();
    if (codTipoAprovador != "")
    {
        $.ajax({
            url: SiteUrl + "AprovadoresExtra/CarregaSubstituido",
            type: 'POST',
            data: { codTipoAprovador: codTipoAprovador },
            success: function (data) {
                carregarDropDinamicamente("codTipoAprovadorSubst", "CODFNC", "NOMFNC", data, true);
            },
            error: function () {
                atualizaMensagem();
            }
        });
    }
    else
    {
        carregarDropDinamicamente("codTipoAprovadorSubst", "CODFNC", "NOMFNC", undefined, false);
    }
}

function ValidaCampos(codTipoEvento, codAprovSub, codAprovExtra, codFuncionario) {
    var valid = 0
    if (codTipoEvento == 0) {
        document.getElementById("validTipoEvento").style.display = 'block';
        valid = valid + 1
    }
    else {
        document.getElementById("validTipoEvento").style.display = "none";
    }

    if (codAprovSub == 0) {
        document.getElementById("ValidTipoAprovador").style.display = 'block';
        valid = valid + 1
    }
    else {
        document.getElementById("ValidTipoAprovador").style.display = "none";
    }

    if (codAprovExtra == 0) {
        document.getElementById("ValidAprovadorSubstituido").style.display = 'block';
        valid = valid + 1
    }
    else {
        document.getElementById("ValidAprovadorSubstituido").style.display = "none";
    }

    if (codFuncionario == 0) {
        document.getElementById("ValidNomFunc").style.display = 'block';
        valid = valid + 1
    }
    else {
        document.getElementById("ValidNomFunc").style.display = "none";
    }

    if (valid > 0) {
        return false;
    } else {
        return true;
    }
}