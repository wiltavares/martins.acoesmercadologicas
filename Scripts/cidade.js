﻿
// Evento change do drop de Fornecedor para atribuir o codigo ao textBox correspondente
function dropFiltroCidade_onChange() {
    $('#filtroCodCid').val($('#dropFiltroCidade').val());
}

// Busca cidades e atualiza o dropdown com os valores da busca
function buscarCidades() {
    var codCidade = $('#filtroCodCid').val();
    postAjax("Parametros", "BuscarDropCidades", { codCid: codCidade },
        function (data) {
            carregarDropDinamicamente("dropFiltroCidade", "CODCID", "NOMCID", data, true);
            $('#filtroCodCid').val($('#dropFiltroCidade').val());
            atualizaMensagemModalCarimbo();
            LoadingOff();
        }, function () {
            atualizaMensagemModalCarimbo();
            LoadingOff();
        }
    );
}


