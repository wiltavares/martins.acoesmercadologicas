﻿
function BuscaFuncionarios() {
    var codfuncionario = $('#FilterCodFuncionario').val();
    var nomfuncionario = $('#FilternomeFuncionario').val();

    $.ajax({
        url: SiteUrl + "RelacaoFuncionarioGerenteMarketing/BuscaFuncionarios",
        type: 'POST',
        data: { codfuncionario: codfuncionario, nomfuncionario: nomfuncionario },
        success: function (data) {
            if (data !== "") {
                $("#resultadosFuncionarios").html(data);
            }
            atualizaMensagem("msgBuscaFuncionarios");
        },
        error: function () {
            atualizaMensagem("msgBuscaFuncionarios");
        }
    });
}

function modalBuscaFuncionarios() {
    //  var desMerc = $('#itemBusca').val();
    abrirModal('RelacaoFuncionarioGerenteMarketing', 'AbrirModalFuncionario', $('#nomFuncionarioGerente').val(), '#modalFuncionarios', teste, undefined, 1);
}

function modalBuscaFuncionariosEmitente() {
    //  var desMerc = $('#itemBusca').val();
    abrirModal('RelacaoFuncionarioGerenteMarketing', 'AbrirModalFuncionario', $('#nomFuncionarioEmitente').val(), '#modalFuncionarios', teste, undefined, 2);
}

function teste(n) {
    $('#idFiltroFunc').val(n);
}

function selecionarFuncionarioGerenteMarketing(CODFNC, NOMFNC) {
    var idFiltro = $('#idFiltroFunc').val();

    if (idFiltro == 1) {

        $('#idCodFuncionarioGerente').val(CODFNC);
        $('#nomFuncionarioGerente').val(NOMFNC);

        document.getElementById("nomFuncionarioGerente").readOnly = true;
        document.getElementById("btBuscaFunc").style.display = "none";
        document.getElementById("btTiraFunc").style = "";
    }
    else {

        $('#idCodFuncionarioEmitente').val(CODFNC);
        $('#nomFuncionarioEmitente').val(NOMFNC);

        document.getElementById("nomFuncionarioEmitente").readOnly = true;
        document.getElementById("btBuscaFuncEmitente").style.display = "none";
        document.getElementById("btTiraFuncEmitente").style = "";
    }

    $('#modalFuncionarios').modal('hide');
}

function tiraFuncionarioGerente() {
    $('#idCodFuncionarioGerente').val("");
    $('#nomFuncionarioGerente').val("");

    document.getElementById("btBuscaFunc").style = "";
    document.getElementById("btTiraFunc").style.display = "none";
    document.getElementById("nomFuncionarioGerente").readOnly = false;
}

function tiraFuncionarioEmitente() {
    $('#idCodFuncionarioEmitente').val("");
    $('#nomFuncionarioEmitente').val("");

    document.getElementById("btBuscaFuncEmitente").style = "";
    document.getElementById("btTiraFuncEmitente").style.display = "none";
    document.getElementById("nomFuncionarioEmitente").readOnly = false;
}


//// Adiciona Relação de Funcionário com Gerente de Marketing
function adicionaRelacaoFuncGerMtk() {
    var codFuncEmiente = $('#idCodFuncionarioEmitente').val();
    var codFuncGerMtk = $('#idCodFuncionarioGerente').val()

    if (codFuncEmiente == "") {
        codFuncEmiente = 0
    }

    if (codFuncGerMtk == "") {
        codFuncGerMtk = 0
    }

    $.ajax({
        url: SiteUrl + "RelacaoFuncionarioGerenteMarketing/Adicionar",
        type: 'POST',
        data: { codFuncEmiente: codFuncEmiente, codFuncGerMtk: codFuncGerMtk },
        success: function (data) {
            if (data !== "") {
                $("#resultadosView").html(data);
            }
            atualizaMensagem("msgGeral");
        },
        error: function () {
            atualizaMensagem("msgGeral");
        }
    });
}


// chama funcao da controller que apaga o vinculo de funcionario com gerente mrtk
function apagaRelacaoFuncGerMtk(codFuncEmiente, codFuncGerMtk) {

    postAjax("RelacaoFuncionarioGerenteMarketing", "Apagar", { codFuncEmiente: codFuncEmiente, codFuncGerMtk: codFuncGerMtk },
        function (data) {

            $('#resultadosView').html(data);
            atualizaMensagem("msgGeral");
            LoadingOff();
        }, function () {
            atualizaMensagem("msgGeral");
            LoadingOff();
        }
    );
}

function btnCancelar() {
    $('#modalFuncionarios').modal('hide');
}