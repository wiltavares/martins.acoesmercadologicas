﻿// Busca fornecedores e atualiza o dropdown com os valores da busca
function buscarFornecedoresPremiacaoExclusiva() {
    var codFornecedor = $('#filtroCodFrn').val();
    LoadingOn()
    $.ajax({
        url: SiteUrl + "Fornecedor/BuscarFornecedores/",
        type: 'POST',
        data: { codFrn: codFornecedor },
        success: function (data) {
            carregarDropDinamicamente("dropFiltroFornecedores", "CODFRN", "CodNomFrn", data, true);
            $('#filtroCodFrn').val($('#dropFiltroFornecedores').val());
            $('#nomeFornecedor').val($("#dropFiltroFornecedores option:selected").text());
            atualizaMensagemModalPremiacao();
            LoadingOff();
        },
        error: function () {
            atualizaMensagemModalPremiacao();
            LoadingOff();
        }
    });
}

function atualizaModalPremiacaoExclusiva(data) {
    if (data.responseText != "") {
        atualizaMensagemModalPremiacao();
        $('#modal').html(data.responseText);
    }
    LoadingOff();
}

// Acao do botao de concluir a insercao de Premiação Exclusiva
function concluirInsercaoPremiacao(data) {
    $('#modal').modal('hide');
    $('#gridPremiacoes').html(data.responseText);
    atualizaMensagem();
    LoadingOff();
}

// Atualiza a mensagem do modal vinda do controller ou da propria tela
function atualizaMensagemModalPremiacao(mensagem) {
    if (!!mensagem) {
        $("#msgModalPremiacao").load(SiteUrl + 'Alert/_Alerts/?' + mensagem, function () { });
    } else {
        $("#msgModalPremiacao").load(SiteUrl + 'Alert/_Alerts', function () { });
    }
}

// Evento change do drop de Fornecedor para atribuir o codigo ao textBox correspondente
function dropFornecedorPremiacao_onChange() {
    $('#filtroCodFrn').val($('#dropFiltroFornecedores').val());
    $('#hdfCodFornecedor').val($('#dropFiltroFornecedores').val());
}