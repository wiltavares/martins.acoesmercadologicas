﻿function CalculaTotal() {

    //var vlrUnidade = parseFloat($('#VlrUntario').val().replace(",", "."));
    //$('#TotIte').val(obj.ValorTotal.toFixed(4).replace(".", ","));

    var vlrOrcamento = ConverterFloat($("#VLROCDCTTPLNMKT").val());
    var vlrExtra = ConverterFloat($("#VLROCDEXACTTPLNMKT").val());
    if (vlrOrcamento === null || vlrOrcamento === undefined)
        vlrOrcamento = 0;

    if (vlrExtra === null || vlrExtra === undefined)
        vlrExtra = 0;

    //if (vlrOrcamento === undefined || vlrOrcamento == "," || vlrOrcamento == "") {
    //    vlrOrcamento = "0,0";
    //    $("#VLROCDCTTPLNMKT").val("");
    //}
    //else {
    //    vlrOrcamento = vlrOrcamento.replace(",", ".");
    //}
    //if (vlrExtra === undefined || vlrExtra == "," || vlrExtra == "") {
    //    vlrExtra = "0,0";
    //    $("#VLROCDEXACTTPLNMKT").val("");
    //}
    //else {
    //    vlrExtra = vlrExtra.replace(",", ".");
    //}

    var resultado = vlrOrcamento + vlrExtra;
    //resultado = resultado.toString();
    $("#VLROCDPLNMKT").val(FormataFloat(resultado, 2));

    $("#VLROCDCTTPLNMKT").val(FormataFloat(vlrOrcamento, 2));
    $("#VLROCDEXACTTPLNMKT").val(FormataFloat(vlrExtra, 2));
}

function FormataFloat(valor, casasDecimais) {
    valor = parseFloat(valor.toFixed(casasDecimais));
    return valor.toLocaleString('pt-BR', { minimumFractionDigits: casasDecimais });
}
function FormataInteiro(valor) {
    return ('' + valor).replace(/\./g, ",");
}
function ConverterFloat(valor) {
    if (valor === '')
        return null;
    return parseFloat(valor.replace('R$', '').replace(/\./g, "").replace(/\,/g, "."));
}


/*
* Carrega Drop de eventos de acordo com tipo evento
*/
function carregarEventosPorTipoEventoPlanoMarketing(idTipoEvento) {
    carregarDropPorFiltro('dropTipoEvento', 'dropEvento', 'CODEVTACOCMC', 'DROPDESC', 'Evento', 'ListarEventosDoTipoEvento');
}


function ModalImportXlsPlanoDeMarketingSuccess() {
    // variavel para salvar o arquivo
    var files;

    // evento ao selecionar arquivo
    $('input[type=file]').on('change', prepareUpload);

    // salva o arquivo na variavel
    function prepareUpload(event) {
        files = event.target.files;
    }

    $('#arquivo').on('submit', uploadFiles);

    // ao submeter o formulario
    function uploadFiles(event) {
        event.stopPropagation(); // para execução
        event.preventDefault(); // para completamente a execução

        var data = new FormData();
        $.each(files, function (key, value) {
            data.append(key, value);
        });

        $.ajax({
            url: SiteUrl + 'PlanoDeMarketing/ImportarPlanilha',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'text',
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    LoadingOn();
                    $('#modalResultImport').html(data);
                    $('#modalResultImport').modal();
                    $('#modal').modal('toggle');
                    LoadingOff();
                }
                else {
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
            }
        });
    }

    $('.ace-file-container').ace_file_input({
        no_file: 'Sem Arquivo ...',
        btn_choose: 'Selecione',
        btn_change: 'Mudar',
        droppable: false,
        onchange: null,
        thumbnail: false
    });
}