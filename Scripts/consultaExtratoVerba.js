﻿

function rblTipoOperacaoExtratoExtrato_checkedChange() {
    $('.visible-extrato').show();
    $('.visible-saldo').hide();
    ChosenResize();
}

function rblTipoOperacaoExtratoSaldo_checkedChange() {
    $('.visible-extrato').hide();
    $('.visible-evento').hide();

    $('#TipoVisaoFornecedor').prop("checked", true);
    $('.visible-saldo').show();
    ChosenResize();
}


function rblTipoVisaoFornecedor_checkedChange() {
    $('.visible-fornecedor').show();
    $('.visible-evento').hide();
    ChosenResize();
}

function rblTipoVisaoEvento_checkedChange() {
    $('.visible-evento').show();
    $('.visible-fornecedor').hide();
    ChosenResize();
}




function btnExportarExtratoVerba() {

    LoadingOn();

    var form = $("#ConsultaExtratoVerba");

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    var data = form.serializeObject();

    if ($('#dataIni').val() != '')
        data["Filter.DataIni"]= $('#dataIni').val().substring(6, 10) + '-' + $('#dataIni').val().substring(3, 5) + '-' + $('#dataIni').val().substring(0, 2) + 'T00:00:00';

    if ($('#dataFim').val() != '')
        data["Filter.DataFim"] = $('#dataFim').val().substring(6, 10) + '-' + $('#dataFim').val().substring(3, 5) + '-' + $('#dataFim').val().substring(0, 2) + 'T00:00:00';

    $.ajax({
        cache: false,
        url: SiteUrl + "ConsultaExtratoVerba/Imprimir",
        data: data,
        success: function (data) {
            var response = data;

            if ($('#dataIni').val() !== "" && $('#dataFim').val() !== "" && data !== "") {
                window.location = SiteUrl + 'ConsultaExtratoVerba/Download?fileGuid=' + response.FileGuid
                    + '&filename=' + response.FileName;
            }
            else {
                atualizaMensagem("msgGeral");
            }
            LoadingOff();
        },
        error: function () {
            LoadingOff();
        }
    })
};