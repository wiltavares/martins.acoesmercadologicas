﻿// Evento change do drop de Fornecedor para atribuir o codigo ao textBox correspondente
function dropIncentivo_onChange() {
    $('#filtroCodInc').val($('#dropFiltroIncentivo').val());
}

// Atualiza a mensagem do modal vinda do controller ou da propria tela
function atualizaMensagemModalIncentivo(mensagem, tipo) {
    if (!!mensagem) {
        mostraMensagem(mensagem, tipo, "msgModalIncentivo");
    } else {
        atualizaMensagem("msgModalIncentivo");
    }
}

function filtroIncentivoFailure() {
    atualizaMensagemModalIncentivo();
    LoadingOff();
}

function filtroIncentivoSuccess(data) {
    if (data !== "") {
        $('#gridIncentivoModal').html(data);
    }
    atualizaMensagemModalIncentivo();
    LoadingOff();
}

// Busca Incentivos de acordo com o filtro
function filtrarIncentivos() {
    var codFiltro = ""

    postAjaxUrlData("Incentivo", "FiltrarIncentivos", codFiltro,
        function (data) {
            if (data !== "") {
                $('#gridIncentivoModal').html(data);
            }
            atualizaMensagemModalIncentivo();
            LoadingOff();
        }, function () {

        }
    );

}


// Pega a lista de itens checkados e coloca na lista de incentivos selecionados
function selecionarIncentivos() {
    var itens = retornaListaDeIncentivosCheckados("ckbIncInc");

    if (itens.length == 0) {
        atualizaMensagemModalIncentivo("SELECIONAR_INCENTIVO", "msgWarning");
    } else {
        $('[id^="ckbIncCar"]').prop("checked", false);

        postAjax("Incentivo", "SelecionarIncentivos", { itensCheckados: itens },
            function (data) {
                if (data !== "") {
                    $('#gridIncentivosSelecionadosModal').html(data);
                }
                atualizaMensagemModalIncentivo();
                LoadingOff();
            }, function () {
                atualizaMensagemModalIncentivo();
                LoadingOff();
            }
        );
    }
}

// Pega a lista de itens selecionados checkados e remove do grid
function removerIncentivos() {
    var itens = retornaListaDeIncentivosCheckados("ckbExInc");

    if (itens.length == 0) {
        atualizaMensagemModalIncentivo("SELECIONAR_INCENTIVO", "msgWarning");
    } else {
        $('[id^="ckbExInc"]').prop("checked", false);

        postAjax("Incentivo", "RemoverIncentivos", { itensCheckados: itens },
            function (data) {
                if (data !== "") {
                    $('#gridIncentivosSelecionadosModal').html(data);
                }
                atualizaMensagemModalIncentivo();
                LoadingOff();
            }, function () {
                atualizaMensagemModalIncentivo();
                LoadingOff();
            }
        );
    }
}

// Retorna a lista dos ids de pedidos checkados de um tipo de checkBox
function retornaListaDeIncentivosCheckados(idCheck) {

    var itensChecados = [];
    var size = idCheck.length;

    $('[id^="' + idCheck + '"]').each(function () {
        var $this = $(this);

        if ($this.is(":checked")) {
            itensChecados.push($this.attr("id").substr(size));
        }
    });

    return itensChecados;
}

// Acao do botao de concluir a insercao de incentivos
function concluirInsercaoIncentivos() {
    postAjax("Incentivo", "ValidarIncentivos", {},
        function (data) {
            $('#modal').modal('hide');
            atualizaMensagem();
            LoadingOff();
        }, function () {
            atualizaMensagemModalIncentivo();
            LoadingOff();
        }
    );
}