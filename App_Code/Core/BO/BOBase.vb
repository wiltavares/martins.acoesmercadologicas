﻿Public Class BOBase(Of T As {Class, New})

    Public daoBase As DAOBase(Of T)

    Public Function ListT(ByVal param As T) As List(Of T)

        Return daoBase.ListT(param)
    End Function

    Public Function SelectT(ByVal param As T) As T

        Return daoBase.SelectT(param)
    End Function

    Public Function InsertT(vo As T, Optional startTransaction As Boolean = True) As T
        Try
            If startTransaction Then
                daoBase.connector.BeginTransaction()
                vo = daoBase.InsertT(vo)
                daoBase.connector.Commit()
            Else
                vo = daoBase.InsertT(vo)
            End If
            Return vo
        Catch ex As Exception
            daoBase.connector.Rollback()

            Throw ex
        End Try
    End Function

    Public Function UpdateT(vo As T, Optional startTransaction As Boolean = True) As T
        Try
            If startTransaction Then
                daoBase.connector.BeginTransaction()
                vo = daoBase.UpdateT(vo)
                daoBase.connector.Commit()
            Else
                vo = daoBase.UpdateT(vo)
            End If
            Return vo
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

    End Function

    Public Sub DeleteT(vo As T, Optional startTransaction As Boolean = True)

        If startTransaction Then
            daoBase.connector.BeginTransaction()
            daoBase.DeleteT(vo)
            daoBase.connector.Commit()
        Else
            daoBase.DeleteT(vo)
        End If

    End Sub
End Class
