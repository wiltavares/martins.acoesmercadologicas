﻿Public Class PageSettings

    Property PageNumber As Integer?
    Property PageSize As Integer?

    ''' <summary>
    ''' Nome da propriedade + C para Crescente e D para Descrescente
    ''' EX: NOMCLIC  = Ordenação pela coluna nome do cliente de forma Crescente
    ''' </summary>
    ''' <returns></returns>
    Property OrderBy As String

    ''' <summary>
    ''' Inicializa o objeto com as configuracoes default de pagina
    ''' </summary>
    Public Sub New()
        PageNumber = Constantes.PAGE_NUMBER_DEFAULT
        PageSize = Constantes.PAGE_SIZE_DEFAULT
    End Sub

End Class
