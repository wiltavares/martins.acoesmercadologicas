﻿Imports PagedList
Public Class GridSettings(Of T)

    Property Filter As T
    Property ListPaged As PagedList.IPagedList(Of T)
    Property ResultID As String
    Property PaginationID As String
    Property ActionName As String
    Property ControllerName As String
    Property Page As PageSettings
    Property TotalCount As Integer

    ''' <summary>
    ''' Inicializa o objeto com as configuracoes default de pagina
    ''' </summary>
    Public Sub New()
        ResultID = "resultadosView"
        PaginationID = "paginacaoView"
        ActionName = "Pesquisar"
        Page = New PageSettings()
        TotalCount = 0
    End Sub

    Public Sub New(divID As String, actionName As String)
        ResultID = divID
        PaginationID = divID
        actionName = actionName
        Page = New PageSettings()
        TotalCount = 0
    End Sub

    Public Sub New(filterPage As T, listPage As PagedList.IPagedList(Of T))
        Filter = filterPage
        ListPaged = listPage
        ResultID = "resultadosView"
        PaginationID = "paginacaoView"
        ActionName = "Pesquisar"
        Page = New PageSettings()
        TotalCount = 0
    End Sub

    Public Sub New(divID As String, nomeAcao As String, filterPage As T, listPage As PagedList.IPagedList(Of T), Optional nomeController As String = Nothing)
        Filter = filterPage
        ListPaged = listPage
        ResultID = divID
        PaginationID = divID
        ActionName = nomeAcao
        ControllerName = nomeController
        Page = New PageSettings()
        TotalCount = 0
    End Sub

    Public Shared Function EmptyGrid() As GridSettings(Of T)
        Dim objectFilter As T = Activator.CreateInstance(Of T)
        Dim emptyGridPage As IEnumerable(Of T) = New List(Of T)
        Return New GridSettings(Of T)(objectFilter, emptyGridPage.ToPagedList(1, 1))
    End Function

    Public Shared Function EmptyGrid(divID As String, actionName As String, Optional controllerName As String = Nothing) As GridSettings(Of T)
        Dim objectFilter As T = Activator.CreateInstance(Of T)
        Dim emptyGridPage As IEnumerable(Of T) = New List(Of T)
        Return New GridSettings(Of T)(divID, actionName, objectFilter, emptyGridPage.ToPagedList(1, 1), controllerName)
    End Function


End Class
