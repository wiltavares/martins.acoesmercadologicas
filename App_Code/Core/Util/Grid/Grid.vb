﻿Imports System.Reflection
Imports PagedList

Public Class Grid

    ''' <summary>
    ''' Retorna uma lista paginada de acordo com as configuracoes da pagina
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="listaObjetos"></param>
    ''' <param name="pageConf"></param>
    ''' <returns></returns>
    Public Shared Function PaginarLista(Of T)(listaObjetos As IEnumerable(Of T), pageConf As PageSettings) As IEnumerable(Of T)
        pageConf = If(pageConf, New PageSettings())
        If pageConf.OrderBy IsNot Nothing And pageConf.OrderBy IsNot String.Empty Then

            Dim typeObject = GetType(T)
            Dim order As String = pageConf.OrderBy(pageConf.OrderBy.Length - 1)
            Dim orderBy = pageConf.OrderBy.Substring(0, pageConf.OrderBy.Length - 1)
            Dim propertyInfo = typeObject.GetProperty(pageConf.OrderBy)

            If order.Equals("C") Then
                listaObjetos = listaObjetos.OrderBy(Function(x) GetPropertyValue(x, orderBy)).ToList()
            Else
                listaObjetos = listaObjetos.OrderByDescending(Function(x) GetPropertyValue(x, orderBy)).ToList()
            End If

        End If

        Return listaObjetos.ToPagedList(pageConf.PageNumber, pageConf.PageSize)
    End Function

    ''' <summary>
    ''' Retorna o valor de uma propriedade de um objeto
    ''' </summary>
    ''' <param name="obj"></param>
    ''' <param name="propertyName"></param>
    ''' <returns></returns>
    Public Shared Function GetPropertyValue(obj As Object, propertyName As String) As Object
        Try
            For Each prop As PropertyInfo In propertyName.Split("."c).[Select](Function(s) obj.[GetType]().GetProperty(s))
                obj = prop.GetValue(obj, Nothing)
            Next
            Return obj
        Catch generatedExceptionName As NullReferenceException
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Retorna o icone decrescente/ crescente caso a coluna esteja ordenada
    ''' </summary>
    ''' <param name="propertyOrder"></param>
    ''' <param name="orderBy"></param>
    ''' <returns></returns>
    Public Shared Function AtribuirIconeOrdenacao(propertyOrder As String, orderBy As String) As String
        Return IIf(Not IsNothing(orderBy) AndAlso (orderBy = propertyOrder & "C" OrElse orderBy = propertyOrder & "D"),
                  IIf(orderBy = propertyOrder & "C", "selected-column sorting_asc", "selected-column sorting_desc"), "sorting")
    End Function

    ''' <summary>
    ''' Atribui a class active ao tamanho de pagina escolhido
    ''' </summary>
    ''' <param name="pageSizeUrl"></param>
    ''' <param name="pageSize"></param>
    ''' <returns></returns>
    Public Shared Function AtivarPageSize(pageSizeUrl As Object, pageSize As Integer) As String
        Return IIf(pageSizeUrl = pageSize OrElse (IsNothing(pageSizeUrl) And pageSize = Constantes.PAGE_SIZE_DEFAULT), "active", "")
    End Function

    ''' <summary>
    ''' Atribui o metodo de ordenacao para as colunas do grid
    ''' </summary>
    ''' <param name="propertyOrder"></param>
    ''' <param name="orderBy"></param>
    ''' <returns></returns>
    Public Shared Function OrdenarColuna(propertyOrder As String, orderBy As String, Optional resultID As String = "resultadosView") As String
        Dim orderByParam = IIf(Not IsNothing(orderBy) AndAlso orderBy IsNot String.Empty AndAlso orderBy = propertyOrder & "C", propertyOrder & "D", propertyOrder & "C")
        Return "javascript:ordenarGrid_" + resultID + "('" + orderByParam.ToString() + "')"
    End Function

End Class
