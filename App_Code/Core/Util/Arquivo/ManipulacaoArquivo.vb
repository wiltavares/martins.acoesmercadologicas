﻿Imports System.IO
Imports Microsoft.VisualBasic
Imports NPOI.HSSF.UserModel

Public Class ManipulacaoArquivo
    Public Shared Function OpenSampleFileStream(sampleFileName As String) As Stream
        Return New FileStream(sampleFileName, FileMode.Open)
    End Function
    Public Shared Function OpenSampleWorkbook(sampleFileName As String) As HSSFWorkbook
        Return New HSSFWorkbook(OpenSampleFileStream(sampleFileName))
    End Function
End Class
