﻿Imports System.Runtime.CompilerServices

Public Module ModuleUtil

    <Extension()>
    Public Function ListCopy(Of T)(ByVal lista As List(Of T)) As List(Of T)
        Dim listaNova = New List(Of T)
        For Each item In lista
            listaNova.Add(item)
        Next
        Return listaNova
    End Function

End Module
