﻿Namespace Core

    Public Class Alert
        Public Const TempDataKey As String = "TempDataAlerts"

        Private m_AlertStyle As String
        Private m_Message As String
        Private m_Exception As Exception
        Private m_Dismissable As Boolean
        Private m_autoclose As Boolean

        Public Property AlertStyle() As String
            Get
                Return m_AlertStyle
            End Get
            Set(value As String)
                m_AlertStyle = value
            End Set
        End Property

        Public Property Message() As String
            Get
                Return m_Message
            End Get
            Set(value As String)
                m_Message = value
            End Set
        End Property

        Public Property Exception() As Exception
            Get
                Return m_Exception
            End Get
            Set(value As Exception)
                m_Exception = value
            End Set
        End Property

        Public Property Dismissable() As Boolean
            Get
                Return m_Dismissable
            End Get
            Set(value As Boolean)
                m_Dismissable = value
            End Set
        End Property
        Public Property AutoClose() As Boolean
            Get
                Return m_autoclose
            End Get
            Set(value As Boolean)
                m_autoclose = value
            End Set
        End Property

        Public Shared Function CreateAlert(alertStyle As String, Optional message As String = Nothing, Optional exception As Exception = Nothing, Optional dismissable As Boolean = True, Optional autoClose As Boolean = True) As List(Of Alert)

            Dim alerts = New List(Of Alert)()
            Dim alert = New Alert()

            alert.AlertStyle = alertStyle
            alert.Message = message
            alert.Dismissable = dismissable
            alert.Exception = exception
            alert.AutoClose = autoClose

            alerts.Add(alert)
            Return alerts

        End Function

    End Class

End Namespace
