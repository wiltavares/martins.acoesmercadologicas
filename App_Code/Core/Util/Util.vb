﻿Imports System.Reflection
Imports System.Text.RegularExpressions

Public Class Util

    Public Shared Function RemoverCharEspecial(valor As String) As String
        If (Not String.IsNullOrWhiteSpace(valor)) Then
            Return Regex.Replace(valor, "[^0-9a-zA-Z]+", "")
        End If
        Return Nothing

    End Function

    Public Shared Sub MergeObject(Of T As {Class, New})(ByRef source As T, ByRef destination As T)

        Dim properties As List(Of PropertyInfo) = GetType(T).GetProperties().ToList()

        Dim defaltValues As New T()
        For Each item In properties
            Dim value As Object = item.GetValue(source, Nothing)

            If (value <> (item.GetValue(defaltValues, Nothing))) Then
                item.SetValue(destination, value, Nothing)
            End If
        Next
    End Sub
    Public Shared Sub CalVlrTot(acao As AcaoComercialViewModel)

        Dim Vlr As Decimal
        Dim vlrTotOcd As Decimal
        Dim VlrTotBde As Decimal
        Dim VlrCli As Decimal
        Dim vlrTotCli As Decimal

        Try

            'verifica se há itens inseridos 
            If acao.ItensOrcamento.Count >= 1 Then
                'percorre todas as linhas do grid
                For Each mercadoria In acao.ItensOrcamento
                    'verifica se não é brinde 
                    If mercadoria.INDBDEOPEDSNACOCMC = 0 Then
                        VlrCli = mercadoria.QDEITEOCDACOCMC * mercadoria.VLRPTCCLIITEACOCMC ' pega o valor total do item 
                        If mercadoria.VLRUNTITEOCDACOCMC - mercadoria.VLRPTCCLIITEACOCMC <= 0 Then
                            Vlr = 0
                        Else
                            Vlr = mercadoria.QDEITEOCDACOCMC * (mercadoria.VLRUNTITEOCDACOCMC - mercadoria.VLRPTCCLIITEACOCMC) ' pega o valor total do item 
                        End If
                        vlrTotCli = vlrTotCli + VlrCli
                        vlrTotOcd = vlrTotOcd + Vlr
                    Else
                        VlrCli = mercadoria.QDEITEOCDACOCMC * mercadoria.VLRPTCCLIITEACOCMC ' pega o valor total do item 
                        Vlr = mercadoria.QDEITEOCDACOCMC * mercadoria.VLRPTCFRNITEACOCMC ' pega o valor total do item 
                        vlrTotCli = vlrTotCli + VlrCli
                        VlrTotBde = VlrTotBde + Vlr
                    End If
                Next

                acao.ValorTotal = vlrTotOcd
                acao.ValorTotalBrinde = VlrTotBde
                acao.ValorCliente = vlrTotCli
            Else
                acao.ValorTotal = 0
                acao.ValorTotalBrinde = 0
                acao.ValorCliente = 0
            End If
        Catch ex As Exception

        End Try

    End Sub

    Public Shared Sub AtuSldOcd(acao As AcaoComercialViewModel)

        Dim Vlr As Decimal
        Dim vlrAux As Decimal
        Dim VlrTotSldOcd As Decimal

        'verifica se há itens inseridos 
        If acao.ItensOrcamento.Count >= 1 Then
            'percorre todas as linhas do grid
            For Each mercadoria In acao.ItensOrcamento
                'verifica se não é brinde 
                'If TabIteOcdAcoCmc.GetData(cont, "indbde") = 0 Then
                Vlr = mercadoria.ValorTotal ' pega o valor total do item 
                'pega o valor total utilizado
                vlrAux = mercadoria.QDEUTZOPEDSNACOCMC 'está sendo atualizada a tabela com o valor total do item neste campo

                'calcula o saldo da linha referente ao item 
                vlrAux = Vlr - vlrAux
                If mercadoria.INDBDEOPEDSNACOCMC = 0 Then
                    VlrTotSldOcd = VlrTotSldOcd + vlrAux 'pega o saldo do orçamento 
                End If
                acao.SaldoOrcamento = vlrAux
            Next
            acao.SaldoOrcamento = VlrTotSldOcd
        Else
            acao.SaldoOrcamento = 0
        End If
    End Sub

    Public Shared Function IsMobile(request As HttpRequestBase) As Boolean

        Dim agent = request.UserAgent

        Return agent.Contains("Android") OrElse
               agent.Contains("iPhone") OrElse
               agent.Contains("Mobi")
    End Function

End Class
