﻿Public Class MappedException
    Inherits Exception

    Private Property m_ExceptionType As String

    Public Property ExceptionType() As String
        Get
            Return m_ExceptionType
        End Get
        Set
            m_ExceptionType = Value
        End Set
    End Property

    Public Sub New(type As String)
        MyBase.New()
        m_ExceptionType = type
    End Sub

    Public Sub New(message As String, type As String)
        MyBase.New(message)
        m_ExceptionType = type
    End Sub

End Class
