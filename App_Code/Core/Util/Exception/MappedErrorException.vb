﻿Public Class MappedErrorException
    Inherits MappedException

    Public Sub New()
        MyBase.New("Ocorreu um erro inesperado. Contate o administrador do sistema para mais informações.", AlertStyle.Danger)
    End Sub

    Public Sub New(message As String)
        MyBase.New(message, AlertStyle.Danger)
    End Sub

End Class
