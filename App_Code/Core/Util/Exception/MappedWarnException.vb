﻿Public Class MappedWarnException
    Inherits MappedException

    Private Property m_ExceptionType As String = AlertStyle.Warning

    Public Sub New()
        MyBase.New("Ocorreu um aviso inesperado. Contate o administrador do sistema para mais informações.", AlertStyle.Warning)
    End Sub

    Public Sub New(message As String)
        MyBase.New(message, AlertStyle.Warning)
    End Sub

End Class
