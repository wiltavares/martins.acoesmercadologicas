﻿Imports System.Web
Imports System.Security.Principal
Imports System.Web.Security

Public Class ControleAcesso
    Private Shared listMenu As List(Of Menu) = Menu.Menus()

    Public Shared Function CodFnc() As Decimal
        Dim authCookie As HttpCookie = HttpContext.Current.Request.Cookies(FormsAuthentication.FormsCookieName)
        If authCookie Is Nothing OrElse authCookie.Value = "" Then
            Return Nothing
        End If

        Dim authTicket As FormsAuthenticationTicket
        Try
            authTicket = FormsAuthentication.Decrypt(authCookie.Value)
        Catch
            Return Nothing
        End Try

        ' retrieve roles from UserData
        Dim rCodFnc As Decimal = authTicket.UserData.Split("|")(0)
        Return rCodFnc
    End Function

    Public Shared Function Menus() As List(Of Menu)
        Return listMenu
    End Function
End Class

Public Class Menu
    Property cdMenu As String
    Property NomMenu As String
    Property NumOrdem As Int16
    Property cdMenuPai As Nullable(Of Int16)
    Property Metodo As String
    Property Controler As String
    Property Parametro As Object
    Property ClassIcone As String

    Public Shared Function Menus() As List(Of Menu)

        Dim listMenu As New List(Of Menu)
        'Menus
        listMenu.Add(New Menu With {.cdMenu = 1, .NomMenu = "Cadastros", .NumOrdem = 1, .ClassIcone = "menu-icon fa fa-folder-open-o"})
        listMenu.Add(New Menu With {.cdMenu = 39, .NomMenu = "Aprovações", .NumOrdem = 2, .ClassIcone = "menu-icon fa fa-check"})
        listMenu.Add(New Menu With {.cdMenu = 2, .NomMenu = "Ação Mercadológica", .NumOrdem = 5, .ClassIcone = String.Format("menu-icon {0}", Constantes.Icone_Acao)})
        listMenu.Add(New Menu With {.cdMenu = 30, .NomMenu = "Pré-Acordo", .NumOrdem = 7, .ClassIcone = String.Format("menu-icon {0}", Constantes.Icone_PreAcordo)})
        listMenu.Add(New Menu With {.cdMenu = 42, .NomMenu = "Promoções", .NumOrdem = 8, .ClassIcone = "menu-icon fa fa-gift"})
        listMenu.Add(New Menu With {.cdMenu = 3, .NomMenu = "Controle de Acesso", .NumOrdem = 10, .ClassIcone = "menu-icon fa fa-gears"})
        listMenu.Add(New Menu With {.cdMenu = 34, .NomMenu = "Relatórios", .NumOrdem = 15, .ClassIcone = "menu-icon fa fa-bar-chart"})


        'Sub-Menus
        '****************Cadastros
        listMenu.Add(New Menu With {.cdMenu = 5, .NomMenu = "Evento", .NumOrdem = 1, .cdMenuPai = 1, .Metodo = "Index", .Controler = "Evento"})
        listMenu.Add(New Menu With {.cdMenu = 6, .NomMenu = "Tipo de Evento", .NumOrdem = 3, .cdMenuPai = 1, .Metodo = "Index", .Controler = "TipoEvento"})
        listMenu.Add(New Menu With {.cdMenu = 7, .NomMenu = "Tipo de Fluxo", .NumOrdem = 5, .cdMenuPai = 1, .Metodo = "Index", .Controler = "TipoFluxo"})
        listMenu.Add(New Menu With {.cdMenu = 8, .NomMenu = "Tipo de Operação", .NumOrdem = 7, .cdMenuPai = 1, .Metodo = "Index", .Controler = "TipoOperacao"})
        listMenu.Add(New Menu With {.cdMenu = 9, .NomMenu = "Plano de Marketing", .NumOrdem = 9, .cdMenuPai = 1, .Metodo = "Index", .Controler = "PlanoDeMarketing"})
        listMenu.Add(New Menu With {.cdMenu = 10, .NomMenu = "Parâmetros", .NumOrdem = 11, .cdMenuPai = 1, .Metodo = "Index", .Controler = "Parametros"})
        listMenu.Add(New Menu With {.cdMenu = 11, .NomMenu = "Políticas", .NumOrdem = 13, .cdMenuPai = 1, .Metodo = "Index", .Controler = "PoliticasAcaoComercial"})
        listMenu.Add(New Menu With {.cdMenu = 16, .NomMenu = "Aprovadores Extras", .NumOrdem = 15, .cdMenuPai = 1, .Metodo = "Index", .Controler = "AprovadoresExtra"})
        listMenu.Add(New Menu With {.cdMenu = 18, .NomMenu = "Funcionário x Ger. Marketing", .NumOrdem = 17, .cdMenuPai = 1, .Metodo = "Index", .Controler = "RelacaoFuncionarioGerenteMarketing"})

        'Sub-Menus
        '****************Aprovações
        listMenu.Add(New Menu With {.cdMenu = 40, .NomMenu = "Minhas Aprovações", .NumOrdem = 1, .cdMenuPai = 39, .Metodo = "Aprovacoes", .Controler = "AcaoComercial"})

        'Sub-Menus
        '****************Ação
        listMenu.Add(New Menu With {.cdMenu = 12, .NomMenu = "Consulta", .NumOrdem = 2, .cdMenuPai = 2, .Metodo = "Consulta", .Controler = "AcaoComercial"})
        listMenu.Add(New Menu With {.cdMenu = 14, .NomMenu = "Nova", .NumOrdem = 4, .cdMenuPai = 2, .Metodo = "CreateEdit", .Controler = "AcaoComercial", .Parametro = New With {.id = Nothing}})
        listMenu.Add(New Menu With {.cdMenu = 17, .NomMenu = "Aprovações", .NumOrdem = 6, .cdMenuPai = 2, .Metodo = "MinhasAprovacoes", .Controler = "AcaoComercial"})

        'Sub-Menus
        '****************Controle de Acesso
        listMenu.Add(New Menu With {.cdMenu = 3, .NomMenu = "Controle de Acesso", .NumOrdem = 1, .cdMenuPai = 3, .Metodo = "Index", .Controler = "ControleAcesso"})
        listMenu.Add(New Menu With {.cdMenu = 37, .NomMenu = "Grupo de Acesso", .NumOrdem = 2, .cdMenuPai = 3, .Metodo = "Index", .Controler = "GrupoAcesso"})

        'Sub-Menus
        '****************Pré-Acordo
        listMenu.Add(New Menu With {.cdMenu = 31, .NomMenu = "Consulta", .NumOrdem = 2, .cdMenuPai = 30, .Metodo = "PreAcordoConsulta", .Controler = "AcaoComercial"})
        listMenu.Add(New Menu With {.cdMenu = 32, .NomMenu = "Novo", .NumOrdem = 4, .cdMenuPai = 30, .Metodo = "PreAcordoCreateEdit", .Controler = "AcaoComercial", .Parametro = New With {.id = Nothing}, .ClassIcone = "menu-icon fa fa-pencil-square-o"})
        listMenu.Add(New Menu With {.cdMenu = 33, .NomMenu = "Aprovações", .NumOrdem = 6, .cdMenuPai = 30, .Metodo = "PreAcordoMinhasAprovacoes", .Controler = "AcaoComercial"})
        listMenu.Add(New Menu With {.cdMenu = 36, .NomMenu = "Simula Funding", .NumOrdem = 8, .cdMenuPai = 30, .Metodo = "Index", .Controler = "SimulaFunding"})

        'Sub-Menus
        '****************Promoções
        listMenu.Add(New Menu With {.cdMenu = 43, .NomMenu = "Consulta", .NumOrdem = 1, .cdMenuPai = 42, .Metodo = "Index", .Controler = "Promocao"})
        listMenu.Add(New Menu With {.cdMenu = 44, .NomMenu = "Pendências", .NumOrdem = 2, .cdMenuPai = 42, .Metodo = "Index", .Controler = "PromocaoWizard"})
        listMenu.Add(New Menu With {.cdMenu = 45, .NomMenu = "Abastecimento", .NumOrdem = 2, .cdMenuPai = 42, .Metodo = "Index", .Controler = "Abastecimento"})


        'Sub-Menus
        '****************Relatórios
        listMenu.Add(New Menu With {.cdMenu = 24, .NomMenu = "Consulta Saldo Ação", .NumOrdem = 8, .cdMenuPai = 34, .Metodo = "Index", .Controler = "ConsultaSaldoAcao"})
        listMenu.Add(New Menu With {.cdMenu = 25, .NomMenu = "Consulta Gasto Ação", .NumOrdem = 10, .cdMenuPai = 34, .Metodo = "Index", .Controler = "ConsultaGastoAcao"})
        listMenu.Add(New Menu With {.cdMenu = 26, .NomMenu = "Consulta Saldo Ação X Fornecedor", .NumOrdem = 12, .cdMenuPai = 34, .Metodo = "Index", .Controler = "ConsultaSaldoAcaoFornecedor"})
        listMenu.Add(New Menu With {.cdMenu = 27, .NomMenu = "Consulta Saldo Carimbo", .NumOrdem = 14, .cdMenuPai = 34, .Metodo = "Index", .Controler = "ConsultaSaldoCarimbo"})
        listMenu.Add(New Menu With {.cdMenu = 28, .NomMenu = "Consulta Extrato Carimbo por Evento", .NumOrdem = 16, .cdMenuPai = 34, .Metodo = "Index", .Controler = "ConsultaExtratoCarimboEvento"})
        listMenu.Add(New Menu With {.cdMenu = 35, .NomMenu = "Consulta Acompanhamento Pré-Acordo", .NumOrdem = 20, .cdMenuPai = 34, .Metodo = "Index", .Controler = "ConsultaAcompanhamentoPreAcordo"})
        listMenu.Add(New Menu With {.cdMenu = 38, .NomMenu = "Consulta Extrato de Verbas", .NumOrdem = 25, .cdMenuPai = 34, .Metodo = "Index", .Controler = "ConsultaExtratoVerba"})
        listMenu.Add(New Menu With {.cdMenu = 41, .NomMenu = "Consulta Acompanhamento BC com Funding", .NumOrdem = 30, .cdMenuPai = 34, .Metodo = "Index", .Controler = "ConsultaAcompanhamentoBcComFunding"})

        Return listMenu
    End Function
End Class
