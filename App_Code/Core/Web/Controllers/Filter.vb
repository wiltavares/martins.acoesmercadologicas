﻿Imports System.Web.Mvc
Imports System.Diagnostics

Public Class Filter
    Inherits ActionFilterAttribute
    Implements IActionFilter

    Public Overrides Sub OnActionExecuting(filterContext As ActionExecutingContext)
        Debug.WriteLine("OnActionExecuting", "Action Filter Log")
    End Sub

    Public Overrides Sub OnActionExecuted(filterContext As ActionExecutedContext)
        Debug.WriteLine("OnActionExecuted", "Action Filter Log")
    End Sub

End Class
