﻿Imports System.IO
Imports System.Web.Mvc

<Authorize>
    Public Class AbstractController
    Inherits Controller

#Region "Mensagens de Alerta"
    ''' <summary>
    ''' Exibe mensagem de sucesso na tela
    ''' </summary>
    ''' <param name="message"></param>
    Public Sub Success(Optional message As String = Nothing, Optional exception As Exception = Nothing)
        AddAlert(AlertStyle.Success, message, exception)
    End Sub

    ''' <summary>
    ''' Exibe mensagem de sucesso na tela
    ''' </summary>
    ''' <param name="message"></param>
    Public Sub SuccessNotDismissible(Optional message As String = Nothing, Optional exception As Exception = Nothing)
        AddAlert(AlertStyle.Success, message, exception, False)
    End Sub

    ''' <summary>
    ''' Exibe mensagem de informação na tela
    ''' </summary>
    ''' <param name="message"></param>
    Public Sub Information(Optional message As String = Nothing, Optional exception As Exception = Nothing)
        AddAlert(AlertStyle.Information, message, exception)
    End Sub

    ''' <summary>
    ''' Exibe mensagem de warning na tela
    ''' </summary>
    ''' <param name="exception"></param>
    Public Sub Warning(exception As Exception)
        AddAlert(AlertStyle.Warning, Nothing, exception)
    End Sub

    ''' <summary>
    ''' Exibe mensagem de warning na tela
    ''' </summary>
    ''' <param name="message"></param>
    Public Sub Warning(message)
        AddAlert(AlertStyle.Warning, message)
    End Sub

    ''' <summary>
    ''' Exibe mensagem de erro
    ''' </summary>
    ''' <param name="exception"></param>
    Public Sub Danger(exception As Exception)
        AddAlert(AlertStyle.Danger, Nothing, exception)
    End Sub

    ''' <summary>
    ''' Exibe mensagem de erro
    ''' </summary>
    ''' <param name="message"></param>
    Public Sub Danger(message As String)
        AddAlert(AlertStyle.Danger, message)
    End Sub

    Public Sub Message(style As String, Optional message As String = Nothing, Optional exception As Exception = Nothing)
        AddAlert(style, message, exception)
    End Sub

    Public Sub AddAlert(alertStyle As String, Optional message As String = Nothing, Optional exception As Exception = Nothing, Optional dismissable As Boolean = True, Optional autoClose As Boolean = True)

        Dim alerts = If(TempData.ContainsKey(Core.Alert.TempDataKey), DirectCast(TempData(Core.Alert.TempDataKey), List(Of Core.Alert)), New List(Of Core.Alert)())
        Dim alert = New Core.Alert()

        alert.AlertStyle = alertStyle
        alert.Message = message
        alert.Dismissable = dismissable
        alert.Exception = exception
        alert.AutoClose = autoClose

        alerts.Add(alert)
        TempData(Core.Alert.TempDataKey) = alerts

    End Sub

#End Region

#Region "Tratamento de Excecoes Genericas"

    ''' <summary>
    ''' Retorna uma mensagem na tela de acordo com o tipo de exceçao interceptada. Apenas para exceções mapeadas.
    ''' </summary>
    ''' <param name="filterContext"></param>
    Protected Overrides Sub OnException(filterContext As ExceptionContext)

        Dim exception = filterContext.Exception
        Dim tipoBase = exception.GetType().BaseType
        If tipoBase.Equals(GetType(MappedException)) Then

            Dim mappedException As MappedException = exception
            Message(mappedException.ExceptionType, filterContext.Exception.Message)

            Dim actionName = filterContext.RouteData.Values("action")
            Dim result = New ViewResult()
            result.ViewName = actionName
            result.TempData = filterContext.Controller.TempData
            filterContext.Result = result
            filterContext.HttpContext.Response.Clear()
            filterContext.HttpContext.Response.StatusCode = 500
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = True
            filterContext.ExceptionHandled = True
        End If

    End Sub
#End Region

#Region "Paginacao e Ordenacao"

    ''' <summary>
    ''' Faz a paginacao de uma lista de objetos de acordo com as configuracoes da pagina
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="listaObjetos"></param>
    ''' <param name="pageConf"></param>
    ''' <returns></returns>
    Public Function PaginarLista(Of T)(listaObjetos As IEnumerable(Of T), Optional pageConf As PageSettings = Nothing) As IEnumerable(Of T)
        pageConf = If(pageConf, New PageSettings())
        Return Grid.PaginarLista(listaObjetos, pageConf)
    End Function

    ''' <summary>
    ''' Retorna o objeto atualizado e paginado de acordo com os parametros para um Grid
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="listaObjetos"></param>
    ''' <param name="pageConf"></param>
    ''' <returns></returns>
    Public Function PaginarEAtualizarGrid(Of T)(listaObjetos As IEnumerable(Of T), pageConf As PageSettings, Optional filtro As T = Nothing, Optional gridConf As GridSettings(Of T) = Nothing) As GridSettings(Of T)
        Dim listaPaginada = PaginarLista(listaObjetos, pageConf)
        gridConf = If(gridConf, New GridSettings(Of T)())
        gridConf.ListPaged = listaPaginada
        gridConf.Filter = filtro
        gridConf.Page = pageConf
        gridConf.TotalCount = listaObjetos.Count
        Return gridConf
    End Function

    ''' <summary>
    ''' Retorna o objeto atualizado e paginado de acordo com os parametros para um Grid
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="listaObjetos"></param>
    ''' <param name="gridConf"></param>
    ''' <returns></returns>
    Public Function PaginarEAtualizarGrid(Of T)(listaObjetos As IEnumerable(Of T), gridConf As GridSettings(Of T)) As GridSettings(Of T)
        Dim listaPaginada = PaginarLista(listaObjetos, gridConf.Page)
        gridConf = If(gridConf, New GridSettings(Of T)())
        gridConf.ListPaged = listaPaginada
        gridConf.Filter = gridConf.Filter
        gridConf.Page = gridConf.Page
        gridConf.TotalCount = listaObjetos.Count
        Return gridConf
    End Function

    ''' <summary>
    ''' Abre um modal de confirmacao com os parametros do model
    ''' </summary>
    ''' <param name="model"></param>
    ''' <returns></returns>
    <HttpPost()>
    Public Function AbreModalConfirmacao(ByVal model As ModalConfirmacaoViewModel) As PartialViewResult
        Try
            model = If(model, New ModalConfirmacaoViewModel(Nothing))
            Return PartialView("~/Views/Shared/_ModalConfirmacaoGenerico.vbhtml", model)
        Catch ex As Exception
            Danger(ex)
            Return Nothing
        End Try
    End Function
#End Region

End Class