﻿Imports System.Web.Mvc
Imports System.Reflection

<AttributeUsage(AttributeTargets.Method, AllowMultiple:=False, Inherited:=True)>
Public Class MultipleButtonAttribute
    Inherits ActionNameSelectorAttribute
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String
    Public Property Argument() As String
        Get
            Return m_Argument
        End Get
        Set(value As String)
            m_Argument = value
        End Set
    End Property
    Public m_Argument As String

    Public Overrides Function IsValidName(controllerContext As ControllerContext, actionName As String, methodInfo As MethodInfo) As Boolean
        Dim isValidName__1 = False
        Dim keyValue = String.Format("{0}:{1}", Name, Argument)
        Dim value = controllerContext.Controller.ValueProvider.GetValue(keyValue)

        If value IsNot Nothing Then
            controllerContext.Controller.ControllerContext.RouteData.Values(Name) = Argument
            isValidName__1 = True
        End If

        Return isValidName__1
    End Function
End Class
