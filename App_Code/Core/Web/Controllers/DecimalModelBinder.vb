﻿Imports System.Globalization
Imports System.Web.Mvc

Public Class DecimalModelBinder
    Implements IModelBinder

    Public Function BindModel(controllerContext As ControllerContext, bindingContext As ModelBindingContext) As Object Implements IModelBinder.BindModel
        Dim valueResult As ValueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName)
        Dim modelState As New ModelState() With {.Value = valueResult}
        Dim actualValue As Object = Nothing
        Try
            If (valueResult Is Nothing OrElse valueResult.AttemptedValue.Equals(String.Empty)) Then
                actualValue = Nothing
            Else
                actualValue = Convert.ToDecimal(valueResult.AttemptedValue, CultureInfo.CurrentCulture)
            End If
        Catch e As FormatException
            modelState.Errors.Add(e)
        End Try

        bindingContext.ModelState.Add(bindingContext.ModelName, modelState)
        Return actualValue
    End Function
End Class
