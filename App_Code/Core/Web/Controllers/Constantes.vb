﻿Imports System.Reflection
Imports System.Web.Mvc.Ajax

Public Class Constantes

#Region "Fornecedor de Marketing e Despesas Martins"
    Public Const EMPENHO_FORNECEDOR_MARKETING As String = "78"
    Public Const DESPESAS_MARTINS As Decimal = 999999
#End Region

#Region "Paginação e Ordenacao"
    Public Const VIEW_PAGINACAO = "~/Views/Shared/_Paginacao10.vbhtml"
    Public Const VIEW_PAGINACAO5 = "~/Views/Shared/_Paginacao5.vbhtml"
    Public Const PAGE_NUMBER_DEFAULT = 1
    Public Const PAGE_SIZE_DEFAULT = 10
    Public Const PAGE_SIZE_DEFAULT5 = 5
    Public Const PAGE_SIZE_DEFAULT1000 = 1000
#End Region

#Region "Modal de Confirmacao"
    Public Const VIEW_MODAL_CONFIRMACAO = "~/Views/Shared/__ModalConfirmacao.vbhtml"
#End Region

#Region "Alertas"
    Public Const VIEW_ALERTAS = "~/Views/Shared/_Alerts.vbhtml"
#End Region

#Region "Loading"
    Public Const VIEW_LOADING = "~/Views/Shared/_Loading.vbhtml"
#End Region

#Region "Ajax"

    ''' <summary>
    ''' Objeto ajax padrao para paginas index
    ''' </summary>
    Public Shared AJAX_OBJECT_POST As New AjaxOptions() With {
            .OnBegin = "LoadingOn",
            .OnComplete = "LoadingOff",
            .HttpMethod = "POST"
    }

    Public Shared AJAX_OBJECT_POST_DEFAULT As New AjaxOptions() With {
            .UpdateTargetId = "resultadosView",
            .InsertionMode = InsertionMode.Replace,
            .OnBegin = "LoadingOn",
            .OnComplete = "LoadingOff_UpdateMessage",
            .HttpMethod = "POST"
    }

    Public Shared AJAX_ACAO_COMERCIAL_POST As New AjaxOptions() With {
            .UpdateTargetId = "msgGeral",
            .InsertionMode = InsertionMode.Replace,
            .OnBegin = "LoadingOn",
            .OnComplete = "LoadingOff",
            .HttpMethod = "POST"
    }
#End Region

#Region "Mensagens"
    Public Const NENHUM_REGISTRO_ENCONTRADO = "Nenhum registro encontrado para esse filtro."
    Public Const REGISTRO_INCLUIDO_SUCESSO = "Registro Incluído com sucesso!"
    Public Const REGISTRO_ENVIADO_APROVACAO = "Registro Enviado para Aprovação!"
    Public Const REGISTRO_ALTERADO_SUCESSO = "Registro Alterado com sucesso!"
    Public Const REGISTRO_EXCLUIDO_SUCESSO = "Registro Excluído com sucesso!"
    Public Const REGISTROS_EXCLUIDOS_SUCESSO = "Registros Excluídos com sucesso!"
    Public Const REGISTRO_DUPLICADO = "Este registro já foi incluído e não pode ser duplicado!"
    Public Const FILTRO_OBRIGATORIO = "Entre com algum filtro para a pesquisa!"
    Public Const FLUXO_NAO_ENCONTRADO = "Nenhum fluxo de aprovação encontrado para essa ação!"
    Public Const PLANO_NAO_DEFINIDO = "Plano de Marketing não definido"
    Public Const DEFINA_PLANO_MARKETING = "Defina um Plano de Marketing para a Ação"
    Public Const ARQUIVO_INEXISTENTE = "Arquivo inexistente"
    Public Const NAO_EXISTEM_REGISTROS_REMOVER = "Não existem registros para serem removidos."

    Public Const PEDIDO_EXISTENTE = "Pedido já selecionado!"
    Public Const INFORME_FILTRO = "Informe o filtro!"
    Public Const SELECIONAR_PEDIDO = "Selecione pelo menos um pedido!"
    Public Const INFORME_VALOR_UTILIZADO_PEDIDO = "Informar o valor utilizado de todos pedidos selecionados!"
    Public Const VALOR_UTILIZADO_MAIOR_QUE_DISPONIVEL = "O valor utilizado de um item é maior que o disponível!"

    Public Const CARIMBO_EXISTENTE = "Carimbo já selecionado!"
    Public Const SELECIONAR_CARIMBO = "Selecione pelo menos um carimbo!"
    Public Const INFORME_VALOR_UTILIZADO_CARIMBO = "Informar o valor utilizado de todos carimbos selecionados!"

    Public Const INCENTIVO_EXISTENTE = "Incentivo já selecionado!"
    Public Const SELECIONAR_INCENTIVO = "Selecione pelo menos um incentivo!"

    Public Const SELECIONAR_FORNECEDOR = "Selecione pelo menos um fornecedor!"
    Public Const SELECIONAR_FORNECEDOR_EDITAR = "Selecione um fornecedor para edição!"
    Public Const INFORME_RATEIO = "Informe o valor de Rateio!"
    Public Const RATEIO_FORA_FAIXA = "O valor deve estar entre 1 e 100"

    Public Const EXCLUI_TODOS_FORNECEDORES = "Deseja realmente remover todos os {0} fornecedores?"
    Public Const EXCLUI_FORNECEDORES_SELECIONADOS = "Deseja realmente remover todos os fornecedores selecionados na lista?"
    Public Const EXCLUI_FORNECEDOR_SEM_DADOS = "Deseja realmente remover todos os fornecedores sem dados (fornecedores de cor acinzentada)?"
    Public Const EXCLUI_FORNECEDOR_SEM_SALDO_ACORDO = "Deseja realmente remover todos os fornecedores sem Saldo de Acordo?"
    Public Const EXCLUI_FORNECEDOR_SEM_SALDO_EXTRA_ACORDO = "Deseja realmente remover todos os fornecedores sem Saldo de Extra Acordo?"
    Public Const EXCLUI_FORNECEDOR_SEM_SALDO_ACORDO_EXTRA_ACORDO = "Deseja realmente remover todos os fornecedores sem Saldo de Acordo e Extra Acordo?"

    Public Const PREMIACAO_EXISTENTE = "Premiação já associada a esse fornecedor!"
    Public Const VALIDADE_ACAOCOMERCIAL_ANTERIOR = "Data de Validade não pode ter início anterior a criação da ação."
    Public Const CANAL_OBRIGATORIO = "Canal de Venda Obrigatório."
    Public Const VLR_ACAO_MAIOR_QUE_VLR_DOS_ITENS = "Valor da ação maior que o valor dos itens do orçamento."
    Public Const SALVAR_ACAO_PARA_INSERIR_ITEM = "Para inserir um item, favor salvar a ação!"
    Public Const SALVAR_ACAO_PARA_INSERIR_MECANICA = "Para cadastrar a mecânica, favor salvar a ação!"
    Public Const VALOR_TOTAL_MINIMO_ITENS_ORCAMENTO = "O item deve ter valor total maior que 0!!!"
    Public Const VALOR_TOTAL_MAXIMO_ITENS_ORCAMENTO = "O item deve ter valor total menor que 10000000000!!!"
    Public Const VALOR_TOTAL_ITENS_ORCAMENTO = "O item deve ter valor total menor que 10000000000 e maior que 0!!!"
    Public Const ESPERAR_SALVAR_ACAO = "Os itens substitutos só podem ser inseridos após a ação ser salva."
    Public Const INFORME_CODIGO_CIDADE_CORRETAMENTE = "Código da cidade não encontrado."

    Public Const SELECIONAR_CANAL_VENDAS = "Selecione pelo menos um canal de vendas!"
    Public Const NAO_PODE_EXCLUIR_ITEM_SUBSTITUTO = "Não é possível excluir item substituto de uma ação que já foi aprovada!"
    Public Const SELECIONAR_ITENS = "Selecione no minimo uma operação para fazer o estorno!"
    Public Const ESTORNO_REALIZADO_SUCESSO = "Estornos realizados com sucesso!"
    Public Const NAO_POSSIVEL_INCLUIR_FORNECEDOR = "Não é possivel incluir mais de um fornecedor!"
    Public Const SELECIONE_PELO_MENOS_UMA_FILIAL = "Selecione pelo menos uma filial para continuar!"
    Public Const VLR_PARTICIPACAO_MAIOR_VLR_TOTAL_ORCAMENTO = "O valor da participação do fornecedor tem que ser igual ao valor do total do Orcamento!"

    Public Const SELECIONAR_MERCADORIA = "Selecione pelo menos uma mercadoria!"

    Public Shared Function FindMessage(msgName As String) As String
        Dim mensagem = GetType(Constantes).GetField(msgName)

        If (mensagem IsNot Nothing) Then
            Return mensagem.GetValue(Nothing).ToString()
        End If

        Return msgName
    End Function

#End Region

#Region "Valores de Tela"
    Public Const SIM = 1
    Public Const NAO = 0
    'Public Const FORNECEDOR = 1
    'Public Const CARIMBO = 2
    'Public Const BONIFICACAO = 3
    'Public Const PREMIACAO = 4
#End Region

#Region "Status Acao Comercial"
    Public Shared Function ConvertStatusAcaoComercial(codigo As Decimal?) As String
        Select Case codigo
            Case 1D
                Return "NOVA"
            Case 2D
                Return "ENVIANDO PARA APROVAÇÃO"
            Case 3D
                Return "EM APROVAÇÃO"
            Case 4D
                Return "REJEITADA"
            Case 5D
                Return "APROVADA"
            Case 6D
                Return "PARECER NEGATIVO"
            Case 7D
                Return "MINHAS APROVAÇÕES"
            Case Else
                Return ""
        End Select
    End Function

    Enum Status
        NOVA = 1
        ENVIANDO_PARA_APROVACAO = 2
        EM_APROVACAO = 3
        REJEITADA = 4
        APROVADA = 5
        PARECER_NEGATIVO = 6
        MINHAS_APROVACOES = 7
    End Enum

#End Region

#Region "Status Aprovacao"
    Enum IndicadorAprovacao
        Normal = 0
        Aprovacao = 1
        Parecer = 2
    End Enum

    Public Enum TipoStatusFluxoAprovacao
        Criacao = 1
        Em_Aprovacao = 3
        Aprovado = 5
        Rejeitado = 4
        Pedido_Parecer = 6
        Resposta_Parecer = 8
        Aguardando_Aprovacao = 9
        Notificacao = 7
    End Enum

    Public Shared Function ConvertStatusFluxoAprovacao(codigo As Integer) As String
        Select Case codigo
            Case 1
                Return "Geração Fluxo"
            Case 3
                Return "Em Aprovação"
            Case 4
                Return "Rejeitada"
            Case 5
                Return "Aprovada"
            Case 6
                Return "Pedido Parecer"
            Case 7
                Return "Notificação"
            Case 8
                Return "Resposta Parecer"
            Case 9
                Return "Espera Aprovação"
            Case Else
                Return ""
        End Select
    End Function

    Public Shared Function GetColorStatusFluxoAprovacao(codigo As Integer) As String
        Select Case codigo
            Case 1
                Return "black-color"
            Case 3
                Return "blue-color"
            Case 4
                Return "red-color"
            Case 5
                Return "green-color"
            Case 6
                Return "pink-color"
            Case 7
                Return "darkblue-color"
            Case 8
                Return "pink-color"
            Case 9
                Return "orange-color"
            Case Else
                Return "black-color"
        End Select
    End Function
#End Region

#Region "Fornecedores Situacao Dados"
    Enum FrnSaldo
        SemDados = 0
        SemSaldoAcordo = 1
        SemSaldoAcordoExtra = 2
        SemSaldoAcordoSaldoExtra = 3
    End Enum
    Public Shared Property Fornecedores As List(Of Frn)
    Public Shared Property FornecedoresEdl As List(Of Frn)

    Public Shared Function FornecedoresCodFrn(CodFrn As Decimal?) As List(Of Frn)
        If (CodFrn Is Nothing) Then
            Return Fornecedores
        Else
            Return Fornecedores.Where(Function(x) x.CODFRN = CodFrn).ToList()
        End If
    End Function

    Public Shared Function FornecedoresEdlCodFrn(CodFrn As Decimal?) As List(Of Frn)
        If (CodFrn Is Nothing) Then
            Return FornecedoresEdl
        Else
            Return FornecedoresEdl.Where(Function(x) x.CODFRN = CodFrn).ToList()
        End If
    End Function
#End Region

#Region "REGEX"
    Public Const RX_INTEIRO = "\d*"
    Public Const RX_DATE = "(^(((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)"
#End Region
#Region "Icones"
    Public Const Icone_Acao As String = "fa fa-line-chart"
    Public Const Icone_PreAcordo As String = "fa fa-check-square-o"

#End Region
End Class
Public Enum TipoAcao
    Fornecedor = 1
    Carimbo = 2
    PedidoBonificacao = 3
    Premiacao = 4
    PreAcordoVigencia = 5
    PreAcordoValor = 6
End Enum
Public Class Frn
    Property CODFRN As Decimal?
    Property NOMFRN As String
    ReadOnly Property CodNomFrn As String
        Get
            Return String.Format("{0} - {1}", CODFRN, NOMFRN)
        End Get
    End Property
End Class
