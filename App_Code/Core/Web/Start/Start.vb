﻿Imports Core

Public Module Start

    Public Sub BuscaFornecedores()
        Dim boFrn As RelacaoAcaoComercialxFornecedoresBO = New RelacaoAcaoComercialxFornecedoresBO
        Dim fornecedores = boFrn.BuscaFornecedores(Nothing)
        Constantes.Fornecedores = fornecedores.Select(Function(x) New Frn With {.CODFRN = x.CODFRN, .NOMFRN = x.NOMFRN}).ToList()

        Dim fornecedoresEdl = boFrn.BuscaFornecedoresEdl(Nothing)
        Constantes.FornecedoresEdl = fornecedoresEdl.Select(Function(x) New Frn With {.CODFRN = x.CODFRN, .NOMFRN = x.NOMFRN}).ToList()
    End Sub
End Module
