﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Mvc
Imports System.Web.Routing

Public Module RouteConfig
    Public Sub RegisterRoutes(ByVal routes As RouteCollection)
        routes.IgnoreRoute("{resource}.axd/{*pathInfo}")

        routes.MapRoute(
            name:="PromocaoComunicacao",
            url:="PromocaoComunicacao/Edit/{CODPMC}",
            defaults:=New With {.controller = "PromocaoComunicacao", .action = "Edit", .CODPMC = 0}
            )

        routes.MapRoute(
            name:="CriterioApuracaoEdit",
            url:="CriterioApuracao/Edit/{CODPMC}/{TIPPMC}",
            defaults:=New With {.controller = "CriterioApuracao", .action = "Edit", .CODPMC = 0, .TIPPMC = 0}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaEditMecanicaPromocao",
            url:="RelacaoAcaoMecanica/EditMecanicaPromocao/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanica", .action = "EditMecanicaPromocao", .CODACOCMC = 0, .NUMSEQRGRACOCMC = 0}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaEditMecanicaPromocaoGridRegistros",
            url:="RelacaoAcaoMecanica/EditMecanicaPromocaoGridRegistros/{CODACOCMC}/{NUMSEQRGRACOCMC}/{TIPPMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanica", .action = "EditMecanicaPromocaoGridRegistros", .CODACOCMC = 0, .NUMSEQRGRACOCMC = 0, .TIPPMC = 0}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaEditMecanicaPromocaoGridRegistrosCombinada",
            url:="RelacaoAcaoMecanica/EditMecanicaPromocaoGridRegistrosCombinada/{CODACOCMC}/{NUMSEQRGRACOCMC}/{QDEGRPMIXPMC}/{TIPPMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanica", .action = "EditMecanicaPromocaoGridRegistrosCombinada", .CODACOCMC = 0, .NUMSEQRGRACOCMC = 0, .QDEGRPMIXPMC = 0, .TIPPMC = 0}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaEditMecanicaPromocaoGridRegistrosFaixa",
            url:="RelacaoAcaoMecanica/EditMecanicaPromocaoGridRegistrosFaixa/{CODACOCMC}/{NUMSEQRGRACOCMC}/{QDEFXAPMC}/{TIPPMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanica", .action = "EditMecanicaPromocaoGridRegistrosFaixa", .CODACOCMC = 0, .NUMSEQRGRACOCMC = 0, .QDEFXAPMC = 0, .TIPPMC = 0}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaEditMecanicaPromocaoPublicoAlvoEstado",
            url:="RelacaoAcaoMecanica/EditMecanicaPromocaoPublicoAlvoEstadoUniao/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanica", .action = "EditMecanicaPromocaoPublicoAlvoEstadoUniao", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaEditMecanicaPromocaoPublicoAlvoEstadoSave",
            url:="RelacaoAcaoMecanicaPublicoAlvoEstado/Save/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoEstado", .action = "Save", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaEditMecanicaPromocaoPublicoAlvoAtividadeSave",
            url:="RelacaoAcaoMecanicaPublicoAlvoAtividade/Save/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoAtividade", .action = "Save", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaEditMecanicaPromocaoPublicoAlvoGrupamentoCliente",
            url:="RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente/EditMecanicaPromocaoPublicoAlvoGrupamentoCliente/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente", .action = "EditMecanicaPromocaoPublicoAlvoGrupamentoCliente", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaPromocaoPublicoAlvoGrupamentoClienteSave",
            url:="RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente/Save/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente", .action = "Save", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaPromocaoPublicoAlvoNegocioClienteSave",
            url:="RelacaoAcaoMecanicaPublicoAlvoNegocioCliente/Save/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoNegocioCliente", .action = "Save", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaPromocaoPublicoAlvoSegmentoClienteSave",
            url:="RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente/Save/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente", .action = "Save", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaPromocaoPublicoAlvoClienteCarregarPesquisa",
            url:="RelacaoAcaoMecanicaPublicoAlvoCliente/EditMecanicaPromocaoWidgetLoadPesquisaCliente/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoCliente", .action = "EditMecanicaPromocaoWidgetLoadPesquisaCliente", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaPromocaoPublicoAlvoRcaVendedorCarregarPesquisa",
            url:="RelacaoAcaoMecanicaPublicoAlvoRcaVendedor/EditMecanicaPromocaoWidgetLoadPesquisaRcaVendedor/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoRcaVendedor", .action = "EditMecanicaPromocaoWidgetLoadPesquisaRcaVendedor", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaPromocaoPublicoAlvoGerenteMercadoCarregarPesquisa",
            url:="RelacaoAcaoMecanicaPublicoAlvoGerenteMercado/EditMecanicaPromocaoWidgetLoadPesquisaGerenteMercado/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoGerenteMercado", .action = "EditMecanicaPromocaoWidgetLoadPesquisaGerenteMercado", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaPromocaoPublicoAlvoClienteSave",
            url:="RelacaoAcaoMecanicaPublicoAlvoCliente/Save/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoCliente", .action = "Save", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaPromocaoPublicoAlvoGerenteVendasSave",
            url:="RelacaoAcaoMecanicaPublicoAlvoGerenteVendas/Save/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoGerenteVendas", .action = "Save", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaPromocaoPublicoAlvoGerenteVendasEdit",
            url:="RelacaoAcaoMecanicaPublicoAlvoGerenteVendas/EditMecanicaPromocaoPublicoAlvoGerenteVendas/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoGerenteVendas", .action = "EditMecanicaPromocaoPublicoAlvoGerenteVendas", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaPublicoAlvoEdit",
            url:="RelacaoAcaoMecanica/EditMecanicaPromocaoWidgetPublicoAlvo/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanica", .action = "EditMecanicaPromocaoWidgetPublicoAlvo", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaFiliaisEdit",
            url:="RelacaoAcaoMecanica/EditMecanicaPromocaoWidgetFiliais/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanica", .action = "EditMecanicaPromocaoWidgetFiliais", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaSave",
            url:="RelacaoAcaoMecanica/Save/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanica", .action = "Save", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="VerificaProdutoVendidoFracionado",
            url:="PromocaoBrinde/VerificaProdutoVendidoFracionado/{CodMer}/{CodFilEmp}",
            defaults:=New With {.controller = "PromocaoBrinde", .action = "VerificaProdutoVendidoFracionado", .CodMer = UrlParameter.Optional, .CodFilEmp = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="ValoresBrinde",
            url:="PromocaoBrinde/ValoresBrinde/{CodPmc}/{CodMer}",
            defaults:=New With {.controller = "PromocaoBrinde", .action = "ValoresBrinde", .CodPmc = UrlParameter.Optional, .CodMer = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaMixEdit",
            url:="RelacaoAcaoMecanica/EditMecanicaPromocaoWidgetMix/{CODACOCMC}/{NUMSEQRGRACOCMC}/{TIPPMC}/{INDPTCTOTMIXPMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanica", .action = "EditMecanicaPromocaoWidgetMix", .CODACOCMC = UrlParameter.Optional, .TIPPMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaMixEditDrpCategoria",
            url:="RelacaoAcaoMecanicaMix/CarregarDropMixCategoria/{CODGRPMER}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "CarregarDropMixCategoria", .CODACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaMixEditDrpSubCategoria",
            url:="RelacaoAcaoMecanicaMix/CarregarDropMixSubCategoria/{CODGRPMER}/{CODFMLMER}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "CarregarDropMixSubCategoria", .CODACOCMC = UrlParameter.Optional, .CODFMLMER = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaMixSaveGrupoMercadoria",
            url:="RelacaoAcaoMecanicaMix/SaveMixGrupoMercadoria/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "SaveMixGrupoMercadoria", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaMixCarregarWidgetPesquisaFamiliaMercadoria",
            url:="RelacaoAcaoMecanicaMix/CarregarWidgetLoadPesquisaMixFamiliaMercadoria/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "CarregarWidgetLoadPesquisaMixFamiliaMercadoria", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaMixCarregarWidgetPesquisaClasseMercadoria",
            url:="RelacaoAcaoMecanicaMix/CarregarWidgetLoadPesquisaMixClasseMercadoria/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "CarregarWidgetLoadPesquisaMixClasseMercadoria", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaMixCarregarWidgetPesquisaMercadoria",
            url:="RelacaoAcaoMecanicaMix/CarregarWidgetLoadPesquisaMixMercadoria/{CODACOCMC}/{NUMSEQRGRACOCMC}/{TIPPMC}/{QTDFXAGRP}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "CarregarWidgetLoadPesquisaMixMercadoria", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional, .TIPPMC = UrlParameter.Optional, .QTDFXAGRP = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaMixCarregarWidgetPesquisaMercadoriaPorPromocao",
            url:="RelacaoAcaoMecanicaMix/CarregarWidgetLoadPesquisaMixMercadoriaPorPromocao/{CODACOCMC}/{NUMSEQRGRACOCMC}/{TIPPMC}/{QTDFXAGRP}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "CarregarWidgetLoadPesquisaMixMercadoriaPorPromocao", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional, .TIPPMC = UrlParameter.Optional, .QTDFXAGRP = UrlParameter.Optional}
            )

        'routes.MapRoute(
        '    name:="RelacaoAcaoMecanicaMixSaveMercadoriaPorPromocao",
        '    url:="RelacaoAcaoMecanicaMix/SaveMixMercadoriaPorPromocao/{CODACOCMC}/{NUMSEQRGRACOCMC}",
        '    defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "SaveMixMercadoriaPorPromocao", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
        '    )

        routes.MapRoute(
            name:="RelacaoAcaoMecanicaMixCarregarWidgetPesquisaFornecedor",
            url:="RelacaoAcaoMecanicaMix/CarregarWidgetLoadPesquisaMixFornecedor/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "CarregarWidgetLoadPesquisaMixFornecedor", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoPublicoAlvoEstadoSave",
            url:="PromocaoPublicoAlvoEstado/Save/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoEstado", .action = "Save", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="INSERT",
            url:="Abastecimento/InsertVarios",
            defaults:=New With {.controller = "Abastecimento", .action = "InsertVarios"}
            )

        routes.MapRoute(
            name:="PromocaoPublicoAlvoAtividadeSave",
            url:="PromocaoPublicoAlvoAtividade/Save/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoAtividade", .action = "Save", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoPublicoAlvoGrupamentoClienteSave",
            url:="PromocaoPublicoAlvoGrupamentoCliente/Save/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoGrupamentoCliente", .action = "Save", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoPublicoAlvoNegocioClienteSave",
            url:="PromocaoPublicoAlvoNegocioCliente/Save/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoNegocioCliente", .action = "Save", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoPublicoAlvoSegmentoClienteSave",
            url:="PromocaoPublicoAlvoSegmentoCliente/Save/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoSegmentoCliente", .action = "Save", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoPublicoAlvoGerenteVendasSave",
            url:="PromocaoPublicoAlvoGerenteVendas/Save/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoGerenteVendas", .action = "Save", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoPublicoAlvoClienteCarregarPesquisa",
            url:="PromocaoPublicoAlvoCliente/EditMecanicaPromocaoWidgetLoadPesquisaCliente/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoCliente", .action = "EditMecanicaPromocaoWidgetLoadPesquisaCliente", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoPublicoAlvoRcaVendedorCarregarPesquisa",
            url:="PromocaoPublicoAlvoRcaVendedor/EditMecanicaPromocaoWidgetLoadPesquisaRcaVendedor/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoRcaVendedor", .action = "EditMecanicaPromocaoWidgetLoadPesquisaRcaVendedor", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoPublicoAlvoGerenteMercadoCarregarPesquisa",
            url:="PromocaoPublicoAlvoGerenteMercado/EditMecanicaPromocaoWidgetLoadPesquisaGerenteMercado/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoGerenteMercado", .action = "EditMecanicaPromocaoWidgetLoadPesquisaGerenteMercado", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoPublicoAlvoCarregarWidget",
            url:="Promocao/CarregarWidgetPublicoAlvo/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "Promocao", .action = "CarregarWidgetPublicoAlvo", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoClientesExcludentesCarregarWidget",
            url:="Promocao/CarregarWidgetClientesExcludentes/{CODPMC}",
            defaults:=New With {.controller = "Promocao", .action = "CarregarWidgetClientesExcludentes", .CODPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="EditPromocaoWidgetLoadPesquisaClientesExcludentes",
            url:="PromocaoClientesExcludentes/EditMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes/{CODPMC}",
            defaults:=New With {.controller = "PromocaoClientesExcludentes", .action = "EditMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes", .CODPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="ModalAbastecerPromocao",
            url:="Abastecimento/ModalAbastecerPromocao/{CODPMC}",
            defaults:=New With {.controller = "Abastecimento", .action = "ModalAbastecerPromocao", .CODPMC = UrlParameter.Optional}
            )
        routes.MapRoute(
            name:="AbastecimentoHistorico",
            url:="Abastecimento/Historico/{CODPMC}/{CODMER}",
            defaults:=New With {.controller = "Abastecimento", .action = "Historico"}
            )

        routes.MapRoute(
            name:="PromocaoMixCarregarWidget",
            url:="PromocaoMix/CarregarWidgetPromocaoMix/{CODPMC}/{TIPEDEPUBALVPMC}/{INDPTCTOTMIXPMC}",
            defaults:=New With {.controller = "PromocaoMix", .action = "CarregarWidgetPromocaoMix", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoLiberar",
            url:="Promocao/Liberar/{CODPMC}",
            defaults:=New With {.controller = "Promocao", .action = "Liberar", .CODPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoAbrir",
            url:="Promocao/Abrir/{CODPMC}",
            defaults:=New With {.controller = "Promocao", .action = "Abrir", .CODPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="CarregarWidgetLoadPesquisaPromocaoFamiliaMercadoria",
            url:="PromocaoMix/CarregarWidgetLoadPesquisaMixFamiliaMercadoria/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoMix", .action = "CarregarWidgetLoadPesquisaMixFamiliaMercadoria", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="CarregarWidgetLoadPesquisaPromocaoClasseMercadoria",
            url:="PromocaoMix/CarregarWidgetLoadPesquisaMixClasseMercadoria/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoMix", .action = "CarregarWidgetLoadPesquisaMixClasseMercadoria", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="CarregarWidgetLoadPesquisaPromocaoMercadoria",
            url:="PromocaoMix/CarregarWidgetLoadPesquisaMixMercadoria/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoMix", .action = "CarregarWidgetLoadPesquisaMixMercadoria", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="CarregarWidgetLoadPesquisaPromocaoMercadoriaPorPromocao",
            url:="PromocaoMix/CarregarWidgetLoadPesquisaMixMercadoriaPorPromocao/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoMix", .action = "CarregarWidgetLoadPesquisaMixMercadoriaPorPromocao", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="CarregarWidgetLoadPesquisaPromocaoFornecedor",
            url:="PromocaoMix/CarregarWidgetLoadPesquisaMixFornecedor/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoMix", .action = "CarregarWidgetLoadPesquisaMixFornecedor", .CODPMC = UrlParameter.Optional, .TIPEDEPUBALVPMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="PromocaoCreateWizard",
            url:="PromocaoWizard/CreateWizard/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "PromocaoWizard", .action = "CreateWizard"}
            )
        routes.MapRoute(
            name:="PromocaoWizardCreateFilial",
            url:="PromocaoWizard/WizardCreateFilial/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "PromocaoWizard", .action = "WizardCreateFilial"}
            )
        routes.MapRoute(
            name:="PromocaoWizardCreateGeral",
            url:="PromocaoWizard/WizardCreateGeral/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "PromocaoWizard", .action = "WizardCreateGeral"}
            )
        routes.MapRoute(
            name:="PromocaoWizardConsultaValoreBrinde",
            url:="PromocaoWizard/ConsultaValoreBrinde/{CODACOCMC}/{CODMER}",
            defaults:=New With {.controller = "PromocaoWizard", .action = "ConsultaValoreBrinde"}
            )

        'routes.MapRoute(
        '    name:="PromocaoWizardCreateBrinde",
        '    url:="PromocaoWizard/WizardCreateBrinde/{CODACOCMC}/{NUMSEQRGRACOCMC}/{INDISRBDEADI}",
        '    defaults:=New With {.controller = "PromocaoWizard", .action = "WizardCreateBrinde"}
        '    )
        routes.MapRoute(
            name:="PromocaoWizardCreateComunicacao",
            url:="PromocaoWizard/WizardCreateComunicacao/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "PromocaoWizard", .action = "WizardCreateComunicacao"}
            )

        routes.MapRoute(
            name:="BuscarGrupamentoClientePorBU",
            url:="RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente/BuscarGrupamentoClientePorBU/{CODUNDESRNGC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente", .action = "BuscarGrupamentoClientePorBU"}
            )

        routes.MapRoute(
            name:="PromocaoMixSaveGrupoMercadoria",
            url:="PromocaoMix/SaveMixGrupoMercadoria/{CODPMC}",
            defaults:=New With {.controller = "PromocaoMix", .action = "SaveMixGrupoMercadoria"}
            )

        routes.MapRoute(
            name:="ImportarXlsCliente",
            url:="RelacaoAcaoMecanicaPublicoAlvoCliente/ImportarXlsCliente/{CODACOCMC}/{NUMSEQRGRACOCMC}/{QDEGRPMIXPMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoCliente", .action = "ImportarXlsCliente"}
            )

        routes.MapRoute(
            name:="ImportarXlsRcaVendedor",
            url:="RelacaoAcaoMecanicaPublicoAlvoRcaVendedor/ImportarXlsRcaVendedor/{CODACOCMC}/{NUMSEQRGRACOCMC}/{QDEGRPMIXPMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoRcaVendedor", .action = "ImportarXlsRcaVendedor"}
            )


        routes.MapRoute(
            name:="ImportarXlsGerenteMercado",
            url:="RelacaoAcaoMecanicaPublicoAlvoGerenteMercado/ImportarXlsGerenteMercado/{CODACOCMC}/{NUMSEQRGRACOCMC}/{QDEGRPMIXPMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaPublicoAlvoGerenteMercado", .action = "ImportarXlsGerenteMercado"}
            )


        routes.MapRoute(
            name:="ImportarXlsBrinde",
            url:="PromocaoBrinde/ImportarXlsBrinde/{CODPMC}",
            defaults:=New With {.controller = "PromocaoBrinde", .action = "ImportarXlsBrinde"}
            )


        routes.MapRoute(
            name:="ImportarXlsMixFornecedor",
            url:="RelacaoAcaoMecanicaMix/ImportarXlsMixFornecedor/{CODACOCMC}/{NUMSEQRGRACOCMC}/{QDEGRPMIXPMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "ImportarXlsMixFornecedor"}
            )

        routes.MapRoute(
            name:="ImportarXlsMixMercadoria",
            url:="RelacaoAcaoMecanicaMix/ImportarXlsMixMercadoria/{CODACOCMC}/{NUMSEQRGRACOCMC}/{QDEGRPMIXPMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "ImportarXlsMixMercadoria"}
            )

        routes.MapRoute(
            name:="ImportarXlsMixClasseMercadoria",
            url:="RelacaoAcaoMecanicaMix/ImportarXlsMixClasseMercadoria/{CODACOCMC}/{NUMSEQRGRACOCMC}/{QDEGRPMIXPMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "ImportarXlsMixClasseMercadoria"}
            )

        routes.MapRoute(
            name:="ImportarXlsMixFamiliaMercadoria",
            url:="RelacaoAcaoMecanicaMix/ImportarXlsMixFamiliaMercadoria/{CODACOCMC}/{NUMSEQRGRACOCMC}/{QDEGRPMIXPMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaMix", .action = "ImportarXlsMixFamiliaMercadoria"}
            )

        'Promoção
        routes.MapRoute(
            name:="PMCImportarXlsCliente",
            url:="PromocaoPublicoAlvoCliente/ImportarXlsCliente/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoCliente", .action = "ImportarXlsCliente"}
            )

        routes.MapRoute(
            name:="PMCImportarXlsRcaVendedor",
            url:="PromocaoPublicoAlvoRcaVendedor/ImportarXlsRcaVendedor/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoRcaVendedor", .action = "ImportarXlsRcaVendedor"}
            )

        routes.MapRoute(
            name:="PMCImportarXlsGerenteMercado",
            url:="PromocaoPublicoAlvoGerenteMercado/ImportarXlsGerenteMercado/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoPublicoAlvoGerenteMercado", .action = "ImportarXlsGerenteMercado"}
            )

        routes.MapRoute(
            name:="PMCImportarXlsMixFornecedor",
            url:="PromocaoMix/ImportarXlsMixFornecedor/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoMix", .action = "ImportarXlsMixFornecedor"}
            )

        routes.MapRoute(
            name:="PMCImportarXlsMixMercadoria",
            url:="PromocaoMix/ImportarXlsMixMercadoria/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoMix", .action = "ImportarXlsMixMercadoria"}
            )

        routes.MapRoute(
            name:="PMCImportarXlsMixClasseMercadoria",
            url:="PromocaoMix/ImportarXlsMixClasseMercadoria/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoMix", .action = "ImportarXlsMixClasseMercadoria"}
            )

        routes.MapRoute(
            name:="PMCImportarXlsMixFamiliaMercadoria",
            url:="PromocaoMix/ImportarXlsMixFamiliaMercadoria/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoMix", .action = "ImportarXlsMixFamiliaMercadoria"}
            )

        'routes.MapMvcAttributeRoutes()

        routes.MapRoute(
            name:="Default",
            url:="{controller}/{action}/{id}",
            defaults:=New With {.controller = "Home", .action = "Index", .id = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="ImportarXlsParametros",
            url:="PromocaoParametrosMix/ImportarXlsParametros/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoParametrosMix", .action = "ImportarXlsParametros"}
            )

        routes.MapRoute(
            name:="ImportarXlsParametrosCash",
            url:="PromocaoParametrosMix/ImportarXlsParametrosCash/{CODPMC}/{TIPEDEPUBALVPMC}",
            defaults:=New With {.controller = "PromocaoParametrosMix", .action = "ImportarXlsParametrosCash"}
            )

        routes.MapRoute(
            name:="EditMecanicaPromocaoWidgetClientesExcludentes",
            url:="RelacaoAcaoMecanica/EditMecanicaPromocaoWidgetClientesExcludentes/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanica", .action = "EditMecanicaPromocaoWidgetClientesExcludentes", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="EditMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes",
            url:="RelacaoAcaoMecanicaClientesExcludentes/EditMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaClientesExcludentes", .action = "EditMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes", .CODACOCMC = UrlParameter.Optional, .NUMSEQRGRACOCMC = UrlParameter.Optional}
            )

        routes.MapRoute(
            name:="ImportarXlsClientesExcludentes",
            url:="RelacaoAcaoMecanicaClientesExcludentes/ImportarXlsClientesExcludentes/{CODACOCMC}/{NUMSEQRGRACOCMC}",
            defaults:=New With {.controller = "RelacaoAcaoMecanicaClientesExcludentes", .action = "ImportarXlsClientesExcludentes"}
            )

        routes.MapRoute(
            name:="ImportarXlsClientesExcludentesPromocao",
            url:="PromocaoClientesExcludentes/ImportarXlsClientesExcludentes/{CODPMC}",
            defaults:=New With {.controller = "PromocaoClientesExcludentes", .action = "ImportarXlsClientesExcludentes"}
            )

    End Sub
End Module