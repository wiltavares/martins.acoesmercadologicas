﻿Imports System.Runtime.CompilerServices
Imports System.Web.Routing

Public Class ViewHelper
    Public Shared Function Enable(value As Boolean) As Object
        Return If(value = True, "", "a-disabled")
    End Function

    Public Shared Function Visible(value As Boolean) As String
        Return If(value = True, "", "display: none;")
    End Function

End Class

