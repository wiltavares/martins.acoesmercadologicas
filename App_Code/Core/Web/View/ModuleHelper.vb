﻿Imports System.Runtime.CompilerServices
Imports System.Web.Routing

Public Module ModuleHelper

    <Extension()>
    Public Function EnableIf(ByVal htmlAttributes As Object, enable As Boolean) As RouteValueDictionary
        Dim attributes = New RouteValueDictionary(htmlAttributes)
        If (Not enable) Then
            attributes("disabled") = "disabled"
        End If
        Return attributes
    End Function

    <Extension()>
    Public Function ReadonlyIf(ByVal htmlAttributes As Object, pReadonly As Boolean) As RouteValueDictionary
        Dim attributes = New RouteValueDictionary(htmlAttributes)
        If (Not pReadonly) Then
            attributes("readonly") = "readonly"
        End If
        Return attributes
    End Function

    <Extension()>
    Public Function VisibledIf(ByVal htmlAttributes As Object, pVisible As Boolean) As RouteValueDictionary
        Dim attributes = New RouteValueDictionary(htmlAttributes)
        If (Not pVisible) Then
            attributes("visible") = "false"
        End If
        Return attributes
    End Function

End Module
