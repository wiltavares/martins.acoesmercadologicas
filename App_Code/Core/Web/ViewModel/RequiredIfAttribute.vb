﻿Imports System.ComponentModel.DataAnnotations

Namespace System.ComponentModel.DataAnnotations
    Public Class RequiredIfAttribute
        Inherits ValidationAttribute

        ' Note: we don't inherit from RequiredAttribute as some elements of the MVC
        ' framework specifically look for it and choose not to add a RequiredValidator
        ' for non-nullable fields if one is found. This would be invalid if we inherited
        ' from it as obviously our RequiredIf only applies if a condition is satisfied.
        ' Therefore we're using a private instance of one just so we can reuse the IsValid
        ' logic, and don't need to rewrite it.
        Private innerAttribute As New RequiredAttribute()
        Public Property DependentProperty() As String
            Get
                Return m_DependentProperty
            End Get
            Set
                m_DependentProperty = Value
            End Set
        End Property
        Private m_DependentProperty As String

        Public Property TargetValue() As Object
            Get
                Return m_TargetValue
            End Get
            Set
                m_TargetValue = Value
            End Set
        End Property
        Private m_TargetValue As Object

        Public Sub New(dependentProperty As String, targetValue As Object)
            Me.DependentProperty = dependentProperty
            Me.TargetValue = targetValue
        End Sub

        Public Overrides Function IsValid(value As Object) As Boolean
            Return innerAttribute.IsValid(value)
        End Function

    End Class

End Namespace
