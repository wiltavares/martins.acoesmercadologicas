﻿Public Class ModalConfirmacaoViewModel

    Property Titulo As String
    Property Mensagem As String
    Property IdDiv As String
    Property BtnSimNome As String
    Property BtnNaoNome As String
    Property BtnSimAcao As String
    Property BtnNaoAcao As String
    Property Objeto As Object

    Public Sub New(param As Object)
        Titulo = "Confirmação de Exclusão"
        Mensagem = "Tem certeza que deseja excluir o registro?"
        BtnSimNome = "Sim"
        BtnNaoNome = "Não"
        BtnSimAcao = "confirmaExclusao"
        BtnNaoAcao = "fechaModalConfirmacao"
        IdDiv = "modal"
        Objeto = param
    End Sub

    Public Sub New()
    End Sub


End Class
