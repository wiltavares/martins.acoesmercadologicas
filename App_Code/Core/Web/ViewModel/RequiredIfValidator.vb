﻿Imports System.Web.Mvc
Imports Core.System.ComponentModel.DataAnnotations

Namespace System.ConditionalValidation.Validation

    Public Class RequiredIfValidator
        Inherits DataAnnotationsModelValidator(Of RequiredIfAttribute)


        Public Sub New(metadata As ModelMetadata, context As ControllerContext, attribute As RequiredIfAttribute)
            MyBase.New(metadata, context, attribute)
        End Sub

        'Public Overrides Function GetClientValidationRules() As IEnumerable(Of ModelClientValidationRule)
        '    ' no client validation - I might well blog about this soon!
        '    Return MyBase.GetClientValidationRules()
        'End Function

        Public Overrides Iterator Function Validate(container As Object) As IEnumerable(Of ModelValidationResult)
            ' get a reference to the property this validation depends upon
            Dim field = Metadata.ContainerType.GetProperty(Attribute.DependentProperty)

            If field IsNot Nothing Then
                ' get the value of the dependent property
                Dim value = field.GetValue(container, Nothing)

                ' compare the value against the target value
                If (value Is Nothing AndAlso Attribute.TargetValue Is Nothing) OrElse (value = Attribute.TargetValue) Then
                    ' match => means we should try validating this field
                    If Not Attribute.IsValid(Metadata.Model) Then
                        ' validation failed - return an error
                        Yield New ModelValidationResult() With {
                                                                    .Message = ErrorMessage
                                                                }
                    End If
                End If
            End If
        End Function

    End Class

End Namespace
