﻿Imports AutoMapper

Public Module AutoMapperConfig
    Public MapperConfiguration As IMapper
    Public Sub RegisterMappings()
        Dim config = New MapperConfiguration(
            Sub(cfg)
                cfg.AddProfile(New DomainToViewModelMappingProfile())
                cfg.AddProfile(New ViewModelToDomainMappingProfile())
            End Sub)
        MapperConfiguration = config.CreateMapper()
    End Sub
End Module
