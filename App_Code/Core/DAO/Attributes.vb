﻿Imports System.Reflection

Public Class TableAttribute
    Inherits Attribute

    Property TableName As String

    Public Sub New(ByVal pTableName As String)
        TableName = pTableName
    End Sub
End Class

Public Class PrimaryKeyAttribute
    Inherits Attribute

End Class

Public Class IdentityAttribute
    Inherits Attribute

End Class

Public Class SelectMaxAttribute
    Inherits Attribute

End Class

Public Class SequenceAttribute
    Inherits Attribute

    Property SequenceName As String

    Public Sub New(ByVal pSequenceName As String)
        SequenceName = pSequenceName
    End Sub
End Class

Public Class DateInsertAttribute
    Inherits Attribute

End Class

Public Class DateUpdateAttribute
    Inherits Attribute

End Class

Public Class ReadonlyAttribute
    Inherits Attribute

    Property Insert As Boolean
    Property Update As Boolean


    Public Sub New()
        Insert = True
        Update = True
    End Sub

    Public Sub New(ByVal pInsert As Boolean, ByVal pUpdate As Boolean)
        Insert = pInsert
        Update = pUpdate
    End Sub
End Class

Public Class IgnoreMapAttribute
    Inherits Attribute

End Class

Public Class Attributes

    Public Shared Function GetAnnotation(Of T)(ByRef obj As Object, ByVal propertyName As String) As T

        If (IsNothing(obj)) Then
            Return Nothing
        End If

        Dim propConfig As List(Of PropertyInfo) = obj.GetType().GetProperties().ToList()
        Dim propertyConfig = (From a In propConfig
                              Where a.Name = propertyName).FirstOrDefault()

        If (propertyConfig Is Nothing) Then
            Return Nothing
        End If


        Dim attributes = propertyConfig.GetCustomAttributes(GetType(T), True)
        If (attributes Is Nothing Or attributes.Count = 0) Then
            Return Nothing
        End If

        Return CType(attributes.First(), T)

    End Function


    Public Shared Function GetAnnotation(Of T)(ByRef obj) As T

        If (obj Is Nothing) Then
            Return Nothing
        End If

        Dim attributes = obj.GetType().GetCustomAttributes(GetType(T), True)
        If (attributes Is Nothing Or attributes.Count = 0) Then
            Return Nothing
        End If

        Return CType(attributes.First(), T)

    End Function

End Class