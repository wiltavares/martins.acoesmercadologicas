﻿Imports Microsoft.VisualBasic

Public Class ParameterAndProperty
    Private listParameter As IList(Of PropertyParameter)

    Public Sub New()
        listParameter = New List(Of PropertyParameter)
    End Sub

    Public Sub Add(ByVal name As String, ByVal value As Object, Optional ByVal direction As System.Data.ParameterDirection = Data.ParameterDirection.Input, Optional ByVal dbType As System.Data.DbType = Nothing, Optional ByVal Size As Integer = 0)
        Dim propertyParameter As New PropertyParameter

        With propertyParameter
            'Parametros obrigatorios
            .Name = name
            .Value = value

            'Parametros optional
            .Direction = direction
            .DbType = dbType
            .Size = Size
        End With
        'Adiciona na Lista de Parametros
        listParameter.Add(propertyParameter)
    End Sub

    Public Function GetAll() As List(Of PropertyParameter)
        Return listParameter
    End Function
End Class
