﻿Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class ParameterMapper
    Implements IParameterMapper

    Public Parameters As Dictionary(Of String, Object) = New Dictionary(Of String, Object)

    Public Sub AssignParameters(ByVal command As DbCommand, ByVal parameterValues() As Object)

    End Sub

    Private Sub IParameterMapper_AssignParameters(command As DbCommand, parameterValues() As Object) Implements IParameterMapper.AssignParameters

        For Each parametro As KeyValuePair(Of String, Object) In Me.Parameters
            Dim parameter As DbParameter = command.CreateParameter
            parameter.ParameterName = parametro.Key
            parameter.Value = parametro.Value
            command.Parameters.Add(parameter)
        Next

    End Sub
End Class
