﻿Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common.Configuration
Imports System.Web
Imports System.Data

Public Class Connector
    Implements IDisposable

    Public Property Transaction() As DbTransaction
        Get
            If Not HttpContext.Current.Items.Contains("DbTransaction") Then
                Return Nothing
            End If
            Return HttpContext.Current.Items("DbTransaction")
        End Get
        Set(ByVal value As DbTransaction)
            HttpContext.Current.Items("DbTransaction") = value
        End Set
    End Property

    Public Property Database() As Database
        Get
            If Not HttpContext.Current.Items.Contains("Database") Then

                Dim factory As DatabaseProviderFactory = New DatabaseProviderFactory(New SystemConfigurationSource())
                DatabaseFactory.SetDatabaseProviderFactory(factory, False)

                HttpContext.Current.Items.Add("Database", factory.Create("MRT001"))
            End If
            Return HttpContext.Current.Items("Database")
        End Get
        Set(ByVal value As Database)
            HttpContext.Current.Items("Database") = value
        End Set
    End Property

    Public Property DatabaseApp() As Database
        Get
            If Not HttpContext.Current.Items.Contains("App") Then

                Dim factory As DatabaseProviderFactory = New DatabaseProviderFactory(New SystemConfigurationSource())
                DatabaseFactory.SetDatabaseProviderFactory(factory, False)

                HttpContext.Current.Items.Add("App", factory.Create("APP"))
            End If
            Return HttpContext.Current.Items("App")
        End Get
        Set(ByVal value As Database)
            HttpContext.Current.Items("App") = value
        End Set
    End Property

    Public Property Connection() As DbConnection
        Get
            If Not HttpContext.Current.Items.Contains("DbConnection") Then
                Dim conn As DbConnection = Database.CreateConnection()
                HttpContext.Current.Items.Add("DbConnection", conn)
            End If
            Return HttpContext.Current.Items("DbConnection")
        End Get
        Set(ByVal value As DbConnection)
            HttpContext.Current.Items("DbConnection") = value
        End Set
    End Property


    Public Function BeginTransaction() As DbTransaction

        If Connection.State = ConnectionState.Closed Then
            Connection.Open()
        End If
        If Transaction Is Nothing Then
            Transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted)
        End If
        Return Transaction
    End Function

    Public Sub Commit()
        If Not Transaction Is Nothing Then
            Transaction.Commit()
            Transaction = Nothing
        End If
        CloseConnection()
    End Sub

    Public Sub Rollback()
        If Not Transaction Is Nothing Then
            Transaction.Rollback()
            Transaction = Nothing
        End If
        CloseConnection()
    End Sub



    Public Function SelectTList(Of TReturn As {Class, New})(ByVal sql As String, Optional ByVal parameterMapper As ParameterMapper = Nothing, Optional ByVal rowMapper As IRowMapper(Of TReturn) = Nothing, Optional BancoApp As Boolean = False) As IEnumerable(Of TReturn)

        Dim BaseData As Database

        If (BancoApp) Then
            BaseData = DatabaseApp
        Else
            BaseData = Database
        End If


        If Connection.State = ConnectionState.Closed Then
            Connection.Open()
        End If
        Dim result As IEnumerable(Of TReturn)
        Try
            If (Not parameterMapper Is Nothing) And (Not rowMapper Is Nothing) Then
                result = BaseData.CreateSqlStringAccessor(Of TReturn)(sql, parameterMapper, rowMapper).Execute(Nothing)
            ElseIf (Not parameterMapper Is Nothing) And (rowMapper Is Nothing) Then
                result = BaseData.CreateSqlStringAccessor(Of TReturn)(sql, parameterMapper).Execute(Nothing)
            ElseIf (parameterMapper Is Nothing) And (Not rowMapper Is Nothing) Then
                result = BaseData.CreateSqlStringAccessor(Of TReturn)(sql, New ParameterMapper, rowMapper).Execute(Nothing)
            Else
                result = BaseData.CreateSqlStringAccessor(Of TReturn)(sql).Execute(Nothing)
            End If
        Catch ex As Exception
            Throw ex
        Finally
            CloseConnection()
        End Try
        Return result.ToList()

    End Function

    Public Function ExecuteScalar(ByVal sql As String, Optional ByVal parameterMapper As ParameterMapper = Nothing, Optional BancoApp As Boolean = False) As Object


        Dim BaseData As Database

        If (BancoApp) Then
            BaseData = DatabaseApp
        Else
            BaseData = Database
        End If

        Dim command As DbCommand = Nothing
        command = BaseData.GetSqlStringCommand(sql)
        command.CommandType = CommandType.Text

        If (Not parameterMapper Is Nothing) Then
            For Each item In parameterMapper.Parameters
                Dim parameter As DbParameter = command.CreateParameter()

                parameter.ParameterName = BaseData.BuildParameterName(item.Key)

                If (item.Value Is Nothing) Then
                    parameter.Value = DBNull.Value
                Else
                    parameter.Value = item.Value
                End If
                command.Parameters.Add(parameter)
            Next
        End If

        command.Connection = Connection

        If command.Connection.State = ConnectionState.Closed Then
            command.Connection.Open()
        End If

        Try
            If (Transaction Is Nothing) Then
                Return BaseData.ExecuteScalar(command)
            Else
                Return BaseData.ExecuteScalar(command, Transaction)
            End If
        Catch ex As Exception
            Throw ex
        Finally
            CloseConnection()
        End Try

    End Function

    Public Function Execute(ByVal sql As String, Optional ByVal parameterMapper As ParameterMapper = Nothing, Optional BancoApp As Boolean = False) As Object


        Dim BaseData As Database

        If (BancoApp) Then
            BaseData = DatabaseApp
        Else
            BaseData = Database
        End If

        Dim command As DbCommand = Nothing
        command = BaseData.GetSqlStringCommand(sql)
        command.CommandType = CommandType.Text

        If (Not parameterMapper Is Nothing) Then
            For Each item In parameterMapper.Parameters
                Dim parameter As DbParameter = command.CreateParameter()

                parameter.ParameterName = BaseData.BuildParameterName(item.Key)

                If (item.Value Is Nothing) Then
                    parameter.Value = DBNull.Value
                Else
                    parameter.Value = item.Value
                End If
                command.Parameters.Add(parameter)
            Next
        End If

        command.Connection = Connection

        If command.Connection.State = ConnectionState.Closed Then
            command.Connection.Open()
        End If
        Try
            Dim result = BaseData.ExecuteScalar(command)
            If result Is DBNull.Value Then
                Return Nothing
            End If
            Return result
        Catch ex As Exception
            Throw ex
        Finally
            CloseConnection()
        End Try
    End Function

    Public Function ExecuteStoredPrecedure(ByVal sql As String, Optional ByVal parameterMapper As ParameterMapper = Nothing, Optional ByVal parameterMapperReturnValue As ParameterMapper = Nothing) As Object

        Dim command As DbCommand = Nothing
        command = Database.GetSqlStringCommand(sql)
        command.CommandType = CommandType.StoredProcedure

        If (Not parameterMapper Is Nothing) Then
            For Each item In parameterMapper.Parameters
                Dim parameter As DbParameter = command.CreateParameter()

                parameter.ParameterName = Database.BuildParameterName(item.Key)

                If (item.Value Is Nothing) Then
                    parameter.Value = DBNull.Value
                Else
                    parameter.Value = item.Value
                End If
                command.Parameters.Add(parameter)
            Next
        End If

        If (Not parameterMapperReturnValue Is Nothing) Then
            For Each item In parameterMapperReturnValue.Parameters
                Dim parameter As DbParameter = command.CreateParameter()

                parameter.ParameterName = Database.BuildParameterName(item.Key)

                'If (item.Value Is Nothing) Then
                '    parameter.Value = DBNull.Value
                'Else
                '    parameter.Value = item.Value
                'End If
                parameter.Direction = ParameterDirection.ReturnValue
                command.Parameters.Add(parameter)
            Next
        End If

        command.Connection = Connection

        If command.Connection.State = ConnectionState.Closed Then
            command.Connection.Open()
        End If

        Try

            Return Database.ExecuteScalar(command, Transaction)
        Catch ex As Exception
            Throw ex
        Finally
            CloseConnection()
        End Try

    End Function


    Public Function ExecuteStoredPrecedure(ByVal sql As String, ByVal listPropertyParameter As List(Of PropertyParameter), Optional ByRef parametersRetorno As Dictionary(Of String, Object) = Nothing) As Boolean

        Dim command As DbCommand = Nothing
        command = Database.GetSqlStringCommand(sql)
        command.CommandType = CommandType.StoredProcedure

        If (listPropertyParameter.Count > 0) Then
            For Each item As PropertyParameter In listPropertyParameter
                Dim parameter As DbParameter = command.CreateParameter()

                With parameter
                    .ParameterName = Database.BuildParameterName(item.Name)
                    .Value = item.Value
                    .Direction = item.Direction

                    'Tipo de Dados
                    If (Not item.DbType = Nothing) Then
                        .DbType = item.DbType
                    End If

                    'Tamanho
                    If (item.Size > 0) Then
                        .Size = item.Size
                    End If
                End With

                command.Parameters.Add(parameter)
            Next
        End If

        command.Connection = Connection

        If command.Connection.State = ConnectionState.Closed Then
            command.Connection.Open()
        End If

        Try

            'Executa Proc
            Database.ExecuteNonQuery(command, Transaction)

            'Captura parametros de retorno e seta o dicionario
            If (Not parametersRetorno Is Nothing) Then
                For Each paramRet In listPropertyParameter.Where(Function(p) p.Direction = ParameterDirection.Output).ToList()
                    If (parametersRetorno.ContainsKey(paramRet.Name)) Then
                        parametersRetorno(paramRet.Name) = command.Parameters(paramRet.Name).Value
                    End If
                Next
            End If

        Catch ex As Exception
            Throw ex
        Finally
            CloseConnection()
        End Try

        Return True

    End Function

    Public Function ExecuteReader(Of TReturn As {Class, New})(ByVal sql As String, Optional ByVal parameterMapper As ParameterMapper = Nothing, Optional BancoApp As Boolean = False) As IEnumerable(Of TReturn)


        Dim BaseData As Database

        If (BancoApp) Then
            BaseData = DatabaseApp
        Else
            BaseData = Database
        End If

        Dim command As DbCommand = Nothing
        command = BaseData.GetSqlStringCommand(sql)
        command.CommandType = CommandType.Text

        If (Not parameterMapper Is Nothing) Then
            For Each item In parameterMapper.Parameters
                Dim parameter As DbParameter = command.CreateParameter()

                parameter.ParameterName = BaseData.BuildParameterName(item.Key)

                If (item.Value Is Nothing) Then
                    parameter.Value = DBNull.Value
                Else
                    parameter.Value = item.Value
                End If
                command.Parameters.Add(parameter)
            Next
        End If

        command.Connection = Connection

        If command.Connection.State = ConnectionState.Closed Then
            command.Connection.Open()
        End If

        Try
            Dim reader As IDataReader
            If (Transaction Is Nothing) Then
                reader = BaseData.ExecuteReader(command)
            Else
                reader = BaseData.ExecuteReader(command, Transaction)
            End If
            Dim dt As DataTable = reader.GetSchemaTable()

            Dim Tlist As New List(Of TReturn)()

            While (reader.Read())
                Dim index As Integer = 0
                Dim tObject As New TReturn()
                For Each row In dt.Rows
                    Dim value = reader.GetValue(index)
                    If (Not IsDBNull(value)) Then
                        tObject.GetType().GetProperty(row(0)).SetValue(tObject, value, Nothing)
                    End If
                    index += 1
                Next
                Tlist.Add(tObject)
            End While

            reader.Close()
            reader.Dispose()
            Return Tlist
        Catch ex As Exception
            Throw ex
        Finally
            CloseConnection()
        End Try

    End Function




    Public Sub Dispose() Implements IDisposable.Dispose
        If Not Transaction Is Nothing Then
            Transaction = Nothing
        End If
        If Connection.State = ConnectionState.Open Then
            Connection.Close()
        End If
        If Not Database Is Nothing Then
            Database = Nothing
        End If
    End Sub





    Public Function SelectTListThread(Of TReturn As {Class, New})(ByVal sql As String, Optional ByVal parameterMapper As ParameterMapper = Nothing, Optional ByVal rowMapper As IRowMapper(Of TReturn) = Nothing, Optional BancoApp As Boolean = False) As IEnumerable(Of TReturn)

        Dim factory As DatabaseProviderFactory = New DatabaseProviderFactory(New SystemConfigurationSource())
        DatabaseFactory.SetDatabaseProviderFactory(factory, False)

        Dim BaseData As Database = factory.Create("MRT001")

        Dim conn As DbConnection = BaseData.CreateConnection()

        conn.Open()
        Dim result As IEnumerable(Of TReturn)
        Try
            If (Not parameterMapper Is Nothing) And (Not rowMapper Is Nothing) Then
                result = BaseData.CreateSqlStringAccessor(Of TReturn)(sql, parameterMapper, rowMapper).Execute(Nothing)
            ElseIf (Not parameterMapper Is Nothing) And (rowMapper Is Nothing) Then
                result = BaseData.CreateSqlStringAccessor(Of TReturn)(sql, parameterMapper).Execute(Nothing)
            ElseIf (parameterMapper Is Nothing) And (Not rowMapper Is Nothing) Then
                result = BaseData.CreateSqlStringAccessor(Of TReturn)(sql, New ParameterMapper, rowMapper).Execute(Nothing)
            Else
                result = BaseData.CreateSqlStringAccessor(Of TReturn)(sql).Execute(Nothing)
            End If
        Catch ex As Exception
            Throw ex
        Finally
            conn.Close()
        End Try
        Return result.ToList()

    End Function


    Private Function checkCanCloseConnection() As Boolean
        If Connection.State <> ConnectionState.Open Then
            Return False
        End If

        If Transaction IsNot Nothing Then
            Return False
        End If

        Return True
    End Function
    Protected Sub CloseConnection()
        If checkCanCloseConnection() Then
            Connection.Close()
        End If
    End Sub



End Class
