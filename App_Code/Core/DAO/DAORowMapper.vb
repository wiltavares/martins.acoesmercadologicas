﻿Imports System.Reflection
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class DAORowMapper(Of T As {Class, New})

    ''' <summary>
    ''' Retorna um RowMapper que ira mapear apenas as propriedades passadas por parametro
    ''' Colunas com ALIAS devem ser adicionadas manualmente
    ''' </summary>
    ''' <param name="listaProperties"></param>
    ''' <returns></returns>
    Public Shared Function RowMapperOnlyProperties(listaProperties As IEnumerable(Of String)) As IRowMapper(Of T)
        Dim mapperBuilder As IMapBuilderContext(Of T) = MapBuilder(Of T).MapNoProperties()
        For Each propertyName As String In listaProperties
            Dim propertyByName = GetType(T).GetProperty(propertyName)
            If propertyByName IsNot Nothing Then
                mapperBuilder.MapByName(propertyByName)
            End If
        Next
        Return mapperBuilder.Build()
    End Function

    ''' <summary>
    ''' Retorna um RowMapper que ira mapear apenas as propriedades do sql
    ''' </summary>
    ''' <param name="sqlQuery"></param>
    ''' <returns></returns>
    Public Shared Function RowMapperOnlyProperties(sqlQuery As String) As IRowMapper(Of T)
        Return RowMapperOnlyProperties(PropertiesFromQuery(sqlQuery))
    End Function

    ''' <summary>
    ''' Retorna um RowMapper que ira mapear todas as propriedades do tipo exceto as passadas por parametro
    ''' Colunas com ALIAS devem ser retiradas manualmente
    ''' </summary>
    ''' <param name="listaProperties"></param>
    ''' <returns></returns>
    Public Shared Function RowMapperExceptProperties(listaProperties As IEnumerable(Of String)) As IRowMapper(Of T)
        Dim mapperBuilder As IMapBuilderContext(Of T) = MapBuilder(Of T).MapAllProperties()
        For Each propertyName As String In listaProperties
            Dim propertyByName = GetType(T).GetProperty(propertyName)
            If propertyByName IsNot Nothing Then
                mapperBuilder.DoNotMap(propertyByName)
            End If
        Next
        Return mapperBuilder.Build()
    End Function

    ''' <summary>
    ''' Retorna um RowMapper que ira mapear todas as propriedades do tipo exceto as passadas por parametro
    ''' </summary>
    ''' <param name="sqlQuery"></param>
    ''' <returns></returns>
    Public Shared Function RowMapperExceptProperties(sqlQuery As String) As IRowMapper(Of T)
        Return RowMapperExceptProperties(PropertiesFromQuery(sqlQuery))
    End Function

    ''' <summary>
    ''' Retorna um RowMapper que ira mapear todas as propriedades do tipo exceto pelas marcadas com a annotation IgnoreMap
    ''' </summary>
    ''' <param name="daoConfiguration"></param>
    ''' <returns></returns>
    Public Shared Function RowMapperOnlyTableProperties(daoConfiguration As DAOConfiguration) As IRowMapper(Of T)
        Dim mapperBuilder As IMapBuilderContext(Of T) = MapBuilder(Of T).MapAllProperties()
        For Each propertyInfo As PropertyInfo In GetType(T).GetProperties()
            Dim ignoreMap As IgnoreMapAttribute = Attributes.GetAnnotation(Of IgnoreMapAttribute)(daoConfiguration, propertyInfo.Name)
            If ignoreMap IsNot Nothing Then
                mapperBuilder.DoNotMap(propertyInfo)
            End If
        Next
        Return mapperBuilder.Build()
    End Function

    Public Shared Function PropertiesFromQuery(sqlQuery As String) As IEnumerable(Of String)
        Dim listaPropriedades = New List(Of String)
        Dim parte As String
        Dim partesEsp As String()
        Dim partesPto As String()

        sqlQuery = sqlQuery.Split({" FROM "}, StringSplitOptions.None).First
        sqlQuery = sqlQuery.Split({" from "}, StringSplitOptions.None).First
        sqlQuery = sqlQuery.Split({" From "}, StringSplitOptions.None).First

        If sqlQuery IsNot String.Empty Then
            Dim partesVirg As String() = sqlQuery.Split(","c)
            For i = 0 To partesVirg.Length - 1
                parte = partesVirg(i).Trim()
                partesEsp = parte.Split(" "c)
                parte = partesEsp.Last
                partesPto = parte.Split("."c)
                parte = partesPto.Last
                listaPropriedades.Add(partesPto.Last.Trim())
            Next

            'parte = partesVirg.Last.Trim()
            'partesEsp = parte.Split(" "c)
            'parte = partesEsp.First
            'partesPto = parte.Split("."c)
            'listaPropriedades.Add(partesPto.Last.Trim())
        End If

        Return listaPropriedades
    End Function
End Class
