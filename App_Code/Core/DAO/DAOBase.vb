﻿Imports System.Reflection
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public MustInherit Class DAOBase(Of T As {Class, New})

    Public connector As Connector = New Connector()
    Protected daoConfiguration As DAOConfiguration

    Public Function ListT(ByVal param As T) As List(Of T)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim properties As List(Of PropertyInfo) = (param).GetType().GetProperties().ToList()

        Dim defaltValues As New T()

        Dim sqlColumns As String = String.Empty
        Dim sqlWhere As String = String.Empty
        Dim mapperBuilder = MapBuilder(Of T).MapAllProperties()

        For Each item As PropertyInfo In properties

            Dim ignoreMap As IgnoreMapAttribute = Attributes.GetAnnotation(Of IgnoreMapAttribute)(daoConfiguration, item.Name)

            If Not item.GetGetMethod().IsVirtual Then
                If ignoreMap Is Nothing Then
                    If Not String.IsNullOrEmpty(sqlColumns) Then
                        sqlColumns += ", "
                    End If
                    sqlColumns += item.Name

                    Dim value As Object = item.GetValue(param, Nothing)

                    If (value <> (item.GetValue(defaltValues, Nothing)) _
                        Or (item.PropertyType.IsGenericType AndAlso Not Nullable.Equals(value, (item.GetValue(defaltValues, Nothing))))) Then
                        If (Not value Is Nothing) Then

                            If String.IsNullOrEmpty(sqlWhere) Then
                                sqlWhere += " WHERE "
                            Else
                                sqlWhere += " AND "
                            End If
                            If (item.PropertyType.Equals(GetType(String))) Then

                                value = "%" + value + "%"

                                sqlWhere += "(LOWER(" + item.Name + ") like LOWER(:" + item.Name + "))"
                            Else
                                sqlWhere += "(" + item.Name + " = :" + item.Name + ")"

                            End If
                            parameterMapper.Parameters.Add(item.Name, value)
                        End If
                    End If
                Else
                    mapperBuilder.DoNotMap(item)
                End If
            End If
        Next

        Dim rowMapper As IRowMapper(Of T) = mapperBuilder.Build()

        Dim tableName As TableAttribute = Attributes.GetAnnotation(Of TableAttribute)(daoConfiguration)

        Dim sql As String = String.Format("select {0} from {1} {2}", sqlColumns, tableName.TableName, sqlWhere)

        Return connector.SelectTList(Of T)(sql, parameterMapper, rowMapper)

    End Function

    Public Function SelectT(ByVal param As T) As T

        'Dim parameterMapper As ParameterMapper = New ParameterMapper()

        'Dim properties As List(Of PropertyInfo) = (param).GetType().GetProperties().ToList()

        'Dim defaltValues As New T()

        'Dim sqlColumns As String = String.Empty
        'Dim sqlWhere As String = String.Empty
        'Dim mapperBuilder = MapBuilder(Of T).MapAllProperties()

        'For Each item As PropertyInfo In properties

        '    Dim ignoreMap As IgnoreMapAttribute = Attributes.GetAnnotation(Of IgnoreMapAttribute)(daoConfiguration, item.Name)

        '    If IsNothing(ignoreMap) Then
        '        If Not String.IsNullOrEmpty(sqlColumns) Then
        '            sqlColumns += ", "
        '        End If
        '        sqlColumns += "TRIM(" + item.Name + ") AS " + item.Name
        '        Dim value As Object = item.GetValue(param, Nothing)
        '        If (value <> (item.GetValue(defaltValues, Nothing))) Then
        '            If (Not value Is Nothing) Then
        '                parameterMapper.Parameters.Add(item.Name, value)

        '                If String.IsNullOrEmpty(sqlWhere) Then
        '                    sqlWhere += " WHERE "
        '                Else
        '                    sqlWhere += " AND "
        '                End If
        '                sqlWhere += "(" + item.Name + " = :" + item.Name + ")"
        '            End If
        '        End If
        '    Else
        '        mapperBuilder.DoNotMap(item)
        '    End If

        'Next

        'Dim rowMapper As IRowMapper(Of T) = mapperBuilder.Build()
        'Dim tableName As TableAttribute = Attributes.GetAnnotation(Of TableAttribute)(daoConfiguration)
        'Dim sql As String = String.Format("SELECT {0} FROM {1} {2}", sqlColumns, tableName.TableName, sqlWhere)

        Return ListT(param).FirstOrDefault()

    End Function

    Public Function InsertT(ByRef vo As T) As T

        Dim tableName As TableAttribute = Attributes.GetAnnotation(Of TableAttribute)(daoConfiguration)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim properties As List(Of PropertyInfo) = (vo).GetType().GetProperties().ToList()
        Dim defaltValues As New T()

        Dim sqlColumns As String = String.Empty
        Dim sqlValues As String = String.Empty
        Dim sqlSelect As String = String.Empty
        Dim sqlSequence As String = String.Empty
        Dim FimSql As String = String.Empty
        For Each item As PropertyInfo In properties
            Dim ignoreMap As IgnoreMapAttribute = Attributes.GetAnnotation(Of IgnoreMapAttribute)(daoConfiguration, item.Name)
            Dim isIdentity As IdentityAttribute = Attributes.GetAnnotation(Of IdentityAttribute)(daoConfiguration, item.Name)
            Dim isSequence As SequenceAttribute = Attributes.GetAnnotation(Of SequenceAttribute)(daoConfiguration, item.Name)
            Dim isSelectMax As SelectMaxAttribute = Attributes.GetAnnotation(Of SelectMaxAttribute)(daoConfiguration, item.Name)

            If ignoreMap Is Nothing Then
                If (Not isIdentity Is Nothing) Then
                    sqlSelect = "SELECT CONVERT(INTEGER, @@IDENTITY) AS " + item.Name
                ElseIf (Not isSequence Is Nothing) Then
                    sqlSequence += "DECLARE :" + item.Name + " AS INTEGER "
                    sqlSequence += "SET :" + item.Name + " = NEXT VALUE FOR " + isSequence.SequenceName + " "
                    If Not String.IsNullOrEmpty(sqlColumns) Then
                        sqlColumns += ", "
                        sqlValues += ", "
                    End If
                    sqlColumns += item.Name
                    sqlValues += ":" + item.Name
                    sqlSelect = "SELECT :" + item.Name + " AS " + item.Name
                ElseIf (Not isSelectMax Is Nothing) Then
                    sqlSequence += "DECLARE TMP" + item.Name + " NUMBER; "
                    sqlSequence += "BEGIN SELECT MAX(" + item.Name + ")  + 1 INTO  TMP" + item.Name + " FROM  " + tableName.TableName + "; "
                    If Not String.IsNullOrEmpty(sqlColumns) Then
                        sqlColumns += ", "
                        sqlValues += ", "
                    End If
                    sqlColumns += item.Name
                    sqlValues += "TMP" + item.Name
                    FimSql = "; END;"
                    'sqlSelect = "SELECT MAX(" + item.Name + ") FROM " + tableName.TableName + "; "
                Else
                    Dim isDateInsert As DateInsertAttribute = Attributes.GetAnnotation(Of DateInsertAttribute)(daoConfiguration, item.Name)
                    If (Not isDateInsert Is Nothing) Then
                        item.SetValue(vo, DateTime.Now)
                    End If

                    Dim value As Object = item.GetValue(vo, Nothing)
                    If (Not value Is Nothing) Then
                        If (Not value.Equals((item.GetValue(defaltValues, Nothing)))) Then
                            Dim isReadonly As ReadonlyAttribute = Attributes.GetAnnotation(Of ReadonlyAttribute)(daoConfiguration, item.Name)

                            If (isReadonly Is Nothing) Or (Not isReadonly Is Nothing AndAlso Not isReadonly.Insert) Then

                                parameterMapper.Parameters.Add(item.Name, value)
                                If Not String.IsNullOrEmpty(sqlColumns) Then
                                    sqlColumns += ", "
                                    sqlValues += ", "
                                End If
                                sqlColumns += item.Name
                                sqlValues += ":" + item.Name
                            End If
                        End If
                    End If
                End If
            End If
        Next

        Dim sql As String = String.Format("{0}INSERT INTO {1} ({2}) VALUES ({3}) {4} {5}", sqlSequence, tableName.TableName, sqlColumns, sqlValues, FimSql, sqlSelect)

        Dim result As Object = connector.ExecuteScalar(sql, parameterMapper)

        For Each item As PropertyInfo In properties

            Dim isIdentity As IdentityAttribute = Attributes.GetAnnotation(Of IdentityAttribute)(daoConfiguration, item.Name)
            Dim isSequence As SequenceAttribute = Attributes.GetAnnotation(Of SequenceAttribute)(daoConfiguration, item.Name)

            If (Not isIdentity Is Nothing) Or (Not isSequence Is Nothing) Then
                item.SetValue(vo, result)
            End If
        Next
        Return vo

    End Function

    Public Function UpdateT(ByRef vo As T) As T

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim properties As List(Of PropertyInfo) = (vo).GetType().GetProperties().ToList()

        Dim defaltValues As New T()

        Dim sqlSetValues As String = String.Empty
        Dim sqlWhere As String = String.Empty
        For Each item As PropertyInfo In properties

            Dim value As Object = item.GetValue(vo, Nothing)

            Dim ignoreMap As IgnoreMapAttribute = Attributes.GetAnnotation(Of IgnoreMapAttribute)(daoConfiguration, item.Name)
            Dim isPrimaryKey As PrimaryKeyAttribute = Attributes.GetAnnotation(Of PrimaryKeyAttribute)(daoConfiguration, item.Name)
            If ignoreMap Is Nothing Then

                If (Not isPrimaryKey Is Nothing) Then
                    If (value.Equals(item.GetValue(defaltValues, Nothing))) Then
                        Throw New Exception("Um valor para a chave primaria deve ser informado em uma operação de Update")
                    End If

                    If String.IsNullOrEmpty(sqlWhere) Then
                        sqlWhere += " WHERE "
                    Else
                        sqlWhere += " AND "
                    End If
                    parameterMapper.Parameters.Add(item.Name, value)
                    sqlWhere += item.Name + " =  :" + item.Name
                Else
                    Dim isReadonly As ReadonlyAttribute = Attributes.GetAnnotation(Of ReadonlyAttribute)(daoConfiguration, item.Name)
                    Dim isDateUpdate As DateUpdateAttribute = Attributes.GetAnnotation(Of DateUpdateAttribute)(daoConfiguration, item.Name)
                    If (Not isDateUpdate Is Nothing) Then
                        item.SetValue(vo, DateTime.Now)
                    End If

                    If (isReadonly Is Nothing) Or (Not isReadonly Is Nothing AndAlso Not isReadonly.Update) Then

                        If (Not String.IsNullOrEmpty(sqlSetValues)) Then
                            sqlSetValues += ", "
                        End If

                        sqlSetValues += item.Name + " = :" + item.Name

                        If (value Is Nothing) Then
                            parameterMapper.Parameters.Add(item.Name, DBNull.Value)
                        Else
                            parameterMapper.Parameters.Add(item.Name, value)
                        End If
                    End If
                End If
            End If
        Next

        Dim tableName As TableAttribute = Attributes.GetAnnotation(Of TableAttribute)(daoConfiguration)

        Dim sql As String = String.Format("UPDATE {0} SET {1} {2}", tableName.TableName, sqlSetValues, sqlWhere)

        connector.ExecuteScalar(sql, parameterMapper)

        Return vo

    End Function

    Public Sub DeleteT(ByVal vo As T)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim properties As List(Of PropertyInfo) = (vo).GetType().GetProperties().ToList()

        Dim defaltValues As New T()

        Dim sqlWhere As String = String.Empty
        For Each item As PropertyInfo In properties

            Dim ignoreMap As IgnoreMapAttribute = Attributes.GetAnnotation(Of IgnoreMapAttribute)(daoConfiguration, item.Name)
            If ignoreMap Is Nothing Then

                Dim value As Object = item.GetValue(vo, Nothing)
                If (value <> (item.GetValue(defaltValues, Nothing))) Then
                    If (Not value Is Nothing) Then
                        If String.IsNullOrEmpty(sqlWhere) Then
                            sqlWhere += " WHERE "
                        Else
                            sqlWhere += " AND "
                        End If
                        parameterMapper.Parameters.Add(item.Name, value)
                        sqlWhere += item.Name + " =  :" + item.Name
                    End If
                End If
            End If
        Next

        If (String.IsNullOrEmpty(sqlWhere)) Then
            Throw New Exception("Nehum valor foi passado como condição Where para o delete")
        End If
        Dim tableName As TableAttribute = Attributes.GetAnnotation(Of TableAttribute)(daoConfiguration)

        Dim sql As String = String.Format("DELETE FROM {0} {1}", tableName.TableName, sqlWhere)

        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    ''' <summary>
    ''' Retorna um RowMapper que ira mapear apenas as propriedades passadas por parametro
    ''' Colunas com ALIAS devem ser adicionadas manualmente
    ''' </summary>
    ''' <param name="listaProperties"></param>
    ''' <returns></returns>
    Public Shared Function RowMapperOnlyProperties(listaProperties As IEnumerable(Of String)) As IRowMapper(Of T)
        Return DAORowMapper(Of T).RowMapperOnlyProperties(listaProperties)
    End Function

    ''' <summary>
    ''' Retorna um RowMapper que ira mapear apenas as propriedades do sql
    ''' </summary>
    ''' <param name="sqlQuery"></param>
    ''' <returns></returns>
    Public Shared Function RowMapperOnlyProperties(sqlQuery As String) As IRowMapper(Of T)
        Return DAORowMapper(Of T).RowMapperOnlyProperties(sqlQuery)
    End Function

    ''' <summary>
    ''' Retorna um RowMapper que ira mapear todas as propriedades do tipo exceto as passadas por parametro
    ''' Colunas com ALIAS devem ser retiradas manualmente
    ''' </summary>
    ''' <param name="listaProperties"></param>
    ''' <returns></returns>
    Public Shared Function RowMapperExceptProperties(listaProperties As IEnumerable(Of String)) As IRowMapper(Of T)
        Return DAORowMapper(Of T).RowMapperExceptProperties(listaProperties)
    End Function

    ''' <summary>
    ''' Retorna um RowMapper que ira mapear todas as propriedades do tipo exceto as passadas por parametro
    ''' </summary>
    ''' <param name="sqlQuery"></param>
    ''' <returns></returns>
    Public Shared Function RowMapperExceptProperties(sqlQuery As String) As IRowMapper(Of T)
        Return DAORowMapper(Of T).RowMapperExceptProperties(sqlQuery)
    End Function

    ''' <summary>
    ''' Retorna um RowMapper que ira mapear todas as propriedades do tipo exceto pelas marcadas com a annotation IgnoreMap
    ''' </summary>
    ''' <param name="daoConfiguration"></param>
    ''' <returns></returns>
    Public Shared Function RowMapperOnlyTableProperties(daoConfiguration As DAOConfiguration) As IRowMapper(Of T)
        Return DAORowMapper(Of T).RowMapperOnlyTableProperties(daoConfiguration)
    End Function

    ''' <summary>
    ''' Retorna valor null para o banco oracle se a propriedade tiver valor Nothing caso contrario retorna a propriedade
    ''' </summary>
    ''' <param name="propertyFilter"></param>
    ''' <returns></returns>
    Public Shared Function ValueDBProperty(propertyFilter As Object) As Object
        If propertyFilter Is Nothing Then
            Return DBNull.Value
        Else
            Return propertyFilter
        End If
    End Function

End Class
