﻿Imports Microsoft.VisualBasic

Public Class PropertyParameter
    Public Property Name As String
    Public Property Value As Object
    Public Property Size As Integer
    Public Property Direction As System.Data.ParameterDirection
    Public Property DbType As System.Data.DbType
End Class
