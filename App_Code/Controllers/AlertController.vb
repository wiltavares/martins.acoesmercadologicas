﻿Imports System.Web.Mvc
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers

Namespace Controllers
    Public Class AlertController
        Inherits AbstractController

        Function _Alerts(Optional msgWarning As String = Nothing,
                         Optional msgInfo As String = Nothing,
                         Optional msgDanger As String = Nothing,
                         Optional msgSuccess As String = Nothing) As PartialViewResult

            If (msgWarning IsNot Nothing) Then
                Warning(Constantes.FindMessage(msgWarning))
            End If

            If (msgInfo IsNot Nothing) Then
                Information(Constantes.FindMessage(msgInfo))
            End If

            If (msgDanger IsNot Nothing) Then
                Danger(Constantes.FindMessage(msgDanger))
            End If

            If (msgSuccess IsNot Nothing) Then
                Success(Constantes.FindMessage(msgSuccess))
            End If

            Return PartialView()
        End Function

    End Class
End Namespace