﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList

Namespace Controllers
    <Authorize(Roles:="11")>
    Public Class PoliticasAcaoComercialController
        Inherits AbstractController

        Private politicasAcaoComercialBO As New PoliticasDaAcaoComercialBO()

        ''' <summary>
        ''' Carrega a tela de consulta
        ''' </summary>
        ''' <returns></returns>
        Function Index() As ViewResult
            Return View(GridSettings(Of PoliticasAcaoComercialViewModel).EmptyGrid())
        End Function

        ''' <summary>
        ''' Carrega a tela para criar ou editar um registro
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        Function CreateEdit(Optional ByVal id As Integer? = Nothing) As ActionResult
            If id Is Nothing Then
                Return Create()
            Else
                Return Edit(id)
            End If
        End Function

        ''' <summary>
        ''' Carrega a tela para criar um registro
        ''' </summary>
        ''' <returns></returns>
        Function Create() As ActionResult
            Return View()
        End Function

        ''' <summary>
        ''' Carrega a tela para editar um registro
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        Function Edit(ByVal id As Integer) As ActionResult

            Dim filtro As New VO.PoliticasDaAcaoComercial()
            filtro.CODPOLACOCMC = id
            Dim politicasAcaoComercial = politicasAcaoComercialBO.SelectT(filtro)

            Dim politicasAcaoComercialModel = Mapper.Map(Of VO.PoliticasDaAcaoComercial, PoliticasAcaoComercialViewModel)(politicasAcaoComercial)
            Return View(politicasAcaoComercialModel)
        End Function

        ''' <summary>
        ''' Consulta registros a partir do filtro da pesquisa
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of PoliticasAcaoComercialViewModel)) As PartialViewResult
            Try

           
            Dim filtro As VO.PoliticasDaAcaoComercial = Mapper.Map(Of PoliticasAcaoComercialViewModel, VO.PoliticasDaAcaoComercial)(dadosPesquisa.Filter)
            Dim listaPoliticaAcaoComercial As List(Of VO.PoliticasDaAcaoComercial) = politicasAcaoComercialBO.ListT(filtro)
            Dim listaPoliticaAcaoComercialModel As List(Of PoliticasAcaoComercialViewModel) = Mapper.Map(Of List(Of VO.PoliticasDaAcaoComercial), List(Of PoliticasAcaoComercialViewModel))(listaPoliticaAcaoComercial)

            Dim gridAtualizado = PaginarEAtualizarGrid(listaPoliticaAcaoComercialModel, dadosPesquisa.Page, dadosPesquisa.Filter)
            If gridAtualizado.TotalCount = 0 Then
                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
            End If
            Return PartialView("~/Views/PoliticasAcaoComercial/_ResultadosView.vbhtml", gridAtualizado)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Salva (insere ou atualiza) um registro
        ''' Sucesso: Redireciona para index
        ''' Fracasso: Permanece na tela com a mensagem de erro
        ''' </summary>
        ''' <param name="politicasAcaoComercialViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Salvar(ByVal politicasAcaoComercialViewModel As PoliticasAcaoComercialViewModel) As ActionResult
            Try
                If (Not ModelState.IsValid) Then
                    Return View("CreateEdit", politicasAcaoComercialViewModel)
                End If

                Dim politicasAcaoComercial = Mapper.Map(Of PoliticasAcaoComercialViewModel, VO.PoliticasDaAcaoComercial)(politicasAcaoComercialViewModel)

                If politicasAcaoComercialViewModel.CODPOLACOCMC Is Nothing Then
                    politicasAcaoComercialBO.InsertT(politicasAcaoComercial)
                    Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                Else
                    politicasAcaoComercialBO.UpdateT(politicasAcaoComercial)
                    Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
                End If

                Return RedirectToAction("Index")
            Catch ex As Exception
                Danger(ex)
                Return View("CreateEdit", politicasAcaoComercialViewModel)
            End Try
        End Function

        ''' <summary>
        ''' Deleta um registro e atualiza o grid
        ''' </summary>
        ''' <param name="id"></param>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        Function Deletar(ByVal id As Decimal, dadosPesquisa As GridSettings(Of PoliticasAcaoComercialViewModel)) As PartialViewResult
            Try
                Dim filtro As New VO.PoliticasDaAcaoComercial()
                filtro.CODPOLACOCMC = id
                politicasAcaoComercialBO.DeleteT(filtro)
                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                Return Pesquisar(dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Pesquisar(dadosPesquisa)
            End Try
        End Function
    End Class
End Namespace