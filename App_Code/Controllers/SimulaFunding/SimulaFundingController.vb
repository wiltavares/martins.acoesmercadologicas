﻿Imports Microsoft.VisualBasic
Imports AutoMapper
Imports System.Web.Mvc
Imports PagedList
Imports System.Data
Imports System.Data.OleDb
Imports Microsoft.Reporting.WebForms

Namespace Controllers
    <Authorize(Roles:="36")>
    Public Class SimulaFundingController
        Inherits AbstractController


        Private boMovimentoQuinzenal As New MovimentoQuinzenalPrecoMercadoriaBO
        Private boMercadoria As New MercadoriaBO()
        Private boFilial As New FilialEmpresaCorporativoBO()
        Private boSimulaFunding As New SimulaFundingBO()


        Function Index(dadosPesquisa As GridSettings(Of SimulaFundingViewModel)) As ViewResult
            CarregaDrops()

            dadosPesquisa.Filter = New SimulaFundingViewModel()
            Dim consulta As IEnumerable(Of SimulaFundingViewModel) = New List(Of SimulaFundingViewModel)
            Dim gridConf = New GridSettings(Of SimulaFundingViewModel)(dadosPesquisa.Filter, consulta.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))
            gridConf.ActionName = "resultadosView"
            gridConf.Filter = dadosPesquisa.Filter

            Return View(gridConf)

        End Function


        ''' <summary>
        ''' Simula Funding
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of SimulaFundingViewModel)) As PartialViewResult
            Try
                Dim gridConf As GridSettings(Of SimulaFundingViewModel)

                If (IsNothing(dadosPesquisa.Filter.CODFRN) AndAlso IsNothing(dadosPesquisa.Filter.CODMER AndAlso IsNothing(dadosPesquisa.Filter.DESMER))) Then
                    Warning("Informe algum filtro: Fornecedor, Código da mercadoria ou Nome da mercadoria")
                    Return PartialView("~/Views/ConsultaAcompanhamentoPreAcordo/_ResultadosView.vbhtml")
                End If
                If (IsNothing(dadosPesquisa.Filter.QUANTIDADE)) Then
                    dadosPesquisa.Filter.QUANTIDADE = 1
                End If

                Dim filtro = Mapper.Map(Of SimulaFundingViewModel, VO.SimulaFunding)(dadosPesquisa.Filter)

                Dim listaSemPreco = Mapper.Map(Of List(Of VO.SimulaFunding), List(Of SimulaFundingViewModel))(boSimulaFunding.BuscaItensSimulaFunding(filtro))


                Dim filial As New VO.FilialEmpresaCorporativo
                filial.CODFILEMP = dadosPesquisa.Filter.CODFILEMP
                filial = boFilial.SelectT(filial)

                For Each item In listaSemPreco
                    'Dim aux = Mapper.Map(Of List(Of VO.MovimentoQuinzenalPrecoMercadoriaVO), List(Of MovimentoQuinzenalPrecoMercadoriaViewModel))(boMovimentoQuinzenal.ConsultaPrecoUnitarioItem(item.CODMER, dadosPesquisa.Filter.CODFILEMP, 5)).FirstOrDefault
                    'If (Not IsNothing(aux)) Then
                    '    item.VLRUNT = aux.VLRCSTUNTMER
                    'Else
                    '    item.VLRUNT = Nothing
                    'End If
                    item.PARTICIPACAO = dadosPesquisa.Filter.PARTICIPACAO
                    item.QUANTIDADE = dadosPesquisa.Filter.QUANTIDADE
                    item.DESFILEMP = filial.NOMFILEMP
                Next


                gridConf = PaginarEAtualizarGrid(listaSemPreco, dadosPesquisa.Page, dadosPesquisa.Filter)

                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                Return PartialView("~/Views/SimulaFunding/_ResultadosView.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' função auxiliar para carregar os drops de nescessarios
        ''' </summary>
        Private Sub CarregaDrops()
            Dim listaFornecedores As List(Of Frn) = BuscaFornecedoresLista(Nothing)
            ViewBag.ListaDropFornecedor = listaFornecedores

            Dim filiais As New List(Of FilialEmpresaCorporativoViewModel)
            filiais = Mapper.Map(Of List(Of VO.FilialEmpresaCorporativo), List(Of FilialEmpresaCorporativoViewModel))(boFilial.BuscarCodNomFilial())
            ViewBag.filiais = filiais
        End Sub

        Private Function BuscaFornecedoresLista(ByVal codFrn As Decimal?)
            Return Constantes.FornecedoresCodFrn(codFrn)
        End Function

        Function Exportar(ByVal CODFRN As Decimal?, ByVal CODFILEMP As Decimal?, ByVal CODMER As Decimal?, ByVal DESMER As String, ByVal PARTICIPACAO As Decimal?, ByVal QUANTIDADE As Decimal?) As ActionResult
            Try
                Dim warnings As Warning()
                Dim streamIds As String()
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim extension As String = String.Empty
                Dim fileNameExtension As String = String.Empty

                'Dim relat As LocalReport = New LocalReport
                Dim relat As New ReportViewer

                Dim filtro As VO.SimulaFunding = New VO.SimulaFunding()
                filtro.CODFRN = CODFRN
                filtro.CODFILEMP = CODFILEMP
                filtro.CODMER = CODMER
                filtro.DESMER = DESMER
                filtro.PARTICIPACAO = PARTICIPACAO
                filtro.QUANTIDADE = If(IsNothing(QUANTIDADE), 1, QUANTIDADE)

                If (IsNothing(filtro.CODFRN) AndAlso IsNothing(filtro.CODMER AndAlso IsNothing(filtro.DESMER))) Then
                    Warning("Informe algum filtro(Fornecedor, Código da mercadoria ou Nome da mercadoria")
                    Return PartialView("~/Views/ConsultaAcompanhamentoPreAcordo/_ResultadosView.vbhtml")
                End If

                Dim listaSemPreco = Mapper.Map(Of List(Of VO.SimulaFunding), List(Of SimulaFundingViewModel))(boSimulaFunding.BuscaItensSimulaFunding(filtro))

                Dim filial As New VO.FilialEmpresaCorporativo
                filial.CODFILEMP = CODFILEMP
                filial = boFilial.SelectT(filial)

                For Each item In listaSemPreco
                    'Dim aux = Mapper.Map(Of List(Of VO.MovimentoQuinzenalPrecoMercadoriaVO), List(Of MovimentoQuinzenalPrecoMercadoriaViewModel))(boMovimentoQuinzenal.ConsultaPrecoUnitarioItem(item.CODMER, filtro.CODFILEMP, 5)).FirstOrDefault
                    'If (Not IsNothing(aux)) Then
                    '    item.VLRUNT = aux.VLRCSTUNTMER
                    'Else
                    '    item.VLRUNT = Nothing
                    'End If
                    item.PARTICIPACAO = filtro.PARTICIPACAO
                    item.QUANTIDADE = filtro.QUANTIDADE
                    item.DESFILEMP = filial.NOMFILEMP
                Next



                relat.LocalReport.ReportPath = Server.MapPath("~/Relatorio/SimulaFunding.rdlc")


                relat.LocalReport.DataSources.Add(New ReportDataSource("SimulaFunding", listaSemPreco))

                Dim deviceInfo As String = "<DeviceInfo>" +
                                                                    "<OutputFormat>Excel</OutputFormat>" +
                                                                    "<MarginBottom>0.5in</MarginBottom>" +
                                                                     "</DeviceInfo>"
                '"<PageWidth>9.5in</PageWidth>"
                '"<PageHeight>11in</PageHeight>" +
                '"<MarginTop>0.5in</MarginTop>" +
                '"<MarginLeft>1in</MarginLeft>" +
                '"<MarginRight>1in</MarginRight>" +

                Dim bytes As Byte()
                Dim handle As String = Guid.NewGuid().ToString()
                'bytes = relat.Render("PDF", deviceInfo, mimeType, encoding, fileNameExtension, streamIds, warnings)
                bytes = relat.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, fileNameExtension, streamIds, warnings)

                Dim nomeArquivo As String = ""

                nomeArquivo = "SimulaFunding.xls"

                Session(handle) = bytes
                Dim resultado As JsonResult = New JsonResult() With {.Data = New With {.FileGuid = handle, .FileName = nomeArquivo}}
                resultado.JsonRequestBehavior = JsonRequestBehavior.AllowGet
                Return resultado
                'Return File(bytes, mimeType)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet>
        Public Overridable Function Download(fileGuid As String, fileName As String) As ActionResult
            If Session(fileGuid) IsNot Nothing Then
                Dim data As Byte() = TryCast(Session(fileGuid), Byte())
                Return File(data, "application/xls", fileName)
            Else
                ' Problem - Log the error, generate a blank file,
                '           redirect to another controller action - whatever fits with your application
                Return New EmptyResult()
            End If
        End Function

        <HttpPost()>
        Function AbrirModalItensOrcamento(objeto As String) As PartialViewResult
            Try
                Dim Codigo As Decimal?
                Dim Nome As String

                If (objeto.Length > 1) Then
                    Dim dados As Array = Split(objeto, ",")
                    If (Not String.IsNullOrEmpty(dados(0))) Then
                        Codigo = Decimal.Parse(dados(0))
                    End If
                    Nome = dados(1)
                End If

                Dim gridConf As GridSettings(Of MercadoriaViewModel)
                Dim mercadoria As New VO.Mercadoria

                mercadoria.CODMER = Codigo
                mercadoria.DESMER = Nome

                Dim dadosPesquisa As New GridSettings(Of MercadoriaViewModel)

                dadosPesquisa.Filter = New MercadoriaViewModel With {.CODMER = Codigo, .DESMER = Nome}

                If (IsNothing(dadosPesquisa.Filter.DESMER) And IsNothing(dadosPesquisa.Filter.CODMER)) Then
                    Warning("Favor Utilizar algum filtro.")

                    Dim grid = New List(Of MercadoriaViewModel)
                    gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT}, New MercadoriaViewModel With {.CODMER = Codigo, .DESMER = Nome})

                    gridConf.ActionName = "PesquisarItensOrcamento"
                    gridConf.ResultID = "resultadosBuscaItens"
                    gridConf.PaginationID = "resultadosBuscaItens"
                    Return PartialView("~/Views/SimulaFunding/_ModalBuscaItensOrcamento.vbhtml", gridConf)
                Else
                    Dim mercadorias As List(Of MercadoriaViewModel) = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.BuscaItensOrcamento(mercadoria))

                    gridConf = PaginarEAtualizarGrid(mercadorias, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT}, New MercadoriaViewModel With {.CODMER = Codigo, .DESMER = Nome})

                    If gridConf.TotalCount = 0 Then
                        Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    End If

                    gridConf.ActionName = "PesquisarItensOrcamento"
                    gridConf.ResultID = "resultadosBuscaItensOrcamento"
                    Return PartialView("~/Views/SimulaFunding/_ModalBuscaItensOrcamento.vbhtml", gridConf)
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        <HttpPost()>
        Function PesquisarItensOrcamento(dadosPesquisa As GridSettings(Of MercadoriaViewModel)) As PartialViewResult
            Try
                Dim Mercadorias As VO.Mercadoria = Mapper.Map(Of MercadoriaViewModel, VO.Mercadoria)(dadosPesquisa.Filter)
                Dim listMercadorias As List(Of VO.Mercadoria) = boMercadoria.BuscaItensOrcamento(Mercadorias)
                Dim mercadoriaViewModel As List(Of MercadoriaViewModel) = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(listMercadorias)

                Dim gridConf = PaginarEAtualizarGrid(mercadoriaViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                gridConf.ResultID = "resultadosBuscaItensOrcamento"
                gridConf.PaginationID = "resultadosBuscaItensOrcamento"
                gridConf.ActionName = "PesquisarItensOrcamento"
                gridConf.ControllerName = "ConsultaAcompanhamentoPreAcordo"
                gridConf.Filter = dadosPesquisa.Filter

                Return PartialView("~/Views/ConsultaAcompanhamentoPreAcordo/_ResultadosItensOrcamentoView.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

    End Class
End Namespace