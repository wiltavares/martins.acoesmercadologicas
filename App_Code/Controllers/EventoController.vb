﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList
Imports Martins.AcoesMercadologicas.Core

Namespace Controllers
    <Authorize(Roles:="5")>
    Public Class EventoController
        Inherits AbstractController
        Private eventoBO As New EventoBO()
        Private boTipoEvento As New TipoDeEventoBO()
        Private boDestino As New DestinoDescontoPorBonificacaoPedidoCompraBO()

        ''' <summary>
        ''' Carrega pagina princial
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        Function Index(dadosPesquisa As GridSettings(Of EventoViewModel)) As ViewResult

            CarregaDrops()


            Return View(GridSettings(Of EventoViewModel).EmptyGrid())

        End Function
        ''' <summary>
        ''' procura registros pelo filtro dado
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of EventoViewModel)) As PartialViewResult
            Try

                Dim evento As VO.Evento = Mapper.Map(Of EventoViewModel, VO.Evento)(dadosPesquisa.Filter)
                Dim eventos As List(Of VO.Evento) = eventoBO.ListEvento(evento)
                Dim eventosViewModel As List(Of EventoViewModel) = Mapper.Map(Of List(Of VO.Evento), List(Of EventoViewModel))(eventos)

                Dim gridConf = PaginarEAtualizarGrid(eventosViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If
                Return PartialView("~/Views/Evento/_ResultadosView.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' função auxiliar para carregar os drops de nescessarios
        ''' </summary>
        Private Sub CarregaDrops()

            Dim tiposEvento As List(Of VO.TipoDeEvento) = boTipoEvento.ListT(New VO.TipoDeEvento())
            ViewBag.tipoEvento = tiposEvento

            Dim empenho As List(Of VO.DestinoDescontoPorBonificacaoPedidoCompra) = boDestino.ListT(New VO.DestinoDescontoPorBonificacaoPedidoCompra())
            ViewBag.empenho = empenho

        End Sub

        ''' <summary>
        ''' Deleta um registro
        ''' </summary>
        ''' <param name="id"></param>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Delete(ByVal id As Decimal, dadosPesquisa As GridSettings(Of EventoViewModel)) As PartialViewResult
            Try
                Dim filtro As New VO.Evento()
                filtro.CODEVTACOCMC = id
                eventoBO.DeleteT(filtro)
                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                Return Pesquisar(dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Pesquisar(dadosPesquisa)
            End Try
        End Function

        ''' <summary>
        ''' Função Para decidir se a ação é de criação ou edição
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        Function CreateEdit(Optional ByVal id As Integer? = Nothing) As ActionResult

            If id Is Nothing Then
                ViewBag.acao = "Save"
                Return Create()
            Else
                ViewBag.acao = "Alterar"
                Return Edit(id)
            End If
        End Function

        ''' <summary>
        ''' Salvar novo registro
        ''' </summary>
        ''' <param name="eventoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Save(ByVal eventoViewModel As EventoViewModel) As ActionResult
            Try
                CarregaDrops()
                If (ModelState.IsValid) Then
                    Dim evento = Mapper.Map(Of EventoViewModel, VO.Evento)(eventoViewModel)
                    eventoBO.InsertT(evento)
                    Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                    Return RedirectToAction("Index")
                End If

                Return View("CreateEdit", eventoViewModel)
            Catch
                Return View()
            End Try
        End Function

        ''' <summary>
        ''' Altera registro
        ''' </summary>
        ''' <param name="eventoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Alterar(ByVal eventoViewModel As EventoViewModel) As ActionResult
            Try
                CarregaDrops()

                If (ModelState.IsValid) Then
                    Dim evento = Mapper.Map(Of EventoViewModel, VO.Evento)(eventoViewModel)
                    eventoBO.UpdateT(evento)
                    Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
                    Return RedirectToAction("Index")

                End If

                Return View("CreateEdit", eventoViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Criar novo registro
        ''' </summary>
        ''' <returns></returns>
        Function Create() As ActionResult

            CarregaDrops()

            Dim retorno As EventoViewModel = New EventoViewModel()
            Dim maior As IEnumerable(Of VO.Evento) = eventoBO.ListT(New VO.Evento())
            retorno.CODEVTACOCMC = maior.Max(Function(f) f.CODEVTACOCMC) + 1

            Return View(retorno)
        End Function

        ''' <summary>
        ''' Editar resgistro existente
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        Function Edit(ByVal id As Integer) As ActionResult
            CarregaDrops()

            Dim filtro As New VO.Evento()
            filtro.CODEVTACOCMC = id

            Dim evento = eventoBO.SelectT(filtro)
            Dim eventoViewModel = Mapper.Map(Of VO.Evento, EventoViewModel)(evento)

            Return View(eventoViewModel)
        End Function


        ''' <summary>
        ''' Retorna os evendo de um tipo evento
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        <HttpGet()>
        <AllowAnonymous>
        Function ListarEventosDoTipoEvento(ByVal id As Integer) As ActionResult
            Dim eventos = eventoBO.ListEventoDoTipoEvento(id)
            Return Json(Mapper.Map(Of List(Of VO.Evento), List(Of EventoViewModel))(eventos), JsonRequestBehavior.AllowGet)
        End Function

    End Class
End Namespace