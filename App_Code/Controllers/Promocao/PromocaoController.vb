﻿Imports Microsoft.VisualBasic
Imports System.Web.Mvc
Imports AutoMapper
Imports System.Data

Namespace Controllers

    <Authorize(Roles:="43,44")>
    Public Class PromocaoController
        Inherits AbstractController
        'Private promocaoBO As New PromocaoBO
        'Private boFilialEmpresaCorporativo As New FilialEmpresaCorporativoBO()
        'Private boBU As New UnidadeEstrategicaDeNegocioBO()



        Function Index2() As ViewResult
            Try
                Return View("~/Views/Promocao/Index2.vbhtml")
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function
        Function Mecanica2() As ViewResult
            Try

                Return View()


            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        ''' <summary>
        ''' Carrega a tela de consulta
        ''' </summary>
        ''' <returns></returns>
        Function Index() As ViewResult
            CarregaListasDropDown()

            Dim grid = GridSettings(Of PromocaoViewModel).EmptyGrid()
            'grid.Filter.CODFNCGRCPMC = ControleAcesso.CodFnc()

            Return View(grid)
        End Function


        ''' <summary>
        ''' Consulta registros a partir do filtro da pesquisa
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of PromocaoViewModel)) As PartialViewResult
            Try
                Dim promocaoBO As New PromocaoBO
                Dim filtro As VO.Promocao = Mapper.Map(Of PromocaoViewModel, VO.Promocao)(dadosPesquisa.Filter)
                Dim listaPromocao As List(Of VO.Promocao) = promocaoBO.ListaParaGrid(filtro)
                Dim listaPromocaoViewModel As List(Of PromocaoViewModel) = Mapper.Map(Of List(Of VO.Promocao), List(Of PromocaoViewModel))(listaPromocao)

                Dim gridAtualizado = PaginarEAtualizarGrid(listaPromocaoViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridAtualizado.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                Return PartialView("~/Views/Promocao/_ResultadosView.vbhtml", gridAtualizado)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function



        ''' <summary>
        ''' Editar resgistro existente
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        Function Edit(ByVal id As Integer) As ActionResult
            ViewBag.acao = "Alterar"

            Dim promocaoBO As New PromocaoBO
            Dim promocao = promocaoBO.SelectT(New VO.Promocao() With {.CODPMC = id, .TIPEDEPUBALVPMC = 4})

            Dim filialBO As New FilialEmpresaCorporativoBO
            Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
            promocao.NOMFILEMP = filial.NOMFILEMP

            Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

            Dim relacaoAcaoComercialFornecedorBO As New RelacaoAcaoComercialxFornecedoresBO
            Dim relacaoAcaoComercialFornecedor = relacaoAcaoComercialFornecedorBO.BuscarFornecedoresDaAcao(promocao.CODACOCMC).FirstOrDefault()
            promocaoViewModel.TIPFRNACOCMC = relacaoAcaoComercialFornecedor.TIPFRNACOCMC
            promocaoViewModel.NOMTIPFRNACOCMC = If(relacaoAcaoComercialFornecedor.TIPFRNACOCMC = 1, "EDL", "Martins")

            Dim criterioAdicionalAvaliacaoPromocaoBO As New CriterioAdicionalAvaliacaoPromocaoBO
            Dim criterioAdicionalAvaliacaoPromocao = criterioAdicionalAvaliacaoPromocaoBO.SelectT(New VO.CriterioAdicionalAvaliacaoPromocao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC})
            If (Not criterioAdicionalAvaliacaoPromocao Is Nothing) Then
                promocaoViewModel.FTRACRCTRAVLPMC = criterioAdicionalAvaliacaoPromocao.FTRACRCTRAVLPMC
                promocaoViewModel.VLRLIMMAXCTRAVLPMC = criterioAdicionalAvaliacaoPromocao.VLRLIMMAXCTRAVLPMC
                promocaoViewModel.QDEMNMITEPEDPMC = criterioAdicionalAvaliacaoPromocao.QDEMNMITEPEDPMC
            End If

            CarregaListasDropDown()
            ControlaVisibilidadeComponentes(promocaoViewModel)
            PreencheListaFaixa(promocao.CODPMC)

            If (promocao.INDCOBBDEPMC = 0) Then
                ViewBag.isCob = False
            Else
                ViewBag.isCob = True
            End If

            If (promocao.TIPSITPMC = "E") Then
                ViewBag.encerrada = True
            Else
                ViewBag.encerrada = False
            End If

            Dim acaoComercialBO As New AcaoComercialBO

            Dim acaoComercial As VO.AcaoComercial = acaoComercialBO.BuscaAcaoPorId(promocaoViewModel.CODACOCMC)
            ViewBag.AcaoComercial = Mapper.Map(Of VO.AcaoComercial, AcaoComercialViewModel)(acaoComercial)

            Dim EscalaProgressivaBO As New RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO()
            Dim FaixaFinitaBO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaBO()
            Dim EscalaProgressivaVO = EscalaProgressivaBO.BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva With {.CODPMC = id})
            Dim FaixaFinitaVO = FaixaFinitaBO.BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita With {.CODPMC = id})

            If (EscalaProgressivaVO.Count > 0 AndAlso Not EscalaProgressivaVO.Where(Function(r) r.INDTIPPTOPMC <> 0).FirstOrDefault() Is Nothing) Then
                ViewBag.INDTIPPTOPMC = EscalaProgressivaVO.Where(Function(r) r.INDTIPPTOPMC <> 0).FirstOrDefault().INDTIPPTOPMC
            ElseIf (FaixaFinitaVO.Count > 0 AndAlso Not FaixaFinitaVO.Where(Function(r) r.INDTIPPTOPMC <> 0).FirstOrDefault() Is Nothing) Then
                ViewBag.INDTIPPTOPMC = FaixaFinitaVO.Where(Function(r) r.INDTIPPTOPMC <> 0).FirstOrDefault().INDTIPPTOPMC
            Else
                ViewBag.INDTIPPTOPMC = 0
            End If

            Dim validaCashBack As VO.Promocao = promocaoBO.VerificaSeCashBack(promocao.CODPMC)
            promocaoViewModel.INDTIPPTOPMC1 = validaCashBack.INDTIPPTOPMC1
            promocaoViewModel.INDTIPPTOPMC2 = validaCashBack.INDTIPPTOPMC2

            If (promocaoViewModel.INDTIPPTOPMC1 > 0 Or promocaoViewModel.INDTIPPTOPMC2 > 0) Then
                ViewBag.IsCashBack = True
            End If

            Dim boEventoPromocional As New EventoPromocionalBO()
                Dim eventoPromocional As VO.EventoPromocional = boEventoPromocional.SelectT(New VO.EventoPromocional With {.CODEVTBTB = promocao.CODEVTBTB})
            If (eventoPromocional IsNot Nothing) Then
                promocaoViewModel.NOMEVTBTB = eventoPromocional.DESEVTBTB
            End If

            Return View("~/Views/Promocao/Edit.vbhtml", promocaoViewModel)
        End Function

        ''' <summary>
        ''' Modal Confirma Liberacao
        ''' </summary>
        ''' <param name="id"></param>
        ''' <param name="tipFrnAcoCmc"></param>
        ''' <returns></returns>
        <HttpGet()>
        Function ModalLiberarPromocao(ByVal id As Integer) As ActionResult
            Dim promocaoBO As New PromocaoBO
            Dim filtro As New VO.Promocao()
            filtro.CODPMC = id
            ViewBag.acao = "Alterar"
            Dim promocao = promocaoBO.SelectT(filtro)

            Dim filialBO As New FilialEmpresaCorporativoBO
            Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
            promocao.NOMFILEMP = filial.NOMFILEMP


            Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

            Dim criterioAdicionalAvaliacaoPromocaoBO As New CriterioAdicionalAvaliacaoPromocaoBO
            Dim criterioAdicionalAvaliacaoPromocao = criterioAdicionalAvaliacaoPromocaoBO.SelectT(New VO.CriterioAdicionalAvaliacaoPromocao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC})
            If (Not criterioAdicionalAvaliacaoPromocao Is Nothing) Then
                promocaoViewModel.FTRACRCTRAVLPMC = criterioAdicionalAvaliacaoPromocao.FTRACRCTRAVLPMC
                promocaoViewModel.VLRLIMMAXCTRAVLPMC = criterioAdicionalAvaliacaoPromocao.VLRLIMMAXCTRAVLPMC
                promocaoViewModel.QDEMNMITEPEDPMC = criterioAdicionalAvaliacaoPromocao.QDEMNMITEPEDPMC
            End If

            CarregaListasDropDown()
            ControlaVisibilidadeComponentes(promocaoViewModel)
            PreencheListaFaixa(promocao.CODPMC)

            Dim CODPMC = promocaoViewModel.CODPMC
            Dim TIPEDEPUBALVPMC = 4
            CarregaListasPublicoAlvo(CODPMC, TIPEDEPUBALVPMC)

            'total mix
            Dim BOrelacaoAcaoFonecedor As RelacaoAcaoComercialxFornecedoresBO = New RelacaoAcaoComercialxFornecedoresBO()
            Dim relacaoAcaoFonecedor = BOrelacaoAcaoFonecedor.BuscarFornecedoresDaAcao(promocao.CODACOCMC).FirstOrDefault()

            Dim PromocaoMixVisualizarVigenteBO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()
            Dim List As List(Of VO.PromocaoMixVisualizarVigente)
            If (relacaoAcaoFonecedor.TIPFRNACOCMC = 1) Then
                List = PromocaoMixVisualizarVigenteBO.PreVisualizarMixPromocaoEdl(CODPMC, promocao.TIPSITPMC)
            Else
                List = PromocaoMixVisualizarVigenteBO.PreVisualizarMixPromocao(CODPMC, TIPEDEPUBALVPMC, promocao.TIPPMC)
            End If
            Dim mixPromocaoList As List(Of PromocaoMixVisualizarVigenteViewModel) = Mapper.Map(Of List(Of VO.PromocaoMixVisualizarVigente), List(Of PromocaoMixVisualizarVigenteViewModel))(List)

            ViewBag.TotalMix = mixPromocaoList.Count()

            Return PartialView(promocaoViewModel)
        End Function


        Private Sub PreencheListaFaixa(CODPMC As Decimal)

            ViewBag.listaFaixa = New List(Of SelectListItem)()

            Dim RelacaoPromocaoCriterioApuracaoFaixaFinitaBO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaBO()
            Dim ListRelacaoPromocaoCriterioApuracaoFaixaFinita = RelacaoPromocaoCriterioApuracaoFaixaFinitaBO.BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {.CODPMC = CODPMC})
            If (ListRelacaoPromocaoCriterioApuracaoFaixaFinita.Count > 0) Then
                Dim cont As Integer = 1
                For Each item In ListRelacaoPromocaoCriterioApuracaoFaixaFinita
                    ViewBag.listaFaixa.Add(New SelectListItem() With {.Text = cont.ToString(), .Value = cont})
                    cont = cont + 1
                Next
            End If
        End Sub

        ''' <summary>
        ''' Altera registro
        ''' </summary>
        ''' <param name="promocaoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Alterar(ByVal promocaoViewModel As PromocaoViewModel) As ActionResult
            Try
                Dim promocaoBO As New PromocaoBO
                Dim brindesAdicionaisBO As New PromocaoBrindeBO

                Dim promocao As VO.Promocao = promocaoBO.SelectT(New VO.Promocao() With {.CODPMC = promocaoViewModel.CODPMC})

                ' Validações do estado da promoçao
                Dim resultValidacao As String = Nothing

                'busca valores dos campos que estão desabilitados na tela
                Select Case promocao.TIPSITPMC
                    Case "I" 'À Liberar
                        promocaoViewModel.DESCMCPMC = promocaoViewModel.DESCMCPMC.Replace(vbCrLf, " ")
                        promocao.CODUNDESRNGC = promocaoViewModel.CODUNDESRNGC
                        promocao.NOMPMC = promocaoViewModel.NOMPMC
                        promocao.FLGENVPMC = promocaoViewModel.FLGENVPMC
                        promocao.DATINIPMC = promocaoViewModel.DATINIPMC
                        promocao.DATFIMPMC = promocaoViewModel.DATFIMPMC
                        promocao.TIPEQIVND = promocaoViewModel.TIPEQIVND
                        promocao.INDCTRADIAVLPMC = promocaoViewModel.INDCTRADIAVLPMC
                        promocao.INDCOBBDEPMC = promocaoViewModel.INDCOBBDEPMC
                        promocao.INDISRBDEADI = promocaoViewModel.INDISRBDEADI
                        promocao.INDPMCVNDSMP = promocaoViewModel.INDPMCVNDSMP
                        promocao.INDPMCVLDAPLMVLCLI = promocaoViewModel.INDPMCVLDAPLMVLCLI
                        promocao.DESCMCPMC = promocaoViewModel.DESCMCPMC
                        promocao.DESPMC = promocaoViewModel.DESCMCPMC.Substring(0, IIf(promocaoViewModel.DESCMCPMC.Length() > 60, 60, promocaoViewModel.DESCMCPMC.Length()))
                        promocao.QDEMAXBDECLI = promocaoViewModel.QDEMAXBDECLI
                        promocao.INDCPTFCH = If(promocaoViewModel.INDCPTFCH Is Nothing, 0, promocaoViewModel.INDCPTFCH)
                        promocao.TIPNIVCLFCPT = If(promocaoViewModel.TIPNIVCLFCPT Is Nothing, 0, promocaoViewModel.TIPNIVCLFCPT)
                        promocao.TIPNATVND = If(promocaoViewModel.TIPNATVND Is Nothing, 0, promocaoViewModel.TIPNATVND)
                        promocao.INDPMCIMD = If(promocaoViewModel.INDPMCIMD Is Nothing, 0, promocaoViewModel.INDPMCIMD)

                        If (promocaoViewModel.INDPMCIMD = 1 And promocaoViewModel.HRAINIPMC IsNot Nothing And promocaoViewModel.HRAFIMPMC IsNot Nothing) Then
                            promocao.HRAINIPMC = New Date(promocao.DATINIPMC.Value.Year, promocao.DATINIPMC.Value.Month, promocao.DATINIPMC.Value.Day, promocaoViewModel.HRAINIPMC.Value.Hour, promocaoViewModel.HRAINIPMC.Value.Minute, promocaoViewModel.HRAINIPMC.Value.Second)
                            promocao.HRAFIMPMC = New Date(promocao.DATFIMPMC.Value.Year, promocao.DATFIMPMC.Value.Month, promocao.DATFIMPMC.Value.Day, promocaoViewModel.HRAFIMPMC.Value.Hour, promocaoViewModel.HRAFIMPMC.Value.Minute, promocaoViewModel.HRAFIMPMC.Value.Second)

                            promocaoViewModel.HRAINIPMC = promocao.HRAINIPMC
                            promocaoViewModel.HRAFIMPMC = promocao.HRAFIMPMC
                        Else
                            promocao.HRAINIPMC = Nothing
                            promocao.HRAFIMPMC = Nothing
                        End If

                    Case "E" 'Encerrada
                        Warning("Promoção encerrada, não é possível alterar.")
                        Return New HttpStatusCodeResult(Net.HttpStatusCode.BadRequest)

                    Case " ", "" 'Liberada
                        promocao.DATFIMPMC = promocaoViewModel.DATFIMPMC
                        promocao.INDPMCVNDSMP = promocaoViewModel.INDPMCVNDSMP
                        promocao.INDPMCVLDAPLMVLCLI = promocaoViewModel.INDPMCVLDAPLMVLCLI

                        If (promocao.INDPMCIMD = 1) Then
                            promocao.HRAFIMPMC = New Date(promocao.DATFIMPMC.Value.Year, promocao.DATFIMPMC.Value.Month, promocao.DATFIMPMC.Value.Day, promocaoViewModel.HRAFIMPMC.Value.Hour, promocaoViewModel.HRAFIMPMC.Value.Minute, promocaoViewModel.HRAFIMPMC.Value.Second)
                        Else
                            promocao.HRAFIMPMC = Nothing
                        End If

                        resultValidacao = promocaoBO.ValidarPromocaoLiberada(promocaoViewModel)
                    Case Else 'desconhecido 
                        Warning("Situação desconhecida.")
                        Return New HttpStatusCodeResult(Net.HttpStatusCode.BadRequest)


                End Select

                resultValidacao = promocaoBO.Validar(promocaoViewModel)

                If (resultValidacao <> Nothing) Then
                    Warning(resultValidacao)
                    Return New HttpStatusCodeResult(Net.HttpStatusCode.BadRequest)
                End If


                'Dim promocao = Mapper.Map(Of PromocaoViewModel, VO.Promocao)(promocaoViewModel)
                Dim criterioAdicionalAvaliacaoPromocao As New VO.CriterioAdicionalAvaliacaoPromocao() With {
                    .FTRACRCTRAVLPMC = promocaoViewModel.FTRACRCTRAVLPMC,
                    .VLRLIMMAXCTRAVLPMC = promocaoViewModel.VLRLIMMAXCTRAVLPMC,
                    .QDEMNMITEPEDPMC = promocaoViewModel.QDEMNMITEPEDPMC
                }


                promocaoBO.UpdatePromocao(promocao, criterioAdicionalAvaliacaoPromocao)

                Success(Constantes.REGISTRO_ALTERADO_SUCESSO)

                Return New HttpStatusCodeResult(Net.HttpStatusCode.OK)
            Catch ex As Exception
                Danger(ex)

                Return New HttpStatusCodeResult(Net.HttpStatusCode.BadRequest)

            End Try
        End Function

        ''' <summary>
        ''' Libera Promocao
        ''' </summary>
        ''' <param name="codPmc"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Liberar(ByVal CODPMC As Integer) As Object
            Try
                ' Validações do estado da promoçao
                Dim promocaoBO As New PromocaoBO
                Dim resultValidacao = promocaoBO.Liberar(CODPMC)

                If (resultValidacao.Erro = "QtdVenda") Then
                    Return PartialView("~/Views/Promocao/CriterioApuracao/_EditCriterioErroValidaQtd.vbhtml", resultValidacao.lstPromocaoErros)
                End If

                If (resultValidacao.Erro = "QtdMult") Then
                    Return PartialView("~/Views/Promocao/CriterioApuracao/_EditCriterioErroValidaQtdPmcNaoMult.vbhtml", resultValidacao.lstPromocaoErros)
                End If

                If (resultValidacao.Erro IsNot Nothing) Then
                    resultValidacao.Erro = resultValidacao.Erro.Replace("#SPACE#", "</br>")
                End If

                Return PartialView(resultValidacao)
            Catch ex As Exception
                Danger(ex)
                Return PartialView(New RetornoValidaQtd With {.Erro = ex.Message})
            End Try
        End Function

        ''' <summary>
        ''' Salvar novo registro
        ''' </summary>
        ''' <param name="promocaoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Save(ByVal promocaoViewModel As PromocaoViewModel) As ActionResult
            Try
                Dim promocaoBO As New PromocaoBO
                If (ModelState.IsValid) Then
                    Dim promocao = Mapper.Map(Of PromocaoViewModel, VO.Promocao)(promocaoViewModel)
                    promocaoBO.InsertT(promocao)
                    Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                    Return RedirectToAction("Index")
                End If

                Return View("CreateEdit", promocaoViewModel)
            Catch
                Return View()
            End Try
        End Function

        ''' <summary>
        ''' Carrega listas de dropdown das telas de cadastro e pesquisa
        ''' </summary>
        Private Sub CarregaListasDropDown()
            Dim boFilialEmpresaCorporativo As New FilialEmpresaCorporativoBO()
            Dim boBU As New UnidadeEstrategicaDeNegocioBO()
            Dim listFilial As List(Of FilialEmpresaCorporativoViewModel) = Mapper.Map(Of List(Of VO.FilialEmpresaCorporativo), List(Of FilialEmpresaCorporativoViewModel))(boFilialEmpresaCorporativo.ListFiliais())
            ViewBag.listaFilial = listFilial

            Dim listaBU As List(Of UnidadeEstrategicaDeNegocioViewModel) = Mapper.Map(Of List(Of VO.UnidadeEstrategicaDeNegocio), List(Of UnidadeEstrategicaDeNegocioViewModel))(boBU.ListT(New VO.UnidadeEstrategicaDeNegocio()))
            ViewBag.listaBU = listaBU

            ViewBag.listaSituacao = New List(Of SelectListItem)()
            ViewBag.listaSituacao.Add(New SelectListItem() With {.Text = "À Liberar", .Value = "I"})
            ViewBag.listaSituacao.Add(New SelectListItem() With {.Text = "Encerrada", .Value = "E"})
            ViewBag.listaSituacao.Add(New SelectListItem() With {.Text = "Liberada", .Value = " "})
            ViewBag.listaSituacao.Add(New SelectListItem() With {.Text = "Suspensa", .Value = "S"})

            ViewBag.listaStatusEnvio = New List(Of SelectListItem)()
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Selecione...", .Value = ""})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Enviado", .Value = " "})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "A Enviar", .Value = "S"})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Não Enviar", .Value = "N"})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Comércio Eletrônico", .Value = "I"})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Telemarketing", .Value = "T"})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Presencial", .Value = "P"})


            ViewBag.listaFaixa = New List(Of SelectListItem)()
            ViewBag.listaFaixa.Add(New SelectListItem() With {.Text = "0", .Value = 0})

            Dim boAauthor As New FuncionarioBO()
            ViewBag.ListaFuncionarios = boAauthor.ListFuncionariosPromocao(New VO.Funcionario()).OrderBy(Function(m) m.CODFNC)

            Dim boTipoClassificacaoCombo As New TipoClassificacaoComboBO()
            ViewBag.listaTipoClassificacaoCombo = boTipoClassificacaoCombo.CarregaDropTipoClassificacaoCombo(Nothing, Nothing)

            ViewBag.listaNaturezaVenda = New List(Of SelectListItem)()
            ViewBag.listaNaturezaVenda.Add(New SelectListItem() With {.Text = "Ambos", .Value = "0"})
            ViewBag.listaNaturezaVenda.Add(New SelectListItem() With {.Text = "Nativo (Cliente)", .Value = "1"})
            ViewBag.listaNaturezaVenda.Add(New SelectListItem() With {.Text = "Não Nativo (Força de Vendas)", .Value = "2"})

        End Sub

        <HttpGet()>
        Function CarregarWidgetPublicoAlvo(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As PartialViewResult
            Try

                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()

                Dim promocaoPublicoAlvoSelecaoList = promocaoPublicoAlvoSelecaoBO.ListT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})

                'Dim BO As PromocaoPublicoAlvoBO = New PromocaoPublicoAlvoBO()
                'Dim obj As VO.PromocaoPublicoAlvo = BO.SelectCountPublicoAlvo(CODPMC, TIPEDEPUBALVPMC)

                'Dim objPublicoAlvo As PromocaoPublicoAlvoViewModel = Mapper.Map(Of VO.PromocaoPublicoAlvo, PromocaoPublicoAlvoViewModel)(obj)

                Dim promocaoPublicoAlvoSelecaoViewModelList As List(Of PromocaoPublicoAlvoSelecaoViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoSelecao), List(Of PromocaoPublicoAlvoSelecaoViewModel))(promocaoPublicoAlvoSelecaoList)

                Return PartialView("~/Views/Promocao/PublicoAlvo/Edit.vbhtml", promocaoPublicoAlvoSelecaoViewModelList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function CarregarWidgetClientesExcludentes(ByVal CODPMC As Decimal) As PartialViewResult
            Try

                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim promocaoClientesExcludentesBO As New PromocaoClientesExcludentesBO()
                Dim promocaoClientesExcludentesList = promocaoClientesExcludentesBO.ListT(New VO.PromocaoClientesExcludentes() With {.CODPMC = CODPMC})

                Dim promocaoClientesExcludentesViewModelList As List(Of PromocaoClientesExcludentesViewModel) = Mapper.Map(Of List(Of VO.PromocaoClientesExcludentes), List(Of PromocaoClientesExcludentesViewModel))(promocaoClientesExcludentesList)

                Return PartialView("~/Views/Promocao/ClientesExcludentes/Edit.vbhtml", promocaoClientesExcludentesViewModelList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            ViewBag.PermiteEdicaoPublicoAlvoPromocao = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")
            ViewBag.PermiteEdicaoPromocaoMix = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")
            ViewBag.PermiteEdicaoClientesExcludentes = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

        End Sub

        <HttpPost()>
        Function AlterarDataFimPromocao(ByVal DATFIMPMC As String, ByVal CODPMCList As List(Of Decimal)) As Boolean

            Dim promocaoBO As New PromocaoBO()

            Dim result = promocaoBO.AlterarDataFimPromocao(DATFIMPMC, CODPMCList)

            'Se der erro mostrar na tela
            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return False
            Else
                Success("Data alterada com sucesso!")
                Return True
            End If

        End Function

        <HttpPost>
        Public Function CarregarModalAlterarDataFimPromocao(ByVal objeto As PromocaoViewModel) As PartialViewResult

            Return PartialView("~/Views/Promocao/ModalAlterarDataFimPromocao.vbhtml", objeto)

        End Function



        ''' <summary>
        ''' Clona a Promocao
        ''' </summary>
        ''' <param name="CODPMC"></param>
        ''' <returns></returns>
        <HttpPost()>
        <Authorize(Roles:="46")>
        Function Clonar(ByVal CODPMC As Decimal) As ActionResult
            Try
                Dim promocaoBO As New PromocaoBO
                Dim novaPromocao = promocaoBO.Clonar(CODPMC)
                Success("Promoção clonada com sucesso!")

                Return Json(novaPromocao)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Reabrir uma promoção
        ''' </summary>
        ''' <param name="CODPMC"></param>
        ''' <returns></returns>
        <HttpPost>
        Function Abrir(ByVal CODPMC As Decimal) As ActionResult
            Try

                Dim mensagem As String = String.Empty

                Dim promocaoBO As New PromocaoBO
                Dim promocao = promocaoBO.ReabrirPromocao(CODPMC, mensagem)

                If Not mensagem = String.Empty Then
                    Warning("Promoção não encontrada")
                    Return Nothing
                End If

                Return Json(promocao)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Carrega lista de Publico alvo de uma promoção para exibir na confirmação de liberação da promoção
        ''' </summary>
        ''' <param name="CODPMC"></param>
        ''' <param name="TIPEDEPUBALVPMC"></param>
        Private Sub CarregaListasPublicoAlvo(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal)
            'Listas Publico Alvo
            'Regiao - Estado //mostrar nome na tela
            Dim PromocaoPublicoAlvoEstadoBO = New PromocaoPublicoAlvoEstadoBO()
            Dim PromocaoPublicoAlvoEstadoList As List(Of VO.PromocaoPublicoAlvoEstado) = PromocaoPublicoAlvoEstadoBO.ListarEstadoUniao(CODPMC, TIPEDEPUBALVPMC)
            Dim objPromocaoPublicoAlvoEstadoList As List(Of PromocaoPublicoAlvoEstadoViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoEstado), List(Of PromocaoPublicoAlvoEstadoViewModel))(PromocaoPublicoAlvoEstadoList)

            Dim promocaoPublicoAlvoEstadoNomes = New List(Of String)
            objPromocaoPublicoAlvoEstadoList.ForEach(Sub(obj)
                                                         If obj.CODPMC IsNot Nothing AndAlso obj.TIPEDEPUBALVPMC IsNot Nothing Then
                                                             promocaoPublicoAlvoEstadoNomes.Add(obj.NOMESTUNI)
                                                         End If
                                                     End Sub)
            ViewBag.promocaoPublicoAlvoEstadoNomes = promocaoPublicoAlvoEstadoNomes


            'Clientes - Atividade //mostrar nome na tela
            Dim PromocaoPublicoAlvoAtividadeBO = New PromocaoPublicoAlvoAtividadeBO()
            Dim PromocaoPublicoAlvoAtividadeList As List(Of VO.PromocaoPublicoAlvoAtividade) = PromocaoPublicoAlvoAtividadeBO.ListarAtividade(CODPMC, TIPEDEPUBALVPMC)
            Dim objPromocaoPublicoAlvoAtividadeList As List(Of PromocaoPublicoAlvoAtividadeViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoAtividade), List(Of PromocaoPublicoAlvoAtividadeViewModel))(PromocaoPublicoAlvoAtividadeList)

            Dim promocaoPublicoAlvoAtividadeNomes = New List(Of String)
            objPromocaoPublicoAlvoAtividadeList.ForEach(Sub(obj)
                                                            If obj.CODPMC IsNot Nothing AndAlso obj.TIPEDEPUBALVPMC IsNot Nothing Then
                                                                promocaoPublicoAlvoAtividadeNomes.Add(obj.NOMATI)
                                                            End If
                                                        End Sub)
            ViewBag.promocaoPublicoAlvoAtividadeNomes = promocaoPublicoAlvoAtividadeNomes


            'Clientes - Grupamento de Clientes //mostrar nome na tela
            Dim PromocaoPublicoAlvoGrupamentoClienteBO = New PromocaoPublicoAlvoGrupamentoClienteBO()
            Dim PromocaoPublicoAlvoGrupamentoClienteList As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente) = PromocaoPublicoAlvoGrupamentoClienteBO.ListarGrupamentoCliente(CODPMC, TIPEDEPUBALVPMC)
            Dim objPromocaoPublicoAlvoGrupamentoClienteList As List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoGrupamentoCliente), List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel))(PromocaoPublicoAlvoGrupamentoClienteList)

            Dim promocaoPublicoAlvoGrupamentoClienteNomes = New List(Of String)
            objPromocaoPublicoAlvoGrupamentoClienteList.ForEach(Sub(obj)
                                                                    If obj.CODPMC IsNot Nothing AndAlso obj.TIPEDEPUBALVPMC IsNot Nothing Then
                                                                        promocaoPublicoAlvoGrupamentoClienteNomes.Add(obj.DESGRPCLI)
                                                                    End If
                                                                End Sub)
            ViewBag.promocaoPublicoAlvoGrupamentoClienteNomes = promocaoPublicoAlvoGrupamentoClienteNomes


            'Clientes - Negocio de Clientes //mostrar nome na tela
            Dim PromocaoPublicoAlvoNegocioClienteBO = New PromocaoPublicoAlvoNegocioClienteBO()
            Dim PromocaoPublicoAlvoNegocioClienteList As List(Of VO.PromocaoPublicoAlvoNegocioCliente) = PromocaoPublicoAlvoNegocioClienteBO.ListarNegocioCliente(CODPMC, TIPEDEPUBALVPMC)
            Dim objPromocaoPublicoAlvoNegocioClienteList As List(Of PromocaoPublicoAlvoNegocioClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoNegocioCliente), List(Of PromocaoPublicoAlvoNegocioClienteViewModel))(PromocaoPublicoAlvoNegocioClienteList)

            Dim promocaoPublicoAlvoNegocioClienteNomes = New List(Of String)
            objPromocaoPublicoAlvoNegocioClienteList.ForEach(Sub(obj)
                                                                 If obj.CODPMC IsNot Nothing AndAlso obj.TIPEDEPUBALVPMC IsNot Nothing Then
                                                                     promocaoPublicoAlvoNegocioClienteNomes.Add(obj.DESUNDESRNGC)
                                                                 End If
                                                             End Sub)
            ViewBag.promocaoPublicoAlvoNegocioClienteNomes = promocaoPublicoAlvoNegocioClienteNomes


            'Clientes - Segmento de Clientes //mostrar nome na tela
            Dim PromocaoPublicoAlvoSegmentoClienteBO As PromocaoPublicoAlvoSegmentoClienteBO = New PromocaoPublicoAlvoSegmentoClienteBO()
            Dim PromocaoPublicoAlvoSegmentoClienteList As List(Of VO.PromocaoPublicoAlvoSegmentoCliente) = PromocaoPublicoAlvoSegmentoClienteBO.ListaTipoSegmentoCliente(CODPMC, TIPEDEPUBALVPMC)
            Dim objPromocaoPublicoAlvoSegmentoClienteList As List(Of PromocaoPublicoAlvoSegmentoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoSegmentoCliente), List(Of PromocaoPublicoAlvoSegmentoClienteViewModel))(PromocaoPublicoAlvoSegmentoClienteList)

            Dim promocaoPublicoAlvoSegmentoClienteNomes = New List(Of String)
            objPromocaoPublicoAlvoSegmentoClienteList.ForEach(Sub(obj)
                                                                  If obj.CODPMC IsNot Nothing AndAlso obj.TIPEDEPUBALVPMC IsNot Nothing Then
                                                                      promocaoPublicoAlvoSegmentoClienteNomes.Add(obj.DESSGMNGCCLI)
                                                                  End If
                                                              End Sub)
            ViewBag.promocaoPublicoAlvoSegmentoClienteNomes = promocaoPublicoAlvoSegmentoClienteNomes


            'Clientes - Cliente // mostrar apenas quantidade 
            Dim PromocaoPublicoAlvoClienteBO = New PromocaoPublicoAlvoClienteBO()
            Dim objPublicoAlvoCliente As VO.PromocaoPublicoAlvoCliente = New VO.PromocaoPublicoAlvoCliente()
            objPublicoAlvoCliente.CODPMC = CODPMC
            objPublicoAlvoCliente.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

            Dim objPublicoAlvoClienteList As List(Of VO.PromocaoPublicoAlvoCliente) = PromocaoPublicoAlvoClienteBO.ListarPublicoAlvoCliente(objPublicoAlvoCliente)
            Dim publicoAlvoClienteList As List(Of PromocaoPublicoAlvoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoCliente), List(Of PromocaoPublicoAlvoClienteViewModel))(objPublicoAlvoClienteList)
            ViewBag.publicoAlvoClienteQuantidade = publicoAlvoClienteList.Count()


            'Força de vendas - RCA/Vendedor // mostrar apenas quantidade 
            Dim PromocaoPublicoAlvoRcaVendedorBO = New PromocaoPublicoAlvoRcaVendedorBO()
            Dim objPublicoAlvoRcaVendedor As VO.PromocaoPublicoAlvoRcaVendedor = New VO.PromocaoPublicoAlvoRcaVendedor()
            objPublicoAlvoRcaVendedor.CODPMC = CODPMC
            objPublicoAlvoRcaVendedor.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

            Dim objPublicoAlvoRcaVendedorList As List(Of VO.PromocaoPublicoAlvoRcaVendedor) = PromocaoPublicoAlvoRcaVendedorBO.ListarPublicoAlvoRcaVendedor(objPublicoAlvoRcaVendedor)
            Dim publicoAlvoRcaVendedorList As List(Of PromocaoPublicoAlvoRcaVendedorViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoRcaVendedor), List(Of PromocaoPublicoAlvoRcaVendedorViewModel))(objPublicoAlvoRcaVendedorList)
            ViewBag.publicoAlvoRcaVendedorQuantidade = publicoAlvoRcaVendedorList.Count()


            'Força de vendas - Gerente de Vendas //mostrar nome na tela
            Dim PromocaoPublicoAlvoGerenteVendasBO = New PromocaoPublicoAlvoGerenteVendasBO()
            Dim PromocaoPublicoAlvoGerenteVendasList As List(Of VO.PromocaoPublicoAlvoGerenteVendas) = PromocaoPublicoAlvoGerenteVendasBO.ListarGerenteVendas(CODPMC, TIPEDEPUBALVPMC)
            PromocaoPublicoAlvoGerenteVendasList.ForEach(Sub(obj) obj.NOMGER = obj.NOMGER.TrimEnd())
            Dim objPromocaoPublicoAlvoGerenteVendasList As List(Of PromocaoPublicoAlvoGerenteVendasViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoGerenteVendas), List(Of PromocaoPublicoAlvoGerenteVendasViewModel))(PromocaoPublicoAlvoGerenteVendasList)

            Dim promocaoPublicoAlvoGerenteVendasNomes = New List(Of String)
            objPromocaoPublicoAlvoGerenteVendasList.ForEach(Sub(obj)
                                                                If obj.CODPMC IsNot Nothing AndAlso obj.TIPEDEPUBALVPMC IsNot Nothing Then
                                                                    promocaoPublicoAlvoGerenteVendasNomes.Add(obj.NOMGER)
                                                                End If
                                                            End Sub)
            ViewBag.promocaoPublicoAlvoGerenteVendasNomes = promocaoPublicoAlvoGerenteVendasNomes


            'Força de vendas - Gerente Mercado // mostrar apenas quantidade 
            Dim PromocaoPublicoAlvoGerenteMercadoBO = New PromocaoPublicoAlvoGerenteMercadoBO()
            Dim objPublicoAlvoGerenteMercado As VO.PromocaoPublicoAlvoGerenteMercado = New VO.PromocaoPublicoAlvoGerenteMercado()
            objPublicoAlvoGerenteMercado.CODPMC = CODPMC
            objPublicoAlvoGerenteMercado.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
            Dim objPublicoAlvoGerenteMercadoList As List(Of VO.PromocaoPublicoAlvoGerenteMercado) = PromocaoPublicoAlvoGerenteMercadoBO.ListarPublicoAlvoGerenteMercado(objPublicoAlvoGerenteMercado)
            Dim publicoAlvoGerenteMercadoList As List(Of PromocaoPublicoAlvoGerenteMercadoViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoGerenteMercado), List(Of PromocaoPublicoAlvoGerenteMercadoViewModel))(objPublicoAlvoGerenteMercadoList)
            ViewBag.publicoAlvoGerenteMercadoQuantidade = publicoAlvoGerenteMercadoList.Count()


            'Nacional
            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecaoList = promocaoPublicoAlvoSelecaoBO.ListT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})

            If (promocaoPublicoAlvoSelecaoList.Count() = 0) Then
                ViewBag.isNacional = True
            Else
                ViewBag.isNacional = False
            End If



        End Sub

        ''' <summary>
        ''' Suspender uma promoção de cashback
        ''' </summary>
        ''' <param name="CODPMC"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function SuspenderPromocaoCashback(ByVal CODPMC As Decimal) As Boolean
            Try
                Dim promocaoBO As New PromocaoBO()
                Dim result = promocaoBO.SuspenderPromocaoCashback(CODPMC)

                If (result) Then
                    Success("Promoção suspensa com sucesso!")
                End If

                Return result
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Liberar uma promoção de cashback suspensa
        ''' </summary>
        ''' <param name="CODPMC"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function RetirarSuspensao(ByVal CODPMC As Decimal) As Boolean
            Try
                Dim promocaoBO As New PromocaoBO()
                Dim result = promocaoBO.RetirarSuspensao(CODPMC)

                If (result) Then
                    Success("Suspensão retirada com sucesso!")
                End If

                Return result
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Consulta históricos de situação das promoções
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function PesquisarHistorico(ByVal CODPMC As Decimal) As PartialViewResult
            Try
                Dim promocaoHistoricoSituacaoBO As New PromocaoHistoricoSituacaoBO
                Dim listaHistoricoSituacao As List(Of VO.PromocaoHistoricoSituacao) = promocaoHistoricoSituacaoBO.ListHistorico(CODPMC)
                Dim listaHistoricoSituacaoViewModel As List(Of PromocaoHistoricoSituacaoViewModel) = Mapper.Map(Of List(Of VO.PromocaoHistoricoSituacao), List(Of PromocaoHistoricoSituacaoViewModel))(listaHistoricoSituacao)

                Dim gridAtualizado = PaginarEAtualizarGrid(listaHistoricoSituacaoViewModel, Nothing, Nothing)
                If gridAtualizado.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                Return PartialView("~/Views/Promocao/_HistoricoSituacao.vbhtml", listaHistoricoSituacaoViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function
    End Class

End Namespace
