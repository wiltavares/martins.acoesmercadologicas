﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="45")>
    Public Class AbastecimentoController
        Inherits AbstractController

        'BOs


        ''' <summary>
        ''' Carrega a tela de consulta
        ''' </summary>
        ''' <returns></returns>
        Function Index() As ViewResult
            CarregaListasDropDown()
            Dim grid = GridSettings(Of AbastecimentoViewModel).EmptyGrid()
            grid.Filter.CODFNCGRCPMC = ControleAcesso.CodFnc()

            Return View(grid)
        End Function

        ''' <summary>
        ''' Carrega listas de dropdown das telas de cadastro e pesquisa
        ''' </summary>
        Private Sub CarregaListasDropDown()
            Dim boFilialEmpresaCorporativo As New FilialEmpresaCorporativoBO()
            Dim boBU As New UnidadeEstrategicaDeNegocioBO()
            Dim listFilial As List(Of FilialEmpresaCorporativoViewModel) = Mapper.Map(Of List(Of VO.FilialEmpresaCorporativo), List(Of FilialEmpresaCorporativoViewModel))(boFilialEmpresaCorporativo.ListFiliais())
            ViewBag.listaFilial = listFilial

            Dim listaBU As List(Of UnidadeEstrategicaDeNegocioViewModel) = Mapper.Map(Of List(Of VO.UnidadeEstrategicaDeNegocio), List(Of UnidadeEstrategicaDeNegocioViewModel))(boBU.ListT(New VO.UnidadeEstrategicaDeNegocio()))
            ViewBag.listaBU = listaBU

            ViewBag.listaSituacao = New List(Of SelectListItem)()
            ViewBag.listaSituacao.Add(New SelectListItem() With {.Text = "À Liberar", .Value = "I"})
            ViewBag.listaSituacao.Add(New SelectListItem() With {.Text = "Encerrada", .Value = "E"})
            ViewBag.listaSituacao.Add(New SelectListItem() With {.Text = "Liberada", .Value = " "})

            ViewBag.listaStatusEnvio = New List(Of SelectListItem)()
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Selecione...", .Value = ""})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Enviado", .Value = " "})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "A Enviar", .Value = "S"})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Não Enviar", .Value = "N"})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Comércio Eletrônico", .Value = "I"})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Telemarketing", .Value = "T"})
            ViewBag.listaStatusEnvio.Add(New SelectListItem() With {.Text = "Presencial", .Value = "P"})


            ViewBag.listaFaixa = New List(Of SelectListItem)()
            ViewBag.listaFaixa.Add(New SelectListItem() With {.Text = "0", .Value = 0})


            Dim boAauthor As New FuncionarioBO()
            ViewBag.ListaFuncionarios = boAauthor.ListFuncionarios(New VO.Funcionario()).OrderBy(Function(m) m.CODFNC)

            ViewBag.ListaDropFornecedor = Constantes.Fornecedores

        End Sub

        <HttpPost()>
        Public Function Pesquisar(dadosPesquisa As GridSettings(Of AbastecimentoViewModel)) As PartialViewResult
            Try
                Dim BO As AbastecimentoBO = New AbastecimentoBO()
                Dim filtro As VO.Abastecimento = Mapper.Map(Of AbastecimentoViewModel, VO.Abastecimento)(dadosPesquisa.Filter)
                Dim list As List(Of VO.Abastecimento) = BO.Pesquisa(filtro)
                Dim listViewModel As List(Of AbastecimentoViewModel) = Mapper.Map(Of List(Of VO.Abastecimento), List(Of AbastecimentoViewModel))(list)

                If listViewModel.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                Return PartialView("~/Views/Abastecimento/_ResultadosView.vbhtml", listViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Public Function ModalAbastecerPromocao(ByVal CODPMC As Integer) As Object
            Try
                Dim BO As AbastecimentoBO = New AbastecimentoBO()
                Dim filtro As New VO.Abastecimento With {.CODPMC = CODPMC}
                Dim list As List(Of VO.Abastecimento) = BO.PesquisaAbastecimento(filtro)
                Dim listViewModel As List(Of AbastecimentoViewModel) = Mapper.Map(Of List(Of VO.Abastecimento), List(Of AbastecimentoViewModel))(list)

                If listViewModel.Count = 0 Then
                    Return "Não há brindes para fazer o abastecimento."
                End If

                Return PartialView("~/Views/Abastecimento/_ResultadosView.vbhtml", listViewModel)
            Catch ex As Exception
                Danger(ex)
                Return ex.Message
            End Try
        End Function

        '<HttpPost()>
        'Public Function InsertUnico(ByVal obj As AbastecimentoViewModel) As JsonResult
        '    Try
        '        Dim BO As AbastecimentoBO = New AbastecimentoBO()
        '        Dim vo As VO.Abastecimento = Mapper.Map(Of AbastecimentoViewModel, VO.Abastecimento)(obj)

        '        Dim voList As New List(Of VO.Abastecimento)

        '        voList.Add(vo)

        '        BO.AbastecerPromocao(voList)


        '        Dim list As List(Of VO.Abastecimento) = BO.Pesquisa(New VO.Abastecimento() With {.CODCPR = obj.CODCPR})
        '        Dim listViewModel As List(Of AbastecimentoViewModel) = Mapper.Map(Of List(Of VO.Abastecimento), List(Of AbastecimentoViewModel))(list)


        '        Return Json(listViewModel, JsonRequestBehavior.AllowGet)
        '    Catch ex As Exception
        '        Return Json(New With {Key .erro = ex.Message}, JsonRequestBehavior.AllowGet)
        '    End Try
        'End Function

        '<HttpPost()>
        'Public Function InsertVarios(ByVal obj As List(Of AbastecimentoViewModel)) As JsonResult
        '    Try
        '        Dim BO As AbastecimentoBO = New AbastecimentoBO()

        '        Dim voList As New List(Of VO.Abastecimento)

        '        For Each item In obj
        '            Dim vo As VO.Abastecimento = Mapper.Map(Of AbastecimentoViewModel, VO.Abastecimento)(item)
        '            voList.Add(vo)
        '        Next

        '        BO.AbastecerPromocao(voList)

        '        Dim list As List(Of VO.Abastecimento) = BO.Pesquisa(New VO.Abastecimento() With {.CODCPR = obj.First().CODCPR})
        '        Dim listViewModel As List(Of AbastecimentoViewModel) = Mapper.Map(Of List(Of VO.Abastecimento), List(Of AbastecimentoViewModel))(list)

        '        Return Json(listViewModel, JsonRequestBehavior.AllowGet)
        '    Catch ex As Exception
        '        Return Json(New With {Key .erro = ex.Message}, JsonRequestBehavior.AllowGet)
        '    End Try
        'End Function


        <HttpGet()>
        Function Historico(ByVal CODPMC As Decimal, ByVal CODMER As Decimal) As PartialViewResult
            Try
                Dim BO As AbastecimentoBO = New AbastecimentoBO()
                Dim list = BO.HistoricoPromocaoItem(CODPMC, CODMER)


                Dim listViewModel As List(Of AbastecimentoViewModel) = Mapper.Map(Of List(Of VO.Abastecimento), List(Of AbastecimentoViewModel))(list)
                Return PartialView("~/Views/Abastecimento/_Historico.vbhtml", listViewModel)

            Catch ex As Exception
                Danger(ex.Message)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Public Function Abastecer(ByVal brindes As List(Of AbastecimentoViewModel)) As JsonResult
            Try
                Dim BO As AbastecimentoBO = New AbastecimentoBO()

                Dim voList As List(Of VO.Abastecimento) = Mapper.Map(Of List(Of AbastecimentoViewModel), List(Of VO.Abastecimento))(brindes)

                BO.AbastecerPromocao(voList)

                Dim filtro As New VO.Abastecimento With {.CODPMC = brindes.First().CODPMC}
                Dim list As List(Of VO.Abastecimento) = BO.PesquisaAbastecimento(filtro)

                'Dim list As List(Of VO.Abastecimento) = BO.Pesquisa(New VO.Abastecimento() With {.CODPMC = brindes.First().CODPMC})
                Dim listViewModel As List(Of AbastecimentoViewModel) = Mapper.Map(Of List(Of VO.Abastecimento), List(Of AbastecimentoViewModel))(list)

                Return Json(listViewModel, JsonRequestBehavior.AllowGet)
            Catch ex As Exception
                Return Json(New With {Key .erro = ex.Message}, JsonRequestBehavior.AllowGet)
            End Try
        End Function
    End Class
End Namespace
