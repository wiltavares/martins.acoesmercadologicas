﻿Imports Microsoft.VisualBasic
Imports System.Web.Mvc
Imports AutoMapper
Imports System.Data
Imports System.IO

Namespace Controllers

    Public Class PromocaoBrindeController
        Inherits AbstractController


        ''' <summary>
        ''' Lista os brindes da promoção
        ''' </summary>
        ''' <param name="Id">Codigo da promoção</param>
        ''' <returns></returns>
        <HttpGet()>
        Function List(ByVal Id As Integer) As PartialViewResult
            Try
                Dim promocaoBO = New PromocaoBO()

                Dim promocao = promocaoBO.SelectT(New VO.Promocao With {
                    .CODPMC = Id
                })

                If (promocao Is Nothing) Then
                    Throw New Exception("Promoção não encontrada.")
                End If

                If (promocao.TIPSITPMC = " " Or promocao.TIPSITPMC = "" Or promocao.TIPSITPMC = "E") Then
                    ViewBag.EncerradaOuLiberada = True
                Else
                    ViewBag.EncerradaOuLiberada = False
                End If


                ViewBag.listaFaixa = False
                If (promocao.TIPPMC = 2) Then ' faixas
                    ViewBag.listaFaixa = True
                End If

                ViewBag.CodFuncionarioLogado = ControleAcesso.CodFnc()
                ViewBag.CodFuncionarioAutorPromocao = promocao.CODFNCGRCPMC

                If (promocao.INDCOBBDEPMC = 0) Then
                    Dim promocaoBrindeBO As New PromocaoBrindeBO
                    Dim listaPromocaoBrinde As List(Of VO.PromocaoBrinde) = promocaoBrindeBO.ListaParaGrid(New VO.PromocaoBrinde() With {.CODPMC = Id})
                    Dim listaPromocaoBrindeViewModel As List(Of PromocaoBrindeViewModel) = Mapper.Map(Of List(Of VO.PromocaoBrinde), List(Of PromocaoBrindeViewModel))(listaPromocaoBrinde)

                    ViewBag.isCob = False
                    Return PartialView("~/Views/Promocao/Brinde/_ResultadosView.vbhtml", listaPromocaoBrindeViewModel)
                Else

                    Dim promocaoBrindeBO As New PromocaoBrindeBO
                    Dim listaPromocaoBrinde As List(Of VO.PromocaoBrinde) = promocaoBrindeBO.ListaParaGrid(New VO.PromocaoBrinde() With {.CODPMC = Id})
                    Dim listaPromocaoBrindeViewModel As List(Of PromocaoBrindeViewModel) = Mapper.Map(Of List(Of VO.PromocaoBrinde), List(Of PromocaoBrindeViewModel))(listaPromocaoBrinde)


                    Dim promocaoMixBO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()
                    'T0161115
                    Dim lstPremioValor As List(Of VO.PromocaoBrinde) = promocaoBrindeBO.SelectRelacaoPromocaoEValorPremioSemMerAco(New VO.PromocaoBrinde() With {.CODPMC = promocao.CODPMC})

                    'T016115JOINT0100086
                    Dim lstPremioValorJoin As List(Of VO.PromocaoBrinde) = promocaoBrindeBO.SelectRelacaoPromocaoEValorPremioJoin(New VO.PromocaoBrinde() With {.CODPMC = promocao.CODPMC})

                    If (lstPremioValorJoin IsNot Nothing AndAlso lstPremioValorJoin.Count > 0) Then
                        Dim lstCodMer As List(Of Decimal) = (From c In lstPremioValorJoin Select (c.CODPRMPMC.GetValueOrDefault())).ToList()

                        Dim lstValorUnitarioVendaMer = promocaoMixBO.SelectValoresUnitariosBrutoDeVendaDaMercadoria(0, lstCodMer)
                        Dim lstValorUnitarioVendaMer2 = promocaoMixBO.SelectValoresUnitariosBrutoDeVendaDaMercadoria2(0, lstCodMer)

                        For Each itemValor As VO.PromocaoMixVisualizarVigente In lstValorUnitarioVendaMer2
                            If (lstValorUnitarioVendaMer.Where(Function(obj) obj.CODMER = itemValor.CODMER).ToList().Count = 0) Then
                                lstValorUnitarioVendaMer.Add(itemValor)
                            End If
                        Next


                        'lstValorUnitarioVendaMer -> VLRUNTPCOBRT pro preço medio venda

                        For Each item As VO.PromocaoBrinde In lstPremioValorJoin
                            listaPromocaoBrindeViewModel.Where(Function(obj) obj.CODPRMPMC = item.CODPRMPMC).ToList().ForEach(Sub(obj) obj.PERBFCBDE = item.PERBFCBDE)
                            listaPromocaoBrindeViewModel.Where(Function(obj) obj.CODPRMPMC = item.CODPRMPMC).ToList().ForEach(Sub(obj) obj.VLRUNTLIQMER = item.VLRUNTLIQMER)
                            listaPromocaoBrindeViewModel.Where(Function(obj) obj.CODPRMPMC = item.CODPRMPMC).ToList().ForEach(Sub(obj) obj.CUSTO = item.CUSTO)
                            listaPromocaoBrindeViewModel.Where(Function(obj) obj.CODPRMPMC = item.CODPRMPMC).ToList().ForEach(Sub(obj) obj.RATEAR = (item.INDCOBIPRMRI = 1))
                        Next

                        'Para pegar o primeiro registro por mercadoria 
                        Dim mercadoriasTratadas As List(Of Decimal) = New List(Of Decimal)

                        For Each item As VO.PromocaoMixVisualizarVigente In lstValorUnitarioVendaMer
                            If (mercadoriasTratadas.Where(Function(obj) obj = item.CODMER).ToList().Count = 0) Then
                                listaPromocaoBrindeViewModel.Where(Function(obj) obj.CODPRMPMC = item.CODMER).ToList().ForEach(Sub(obj) obj.VLRUNTPCOBRT = item.VLRUNTPCOBRT)
                                mercadoriasTratadas.Add(item.CODMER)
                            End If
                        Next

                    End If

                    ViewBag.isCob = True
                    Return PartialView("~/Views/Promocao/Brinde/_ResultadosView.vbhtml", listaPromocaoBrindeViewModel)

                End If



            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <returns></returns>
        <HttpGet()>
        Function VerificaProdutoVendidoFracionado(ByVal CodMer As Decimal, ByVal CodFilEmp As Decimal) As Boolean
            Try
                Dim mercadoriaBO As New MercadoriaBO
                Dim listMercadoriaVerifica As List(Of VO.Mercadoria) = mercadoriaBO.VerificaProdutoVendidoFracionado(CodMer, CodFilEmp)

                If (listMercadoriaVerifica.Count > 0) Then
                    Return True
                Else
                    Return False
                End If

            Catch ex As Exception
                Danger(ex)
                Return False
            End Try

        End Function

        ''' <returns></returns>
        <HttpGet()>
        Function ValoresBrinde(ByVal CodPmc As Decimal, ByVal CodMer As Decimal) As Object
            Try
                Dim promocaoBO = New PromocaoBO()

                Dim filtro As New VO.Promocao()
                filtro.CODPMC = CodPmc
                Dim promocao = promocaoBO.SelectT(filtro)

                If (promocao Is Nothing) Then
                    Throw New Exception("Promoção não encontrada.")
                End If

                Dim lstMer As List(Of Decimal) = New List(Of Decimal)
                lstMer.Add(CodMer)

                Dim mercadoriaBO As New MercadoriaBO
                Dim promocaoMixBO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()


                Dim lstCusto = promocaoMixBO.SelectValoresUnitariosBrutoDeVendaDaMercadoriaCusto(promocao.CODACOCMC, lstMer)



                Dim lstValorUnitarioVendaMer = promocaoMixBO.SelectValoresUnitariosBrutoDeVendaDaMercadoria(0, lstMer)
                Dim lstValorUnitarioVendaMer2 = promocaoMixBO.SelectValoresUnitariosBrutoDeVendaDaMercadoria2(0, lstMer)

                For Each itemValor As VO.PromocaoMixVisualizarVigente In lstValorUnitarioVendaMer2
                    If (lstValorUnitarioVendaMer.Where(Function(obj) obj.CODMER = itemValor.CODMER).ToList().Count = 0) Then
                        lstValorUnitarioVendaMer.Add(itemValor)
                    End If
                Next

                If (lstCusto.Count > 0) Then
                    If (lstValorUnitarioVendaMer.Count > 0) Then
                        'VALOR MEDIO
                        Return Json(New With {Key .Custo = lstCusto(0).VLRUNTPCOBRT, .ValorMedio = lstValorUnitarioVendaMer(0).VLRUNTPCOBRT}, JsonRequestBehavior.AllowGet)
                    Else
                        Return Json(New With {Key .Custo = -1}, JsonRequestBehavior.AllowGet)
                    End If
                Else
                    Return Json(New With {Key .Custo = -1}, JsonRequestBehavior.AllowGet)
                End If

            Catch ex As Exception
                Danger(ex)
                Return Json(New With {Key .Custo = -1}, JsonRequestBehavior.AllowGet)
            End Try

        End Function

        <HttpPost()>
        Public Function Create(ByVal obj As PromocaoBrindeViewModel) As PartialViewResult
            Try
                Dim promocaoBrindeBO As New PromocaoBrindeBO
                Dim promocaoBrinde As VO.PromocaoBrinde = Mapper.Map(Of PromocaoBrindeViewModel, VO.PromocaoBrinde)(obj)

                If (promocaoBrinde.TIPEDENIVPRMPMC Is Nothing) Then
                    promocaoBrinde.TIPEDENIVPRMPMC = 0
                End If

                PreencheListaFaixa(obj.CODPMC)

                Dim promocaoBO = New PromocaoBO()

                Dim filtro As New VO.Promocao()
                filtro.CODPMC = obj.CODPMC
                Dim promocao = promocaoBO.SelectT(filtro)

                If (promocao Is Nothing) Then
                    Throw New Exception("Promoção não encontrada.")
                End If

                If (promocao.INDCOBBDEPMC = 0) Then
                    ViewBag.isCob = False
                Else
                    ViewBag.isCob = True
                End If

                'Validar
                Dim resultValidacao = promocaoBrindeBO.InsertBrinde(promocaoBrinde)
                If (resultValidacao <> Nothing) Then
                    Warning(resultValidacao)
                    Return PartialView("~/Views/Promocao/Brinde/Create.vbhtml", New PromocaoBrindeViewModel() With {.CODPMC = obj.CODPMC})
                End If

                Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
                Return PartialView("~/Views/Promocao/Brinde/Create.vbhtml", New PromocaoBrindeViewModel() With {.CODPMC = obj.CODPMC})
            Catch ex As Exception
                Danger(ex)
                Return PartialView("~/Views/Promocao/Brinde/Create.vbhtml", New PromocaoBrindeViewModel() With {.CODPMC = obj.CODPMC})
            End Try
        End Function

        Private Sub PreencheListaFaixa(CODPMC As Decimal)

            ViewBag.listaFaixa = New List(Of SelectListItem)()

            Dim RelacaoPromocaoCriterioApuracaoFaixaFinitaBO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaBO()
            Dim ListRelacaoPromocaoCriterioApuracaoFaixaFinita = RelacaoPromocaoCriterioApuracaoFaixaFinitaBO.BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {.CODPMC = CODPMC})
            If (ListRelacaoPromocaoCriterioApuracaoFaixaFinita.Count > 0) Then
                'ViewBag.listaFaixa.Add(New SelectListItem() With {.Text = "0", .Value = 0})
                Dim cont As Integer = 1
                For Each item In ListRelacaoPromocaoCriterioApuracaoFaixaFinita
                    ViewBag.listaFaixa.Add(New SelectListItem() With {.Text = cont.ToString(), .Value = cont})
                    cont = cont + 1
                Next
            End If
        End Sub

        ''' <summary>
        ''' Delete brinde
        ''' </summary>
        ''' <param name="obj"></param>
        <HttpPost()>
        Public Function Delete(ByVal obj As PromocaoBrindeViewModel) As Object
            Try
                Dim promocaoBO = New PromocaoBO()

                Dim filtro As New VO.Promocao()
                filtro.CODPMC = obj.CODPMC
                Dim promocao = promocaoBO.SelectT(filtro)

                If (promocao Is Nothing) Then
                    Danger("Promoção não encontrada.")
                    Return List(obj.CODPMC)
                End If

                'Valida promocao liberada
                If (promocao.TIPSITPMC = " " Or promocao.TIPSITPMC = "") Then
                    Danger("Promoção liberada, não é permitido excluir o prêmio.")
                    Return List(obj.CODPMC)
                End If

                If (promocao.TIPSITPMC = "E") Then
                    Danger("Promoção Encerrada!")
                    Return List(obj.CODPMC)
                End If


                Dim promocaoBrindeDelete As VO.PromocaoBrinde = New VO.PromocaoBrinde() With {
                    .CODPMC = obj.CODPMC,
                    .CODPRMPMC = obj.CODPRMPMC
                }

                Dim daoBasePromocaoBrinde = New PromocaoBrindeDAO()

                Dim brinde = daoBasePromocaoBrinde.SelectT(promocaoBrindeDelete)

                If (brinde Is Nothing) Then
                    Danger("Brinde não encontrado.")
                    Return List(obj.CODPMC)
                End If

                brinde.CODACOCMC = promocao.CODACOCMC

                Dim promocaoBrindeBO As New PromocaoBrindeBO
                Dim resultValidacao = promocaoBrindeBO.RemoveBrinde(brinde)
                If (resultValidacao <> "Sucesso") Then
                    Warning(resultValidacao)
                    Return New HttpStatusCodeResult(Net.HttpStatusCode.BadRequest)

                End If


                Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
                Return List(obj.CODPMC)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        <HttpPost()>
        Public Function RemoverBrindePromocaoLiberada(ByVal obj As PromocaoBrindeViewModel) As Object
            Try
                Dim promocaoBO = New PromocaoBO()
                Dim promocao = promocaoBO.SelectT(New VO.Promocao With {
                    .CODPMC = obj.CODPMC
                })

                If (promocao Is Nothing) Then
                    Danger("Promoção não encontrada.")
                    Return List(obj.CODPMC)
                End If

                If (promocao.TIPSITPMC = "E") Then
                    Danger("Promoção Encerrada!")
                    Return List(obj.CODPMC)
                End If


                'Valida promocao liberada
                If (promocao.TIPSITPMC = " " Or promocao.TIPSITPMC = "") Then
                    Dim promocaoBrindeBO As New PromocaoBrindeBO

                    Dim brinde = promocaoBrindeBO.SelectT(New VO.PromocaoBrinde() With {
                        .CODPMC = obj.CODPMC,
                        .CODPRMPMC = obj.CODPRMPMC
                    })

                    If (brinde Is Nothing) Then
                        Danger("Brinde não encontrado.")
                        Return List(obj.CODPMC)
                    End If

                    brinde.CODACOCMC = promocao.CODACOCMC

                    Dim result = promocaoBrindeBO.RemoverBrindePromocaoLiberada(brinde.TIPEDEPUBALVPMC, brinde.CODPMC, brinde.TIPEDENIVPRMPMC, brinde.CODPRMPMC, DateTime.Now, ControleAcesso.CodFnc())
                    If (result <> "Sucesso") Then
                        Warning(result)
                        Return New HttpStatusCodeResult(Net.HttpStatusCode.BadRequest)

                    End If
                    Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
                End If
                Return List(obj.CODPMC)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Public Function RetornaBrindePromocaoLiberada(ByVal obj As PromocaoBrindeViewModel) As Object
            Try
                Dim promocaoBO = New PromocaoBO()
                Dim promocao = promocaoBO.SelectT(New VO.Promocao With {
                    .CODPMC = obj.CODPMC
                })

                If (promocao Is Nothing) Then
                    Danger("Promoção não encontrada.")
                    Return List(obj.CODPMC)
                End If

                If (promocao.TIPSITPMC = "E") Then
                    Danger("Promoção Encerrada!")
                    Return List(obj.CODPMC)
                End If


                'Valida promocao liberada
                If (promocao.TIPSITPMC = " " Or promocao.TIPSITPMC = "") Then
                    Dim promocaoBrindeBO As New PromocaoBrindeBO

                    Dim brinde = promocaoBrindeBO.SelectT(New VO.PromocaoBrinde() With {
                        .CODPMC = obj.CODPMC,
                        .CODPRMPMC = obj.CODPRMPMC
                    })

                    If (brinde Is Nothing) Then
                        Danger("Brinde não encontrado.")
                        Return List(obj.CODPMC)
                    End If

                    brinde.CODACOCMC = promocao.CODACOCMC

                    Dim result = promocaoBrindeBO.RetornaBrindePromocaoLiberada(brinde.TIPEDEPUBALVPMC, brinde.CODPMC, brinde.TIPEDENIVPRMPMC, brinde.CODPRMPMC, DateTime.Now, ControleAcesso.CodFnc())
                    If (result <> "Sucesso") Then
                        Warning(result)
                        Return New HttpStatusCodeResult(Net.HttpStatusCode.BadRequest)

                    End If
                    Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
                End If
                Return List(obj.CODPMC)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function Historico(ByVal obj As PromocaoBrindeViewModel) As PartialViewResult
            Try
                Dim BO As PromocaoBrindeHistoricoBO = New PromocaoBrindeHistoricoBO()
                Dim list = BO.ListHistorico(4, obj.CODPRMPMC, obj.CODPMC, obj.TIPEDENIVPRMPMC)


                Dim listViewModel As List(Of PromocaoBrindeHistoricoViewModel) = Mapper.Map(Of List(Of VO.PromocaoBrindeHistorico), List(Of PromocaoBrindeHistoricoViewModel))(list)
                Return PartialView("~/Views/Promocao/Brinde/_Historico.vbhtml", listViewModel)

            Catch ex As Exception
                Danger(ex.Message)
                Return Nothing
            End Try
        End Function


        Public Sub CarregarDrop(ByVal CodPmc As Decimal)
            Dim promocaoBrindeBO As New PromocaoBrindeBO
            Dim RelacaoCriterioBO As New RelacaoCriterioPromocaoPromocaoTipoEntidadeBO
            Dim promocaoBrinde As VO.PromocaoBrinde = promocaoBrindeBO.SelectT(New VO.PromocaoBrinde() With {.CODPMC = CodPmc})
            Dim RelacaoCriterio As VO.RelacaoCriterioPromocaoPromocaoTipoEntidade = RelacaoCriterioBO.SelectT(New VO.RelacaoCriterioPromocaoPromocaoTipoEntidade() With {.CODPMC = CodPmc, .TIPEDEPUBALVPMC = promocaoBrinde.TIPEDEPUBALVPMC})

        End Sub

        <HttpPost()>
        Public Function AbrirModalImport() As PartialViewResult
            Return PartialView("~/Views/Promocao/Brinde/_ModalImport.vbhtml")
        End Function

        <HttpPost()>
        Public Function ImportarXlsBrinde(ByVal CODPMC As Decimal) As PartialViewResult
            Dim erros As List(Of String) = New List(Of String)
            Dim promocaoBrindeBO As New PromocaoBrindeBO()
            Try
                If (Request.Files.Count <= 0) Then
                    erros.Add("Selecione um arquivo para importar!")
                    'Warning("Selecione um arquivo para importar!")
                    'Return "FalseBrinde"
                End If

                Dim savedFileName As String = ""
                If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                    erros.Add("Não foi possivel abrir o arquivo!")
                    'Warning("Não foi possivel abrir o arquivo!")
                    'Return "FalseBrinde"
                End If
                For Each item As String In Request.Files

                    Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                    If (hpf.ContentLength = 0) Then
                        Continue For
                    End If

                    Dim dataDoArquivo = Date.Now
                    Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                    Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                    Dim index As Integer = nomeArquivo.IndexOf(".xls")
                    nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                    savedFileName = Path.Combine(caminho, nomeArquivo)
                    hpf.SaveAs(savedFileName)

                    erros = promocaoBrindeBO.ImportarExelBrinde(savedFileName, CODPMC)
                Next
                Return PartialView("~/Views/Promocao/Brinde/_ModalErrosImportacao.vbhtml", erros)
            Catch ex As Exception
                If ex.Message.Contains("Não foi possível localizar uma parte do caminho") Then
                    erros.Add("Não foi possível salvar o arquivo no servidor pois não foi possível localizar uma parte do caminho, favor entrar em contato com o administrador do sistema.")
                    Return PartialView("~/Views/PlanoDeMarketing/_ModalErrosImportacao.vbhtml", erros)
                End If
                Danger(ex)
            End Try
            Return Nothing
        End Function

        <HttpPost()>
        Public Function CarregarWidgetBrindes(ByVal obj As PromocaoBrindeViewModel) As PartialViewResult
            Return PartialView("~/Views/Promocao/Brinde/Create.vbhtml", New PromocaoBrindeViewModel() With {.CODPMC = obj.CODPMC})
        End Function

    End Class
End Namespace

