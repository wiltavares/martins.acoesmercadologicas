﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class PromocaoClientesExcludentesController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoClientesExcludentes(ByVal objeto As PromocaoClientesExcludentesViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim obj As PromocaoClientesExcludentesViewModel = New PromocaoClientesExcludentesViewModel()

                Return PartialView("~/Views/Promocao/ClientesExcludentes/_ModalPromocaoClientesExcludentes.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function EditMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes(ByVal CODPMC As Decimal) As PartialViewResult
            Try

                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of PromocaoClientesExcludentesViewModel)
                gridConf.Filter = New PromocaoClientesExcludentesViewModel()
                gridConf.Filter.CODPMC = CODPMC

                gridConf.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes"
                gridConf.ResultID = "divGridItensPesquisaPromocaoClientesExcludentes"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMCLIC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of PromocaoClientesExcludentesViewModel), gridConf.Page)

                Return PartialView("~/Views/Promocao/ClientesExcludentes/_WidgetLoadPesquisaPromocaoClientesExcludentes.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes(dadosPesquisa As GridSettings(Of PromocaoClientesExcludentesViewModel)) As PartialViewResult
            Try
                Dim BO = New PromocaoClientesExcludentesBO()

                Dim filtro As VO.PromocaoClientesExcludentes = Mapper.Map(Of PromocaoClientesExcludentesViewModel, VO.PromocaoClientesExcludentes)(dadosPesquisa.Filter)

                If Not ValidaFiltroCliente(dadosPesquisa.Filter) Then
                    Return Nothing
                End If

                Dim voList As List(Of VO.PromocaoClientesExcludentes) = BO.BuscarListaCliente(filtro)
                Dim viewModeList As List(Of PromocaoClientesExcludentesViewModel) = Mapper.Map(Of List(Of VO.PromocaoClientesExcludentes), List(Of PromocaoClientesExcludentesViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoClientesExcludentesViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "divGridItensPesquisaPromocaoClientesExcludentes"
                dadosPesquisa.PaginationID = "divGridItensPesquisaPromocaoClientesExcludentes"
                dadosPesquisa.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes"
                dadosPesquisa.ControllerName = "PromocaoClientesExcludentes"

                Return PartialView("~/Views/Promocao/ClientesExcludentes/_GridPesquisaPromocaoClientesExcludentes.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        Private Function ValidaFiltroCliente(ByRef filtro As PromocaoClientesExcludentesViewModel) As Boolean

            If filtro.CODCLI Is Nothing And filtro.NOMCLI Is Nothing Then
                Warning("Informe pelo menos um filtro")
                Return False
            End If

            If Not filtro.NOMCLI Is Nothing Then
                If (filtro.NOMCLI.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do cliente")
                    Return False
                End If
            End If

            Return True
        End Function


        <HttpPost()>
        Function ListarClientesExcludentes(obj As PromocaoClientesExcludentesViewModel) As PartialViewResult
            Try

                'Validar Se Dados Pode Ser Alterado
                ViewBag.acao = "Alterar"
                Dim promocaoBO As New PromocaoBO
                Dim promocao = promocaoBO.SelectT(New VO.Promocao() With {.CODPMC = obj.CODPMC})
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)
                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim BO = New PromocaoClientesExcludentesBO()

                Dim objClientesExcludentes As VO.PromocaoClientesExcludentes = Mapper.Map(Of PromocaoClientesExcludentesViewModel, VO.PromocaoClientesExcludentes)(obj)
                'Dim lista = BO.ListT(objPublicoAlvoCliente)


                Dim objClientesExcludentesList As List(Of VO.PromocaoClientesExcludentes) = BO.ListarClienteExcludentes(objClientesExcludentes)

                Dim ClientesExcludentesList As List(Of PromocaoClientesExcludentesViewModel) = Mapper.Map(Of List(Of VO.PromocaoClientesExcludentes), List(Of PromocaoClientesExcludentesViewModel))(objClientesExcludentesList)



                'objeto da pesquisa.
                Dim gridConf = New GridSettings(Of PromocaoClientesExcludentesViewModel)
                gridConf.Filter = obj
                gridConf.ActionName = "PesquisaClientesExcludentes"
                gridConf.ResultID = "divGridPromocaoClientesExcludentes"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMCLIC"
                gridConf.Page.PageSize = 10


                gridConf.ListPaged = PaginarLista(ClientesExcludentesList, gridConf.Page)
                gridConf.TotalCount = ClientesExcludentesList.Count


                Return PartialView("~/Views/Promocao/ClientesExcludentes/_WidgetPromocaoClientesExcludentes.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaClientesExcludentes(dadosPesquisa As GridSettings(Of PromocaoClientesExcludentesViewModel)) As PartialViewResult
            Try


                'Validar Se Dados Pode Ser Alterado
                ViewBag.acao = "Alterar"
                Dim promocaoBO As New PromocaoBO
                Dim promocao = promocaoBO.SelectT(New VO.Promocao() With {.CODPMC = dadosPesquisa.Filter.CODPMC})
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)
                ControlaVisibilidadeComponentes(promocaoViewModel)


                Dim BO = New PromocaoClientesExcludentesBO()
                Dim filtro As VO.PromocaoClientesExcludentes = Mapper.Map(Of PromocaoClientesExcludentesViewModel, VO.PromocaoClientesExcludentes)(dadosPesquisa.Filter)
                Dim voList As List(Of VO.PromocaoClientesExcludentes) = BO.ListarClienteExcludentes(filtro)
                Dim viewModeList As List(Of PromocaoClientesExcludentesViewModel) = Mapper.Map(Of List(Of VO.PromocaoClientesExcludentes), List(Of PromocaoClientesExcludentesViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoClientesExcludentesViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.PaginationID = "divGridPromocaoClientesExcludentes"
                dadosPesquisa.ActionName = "PesquisaClientesExcludentes"
                dadosPesquisa.ResultID = "divGridPromocaoClientesExcludentes"
                dadosPesquisa.ControllerName = "PromocaoClientesExcludentes"

                Return PartialView("~/Views/Promocao/ClientesExcludentes/_GridPromocaoClientesExcludentes.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal obj As PromocaoClientesExcludentesViewModel) As PartialViewResult
            Dim PromocaoClientesExcludentesBO = New PromocaoClientesExcludentesBO()

            If (ModelState.IsValid) Then
                Dim PromocaoClientesExcludentes = Mapper.Map(Of PromocaoClientesExcludentesViewModel, VO.PromocaoClientesExcludentes)(obj)
                'Promocao.DesMecanica = "AI                  "
                PromocaoClientesExcludentesBO.InsertT(PromocaoClientesExcludentes)
                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                Return Nothing
            End If

            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function

        'Adiciona o resultado da pesquisa.
        <HttpPost()>
        Function AddPesquisa(filtro As PromocaoClientesExcludentesViewModel) As PartialViewResult
            Try
                If Not ValidaFiltroCliente(filtro) Then
                    Return Nothing
                End If

                Dim filtroVO As VO.PromocaoClientesExcludentes = Mapper.Map(Of PromocaoClientesExcludentesViewModel, VO.PromocaoClientesExcludentes)(filtro)

                Dim BO As PromocaoClientesExcludentesBO = New PromocaoClientesExcludentesBO()
                BO.AddPesquisa(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function Delete(ByVal obj As PromocaoClientesExcludentesViewModel) As PartialViewResult
            Dim PromocaoClientesExcludentesBO = New PromocaoClientesExcludentesBO()

            If (ModelState.IsValid) Then
                Dim PromocaoClientesExcludentes = Mapper.Map(Of PromocaoClientesExcludentesViewModel, VO.PromocaoClientesExcludentes)(obj)
                'Promocao.DesMecanica = "AI                  "
                PromocaoClientesExcludentesBO.DeleteT(PromocaoClientesExcludentes)
                Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
                Return Nothing
            End If

            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function


        <HttpPost()>
        Public Function ImportarXlsClientesExcludentes(ByVal CODPMC As Decimal) As String
            Dim promocaoClientesExcludentesBO As New PromocaoClientesExcludentesBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseCliente"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseCliente"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = promocaoClientesExcludentesBO.ImportarExelCliente(savedFileName, CODPMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseCliente"
            Else
                Return "TrueCliente"
            End If
        End Function

        '<HttpPost()>
        'Public Function ImportarXlsClientesExcludentes(ByVal CODPMC As Decimal) As String
        '    Try


        '        Dim promocaoClientesExcludentesBO As New PromocaoClientesExcludentesBO()

        '        If (Request.Files.Count <= 0) Then
        '            Warning("Selecione um arquivo para importar!")
        '            Return "FalseCliente"
        '        End If

        '        Dim savedFileName As String = ""
        '        If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
        '            Warning("Não foi possivel abrir o arquivo!")
        '            Return "FalseCliente"
        '        End If
        '        For Each item As String In Request.Files

        '            Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
        '            If (hpf.ContentLength = 0) Then
        '                Continue For
        '            End If

        '            Dim keybAuth As New Renci.SshNet.KeyboardInteractiveAuthenticationMethod(ConfigurationManager.AppSettings("usuarioDatastage"))
        '            AddHandler keybAuth.AuthenticationPrompt, AddressOf HandleKeyEvent

        '            Dim connectionInfo As New Renci.SshNet.ConnectionInfo(ConfigurationManager.AppSettings("urlDatastage"),
        '                                    ConfigurationManager.AppSettings("usuarioDatastage"),
        '                                    keybAuth) ',

        '            Dim filename As String = String.Format("promo_cli_{0}", Date.Now.ToString("yyyyMMddHHmmss"))
        '            Using client As New Renci.SshNet.SftpClient(connectionInfo)
        '                client.Connect()
        '                client.UploadFile(hpf.InputStream, String.Format("/geral/Sistemas/Arquivos_rest/promo_cli/waiting/{0}.xls", filename))


        '                Dim atributes = client.GetAttributes(String.Format("/geral/Sistemas/Arquivos_rest/promo_cli/waiting/{0}.xls", filename))

        '                atributes.OwnerCanExecute = True
        '                atributes.OwnerCanRead = True
        '                atributes.OwnerCanWrite = True
        '                atributes.OthersCanExecute = True
        '                atributes.OthersCanRead = True
        '                atributes.OthersCanWrite = True
        '                atributes.GroupCanExecute = True
        '                atributes.GroupCanRead = True
        '                atributes.GroupCanWrite = True

        '                client.SetAttributes(String.Format("/geral/Sistemas/Arquivos_rest/promo_cli/waiting/{0}.xls", filename), atributes)
        '            End Using

        '            Dim codfnc As String = ControleAcesso.CodFnc()
        '            Dim nomacsusrsis As String = System.Web.HttpContext.Current.User.Identity.Name

        '            Dim Url = String.Format("https://{0}:9446/wisd-rest2/ProcessaArquivo/Tabela/RLCPMCCLIECS?codpmc={1}&nomarqrcb={3}&nomacsusrsis={4}&codfnc={5}&datslc={6}%20{7}", ConfigurationManager.AppSettings("urlDatastage"), CODPMC, filename, nomacsusrsis, codfnc, Date.Now.ToString("yyyy-MM-dd"), Date.Now.ToString("HH:mm:ss"))


        '            Dim httpClient As New System.Net.Http.HttpClient()
        '            Dim responsex = httpClient.GetAsync(Url)
        '            responsex.Wait()


        '            Dim log As New LogDataStageBO()
        '            Dim logErros = log.ListaParaGrid(New VO.LogDataStage() With {.NOMARQRCB = filename, .FLGSTAPCS = 0})

        '            Dim strLog As New StringBuilder
        '            If (logErros.Count > 0) Then
        '                strLog.AppendLine("As seguintes linhas não foram importadas:")
        '                For Each linha In logErros
        '                    strLog.Append(linha.CODLNHARQDTS.Substring(0, linha.CODLNHARQDTS.IndexOf("#"))).Append(" Motivo: ").Append(linha.DESMTVSTA).AppendLine()
        '                Next
        '            End If

        '            If (responsex.Result.IsSuccessStatusCode) Then
        '                Dim Content = responsex.Result.Content.ReadAsStringAsync()
        '                Content.Wait()

        '                'Dim res = Content.Result
        '                If (logErros.Count = 0) Then
        '                    AddAlert(AlertStyle.Success, "Importação concluida com sucesso", Nothing, True, False)
        '                Else
        '                    AddAlert(AlertStyle.Success, "Importação concluida com sucesso porem alguns algumas linhas não foram importadas, verifique", Nothing, True, False)
        '                    AddAlert(AlertStyle.Warning, strLog.ToString().Replace(vbCrLf, "<br/>"), Nothing, True, False)
        '                End If

        '                Return "TrueCliente"
        '            Else
        '                Dim Content = responsex.Result.Content.ReadAsStringAsync()
        '                Content.Wait()

        '                AddAlert(AlertStyle.Danger, "Importação concluida com erro, verifique se o arquivo esta conforme o template", Nothing, True, False)

        '                If (logErros.Count > 0) Then
        '                    AddAlert(AlertStyle.Warning, strLog.ToString().Replace(vbCrLf, "<br/>"), Nothing, True, False)
        '                End If
        '                Return "FalseCliente"
        '            End If
        '        Next

        '        Return "FalseCliente"
        '    Catch ex As Exception
        '        Danger(ex)
        '        Return "FalseCliente"
        '    End Try
        'End Function
        Sub HandleKeyEvent(ByVal sender As Object, ByVal e As Renci.SshNet.Common.AuthenticationPromptEventArgs)
            For Each prompt As Renci.SshNet.Common.AuthenticationPrompt In e.Prompts
                If prompt.Request.IndexOf("Password:", StringComparison.InvariantCultureIgnoreCase) <> -1 Then
                    prompt.Response = ConfigurationManager.AppSettings("senhaDatastage")
                End If
            Next
        End Sub



        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            Dim PermiteEdicao As Boolean = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

            If (PermiteEdicao) Then
                Dim authCookie As HttpCookie = Request.RequestContext.HttpContext.Request.Cookies(FormsAuthentication.FormsCookieName)
                If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                    Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                    Dim roles As String() = Split(authTicket.UserData, "|")(1).Split(";"c)
                    If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
                        PermiteEdicao = True
                    Else
                        PermiteEdicao = False
                    End If
                End If
            End If

            ViewBag.SituacaoPromocao = obj.DESSITPMC
            ViewBag.PermiteEdicaoPromocaoClientesExcludentes = PermiteEdicao
        End Sub


    End Class

End Namespace
