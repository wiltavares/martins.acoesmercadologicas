﻿Imports Microsoft.VisualBasic
Imports System.Web.Mvc
Imports AutoMapper
Imports System.Data


Namespace Controllers

    Public Class CriterioApuracaoController
        Inherits AbstractController



        <HttpGet()>
        Function Edit(ByVal CODPMC As Decimal, ByVal TIPPMC As Decimal) As PartialViewResult
            Try
                'If (QDEFXAPMC Is Nothing) Then
                '    QDEFXAPMC = 0
                'End If
                'If (QDEGRPMIXPMC Is Nothing) Then
                '    QDEGRPMIXPMC = 0
                'End If
                'If (TIPPMC Is Nothing) Then
                '    TIPPMC = 0
                'End If

                CarregaDropCreateEditRelacaoAcaoMecanica("")

                Dim RelacaoPromocaoCriterioApuracaoFaixaFinitaBO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaBO()
                Dim RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO As New RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO()

                Dim parametrosCalculo As List(Of RelacaoPromocaoCriterioApuracaoParamCalculoViewModel)

                Dim ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva As New List(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva)


                Dim ListRelacaoPromocaoCriterioApuracaoFaixaFinita = RelacaoPromocaoCriterioApuracaoFaixaFinitaBO.BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {.CODPMC = CODPMC})
                If (ListRelacaoPromocaoCriterioApuracaoFaixaFinita.Count > 0) Then
                    parametrosCalculo = Mapper.Map(Of List(Of VO.RelacaoPromocaoCriterioApuracaoFaixaFinita), List(Of RelacaoPromocaoCriterioApuracaoParamCalculoViewModel))(ListRelacaoPromocaoCriterioApuracaoFaixaFinita)
                    parametrosCalculo = parametrosCalculo.OrderBy(Function(x) x.TIPEDENIVPRMPMC).ToList()
                Else
                    ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva = RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO.BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {.CODPMC = CODPMC})
                    parametrosCalculo = Mapper.Map(Of List(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva), List(Of RelacaoPromocaoCriterioApuracaoParamCalculoViewModel))(ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva)
                    parametrosCalculo = parametrosCalculo.OrderBy(Function(x) x.TIPEDENIVPRMPMC).ToList()
                End If



                'Dim ListRelacaoPromocaoCriterioApuracaoEscalaProgressivaViewModel As List(Of RelacaoPromocaoCriterioApuracaoEscalaProgressivaViewModel) = Mapper.Map(Of List(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva), List(Of RelacaoPromocaoCriterioApuracaoEscalaProgressivaViewModel))(ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva)

                Dim criterioApuracaoViewModel As New PromocaoCriterioApuracaoViewModel

                criterioApuracaoViewModel.CODPMC = CODPMC

                'RelacaoPromocaoCriterioApuracaoFaixaFinita   T0107960
                If (ListRelacaoPromocaoCriterioApuracaoFaixaFinita.Count > 0) Then
                    criterioApuracaoViewModel.QDEPTOFXACTRPMC = ListRelacaoPromocaoCriterioApuracaoFaixaFinita.First().QDEPTOFXACTRPMC
                    criterioApuracaoViewModel.INDTIPPTOPMC = ListRelacaoPromocaoCriterioApuracaoFaixaFinita.First().INDTIPPTOPMC
                    criterioApuracaoViewModel.VLRLIMMAXCTR = ListRelacaoPromocaoCriterioApuracaoFaixaFinita.First().VLRLIMMAXCTR

                    If (TIPPMC = 0) Then 'Normal
                        'If (QTDGRPFAIXA <= 0 AndAlso QDEGRPMIXPMC <= 0) Then 'Normal
                        criterioApuracaoViewModel.CodFormaCalculo = 2
                        criterioApuracaoViewModel.ParametrosCalculo = parametrosCalculo
                        Return PartialView("~/Views/Promocao/CriterioApuracao/_EditFaixaFinitaRegistros.vbhtml", criterioApuracaoViewModel)

                    ElseIf (TIPPMC = 2) Then 'Faixa
                        'ElseIf (QDEGRPMIXPMC <= 0 AndAlso QTDGRPFAIXA > 0) Then 'Faixa
                        criterioApuracaoViewModel.CodFormaCalculo = 2
                        criterioApuracaoViewModel.ParametrosCalculo = parametrosCalculo
                        criterioApuracaoViewModel.CODCTRPMC = parametrosCalculo.FirstOrDefault().CODCTRPMC
                        Return PartialView("~/Views/Promocao/CriterioApuracao/_EditFaixaFinitaFaixa.vbhtml", criterioApuracaoViewModel)

                    ElseIf (TIPPMC = 1) Then 'Combinada
                        'ElseIf (QDEGRPMIXPMC > 0 AndAlso QTDGRPFAIXA <= 0) Then 'Combinada
                        criterioApuracaoViewModel.CodFormaCalculo = 2
                        criterioApuracaoViewModel.ParametrosCalculo = parametrosCalculo
                        Return PartialView("~/Views/Promocao/CriterioApuracao/_EditFaixaFinitaRegistrosCombinada.vbhtml", criterioApuracaoViewModel)

                    End If
                ElseIf (ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva.Count > 0) Then 'RelacaoPromocaoCriterioApuracaoEscalaProgressiva  T0107951

                    criterioApuracaoViewModel.QDEPTOFXAEQCCALCTR = parametrosCalculo.First().QDEPTOFXAEQCCALCTR
                    criterioApuracaoViewModel.CodFormaCalculo = 1
                    criterioApuracaoViewModel.ParametrosCalculo = parametrosCalculo
                    criterioApuracaoViewModel.INDTIPPTOPMC = ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva.First().INDTIPPTOPMC
                    criterioApuracaoViewModel.VLRLIMMAXCTR = ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva.First().VLRLIMMAXCTR


                    If (TIPPMC = 2) Then 'Faixa
                        criterioApuracaoViewModel.CODCTRPMC = parametrosCalculo.First().CODCTRPMC
                        Return PartialView("~/Views/Promocao/CriterioApuracao/_EditEscalaProgressivaFaixa.vbhtml", criterioApuracaoViewModel)

                    ElseIf (TIPPMC = 1) Then 'Combinada 

                        Return PartialView("~/Views/Promocao/CriterioApuracao/_EditEscalaProgressivaCombinada.vbhtml", criterioApuracaoViewModel)

                    Else 'Normal
                        'criterioApuracaoViewModel.CODCTRPMC = ListRelacaoPromocaoCriterioApuracaoEscalaProgressivaViewModel.Item(0).CODCTRPMC
                        Return PartialView("~/Views/Promocao/CriterioApuracao/_EditEscalaProgressivaRegistros.vbhtml", criterioApuracaoViewModel)

                    End If

                Else

                    Dim promocaoBO = New PromocaoBO()
                    Dim promocao = PromocaoBO.SelectT(New VO.Promocao() With {.CODPMC = CODPMC})

                    If (promocao Is Nothing) Then
                        Throw New Exception("Promoção não encontrada.")
                    End If

                    criterioApuracaoViewModel.ParametrosCalculo = New List(Of RelacaoPromocaoCriterioApuracaoParamCalculoViewModel)

                    If (TIPPMC = 2) Then 'Faixa
                        If (promocao.QDEFXAPMC > 1) Then
                            For i As Integer = 1 To promocao.QDEFXAPMC
                                criterioApuracaoViewModel.ParametrosCalculo.Add(New RelacaoPromocaoCriterioApuracaoParamCalculoViewModel())
                            Next
                        End If

                        Return PartialView("~/Views/Promocao/CriterioApuracao/_EditEscalaProgressivaFaixa.vbhtml", criterioApuracaoViewModel)

                    ElseIf (TIPPMC = 1) Then 'Combinada 
                        If (promocao.QDEGRPMIXPMC > 1) Then
                            For i As Integer = 1 To promocao.QDEGRPMIXPMC
                                criterioApuracaoViewModel.ParametrosCalculo.Add(New RelacaoPromocaoCriterioApuracaoParamCalculoViewModel() With {.CODGRPMIXMERPMC = i, .DESCTRMIXPMC = ""})
                            Next
                        End If

                        Return PartialView("~/Views/Promocao/CriterioApuracao/_EditEscalaProgressivaCombinada.vbhtml", criterioApuracaoViewModel)

                    Else 'Normal
                        criterioApuracaoViewModel.ParametrosCalculo.Add(New RelacaoPromocaoCriterioApuracaoParamCalculoViewModel())

                        'criterioApuracaoViewModel.CODCTRPMC = ListRelacaoPromocaoCriterioApuracaoEscalaProgressivaViewModel.Item(0).CODCTRPMC
                        Return PartialView("~/Views/Promocao/CriterioApuracao/_EditEscalaProgressivaRegistros.vbhtml", criterioApuracaoViewModel)

                    End If
                End If


                Return Nothing

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function



        '<HttpPost()>
        'Public Function Alterar(ByVal criterioViewModel As PromocaoCriterioApuracaoViewModel) As Object
        '    Try

        '        If (criterioViewModel.TIPSITPMC = "E") Then
        '            'Warning("Promoção Encerrada!")
        '            Return "Promoção Encerrada!"
        '        End If

        '        Dim QDEPTOG As Decimal
        '        If (criterioViewModel.QDEPTOFXACTRPMC IsNot Nothing) Then
        '            QDEPTOG = criterioViewModel.QDEPTOFXACTRPMC
        '        Else
        '            QDEPTOG = criterioViewModel.QDEPTOFXAEQCCALCTR
        '        End If

        '        'VerificaQtdeMinima
        '        'VerificaQtdMinVndMaiorQtdPmc
        '        Dim promocaoBO = New PromocaoBO()
        '        Dim lstVerificaQtdMin = promocaoBO.VerificaQtdMinVndMaiorQtdPmc(New VO.Promocao With {.CODPMC = criterioViewModel.CODPMC})
        '        If (lstVerificaQtdMin.Count > 0) Then
        '            If (lstVerificaQtdMin(0).QDEMIN > QDEPTOG) Then
        '                Dim vm = Mapper.Map(Of List(Of VO.Promocao), List(Of PromocaoViewModel))(lstVerificaQtdMin)
        '                vm.ForEach(Sub(obj) obj.QDEPREMIOATINGIMENTO = QDEPTOG)

        '                Return PartialView("~/Views/Promocao/CriterioApuracao/_EditCriterioErroValidaQtd.vbhtml", vm)
        '            End If
        '        End If

        '        'VerificaQtdPmcNaoMultiploNaLiberacao
        '        Dim lstVerificaQtdPmcNaoMult = promocaoBO.VerificaQtdPmcNaoMult(New VO.Promocao With {.CODPMC = criterioViewModel.CODPMC})
        '        If (lstVerificaQtdPmcNaoMult.Count > 0) Then
        '            If ((QDEPTOG Mod lstVerificaQtdPmcNaoMult(0).QDEMIN) > 0) Then

        '                Dim vm = Mapper.Map(Of List(Of VO.Promocao), List(Of PromocaoViewModel))(lstVerificaQtdPmcNaoMult)
        '                vm.ForEach(Sub(obj) obj.QDEPREMIOATINGIMENTO = QDEPTOG)

        '                Return PartialView("~/Views/Promocao/CriterioApuracao/_EditCriterioErroValidaQtdPmcNaoMult.vbhtml", vm)
        '            End If
        '        End If

        '        Dim filtro As New VO.Promocao()
        '        filtro.CODPMC = criterioViewModel.CODPMC
        '        Dim promocao = promocaoBO.SelectT(filtro)

        '        'VerificaQtd
        '        If (promocao IsNot Nothing) Then
        '            Dim lstVerificaQtdMinVndMenorQtdPmcCriterio = promocaoBO.VerificaQtdMinVndMenorQtdPmcCriterio(New VO.Promocao With {.CODPMC = criterioViewModel.CODPMC})
        '            Dim lstVerificaQtdPmcNaoMultiploCriterio = promocaoBO.VerificaQtdPmcNaoMultiploCriterio(New VO.Promocao With {.CODPMC = criterioViewModel.CODPMC})


        '            If (lstVerificaQtdMinVndMenorQtdPmcCriterio.Count > 0) Then
        '                If (lstVerificaQtdMinVndMenorQtdPmcCriterio(0).QDEPMC * QDEPTOG < lstVerificaQtdMinVndMenorQtdPmcCriterio(0).QDEMIN) Then
        '                    Dim vm = Mapper.Map(Of List(Of VO.Promocao), List(Of PromocaoViewModel))(lstVerificaQtdMinVndMenorQtdPmcCriterio)
        '                    vm.ForEach(Sub(obj) obj.QDEPREMIOATINGIMENTO = QDEPTOG)

        '                    Return PartialView("~/Views/Promocao/CriterioApuracao/_EditCriterioErroValidaQtd2.vbhtml", vm)
        '                End If
        '            End If


        '            If (lstVerificaQtdPmcNaoMultiploCriterio.Count > 0) Then
        '                Dim vm = Mapper.Map(Of List(Of VO.Promocao), List(Of PromocaoViewModel))(lstVerificaQtdPmcNaoMultiploCriterio)
        '                vm.ForEach(Sub(obj) obj.QDEPREMIOATINGIMENTO = QDEPTOG)

        '                Return PartialView("~/Views/Promocao/CriterioApuracao/_EditCriterioErroValidaQtdPmcNaoMult2.vbhtml", vm)
        '            End If

        '            'Valida promocao liberada
        '            If (promocao.TIPSITPMC = " " Or promocao.TIPSITPMC = "") Then
        '                Return "Promoção já foi liberada"
        '            End If

        '            Dim criterioBO = New CriterioApuracaoBO()


        '            'AtualizaDataset
        '            criterioBO.InserOrUpdate(criterioViewModel)

        '        End If






        '        Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
        '        Return "Salvar"
        '    Catch ex As Exception
        '        Danger(ex)
        '        Return "Ocorreu um erro durante o processo. Erro: " + ex.Message
        '    End Try
        'End Function

        'Private Shared Sub AtualizaDataSet(criterioViewModel As PromocaoCriterioApuracaoViewModel, promocao As VO.Promocao)
        '    If (criterioViewModel.CodFormaCalculo = "2") Then 'FAIXA FINITAS

        '        'AtualizaEscalaProgressivaT0107951

        '        Dim RelacaoCriterioBO As New RelacaoCriterioPromocaoPromocaoTipoEntidadeBO
        '        Dim RelacaoPromocaoCriterioApuracaoFaixaFinitaBO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaBO
        '        Dim RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO As New RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO

        '        Dim ListRelacaoCriterio As List(Of VO.RelacaoCriterioPromocaoPromocaoTipoEntidade) = RelacaoCriterioBO.SelectRel(New VO.RelacaoCriterioPromocaoPromocaoTipoEntidade() With {.CODPMC = criterioViewModel.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC})

        '        Dim ListRelacaoPromocaoCriterioApuracaoFaixaFinita = RelacaoPromocaoCriterioApuracaoFaixaFinitaBO.BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {.CODPMC = criterioViewModel.CODPMC})
        '        Dim ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva = RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO.BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {.CODPMC = criterioViewModel.CODPMC})

        '        'Delete T0107960
        '        ListRelacaoPromocaoCriterioApuracaoFaixaFinita.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoFaixaFinitaBO.DeleteT(obj))

        '        'Delete T0107951
        '        ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO.DeleteT(obj))

        '        Dim parametrosCalculo As List(Of VO.RelacaoPromocaoCriterioApuracaoFaixaFinita)
        '        parametrosCalculo = Mapper.Map(Of List(Of RelacaoPromocaoCriterioApuracaoParamCalculoViewModel), List(Of VO.RelacaoPromocaoCriterioApuracaoFaixaFinita))(criterioViewModel.ParametrosCalculo)

        '        Dim QDEPTO As Decimal?
        '        Dim DefaultMaxVal As Decimal

        '        DefaultMaxVal = If(criterioViewModel.CODCTRPMC = 1, 9999999999.99, 9999999999)
        '        QDEPTO = criterioViewModel.QDEPTOFXACTRPMC

        '        If (criterioViewModel.Combinada) Then
        '            parametrosCalculo.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoFaixaFinitaBO.InsertT(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {
        '            .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC,
        '            .CODPMC = promocao.CODPMC,
        '            .CODCTRPMC = obj.CODCTRPMC,
        '            .VLRLIMMNMFXACTRPMC = obj.VLRLIMMNMFXACTRPMC,
        '            .DESCTRMIXPMC = obj.DESCTRMIXPMC,
        '            .VLRLIMMAXFXACTRPMC = DefaultMaxVal,
        '            .TIPEDENIVPRMPMC = 1,
        '            .CODGRPMIXMERPMC = obj.CODGRPMIXMERPMC,
        '            .QDEPTOFXACTRPMC = QDEPTO
        '            }))
        '        Else
        '            parametrosCalculo.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoFaixaFinitaBO.InsertT(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {
        '            .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC,
        '            .CODPMC = promocao.CODPMC,
        '            .CODCTRPMC = If(obj.CODCTRPMC IsNot Nothing, obj.CODCTRPMC, criterioViewModel.CODCTRPMC),
        '            .VLRLIMMNMFXACTRPMC = obj.VLRLIMMNMFXACTRPMC,
        '            .DESCTRMIXPMC = " ",
        '            .VLRLIMMAXFXACTRPMC = If(obj.VLRLIMMAXFXACTRPMC IsNot Nothing, obj.VLRLIMMAXFXACTRPMC, DefaultMaxVal),
        '            .TIPEDENIVPRMPMC = obj.TIPEDENIVPRMPMC,
        '            .CODGRPMIXMERPMC = 0,
        '            .QDEPTOFXACTRPMC = QDEPTO
        '            }))
        '        End If

        '        'AtualizaEscalaProgressivaT0107960





        '    ElseIf (criterioViewModel.CodFormaCalculo = "1") Then 'T0107960
        '        'AtualizaEscalaProgressivaT0107951

        '        Dim RelacaoCriterioBO As New RelacaoCriterioPromocaoPromocaoTipoEntidadeBO
        '        Dim RelacaoPromocaoCriterioApuracaoFaixaFinitaBO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaBO
        '        Dim RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO As New RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO

        '        Dim ListRelacaoCriterio As List(Of VO.RelacaoCriterioPromocaoPromocaoTipoEntidade) = RelacaoCriterioBO.SelectRel(New VO.RelacaoCriterioPromocaoPromocaoTipoEntidade() With {.CODPMC = criterioViewModel.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC})

        '        Dim ListRelacaoPromocaoCriterioApuracaoFaixaFinita = RelacaoPromocaoCriterioApuracaoFaixaFinitaBO.BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {.CODPMC = criterioViewModel.CODPMC})
        '        Dim ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva = RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO.BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {.CODPMC = criterioViewModel.CODPMC})

        '        'Delete T0107951
        '        ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO.DeleteT(obj))

        '        'Delete T0107960
        '        ListRelacaoPromocaoCriterioApuracaoFaixaFinita.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoFaixaFinitaBO.DeleteT(obj))

        '        Dim parametrosCalculo As List(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva)
        '        parametrosCalculo = Mapper.Map(Of List(Of RelacaoPromocaoCriterioApuracaoParamCalculoViewModel), List(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva))(criterioViewModel.ParametrosCalculo)

        '        Dim QDEPTO As Decimal?

        '        QDEPTO = criterioViewModel.QDEPTOFXAEQCCALCTR

        '        If (criterioViewModel.Combinada) Then
        '            parametrosCalculo.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO.InsertT(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {
        '            .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC,
        '            .CODPMC = promocao.CODPMC,
        '            .CODGRPMIXMERPMC = obj.CODGRPMIXMERPMC,
        '            .QDEPTOFXAEQCCALCTR = QDEPTO,
        '            .CODCTRPMC = obj.CODCTRPMC,
        '            .VLRINIFXAEQCCALCTR = 0,
        '            .TAMIVLFXAEQCCALCTR = obj.TAMIVLFXAEQCCALCTR,
        '            .DESCTRMIXPMC = obj.DESCTRMIXPMC
        '            }))
        '        Else
        '            parametrosCalculo.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO.InsertT(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {
        '            .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC,
        '            .CODPMC = promocao.CODPMC,
        '            .CODGRPMIXMERPMC = 0,
        '            .QDEPTOFXAEQCCALCTR = QDEPTO,
        '            .CODCTRPMC = If(criterioViewModel.CODCTRPMC IsNot Nothing, criterioViewModel.CODCTRPMC, obj.CODCTRPMC),
        '            .VLRINIFXAEQCCALCTR = 0,
        '            .TAMIVLFXAEQCCALCTR = obj.TAMIVLFXAEQCCALCTR,
        '            .DESCTRMIXPMC = " "
        '            }))
        '        End If

        '        'AtualizaEscalaProgressivaT0107960

        '    End If
        'End Sub

        'Function ValidarCriterioApuracao(ByVal SituacaoPromocao As String) As String

        '    If (SituacaoPromocao = "E") Then
        '        Return "Promoção Encerrada!"
        '    End If


        '    Return Nothing
        'End Function

        'Metodo Carrega Drop
        Sub CarregaDropCreateEditRelacaoAcaoMecanica(ByVal DESEQCCALCTRPMC As String)
            'Carrega Drop Tipo Promoção
            Dim formaDeCalculoBo As New FormaDeCalculoBO()
            Dim tipoCriterioBO As New TipoCriterioBO()
            Dim TipoPromoList As List(Of TipoPromocaoViewModel) = New List(Of TipoPromocaoViewModel)

            TipoPromoList.Add(New TipoPromocaoViewModel(0, "Normal"))
            TipoPromoList.Add(New TipoPromocaoViewModel(1, "Combinada"))
            TipoPromoList.Add(New TipoPromocaoViewModel(2, "Faixas Multiplas"))
            ViewBag.ListaTipoPromoca = TipoPromoList
            '------------------------------------------------------------------------------------------------------------
            'Carrega Drop Forma de Calculo
            Dim formaDeCalculoList As List(Of FormaDeCalculoViewModel) = New List(Of FormaDeCalculoViewModel)

            ViewBag.formaDeCalculo = formaDeCalculoBo.CarregaDropFormaDeCalculo(DESEQCCALCTRPMC)

            '------------------------------------------------------------------------------------------------------------
            'Carrega Drop Critério

            ViewBag.criterio = tipoCriterioBO.CarregaDropTipoCriterio("", 0, 4)

            '------------------------------------------------------------------------------------------------------------
            'Carrega Drop Critério
            Dim tipoCriterioList As List(Of RelacaoAcaoMecanicaViewModel) = New List(Of RelacaoAcaoMecanicaViewModel)

            ViewBag.tipoCriterioSimplesList = tipoCriterioList
        End Sub

    End Class
End Namespace

