﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net
Imports NPOI.SS.Formula.Functions

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    <RoutePrefix(“PromocaoMix”)>
    Public Class PromocaoMixVisualizarVigenteController
        Inherits AbstractController

        Function Index() As RedirectResult
            Return New RedirectResult("Home")
        End Function

        <HttpPost()>
        Public Function CarregarModalItensVisualizarMixVigente(objeto As PromocaoMixVisualizarVigenteViewModel) As PartialViewResult
            Try

                Dim dadosPesquisa As GridSettings(Of PromocaoMixVisualizarVigenteViewModel) = New GridSettings(Of PromocaoMixVisualizarVigenteViewModel)()
                dadosPesquisa.Page = New PageSettings()
                dadosPesquisa.Page.OrderBy = "DESMERC"
                dadosPesquisa.Page.PageSize = 50

                dadosPesquisa.Filter = New PromocaoMixVisualizarVigenteViewModel()
                dadosPesquisa.Filter.CODPMC = objeto.CODPMC
                dadosPesquisa.Filter.TIPEDEPUBALVPMC = objeto.TIPEDEPUBALVPMC

                Dim BO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()
                Dim List As List(Of VO.PromocaoMixVisualizarVigente) = IIf(objeto.TIPFRNACOCMC = 1 _
                                                                           , BO.PesquisaItensVisualizarMixVigenteEdl(objeto.CODPMC, objeto.TIPEDEPUBALVPMC) _
                                                                           , BO.PesquisaItensVisualizarMixVigente(objeto.CODPMC, objeto.TIPEDEPUBALVPMC))

                Dim viewModelList As List(Of PromocaoMixVisualizarVigenteViewModel) = Mapper.Map(Of List(Of VO.PromocaoMixVisualizarVigente), List(Of PromocaoMixVisualizarVigenteViewModel))(List)


                Dim PromocaoBO As New PromocaoBO()
                Dim promocao = PromocaoBO.SelectT(New VO.Promocao() With {.CODPMC = objeto.CODPMC, .TIPEDEPUBALVPMC = objeto.TIPEDEPUBALVPMC})
                ViewBag.TIPPMC = promocao.TIPPMC

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixVisualizarVigenteViewModel)(viewModelList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModelList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaVisualizarMixVigente"
                dadosPesquisa.PaginationID = "gridItensPesquisaVisualizarMixVigente"
                dadosPesquisa.ActionName = "PesquisaVisualizarMixVigente"
                dadosPesquisa.ControllerName = "PromocaoMixVisualizarVigente"

                dadosPesquisa.ActionName = "PesquisaVisualizarMixVigente"
                dadosPesquisa.ResultID = "gridItensPesquisaVisualizarMixVigente"


                Return PartialView("~/Views/Promocao/Mix/_ModalVisualizarMixVigente.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaVisualizarMixVigente(dadosPesquisa As GridSettings(Of PromocaoMixVisualizarVigenteViewModel)) As PartialViewResult
            Try
                Dim BO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()

                Dim List As List(Of VO.PromocaoMixVisualizarVigente) = BO.PesquisaItensVisualizarMixVigente(dadosPesquisa.Filter.CODPMC, dadosPesquisa.Filter.TIPEDEPUBALVPMC)
                Dim viewModelList As List(Of PromocaoMixVisualizarVigenteViewModel) = Mapper.Map(Of List(Of VO.PromocaoMixVisualizarVigente), List(Of PromocaoMixVisualizarVigenteViewModel))(List)

                Dim PromocaoBO As New PromocaoBO()
                Dim promocao = PromocaoBO.SelectT(New VO.Promocao() With {.CODPMC = dadosPesquisa.Filter.CODPMC, .TIPEDEPUBALVPMC = dadosPesquisa.Filter.TIPEDEPUBALVPMC})
                ViewBag.TIPPMC = promocao.TIPPMC

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixVisualizarVigenteViewModel)(viewModelList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModelList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaVisualizarMixVigente"
                dadosPesquisa.PaginationID = "gridItensPesquisaVisualizarMixVigente"
                dadosPesquisa.ActionName = "PesquisaVisualizarMixVigente"
                dadosPesquisa.ControllerName = "PromocaoMixVisualizarVigente"

                Return PartialView("~/Views/Promocao/Mix/_GridVisualizarMixVigente.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        'Pre Visualizar Mix Mecanica Acao

        <HttpPost()>
        Public Function CarregarModalPreVisualizarMixMecanicaAcao(objeto As PromocaoMixVisualizarVigenteViewModel) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                If (acaoCache.Fornecedores IsNot Nothing AndAlso acaoCache.Fornecedores.Any()) Then
                    objeto.TIPFRNACOCMC = acaoCache.Fornecedores.FirstOrDefault().TIPFRNACOCMC
                End If

                Dim relacaoAcaoMecanicaBO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
                Dim relacaoAcaoMecanica = relacaoAcaoMecanicaBO.SelectT(New VO.RelacaoAcaoMecanica() With {.CODACOCMC = objeto.CODACOCMC, .NUMSEQRGRACOCMC = objeto.NUMSEQRGRACOCMC})

                Dim BO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()
                Dim List As List(Of VO.PromocaoMixVisualizarVigente)
                If (objeto.TIPFRNACOCMC = 1) Then
                    List = BO.PreVisualizarMixMecanicaAcaoEdl(objeto.CODACOCMC, objeto.NUMSEQRGRACOCMC, relacaoAcaoMecanica.TIPPMC)
                Else
                    List = BO.PreVisualizarMixMecanicaAcao(objeto.CODACOCMC, objeto.NUMSEQRGRACOCMC, relacaoAcaoMecanica.TIPPMC)
                End If

                Dim viewModelList As List(Of PromocaoMixVisualizarVigenteViewModel) = Mapper.Map(Of List(Of VO.PromocaoMixVisualizarVigente), List(Of PromocaoMixVisualizarVigenteViewModel))(List)

                Dim dadosPesquisa As GridSettings(Of PromocaoMixVisualizarVigenteViewModel) = New GridSettings(Of PromocaoMixVisualizarVigenteViewModel)()
                dadosPesquisa.Filter = New PromocaoMixVisualizarVigenteViewModel()
                dadosPesquisa.Filter.CODACOCMC = objeto.CODACOCMC
                dadosPesquisa.Filter.NUMSEQRGRACOCMC = objeto.NUMSEQRGRACOCMC


                dadosPesquisa.Page = New PageSettings()
                dadosPesquisa.Page.OrderBy = "DESMERC"
                dadosPesquisa.Page.PageSize = 50

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixVisualizarVigenteViewModel)(viewModelList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModelList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaPreVisualizarMixMecanicaAcao"
                dadosPesquisa.PaginationID = "gridItensPesquisaPreVisualizarMixMecanicaAcao"
                dadosPesquisa.ActionName = "PesquisaPreVisualizarMixMecanicaAcao"
                dadosPesquisa.ControllerName = "PromocaoMixVisualizarVigente"


                Return PartialView("~/Views/Promocao/Mix/_ModalPreVisualizarMixMecanicaAcao.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaPreVisualizarMixMecanicaAcao(dadosPesquisa As GridSettings(Of PromocaoMixVisualizarVigenteViewModel)) As PartialViewResult
            Try

                Dim relacaoAcaoMecanicaBO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
                Dim relacaoAcaoMecanica = relacaoAcaoMecanicaBO.SelectT(New VO.RelacaoAcaoMecanica() With {.CODACOCMC = dadosPesquisa.Filter.CODACOCMC, .NUMSEQRGRACOCMC = dadosPesquisa.Filter.NUMSEQRGRACOCMC})


                Dim BO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()

                Dim List As List(Of VO.PromocaoMixVisualizarVigente) = BO.PreVisualizarMixMecanicaAcao(dadosPesquisa.Filter.CODACOCMC, dadosPesquisa.Filter.NUMSEQRGRACOCMC, relacaoAcaoMecanica.TIPPMC)
                Dim viewModelList As List(Of PromocaoMixVisualizarVigenteViewModel) = Mapper.Map(Of List(Of VO.PromocaoMixVisualizarVigente), List(Of PromocaoMixVisualizarVigenteViewModel))(List)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixVisualizarVigenteViewModel)(viewModelList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModelList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaPreVisualizarMixMecanicaAcao"
                dadosPesquisa.PaginationID = "gridItensPesquisaPreVisualizarMixMecanicaAcao"
                dadosPesquisa.ActionName = "PesquisaPreVisualizarMixMecanicaAcao"
                dadosPesquisa.ControllerName = "PromocaoMixVisualizarVigente"

                Return PartialView("~/Views/Promocao/Mix/_GridPreVisualizarMixMecanicaAcao.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        'Pre Visualizar Mix Promocao

        <HttpPost()>
        Public Function CarregarModalPreVisualizarMixPromocao(objeto As PromocaoMixVisualizarVigenteViewModel) As PartialViewResult
            Try
                Dim PromocaoBO As New PromocaoBO()
                Dim promocao = PromocaoBO.SelectT(New VO.Promocao() With {.CODPMC = objeto.CODPMC, .TIPEDEPUBALVPMC = objeto.TIPEDEPUBALVPMC})
                ViewBag.TIPPMC = promocao.TIPPMC
                Dim BO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()
                Dim List As List(Of VO.PromocaoMixVisualizarVigente) = IIf(objeto.TIPFRNACOCMC = 1 _
                                                                           , BO.PreVisualizarMixPromocaoEdl(objeto.CODPMC, promocao.TIPSITPMC) _
                                                                           , BO.PreVisualizarMixPromocao(objeto.CODPMC, objeto.TIPEDEPUBALVPMC, promocao.TIPPMC))

                Dim viewModelList As List(Of PromocaoMixVisualizarVigenteViewModel) = Mapper.Map(Of List(Of VO.PromocaoMixVisualizarVigente), List(Of PromocaoMixVisualizarVigenteViewModel))(List)

                Dim dadosPesquisa As GridSettings(Of PromocaoMixVisualizarVigenteViewModel) = New GridSettings(Of PromocaoMixVisualizarVigenteViewModel)()


                dadosPesquisa.Filter = New PromocaoMixVisualizarVigenteViewModel()
                dadosPesquisa.Filter.CODPMC = objeto.CODPMC
                dadosPesquisa.Filter.TIPEDEPUBALVPMC = objeto.TIPEDEPUBALVPMC

                dadosPesquisa.Page = New PageSettings()
                dadosPesquisa.Page.OrderBy = "DESMERC"
                dadosPesquisa.Page.PageSize = 50


                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixVisualizarVigenteViewModel)(viewModelList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModelList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaPreVisualizarMixPromocao"
                dadosPesquisa.PaginationID = "gridItensPesquisaPreVisualizarMixPromocao"
                dadosPesquisa.ActionName = "PesquisaPreVisualizarMixPromocao"
                dadosPesquisa.ControllerName = "PromocaoMixVisualizarVigente"


                Return PartialView("~/Views/Promocao/Mix/_ModalPreVisualizarMixPromocao.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaPreVisualizarMixPromocao(dadosPesquisa As GridSettings(Of PromocaoMixVisualizarVigenteViewModel)) As PartialViewResult
            Try
                Dim PromocaoBO As New PromocaoBO()
                Dim promocao = PromocaoBO.SelectT(New VO.Promocao() With {.CODPMC = dadosPesquisa.Filter.CODPMC, .TIPEDEPUBALVPMC = dadosPesquisa.Filter.TIPEDEPUBALVPMC})

                Dim BO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()
                Dim List As List(Of VO.PromocaoMixVisualizarVigente) = BO.PreVisualizarMixPromocao(dadosPesquisa.Filter.CODPMC, dadosPesquisa.Filter.TIPEDEPUBALVPMC, promocao.TIPPMC)
                Dim viewModelList As List(Of PromocaoMixVisualizarVigenteViewModel) = Mapper.Map(Of List(Of VO.PromocaoMixVisualizarVigente), List(Of PromocaoMixVisualizarVigenteViewModel))(List)

                ViewBag.TIPPMC = promocao.TIPPMC

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixVisualizarVigenteViewModel)(viewModelList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModelList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaPreVisualizarMixPromocao"
                dadosPesquisa.PaginationID = "gridItensPesquisaPreVisualizarMixPromocao"
                dadosPesquisa.ActionName = "PesquisaPreVisualizarMixPromocao"
                dadosPesquisa.ControllerName = "PromocaoMixVisualizarVigente"

                Return PartialView("~/Views/Promocao/Mix/_GridPreVisualizarMixPromocao.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Sub AtualizaMixPromocao(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal TIPFRNACOCMC As Decimal)
            Try
                Dim PromocaoBO As New PromocaoBO()
                Dim promocao = PromocaoBO.SelectT(New VO.Promocao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})



                Dim BO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()
                BO.AtualizaMixPromocao(CODPMC, TIPEDEPUBALVPMC, promocao.TIPPMC, promocao.TIPSITPMC, TIPFRNACOCMC)

                Success("Atualização realizada com sucesso")

            Catch ex As Exception
                Danger(ex)
            End Try
        End Sub





    End Class

End Namespace
