﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net
Imports NPOI.SS.Formula.Functions
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    <RoutePrefix("PromocaoParametrosMix")>
    Public Class PromocaoParametrosMixController
        Inherits AbstractController

        Function Index() As RedirectResult
            Return New RedirectResult("Home")
        End Function

        <HttpPost()>
        Public Function CarregarModalParametrosMixPromocao(objeto As PromocaoMixViewModel) As PartialViewResult
            Try

                Dim dadosPesquisa As GridSettings(Of PromocaoMixViewModel) = New GridSettings(Of PromocaoMixViewModel)()
                dadosPesquisa.Page = New PageSettings()
                dadosPesquisa.Page.OrderBy = "DESMERC"
                dadosPesquisa.Page.PageSize = 100

                dadosPesquisa.Filter = New PromocaoMixViewModel()
                dadosPesquisa.Filter.CODPMC = objeto.CODPMC
                dadosPesquisa.Filter.TIPEDEPUBALVPMC = objeto.TIPEDEPUBALVPMC

                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                Dim List As List(Of VO.PromocaoMix) = If(objeto.TIPFRNACOCMC = 1 _
                                                            , BO.ListarMixMercadoriasParametrosEdl(objeto.CODPMC, objeto.TIPSITPMC) _
                                                            , BO.ListarMixMercadoriasParametros(objeto.CODPMC, objeto.TIPSITPMC))

                Dim viewModelList As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(List)

                dadosPesquisa.Filter.TIPFRNACOCMC = objeto.TIPFRNACOCMC
                dadosPesquisa.Filter.INDCPTFCH = objeto.INDCPTFCH

                dadosPesquisa.Filter.INDITEOBR = 1
                dadosPesquisa.Filter.INDJRNSMA = 1

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixViewModel)(viewModelList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModelList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaParametrosMixPromocao"
                dadosPesquisa.PaginationID = "gridItensPesquisaParametrosMixPromocao"
                dadosPesquisa.ActionName = "PesquisaParametrosMixPromocao"
                dadosPesquisa.ControllerName = "PromocaoParametrosMix"

                Return PartialView("~/Views/Promocao/Mix/_ModalParametrosMixPromocao.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Pesquisar as Mercadorias - Paginação
        <HttpPost()>
        Function PesquisaParametrosMixPromocao(dadosPesquisa As GridSettings(Of PromocaoMixViewModel)) As PartialViewResult
            Try
                If (IsNothing(dadosPesquisa.Filter.TIPSITPMC)) Then
                    Dim BOpromocao As PromocaoBO = New PromocaoBO()
                    dadosPesquisa.Filter.TIPSITPMC = BOpromocao.SelectT(New VO.Promocao() With {.CODPMC = dadosPesquisa.Filter.CODPMC}).TIPSITPMC
                End If

                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                Dim List As List(Of VO.PromocaoMix) = If(dadosPesquisa.Filter.TIPFRNACOCMC = 1 _
                                                            , BO.ListarMixMercadoriasParametrosEdl(dadosPesquisa.Filter.CODPMC, dadosPesquisa.Filter.TIPSITPMC) _
                                                            , BO.ListarMixMercadoriasParametros(dadosPesquisa.Filter.CODPMC, dadosPesquisa.Filter.TIPSITPMC))

                Dim viewModelList As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(List)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixViewModel)(viewModelList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModelList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaParametrosMixPromocao"
                dadosPesquisa.PaginationID = "gridItensPesquisaParametrosMixPromocao"
                dadosPesquisa.ActionName = "PesquisaParametrosMixPromocao"
                dadosPesquisa.ControllerName = "PromocaoParametrosMix"

                Return PartialView("~/Views/Promocao/Mix/_GridParametrosMixPromocao.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Acao do botao de Aplicar do modal Parâmetros Mix Promoção
        ''' </summary>
        ''' <param name="list"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Aplicar(ByVal list As List(Of PromocaoParametrosMixViewModel))
            Try
                If (IsNothing(list)) Then
                    Warning(Constantes.SELECIONAR_MERCADORIA)
                    Return Nothing
                End If

                Dim BOpromocao As PromocaoBO = New PromocaoBO()
                Dim result As ValidacaoParametrosMixViewModel = BOpromocao.ValidarParametrizacao(list, list.FirstOrDefault().TIPFRNACOCMC)
                If (result.Mensagem <> String.Empty) Then
                    Warning(result.Mensagem.Replace(vbCrLf, "<br/>"))
                    Return result.Mensagem
                End If

                Dim BOpromocaoMix As PromocaoMixBO = New PromocaoMixBO()
                If (IsNothing(result.List)) Then
                    BOpromocaoMix.AplicarParametrosMixMercadoria(list)
                Else
                    BOpromocaoMix.AplicarParametrosMixMercadoria(result.List)
                End If

                If (result.Mensagem <> String.Empty) Then
                    Warning(result.Mensagem.Replace(vbCrLf, "<br/>"))
                    Return result.Mensagem
                End If
                Return String.Empty

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Importar Planila Execel
        <HttpPost()>
        Function ImportarXlsParametros(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
            Dim BOpromocaoMix As PromocaoMixBO = New PromocaoMixBO()

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseMixParametrosMercadoria"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = BOpromocaoMix.ImportarExelMixParametrosMercadoria(savedFileName, CODPMC, TIPEDEPUBALVPMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseMixParametrosMercadoria"
            Else
                Return "TrueMixParametrosMercadoria"
            End If
        End Function

#Region "CASH VIP"
        <HttpPost()>
        Public Function CarregarModalParametrosMixCashPromocao(objeto As PromocaoMixViewModel) As PartialViewResult
            Try

                Dim dadosPesquisa As GridSettings(Of PromocaoMixViewModel) = New GridSettings(Of PromocaoMixViewModel)()
                dadosPesquisa.Page = New PageSettings()
                dadosPesquisa.Page.OrderBy = "DESMERC"
                dadosPesquisa.Page.PageSize = 100

                dadosPesquisa.Filter = New PromocaoMixViewModel()
                dadosPesquisa.Filter.CODPMC = objeto.CODPMC
                dadosPesquisa.Filter.TIPEDEPUBALVPMC = objeto.TIPEDEPUBALVPMC

                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                Dim List As List(Of VO.PromocaoMix) = If(objeto.TIPFRNACOCMC = 1 _
                                                            , BO.ListarMixMercadoriasParametrosEdl(objeto.CODPMC, objeto.TIPSITPMC) _
                                                            , BO.ListarMixMercadoriasParametros(objeto.CODPMC, objeto.TIPSITPMC))

                Dim viewModelList As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(List)

                dadosPesquisa.Filter.TIPFRNACOCMC = objeto.TIPFRNACOCMC

                dadosPesquisa.Filter.INDITEOBR = 1
                dadosPesquisa.Filter.INDJRNSMA = 1

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixViewModel)(viewModelList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModelList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaParametrosMixCashPromocao"
                dadosPesquisa.PaginationID = "gridItensPesquisaParametrosMixCashPromocao"
                dadosPesquisa.ActionName = "PesquisaParametrosMixCashPromocao"
                dadosPesquisa.ControllerName = "PromocaoParametrosMix"

                Return PartialView("~/Views/Promocao/Mix/_ModalParametrosMixCashPromocao.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Pesquisar as Mercadorias - Paginação
        <HttpPost()>
        Function PesquisaParametrosMixCashPromocao(dadosPesquisa As GridSettings(Of PromocaoMixViewModel)) As PartialViewResult
            Try
                If (IsNothing(dadosPesquisa.Filter.TIPSITPMC)) Then
                    Dim BOpromocao As PromocaoBO = New PromocaoBO()
                    dadosPesquisa.Filter.TIPSITPMC = BOpromocao.SelectT(New VO.Promocao() With {.CODPMC = dadosPesquisa.Filter.CODPMC}).TIPSITPMC
                End If

                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                Dim List As List(Of VO.PromocaoMix) = If(dadosPesquisa.Filter.TIPFRNACOCMC = 1 _
                                                            , BO.ListarMixMercadoriasParametrosEdl(dadosPesquisa.Filter.CODPMC, dadosPesquisa.Filter.TIPSITPMC) _
                                                            , BO.ListarMixMercadoriasParametros(dadosPesquisa.Filter.CODPMC, dadosPesquisa.Filter.TIPSITPMC))

                Dim viewModelList As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(List)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixViewModel)(viewModelList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModelList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaParametrosMixCashPromocao"
                dadosPesquisa.PaginationID = "gridItensPesquisaParametrosMixCashPromocao"
                dadosPesquisa.ActionName = "PesquisaParametrosMixCashPromocao"
                dadosPesquisa.ControllerName = "PromocaoParametrosMix"

                Return PartialView("~/Views/Promocao/Mix/_GridParametrosMixCashPromocao.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Acao do botao de Aplicar do modal Parâmetros Mix Promoção
        ''' </summary>
        ''' <param name="list"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AplicarCash(ByVal list As List(Of PromocaoParametrosMixViewModel))
            Try
                If (IsNothing(list)) Then
                    Warning(Constantes.SELECIONAR_MERCADORIA)
                    Return Nothing
                End If

                Dim BOpromocao As PromocaoBO = New PromocaoBO()
                Dim result As ValidacaoParametrosMixViewModel = BOpromocao.ValidarParametrizacaoCash(list, list.FirstOrDefault().TIPFRNACOCMC)
                If (result.Mensagem <> String.Empty) Then
                    Warning(result.Mensagem.Replace(vbCrLf, "<br/>"))
                    Return result.Mensagem
                End If

                Dim BOpromocaoMix As PromocaoMixBO = New PromocaoMixBO()
                If (IsNothing(result.List)) Then
                    BOpromocaoMix.AplicarParametrosMixCashMercadoria(list)
                Else
                    BOpromocaoMix.AplicarParametrosMixCashMercadoria(result.List)
                End If

                If (result.Mensagem <> String.Empty) Then
                    Warning(result.Mensagem.Replace(vbCrLf, "<br/>"))
                    Return result.Mensagem
                End If
                Return String.Empty

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function ImportarXlsParametrosCash(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
            Dim BOpromocaoMix As PromocaoMixBO = New PromocaoMixBO()

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseMixParametrosMercadoria"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = BOpromocaoMix.ImportarExelMixCashParametrosMercadoria(savedFileName, CODPMC, TIPEDEPUBALVPMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseMixCashParametrosMercadoria"
            Else
                Return "TrueMixCashParametrosMercadoria"
            End If
        End Function
#End Region

    End Class

End Namespace
