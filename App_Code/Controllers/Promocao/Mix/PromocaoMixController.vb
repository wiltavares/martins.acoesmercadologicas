﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    <RoutePrefix(“PromocaoMix”)>
    Public Class PromocaoMixController
        Inherits AbstractController

        <HttpGet()>
        Function CarregarWidgetPromocaoMix(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal INDPTCTOTMIXPMC As Decimal) As PartialViewResult
            Try
                ViewBag.acao = "Alterar"

                'Controle Visibilidade se Participação Mix Total Sim ou Não
                ViewBag.ParticipacaoMixTotal = INDPTCTOTMIXPMC


                Dim promocaoBO As New PromocaoBO
                Dim promocao = promocaoBO.SelectT(New VO.Promocao() With {.CODPMC = CODPMC})
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)
                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim verificaCashBack = promocaoBO.VerificaSeCashBack(CODPMC)
                promocaoViewModel.INDTIPPTOPMC1 = verificaCashBack.INDTIPPTOPMC1
                promocaoViewModel.INDTIPPTOPMC2 = verificaCashBack.INDTIPPTOPMC2

                ViewBag.INDEVTSMA = promocaoViewModel.INDEVTSMA
                ViewBag.INDCASHBACK = promocaoViewModel.INDTIPPTOPMC

                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                Dim obj As VO.PromocaoMix = BO.SelectCountMix(CODPMC)
                Dim objMix As PromocaoMixViewModel = Mapper.Map(Of VO.PromocaoMix, PromocaoMixViewModel)(obj)

                Return PartialView("~/Views/Promocao/Mix/Edit.vbhtml", objMix)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Function Index() As RedirectResult
            Return New RedirectResult("Home")
        End Function
#Region "Mix Grupo Mercadoria"
        <HttpPost()>
        Public Function EditPromocaoMixGrupoMercadoria(objeto As VO.PromocaoMix) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim promocao = promocaoBO.SelectT(New VO.Promocao() With {.CODPMC = objeto.CODPMC})
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)
                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim BO As PromocaoMixBO = New PromocaoMixBO()

                Dim CODPMC = objeto.CODPMC
                'Dim TIPEDEPUBALVPMC = 4

                Dim GrupoMercadoriaList As List(Of VO.PromocaoMix) = BO.EditMixGrupoMercadoria(CODPMC)
                Dim GrupoMercadoriaViewModelList As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(GrupoMercadoriaList)


                Return PartialView("~/Views/Promocao/Mix/GrupoMercadoria/_ModalMixGrupoMercadoria.vbhtml", GrupoMercadoriaViewModelList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados Grupo Mercadoria
        <HttpPost()>
        Function SaveMixGrupoMercadoria(ByVal CODPMC As Decimal, ByVal obj As List(Of PromocaoMixViewModel)) As Boolean
            Dim PromocaoMixBO = New PromocaoMixBO()
            Try
                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                If (ModelState.IsValid) Then
                    Dim PromocaoMixGrupoMercadoria = Mapper.Map(Of List(Of PromocaoMixViewModel), List(Of VO.PromocaoMix))(obj)

                    BO.InsertListMixGrupoMercadoria(CODPMC, PromocaoMixGrupoMercadoria)

                    Return True
                End If

                Return False

            Catch ex As Exception
                Danger(ex)
                Return False
            End Try
        End Function
#End Region

#Region "Mix Familia Mercadoria"
        <HttpPost()>
        Public Function EditPromocaoMixFamiliaMercadoria(ByVal objeto As PromocaoMixViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = objeto.CODPMC
                'ObjFiltro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim obj As PromocaoMixViewModel = New PromocaoMixViewModel()

                Return PartialView("~/Views/Promocao/Mix/FamiliaMercadoria/_ModalMixFamiliaMercadoria.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function CarregarWidgetLoadPesquisaMixFamiliaMercadoria(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As PartialViewResult
            Try

                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = CODPMC
                'ObjFiltro.CODPMC = CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)


                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of PromocaoMixViewModel)
                gridConf.Filter = New PromocaoMixViewModel()
                gridConf.Filter.CODPMC = CODPMC
                gridConf.Filter.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                gridConf.ActionName = "PesquisaMixFamiliaMercadoria"
                gridConf.ResultID = "gridItensPesquisaPromocaoMixFamiliaMercadoria"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "DESFMLMERC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of PromocaoMixViewModel), gridConf.Page)

                Return PartialView("~/Views/Promocao/Mix/FamiliaMercadoria/_WidgetLoadPesquisaMixFamiliaMercadoria.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMixFamiliaMercadoria(dadosPesquisa As GridSettings(Of PromocaoMixViewModel)) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = dadosPesquisa.Filter.CODPMC
                'ObjFiltro.CODPMC = dadosPesquisa.Filter.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim BO As PromocaoMixBO = New PromocaoMixBO()

                Dim dadosFiltro As VO.PromocaoMix = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.PromocaoMix) = BO.PesquisaMixFamiliaMercadoria(dadosFiltro)
                Dim viewModeList As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaPromocaoMixFamiliaMercadoria"
                dadosPesquisa.PaginationID = "gridItensPesquisaPromocaoMixFamiliaMercadoria"
                dadosPesquisa.ActionName = "PesquisaMixFamiliaMercadoria"
                dadosPesquisa.ControllerName = "PromocaoMix"

                Return PartialView("~/Views/Promocao/Mix/FamiliaMercadoria/_GridPesquisaMixFamiliaMercadoria.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function CarregarWidgetMixFamiliaMercadoriaAdicionados(VO As VO.PromocaoMix) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = VO.CODPMC
                'ObjFiltro.CODPMC = VO.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim BO As PromocaoMixBO = New PromocaoMixBO()

                Dim list As List(Of VO.PromocaoMix) = BO.ListarMixFamiliaMercadoriaAdicionados(VO)
                'ViewBag.ItensLoadPesquisaMixGrupoMercadoria = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(list)

                Dim objViewModel As PromocaoMixViewModel = New PromocaoMixViewModel()
                objViewModel.ListMixFamiliaMercadoria = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(list)


                Return PartialView("~/Views/Promocao/Mix/FamiliaMercadoria/_WidgetMixFamiliaMercadoria.vbhtml", objViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Adiciona o resultado da pesquisa ao mix.
        <HttpPost()>
        Function AddPesquisaMixFamiliaMercadoria(filtro As PromocaoMixViewModel) As PartialViewResult
            Try
                Dim filtroVO As VO.PromocaoMix = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(filtro)

                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                BO.AddPesquisaMixFamiliaMercadoria(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados Mix Familia Mercadoria
        <HttpPost()>
        Function SaveMixFamiliaMercadoria(ByVal obj As PromocaoMixViewModel) As PartialViewResult
            Dim BO = New PromocaoMixBO()

            Dim objFamiliaMercadoriaExiste As VO.PromocaoMix = BO.SelectT(New VO.PromocaoMix With {.CODPMC = obj.CODPMC, .CODFMLMER = obj.CODFMLMER})

            If (objFamiliaMercadoriaExiste IsNot Nothing) Then
                Warning(Constantes.REGISTRO_DUPLICADO)
                Return Nothing
            End If

            Dim relacaoAcaoMecanicMixFamiliaMercadoria = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(obj)

            BO.InsertT(relacaoAcaoMecanicMixFamiliaMercadoria)

            Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

            Return Nothing

        End Function

        'Deletar Itens Mix Familia Mercadoria
        <HttpPost()>
        Function DeleteMixFamiliaMercadoria(ByVal obj As PromocaoMixViewModel) As PartialViewResult
            Dim BO = New PromocaoMixBO()

            Dim PromocaoMixFamiliaMercadoria = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(obj)
            BO.DeleteMixFamiliaMercadoria(PromocaoMixFamiliaMercadoria)
            Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
            Return Nothing

        End Function

        'Importar Planila Execel
        <HttpPost()>
        Public Function ImportarXlsMixFamiliaMercadoria(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
            Dim promocaoMixBO As New PromocaoMixBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseMixFamiliaMercadoria"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseMixFamiliaMercadoria"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = promocaoMixBO.ImportarExelMixFamiliaMercadoria(savedFileName, CODPMC, TIPEDEPUBALVPMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseMixFamiliaMercadoria"
            Else
                Return "TrueMixFamiliaMercadoria"
            End If
        End Function
#End Region

#Region "Mix Classe Mercadoria"
        <HttpPost()>
        Public Function EditPromocaoMixClasseMercadoria(ByVal objeto As PromocaoMixViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = objeto.CODPMC
                'ObjFiltro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim obj As PromocaoMixViewModel = New PromocaoMixViewModel()

                Return PartialView("~/Views/Promocao/Mix/ClasseMercadoria/_ModalMixClasseMercadoria.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function CarregarWidgetLoadPesquisaMixClasseMercadoria(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = CODPMC
                'ObjFiltro.CODPMC = CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)


                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of PromocaoMixViewModel)
                gridConf.Filter = New PromocaoMixViewModel()
                gridConf.Filter.CODPMC = CODPMC
                gridConf.Filter.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                gridConf.ActionName = "PesquisaMixClasseMercadoria"
                gridConf.ResultID = "gridItensPesquisaPromocaoMixClasseMercadoria"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "DESCLSMERC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of PromocaoMixViewModel), gridConf.Page)

                Return PartialView("~/Views/Promocao/Mix/ClasseMercadoria/_WidgetLoadPesquisaMixClasseMercadoria.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMixClasseMercadoria(dadosPesquisa As GridSettings(Of PromocaoMixViewModel)) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = dadosPesquisa.Filter.CODPMC
                'ObjFiltro.CODPMC = dadosPesquisa.Filter.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)


                Dim BO As PromocaoMixBO = New PromocaoMixBO()

                Dim dadosFiltro As VO.PromocaoMix = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.PromocaoMix) = BO.PesquisaMixClasseMercadoria(dadosFiltro)
                Dim viewModeList As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaPromocaoMixClasseMercadoria"
                dadosPesquisa.PaginationID = "gridItensPesquisaPromocaoMixClasseMercadoria"
                dadosPesquisa.ActionName = "PesquisaMixClasseMercadoria"
                dadosPesquisa.ControllerName = "PromocaoMix"

                Return PartialView("~/Views/Promocao/Mix/ClasseMercadoria/_GridPesquisaMixClasseMercadoria.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function CarregarWidgetMixClasseMercadoriaAdicionados(VO As VO.PromocaoMix) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = VO.CODPMC
                'ObjFiltro.CODPMC = VO.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim BO As PromocaoMixBO = New PromocaoMixBO()

                Dim list As List(Of VO.PromocaoMix) = BO.ListarMixClasseMercadoriaAdicionados(VO)
                Dim listViewModel As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(list)

                Return PartialView("~/Views/Promocao/Mix/ClasseMercadoria/_WidgetMixClasseMercadoria.vbhtml", listViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Adiciona o resultado da pesquisa ao mix.
        <HttpPost()>
        Function AddPesquisaMixClasseMercadoria(filtro As PromocaoMixViewModel) As PartialViewResult
            Try
                Dim filtroVO As VO.PromocaoMix = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(filtro)

                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                BO.AddPesquisaMixClasseMercadoria(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados Mix Classe Mercadoria
        <HttpPost()>
        Function SaveMixClasseMercadoria(ByVal obj As PromocaoMixViewModel) As PartialViewResult
            Dim BO = New PromocaoMixBO()

            Dim objClasseMercadoriaExiste As VO.PromocaoMix = BO.SelectT(New VO.PromocaoMix With {.CODPMC = obj.CODPMC, .CODCLSMER = obj.CODCLSMER})

            If (objClasseMercadoriaExiste IsNot Nothing) Then
                Warning(Constantes.REGISTRO_DUPLICADO)
                Return Nothing
            End If

            Dim relacaoAcaoMecanicMixClasseMercadoria = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(obj)

            BO.InsertT(relacaoAcaoMecanicMixClasseMercadoria)

            Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

            Return Nothing

        End Function

        'Deletar Itens Mix Classe Mercadoria
        <HttpPost()>
        Function DeleteMixClasseMercadoria(ByVal obj As PromocaoMixViewModel) As PartialViewResult
            Dim BO = New PromocaoMixBO()

            Dim PromocaoMixClasseMercadoria = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(obj)

            BO.DeleteMixClasseMercadoria(PromocaoMixClasseMercadoria)

            Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
            Return Nothing

        End Function

        'Importar Planila Execel
        <HttpPost()>
        Public Function ImportarXlsMixClasseMercadoria(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
            Dim promocaoMixBO As New PromocaoMixBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseMixClasseMercadoria"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseMixClasseMercadoria"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = promocaoMixBO.ImportarExelMixClasseMercadoria(savedFileName, CODPMC, TIPEDEPUBALVPMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseMixClasseMercadoria"
            Else
                Return "TrueMixClasseMercadoria"
            End If
        End Function
#End Region

#Region "Mix Mercadoria"
        <HttpPost()>
        Public Function EditPromocaoMixMercadoria(ByVal objeto As PromocaoMixViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim obj As PromocaoMixViewModel = New PromocaoMixViewModel()

                Return PartialView("~/Views/Promocao/Mix/Mercadoria/_ModalMixMercadoria.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function CarregarWidgetLoadPesquisaMixMercadoria(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As PartialViewResult
            Try
                ViewBag.acao = "Alterar"


                Dim promocaoBO As New PromocaoBO
                Dim promocao = promocaoBO.SelectT(New VO.Promocao() With {.CODPMC = CODPMC})

                Dim filialBO As New FilialEmpresaCorporativoBO
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP

                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(New VO.EntidadePublicoAlvoPromocao() With {.CODPMC = CODPMC})
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                'Validar Se Dados Pode Ser Alterado
                ControlaVisibilidadeComponentes(promocaoViewModel)


                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of PromocaoMixViewModel)
                gridConf.Filter = New PromocaoMixViewModel()
                gridConf.Filter.CODPMC = CODPMC
                gridConf.Filter.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                gridConf.ActionName = "PesquisaMixMercadoria"
                gridConf.ResultID = "gridItensPesquisaPromocaoMixMercadoria"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "DESMERC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of PromocaoMixViewModel), gridConf.Page)

                ViewBag.TIPPMC = promocao.TIPPMC
                If (promocao.TIPPMC = 1) Then
                    ViewBag.QDEGRPMIXPMC = promocao.QDEGRPMIXPMC
                Else
                    ViewBag.QDEGRPMIXPMC = 0
                End If



                Return PartialView("~/Views/Promocao/Mix/Mercadoria/_WidgetLoadPesquisaMixMercadoria.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMixMercadoria(dadosPesquisa As GridSettings(Of PromocaoMixViewModel)) As PartialViewResult
            Try
                If Not ValidaFiltroMixMercadoria(dadosPesquisa.Filter) Then
                    Return Nothing
                End If

                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = dadosPesquisa.Filter.CODPMC
                'ObjFiltro.CODPMC = dadosPesquisa.Filter.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                ' Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim BOrelacaoAcaoFornecedor As RelacaoAcaoComercialxFornecedoresBO = New RelacaoAcaoComercialxFornecedoresBO()
                Dim relacaoAcaoFornecedor As VO.RelacaoAcaoComercialxFornecedores = BOrelacaoAcaoFornecedor.BuscarFornecedoresDaAcao(promocao.CODACOCMC).FirstOrDefault()

                Dim BO As PromocaoMixBO = New PromocaoMixBO()

                Dim dadosFiltro As VO.PromocaoMix = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(dadosPesquisa.Filter)
                Dim voList As List(Of VO.PromocaoMix)
                If (relacaoAcaoFornecedor.TIPFRNACOCMC = 1) Then
                    dadosFiltro.CODFRN = relacaoAcaoFornecedor.CODFRN
                    voList = BO.PesquisaMixMercadoriaEdl(dadosFiltro)
                Else
                    voList = BO.PesquisaMixMercadoria(dadosFiltro)
                End If

                Dim viewModeList As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaPromocaoMixMercadoria"
                dadosPesquisa.PaginationID = "gridItensPesquisaPromocaoMixMercadoria"
                dadosPesquisa.ActionName = "PesquisaMixMercadoria"
                dadosPesquisa.ControllerName = "PromocaoMix"

                Return PartialView("~/Views/Promocao/Mix/Mercadoria/_GridPesquisaMixMercadoria.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function CarregarWidgetMixMercadoriaAdicionados(VO As VO.PromocaoMix) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = VO.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim BOrelacaoAcaoFornecedor As RelacaoAcaoComercialxFornecedoresBO = New RelacaoAcaoComercialxFornecedoresBO()
                Dim relacaoAcaoFornecedor As VO.RelacaoAcaoComercialxFornecedores = BOrelacaoAcaoFornecedor.BuscarFornecedoresDaAcao(promocao.CODACOCMC).FirstOrDefault()

                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                Dim list As List(Of VO.PromocaoMix)
                If (relacaoAcaoFornecedor.TIPFRNACOCMC = 1) Then
                    list = BO.ListarMixMercadoriaAdicionadosEdl(VO)
                Else
                    list = BO.ListarMixMercadoriaAdicionados(VO)
                End If
                Dim listViewModel As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(list)

                Return PartialView("~/Views/Promocao/Mix/Mercadoria/_WidgetMixMercadoria.vbhtml", listViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Adiciona o resultado da pesquisa ao mix.
        <HttpPost()>
        Function AddPesquisaMixMercadoria(filtro As PromocaoMixViewModel) As PartialViewResult
            Try
                If Not ValidaFiltroMixMercadoria(filtro) Then
                    Return Nothing
                End If

                Dim filtroVO As VO.PromocaoMix = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(filtro)

                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                BO.AddPesquisaMixMercadoria(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Private Function ValidaFiltroMixMercadoria(ByRef filtro As PromocaoMixViewModel) As Boolean

            If filtro.CODMER Is Nothing And filtro.DESMER Is Nothing And
                filtro.CODGRPMER Is Nothing And filtro.DESGRPMER Is Nothing And
                filtro.CODFMLMER Is Nothing And filtro.DESFMLMER Is Nothing And
                filtro.CODCLSMER Is Nothing And filtro.DESCLSMER Is Nothing And
                filtro.CODFRN Is Nothing And filtro.NOMFRN Is Nothing Then
                Warning("Informe pelo menos um filtro")
                Return False
            End If

            If Not filtro.DESMER Is Nothing Then
                If (filtro.DESMER.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome da mercadoria")
                    Return False
                End If
            End If
            If Not filtro.DESGRPMER Is Nothing Then
                If (filtro.DESGRPMER.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do grupo de mercadoria")
                    Return False
                End If
            End If
            If Not filtro.DESFMLMER Is Nothing Then
                If (filtro.DESFMLMER.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome da familia de mercadoria")
                    Return False
                End If
            End If
            If Not filtro.DESCLSMER Is Nothing Then
                If (filtro.DESCLSMER.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome da classe de mercadoria")
                    Return False
                End If
            End If
            If Not filtro.NOMFRN Is Nothing Then
                If (filtro.NOMFRN.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do fornecedor")
                    Return False
                End If
            End If
            Return True
        End Function

        'Metodo salvar dados Mix Mercadoria
        <HttpPost()>
        Function SaveMixMercadoria(ByVal obj As PromocaoMixViewModel) As PartialViewResult
            Dim BO = New PromocaoMixBO()

            Dim objMercadoriaExiste As VO.PromocaoMix = BO.SelectT(New VO.PromocaoMix With {.CODPMC = obj.CODPMC, .CODMER = obj.CODMER})

            If (objMercadoriaExiste IsNot Nothing) Then
                Warning(Constantes.REGISTRO_DUPLICADO)
                Return Nothing
            End If

            Dim relacaoAcaoMecanicMixMercadoria = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(obj)

            BO.InsertT(relacaoAcaoMecanicMixMercadoria)

            Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

            Return Nothing

        End Function


        'Deletar Itens Mix Mercadoria
        <HttpPost()>
        Function DeleteMixMercadoria(ByVal obj As PromocaoMixViewModel) As PartialViewResult
            Dim BO = New PromocaoMixBO()

            Dim PromocaoMixMercadoria = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(obj)

            BO.DeleteMixMercadoria(PromocaoMixMercadoria)

            Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
            Return Nothing

        End Function

        'Importar Planila Execel
        <HttpPost()>
        Public Function ImportarXlsMixMercadoria(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
            Dim promocaoMixBO As New PromocaoMixBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseMixMercadoria"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseMixMercadoria"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = promocaoMixBO.ImportarExelMixMercadoria(savedFileName, CODPMC, TIPEDEPUBALVPMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseMixMercadoria"
            Else
                Return "TrueMixMercadoria"
            End If
        End Function


        <HttpGet()>
        Function CarregarWidgetLoadPesquisaMixMercadoriaPorPromocao(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As PartialViewResult
            Try
                ViewBag.acao = "Alterar"


                Dim promocaoBO As New PromocaoBO
                Dim promocao = promocaoBO.SelectT(New VO.Promocao() With {.CODPMC = CODPMC})

                Dim filialBO As New FilialEmpresaCorporativoBO
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP

                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                'Validar Se Dados Pode Ser Alterado
                ControlaVisibilidadeComponentes(promocaoViewModel)

                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of PromocaoMixViewModel)
                gridConf.Filter = New PromocaoMixViewModel()
                gridConf.Filter.CODPMC = CODPMC
                gridConf.Filter.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                gridConf.ActionName = "PesquisaMixMercadoriaPorPromocao"
                gridConf.ResultID = "gridItensPesquisaPromocaoMixMercadoriaPorPromocao"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "DESMERC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of PromocaoMixViewModel), gridConf.Page)

                ViewBag.TIPPMC = promocao.TIPPMC
                If (promocao.TIPPMC = 1) Then
                    ViewBag.QDEGRPMIXPMC = promocao.QDEGRPMIXPMC
                Else
                    ViewBag.QDEGRPMIXPMC = 0
                End If



                Return PartialView("~/Views/Promocao/Mix/Mercadoria/PorPromocao/_WidgetLoadPesquisaMixMercadoriaPorPromocao.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMixMercadoriaPorPromocao(dadosPesquisa As GridSettings(Of PromocaoMixViewModel)) As PartialViewResult
            Try
                If dadosPesquisa.Filter.CODPMC Is Nothing Or dadosPesquisa.Filter.CODPMCtoFind Is Nothing Then
                    Warning("Informe pelo menos um filtro")
                    Return Nothing
                End If

                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = dadosPesquisa.Filter.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim BO As PromocaoMixBO = New PromocaoMixBO()

                Dim voList As List(Of VO.PromocaoMix) = BO.PesquisaMixMercadoriaPorPromocao(dadosPesquisa.Filter.CODPMC, dadosPesquisa.Filter.CODPMCtoFind)
                Dim viewModeList As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaPromocaoMixMercadoriaPorPromocao"
                dadosPesquisa.PaginationID = "gridItensPesquisaPromocaoMixMercadoriaPorPromocao"
                dadosPesquisa.ActionName = "PesquisaMixMercadoriaPorPromocao"
                dadosPesquisa.ControllerName = "PromocaoMix"

                Return PartialView("~/Views/Promocao/Mix/Mercadoria/PorPromocao/_GridPesquisaMixMercadoriaPorPromocao.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados Mix Mercadoria por promoção
        <HttpPost()>
        Function SaveMixMercadoriaPorPromocao(ByVal obj As PromocaoMixViewModel) As PartialViewResult
            Dim BO = New PromocaoMixBO()

            Dim objMercadoriaExiste As VO.PromocaoMix = BO.SelectT(New VO.PromocaoMix With {.CODPMC = obj.CODPMC, .CODMER = obj.CODMER})

            If (objMercadoriaExiste IsNot Nothing) Then
                Warning(Constantes.REGISTRO_DUPLICADO)
                Return Nothing
            End If

            Dim relacaoAcaoMecanicMixMercadoria = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(obj)

            BO.InsertT(relacaoAcaoMecanicMixMercadoria)

            Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

            Return Nothing

        End Function

        'Adiciona o resultado da pesquisa ao mix por promocao.
        <HttpPost()>
        Function AddPesquisaMixMercadoriaPorPromocao(filtro As PromocaoMixViewModel) As PartialViewResult
            Try

                If filtro.CODPMC Is Nothing Or filtro.CODPMCtoFind Is Nothing Then
                    Warning("Informe pelo menos um filtro")
                    Return Nothing
                End If

                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                BO.AddPesquisaMixMercadoriaPorPromocao(filtro.CODPMC, filtro.CODPMCtoFind, filtro.TIPEDEPUBALVPMC, filtro.CODGRPMIXMERPMC)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

#End Region

#Region "Mix Fornecedor"
        <HttpPost()>
        Public Function EditPromocaoMixFornecedor(ByVal objeto As PromocaoMixViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim obj As PromocaoMixViewModel = New PromocaoMixViewModel()

                Return PartialView("~/Views/Promocao/Mix/Fornecedor/_ModalMixFornecedor.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function CarregarWidgetLoadPesquisaMixFornecedor(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As PartialViewResult
            Try

                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)


                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of PromocaoMixViewModel)
                gridConf.Filter = New PromocaoMixViewModel()
                gridConf.Filter.CODPMC = CODPMC
                gridConf.Filter.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                gridConf.ActionName = "PesquisaMixFornecedor"
                gridConf.ResultID = "gridItensPesquisaPromocaoMixFornecedor"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMFRNC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of PromocaoMixViewModel), gridConf.Page)

                Dim boRelacaoEquipeDeComprasxGerenteNegocioServCompras As New RelacaoEquipeDeComprasxGerenteNegocioServComprasBO
                ViewBag.ListaRelacaoEquipeDeComprasxGerenteNegocioServCompras = boRelacaoEquipeDeComprasxGerenteNegocioServCompras.ListT(New VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras()).OrderBy(Function(model) model.DESDIVCMP)

                Dim boDiretoriaCompra As New DiretoriaCompraBO
                ViewBag.ListaDiretoriaCompra = boDiretoriaCompra.ListT(New VO.DiretoriaCompra()).OrderBy(Function(model) model.DESDRTCMP)


                Return PartialView("~/Views/Promocao/Mix/Fornecedor/_WidgetLoadPesquisaMixFornecedor.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMixFornecedor(dadosPesquisa As GridSettings(Of PromocaoMixViewModel)) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = dadosPesquisa.Filter.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim BO As PromocaoMixBO = New PromocaoMixBO()

                Dim dadosFiltro As VO.PromocaoMix = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.PromocaoMix) = BO.PesquisaMixFornecedor(dadosFiltro)
                Dim viewModeList As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoMixViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaPromocaoMixFornecedor"
                dadosPesquisa.PaginationID = "gridItensPesquisaPromocaoMixFornecedor"
                dadosPesquisa.ActionName = "PesquisaMixFornecedor"
                dadosPesquisa.ControllerName = "PromocaoMix"

                Return PartialView("~/Views/Promocao/Mix/Fornecedor/_GridPesquisaMixFornecedor.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function CarregarWidgetMixFornecedorAdicionados(VO As VO.PromocaoMix) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = VO.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim BO As PromocaoMixBO = New PromocaoMixBO()

                Dim list As List(Of VO.PromocaoMix) = BO.ListarMixFornecedorAdicionados(VO)
                Dim listViewModel As List(Of PromocaoMixViewModel) = Mapper.Map(Of List(Of VO.PromocaoMix), List(Of PromocaoMixViewModel))(list)

                Return PartialView("~/Views/Promocao/Mix/Fornecedor/_WidgetMixFornecedor.vbhtml", listViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Adiciona o resultado da pesquisa ao mix.
        <HttpPost()>
        Function AddPesquisaMixFornecedor(filtro As PromocaoMixViewModel) As PartialViewResult
            Try
                Dim filtroVO As VO.PromocaoMix = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(filtro)

                Dim BO As PromocaoMixBO = New PromocaoMixBO()
                BO.AddPesquisaMixFornecedor(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados Mix Classe Mercadoria
        <HttpPost()>
        Function SaveMixFornecedor(ByVal obj As PromocaoMixViewModel) As PartialViewResult
            Dim BO = New PromocaoMixBO()

            Dim objFornecedorExiste As VO.PromocaoMix = BO.SelectT(New VO.PromocaoMix With {.CODPMC = obj.CODPMC, .CODFRN = obj.CODFRN})

            If (objFornecedorExiste IsNot Nothing) Then
                Warning(Constantes.REGISTRO_DUPLICADO)
                Return Nothing
            End If

            Dim relacaoAcaoMecanicMixFornecedor = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(obj)

            BO.InsertT(relacaoAcaoMecanicMixFornecedor)

            Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

            Return Nothing

        End Function

        'Deletar Itens Mix Classe Mercadoria
        <HttpPost()>
        Function DeleteMixFornecedor(ByVal obj As PromocaoMixViewModel) As PartialViewResult
            Dim BO = New PromocaoMixBO()

            Dim PromocaoMixFornecedor = Mapper.Map(Of PromocaoMixViewModel, VO.PromocaoMix)(obj)

            BO.DeleteMixFornecedor(PromocaoMixFornecedor)

            Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
            Return Nothing

        End Function


        'Importar Planila Execel
        <HttpPost()>
        Public Function ImportarXlsMixFornecedor(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
            Dim promocaoMixBO As New PromocaoMixBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseMixFornecedor"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseMixFornecedor"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = promocaoMixBO.ImportarExelMixFornecedor(savedFileName, CODPMC, TIPEDEPUBALVPMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseMixFornecedor"
            Else
                Return "TrueMixFornecedor"
            End If
        End Function
#End Region


        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            Dim PermiteEdicao As Boolean = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

            If (PermiteEdicao) Then
                Dim authCookie As HttpCookie = Request.RequestContext.HttpContext.Request.Cookies(FormsAuthentication.FormsCookieName)
                If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                    Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                    Dim roles As String() = Split(authTicket.UserData, "|")(1).Split(";"c)
                    If (Not roles.Where(Function(f) f = "46").FirstOrDefault() Is Nothing) Then
                        PermiteEdicao = True
                    Else
                        PermiteEdicao = False
                    End If
                End If
            End If
            If obj.TIPPMC = 1 Then
                ViewBag.PermiteEdicaoPromocaoMixGrupoMercadoria = False
                ViewBag.PermiteEdicaoPromocaoMixFamiliaMercadoria = False
                ViewBag.PermiteEdicaoPromocaoMixClasseMercadoria = False
                ViewBag.PermiteEdicaoPromocaoMixFornecedor = False
            Else
                ViewBag.PermiteEdicaoPromocaoMixGrupoMercadoria = PermiteEdicao
                ViewBag.PermiteEdicaoPromocaoMixFamiliaMercadoria = PermiteEdicao
                ViewBag.PermiteEdicaoPromocaoMixClasseMercadoria = PermiteEdicao
                ViewBag.PermiteEdicaoPromocaoMixFornecedor = PermiteEdicao
            End If

            ViewBag.PermiteEdicaoPromocaoMixMercadoria = PermiteEdicao

            ViewBag.SituacaoPromocao = obj.DESSITPMC
            ViewBag.TIPPMC = obj.TIPPMC

        End Sub



        '<HttpPost()>
        '<AllowAnonymous>
        'Function AtualizaMixPromocoesVigentes() As Newtonsoft.Json.Linq.JArray

        '    Dim promocaoBO As New PromocaoBO()
        '    Dim promocoesVigentes = promocaoBO.ListaParaGrid(New VO.Promocao() With {.TIPSITPMC = " "})


        '    Dim resultado As New Newtonsoft.Json.Linq.JArray()
        '    For Each item In promocoesVigentes
        '        Try
        '            Dim bo As New PromocaoMixVisualizarVigenteBO()
        '            bo.AtualizaMixPromocao(item.CODPMC, item.TIPEDEPUBALVPMC, item.TIPPMC, True)
        '        Catch ex As Exception
        '            resultado.Add(New Newtonsoft.Json.Linq.JObject From {{"CODPMC", item.CODPMC}, {"ERRO", ex.Message}})
        '        End Try
        '    Next


        '    Return resultado
        'End Function
    End Class

End Namespace
