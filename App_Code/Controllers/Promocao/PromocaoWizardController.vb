﻿Imports Microsoft.VisualBasic
Imports System.Web.Mvc
Imports AutoMapper
Imports System.Data

Namespace Controllers

    <Authorize(Roles:="44")>
    Public Class PromocaoWizardController
        Inherits AbstractController




        <HttpPost()>
        <Authorize(Roles:="44")>
        Function Pesquisar(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaViewModel)) As PartialViewResult
            Try

                Dim boAauthor As New FuncionarioBO()
                ViewBag.ListaFuncionarios = boAauthor.ListFuncionariosPromocao(New VO.Funcionario()).OrderBy(Function(m) m.CODFNC)

                ' Busca os dados da mecanica da promocão
                Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
                Dim relacaoAcaoMecanica As List(Of VO.RelacaoAcaoMecanica) = BO.ListPromocoesPendenteGeracao(dadosPesquisa.Filter.CODACOCMC, dadosPesquisa.Filter.NOMACOCMC, dadosPesquisa.Filter.DATINIACOCMC, dadosPesquisa.Filter.DATFIMACOCMC, dadosPesquisa.Filter.CODFNCCRIACOCMC)
                Dim relacaoAcaoMecanicaViewModel As List(Of RelacaoAcaoMecanicaViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanica), List(Of RelacaoAcaoMecanicaViewModel))(relacaoAcaoMecanica)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaViewModel)(relacaoAcaoMecanicaViewModel, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = relacaoAcaoMecanicaViewModel.Count
                '
                If (dadosPesquisa.ListPaged.Count > 0) Then
                    Dim BOFilial As RelacaoAcaoMecanicaFilialBO = New RelacaoAcaoMecanicaFilialBO()
                    For Each item In relacaoAcaoMecanicaViewModel
                        Dim listFilial As List(Of VO.RelacaoAcaoMecanicaFilial) = BOFilial.ListFiliaisRelacaoMecanica(item.CODACOCMC, item.NUMSEQRGRACOCMC)
                        item.RelacaoAcaoMecanicaFilialList = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaFilial), List(Of RelacaoAcaoMecanicaFilialViewModel))(listFilial)

                    Next
                End If

                If dadosPesquisa.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                Return PartialView("~/Views/Promocao/Wizard/PendenciasResultados.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return PartialView("~/Views/Promocao/Wizard/PendenciasResultados.vbhtml", dadosPesquisa)
            End Try
        End Function

        <HttpGet()>
        <Authorize(Roles:="44")>
        Function Index() As ViewResult
            Dim gridSettings As New GridSettings(Of RelacaoAcaoMecanicaViewModel)
            Try

                gridSettings.Filter = New RelacaoAcaoMecanicaViewModel()
                gridSettings.Filter.CODFNCCRIACOCMC = ControleAcesso.CodFnc()

                Dim boAauthor As New FuncionarioBO()
                ViewBag.ListaFuncionarios = boAauthor.ListFuncionariosPromocao(New VO.Funcionario()).OrderBy(Function(m) m.CODFNC)

                ' Busca os dados da mecanica da promocão
                Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
                Dim relacaoAcaoMecanica As List(Of VO.RelacaoAcaoMecanica) = BO.ListPromocoesPendenteGeracao(Nothing, Nothing, Nothing, Nothing, gridSettings.Filter.CODFNCCRIACOCMC)
                'Dim relacaoAcaoMecanica As List(Of VO.RelacaoAcaoMecanica) = BO.ListPromocoesPendenteGeracao()
                Dim relacaoAcaoMecanicaViewModel As List(Of RelacaoAcaoMecanicaViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanica), List(Of RelacaoAcaoMecanicaViewModel))(relacaoAcaoMecanica)

                gridSettings.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaViewModel)(relacaoAcaoMecanicaViewModel, gridSettings.Page)
                gridSettings.TotalCount = relacaoAcaoMecanicaViewModel.Count
                '
                If (gridSettings.ListPaged.Count > 0) Then
                    Dim BOFilial As RelacaoAcaoMecanicaFilialBO = New RelacaoAcaoMecanicaFilialBO()
                    For Each item In relacaoAcaoMecanicaViewModel
                        Dim listFilial As List(Of VO.RelacaoAcaoMecanicaFilial) = BOFilial.ListFiliaisRelacaoMecanica(item.CODACOCMC, item.NUMSEQRGRACOCMC)
                        item.RelacaoAcaoMecanicaFilialList = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaFilial), List(Of RelacaoAcaoMecanicaFilialViewModel))(listFilial)

                    Next
                End If

                If gridSettings.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                Return View("~/Views/Promocao/Wizard/Pendencias.vbhtml", gridSettings)
            Catch ex As Exception
                Danger(ex)
                Return View("~/Views/Promocao/Wizard/Pendencias.vbhtml", gridSettings)
            End Try
        End Function

        <HttpGet()>
        <Authorize(Roles:="44")>
        Function CreateWizard(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As ViewResult
            Try
                ' Busca os dados da mecanica da promocão
                Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
                Dim relacaoAcaoMecanica As VO.RelacaoAcaoMecanica = BO.SelectEditMecanicaGerais(CODACOCMC, NUMSEQRGRACOCMC)

                Dim relacaoAcaoMecanicaViewModel As RelacaoAcaoMecanicaViewModel = Mapper.Map(Of VO.RelacaoAcaoMecanica, RelacaoAcaoMecanicaViewModel)(relacaoAcaoMecanica)


                Dim acaoComercialBO As New AcaoComercialBO
                Dim acaoComercial As VO.AcaoComercial = acaoComercialBO.BuscaAcaoPorId(relacaoAcaoMecanicaViewModel.CODACOCMC)
                ViewBag.AcaoComercial = Mapper.Map(Of VO.AcaoComercial, AcaoComercialViewModel)(acaoComercial)

                Return View("~/Views/Promocao/Wizard/Wizard.vbhtml", relacaoAcaoMecanicaViewModel)
            Catch ex As Exception
                Danger(ex)
                Return View("~/Views/Promocao/Wizard/Wizard.vbhtml")
            End Try
        End Function

        <Authorize(Roles:="44")>
        <HttpGet()>
        Function WizardCreateFilial(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try
                Dim BO As RelacaoAcaoMecanicaFilialBO = New RelacaoAcaoMecanicaFilialBO()
                Dim list As List(Of VO.RelacaoAcaoMecanicaFilial) = BO.ListFiliaisRelacaoMecanica(CODACOCMC, NUMSEQRGRACOCMC)
                Dim filiaisList As List(Of RelacaoAcaoMecanicaFilialViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaFilial), List(Of RelacaoAcaoMecanicaFilialViewModel))(list)
                Return PartialView("~/Views/Promocao/Wizard/StepFiliais.vbhtml", filiaisList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <Authorize(Roles:="44")>
        <HttpGet()>
        Function WizardCreateGeral(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try
                Dim relacaoAcaoMecanicaBO As New RelacaoAcaoMecanicaBO()
                Dim relacaoAcaoMecanica As VO.RelacaoAcaoMecanica = relacaoAcaoMecanicaBO.SelectT(New VO.RelacaoAcaoMecanica() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

                Dim boBU As New UnidadeEstrategicaDeNegocioBO()
                Dim listaBU As List(Of UnidadeEstrategicaDeNegocioViewModel) = Mapper.Map(Of List(Of VO.UnidadeEstrategicaDeNegocio), List(Of UnidadeEstrategicaDeNegocioViewModel))(boBU.ListT(New VO.UnidadeEstrategicaDeNegocio()))
                ViewBag.listaBU = listaBU

                Dim boTipoClassificacaoCombo As New TipoClassificacaoComboBO()
                ViewBag.listaTipoClassificacaoCombo = boTipoClassificacaoCombo.CarregaDropTipoClassificacaoCombo(Nothing, Nothing)

                Dim BOCriterioApuracao As New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
                Dim CriterioApuracao = BOCriterioApuracao.ListT(New VO.RelacaoAcaoMecanicaCriterioDeApuracao() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

                ViewBag.IsCashBack = CriterioApuracao.Where(Function(a) a.INDTIPPTOPMC = 0).FirstOrDefault() Is Nothing

                Dim promocaoViewModel As New PromocaoViewModel()
                promocaoViewModel.CODACOCMC = relacaoAcaoMecanica.CODACOCMC
                promocaoViewModel.TIPPMC = relacaoAcaoMecanica.TIPPMC
                promocaoViewModel.QDEGRPMIXPMC = relacaoAcaoMecanica.QDEGRPMIXPMC
                promocaoViewModel.QDEFXAPMC = relacaoAcaoMecanica.QDEFXAPMC
                promocaoViewModel.INDPTCTOTMIXPMC = relacaoAcaoMecanica.INDPTCTOTMIXPMC
                promocaoViewModel.INDPMCVNDSMP = 1
                promocaoViewModel.FLGENVPMC = "S"

                Dim boAcaoComercial As New AcaoComercialBO()
                promocaoViewModel.INDEVTSMA = boAcaoComercial.BuscaAcaoPorId(CODACOCMC).INDEVTSMA

                CarregaListasDropDown()

                'promocaoViewModel.des = relacaoAcaoMecanica.DESRGRACOCMC

                Return PartialView("~/Views/Promocao/Wizard/StepGeral.vbhtml", promocaoViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <Authorize(Roles:="44")>
        <HttpPost()>
        Function WizardCreateBrinde(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal Promocao As PromocaoViewModel) As PartialViewResult
            Try

                'Validações da promoção - INICIO
                'BU
                If Promocao.CODUNDESRNGC Is Nothing Then
                    Warning("Informe o BU responsável pela promoção!")
                    Return Nothing
                End If

                'Nome
                If String.IsNullOrWhiteSpace(Promocao.NOMPMC) Then
                    Warning("Informe o Nome da Promoção !")
                    Return Nothing
                End If

                'Data Inicio promoção
                If Promocao.FLGENVPMC <> " " Then
                    If Promocao.DATINIPMC Is Nothing Then
                        Warning("Data de Início da Promoção Inválida!")
                        Return Nothing
                    End If
                    If Promocao.DATINIPMC < Date.Now.Date Then
                        Warning("Data de Início não pode ser menor que a Data Atual!")
                        Return Nothing
                    End If
                End If

                'Data Término da promoção
                If Promocao.DATFIMPMC Is Nothing Then
                    Warning("Data de Término da Promoção Inválida!")
                    Return Nothing
                End If
                If Promocao.DATFIMPMC < Promocao.DATINIPMC Then
                    Warning("Data de Término da Promoção deve ser maior ou igual a Data de Início!")
                    Return Nothing
                End If

                If Promocao.DATFIMPMC < Date.Now.Date Then
                    Warning("Data de Término da Promoção não pode ser menor que o dia atual!")
                    Return Nothing
                End If

                If (Promocao.INDPMCIMD = 1) Then

                    Promocao.HRAINIPMC = New Date(Promocao.DATINIPMC.Value.Year, Promocao.DATINIPMC.Value.Month, Promocao.DATINIPMC.Value.Day, Promocao.HRAINIPMC.Value.Hour, Promocao.HRAINIPMC.Value.Minute, Promocao.HRAINIPMC.Value.Second)
                    Promocao.HRAFIMPMC = New Date(Promocao.DATFIMPMC.Value.Year, Promocao.DATFIMPMC.Value.Month, Promocao.DATFIMPMC.Value.Day, Promocao.HRAFIMPMC.Value.Hour, Promocao.HRAFIMPMC.Value.Minute, Promocao.HRAFIMPMC.Value.Second)

                    If (IsNothing(Promocao.HRAINIPMC) Or IsNothing(Promocao.HRAFIMPMC)) Then
                        Warning("Para Promoção Relâmpago é obrigatório informar Hr. Inicio e Hr. Fim.")
                        Return Nothing
                    End If

                    If (DateDiff(DateInterval.Day, Promocao.DATINIPMC.Value, Promocao.DATFIMPMC.Value) > 3) Then
                        Warning("Para Promoção Relâmpago, o campo Dt. de Fim não pode ter mais de 3 dias de diferença para o campo Dt. de Início.")
                        Return Nothing
                    End If

                    If (Promocao.HRAINIPMC >= Promocao.HRAFIMPMC) Then
                        Warning("Hr. Fim precisa ser maior que Hr. Inicio.")
                        Return Nothing
                    End If
                End If
                'Validações da promoção - FIM

                'Validação equipe de vendas 
                If Promocao.TIPEQIVND Is Nothing Then
                    Warning("Selecione a equipe de vendas!")
                    Return Nothing
                End If


                '' Carrega os campos do step Brindes
                Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
                Dim relacaoAcaoMecanica As VO.RelacaoAcaoMecanica = BO.SelectEditMecanicaGerais(CODACOCMC, NUMSEQRGRACOCMC)

                If Promocao.DATFIMPMC > relacaoAcaoMecanica.DATFIMACOCMC Then
                    Warning("Data de Término da Promoção não pode ser maior que data de término da ação!")
                    Return Nothing
                End If

                ViewBag.IndCobrancaBrinde = Promocao.INDCOBBDEPMCBool
                ViewBag.BrindeAdicional = Promocao.INDISRBDEADIBool

                ViewBag.listaFaixa = New List(Of SelectListItem)()
                Dim relacaoAcaoMecanicaCriterioDeApuracaoBO As New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
                Dim faixas = relacaoAcaoMecanicaCriterioDeApuracaoBO.ListT(New VO.RelacaoAcaoMecanicaCriterioDeApuracao() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})
                For Each item In faixas
                    If (item.TIPEQCCALCTRPMC = 2) Then '2 = FAIXAS FINITAS
                        ViewBag.listaFaixa.Add(New SelectListItem() With {.Text = item.NUMSEQCTRACOCMC, .Value = item.NUMSEQCTRACOCMC})
                    End If
                Next

                Dim promocaoBrindeViewModel As New PromocaoBrindeViewModel()
                promocaoBrindeViewModel.PERMERRSVPRMPMC = 100

                Return PartialView("~/Views/Promocao/Wizard/StepBrinde.vbhtml", promocaoBrindeViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function



        <HttpGet()>
        Function ConsultaValoreBrinde(ByVal CODACOCMC As Decimal, ByVal CODMER As Decimal) As Object
            Try
                Dim lstMer As List(Of Decimal) = New List(Of Decimal)
                lstMer.Add(CODMER)

                Dim mercadoriaBO As New MercadoriaBO
                Dim promocaoMixBO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()

                Dim lstCusto = promocaoMixBO.SelectValoresUnitariosBrutoDeVendaDaMercadoriaCusto(CODACOCMC, lstMer)

                Dim lstValorUnitarioVendaMer = promocaoMixBO.SelectValoresUnitariosBrutoDeVendaDaMercadoria(0, lstMer)
                Dim lstValorUnitarioVendaMer2 = promocaoMixBO.SelectValoresUnitariosBrutoDeVendaDaMercadoria2(0, lstMer)

                For Each itemValor As VO.PromocaoMixVisualizarVigente In lstValorUnitarioVendaMer2
                    If (lstValorUnitarioVendaMer.Where(Function(obj) obj.CODMER = itemValor.CODMER).ToList().Count = 0) Then
                        lstValorUnitarioVendaMer.Add(itemValor)
                    End If
                Next

                If (lstCusto.Count > 0) Then
                    If (lstValorUnitarioVendaMer.Count > 0) Then
                        'VALOR MEDIO
                        Return Json(New With {Key .Custo = lstCusto(0).VLRUNTPCOBRT, .ValorMedio = lstValorUnitarioVendaMer(0).VLRUNTPCOBRT}, JsonRequestBehavior.AllowGet)
                    Else
                        Return Json(New With {Key .Custo = -1}, JsonRequestBehavior.AllowGet)
                    End If
                Else
                    Return Json(New With {Key .Custo = -1}, JsonRequestBehavior.AllowGet)
                End If

            Catch ex As Exception
                Danger(ex)
                Return Json(New With {Key .Custo = -1}, JsonRequestBehavior.AllowGet)
            End Try

        End Function


        <Authorize(Roles:="44")>
        <HttpPost()>
        Function WizardAddBrinde(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal Brinde As PromocaoBrindeViewModel) As JsonResult
            Try

                If (String.IsNullOrEmpty(Brinde.DESPRMPMC) Or Brinde.CODPRMPMC Is Nothing Or Brinde.CODPRMPMC = 0) Then
                    Warning("Informe o Brinde")
                    Return Nothing
                End If

                If Brinde.QDEBDEMPLCTRPMC.GetValueOrDefault() = 0 Then
                    Warning("Informe a quantidade a ser inserida.")
                    Return Nothing
                End If

                If (Brinde.PERMERRSVPRMPMC <= 0) Then
                    Warning("Percentual de reserva tem que ser maior que zero")
                    Return Nothing
                End If
                If (Brinde.PERMERRSVPRMPMC > 100) Then
                    Warning("Percentual de reserva tem que ser menor que 100")
                    Return Nothing
                End If

                Dim acaoComercialBO As New AcaoComercialBO()

                Dim acaoComercialObj As VO.AcaoComercial = acaoComercialBO.SelectAcaoComercialParaPromocao(New VO.AcaoComercial() With {.CODACOCMC = CODACOCMC})

                If (acaoComercialObj IsNot Nothing) Then
                    If (acaoComercialObj.TIPACOMCD IsNot Nothing) Then
                        If (acaoComercialObj.TIPACOMCD = 5 Or acaoComercialObj.TIPACOMCD = 6) Then
                            Dim acaoComercialFunding As List(Of VO.AcaoComercial) = acaoComercialBO.SelectValorFunding(New VO.AcaoComercial() With {.CODACOCMC = CODACOCMC, .CODMER = Brinde.CODPRMPMC})

                            If (acaoComercialFunding Is Nothing) Then
                                Throw New Exception("O item não é principal nem substituto nesta ação comercial.")
                            End If

                            If (acaoComercialFunding.Count = 0) Then
                                Throw New Exception("O item não é principal nem substituto nesta ação comercial.")
                            End If
                        End If
                    End If
                Else
                    Throw New Exception("Ação comercial não encontrada.")
                End If

                Brinde.QDEMERMAXPRMPMC = 0
                Brinde.QDEMERVNDPRMPMC = 0
                Brinde.SALDO = Brinde.QDEMERMAXPRMPMC - Brinde.QDEMERVNDPRMPMC
                Brinde.MEDIA = 0
                Brinde.VLRPRMPMC = 1
                Brinde.FLGTRSMSGFIMPRMPMC = "S"
                Brinde.CODCNDPGT = 1
                Brinde.VLRUNTVNDPRMPMC = 1
                Brinde.INDACUBDEPMCFAT = 1

                If (Brinde.TIPEDENIVPRMPMC Is Nothing) Then
                    Brinde.TIPEDENIVPRMPMC = 0
                End If

                Return Json(Brinde)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <Authorize(Roles:="44")>
        <HttpPost()>
        Function WizardCreateComunicacao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal Promocao As PromocaoViewModel) As PartialViewResult
            Try
                Dim BOCriterioApuracao As New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
                Dim CriterioApuracao = BOCriterioApuracao.ListT(New VO.RelacaoAcaoMecanicaCriterioDeApuracao() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

                ViewBag.IsCashBack = CriterioApuracao.Where(Function(a) a.INDTIPPTOPMC = 0).FirstOrDefault() Is Nothing

                If (ViewBag.IsCashBack) Then
                    If (Promocao.INDPMCIMD = 1) Then

                        If (IsNothing(Promocao.HRAINIPMC) Or IsNothing(Promocao.HRAFIMPMC)) Then
                            Warning("Para Promoção Relâmpago é obrigatório informar Hr. Inicio e Hr. Fim.")
                            Return Nothing
                        End If

                        Promocao.HRAINIPMC = New Date(Promocao.DATINIPMC.Value.Year, Promocao.DATINIPMC.Value.Month, Promocao.DATINIPMC.Value.Day, Promocao.HRAINIPMC.Value.Hour, Promocao.HRAINIPMC.Value.Minute, Promocao.HRAINIPMC.Value.Second)
                        Promocao.HRAFIMPMC = New Date(Promocao.DATFIMPMC.Value.Year, Promocao.DATFIMPMC.Value.Month, Promocao.DATFIMPMC.Value.Day, Promocao.HRAFIMPMC.Value.Hour, Promocao.HRAFIMPMC.Value.Minute, Promocao.HRAFIMPMC.Value.Second)

                        If (DateDiff(DateInterval.Day, Promocao.DATINIPMC.Value, Promocao.DATFIMPMC.Value) > 3) Then
                            Warning("Para Promoção Relâmpago, o campo Dt. de Fim não pode ter mais de 3 dias de diferença para o campo Dt. de Início.")
                            Return Nothing
                        End If

                        If (Promocao.HRAINIPMC >= Promocao.HRAFIMPMC) Then
                            Warning("Hr. Fim precisa ser maior que Hr. Inicio.")
                            Return Nothing
                        End If
                    End If
                End If

                Dim promocaoComunicacaoBO As New PromocaoComunicacaoBO
                Dim retorno As New PromocaoComunicacaoSaveViewModel()
                retorno.RelacaoComunicacao = Mapper.Map(Of List(Of VO.PromocaoComunicacao), List(Of PromocaoComunicacaoViewModel))(promocaoComunicacaoBO.GetRelPromocaoComunicacao(0))

                ViewBag.OpcoesComunicacao = promocaoComunicacaoBO.ObterOpcoesPromocaoComunicacao()

                Return PartialView("~/Views/Promocao/Wizard/StepComunicacao.vbhtml", retorno)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        <Authorize(Roles:="44")>
        Function WizardCreatePromocao(ByVal CODACOCMC As Decimal _
                        , ByVal NUMSEQRGRACOCMC As Decimal _
                        , ByVal filiais As List(Of Decimal) _
                        , ByVal promocao As PromocaoViewModel _
                        , ByVal brindes As List(Of PromocaoBrindeViewModel) _
                        , ByVal comunicacao As List(Of PromocaoComunicacaoViewModel)
                ) As PartialViewResult
            Try

                Dim promocaoVO As VO.Promocao = Mapper.Map(Of PromocaoViewModel, VO.Promocao)(promocao)
                Dim brindesVO As List(Of VO.PromocaoBrinde) = Mapper.Map(Of List(Of PromocaoBrindeViewModel), List(Of VO.PromocaoBrinde))(brindes)
                Dim comunicacaoVO As List(Of VO.PromocaoComunicacao) = Mapper.Map(Of List(Of PromocaoComunicacaoViewModel), List(Of VO.PromocaoComunicacao))(comunicacao)

                Dim criterioAdicionalAvaliacaoPromocaoVO As New VO.CriterioAdicionalAvaliacaoPromocao() With {
                    .FTRACRCTRAVLPMC = promocao.FTRACRCTRAVLPMC,
                    .VLRLIMMAXCTRAVLPMC = promocao.VLRLIMMAXCTRAVLPMC,
                    .QDEMNMITEPEDPMC = promocao.QDEMNMITEPEDPMC
                }

                Dim boAcaoComercial As New AcaoComercialBO()
                Dim acao As VO.AcaoComercial = boAcaoComercial.BuscaAcaoPorId(CODACOCMC)
                promocaoVO.INDEVTSMA = acao.INDEVTSMA
                promocaoVO.CODEVTBTB = acao.CODEVTBTB

                If (promocaoVO.INDPMCIMD = 1 And promocaoVO.HRAINIPMC IsNot Nothing And promocaoVO.HRAFIMPMC IsNot Nothing) Then
                    promocaoVO.HRAINIPMC = New Date(promocaoVO.DATINIPMC.Value.Year, promocaoVO.DATINIPMC.Value.Month, promocaoVO.DATINIPMC.Value.Day, promocaoVO.HRAINIPMC.Value.Hour, promocaoVO.HRAINIPMC.Value.Minute, promocaoVO.HRAINIPMC.Value.Second)
                    promocaoVO.HRAFIMPMC = New Date(promocaoVO.DATFIMPMC.Value.Year, promocaoVO.DATFIMPMC.Value.Month, promocaoVO.DATFIMPMC.Value.Day, promocaoVO.HRAFIMPMC.Value.Hour, promocaoVO.HRAFIMPMC.Value.Minute, promocaoVO.HRAFIMPMC.Value.Second)
                Else
                    promocaoVO.HRAINIPMC = Nothing
                    promocaoVO.HRAFIMPMC = Nothing
                End If

                Dim promocaoBO As New PromocaoBO()
                Dim promocaoList As List(Of VO.Promocao) = promocaoBO.GerarPromocaoMecanica(CODACOCMC, NUMSEQRGRACOCMC, filiais, promocaoVO, criterioAdicionalAvaliacaoPromocaoVO, brindesVO, comunicacaoVO)



                'Dim promocaoListViewModel As List(Of PromocaoViewModel) = Mapper.Map(Of List(Of VO.Promocao), List(Of PromocaoViewModel))(promocaoList)
                'Dim BO As RelacaoAcaoMecanicaFilialBO = New RelacaoAcaoMecanicaFilialBO()
                'Dim filiaisPromocao As List(Of VO.RelacaoAcaoMecanicaFilial) = BO.ListFiliaisRelacaoMecanica(CODACOCMC, NUMSEQRGRACOCMC)
                'ViewBag.FiliaisPendentes = (filiaisPromocao.Where(Function(f) f.CODPMC Is Nothing).Count() > 0)

                'Return PartialView("~/Views/Promocao/Wizard/StepResumo.vbhtml", promocaoListViewModel)


                Return RefreshStepResumo(CODACOCMC, NUMSEQRGRACOCMC)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        <HttpPost()>
        <Authorize(Roles:="44")>
        Function RefreshStepResumo(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try
                Dim promocaoBO As New PromocaoBO()
                Dim promocaoList As List(Of VO.Promocao) = promocaoBO.ListarPromocaoPorAcaoMecanica(CODACOCMC, NUMSEQRGRACOCMC)
                Dim promocaoListViewModel As List(Of PromocaoViewModel) = Mapper.Map(Of List(Of VO.Promocao), List(Of PromocaoViewModel))(promocaoList)

                Dim BO As RelacaoAcaoMecanicaFilialBO = New RelacaoAcaoMecanicaFilialBO()
                Dim filiaisPromocao As List(Of VO.RelacaoAcaoMecanicaFilial) = BO.ListFiliaisRelacaoMecanica(CODACOCMC, NUMSEQRGRACOCMC)
                ViewBag.FiliaisPendentes = (filiaisPromocao.Where(Function(f) f.CODPMC Is Nothing).Count() > 0)

                Return PartialView("~/Views/Promocao/Wizard/StepResumo.vbhtml", promocaoListViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function



        <HttpPost()>
        <Authorize(Roles:="44")>
        Function WizardExcluirFiliais(ByVal CODACOCMC As Decimal _
                        , ByVal NUMSEQRGRACOCMC As Decimal _
                        , ByVal filiais As List(Of Decimal)
                ) As Boolean
            Try

                Dim relacaoAcaoMecanicaFilialBO As New RelacaoAcaoMecanicaFilialBO()
                Dim filiaisAcaoMecanica = relacaoAcaoMecanicaFilialBO.ListFiliaisRelacaoMecanica(CODACOCMC, NUMSEQRGRACOCMC)

                For Each item In filiais
                    Dim filial = filiaisAcaoMecanica.Where(Function(f) f.CODFILEMP = item).FirstOrDefault()
                    If (filial Is Nothing) Then
                        Warning(String.Format("Esta promoção não esta prevista para a filial: {0}", item))
                        Return False
                    End If
                    If (filial.CODPMC IsNot Nothing) Then
                        Warning(String.Format("Já foi gerado a promoção {0} para a filial {1} - {2}", filial.CODPMC, filial.CODFILEMP, filial.DESABVFILEMP))
                        Return False
                    End If
                Next


                Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
                Dim relacaoAcaoMecanica As VO.RelacaoAcaoMecanica = BO.ListPromocoesPendenteGeracao(CODACOCMC, Nothing, Nothing, Nothing, ControleAcesso.CodFnc()).FirstOrDefault()
                If (relacaoAcaoMecanica Is Nothing) Then
                    Warning("Somente o autor da ação comercial pode excluir filiais da mecânica.")
                    Return False
                End If



                For Each item In filiais
                    Dim filial = filiaisAcaoMecanica.Where(Function(f) f.CODFILEMP = item And f.CODACOCMC = CODACOCMC And f.NUMSEQRGRACOCMC = f.NUMSEQRGRACOCMC).FirstOrDefault()
                    'Atualiza a mecanica da ação
                    filial.DATECSUSR = DateTime.Now
                    relacaoAcaoMecanicaFilialBO.UpdateT(filial, False)
                Next



                Return True
            Catch ex As Exception
                Danger(ex)
                Return False
            End Try
        End Function



        <HttpPost()>
        <Authorize(Roles:="44")>
        Function ClonarPromocoesDaAcao(ByVal CODACOCMC As Decimal) As PartialViewResult
            Try
                Dim promocaoBO As New PromocaoBO()

                Dim mensagens As String = String.Empty
                Dim promocaoList As List(Of VO.Promocao) = promocaoBO.ClonarPromocoesDaAcao(CODACOCMC, mensagens)

                If (mensagens <> String.Empty) Then
                    ViewBag.Mensagem = "Não foi possível gerar algumas promoções, verifique as mensagens abaixo: <br/>" + mensagens
                End If

                Dim promocaoListViewModel As List(Of PromocaoViewModel) = Mapper.Map(Of List(Of VO.Promocao), List(Of PromocaoViewModel))(promocaoList)


                Dim filial As New FilialEmpresaCorporativoBO()

                For Each item In promocaoListViewModel
                    item.NOMFILEMP = filial.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = item.CODFILEMP}).NOMFILEMP
                Next

                Return PartialView("~/Views/Promocao/Wizard/ResumoCloneProcoesDaAcao.vbhtml", promocaoListViewModel)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Private Sub CarregaListasDropDown()
            ViewBag.listaNaturezaVenda = New List(Of SelectListItem)()
            ViewBag.listaNaturezaVenda.Add(New SelectListItem() With {.Text = "Ambos", .Value = "0"})
            ViewBag.listaNaturezaVenda.Add(New SelectListItem() With {.Text = "Nativo (Cliente)", .Value = "1"})
            ViewBag.listaNaturezaVenda.Add(New SelectListItem() With {.Text = "Não Nativo (Força de Vendas)", .Value = "2"})
        End Sub

    End Class


End Namespace
