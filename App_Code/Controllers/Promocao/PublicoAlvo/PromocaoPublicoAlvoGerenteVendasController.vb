﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class PromocaoPublicoAlvoGerenteVendasController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoGerenteVendas(ByVal objeto As PromocaoPublicoAlvoGerenteVendasViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = objeto.CODPMC
                'ObjFiltro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim BO = New PromocaoPublicoAlvoGerenteVendasBO()

                objeto.TIPEDEPUBALVPMC = 4
                Dim PromocaoPublicoAlvoGerenteVendasList As List(Of VO.PromocaoPublicoAlvoGerenteVendas) = BO.ListarGerenteVendas(objeto.CODPMC, objeto.TIPEDEPUBALVPMC)
                PromocaoPublicoAlvoGerenteVendasList.ForEach(Sub(obj) obj.NOMGER = obj.NOMGER.TrimEnd())

                Dim objPromocaoPublicoAlvoGerenteVendasList As List(Of PromocaoPublicoAlvoGerenteVendasViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoGerenteVendas), List(Of PromocaoPublicoAlvoGerenteVendasViewModel))(PromocaoPublicoAlvoGerenteVendasList)


                Return PartialView("~/Views/Promocao/PublicoAlvo/_ModalPromocaoGerenteVendas.vbhtml", objPromocaoPublicoAlvoGerenteVendasList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal obj As List(Of PromocaoPublicoAlvoGerenteVendasViewModel)) As PartialViewResult
            Dim BO As PromocaoPublicoAlvoGerenteVendasBO = New PromocaoPublicoAlvoGerenteVendasBO()
            Try
                If (ModelState.IsValid) Then
                    Dim PromocaoPublicoAlvoGerenteVendasList = Mapper.Map(Of List(Of PromocaoPublicoAlvoGerenteVendasViewModel), List(Of VO.PromocaoPublicoAlvoGerenteVendas))(obj)

                    BO.InsertList(CODPMC, TIPEDEPUBALVPMC, PromocaoPublicoAlvoGerenteVendasList)

                    Return Nothing
                End If


                Throw New WebFaultException(HttpStatusCode.BadRequest)
                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function


        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            Dim PermiteEdicao As Boolean = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

            If (PermiteEdicao) Then
                Dim authCookie As HttpCookie = Request.RequestContext.HttpContext.Request.Cookies(FormsAuthentication.FormsCookieName)
                If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                    Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                    Dim roles As String() = Split(authTicket.UserData, "|")(1).Split(";"c)
                    If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
                        PermiteEdicao = True
                    Else
                        PermiteEdicao = False
                    End If
                End If
            End If

            ViewBag.SituacaoPromocao = obj.DESSITPMC
            ViewBag.PermiteEdicaoPublicoAlvoPromocaoGerenteVendas = PermiteEdicao
        End Sub
    End Class
End Namespace
