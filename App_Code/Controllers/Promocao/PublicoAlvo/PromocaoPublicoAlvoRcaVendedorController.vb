﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class PromocaoPublicoAlvoRcaVendedorController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoRcaVendedor(ByVal objeto As PromocaoPublicoAlvoClienteViewModel) As PartialViewResult
            Try

                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = objeto.CODPMC
                'ObjFiltro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim obj As PromocaoPublicoAlvoRcaVendedorViewModel = New PromocaoPublicoAlvoRcaVendedorViewModel()

                Return PartialView("~/Views/Promocao/PublicoAlvo/RcaVendedor/_ModalPromocaoRcaVendedor.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function EditMecanicaPromocaoWidgetLoadPesquisaRcaVendedor(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As PartialViewResult
            Try
                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of PromocaoPublicoAlvoRcaVendedorViewModel)
                gridConf.Filter = New PromocaoPublicoAlvoRcaVendedorViewModel()
                gridConf.Filter.CODPMC = CODPMC
                gridConf.Filter.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                gridConf.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaRcaVendedor"
                gridConf.ResultID = "divGridPesquisaPromocaoPublicoAlvoRcaVendedor"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMREPC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of PromocaoPublicoAlvoRcaVendedorViewModel), gridConf.Page)

                Return PartialView("~/Views/Promocao/PublicoAlvo/RcaVendedor/_WidgetLoadPesquisaPromocaoRcaVendedor.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMecanicaPromocaoWidgetLoadPesquisaRcaVendedor(dadosPesquisa As GridSettings(Of PromocaoPublicoAlvoRcaVendedorViewModel)) As PartialViewResult
            Try
                Dim BO = New PromocaoPublicoAlvoRcaVendedorBO()

                Dim filtro As VO.PromocaoPublicoAlvoRcaVendedor = Mapper.Map(Of PromocaoPublicoAlvoRcaVendedorViewModel, VO.PromocaoPublicoAlvoRcaVendedor)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.PromocaoPublicoAlvoRcaVendedor) = BO.BuscarListaRcaVendedor(filtro)
                Dim viewModeList As List(Of PromocaoPublicoAlvoRcaVendedorViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoRcaVendedor), List(Of PromocaoPublicoAlvoRcaVendedorViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoPublicoAlvoRcaVendedorViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "divGridPesquisaPromocaoPublicoAlvoRcaVendedor"
                dadosPesquisa.PaginationID = "divGridPesquisaPromocaoPublicoAlvoRcaVendedor"
                dadosPesquisa.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaRcaVendedor"
                dadosPesquisa.ControllerName = "PromocaoPublicoAlvoRcaVendedor"

                Return PartialView("~/Views/Promocao/PublicoAlvo/RcaVendedor/_GridPesquisaPromocaoPublicoAlvoRcaVendedor.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function ListarPublicoAlvoRcaVendedor(obj As PromocaoPublicoAlvoRcaVendedorViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = obj.CODPMC
                'ObjFiltro.CODPMC = obj.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim BO = New PromocaoPublicoAlvoRcaVendedorBO()

                Dim objPublicoAlvoRcaVendedor As VO.PromocaoPublicoAlvoRcaVendedor = Mapper.Map(Of PromocaoPublicoAlvoRcaVendedorViewModel, VO.PromocaoPublicoAlvoRcaVendedor)(obj)

                Dim objPublicoAlvoRcaVendedorList As List(Of VO.PromocaoPublicoAlvoRcaVendedor) = BO.ListarPublicoAlvoRcaVendedor(objPublicoAlvoRcaVendedor)

                Dim publicoAlvoRcaVendedorList As List(Of PromocaoPublicoAlvoRcaVendedorViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoRcaVendedor), List(Of PromocaoPublicoAlvoRcaVendedorViewModel))(objPublicoAlvoRcaVendedorList)

                Return PartialView("~/Views/Promocao/PublicoAlvo/RcaVendedor/_WidgetPromocaoPublicoAlvoRcaVendedor.vbhtml", publicoAlvoRcaVendedorList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal obj As PromocaoPublicoAlvoRcaVendedorViewModel) As PartialViewResult
            Dim PromocaoPublicoAlvoRcaVendedorBO = New PromocaoPublicoAlvoRcaVendedorBO()

            If (ModelState.IsValid) Then
                Dim PromocaoPublicoAlvoRcaVendedor = Mapper.Map(Of PromocaoPublicoAlvoRcaVendedorViewModel, VO.PromocaoPublicoAlvoRcaVendedor)(obj)
                'Promocao.DesMecanica = "AI                  "
                PromocaoPublicoAlvoRcaVendedorBO.Insert(PromocaoPublicoAlvoRcaVendedor)
                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                Return Nothing
            End If


            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function

        'Adiciona o resultado da pesquisa.
        <HttpPost()>
        Function AddPesquisa(filtro As PromocaoPublicoAlvoRcaVendedorViewModel) As PartialViewResult
            Try
                Dim filtroVO As VO.PromocaoPublicoAlvoRcaVendedor = Mapper.Map(Of PromocaoPublicoAlvoRcaVendedorViewModel, VO.PromocaoPublicoAlvoRcaVendedor)(filtro)

                Dim BO As PromocaoPublicoAlvoRcaVendedorBO = New PromocaoPublicoAlvoRcaVendedorBO()
                BO.AddPesquisa(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function Delete(ByVal obj As PromocaoPublicoAlvoRcaVendedorViewModel) As PartialViewResult
            Dim PromocaoPublicoAlvoRcaVendedorBO = New PromocaoPublicoAlvoRcaVendedorBO()

            If (ModelState.IsValid) Then
                Dim PromocaoPublicoAlvoRcaVendedor = Mapper.Map(Of PromocaoPublicoAlvoRcaVendedorViewModel, VO.PromocaoPublicoAlvoRcaVendedor)(obj)
                'Promocao.DesMecanica = "AI                  "
                PromocaoPublicoAlvoRcaVendedorBO.Delete(PromocaoPublicoAlvoRcaVendedor)
                Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
                Return Nothing
            End If


            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function

        <HttpPost()>
        Public Function ImportarXlsRcaVendedor(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
            Dim promocaoPublicoAlvoRcaVendedorBO As New PromocaoPublicoAlvoRcaVendedorBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseRcaVendedor"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseRcaVendedor"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = promocaoPublicoAlvoRcaVendedorBO.ImportarExelRcaVendedor(savedFileName, CODPMC, TIPEDEPUBALVPMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseRcaVendedor"
            Else
                Return "TrueRcaVendedor"
            End If
        End Function


        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            Dim PermiteEdicao As Boolean = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

            If (PermiteEdicao) Then
                Dim authCookie As HttpCookie = Request.RequestContext.HttpContext.Request.Cookies(FormsAuthentication.FormsCookieName)
                If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                    Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                    Dim roles As String() = Split(authTicket.UserData, "|")(1).Split(";"c)
                    If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
                        PermiteEdicao = True
                    Else
                        PermiteEdicao = False
                    End If
                End If
            End If

            ViewBag.SituacaoPromocao = obj.DESSITPMC
            ViewBag.PermiteEdicaoPublicoAlvoPromocaoRcaVendedor = PermiteEdicao
        End Sub

    End Class
End Namespace
