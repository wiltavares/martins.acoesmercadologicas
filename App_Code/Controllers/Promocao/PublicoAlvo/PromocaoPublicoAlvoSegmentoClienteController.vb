﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class PromocaoPublicoAlvoSegmentoClienteController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoSegmentoCliente(ByVal objeto As PromocaoPublicoAlvoSegmentoClienteViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = objeto.CODPMC
                'ObjFiltro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)


                Dim BO As PromocaoPublicoAlvoSegmentoClienteBO = New PromocaoPublicoAlvoSegmentoClienteBO()

                objeto.TIPEDEPUBALVPMC = 4
                Dim PromocaoPublicoAlvoSegmentoClienteList As List(Of VO.PromocaoPublicoAlvoSegmentoCliente) = BO.ListaTipoSegmentoCliente(objeto.CODPMC, objeto.TIPEDEPUBALVPMC)
                Dim objPromocaoPublicoAlvoSegmentoClienteList As List(Of PromocaoPublicoAlvoSegmentoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoSegmentoCliente), List(Of PromocaoPublicoAlvoSegmentoClienteViewModel))(PromocaoPublicoAlvoSegmentoClienteList)

                Return PartialView("~/Views/Promocao/PublicoAlvo/_ModalPromocaoSegmentoCliente.vbhtml", objPromocaoPublicoAlvoSegmentoClienteList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal obj As List(Of PromocaoPublicoAlvoSegmentoClienteViewModel)) As PartialViewResult
            Dim BO As PromocaoPublicoAlvoSegmentoClienteBO = New PromocaoPublicoAlvoSegmentoClienteBO()
            Try
                If (ModelState.IsValid) Then
                    Dim PromocaoPublicoAlvoSegmentoClienteList = Mapper.Map(Of List(Of PromocaoPublicoAlvoSegmentoClienteViewModel), List(Of VO.PromocaoPublicoAlvoSegmentoCliente))(obj)

                    BO.InsertList(CODPMC, TIPEDEPUBALVPMC, PromocaoPublicoAlvoSegmentoClienteList)

                    Return Nothing
                End If


                Throw New WebFaultException(HttpStatusCode.BadRequest)
                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function

        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            Dim PermiteEdicao As Boolean = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

            If (PermiteEdicao) Then
                Dim authCookie As HttpCookie = Request.RequestContext.HttpContext.Request.Cookies(FormsAuthentication.FormsCookieName)
                If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                    Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                    Dim roles As String() = Split(authTicket.UserData, "|")(1).Split(";"c)
                    If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
                        PermiteEdicao = True
                    Else
                        PermiteEdicao = False
                    End If
                End If
            End If

            ViewBag.SituacaoPromocao = obj.DESSITPMC
            ViewBag.PermiteEdicaoPublicoAlvoPromocaoSegmentoCliente = PermiteEdicao
        End Sub

    End Class
End Namespace
