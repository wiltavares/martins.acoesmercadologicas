﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class PromocaoPublicoAlvoClienteController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoCliente(ByVal objeto As PromocaoPublicoAlvoClienteViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim obj As PromocaoPublicoAlvoClienteViewModel = New PromocaoPublicoAlvoClienteViewModel()

                Return PartialView("~/Views/Promocao/PublicoAlvo/Cliente/_ModalPromocaoCliente.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function EditMecanicaPromocaoWidgetLoadPesquisaCliente(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As PartialViewResult
            Try

                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of PromocaoPublicoAlvoClienteViewModel)
                gridConf.Filter = New PromocaoPublicoAlvoClienteViewModel()
                gridConf.Filter.CODPMC = CODPMC
                gridConf.Filter.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                gridConf.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaCliente"
                gridConf.ResultID = "divGridItensPesquisaPromocaoPublicoAlvoCliente"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMCLIC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of PromocaoPublicoAlvoClienteViewModel), gridConf.Page)

                Return PartialView("~/Views/Promocao/PublicoAlvo/Cliente/_WidgetLoadPesquisaPromocaoCliente.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMecanicaPromocaoWidgetLoadPesquisaCliente(dadosPesquisa As GridSettings(Of PromocaoPublicoAlvoClienteViewModel)) As PartialViewResult
            Try
                Dim BO = New PromocaoPublicoAlvoClienteBO()

                Dim filtro As VO.PromocaoPublicoAlvoCliente = Mapper.Map(Of PromocaoPublicoAlvoClienteViewModel, VO.PromocaoPublicoAlvoCliente)(dadosPesquisa.Filter)

                If Not ValidaFiltroCliente(dadosPesquisa.Filter) Then
                    Return Nothing
                End If

                Dim voList As List(Of VO.PromocaoPublicoAlvoCliente) = BO.BuscarListaCliente(filtro)
                Dim viewModeList As List(Of PromocaoPublicoAlvoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoCliente), List(Of PromocaoPublicoAlvoClienteViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoPublicoAlvoClienteViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "divGridItensPesquisaPromocaoPublicoAlvoCliente"
                dadosPesquisa.PaginationID = "divGridItensPesquisaPromocaoPublicoAlvoCliente"
                dadosPesquisa.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaCliente"
                dadosPesquisa.ControllerName = "PromocaoPublicoAlvoCliente"

                Return PartialView("~/Views/Promocao/PublicoAlvo/Cliente/_GridPesquisaPromocaoPublicoAlvoCliente.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        Private Function ValidaFiltroCliente(ByRef filtro As PromocaoPublicoAlvoClienteViewModel) As Boolean

            If filtro.CODCLI Is Nothing And filtro.NOMCLI Is Nothing And
                filtro.CODREP Is Nothing And filtro.NOMREP Is Nothing And
                filtro.CODSUP Is Nothing And filtro.NOMSUP Is Nothing Then 'filtro.CODGER Is Nothing And filtro.NOMGER Is Nothing And
                Warning("Informe pelo menos um filtro")
                Return False
            End If

            If Not filtro.NOMCLI Is Nothing Then
                If (filtro.NOMCLI.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do cliente")
                    Return False
                End If
            End If
            If Not filtro.NOMREP Is Nothing Then
                If (filtro.NOMREP.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do representante vendas")
                    Return False
                End If
            End If
            'If Not filtro.NOMGER Is Nothing Then
            '    If (filtro.NOMGER.Length < 3) Then
            '        Warning("Informe pelo menos 3 caracteres no filtro de nome do gerente de vendas")
            '        Return False
            '    End If
            'End If
            If Not filtro.NOMSUP Is Nothing Then
                If (filtro.NOMSUP.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do gerente de mercado")
                    Return False
                End If
            End If

            Return True
        End Function


        <HttpPost()>
        Function ListarPublicoAlvoCliente(obj As PromocaoPublicoAlvoClienteViewModel) As PartialViewResult
            Try

                'Validar Se Dados Pode Ser Alterado
                ViewBag.acao = "Alterar"
                Dim promocaoBO As New PromocaoBO
                Dim promocao = promocaoBO.SelectT(New VO.Promocao() With {.CODPMC = obj.CODPMC})
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)
                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim BO = New PromocaoPublicoAlvoClienteBO()

                Dim objPublicoAlvoCliente As VO.PromocaoPublicoAlvoCliente = Mapper.Map(Of PromocaoPublicoAlvoClienteViewModel, VO.PromocaoPublicoAlvoCliente)(obj)
                'Dim lista = BO.ListT(objPublicoAlvoCliente)


                Dim objPublicoAlvoClienteList As List(Of VO.PromocaoPublicoAlvoCliente) = BO.ListarPublicoAlvoCliente(objPublicoAlvoCliente)

                Dim publicoAlvoClienteList As List(Of PromocaoPublicoAlvoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoCliente), List(Of PromocaoPublicoAlvoClienteViewModel))(objPublicoAlvoClienteList)



                'objeto da pesquisa.
                Dim gridConf = New GridSettings(Of PromocaoPublicoAlvoClienteViewModel)
                gridConf.Filter = obj
                gridConf.ActionName = "PesquisaPublicoAlvoCliente"
                gridConf.ResultID = "divGridPromocaoPublicoAlvoCliente"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMCLIC"
                gridConf.Page.PageSize = 10


                gridConf.ListPaged = PaginarLista(publicoAlvoClienteList, gridConf.Page)
                gridConf.TotalCount = publicoAlvoClienteList.Count


                Return PartialView("~/Views/Promocao/PublicoAlvo/Cliente/_WidgetPromocaoPublicoAlvoCliente.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaPublicoAlvoCliente(dadosPesquisa As GridSettings(Of PromocaoPublicoAlvoClienteViewModel)) As PartialViewResult
            Try


                'Validar Se Dados Pode Ser Alterado
                ViewBag.acao = "Alterar"
                Dim promocaoBO As New PromocaoBO
                Dim promocao = promocaoBO.SelectT(New VO.Promocao() With {.CODPMC = dadosPesquisa.Filter.CODPMC})
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)
                ControlaVisibilidadeComponentes(promocaoViewModel)


                Dim BO = New PromocaoPublicoAlvoClienteBO()
                Dim filtro As VO.PromocaoPublicoAlvoCliente = Mapper.Map(Of PromocaoPublicoAlvoClienteViewModel, VO.PromocaoPublicoAlvoCliente)(dadosPesquisa.Filter)
                Dim voList As List(Of VO.PromocaoPublicoAlvoCliente) = BO.ListarPublicoAlvoCliente(filtro)
                Dim viewModeList As List(Of PromocaoPublicoAlvoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoCliente), List(Of PromocaoPublicoAlvoClienteViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoPublicoAlvoClienteViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.PaginationID = "divGridPromocaoPublicoAlvoCliente"
                dadosPesquisa.ActionName = "PesquisaPublicoAlvoCliente"
                dadosPesquisa.ResultID = "divGridPromocaoPublicoAlvoCliente"
                dadosPesquisa.ControllerName = "PromocaoPublicoAlvoCliente"

                Return PartialView("~/Views/Promocao/PublicoAlvo/Cliente/_GridPromocaoPublicoAlvoCliente.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal obj As PromocaoPublicoAlvoClienteViewModel) As PartialViewResult
            Dim PromocaoPublicoAlvoClienteBO = New PromocaoPublicoAlvoClienteBO()

            If (ModelState.IsValid) Then
                Dim PromocaoPublicoAlvoCliente = Mapper.Map(Of PromocaoPublicoAlvoClienteViewModel, VO.PromocaoPublicoAlvoCliente)(obj)
                'Promocao.DesMecanica = "AI                  "
                PromocaoPublicoAlvoClienteBO.Insert(PromocaoPublicoAlvoCliente)
                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                Return Nothing
            End If

            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function

        'Adiciona o resultado da pesquisa.
        <HttpPost()>
        Function AddPesquisa(filtro As PromocaoPublicoAlvoClienteViewModel) As PartialViewResult
            Try
                If Not ValidaFiltroCliente(filtro) Then
                    Return Nothing
                End If

                Dim filtroVO As VO.PromocaoPublicoAlvoCliente = Mapper.Map(Of PromocaoPublicoAlvoClienteViewModel, VO.PromocaoPublicoAlvoCliente)(filtro)

                Dim BO As PromocaoPublicoAlvoClienteBO = New PromocaoPublicoAlvoClienteBO()
                BO.AddPesquisa(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function Delete(ByVal obj As PromocaoPublicoAlvoClienteViewModel) As PartialViewResult
            Dim PromocaoPublicoAlvoClienteBO = New PromocaoPublicoAlvoClienteBO()

            If (ModelState.IsValid) Then
                Dim PromocaoPublicoAlvoCliente = Mapper.Map(Of PromocaoPublicoAlvoClienteViewModel, VO.PromocaoPublicoAlvoCliente)(obj)
                'Promocao.DesMecanica = "AI                  "
                PromocaoPublicoAlvoClienteBO.Delete(PromocaoPublicoAlvoCliente)
                Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
                Return Nothing
            End If

            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function


        <HttpPost()>
        Public Function ImportarXlsCliente(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
            Dim promocaoPublicoAlvoClienteBO As New PromocaoPublicoAlvoClienteBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseCliente"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseCliente"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = promocaoPublicoAlvoClienteBO.ImportarExelCliente(savedFileName, CODPMC, TIPEDEPUBALVPMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseCliente"
            Else
                Return "TrueCliente"
            End If
        End Function

        '<HttpPost()>
        'Public Function ImportarXlsCliente(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
        '    Try


        '        Dim promocaoPublicoAlvoClienteBO As New PromocaoPublicoAlvoClienteBO()

        '        If (Request.Files.Count <= 0) Then
        '            Warning("Selecione um arquivo para importar!")
        '            Return "FalseCliente"
        '        End If

        '        Dim savedFileName As String = ""
        '        If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
        '            Warning("Não foi possivel abrir o arquivo!")
        '            Return "FalseCliente"
        '        End If
        '        For Each item As String In Request.Files

        '            Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
        '            If (hpf.ContentLength = 0) Then
        '                Continue For
        '            End If

        '            Dim keybAuth As New Renci.SshNet.KeyboardInteractiveAuthenticationMethod(ConfigurationManager.AppSettings("usuarioDatastage"))
        '            AddHandler keybAuth.AuthenticationPrompt, AddressOf HandleKeyEvent

        '            Dim connectionInfo As New Renci.SshNet.ConnectionInfo(ConfigurationManager.AppSettings("urlDatastage"),
        '                                    ConfigurationManager.AppSettings("usuarioDatastage"),
        '                                    keybAuth) ',

        '            Dim filename As String = String.Format("promo_cli_{0}", Date.Now.ToString("yyyyMMddHHmmss"))
        '            Using client As New Renci.SshNet.SftpClient(connectionInfo)
        '                client.Connect()
        '                client.UploadFile(hpf.InputStream, String.Format("/geral/Sistemas/Arquivos_rest/promo_cli/waiting/{0}.xls", filename))


        '                Dim atributes = client.GetAttributes(String.Format("/geral/Sistemas/Arquivos_rest/promo_cli/waiting/{0}.xls", filename))

        '                atributes.OwnerCanExecute = True
        '                atributes.OwnerCanRead = True
        '                atributes.OwnerCanWrite = True
        '                atributes.OthersCanExecute = True
        '                atributes.OthersCanRead = True
        '                atributes.OthersCanWrite = True
        '                atributes.GroupCanExecute = True
        '                atributes.GroupCanRead = True
        '                atributes.GroupCanWrite = True

        '                client.SetAttributes(String.Format("/geral/Sistemas/Arquivos_rest/promo_cli/waiting/{0}.xls", filename), atributes)
        '            End Using

        '            Dim codfnc As String = ControleAcesso.CodFnc()
        '            Dim nomacsusrsis As String = System.Web.HttpContext.Current.User.Identity.Name

        '            Dim Url = String.Format("https://{0}:9446/wisd-rest2/ProcessaArquivo/Tabela/RLCPMCCLI?codpmc={1}&tipedepubalvpmc={2}&nomarqrcb={3}&nomacsusrsis={4}&codfnc={5}&datslc={6}%20{7}", ConfigurationManager.AppSettings("urlDatastage"), CODPMC, TIPEDEPUBALVPMC, filename, nomacsusrsis, codfnc, Date.Now.ToString("yyyy-MM-dd"), Date.Now.ToString("HH:mm:ss"))


        '            Dim httpClient As New System.Net.Http.HttpClient()
        '            Dim responsex = httpClient.GetAsync(Url)
        '            responsex.Wait()


        '            Dim log As New LogDataStageBO()
        '            Dim logErros = log.ListaParaGrid(New VO.LogDataStage() With {.NOMARQRCB = filename, .FLGSTAPCS = 0})

        '            Dim strLog As New StringBuilder
        '            If (logErros.Count > 0) Then
        '                strLog.AppendLine("As seguintes linhas não foram importadas:")
        '                For Each linha In logErros
        '                    strLog.Append(linha.CODLNHARQDTS.Substring(0, linha.CODLNHARQDTS.IndexOf("#"))).Append(" Motivo: ").Append(linha.DESMTVSTA).AppendLine()
        '                Next
        '            End If

        '            If (responsex.Result.IsSuccessStatusCode) Then
        '                Dim Content = responsex.Result.Content.ReadAsStringAsync()
        '                Content.Wait()

        '                'Dim res = Content.Result
        '                If (logErros.Count = 0) Then
        '                    AddAlert(AlertStyle.Success, "Importação concluida com sucesso", Nothing, True, False)
        '                Else
        '                    AddAlert(AlertStyle.Success, "Importação concluida com sucesso porem alguns algumas linhas não foram importadas, verifique", Nothing, True, False)
        '                    AddAlert(AlertStyle.Warning, strLog.ToString().Replace(vbCrLf, "<br/>"), Nothing, True, False)
        '                End If

        '                Return "TrueCliente"
        '            Else
        '                Dim Content = responsex.Result.Content.ReadAsStringAsync()
        '                Content.Wait()

        '                AddAlert(AlertStyle.Danger, "Importação concluida com erro, verifique se o arquivo esta conforme o template", Nothing, True, False)

        '                If (logErros.Count > 0) Then
        '                    AddAlert(AlertStyle.Warning, strLog.ToString().Replace(vbCrLf, "<br/>"), Nothing, True, False)
        '                End If
        '                Return "FalseCliente"
        '            End If
        '        Next

        '        Return "FalseCliente"
        '    Catch ex As Exception
        '        Danger(ex)
        '        Return "FalseCliente"
        '    End Try
        'End Function
        Sub HandleKeyEvent(ByVal sender As Object, ByVal e As Renci.SshNet.Common.AuthenticationPromptEventArgs)
            For Each prompt As Renci.SshNet.Common.AuthenticationPrompt In e.Prompts
                If prompt.Request.IndexOf("Password:", StringComparison.InvariantCultureIgnoreCase) <> -1 Then
                    prompt.Response = ConfigurationManager.AppSettings("senhaDatastage")
                End If
            Next
        End Sub



        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            Dim PermiteEdicao As Boolean = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

            If (PermiteEdicao) Then
                Dim authCookie As HttpCookie = Request.RequestContext.HttpContext.Request.Cookies(FormsAuthentication.FormsCookieName)
                If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                    Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                    Dim roles As String() = Split(authTicket.UserData, "|")(1).Split(";"c)
                    If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
                        PermiteEdicao = True
                    Else
                        PermiteEdicao = False
                    End If
                End If
            End If

            ViewBag.SituacaoPromocao = obj.DESSITPMC
            ViewBag.PermiteEdicaoPublicoAlvoPromocaoCliente = PermiteEdicao
        End Sub


    End Class

End Namespace
