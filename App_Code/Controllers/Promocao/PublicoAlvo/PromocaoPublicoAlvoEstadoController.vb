﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class PromocaoPublicoAlvoEstadoController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditPromocaoPublicoAlvoEstadoUniao(ByVal objeto As PromocaoPublicoAlvoEstadoViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim BO = New PromocaoPublicoAlvoEstadoBO()

                Dim CODPMC = objeto.CODPMC
                Dim TIPEDEPUBALVPMC = 4

                Dim PromocaoPublicoAlvoEstadoList As List(Of VO.PromocaoPublicoAlvoEstado) = BO.ListarEstadoUniao(CODPMC, TIPEDEPUBALVPMC)

                Dim objPromocaoPublicoAlvoEstadoList As List(Of PromocaoPublicoAlvoEstadoViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoEstado), List(Of PromocaoPublicoAlvoEstadoViewModel))(PromocaoPublicoAlvoEstadoList)

                Return PartialView("~/Views/Promocao/PublicoAlvo/_ModalPromocaoEstadoUni.vbhtml", objPromocaoPublicoAlvoEstadoList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal obj As List(Of PromocaoPublicoAlvoEstadoViewModel)) As ActionResult
            Dim PromocaoPublicoAlvoEstadoBO = New PromocaoPublicoAlvoEstadoBO()
            Try
                Dim BO As PromocaoPublicoAlvoEstadoBO = New PromocaoPublicoAlvoEstadoBO()
                If (ModelState.IsValid) Then
                    Dim PromocaoPublicoAlvoEstado = Mapper.Map(Of List(Of PromocaoPublicoAlvoEstadoViewModel), List(Of VO.PromocaoPublicoAlvoEstado))(obj)

                    BO.InsertList(CODPMC, 4, PromocaoPublicoAlvoEstado)

                    Return Nothing
                End If

                Throw New WebFaultException(HttpStatusCode.BadRequest)
                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function

        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            Dim PermiteEdicao As Boolean = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

            If (PermiteEdicao) Then
                Dim authCookie As HttpCookie = Request.RequestContext.HttpContext.Request.Cookies(FormsAuthentication.FormsCookieName)
                If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                    Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                    Dim roles As String() = Split(authTicket.UserData, "|")(1).Split(";"c)
                    If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
                        PermiteEdicao = True
                    Else
                        PermiteEdicao = False
                    End If
                End If
            End If

            ViewBag.SituacaoPromocao = obj.DESSITPMC
            ViewBag.PermiteEdicaoPublicoAlvoPromocaoEstadoUni = PermiteEdicao
        End Sub


    End Class
End Namespace

