﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class PromocaoPublicoAlvoGrupamentoClienteController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoGrupamentoCliente(ByVal objeto As PromocaoPublicoAlvoGrupamentoClienteViewModel) As PartialViewResult
            Try

                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim BO = New PromocaoPublicoAlvoGrupamentoClienteBO()

                objeto.TIPEDEPUBALVPMC = 4
                Dim PromocaoPublicoAlvoGrupamentoClienteList As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente) = BO.ListarGrupamentoCliente(objeto.CODPMC, objeto.TIPEDEPUBALVPMC)
                Dim objPromocaoPublicoAlvoGrupamentoClienteList As List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoGrupamentoCliente), List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel))(PromocaoPublicoAlvoGrupamentoClienteList)

                Dim tipoGrupoClienteList As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente) = BO.BuscarTipoGrupamentoCliente()
                Dim objTipoGrupoClienteList As List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoGrupamentoCliente), List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel))(tipoGrupoClienteList)
                ViewBag.TipoGrupoCliente = objTipoGrupoClienteList

                Dim dadosPesquisa As GridSettings(Of PromocaoPublicoAlvoGrupamentoClienteViewModel) = New GridSettings(Of PromocaoPublicoAlvoGrupamentoClienteViewModel)()

                dadosPesquisa.Filter = New PromocaoPublicoAlvoGrupamentoClienteViewModel()
                dadosPesquisa.Filter.CODPMC = objeto.CODPMC
                dadosPesquisa.Filter.TIPEDEPUBALVPMC = objeto.TIPEDEPUBALVPMC

                dadosPesquisa.Page = New PageSettings()
                dadosPesquisa.Page.OrderBy = "CODGRPCLIC"
                dadosPesquisa.Page.PageSize = objPromocaoPublicoAlvoGrupamentoClienteList.Count
                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoPublicoAlvoGrupamentoClienteViewModel)(objPromocaoPublicoAlvoGrupamentoClienteList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = objPromocaoPublicoAlvoGrupamentoClienteList.Count
                dadosPesquisa.ResultID = "divModalPromocaoGrupamentoCliente"
                dadosPesquisa.ActionName = "PesquisaMecanicaPromocaoPublicoAlvoGrupamentoCliente"


                Return PartialView("~/Views/Promocao/PublicoAlvo/_ModalPromocaoGrupamentoCliente.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Public Function PesquisaMecanicaPromocaoPublicoAlvoGrupamentoCliente(dadosPesquisa As GridSettings(Of PromocaoPublicoAlvoGrupamentoClienteViewModel)) As PartialViewResult
            Try

                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = dadosPesquisa.Filter.CODPMC
                'ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim BO = New PromocaoPublicoAlvoGrupamentoClienteBO()

                'objeto.TIPEDEPUBALVPMC = 4
                Dim PromocaoPublicoAlvoGrupamentoClienteList As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente) = BO.ListarGrupamentoCliente(dadosPesquisa.Filter.CODPMC, dadosPesquisa.Filter.TIPEDEPUBALVPMC)
                Dim objPromocaoPublicoAlvoGrupamentoClienteList As List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoGrupamentoCliente), List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel))(PromocaoPublicoAlvoGrupamentoClienteList)

                Dim tipoGrupoClienteList As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente) = BO.BuscarTipoGrupamentoCliente()
                Dim objTipoGrupoClienteList As List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoGrupamentoCliente), List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel))(tipoGrupoClienteList)
                ViewBag.TipoGrupoCliente = objTipoGrupoClienteList

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoPublicoAlvoGrupamentoClienteViewModel)(objPromocaoPublicoAlvoGrupamentoClienteList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = objPromocaoPublicoAlvoGrupamentoClienteList.Count
                dadosPesquisa.ResultID = "divModalPromocaoGrupamentoCliente"
                dadosPesquisa.PaginationID = "divModalPromocaoGrupamentoCliente"
                dadosPesquisa.ActionName = "PesquisaMecanicaPromocaoPublicoAlvoGrupamentoCliente"
                dadosPesquisa.ControllerName = "PromocaoPublicoAlvoGrupamentoCliente"


                Return PartialView("~/Views/Promocao/PublicoAlvo/_GridPromocaoGrupamentoCliente.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal obj As List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel)) As ActionResult
            Dim PromocaoPublicoAlvoGrupamentoClienteBO = New PromocaoPublicoAlvoGrupamentoClienteBO()
            Try
                Dim BO As PromocaoPublicoAlvoGrupamentoClienteBO = New PromocaoPublicoAlvoGrupamentoClienteBO()
                If (ModelState.IsValid) Then
                    Dim PromocaoPublicoAlvoGrupamentoClienteList = Mapper.Map(Of List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel), List(Of VO.PromocaoPublicoAlvoGrupamentoCliente))(obj)

                    BO.InsertList(CODPMC, TIPEDEPUBALVPMC, PromocaoPublicoAlvoGrupamentoClienteList)

                    Return Nothing
                End If

                Throw New WebFaultException(HttpStatusCode.BadRequest)
                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function

        <HttpPost>
        Function BuscarGrupamentoClientePorBU(ByVal obj As List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel)) As JsonResult

            Dim BO As PromocaoPublicoAlvoGrupamentoClienteBO = New PromocaoPublicoAlvoGrupamentoClienteBO()

            Dim objeto As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente) = Mapper.Map(Of List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel), List(Of VO.PromocaoPublicoAlvoGrupamentoCliente))(obj)

            Dim list As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente) = BO.BuscarGrupamentoClientePorBU(objeto)
            Dim listViewModel As List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoGrupamentoCliente), List(Of PromocaoPublicoAlvoGrupamentoClienteViewModel))(list)

            Return Json(listViewModel, JsonRequestBehavior.AllowGet)
        End Function

        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            Dim PermiteEdicao As Boolean = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

            If (PermiteEdicao) Then
                Dim authCookie As HttpCookie = Request.RequestContext.HttpContext.Request.Cookies(FormsAuthentication.FormsCookieName)
                If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                    Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                    Dim roles As String() = Split(authTicket.UserData, "|")(1).Split(";"c)
                    If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
                        PermiteEdicao = True
                    Else
                        PermiteEdicao = False
                    End If
                End If
            End If

            ViewBag.SituacaoPromocao = obj.DESSITPMC
            ViewBag.PermiteEdicaoPublicoAlvoPromocaoGrupamentoCliente = PermiteEdicao
        End Sub

    End Class
End Namespace
