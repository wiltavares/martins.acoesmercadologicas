﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class PromocaoPublicoAlvoGerenteMercadoController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoGerenteMercado(ByVal objeto As PromocaoPublicoAlvoClienteViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = objeto.CODPMC
                'ObjFiltro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim eventoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(eventoViewModel)

                Dim obj As PromocaoPublicoAlvoGerenteMercadoViewModel = New PromocaoPublicoAlvoGerenteMercadoViewModel()

                Return PartialView("~/Views/Promocao/PublicoAlvo/GerenteMercado/_ModalPromocaoGerenteMercado.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function EditMecanicaPromocaoWidgetLoadPesquisaGerenteMercado(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As PartialViewResult
            Try
                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of PromocaoPublicoAlvoGerenteMercadoViewModel)
                gridConf.Filter = New PromocaoPublicoAlvoGerenteMercadoViewModel()
                gridConf.Filter.CODPMC = CODPMC
                gridConf.Filter.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                gridConf.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaGerenteMercado"
                gridConf.ResultID = "divGridPesquisaPromocaoPublicoAlvoGerenteMercado"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMSUPC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of PromocaoPublicoAlvoGerenteMercadoViewModel), gridConf.Page)

                Return PartialView("~/Views/Promocao/PublicoAlvo/GerenteMercado/_WidgetLoadPesquisaPromocaoGerenteMercado.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMecanicaPromocaoWidgetLoadPesquisaGerenteMercado(dadosPesquisa As GridSettings(Of PromocaoPublicoAlvoGerenteMercadoViewModel)) As PartialViewResult
            Try
                Dim BO = New PromocaoPublicoAlvoGerenteMercadoBO()

                Dim filtro As VO.PromocaoPublicoAlvoGerenteMercado = Mapper.Map(Of PromocaoPublicoAlvoGerenteMercadoViewModel, VO.PromocaoPublicoAlvoGerenteMercado)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.PromocaoPublicoAlvoGerenteMercado) = BO.BuscarListaGerenteMercado(filtro)
                Dim viewModeList As List(Of PromocaoPublicoAlvoGerenteMercadoViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoGerenteMercado), List(Of PromocaoPublicoAlvoGerenteMercadoViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of PromocaoPublicoAlvoGerenteMercadoViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "divGridPesquisaPromocaoPublicoAlvoGerenteMercado"
                dadosPesquisa.PaginationID = "divGridPesquisaPromocaoPublicoAlvoGerenteMercado"
                dadosPesquisa.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaGerenteMercado"
                dadosPesquisa.ControllerName = "PromocaoPublicoAlvoGerenteMercado"

                Return PartialView("~/Views/Promocao/PublicoAlvo/GerenteMercado/_GridPesquisaPromocaoPublicoAlvoGerenteMercado.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function ListarPublicoAlvoGerenteMercado(obj As PromocaoPublicoAlvoGerenteMercadoViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = obj.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim BO = New PromocaoPublicoAlvoGerenteMercadoBO()

                Dim objPublicoAlvoGerenteMercado As VO.PromocaoPublicoAlvoGerenteMercado = Mapper.Map(Of PromocaoPublicoAlvoGerenteMercadoViewModel, VO.PromocaoPublicoAlvoGerenteMercado)(obj)

                Dim objPublicoAlvoGerenteMercadoList As List(Of VO.PromocaoPublicoAlvoGerenteMercado) = BO.ListarPublicoAlvoGerenteMercado(objPublicoAlvoGerenteMercado)

                Dim publicoAlvoGerenteMercadoList As List(Of PromocaoPublicoAlvoGerenteMercadoViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoGerenteMercado), List(Of PromocaoPublicoAlvoGerenteMercadoViewModel))(objPublicoAlvoGerenteMercadoList)


                Return PartialView("~/Views/Promocao/PublicoAlvo/GerenteMercado/_WidgetPromocaoPublicoAlvoGerenteMercado.vbhtml", publicoAlvoGerenteMercadoList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal obj As PromocaoPublicoAlvoGerenteMercadoViewModel) As PartialViewResult
            Dim PromocaoPublicoAlvoGerenteMercadoBO = New PromocaoPublicoAlvoGerenteMercadoBO()

            If (ModelState.IsValid) Then
                Dim PromocaoPublicoAlvoGerenteMercado = Mapper.Map(Of PromocaoPublicoAlvoGerenteMercadoViewModel, VO.PromocaoPublicoAlvoGerenteMercado)(obj)
                'Promocao.DesMecanica = "AI                  "
                PromocaoPublicoAlvoGerenteMercadoBO.Insert(PromocaoPublicoAlvoGerenteMercado)
                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                Return Nothing
            End If

            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function


        'Adiciona o resultado da pesquisa.
        <HttpPost()>
        Function AddPesquisa(filtro As PromocaoPublicoAlvoGerenteMercadoViewModel) As PartialViewResult
            Try
                Dim filtroVO As VO.PromocaoPublicoAlvoGerenteMercado = Mapper.Map(Of PromocaoPublicoAlvoGerenteMercadoViewModel, VO.PromocaoPublicoAlvoGerenteMercado)(filtro)

                Dim BO As PromocaoPublicoAlvoGerenteMercadoBO = New PromocaoPublicoAlvoGerenteMercadoBO()
                BO.AddPesquisa(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function Delete(ByVal obj As PromocaoPublicoAlvoGerenteMercadoViewModel) As PartialViewResult
            Dim PromocaoPublicoAlvoGerenteMercadoBO = New PromocaoPublicoAlvoGerenteMercadoBO()

            If (ModelState.IsValid) Then
                Dim PromocaoPublicoAlvoGerenteMercado = Mapper.Map(Of PromocaoPublicoAlvoGerenteMercadoViewModel, VO.PromocaoPublicoAlvoGerenteMercado)(obj)
                'Promocao.DesMecanica = "AI                  "
                PromocaoPublicoAlvoGerenteMercadoBO.Delete(PromocaoPublicoAlvoGerenteMercado)
                Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
                Return Nothing
            End If

            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function

        <HttpPost()>
        Public Function ImportarXlsGerenteMercado(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
            Dim promocaoPublicoAlvoGerenteMercadoBO As New PromocaoPublicoAlvoGerenteMercadoBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseGerenteMercado"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseGerenteMercado"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = promocaoPublicoAlvoGerenteMercadoBO.ImportarExelGerenteMercado(savedFileName, CODPMC, TIPEDEPUBALVPMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseGerenteMercado"
            Else
                Return "TrueGerenteMercado"
            End If
        End Function


        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            Dim PermiteEdicao As Boolean = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

            If (PermiteEdicao) Then
                Dim authCookie As HttpCookie = Request.RequestContext.HttpContext.Request.Cookies(FormsAuthentication.FormsCookieName)
                If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                    Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                    Dim roles As String() = Split(authTicket.UserData, "|")(1).Split(";"c)
                    If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
                        PermiteEdicao = True
                    Else
                        PermiteEdicao = False
                    End If
                End If
            End If

            ViewBag.SituacaoPromocao = obj.DESSITPMC
            ViewBag.PermiteEdicaoPublicoAlvoPromocaoGerenteMercado = PermiteEdicao
        End Sub

    End Class
End Namespace
