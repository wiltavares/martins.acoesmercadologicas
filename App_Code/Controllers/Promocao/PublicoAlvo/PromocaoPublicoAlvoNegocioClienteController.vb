﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class PromocaoPublicoAlvoNegocioClienteController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoNegocioCliente(ByVal objeto As PromocaoPublicoAlvoNegocioClienteViewModel) As PartialViewResult
            Try

                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filialBO As New FilialEmpresaCorporativoBO
                'Dim entidadePublicoAlvoPromocaoBO As New EntidadePublicoAlvoPromocaoBO
                Dim filtro As New VO.Promocao()
                'Dim ObjFiltro As New VO.EntidadePublicoAlvoPromocao()
                filtro.CODPMC = objeto.CODPMC
                'ObjFiltro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim filial = filialBO.SelectT(New VO.FilialEmpresaCorporativo() With {.CODFILEMP = promocao.CODFILEMP})
                promocao.NOMFILEMP = filial.NOMFILEMP
                'Dim entidadePublicoAlvoPromocao = entidadePublicoAlvoPromocaoBO.SelectT(ObjFiltro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)


                Dim BO = New PromocaoPublicoAlvoNegocioClienteBO()
                objeto.TIPEDEPUBALVPMC = 4
                Dim PromocaoPublicoAlvoNegocioClienteList As List(Of VO.PromocaoPublicoAlvoNegocioCliente) = BO.ListarNegocioCliente(objeto.CODPMC, objeto.TIPEDEPUBALVPMC)
                Dim objPromocaoPublicoAlvoNegocioClienteList As List(Of PromocaoPublicoAlvoNegocioClienteViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoNegocioCliente), List(Of PromocaoPublicoAlvoNegocioClienteViewModel))(PromocaoPublicoAlvoNegocioClienteList)


                Return PartialView("~/Views/Promocao/PublicoAlvo/_ModalPromocaoNegocioCliente.vbhtml", objPromocaoPublicoAlvoNegocioClienteList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal obj As List(Of PromocaoPublicoAlvoNegocioClienteViewModel)) As PartialViewResult
            Dim BO As PromocaoPublicoAlvoNegocioClienteBO = New PromocaoPublicoAlvoNegocioClienteBO()
            Try
                If (ModelState.IsValid) Then
                    Dim PromocaoPublicoAlvoNegocioClienteList = Mapper.Map(Of List(Of PromocaoPublicoAlvoNegocioClienteViewModel), List(Of VO.PromocaoPublicoAlvoNegocioCliente))(obj)

                    BO.InsertList(CODPMC, TIPEDEPUBALVPMC, PromocaoPublicoAlvoNegocioClienteList)

                    Return Nothing
                End If


                Throw New WebFaultException(HttpStatusCode.BadRequest)
                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function

        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            Dim PermiteEdicao As Boolean = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

            If (PermiteEdicao) Then
                Dim authCookie As HttpCookie = Request.RequestContext.HttpContext.Request.Cookies(FormsAuthentication.FormsCookieName)
                If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                    Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                    Dim roles As String() = Split(authTicket.UserData, "|")(1).Split(";"c)
                    If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
                        PermiteEdicao = True
                    Else
                        PermiteEdicao = False
                    End If
                End If
            End If

            ViewBag.SituacaoPromocao = obj.DESSITPMC
            ViewBag.PermiteEdicaoPublicoAlvoPromocaoNegocioCliente = PermiteEdicao
        End Sub

    End Class
End Namespace
