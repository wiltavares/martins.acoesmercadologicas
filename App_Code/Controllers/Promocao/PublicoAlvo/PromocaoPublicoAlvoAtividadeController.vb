﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class PromocaoPublicoAlvoAtividadeController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoAtividade(ByVal objeto As PromocaoPublicoAlvoAtividadeViewModel) As PartialViewResult
            Try
                'Validar Se Dados Pode Ser Alterado
                Dim promocaoBO As New PromocaoBO
                Dim filtro As New VO.Promocao()
                filtro.CODPMC = objeto.CODPMC
                ViewBag.acao = "Alterar"
                Dim promocao = promocaoBO.SelectT(filtro)
                Dim promocaoViewModel = Mapper.Map(Of VO.Promocao, PromocaoViewModel)(promocao)

                ControlaVisibilidadeComponentes(promocaoViewModel)

                Dim BO = New PromocaoPublicoAlvoAtividadeBO()

                objeto.TIPEDEPUBALVPMC = 4
                Dim PromocaoPublicoAlvoAtividadeList As List(Of VO.PromocaoPublicoAlvoAtividade) = BO.ListarAtividade(objeto.CODPMC, objeto.TIPEDEPUBALVPMC)
                Dim objPromocaoPublicoAlvoAtividadeList As List(Of PromocaoPublicoAlvoAtividadeViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoAtividade), List(Of PromocaoPublicoAlvoAtividadeViewModel))(PromocaoPublicoAlvoAtividadeList)

                Dim tipoAtividadeList As List(Of VO.PromocaoPublicoAlvoAtividade) = BO.BuscarTipoAtividade()
                Dim objTipoAtividadeList As List(Of PromocaoPublicoAlvoAtividadeViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoAtividade), List(Of PromocaoPublicoAlvoAtividadeViewModel))(tipoAtividadeList)
                ViewBag.TipoAtividade = objTipoAtividadeList


                Return PartialView("~/Views/Promocao/PublicoAlvo/_ModalPromocaoAtividade.vbhtml", objPromocaoPublicoAlvoAtividadeList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal obj As List(Of PromocaoPublicoAlvoAtividadeViewModel)) As ActionResult
            Dim BO As PromocaoPublicoAlvoAtividadeBO = New PromocaoPublicoAlvoAtividadeBO()
            Try
                If (ModelState.IsValid) Then
                    Dim atividadeList = Mapper.Map(Of List(Of PromocaoPublicoAlvoAtividadeViewModel), List(Of VO.PromocaoPublicoAlvoAtividade))(obj)

                    BO.InsertList(CODPMC, TIPEDEPUBALVPMC, atividadeList)

                    'Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                    Return Nothing
                End If

                Throw New WebFaultException(HttpStatusCode.BadRequest)
                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function

        <HttpPost>
        Function BuscarAtividadePorBU(ByVal obj As List(Of PromocaoPublicoAlvoAtividadeViewModel)) As JsonResult

            Dim BO As PromocaoPublicoAlvoAtividadeBO = New PromocaoPublicoAlvoAtividadeBO()

            Dim objeto As List(Of VO.PromocaoPublicoAlvoAtividade) = Mapper.Map(Of List(Of PromocaoPublicoAlvoAtividadeViewModel), List(Of VO.PromocaoPublicoAlvoAtividade))(obj)

            Dim list As List(Of VO.PromocaoPublicoAlvoAtividade) = BO.BuscarAtividadePorBU(objeto)
            Dim listViewModel As List(Of PromocaoPublicoAlvoAtividadeViewModel) = Mapper.Map(Of List(Of VO.PromocaoPublicoAlvoAtividade), List(Of PromocaoPublicoAlvoAtividadeViewModel))(list)

            Return Json(listViewModel, JsonRequestBehavior.AllowGet)
        End Function

        Public Sub ControlaVisibilidadeComponentes(obj As PromocaoViewModel)

            Dim PermiteEdicao As Boolean = (Not obj.TIPSITPMC Is Nothing And obj.TIPSITPMC <> "E")

            If (PermiteEdicao) Then
                Dim authCookie As HttpCookie = Request.RequestContext.HttpContext.Request.Cookies(FormsAuthentication.FormsCookieName)
                If Not authCookie Is Nothing AndAlso authCookie.Value <> "" Then
                    Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                    Dim roles As String() = Split(authTicket.UserData, "|")(1).Split(";"c)
                    If (Not roles.Where(Function(f) f = "47").FirstOrDefault() Is Nothing) Then
                        PermiteEdicao = True
                    Else
                        PermiteEdicao = False
                    End If
                End If
            End If

            ViewBag.SituacaoPromocao = obj.DESSITPMC
            ViewBag.PermiteEdicaoPublicoAlvoPromocaoAtividade = PermiteEdicao
        End Sub

    End Class
End Namespace
