﻿Imports Microsoft.VisualBasic
Imports System.Web.Mvc
Imports AutoMapper
Imports System.Data

Namespace Controllers

    <Authorize(Roles:="43,44")>
    Public Class PromocaoComunicacaoController
        Inherits AbstractController
        'Private promocaoBO As New PromocaoBO
        'Private boFilialEmpresaCorporativo As New FilialEmpresaCorporativoBO()
        'Private boBU As New UnidadeEstrategicaDeNegocioBO()


        ''' <summary>
        ''' Carrega a tela de consulta
        ''' </summary>
        ''' <returns></returns>
        <HttpGet()>
        Function Edit(ByVal CODPMC As Integer) As PartialViewResult
            Dim promocaoComunicacaoBO As New PromocaoComunicacaoBO

            Dim retorno As New PromocaoComunicacaoSaveViewModel()
            retorno.RelacaoComunicacao = Mapper.Map(Of List(Of VO.PromocaoComunicacao), List(Of PromocaoComunicacaoViewModel))(promocaoComunicacaoBO.GetRelPromocaoComunicacao(CODPMC))

            Dim item3 = retorno.RelacaoComunicacao.Where(Function(f) f.TIPCMNPMC = 3).FirstOrDefault()
            If (Not item3 Is Nothing) Then
                Dim config = promocaoComunicacaoBO.ObterParametroPromocaoComunicacao(CODPMC).FirstOrDefault()
                If (Not config Is Nothing) Then
                    item3.CODSMBDVGPMCLIVPCO = config.CODSMBDVGPMCLIVPCO
                    item3.TIPDVGPMCLIVPCO = config.TIPDVGPMCLIVPCO
                End If
            End If

            ViewBag.OpcoesComunicacao = Mapper.Map(Of List(Of VO.PromocaoComunicacao), List(Of PromocaoComunicacaoViewModel))(promocaoComunicacaoBO.ObterOpcoesPromocaoComunicacao())

            Return PartialView("~/Views/Promocao/Comunicacao/_EditPromocaoComunicacao.vbhtml", retorno)
        End Function

        <HttpPost()>
        Public Function Alterar(ByVal comunicacaoViewModel As PromocaoComunicacaoSaveViewModel) As Object
            Try

                Dim promocaoBO As New PromocaoBO
                Dim promocao = promocaoBO.SelectT(New VO.Promocao() With {.CODPMC = comunicacaoViewModel.CODPMC})

                If (promocao Is Nothing) Then
                    Return "Promoção não encontrada."
                End If

                If (promocao.TIPSITPMC = "E") Then
                    Return "Promoção Encerrada!"
                End If

                If (promocao.TIPSITPMC = " " Or promocao.TIPSITPMC = "") Then
                    Return "Promoção já liberada!"
                End If

                Dim comunicacaoList As List(Of VO.PromocaoComunicacao) = Mapper.Map(Of List(Of PromocaoComunicacaoViewModel), List(Of VO.PromocaoComunicacao))(comunicacaoViewModel.RelacaoComunicacao)

                Dim promocaoComunicacaoBO As New PromocaoComunicacaoBO
                promocaoComunicacaoBO.SalvarComunicacao(comunicacaoViewModel.CODPMC, comunicacaoList)

                Return "Salvar"

            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

    End Class


End Namespace
