﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList
Imports System.IO
Imports Microsoft.Reporting.WebForms

Namespace Controllers
    <Authorize(Roles:="38")>
    Public Class ConsultaExtratoVerbaController
        Inherits AbstractController

        'Private boDiretoriaCompra As New DiretoriaCompraBO()


        Function Index() As ViewResult

            CarregaDrops()

            Dim gridConf = New GridSettings(Of ExtratoVerbaViewModel)
            gridConf.Filter = New ExtratoVerbaViewModel()
            gridConf.Filter.TipoVisao = "FORNECEDOR"
            gridConf.Filter.TipoOperacao = "EXTRATO"
            gridConf.Filter.DataIni = New Date(Date.Now.Year, DateTime.Now.Month, 1)
            gridConf.Filter.DataFim = Date.Now.Date

            gridConf.ActionName = "resultadosView"
            gridConf.Page = New PageSettings()
            gridConf.Page.OrderBy = "CODFRNC"
            gridConf.Page.PageSize = 100
            gridConf.ListPaged = PaginarLista(New List(Of ExtratoVerbaViewModel), gridConf.Page)

            Return View(gridConf)

        End Function

        Private Sub CarregaDrops()
            'Dim boCelula As New CelulaBO()
            ViewBag.FiltroCelula = Nothing 'boCelula.ListT(New VO.Celula()).OrderBy(Function(model) model.DESDIVCMP)
            ViewBag.FiltroFornecedor = Constantes.Fornecedores


            Dim boTipoEvento As New TipoDeEventoBO()
            ViewBag.FiltroTipoEvento = boTipoEvento.ListTipoDeEventosPreAcordo(False)

            Dim boEvento As New EventoBO()
            ViewBag.FiltroEvento = boEvento.ListEvento(New VO.Evento())

            Dim boCelula As New RelacaoEquipeDeComprasxGerenteNegocioServComprasBO()
            ViewBag.FiltroCelula = Mapper.Map(Of List(Of VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras), List(Of RelacaoEquipeDeComprasxGerenteNegocioServComprasViewModel))(boCelula.ListDescricaoDivisaoDeCompra(New VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras()))


            Dim boComprador As New CompradorBO()
            ViewBag.FiltroComprador = Mapper.Map(Of List(Of VO.Comprador), List(Of CompradorViewModel))(boComprador.ListT(New VO.Comprador()).OrderBy(Function(model) model.NOMCPR).ToList())

            Dim boDiretoriaCompra As New DiretoriaCompraBO
            ViewBag.FiltroDiretoria = Mapper.Map(Of List(Of VO.DiretoriaCompra), List(Of DiretoriaCompraViewModel))(boDiretoriaCompra.ListT(New VO.DiretoriaCompra()).OrderBy(Function(model) model.DESDRTCMP).ToList())



        End Sub

        Function Pesquisar(dadosPesquisa As GridSettings(Of ExtratoVerbaViewModel)) As PartialViewResult
            Try
                Dim boExtratoVerba As New ExtratoVerbaBO()




                If (Not dadosPesquisa.Filter.DataFim Is Nothing) Then
                    dadosPesquisa.Filter.DataFim = dadosPesquisa.Filter.DataFim.GetValueOrDefault().Date.AddDays(1).AddSeconds(-1)
                End If

                Dim filtro As VO.ExtratoVerba = Mapper.Map(Of ExtratoVerbaViewModel, VO.ExtratoVerba)(dadosPesquisa.Filter)
                Dim listResult As New List(Of ExtratoVerbaViewModel)()

                filtro.TIPDSNDSCBNF = 85 '85 = ACOES COMERCIAIS (AC3)   

                If (dadosPesquisa.Filter.TipoOperacao = "SALDO") Then
                    If (dadosPesquisa.Filter.CODFRN Is Nothing _
                        And dadosPesquisa.Filter.CODCPR Is Nothing _
                        And dadosPesquisa.Filter.CODDIVCMP Is Nothing _
                        And dadosPesquisa.Filter.CODDRTCMP Is Nothing) Then
                        Information("Informe Pelo menos um dos campos como filtro: Fornecedor, Comprador, Célula ou Diretoria")
                        Return Nothing
                    End If
                    listResult = Mapper.Map(Of List(Of VO.ExtratoVerba), List(Of ExtratoVerbaViewModel))(boExtratoVerba.ListaSaldoFornecedor(filtro))
                ElseIf (dadosPesquisa.Filter.TipoOperacao = "EXTRATO" And dadosPesquisa.Filter.TipoVisao = "FORNECEDOR") Then

                    If (dadosPesquisa.Filter.DataIni Is Nothing) Then
                        Information("O campo de filtro Período é obrigatório")
                        Return Nothing
                    End If

                    If (dadosPesquisa.Filter.CODFRN Is Nothing _
                        And dadosPesquisa.Filter.CODCPR Is Nothing _
                        And dadosPesquisa.Filter.CODDIVCMP Is Nothing _
                        And dadosPesquisa.Filter.CODDRTCMP Is Nothing) Then
                        Information("Informe Pelo menos um dos campos como filtro: Fornecedor, Comprador, Célula ou Diretoria")
                        Return Nothing
                    End If
                    dadosPesquisa.Page.PageSize = 100
                    listResult = Mapper.Map(Of List(Of VO.ExtratoVerba), List(Of ExtratoVerbaViewModel))(boExtratoVerba.ListaExtratoFornecedor(filtro))
                ElseIf (dadosPesquisa.Filter.TipoOperacao = "EXTRATO" And dadosPesquisa.Filter.TipoVisao = "EVENTO") Then

                    If (dadosPesquisa.Filter.DataIni Is Nothing) Then
                        Information("O campo de filtro Período é obrigatório")
                        Return Nothing
                    End If
                    If (dadosPesquisa.Filter.CODFRN Is Nothing _
                        And dadosPesquisa.Filter.CODCPR Is Nothing _
                        And dadosPesquisa.Filter.CODDIVCMP Is Nothing _
                        And dadosPesquisa.Filter.CODDRTCMP Is Nothing _
                        And dadosPesquisa.Filter.TIPEVTACOCMC Is Nothing _
                        And dadosPesquisa.Filter.CODEVTACOCMC Is Nothing) Then
                        Information("Informe Pelo menos um dos campos como filtro: Fornecedor, Comprador, Célula, Diretoria, Tipo de Evento ou Evento")
                        Return Nothing
                    End If

                    listResult = Mapper.Map(Of List(Of VO.ExtratoVerba), List(Of ExtratoVerbaViewModel))(boExtratoVerba.ListaSaldoEvento(filtro))
                End If

                Session("ConsultaExtratoVerba") = listResult

                dadosPesquisa.ListPaged = PaginarLista(listResult, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = listResult.Count
                dadosPesquisa.ActionName = "Paginar"

                If (listResult.Count = 0) Then
                    Information("Nenhum registro encontrado para estes filtros informados!")
                End If

                If (dadosPesquisa.Filter.TipoOperacao = "SALDO") Then
                    Return PartialView("~/Views/ConsultaExtratoVerba/_SaldoFornecedorResultadosView.vbhtml", dadosPesquisa)
                ElseIf (dadosPesquisa.Filter.TipoOperacao = "EXTRATO" And dadosPesquisa.Filter.TipoVisao = "FORNECEDOR") Then
                    Return PartialView("~/Views/ConsultaExtratoVerba/_ExtratoFornecedorResultadosView.vbhtml", dadosPesquisa)
                ElseIf (dadosPesquisa.Filter.TipoOperacao = "EXTRATO" And dadosPesquisa.Filter.TipoVisao = "EVENTO") Then
                    Return PartialView("~/Views/ConsultaExtratoVerba/_ExtratoEventoResultadosView.vbhtml", dadosPesquisa)
                Else
                    Return Nothing
                End If


            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Function Paginar(dadosPesquisa As GridSettings(Of ExtratoVerbaViewModel)) As PartialViewResult
            Try
                Dim listResult As New List(Of ExtratoVerbaViewModel)()
                listResult = Session("ConsultaExtratoVerba")

                dadosPesquisa.ListPaged = PaginarLista(listResult, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = listResult.Count
                dadosPesquisa.ActionName = "Paginar"


                If (dadosPesquisa.Filter.TipoOperacao = "SALDO") Then
                    Return PartialView("~/Views/ConsultaExtratoVerba/_SaldoFornecedorResultadosView.vbhtml", dadosPesquisa)
                ElseIf (dadosPesquisa.Filter.TipoOperacao = "EXTRATO" And dadosPesquisa.Filter.TipoVisao = "FORNECEDOR") Then
                    Return PartialView("~/Views/ConsultaExtratoVerba/_ExtratoFornecedorResultadosView.vbhtml", dadosPesquisa)
                ElseIf (dadosPesquisa.Filter.TipoOperacao = "EXTRATO" And dadosPesquisa.Filter.TipoVisao = "EVENTO") Then
                    Return PartialView("~/Views/ConsultaExtratoVerba/_ExtratoEventoResultadosView.vbhtml", dadosPesquisa)
                Else
                    Return Nothing
                End If

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        ''' <summary>
        ''' Método para imprimir o relatório 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function Imprimir(dadosPesquisa As GridSettings(Of ExtratoVerbaViewModel)) As ActionResult
            Try
                Dim boExtratoVerba As New ExtratoVerbaBO()

                If (Not dadosPesquisa.Filter.DataFim Is Nothing) Then
                    dadosPesquisa.Filter.DataFim = dadosPesquisa.Filter.DataFim.GetValueOrDefault().Date.AddDays(1).AddSeconds(-1)
                End If

                Dim filtro As VO.ExtratoVerba = Mapper.Map(Of ExtratoVerbaViewModel, VO.ExtratoVerba)(dadosPesquisa.Filter)
                Dim listResult As New List(Of ExtratoVerbaViewModel)()

                filtro.TIPDSNDSCBNF = 85 '85 = ACOES COMERCIAIS (AC3)   

                If (dadosPesquisa.Filter.TipoOperacao = "SALDO") Then
                    listResult = Mapper.Map(Of List(Of VO.ExtratoVerba), List(Of ExtratoVerbaViewModel))(boExtratoVerba.ListaSaldoFornecedor(filtro))
                ElseIf (dadosPesquisa.Filter.TipoOperacao = "EXTRATO" And dadosPesquisa.Filter.TipoVisao = "FORNECEDOR") Then
                    listResult = Mapper.Map(Of List(Of VO.ExtratoVerba), List(Of ExtratoVerbaViewModel))(boExtratoVerba.ListaExtratoFornecedor(filtro))
                ElseIf (dadosPesquisa.Filter.TipoOperacao = "EXTRATO" And dadosPesquisa.Filter.TipoVisao = "EVENTO") Then
                    listResult = Mapper.Map(Of List(Of VO.ExtratoVerba), List(Of ExtratoVerbaViewModel))(boExtratoVerba.ListaSaldoEvento(filtro))
                End If

                If (listResult.Count = 0) Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                Else

                    Dim relat As New ReportViewer
                    Dim nomeArquivo As String = ""

                    If (dadosPesquisa.Filter.TipoOperacao = "SALDO") Then
                        relat.LocalReport.ReportPath = Server.MapPath("~/Relatorio/ExtratoVerba_Saldo.rdlc")
                        nomeArquivo = "Extrato de Verba Saldo.xls"
                    ElseIf (dadosPesquisa.Filter.TipoOperacao = "EXTRATO" And dadosPesquisa.Filter.TipoVisao = "FORNECEDOR") Then
                        relat.LocalReport.ReportPath = Server.MapPath("~/Relatorio/ExtratoVerba_Extrato_Fornecedor.rdlc")
                        nomeArquivo = "Extrato de Verba Fornecedor.xls"
                    ElseIf (dadosPesquisa.Filter.TipoOperacao = "EXTRATO" And dadosPesquisa.Filter.TipoVisao = "EVENTO") Then
                        relat.LocalReport.ReportPath = Server.MapPath("~/Relatorio/ExtratoVerba_Extrato_Evento.rdlc")
                        nomeArquivo = "Extrato de Verba Evento.xls"
                    End If
                    relat.LocalReport.DataSources.Add(New ReportDataSource("odsExtratoVerba", listResult))

                    Dim deviceInfo As String = "<DeviceInfo>" +
                                               "<OutputFormat>Excel</OutputFormat>" +
                                               "<MarginBottom>0.5in</MarginBottom>" +
                                               "</DeviceInfo>"


                    Dim bytes As Byte()
                    Dim handle As String = Guid.NewGuid().ToString()

                    Dim mimeType As String = String.Empty
                    Dim encoding As String = String.Empty
                    Dim fileNameExtension As String = String.Empty
                    Dim streamIds As String()
                    Dim warnings As Warning()

                    bytes = relat.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, fileNameExtension, streamIds, warnings)

                    Session(handle) = bytes
                    Dim resultado As JsonResult = New JsonResult() With {.Data = New With {.FileGuid = handle, .FileName = nomeArquivo}}
                    resultado.JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    Return resultado
                End If

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        <HttpGet>
        Public Overridable Function Download(fileGuid As String, fileName As String) As ActionResult
            If Session(fileGuid) IsNot Nothing Then
                Dim data As Byte() = TryCast(Session(fileGuid), Byte())
                Return File(data, "application/xls", fileName)
            Else
                ' Problem - Log the error, generate a blank file,
                '           redirect to another controller action - whatever fits with your application
                Return New EmptyResult()
            End If
        End Function

    End Class
End Namespace