﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList
Imports System.IO

Namespace Controllers

    <Authorize(Roles:="27")>
    Public Class ConsultaSaldoCarimboController
        Inherits AbstractController

        Private boCarimbos As New RelacaoAcaoxFornecedorxCarimboBO()
        Private boRelacaoEquipeDeComprasxGerenteNegocioServCompras As New RelacaoEquipeDeComprasxGerenteNegocioServComprasBO

        'Property Cache As ICacheProvider = New DefaultCacheProvider()

        ''' <param name="dadosPesquisa"></param>
        ''' 
        Function Index(dadosPesquisa As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)) As ViewResult

            Dim carimbos As New List(Of RelacaoAcaoxFornecedorxCarimboViewModel)
            Dim gridConf = GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid()
            gridConf.ActionName = "consultaSaldo"
            

            Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")

            dadosPesquisa.Filter = New RelacaoAcaoxFornecedorxCarimboViewModel()
            ViewBag.filtroFornecedorCarimbo = New List(Of RelacaoAcaoxFornecedorxCarimboViewModel)
            ViewBag.filtroCelulaCarimbo = boRelacaoEquipeDeComprasxGerenteNegocioServCompras.ListT(New VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras()).OrderBy(Function(model) model.DESDIVCMP)
            ViewBag.filtroCarimbos = New List(Of SelectListItem)()
            ViewBag.filtroCarimbos.Add(New SelectListItem() With {.Text = "Carimbo", .Value = 1})
            ViewBag.filtroCarimbos.Add(New SelectListItem() With {.Text = "Célula", .Value = 2})
            ViewBag.filtroCarimbos.Add(New SelectListItem() With {.Text = "Fornecedor", .Value = 3})
            Session("listaTempCarimbosSelecionados") = carimbos
            ViewBag.filtroExtratocarimbo = True
            ViewBag.btnExtratoCarimbo = False

            ViewBag.GridResultado = GetGridResultado()

            Return View(gridConf)


        End Function


        ''' <summary>
        ''' Filtrar carimbos de acordo com fornecedor, codigo do carimbo ou codigo da celula
        ''' </summary>
        ''' <param name="codFrn"></param>
        ''' <param name="codCelula"></param>
        ''' <param name="codCarimbo"></param>
        ''' <param name="nomFrn"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function FiltrarCarimbos(ByVal codFrn As Decimal?, ByVal codCelula As Decimal?, ByVal codCarimbo As Decimal?, ByVal nomFrn As String, flSaldo As Boolean) As PartialViewResult
            Try

                Dim carimbos = boCarimbos.PesquisarCarimbos(codFrn, codCelula, codCarimbo, nomFrn, flSaldo)

                If carimbos Is Nothing Or carimbos.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    Return Nothing
                End If

                Dim carimbosLista = Mapper.Map(Of List(Of VO.RelacaoAcaoxFornecedorxCarimbo), List(Of RelacaoAcaoxFornecedorxCarimboViewModel))(carimbos)
                Session("listaTempCarimbo") = carimbosLista

                Dim gridConf = GetGridResultado()
                gridConf.ListPaged = PaginarLista(carimbosLista)
                gridConf.TotalCount = carimbosLista.Count

                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                Return PartialView("~/Views/ConsultaSaldocarimbo/_ResultadoConsultaSaldo.vbhtml", gridConf)


            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Retorna um grid vazio com as configuracoes de grid de carimbos do modal
        ''' </summary>
        ''' <returns></returns>
        Function GetGridResultado() As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)
            Return GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid("consultaSaldo", "AtualizarGridCarimboModal", "ConsultaSaldoCarimbo")
        End Function

        ''' <summary>
        ''' Atualiza o grid de carimbos da busca por carimbos no modal de insercao de carimbo
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridCarimboModal(dadosPesquisa As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)) As PartialViewResult
            Try
                Dim listaTempCarimbo As List(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Session.Item("listaTempCarimbo")
                Dim gridConf = PaginarEAtualizarGrid(listaTempCarimbo, dadosPesquisa.Page, dadosPesquisa.Filter, GetGridResultado)
                gridConf.TotalCount = listaTempCarimbo.Count
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If
                Return PartialView("~/Views/COnsultaSaldoCarimbo/_ResultadoConsultaSaldo.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Atualiza o valor do atributo Valor Utilizado de um pedido selecionado
        ''' </summary>
        <HttpPost()>
        Public Sub AtualizaValorUlitizadoCampoCarimbo(ByVal valores As List(Of Decimal))
            Try
                Dim codCarimbo = valores.FirstOrDefault
                Dim valorUtilizado = valores.LastOrDefault

                Dim listaTempCarimbosSelecionados As List(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Session.Item("listaTempCarimbosSelecionados")
                listaTempCarimbosSelecionados.Where(Function(f) f.CODMCOVBAFRN = codCarimbo).ToList().FirstOrDefault().VLRUTZACOCMC = valorUtilizado
            Catch ex As Exception
                Danger(ex)
            End Try
        End Sub


        Public Function ValidarCarimbos() As ViewResult
            Try
                Dim listaTempCarimbosSelecionados As List(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Session.Item("listaTempCarimbosSelecionados")

                If (listaTempCarimbosSelecionados.Find(Function(f) f.VLRUTZACOCMC = Nothing) IsNot Nothing) Then
                    Warning(Constantes.INFORME_VALOR_UTILIZADO_CARIMBO)
                    Return Nothing
                End If

                If (listaTempCarimbosSelecionados.Find(Function(f) f.VLRUTZACOCMC > f.VALOR_DISPONIVEL) IsNot Nothing) Then
                    Warning(Constantes.VALOR_UTILIZADO_MAIOR_QUE_DISPONIVEL)
                    Return Nothing
                End If

                Dim gridConf = GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid("consultaSaldo", "AtualizarGridCarimbos", "Carimbo")
                gridConf.ListPaged = PaginarLista(listaTempCarimbosSelecionados)
                gridConf.TotalCount = listaTempCarimbosSelecionados.Count

                'Salva no cache e exclui lista temporaria do cache
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoModel.Carimbos = listaTempCarimbosSelecionados

                'Atualiza grid da pagina de edicao da acao
                Return View("~/Views/ConsultaSaldoCarimbo/_ResultadoConsultaSaldo.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Deleta um registro
        ''' </summary>
        ''' <param name="id"></param>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Delete(ByVal id As Decimal, dadosPesquisa As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)) As ViewResult
            Try
                Dim filtro As New VO.RelacaoAcaoxFornecedorxCarimbo()
                'filtro.dropFiltroCarimbos = id
                'filtro.DESDIVCMP = id
                'filtro.DATGRCMCOVBAFRN = id
                'filtro.CODFRN = id
                'filtro.VLRPRVMCOVBAFRN = id
                'filtro.VLRUTZACOCMC = id
                'filtro.VALOR_DISPONIVEL = id
                'filtro.OBSERVACAO = id



                boCarimbos.DeleteT(filtro)

                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                Return Nothing
                'Return FiltrarCarimbos(dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                'Return FiltrarCarimbos(dadosPesquisa)
                Return Nothing
            End Try
        End Function


        ''' <summary>
        ''' Abrir modal de Extrato Carimbo
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function ModalExtratoCarimbo() As ActionResult
            Try
                'ViewBag.filtroExtratocarimbo = True
                ViewBag.btnExtratoCarimbo = False

                Dim dadosPesquisa As New GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)
                dadosPesquisa.Filter = New RelacaoAcaoxFornecedorxCarimboViewModel

                Dim extrato As IEnumerable(Of RelacaoAcaoxFornecedorxCarimboViewModel) = New List(Of RelacaoAcaoxFornecedorxCarimboViewModel)

                Dim gridConf = New GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)(dadosPesquisa.Filter, extrato.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))
                gridConf.ResultID = "extratcarimbo"
                'Return View(gridConf)


                'Return View(gridConf)
                Return View("~/Views/ConsultaSaldoCarimbo/_ModalExtratoCarimbo.vbhtml")

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function


    End Class
End Namespace