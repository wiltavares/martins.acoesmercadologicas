﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList
Imports System.IO
Imports Microsoft.Reporting.WebForms

Namespace Controllers
    <Authorize(Roles:="41")>
    Public Class ConsultaAcompanhamentoBcComFundingController
        Inherits AbstractController

        Function Index() As ViewResult

            CarregaDrops()

            Dim gridConf = New GridSettings(Of ConsultaAcompanhamentoBcComFundingViewModel)
            gridConf.Filter = New ConsultaAcompanhamentoBcComFundingViewModel()
            gridConf.Filter.TIPOPEINIGRCHST = "F"
            gridConf.Filter.DATREFOPEINI = New Date(Date.Now.Year, DateTime.Now.Month, 1)
            gridConf.Filter.DATREFOPEFIM = Date.Now.Date

            gridConf.ListPaged = PaginarLista(New List(Of ConsultaAcompanhamentoBcComFundingViewModel), gridConf.Page)
            gridConf.ActionName = "resultadosView"
            gridConf.Page = New PageSettings()
            gridConf.Page.OrderBy = "CODFRNC"

            Return View(gridConf)

        End Function

        Private Sub CarregaDrops()
            ViewBag.ListaDropFornecedor = Constantes.Fornecedores


            Dim boBeneficiario As New BeneficioBO()
            ViewBag.ListaDropBeneficiario = Mapper.Map(Of List(Of VO.Beneficio), List(Of BeneficiarioViewModel))(boBeneficiario.ListT(New VO.Beneficio()))

            Dim boGrupoFornecedor As New GrupoFornecedorBO()
            ViewBag.ListaDropGrupoFornecedor = Mapper.Map(Of List(Of VO.GrupoFornecedor), List(Of GrupoFornecedorViewModel))(boGrupoFornecedor.ListLookup())


        End Sub

        Function Pesquisar(dadosPesquisa As GridSettings(Of ConsultaAcompanhamentoBcComFundingViewModel)) As PartialViewResult
            Try
                Dim boAcompanhamentoBcComFunding As New ConsultaAcompanhamentoBcComFundingBO()


                Dim filtro As VO.ConsultaAcompanhamentoBcComFunding = Mapper.Map(Of ConsultaAcompanhamentoBcComFundingViewModel, VO.ConsultaAcompanhamentoBcComFunding)(dadosPesquisa.Filter)
                Dim listResult As New List(Of ConsultaAcompanhamentoBcComFundingViewModel)()


                listResult = Mapper.Map(Of List(Of VO.ConsultaAcompanhamentoBcComFunding), List(Of ConsultaAcompanhamentoBcComFundingViewModel))(boAcompanhamentoBcComFunding.Listar(filtro))


                dadosPesquisa.ListPaged = PaginarLista(listResult, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = listResult.Count

                Return PartialView("~/Views/ConsultaAcompanhamentoBcComFunding/_ResultadosView.vbhtml", dadosPesquisa)


            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function




        ''' <summary>
        ''' Método para imprimir o relatório 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function Imprimir(dadosPesquisa As GridSettings(Of ConsultaAcompanhamentoBcComFundingViewModel)) As ActionResult
            Try
                Dim boAcompanhamentoBcComFunding As New ConsultaAcompanhamentoBcComFundingBO()

                Dim filtro As VO.ConsultaAcompanhamentoBcComFunding = Mapper.Map(Of ConsultaAcompanhamentoBcComFundingViewModel, VO.ConsultaAcompanhamentoBcComFunding)(dadosPesquisa.Filter)
                Dim listResult As New List(Of ConsultaAcompanhamentoBcComFundingViewModel)()

                listResult = Mapper.Map(Of List(Of VO.ConsultaAcompanhamentoBcComFunding), List(Of ConsultaAcompanhamentoBcComFundingViewModel))(boAcompanhamentoBcComFunding.Listar(filtro))

                If (listResult.Count = 0) Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                Else

                    Dim relat As New ReportViewer
                    Dim nomeArquivo As String = ""

                    relat.LocalReport.ReportPath = Server.MapPath("~/Relatorio/AcompanhamentoBcComFunding.rdlc")
                    nomeArquivo = "Relatorio de Acompanhamento BC com Funding.xls"
                    relat.LocalReport.DataSources.Add(New ReportDataSource("odsGenerico", listResult))

                    Dim deviceInfo As String = "<DeviceInfo>" +
                                               "<OutputFormat>Excel</OutputFormat>" +
                                               "<MarginBottom>0.5in</MarginBottom>" +
                                               "</DeviceInfo>"
                    Dim bytes As Byte()
                    Dim handle As String = Guid.NewGuid().ToString()

                    Dim mimeType As String = String.Empty
                    Dim encoding As String = String.Empty
                    Dim fileNameExtension As String = String.Empty
                    Dim streamIds As String()
                    Dim warnings As Warning()

                    bytes = relat.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, fileNameExtension, streamIds, warnings)

                    Session(handle) = bytes
                    Dim resultado As JsonResult = New JsonResult() With {.Data = New With {.FileGuid = handle, .FileName = nomeArquivo}}
                    resultado.JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    Return resultado
                End If

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet>
        Public Overridable Function Download(fileGuid As String, fileName As String) As ActionResult
            If Session(fileGuid) IsNot Nothing Then
                Dim data As Byte() = TryCast(Session(fileGuid), Byte())
                Return File(data, "application/xls", fileName)
            Else
                ' Problem - Log the error, generate a blank file,
                '           redirect to another controller action - whatever fits with your application
                Return New EmptyResult()
            End If
        End Function

    End Class
End Namespace