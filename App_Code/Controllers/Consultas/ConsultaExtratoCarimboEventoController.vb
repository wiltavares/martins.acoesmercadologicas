﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList
Imports Martins.AcoesMercadologicas.Core
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="28")>
    Public Class ConsultaExtratoCarimboEventoController
        Inherits AbstractController

        Private boCarimbos As New RelacaoAcaoxFornecedorxCarimboBO()
        Private eventoBO As New EventoBO()
        Private boTipoEvento As New TipoDeEventoBO()
        Private ObjetivoBO As New ObjetivoCarimbodeVerbadoFornecedorBO()
        'Property Cache As ICacheProvider = New DefaultCacheProvider()


        Function Index(dadosPesquisa As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)) As ViewResult

            CarregaDrops()

            dadosPesquisa.Filter = New RelacaoAcaoxFornecedorxCarimboViewModel()
            Dim consulta As IEnumerable(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoxFornecedorxCarimbo), List(Of RelacaoAcaoxFornecedorxCarimboViewModel))(New List(Of VO.RelacaoAcaoxFornecedorxCarimbo))
            Dim gridConf = New GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)(dadosPesquisa.Filter, consulta.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))
            'Dim gridConf = GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid()
            gridConf.ActionName = "resultadosView"

            Return View(gridConf)

        End Function

        ''' <summary>
        ''' procura registros pelo filtro dado
        ''' <param name="codCRB"></param>
        ''' <param name="NOMTIPEVTACOCMC"></param>
        ''' <param name="NOMEVTACOCMC"></param>
        ''' <param name="NOMOBJ"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)) As PartialViewResult

            Try

                Dim consulta As VO.RelacaoAcaoxFornecedorxCarimbo = Mapper.Map(Of RelacaoAcaoxFornecedorxCarimboViewModel, VO.RelacaoAcaoxFornecedorxCarimbo)(dadosPesquisa.Filter)
                Dim consultas As List(Of VO.RelacaoAcaoxFornecedorxCarimbo) = boCarimbos.ListConsulta(consulta)
                Dim consultasViewModel As List(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoxFornecedorxCarimbo), List(Of RelacaoAcaoxFornecedorxCarimboViewModel))(consultas)

                Dim gridConf = PaginarEAtualizarGrid(consultasViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)

                If gridConf.TotalCount = 0 Then

                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)

                End If
                Return PartialView("~/Views/ConsultaExtratoCarimboEvento/_ResultadosView.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' função auxiliar para carregar os drops de nescessarios
        ''' </summary>
        Private Sub CarregaDrops()

            Dim tiposEvento As List(Of VO.TipoDeEvento) = boTipoEvento.ListTipoDeEventos(New VO.TipoDeEvento())
            ViewBag.tipoEvento = tiposEvento

            Dim evento As List(Of VO.Evento) = eventoBO.ListT(New VO.Evento())
            ViewBag.evento = evento

            Dim objetivo As List(Of VO.ObjetivoCarimbodeVerbadoFornecedor) = ObjetivoBO.ListObjetivos(New VO.ObjetivoCarimbodeVerbadoFornecedor())
            ViewBag.objetivo = objetivo



        End Sub

        ''' <summary>
        ''' Deleta um registro
        ''' </summary>
        ''' <param name="id"></param>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Delete(ByVal id As Decimal, dadosPesquisa As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)) As PartialViewResult
            Try
                Dim filtro As New VO.RelacaoAcaoxFornecedorxCarimbo()
                filtro.CODMCOVBAFRN = id
                boCarimbos.DeleteT(filtro)
                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

    End Class
End Namespace