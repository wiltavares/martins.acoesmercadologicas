﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList
Imports Martins.AcoesMercadologicas.Core
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports Sytem.Linq

Namespace Controllers
    <Authorize(Roles:="35")>
    Public Class ConsultaAcompanhamentoPreAcordoController
        Inherits AbstractController

        Private boComprador As New CompradorBO()
        Private boRelacaoEquipeDeComprasxGerenteNegocioServCompras As New RelacaoEquipeDeComprasxGerenteNegocioServComprasBO()
        Private boMercadoria As New MercadoriaBO()
        Private boAcompanhamentoPreAcordo As New AcompanhamentoPreAcordoBO()
        Private boPromocao As New PromocaoBO()
        Private boFilialEmpresaCorporativo As New FilialEmpresaCorporativoBO()

#Region "Relatório de Acompanhamento Pré-Acordo"
        Function Index(dadosPesquisa As GridSettings(Of AcompanhamentoPreAcordoViewModel)) As ViewResult
            CarregaDrops()

            dadosPesquisa.Filter = New AcompanhamentoPreAcordoViewModel()
            Dim consulta As IEnumerable(Of AcompanhamentoPreAcordoViewModel) = Mapper.Map(Of List(Of VO.AcompanhamentoPreAcordo), List(Of AcompanhamentoPreAcordoViewModel))(New List(Of VO.AcompanhamentoPreAcordo))
            Dim gridConf = New GridSettings(Of AcompanhamentoPreAcordoViewModel)(dadosPesquisa.Filter, consulta.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))
            gridConf.ActionName = "resultadosView"

            Return View(gridConf)

        End Function

        ''' <summary>
        ''' Procura registros pelo filtro informado
        ''' <param name="codCRB"></param>
        ''' <param name="NOMTIPEVTACOCMC"></param>
        ''' <param name="NOMEVTACOCMC"></param>
        ''' <param name="NOMOBJ"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of AcompanhamentoPreAcordoViewModel)) As PartialViewResult
            Try
                Dim gridConf As GridSettings(Of AcompanhamentoPreAcordoViewModel)

                If (IsNothing(dadosPesquisa.Filter.DATINIACOCMC) And Not IsNothing(dadosPesquisa.Filter.DataIniAux)) Then
                    dadosPesquisa.Filter.DATINIACOCMC = CDate(dadosPesquisa.Filter.DataIniAux)
                End If
                If (IsNothing(dadosPesquisa.Filter.DATFIMACOCMC) And Not IsNothing(dadosPesquisa.Filter.DataFimAux)) Then
                    dadosPesquisa.Filter.DATFIMACOCMC = CDate(dadosPesquisa.Filter.DataFimAux)
                End If

                Dim acompanhamento As VO.AcompanhamentoPreAcordo = Mapper.Map(Of AcompanhamentoPreAcordoViewModel, VO.AcompanhamentoPreAcordo)(dadosPesquisa.Filter)

                If (IsNothing(acompanhamento.DATINIACOCMC) And IsNothing(acompanhamento.DATFIMACOCMC)) Then
                    Warning("Informe o Período!")
                    gridConf = GridSettings(Of AcompanhamentoPreAcordoViewModel).EmptyGrid()
                Else
                    Dim acompanhamentos As List(Of VO.AcompanhamentoPreAcordo)
                    If (dadosPesquisa.Filter.IndVisao = 2) Then 'Cliente
                        acompanhamentos = boAcompanhamentoPreAcordo.ListaAcompanhamentoPreAcordoCliente(acompanhamento)
                    Else
                        acompanhamentos = boAcompanhamentoPreAcordo.ListaAcompanhamentoPreAcordo(acompanhamento)
                    End If

                    dadosPesquisa.Filter.QuantidadeMerc = acompanhamentos.Sum(Function(item) item.QDEITEPMC)
                    dadosPesquisa.Filter.TotalGeral = acompanhamentos.Sum(Function(item) item.QDEITEPMC * item.VLRUNTFNDMERPMC)

                    Dim acompanhamentoPreAcordoViewModel As List(Of AcompanhamentoPreAcordoViewModel) = Mapper.Map(Of List(Of VO.AcompanhamentoPreAcordo), List(Of AcompanhamentoPreAcordoViewModel))(acompanhamentos)

                    gridConf = PaginarEAtualizarGrid(acompanhamentoPreAcordoViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)

                    If gridConf.TotalCount = 0 Then
                        Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    End If
                End If

                Return PartialView("~/Views/ConsultaAcompanhamentoPreAcordo/_ResultadosView.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' função auxiliar para carregar os drops de nescessarios
        ''' </summary>
        Private Sub CarregaDrops()
            Dim listFilial As List(Of FilialEmpresaCorporativoViewModel) = Mapper.Map(Of List(Of VO.FilialEmpresaCorporativo), List(Of FilialEmpresaCorporativoViewModel))(boFilialEmpresaCorporativo.ListFiliais())
            ViewBag.filtroCelulaCarimbo = boRelacaoEquipeDeComprasxGerenteNegocioServCompras.ListT(New VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras()).OrderBy(Function(model) model.DESDIVCMP)
            ViewBag.ListaDropFornecedor = BuscaFornecedoresLista(Nothing)
            ViewBag.ListaDropFornecedorPatrocinador = BuscaFornecedoresLista(Nothing)
            ViewBag.filtroComprador = boComprador.ListT(New VO.Comprador()).OrderBy(Function(model) model.NOMCPR)
            ViewBag.filtroPromocao = Mapper.Map(Of List(Of VO.Promocao), List(Of PromocaoViewModel))(boPromocao.CarregaPromocao())
            ViewBag.listaFilial = listFilial

        End Sub

        Private Function BuscaFornecedoresLista(ByVal codFrn As Decimal?)
            Return Constantes.FornecedoresCodFrn(codFrn)
        End Function

        ''' <summary>
        ''' Método para imprimir o relatório 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function Imprimir(ByVal codPreAcordo As Decimal?, ByVal tipoOperacao As String, ByVal dataIni As String, ByVal dataFim As String, ByVal codCelula As Decimal?, ByVal codComprador As Decimal?, ByVal codFornecedor As Decimal?, ByVal codFornecedorPatrocinador As Decimal?, ByVal codMercadoria As Decimal?, ByVal opcaoVisao As Decimal?, ByVal desMercadoria As String, ByVal codPromocao As Decimal?, ByVal codFilial As Decimal?, ByVal indCampanhaB2B As Decimal?) As ActionResult
            Try
                If dataIni <> "" And dataFim <> "" Then
                    Dim warnings As Warning()
                    Dim streamIds As String()
                    Dim mimeType As String = String.Empty
                    Dim encoding As String = String.Empty
                    Dim extension As String = String.Empty
                    Dim fileNameExtension As String = String.Empty

                    'Dim relat As LocalReport = New LocalReport
                    Dim relat As New ReportViewer

                    Dim acompanhamentoPreAcordo As New VO.AcompanhamentoPreAcordo()

                    If dataIni <> "" Then
                        acompanhamentoPreAcordo.DATINIACOCMC = Date.Parse(dataIni)
                    End If
                    If dataFim <> "" Then
                        acompanhamentoPreAcordo.DATFIMACOCMC = Date.Parse(dataFim)
                    End If

                    acompanhamentoPreAcordo.CODACOCMC = codPreAcordo
                    acompanhamentoPreAcordo.TIPOPEINIGRCHST = tipoOperacao
                    acompanhamentoPreAcordo.CODDIVCMP = codCelula
                    acompanhamentoPreAcordo.CODCPR = codComprador
                    acompanhamentoPreAcordo.CODFRN = codFornecedor
                    acompanhamentoPreAcordo.CODFRNPTR = codFornecedorPatrocinador
                    acompanhamentoPreAcordo.CODMER = codMercadoria
                    acompanhamentoPreAcordo.DESMER = desMercadoria
                    acompanhamentoPreAcordo.CODPMC = codPromocao
                    acompanhamentoPreAcordo.CODFILEMP = codFilial
                    acompanhamentoPreAcordo.INDCPHBTB = indCampanhaB2B

                    Dim acompanhamentos As List(Of VO.AcompanhamentoPreAcordo)
                    If (opcaoVisao = 2) Then 'Cliente
                        acompanhamentos = boAcompanhamentoPreAcordo.ListaAcompanhamentoPreAcordoCliente(acompanhamentoPreAcordo)
                    Else
                        acompanhamentos = boAcompanhamentoPreAcordo.ListaAcompanhamentoPreAcordo(acompanhamentoPreAcordo)
                    End If

                    If (acompanhamentos.Count > 0) Then
                        If (opcaoVisao = 0) Then
                            relat.LocalReport.ReportPath = Server.MapPath("~/Relatorio/AcompanhamentoPreAcordoFornecedor.rdlc")
                        ElseIf (opcaoVisao = 1) Then
                            relat.LocalReport.ReportPath = Server.MapPath("~/Relatorio/AcompanhamentoPreAcordoMercadoria.rdlc")
                        Else
                            relat.LocalReport.ReportPath = Server.MapPath("~/Relatorio/AcompanhamentoPreAcordoCliente.rdlc")
                            relat.LocalReport.SetParameters(New ReportParameter("TipOperacao", tipoOperacao))
                        End If

                        relat.LocalReport.DataSources.Add(New ReportDataSource("odsAcompanhamentoPreAcordo", acompanhamentos))

                        Dim deviceInfo As String = "<DeviceInfo>" +
                                                                    "<OutputFormat>Excel</OutputFormat>" +
                                                                    "<MarginBottom>0.5in</MarginBottom>" +
                                                                     "</DeviceInfo>"
                        '"<PageWidth>9.5in</PageWidth>"
                        '"<PageHeight>11in</PageHeight>" +
                        '"<MarginTop>0.5in</MarginTop>" +
                        '"<MarginLeft>1in</MarginLeft>" +
                        '"<MarginRight>1in</MarginRight>" +

                        Dim bytes As Byte()
                        Dim handle As String = Guid.NewGuid().ToString()
                        'bytes = relat.Render("PDF", deviceInfo, mimeType, encoding, fileNameExtension, streamIds, warnings)
                        bytes = relat.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, fileNameExtension, streamIds, warnings)

                        Dim nomeArquivo As String = ""

                        If (opcaoVisao = 0) Then
                            nomeArquivo = "AcompanhamentoPreAcordoFornecedor.xls"
                        ElseIf (opcaoVisao = 1) Then
                            nomeArquivo = "AcompanhamentoPreAcordoMercadoria.xls"
                        Else
                            nomeArquivo = "AcompanhamentoPreAcordoCliente.xls"
                        End If

                        Session(handle) = bytes
                        Dim resultado As JsonResult = New JsonResult() With {.Data = New With {.FileGuid = handle, .FileName = nomeArquivo}}
                        resultado.JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        Return resultado
                        'Return File(bytes, mimeType)
                    Else
                        Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    End If
                Else
                    Warning("Informe o Período!")
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet>
        Public Overridable Function Download(fileGuid As String, fileName As String) As ActionResult
            If Session(fileGuid) IsNot Nothing Then
                Dim data As Byte() = TryCast(Session(fileGuid), Byte())
                Return File(data, "application/xls", fileName)
            Else
                ' Problem - Log the error, generate a blank file,
                '           redirect to another controller action - whatever fits with your application
                Return New EmptyResult()
            End If
        End Function
#End Region

#Region "Modal Mercadorias"
        <HttpPost()>
        Function AbrirModalItensOrcamento(objeto As String) As PartialViewResult
            Try
                Dim Codigo As Decimal?
                Dim Nome As String

                If (objeto.Length > 1) Then
                    Dim dados As Array = Split(objeto, ",")
                    If (Not String.IsNullOrEmpty(dados(0))) Then
                        Codigo = Decimal.Parse(dados(0))
                    End If
                    Nome = dados(1)
                End If

                Dim gridConf As GridSettings(Of MercadoriaViewModel)
                Dim mercadoria As New VO.Mercadoria

                mercadoria.CODMER = Codigo
                mercadoria.DESMER = Nome

                Dim dadosPesquisa As New GridSettings(Of MercadoriaViewModel)

                dadosPesquisa.Filter = New MercadoriaViewModel With {.CODMER = Codigo, .DESMER = Nome}

                If (IsNothing(dadosPesquisa.Filter.DESMER) And IsNothing(dadosPesquisa.Filter.CODMER)) Then
                    Warning("Favor Utilizar algum filtro.")

                    Dim grid = New List(Of MercadoriaViewModel)
                    gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT}, New MercadoriaViewModel With {.CODMER = Codigo, .DESMER = Nome})

                    gridConf.ResultID = "resultadosBuscaItens"
                    gridConf.PaginationID = "resultadosBuscaItens"
                    Return PartialView("~/Views/ConsultaAcompanhamentoPreAcordo/_ModalBuscaItensOrcamento.vbhtml", gridConf)
                Else
                    Dim mercadorias As List(Of MercadoriaViewModel) = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.BuscaItensOrcamento(mercadoria))

                    gridConf = PaginarEAtualizarGrid(mercadorias, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT}, New MercadoriaViewModel With {.CODMER = Codigo, .DESMER = Nome})
                    gridConf.ResultID = "resultadosBuscaItensOrcamento"
                    Return PartialView("~/Views/ConsultaAcompanhamentoPreAcordo/_ModalBuscaItensOrcamento.vbhtml", gridConf)
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        <HttpPost()>
        Function PesquisarItensOrcamento(dadosPesquisa As GridSettings(Of MercadoriaViewModel)) As PartialViewResult
            Try
                Dim Mercadorias As VO.Mercadoria = Mapper.Map(Of MercadoriaViewModel, VO.Mercadoria)(dadosPesquisa.Filter)
                Dim listMercadorias As List(Of VO.Mercadoria) = boMercadoria.BuscaItensOrcamento(Mercadorias)
                Dim mercadoriaViewModel As List(Of MercadoriaViewModel) = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(listMercadorias)

                Dim gridConf = PaginarEAtualizarGrid(mercadoriaViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                gridConf.ResultID = "resultadosBuscaItensOrcamento"
                gridConf.PaginationID = "resultadosBuscaItensOrcamento"
                gridConf.ActionName = "PesquisarItensOrcamento"
                gridConf.ControllerName = "ConsultaAcompanhamentoPreAcordo"
                gridConf.Filter = dadosPesquisa.Filter

                Return PartialView("~/Views/ConsultaAcompanhamentoPreAcordo/_ResultadosItensOrcamentoView.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' retorna partialview com grid de mercadorias
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function BuscaItensOrcamentoModal(codItem As Integer, desItem As String) As PartialViewResult
            Try
                Dim dados As New VO.Mercadoria
                dados.CODMER = codItem
                dados.DESMER = desItem

                Dim grid = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.BuscaItensOrcamento(dados))

                Dim gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT})

                If (codItem <> 0 Or desItem <> "") Then
                    If grid.Count = 0 Then
                        Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    End If
                Else
                    Information(Constantes.FILTRO_OBRIGATORIO)
                    gridConf = GridSettings(Of MercadoriaViewModel).EmptyGrid()
                End If

                gridConf.ResultID = "resultadosBuscaItensOrcamento"
                gridConf.PaginationID = "resultadosBuscaItensOrcamento"
                gridConf.ActionName = "PesquisarItensOrcamento"
                gridConf.ControllerName = "ConsultaAcompanhamentoPreAcordo"
                gridConf.Filter = New MercadoriaViewModel With {.CODMER = codItem, .DESMER = desItem}

                Return PartialView("~/Views/ConsultaAcompanhamentoPreAcordo/_ResultadosItensOrcamentoView.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function
#End Region

    End Class
End Namespace