﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList

Namespace Controllers

    <Authorize(Roles:="24,26")>
    Public Class ConsultaSaldoAcaoFornecedorController
        Inherits AbstractController

        Private eventoBO As New EventoBO()
        Private fornecedorBO As New FornecedoresBO()
        Private acaoComercialBO As New AcaoComercialBO()
        Private tipoOperacaoBO As New OperacaoFiscalBO()

        Private boFluxoAprovacao As New RelacaoAcaoComercialxFluxoAprovacaoBO

        'Property Cache As ICacheProvider = New DefaultCacheProvider()

#Region "ConsultaSaldoAcaoFornecedor"

        ''' <summary>
        ''' Carrega pagina princial
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        Function Index(Optional ByVal CODACOCMC As Decimal? = 0) As ViewResult

            CarregaDrops()

            Dim grid As GridSettings(Of AcaoComercialViewModel)

            If CODACOCMC = 0 Then 'verifica se veio o paramtro de codigo da ação

                Dim gridInicioVazio = GridSettings(Of AcaoComercialViewModel).EmptyGrid("gridFornecedores", "PesquisarSaldoAcaoFornecedor", "Fornecedor")
                gridInicioVazio.Filter = New AcaoComercialViewModel()
                grid = gridInicioVazio

                Return View(grid)
            Else

                'busca os dados da acção comercial e mostra na tela - funcao feita para ser usada quando o usuario vier redirecionado da tela de Consulta Saldo Ação
                Dim dadosPesquisa = acaoComercialBO.BuscarAcaoComercialPorId(CODACOCMC)

                Dim gridConsulta = GridSettings(Of AcaoComercialViewModel).EmptyGrid("gridFornecedores", "IndexSaldoAcaoFornecedorRedirect", "ConsultaSaldoAcaoFornecedor")
                gridConsulta.Filter = New AcaoComercialViewModel With {.CODACOCMC = CODACOCMC, .NOMACOCMC = dadosPesquisa.NOMACOCMC.Trim()}

                'consulta os dados de acoes no banco pelo codigo da acao
                Dim acao As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(gridConsulta.Filter)
                Dim acoes As List(Of VO.AcaoComercial) = acaoComercialBO.ListarSaldoAcaoFornecedor(acao)
                Dim acoesViewModel As List(Of AcaoComercialViewModel) = Mapper.Map(Of List(Of VO.AcaoComercial), List(Of AcaoComercialViewModel))(acoes)

                Dim gridConf = PaginarEAtualizarGrid(acoesViewModel, gridConsulta.Page, gridConsulta.Filter)

                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    Return View(gridConf)
                End If

                grid = gridConf

                Return View(grid)

            End If

        End Function

        ''' <summary>
        ''' procura registros pelo filtro dado
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of AcaoComercialViewModel)) As PartialViewResult
            Try


                Dim acao As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(dadosPesquisa.Filter)
                Dim acoes As List(Of VO.AcaoComercial) = acaoComercialBO.ListarSaldoAcaoFornecedor(acao)
                Dim acoesViewModel As List(Of AcaoComercialViewModel) = Mapper.Map(Of List(Of VO.AcaoComercial), List(Of AcaoComercialViewModel))(acoes)

                Dim gridConf = PaginarEAtualizarGrid(acoesViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    Return Nothing
                End If
                Return PartialView("~/Views/ConsultaSaldoAcaoFornecedor/_ResultadosView.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' função auxiliar para carregar os drops de nescessarios na tela Saldo Ação Evento
        ''' </summary>
        Private Sub CarregaDrops()

            'VIEW BAG usada para inicial o filtro de Comprador na modal
            Dim boComprador As New CompradorBO
            ViewBag.filtroComprador = boComprador.ListT(New VO.Comprador()).OrderBy(Function(model) model.NOMCPR)

            'VIEW BAG usada para inicial o filtro de Fornecedor na modal
            Dim boRelacaoEquipeDeComprasxGerenteNegocioServCompras As New RelacaoEquipeDeComprasxGerenteNegocioServComprasBO
            ViewBag.fornecedorAcao = New List(Of AcaoComercialViewModel)

            'VIEW BAG usada para inicial o filtro de Celula na modal
            ViewBag.filtroCelula = boRelacaoEquipeDeComprasxGerenteNegocioServCompras.ListT(New VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras()).OrderBy(Function(model) model.DESDIVCMP)

            'VIEW BAG usada para iniciar o filtro de Fornecedor na modal
            ViewBag.filtroSaldoAcaoFrn = New List(Of SelectListItem)()
            ViewBag.filtroSaldoAcaoFrn.Add(New SelectListItem() With {.Text = "Comprador", .Value = 1})
            ViewBag.filtroSaldoAcaoFrn.Add(New SelectListItem() With {.Text = "Célula", .Value = 2})
            ViewBag.filtroSaldoAcaoFrn.Add(New SelectListItem() With {.Text = "Fornecedor", .Value = 3})

        End Sub

#End Region

        '#Region "ModalConsultaSaldoAcaoFornecedor - NAO UTILIZADO NO MOMENTO"
        '        ''' <summary>
        '        ''' Abre o modal de fluxo de aprovacoes de uma acao
        '        ''' </summary>
        '        ''' <param name="objeto"></param>
        '        ''' <returns></returns>
        '        <HttpPost()>
        '        Public Function IndexSaldoAcaoFornecedor(objeto As Decimal) As PartialViewResult

        '            'VIEW BAG usada para inicial o filtro de Comprador na modal
        '            Dim boComprador As New CompradorBO
        '            ViewBag.filtroComprador = boComprador.ListT(New VO.Comprador()).OrderBy(Function(model) model.NOMCPR)

        '            'VIEW BAG usada para inicial o filtro de Fornecedor na modal
        '            Dim boRelacaoEquipeDeComprasxGerenteNegocioServCompras As New RelacaoEquipeDeComprasxGerenteNegocioServComprasBO
        '            ViewBag.fornecedorAcao = New List(Of AcaoComercialViewModel)

        '            'VIEW BAG usada para inicial o filtro de Celula na modal
        '            ViewBag.filtroCelula = boRelacaoEquipeDeComprasxGerenteNegocioServCompras.ListT(New VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras()).OrderBy(Function(model) model.DESDIVCMP)

        '            'VIEW BAG usada para iniciar o filtro de Fornecedor na modal
        '            ViewBag.filtroSaldoAcaoFrn = New List(Of SelectListItem)()
        '            ViewBag.filtroSaldoAcaoFrn.Add(New SelectListItem() With {.Text = "Comprador", .Value = 1})
        '            ViewBag.filtroSaldoAcaoFrn.Add(New SelectListItem() With {.Text = "Célula", .Value = 2})
        '            ViewBag.filtroSaldoAcaoFrn.Add(New SelectListItem() With {.Text = "Fornecedor", .Value = 3})

        '            Dim dadosPesquisa = acaoComercialBO.BuscarAcaoComercialPorId(objeto)

        '            If (dadosPesquisa Is Nothing) Then
        '                Information(Constantes.FLUXO_NAO_ENCONTRADO)
        '                Return Nothing
        '            End If

        '            Dim gridConf = GridSettings(Of AcaoComercialViewModel).EmptyGrid("gridFornecedores", "PesquisarSaldoAcaoFornecedor", "Fornecedor")
        '            gridConf.Filter = New AcaoComercialViewModel With {.CODACOCMC = objeto, .NOMACOCMC = dadosPesquisa.NOMACOCMC.Trim()}

        '            Return PartialView("~/Views/ConsultaSaldoAcao/_ModalSaldoAcaoFornecedor.vbhtml", gridConf)

        '        End Function

        '        ''' <summary>
        '        ''' procura registros pelo filtro dado
        '        ''' </summary>
        '        ''' <param name="dadosPesquisa"></param>
        '        ''' <returns></returns>
        '        <HttpPost()>
        '        Function PesquisarSaldoAcaoFornecedor(dadosPesquisa As GridSettings(Of AcaoComercialViewModel)) As PartialViewResult

        '            Dim acao As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(dadosPesquisa.Filter)
        '            Dim acoes As List(Of VO.AcaoComercial) = acaoComercialBO.ListarSaldoAcaoFornecedor(acao)
        '            Dim acoesViewModel As List(Of AcaoComercialViewModel) = Mapper.Map(Of List(Of VO.AcaoComercial), List(Of AcaoComercialViewModel))(acoes)

        '            Dim gridConf = PaginarEAtualizarGrid(acoesViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
        '            gridConf.ActionName = "PesquisarSaldoAcaoFornecedor"

        '            If gridConf.TotalCount = 0 Then

        '                'Define a div onde vai aparecer a msg e renderiza a pagina de alerta
        '                gridConf.ResultID = "msgModalSaldoAcaoFornecedor"
        '                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)

        '                Return PartialView("~/Views/Shared/_Alerts.vbhtml", gridConf)
        '            Else
        '                Return PartialView("~/Views/ConsultaSaldoAcao/_ResultadosViewAcaoFornecedor.vbhtml", gridConf)
        '            End If

        '        End Function

        '#End Region

    End Class
End Namespace