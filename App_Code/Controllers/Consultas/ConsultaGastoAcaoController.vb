﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports Microsoft.Reporting.WebForms

Namespace Controllers

    <Authorize(Roles:="25")>
    Public Class ConsultaGastoAcaoController
        Inherits AbstractController

        Private eventoBO As New EventoBO()
        Private fornecedorBO As New FornecedoresBO()
        Private acaoComercialBO As New AcaoComercialBO()
        Private tipoOperacaoBO As New OperacaoFiscalBO()

        Private boFluxoAprovacao As New RelacaoAcaoComercialxFluxoAprovacaoBO

        'Property Cache As ICacheProvider = New DefaultCacheProvider()

       




#Region "Consulta Gasto Acao Evento"

        ''' <summary>
        ''' Carrega pagina princial
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        Function Index(dadosPesquisa As GridSettings(Of AcaoComercialViewModel)) As ViewResult

            CarregaDropsGastoAcaoEvento()

            dadosPesquisa.Filter = New AcaoComercialViewModel()
            Dim acao As IEnumerable(Of AcaoComercialViewModel) = Mapper.Map(Of List(Of VO.AcaoComercial), List(Of AcaoComercialViewModel))(New List(Of VO.AcaoComercial))
            Dim gridConf = New GridSettings(Of AcaoComercialViewModel)(dadosPesquisa.Filter, acao.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))

            Return View(gridConf)
        End Function

        ''' <summary>
        ''' procura registros pelo filtro dado
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of AcaoComercialViewModel)) As PartialViewResult
            Try

            
            Dim acao As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(dadosPesquisa.Filter)
            Dim acoes As List(Of VO.AcaoComercial) = acaoComercialBO.ListarGastoAcaoEvento(acao)
            Dim acoesViewModel As List(Of AcaoComercialViewModel) = Mapper.Map(Of List(Of VO.AcaoComercial), List(Of AcaoComercialViewModel))(acoes)

            Dim gridConf = PaginarEAtualizarGrid(acoesViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
            If gridConf.TotalCount = 0 Then
                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
            End If
            Return PartialView("~/Views/ConsultaGastoAcao/_ResultadosView.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        Function Imprimir(ByVal CODEVTACOCMC As Decimal?) As ActionResult

            '(ByVal DATINIACOCMC  As Date?, ByVal DATFIMACOCMC As Date, ByVal CODEVTACOCMC As Nullable(Of Decimal), ByVal CODOPEFSCDSNACOCMC As Decimal, CODACOCMC As Decimal?,ByVal NOMACOCMC As String)
            Try
                Dim warnings As Warning()
                Dim streamIds As String()
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim extension As String = String.Empty
                Dim fileNameExtension As String = String.Empty
                Dim relat As LocalReport = New LocalReport

                relat.ReportPath = Server.MapPath("~/Relatorio/ConsultaGastoAcao.rdlc")

                Dim acao As New VO.AcaoComercial()
                
                acao.CODEVTACOCMC = CODEVTACOCMC
                

                '  Dim acao As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(filtro)

                Dim acoes As List(Of VO.AcaoComercial) = acaoComercialBO.ListarGastoAcaoEvento(acao)

                If (acoes.Count > 0) Then

                    relat.DataSources.Add(New ReportDataSource("odsAcaoComercial", acoes))

                    Dim deviceInfo As String = "<DeviceInfo>" +
                                                                "<OutputFormat>Excel</OutputFormat>" +
                                                                "<MarginBottom>0.5in</MarginBottom>" +
                                                                 "</DeviceInfo>"

                    '"<PageWidth>9.5in</PageWidth>"
                    '"<PageHeight>11in</PageHeight>" +
                    '"<MarginTop>0.5in</MarginTop>" +
                    '"<MarginLeft>1in</MarginLeft>" +
                    '"<MarginRight>1in</MarginRight>" +


                    Dim bytes As Byte()
                    Dim handle As String = Guid.NewGuid().ToString()
                    'bytes = relat.Render("PDF", deviceInfo, mimeType, encoding, fileNameExtension, streamIds, warnings)
                    bytes = relat.Render("Excel", deviceInfo, mimeType, encoding, fileNameExtension, streamIds, warnings)

                    Session(handle) = bytes
                    Dim resultado As JsonResult = New JsonResult() With {.Data = New With {.FileGuid = handle, .FileName = "ConsultaGastoAcao.xls"}}
                    resultado.JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    Return resultado
                    'Return File(bytes, mimeType)
                Else
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)

                End If


            Catch ex As Exception
                Danger(ex)
            End Try

        End Function
        <HttpGet>
        Public Overridable Function Download(fileGuid As String, fileName As String) As ActionResult
            If Session(fileGuid) IsNot Nothing Then
                Dim data As Byte() = TryCast(Session(fileGuid), Byte())
                Return File(data, "application/xls", fileName)
            Else
                ' Problem - Log the error, generate a blank file,
                '           redirect to another controller action - whatever fits with your application
                Return New EmptyResult()
            End If
        End Function




        ''' <summary>
        ''' função auxiliar para carregar os drops de nescessarios na tela Saldo Ação Evento
        ''' </summary>
        Private Sub CarregaDropsGastoAcaoEvento()

            Dim evento As List(Of EventoViewModel) = Mapper.Map(Of List(Of VO.Evento), List(Of EventoViewModel))(eventoBO.ListT(New VO.Evento()))
            ViewBag.evento = evento.OrderBy(Function(f) f.CODEVTACOCMC)

        End Sub

#End Region

    End Class

End Namespace