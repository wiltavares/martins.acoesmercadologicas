﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList
Imports Microsoft.Reporting.WebForms
Namespace Controllers

    <Authorize(Roles:="24")>
    Public Class ConsultaSaldoAcaoController
        Inherits AbstractController

        Private eventoBO As New EventoBO()
        Private fornecedorBO As New FornecedoresBO()
        Private acaoComercialBO As New AcaoComercialBO()
        Private tipoOperacaoBO As New OperacaoFiscalBO()

        Private boFluxoAprovacao As New RelacaoAcaoComercialxFluxoAprovacaoBO

        'Property Cache As ICacheProvider = New DefaultCacheProvider()

#Region "ConsultaSaldoAcaoEvento"

        ''' <summary>
        ''' Carrega pagina princial
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        Function Index(dadosPesquisa As GridSettings(Of AcaoComercialViewModel)) As ViewResult

            CarregaDropsSaldoAcaoEvento()

            dadosPesquisa.Filter = New AcaoComercialViewModel()
            dadosPesquisa.Filter.DATFIMACOCMC = Date.Now
            Dim acao As IEnumerable(Of AcaoComercialViewModel) = Mapper.Map(Of List(Of VO.AcaoComercial), List(Of AcaoComercialViewModel))(New List(Of VO.AcaoComercial))
            Dim gridConf = New GridSettings(Of AcaoComercialViewModel)(dadosPesquisa.Filter, acao.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))

            Return View(gridConf)
        End Function

        ''' <summary>
        ''' procura registros pelo filtro dado
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <AcceptVerbs(HttpVerbs.Post)>
        Function Pesquisar(dadosPesquisa As GridSettings(Of AcaoComercialViewModel))
            Try

                If (IsNothing(dadosPesquisa.Filter.DATINIACOCMC) And Not IsNothing(dadosPesquisa.Filter.DataIniAux)) Then
                    dadosPesquisa.Filter.DATINIACOCMC = CDate(dadosPesquisa.Filter.DataIniAux)
                End If
                If (IsNothing(dadosPesquisa.Filter.DATFIMACOCMC) And Not IsNothing(dadosPesquisa.Filter.DataFimAux)) Then
                    dadosPesquisa.Filter.DATFIMACOCMC = CDate(dadosPesquisa.Filter.DataFimAux)
                End If

                Dim acao As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(dadosPesquisa.Filter)
                Dim acoes As List(Of VO.AcaoComercial) = acaoComercialBO.ListarSaldoAcaoEventoEmAberto(acao)

                If (acoes.Count > 0) Then

                    Dim acoesViewModel As List(Of AcaoComercialViewModel) = Mapper.Map(Of List(Of VO.AcaoComercial), List(Of AcaoComercialViewModel))(acoes)

                    Dim gridConf = PaginarEAtualizarGrid(acoesViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)

                    Return PartialView("~/Views/ConsultaSaldoAcao/_ResultadosView.vbhtml", gridConf)
                Else
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)

                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
            'PartialViewResult
        End Function


        'Function Imprimir(filtro As GridSettings(Of AcaoComercialViewModel)) As ActionResult
        '<AcceptVerbs(HttpVerbs.Post)>
        'Function Imprimir(filtro As AcaoComercialViewModel) As ActionResult
        Function Imprimir(ByVal CODEVTACOCMC As Decimal?, ByVal CODACOCMC As Decimal?, ByVal DATINIACOCMC As String, ByVal DATFIMACOCMC As String, ByVal CODOPEFSCDSNACOCMC As Decimal?, ByVal NOMACOCMC As String) As ActionResult
            ', ByVal DATFIMACOCMC As Date?, ByVal CODEVTACOCMC As Decimal?, ByVal CODOPEFSCDSNACOCMC As Decimal?, CODACOCMC As Decimal?, ByVal NOMACOCMC As String

            ' Dim CODACOCMC As Decimal?
            ' Dim DATINIACOCMC As Date?
            'Dim DATFIMACOCMC As Date?
            '  Dim CODOPEFSCDSNACOCMC As Decimal?
            '  Dim NOMACOCMC As String

            Try
                Dim warnings As Warning()
                Dim streamIds As String()
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim extension As String = String.Empty
                Dim fileNameExtension As String = String.Empty
                Dim relat As LocalReport = New LocalReport

                relat.ReportPath = Server.MapPath("~/Relatorio/ConsultaSaldoAcao.rdlc")

                Dim acao As New VO.AcaoComercial()

                If DATINIACOCMC <> "" Then
                    acao.DATINIACOCMC = Date.Parse(DATINIACOCMC)
                End If
                If DATFIMACOCMC <> "" Then
                    acao.DATFIMACOCMC = Date.Parse(DATFIMACOCMC)
                End If

                acao.CODEVTACOCMC = CODEVTACOCMC
                acao.CODOPEFSCDSNACOCMC = CODOPEFSCDSNACOCMC
                acao.CODACOCMC = CODACOCMC
                acao.NOMACOCMC = NOMACOCMC

                '  Dim acao As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(filtro)

                Dim acoes As List(Of VO.AcaoComercial) = acaoComercialBO.ListarSaldoAcaoEventoEmAberto(acao)

                If (acoes.Count > 0) Then

                    relat.DataSources.Add(New ReportDataSource("odsAcaoComercial", acoes))

                    Dim deviceInfo As String = "<DeviceInfo>" +
                                                                "<OutputFormat>Excel</OutputFormat>" +
                                                                "<MarginBottom>0.5in</MarginBottom>" +
                                                                 "</DeviceInfo>"

                    '"<PageWidth>9.5in</PageWidth>"
                    '"<PageHeight>11in</PageHeight>" +
                    '"<MarginTop>0.5in</MarginTop>" +
                    '"<MarginLeft>1in</MarginLeft>" +
                    '"<MarginRight>1in</MarginRight>" +


                    Dim bytes As Byte()
                    Dim handle As String = Guid.NewGuid().ToString()
                    'bytes = relat.Render("PDF", deviceInfo, mimeType, encoding, fileNameExtension, streamIds, warnings)
                    bytes = relat.Render("Excel", deviceInfo, mimeType, encoding, fileNameExtension, streamIds, warnings)

                    Session(handle) = bytes
                    Dim resultado As JsonResult = New JsonResult() With {.Data = New With {.FileGuid = handle, .FileName = "ConsultaSaldoAcao.xls"}}
                    resultado.JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    Return resultado
                    'Return File(bytes, mimeType)
                Else
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        <HttpGet>
        Public Overridable Function Download(fileGuid As String, fileName As String) As ActionResult
            If Session(fileGuid) IsNot Nothing Then
                Dim data As Byte() = TryCast(Session(fileGuid), Byte())
                'Return File(data, "application/pdf", fileName)
                Return File(data, "application/xls", fileName)            
            Else
                ' Problem - Log the error, generate a blank file,
                '           redirect to another controller action - whatever fits with your application
                Return New EmptyResult()
            End If
        End Function

        'Function Imprimir2() As ActionResult

        '    'ByVal codFrn As Decimal?, ByVal codCelula As Decimal?, ByVal codCarimbo As Decimal?, ByVal nomFrn As String, flSaldo As Boolean
        '    Try
        '        Dim warnings As Warning()
        '        Dim streamIds As String()
        '        Dim mimeType As String = String.Empty
        '        Dim encoding As String = String.Empty
        '        Dim extension As String = String.Empty
        '        Dim fileNameExtension As String = String.Empty
        '        Dim relat As LocalReport = New LocalReport
        '        'Dim dadosPesquisa As GridSettings(Of AcaoComercialViewModel)

        '        relat.ReportPath = Server.MapPath("~/Relatorio/ConsultaSaldoAcao.rdlc")

        '        Dim acao As VO.AcaoComercial = New VO.AcaoComercial
        '        Dim acoes As List(Of VO.AcaoComercial) = acaoComercialBO.ListarSaldoAcaoEventoEmAberto(acao)

        '        If (acoes.Count > 0) Then

        '            relat.DataSources.Add(New ReportDataSource("odsAcaoComercial", acoes))

        '            Dim deviceInfo As String = "<DeviceInfo>" +
        '                                                        "<OutputFormat>PDF</OutputFormat>" +
        '                                                        "<PageWidth>8.5in</PageWidth>" +
        '                                                        "<PageHeight>11in</PageHeight>" +
        '                                                        "<MarginTop>0.5in</MarginTop>" +
        '                                                        "<MarginLeft>1in</MarginLeft>" +
        '                                                        "<MarginRight>1in</MarginRight>" +
        '                                                        "<MarginBottom>0.5in</MarginBottom>" +
        '                                                        "</DeviceInfo>"



        '            Dim bytes As Byte()

        '            bytes = relat.Render("PDF", deviceInfo, mimeType, encoding, fileNameExtension, streamIds, warnings)

        '            Return File(bytes, mimeType, "ConsultaSaldoAcao.pdf")
        '        Else
        '            Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)

        '        End If


        '    Catch ex As Exception
        '        Danger(ex)
        '        Return Nothing
        '    End Try

        'End Function


        '"<DeviceInfo>" +
        '                                            "<OutputFormat>PDF</OutputFormat>" +
        '                                            "<PageWidth>8.5in</PageWidth>" +
        '                                            "<PageHeight>11in</PageHeight>" +
        '                                            "<MarginTop>0.5in</MarginTop>" +
        '                                            "<MarginLeft>1in</MarginLeft>" +
        '                                            "<MarginRight>1in</MarginRight>" +
        '                                            "<MarginBottom>0.5in</MarginBottom>" +
        '                                            "</DeviceInfo>"

        ' ''' <summary>
        ' ''' imprime registros pelo filtro dado
        ' ''' </summary>
        ' ''' <param name="dadosPesquisa"></param>
        ' ''' <returns></returns>
        '<HttpPost()>






        ''' <summary>
        ''' função auxiliar para carregar os drops de nescessarios na tela Saldo Ação Evento
        ''' </summary>
        ''' 
        Private Sub CarregaDropsSaldoAcaoEvento()

            Dim evento As List(Of EventoViewModel) = Mapper.Map(Of List(Of VO.Evento), List(Of EventoViewModel))(eventoBO.ListT(New VO.Evento()))
            ViewBag.evento = evento.OrderBy(Function(f) f.CODEVTACOCMC)

            Dim operacoes As List(Of TipoOperacaoViewModel) = Mapper.Map(Of List(Of VO.OperacaoFiscal), List(Of TipoOperacaoViewModel))(tipoOperacaoBO.ListT(New VO.OperacaoFiscal()))
            ViewBag.operacoes = operacoes.OrderBy(Function(f) f.DROPDESC)
        End Sub

#End Region


    End Class
End Namespace