﻿Imports System.Web.Mvc
Imports System.DirectoryServices
Imports System.DirectoryServices.AccountManagement
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.IO
Imports System.Xml
Imports Newtonsoft.Json.Linq

Namespace Controllers
    Public Class LoginController
        Inherits AbstractController

        <AllowAnonymous>
        Public Function Index() As ActionResult


            Dim path As String = Request.QueryString("path")

            If Not String.IsNullOrEmpty(path) Then
                path = "~" & path
            End If

            Try
                If ControleAcesso.CodFnc() > 0 Then
                    If Not String.IsNullOrEmpty(path) Then
                        Return Redirect(path)
                    Else
                        Return RedirectToAction("Index", "Home")
                    End If
                End If
            Catch ex As Exception

            End Try

            Dim IdNetwork As String = Request.QueryString("IdNetwork")
            Dim TokenAcessoUsuario As String = Request.QueryString("Token")

            If Not String.IsNullOrEmpty(IdNetwork) AndAlso Not String.IsNullOrEmpty(TokenAcessoUsuario) Then

                'Session("IsMobile") = Util.IsMobile(Request)


                Dim retorno As RetornoApi = AprovaSIMApi.GetSessionTimeout(IdNetwork, TokenAcessoUsuario)
                If retorno.Sucesso Then

                    Dim autenticado = CType(retorno.Resultado, Boolean)
                    If autenticado Then

                        Session("IsMobile") = True
                        Dim usuario = BuscaDadosUsuario(IdNetwork, "")

                        Dim authCookie As HttpCookie = GetCookie(IdNetwork, usuario, False)
                        System.Web.HttpContext.Current.Response.Cookies.Add(authCookie)

                        Return RedirectToAction("Aprovacoes", "AcaoComercial")
                        'If Not String.IsNullOrEmpty(path) Then
                        '    Return Redirect(path)
                        'Else
                        '    Return RedirectToAction("Index", "Home")
                        'End If
                    End If
                End If
            Else
                Dim url = Request.QueryString.Item("ReturnUrl")
                Session("UrlRetorno") = url
            End If

            Return View(New LoginViewModel())


        End Function

        <HttpPost>
        <AllowAnonymous>
        Public Function Index(model As LoginViewModel, returnUrl As String) As ActionResult
            Try
                If Not IsNothing(Session("UrlRetorno")) Then
                    returnUrl = Session("UrlRetorno").ToString
                End If
                Session.Remove("UrlRetorno")
                If Not ModelState.IsValid Then
                    Return View(model)
                End If


                Dim ntUser As String = model.Usuario
                Dim loginComoAdmin As Boolean = False
                If (model.Usuario.Contains("@")) Then
                    ntUser = model.Usuario.Split("@")(0)
                    model.Usuario = model.Usuario.Split("@")(1)
                    loginComoAdmin = True
                End If
                Dim gruposusuario As String()

                Dim ret = AutenticacaoApi.AutenticaPOST(ntUser, model.Senha)
                Dim retornoJObject As JObject = ret.Resultado

                If (retornoJObject Is Nothing) Then
                    ModelState.AddModelError("LoginInvalido", "Não foi possível consultar dados.")
                    Return View(model)
                End If

                If (retornoJObject.GetValue("Code").ToString() = "102") Then
                    ModelState.AddModelError("LoginInvalido", "Usuário ou Senha inválidos.")
                    Return View(model)
                End If

                If (retornoJObject.GetValue("Code").ToString() <> "104") Then
                    ModelState.AddModelError("LoginInvalido", retornoJObject.GetValue("Message").ToString())
                    Return View(model)
                Else
                    gruposusuario = retornoJObject.SelectToken("UserAD.lstGroupAD").ToList().Select(Function(x) x.SelectToken("$.Name").Value(Of String)().ToUpper()).ToArray()
                End If

                If (loginComoAdmin) Then
                    If (String.IsNullOrEmpty(ConfigurationManager.AppSettings("gruposTI"))) Then
                        ModelState.AddModelError("LoginInvalido", "Não há grupos configurados para login como admin.")
                        Return View(model)
                    End If
                    Dim gruposTI As String() = ConfigurationManager.AppSettings("gruposTI").Split(";")

                    If (Not gruposusuario.Any(Function(item) gruposTI.Contains(item))) Then
                        ModelState.AddModelError("LoginInvalido", "Você não tem permissão para logar como administrador.")
                        Return View(model)
                    End If
                End If

                Dim gruposStr = ""
                For Each item As String In gruposusuario
                    gruposStr += item + ";"
                Next
                If String.IsNullOrWhiteSpace(gruposStr) Then
                    gruposStr = gruposStr.Substring(0, gruposStr.Length - 1)
                End If

                Dim usuario = BuscaDadosUsuario(model.Usuario, gruposStr)
                If Not usuario Is Nothing Then

                    Dim authCookie As HttpCookie = GetCookie(model.Usuario, usuario, model.Lembrar)
                    System.Web.HttpContext.Current.Response.Cookies.Add(authCookie)

                    If Url.IsLocalUrl(returnUrl) AndAlso returnUrl.Length > 1 AndAlso returnUrl.StartsWith("/") AndAlso Not returnUrl.StartsWith("//") AndAlso Not returnUrl.StartsWith("/\") Then
                        Return Redirect(returnUrl)
                    End If
                    Return RedirectToAction("Index", "Home")
                Else
                    ModelState.AddModelError("LoginInvalido", "Usuário não encontrado")
                End If

                Return View(model)
            Catch ex As Exception
                ModelState.AddModelError("LoginInvalido", ex.Message)
                Return View(model)
            End Try
        End Function

        Private Function GetCookie(idNetwork As String, usuario As String, lembrar As Boolean) As HttpCookie

            Dim tempo As Integer = ConfigurationManager.AppSettings("TempoAutenticacaoMin")
            Dim authTicket = New FormsAuthenticationTicket(1, idNetwork, DateTime.Now, DateTime.Now.AddMinutes(tempo), lembrar, usuario)
            Dim encryptedTicket As String = FormsAuthentication.Encrypt(authTicket)
            Dim authCookie = New HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)

            Return authCookie
        End Function

        'Private Function AutenticaUsuarioAD(dadosLogin As LoginViewModel) As Boolean
        '    Try
        '        'Dim endAD As New DirectoryEntry("LDAP://mrtw2k", txtUsu.Text.Trim, txtPwd.Text.ToString())
        '        Dim endAD As New DirectoryEntry("LDAP://interacao.com.br", dadosLogin.Usuario, dadosLogin.Senha)
        '        Dim buscaAD As New DirectorySearcher(endAD)
        '        buscaAD.Filter = "(sAMAccountName=" & dadosLogin.Usuario & ")"
        '        '' '' ''buscaAD.PropertiesToLoad.Add("cn")
        '        '' '' ''buscaAD.PropertiesToLoad.Add("MemberOf")
        '        Dim resultado As SearchResult
        '        resultado = buscaAD.FindOne()

        '        Dim grupos(resultado.Properties.Item("MemberOf").Count - 1) As String

        '        For x As Integer = 0 To resultado.Properties.Item("MemberOf").Count - 1
        '            grupos(x) = resultado.Properties("MemberOf").Item(x).ToString().Split(","c)(0).Replace("CN=", "")
        '        Next

        '        Dim gruposAdUsuario = grupos


        '        Return True 'Usuário foi autenticado no AD (Login e Senha corretos)
        '    Catch ex As Exception
        '        Dim gruposAdUsuario = Nothing
        '        Return False 'Usuário não foi autenticado no AD (Login e/ou Senha incorretos)
        '    End Try
        'End Function

        'Public Function VerificaUsuarioPossuiGrupoSAC() As Boolean
        '    Dim sr As New StreamReader(Server.MapPath("~/Mvc.sitemap"))
        '    Dim dstGruposSac As New DataSet
        '    Dim GruposSac As New ArrayList
        '    Dim grupos() As String
        '    Dim descricaoGrupo As String
        '    Dim possuiAceeso As Boolean = False
        '    Dim gruposAdUsuario = ""

        '    Try
        '        dstGruposSac.ReadXml((XmlReader.Create(sr)))
        '        For Each linhaGrupo As DataRow In dstGruposSac.Tables(0).Rows
        '            descricaoGrupo = linhaGrupo("roles").ToString.Replace("MRTW2K\", "")
        '            If descricaoGrupo.Trim.Length > 0 AndAlso descricaoGrupo <> "*" AndAlso Not GruposSac.Contains(descricaoGrupo) Then
        '                grupos = descricaoGrupo.Split(","c)
        '                For Each nomeGrupo As String In grupos
        '                    If Not GruposSac.Contains(nomeGrupo) Then
        '                        GruposSac.Add(nomeGrupo.Trim)
        '                    End If
        '                Next
        '            End If
        '        Next

        '        For Each grupo As String In GruposSac
        '            If gruposAdUsuario.Contains(grupo) Then
        '                possuiAceeso = True
        '                Exit For
        '            End If
        '        Next

        '        Return possuiAceeso
        '    Finally
        '        dstGruposSac.Dispose()
        '        sr.Close()
        '        sr.Dispose()
        '        dstGruposSac = Nothing
        '        sr = Nothing
        '    End Try
        'End Function

        <AllowAnonymous>
        Public Function LogOff() As ActionResult
            FormsAuthentication.SignOut()

            Session.Abandon()

            Return RedirectToAction("Index", "Login")

        End Function

        Private Function BuscaDadosUsuario(usuarioRede As String, grupoAD As String) As String

            Dim DadosUsuario As String = Nothing
            Dim boUser As New RelacaoFuncionarioUserRacfAcountBO
            Dim caBo As New ControleAcessoBO
            Dim RelacaoFuncionarioUserRacfAcount = boUser.BuscaPorUsuarioRede(New VO.RelacaoFuncionarioUserRacfAcount With {.NOMUSRRCF = usuarioRede}).FirstOrDefault()

            If (RelacaoFuncionarioUserRacfAcount Is Nothing) Then
                ModelState.AddModelError("LoginInvalido", "Usuário não encontrado")
            Else
                Dim controleAcesso As List(Of VO.ControleAcesso)

                If ((Session("IsMobile") = True)) Then
                    controleAcesso = New List(Of VO.ControleAcesso)
                    controleAcesso.Add(New VO.ControleAcesso() With {.CODMNU = "39"}) 'Aprovações
                    controleAcesso.Add(New VO.ControleAcesso() With {.CODMNU = "40"}) 'Minhas Aprovações
                Else
                    controleAcesso = caBo.ControleAcessoUsuarioGrupoAcesso(grupoAD)
                End If

                DadosUsuario = String.Format("{0}|{1}", RelacaoFuncionarioUserRacfAcount.CODFNC, String.Join(";", controleAcesso.Select(Function(m) m.CODMNU.Trim.ToString())))
            End If
            Return DadosUsuario
        End Function
    End Class
End Namespace
