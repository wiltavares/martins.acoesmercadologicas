﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList

Namespace Controllers
    <Authorize(Roles:="3,13")>
    Public Class ControleAcessoController
        Inherits AbstractController
        ' Private funcionarioBO As New FuncionarioBO()
        Private grupoAcessoBO As New GrupoAcessoBO()
        Private _ControleAcessoBO As New ControleAcessoBO()

        ' GET: /Grupo de Acesso
        ''' <summary>
        ''' Carrega a tela de consulta
        ''' </summary>
        ''' <returns></returns>
        Function Index() As ViewResult

            Return View(GridSettings(Of GrupoAcessoViewModel).EmptyGrid())
        End Function

        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of GrupoAcessoViewModel)) As PartialViewResult
            Try

                Dim GrupoAcesso As VO.GrupoAcesso = Mapper.Map(Of GrupoAcessoViewModel, VO.GrupoAcesso)(dadosPesquisa.Filter)

                Dim grupoAcessoList As List(Of VO.GrupoAcesso) = grupoAcessoBO.ListGruposAcesso(GrupoAcesso)
                Dim grupoAcessoViewModel As List(Of GrupoAcessoViewModel) = Mapper.Map(Of List(Of VO.GrupoAcesso), List(Of GrupoAcessoViewModel))(grupoAcessoList)


                Dim gridConf = PaginarEAtualizarGrid(grupoAcessoViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If
                Return PartialView("~/Views/ControleAcesso/_ResultadosView.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Function DefinirAcesso(ByVal id As Decimal) As ActionResult
            Try
                Dim filtro As New VO.GrupoAcesso()
                filtro.CODGRPACS = id

                Dim GrupoAcesso = grupoAcessoBO.ListGruposAcesso(filtro)(0)
                Dim GrupoAcessoViewModel = Mapper.Map(Of VO.GrupoAcesso, GrupoAcessoViewModel)(GrupoAcesso)

                GrupoAcessoViewModel.CodMenuCorrente = 0

                GrupoAcessoViewModel.ControleAcessos = Mapper.Map(Of List(Of VO.ControleAcesso), List(Of ControleAcessoViewModel))(_ControleAcessoBO.ListarAcessosGrupo(id))

                Return View(GrupoAcessoViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Function Salvar(ByVal CodGrup As Decimal?, ByVal MenusSelecionados As List(Of Decimal))
            Try
                Dim ControleAcessos As List(Of VO.ControleAcesso) = New List(Of VO.ControleAcesso)

                If Not (MenusSelecionados Is Nothing) Then

                    For Each m As Decimal In MenusSelecionados
                        Dim controle As VO.ControleAcesso = New VO.ControleAcesso With {.CODGRPACS = CodGrup, .CODMNU = m}
                        ControleAcessos.Add(controle)
                    Next
                End If

                _ControleAcessoBO.SalvaDados(ControleAcessos, CodGrup)
                Information(Constantes.REGISTRO_ALTERADO_SUCESSO)

                Return DefinirAcesso(CodGrup)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Function CarregaAcesso(ByVal matricula As Decimal)
        '    Dim filtro As New VO.Funcionario()
        '    filtro.CODFNC = matricula

        '    Dim FuncionarioList As List(Of VO.Funcionario) = FuncionarioBO.CarregaAcesso(filtro)
        '    Dim FuncionarioViewModel As List(Of FuncionarioViewModel) = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(FuncionarioList)

        '    Return View(FuncionarioViewModel)
        'End Function

    End Class
End Namespace