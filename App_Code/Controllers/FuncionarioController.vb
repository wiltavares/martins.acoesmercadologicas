﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports Martins.AcoesMercadologicas
Imports PagedList


Namespace Controllers
    Public Class FuncionarioController
        Inherits AbstractController
        Private funcionarioBO As New FuncionarioBO()

        ' GET: Funcionario 
        Function Index(page As System.Nullable(Of Integer), Optional ByVal pageSize As Integer = 5) As ViewResult

            Dim filtro As New VO.Funcionario()
            Dim funcionario As List(Of VO.Funcionario) = funcionarioBO.ListT(filtro)
            Dim pageNumber = If(page, 1)
            Dim onePageOfProducts As IPagedList = funcionario.ToPagedList(pageNumber, pageSize)
            Dim funcionarioViewModel As List(Of FuncionarioViewModel) = Mapper.Map(Of IEnumerable(Of VO.Funcionario), IEnumerable(Of FuncionarioViewModel))(funcionario)

            Dim fullObject = From a In funcionarioViewModel
                             Select New FuncionarioViewModel() With {.CODFNC = a.CODFNC, .NOMFNC = a.NOMFNC, .CODDPT = a.CODDPT, .CODSEC = a.CODSEC, .CODEMP = a.CODEMP, .DATADSFNC = a.DATADSFNC, .DATDEMFNC = a.DATDEMFNC,
            .ENDFNC = a.ENDFNC, .CODCID = a.CODCID, .CODBAIFNC = a.CODBAIFNC, .CODCEPFNC = a.CODCEPFNC, .NUMTLFFNC = a.NUMTLFFNC, .CODFILFNC = a.CODFILFNC, .CODSEXFNC = a.CODSEXFNC, .CODDRT = a.CODDRT, .CODDIV = a.CODDIV,
            .CODCENCST = a.CODCENCST, .CODGRPPGTFNC = a.CODGRPPGTFNC, .CODSETFNC = a.CODSETFNC, .NUMCNTCRRFNC = a.NUMCNTCRRFNC, .CODAGEBCOFNC = a.CODAGEBCOFNC, .CODBCOFNC = a.CODBCOFNC, .CODCGRFNC = a.CODCGRFNC,
            .NUMCPFFNC = a.NUMCPFFNC, .CODSNDFNC = a.CODSNDFNC, .DATNSC = a.DATNSC, .CODUNDNGC = a.CODUNDNGC, .CODFILEMP = a.CODFILEMP, .CODUNDREG = a.CODUNDREG, .CODSGMMCDCTB = a.CODSGMMCDCTB, .NUMRMLTLF = a.NUMRMLTLF,
            .DESPSWFNCPAD = a.DESPSWFNCPAD, .DESPSWCDF = a.DESPSWCDF, .DESENDCREETNFNC = a.DESENDCREETNFNC, .CODNIVFNC = a.CODNIVFNC, .INDTRBFNC = a.INDTRBFNC, .NUMCARCCP = a.NUMCARCCP, .NUMCARRLP = a.NUMCARRLP,
            .DATVLDCARCCP = a.DATVLDCARCCP, .DATVLDCARRLP = a.DATVLDCARRLP, .DATPRVDEMFNC = a.DATPRVDEMFNC, .NUMCPF = a.NUMCPF, .DATALTCADFNC = a.DATALTCADFNC, .TIPFNC = a.TIPFNC, .NOMFNCEXD = a.NOMFNCEXD, .INDEQIFNC = a.INDEQIFNC,
            .NUMENDFNC = a.NUMENDFNC, .CODEDEOCDRDF = a.CODEDEOCDRDF}

            ViewBag.OnePageOfProducts = onePageOfProducts

            fullObject = fullObject.ToPagedList(pageNumber, pageSize)

            Return View(fullObject)

        End Function
    End Class
End Namespace
