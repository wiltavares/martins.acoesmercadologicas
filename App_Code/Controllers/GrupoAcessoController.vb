﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList

Namespace Controllers
    <Authorize(Roles:="37")>
    Public Class GrupoAcessoController
        Inherits AbstractController
        Private grupoAcessoBO As New GrupoAcessoBO
        Private _ControleAcessoBO As New ControleAcessoBO()

        ''' <summary>
        ''' Carrega a tela de consulta
        ''' </summary>
        ''' <returns></returns>
        Function Index() As ViewResult
            Return View(GridSettings(Of GrupoAcessoViewModel).EmptyGrid())
        End Function

        ''' <summary>
        ''' Consulta registros a partir do filtro da pesquisa
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of GrupoAcessoViewModel)) As PartialViewResult
            Try
                Dim filtro As VO.GrupoAcesso = Mapper.Map(Of GrupoAcessoViewModel, VO.GrupoAcesso)(dadosPesquisa.Filter)

                filtro.NOMSISINF = "AI                  "
                Dim listaGrupoAcesso As List(Of VO.GrupoAcesso) = grupoAcessoBO.ListT(filtro)
                Dim listaGrupoAcessoViewModel As List(Of GrupoAcessoViewModel) = Mapper.Map(Of List(Of VO.GrupoAcesso), List(Of GrupoAcessoViewModel))(listaGrupoAcesso)

                Dim gridAtualizado = PaginarEAtualizarGrid(listaGrupoAcessoViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridAtualizado.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If
                Return PartialView("~/Views/GrupoAcesso/_ResultadosView.vbhtml", gridAtualizado)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Função Para decidir se a ação é de criação ou edição
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        Function CreateEdit(Optional ByVal id As Integer? = Nothing) As ActionResult

            If id Is Nothing Then
                ViewBag.acao = "Save"
                ViewBag.tela = "Incluir"
                Return Create()
            Else
                ViewBag.acao = "Alterar"
                Return Edit(id)
            End If
        End Function

        ''' <summary>
        ''' Criar novo registro
        ''' </summary>
        ''' <returns></returns>
        Function Create() As ActionResult
            Dim grupoAcesso As GrupoAcessoViewModel = New GrupoAcessoViewModel()

            grupoAcesso.CODGRPACS = grupoAcessoBO.GetNextCODGRPACS()

            Return View(grupoAcesso)
        End Function

        ''' <summary>
        ''' Editar resgistro existente
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        Function Edit(ByVal id As Integer) As ActionResult

            Dim filtro As New VO.GrupoAcesso()
            filtro.CODGRPACS = id
            filtro.NOMSISINF = "AI                  "

            Dim grupoAcesso = grupoAcessoBO.SelectT(filtro)
            grupoAcesso.DESGRPACS = grupoAcesso.DESGRPACS.Trim()
            grupoAcesso.DESGRPRDE = grupoAcesso.DESGRPRDE.Trim()
            Dim eventoViewModel = Mapper.Map(Of VO.GrupoAcesso, GrupoAcessoViewModel)(grupoAcesso)

            Return View(eventoViewModel)
        End Function


        ''' <summary>
        ''' Altera registro
        ''' </summary>
        ''' <param name="grupoAcessoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Alterar(ByVal grupoAcessoViewModel As GrupoAcessoViewModel) As ActionResult
            Try

                If (ModelState.IsValid) Then
                    Dim grupoAcesso = Mapper.Map(Of GrupoAcessoViewModel, VO.GrupoAcesso)(grupoAcessoViewModel)
                    grupoAcesso.NOMSISINF = "AI                  "
                    grupoAcessoBO.UpdateT(grupoAcesso)
                    Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
                    Return RedirectToAction("Index")

                End If

                Return View("CreateEdit", grupoAcessoViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function



        ''' <summary>
        ''' Salvar novo registro
        ''' </summary>
        ''' <param name="grupoAcessoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Save(ByVal grupoAcessoViewModel As GrupoAcessoViewModel) As ActionResult
            Try
                If (ModelState.IsValid) Then
                    Dim grupoAcesso = Mapper.Map(Of GrupoAcessoViewModel, VO.GrupoAcesso)(grupoAcessoViewModel)
                    grupoAcesso.NOMSISINF = "AI                  "
                    grupoAcessoBO.InsertT(grupoAcesso)
                    Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                    Return RedirectToAction("Index")
                End If

                Return View("CreateEdit", grupoAcessoViewModel)
            Catch
                Return View()
            End Try
        End Function
    End Class

   
End Namespace