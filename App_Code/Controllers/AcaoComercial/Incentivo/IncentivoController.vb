﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers

Namespace Controllers.AcaoComercial.Incentivo
    <Authorize(Roles:="12,17,14")>
    Public Class IncentivoController
        Inherits AbstractController

        Private boIncentivo As New RelacaoIncentivoxAcaoComercialBO
        Private boParametrosGeraisDoIncentivoDeVendas As New ParametrosGeraisDoIncentivoDeVendasBO

        ''' <summary>
        ''' Abre o modal de inserir Incentivo
        ''' </summary>
        ''' <param name="codAcao"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function InserirIncentivo(Optional codAcao As Decimal? = Nothing) As PartialViewResult
            Try
                ViewBag.Incentivo = New List(Of ParametrosGeraisDoIncentivoDeVendasViewModel)

                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                If (Not IsNothing(acaoModel.Incentivos) AndAlso acaoModel.Incentivos.Count > 0) Then

                    Dim listaSelecionadoseSalvos As New List(Of ParametrosGeraisDoIncentivoDeVendasViewModel)
                    For Each item As RelacaoIncentivoxAcaoComercialViewModel In acaoModel.Incentivos
                        Dim itemParaConverter As New ParametrosGeraisDoIncentivoDeVendasViewModel
                        itemParaConverter.CODICT = item.CODICT
                        itemParaConverter.DESICT = item.DESICT
                        listaSelecionadoseSalvos.Add(itemParaConverter)
                    Next
                    ViewBag.GridIncentivosSelecionados = GetGridIncentivosSelecionadosModal()
                    ViewBag.GridIncentivosSelecionados.ListPaged = PaginarLista(listaSelecionadoseSalvos)
                    ViewBag.GridIncentivosSelecionados.TotalCount = listaSelecionadoseSalvos.Count
                    Session("listaTempIncentivosSelecionados") = listaSelecionadoseSalvos

                Else
                    Dim incentivos As New List(Of ParametrosGeraisDoIncentivoDeVendasViewModel)
                    ViewBag.GridIncentivosSelecionados = GetGridIncentivosSelecionadosModal()
                    ViewBag.GridIncentivosSelecionados.ListPaged = PaginarLista(incentivos)
                    ViewBag.GridIncentivosSelecionados.TotalCount = incentivos.Count
                    Session("listaTempIncentivosSelecionados") = incentivos
                End If

                ViewBag.GridIncentivoModal = GetGridIncentivoModal()

                Return PartialView("~/Views/AcaoComercial/Incentivo/_ModalInserirIncentivo.vbhtml", New ParametrosGeraisDoIncentivoDeVendasViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Retorna um grid vazio com as configuracoes de grid de Incentivo do modal
        ''' </summary>
        ''' <returns></returns>
        Function GetGridIncentivoModal() As GridSettings(Of ParametrosGeraisDoIncentivoDeVendasViewModel)
            Return GridSettings(Of ParametrosGeraisDoIncentivoDeVendasViewModel).EmptyGrid("gridIncentivoModal", "AtualizarGridIncentivoModal", "Incentivo")
        End Function


        ''' <summary>
        ''' Atualiza o grid de Incentivos selecionados na tela de inserir Incentivo
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridIncentivoModal(dadosPesquisa As GridSettings(Of ParametrosGeraisDoIncentivoDeVendasViewModel)) As PartialViewResult
            Try
                Dim listaTempIncentivos As List(Of ParametrosGeraisDoIncentivoDeVendasViewModel) = Session.Item("listaTempIncentivo")
                Dim gridConf = PaginarEAtualizarGrid(listaTempIncentivos, dadosPesquisa.Page, dadosPesquisa.Filter, GetGridIncentivoModal)
                gridConf.TotalCount = listaTempIncentivos.Count

                Return PartialView("~/Views/AcaoComercial/Incentivo/_GridIncentivoModal.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Filtrar Incentivos de acordo com nome ou código
        ''' </summary>
        ''' <param name="incentivo"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function FiltrarIncentivos(incentivo As ParametrosGeraisDoIncentivoDeVendasViewModel) As PartialViewResult
            Try
                If (Not IsNothing(incentivo) AndAlso (IsNothing(incentivo.CODICT) AndAlso IsNothing(incentivo.DESICT))) Then
                    Warning(Constantes.INFORME_FILTRO)
                    Dim gridConfVazio = GetGridIncentivoModal()
                    Return PartialView("~/Views/AcaoComercial/Incentivo/_GridIncentivoModal.vbhtml", gridConfVazio)
                End If
                Dim incentivos = boParametrosGeraisDoIncentivoDeVendas.BuscarIncentivos(incentivo.CODICT, incentivo.DESICT)
                If incentivos Is Nothing Or incentivos.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    Return Nothing
                End If

                Dim incentivosLista = Mapper.Map(Of List(Of VO.ParametrosGeraisDoIncentivoDeVendas), List(Of ParametrosGeraisDoIncentivoDeVendasViewModel))(incentivos)
                Session("listaTempIncentivo") = incentivosLista

                Dim gridConf = GetGridIncentivoModal()
                gridConf.ListPaged = PaginarLista(incentivosLista)
                gridConf.TotalCount = incentivosLista.Count
                Return PartialView("~/Views/AcaoComercial/Incentivo/_GridIncentivoModal.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Retorna um grid vazio com as configuracoes de grid de Incentivos selecionados
        ''' </summary>
        ''' <returns></returns>
        Function GetGridIncentivosSelecionadosModal() As GridSettings(Of ParametrosGeraisDoIncentivoDeVendasViewModel)
            Return GridSettings(Of ParametrosGeraisDoIncentivoDeVendasViewModel).EmptyGrid("gridIncentivosSelecionadosModal", "AtualizarGridIncentivosSelecionados", "Incentivo")
        End Function

        ''' <summary>
        ''' Atualiza o grid de Incentivos selecionados na tela de inserir Incentivo
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridIncentivosSelecionados(dadosPesquisa As GridSettings(Of ParametrosGeraisDoIncentivoDeVendasViewModel)) As PartialViewResult
            Try
                Dim listaTempIncentivosSelecionados As List(Of ParametrosGeraisDoIncentivoDeVendasViewModel) = Session.Item("listaTempIncentivosSelecionados")
                Dim gridConf = PaginarEAtualizarGrid(listaTempIncentivosSelecionados, dadosPesquisa.Page, dadosPesquisa.Filter, GetGridIncentivosSelecionadosModal)
                gridConf.TotalCount = listaTempIncentivosSelecionados.Count

                Return PartialView("~/Views/AcaoComercial/Incentivo/_GridIncentivosSelecionadosModal.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Selecionar incentivos checkados e adicionar na lista de incentivos selecionados
        ''' </summary>
        ''' <param name="itensCheckados"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function SelecionarIncentivos(ByVal itensCheckados As List(Of Decimal)) As PartialViewResult
            Try
                Dim listaTempIncentivosSelecionados As List(Of ParametrosGeraisDoIncentivoDeVendasViewModel) = Session.Item("listaTempIncentivosSelecionados")
                Dim listaTempIncentivo As List(Of ParametrosGeraisDoIncentivoDeVendasViewModel) = Session.Item("listaTempIncentivo")

                'Verifica se o Incentivo ja foi inserido
                For Each item In itensCheckados
                    If IncentivoJaExistente(listaTempIncentivosSelecionados, item) Then
                        Warning(Constantes.INCENTIVO_EXISTENTE)
                        Return Nothing
                    End If
                Next

                Dim IncentivosParaAdd = listaTempIncentivo.Where(Function(f) itensCheckados.Contains(f.CODICT)).ToList()
                listaTempIncentivosSelecionados.AddRange(IncentivosParaAdd)

                Dim gridConf = GetGridIncentivosSelecionadosModal()
                gridConf.ListPaged = PaginarLista(listaTempIncentivosSelecionados)
                gridConf.TotalCount = listaTempIncentivosSelecionados.Count

                Return PartialView("~/Views/AcaoComercial/Incentivo/_GridIncentivosSelecionadosModal.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Verifica se um Incentivo ja existe em uma lista
        ''' </summary>
        ''' <param name="lista"></param>
        ''' <param name="codInc"></param>
        ''' <returns></returns>
        Public Function IncentivoJaExistente(lista As List(Of ParametrosGeraisDoIncentivoDeVendasViewModel), codInc As Decimal) As Boolean
            If lista IsNot Nothing AndAlso lista.Count > 0 Then
                For i = 0 To lista.Count - 1
                    If (lista(i).CODICT = codInc) Then
                        Return True
                    End If
                Next
            End If
            Return False
        End Function


        ''' <summary>
        ''' Remover incentivo checkados da lista de incentivos selecionados
        ''' </summary>
        ''' <param name="itensCheckados"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RemoverIncentivos(ByVal itensCheckados As List(Of Decimal)) As PartialViewResult
            Try
                Dim listaTempIncentivosSelecionados As List(Of ParametrosGeraisDoIncentivoDeVendasViewModel) = Session.Item("listaTempIncentivosSelecionados")
                listaTempIncentivosSelecionados = listaTempIncentivosSelecionados.Where(Function(f) Not itensCheckados.Contains(f.CODICT)).ToList()
                Session("listaTempIncentivosSelecionados") = listaTempIncentivosSelecionados

                Dim gridConf = GetGridIncentivosSelecionadosModal()
                gridConf.ListPaged = PaginarLista(listaTempIncentivosSelecionados)
                gridConf.TotalCount = listaTempIncentivosSelecionados.Count

                Return PartialView("~/Views/AcaoComercial/Incentivo/_GridIncentivosSelecionadosModal.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Concluir adicao de Incentivos selecionados
        ''' </summary>
        Public Sub ValidarIncentivos()
            Try
                Dim listaTempIncentivosSelecionados As List(Of ParametrosGeraisDoIncentivoDeVendasViewModel) = Session.Item("listaTempIncentivosSelecionados")

                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim listaIncentivosAdd = New List(Of RelacaoIncentivoxAcaoComercialViewModel)
                If (Not IsNothing(listaTempIncentivosSelecionados)) Then
                    For Each incentivo As ParametrosGeraisDoIncentivoDeVendasViewModel In listaTempIncentivosSelecionados
                        Dim novo_incentivo = New RelacaoIncentivoxAcaoComercialViewModel
                        novo_incentivo.CODACOCMC = acaoModel.CODACOCMC
                        novo_incentivo.DESICT = incentivo.DESICT
                        novo_incentivo.CODICT = incentivo.CODICT
                        listaIncentivosAdd.Add(novo_incentivo)
                    Next
                    acaoModel.Incentivos = listaIncentivosAdd
                End If
            Catch ex As Exception
                Danger(ex)
            End Try
        End Sub

    End Class
End Namespace