﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers

Namespace Controllers
    <Authorize(Roles:="12,17,14")>
    Public Class CloneController
        Inherits AbstractController

        ''' <summary>
        ''' Acao do botao de Clonar acao mercadologica
        ''' </summary>
        ''' <returns></returns>
        Function Clonar(ByVal objeto As List(Of Decimal)) As ActionResult
            Try
                Return View("~/Views/AcaoComercial/Clone/_CloneAcao.vbhtml", New CloneViewModel())

            Catch mappedWarn As MappedWarnException
                Warning(mappedWarn)
                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Acao do botao de salvar acao mercadologica
        ''' </summary>
        ''' <param name="CloneViewModel"></param>
        <HttpPost()>
        Public Function CriarClone(ByVal cloneViewModel As CloneViewModel) As JavaScriptResult
            Try
                If (cloneViewModel.CODACOCMC Is Nothing) Then
                    Warning("Informe o Cód. da Ação.")
                    Return Nothing
                ElseIf (cloneViewModel.INDITEMORCAMENTO = False And cloneViewModel.INDMECANICA = False) Then
                    Warning("Escolha uma das opções para clonar.")
                    Return Nothing
                End If

                Dim bo As New AcaoComercialBO()
                Dim acao = bo.CriarClone(cloneViewModel.CODACOCMC, cloneViewModel.INDITEMORCAMENTO, cloneViewModel.INDMECANICA, ControleAcesso.CodFnc())

                If (acao Is Nothing) Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    Return Nothing
                End If
                Dim action As String = "CreateEdit"
                Select Case acao.TIPACOMCD
                    Case TipoAcao.PreAcordoVigencia
                        action = "PreAcordoCreateEditVigencia"
                    Case TipoAcao.PreAcordoValor
                        action = "PreAcordoCreateEditValor"
                End Select

                Return JavaScript(String.Format("setTimeout(function(){{ window.location = SiteUrl + 'AcaoComercial/{1}/{0}'; }}, 3000);", acao.CODACOCMC, action))
                'Return JavaScript(String.Format("window.location = SiteUrl + 'AcaoComercial/CreateEdit/{0}';", acao.CODACOCMC))

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function
    End Class
End Namespace