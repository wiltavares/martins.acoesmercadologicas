﻿Imports System.Web.Mvc
Imports AutoMapper
Imports System.Data

Namespace Controllers

    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class AcaoComercialController
        Inherits AbstractController

#Region "BOs"

        Private bo As New AcaoComercialBO()
        Private boFornecedor As New FornecedoresBO()
        Private boFornecedores As New RelacaoAcaoComercialxFornecedoresBO()
        Private boItensOrcamento As New RelacaoAcaoComercialPorItemOrcamentoBO()
        Private boCarimbos As New RelacaoAcaoxFornecedorxCarimboBO()
        Private boAauthor As New FuncionarioBO()
        Private boTipoEvento As New TipoDeEventoBO()
        Private boEvento As New EventoBO()
        Private boBU As New UnidadeEstrategicaDeNegocioBO()
        Private boEmpenho As New DestinoDescontoPorBonificacaoPedidoCompraBO
        Private boPlanoMarketing As New PlanoDeMarketingBO
        Private boRelacaoAcaoComercialxPlanoMarketing As New RelacaoAcaoComercialxPlanoMarketingBO
        Private boParametros As New ParametroSistemaAdminitracaoVendaBO
        Private boRelacaoEquipeDeComprasxGerenteNegocioServCompras As New RelacaoEquipeDeComprasxGerenteNegocioServComprasBO
        Private boDiretoriaCompra As New DiretoriaCompraBO
        Private boCanalVenda As New CanalDeVendaInicialBO
        Private boUnidadeNegocio As New UnidadeEstrategicaDeNegocioBO
        Private boFluxoAprovacao As New RelacaoAcaoComercialxFluxoAprovacaoBO
        Private boPoliticas As New PoliticasDaAcaoComercialBO
        Private boPedidoBonificacao As New RelacaoAcaoComercialXPedidoBonificadoBO()
        Private boRelacaoAcaoComercialxPolitica As RelacaoAcaoComercialxPoliticaBO
        Private boRelacaoPlanoMarketing As New RelacaoAcaoComercialxPlanoMarketingBO
        Private boperPeopple As New perAllPeopleFBO()
        Private boFluxoDeAprovacao As New FluxoDeAprovacaoBO()
        Private boFilialEmpresaCorporativo As New FilialEmpresaCorporativoBO()
        Private boEventoPromocional As New EventoPromocionalBO()


#End Region

        Function Index() As RedirectResult
            Return New RedirectResult("Home")
        End Function

#Region "Consulta"

        Function Aprovacoes() As ViewResult
            Try
                'Session("IsMobile") = Util.IsMobile(Request)
                CarregaListasDropDownIndex(Nothing)

                Dim acao = New AcaoComercialViewModel With {.CODSTAAPVACOCMC = Constantes.Status.MINHAS_APROVACOES}
                'acao.TIPACOMCD = 1 'Acao
                Dim grid As New GridSettings(Of AcaoComercialViewModel)
                grid.Filter = acao
                grid = BuscaAcaoGrid(grid)
                grid.ActionName = "AprovacoesPesquisar"
                Return View("~/Views/AcaoComercial/Index.vbhtml", grid)
            Catch ex As Exception
                Danger(ex)
                Return View("~/Views/AcaoComercial/Index.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())
            End Try
        End Function

        Function AprovacoesPesquisar(dadosPesquisa As GridSettings(Of AcaoComercialViewModel)) As PartialViewResult
            Try
                If (IsNothing(dadosPesquisa.Filter.DATINIACOCMC) And Not IsNothing(dadosPesquisa.Filter.DataIniAux)) Then
                    dadosPesquisa.Filter.DATINIACOCMC = CDate(dadosPesquisa.Filter.DataIniAux)
                End If
                If (IsNothing(dadosPesquisa.Filter.DATFIMACOCMC) And Not IsNothing(dadosPesquisa.Filter.DataFimAux)) Then
                    dadosPesquisa.Filter.DATFIMACOCMC = CDate(dadosPesquisa.Filter.DataFimAux)
                End If

                Dim acao As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(dadosPesquisa.Filter)

                If acao.CODACOCMC Is Nothing AndAlso acao.NOMACOCMC Is Nothing AndAlso acao.CODFNCCRIACOCMC Is Nothing _
                    AndAlso acao.CODSTAAPVACOCMC Is Nothing AndAlso acao.TIPEVTACOCMC Is Nothing AndAlso acao.CODEVTACOCMC _
                    Is Nothing AndAlso acao.DATINIACOCMC Is Nothing AndAlso acao.DATFIMACOCMC Is Nothing Then

                    Warning(Constantes.FILTRO_OBRIGATORIO)
                    Return PartialView("~/Views/AcaoComercial/_ResultadosView.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())

                End If
                Session("AcaoFiltro") = dadosPesquisa.Filter

                dadosPesquisa = BuscaAcaoGrid(dadosPesquisa)

                dadosPesquisa.ActionName = "AprovacoesPesquisar"
                Return PartialView("~/Views/AcaoComercial/_ResultadosView.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        Private Function BuscaAcaoGrid(dadosPesquisa As GridSettings(Of AcaoComercialViewModel)) As GridSettings(Of AcaoComercialViewModel)
            Dim acao As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(dadosPesquisa.Filter)
            Dim acoes As List(Of VO.AcaoComercial) = bo.ListarAcaoComercial(acao, ControleAcesso.CodFnc())
            Dim acoesViewModel As List(Of AcaoComercialViewModel) = Mapper.Map(Of List(Of VO.AcaoComercial), List(Of AcaoComercialViewModel))(acoes)

            ViewBag.usuarioPodeEstornar = bo.ConsultaAutorizacaoEstorno(ControleAcesso.CodFnc())

            If (dadosPesquisa.Page.OrderBy Is Nothing) Then
                dadosPesquisa.Page.OrderBy = "CODACOCMCD"
            End If

            Dim gridConf = PaginarEAtualizarGrid(acoesViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
            If gridConf.TotalCount = 0 Then
                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
            End If
            Return gridConf
        End Function
#Region "Acao"

        ''' <summary>
        ''' Tela inicial de pesquisa
        ''' </summary>
        ''' <returns></returns>
        Function ConsultaAcao() As ViewResult
            Try
                CarregaListasDropDownIndex(False)

                Dim acaoView As AcaoComercialViewModel = Session("AcaoFiltro")

                If acaoView IsNot Nothing Then

                    Dim grid As New GridSettings(Of AcaoComercialViewModel)
                    grid.Filter = acaoView

                    Return View("~/Views/AcaoComercial/Index.vbhtml", BuscaAcaoGrid(grid))
                Else
                    Return View("~/Views/AcaoComercial/Index.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())
                End If
            Catch ex As Exception
                Danger(ex)
                Return View("~/Views/AcaoComercial/Index.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())
            End Try
        End Function

        Function MinhasAprovacoes() As ViewResult
            Try
                'Session("IsMobile") = Util.IsMobile(Request)
                CarregaListasDropDownIndex(False)

                Dim acao = New AcaoComercialViewModel With {.CODSTAAPVACOCMC = Constantes.Status.MINHAS_APROVACOES}
                acao.TIPACOMCD = 1 'Acao
                Dim grid As New GridSettings(Of AcaoComercialViewModel)
                grid.Filter = acao

                Return View("~/Views/AcaoComercial/Index.vbhtml", BuscaAcaoGrid(grid))
            Catch ex As Exception
                Danger(ex)
                Return View("~/Views/AcaoComercial/Index.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())
            End Try
        End Function

        Function Consulta() As ViewResult
            Try
                CarregaListasDropDownIndex(False)

                Return View("~/Views/AcaoComercial/Index.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())
            Catch ex As Exception
                Danger(ex)
                Return View("~/Views/AcaoComercial/Index.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())
            End Try
        End Function

        ''' <summary>
        ''' Acao do botao de pesquisar da tela de pesquisa de acao comercial
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of AcaoComercialViewModel)) As PartialViewResult
            Try
                If (IsNothing(dadosPesquisa.Filter.DATINIACOCMC) And Not IsNothing(dadosPesquisa.Filter.DataIniAux)) Then
                    dadosPesquisa.Filter.DATINIACOCMC = CDate(dadosPesquisa.Filter.DataIniAux)
                End If
                If (IsNothing(dadosPesquisa.Filter.DATFIMACOCMC) And Not IsNothing(dadosPesquisa.Filter.DataFimAux)) Then
                    dadosPesquisa.Filter.DATFIMACOCMC = CDate(dadosPesquisa.Filter.DataFimAux)
                End If

                If (IsNothing(dadosPesquisa.Filter.TIPACOMCD)) Then 'Acao
                    dadosPesquisa.Filter.TIPACOMCD = 1
                End If

                Dim acao As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(dadosPesquisa.Filter)

                If acao.CODACOCMC Is Nothing AndAlso acao.NOMACOCMC Is Nothing AndAlso acao.CODFNCCRIACOCMC Is Nothing _
                    AndAlso acao.CODSTAAPVACOCMC Is Nothing AndAlso acao.TIPEVTACOCMC Is Nothing AndAlso acao.CODEVTACOCMC _
                    Is Nothing AndAlso acao.DATINIACOCMC Is Nothing AndAlso acao.DATFIMACOCMC Is Nothing Then

                    Warning(Constantes.FILTRO_OBRIGATORIO)
                    Return PartialView("~/Views/AcaoComercial/_ResultadosView.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())

                End If
                Session("AcaoFiltro") = dadosPesquisa.Filter

                Return PartialView("~/Views/AcaoComercial/_ResultadosView.vbhtml", BuscaAcaoGrid(dadosPesquisa))
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function
#End Region

#Region "Pré-Acordo"

        ''' <summary>
        ''' Tela inicial de pesquisa Pré-Acordo
        ''' </summary>
        ''' <returns></returns>
        Function ConsultaAcaoPreAcordo() As ViewResult
            Try
                CarregaListasDropDownIndex(True)

                Dim acaoView As AcaoComercialViewModel = Session("AcaoFiltro")

                If acaoView IsNot Nothing Then

                    Dim grid As New GridSettings(Of AcaoComercialViewModel)
                    grid.Filter = acaoView

                    Return View("~/Views/AcaoComercial/Index.vbhtml", BuscaAcaoGrid(grid))
                Else
                    Return View("~/Views/AcaoComercial/Index.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())
                End If
            Catch ex As Exception
                Danger(ex)
                Return View("~/Views/AcaoComercial/Index.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())
            End Try
        End Function

        Function ConsultaPreAcordo() As ViewResult
            Try
                CarregaListasDropDownIndex(True)

                Return View("~/Views/AcaoComercial/Index.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())
            Catch ex As Exception
                Danger(ex)
                Return View("~/Views/AcaoComercial/Index.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())
            End Try
        End Function

        ''' <summary>
        ''' Tela inicial de pesquisa
        ''' </summary>
        ''' <returns></returns>
        Function PreAcordoConsultar() As ViewResult
            Return ConsultaAcaoPreAcordo()
        End Function

        Function PreAcordoConsulta() As ViewResult
            Return ConsultaPreAcordo()
        End Function

        Function PreAcordoMinhasAprovacoes() As ViewResult
            Try
                'Session("IsMobile") = Util.IsMobile(Request)
                CarregaListasDropDownIndex(True)

                Dim acao = New AcaoComercialViewModel With {.CODSTAAPVACOCMC = Constantes.Status.MINHAS_APROVACOES}
                acao.TIPACOMCD = TipoAcao.PreAcordoVigencia 'PreAcordo

                Dim grid As New GridSettings(Of AcaoComercialViewModel)
                grid.Filter = acao

                Return View("~/Views/AcaoComercial/Index.vbhtml", BuscaAcaoGrid(grid))
            Catch ex As Exception
                Danger(ex)
                Return View("~/Views/AcaoComercial/Index.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())
            End Try
        End Function

        <HttpPost()>
        Function PreAcordoPesquisar(dadosPesquisa As GridSettings(Of AcaoComercialViewModel)) As PartialViewResult
            Try
                If (IsNothing(dadosPesquisa.Filter.DATINIACOCMC) And Not IsNothing(dadosPesquisa.Filter.DataIniAux)) Then
                    dadosPesquisa.Filter.DATINIACOCMC = CDate(dadosPesquisa.Filter.DataIniAux)
                End If
                If (IsNothing(dadosPesquisa.Filter.DATFIMACOCMC) And Not IsNothing(dadosPesquisa.Filter.DataFimAux)) Then
                    dadosPesquisa.Filter.DATFIMACOCMC = CDate(dadosPesquisa.Filter.DataFimAux)
                End If

                dadosPesquisa.Filter.TIPACOMCD = TipoAcao.PreAcordoVigencia 'PreAcordo

                Dim acao As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(dadosPesquisa.Filter)

                If acao.CODACOCMC Is Nothing AndAlso acao.NOMACOCMC Is Nothing AndAlso acao.CODFNCCRIACOCMC Is Nothing _
                    AndAlso acao.CODSTAAPVACOCMC Is Nothing AndAlso acao.TIPEVTACOCMC Is Nothing AndAlso acao.CODEVTACOCMC _
                    Is Nothing AndAlso acao.DATINIACOCMC Is Nothing AndAlso acao.DATFIMACOCMC Is Nothing Then

                    Warning(Constantes.FILTRO_OBRIGATORIO)
                    Return PartialView("~/Views/AcaoComercial/_ResultadosView.vbhtml", GridSettings(Of AcaoComercialViewModel).EmptyGrid())

                End If
                Session("AcaoFiltro") = dadosPesquisa.Filter


                Return PartialView("~/Views/AcaoComercial/_ResultadosView.vbhtml", BuscaAcaoGrid(dadosPesquisa))
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function
#End Region

#End Region

#Region "Inserir/Editar Ação"

        Function PreAcordoCreateEditVigencia(ByVal acao As String, Optional ByVal id As Decimal? = Nothing) As ActionResult

            Return CreateEdit(acao, id, True, TipoAcao.PreAcordoVigencia)

        End Function

        Function PreAcordoCreateEditValor(ByVal acao As String, Optional ByVal id As Decimal? = Nothing) As ActionResult

            Return CreateEdit(acao, id, True, TipoAcao.PreAcordoValor)

        End Function

        ''' <summary>
        ''' Tela de criar ou editar uma acao
        ''' </summary>
        ''' <param name="acao"></param>
        ''' <param name="id"></param>
        ''' <returns></returns>
        Function CreateEdit(ByVal acao As String, Optional ByVal id As Decimal? = Nothing) As ActionResult

            Return CreateEdit(acao, id, False, TipoAcao.Fornecedor)

        End Function

        Private Function CreateEdit(ByVal acao As String, ByVal id As Decimal?, ByVal indPreAcordo As Boolean, ByVal TipoAcao As TipoAcao) As ActionResult

            CarregaListasDropDown(indPreAcordo)

            If id Is Nothing Then
                Return Create(TipoAcao)
            Else
                Return Edit(id)
            End If

        End Function

        Function Create(ByVal tipo As TipoAcao) As ActionResult

            Dim novaAcaoComercial = New AcaoComercialViewModel()
            novaAcaoComercial.INDVLDRGRICT = 0
            novaAcaoComercial.INDACOCMCCPL = 0

            'Atribui dados do emitente
            novaAcaoComercial.CODFNCCRIACOCMC = ControleAcesso.CodFnc()
            novaAcaoComercial.NOMFNC = System.Web.HttpContext.Current.User.Identity.Name
            'Atribui status
            novaAcaoComercial.CODSTAAPVACOCMC = Constantes.Status.NOVA
            'Atribui acao complementar como nao
            novaAcaoComercial.INDACOCMCCPL = 0
            novaAcaoComercial.TIPACOMCD = tipo

            'Inicialização dos campos dos grids
            novaAcaoComercial.ValorTotal = 0.0
            novaAcaoComercial.SaldoOrcamento = 0.0
            novaAcaoComercial.ValorTotalBrinde = 0.0
            novaAcaoComercial.ValorCliente = 0.0
            ' Campos Valores Patrocinador
            novaAcaoComercial.SaldoReceitaCMV = 0.0
            novaAcaoComercial.VLRTOTOCDACOCMC = 0.0
            novaAcaoComercial.VLRTOTBDEACOCMC = 0.0
            novaAcaoComercial.VLRTOTTXAADMACOCMC = 0.0

            Dim parametros = boParametros.ListarParametrosAcaoComercial()
            If (parametros.Count > 0) Then
                novaAcaoComercial.VLRMAXACOCMC = parametros(0).VLRMAXACOCMC
                novaAcaoComercial.VLRTXAADM = parametros(0).VLRTXAADMFRNACOCMC
                novaAcaoComercial.VLRTXABDE = parametros(0).VLRTXAADMBDEACOCMC
            End If


            'TODO: checar metodo CarregaFornecedorMarketing que nunca eh chamado mas aparece no codigo original

            'Inicializa os grids da tela
            novaAcaoComercial.StateGridFornecedores = GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel).EmptyGrid("gridFornecedores", "AtualizarGridFornecedores", "Fornecedor")
            novaAcaoComercial.StateGridItensOrcamento = GridSettings(Of RelacaoAcaoComercialPorItemOrcamentoViewModel).EmptyGrid("gridItensOrcamento", "AtualizarGridItensOrcamento", "ItensOrcamento")
            novaAcaoComercial.StateGridCarimbos = GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid("gridCarimbos", "AtualizarGridCarimbos", "Carimbo")
            novaAcaoComercial.StateGridBonificacoes = GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel).EmptyGrid("gridBonificacoes", "AtualizarGridBonificacoes", "PedidoBonificacao")
            novaAcaoComercial.StateGridPremiacao = GridSettings(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel).EmptyGrid("gridPremiacoes", "AtualizarGridPremiacoes", "PremiacaoExclusiva")

            ControlaVisibilidadeComponentes(novaAcaoComercial)

            Session("acaoModel") = novaAcaoComercial

            Return View("~/Views/AcaoComercial/CreateEdit.vbhtml", novaAcaoComercial)
        End Function

        Public Function Edit(ByVal id As Decimal?) As ActionResult

            Dim acao = bo.BuscarAcaoComercialPorId(id)
            Dim acaoModel = Mapper.Map(Of VO.AcaoComercial, AcaoComercialViewModel)(acao)

            Dim parametros = boParametros.ListarParametrosAcaoComercial()
            If (parametros.Count > 0) Then
                acaoModel.VLRMAXACOCMC = parametros(0).VLRMAXACOCMC
                acaoModel.VLRTXAADM = parametros(0).VLRTXAADMFRNACOCMC
                acaoModel.VLRTXABDE = parametros(0).VLRTXAADMBDEACOCMC
            End If

            'Verifica se é Fornecedor de Marketing, se for recarrega o grid de fornecedores
            Dim Empenho As String = If(acaoModel.EmpenhoExa Is Nothing, "", acaoModel.EmpenhoExa.Split("-"c)(0).Trim)
            If Empenho = Constantes.EMPENHO_FORNECEDOR_MARKETING Then
                CarregaFornecedorMarketing(Empenho, acao.DESCSTA, acaoModel)
            Else
                'ConfiguraGridOriginal() (visibilidade das colunas do grid)
            End If

            acaoModel.StateGridFornecedores = GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel).EmptyGrid("gridFornecedores", "AtualizarGridFornecedores", "Fornecedor")
            acaoModel.StateGridFornecedores.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
            acaoModel.StateGridFornecedores.ListPaged = PaginarLista(acaoModel.Fornecedores, acaoModel.StateGridFornecedores.Page)
            acaoModel.StateGridFornecedores.TotalCount = acaoModel.Fornecedores.Count

            acaoModel.StateGridItensOrcamento = GridSettings(Of RelacaoAcaoComercialPorItemOrcamentoViewModel).EmptyGrid("gridItensOrcamento", "AtualizarGridItensOrcamento", "AcaoComercial")
            acaoModel.StateGridItensOrcamento.ResultID = "divWidgetOrcamento"
            acaoModel.StateGridItensOrcamento.ListPaged = PaginarLista(acaoModel.ItensOrcamento)
            acaoModel.StateGridItensOrcamento.TotalCount = acaoModel.ItensOrcamento.Count

            acaoModel.StateGridCarimbos = GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid("gridCarimbos", "AtualizarGridCarimbos", "Carimbo")
            acaoModel.StateGridCarimbos.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
            acaoModel.StateGridCarimbos.ListPaged = PaginarLista(acaoModel.Carimbos, acaoModel.StateGridCarimbos.Page)
            acaoModel.StateGridCarimbos.TotalCount = acaoModel.Carimbos.Count

            acaoModel.StateGridBonificacoes = GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel).EmptyGrid("gridBonificacoes", "AtualizarGridBonificacoes", "PedidoBonificacao")
            acaoModel.StateGridBonificacoes.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
            acaoModel.StateGridBonificacoes.ListPaged = PaginarLista(acaoModel.Bonificacoes, acaoModel.StateGridBonificacoes.Page)
            acaoModel.StateGridBonificacoes.TotalCount = acaoModel.Bonificacoes.Count

            acaoModel.StateGridPremiacao = GridSettings(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel).EmptyGrid("gridPremiacoes", "AtualizarGridPremiacoes", "PremiacaoExclusiva")
            acaoModel.StateGridPremiacao.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
            acaoModel.StateGridPremiacao.ListPaged = PaginarLista(acaoModel.Premiacao, acaoModel.StateGridPremiacao.Page)
            acaoModel.StateGridPremiacao.TotalCount = acaoModel.Premiacao.Count

            'dados dos itens orcamento
            Dim TotalValorOrcamento As Decimal = 0
            Dim TotalBrinde As Decimal = 0
            Dim saldoOrcamento As Decimal = 0
            Dim valorCliente As Decimal = 0



            For Each item As RelacaoAcaoComercialPorItemOrcamentoViewModel In acaoModel.ItensOrcamento
                If item.INDBDEOPEDSNACOCMC = 1 Then
                    TotalBrinde += (item.VLRUNTITEOCDACOCMC * item.QDEITEOCDACOCMC)
                Else
                    If Not ((item.VLRUNTITEOCDACOCMC - item.VLRPTCCLIITEACOCMC) <= 0) Then
                        TotalValorOrcamento += (item.QDEITEOCDACOCMC * (item.VLRUNTITEOCDACOCMC - item.VLRPTCCLIITEACOCMC)) ' quantidade*(valorItem-ValorCliente)
                        saldoOrcamento += item.SaldoItem
                    End If
                End If
                valorCliente += item.VLRPTCCLIITEACOCMC * item.QDEITEOCDACOCMC 'ValorCliente * Quantidade
            Next

            acaoModel.ValorTotal = TotalValorOrcamento
            acaoModel.ValorTotalBrinde = TotalBrinde
            acaoModel.SaldoOrcamento = saldoOrcamento
            acaoModel.ValorCliente = valorCliente

            acaoModel.CODFILEMP = acaoModel.CODFILEMPORIVBA

            If (acaoModel.IndPreAcordo = False) Then
                Dim BOCriterioApuracao As New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
                Dim CriterioApuracao = BOCriterioApuracao.ListT(New VO.RelacaoAcaoMecanicaCriterioDeApuracao() With {.CODACOCMC = acaoModel.CODACOCMC})
                acaoModel.INDCASHBACK = CriterioApuracao.Any(Function(a) a.INDTIPPTOPMC > 0)
            End If

            Session("acaoModel") = acaoModel

            If (acaoModel.TIPEVTACOCMC IsNot Nothing) Then
                ViewBag.listaEventos = boEvento.ListEventoAno(New VO.Evento With {.TIPEVTACOCMC = acaoModel.TIPEVTACOCMC}, acaoModel.CODACOCMC)
            End If
            ControlaVisibilidadeComponentes(acaoModel)

            Return View("~/Views/AcaoComercial/CreateEdit.vbhtml", acaoModel)

        End Function

        ''' <summary>
        ''' Acao do botao de salvar acao mercadologica
        ''' </summary>
        ''' <param name="acaoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Salvar(id As String, ByVal acaoViewModel As AcaoComercialViewModel)

            'Dim acaoModel = Mapper.Map(Of VO.AcaoComercial, AcaoComercialViewModel)(acao)
            Dim indPreAcordo As Boolean = False
            Dim VlrTot As Double = 0
            Dim TxaAdm As Double = 0
            Try
                ''Esse codigo cria problema .............. Apagar
                'If (Not IsNothing(acaoViewModel.CODACOCMCORI) AndAlso acaoViewModel.CODACOCMCORI <> 0) Then
                '    Dim acaoPrincipal = bo.BuscarAcaoComercialPorId(acaoViewModel.CODACOCMCORI)

                '    If acaoPrincipal IsNot Nothing Then
                '        acaoViewModel.TIPEVTACOCMC = acaoPrincipal.TIPEVTACOCMC
                '        acaoViewModel.CODEVTACOCMC = acaoPrincipal.CODEVTACOCMC
                '    End If
                'End If

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel") 'Session.Item("acaoModel")
                If (acaoCache Is Nothing) Then
                    Warning("Esta tela esta a muito tempo aberta e as informações podem estar desatualizadas, atualize a pagina para prosseguir")
                    Return Nothing
                End If


                If (acaoCache IsNot Nothing And acaoViewModel.INDACOCMCCPL = 1) Then
                    acaoViewModel.TIPEVTACOCMC = acaoCache.TIPEVTACOCMC
                    acaoViewModel.CODEVTACOCMC = acaoCache.CODEVTACOCMC
                    acaoViewModel.CODFILEMPORIVBA = acaoCache.CODFILEMPORIVBA
                End If

                If (acaoViewModel.TIPACOMCD = TipoAcao.PreAcordoVigencia Or acaoViewModel.TIPACOMCD = TipoAcao.PreAcordoValor) Then
                    indPreAcordo = True
                End If

                Dim TipoFornecedor As Decimal?
                'Calcular o Valor Participação do Fornecedor
                If (acaoCache.Fornecedores.Count > 0) Then
                    For Each item In acaoCache.Fornecedores
                        VlrTot = item.VLRUTZCTTACOCMC + item.VLRUTZEXACTTACOCMC + item.VLRUTZRCTCSTMER

                        'Se for despesas martins não há taxa de adm. e o valor total é o valor da participação
                        'Todo: buscar taxa de administrativa de parametro
                        If (item.CODFRN <> Constantes.DESPESAS_MARTINS) Then
                            TxaAdm = VlrTot * (0 / 100) 'Calcula a taxa de administração
                        End If
                        item.VLRPTCFRNACOCMC = VlrTot - TxaAdm  'Mostra o Valor total da Participação do fornecedor
                        item.VLRTXAADMFRNACOCMC = TxaAdm

                        TipoFornecedor = item.TIPFRNACOCMC
                    Next
                End If

                'Calcula o valor total de Participação do Fornecedor
                Dim TotalParticipacao As Decimal
                Select Case acaoViewModel.TIPACOMCD
                    Case TipoAcao.Carimbo
                        For Each item In acaoCache.Carimbos
                            TotalParticipacao += item.VLRUTZACOCMC
                        Next
                    Case TipoAcao.Premiacao
                        For Each item In acaoCache.Premiacao
                            TotalParticipacao += item.VLRUTZACOCMC
                        Next
                    Case TipoAcao.PedidoBonificacao
                        For Each item In acaoCache.Bonificacoes
                            TotalParticipacao += item.VLRUTZACOCMC
                        Next
                    Case TipoAcao.PreAcordoVigencia, TipoAcao.PreAcordoValor
                        For Each item As RelacaoAcaoComercialxFornecedoresViewModel In acaoCache.Fornecedores
                            TotalParticipacao += item.VLRPTCFRNACOCMC
                        Next
                    Case Else
                        For Each item As RelacaoAcaoComercialxFornecedoresViewModel In acaoCache.Fornecedores
                            TotalParticipacao += item.VLRPTCFRNACOCMC
                        Next
                End Select


                If (acaoViewModel.TIPACOMCD = TipoAcao.Fornecedor And Not acaoCache.Fornecedores Is Nothing) Then
                    Dim saldoinsuficiente = acaoCache.Fornecedores.Where(Function(x) x.CODFRN <> Constantes.DESPESAS_MARTINS AndAlso x.SaldoAcordoExtraInsuficiente() = True).ToList()
                    If (saldoinsuficiente.Count > 0) Then
                        Warning("Existem fornecedores com saldo insuficiente para atender esta ação, Verifique!")
                        Return Nothing
                    End If
                End If

                If (acaoViewModel.TIPACOMCD = TipoAcao.Fornecedor And acaoCache.TIPDSNDSCBNF = 99 And TipoFornecedor <> 1) Then '99 - ACOES COMERCIAIS              
                    If (acaoCache.Fornecedores.Where(Function(x) x.CODFRN <> Constantes.DESPESAS_MARTINS).Count() > 0) Then
                        Warning("Ações mercadológicas com empenho '99 - ACOES COMERCIAIS', são exclusivas para o fornecedor '999999 - DESPESAS MARTINS'")
                        Return Nothing
                    End If
                End If

                If ((acaoViewModel.TIPACOMCD = TipoAcao.PreAcordoVigencia Or acaoViewModel.TIPACOMCD = TipoAcao.PreAcordoValor) And acaoCache.TIPDSNDSCBNF = 20) Then  '20 - PROMOCOES MARTINS
                    If (acaoCache.Fornecedores.Where(Function(x) x.CODFRN = Constantes.DESPESAS_MARTINS).Count() > 0) Then
                        Warning("Não é permitido Pré-Acordos com empenho '20 - PROMOCOES MARTINS', para o fornecedor '999999 - DESPESAS MARTINS'")
                        Return Nothing
                    End If
                End If

                If (acaoViewModel.TIPACOMCD = TipoAcao.Fornecedor Or acaoViewModel.TIPACOMCD = TipoAcao.PreAcordoVigencia Or acaoViewModel.TIPACOMCD = TipoAcao.PreAcordoValor) Then
                    If (acaoCache.Fornecedores.Count() = 0) Then
                        Warning("Selecione o patrocinador para prosseguir!")
                        Return Nothing
                    End If
                End If



                If (acaoViewModel.Enviar) Then

                    If ((Not acaoViewModel.CODACOCMC Is Nothing) AndAlso boItensOrcamento.ValidaNecessidadeCanalVenda(acaoViewModel.CODACOCMC)) Then
                        If (acaoViewModel.CODCNLVNDINI Is Nothing OrElse acaoViewModel.CODCNLVNDINI = 0) Then
                            Warning("Selecione o Canal de Venda para prosseguir!")
                            Return Nothing
                        End If
                    End If

                    'Comparação do valor de participação com o total do orçamento
                    If (Math.Round(TotalParticipacao, 2) <> Math.Round(acaoCache.ValorTotal, 2) And indPreAcordo = False) Then
                        Information(Constantes.VLR_PARTICIPACAO_MAIOR_VLR_TOTAL_ORCAMENTO)

                        CarregaListasDropDown(indPreAcordo)

                        If (acaoViewModel.TIPEVTACOCMC IsNot Nothing) Then
                            ViewBag.listaEventos = boEvento.ListEventoAno(New VO.Evento With {.TIPEVTACOCMC = acaoViewModel.TIPEVTACOCMC})
                        End If

                        'Status da Ação
                        acaoViewModel.CODSTAAPVACOCMC = acaoCache.CODSTAAPVACOCMC

                        ControlaVisibilidadeComponentes(acaoViewModel)
                        Return PartialView("~/Views/AcaoComercial/_DadosAcao.vbhtml", acaoViewModel)
                    End If

                    'Chama o método de Enviar aprovação para o fluxo
                    Return Enviar(acaoViewModel)
                Else
                    'Validação do botão salvar.
                    Dim indIncluir As Boolean = (acaoViewModel.CODACOCMC Is Nothing)

                    Dim tipoEventoBO As New TipoDeEventoBO()
                    Dim tipoEvento = tipoEventoBO.SelectT(New VO.TipoDeEvento() With {.TIPEVTACOCMC = acaoViewModel.TIPEVTACOCMC})
                    If (tipoEvento.INDCPHBTB = 1) Then
                        If (String.IsNullOrWhiteSpace(acaoViewModel.DESCPHBTB)) Then
                            ModelState.AddModelError("DESCPHBTB", "Preencha o campo Campanha B2B")
                        Else
                            acaoViewModel.DESCPHBTB = acaoViewModel.DESCPHBTB.ToUpper()
                        End If
                    Else
                        acaoViewModel.DESCPHBTB = " "
                    End If

                    If (ModelState.IsValid) Then


                        'Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel") 'Session.Item("acaoModel")

                        If acaoCache IsNot Nothing Then
                            'Atribui a lista de patrocinador
                            Select Case acaoViewModel.TIPACOMCD
                                Case TipoAcao.Carimbo
                                    acaoViewModel.Carimbos = acaoCache.Carimbos
                                Case TipoAcao.Premiacao
                                    acaoViewModel.Premiacao = acaoCache.Premiacao
                                Case TipoAcao.PedidoBonificacao
                                    acaoViewModel.Bonificacoes = acaoCache.Bonificacoes
                                Case TipoAcao.PreAcordoVigencia, TipoAcao.PreAcordoValor
                                    acaoViewModel.Fornecedores = acaoCache.Fornecedores
                                Case Else
                                    acaoViewModel.Fornecedores = acaoCache.Fornecedores
                                    acaoViewModel.TIPACOMCD = (TipoAcao.Fornecedor)
                            End Select
                        End If
                        acaoViewModel.ItensOrcamento = acaoCache.ItensOrcamento
                        If Not (acaoViewModel.ItensOrcamento Is Nothing) Then
                            Util.AtuSldOcd(acaoViewModel) 'atualiza o saldo do orçamento 
                            Util.CalVlrTot(acaoViewModel) 'calcula vlr total do brinde e do orçamento
                        End If

                        If acaoViewModel.TIPACOMCD = TipoAcao.PreAcordoVigencia OrElse acaoViewModel.TIPACOMCD = TipoAcao.PreAcordoValor Then
                            If Not (acaoViewModel.ItensOrcamento Is Nothing) Then
                                bo.AtualizaValorFornecedorPreAcordo(acaoViewModel)
                            End If

                        End If

                        If (acaoCache.Incentivos IsNot Nothing AndAlso acaoCache.Incentivos.Count > 0) Then
                            acaoViewModel.INDVLDRGRICT = 1
                        Else
                            acaoViewModel.INDVLDRGRICT = 0
                        End If
                        acaoViewModel.CODSTAAPVACOCMC = acaoCache.CODSTAAPVACOCMC
                        acaoViewModel.Incentivos = acaoCache.Incentivos
                        acaoViewModel.PlanosDeMarketing = acaoCache.PlanosDeMarketing
                        acaoViewModel.Mecanica = acaoCache.Mecanica

                        Dim acao = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(acaoViewModel)
                        acao.VLRTOTOCDACOCMC = acaoViewModel.ValorTotal
                        acao.VLRTOTBDEACOCMC = acaoViewModel.ValorTotalBrinde
                        acao.VLRTOTACOCMC = acaoViewModel.ValorTotal + acaoViewModel.ValorTotalBrinde

                        acao = bo.SalvarAcaoComercial(acao, True)
                        If (indIncluir) Then
                            Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                        Else
                            Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
                        End If
                        acaoViewModel.CODACOCMC = acao.CODACOCMC
                        Dim action As String = "CreateEdit"
                        Select Case acaoViewModel.TIPACOMCD
                            Case TipoAcao.PreAcordoVigencia
                                action = "PreAcordoCreateEditVigencia"
                            Case TipoAcao.PreAcordoValor
                                action = "PreAcordoCreateEditValor"
                        End Select

                        Return JavaScript(String.Format("setTimeout(function(){{ window.location = SiteUrl + 'AcaoComercial/{1}/{0}'; }}, 3000);", acaoViewModel.CODACOCMC, action))


                    Else

                        CarregaListasDropDown(indPreAcordo)

                        If (acaoViewModel.TIPEVTACOCMC IsNot Nothing) Then
                            ViewBag.listaEventos = boEvento.ListEventoAno(New VO.Evento With {.TIPEVTACOCMC = acaoViewModel.TIPEVTACOCMC})

                            'Dim CodAcao As Decimal
                            'ViewBag.ListaEventos = boEvento.ListEventoAno(New VO.Evento, CodAcao)
                            'ViewBag.listaEventos = boEvento.ListEventoDoTipoEvento(acaoViewModel.TIPEVTACOCMC)
                            ', acaoViewModel.CODACOCMC
                        End If

                        ControlaVisibilidadeComponentes(acaoViewModel)
                        Return PartialView("~/Views/AcaoComercial/_DadosAcao.vbhtml", acaoViewModel)
                    End If

                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PreAcordoSalvarVigencia(id As String, ByVal acaoViewModel As AcaoComercialViewModel)
            acaoViewModel.TIPACOMCD = TipoAcao.PreAcordoVigencia
            Return Salvar(id, acaoViewModel)
        End Function

        Function PreAcordoSalvarValor(id As String, ByVal acaoViewModel As AcaoComercialViewModel)
            acaoViewModel.TIPACOMCD = TipoAcao.PreAcordoValor
            Return Salvar(id, acaoViewModel)
        End Function

        ''' <summary>
        ''' Acao do botao de enviar acao mercadologica
        ''' </summary>
        ''' <param name="acaoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function Enviar(ByVal acaoViewModel As AcaoComercialViewModel) As JavaScriptResult
            Dim IcentivosAbilitado As Boolean = False
            Dim bBotaoPolitica As Boolean = False
            Dim binsereacao As Boolean = False
            Dim benviar As Boolean = False
            Dim bDesabilitabotao As Integer = 0
            Dim indPreAcordo As Boolean = False

            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                If (acaoCache Is Nothing) Then
                    Warning("Esta tela esta a muito tempo aberta e as informações podem estar desatualizadas, atualize a pagina para prosseguir")
                    Return Nothing
                End If

                'Atribui a lista de patrocinador
                acaoViewModel.Incentivos = acaoCache.Incentivos
                acaoViewModel.PlanosDeMarketing = acaoCache.PlanosDeMarketing
                acaoViewModel.Mecanica = acaoCache.Mecanica
                acaoViewModel.TIPDSNDSCBNF = acaoCache.TIPDSNDSCBNF

                IcentivosAbilitado = ViewBag.btnSlcIncentivoVisible
                bBotaoPolitica = ViewBag.btnPoliticaVisible

                Select Case acaoViewModel.TIPACOMCD
                    Case TipoAcao.Fornecedor, TipoAcao.PreAcordoVigencia, TipoAcao.PreAcordoValor
                        acaoViewModel.Fornecedores = acaoCache.Fornecedores
                    Case TipoAcao.Carimbo
                        acaoViewModel.Carimbos = acaoCache.Carimbos
                    Case TipoAcao.Premiacao
                        acaoViewModel.Premiacao = acaoCache.Premiacao
                    Case TipoAcao.PedidoBonificacao
                        acaoViewModel.Bonificacoes = acaoCache.Bonificacoes
                    Case Else
                End Select

                Dim acao = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(acaoViewModel)

                If (acaoViewModel.TIPACOMCD = 5 Or acaoViewModel.TIPACOMCD = 6) Then
                    indPreAcordo = True
                End If

                If (Not acao.CODACOCMC Is Nothing) Then
                    acao.ItensOrcamento = boItensOrcamento.BuscarItensOrcamentoDaAcao(acao.CODACOCMC)
                End If
                Dim retorno As JavaScriptResult
                retorno = VerificacoesFornecedor(acao, benviar, binsereacao)
                If (Not IsNothing(retorno)) Then
                    Return retorno
                End If
                If binsereacao Then
                    bo.EnviarAcaoComercial(acao, IcentivosAbilitado, bBotaoPolitica, benviar, bDesabilitabotao, indPreAcordo)
                    If bDesabilitabotao = 1 Then
                        ViewBag.btPolAcoCmc.Visible = True
                        ViewBag.btnEnviarVisible = True
                        ViewBag.btnEnviarVisible = False
                        ViewBag.btnGravarVisible = False
                        ViewBag.btnClonarVisible = False
                        ViewBag.btnIsrFornecedorVisible = False
                        ViewBag.btnIsrItemVisible = False
                    ElseIf bDesabilitabotao = 2 Then
                        ViewBag.btnEnviarVisible = False
                        ViewBag.btnGravarVisible = False
                        ViewBag.btnClonarVisible = False
                        ViewBag.btnIsrFornecedorVisible = False
                        ViewBag.btnIsrItemVisible = False
                    End If

                    Success(Constantes.REGISTRO_ENVIADO_APROVACAO)
                    acaoViewModel.CODACOCMC = acao.CODACOCMC
                    'Return True
                End If

                CarregaListasDropDown(indPreAcordo)


                If (acaoViewModel.TIPEVTACOCMC IsNot Nothing) Then
                    ViewBag.listaEventos = boEvento.ListEventoAno(New VO.Evento With {.TIPEVTACOCMC = acaoViewModel.TIPEVTACOCMC})
                End If
                ControlaVisibilidadeComponentes(acaoViewModel)

                Dim action As String = "CreateEdit"
                Select Case acaoViewModel.TIPACOMCD
                    Case TipoAcao.PreAcordoVigencia
                        action = "PreAcordoCreateEditVigencia"
                    Case TipoAcao.PreAcordoValor
                        action = "PreAcordoCreateEditValor"
                End Select

                'Return PartialView("~/Views/AcaoComercial/_DadosAcao.vbhtml", acaoViewModel)
                Return JavaScript(String.Format("setTimeout(function(){{ window.location = SiteUrl + 'AcaoComercial/{1}/{0}'; }}, 3000);", acaoViewModel.CODACOCMC, action))
                'Return JavaScript(String.Format("window.location = SiteUrl + 'AcaoComercial/CreateEdit/{0}';", acaoViewModel.CODACOCMC))

            Catch mappedWarn As MappedWarnException
                Warning(mappedWarn)
                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Function VerificacoesFornecedor(acaoComercial As VO.AcaoComercial, ByRef bEnviar As Boolean, ByRef binsereacao As Boolean)
            Dim dsPoliticaDivergente As List(Of VO.RelacaoAcaoComercialxPolitica)
            Dim valorGasto, valorOrcado As Decimal

            Dim boRelacaoAcaoComercialxPolitica As New RelacaoAcaoComercialxPoliticaBO

            If Not (acaoComercial.CODEVTACOCMC Is Nothing) Then
                dsPoliticaDivergente = boRelacaoAcaoComercialxPolitica.ConsultaPoliticaDivergente(acaoComercial.CODEVTACOCMC)

                If Not dsPoliticaDivergente Is Nothing Then
                    If dsPoliticaDivergente.Count > 0 And acaoComercial.PlanosDeMarketing.Count > 0 Then
                        If dsPoliticaDivergente.Item(0).INDEVTMTDPOLDVR = 1 Then

                            If IsNothing(acaoComercial.PlanosDeMarketing.FirstOrDefault().VLTOTALGASTO) Then  'frmSldPlnMkt.TabPlnMkt.GetData(1, "TotOrcAno")
                                valorGasto = 0
                            Else
                                valorGasto = acaoComercial.PlanosDeMarketing.FirstOrDefault().VLTOTALGASTO
                            End If

                            If IsNothing(acaoComercial.PlanosDeMarketing.FirstOrDefault().VLTOTALORCADO) Then  'frmSldPlnMkt.TabPlnMkt.GetData(1, "TotOrcAno")
                                valorOrcado = 0
                            Else
                                valorOrcado = acaoComercial.PlanosDeMarketing.FirstOrDefault().VLTOTALORCADO
                            End If

                            If valorGasto > valorOrcado Then

                                If Not IsNothing(acaoComercial.confirmaEnviar1) AndAlso acaoComercial.confirmaEnviar1 = False Then
                                    binsereacao = False
                                    Return Nothing
                                ElseIf Not IsNothing(acaoComercial.confirmaEnviar1) AndAlso acaoComercial.confirmaEnviar1 = True Then
                                    binsereacao = True
                                Else
                                    Return JavaScript(String.Format("LoadingOff();abrirModalSemLoading(""AcaoComercial"", ""abrirModalConfirmacao"", {0}, ""#modal"");", 1))
                                End If
                            End If
                        End If
                    End If
                End If
            End If

            If InsDdoPol(acaoComercial) > 0 Then

                If Not IsNothing(acaoComercial.confirmaEnviar1) AndAlso acaoComercial.confirmaEnviar2 = False Then
                    binsereacao = False
                    Return Nothing
                ElseIf acaoComercial.confirmaEnviar1 Or acaoComercial.confirmaEnviar2 Then
                    binsereacao = True
                Else
                    Return JavaScript(String.Format("LoadingOff();abrirModalSemLoading(""AcaoComercial"", ""abrirModalConfirmacao"", {0}, ""#modal"");", 2))
                End If
            End If

            If VrfTipOpe(acaoComercial) Then
                If Not IsNothing(acaoComercial.confirmaEnviarGerente) AndAlso acaoComercial.confirmaEnviarGerente = 0 Then
                    binsereacao = False
                    Return Nothing
                ElseIf Not IsNothing(acaoComercial.confirmaEnviarGerente) AndAlso acaoComercial.confirmaEnviarGerente <> 0 Then
                    binsereacao = True
                Else
                    Return JavaScript(String.Format("abrirModal(""AcaoComercial"", ""modalBuscarFuncionarios"",undefined,undefined,LoadingOff);"))
                End If
            End If

            binsereacao = True

        End Function

        <HttpPost()>
        Public Function modalBuscarFuncionarios() As PartialViewResult

            Dim func As New FuncionarioViewModel

            Return PartialView("~/Views/AcaoComercial/ModalSelecionarGerente/_ModalGerente.vbhtml", func)
        End Function

        <HttpPost()>
        Public Function BuscarFuncionariosModal(idGerente As Integer, nomGerente As String) As PartialViewResult
            Dim boFuncionario As New FuncionarioBO
            Dim funcionarios As List(Of FuncionarioViewModel) = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(boFuncionario.selecionaGerente(idGerente, nomGerente))


            Dim gridConf = GridSettings(Of FuncionarioViewModel).EmptyGrid("resultadosGerente", "BuscarFuncionarios", "AcaoComercial")
            gridConf = PaginarEAtualizarGrid(funcionarios, gridConf)
            gridConf.TotalCount = funcionarios.Count
            gridConf.ResultID = "resultadosGerente"
            gridConf.ActionName = "BuscarFuncionarios"
            gridConf.PaginationID = "resultadosGerente"
            gridConf.Filter.CODFNC = idGerente
            gridConf.Filter.NOMFNC = nomGerente

            Return PartialView("~/Views/AcaoComercial/ModalSelecionarGerente/_ModalSelecionarGerente.vbhtml", gridConf)
        End Function

        <HttpPost()>
        Public Function BuscarFuncionariosModalResult(idGerente As Integer, nomGerente As String) As PartialViewResult
            Dim boFuncionario As New FuncionarioBO
            Dim funcionarios As List(Of FuncionarioViewModel) = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(boFuncionario.selecionaGerente(idGerente, nomGerente))


            Dim gridConf = GridSettings(Of FuncionarioViewModel).EmptyGrid("resultadosGerente", "BuscarFuncionarios", "AcaoComercial")
            gridConf = PaginarEAtualizarGrid(funcionarios, gridConf)
            gridConf.TotalCount = funcionarios.Count
            gridConf.ResultID = "resultadosGerente"
            gridConf.ActionName = "BuscarFuncionarios"
            gridConf.PaginationID = "resultadosGerente"
            gridConf.Filter.CODFNC = idGerente
            gridConf.Filter.NOMFNC = nomGerente

            Return PartialView("~/Views/AcaoComercial/ModalSelecionarGerente/_ResultadoGerentes.vbhtml", gridConf)
        End Function

        <HttpPost()>
        Public Function BuscarFuncionarios(dadosPesquisa As GridSettings(Of FuncionarioViewModel)) As PartialViewResult
            Dim boFuncionario As New FuncionarioBO
            Dim funcionarios As List(Of FuncionarioViewModel) = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(boFuncionario.selecionaGerente(dadosPesquisa.Filter.CODFNC, dadosPesquisa.Filter.NOMFNC))


            Dim gridConf = PaginarEAtualizarGrid(funcionarios, dadosPesquisa.Page, dadosPesquisa.Filter)
            If gridConf.TotalCount = 0 Then
                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
            End If
            gridConf.ResultID = "resultadosGerente"
            gridConf.PaginationID = "resultadosGerente"
            gridConf.ActionName = "BuscarFuncionarios"
            gridConf.ControllerName = "AcaoComercial"

            Return PartialView("~/Views/AcaoComercial/ModalSelecionarGerente/_ResultadoGerentes.vbhtml", gridConf)
        End Function

        <HttpPost()>
        Public Function abrirModalConfirmacao(objeto As Integer) As PartialViewResult
            Dim modal As New ModalConfirmacaoViewModel
            modal.Titulo = "Políticas divergentes!"
            If objeto = 1 Then
                modal.Mensagem = "O valor gasto até o momento é superior ao valor orçado no evento!Deseja enviar mesmo assim?"
            ElseIf objeto = 2 Then
                modal.Mensagem = "Deseja enviar mesmo assim?"
            End If
            modal.BtnSimNome = "Enviar"
            modal.BtnNaoNome = "Cancelar"
            modal.BtnSimAcao = "confirmaEnvio"
            modal.BtnNaoAcao = "fechaModalEnvio"
            modal.Objeto = objeto
            Return PartialView("~\Views\Shared\_ModalConfirmacaoGenerico.vbhtml", modal)
        End Function

        Function VrfTipOpe(acaoComercial As VO.AcaoComercial)

            Dim indItePmc As Boolean = False

            For Cont As Integer = 0 To (acaoComercial.ItensOrcamento.Count - 1)
                If acaoComercial.ItensOrcamento(Cont).CODOPEFSCDSNACOCMC = 2 Or acaoComercial.ItensOrcamento(Cont).CODOPEFSCDSNACOCMC = 4 Then
                    indItePmc = True
                    Exit For
                End If
            Next

            Return indItePmc
        End Function

        Function CalVlrTotAcoCmc(acaoComercial As VO.AcaoComercial) As String

            Dim VlrTot As Double = 0
            Dim Str As String = ""

            Try
                If Not IsDBNull(acaoComercial.ValorParticipacao) And acaoComercial.ValorParticipacao <> 0 Then
                    VlrTot = Convert.ToDecimal(acaoComercial.ValorParticipacao)
                End If

                If Not IsDBNull(acaoComercial.VLRTOTTXAADMACOCMC) Then
                    VlrTot = VlrTot + acaoComercial.VLRTOTTXAADMACOCMC
                End If

                If Not IsDBNull(acaoComercial.VLRTOTBDEACOCMC) Then
                    VlrTot = VlrTot + acaoComercial.VLRTOTBDEACOCMC
                End If
                Str = Convert.ToString(VlrTot)

                Return Str

            Catch ex As Exception
                Throw New MappedWarnException("CalVlrTotAcoCmc" & Str & ex.Message)
            End Try
        End Function

        Public Function InsDdoPol(acaoComercial As VO.AcaoComercial) As Integer
            Dim TipEvt As Integer
            Dim CodEvt As Integer
            Dim datRef As Integer
            Dim CodUnNgc As Integer
            Dim codUndNgc As Integer
            Dim aux As Integer
            Dim VlrTotAco As String
            Dim str As String
            Dim vlr As Decimal
            Dim data As Date
            'Dim rsDdoAux As DataSet
            Dim cnt As Integer
            Dim oRow As String = ""
            Dim CodFrn As Integer
            Dim i As Integer
            Dim rsDdo As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
            Dim VlrMaxAcoCmc As Decimal
            Dim IndErrAcoCmc As Decimal

            Try

                VlrTotAco = CalVlrTotAcoCmc(acaoComercial)
                VlrTotAco = VlrTotAco.Replace(".", ",")

                'Valor da ação superior ao valor máximo
                'busca valor máximo para as ações
                vlr = VlrMaxAcoCmc
                If Convert.ToDecimal(VlrTotAco) > vlr Then
                    oRow = "1" + "|"
                End If

                If Not (acaoComercial.CODEVTACOCMC Is Nothing) Then
                    Dim evento = boEvento.ConsultaDadosEventoPeloCodigo(acaoComercial.CODEVTACOCMC)
                    If evento.INDEVTADIACOCMC = 1 Then
                        oRow = oRow + "2" + "|"
                    End If
                End If

                If (acaoComercial.Fornecedores.Count > 0) Then
                    CodFrn = acaoComercial.Fornecedores(0).CODFRN   'TabFrnAcoCmc.GetData(1, "Frn").split("-")(0)
                    If CodFrn = Constantes.DESPESAS_MARTINS Then
                        oRow = oRow + "3" + "|"
                    End If
                End If

                If acaoComercial.INDACOCMCCPL = Constantes.SIM Then
                    oRow = oRow + "5" + "|"
                End If

                i = acaoComercial.PlanosDeMarketing.Count - 1
                cnt = 0
                While cnt <= i
                    If Not (acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOMENSAL Is Nothing) Then
                        vlr = acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOMENSAL 'frmSldPlnMkt.TabPlnMkt.GetData(cnt, "RstMes") 'pega o valor para o BU
                        If vlr < 0 Then
                            oRow = oRow + "6" + "|"
                            cnt = i
                        End If
                    End If
                    cnt += 1
                End While

                'Valor do evento atual negativo
                cnt = 0
                While cnt <= i
                    If Not (acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOATUAL Is Nothing) Then
                        vlr = acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOATUAL 'frmSldPlnMkt.TabPlnMkt.GetData(cnt, "RstAtu")
                        If vlr < 0 Then
                            oRow = oRow + "7" + "|"
                            cnt = i
                        End If
                    End If
                    cnt += 1
                End While

                'Valor do evento anual negativo
                cnt = 0
                While cnt <= i
                    If Not (acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOANUAL Is Nothing) Then
                        vlr = acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOANUAL 'frmSldPlnMkt.TabPlnMkt.GetData(cnt, "RstAnO")
                        If vlr < 0 Then
                            oRow = oRow + "8" + "|"
                            cnt = i
                        End If
                    End If
                    cnt += 1
                End While

                Dim VlrTot As Decimal
                Dim cod As String
                'Valor da taxa de administração menor que a estipulada
                cnt = 0
                i = acaoComercial.Fornecedores.Count - 1 'TabFrnAcoCmc.Rows.Count  'número de linhas da tabela de fornecedores
                'percorre todas as linhas da tabela
                While cnt <= i
                    cod = acaoComercial.Fornecedores(cnt).CODFRN   'TabFrnAcoCmc.GetData(cnt, "Frn")
                    'cod = cod.Split("-")(0)
                    If Integer.Parse(cod) <> Constantes.DESPESAS_MARTINS Then

                        If Not (acaoComercial.Fornecedores(cnt).VLRTOTTXAADMACOCMC Is Nothing) Then
                            vlr = acaoComercial.Fornecedores(cnt).VLRTOTTXAADMACOCMC  'TabFrnAcoCmc.GetData(cnt, "txaAdm") ' pega a taxa de administração que está na tabela
                        Else
                            vlr = 0
                        End If

                        VlrTot = 0
                        'If Not (TabFrnAcoCmc.GetData(cnt, "ctt") Is Nothing) Then
                        If Not (acaoComercial.Fornecedores(cnt).VLRUTZCTTACOCMC Is Nothing) Then
                            'If Not (TabFrnAcoCmc.GetData(cnt, "ext") Is Nothing) Then
                            If Not (acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC Is Nothing) Then
                                VlrTot = (acaoComercial.Fornecedores(cnt).VLRUTZCTTACOCMC + acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC) * (acaoComercial.VLRTOTTXAADMACOCMC / 100) '(TabFrnAcoCmc.GetData(cnt, "ctt") + TabFrnAcoCmc.GetData(cnt, "ext")) * (VlrTxaAdm / 100)
                            Else
                                VlrTot = (acaoComercial.Fornecedores(cnt).VLRUTZCTTACOCMC) * (acaoComercial.VLRTOTTXAADMACOCMC / 100) '(TabFrnAcoCmc.GetData(cnt, "ctt")) * (VlrTxaAdm / 100)
                            End If
                        Else
                            'If Not (TabFrnAcoCmc.GetData(cnt, "ext") Is Nothing) Then
                            If Not (acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC Is Nothing) Then
                                VlrTot = (acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC) * (acaoComercial.VLRTOTTXAADMACOCMC / 100) 'TabFrnAcoCmc.GetData(cnt, "ext") * (VlrTxaAdm / 100)
                            End If
                        End If
                        'If CDbl(vlr.Round(vlr, 2)) < CDbl(VlrTot.Round(VlrTot, 2)) Then
                        If CDbl(vlr) < CDbl(VlrTot) Then
                            oRow = oRow + "9" + "|"
                            cnt = i
                        End If
                        cnt += 1
                    Else
                        cnt += 1
                    End If
                End While

                Dim CntAux As Integer = 1
                Dim VlrAux As Decimal
                Dim ind As Integer = 0


                'Valor da taxa de brinde menor que a estipulada
                cnt = 0
                i = acaoComercial.ItensOrcamento.Count  'TabIteOcdAcoCmc.Rows.Count  'número de linhas da tabela de itens
                'TabIteOcdAcoCmc.BeginInit()
                While cnt <= i - 1
                    acaoComercial.ItensOrcamento(cnt).IndUtzIte = 0 'TabIteOcdAcoCmc.SetData(cnt, "IndUtzIte", 0)
                    cnt += 1
                End While

                cnt = 0
                'percorre todas as linhas da tabela de itens 
                While cnt <= i - 1

                    'verifico se é brinde 
                    'If TabIteOcdAcoCmc.GetData(cnt, "IndBde") = 1 And TabIteOcdAcoCmc.GetData(cnt, "IndUtzIte") = 0 Then
                    If acaoComercial.ItensOrcamento(cnt).INDBDEOPEDSNACOCMC = 1 And acaoComercial.ItensOrcamento(cnt).IndUtzIte = 0 Then

                        'TabIteOcdAcoCmc.SetData(cnt, "IndUtzIte", 1) 'marco item como utilizado
                        'acaoComercial.ItensOrcamento(cnt).VLRUNTITEOCDACOCMC
                        VlrTot = (acaoComercial.ItensOrcamento(cnt).VLRUNTITEOCDACOCMC * acaoComercial.ItensOrcamento(cnt).QDEITEOCDACOCMC)
                        VlrTot = VlrTot * (acaoComercial.VLRTOTBDEACOCMC / 100) ' pega a taxa de brinde que está na tabela
                        CodFrn = acaoComercial.ItensOrcamento(cnt).CODFRNPCPMER 'pega o código do fornecedor da mercadoria

                        i = cnt + 1
                        While i <= acaoComercial.ItensOrcamento.Count - 1 'conta a partir do primeiro brinde
                            ' verifico se o código item é brinde e se for  verifico se o código do fornecedor é igual
                            If acaoComercial.ItensOrcamento(i).INDBDEOPEDSNACOCMC = 1 Then
                                'If acaoComercial.ItensOrcamento(i).CODFRNPCPMER = CodFrn And acaoComercial.ItensOrcamento(i)., "IndUtzIte") = 0 Then
                                If acaoComercial.ItensOrcamento(i).CODFRNPCPMER = CodFrn And acaoComercial.ItensOrcamento(cnt).IndUtzIte = 0 Then
                                    'TabIteOcdAcoCmc.SetData(i, "IndUtzIte", 1)
                                    acaoComercial.ItensOrcamento(cnt).IndUtzIte = 1
                                    VlrAux = ((acaoComercial.ItensOrcamento(i).VLRUNTITEOCDACOCMC) * (acaoComercial.ItensOrcamento(i).QDEITEOCDACOCMC))
                                    VlrAux = VlrAux * (acaoComercial.VLRTOTBDEACOCMC / 100) ' pega a taxa de brinde que está na tabela
                                    VlrTot = VlrTot + VlrAux
                                End If
                            End If
                            i += 1
                        End While

                        'percorre a tabela de fornecedores 
                        CntAux = 0
                        While CntAux <= acaoComercial.Fornecedores.Count - 1
                            'verifico se o fornecedor é igual ao da mercadoria
                            If CodFrn = acaoComercial.Fornecedores(CntAux).CODFRN Then
                                vlr = acaoComercial.Fornecedores(CntAux).VLRTXAADMBDEACOCMC
                                If CDbl(vlr) < CDbl(VlrTot) Then
                                    oRow = oRow + "10" + "|"
                                    CntAux = acaoComercial.Fornecedores.Count - 1
                                End If
                            End If
                            CntAux += 1
                        End While
                    End If
                    cnt += 1
                End While

                'PAGAMENTOS DIVERSOS
                cnt = 0
                i = acaoComercial.ItensOrcamento.Count   'TabIteOcdAcoCmc.Rows.Count  'número de linhas da tabela de itens
                'percorre todas as linhas da tabela de itens 
                While cnt <= i - 1
                    str = acaoComercial.ItensOrcamento(cnt).DESOPEFSCDSNACOCMC.Trim()  'Trim(TabIteOcdAcoCmc.GetData(cnt, "TipOpe").Split("-")(1))
                    If str.Equals("PAGAMENTOS DIVERSOS") = True Then
                        oRow = oRow + "14" + "|"
                        CntAux = i
                    End If
                    cnt += 1
                End While

                'VALOR DE ACORDO OU EXTRA ACORDO NAO ORCADO
                data = Now
                str = Format(data, "yyyy-MM-dd")
                datRef = Integer.Parse(str.Split("-")(0) & str.Split("-")(1))


                'busca dados do plano de marketing
                If Not (acaoComercial.TIPEVTACOCMC Is Nothing) Then
                    TipEvt = acaoComercial.TIPEVTACOCMC
                End If
                If Not (acaoComercial.CODEVTACOCMC Is Nothing) Then
                    CodEvt = acaoComercial.CODEVTACOCMC
                End If
                datRef = Integer.Parse(datRef)
                CodUnNgc = Integer.Parse(0)

                rsDdo = boRelacaoPlanoMarketing.BscDdoPlnMkt(TipEvt, CodEvt, datRef, CodUnNgc)
                i = acaoComercial.Fornecedores.Count 'número de linhas de fornecedores

                'percorre as linhas do grid de fornecedores
                cnt = 0
                For cnt = 0 To i - 1
                    codUndNgc = CInt(acaoComercial.Fornecedores(cnt).CODUNDESRNGC)
                    CntAux = rsDdo.Count - 1
                    aux = 0
                    While aux < CntAux
                        If codUndNgc = rsDdo.Item(aux).CODUNDESRNGC Then
                            'verifico se foi digitado valor de contrato
                            If Not (acaoComercial.Fornecedores(cnt).VLRUTZCTTACOCMC Is Nothing) Then
                                If acaoComercial.Fornecedores(cnt).VLRUTZCTTACOCMC > 0 Then
                                    If rsDdo.Item(aux).VLRSLDATUPLNMKT = 0 Then '("VlrOcdCttPlnMkt") 
                                        oRow = oRow + "13" + "|"
                                        aux = CntAux
                                        cnt = i
                                    End If

                                End If
                            End If
                        End If
                        aux += 1
                    End While
                Next

                cnt = 0
                For cnt = 0 To i - 1
                    codUndNgc = acaoComercial.Fornecedores(cnt).CODUNDESRNGC
                    CntAux = rsDdo.Count - 1
                    aux = 0
                    While aux < CntAux
                        If codUndNgc = rsDdo.Item(aux).CODUNDESRNGC Then
                            'verifico se foi digitado valor de contrato
                            If Not (acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC Is Nothing) Then
                                If acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC > 0 Then
                                    If rsDdo.Item(aux).VLRSLDMNSPLNMKT = 0 Then '("VlrOcdExaCttPlnMkt")
                                        oRow = oRow + "12" + "|"
                                        aux = CntAux
                                        cnt = i
                                    End If
                                End If
                            End If
                        End If
                        aux += 1
                    End While
                Next

                'Verifica se há algum código de política infringida
                If oRow <> "" Then
                    'insere sim 
                End If

                Return aux

            Catch ex As Exception
                IndErrAcoCmc = 1
                Throw New Exception(String.Format("Erro ao verificar a ação -Ação {0} - {1}", acaoComercial.CODACOCMC, ex.Message))
            Finally
            End Try
        End Function

        ''' <summary>
        ''' Carrega listas de dropdown das telas de cadastro e pesquisa
        ''' </summary>
        Public Function CarregaListasDropDown(ByVal indPreAcordo As Boolean)
            ViewBag.listaStatus = ListarStatus()
            ViewBag.listaTipoEvento = boTipoEvento.ListTipoDeEventosPreAcordo(indPreAcordo)

            Dim listFilial As List(Of FilialEmpresaCorporativoViewModel) = Mapper.Map(Of List(Of VO.FilialEmpresaCorporativo), List(Of FilialEmpresaCorporativoViewModel))(boFilialEmpresaCorporativo.ListFiliais())
            ViewBag.listaFilial = listFilial

            ViewBag.listarB2B = ListarB2B()
            ViewBag.listarDestinoDespesasCash = ListarDestinoDespesasCash()

            ViewBag.listarEventoPromocional = boEventoPromocional.ListEventoPromocional()

            ViewBag.listaEventos = New List(Of VO.Evento())
            ViewBag.listaCanaisVenda = boCanalVenda.ListarCanalDeVenda()
            'If (indPreAcordo) Then
            '    'Carregar lista de Filial
            'End If
        End Function

        ''' <summary>
        ''' Carrega listas de dropdown da tela de pesquisa
        ''' </summary>
        Public Sub CarregaListasDropDownIndex(ByVal indPreAcordo As Nullable(Of Boolean))
            ViewBag.listarB2B = ListarB2B()
            ViewBag.ListaStatus = ListarStatus()
            ViewBag.ListaFuncionarios = boAauthor.ListFuncionarios(New VO.Funcionario()).OrderBy(Function(m) m.CODFNC)
            ViewBag.listaTipoEvento = boTipoEvento.ListTipoDeEventosPreAcordo(indPreAcordo)
            ViewBag.ListaEventos = boEvento.ListT(New VO.Evento()).OrderBy(Function(m) m.CODEVTACOCMC)

        End Sub


        '''' <summary>
        '''' Controla a visibilidade de componentes da tela quando a acao eh nova
        '''' </summary>
        'Public Sub ControlaVisibilidadeComponentes()

        '    ViewBag.btnReprovarVisible = False
        '    ViewBag.btnPoliticaVisible = False
        '    ViewBag.btnRejeitarVisible = False
        '    ViewBag.btnSlcIncentivoVisible = False
        '    ViewBag.btnPlanoMarketingEnable = True

        '    ViewBag.canalDeVendaVisible = False
        '    ViewBag.txtAcaoOriginalVisible = False
        '    ViewBag.aprovadorVisible = True
        '    ViewBag.dropEventoEnable = True
        '    ViewBag.dropTipoEventoEnable = True
        '    ViewBag.dropFilial = True
        '    ViewBag.dataValidadeEnable = True
        '    ViewBag.txtNomeAcaoEnable = True
        '    ViewBag.complementarEnable = True
        '    ViewBag.txtAcaoOriginalEnable = True
        '    ViewBag.btnMecanicaEnable = True

        '    ViewBag.btnIsrItemVisible = True
        '    ViewBag.btnIsrFornecedorVisible = True
        '    ViewBag.btnIsrPremiacaoVisible = False
        '    ViewBag.btnIsrBonificacaoVisible = False
        '    ViewBag.btnIsrPatrocinadorEnable = True
        '    ViewBag.pnlTipoPatrocinadorEnable = False

        '    ViewBag.btnGravarVisible = True
        '    ViewBag.btnEnviarVisible = True
        '    ViewBag.btnClonarVisible = True
        '    ViewBag.btnDarParecerVisible = False
        '    ViewBag.btnAprovacaoVisible = False

        'End Sub

        ''' <summary>
        ''' Controla a visibilidade de componentes da tela quando a acao nao eh nova
        ''' </summary>
        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            If (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc) Then
                ViewBag.btnAdicinarMecanica = True
                ViewBag.btnEditarMecanica = True
            Else
                ViewBag.btnAdicinarMecanica = False
                ViewBag.btnEditarMecanica = False
            End If

            ViewBag.btnIsrFornecedorVisible = False
            ViewBag.btnIsrCarimboVisible = False
            ViewBag.btnIsrPremiacaoVisible = False
            ViewBag.btnIsrBonificacaoVisible = False

            ViewBag.dropEventoEnable = True
            ViewBag.dropTipoEventoEnable = True
            ViewBag.dropFilial = True
            ViewBag.dataValidadeEnable = True
            ViewBag.txtNomeAcaoEnable = True
            ViewBag.complementarEnable = True
            ViewBag.txtAcaoOriginalEnable = True
            ViewBag.btnMecanicaEnable = True
            ViewBag.canalVendaVisible = False

            ViewBag.btnGravarVisible = True
            ViewBag.btnClonarVisible = True
            ViewBag.btnEnviarVisible = True
            ViewBag.aprovadorVisible = True
            If (Not IsNothing(acao.CODSTAAPVACOCMC) AndAlso acao.CODSTAAPVACOCMC = 5) Then
                ViewBag.btnGasto = True
            Else
                ViewBag.btnGasto = False
            End If

            ViewBag.btnPoliticaVisible = False
            ViewBag.btnRejeitarVisible = False

            ViewBag.btnIsrItemVisible = True
            ViewBag.btnIsrPatrocinadorEnable = True

            ViewBag.btnPlanoMarketingEnable = True
            ViewBag.pnlTipoPatrocinadorEnable = False
            ViewBag.canalVendaEnable = True

            If (Not acao.CODSTAAPVACOCMC Is Nothing) And (acao.CODSTAAPVACOCMC <> Constantes.Status.NOVA And acao.CODSTAAPVACOCMC <> Constantes.Status.REJEITADA) Then
                ViewBag.canalVendaEnable = False
                ViewBag.dropTipoEventoEnable = False
                ViewBag.dropEventoEnable = False
                ViewBag.dropFilial = False
                ViewBag.txtNomeAcaoEnable = False
            End If

            ViewBag.btnSlcIncentivoVisible = (acao.INDVLDRGRICT = 1) 'Incentivo


            If (Not acao.TIPEVTACOCMC Is Nothing) Then
                Dim bo As New TipoDeEventoBO
                Dim tipoEvento = bo.SelectT(New VO.TipoDeEvento With {.TIPEVTACOCMC = acao.TIPEVTACOCMC})
                ViewBag.INDCPHBTB = tipoEvento.INDCPHBTB
                ViewBag.INDEVTSMAVisible = tipoEvento.INDEVTSMA
            End If


            If (Not acao.CODEVTACOCMC Is Nothing) Then
                Dim bo As New EventoBO
                Dim evento = bo.SelectT(New VO.Evento With {.CODEVTACOCMC = acao.CODEVTACOCMC})
                ViewBag.INDVCLPMC = evento.INDVCLPMC
            End If


            If (acao.INDACOCMCCPL = 1) Then
                ViewBag.txtAcaoOriginalVisible = True
            End If

            If acao.Carimbos IsNot Nothing AndAlso acao.Carimbos.Count > 0 Then
                'acao.TIPACOMCD = TipoAcao.Carimbo
                ViewBag.btnIsrCarimboVisible = True
                Dim parametro As Integer
                If IsNothing(acao.CODSTAAPVACOCMC) Then
                    parametro = 0
                Else
                    parametro = acao.CODSTAAPVACOCMC
                End If
                ControlaVisibilidadeComponentesCarimbo(parametro)
            ElseIf acao.Bonificacoes IsNot Nothing AndAlso acao.Bonificacoes.Count > 0 Then
                ViewBag.btnIsrBonificacaoVisible = True
                'acao.TIPACOMCD = TipoAcao.PedidoBonificacao
            ElseIf acao.Premiacao IsNot Nothing AndAlso acao.Premiacao.Count > 0 Then
                ViewBag.btnIsrPremiacaoVisible = True
                'acao.TIPACOMCD = TipoAcao.Premiacao
            Else
                ViewBag.btnIsrFornecedorVisible = True
                'acao.TIPACOMCD = TipoAcao.Fornecedor
            End If

            'AtuDdoAco
            If (acao.CODSTAAPVACOCMC IsNot Nothing AndAlso acao.CODSTAAPVACOCMC <> 0) Then
                ViewBag.aprovadorVisible = False
            End If

            If (acao.TIPDSNDSCBNF = 99 And acao.CODSTAAPVACOCMC = Constantes.Status.NOVA) Then
                ViewBag.pnlTipoPatrocinadorEnable = True
            End If

            If acao.CODACOCMC IsNot Nothing Then
                'Busca dados da política da ação
                Dim reprovacoesEPoliticas = bo.BuscarNumeroReprovacoesEPoliticas(acao.CODACOCMC)
                If reprovacoesEPoliticas.TotalPoliticas > 0 And acao.CODSTAAPVACOCMC <> Constantes.Status.NOVA Then
                    ViewBag.btnPoliticaVisible = True
                End If
                'atuCanalVenda
                ViewBag.canalVendaVisible = (acao.CODCNLVNDINI IsNot Nothing AndAlso acao.CODCNLVNDINI <> 0) Or boItensOrcamento.ValidaNecessidadeCanalVenda(acao.CODACOCMC)
                acao.CanalVendaVisible = ViewBag.canalVendaVisible
            End If

            'atuCanalVenda


            If acao.CODACOCMC IsNot Nothing Then

                ControleBotoesIndicadorAprovacao(Nothing)

                Dim nomUsrRede = If(acao.NOMUSRRCF, "")
                If (acao.CODSTAAPVACOCMC = Constantes.Status.ENVIANDO_PARA_APROVACAO Or
                acao.CODSTAAPVACOCMC = Constantes.Status.EM_APROVACAO Or
                acao.CODSTAAPVACOCMC = Constantes.Status.APROVADA Or
                acao.CODSTAAPVACOCMC = Constantes.Status.PARECER_NEGATIVO Or
                Not ControleAcesso.CodFnc() = acao.CODFNCCRIACOCMC) Then

                    ViewBag.btnGravarVisible = False
                    ViewBag.btnClonarVisible = False
                    ViewBag.btnEnviarVisible = False

                    'ViewBag.btnIsrFornecedorVisible = False
                    ViewBag.btnIsrItemVisible = False

                    ViewBag.btnIsrPatrocinadorEnable = False

                    ViewBag.dataValidadeEnable = False
                    ViewBag.txtAcaoOriginalEnable = False
                    ViewBag.complementarEnable = False

                End If
            End If

            If (Not IsNothing(acao.INDACOCMCCPL) AndAlso acao.INDACOCMCCPL <> 0) Then
                ViewBag.dropTipoEventoEnable = False
                ViewBag.dropEventoEnable = False
                ViewBag.dropFilial = False
            End If

            Dim dvFluApv As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)
            If Not acao.CODACOCMC Is Nothing Then
                dvFluApv = boFluxoAprovacao.BuscaFluxosAprovacaoDaAcaoDoUsuario(acao.CODACOCMC, ControleAcesso.CodFnc())
                If Not IsNothing(dvFluApv) Then
                    If dvFluApv.Count > 0 Then
                        If dvFluApv.FirstOrDefault.TIPSTAFLUAPV = 3 Then
                            ViewBag.btnDarParecerVisible = False
                            ViewBag.btnAprovacaoVisible = True
                        ElseIf dvFluApv.FirstOrDefault.TIPSTAFLUAPV = 8 Then
                            ViewBag.btnDarParecerVisible = True
                            ViewBag.btnAprovacaoVisible = False
                        Else
                            ViewBag.btnDarParecerVisible = False
                            ViewBag.btnAprovacaoVisible = False
                        End If
                    Else
                        ViewBag.btnDarParecerVisible = False
                        ViewBag.btnAprovacaoVisible = False
                    End If
                Else
                    ViewBag.btnDarParecerVisible = False
                    ViewBag.btnAprovacaoVisible = False
                End If
            Else
                ViewBag.btnDarParecerVisible = False
                ViewBag.btnAprovacaoVisible = False
            End If

        End Sub

        ''' <summary>
        ''' InitializeComponent
        ''' Controla a visibilidade de campos para quando a acao contem carimbos
        ''' </summary>
        ''' <param name="statusAcao"></param>
        Public Sub ControlaVisibilidadeComponentesCarimbo(statusAcao As Integer)

            'Explicito
            If (statusAcao <> Constantes.Status.NOVA) Then
                ViewBag.btnDarParecerVisible = False
                ViewBag.btnAprovacaoVisible = False
                ViewBag.btnClonarVisible = False
                ViewBag.btnGravarVisible = False
                ViewBag.btnEnviarVisible = False
                ViewBag.btnPlanoMarketingEnable = False
                ViewBag.btnMecanicaEnable = False
                ViewBag.btnCarimboEnable = False

                ViewBag.dropEventoEnable = False
                ViewBag.dropTipoEventoEnable = False
                ViewBag.dropFilial = False
                ViewBag.dataValidadeEnable = False
                ViewBag.txtNomeAcaoEnable = False
            End If

        End Sub

        ''' <summary>
        ''' Controla visibilidade dos botoes da tela de acordo com o indicador de aprovacao
        ''' configurarAprovacao
        ''' </summary>
        Private Sub ControleBotoesIndicadorAprovacao(dsFluApv As DataSet)

            Dim IndApv = boFluxoAprovacao.VerificarTipoDeAprovacao(dsFluApv)

            Select Case IndApv
                Case Constantes.IndicadorAprovacao.Normal
                    ViewBag.btnAprovacaoVisible = False
                    ViewBag.btnDarParecerVisible = False
                Case Constantes.IndicadorAprovacao.Aprovacao
                    ViewBag.btnAprovacaoVisible = True
                    ViewBag.btnDarParecerVisible = False
                Case Constantes.IndicadorAprovacao.Parecer
                    ViewBag.btnAprovacaoVisible = False
                    ViewBag.btnDarParecerVisible = True
            End Select
        End Sub

        ''' <summary>
        ''' Busca o Empenho Exa Acordo de acordo com o evento escolhido
        ''' </summary>
        ''' <param name="idEvento"></param>
        ''' <returns></returns>
        <HttpGet()>
        Public Function BuscaEmpenhoExaAcordo(idEvento As Decimal?) As ActionResult

            Dim evento = boEvento.ConsultaDadosEventoPeloCodigo(idEvento)

            evento.DESDSNDSCBNF = evento.DESDSNDSCBNF.Trim()
            Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
            acaoModel.TIPDSNDSCBNF = evento.TIPDSNDSCBNF
            acaoModel.EmpenhoExa = evento.TIPDSNDSCBNF & " - " & evento.DESDSNDSCBNF
            Session("acaoModel") = acaoModel
            Return Json(evento, JsonRequestBehavior.AllowGet)

        End Function

        ''' <summary>
        ''' CarregaFornecedorMarketing
        ''' </summary>
        ''' <param name="empenhoFrnMkt"></param>
        ''' <param name="status"></param>
        ''' <param name="acaoModel"></param>
        Private Sub CarregaFornecedorMarketing(empenhoFrnMkt As String, status As String, acaoModel As AcaoComercialViewModel)
            Dim codigoFornecedorMarketing As Decimal
            Dim fornecedorMarketing As VO.Fornecedores
            Dim saldoFornecedorMarketing As Decimal
            Dim nomeFornecedorMarketing As String

            'Obter o codigo do fornecedor de marketing
            Dim dsFornecedorMarketing = boParametros.ListarParametrosAcaoComercial()
            If Not dsFornecedorMarketing Is Nothing Then
                If dsFornecedorMarketing.Count > 0 Then
                    codigoFornecedorMarketing = dsFornecedorMarketing(0).CODFRNMKT
                Else
                    MsgBox("Código Do fornecedor de Marketing não encontrado!" & vbCrLf & "Contate o analista responsável.", MsgBoxStyle.Critical, "Fornecedor de Marketing")
                End If
            End If

            'Obter o codigo do fornecedor de marketing
            fornecedorMarketing = boFornecedor.BuscarNomeCodigoFornecedorPorCodigo(codigoFornecedorMarketing)
            If Not fornecedorMarketing Is Nothing Then
                nomeFornecedorMarketing = fornecedorMarketing.NOMFRN
            Else
                MsgBox("Nome Do fornecedor de Marketing não encontrado!" & vbCrLf & "Contate o analista responsável.", MsgBoxStyle.Critical, "Fornecedor de Marketing")
            End If

            If status = "NOVA" Then
                'Obter saldo do fornecedor de marketing
                Dim data = Now
                Dim strAux = Format(data, "dd/MM/yyyy")
                Dim strDat = strAux.Split("/"c)(2) & strAux.Split("/"c)(1)

                Dim dsSaldoFornecedorMarketing = bo.BuscarSaldoFornecedorMarketing(codigoFornecedorMarketing, empenhoFrnMkt, strDat)

                If dsSaldoFornecedorMarketing.Count > 0 Then
                    saldoFornecedorMarketing = dsSaldoFornecedorMarketing(0).VLRSLDDSP
                Else
                    saldoFornecedorMarketing = CType("0,00", Decimal)
                End If
            Else
                If acaoModel.Fornecedores.Count > 1 Then
                    saldoFornecedorMarketing = acaoModel.Fornecedores(0).VLRSLDCNTCRRFRN
                End If
            End If

            'Obter BU do fornecedor
            Dim dsBU = boUnidadeNegocio.BuscaUnidadeNegocioFornecedor(codigoFornecedorMarketing)

            'If dsBU.Count > 0 Then
            '    BU = dsBU(0).CODUNDESRNGC & " - " & dsBU(0).DESUNDESRNGC
            'Else
            '    MsgBox("BU Do fornecedor de Marketing não encontrado!" & vbCrLf & "Contate o analista responsável.", MsgBoxStyle.Critical, "Fornecedor de Marketing")
            'End If

            ''Insere Dados no grid
            'With TabFrnAcoCmc
            '    .Rows.Add()
            '    .BeginInit()
            '    .SetData(1, "Frn", codigoFornecedorMarketing & " - " & nomeFornecedorMarketing)
            '    .SetData(1, "SldCntCrr", saldoFornecedorMarketing)
            '    .SetData(1, "UndNgc", BU)

            '    If .Rows.Count >= 2 Then
            '        If CType(.GetData(1, "extAux"), Decimal) = 0 Then
            '            .SetData(1, "extAux", CType("0,00", Decimal))
            '        End If
            '    Else
            '        .SetData(1, "extAux", CType("0,00", Decimal))
            '    End If

            '    .EndInit()
            '    .Refresh()
            'End With

            'ConfiguraGridFornecedorMarketing()

            'If Not txtStaAcoCmc.Text = "NOVA" Then 'Or txtStaAcoCmc.Text = "REJEITADA" Then 'verifica se é nova 
            '    TabFrnAcoCmc.Cols("extAux").AllowEditing = False
            'End If
        End Sub

        <HttpGet()>
        Function ListarEventosDoTipoEventoAno(ByVal id As Integer) As ActionResult
            Dim bo As New EventoBO
            Dim eventos = bo.ListEventoAno(New VO.Evento With {.TIPEVTACOCMC = id})
            'Dim eventos = boEvento.ListT(New VO.Evento()).OrderBy(Function(m) m.CODEVTACOCMC)

            Return Json(Mapper.Map(Of List(Of VO.Evento), List(Of EventoViewModel))(eventos), JsonRequestBehavior.AllowGet)
        End Function

        <HttpPost()>
        Function BuscarTipoEvento(ByVal id As Integer) As JsonResult
            Try
                Dim bo As New TipoDeEventoBO
                Dim tipoEvento = bo.SelectT(New VO.TipoDeEvento With {.TIPEVTACOCMC = id})

                Dim tipoEventoViewModel = Mapper.Map(Of VO.TipoDeEvento, TipoEventoViewModel)(tipoEvento)

                If (tipoEvento.INDVLDRGRICT = 1) Then
                    ViewBag.btnSlcIncentivoVisible = True
                Else
                    ViewBag.btnSlcIncentivoVisible = False
                End If

                Session.Item("INDEVTSMA") = tipoEvento.INDEVTSMA

                Return Json(tipoEventoViewModel, JsonRequestBehavior.AllowGet)  'tipoEvento.INDVLDRGRICT.GetValueOrDefault()
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' busca ação comercial principal
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function buscarAcaoPrincipal(codAcaoPrincipal As Integer, ByVal preAcordo As Boolean) As JsonResult
            Try
                Dim resultado = bo.BuscarAcaoComercialPorId(codAcaoPrincipal)

                If (preAcordo = True) Then
                    If (IsNothing(resultado) OrElse (resultado.TIPACOMCD < TipoAcao.PreAcordoVigencia)) Then
                        Warning("Pré-Acordo não encontrado.")
                    ElseIf (resultado.TIPACOMCD = 5 Or resultado.TIPACOMCD = 6) Then
                        Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                        acaoCache.TIPEVTACOCMC = resultado.TIPEVTACOCMC
                        acaoCache.CODEVTACOCMC = resultado.CODEVTACOCMC
                        acaoCache.CODFILEMPORIVBA = resultado.CODFILEMPORIVBA

                        Session("acaoModel") = acaoCache

                        Return Json(resultado, JsonRequestBehavior.AllowGet)

                    Else
                        If resultado.TIPACOMCD = TipoAcao.PreAcordoVigencia Then
                            Warning("Esse Pré-Acordo é Limitado por Vigência!")
                        ElseIf resultado.TIPACOMCD = TipoAcao.PreAcordoValor Then
                            Warning("Esse Pré-Acordo é Limitado por Valor!")
                        End If

                    End If

                Else
                    If (IsNothing(resultado) OrElse (resultado.TIPACOMCD > TipoAcao.Premiacao)) Then
                        Warning("Ação Principal não foi encontrada.")
                    ElseIf (resultado.TIPACOMCD <> 5 And resultado.TIPACOMCD <> 6) Then
                        Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                        acaoCache.TIPEVTACOCMC = resultado.TIPEVTACOCMC
                        acaoCache.CODEVTACOCMC = resultado.CODEVTACOCMC
                        acaoCache.CODFILEMPORIVBA = resultado.CODFILEMPORIVBA
                        Session("acaoModel") = acaoCache

                        Return Json(resultado, JsonRequestBehavior.AllowGet)
                    End If

                End If

                Return Nothing
            Catch ex As Exception
                Danger(ex)
            End Try
        End Function

        ''' <summary>
        ''' retorna grid de fornecedores da ação principal
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function atualizarFornecedoresAcaoPrincipal(codAcaoPrincipal As Integer) As PartialViewResult
            Try
                Dim resultado = bo.BuscarAcaoComercialPorId(codAcaoPrincipal)

                Dim retorno As AcaoComercialViewModel = Mapper.Map(Of VO.AcaoComercial, AcaoComercialViewModel)(resultado)

                retorno.StateGridFornecedores = GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel).EmptyGrid("gridFornecedores", "AtualizarGridFornecedores", "Fornecedor")
                retorno.StateGridFornecedores.ListPaged = PaginarLista(retorno.Fornecedores)
                retorno.StateGridFornecedores.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                retorno.StateGridFornecedores.TotalCount = retorno.Fornecedores.Count

                retorno.StateGridCarimbos = GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid("gridCarimbos", "AtualizarGridCarimbos", "Carimbo")
                retorno.StateGridCarimbos.ListPaged = PaginarLista(retorno.Carimbos)
                retorno.StateGridCarimbos.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                retorno.StateGridCarimbos.TotalCount = retorno.Carimbos.Count

                retorno.StateGridBonificacoes = GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel).EmptyGrid("gridBonificacoes", "AtualizarGridBonificacoes", "PedidoBonificacao")
                retorno.StateGridBonificacoes.ListPaged = PaginarLista(retorno.Bonificacoes)
                retorno.StateGridBonificacoes.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                retorno.StateGridBonificacoes.TotalCount = retorno.Bonificacoes.Count

                retorno.StateGridPremiacao = GridSettings(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel).EmptyGrid("gridPremiacoes", "AtualizarGridPremiacoes", "PremiacaoExclusiva")
                retorno.StateGridPremiacao.ListPaged = PaginarLista(retorno.Premiacao)
                retorno.StateGridPremiacao.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                retorno.StateGridPremiacao.TotalCount = retorno.Premiacao.Count

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoCache.StateGridFornecedores = retorno.StateGridFornecedores
                acaoCache.Fornecedores = retorno.Fornecedores
                acaoCache.StateGridCarimbos = retorno.StateGridCarimbos
                acaoCache.Carimbos = retorno.Carimbos
                acaoCache.StateGridBonificacoes = retorno.StateGridBonificacoes
                acaoCache.Bonificacoes = retorno.Bonificacoes
                acaoCache.StateGridPremiacao = retorno.StateGridPremiacao
                acaoCache.Premiacao = retorno.Premiacao

                retorno.TIPEVTACOCMC = resultado.TIPEVTACOCMC
                Session("acaoModel") = acaoCache

                ControlaVisibilidadeComponentes(acaoCache)
                Return PartialView("~/Views/AcaoComercial/Patrocinador/_WidgetFornecedor.vbhtml", retorno)
            Catch ex As Exception
                Danger(ex)
            End Try
        End Function
#End Region

#Region "Fluxo"
        ''' <summary>
        ''' Abre o modal de fluxo de aprovacoes de uma acao
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function VisualizarFluxo(ByVal objeto As Decimal) As PartialViewResult

            Dim dadosFluxo = boFluxoAprovacao.BuscaFluxosAprovacaoDaAcao(objeto)

            If (dadosFluxo.Count = 0) Then
                Information(Constantes.FLUXO_NAO_ENCONTRADO)
                Return Nothing
            End If

            Dim dadosFluxoView = Mapper.Map(Of List(Of VO.RelacaoAcaoComercialxFluxoAprovacao), List(Of RelacaoAcaoComercialxFluxoAprovacaoViewModel))(dadosFluxo)
            Dim gridFluxo = PaginarEAtualizarGrid(dadosFluxoView, New PageSettings())
            gridFluxo.ActionName = "AtualizarGridFluxo"
            gridFluxo.ResultID = "gridFluxoAprovacoes"
            gridFluxo.PaginationID = "gridFluxoAprovacoes"
            gridFluxo.Filter = New RelacaoAcaoComercialxFluxoAprovacaoViewModel With {.CODACOCMC = objeto}


            Return PartialView("~/Views/AcaoComercial/Fluxo/_ModalFluxo.vbhtml", gridFluxo)

        End Function

        ''' <summary>
        ''' Atualiza o grid do modal de fluxo de aprovacoes (paginacao e ordenacao)
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function AtualizarGridFluxo(dadosPesquisa As GridSettings(Of RelacaoAcaoComercialxFluxoAprovacaoViewModel)) As PartialViewResult

            Dim dadosFluxo As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao) = boFluxoAprovacao.BuscaFluxosAprovacaoDaAcao(dadosPesquisa.Filter.CODACOCMC.Value)
            Dim dadosFluxoView = Mapper.Map(Of List(Of VO.RelacaoAcaoComercialxFluxoAprovacao), List(Of RelacaoAcaoComercialxFluxoAprovacaoViewModel))(dadosFluxo)
            Dim gridConf = PaginarEAtualizarGrid(dadosFluxoView, dadosPesquisa.Page, dadosPesquisa.Filter, dadosPesquisa)

            Return PartialView("~/Views/AcaoComercial/Fluxo/_GridFluxo.vbhtml", gridConf)

        End Function

        ''' <summary>
        ''' função para abrir modal de aprovação
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function abrirModalAprovacao(objeto As String) As PartialViewResult
            Try
                Dim resultado() As String = objeto.Split("|")
                Dim descricao As String = resultado(0)
                Dim nomeTela As String = resultado(1)
                Dim codAcao As String = resultado(2)
                Dim codigo As Integer = CInt(codAcao)
                Dim fluxo As New DadosFluxoAcaoComercial()
                Dim CodFnc As Integer = ControleAcesso.CodFnc()

                If nomeTela = "PreAcordo" Then
                    fluxo.IndPreAcordo = True

                    Dim dadosRelacaoAcaoComercialxFluxoAprovacao As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao) = boFluxoAprovacao.BuscaFluxosAprovacaoDaAcao(codigo)

                    For Each item In dadosRelacaoAcaoComercialxFluxoAprovacao.Where(Function(x) x.NUMSEQNIVAPV = 101 And x.DATHRAAPVFLU Is Nothing)
                        fluxo.NUMSEQNIVAPV = item.NUMSEQNIVAPV
                    Next

                    If (fluxo.NUMSEQNIVAPV = 101) Then
                        ViewBag.listaFormaPagamento = boFluxoDeAprovacao.BuscaFormaPagamento()
                    End If
                End If

                If (descricao = "parecer") Then
                    fluxo.radio = 3
                    ViewBag.parecer = False
                Else
                    fluxo.radio = 1
                    ViewBag.parecer = True
                End If

                Return PartialView("~/Views/AcaoComercial/Aprovacao/_ModalAprovacao.vbhtml", fluxo)
            Catch ex As Exception
                Danger(ex)
            End Try
        End Function

        '''<summary>
        ''' função de clicar em ok no modal de aprovação
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function SalvarAprovacao(flux As DadosFluxoAcaoComercial) As JavaScriptResult
            Try
                Select Case flux.radio
                    Case 1
                        flux.aprovar = True
                        flux.rejeitar = False
                        flux.parecer = False
                        flux.pedir = False
                    Case 2
                        flux.aprovar = False
                        flux.rejeitar = True
                        flux.parecer = False
                        flux.pedir = False
                    Case 3
                        flux.aprovar = False
                        flux.rejeitar = False
                        flux.parecer = True
                        flux.pedir = False
                    Case 4
                        flux.aprovar = False
                        flux.rejeitar = False
                        flux.parecer = False
                        flux.pedir = True
                    Case Else
                        Return Nothing

                End Select
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                If (acaoCache Is Nothing) Then
                    Warning("Esta tela esta a muito tempo aberta e as informações podem estar desatualizadas, atualize a pagina para prosseguir")
                    Return Nothing
                End If

                If (acaoCache.TIPACOMCD = TipoAcao.PreAcordoVigencia Or acaoCache.TIPACOMCD = TipoAcao.PreAcordoValor) Then
                    flux.IndPreAcordo = True
                End If

                If validarEntrada(flux) Then

                    If atuFluApvAcoCmc(flux) Then

                        'Information("Operação realizada com sucesso!")
                        'Return Nothing
                    Else

                        Dim dsPoliticaDivergente As List(Of VO.Evento) = boEvento.ConsultaParaFluxo(acaoCache.CODACOCMC)

                        If (Not IsNothing(dsPoliticaDivergente) AndAlso dsPoliticaDivergente.Count > 0 AndAlso Not (dsPoliticaDivergente.FirstOrDefault.INDEVTMTDPOLDVR = 1)) Then
                            Danger("Ocorreu um erro na organização do fluxo da ação.Contacte o analista responsável!")
                        End If
                    End If

                End If

#If False Then
                Dim action As String = "CreateEdit"
                Select Case acaoCache.TIPACOMCD
                    Case TipoAcao.PreAcordoVigencia
                        action = "PreAcordoCreateEditVigencia"
                    Case TipoAcao.PreAcordoValor
                        action = "PreAcordoCreateEditValor"
                End Select
                Return JavaScript(String.Format("setTimeout(function(){{ window.location = SiteUrl + 'AcaoComercial/{1}/{0}'; }}, 3000);", acaoCache.CODACOCMC, action))
#Else

                Dim action As String = "Aprovacoes"
                If acaoCache.IndPreAcordo Then
                    action = "PreAcordoMinhasAprovacoes"
                Else
                    action = "MinhasAprovacoes"
                End If

                If (Session("IsMobile") = True) Then
                    action = "Aprovacoes"
                End If
                Return JavaScript(String.Format("setTimeout(function(){{ window.location = SiteUrl + 'AcaoComercial/{0}'; }}, 3000);", action))
#End If

            Catch ex As Exception
                Danger(ex)
            End Try
        End Function

        Private Function validarEntrada(objeto As DadosFluxoAcaoComercial) As Boolean
            Try
                If objeto.parecer Then
                    If String.IsNullOrEmpty(objeto.observacao) Then
                        Warning("É obrigatório preencher o campo de observação para a resposta do parecer!")
                        Return False
                    Else
                        Return True
                    End If
                ElseIf objeto.pedir Then

                    If objeto.funcionario <> 0 Then
                        Return True
                    Else
                        Warning("É obrigatório preencher o usuário que dará o parecer sobre a ação!")
                        Return False
                    End If
                Else
                    Return True
                End If

            Catch ex As Exception
                Danger(ex)
            End Try

        End Function

        Public Function CnsUsrOra(ByVal CodFnc As Integer) As Boolean
            'Busca usuario cadastrado no iPROC       
            Dim resultado = boperPeopple.PsqCadFnc(CodFnc)

            If Not resultado Is Nothing Then
                If resultado.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        End Function

        Private Function atuFluApvAcoCmc(objeto As DadosFluxoAcaoComercial) As Boolean
            Dim dvFluApv As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)
            Dim NumFluApv As Integer
            Dim NumSeqFluApv As Integer
            Dim NumSeqNivApv As Integer
            Dim ObsApv As String = Nothing
            Dim bFlgDlg As Boolean
            Dim CodFnc As Integer
            Dim CodEdeApv As Integer
            Dim TipAco As Integer

            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                CodFnc = ControleAcesso.CodFnc()

                If Not IsNothing(objeto.observacao) Then
                    ObsApv = objeto.observacao.Replace("'", " ").Trim()
                End If

                dvFluApv = boFluxoAprovacao.BuscaFluxosAprovacaoDaAcaoDoUsuario(acaoCache.CODACOCMC, CodFnc)
                dvFluApv = dvFluApv.OrderBy(Function(f) f.NUMSEQFLUAPV).ToList

                If dvFluApv.Count > 0 Then

                    CodEdeApv = CInt(dvFluApv.FirstOrDefault.CODEDEAPV)
                    NumFluApv = CInt(dvFluApv.FirstOrDefault.NUMFLUAPV)
                    NumSeqFluApv = CInt(dvFluApv.FirstOrDefault.NUMSEQFLUAPV)
                    NumSeqNivApv = CInt(dvFluApv.FirstOrDefault.NUMSEQNIVAPV)

                    If CodFnc = CodEdeApv Then
                        bFlgDlg = False
                    Else
                        bFlgDlg = True
                    End If

                End If

                If objeto.aprovar And objeto.IndPreAcordo And objeto.TIPFRMDSCBNF <> 0 Then
                    TipAco = 5
                ElseIf objeto.aprovar And objeto.IndPreAcordo And objeto.TIPFRMDSCBNF Is Nothing Then
                    TipAco = 5
                    objeto.TIPFRMDSCBNF = 0
                    objeto.QDEDIARCBPMS = 0
                ElseIf objeto.aprovar And objeto.IndPreAcordo = False Then
                    TipAco = 5
                    objeto.TIPFRMDSCBNF = 0
                    objeto.QDEDIARCBPMS = 0
                ElseIf objeto.parecer Then
                    TipAco = 8
                    objeto.TIPFRMDSCBNF = 0
                    objeto.QDEDIARCBPMS = 0
                ElseIf objeto.pedir Then
                    TipAco = 6
                    objeto.TIPFRMDSCBNF = 0
                    objeto.QDEDIARCBPMS = 0
                ElseIf objeto.rejeitar Then
                    TipAco = 4
                    objeto.TIPFRMDSCBNF = 0
                    objeto.QDEDIARCBPMS = 0
                End If

                If TipAco = 4 Then
                    RetornaCadMcoVbaFrn()
                End If

                If objeto.aprovar Then

                    Dim dsPoliticaDivergente As List(Of VO.Evento) = boEvento.ConsultaParaFluxo(acaoCache.CODACOCMC)

                    'If Not (CType(dsPoliticaDivergente.FirstOrDefault.INDEVTMTDPOLDVR, Integer) = 1) Then
                    '    'TODO: nao encontrei valor das variais utilizadas no if
                    '    'If valorGastoEvento > valorOrcadoEvento Then
                    '    '    'If MessageBox.Show("O valor gasto até o momento de: " & Format(valorGastoEvento, "###,##0.#0") & " é superior ao valor orçado no evento de: " & Format(valorOrcadoEvento, "###,##0.#0") & " !" & vbCrLf _
                    '    '    '                    & "Deseja aprovar mesmo assim?", _
                    '    '    '                    "Políticas divergentes!", MessageBoxButtons.YesNo) = DialogResult.No Then
                    '    '    '    Exit Function
                    '    '    'End If
                    '    'End If
                    'End If
                End If

                Dim funcionariobo As New FuncionarioBO
                Dim filtro As New VO.Funcionario
                Dim objfuncionario As New VO.Funcionario
                filtro.CODFNC = acaoCache.CODFNCCRIACOCMC

                'Dim NomFnc = funcionariobo.BuscaFuncionarios(filtro).FirstOrDefault.NOMFNC

                'Busca o funcionário ou terceiro logado.
                objfuncionario = funcionariobo.BuscaFuncionariosETerceiros(filtro)
                Try
                    boFluxoDeAprovacao.MovFluApvAcoCmc(8,
                                    acaoCache.CODACOCMC,
                                    NumFluApv,
                                    NumSeqFluApv,
                                    NumSeqNivApv,
                                    TipAco,
                                    acaoCache.CODFNCCRIACOCMC,
                                    objfuncionario.NOMFNC,
                                    objeto.funcionario,
                                    ObsApv,
                                    bFlgDlg,
                                    objeto.TIPFRMDSCBNF,
                                    objeto.QDEDIARCBPMS,
                                    objeto.IndPreAcordo)

                    If objeto.aprovar Then
                        Success("Aprovação realizada com sucesso!")
                    ElseIf objeto.parecer Then
                        Success("Parecer realizado com sucesso!")
                    ElseIf objeto.pedir Then
                        Success("Pedido de Parecer realizado com sucesso!")
                    ElseIf objeto.rejeitar Then
                        Success("Rejeição realizada com sucesso!")
                    End If
                    Return True  'Sucesso
                Catch ex As Exception
                    Danger(ex)
                End Try


                Return False

            Catch ex As Exception
                Danger(ex)
                Return False
            End Try
        End Function


        Private Sub RetornaCadMcoVbaFrn()
            Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
            Dim CarimboDeVerbaDoFornecedorBO As New CarimboDeVerbaDoFornecedorBO()
            Dim lista As List(Of VO.RelacaoAcaoxFornecedorxCarimbo) = Mapper.Map(Of List(Of RelacaoAcaoxFornecedorxCarimboViewModel), List(Of VO.RelacaoAcaoxFornecedorxCarimbo))(acaoCache.Carimbos)
            CarimboDeVerbaDoFornecedorBO.RetornaCadMcoVbaFrn(lista)
        End Sub

        <HttpPost()>
        Function AbrirModalFuncionarioAprovacao(objeto As String) As PartialViewResult
            Try
                Dim dados As New VO.Funcionario
                dados.NOMFNC = objeto

                Dim gridConf As New GridSettings(Of FuncionarioViewModel)

                If (dados.NOMFNC <> "") Then
                    Dim grid = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(boAauthor.BuscaFuncionarios(dados))
                    gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT})

                    gridConf.ResultID = "resultadosFuncionarios"
                    gridConf.PaginationID = "resultadosFuncionarios"
                    gridConf.ActionName = "BuscaFuncionariosAprovacao"
                    gridConf.ControllerName = "AcaoComercial"
                    gridConf.Filter = New FuncionarioViewModel With {.NOMFNC = objeto}

                    If grid.Count = 0 Then
                        Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    End If
                Else
                    Information(Constantes.FILTRO_OBRIGATORIO)
                    gridConf = GridSettings(Of FuncionarioViewModel).EmptyGrid()
                End If

                Return PartialView("~/Views/AcaoComercial/Aprovacao/_ModalBuscaFuncionarios.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        <HttpPost()>
        Function BuscaFuncionariosAprovacao(ByVal codfuncionario As Decimal?, ByVal nomfuncionario As String) As PartialViewResult
            Try
                Dim dados As New VO.Funcionario
                dados.CODFNC = codfuncionario
                dados.NOMFNC = nomfuncionario

                Dim grid = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(boAauthor.BuscaFuncionarios(dados))

                Dim gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT})

                If (codfuncionario <> 0 Or nomfuncionario <> "") Then
                    If grid.Count = 0 Then
                        Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    End If
                Else
                    Information(Constantes.FILTRO_OBRIGATORIO)
                    gridConf = GridSettings(Of FuncionarioViewModel).EmptyGrid()
                End If

                gridConf.ResultID = "resultadosFuncionarios"
                gridConf.PaginationID = "resultadosFuncionarios"
                gridConf.ActionName = "BuscaFuncionariosAprovacao"
                gridConf.ControllerName = "AcaoComercial"
                gridConf.Filter = New FuncionarioViewModel With {.CODFNC = codfuncionario, .NOMFNC = nomfuncionario}

                Return PartialView("~/Views/AcaoComercial/Aprovacao/_ResultadosViewFuncionarios.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function
#End Region

#Region "Estorno"
        ''' <summary>
        ''' Abre o modal de estorno de uma acao
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RealizarEstorno(ByVal objeto As Decimal) As PartialViewResult

            Dim dadosEstorno = bo.BuscaDadosParaEstorno(objeto)
            'No codigo antigo contem valor 0 para a acao

            Dim dadosEstornoView = Mapper.Map(Of VO.EstornoAcaoMercadologica, EstornoAcaoMercadologicaViewModel)(dadosEstorno)
            dadosEstornoView.StateGridFornecedores = PaginarEAtualizarGrid(dadosEstornoView.Fornecedores, New PageSettings())

            dadosEstornoView.StateGridFornecedores.ActionName = "AtualizarGridEstornoFornecedores"
            dadosEstornoView.StateGridFornecedores.ResultID = "gridEstornoFornecedores"
            dadosEstornoView.StateGridFornecedores.PaginationID = "gridEstornoFornecedores"
            dadosEstornoView.StateGridFornecedores.Filter = New RelacaoAcaoComercialxFornecedoresViewModel With {.CODACOCMC = objeto}

            Session("dadosEstornoView") = dadosEstornoView

            Return PartialView("~/Views/AcaoComercial/Estorno/_ModalEstorno.vbhtml", dadosEstornoView)

        End Function

        ''' <summary>
        ''' Recalcula valores para o estorno ao checar itens
        ''' </summary>
        ''' <param name="itensChecados"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RecalcularEstorno(ByVal itensChecados As List(Of Integer)) As PartialViewResult

            Dim dadosEstornoView As EstornoAcaoMercadologicaViewModel = Session.Item("dadosEstornoView")
            Dim dadosEstorno = Mapper.Map(Of EstornoAcaoMercadologicaViewModel, VO.EstornoAcaoMercadologica)(dadosEstornoView)

            dadosEstorno = bo.RecalcularTotalValorOperacoes(dadosEstorno, itensChecados)

            Dim dadosEstornoViewAtualizado = Mapper.Map(Of VO.EstornoAcaoMercadologica, EstornoAcaoMercadologicaViewModel)(dadosEstorno)

            dadosEstornoViewAtualizado.StateGridFornecedores = PaginarEAtualizarGrid(dadosEstornoViewAtualizado.Fornecedores, dadosEstornoView.StateGridFornecedores)

            Session("dadosEstornoView") = dadosEstornoViewAtualizado

            Return PartialView("~/Views/AcaoComercial/Estorno/_GridFornecedores.vbhtml", dadosEstornoViewAtualizado.StateGridFornecedores)

        End Function

        ''' <summary>
        ''' Atualiza e pagina grid de fornecedores no estorno
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridEstornoFornecedores(dadosPesquisa As GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel)) As PartialViewResult

            Dim dadosEstorno As EstornoAcaoMercadologicaViewModel = Session.Item("dadosEstornoView")
            Dim gridConf = PaginarEAtualizarGrid(dadosEstorno.Fornecedores, dadosPesquisa.Page, dadosPesquisa.Filter, dadosEstorno.StateGridFornecedores)
            dadosEstorno.StateGridFornecedores = gridConf
            Session("dadosEstornoView") = dadosEstorno

            Return PartialView("~/Views/AcaoComercial/Estorno/_GridFornecedores.vbhtml", gridConf)

        End Function

        ''' <summary>
        '''  Estorna todos os valores dos itens
        ''' </summary>
        ''' <param name="itensChecados"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function EstornarItensAcao(ByVal itensChecados As List(Of Integer), ByVal codAcaoComercial As Decimal) As Boolean

            If itensChecados Is Nothing Then
                Information(Constantes.SELECIONAR_ITENS)
                Return False
            Else

                Dim dadosEstorno = bo.BuscaDadosParaEstorno(codAcaoComercial)
                Dim resultEstorno = bo.InserirEstornoManual(dadosEstorno)
                Success(Constantes.ESTORNO_REALIZADO_SUCESSO)
                Return True

            End If
        End Function

#End Region

#Region "Status Acao Comercial"
        Public Shared Function ListarStatus() As List(Of SelectListItem)
            Dim listaStatus As New List(Of SelectListItem)()
            listaStatus.Add(New SelectListItem() With {.Text = "1 - NOVA", .Value = 1})
            listaStatus.Add(New SelectListItem() With {.Text = "2 - ENVIANDO PARA APROVAÇÃO", .Value = 2})
            listaStatus.Add(New SelectListItem() With {.Text = "3 - EM APROVAÇÃO", .Value = 3})
            listaStatus.Add(New SelectListItem() With {.Text = "4 - REJEITADA", .Value = 4})
            listaStatus.Add(New SelectListItem() With {.Text = "5 - APROVADA", .Value = 5})
            listaStatus.Add(New SelectListItem() With {.Text = "6 - PARECER NEGATIVO", .Value = 6})
            listaStatus.Add(New SelectListItem() With {.Text = "7 - MINHAS APROVAÇÕES", .Value = 7})
            Return listaStatus
        End Function

        Public Shared Function ConvertStatus(codigo As Integer) As String
            Select Case codigo
                Case 1
                    Return "NOVA"
                Case 2
                    Return "ENVIANDO PARA APROVAÇÃO"
                Case 3
                    Return "EM APROVAÇÃO"
                Case 4
                    Return "REJEITADA"
                Case 5
                    Return "APROVADA"
                Case 6
                    Return "PARECER NEGATIVO"
                Case 7
                    Return "MINHAS APROVAÇÕES"
                Case Else
                    Return ""
            End Select
        End Function

        Public Shared Function ConvertStatus(lista As IEnumerable(Of AcaoComercialViewModel)) As IEnumerable(Of AcaoComercialViewModel)
            For Each item In lista
                Select Case item.CODSTAAPVACOCMC
                    Case 1
                        item.DESCSTA = "NOVA"
                    Case 2
                        item.DESCSTA = "ENVIANDO PARA APROVAÇÃO"
                    Case 3
                        item.DESCSTA = "EM APROVAÇÃO"
                    Case 4
                        item.DESCSTA = "REJEITADA"
                    Case 5
                        item.DESCSTA = "APROVADA"
                    Case 6
                        item.DESCSTA = "PARECER NEGATIVO"
                    Case 7
                        item.DESCSTA = "MINHAS APROVAÇÕES"
                    Case Else
                        item.DESCSTA = ""
                End Select
            Next
            Return lista
        End Function

#End Region

#Region "PlanoMarketing"

        <HttpPost()>
        Public Function ExibePlanoMarketing(ByVal objeto As List(Of Decimal)) As PartialViewResult
            Dim filtro = New VO.PlanoDeMarketing()
            filtro.TIPEVTACOCMC = objeto(0)
            filtro.CODEVTACOCMC = objeto(1)
            filtro.ANOMESREF = objeto(2)
            If (filtro.ANOMESREF = 0) Then
                filtro.ANOMESREF = DateTime.Now.Year
            End If

            Dim ListPlanoDeMarketingViewModel As List(Of PlanoDeMarketingViewModel) = Mapper.Map(Of List(Of VO.PlanoDeMarketing), List(Of PlanoDeMarketingViewModel))(boPlanoMarketing.ListPlanoDeMarketingAno(filtro))

            If (ListPlanoDeMarketingViewModel.Count = 0) Then
                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                Return Nothing
            End If
            Return PartialView("~/Views/AcaoComercial/PlanoMarketing/_ModalPlanoMarketing.vbhtml", ListPlanoDeMarketingViewModel)

        End Function

        Public Function ExibeSaldoPlanoMarketing(ByVal objeto As List(Of Decimal)) As PartialViewResult
            Try

                Dim filtro = New VO.RelacaoAcaoComercialxPlanoMarketing()
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                filtro.TIPEVTACOCMC = objeto(0)
                filtro.CODEVTACOCMC = objeto(1)

                filtro.CODACOCMC = acaoModel.CODACOCMC
                Dim AcaoComercial As VO.AcaoComercial = Mapper.Map(Of AcaoComercialViewModel, VO.AcaoComercial)(acaoModel)
                Dim ListRelacaoAcaoComercialxPlanoMarketingViewModel As List(Of RelacaoAcaoComercialxPlanoMarketingViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoComercialxPlanoMarketing), List(Of RelacaoAcaoComercialxPlanoMarketingViewModel))(boRelacaoAcaoComercialxPlanoMarketing.ListSaldoPlanoDeMarketingAcao(filtro, AcaoComercial))

                If (ListRelacaoAcaoComercialxPlanoMarketingViewModel.Count = 0) Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    Return Nothing
                End If
                Return PartialView("~/Views/AcaoComercial/PlanoMarketing/_ModalSaldoPlanoMarketing.vbhtml", ListRelacaoAcaoComercialxPlanoMarketingViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function
#End Region

#Region "Orcamento"
        ''' <summary>
        ''' Atualiza o grid de itens de orcamento (paginacao e ordenacao)
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridItensOrcamento(dadosPesquisa As GridSettings(Of RelacaoAcaoComercialPorItemOrcamentoViewModel)) As PartialViewResult

            Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
            Dim gridConf = PaginarEAtualizarGrid(acaoModel.ItensOrcamento, dadosPesquisa.Page, dadosPesquisa.Filter, acaoModel.StateGridItensOrcamento)
            acaoModel.StateGridItensOrcamento = gridConf

            If (acaoModel.CODSTAAPVACOCMC <> 1) Then
                ViewBag.btnGravarVisible = False
                If (acaoModel.CODSTAAPVACOCMC = 5) Then
                    ViewBag.btnGasto = True
                End If
            End If
            If acaoModel.CODSTAAPVACOCMC = 1 Then
                ViewBag.btnIsrItemVisible = True
            Else
                ViewBag.btnIsrItemVisible = False
            End If

            Return PartialView("~/Views/AcaoComercial/Orcamento/_WidgetOrcamento.vbhtml", acaoModel)

        End Function

#End Region

#Region "Politicas"

        <HttpPost()>
        Public Function ExibirPoliticas(ByVal objeto As Decimal) As PartialViewResult

            Dim ListPoliticasViewModel As List(Of PoliticasAcaoComercialViewModel) = Mapper.Map(Of List(Of VO.PoliticasDaAcaoComercial), List(Of PoliticasAcaoComercialViewModel))(boPoliticas.ListPolitcasAcao(objeto))

            Return PartialView("~/Views/AcaoComercial/Politicas/_ModalPoliticas.vbhtml", ListPoliticasViewModel)

        End Function

#End Region

#Region "Mecanica"
        <HttpPost()>
        Public Function ExibirMecanica() As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")


                If (acaoCache.INDVCLPMC = 1) Then
                    ' Busca os dados da mecanica da promocão
                    Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
                    Dim BOFilial As RelacaoAcaoMecanicaFilialBO = New RelacaoAcaoMecanicaFilialBO()
                    Dim relacaoAcaoMecanica As List(Of VO.RelacaoAcaoMecanica) = BO.ListT(New VO.RelacaoAcaoMecanica() With {.CODACOCMC = acaoCache.CODACOCMC}).OrderBy(Function(r) r.NUMSEQRGRACOCMC).ToList()
                    Dim relacaoAcaoMecanicaViewModel As List(Of RelacaoAcaoMecanicaViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanica), List(Of RelacaoAcaoMecanicaViewModel))(relacaoAcaoMecanica)
                    For Each item In relacaoAcaoMecanicaViewModel
                        Dim listFilial As List(Of VO.RelacaoAcaoMecanicaFilial) = BOFilial.ListFiliaisRelacaoMecanica(item.CODACOCMC, item.NUMSEQRGRACOCMC)
                        Dim listFilialViewModel As List(Of RelacaoAcaoMecanicaFilialViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaFilial), List(Of RelacaoAcaoMecanicaFilialViewModel))(listFilial)
                        item.RelacaoAcaoMecanicaFilialList = listFilialViewModel
                    Next

                    ''Ações antigas possuem apenas a descrição da mecanica.
                    'If (relacaoAcaoMecanicaViewModel.Count = 0 And acaoCache.CODSTAAPVACOCMC <> 1) Then  'Açoes que nao esta no stats nova e nao possui mecanica detalhada exibe a descrição
                    '    Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalMecanica.vbhtml", acaoCache)
                    'End If

                    ControlaVisibilidadeComponentes(acaoCache)

                    Return PartialView("~/Views/AcaoComercial/Mecanica/_Promocao.vbhtml", relacaoAcaoMecanicaViewModel)
                Else 'Nao e promocao exibe a mecanida descritiva.
                    Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalMecanica.vbhtml", acaoCache)
                End If

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Public Sub AtualizarMecanica(acaoViewModel As AcaoComercialViewModel)
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoViewModel.Mecanica.CODACOCMC = acaoCache.CODACOCMC

                Dim mecanica = Mapper.Map(Of RelacaoAcaoComercialxMecanicaDaAcaoViewModel, VO.RelacaoAcaoComercialxMecanicaDaAcao)(acaoViewModel.Mecanica)
                Dim boMecanicaAcao As RelacaoAcaoComercialxMecanicaDaAcaoBO = New RelacaoAcaoComercialxMecanicaDaAcaoBO()
                boMecanicaAcao.InsereDadosMecanicaDaAcao(mecanica)

                acaoCache.Mecanica = acaoViewModel.Mecanica
                Session.Item("acaoModel") = acaoCache
            Catch ex As Exception
                Danger(ex)
            End Try
        End Sub
#End Region

#Region "Fornecedores"
        ''' <summary>
        ''' função para atualizar valores do fornecedor em ações de pre acordo
        ''' </summary>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function AtualizaValorFornecedorPreAcordo(acao As Integer) As PartialViewResult
            'Dim acaoPrincipal = Mapper.Map(Of VO.AcaoComercial, AcaoComercialViewModel)(bo.BuscarAcaoComercialPorId(acao))

            Dim acaoPrincipal As AcaoComercialViewModel = Session.Item("acaoModel")

            If acaoPrincipal.TIPACOMCD = TipoAcao.PreAcordoVigencia OrElse acaoPrincipal.TIPACOMCD = TipoAcao.PreAcordoValor Then
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                bo.AtualizaValorFornecedorPreAcordo(acaoCache)

                acaoPrincipal.StateGridFornecedores = GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel).EmptyGrid("gridFornecedores", "AtualizarGridFornecedores", "Fornecedor")
                acaoPrincipal.StateGridFornecedores.ListPaged = PaginarLista(acaoPrincipal.Fornecedores)
                acaoPrincipal.StateGridFornecedores.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                acaoPrincipal.StateGridFornecedores.TotalCount = acaoPrincipal.Fornecedores.Count

                acaoPrincipal.StateGridCarimbos = GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid("gridCarimbos", "AtualizarGridCarimbos", "Carimbo")
                acaoPrincipal.StateGridCarimbos.ListPaged = PaginarLista(acaoPrincipal.Carimbos)
                acaoPrincipal.StateGridCarimbos.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                acaoPrincipal.StateGridCarimbos.TotalCount = acaoPrincipal.Carimbos.Count

                acaoPrincipal.StateGridBonificacoes = GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel).EmptyGrid("gridBonificacoes", "AtualizarGridBonificacoes", "PedidoBonificacao")
                acaoPrincipal.StateGridBonificacoes.ListPaged = PaginarLista(acaoPrincipal.Bonificacoes)
                acaoPrincipal.StateGridBonificacoes.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                acaoPrincipal.StateGridBonificacoes.TotalCount = acaoPrincipal.Bonificacoes.Count

                acaoPrincipal.StateGridPremiacao = GridSettings(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel).EmptyGrid("gridPremiacoes", "AtualizarGridPremiacoes", "PremiacaoExclusiva")
                acaoPrincipal.StateGridPremiacao.ListPaged = PaginarLista(acaoPrincipal.Premiacao)
                acaoPrincipal.StateGridPremiacao.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                acaoPrincipal.StateGridPremiacao.TotalCount = acaoPrincipal.Premiacao.Count

            End If
            ControlaVisibilidadeComponentes(acaoPrincipal)


            Return PartialView("~/Views/AcaoComercial/Patrocinador/_WidgetFornecedor.vbhtml", acaoPrincipal)
        End Function

#End Region

#Region "B2B"
        Public Shared Function ListarB2B() As List(Of SelectListItem)
            Dim ListaB2B As New List(Of SelectListItem)()
            ListaB2B.Add(New SelectListItem() With {.Text = "0 - Não selecionado", .Value = 0})
            ListaB2B.Add(New SelectListItem() With {.Text = "1 - QUANTIDADE", .Value = 1})
            ListaB2B.Add(New SelectListItem() With {.Text = "2 - VALOR", .Value = 2})
            ListaB2B.Add(New SelectListItem() With {.Text = "3 - BUDGET UNICO", .Value = 3})
            Return ListaB2B
        End Function
#End Region

#Region "DESTINO DESPESAS CASH"
        Public Shared Function ListarDestinoDespesasCash() As List(Of SelectListItem)
            Dim ListaB2B As New List(Of SelectListItem)()
            ListaB2B.Add(New SelectListItem() With {.Text = "0 - Selecione", .Value = 0})
            ListaB2B.Add(New SelectListItem() With {.Text = "1 - BEM", .Value = 1})
            ListaB2B.Add(New SelectListItem() With {.Text = "2 - Marketplace", .Value = 2})
            Return ListaB2B
        End Function
#End Region
    End Class
End Namespace