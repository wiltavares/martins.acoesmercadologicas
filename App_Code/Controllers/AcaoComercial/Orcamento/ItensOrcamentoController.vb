﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.IO
Imports NPOI.HSSF.UserModel
Imports NPOI.HSSF.Extractor
Imports System.Data.OleDb

'TODO: recarregar pagina de createedit para poder mudar itens como calculo do brinde
Namespace Controllers.AcaoComercial.Orcamento
    <Authorize(Roles:="12,17,14")>
    Public Class ItensOrcamentoController
        Inherits AbstractController

        Private bo As New AcaoComercialBO()
        Private boOperacao As New OperacaoFiscalBO()
        Private boUnidadeNegocio As New UnidadeEstrategicaDeNegocioBO
        Private boFilial As New FilialEmpresaCorporativoBO()
        Private boRelacaoAcaoComercialPorItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoBO()
        Private boCentroCusto As New CentroCustoBO()
        Private boParametros As New ParametroSistemaAdminitracaoVendaBO
        Private boMercadoria As New MercadoriaBO
        Private boMovimentoQuinzenal As New MovimentoQuinzenalPrecoMercadoriaBO
        Private boTipoEvento As New TipoDeEventoBO()
        Private boperPeopple As New perAllPeopleFBO()
        Private boRelacaoItemPrincipalItemSubstitutoAcaoComercial As New RelacaoItemPrincipalItemSubstitutoAcaoComercialBO()
        Private boRelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPreco As New RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoBO()
        Property boApp As New MtsPorRequisicaoBO
        Property VendorSitesAll As New PoVendorSitesAllBO

        ''' <summary>
        ''' Abrir modal de inserir itens
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function IncluirItem(objeto As RelacaoAcaoComercialPorItemOrcamentoViewModel) As PartialViewResult

            Try

                'Dim boParametro As New ParamentroSistemaDeGestaoDeAcaoComercialBO()

                'Session.Item("markup") = boParametro.SelectT(New VO.ParamentroSistemaDeGestaoDeAcaoComercial()).PERMRGADIBDEPMC

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                If (IsNothing(objeto)) Then
                    objeto = New RelacaoAcaoComercialPorItemOrcamentoViewModel
                End If

                If (String.IsNullOrWhiteSpace(acaoCache.NOMACOCMC)) Then
                    Warning("Entre com o nome da ação comercial antes de prosseguir!")
                    Return Nothing
                End If
                If (IsNothing(acaoCache.DATINIACOCMC) OrElse IsNothing(acaoCache.DATFIMACOCMC)) Then
                    Warning("Escolha um interalo válido para a ação.")
                    Return Nothing
                ElseIf (acaoCache.DATINIACOCMC > acaoCache.DATFIMACOCMC) Then
                    Warning("Escolha um interalo válido para a ação.")
                    Return Nothing
                End If

                objeto.AcaoComercial = acaoCache

                CarregaDropsItensOrcamento(objeto)

                'colocar o valor do centro de custo diretamente no objeto será temporario,
                'o certo será usar uma variavel global setada durante a fase de login do usuario.
                objeto.CODCENCST = boRelacaoAcaoComercialPorItemOrcamento.BuscaCentrodeCusto(objeto.AcaoComercial.NOMUSRRCF)

                Dim centroCusto As JsonResult = BuscaCentroCusto(objeto.CODCENCST, String.Empty)
                Dim dropCentroCusto As New List(Of CentroCustoViewModel)
                For Each item In centroCusto.Data
                    Dim novoCentroCusto As New CentroCustoViewModel
                    novoCentroCusto.CODCENCST = item.CODCENCST
                    novoCentroCusto.DESCENCST = item.DESCENCST

                    dropCentroCusto.Add(novoCentroCusto)
                Next

                ViewBag.dropCentroCusto = dropCentroCusto

                Dim mercadoria As Object = New With {.icoditegstgr = 0,
                                                         .sdesitegstgr = "",
                                                         .iorgidt = 8,
                                                         .icodatr = 1,
                                                         .sdesatr = "AC"}

                Dim ListItemOrcamento As New List(Of MercadoriaViewModel)

                'TODO: Comentar para realizar testes sem conectar no banco de dados D01A
                ListItemOrcamento = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.ConsultaItemGastoGeral(mercadoria))

                Dim novaListItemOrcamento As New List(Of RelacaoAcaoComercialPorItemOrcamentoViewModel)
                For Each item In ListItemOrcamento
                    Dim novoItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoViewModel
                    novoItemOrcamento.CODMERITEMDROP = item.CODMER
                    novoItemOrcamento.NOMITEMORCDROP = item.DESMER
                    novaListItemOrcamento.Add(novoItemOrcamento)
                Next
                ViewBag.dropItemOrcamento = novaListItemOrcamento


                If IsNothing(objeto.DATUTZITEACOCMC) Then
                    objeto.DATUTZITEACOCMC = Date.Now
                End If

                Return PartialView("~/Views/AcaoComercial/Orcamento/ModalPrincipal/_ModalIncluirItensOrcamento.vbhtml", objeto)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function


        ''' <summary>
        ''' Abrir modal de editar itens
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function EditarItem(objeto As RelacaoAcaoComercialPorItemOrcamentoViewModel) As PartialViewResult

            Try
                Dim acaoModel As AcaoComercialViewModel = Session("acaoModel")

                'Dim boParametro As New ParamentroSistemaDeGestaoDeAcaoComercialBO()
                'Session.Item("markup") = boParametro.SelectT(New VO.ParamentroSistemaDeGestaoDeAcaoComercial()).PERMRGADIBDEPMC

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                If (IsNothing(objeto)) Then
                    objeto = New RelacaoAcaoComercialPorItemOrcamentoViewModel
                End If

                If (Not acaoModel.ItensOrcamento.Where(Function(f) f.CODACOCMC = objeto.CODACOCMC And f.NUMLNHITEOCDACOCMC = objeto.NUMLNHITEOCDACOCMC).FirstOrDefault() Is Nothing) Then
                    objeto.DATUTZITEACOCMC = acaoModel.ItensOrcamento.Where(Function(f) f.CODACOCMC = objeto.CODACOCMC And f.NUMLNHITEOCDACOCMC = objeto.NUMLNHITEOCDACOCMC).FirstOrDefault().DATUTZITEACOCMC
                End If

                objeto.AcaoComercial = acaoCache

                Dim centroCusto As JsonResult = BuscaCentroCusto(objeto.CODCENCST, String.Empty)
                Dim dropCentroCusto As New List(Of CentroCustoViewModel)
                For Each item In centroCusto.Data
                    Dim novoCentroCusto As New CentroCustoViewModel
                    novoCentroCusto.CODCENCST = item.CODCENCST
                    novoCentroCusto.DESCENCST = item.DESCENCST

                    dropCentroCusto.Add(novoCentroCusto)
                Next

                ViewBag.dropCentroCusto = dropCentroCusto

                'Popular DropdownList dropItemOrcamento
                Dim mercadoria As Object = New With {.icoditegstgr = 0,
                                                         .sdesitegstgr = "",
                                                         .iorgidt = 8,
                                                         .icodatr = 1,
                                                         .sdesatr = "AC"}

                Dim ListItemOrcamento As New List(Of MercadoriaViewModel)
                ListItemOrcamento = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.ConsultaItemGastoGeral(mercadoria))
                Dim novaListItemOrcamento As New List(Of RelacaoAcaoComercialPorItemOrcamentoViewModel)
                For Each item In ListItemOrcamento
                    Dim novoItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoViewModel
                    novoItemOrcamento.CODMERITEMDROP = item.CODMER
                    novoItemOrcamento.NOMITEMORCDROP = item.DESMER
                    novaListItemOrcamento.Add(novoItemOrcamento)
                Next
                ViewBag.dropItemOrcamento = novaListItemOrcamento

                CarregaDropsItensOrcamento(objeto)
                objeto.INDTIPOPEDSNACOCMC = objeto.CODOPEFSCDSNACOCMC

                Dim filtroSubstituto As New VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO
                filtroSubstituto.CODACOCMC = objeto.CODACOCMC
                filtroSubstituto.CODMER = objeto.CODMER
                filtroSubstituto.NUMLNHITEOCDACOCMC = objeto.NUMLNHITEOCDACOCMC

                Dim mercadorias As New List(Of MercadoriaViewModel)

                mercadorias = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.ListaItensSubstitutos(objeto.CODACOCMC, objeto.CODMER, objeto.NUMLNHITEOCDACOCMC))

                'Dim substitutosSalvos As List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)
                'If (Not IsNothing(filtroSubstituto.NUMLNHITEOCDACOCMC)) Then
                '    substitutosSalvos = boRelacaoItemPrincipalItemSubstitutoAcaoComercial.ListT(filtroSubstituto)
                'Else
                '    substitutosSalvos = New List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)
                'End If
                'If (Not IsNothing(substitutosSalvos) AndAlso substitutosSalvos.Count > 0) Then
                '    Dim marcadoriaSubstituta As New MercadoriaViewModel
                '    For Each item In substitutosSalvos
                '        marcadoriaSubstituta = Mapper.Map(Of VO.Mercadoria, MercadoriaViewModel)(boMercadoria.ListT(New VO.Mercadoria With {.CODMER = item.CODMERSBT}).FirstOrDefault)
                '        mercadorias.Add(marcadoriaSubstituta)
                '    Next

                '    ''Dim mercCahce = Session.Item("itensSub")
                '    ''If (Not IsNothing(mercCahce)) Then
                '    ''    mercadorias.AddRange(mercCahce)
                '    ''End If

                'Else
                '    mercadorias = Session.Item("itensSub")
                'End If

                Session.Item("itensSub") = mercadorias
                Session.Item("listaTempItensSelecionados") = mercadorias

                'objeto.VLRUNTITEOCDACOCMC = ConsultaPrecoUnitarioItem(objeto.CODMER, objeto.CODFILEMP)

                'Validação dos tipos de operações 10, 13, 15, 16 e 18
                If (objeto.CODOPEFSCDSNACOCMC = 10 Or objeto.CODOPEFSCDSNACOCMC = 13 Or objeto.CODOPEFSCDSNACOCMC = 15 Or objeto.CODOPEFSCDSNACOCMC = 16 Or objeto.CODOPEFSCDSNACOCMC = 18) Then
                    objeto.NOMITEMORC = objeto.CODITEOCDACOCMC
                End If

                Return PartialView("~/Views/AcaoComercial/Orcamento/ModalPrincipal/_ModalIncluirItensOrcamento.vbhtml", objeto)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' carrega drops iniciais do modal de itens orçamento
        ''' </summary>
        ''' <param name="relacao"></param>
        Private Sub CarregaDropsItensOrcamento(relacao As RelacaoAcaoComercialPorItemOrcamentoViewModel)

            Dim operacao As List(Of TipoOperacaoViewModel) = Mapper.Map(Of List(Of VO.OperacaoFiscal), List(Of TipoOperacaoViewModel))(boOperacao.ListT(New VO.OperacaoFiscal))
            Dim unidade As List(Of UnidadeEstrategicaDeNegocioViewModel) = Mapper.Map(Of List(Of VO.UnidadeEstrategicaDeNegocio), List(Of UnidadeEstrategicaDeNegocioViewModel))(boUnidadeNegocio.BuscaTodosCodNomUnidadeNegocio())
            Dim acao As AcaoComercialViewModel = Session.Item("acaoModel")

            If acao.TIPACOMCD = TipoAcao.PreAcordoValor Or acao.TIPACOMCD = TipoAcao.PreAcordoVigencia Then
                operacao = operacao.Where(Function(f) Not IsNothing(f.INDTIPOPEDSNACOCMC) AndAlso f.INDTIPOPEDSNACOCMC = 1).ToList
            Else
                If (acao.TIPACOMCD = 1 AndAlso acao.Fornecedores.Where(Function(f) f.CODFRN <> 999999).Count > 0) Then ' Patrocinador = "Fornecedor" e fornecedor  fornecedor não é "DESPESAS MARTINS"
                    operacao = operacao.Where(Function(f) Not IsNothing(f.INDTIPOPEDSNACOCMC) AndAlso (f.INDTIPOPEDSNACOCMC = 0)).ToList
                End If

                If (acao.TIPEVTACOCMC = 3) Then
                    operacao = operacao.Where(Function(f) Not IsNothing(f.INDTIPOPEACOCMCAUT) AndAlso (f.INDTIPOPEACOCMCAUT = 0 And f.INDTIPOPEICT = 1)).ToList
                Else
                    operacao = operacao.Where(Function(f) Not IsNothing(f.INDTIPOPEACOCMCAUT) AndAlso f.INDTIPOPEACOCMCAUT = 0).ToList
                End If
            End If

            If (Not acao.TIPEVTACOCMC Is Nothing) Then ' verifica se for campanha b2b filtra somente os eventos 2 e 4
                Dim bo As New TipoDeEventoBO
                Dim tipoEvento = bo.SelectT(New VO.TipoDeEvento With {.TIPEVTACOCMC = acao.TIPEVTACOCMC})
                If (tipoEvento.INDCPHBTB = 1) Then
                    If (acao.INDACOCMCBTBVLR = 3) Then
                        operacao = operacao.Where(Function(f) f.CODOPEFSCDSNACOCMC = 2).ToList
                    Else
                        operacao = operacao.Where(Function(f) f.CODOPEFSCDSNACOCMC = 2 Or f.CODOPEFSCDSNACOCMC = 4).ToList
                    End If
                End If
            End If

            Dim filiais As New List(Of FilialEmpresaCorporativoViewModel)


            If (relacao.INDTIPOPEDSNACOCMC = 1) Then
                filiais = Mapper.Map(Of List(Of VO.FilialEmpresaCorporativo), List(Of FilialEmpresaCorporativoViewModel))(boFilial.BuscarCodNomFilial())
            Else
                Dim filial As New FilialEmpresaCorporativoViewModel
                filial.CODFILEMP = 1
                filial.NOMFILEMP = "UBERLANDIA MG (MATRIZ ATACADO)"

                filiais.Add(filial)
            End If

            ViewBag.filiais = filiais
            ViewBag.operacao = operacao.OrderBy(Function(f) f.CODOPEFSCDSNACOCMC).ToList

            Dim novaUnidade As UnidadeEstrategicaDeNegocioViewModel = New UnidadeEstrategicaDeNegocioViewModel With {.CODUNDESRNGC = 99, .DESUNDESRNGC = "TODOS"}
            unidade.Add(novaUnidade)

            ViewBag.unidadeNegocio = unidade.OrderBy(Function(f) f.CODUNDESRNGC).ToList

        End Sub

        ''' <summary>
        ''' carrega os dados dependentes da mudança de tipo de operação
        ''' </summary>
        <HttpPost()>
        Public Function TipoOperacaoChange(codOperacao As Decimal) As JsonResult
            Try
                Dim filiais As New List(Of FilialEmpresaCorporativoViewModel)
                Dim operacao As TipoOperacaoViewModel
                operacao = Mapper.Map(Of VO.OperacaoFiscal, TipoOperacaoViewModel)(boOperacao.SelectT(New VO.OperacaoFiscal With {.CODOPEFSCDSNACOCMC = codOperacao}))

                If (operacao.INDTIPOPEDSNACOCMC = 1) Then
                    filiais = Mapper.Map(Of List(Of VO.FilialEmpresaCorporativo), List(Of FilialEmpresaCorporativoViewModel))(boFilial.BuscarCodNomFilial())
                Else
                    Dim filial As New FilialEmpresaCorporativoViewModel
                    filial.CODFILEMP = 1
                    filial.NOMFILEMP = "UBERLANDIA MG (MATRIZ ATACADO)"

                    filiais.Add(filial)
                End If
                ViewBag.filiais = filiais.ToList

                If (operacao.CODOPEFSCDSNACOCMC = 7) Then

                    Dim filtro As Object = New With {.idMerc = 0,
                                                     .desMerc = ""}

                    ViewBag.dropItemOrcamento = CsnIteGstGrl(filtro, operacao)
                End If

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim tipoEventoViewModel As New TipoEventoViewModel()
                If (Not acaoCache.TIPEVTACOCMC Is Nothing) Then
                    Dim bo As New TipoDeEventoBO
                    Dim tipoEvento = bo.SelectT(New VO.TipoDeEvento With {.TIPEVTACOCMC = acaoCache.TIPEVTACOCMC})
                    tipoEventoViewModel = Mapper.Map(Of VO.TipoDeEvento, TipoEventoViewModel)(tipoEvento)
                End If

                Dim retorno As Object = New With {.filiais = filiais.ToList, .operacao = operacao, .gridOrcamento = acaoCache.StateGridItensOrcamento, .tipoEvento = tipoEventoViewModel, .INDACOCMCBTBVLR = acaoCache.INDACOCMCBTBVLR}

                Return Json(retorno, JsonRequestBehavior.AllowGet)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function
        <HttpPost()>
        Public Function TipoOperacaoChangeItensOrcamento(codOperacao As Decimal) As JsonResult
            Try
                Dim filiais As New List(Of FilialEmpresaCorporativoViewModel)
                Dim operacao As TipoOperacaoViewModel
                operacao = Mapper.Map(Of VO.OperacaoFiscal, TipoOperacaoViewModel)(boOperacao.SelectT(New VO.OperacaoFiscal With {.CODOPEFSCDSNACOCMC = codOperacao}))

                If (operacao.INDTIPOPEDSNACOCMC = 1) Then
                    filiais = Mapper.Map(Of List(Of VO.FilialEmpresaCorporativo), List(Of FilialEmpresaCorporativoViewModel))(boFilial.BuscarCodNomFilial())
                Else
                    Dim filial As New FilialEmpresaCorporativoViewModel
                    filial.CODFILEMP = 1
                    filial.NOMFILEMP = "UBERLANDIA MG (MATRIZ ATACADO)"

                    filiais.Add(filial)
                End If
                ' ViewBag.filiais = filiais.ToList

                'If (operacao.CODOPEFSCDSNACOCMC = 7) Then

                '    Dim filtro As Object = New With {.idMerc = 0,
                '                                     .desMerc = ""}

                '    '  ViewBag.dropItemOrcamento = CsnIteGstGrl(filtro, operacao)
                'End If

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim tipoEventoViewModel As New TipoEventoViewModel()
                If (Not acaoCache.TIPEVTACOCMC Is Nothing) Then
                    Dim bo As New TipoDeEventoBO
                    Dim tipoEvento = bo.SelectT(New VO.TipoDeEvento With {.TIPEVTACOCMC = acaoCache.TIPEVTACOCMC})
                    tipoEventoViewModel = Mapper.Map(Of VO.TipoDeEvento, TipoEventoViewModel)(tipoEvento)
                End If

                Dim retorno As Object = New With {.filiais = filiais.ToList, .operacao = operacao, .tipoEvento = tipoEventoViewModel}

                Return Json(retorno, JsonRequestBehavior.AllowGet)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function
        ''' <summary>
        ''' atualiza drop de unidade de negócio
        ''' </summary>
        <HttpPost()>
        Public Function AtualizaDropBU() As JsonResult
            Try
                Dim unidade As List(Of UnidadeEstrategicaDeNegocioViewModel) = Mapper.Map(Of List(Of VO.UnidadeEstrategicaDeNegocio), List(Of UnidadeEstrategicaDeNegocioViewModel))(boUnidadeNegocio.BuscaTodosCodNomUnidadeNegocio())
                Dim novaUnidade As UnidadeEstrategicaDeNegocioViewModel = New UnidadeEstrategicaDeNegocioViewModel With {.CODUNDESRNGC = 99, .DESUNDESRNGC = "TODOS"}
                unidade.Add(novaUnidade)

                Return Json(unidade.ToList, JsonRequestBehavior.AllowGet)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' botao de buscar centros de custo
        ''' </summary>
        ''' <returns></returns>
        Public Function BuscaCentroCusto(codCentroCusto As Integer, desCentroCusto As String) As JsonResult

            Dim centro As List(Of CentroCustoViewModel) = Mapper.Map(Of List(Of VO.CentroCusto), List(Of CentroCustoViewModel))(boCentroCusto.BuscaCentrodeCustoFuncionario(codCentroCusto, desCentroCusto))

            Return Json(centro.ToList, JsonRequestBehavior.AllowGet)
        End Function
        ''' <summary>
        ''' busca dados da tabela de parametros para configuração da tela
        ''' </summary>
        ''' <returns></returns>
        <HttpPost()>
        Public Function pesquisaParametrosAcaoComercial() As List(Of VO.ParametroSistemaAdminitracaoVenda)
            Return boParametros.ListarParametrosAcaoComercial()
        End Function

        ''' <summary>
        ''' busca uma lista de itens de orçamento pelos filtros passados
        ''' </summary>
        ''' <returns>lista de mercadorias</returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function BuscaItensOrcamento(idMerc As Integer, desMerc As String, codOperacao As Integer, codFilial As Decimal, patrocinador As Integer) As List(Of MercadoriaViewModel)

            Dim acao As AcaoComercialViewModel = Session.Item("acaoModel")
            Dim retorno As List(Of MercadoriaViewModel) = New List(Of MercadoriaViewModel)
            Dim operacao As TipoOperacaoViewModel = Mapper.Map(Of VO.OperacaoFiscal, TipoOperacaoViewModel)(boOperacao.ListT(New VO.OperacaoFiscal With {.CODOPEFSCDSNACOCMC = codOperacao}).FirstOrDefault)


            Dim filtro As Object = New With {.idMerc = idMerc,
                                             .desMerc = desMerc,
                                             .codOperacao = codOperacao,
                                             .codFilial = codFilial,
                                             .patrocinador = patrocinador}

            Session("filtroItensOrcamento") = filtro

            If operacao.INDTIPOPEDSNACOCMC = 1 Then
                If codFilial <> 0 Then
                    retorno = CnsIte(filtro, acao, operacao)
                Else
                    Warning("Selecione uma filial!")
                    Return Nothing
                End If
            Else
                If operacao.INDTIPOPEREQVGM = 1 Or operacao.INDTIPOPEREQEVT = 1 Or operacao.INDTIPOPESLCPGT = 1 Then
                    retorno = CsnIteGstGrl(filtro, operacao)
                Else
                    retorno = PsqIteCsm(filtro, operacao)
                End If
            End If

            Return retorno

        End Function


        Public Function PsqIteCsm(filtro As Object, operacao As TipoOperacaoViewModel) As List(Of MercadoriaViewModel)

            Dim mercadoria As Object = New With {.CodUndNgc = 1, .CodFilEmp = "143", .CodUndOpe = "8",
                                                             .DesMer = filtro.desMerc,
                                                             .CodMerRsu = filtro.idMerc}
            If (String.IsNullOrEmpty(mercadoria.DesMer) And mercadoria.CodMerRsu = 0) Then
                Warning("Use algum filtro!")
                Return Nothing
            Else
                If (mercadoria.CodMerRsu = 0 And mercadoria.DesMer.ToString.Length < 3) Then
                    Information("Descrição deve conter mais de 3 caracteres!")
                Else
                    Return Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.PsqIteCsm(mercadoria))
                End If
            End If
            Return New List(Of MercadoriaViewModel)
        End Function

        ''' <summary>
        ''' Abrir modal para buscar mercadorias
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function BuscaMercadorias(objeto As MercadoriaViewModel) As PartialViewResult

            Try

                Dim dadosPesquisa As New GridSettings(Of MercadoriaViewModel)
                dadosPesquisa.Filter = New MercadoriaViewModel With {.DESMER = objeto.DESMER, .CODMER = objeto.CODMER}

                Dim mercadorias As IEnumerable(Of MercadoriaViewModel) = New List(Of MercadoriaViewModel)

                Dim gridConf = New GridSettings(Of MercadoriaViewModel)(dadosPesquisa.Filter, mercadorias.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))
                gridConf.ResultID = "resultadosBuscaItens"

                If (Not IsNothing(dadosPesquisa.Filter.DESMER) Or Not IsNothing(dadosPesquisa.Filter.CODMER)) Then
                    'mercadorias = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.ListT(Mapper.Map(Of MercadoriaViewModel, VO.Mercadoria)(dadosPesquisa.Filter)))
                    'gridConf.ListPaged = mercadorias.ToPagedList(gridConf.Page.PageNumber, gridConf.Page.PageSize)
                Else
                    Warning("Favor Utilizar algum filtro.")
                End If

                Return PartialView("~/Views/AcaoComercial/Orcamento/BuscarMercadoria/_ModalBuscaMercadoria.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' retorna partialview com grid de mercadorias
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function buscaGridMercadorias(idMerc As Integer, desMerc As String, codOperacao As Integer, codFilial As Decimal, patrocinador As Integer) As PartialViewResult
            Try
                If (idMerc = 0 AndAlso (String.IsNullOrWhiteSpace(desMerc) OrElse desMerc.Length < 3)) Then
                    Warning("Favor Utilizar algum filtro.")
                    Dim grid = New List(Of MercadoriaViewModel)
                    Dim gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT}, New MercadoriaViewModel With {.CODMER = idMerc, .DESMER = desMerc})

                    gridConf.ResultID = "resultadosBuscaItens"
                    gridConf.PaginationID = "resultadosBuscaItens"
                    Return PartialView("~/Views/AcaoComercial/Orcamento/BuscarMercadoria/_ResultadoMercadorias.vbhtml", gridConf)
                Else
                    Dim grid = BuscaItensOrcamento(idMerc, desMerc, codOperacao, codFilial, patrocinador)
                    If IsNothing(grid) Then
                        grid = New List(Of MercadoriaViewModel)
                    ElseIf grid.Count = 0 Then
                        Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    End If
                    Dim gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT}, New MercadoriaViewModel With {.CODMER = idMerc, .DESMER = desMerc})

                    gridConf.ResultID = "resultadosBuscaItens"
                    gridConf.PaginationID = "resultadosBuscaItens"
                    Return PartialView("~/Views/AcaoComercial/Orcamento/BuscarMercadoria/_ResultadoMercadorias.vbhtml", gridConf)
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        ''' <summary>
        ''' retorna partialview com grid de mercadorias
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function BuscaGridMercadoriasBrinde(idMerc As Integer, desMerc As String) As PartialViewResult
            Try
                If (idMerc = 0 AndAlso (String.IsNullOrWhiteSpace(desMerc) OrElse desMerc.Length < 3)) Then
                    Warning("Favor Utilizar algum filtro.")
                    Dim grid = New List(Of MercadoriaViewModel)
                    Dim gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT}, New MercadoriaViewModel With {.CODMER = idMerc, .DESMER = desMerc})

                    gridConf.ResultID = "resultadosBuscaItens"
                    gridConf.PaginationID = "resultadosBuscaItens"
                    Return PartialView("~/Views/AcaoComercial/Orcamento/BuscarMercadoria/_ResultadoMercadorias.vbhtml", gridConf)
                Else
                    Dim mercadoriaBO As New MercadoriaBO

                    Dim listaMercadoria = mercadoriaBO.BuscaItensOrcamento(New VO.Mercadoria() With {.CODMER = idMerc, .DESMER = desMerc})
                    Dim grid As List(Of MercadoriaViewModel) = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(listaMercadoria)
                    If IsNothing(grid) Then
                        grid = New List(Of MercadoriaViewModel)
                    ElseIf grid.Count = 0 Then
                        Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    End If
                    Dim gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT}, New MercadoriaViewModel With {.CODMER = idMerc, .DESMER = desMerc})

                    gridConf.ResultID = "resultadosBuscaItens"
                    gridConf.PaginationID = "resultadosBuscaItens"
                    Return PartialView("~/Views/AcaoComercial/Orcamento/BuscarMercadoria/_ResultadoMercadorias.vbhtml", gridConf)
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        ''' <summary>
        ''' função de busca que tambem sera utilizada na paginação
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function Pesquisar(dadosPesquisa As GridSettings(Of MercadoriaViewModel)) As PartialViewResult

            Dim mercadoriaBO As New MercadoriaBO

            Dim mercadoriaFiltro As VO.Mercadoria = Mapper.Map(Of MercadoriaViewModel, VO.Mercadoria)(dadosPesquisa.Filter)

            Dim listaMercadoria = mercadoriaBO.BuscaItensOrcamento(mercadoriaFiltro)
            Dim grid As List(Of MercadoriaViewModel) = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(listaMercadoria)
            If IsNothing(grid) Then
                grid = New List(Of MercadoriaViewModel)
            ElseIf grid.Count = 0 Then
                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
            End If
            Dim gridConf = PaginarEAtualizarGrid(grid, dadosPesquisa.Page, dadosPesquisa.Filter)

            gridConf.ResultID = "resultadosBuscaItens"
            gridConf.PaginationID = "resultadosBuscaItens"
            Return PartialView("~/Views/AcaoComercial/Orcamento/BuscarMercadoria/_ResultadoMercadorias.vbhtml", gridConf)

            'Dim filtro = Session.Item("filtroItensOrcamento")

            'Dim mercadoriassViewModel As List(Of MercadoriaViewModel) = BuscaItensOrcamento(filtro.idMerc, filtro.desMerc, filtro.codOperacao, filtro.codFilial, filtro.patrocinador)

            'Dim gridConf = PaginarEAtualizarGrid(mercadoriassViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
            'If gridConf.TotalCount = 0 Then
            '    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
            'End If
            'gridConf.ResultID = "resultadosBuscaItens"
            'gridConf.PaginationID = "resultadosBuscaItens"
            'Return PartialView("~/Views/AcaoComercial/Orcamento/BuscarMercadoria/_ResultadoMercadorias.vbhtml", gridConf)

        End Function

        ''' <summary>
        ''' limpa cache usado para salvar filtros de pesquisa da tela
        ''' </summary>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Sub LimparCacheFiltro()
            'Cache.Invalidate("filtroItensOrcamento")
        End Sub

#Region "funções para busca de dados ao mudar drop de tipo operaçao"

        Function CnsIte(filtro As Object, acao As AcaoComercialViewModel, operacao As TipoOperacaoViewModel) As List(Of MercadoriaViewModel)



            Dim STRFRNACO As String
            If filtro.patrocinador = 1 Then
                Dim fornecedores As List(Of Decimal?) = acao.Fornecedores.Select(Function(f) f.CODFRN).ToList
                STRFRNACO = String.Join(",", fornecedores)
            ElseIf filtro.patrocinador = 2 Then
                Dim fornecedores As List(Of Decimal?) = acao.Carimbos.Select(Function(f) f.CODFRN).ToList
                STRFRNACO = String.Join(",", fornecedores)
            ElseIf filtro.patrocinador = 1 Then
                Dim fornecedores As List(Of Decimal?) = acao.Bonificacoes.Select(Function(f) f.CODFRN).ToList
                STRFRNACO = String.Join(",", fornecedores)
            Else
                Dim fornecedores As List(Of Decimal?) = acao.Premiacao.Select(Function(f) f.CODFRN).ToList
                STRFRNACO = String.Join(",", fornecedores)
            End If

            Return Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.ConsultaItem(filtro.idMerc, filtro.desMerc, filtro.codFilial, operacao.INDBDEOPEDSNACOCMC, STRFRNACO))
        End Function

        Public Function CsnIteGstGrl(filtro As Object, operacao As TipoOperacaoViewModel) As List(Of MercadoriaViewModel)


            Dim codatr As Integer = 1 'solicitação de viagem
            Dim objcsnaux(4) As Object
            If operacao.INDTIPOPESLCPGT = 1 Then
                codatr = 2 'solicitação de pagamento
            ElseIf operacao.INDTIPOPEREQEVT = 1 Then
                codatr = 3 'requisição de evento
            End If


            Dim mercadoria As Object = New With {.icoditegstgr = filtro.idMerc,
                                                             .sdesitegstgr = filtro.desMerc,
                                                             .iorgidt = 8,
                                                             .icodatr = codatr,
                                                             .sdesatr = "AC"}

            Return Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.ConsultaItemGastoGeral(mercadoria))

        End Function

#End Region

        ''' <summary>
        ''' busca valor unitario do item selecionado
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function ConsultaPrecoUnitarioItem(codMercadoria As Integer, codFilial As Integer) As Decimal
            Try
                'Dim markup As Decimal = Session.Item("markup")
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                Dim movimentos As List(Of MovimentoQuinzenalPrecoMercadoriaViewModel) = Mapper.Map(Of List(Of VO.MovimentoQuinzenalPrecoMercadoriaVO), List(Of MovimentoQuinzenalPrecoMercadoriaViewModel))(boMovimentoQuinzenal.ConsultaPrecoUnitarioItem(codMercadoria, codFilial, acaoCache.TIPACOMCD))
                If movimentos.Count > 0 Then
                    Dim ValIte = movimentos.FirstOrDefault.VLRCSTUNTMER
                    If Trim(movimentos.FirstOrDefault.FLGMERPMC) = "S" Then
                        Return ValIte
                    Else
                        'Se for Ação do tipo Pré-Acordo.
                        If (acaoCache.TIPACOMCD = TipoAcao.PreAcordoVigencia Or acaoCache.TIPACOMCD = TipoAcao.PreAcordoValor) Then
                            ValIte = ValIte
                        Else 'Se não for Ação do tipo Pré-Acordo.
                            ValIte = ValIte / 0.78
                        End If
                        Return ValIte
                    End If
                Else
                    Return 0.0
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' busca dados de sprimetos da mercadoria
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function Suprimentos(codMercadoria As Integer, codAcao As Integer) As PartialViewResult
            Try
                If codAcao <> 0 And codMercadoria <> 0 Then

                    Dim listaSuplimentos As List(Of VO.MtsPorRequisicaoV) = boApp.CnsIteRsu(codMercadoria, codAcao)

                    If listaSuplimentos.Count = 0 Then
                        Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                        Return Nothing
                    End If

                    Dim gridPedido As List(Of MtsPorRequisicaoVViewModel) = New List(Of MtsPorRequisicaoVViewModel)()

                    Dim PURCHASE_NUM As String = Nothing
                    For Each item In listaSuplimentos.OrderBy(Function(x) x.PURCHASE_NUM)
                        If (item.PURCHASE_NUM <> PURCHASE_NUM) Then

                            Dim newobj As New MtsPorRequisicaoVViewModel()
                            With newobj
                                .PURCHASE_NUM = item.PURCHASE_NUM
                                .PURCHASE_TYPE = item.PURCHASE_TYPE
                                .PURCHASE_STATUS = item.PURCHASE_STATUS
                                .VENDOR_NAME = item.VENDOR_NAME
                                .PURCHASE_UNIT_PRICE = item.PURCHASE_UNIT_PRICE
                                .PURCHASE_QUANTITY = item.PURCHASE_QUANTITY
                                .PURCHASE_QUANTITY_RECEIVED = item.PURCHASE_QUANTITY_RECEIVED
                                .PURCHASE_QUANTITY_CANCELLED = item.PURCHASE_QUANTITY_CANCELLED
                            End With
                            gridPedido.Add(newobj)

                            PURCHASE_NUM = item.PURCHASE_NUM
                        End If
                    Next

                    Dim gridNotafiscal As List(Of MtsPorRequisicaoVViewModel) = New List(Of MtsPorRequisicaoVViewModel)()

                    Dim INVOICE_NUM As Decimal = -1
                    For Each item In listaSuplimentos.OrderBy(Function(x) x.INVOICE_NUM)
                        If (item.INVOICE_NUM <> INVOICE_NUM) Then

                            Dim newobj As New MtsPorRequisicaoVViewModel()
                            With newobj
                                .INVOICE_NUM = item.INVOICE_NUM
                                .OPERATION_STATUS = item.OPERATION_STATUS
                                .INVOICE_DATE = item.INVOICE_DATE
                                .INVOICE_UNIT_PRICE = item.INVOICE_UNIT_PRICE
                                .INVOICE_QUANTITY = item.INVOICE_QUANTITY
                            End With
                            gridNotafiscal.Add(newobj)

                            INVOICE_NUM = item.INVOICE_NUM
                        End If
                    Next

                    Dim resultados As Tuple(Of List(Of MtsPorRequisicaoVViewModel), List(Of MtsPorRequisicaoVViewModel)) = New Tuple(Of List(Of MtsPorRequisicaoVViewModel), List(Of MtsPorRequisicaoVViewModel))(gridPedido, gridNotafiscal)
                    Return PartialView("~/Views/AcaoComercial/Orcamento/ModalPrincipal/_ModalSuprimentos.vbhtml", resultados)
                Else
                    Information("Nenhuma mercadoria selecionada para este item")
                End If
                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' função que sera executada ao sair do modal, independente se for salvando ou cancelando
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function fecharModalItens(mercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel, patrocinador As Integer) As PartialViewResult
            Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

            AtuIteSub(acaoCache)
            calculaItemFolhaPagamento(acaoCache)

            If patrocinador = 1 Then
                CalVlrRefIteBde(acaoCache) 'Calcula valor referente ao item
            End If

            Util.AtuSldOcd(acaoCache) 'atualiza o saldo do orçamento 
            Util.CalVlrTot(acaoCache) 'calcula vlr total do brinde e do orçamento

            If (Not IsNothing(mercadoria)) Then
                'Cache.Invalidate("itensSub")
                acaoCache.StateGridItensOrcamento = PaginarEAtualizarGrid(acaoCache.ItensOrcamento, acaoCache.StateGridItensOrcamento)

                'Canal de Venda
                ValidaNecessidadeCanalVenda(acaoCache)
            Else
                Warning("Não foi possivel inserir o item.")
                Exit Function
                Return Nothing
            End If

            ViewBag.btnIsrItemVisible = True
            limparCacheItensSub()
            limparCacheListaTempItensSelecionados()
            If (Not IsNothing(acaoCache.CODSTAAPVACOCMC) AndAlso acaoCache.CODSTAAPVACOCMC = 5) Then
                ViewBag.btnGasto = True
            Else
                ViewBag.btnGasto = False
            End If
            Return PartialView("~/Views/AcaoComercial/Orcamento/_WidgetOrcamento.vbhtml", acaoCache)

        End Function

        Private Sub AtuIteSub(acao As AcaoComercialViewModel)
            Dim NroLnh As Integer
            Try
                NroLnh = acao.ItensOrcamento.Count - 1
                For Each item In acao.ItensSubstituidos.Where(Function(f) f.NUMLNHITEOCDACOCMC > NroLnh)
                    acao.ItensOrcamento.Remove(item)
                Next
            Catch oEcc As Exception
            End Try
        End Sub

        Private Function ValidaNecessidadeCanalVenda(acao As AcaoComercialViewModel) As Boolean
            Try
                If boRelacaoAcaoComercialPorItemOrcamento.ValidaNecessidadeCanalVenda(acao.CODACOCMC) Then

                    'lblCanalVenda.Visible = True
                    'cmbCanalVenda.Visible = True
                    Return True
                Else
                    'lblCanalVenda.Visible = False
                    'cmbCanalVenda.Visible = False
                    Return False
                End If

            Catch ex As Exception

            End Try
        End Function

        Private Function InsDdoIteAcoCmc(ByVal mercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel, acao As AcaoComercialViewModel) As RelacaoAcaoComercialPorItemOrcamentoViewModel

            Dim bFlgFncOra As Boolean

            Try
                Dim novaMercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel = mercadoria

                'Combinação Contabil para requisição de bens de serviço
                If Not IsNothing(novaMercadoria.INDGRCREQAUTITERSU) AndAlso novaMercadoria.INDGRCREQAUTITERSU = 1 Then
                    'novaMercadoria.INDITEREQRSUVLRMNM =
                    novaMercadoria.IDTCNTCTB = GrcIdtIteRsu(novaMercadoria.CODCENCST, mercadoria, acao)
                    If Not bFlgFncOra Then

                        If Not CnsUsrOra(ControleAcesso.CodFnc()) Then
                            Warning("Usuário não está cadastrado no iPROC. Contate o analista!")
                            Return Nothing
                        Else
                            bFlgFncOra = True
                        End If
                    End If
                Else
                    'novaMercadoria.INDITEREQRSUVLRMNM = 0
                    novaMercadoria.IDTCNTCTB = 0
                End If



                Dim mercadoriaInserir As VO.RelacaoAcaoComercialPorItemOrcamento = Mapper.Map(Of RelacaoAcaoComercialPorItemOrcamentoViewModel, VO.RelacaoAcaoComercialPorItemOrcamento)(novaMercadoria)
                mercadoriaInserir.CODACOCMC = acao.CODACOCMC
                'mercadoriaInserir.NUMLNHITEOCDACOCMC = acao.ItensOrcamento.Max(Function(f) f.NUMLNHITEOCDACOCMC) + 1 'coloca como proximo numero de linha
                If IsNothing(mercadoriaInserir.QDEDIACMPITEACOCMC) Then
                    mercadoriaInserir.QDEDIACMPITEACOCMC = 0
                End If
                If IsNothing(mercadoriaInserir.IDTEMPFRNITERSU) Then
                    mercadoriaInserir.IDTEMPFRNITERSU = 0
                End If
                If IsNothing(mercadoriaInserir.IDTLCLEMPFRNITERSU) Then
                    mercadoriaInserir.IDTLCLEMPFRNITERSU = 0
                End If
                If IsNothing(mercadoriaInserir.IDTTERPGTITERSU) Then
                    mercadoriaInserir.IDTTERPGTITERSU = 0
                End If
                If IsNothing(mercadoriaInserir.CODIDTTSC) Then
                    mercadoriaInserir.CODIDTTSC = 0
                End If
                If IsNothing(mercadoriaInserir.NUMCTTFRN) Then
                    mercadoriaInserir.NUMCTTFRN = 0
                End If
                If IsNothing(mercadoriaInserir.IDTITERSU) Then
                    mercadoriaInserir.IDTITERSU = 0
                End If
                If IsNothing(mercadoriaInserir.NUMLNHCTTRSU) Then
                    mercadoriaInserir.NUMLNHCTTRSU = 0
                End If
                If IsNothing(mercadoriaInserir.DESCTTRSU) Then
                    mercadoriaInserir.DESCTTRSU = 0
                End If
                If IsNothing(mercadoriaInserir.NUMCGCEMPFRN) Then
                    mercadoriaInserir.NUMCGCEMPFRN = 0
                End If
                If IsNothing(mercadoriaInserir.DATUTZITEACOCMC) Then
                    mercadoriaInserir.DATUTZITEACOCMC = Date.MinValue
                End If
                If IsNothing(mercadoriaInserir.CODUNDESRNGCOPEDSN) Then
                    mercadoriaInserir.CODUNDESRNGCOPEDSN = 0
                End If
                If IsNothing(mercadoriaInserir.CODSTAITERSU) Then
                    mercadoriaInserir.CODSTAITERSU = 1
                End If
                If IsNothing(mercadoriaInserir.NUMLNHITEOCDACOCMC) Then
                    mercadoriaInserir.NUMLNHITEOCDACOCMC = acao.ItensOrcamento.Max(Function(f) f.NUMLNHITEOCDACOCMC) + 1
                End If
                'mercadoriaInserir.NUMLNHITEOCDACOCMC = acao.ItensOrcamento.Max(Function(f) f.NUMLNHITEOCDACOCMC) + 1
                boRelacaoAcaoComercialPorItemOrcamento.InserirRelacao(mercadoriaInserir)
                'limparCacheItensSub()
                Return Mapper.Map(Of VO.RelacaoAcaoComercialPorItemOrcamento, RelacaoAcaoComercialPorItemOrcamentoViewModel)(mercadoriaInserir)
            Catch ex As Exception
                Danger(ex)
            End Try
        End Function

        Private Function GrcIdtIteRsu(ByVal CodCenCst As String, mercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel, acao As AcaoComercialViewModel) As Integer
            Dim Str As Object
            Dim StrAux As String
            Dim TipEvt As String
            If Not IsNothing(acao.TIPEVTACOCMC) AndAlso acao.TIPEVTACOCMC <> 0 Then
                TipEvt = BscDdoTipEvt(acao.TIPEVTACOCMC)
            End If
            '
            StrAux = "01." & TipEvt & "." & CodCenCst & ".00.0143.000.00.000" 'IdtCntCtb
            Str = FunVldGrpSgmCtb(StrAux)
            '
            If IsNumeric(Str) Then
                If CLng(Str) = 0 Then
                    Warning("Não foi possível efetuar a validação do seguinte segmento contábil porque o mesmo retornou zero. Favor contactar o analista responsável.")
                    Return 0
                Else
                    Return CInt(Str)
                End If
            Else
                Warning("Não foi possível efetuar a validação de segmento contábil.")
                Return 0
            End If

        End Function

        Private Function FunVldGrpSgmCtb(ByVal GrpSgmCtb As String) As Object
            Dim VarRetAux As Object

            VarRetAux = boRelacaoAcaoComercialPorItemOrcamento.VldGrpSgmCtb(GrpSgmCtb)
            Dim varString = VarRetAux.ToString
            If IsNothing(varString) OrElse String.IsNullOrWhiteSpace(varString) Then
                Return 0
            ElseIf IsNumeric(varString) Then
                Return varString
            Else
                Return 0
            End If

        End Function

        Public Function CnsUsrOra(ByVal CodFnc As Integer) As Boolean
            'Busca usuario cadastrado no iPROC                                        
            Dim resultado = boperPeopple.PsqCadFnc(CodFnc)

            If Not resultado Is Nothing Then
                If resultado.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        End Function

        Public Function BscDdoTipEvt(ByVal CodTipEvt As Integer) As String
            Try

                Dim tipoEvento As New VO.TipoDeEvento
                tipoEvento.CODCNTCTB = 0
                tipoEvento.NOMTIPEVTACOCMC = ""
                tipoEvento.CODCNTCTB = 0
                Dim tiposEvento = boTipoEvento.buscaDadosTipoEvento(tipoEvento)

                If tiposEvento.Count > 0 Then
                    Return tiposEvento.FirstOrDefault.CODCNTCTB
                End If
            Catch ex As Exception
                Return "000000000"
            End Try
        End Function


        Public Sub CalVlrRefIteBde(acao As AcaoComercialViewModel)


            Dim mercadorias As List(Of VO.Mercadoria)


            Dim VlrTotBde As Decimal
            Dim VlrTotTxaBde As Decimal
            Dim Str As String
            Dim VlrAux As Decimal
            Dim CodFrnIte As Integer
            Try
                'Calcula a taxa de brinde anterior novamente
                For Each mercadoria In acao.ItensOrcamento
                    'verifica se é brinde 
                    If mercadoria.INDBDEOPEDSNACOCMC = 1 Then
                        'busca o código do fornecedor principal da mercadoria
                        If Not IsNothing(mercadoria.CODFRNPCPMER) Then
                            CodFrnIte = mercadoria.CODFRNPCPMER
                        Else
                            mercadorias = boMercadoria.ConsultaItem(mercadoria.CODMER, "", mercadoria.CODFILEMP, 1, "")

                            If mercadorias.Count > 0 Then
                                CodFrnIte = mercadorias.FirstOrDefault.CODFRNPCPMER
                                mercadoria.CODFRNPCPMER = CodFrnIte
                            End If
                        End If

                        'Valor total do brinde 
                        VlrTotBde = mercadoria.ValorTotal

                        'Busca o fornecedor do brinde escolhido
                        For Each fornecedor In acao.Fornecedores

                            If fornecedor.CODFRN = CodFrnIte Then
                                'Calcula o valor total da taxa de brinde 
                                VlrAux = mercadoria.QDEITEOCDACOCMC * mercadoria.VLRUNTITEOCDACOCMC
                                VlrTotTxaBde = VlrAux * (acao.TaxaBrinde / 100)

                                VlrAux = VlrTotTxaBde

                                If Not IsNothing(acao.TaxaBrinde) AndAlso acao.TaxaBrinde > 0 Then
                                    VlrAux = VlrTotTxaBde + acao.TaxaBrinde
                                End If
                                'Formatando valor de brinde
                                Str = VlrAux

                                'Calcula Taxa de Brinde
                                '---------------------------------------------------
                                'Colocando Taxa de Brinde para o campo TAXA BRINDE do fornecedor
                                acao.TaxaBrinde = CInt(Str)
                                CalTxaBde(fornecedor, acao)
                                '---------------------------------------------------

                                'Calcula o valor total de brindes
                                VlrAux = VlrTotTxaBde
                                acao.VLRTOTBDEACOCMC = VlrAux 'TODO: atualizar campo da tela
                                Exit For
                            Else
                            End If
                        Next
                    End If
                Next

                Util.AtuSldOcd(acao) 'atualiza o saldo do orçamento 
                Util.CalVlrTot(acao) 'calcula vlr total do brinde e do orçamento 

            Catch ex As Exception

            End Try
        End Sub

        Public Function CalTxaBde(ByVal fornecedor As RelacaoAcaoComercialxFornecedoresViewModel, acao As AcaoComercialViewModel)
            Dim ResExtBde As Double = 0 'Resto da taxa de brinde a ser adicionado no valor Extra. 
            Dim ResCttBde As Double = 0 'Resto da taxa de brinde a ser adicionado no valor Contrato. 
            Dim txaBde As Double = 0
            If Not IsNothing(acao.TaxaBrinde) Then
                txaBde = acao.TaxaBrinde
            End If

            If txaBde <> 0 Then
                Dim SldCtt As Double = 0
                Dim SldCntCrr As Double = 0
                Dim SldMrg As Double = 0
                Dim CMV As Double = 0
                Dim CMVaux As Double = 0

                Dim ctt As Double = 0
                Dim cttaux As Double = 0
                Dim ext As Double = 0
                Dim extaux As Double = 0

                If Not IsNothing(fornecedor.VLRSLDDSNCTTACOCMC) Then SldCtt = fornecedor.VLRSLDDSNCTTACOCMC
                If Not IsNothing(fornecedor.VLRUTZEXACTTACOCMC) Then SldCntCrr = fornecedor.VLRUTZEXACTTACOCMC
                If Not IsNothing(fornecedor.VLRSLDDSNACR) Then SldMrg = fornecedor.VLRSLDDSNACR
                If Not IsNothing(fornecedor.VLRUTZRCTCSTMER) Then CMV = fornecedor.VLRUTZRCTCSTMER
                If Not IsNothing(fornecedor.VLRUTZRCTCSTMER) Then CMVaux = fornecedor.VLRUTZRCTCSTMER

                'If Not Not IsNothing(fornecedor.ctt) Then ctt = TabFrnAcoCmc.Item(NroLnhTabFrn, "ctt")
                If Not Not IsNothing(fornecedor.VLRUTZCTTACOCMC) Then cttaux = fornecedor.VLRUTZCTTACOCMC
                'If Not Not IsNothing(fornecedor.ext) Then ext = TabFrnAcoCmc.Item(NroLnhTabFrn, "ext")
                If Not Not IsNothing(fornecedor.VLRUTZEXACTTACOCMC) Then extaux = fornecedor.VLRUTZEXACTTACOCMC

                'CalTxaBde(CMV, CMVaux, SldMrg, SldCtt, SldCntCrr, txaBde, ctt, cttaux, ext, extaux, "") 'TODO: função sem retorno nem altearção nos dados, possivelmente inutil, verifir se pode ser retirada

                fornecedor.VLRUTZEXACTTACOCMC = extaux
                fornecedor.VLRUTZCTTACOCMC = cttaux
            End If
        End Function

        Public Function CalTxaBde(ByVal CMV As Double, ByVal CMVaux As Double, ByVal SldReceitaCMV As Double, ByVal SldCtt As Double, ByVal SldCntCrr As Double, ByVal txaBde As Double,
                              ByVal ctt As Double, ByRef cttAux As Double,
                              ByVal ext As Double, ByRef extaux As Double, ByVal NomFrnBde As String)

            Dim SldExaCttBde As Double = SldCntCrr - ext 'Saldo Extra Contrato Disponivel
            Dim SldCttBde As Double = SldCtt - ctt 'Saldo Contrato Disponivel
            Dim SldReceitaCMVBde As Double = SldReceitaCMV - CMV 'Saldo Receita CMV Disponivel
            txaBde -= SldReceitaCMV 'Utilizando Saldo de Margem para pagar taxa de brinde
            If txaBde < 0 Then txaBde = 0 'Se o valor ficar negativo (Saldo Maior que taxa) entao zerar o valor da taxa de brinde)

            If SldExaCttBde >= txaBde Then
                extaux = ext + txaBde 'coloca bde na totalidade no ext
            ElseIf SldCttBde >= txaBde Then
                cttAux = ctt + txaBde 'coloca bde na totalidade no ctt
            ElseIf SldReceitaCMVBde >= txaBde Then
                CMVaux = CMV + txaBde 'coloca bde na totalidade no ctt
            Else
                If SldExaCttBde + SldCttBde + SldReceitaCMVBde >= txaBde Then 'Verifico se o saldo (cx1 + cx2) é > que Brinde
                    If SldExaCttBde > SldCttBde Then 'Verifico se a EXACTT é maior que CTT
                        'Se for, entao coloco tudo nesta caixa e coloco o restante na outra caixa
                        extaux = SldCntCrr
                        cttAux = Math.Abs(txaBde - SldExaCttBde) + cttAux
                    ElseIf SldExaCttBde < SldCttBde Then 'Verifico se a CTT é maior que EXACTT
                        'Se for, entao coloco tudo nesta caixa e coloco o restante na outra caixa
                        cttAux = SldCtt
                        extaux = Math.Abs(txaBde - SldCttBde) + extaux
                    ElseIf ((SldExaCttBde + SldCttBde) = txaBde) Then 'Se CTT + EXACTT = BRINDE
                        cttAux = SldCtt
                        extaux = SldCntCrr
                    Else
                        If (Math.Abs(txaBde - SldCttBde) + extaux) <= SldCntCrr Then
                            cttAux = SldCtt
                            extaux = Math.Abs(txaBde - SldCttBde) + extaux
                        Else
                            extaux = SldCntCrr
                            cttAux = Math.Abs(txaBde - SldExaCttBde) + cttAux
                        End If
                    End If
                Else
                End If
            End If
        End Function

        Private Sub calculaItemFolhaPagamento(acao As AcaoComercialViewModel)

            Dim CodIndIteCsm As Integer = 99999
            Dim FlgIteCsm As Boolean = 0
            Dim ValTotIte As Decimal = 0
            Dim CalValTotIte As Decimal = 0
            Dim PerCalEncFolPgt As Decimal

            Try
                If acao.ItensOrcamento.Count >= 1 Then
                    'Obtem percentual do calculo de encargos
                    Dim parametros As List(Of VO.ParametroSistemaAdminitracaoVenda) = pesquisaParametrosAcaoComercial()
                    If parametros.Count > 0 Then
                        PerCalEncFolPgt = parametros.FirstOrDefault.PERCALENCFOLPGT
                    End If


                    'Varre o grid a procura de itens de consumo
                    For Each mercadoria In acao.ItensOrcamento
                        If mercadoria.INDGRCITEENCFOLPGT = 1 Then
                            ValTotIte += mercadoria.ValorTotal
                            FlgIteCsm = 1
                        End If
                    Next

                    'Gera o item de encargos de folha
                    If FlgIteCsm Then
                        If CodIndIteCsm = 99999 Then
                            Dim operacao As List(Of TipoOperacaoViewModel) = Mapper.Map(Of List(Of VO.OperacaoFiscal), List(Of TipoOperacaoViewModel))(boOperacao.ListT(New VO.OperacaoFiscal))
                            operacao = operacao.OrderBy(Function(f) f.CODOPEFSCDSNACOCMC).ToList

                            operacao = operacao.Where(Function(f) f.INDTIPOPEACOCMCAUT = 1).ToList

                            Dim item As New RelacaoAcaoComercialPorItemOrcamentoViewModel With {
                                .CODITEOCDACOCMC = "0 - ENCARGOS DE FOLHA",
                                .CODOPEFSCDSNACOCMC = operacao.FirstOrDefault.CODOPEFSCDSNACOCMC + " - ENCARGOS DE FOLHA",
                                .VLRUNTITEOCDACOCMC = CalValTotIte,
                                .CODSTAITERSU = 0,
                                .QDEITEOCDACOCMC = "1",
                                .QDEUTZOPEDSNACOCMC = "0",
                                .VLRUTZOPEDSNACOCMC = "0",
                                .DESFRMUTZITEACOCMC = "",
                                .INDBDEOPEDSNACOCMC = 0,
                                .CODUNDESRNGCOPEDSN = "0",
                                .CODFILEMP = "1",
                                .INDTIPOPEDSNACOCMC = "1",
                                .INDUNDESRNGCACOCMC = "1",
                                .DESOBSCPR = "",
                                .SALDO = CalValTotIte
                        }
                        Else
                            acao.ValorTotal = CalValTotIte
                            acao.SaldoOrcamento = CalValTotIte
                        End If
                    End If
                Else
                    acao.ValorTotal = 0
                    acao.SaldoOrcamento = 0
                End If
            Catch ex As Exception
                Danger(ex.ToString)
            End Try
        End Sub

        <HttpPost()>
        Public Function Salvar(mercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel, fornecedorUnico As Boolean, viaApp As Boolean, patrocinador As Integer) As PartialViewResult
            Try

                Dim IndDat As Integer
                Dim count As Integer
                Dim operacao = boOperacao.ListT(New VO.OperacaoFiscal With {.CODOPEFSCDSNACOCMC = mercadoria.INDTIPOPEDSNACOCMC}).FirstOrDefault

                If viaApp Then
                    mercadoria.INDITERSUARZPRVPGT = 1
                End If
                If fornecedorUnico Then
                    mercadoria.INDITERSUFRNUNC = 1
                End If

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                'If cbTerPgt.Visible = True And cbTerPgt.SelectedItem Is Nothing Then 'combo que só aparece com a dll
                '    Warning("Selecione a Forma de Pagamento")
                '    Exit Sub
                'End If

                'If cbFrnCsm.Visible = True And cbFrnCsm.SelectedItem Is Nothing Then 'combo que só aparece com a dll
                '    Warning("Selecione o Contrato/Fornecedor")
                '    Exit Sub
                'End If

                'Valida a insersao de itens do tipo 5. So sera permitido a insercao desse tipo de item se
                'os campos fornecedor unico e item via app estiverem no mesmo formato dos que ja foram cadastrados
                If mercadoria.INDTIPOPEDSNACOCMC = 5 Then
                    If acaoCache.ItensOrcamento.Where(Function(f) f.INDTIPOPEDSNACOCMC = 5).Count > 0 Then
                        If (fornecedorUnico = False) And (viaApp = False) Then
                            Warning("Não é permitido em uma mesma ação gerar itens app ou de fornecedor único com itens de bancada.")
                            Exit Function
                        Else
                            If (fornecedorUnico = True) Or (viaApp = True) Then
                                Warning("Não é permitido em uma mesma ação gerar itens app ou de fornecedor único com itens de bancada.")
                                Exit Function
                            End If
                        End If
                    End If
                End If

                'Validação dos tipos de operações 10, 13, 15, 16 e 18
                If (mercadoria.INDTIPOPEDSNACOCMC = 10 Or mercadoria.INDTIPOPEDSNACOCMC = 13 Or mercadoria.INDTIPOPEDSNACOCMC = 15 Or
                    mercadoria.INDTIPOPEDSNACOCMC = 16 Or mercadoria.INDTIPOPEDSNACOCMC = 18) Then
                    mercadoria.CODMER = 0
                    mercadoria.CODITEOCDACOCMC = mercadoria.NOMITEMORC
                ElseIf (mercadoria.INDTIPOPEDSNACOCMC = 6 Or mercadoria.INDTIPOPEDSNACOCMC = 7 Or
                        mercadoria.INDTIPOPEDSNACOCMC = 17) Then 'Validação dos tipos de operações 6, 7 e 17
                    Dim campos As String() = Split(mercadoria.NOMITEMORCDROP)
                    mercadoria.CODMER = CDec(campos(0))
                    mercadoria.CODITEOCDACOCMC = campos(2)
                End If

                If (String.IsNullOrWhiteSpace(mercadoria.CODITEOCDACOCMC)) Then
                    Warning("Informe o item do orçamento")
                    Exit Function
                End If

                count = boRelacaoItemPrincipalItemSubstitutoAcaoComercial.ListT(New VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO With {.CODACOCMC = acaoCache.CODACOCMC, .CODMERSBT = mercadoria.CODMER}).Count
                If count > 0 Then
                    Warning("Item de ação já cadastrado como item substituto nesta ação.")
                    Exit Function
                End If

                If FunVrfInsDdoCpo(mercadoria, operacao, acaoCache) = 0 Then 'Verifica se todos os campos foram preenchidos
                    If operacao.INDTIPOPEDSNACOCMC <> 1 Then 'se o campo está habilitado verifica se a data é maior que a data inicial
                        IndDat = FunVrfDatUtzIte(mercadoria) 'verifica a data de utilização do item
                    End If

                    If IndDat = 0 Then
                        If mercadoria.INDITERSUFRNUNC Or mercadoria.INDITERSUARZPRVPGT Then
                            If ((Not IsNothing(mercadoria.IDTEMPFRNITERSU) AndAlso String.IsNullOrWhiteSpace(mercadoria.IDTEMPFRNITERSU)) And (Not IsNothing(mercadoria.NUMCGCEMPFRN) AndAlso String.IsNullOrWhiteSpace(mercadoria.NUMCGCEMPFRN))) Then 'verificar
                                Warning("A forma de pagamento e o fornecedor devem ser obrigatóriamente selecionados devido a seleção de fornecedor único e/ou item via app!!!")
                                Exit Function
                            End If
                        End If

                        'Valida se o item a ser incluido ja foi inserido na acao
                        If (mercadoria.INDTIPOPEDSNACOCMC = 2) Or (mercadoria.INDTIPOPEDSNACOCMC = 4) Then
                            If acaoCache.ItensOrcamento.Count > 0 Then

                                Dim bo As New TipoDeEventoBO
                                Dim tipoEvento = bo.SelectT(New VO.TipoDeEvento With {.TIPEVTACOCMC = acaoCache.TIPEVTACOCMC})
                                If (tipoEvento.INDCPHBTB = 1) Then ' verifica se for campanha b2b permite mercadorias duplicadas para empresas diferentes.
                                    If (acaoCache.ItensOrcamento.Where(Function(f) f.CODMER = mercadoria.CODMER And f.CODOPEFSCDSNACOCMC = mercadoria.INDTIPOPEDSNACOCMC And f.CODFILEMP = mercadoria.CODFILEMP).FirstOrDefault() IsNot Nothing) Then
                                        Warning("Este Item já foi adicionado a esta Ação Comercial/Empresa!")
                                        Exit Function
                                    End If
                                Else
                                    If (acaoCache.ItensOrcamento.Where(Function(f) f.CODMER = mercadoria.CODMER And f.CODOPEFSCDSNACOCMC = mercadoria.INDTIPOPEDSNACOCMC).FirstOrDefault() IsNot Nothing) Then
                                        Warning("Este Item já foi adicionado a esta Ação Comercial!")
                                        Exit Function
                                    End If
                                End If

                                'Dim arrayCodIte = acaoCache.ItensOrcamento.Select(Function(f) f.CODMER).ToArray
                                '    Dim arrayTipIte = acaoCache.ItensOrcamento.Select(Function(f) f.CODOPEFSCDSNACOCMC).ToArray
                                '    For aux As Integer = 0 To arrayCodIte.Length - 1
                                '        If (arrayCodIte(aux) = mercadoria.CODMER) And (arrayTipIte(aux) = mercadoria.INDTIPOPEDSNACOCMC) Then
                                '            Warning("Este Item já foi adicionado a esta Ação Comercial!")
                                '        Exit Function
                                '        End If
                                '    Next
                            End If
                        End If

                        Dim tipoOperacao As Decimal = mercadoria.INDTIPOPEDSNACOCMC

                        If operacao.INDTIPOPEDSNACOCMC = 1 Then

                            If boRelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPreco.ValidaProdutoFracionado(mercadoria.CODFILEMP, mercadoria.CODMER).Count <> 0 Then
                                'TODO: fazer confirmação com usuario se ele deseja continuar com a mesma mensagem abaixo
                                'antes de sair da função, sair apenas se ele escolher "nao"
                                Warning("Atenção ! A mercadoria que está sendo inserida é fracionada " &
                                         "e pode ser fornecida pelo CAD há regiões atendidas por um CDR. " +
                                         "Caso isto aconteça as regioes atendidas pelo CDR não conseguirão " +
                                         "fazer bonificações, consumo ou promoções com esta mercadoria " +
                                         "pelo CAD que abastece o CDR.")
                                Exit Function
                            End If

                        End If


                        AtuIteSub(acaoCache)


                        If operacao.INDGRCREQAUTITERSU = 1 Then
                            If GrcIdtIteRsu(mercadoria.CODCENCST, mercadoria, acaoCache) = 0 Then
                                Exit Function
                            End If
                        End If
                        mercadoria.INDGRCREQAUTITERSU = operacao.INDGRCREQAUTITERSU
                        'TODO: este codigo nao faz o menor sentido -- INICIO
                        'If operacao.INDTIPOPEREQVGM = 1 Or operacao.INDTIPOPEREQEVT = 1 Or operacao.INDTIPOPESLCPGT = 1 Then 'item do orçamento
                        '    'mercadoria.CODITEOCDACOCMC = mercadoria.CODITEOCDACOCMC 'Se for requisição de viagem viagem
                        'Else
                        '    If Not IsNothing(mercadoria.CODMER) Then
                        '        mercadoria.CODITEOCDACOCMC = mercadoria.CODITEOCDACOCMC
                        '    Else
                        '        mercadoria.CODITEOCDACOCMC = mercadoria.CODITEOCDACOCMC
                        '    End If
                        'End If
                        'TODO: este codigo nao faz o menor sentido -- FIM
                        If Not String.IsNullOrEmpty(mercadoria.DescDropSuprimentos) AndAlso Not String.IsNullOrWhiteSpace(mercadoria.DescDropSuprimentos) Then
                            'mercadoria.CODSTAITERSU = mercadoria.CODSTAITERSU
                            'mercadoria.DESSTAITERSU = mercadoria.DESSTAITERSU
                        Else
                            If operacao.INDGRCREQAUTITERSU = 1 Then
                                mercadoria.CODSTAITERSU = 1
                                mercadoria.DESSTAITERSU = "Em Aberto"
                            Else
                                mercadoria.CODSTAITERSU = 0
                                mercadoria.DESSTAITERSU = ""
                            End If
                        End If

                        mercadoria.INDBDEOPEDSNACOCMC = operacao.INDBDEOPEDSNACOCMC 'Indicador de brinde
                        mercadoria.INDTIPOPEDSNACOCMC = operacao.INDTIPOPEDSNACOCMC 'Indicador do tipo de operação
                        mercadoria.INDUNDESRNGCACOCMC = operacao.INDUNDESRNGCACOCMC 'Nome do tipo de operação
                        mercadoria.QDEUTZOPEDSNACOCMC = 0 'Quantidade Utilizada
                        mercadoria.VLRUTZOPEDSNACOCMC = 0 'Valor utilizado
                        mercadoria.DESOBSCPR = Util.RemoverCharEspecial(mercadoria.DESOBSCPR) 'Observação ao comprador de suprimentos
                        mercadoria.INDITEREQRSUVLRMNM = 0 'Indicador de valor minimo do item


                        If IsNothing(mercadoria.VLRPTCCLIITEACOCMC) Then
                            mercadoria.VLRPTCCLIITEACOCMC = 0 'valor participacao cliente cliente no item.
                        End If
                        If IsNothing(mercadoria.VLRPTCFRNITEACOCMC) Then
                            mercadoria.VLRPTCFRNITEACOCMC = 0 'valor participacao fornecedor no item.
                        End If
                        If mercadoria.INDITERSUFRNUNC Then
                            mercadoria.INDITERSUFRNUNC = 1
                        Else
                            mercadoria.INDITERSUFRNUNC = 0
                        End If

                        If mercadoria.INDITERSUARZPRVPGT Then
                            mercadoria.INDITERSUARZPRVPGT = 1
                        Else
                            mercadoria.INDITERSUARZPRVPGT = 0
                        End If
                        If IsNothing(mercadoria.DATUTZITEACOCMC) Then
                            mercadoria.DATUTZITEACOCMC = Date.MinValue
                        End If
                        If IsNothing(mercadoria.CODACOCMC) Then
                            mercadoria.CODACOCMC = acaoCache.CODACOCMC
                        End If
                        If IsNothing(mercadoria.NUMLNHITEOCDACOCMC) Then
                            mercadoria.NUMLNHITEOCDACOCMC = acaoCache.ItensOrcamento.Max(Function(f) f.NUMLNHITEOCDACOCMC) + 1
                            If IsNothing(mercadoria.NUMLNHITEOCDACOCMC) Then
                                mercadoria.NUMLNHITEOCDACOCMC = 1
                            End If
                        End If

                        '
                        Dim itensSub As List(Of MercadoriaViewModel) = Session.Item("itensSub")
                        If (Not IsNothing(mercadoria)) Then
                            If Not IsNothing(itensSub) AndAlso itensSub.Count > 0 Then
                                Dim retornoItensSub As Boolean = SalvarItensSub(True, itensSub, mercadoria, acaoCache)

                                If (retornoItensSub = False) Then
                                    EditarItem(mercadoria)
                                    Return Nothing
                                    Exit Function
                                End If
                            End If
                        End If

                        acaoCache.ItensOrcamento.Add(mercadoria)
                        Session("acaoModel") = acaoCache

                        mercadoria.CODOPEFSCDSNACOCMC = operacao.CODOPEFSCDSNACOCMC
                        mercadoria.DESOPEFSCDSNACOCMC = operacao.DESOPEFSCDSNACOCMC

                        mercadoria = InsDdoIteAcoCmc(mercadoria, acaoCache)

                        Return fecharModalItens(mercadoria, patrocinador)

                    Else
                        Warning("A data de utilização do item deve estar no intervalo válido para a ação!")
                        Exit Function
                    End If
                End If
            Catch ex As Exception
                Danger(ex)
            End Try
        End Function



        Public Function FunVrfDatUtzIte(mercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel) As Integer
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim operacaoVM As TipoOperacaoViewModel = Mapper.Map(Of VO.OperacaoFiscal, TipoOperacaoViewModel)(boOperacao.ListT(New VO.OperacaoFiscal With {.CODOPEFSCDSNACOCMC = mercadoria.CODOPEFSCDSNACOCMC}).FirstOrDefault)

                If operacaoVM.INDGRCREQAUTITERSU = 0 Then
                    If (acaoCache.DATINIACOCMC <= mercadoria.DATUTZITEACOCMC) And (acaoCache.DATFIMACOCMC >= mercadoria.DATUTZITEACOCMC) Then
                        Return 0
                    Else
                        Warning("A data de utilização do item deve estar no intervalo válido para a ação!")
                        mercadoria.DATUTZITEACOCMC = Nothing
                        Return 1
                    End If
                Else
                    'Não verifica data para item de requisição de bens de consumo e serviços
                    Return 0
                End If
            Catch ex As Exception
                Danger(ex)
            End Try
        End Function

        Public Function FunVrfInsDdoCpo(mercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel, operacao As VO.OperacaoFiscal, acao As AcaoComercialViewModel) As Integer

            'Verifica tipo de operação
            If IsNothing(mercadoria.INDTIPOPEDSNACOCMC) Then
                Warning("Escolha um tipo de operação")
                Return 1
            End If

            'Verifica filial
            If mercadoria.INDTIPOPEDSNACOCMC = 1 And IsNothing(mercadoria.CODFILEMP) Then
                Warning("Escolha uma filial")
                Return 1
            End If

            'Verifica BU
            If mercadoria.INDUNDESRNGCACOCMC = 1 And mercadoria.CODUNDESRNGCOPEDSN Then
                Warning("Escolha um BU")
                Return 1
            End If

            'Item do orçamento
            If IsNothing(mercadoria.CODMER) Then
                Warning("Item do orçamento não escolhido")
                Return 1
            End If

            'Verificando Valor Unitario
            If IsNothing(mercadoria.VLRUNTITEOCDACOCMC) Then
                Warning("Digite o valor unitário do item")
                Return 1
            End If

            'Verificando Forma de Utilizacao
            If String.IsNullOrWhiteSpace(mercadoria.DESFRMUTZITEACOCMC) Then
                Warning("Digite a forma de utilização do item")
                Return 1
            End If

            'Verificando Quantidade Utilizada
            If IsNothing(mercadoria.QDEITEOCDACOCMC) Or mercadoria.QDEITEOCDACOCMC <= 0 Then
                Warning("Digite a quantidade utilizada do item")
                Return 1
            End If

            If mercadoria.INDITERSUARZPRVPGT = 1 Or mercadoria.INDITERSUFRNUNC = 1 Then
                'Verificando Condicao de pagamento
                If (IsNothing(mercadoria.IDTTERPGTITERSU) OrElse mercadoria.IDTTERPGTITERSU = 0) Then
                    Warning("Selecione a forma de pagamento para este item!")
                    Return 1
                End If
                'Veficando Fornecedor de Item de Consumo
                If (IsNothing(mercadoria.IDTEMPFRNITERSU) OrElse mercadoria.IDTEMPFRNITERSU = 0) Then
                    Warning("Selecione o fornecedor para este item!")
                    Return 1
                End If
            End If

            'Verificando se já existe algum item de viagem igual ao que esta cadastrado para a ação corrente
            If operacao.INDTIPOPEREQVGM = 1 Or operacao.INDTIPOPEREQEVT = 1 Or operacao.INDTIPOPESLCPGT = 1 Then
                If acao.ItensOrcamento.Count > 1 Then
                    For Each item In acao.ItensOrcamento
                        If item.CODITEOCDACOCMC = mercadoria.CODITEOCDACOCMC And item.INDTIPOPEDSNACOCMC = mercadoria.INDTIPOPEDSNACOCMC Then
                            Warning("Este Item já foi adicionado a esta Ação Comercial!")
                            Return 1
                        End If
                    Next
                End If

                If mercadoria.VLRUNTITEOCDACOCMC <= 0 Then
                    Warning("Por favor, preencha corretamente o valor unitário do item.")
                    Return 1
                End If
            End If

            'Verificando se já existe algum item de requisição de bens e serviços igual ao que esta cadastrado para a ação corrente
            If operacao.INDGRCREQAUTITERSU = 1 Then
                If Not CnsUsrOra(ControleAcesso.CodFnc()) Then 'pegar codigo do usuario logado
                    Warning("Usuário não está cadastrado no iPROC. Contate o analista!")
                    Return 1
                End If

                If acao.ItensOrcamento.Count > 1 Then
                    For Each item In acao.ItensOrcamento
                        If item.IDTITERSU = mercadoria.IDTITERSU Then
                            Warning("Este Item já foi adicionado a esta Ação Comercial!")
                            Return 1
                        End If
                    Next
                End If

                If mercadoria.VLRUNTITEOCDACOCMC <= 0 Then
                    Warning("Por favor, preencha corretamente o valor unitário do item.")
                    Return 1
                End If
            End If
            Return 0
        End Function

        ''' <summary>
        ''' função para salvar alterações feitas nos itens
        ''' </summary>
        ''' <param name="mercadoria"></param>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function Alterar(mercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel, patrocinador As Integer)

            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                If IsNothing(mercadoria.AcaoComercial) Then
                    mercadoria.AcaoComercial = acaoCache
                End If
                If IsNothing(mercadoria.CODACOCMC) Then
                    mercadoria.CODACOCMC = acaoCache.CODACOCMC
                End If

                If (acaoCache.CODSTAAPVACOCMC <> 5) Then
                    Dim mercParaAtt As RelacaoAcaoComercialPorItemOrcamentoViewModel = acaoCache.ItensOrcamento.Where(Function(f) f.NUMLNHITEOCDACOCMC = mercadoria.NUMLNHITEOCDACOCMC).FirstOrDefault

                    mercParaAtt.QDEITEOCDACOCMC = mercadoria.QDEITEOCDACOCMC
                    mercParaAtt.CODOPEFSCDSNACOCMC = mercadoria.INDTIPOPEDSNACOCMC
                    mercParaAtt.CODFILEMP = mercadoria.CODFILEMP
                    mercParaAtt.CODUNDESRNGCOPEDSN = mercadoria.CODUNDESRNGCOPEDSN
                    mercParaAtt.CODCENCST = mercadoria.CODCENCST
                    mercParaAtt.CODMER = mercadoria.CODMER
                    mercParaAtt.DESCENCST = mercadoria.DESCENCST
                    mercParaAtt.VLRUNTITEOCDACOCMC = mercadoria.VLRUNTITEOCDACOCMC
                    mercParaAtt.DATUTZITEACOCMC = mercadoria.DATUTZITEACOCMC
                    mercParaAtt.VLRPTCCLIITEACOCMC = mercadoria.VLRPTCCLIITEACOCMC
                    mercParaAtt.VLRPTCFRNITEACOCMC = mercadoria.VLRPTCFRNITEACOCMC
                    mercParaAtt.QDEDIACMPITEACOCMC = mercadoria.QDEDIACMPITEACOCMC
                    mercParaAtt.INDITERSUFRNUNC = mercadoria.INDITERSUFRNUNC
                    mercParaAtt.INDITERSUARZPRVPGT = mercadoria.INDITERSUARZPRVPGT
                    mercParaAtt.IDTTERPGTITERSU = mercadoria.IDTTERPGTITERSU
                    mercParaAtt.IDTLCLEMPFRNITERSU = mercadoria.IDTLCLEMPFRNITERSU
                    mercParaAtt.NUMCGCEMPFRN = mercadoria.NUMCGCEMPFRN
                    mercParaAtt.IDTEMPFRNITERSU = mercadoria.IDTEMPFRNITERSU
                    mercParaAtt.DESFRMUTZITEACOCMC = mercadoria.DESFRMUTZITEACOCMC
                    mercParaAtt.DESOBSCPR = mercadoria.DESOBSCPR

                    If IsNothing(mercParaAtt.QDEDIACMPITEACOCMC) Then
                        mercParaAtt.QDEDIACMPITEACOCMC = 0
                    End If
                    If IsNothing(mercParaAtt.IDTEMPFRNITERSU) Then
                        mercParaAtt.IDTEMPFRNITERSU = 0
                    End If
                    If IsNothing(mercParaAtt.IDTLCLEMPFRNITERSU) Then
                        mercParaAtt.IDTLCLEMPFRNITERSU = 0
                    End If
                    If IsNothing(mercParaAtt.IDTTERPGTITERSU) Then
                        mercParaAtt.IDTTERPGTITERSU = 0
                    End If
                    If IsNothing(mercParaAtt.CODIDTTSC) Then
                        mercParaAtt.CODIDTTSC = 0
                    End If
                    If IsNothing(mercParaAtt.NUMCTTFRN) Then
                        mercParaAtt.NUMCTTFRN = 0
                    End If
                    If IsNothing(mercParaAtt.IDTITERSU) Then
                        mercParaAtt.IDTITERSU = 0
                    End If
                    If IsNothing(mercParaAtt.NUMLNHCTTRSU) Then
                        mercParaAtt.NUMLNHCTTRSU = 0
                    End If
                    If IsNothing(mercParaAtt.DESCTTRSU) Then
                        mercParaAtt.DESCTTRSU = 0
                    End If
                    If IsNothing(mercParaAtt.NUMCGCEMPFRN) Then
                        mercParaAtt.NUMCGCEMPFRN = 0
                    End If
                    If IsNothing(mercParaAtt.DATUTZITEACOCMC) Then
                        mercParaAtt.DATUTZITEACOCMC = Date.MinValue
                    End If
                    If IsNothing(mercParaAtt.CODUNDESRNGCOPEDSN) Then
                        mercParaAtt.CODUNDESRNGCOPEDSN = 0
                    End If
                    If IsNothing(mercParaAtt.CODSTAITERSU) Then
                        mercParaAtt.CODSTAITERSU = 1
                    End If
                    If (IsNothing(mercParaAtt.IDTEMPFRNITERSU)) Then
                        mercParaAtt.IDTEMPFRNITERSU = 0
                    End If
                    If (IsNothing(mercParaAtt.IDTCNTCTB)) Then
                        mercParaAtt.IDTCNTCTB = 0
                    End If
                    If (IsNothing(mercParaAtt.DESOBSCPR)) Then
                        mercParaAtt.DESOBSCPR = " "
                    End If

                    'Validação dos tipos de operações 10, 13, 15, 16 e 18
                    If (mercParaAtt.INDTIPOPEDSNACOCMC = 10 Or mercParaAtt.INDTIPOPEDSNACOCMC = 13 Or mercParaAtt.INDTIPOPEDSNACOCMC = 15 Or
                    mercParaAtt.INDTIPOPEDSNACOCMC = 16 Or mercParaAtt.INDTIPOPEDSNACOCMC = 16) Then
                        mercParaAtt.CODMER = 0
                        mercParaAtt.CODITEOCDACOCMC = mercadoria.NOMITEMORC
                    ElseIf (mercParaAtt.INDTIPOPEDSNACOCMC = 6 Or mercParaAtt.INDTIPOPEDSNACOCMC = 7 Or
                        mercParaAtt.INDTIPOPEDSNACOCMC = 17) Then 'Validação dos tipos de operações 6, 7 e 17
                        Dim campos As String() = Split(mercadoria.NOMITEMORCDROP)
                        mercParaAtt.CODMER = CDec(campos(0))
                        mercParaAtt.CODITEOCDACOCMC = campos(2)
                    End If

                    boRelacaoAcaoComercialPorItemOrcamento.UpdateT(Mapper.Map(Of RelacaoAcaoComercialPorItemOrcamentoViewModel, VO.RelacaoAcaoComercialPorItemOrcamento)(mercParaAtt))
                    acaoCache.StateGridItensOrcamento = PaginarEAtualizarGrid(acaoCache.ItensOrcamento, acaoCache.StateGridItensOrcamento)
                    ViewBag.btnIsrItemVisible = True

                    calculaItemFolhaPagamento(acaoCache)

                    Util.AtuSldOcd(acaoCache) 'atualiza o saldo do orçamento 
                    Util.CalVlrTot(acaoCache) 'calcula vlr total do brinde e do orçamento

                Else
                    ViewBag.btnGravarVisible = False
                    ViewBag.btnGasto = True
                End If

                'Verifica se já existe o item substituto na ação
                Dim itensSub As List(Of MercadoriaViewModel) = Session.Item("itensSub")
                Dim retorno As Boolean = SalvarItensSub(False, itensSub, mercadoria, acaoCache)


                Session("itensSub") = itensSub

                If (retorno) Then
                    Return PartialView("~/Views/AcaoComercial/Orcamento/_WidgetOrcamento.vbhtml", acaoCache)

                Else
                    EditarItem(mercadoria)
                    Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
                    Return Nothing
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Public Function FunVrfAltDdoCpo(mercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel) As Boolean
            If mercadoria.INDITEREQRSUVLRMNM = 1 Then
                'Verificando Condicao de pagamento
                If mercadoria.INDITERSUARZPRVPGT = 1 Or mercadoria.INDITERSUFRNUNC = 1 Then
                    'Verificando Condicao de pagamento
                    If (IsNothing(mercadoria.IDTTERPGTITERSU) OrElse mercadoria.IDTTERPGTITERSU = 0) Then
                        Warning("Selecione a forma de pagamento para este item!")
                        Return False
                    End If
                    'Veficando Fornecedor de Item de Consumo
                    If (IsNothing(mercadoria.INDITERSUFRNUNC) OrElse mercadoria.INDITERSUFRNUNC = 0) Then
                        Warning("Selecione o fornecedor para este item!")
                        Return False
                    End If
                End If
            End If

            Return True

        End Function


        Public Function AtuDtaUtzIte(mercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel) As Integer

            Dim indAtu As Integer ' indicador de atualização

            Try
                If Not IsNothing(mercadoria.DATUTZITEACOCMC) AndAlso mercadoria.DATUTZITEACOCMC > Date.MinValue Then


                    indAtu = boRelacaoAcaoComercialPorItemOrcamento.AtuDatUtzIte(Mapper.Map(Of RelacaoAcaoComercialPorItemOrcamentoViewModel, VO.RelacaoAcaoComercialPorItemOrcamento)(mercadoria))
                End If
                Return indAtu
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Public Function AtuDdoFrnIte(mercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel) As Boolean

            Try
                If Not IsNothing(mercadoria.DATUTZITEACOCMC) Then
                    boRelacaoAcaoComercialPorItemOrcamento.AtuDdoFrnIte(Mapper.Map(Of RelacaoAcaoComercialPorItemOrcamentoViewModel, VO.RelacaoAcaoComercialPorItemOrcamento)(mercadoria))
                End If
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        ''' <summary>
        ''' abrir modal de itens substitutos
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function ItensSubstitutos(objeto As RelacaoAcaoComercialPorItemOrcamentoViewModel) As PartialViewResult
            Try

                Dim operacao = Mapper.Map(Of VO.OperacaoFiscal, TipoOperacaoViewModel)(boOperacao.SelectT(New VO.OperacaoFiscal With {.CODOPEFSCDSNACOCMC = objeto.INDTIPOPEDSNACOCMC}))
                ViewBag.descOperacao = operacao.DROPDESC

                If (Not IsNothing(objeto.VLRUNTITEOCDACOCMC) AndAlso objeto.VLRUNTITEOCDACOCMC = 0) Then
                    objeto.VLRUNTITEOCDACOCMC = ConsultaPrecoUnitarioItem(objeto.CODMER, objeto.CODFILEMP)
                End If


                Dim dadosPesquisa As New GridSettings(Of MercadoriaViewModel)
                dadosPesquisa.Filter = New MercadoriaViewModel With {.DESMER = objeto.CODITEOCDACOCMC, .CODMER = objeto.CODMER}


                Dim filtroSubstituto As New VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO
                filtroSubstituto.CODACOCMC = objeto.CODACOCMC
                filtroSubstituto.CODMER = objeto.CODMER
                filtroSubstituto.NUMLNHITEOCDACOCMC = objeto.NUMLNHITEOCDACOCMC
                'filtroSubstituto.VLRUNTITEOCDACOCMC = objeto.VLRUNTITEOCDACOCMC

                Dim mercadorias As New List(Of MercadoriaViewModel)

                If (Not IsNothing(Session("itensSub"))) Then
                    mercadorias = Session("itensSub")
                End If

                'If (Not IsNothing(Session("itensSubAntigos"))) Then
                '    mercadorias = Session("itensSubAntigos")
                'End If



                Dim mercadoriasVazias As List(Of MercadoriaViewModel) = New List(Of MercadoriaViewModel)


                Dim tasks As New List(Of Threading.Tasks.Task)
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                For Each item In mercadorias
                    ' Abre uma thread para cada Mercadoria, para consultar o preço.
                    ' A criação das threads foi necessario para reduzir o tempo total de consulta dos preços quando tem centenas de mercadorias.
                    tasks.Add(Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                        item.ValorUnitario = boMovimentoQuinzenal.ConsultaPrecoUnitarioItemThread(item.CODMER, objeto.CODFILEMP, acaoCache.TIPACOMCD)
                                                                        If (item.ValorUnitario <> 0) Then
                                                                            item.quantdadeSubstitutos = System.Math.Floor(objeto.ValorTotal / item.ValorUnitario.Value)
                                                                            item.valorTotalSubs = item.quantdadeSubstitutos * item.ValorUnitario.Value
                                                                        Else
                                                                            item.quantdadeSubstitutos = System.Math.Floor(objeto.ValorTotal)
                                                                            item.valorTotalSubs = item.quantdadeSubstitutos
                                                                        End If
                                                                    End Sub
                                                                    )
                                                                )
                Next
                Threading.Tasks.Task.WaitAll(tasks.ToArray()) ' aguarda que todas as consultas de preços sejam concluidas


                'For Each item In mercadorias
                '    item.ValorUnitario = ConsultaPrecoUnitarioItem(item.CODMER, objeto.CODFILEMP)

                '    If (item.ValorUnitario <> 0) Then
                '        item.quantdadeSubstitutos = System.Math.Floor(objeto.ValorTotal / item.ValorUnitario.Value)
                '        item.valorTotalSubs = item.quantdadeSubstitutos * item.ValorUnitario.Value
                '    Else
                '        item.quantdadeSubstitutos = System.Math.Floor(objeto.ValorTotal)
                '        item.valorTotalSubs = item.quantdadeSubstitutos
                '    End If
                'Next

                Dim gridConf1 = New GridSettings(Of MercadoriaViewModel)(New MercadoriaViewModel(), mercadoriasVazias.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))
                gridConf1.ResultID = "resultadosBuscaItensSubstitutos"
                gridConf1.TotalCount = mercadoriasVazias.Count
                gridConf1.ActionName = "recarregaItensSubBuscados"
                gridConf1.PaginationID = "resultadosBuscaItensSubstitutos"


                Dim gridConf2 = New GridSettings(Of MercadoriaViewModel)(dadosPesquisa.Filter, mercadorias.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))
                gridConf2.ResultID = "gridItensSubstitutosSelecionados"
                gridConf2.TotalCount = mercadorias.Count
                gridConf2.ActionName = "recarregaItensSubSelecionados"
                gridConf2.PaginationID = "gridItensSubstitutosSelecionados"

                Dim resultados As Tuple(Of GridSettings(Of MercadoriaViewModel), RelacaoAcaoComercialPorItemOrcamentoViewModel, GridSettings(Of MercadoriaViewModel)) =
                    New Tuple(Of GridSettings(Of MercadoriaViewModel), RelacaoAcaoComercialPorItemOrcamentoViewModel, GridSettings(Of MercadoriaViewModel)) _
                    (gridConf1, objeto, gridConf2)

                Return PartialView("~/Views/AcaoComercial/Orcamento/ItensSubstitutos/_ModalItensSubstitutos.vbhtml", resultados)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' função para paginar e recarregar itens subs selecionados
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function recarregaItensSubSelecionados(dadosPesquisa As GridSettings(Of MercadoriaViewModel)) As PartialViewResult

            Dim novosDados As List(Of MercadoriaViewModel)
            novosDados = Session.Item("listaTempItensSelecionados")

            dadosPesquisa = PaginarEAtualizarGrid(novosDados, dadosPesquisa)
            Return PartialView("~/Views/AcaoComercial/Orcamento/ItensSubstitutos/_ResultadoItensSubstitutosSelecionados.vbhtml", dadosPesquisa)
        End Function


        ''' <summary>
        ''' função para paginar e recarregar itens subs buscados
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function recarregaItensSubBuscados(dadosPesquisa As GridSettings(Of MercadoriaViewModel)) As PartialViewResult
            Dim filtroAuxItensSub = Session.Item("filtroAuxItensSub")

            Dim filtro As VO.Mercadoria = Mapper.Map(Of MercadoriaViewModel, VO.Mercadoria)(dadosPesquisa.Filter)

            Dim mercadoriassViewModel As List(Of MercadoriaViewModel) = BuscaItensOrcamento(filtro.CODMER, filtro.DESMER, filtroAuxItensSub.codOperacao, filtroAuxItensSub.codFilial, filtroAuxItensSub.patrocinador)
            For Each item In mercadoriassViewModel
                item.ValorUnitario = ConsultaPrecoUnitarioItem(item.CODMER, filtroAuxItensSub.codFilial)
                If item.ValorUnitario.Value <= 0 Then
                    item.quantdadeSubstitutos = 0
                Else
                    item.quantdadeSubstitutos = System.Math.Floor(filtroAuxItensSub.valorTotalItemOriginal / item.ValorUnitario.Value)
                End If
                item.valorTotalSubs = item.quantdadeSubstitutos * item.ValorUnitario.Value
            Next

            mercadoriassViewModel = mercadoriassViewModel.Where(Function(f) f.quantdadeSubstitutos > 0).ToList

            dadosPesquisa = PaginarEAtualizarGrid(mercadoriassViewModel, dadosPesquisa)
            Return PartialView("~/Views/AcaoComercial/Orcamento/ItensSubstitutos/_ResultadoItensSubstitutos.vbhtml", dadosPesquisa)
        End Function

        ''' <summary>
        ''' função para pesquisar itens substitutos
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function PesquisarSubstitutos(dadosPesquisa As MercadoriaViewModel, codOperacao As Integer, codFilial As Integer, patrocinador As Integer, valorTotalItemOriginal As Decimal) As PartialViewResult

            Dim filtroAuxItensSub As Object = New With {.codOperacao = codOperacao, .codFilial = codFilial, .patrocinador = patrocinador, .valorTotalItemOriginal = valorTotalItemOriginal}
            Session("filtroAuxItensSub") = filtroAuxItensSub

            If IsNothing(dadosPesquisa.CODMER) Then
                dadosPesquisa.CODMER = 0
            End If

            Dim mercadoriassViewModel As List(Of MercadoriaViewModel) = BuscaItensOrcamento(dadosPesquisa.CODMER, dadosPesquisa.DESMER, codOperacao, codFilial, patrocinador)

            For Each item In mercadoriassViewModel
                item.ValorUnitario = ConsultaPrecoUnitarioItem(item.CODMER, codFilial)
                If (item.ValorUnitario.Value > 0) Then
                    item.quantdadeSubstitutos = System.Math.Floor(valorTotalItemOriginal / item.ValorUnitario.Value)
                Else
                    item.quantdadeSubstitutos = 0
                End If
                item.valorTotalSubs = item.quantdadeSubstitutos * item.ValorUnitario.Value
            Next
            mercadoriassViewModel = mercadoriassViewModel.Where(Function(f) f.quantdadeSubstitutos > 0).ToList

            Dim gridConf = PaginarEAtualizarGrid(mercadoriassViewModel, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT}, dadosPesquisa)
            If gridConf.TotalCount = 0 Then
                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
            End If

            'For Each merc In gridConf.ListPaged
            '    merc.ValorUnitario = ConsultaPrecoUnitarioItem(merc.CODMER, codFilial)
            'Next

            Session("listaTempItens") = mercadoriassViewModel
            Dim listaTemp = Session.Item("listaTempItensSelecionados")
            If IsNothing(listaTemp) Then
                Session("listaTempItensSelecionados") = New List(Of MercadoriaViewModel)
            End If

            gridConf.ResultID = "resultadosBuscaItensSubstitutos"
            gridConf.ActionName = "recarregaItensSubBuscados"
            gridConf.PaginationID = "resultadosBuscaItensSubstitutos"
            gridConf.ControllerName = "ItensOrcamento"
            gridConf.Filter = dadosPesquisa
            Return PartialView("~/Views/AcaoComercial/Orcamento/ItensSubstitutos/_ResultadoItensSubstitutos.vbhtml", gridConf)
        End Function

        ''' <summary>
        ''' função para selecionar itens substitutos
        ''' </summary>
        ''' <param name="itensCheckados"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function SelecionarItensSubstitutos(ByVal itensCheckados As Decimal) As PartialViewResult
            Try
                Dim listaItens As List(Of MercadoriaViewModel) = Session.Item("listaTempItens")
                Dim listaItensSelecionados As List(Of MercadoriaViewModel) = Session.Item("listaTempItensSelecionados")

                Dim itensParaAdd = listaItens.Where(Function(f) itensCheckados = f.CODMER).FirstOrDefault

                If Not (itensParaAdd Is Nothing) Then
                    If (listaItensSelecionados.Where(Function(x) x.CODMER = itensParaAdd.CODMER And x.CODFRNPCPMER = itensParaAdd.CODFRNPCPMER).Count > 0) Then
                        Return Nothing
                    End If
                    listaItensSelecionados.Add(itensParaAdd)
                End If


                'If (Not IsNothing(mercadorias)) Then
                '    mercadorias.AddRange(listaItensSelecionados)
                'Else
                '    mercadorias = listaItensSelecionados
                'End If
                Session("listaTempItensSelecionados") = listaItensSelecionados

                Dim gridConf = GridSettings(Of MercadoriaViewModel).EmptyGrid("gridItensSubstitutosSelecionados", "SelecionarItensSubstitutos", "ItensOrcamento")
                gridConf = PaginarEAtualizarGrid(listaItensSelecionados, gridConf)
                gridConf.TotalCount = listaItensSelecionados.Count
                gridConf.ResultID = "gridItensSubstitutosSelecionados"
                gridConf.ActionName = "recarregaItensSubSelecionados"
                gridConf.PaginationID = "gridItensSubstitutosSelecionados"

                Return PartialView("~/Views/AcaoComercial/Orcamento/ItensSubstitutos/_ResultadoItensSubstitutosSelecionados.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        <HttpPost()>
        Public Function RemoverItensSubstitutos(ByVal itensCheckados As Decimal) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                If (acaoCache.CODSTAAPVACOCMC <> 5) Then

                    Dim listaItensSelecionados As List(Of MercadoriaViewModel) = Session.Item("listaTempItensSelecionados")
                    'listaItensSelecionados = listaItensSelecionados.Where(Function(f) Not itensCheckados.Contains(f.CODMER)).ToList()
                    'itensCheckados = itensCheckados.OrderByDescending(Function(f) f).ToList
                    'For Each indice In itensCheckados
                    listaItensSelecionados.Remove(listaItensSelecionados.Where(Function(g) g.CODMER = itensCheckados).FirstOrDefault)
                    'Next
                    Session("listaTempItensSelecionados") = listaItensSelecionados

                    Dim gridConf = GridSettings(Of MercadoriaViewModel).EmptyGrid("gridItensSubstitutosSelecionados", "SelecionarItensSubstitutos", "ItensOrcamento")
                    gridConf.ListPaged = PaginarLista(listaItensSelecionados)
                    gridConf.TotalCount = listaItensSelecionados.Count

                    Return PartialView("~/Views/AcaoComercial/Orcamento/ItensSubstitutos/_ResultadoItensSubstitutosSelecionados.vbhtml", gridConf)
                Else
                    Dim listaItensSelecionados As List(Of MercadoriaViewModel) = Session.Item("listaTempItensSelecionados")

                    Dim gridConf = GridSettings(Of MercadoriaViewModel).EmptyGrid("gridItensSubstitutosSelecionados", "SelecionarItensSubstitutos", "ItensOrcamento")
                    gridConf.ListPaged = PaginarLista(listaItensSelecionados)
                    gridConf.TotalCount = listaItensSelecionados.Count

                    Warning(Constantes.NAO_PODE_EXCLUIR_ITEM_SUBSTITUTO)

                    Return PartialView("~/Views/AcaoComercial/Orcamento/ItensSubstitutos/_ResultadoItensSubstitutosSelecionados.vbhtml", gridConf)
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' função para salvar lista de itens sub temporariamente
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function SalvarCacheItensSub() As Boolean
            Dim listaItensSelecionados As List(Of MercadoriaViewModel) = Session.Item("listaTempItensSelecionados")
            Session("itensSub") = listaItensSelecionados
            Return True
        End Function

        Public Function SalvarItensSub(ByVal incluir As Boolean, ByRef itens As List(Of MercadoriaViewModel), relacao As RelacaoAcaoComercialPorItemOrcamentoViewModel, acao As AcaoComercialViewModel) As Boolean

            If (Not IsNothing(itens)) Then

                Dim countReg As Integer = 0

                Dim velhos = boRelacaoItemPrincipalItemSubstitutoAcaoComercial.ListT(New VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO() With {.CODACOCMC = relacao.CODACOCMC, .CODMER = relacao.CODMER, .NUMLNHITEOCDACOCMC = relacao.NUMLNHITEOCDACOCMC})
                Dim listSubstituto As New List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)

                Dim tasks As New List(Of Threading.Tasks.Task)
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                'Verifica se já existe o item substituto na ação
                For Each item In itens
                    ' ############ Estas validações foram comentadas pois gastavam muito recurso de servidor quando tem muitas mercadorias, e ao incluir o item já nao permite inserir duplicado ############
                    '    If (incluir) Then
                    '        countReg = boRelacaoItemPrincipalItemSubstitutoAcaoComercial.ListT(New VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO With {.CODACOCMC = relacao.CODACOCMC, .CODMERSBT = item.CODMER}).Count

                    '        If countReg > 0 Then
                    '            Warning("Item substituto " + item.CODMER.ToString() + " já cadastrado nesta ação.")
                    '            itens.Remove(item)
                    '            Return False
                    '            Exit Function
                    '        End If
                    '    Else
                    '        For Each itemVelhos In velhos
                    '            If (item.CODMER <> itemVelhos.CODMERSBT) Then
                    '                countReg = boRelacaoItemPrincipalItemSubstitutoAcaoComercial.ListT(New VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO With {.CODACOCMC = relacao.CODACOCMC, .CODMERSBT = item.CODMER}).Count

                    '                If countReg > 1 Then
                    '                    Warning("Item substituto " + item.CODMER.ToString() + " já cadastrado nesta ação.")
                    '                    itens.Remove(item)
                    '                    Return False
                    '                    Exit Function
                    '                End If
                    '            End If
                    '        Next
                    '    End If

                    Dim substituto As New VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO
                    'substituto.VLRUNTITEOCDACOCMC = ConsultaPrecoUnitarioItem(item.CODMER, relacao.CODFILEMP) ############ Alterado para consultar dentro da thread ############
                    substituto.QDEITEOCDACOCMC = relacao.QDEITEOCDACOCMC
                    substituto.NUMLNHITEOCDACOCMC = relacao.NUMLNHITEOCDACOCMC
                    substituto.CODMERSBT = item.CODMER
                    substituto.CODMER = relacao.CODMER
                    substituto.CODACOCMC = acao.CODACOCMC

                    ' Abre uma thread para cada Mercadoria, para consultar o preço.
                    ' A criação das threads foi necessario para reduzir o tempo total de consulta dos preços quando tem centenas de mercadorias.
                    tasks.Add(Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                        substituto.VLRUNTITEOCDACOCMC = boMovimentoQuinzenal.ConsultaPrecoUnitarioItemThread(item.CODMER, relacao.CODFILEMP, acaoCache.TIPACOMCD)
                                                                    End Sub
                                                                    )
                                                                )
                    listSubstituto.Add(substituto)
                Next

                Threading.Tasks.Task.WaitAll(tasks.ToArray()) ' aguarda que todas as consultas de preços sejam concluidas

                boRelacaoItemPrincipalItemSubstitutoAcaoComercial.SalvarItensSubs(listSubstituto, velhos)

            End If

            Return True

        End Function

        ''' <summary>
        ''' função para clique no tanto no check de itens via app quanto fornecedor unico
        ''' </summary>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function ViaAppFornecedorUnicoCheked(viaApp As Boolean, fornecedor As Boolean) As Tuple(Of List(Of VO.PoVendorSitesAllVO), List(Of VO.ApTermsVO))
            Try
                If viaApp Or fornecedor Then

                    'Dim retorno As Tuple(Of List(Of VO.PoVendorSitesAllVO), List(Of VO.ApTermsVO)) = New Tuple(Of List(Of VO.PoVendorSitesAllVO), List(Of VO.ApTermsVO))(CsnFrnIteRsu(8), AtuCbTerPgt())

                    'CsnFrnIteRsu(8)

                    'AtuCbTerPgt()
                    'Return retorno
                End If
                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Public Function abrirModalFornecedorUnico(objeto As String) As PartialViewResult
            Try

                Dim dadosPesquisa As New GridSettings(Of PoVendorSitesAllViewModel)
                dadosPesquisa.Filter = New PoVendorSitesAllViewModel()
                dadosPesquisa.Filter.RAZSOCEMPFRN = objeto
                Dim fornecedores As IEnumerable(Of PoVendorSitesAllViewModel)
                If (String.IsNullOrWhiteSpace(objeto)) Then
                    fornecedores = New List(Of PoVendorSitesAllViewModel)
                Else
                    fornecedores = Mapper.Map(Of List(Of VO.PoVendorSitesAllVO), List(Of PoVendorSitesAllViewModel))(VendorSitesAll.PsqFrnCsmUndNgc(8, dadosPesquisa.Filter.NUMCGCEMPFRN, dadosPesquisa.Filter.RAZSOCEMPFRN))
                End If

                Dim gridConf = New GridSettings(Of PoVendorSitesAllViewModel)(dadosPesquisa.Filter, fornecedores.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))
                gridConf.ResultID = "resultadosFrnUnico"
                gridConf.ActionName = "CsnFrnIteRsu"

                gridConf.TotalCount = fornecedores.Count
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                Return PartialView("~/Views/AcaoComercial/Orcamento/ModalPrincipal/_ModalBuscaFornecedorUnico.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        <HttpPost()>
        Public Function CsnFrnIteRsu(dadosPesquisa As GridSettings(Of PoVendorSitesAllViewModel)) As PartialViewResult

            Try
                If IsNothing(dadosPesquisa) Then
                    dadosPesquisa = New GridSettings(Of PoVendorSitesAllViewModel)
                    dadosPesquisa.Filter = New PoVendorSitesAllViewModel
                End If

                Dim resultados As List(Of PoVendorSitesAllViewModel) = Mapper.Map(Of List(Of VO.PoVendorSitesAllVO), List(Of PoVendorSitesAllViewModel))(VendorSitesAll.PsqFrnCsmUndNgc(8, dadosPesquisa.Filter.NUMCGCEMPFRN, dadosPesquisa.Filter.RAZSOCEMPFRN))
                'Dim mercadoriassViewModel As List(Of MercadoriaViewModel) = BuscaItensOrcamento(dadosPesquisa.CODMER, dadosPesquisa.DESMER, codOperacao, codFilial, patrocinador)

                Dim gridConf = PaginarEAtualizarGrid(resultados, dadosPesquisa.Page, dadosPesquisa.ListPaged)
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                gridConf.ResultID = "resultadosFrnUnico"
                gridConf.PaginationID = "resultadosFrnUnico"
                gridConf.ActionName = "CsnFrnIteRsu"

                Return PartialView("~/Views/AcaoComercial/Orcamento/ModalPrincipal/_ResultadosFornecedorUnico.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
            End Try
        End Function


        <HttpPost()>
        Public Function abrirModalFormaPgt(objeto As String) As PartialViewResult
            Try

                Dim aptermos As New ApTermsBO()

                Dim dadosPesquisa As New GridSettings(Of ApTermsViewModel)
                dadosPesquisa.Filter = New ApTermsViewModel()
                dadosPesquisa.Filter.DESCRIPTION = objeto

                Dim filtro = Mapper.Map(Of ApTermsViewModel, VO.ApTermsVO)(dadosPesquisa.Filter)

                Dim fornecedores As IEnumerable(Of ApTermsViewModel) = Mapper.Map(Of List(Of VO.ApTermsVO), List(Of ApTermsViewModel))(aptermos.LstTerPgt(filtro))

                Dim gridConf = New GridSettings(Of ApTermsViewModel)(dadosPesquisa.Filter, fornecedores.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))
                gridConf.ResultID = "resultadosFormPgt"
                gridConf.ActionName = "AtuCbTerPgt"

                gridConf.TotalCount = fornecedores.Count
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                Return PartialView("~/Views/AcaoComercial/Orcamento/ModalPrincipal/_ModalFormaPagamento.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function


        <HttpPost()>
        Public Function AtuCbTerPgt(dadosPesquisa As GridSettings(Of ApTermsViewModel)) As PartialViewResult
            Try
                Dim aptermos As New ApTermsBO()
                If IsNothing(dadosPesquisa) Then
                    dadosPesquisa = New GridSettings(Of ApTermsViewModel)
                    dadosPesquisa.Filter = New ApTermsViewModel
                End If

                Dim filtro = Mapper.Map(Of ApTermsViewModel, VO.ApTermsVO)(dadosPesquisa.Filter)

                Dim resultados As List(Of ApTermsViewModel) = Mapper.Map(Of List(Of VO.ApTermsVO), List(Of ApTermsViewModel))(aptermos.LstTerPgt(filtro))

                Dim gridConf = PaginarEAtualizarGrid(resultados, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                gridConf.ResultID = "resultadosFormPgt"
                gridConf.PaginationID = "resultadosFormPgt"
                gridConf.ActionName = "AtuCbTerPgt"

                Return PartialView("~/Views/AcaoComercial/Orcamento/ModalPrincipal/_ResultadoFormaPagamento.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        <HttpPost()>
        Public Function AbrirModarImport() As PartialViewResult
            Return PartialView("~/Views/AcaoComercial/Orcamento/ImportacaoItensSubstitutos/_ModalImport.vbhtml")
        End Function


        <HttpPost()>
        Public Function ImportarPlanilha(codOperacao As Integer, codFilial As Decimal, valorTotalItemOriginal As Decimal) As PartialViewResult
            Dim erros As List(Of String) = New List(Of String)
            Dim mercadoriasImportadas As New List(Of MercadoriaViewModel)
            Dim savedFileName As String = ""
            Try
                If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                    erros.Add("Não foi possivel abrir o arquivo!")
                End If
                For Each item As String In Request.Files

                    Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                    If (hpf.ContentLength = 0) Then
                        Continue For
                    End If
                    Dim dataDoArquivo = Date.Now
                    Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                    Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                    Dim index As Integer = nomeArquivo.IndexOf(".xls")
                    nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                    savedFileName = Path.Combine(caminho, nomeArquivo)
                    hpf.SaveAs(savedFileName)

                    mercadoriasImportadas = ImportarExel(savedFileName, erros, codOperacao, codFilial, valorTotalItemOriginal)
                Next

                Dim itensSubs = Session.Item("itensSub")
                If (Not IsNothing(itensSubs)) Then
                    mercadoriasImportadas.AddRange(itensSubs)
                End If
                Session("itensSub") = mercadoriasImportadas

                Return PartialView("~/Views/AcaoComercial/Orcamento/ImportacaoItensSubstitutos/_ModalErrosImportacao.vbhtml", erros)
            Catch ex As ArgumentException
                If (ex.HResult = -2147024809) Then
                    erros.Add("Não foi possível encontrar a coluna 'codigo' na planilha, favor verificar a mesma.")
                    Return PartialView("~/Views/AcaoComercial/Orcamento/ImportacaoItensSubstitutos/_ModalErrosImportacao.vbhtml", erros)
                End If
            Catch ex As System.IO.DirectoryNotFoundException
                erros.Add("Não foi possível salvar o arquivo no servidor pois não foi possível localizar uma parte do caminho, favor entrar em contato com o administrador do sistema.")
                Return PartialView("~/Views/AcaoComercial/Orcamento/ImportacaoItensSubstitutos/_ModalErrosImportacao.vbhtml", erros)
            Catch ex As Exception
                erros.Add(("Ocorreu um erro inesperado, Favor entrar em contato com o administrador do sistema.") + ex.Message)
                Return PartialView("~/Views/AcaoComercial/Orcamento/ImportacaoItensSubstitutos/_ModalErrosImportacao.vbhtml", erros)
            Finally
                If Not String.IsNullOrWhiteSpace(savedFileName) Then
                    My.Computer.FileSystem.DeleteFile(savedFileName)
                End If
            End Try
            Return Nothing
        End Function



        Private Function ImportarExel(path As String, ByRef erros As List(Of String), codOperacao As Integer, codFilial As Decimal, valorTotalItemOriginal As Decimal) As List(Of MercadoriaViewModel)

            Dim mercadoriasNovas As New List(Of MercadoriaViewModel)
            Dim acao As AcaoComercialViewModel = Session.Item("acaoModel")

            Dim mercadoriasSalvas As List(Of MercadoriaViewModel) = Session("itensSub")


            Dim erroImportExcel As String = String.Empty
            Dim mercadoriasExcel As List(Of VO.Mercadoria) = ListaExcel(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                erros.Add(erroImportExcel)
            End If


            If IsNothing(mercadoriasExcel) Or mercadoriasExcel.Count = 0 Then
                erros.Add("Este arquivo não possui uma relação de codigos de mercadorias para importar")
            ElseIf mercadoriasExcel.Count > 30000 Then
                erros.Add("Este arquivo possui mais de 30 mil codigos de mercadorias para importar")
            Else
                Dim operacao As TipoOperacaoViewModel = Mapper.Map(Of VO.OperacaoFiscal, TipoOperacaoViewModel)(boOperacao.ListT(New VO.OperacaoFiscal With {.CODOPEFSCDSNACOCMC = codOperacao}).FirstOrDefault)

                For Each linha In mercadoriasExcel
                    Dim erroLinha As String = "Linha " + linha.NUMPODVLD.GetValueOrDefault().ToString() + ":"
                    Dim temErro As Boolean = False

                    If (linha.CODMER Is Nothing) Then
                        temErro = True
                        erroLinha = erroLinha + (" não possui código da mercadoria.")
                    End If
                    If temErro Then
                        erros.Add(erroLinha)
                        Continue For
                    End If


                    If Not mercadoriasSalvas.Where(Function(f) f.CODMER = linha.CODMER).FirstOrDefault() Is Nothing Then
                        erroLinha = String.Format("{0} {1}: Mercadoria já adicionada..", erroLinha, linha.CODMER)
                        erros.Add(erroLinha)
                        Continue For
                    End If

                    'Dim novaMercadoria As VO.Mercadoria = New VO.Mercadoria With {.CODMER = linha.CODMER, .CODEMP = linha.CODEMP}

                    Dim mercadoria As MercadoriaViewModel
                    If operacao.INDTIPOPEDSNACOCMC = 1 Then
                        If codFilial <> 0 Then
                            mercadoria = CnsIte(New With {.idMerc = linha.CODMER, .desMerc = "", .codOperacao = codOperacao, .codFilial = codFilial, .patrocinador = acao.TIPACOMCD}, acao, operacao).FirstOrDefault()
                        Else
                            Warning("Selecione uma filial!")
                            Return Nothing
                        End If
                    Else
                        If operacao.INDTIPOPEREQVGM = 1 Or operacao.INDTIPOPEREQEVT = 1 Or operacao.INDTIPOPESLCPGT = 1 Then
                            mercadoria = CsnIteGstGrl(New With {.idMerc = linha.CODMER, .desMerc = "", .codOperacao = codOperacao, .codFilial = codFilial, .patrocinador = acao.TIPACOMCD}, operacao).FirstOrDefault()
                        Else
                            mercadoria = PsqIteCsm(New With {.idMerc = linha.CODMER, .desMerc = "", .codOperacao = codOperacao, .codFilial = codFilial, .patrocinador = acao.TIPACOMCD}, operacao).FirstOrDefault()
                        End If
                    End If

                    If (IsNothing(mercadoria)) Then
                        temErro = True
                        erroLinha = String.Format("{0} {1}: não encontrou mercadoria ou mercadoria está desativada.", erroLinha, linha.CODMER)
                    Else
                        mercadoria.ValorUnitario = ConsultaPrecoUnitarioItem(mercadoria.CODMER, codFilial)
                        If (mercadoria.ValorUnitario.Value > 0) Then
                            mercadoria.quantdadeSubstitutos = System.Math.Floor(valorTotalItemOriginal / mercadoria.ValorUnitario.Value)
                        Else
                            mercadoria.quantdadeSubstitutos = 0
                        End If
                        mercadoria.valorTotalSubs = mercadoria.quantdadeSubstitutos * mercadoria.ValorUnitario.Value

                        If (mercadoria.quantdadeSubstitutos <= 0) Then
                            temErro = True
                            erroLinha = String.Format("{0} {1}: não encontrou mercadoria ou mercadoria está desativada.", erroLinha, linha.CODMER)
                        End If
                        mercadoria.CODEMP = codFilial

                    End If

                    'Dim boMercadoria As New MercadoriaBO
                    'Dim mercadoria = boMercadoria.BuscaMercadorias(novaMercadoria).FirstOrDefault


                    If Not temErro Then
                        If (mercadoriasNovas.Where(Function(x) x.CODMER = mercadoria.CODMER And x.CODEMP = mercadoria.CODEMP).Count > 0) Then
                            temErro = True
                            erroLinha = String.Format("{0} {1}:  não foi importada pois está duplicada.", erroLinha, linha.CODMER)
                        End If
                    End If

                    If temErro Then
                        erros.Add(erroLinha)
                    Else
                        mercadoriasNovas.Add(mercadoria)
                    End If

                Next
            End If
            Return mercadoriasNovas
        End Function


        Private Function ListaExcel(path As String, ByRef erros As String) As List(Of VO.Mercadoria)
            Dim mercadoriasImportadas As New List(Of VO.Mercadoria)

            Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
            Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

            Dim arg() As String = {vbCrLf, vbLf}
            Dim NumLinha As Int32 = 0
            For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)

                Dim alinha As String() = linha.Split({vbTab}, StringSplitOptions.None)

                If (NumLinha = 0) Then 'Aba da planilha
                    NumLinha += 1
                    Continue For
                End If

                If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                    NumLinha += 1
                    Continue For
                End If

                If (alinha.Count <> 1) Then
                    erros = String.Format("Erro Linha {0}: O excel deve conter 1 coluna de dados sendo as colunas ""Codigo da Mercadoria""", NumLinha)
                    Return Nothing
                End If

                If (NumLinha = 1) Then ' cabeçalhos do excel
                    If (alinha(0).Replace("""", "") <> "Codigo da Mercadoria") Then
                        erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Mercadoria""", NumLinha)
                        Return Nothing
                    End If
                Else ' Linhas de dados
                    Dim CODMER As Decimal
                    If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODMER)) Then
                        erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo de Mercadoria"" válido", NumLinha, alinha(0))
                        Return Nothing
                    End If
                    Dim mercadoria As New VO.Mercadoria
                    mercadoria.CODMER = CODMER
                    mercadoria.NUMPODVLD = NumLinha - 1
                    mercadoriasImportadas.Add(mercadoria)
                End If
                NumLinha += 1
            Next

            Return mercadoriasImportadas
        End Function


        <HttpPost()>
        Public Function AbrirModarImportItemOrcamento(objeto As RelacaoAcaoComercialPorItemOrcamentoViewModel) As PartialViewResult
            Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
            If (IsNothing(objeto)) Then
                objeto = New RelacaoAcaoComercialPorItemOrcamentoViewModel
            End If

            If (acaoCache Is Nothing) Then
                Warning("Esta tela esta a muito tempo aberta e as informações podem estar desatualizadas, atualize a pagina para prosseguir")
                Return Nothing
            End If

            If (String.IsNullOrWhiteSpace(acaoCache.NOMACOCMC)) Then
                Warning("Entre com o nome da ação comercial antes de prosseguir!")
                Return Nothing
            End If
            If (IsNothing(acaoCache.DATINIACOCMC) OrElse IsNothing(acaoCache.DATFIMACOCMC)) Then
                Warning("Escolha um interalo válido para a ação.")
                Return Nothing
            ElseIf (acaoCache.DATINIACOCMC > acaoCache.DATFIMACOCMC) Then
                Warning("Escolha um interalo válido para a ação.")
                Return Nothing
            End If

            objeto.AcaoComercial = acaoCache

            CarregaDropsItensOrcamento(objeto)

            Dim operacoesNaoImportar As List(Of Integer) = {10, 13, 15, 16, 18, 6, 7, 17}.ToList()

            ViewBag.operacao = DirectCast(ViewBag.operacao, List(Of TipoOperacaoViewModel)).Where(Function(f) Not operacoesNaoImportar.Any(Function(k) k = f.CODOPEFSCDSNACOCMC)).ToList


            'colocar o valor do centro de custo diretamente no objeto será temporario,
            'o certo será usar uma variavel global setada durante a fase de login do usuario.
            objeto.CODCENCST = boRelacaoAcaoComercialPorItemOrcamento.BuscaCentrodeCusto(objeto.AcaoComercial.NOMUSRRCF)

            Dim centroCusto As JsonResult = BuscaCentroCusto(objeto.CODCENCST, String.Empty)
            Dim dropCentroCusto As New List(Of CentroCustoViewModel)
            For Each item In centroCusto.Data
                Dim novoCentroCusto As New CentroCustoViewModel
                novoCentroCusto.CODCENCST = item.CODCENCST
                novoCentroCusto.DESCENCST = item.DESCENCST

                dropCentroCusto.Add(novoCentroCusto)
            Next

            ViewBag.dropCentroCusto = dropCentroCusto

            'Dim mercadoria As Object = New With {.icoditegstgr = 0,
            '                                             .sdesitegstgr = "",
            '                                             .iorgidt = 8,
            '                                             .icodatr = 1,
            '                                             .sdesatr = "AC"}

            'Dim ListItemOrcamento As New List(Of MercadoriaViewModel)

            ''TODO: Comentar para realizar testes sem conectar no banco de dados D01A
            ''ListItemOrcamento = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.ConsultaItemGastoGeral(mercadoria))

            'Dim novaListItemOrcamento As New List(Of RelacaoAcaoComercialPorItemOrcamentoViewModel)
            'For Each item In ListItemOrcamento
            '    Dim novoItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoViewModel
            '    novoItemOrcamento.CODMERITEMDROP = item.CODMER
            '    novoItemOrcamento.NOMITEMORCDROP = item.DESMER
            '    novaListItemOrcamento.Add(novoItemOrcamento)
            'Next
            'ViewBag.dropItemOrcamento = novaListItemOrcamento


            If IsNothing(objeto.DATUTZITEACOCMC) Then
                objeto.DATUTZITEACOCMC = Date.Now
            End If
            Return PartialView("~/Views/AcaoComercial/Orcamento/ImportacaoItensOrcamento/_ModalImport.vbhtml", objeto)
        End Function

        <HttpPost()>
        Public Function ImportarPlanilhaItemOrcamento(mercadoria As RelacaoAcaoComercialPorItemOrcamentoViewModel) As PartialViewResult

            'Verifica tipo de operação
            If IsNothing(mercadoria.INDTIPOPEDSNACOCMC) Then
                Warning("Escolha um tipo de operação")
                Return Nothing
            End If

            ''Verifica filial
            'If mercadoria.INDTIPOPEDSNACOCMC = 1 And IsNothing(mercadoria.CODFILEMP) Then
            '    Warning("Escolha uma filial")
            '    Return Nothing
            'End If

            'Verificando Forma de Utilizacao
            If String.IsNullOrWhiteSpace(mercadoria.DESFRMUTZITEACOCMC) Then
                Warning("Digite a forma de utilização do item")
                Return Nothing
            End If


            Dim operacao = boOperacao.ListT(New VO.OperacaoFiscal With {.CODOPEFSCDSNACOCMC = mercadoria.INDTIPOPEDSNACOCMC}).FirstOrDefault
            'Verifica BU
            If operacao.INDUNDESRNGCACOCMC = 1 And IsNothing(mercadoria.CODUNDESRNGCOPEDSN) Then
                Warning("Escolha um BU")
                Return Nothing
            End If

            Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
            If operacao.INDGRCREQAUTITERSU = 1 Then
                If GrcIdtIteRsu(mercadoria.CODCENCST, mercadoria, acaoCache) = 0 Then
                    Return Nothing
                End If
            End If



            Dim erros As List(Of String) = New List(Of String)
            Dim mercadoriasImportadas As New List(Of MercadoriaViewModel)

            Dim savedFileName As String = ""
            Try

                If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                    Warning("Selecione o arquivo para importa-lo!")
                    Return Nothing
                End If


                For Each item As String In Request.Files

                    Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                    If (hpf.ContentLength = 0) Then
                        Continue For
                    End If
                    Dim dataDoArquivo = Date.Now
                    Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                    Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                    Dim index As Integer = nomeArquivo.IndexOf(".xls")
                    nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                    savedFileName = Path.Combine(caminho, nomeArquivo)
                    hpf.SaveAs(savedFileName)

                    boRelacaoItemPrincipalItemSubstitutoAcaoComercial.ImportarExelItemOrcamento(mercadoria.INDTIPOPEDSNACOCMC, mercadoria.CODUNDESRNGCOPEDSN, mercadoria.CODCENCST, mercadoria.DESFRMUTZITEACOCMC, acaoCache, savedFileName, erros)

                    'mercadoriasImportadas = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boRelacaoItemPrincipalItemSubstitutoAcaoComercial.ImportarExelItemOrcamento(savedFileName, erros))
                Next

                Return PartialView("~/Views/AcaoComercial/Orcamento/ImportacaoItensOrcamento/_ModalErrosImportacao.vbhtml", erros)




                'Return fecharModalItens(mercadoria, acaoCache.TIPACOMCD)

                '


                'Catch ex As ArgumentException
                '    If (ex.HResult = -2147024809) Then
                '        erros.Add("Não foi possível encontrar a coluna 'codigo' na planilha, favor verificar a mesma.")
                '        Return PartialView("~/Views/AcaoComercial/Orcamento/ImportacaoItensOrcamento/_ModalErrosImportacao.vbhtml", erros)
                '    End If
            Catch ex As System.IO.DirectoryNotFoundException
                erros.Add("Não foi possível salvar o arquivo no servidor pois não foi possível localizar uma parte do caminho, favor entrar em contato com o administrador do sistema.")
                Return PartialView("~/Views/AcaoComercial/Orcamento/ImportacaoItensOrcamento/_ModalErrosImportacao.vbhtml", erros)
            Catch ex As Exception
                erros.Add(("Ocorreu um erro inesperado, Favor entrar em contato com o administrador do sistema.") + ex.Message)
                Return PartialView("~/Views/AcaoComercial/Orcamento/ImportacaoItensOrcamento/_ModalErrosImportacao.vbhtml", erros)
            Finally

                Try
                    If Not String.IsNullOrWhiteSpace(savedFileName) Then
                        My.Computer.FileSystem.DeleteFile(savedFileName)
                    End If
                Catch ex As Exception
                End Try

            End Try

            Return Nothing

        End Function

        <HttpPost()>
        Public Sub limparCacheFiltroAuxItensSub()
            If (Not IsNothing(Session.Item("filtroAuxItensSub"))) Then
                Session.Remove("filtroAuxItensSub")
            End If
        End Sub
        <HttpPost()>
        Public Sub limparCacheItensSub()
            If (Not IsNothing(Session.Item("itensSub"))) Then
                Session.Remove("itensSub")
            End If
            If (Not IsNothing(Session.Item("listaTempItensSelecionados"))) Then
                Session.Remove("listaTempItensSelecionados")
            End If
        End Sub

        <HttpPost()>
        Public Sub limparCacheListaTempItensSelecionados()
            If (Not IsNothing(Session.Item("listaTempItensSelecionados"))) Then
                Session.Remove("listaTempItensSelecionados")
            End If
        End Sub

        <HttpPost()>
        Public Function validaTrocarOperacaoFilial(Optional objeto As RelacaoAcaoComercialPorItemOrcamentoViewModel = Nothing) As Boolean

            Dim salvos As List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)
            salvos = Nothing
            If (Not IsNothing(objeto)) Then
                If (Not IsNothing(objeto.CODACOCMC) AndAlso Not IsNothing(objeto.CODMER) AndAlso Not IsNothing(objeto.NUMLNHITEOCDACOCMC)) Then
                    Dim filtroSubstituto As New VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO
                    filtroSubstituto.CODACOCMC = objeto.CODACOCMC
                    filtroSubstituto.CODMER = objeto.CODMER
                    filtroSubstituto.NUMLNHITEOCDACOCMC = objeto.NUMLNHITEOCDACOCMC
                    salvos = boRelacaoItemPrincipalItemSubstitutoAcaoComercial.ListT(filtroSubstituto)
                End If
            End If

            Dim itemcache = Session.Item("itensSub")
            If ((IsNothing(itemcache) OrElse itemcache.count = 0) And (IsNothing(salvos) OrElse salvos.Count = 0)) Then
                Return True
            End If
            Return False
        End Function

        <HttpPost()>
        Public Function LimpaItensSubs() As PartialViewResult
            Dim itemcache = Session.Item("itensSub")
            If (Not IsNothing(itemcache)) Then
                Session.Remove("itensSub")
            End If
            Session.Remove("listaTempItensSelecionados")

            Dim gridConf = GridSettings(Of MercadoriaViewModel).EmptyGrid("gridItensSubstitutosSelecionados", "recarregaItensSubSelecionados", "ItensOrcamento")

            gridConf.ResultID = "gridItensSubstitutosSelecionados"
            gridConf.TotalCount = 0
            gridConf.ActionName = "recarregaItensSubSelecionados"
            gridConf.PaginationID = "gridItensSubstitutosSelecionados"

            Return PartialView("~/Views/AcaoComercial/Orcamento/ItensSubstitutos/_ResultadoItensSubstitutosSelecionados.vbhtml", gridConf)
        End Function

        ''' <summary>
        ''' Deleta um item e seus substitutos da ação
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Function Delete(ByVal id As String, dadosPesquisa As GridSettings(Of RelacaoAcaoComercialPorItemOrcamentoViewModel)) As PartialViewResult
            Try
                Dim dados As IEnumerable(Of String) = Split(id, ":")

                Dim filtro As New VO.RelacaoAcaoComercialPorItemOrcamento()
                filtro.CODACOCMC = CDec(dados(0))
                filtro.NUMLNHITEOCDACOCMC = CDec(dados(1))

                Dim substituto As New VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO
                substituto.CODACOCMC = CDec(dados(0))
                substituto.NUMLNHITEOCDACOCMC = CDec(dados(1))

                boRelacaoAcaoComercialPorItemOrcamento.DeletarItem(filtro, substituto)

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoCache.ItensOrcamento = acaoCache.ItensOrcamento.Where(Function(f) f.NUMLNHITEOCDACOCMC <> filtro.NUMLNHITEOCDACOCMC).ToList
                acaoCache.ItensSubstituidos = acaoCache.ItensSubstituidos.Where(Function(f) f.NUMLNHITEOCDACOCMC <> substituto.NUMLNHITEOCDACOCMC).ToList

                Dim novoGrid = PaginarEAtualizarGrid(acaoCache.ItensOrcamento, dadosPesquisa)
                novoGrid.TotalCount = acaoCache.ItensOrcamento.Count
                novoGrid.ResultID = "divWidgetOrcamento"
                novoGrid.PaginationID = "gridItensOrcamento"
                novoGrid.ActionName = "AtualizarGridItensOrcamento"
                novoGrid.ControllerName = "AcaoComercial"
                acaoCache.StateGridItensOrcamento = novoGrid


                calculaItemFolhaPagamento(acaoCache)

                Util.AtuSldOcd(acaoCache) 'atualiza o saldo do orçamento 
                Util.CalVlrTot(acaoCache) 'calcula vlr total do brinde e do orçamento

                Session("acaoModel") = acaoCache

                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                ViewBag.btnIsrItemVisible = True
                Return PartialView("~/Views/AcaoComercial/Orcamento/_WidgetOrcamento.vbhtml", acaoCache)
                'Return PartialView("~/Views/AcaoComercial/Orcamento/_GridOrcamento.vbhtml", acaoCache.StateGridItensOrcamento)
            Catch ex As Exception
                Danger(ex)
                Return PartialView("~/Views/AcaoComercial/Orcamento/_WidgetOrcamento.vbhtml")
            End Try
        End Function

        <HttpPost()>
        Public Function RecarregaImportados(codFilial As Integer, valorTotalItemOriginal As Decimal) As PartialViewResult
            Try

                Dim itensSubs As List(Of MercadoriaViewModel) = Session.Item("itensSub")
                If Not IsNothing(itensSubs) AndAlso itensSubs.Count > 0 Then

                    For Each merc In itensSubs
                        Dim filtro As New VO.Mercadoria With {.CODMER = merc.CODMER}

                        Dim itensParaAdd = Mapper.Map(Of VO.Mercadoria, MercadoriaViewModel)(boMercadoria.ListT(filtro).FirstOrDefault)

                        merc.ValorUnitario = ConsultaPrecoUnitarioItem(merc.CODMER, codFilial)

                        If Not (merc.ValorUnitario = 0) Then
                            merc.quantdadeSubstitutos = System.Math.Floor(valorTotalItemOriginal / merc.ValorUnitario.Value)
                            merc.valorTotalSubs = merc.quantdadeSubstitutos * merc.ValorUnitario.Value
                        End If
                    Next
                    Session("listaTempItensSelecionados") = itensSubs

                    Dim gridConf = GridSettings(Of MercadoriaViewModel).EmptyGrid("gridItensSubstitutosSelecionados", "SelecionarItensSubstitutos", "ItensOrcamento")
                    gridConf = PaginarEAtualizarGrid(itensSubs, gridConf)
                    gridConf.TotalCount = itensSubs.Count
                    gridConf.ResultID = "gridItensSubstitutosSelecionados"
                    gridConf.ActionName = "recarregaItensSubSelecionados"
                    gridConf.PaginationID = "gridItensSubstitutosSelecionados"

                    Return PartialView("~/Views/AcaoComercial/Orcamento/ItensSubstitutos/_ResultadoItensSubstitutosSelecionados.vbhtml", gridConf)
                End If
                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' função que abre modal da tela de consulta gasto
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function ModalConsultaGasto(objeto As RelacaoAcaoComercialPorItemOrcamentoViewModel) As PartialViewResult
            Dim acao As AcaoComercialViewModel = New AcaoComercialViewModel

            acao = Mapper.Map(Of VO.AcaoComercial, AcaoComercialViewModel)(bo.BuscaAcaoPorId(objeto.CODACOCMC))
            'Dim tipOper = boOperacao.SelectT(New VO.OperacaoFiscal With {.CODOPEFSCDSNACOCMC = objeto.INDTIPOPEDSNACOCMC})

            ViewBag.CodTipoOperacao = objeto.CODOPEFSCDSNACOCMC
            ViewBag.NomAcaoGasto = acao.NOMACOCMC
            ViewBag.CodItemGasto = objeto.NUMLNHITEOCDACOCMC
            ViewBag.NomItemGasto = objeto.CODITEOCDACOCMC
            ViewBag.TipoOperacaoGasto = objeto.DESOPEFSCDSNACOCMC
            ViewBag.ValorTotal = CDec(objeto.VLRUNTITEOCDACOCMC * objeto.QDEITEOCDACOCMC).ToString("#,##0.0000")


            Dim boGasto = New RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemBO()

            Dim mercadoriassViewModel As List(Of RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel) = Mapper.Map(Of List(Of VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem), List(Of RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel))(boGasto.consultaGastoItem(objeto.CODACOCMC, objeto.NUMLNHITEOCDACOCMC))

            Dim gridConf = PaginarEAtualizarGrid(mercadoriassViewModel, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT}, New RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel With {.CODACOCMC = objeto.CODACOCMC, .NUMLNHITEOCDACOCMC = objeto.NUMLNHITEOCDACOCMC})

            Dim gasto As Decimal = 0
            Dim estorno As Decimal = 0
            For Each item In mercadoriassViewModel
                If item.TIPUTLZ = "Gasto" Then
                    gasto = gasto + item.VlrUtz
                Else
                    estorno = estorno + item.VlrUtz
                End If
            Next
            ViewBag.TotalGasto = gasto.ToString("#,##0.00")
            ViewBag.TotalEstorno = estorno.ToString("#,##0.00")
            ViewBag.TotalUtz = (gasto - estorno).ToString("#,##0.00")

            gridConf.ResultID = "gridItensGastos"
            gridConf.PaginationID = "gridItensGastos"
            gridConf.ActionName = "PesquisaGridItemGasto"
            gridConf.TotalCount = mercadoriassViewModel.Count

            Return PartialView("~/Views/AcaoComercial/Orcamento/ConsultaGasto/_ModalConsultaGasto.vbhtml", gridConf)

        End Function


        ''' <summary>
        ''' Função que abre modal de adicionar gasto
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function ModalAdicionarGasto(objeto As RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel) As PartialViewResult
            'Dim retorno As RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel = New RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel
            objeto.INDTIPISRUTZITEOCD = 0


            Return PartialView("~/Views/AcaoComercial/Orcamento/ConsultaGasto/_ModalEditGasto.vbhtml", objeto)
        End Function

        ''' <summary>
        ''' Função que abre modal de adicionar gasto
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function SalvarUtilizacaoGasto(objeto As RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim vo As VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem = Mapper.Map(Of RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel, VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem)(objeto)
                vo.CODFNC = ControleAcesso.CodFnc()
                vo.DATGRCRLC = Date.Now

                Dim boGasto = New RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemBO()
                boGasto.RealizaLancamentoUtilizacaoDoItem(vo)

                Dim filtro As RelacaoAcaoComercialPorItemOrcamentoViewModel = acaoCache.ItensOrcamento.Where(Function(f) f.NUMLNHITEOCDACOCMC = objeto.NUMLNHITEOCDACOCMC).FirstOrDefault()
                Return ModalConsultaGasto(filtro)

            Catch ex As MappedWarnException
                Warning(ex.Message)
                Return PartialView("~/Views/AcaoComercial/Orcamento/ConsultaGasto/_ModalEditGasto.vbhtml", objeto)
            Catch ex As Exception
                Danger(ex)
                Return PartialView("~/Views/AcaoComercial/Orcamento/ConsultaGasto/_ModalEditGasto.vbhtml", objeto)
            End Try
        End Function

        ''' <summary>
        ''' função para buscar e paginar tabela de itens na modal de consulta gasto
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function PesquisaGridItemGasto(dadosPesquisa As GridSettings(Of RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel)) As PartialViewResult

            Dim boGasto = New RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemBO()

            Dim mercadoriassViewModel As List(Of RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel) = Mapper.Map(Of List(Of VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem), List(Of RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel))(boGasto.consultaGastoItem(dadosPesquisa.Filter.CODACOCMC, dadosPesquisa.Filter.NUMLNHITEOCDACOCMC))

            Dim gridConf = PaginarEAtualizarGrid(mercadoriassViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)

            gridConf.ResultID = "gridItensGastos"
            gridConf.PaginationID = "gridItensGastos"
            gridConf.ActionName = "PesquisaGridItemGasto"
            gridConf.Filter = dadosPesquisa.Filter
            gridConf.TotalCount = mercadoriassViewModel.Count

            Return PartialView("~/Views/AcaoComercial/Orcamento/ConsultaGasto/_ResultConsultaGasto.vbhtml", gridConf)

        End Function

        ''' <summary>
        ''' função que valida se ação deve ou nao ter cnal de venda
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <HttpPost()>
        Public Function AtualizarCanalDeVenda()

            Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

            Dim operacao As New VO.OperacaoFiscal
            operacao.INDUNDESRNGCACOCMC = 1


            'If Not IsNothing(codOperacao) Then
            '    Dim operacoes As List(Of VO.OperacaoFiscal) = boOperacao.ListT(operacao)

            '    For Each op In operacoes
            '        If (op.CODOPEFSCDSNACOCMC = codOperacao) Then
            '            Return True
            '        End If
            '    Next

            'Else
            Dim itens = acaoCache.ItensOrcamento

            Dim operacoes As List(Of VO.OperacaoFiscal) = boOperacao.ListT(operacao)

            For Each item In itens
                If operacoes.Where(Function(f) f.CODOPEFSCDSNACOCMC = item.CODOPEFSCDSNACOCMC).Count > 0 Then
                    Return True
                End If
            Next
            'End If

            Return False

        End Function



        <HttpGet()>
        Public Function AtualizaWidgetOrcamento() As PartialViewResult
            Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

            acaoCache.ItensOrcamento = Mapper.Map(Of List(Of VO.RelacaoAcaoComercialPorItemOrcamento), List(Of RelacaoAcaoComercialPorItemOrcamentoViewModel))(boRelacaoAcaoComercialPorItemOrcamento.BuscarItensOrcamentoDaAcao(acaoCache.CODACOCMC))
            acaoCache.StateGridItensOrcamento = PaginarEAtualizarGrid(acaoCache.ItensOrcamento, acaoCache.StateGridItensOrcamento)

            If (acaoCache.CODSTAAPVACOCMC <> 1) Then
                ViewBag.btnGravarVisible = False
                If (acaoCache.CODSTAAPVACOCMC = 5) Then
                    ViewBag.btnGasto = True
                End If
            End If
            If acaoCache.CODSTAAPVACOCMC = 1 Then
                ViewBag.btnIsrItemVisible = True
            Else
                ViewBag.btnIsrItemVisible = False
            End If
            Return PartialView("~/Views/AcaoComercial/Orcamento/_WidgetOrcamento.vbhtml", acaoCache)

        End Function


        <HttpGet()>
        Public Function GetDescricaoFormaPagamento(ByVal id As Integer) As String
            Try
                Dim aptermos As New ApTermsBO()
                Dim retorno As VO.ApTermsVO = aptermos.SelecionarPorId(id)
                'Dim retorno As ApTermsViewModel = Mapper.Map(Of VO.ApTermsVO, ApTermsViewModel)(aptermos.SelecionarPorId(id))
                Return retorno.DESCRIPTION

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        <HttpGet()>
        Public Function GetInfoFornecedorUnico(ByVal id As Integer) As JsonResult
            Try
                Dim fornecedor As VO.PoVendorSitesAllVO = VendorSitesAll.PsqFrnCsm(id)

                Dim retorno As Object = New With {.RAZSOCEMPFRN = fornecedor.RAZSOCEMPFRN, .NUMCGCEMPFRN = fornecedor.NUMCGCEMPFRN}

                Return Json(retorno, JsonRequestBehavior.AllowGet)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function



    End Class
End Namespace