﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers

Namespace Controllers

    <Authorize(Roles:="12,17,14")>
    Public Class PedidoBonificacaoController
        Inherits AbstractController

        Private boFornecedores As New RelacaoAcaoComercialxFornecedoresBO()
        Private boPedidoBonificacao As New RelacaoAcaoComercialXPedidoBonificadoBO()
        'Property Cache As ICacheProvider = New DefaultCacheProvider()

        ''' <summary>
        ''' Abre o modal de inserir pedido bonificacao
        ''' </summary>
        ''' <param name="codAcao"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function InserirPedidoBonificacao(Optional codAcao As Decimal? = Nothing) As PartialViewResult

            Try
                ViewBag.filtroFornecedorBonificacao = Constantes.Fornecedores
                ViewBag.filtroPedidos = New List(Of SelectListItem)()
                ViewBag.filtroPedidos.Add(New SelectListItem() With {.Text = "Fornecedor", .Value = 1})
                ViewBag.filtroPedidos.Add(New SelectListItem() With {.Text = "Número do Pedido", .Value = 2})

                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                Dim pedidosBonificacao As New List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)
                pedidosBonificacao = acaoModel.Bonificacoes.ListCopy()
                ViewBag.GridPedidosBonificacao = GetGridPedidosBonificacoesModal()
                ViewBag.GridPedidosBonificacao.ListPaged = PaginarLista(pedidosBonificacao)
                ViewBag.GridPedidosBonificacao.TotalCount = pedidosBonificacao.Count
                Session("listaTempPedidosBonificacao") = pedidosBonificacao
                Session("listaTempPedidosCompra") = New List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)

                ViewBag.GridPedidosCompra = GetGridPedidosCompraModal()

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Bonificacao/_ModalInserirPedidoBonificacao.vbhtml", New RelacaoAcaoComercialXPedidoBonificadoViewModel)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try


        End Function

        ''' <summary>
        ''' Remover pedido do grid de pedidos da tela de acao
        ''' </summary>
        ''' <param name="numPed"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RemoverPedido(ByVal numPed As Decimal) As PartialViewResult
            Try
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoModel.Bonificacoes = acaoModel.Bonificacoes.Where(Function(f) f.NUMPEDCMP <> numPed).ToList()

                Dim gridConf = GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel).EmptyGrid("gridBonificacoes", "AtualizarGridBonificacoes")
                gridConf.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                gridConf.ListPaged = PaginarLista(acaoModel.Bonificacoes, gridConf.Page)
                gridConf.TotalCount = acaoModel.Bonificacoes.Count

                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                Return PartialView("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridBonificacao.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Confirmacao para exclusao de pedido do grid de pedidos da tela de acao
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function ConfirmaRemoverPedido(ByVal objeto As Decimal) As PartialViewResult
            Try
                Dim modelConfirmacao = New ModalConfirmacaoViewModel(objeto)
                modelConfirmacao.Mensagem = "Tem certeza que deseja excluir o pedido bonificação da ação?"
                modelConfirmacao.BtnSimAcao = "removePedido"
                Return PartialView("~/Views/Shared/_ModalConfirmacaoGenerico.vbhtml", modelConfirmacao)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Filtrar pedidos de compra de acordo com fornecedor ou numero do pedido
        ''' </summary>
        ''' <param name="codFrn"></param>
        ''' <param name="numPed"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function FiltrarPedidosCompra(Optional ByVal codFrn As Decimal? = Nothing, Optional ByVal numPed As Decimal? = Nothing) As PartialViewResult
            Try
                Dim pedidosCompra = boPedidoBonificacao.ListarPedidosDeCompra(codFrn, numPed)

                If pedidosCompra Is Nothing Or pedidosCompra.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    ViewBag.GridPedidosCompra = GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel).EmptyGrid()
                    Return PartialView("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridPedidosCompraModal.vbhtml", ViewBag.GridPedidosCompra)
                End If

                Dim pedidosCompraModel = Mapper.Map(Of List(Of VO.RelacaoAcaoComercialXPedidoBonificado), List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel))(pedidosCompra)
                Session("listaTempPedidosCompra") = pedidosCompraModel

                Dim gridConf = GetGridPedidosCompraModal()
                gridConf.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT5
                gridConf.ListPaged = PaginarLista(pedidosCompraModel, gridConf.Page)
                gridConf.TotalCount = pedidosCompraModel.Count
                Return PartialView("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridPedidosCompraModal.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Selecionar pedidos checkados e adicionar na lista de pedidos selecionados
        ''' </summary>
        ''' <param name="itensCheckados"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function SelecionarPedidos(ByVal itensCheckados As List(Of Decimal)) As PartialViewResult
            Try
                Dim listaTempPedidosBonificacao As List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel) = Session.Item("listaTempPedidosBonificacao")
                Dim listaTempPedidosCompra As List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel) = Session.Item("listaTempPedidosCompra")

                'Verifica se o pedido ja foi inserido
                For Each item In itensCheckados
                    If PedidoExistente(listaTempPedidosBonificacao, item) Then
                        Warning(Constantes.PEDIDO_EXISTENTE)
                        Return Nothing
                    End If
                Next

                Dim pedidosParaAdd = listaTempPedidosCompra.Where(Function(f) itensCheckados.Contains(f.NUMPEDCMP)).ToList()
                listaTempPedidosBonificacao.AddRange(pedidosParaAdd)
                Session("listaTempPedidosBonificacao") = listaTempPedidosBonificacao

                Dim gridConf = GetGridPedidosBonificacoesModal()
                gridConf.ListPaged = PaginarLista(listaTempPedidosBonificacao)
                gridConf.TotalCount = listaTempPedidosBonificacao.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridBonificacaoModal.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Remover pedidos checkados da lista de pedidos selecionados
        ''' </summary>
        ''' <param name="itensCheckados"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RemoverPedidos(ByVal itensCheckados As List(Of Decimal)) As PartialViewResult
            Try
                Dim listaTempPedidosBonificacao As List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel) = Session.Item("listaTempPedidosBonificacao")
                listaTempPedidosBonificacao = listaTempPedidosBonificacao.Where(Function(f) Not itensCheckados.Contains(f.NUMPEDCMP)).ToList()
                Session("listaTempPedidosBonificacao") = listaTempPedidosBonificacao

                Dim gridConf = GetGridPedidosBonificacoesModal()
                gridConf.ListPaged = PaginarLista(listaTempPedidosBonificacao)
                gridConf.TotalCount = listaTempPedidosBonificacao.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridBonificacaoModal.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Concluir adicao e edicao de pedidos selecionados
        ''' Valida se todos os pedidos tem valor utilizado
        ''' </summary>
        ''' <returns></returns>
        Public Function ValidarPedidosBonificacao() As PartialViewResult
            Try
                Dim listaTempPedidosBonificacao As List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel) = Session.Item("listaTempPedidosBonificacao")

                If (listaTempPedidosBonificacao.Find(Function(f) f.VLRUTZACOCMC = Nothing) IsNot Nothing) Then
                    Warning(Constantes.INFORME_VALOR_UTILIZADO_PEDIDO)
                    Return Nothing
                End If

                If (listaTempPedidosBonificacao.Find(Function(f) f.VLRUTZACOCMC > f.VERBA_DISPONIVEL) IsNot Nothing) Then
                    Warning(Constantes.VALOR_UTILIZADO_MAIOR_QUE_DISPONIVEL)
                    Return Nothing
                End If

                Dim gridConf = GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel).EmptyGrid("gridBonificacoes", "AtualizarGridBonificacoes")
                gridConf.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                gridConf.ListPaged = PaginarLista(listaTempPedidosBonificacao, gridConf.Page)
                gridConf.TotalCount = listaTempPedidosBonificacao.Count

                'Salva no cache e exclui lista temporaria do cache
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoModel.Bonificacoes = listaTempPedidosBonificacao

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridBonificacao.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Atualiza o valor do atributo Valor Utilizado de um pedido selecionado
        ''' </summary>
        <HttpPost()>
        Public Sub AtualizaValorUlitizadoCampo(ByVal valores As List(Of Decimal))
            Try
                Dim numPedido = valores.FirstOrDefault
                Dim valorUtilizado = valores.LastOrDefault

                Dim listaTempPedidosBonificacao As List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel) = Session.Item("listaTempPedidosBonificacao")
                listaTempPedidosBonificacao.Where(Function(f) f.NUMPEDCMP = numPedido).ToList().FirstOrDefault().VLRUTZACOCMC = valorUtilizado
                Session("listaTempPedidosBonificacao") = listaTempPedidosBonificacao
            Catch ex As Exception
                Danger(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Verifica se um pedido ja existe em uma lista
        ''' </summary>
        ''' <param name="lista"></param>
        ''' <param name="numPed"></param>
        ''' <returns></returns>
        Public Function PedidoExistente(lista As List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel), numPed As Decimal) As Boolean
            If lista IsNot Nothing AndAlso lista.Count > 0 Then
                For i = 0 To lista.Count - 1
                    If (lista(i).NUMPEDCMP = numPed) Then
                        Return True
                    End If
                Next
            End If
            Return False
        End Function

        ''' <summary>
        ''' Retorna um grid vazio com as configuracoes de bonificacoes do modal de bonificacao
        ''' </summary>
        ''' <returns></returns>
        Function GetGridPedidosBonificacoesModal() As GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)
            Return GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel).EmptyGrid("gridPedidosBonificacaoModal", "AtualizarGridPedidosBonificacao", "PedidoBonificacao")
        End Function

        ''' <summary>
        ''' Retorna um grid vazio com as configuracoes da lista de pedidos do modal de bonificacao
        ''' </summary>
        ''' <returns></returns>
        Function GetGridPedidosCompraModal() As GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)
            Return GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel).EmptyGrid("gridPedidosCompraModal", "AtualizarGridPedidosCompra", "PedidoBonificacao")
        End Function

        ''' <summary>
        ''' Atualiza o grid de pedidos bonificados da tela(paginacao e ordenacao)
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridBonificacoes(dadosPesquisa As GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)) As PartialViewResult
            Try
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoModel.StateGridBonificacoes.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                Dim gridConf = PaginarEAtualizarGrid(acaoModel.Bonificacoes, dadosPesquisa.Page, dadosPesquisa.Filter, acaoModel.StateGridBonificacoes)
                acaoModel.StateGridBonificacoes = gridConf
                gridConf.TotalCount = acaoModel.Bonificacoes.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridBonificacao.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Atualiza o grid de pedidos bonificados do modal (paginacao e ordenacao)
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridPedidosBonificacao(dadosPesquisa As GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)) As PartialViewResult
            Try
                Dim listaTempPedidosBonificacao As List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel) = Session.Item("listaTempPedidosBonificacao")
                If (listaTempPedidosBonificacao Is Nothing) Then
                    listaTempPedidosBonificacao = New List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)
                End If
                Dim gridConf = PaginarEAtualizarGrid(listaTempPedidosBonificacao, dadosPesquisa.Page, dadosPesquisa.Filter, GetGridPedidosBonificacoesModal)
                gridConf.TotalCount = listaTempPedidosBonificacao.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridBonificacaoModal.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Atualiza o grid de pedidos de compra do modal (paginacao e ordenacao)
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridPedidosCompra(dadosPesquisa As GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)) As PartialViewResult
            Try
                Dim listaTempPedidosCompra As List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel) = Session.Item("listaTempPedidosCompra")
                If (listaTempPedidosCompra Is Nothing) Then
                    listaTempPedidosCompra = New List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)
                End If
                Dim gridConf = PaginarEAtualizarGrid(listaTempPedidosCompra, dadosPesquisa.Page, dadosPesquisa.Filter, GetGridPedidosCompraModal)
                gridConf.TotalCount = listaTempPedidosCompra.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Bonificacao/_GridPedidosCompraModal.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

    End Class
End Namespace