﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers

Namespace Controllers
    <Authorize(Roles:="12,17,14")>
    Public Class PremiacaoExclusivaController
        Inherits AbstractController

        Private boFornecedores As New RelacaoAcaoComercialxFornecedoresBO()

        'Property Cache As ICacheProvider = New DefaultCacheProvider()

        ''' <summary>
        ''' Abrir o modal de inserir premiacao exclusiva
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function EditarPremiacao(ByVal objeto As Decimal?) As PartialViewResult
            Try
                ViewBag.Fornecedor = Constantes.FornecedoresCodFrn(objeto)

                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim premiacao As RelacaoAcaoComercialxPremiacaoExclusivaViewModel = acaoModel.Premiacao.Where(Function(p) p.CODFRN = objeto).FirstOrDefault()
                premiacao.Novo = False

                Return PartialView("~/Views/AcaoComercial/Patrocinador/PremiacaoExclusiva/_ModalInserirPremiacaoExclusiva.vbhtml", premiacao)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function
        <HttpPost()>
        Public Function InserirPremiacao() As PartialViewResult
            Try
                ViewBag.Fornecedor = Constantes.Fornecedores
                Return PartialView("~/Views/AcaoComercial/Patrocinador/PremiacaoExclusiva/_ModalInserirPremiacaoExclusiva.vbhtml", New RelacaoAcaoComercialxPremiacaoExclusivaViewModel())
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Concluir adicao e edicao de Premiação
        ''' </summary>
        ''' <returns></returns>
        <HttpPost()>
        Public Function ValidarPremiacao(premiacao As RelacaoAcaoComercialxPremiacaoExclusivaViewModel) As PartialViewResult
            Try
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")

                ViewBag.Fornecedor = New List(Of VO.RelacaoAcaoComercialxFornecedores)

                Dim nomeValido As Boolean = False

                If (premiacao IsNot Nothing AndAlso premiacao.CODFRN IsNot Nothing) Then
                    Dim lista = Constantes.FornecedoresCodFrn(premiacao.CODFRN)
                    If (lista.Count > 0) Then
                        ViewBag.Fornecedor = lista
                        premiacao.NOMFRN = lista.FirstOrDefault().NOMFRN
                        nomeValido = True
                    Else
                        Response.StatusCode = 400 'código para que seja chamado OnError do ajaxform
                        Return EditarPremiacao(premiacao.CODFRN)
                    End If
                End If


                If (ModelState.IsValid AndAlso nomeValido) Then

                    If PremiacaoExistente(premiacao, acaoModel.Premiacao) Then
                        Warning(Constantes.PREMIACAO_EXISTENTE)
                        Response.StatusCode = 400 'código para que seja chamado OnError do ajaxform
                        Return EditarPremiacao(premiacao.CODFRN)
                    End If

                    premiacao.CODACOCMC = acaoModel.CODACOCMC
                    If premiacao.Novo Then
                        Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                    Else
                        Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
                    End If

                    premiacao.Novo = False
                    acaoModel.Premiacao = acaoModel.Premiacao.Where(Function(f) f.CODFRN <> premiacao.CODFRN).ToList()
                    acaoModel.Premiacao.Add(premiacao)

                    Dim gridConf = GridSettings(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel).EmptyGrid("gridPremiacoes", "AtualizarGridPremiacoes", "PremiacaoExclusiva")
                    gridConf.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                    gridConf.ListPaged = PaginarLista(acaoModel.Premiacao, gridConf.Page)
                    gridConf.TotalCount = acaoModel.Premiacao.Count
                    Return PartialView("~/Views/AcaoComercial/Patrocinador/PremiacaoExclusiva/_GridPremiacao.vbhtml", gridConf)
                Else
                    Response.StatusCode = 400 'código para que seja chamado OnError do ajaxform
                    Return EditarPremiacao(premiacao.CODFRN)
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Verifica se a premiacao já existe na acao
        ''' </summary>
        ''' <param name="premiacao"></param>
        ''' <param name="premiacoes"></param>
        ''' <returns></returns>
        Public Function PremiacaoExistente(premiacao As RelacaoAcaoComercialxPremiacaoExclusivaViewModel, premiacoes As List(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel)) As Boolean

            Dim count = premiacoes.Where(Function(f) f.CODFRN = premiacao.CODFRN).Count()

            If premiacao.Novo = True Then
                Return count > 0
            End If
            Return count > 1

            Return False
        End Function

        Public Function ConfirmaRemoverPremiacao(ByVal objeto As Decimal?) As PartialViewResult
            Try
                Dim modelConfirmacao = New ModalConfirmacaoViewModel(objeto)
                modelConfirmacao.Mensagem = "Tem certeza que deseja excluir o essa Premiação Exclusiva?"
                modelConfirmacao.BtnSimAcao = "removePremiacao"
                Return PartialView("~/Views/Shared/_ModalConfirmacaoGenerico.vbhtml", modelConfirmacao)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Remover premiação do grid de premiações da tela de acao
        ''' </summary>
        ''' <param name="indexPremiacao"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RemoverPremiacao(ByVal indexPremiacao As Decimal) As PartialViewResult
            Try
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoModel.Premiacao = acaoModel.Premiacao.Where(Function(f) Not (f.CODFRN = indexPremiacao)).ToList
                Session("acaoModel") = acaoModel

                Dim gridConf = GridSettings(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel).EmptyGrid("gridPremiacoes", "AtualizarGridPremiacoes", "PremiacaoExclusiva")
                gridConf.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                gridConf.ListPaged = PaginarLista(acaoModel.Premiacao, gridConf.Page)
                gridConf.TotalCount = acaoModel.Premiacao.Count
                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                Return PartialView("~/Views/AcaoComercial/Patrocinador/PremiacaoExclusiva/_GridPremiacao.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Atualiza o grid de Premiacao da tela
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridPremiacoes(dadosPesquisa As GridSettings(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel)) As PartialViewResult

            Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
            acaoModel.StateGridPremiacao.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
            Dim gridConf = PaginarEAtualizarGrid(acaoModel.Premiacao, dadosPesquisa.Page, dadosPesquisa.Filter, acaoModel.StateGridPremiacao)
            acaoModel.StateGridPremiacao = gridConf

            Return PartialView("~/Views/AcaoComercial/Patrocinador/PremiacaoExclusiva/_GridPremiacao.vbhtml", gridConf)

        End Function

    End Class
End Namespace