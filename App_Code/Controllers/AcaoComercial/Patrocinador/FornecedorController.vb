﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers

Namespace Controllers

    <Authorize(Roles:="12,17,14")>
    Public Class FornecedorController
        Inherits AbstractController

        Private boFornecedores As New RelacaoAcaoComercialxFornecedoresBO()
        Private boEmpenho As New DestinoDescontoPorBonificacaoPedidoCompraBO
        Private boBU As New UnidadeEstrategicaDeNegocioBO()
        Private boDiretoriaCompra As New DiretoriaCompraBO
        Private boRelacaoEquipeDeComprasxGerenteNegocioServCompras As New RelacaoEquipeDeComprasxGerenteNegocioServComprasBO

        'Property Cache As ICacheProvider = New DefaultCacheProvider()



#Region "Metodos chamados a partir do modal"

        ''' <summary>
        ''' Busca fornecedores (se passar o codigo retorna um só, se o parametro for null retorna todos os fornecedores)
        ''' </summary>
        ''' <param name="codFrn"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function BuscarFornecedores(ByVal codFrn As Decimal?, ByVal tipofornecedor As Decimal?) As JsonResult
            Try
                Dim listaFornecedores As Object
                If (tipofornecedor.GetValueOrDefault() = 1) Then
                    listaFornecedores = BuscaFornecedoresEdlLista(codFrn)
                Else
                    listaFornecedores = BuscaFornecedoresLista(codFrn)
                End If

                If listaFornecedores Is Nothing Or listaFornecedores.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    Return Nothing
                End If

                Return Json(listaFornecedores, JsonRequestBehavior.AllowGet)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function
        Private Function BuscaFornecedoresLista(ByVal codFrn As Decimal?)
            Dim boFrn As RelacaoAcaoComercialxFornecedoresBO = New RelacaoAcaoComercialxFornecedoresBO
            Dim fornecedores = boFrn.BuscaFornecedores(codFrn)
            Return fornecedores
        End Function

        Private Function BuscaFornecedoresEdlLista(ByVal codFrn As Decimal?)
            Dim boFrn As RelacaoAcaoComercialxFornecedoresBO = New RelacaoAcaoComercialxFornecedoresBO
            Dim fornecedoresEdl = boFrn.BuscaFornecedoresEdl(codFrn)
            Return fornecedoresEdl
        End Function

        ''' <summary>
        ''' Atualiza o grid de fornecedores do modal de incluir fornecedores
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridInserirFornecedor(dadosPesquisa As GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel)) As PartialViewResult
            Try
                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")
                Dim gridInserirFornecedor = GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel).EmptyGrid("gridInserirFornecedor", "AtualizarGridInserirFornecedor", "Fornecedor")
                gridInserirFornecedor = PaginarEAtualizarGrid(listaTempFornecedores, dadosPesquisa.Page, dadosPesquisa.Filter, gridInserirFornecedor)
                gridInserirFornecedor.TotalCount = listaTempFornecedores.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridInserirFornecedor.vbhtml", gridInserirFornecedor)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Adiciona fornecedores ao grid de fornecedores do modal
        ''' </summary>
        ''' <param name="codFrn"></param>
        ''' <param name="codCelula"></param>
        ''' <param name="codDiretoria"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function AdicionarFornecedorGrid(Optional codFrn As Decimal? = Nothing,
                                                Optional codCelula As Decimal? = Nothing,
                                                Optional codDiretoria As Decimal? = Nothing,
                                                Optional tipoFornecedor As Decimal? = Nothing) As ActionResult
            Try
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")

                If acaoModel.IndPreAcordo = True And listaTempFornecedores.Count = 0 Or acaoModel.IndPreAcordo = False And listaTempFornecedores.Count >= 0 Then
                    Dim NovaListaFornecedoresVO = boFornecedores.BuscaFornecedoresFiltro(codFrn, codCelula, codDiretoria, acaoModel.TIPDSNDSCBNF, acaoModel.IndPreAcordo, tipoFornecedor)

                    'Adiciona apenas os fornecedores que ainda nao existem na lista
                    For Each fornecedorVO As VO.RelacaoAcaoComercialxFornecedores In NovaListaFornecedoresVO
                        If (listaTempFornecedores.Where(Function(f) f.CODFRN = fornecedorVO.CODFRN).Count = 0) Then

                            Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                            If (acaoCache.TIPACOMCD = TipoAcao.Fornecedor AndAlso fornecedorVO.CODFRN <> Constantes.DESPESAS_MARTINS AndAlso acaoCache.TIPDSNDSCBNF = 99 AndAlso tipoFornecedor <> 1) Then '99 - ACOES COMERCIAIS
                                Warning("Ações mercadológicas com empenho '99 - ACOES COMERCIAIS', são exclusivas para o fornecedor '999999 - DESPESAS MARTINS'")
                                Return Nothing
                            End If
                            If (((acaoCache.TIPACOMCD = TipoAcao.PreAcordoValor Or acaoCache.TIPACOMCD = TipoAcao.PreAcordoVigencia)) _
                                        AndAlso fornecedorVO.CODFRN = Constantes.DESPESAS_MARTINS AndAlso acaoCache.TIPDSNDSCBNF = 20) Then '20 - PROMOCOES MARTINS
                                Warning("Não é permitido Pré-Acordos com empenho '20 - PROMOCOES MARTINS', para o fornecedor '999999 - DESPESAS MARTINS'")
                                Return Nothing
                            End If

                            listaTempFornecedores.Add(Mapper.Map(Of VO.RelacaoAcaoComercialxFornecedores, RelacaoAcaoComercialxFornecedoresViewModel)(fornecedorVO))
                        End If
                    Next
                    'Dim bu As UnidadeEstrategicaDeNegocioViewModel
                    'If IsNothing(bu.CODUNDESRNGC) Then
                    '    ViewBag.listaBU = True
                    'Else
                    '    ViewBag.listaBU = False
                    'End If

                    Session("listaTempFornecedores") = listaTempFornecedores

                Else
                    Warning(Constantes.NAO_POSSIVEL_INCLUIR_FORNECEDOR)
                    Return Nothing
                End If

                Dim gridInserirFornecedor = GetGridInserirFornecedoresModal()
                gridInserirFornecedor.ListPaged = PaginarLista(listaTempFornecedores)
                gridInserirFornecedor.TotalCount = listaTempFornecedores.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridInserirFornecedor.vbhtml", gridInserirFornecedor)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Salva as alteracoes de edição de fornecedor na lista
        ''' </summary>
        ''' <param name="fornecedor"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function AlterarFornecedor(ByVal fornecedor As RelacaoAcaoComercialxFornecedoresViewModel) As PartialViewResult
            Try

                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")

                If fornecedor Is Nothing Or fornecedor.CODFRN Is Nothing Then
                    Warning(Constantes.SELECIONAR_FORNECEDOR_EDITAR)
                Else
                    Dim fornecedorDaLista = acaoModel.Fornecedores.Find(Function(f) f.CODFRN = fornecedor.CODFRN)


                    If (fornecedor.CODFRN.GetValueOrDefault() = Constantes.DESPESAS_MARTINS) Then
                        fornecedorDaLista.CODUNDESRNGC = fornecedor.CODUNDESRNGC
                        fornecedorDaLista.DESUNDESRNGC = fornecedor.DESUNDESRNGC
                    End If

                    'Beneficio
                    fornecedorDaLista.DESBFCFRNACOCMC = fornecedor.DESBFCFRNACOCMC
                    'Taxa brinde
                    fornecedorDaLista.VLRTXAADMBDEACOCMC = fornecedor.VLRTXAADMBDEACOCMC.GetValueOrDefault()
                    'Valor Extra Acordo
                    fornecedorDaLista.VLRUTZEXACTTACOCMC = fornecedor.VLRUTZEXACTTACOCMC.GetValueOrDefault()
                    'Valor receita cmv
                    fornecedorDaLista.VLRUTZRCTCSTMER = fornecedor.VLRUTZRCTCSTMER.GetValueOrDefault()
                    'Valor acordo
                    fornecedorDaLista.VLRUTZCTTACOCMC = fornecedor.VLRUTZCTTACOCMC.GetValueOrDefault()
                    fornecedorDaLista.TIPFRNACOCMC = fornecedor.TIPFRNACOCMC.GetValueOrDefault()
                    fornecedorDaLista.PERPTCFRNACOCMC = fornecedor.PERPTCFRNACOCMC.GetValueOrDefault()

                    CalVlrPtcFrn(fornecedorDaLista)

                End If
                acaoModel.StateGridFornecedores.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                Dim gridConf = PaginarEAtualizarGrid(acaoModel.Fornecedores, acaoModel.StateGridFornecedores)
                acaoModel.StateGridFornecedores = gridConf
                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridFornecedor.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Carrega o drop de filtro do modal de inserir fornecedor
        ''' </summary>
        ''' <returns></returns>
        Public Function CarregarDropFornecedor()
            Dim DropFornecedor As New List(Of SelectListItem)
            DropFornecedor.Add(New SelectListItem() With {.Text = "DIRETORIA", .Value = "0"})
            DropFornecedor.Add(New SelectListItem() With {.Text = "CELULA", .Value = "1"})
            DropFornecedor.Add(New SelectListItem() With {.Text = "FORNECEDOR", .Value = "2", .Selected = True})
            Return DropFornecedor
        End Function

        ''' <summary>
        ''' Exibe dados do fornecedor na tab de dados no modal
        ''' </summary>
        ''' <param name="codFrn"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function ExibeDadosFornecedor(ByVal codFrn As Decimal) As PartialViewResult
            Try
                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")
                Dim fornecedor = listaTempFornecedores.Find(Function(f) f.CODFRN = codFrn)
                ViewBag.listaEmpenho = boEmpenho.ListT(New VO.DestinoDescontoPorBonificacaoPedidoCompra())
                ViewBag.listaBU = boBU.ListT(New VO.UnidadeEstrategicaDeNegocio())
                'If (Not IsNothing(fornecedor.CODUNDESRNGC)) Then
                '    ViewBag.listaBU = True
                'Else
                '    ViewBag.listaBU = False
                'End If

                ViewBag.ListaDropFornecedor = Constantes.FornecedoresCodFrn(codFrn) ' boFornecedores.BuscaFornecedores(codFrn)
                fornecedor.TIPDES = fornecedor.TIPDSNDSCBNFEXACTT & " - " & fornecedor.DESDSNDSCBNFEXACTT

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_DadosFornecedor.vbhtml", fornecedor)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Altera os dados do fornecedor pelo modal de inserir fornecedor
        ''' </summary>
        ''' <param name="fornecedor"></param>
        ''' <returns></returns>
        Public Function AlterarFornecedorModal(ByVal fornecedor As RelacaoAcaoComercialxFornecedoresViewModel) As PartialViewResult
            Try
                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")
                If fornecedor Is Nothing Or fornecedor.CODFRN Is Nothing Then
                    Warning(Constantes.SELECIONAR_FORNECEDOR_EDITAR)
                Else

                    Dim fornecedorDaLista = listaTempFornecedores.Find(Function(f) f.CODFRN = fornecedor.CODFRN)

                    If (fornecedor.CODFRN.GetValueOrDefault() = Constantes.DESPESAS_MARTINS) Then
                        fornecedorDaLista.CODUNDESRNGC = fornecedor.CODUNDESRNGC
                        fornecedorDaLista.DESUNDESRNGC = fornecedor.DESUNDESRNGC
                    End If
                    'Beneficio
                    fornecedorDaLista.DESBFCFRNACOCMC = fornecedor.DESBFCFRNACOCMC
                    'Taxa brinde
                    fornecedorDaLista.VLRTXAADMBDEACOCMC = fornecedor.VLRTXAADMBDEACOCMC.GetValueOrDefault()
                    'Valor Extra Acordo
                    fornecedorDaLista.VLRUTZEXACTTACOCMC = fornecedor.VLRUTZEXACTTACOCMC.GetValueOrDefault()
                    'Valor receita cmv
                    fornecedorDaLista.VLRUTZRCTCSTMER = fornecedor.VLRUTZRCTCSTMER.GetValueOrDefault()
                    'Valor acordo
                    fornecedorDaLista.VLRUTZCTTACOCMC = fornecedor.VLRUTZCTTACOCMC.GetValueOrDefault()
                    'Percentual Participação
                    fornecedorDaLista.PERPTCFRNACOCMC = fornecedor.PERPTCFRNACOCMC.GetValueOrDefault()


                    CalVlrPtcFrn(fornecedorDaLista)


                End If

                Dim gridConf = GetGridInserirFornecedoresModal()
                gridConf.ListPaged = PaginarLista(listaTempFornecedores)
                gridConf.TotalCount = listaTempFornecedores.Count
                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridInserirFornecedor.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Realiza calculo de rateio e atribui aos fornecedores
        ''' </summary>
        ''' <param name="tipo"></param>
        ''' <param name="vlrRateio"></param>
        ''' <returns></returns>
        Public Function CalcularRateio(ByVal tipo As String, ByVal vlrRateio As Decimal) As PartialViewResult
            Try
                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")
                For NroAux As Integer = 0 To listaTempFornecedores.Count - 1
                    If tipo = "VALOR_FIXO" Then
                        listaTempFornecedores(NroAux).VLRUTZEXACTTACOCMC = vlrRateio
                    ElseIf tipo = "PROPORCIONAL_SALDO" Then
                        Dim percentual = (listaTempFornecedores(NroAux).VLRSLDDSNDSCACOCMC.GetValueOrDefault() * 100) / listaTempFornecedores.Sum(Function(p) p.VLRSLDDSNDSCACOCMC.GetValueOrDefault())
                        listaTempFornecedores(NroAux).VLRUTZEXACTTACOCMC = (vlrRateio * percentual) / 100
                    ElseIf tipo = "VALOR_DIVIDO_QTD" Then
                        listaTempFornecedores(NroAux).VLRUTZEXACTTACOCMC = vlrRateio / listaTempFornecedores.Count
                    End If
                Next

                Session("listaTempFornecedores") = listaTempFornecedores

                Dim gridInserirFornecedor = GetGridInserirFornecedoresModal()
                gridInserirFornecedor.ListPaged = PaginarLista(listaTempFornecedores)
                gridInserirFornecedor.TotalCount = listaTempFornecedores.Count

                If tipo = "VALOR_FIXO" Then
                    SuccessNotDismissible("Rateio efetuado com sucesso!</br> R$" & vlrRateio.ToString("#,##0.00") & " foi atribuído para " & listaTempFornecedores.Count & " fornecedores.")
                ElseIf tipo = "PROPORCIONAL_SALDO" Then
                    SuccessNotDismissible("Rateio efetuado com sucesso!</br> O valor R$" & vlrRateio.ToString("#,##0.00") & " foi dividido de forma proporcional ao saldo do fornecedor")
                ElseIf tipo = "VALOR_DIVIDO_QTD" Then
                    SuccessNotDismissible("Rateio efetuado com sucesso!</br> O valor R$" & vlrRateio.ToString("#,##0.00") & " foi dividido igualmente entre " & listaTempFornecedores.Count & " fornecedores.")
                End If

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridInserirFornecedor.vbhtml", gridInserirFornecedor)


            Catch ex As Exception
                Danger(ex)
            End Try

            Return Nothing

        End Function

        ''' <summary>
        ''' Atribui o mesmo beneficio para todos os fornecedores selecionados
        ''' </summary>
        ''' <param name="sBeneficio"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function InserirBeneficioParaOsfornecedores(ByVal sBeneficio As String) As PartialViewResult
            Try
                If sBeneficio Is Nothing Then
                    Return Nothing
                End If

                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")
                For Each fornecedor In listaTempFornecedores
                    fornecedor.DESBFCFRNACOCMC = sBeneficio
                Next

                Session("listaTempFornecedores") = listaTempFornecedores

                Dim gridInserirFornecedor = GetGridInserirFornecedoresModal()
                gridInserirFornecedor.ListPaged = PaginarLista(listaTempFornecedores)
                gridInserirFornecedor.TotalCount = listaTempFornecedores.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridInserirFornecedor.vbhtml", gridInserirFornecedor)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Retorna o grid de fornecedores do modal vazio
        ''' </summary>
        ''' <returns></returns>
        Function GetGridInserirFornecedoresModal() As GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel)
            Return GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel).EmptyGrid("gridInserirFornecedor", "AtualizarGridInserirFornecedor", "Fornecedor")
        End Function

        ''' <summary>
        ''' Fecha o modal salvando as alterações nos fornecedores
        ''' </summary>
        ''' <returns></returns>
        <HttpPost()>
        Public Function ValidaInserirFornecedores(ByVal tipoFornecedor As Decimal?, ByVal percentualParticipacao As Decimal?) As PartialViewResult
            Try
                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")

                'Incluir os campos Tipo Fornecedor e Percentual Participação
                For Each fornecedor As RelacaoAcaoComercialxFornecedoresViewModel In listaTempFornecedores
                    fornecedor.TIPFRNACOCMC = tipoFornecedor
                    fornecedor.PERPTCFRNACOCMC = If(IsNothing(percentualParticipacao), 0, percentualParticipacao)
                Next

                'Salva no cache e exclui lista temporaria do cache
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoModel.Fornecedores = listaTempFornecedores

                Dim gridConf = GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel).EmptyGrid("gridFornecedores", "AtualizarGridFornecedores", "Fornecedor")
                gridConf.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                gridConf.ListPaged = PaginarLista(acaoModel.Fornecedores, gridConf.Page)
                gridConf.TotalCount = acaoModel.Fornecedores.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridFornecedor.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function


        Private Sub CalVlrPtcFrn(ByRef forn As RelacaoAcaoComercialxFornecedoresViewModel)
            Dim VlrTot As Double = 0
            Dim TxaAdm As Double = 0

            VlrTot = forn.VLRUTZCTTACOCMC + forn.VLRUTZEXACTTACOCMC + forn.VLRUTZRCTCSTMER

            'se for despesas martins não há taxa de adm. e o valor total é o valor da participação
            'Todo: buscar taxa de administrativa de parametro
            If forn.CODFRN <> Constantes.DESPESAS_MARTINS Then
                TxaAdm = VlrTot * (0 / 100) 'Calcula a txa de administração
            End If
            forn.VLRPTCFRNACOCMC = VlrTot - TxaAdm  'Mostra o Valor total da Participação do fornecedor
            forn.VLRTXAADMFRNACOCMC = TxaAdm
            'CalTxaBdeFrnAcoCmc() ' Recalcula Taxa de Brinde

        End Sub

        Public Function CarregarDropTipoFornecedor(IndPreAcordo As Boolean)
            Dim DropTipoFornecedor As New List(Of SelectListItem)
            DropTipoFornecedor.Add(New SelectListItem() With {.Text = "Martins", .Value = "0", .Selected = IndPreAcordo})
            DropTipoFornecedor.Add(New SelectListItem() With {.Text = "EDL", .Value = "1"})
            Return DropTipoFornecedor
        End Function

        Public Function SelecionaTipoFornecedor(TipoFornecedor As Decimal)
            Dim DropTipoFornecedor As New List(Of SelectListItem)
            DropTipoFornecedor.Add(New SelectListItem() With {.Text = "Martins", .Value = "0", .Selected = (TipoFornecedor = 0)})
            DropTipoFornecedor.Add(New SelectListItem() With {.Text = "EDL", .Value = "1", .Selected = (TipoFornecedor = 1)})
            Return DropTipoFornecedor
        End Function

#Region "Metodos de remocao"

        ''' <summary>
        ''' Confirmacao para exclusao de fornecedor do grid do modal de fornecedores
        ''' </summary>
        ''' <param name="tipo"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function ConfirmaRemoverFornecedorModal(tipo As String) As PartialViewResult
            Try

                Dim fornecedores = Session.Item("listaTempFornecedores")
                If fornecedores Is Nothing Or fornecedores.Count = 0 Then
                    Information(Constantes.NAO_EXISTEM_REGISTROS_REMOVER)
                    Return Nothing
                End If

                Dim confirmaExclusaoTodos = New ModalConfirmacaoViewModel(Nothing)
                confirmaExclusaoTodos.IdDiv = "modalFornecedor"

                Select Case tipo
                    Case "todos"
                        Dim count = IIf(fornecedores Is Nothing, 0, fornecedores.Count)
                        confirmaExclusaoTodos.BtnSimAcao = "removerTodosFornecedores"
                        confirmaExclusaoTodos.Mensagem = String.Format(Constantes.EXCLUI_TODOS_FORNECEDORES, count)
                    Case "selecionados"
                        confirmaExclusaoTodos.BtnSimAcao = "removerFornecedoresSelecionados"
                        confirmaExclusaoTodos.Mensagem = Constantes.EXCLUI_FORNECEDORES_SELECIONADOS
                    Case "semDados"
                        confirmaExclusaoTodos.BtnSimAcao = "removerFornecedoresPorSaldo"
                        confirmaExclusaoTodos.Mensagem = Constantes.EXCLUI_FORNECEDOR_SEM_DADOS
                        confirmaExclusaoTodos.Objeto = Constantes.FrnSaldo.SemDados
                    Case "semSaldoAcordo"
                        confirmaExclusaoTodos.BtnSimAcao = "removerFornecedoresPorSaldo"
                        confirmaExclusaoTodos.Mensagem = Constantes.EXCLUI_FORNECEDOR_SEM_SALDO_ACORDO
                        confirmaExclusaoTodos.Objeto = Constantes.FrnSaldo.SemSaldoAcordo
                    Case "semSaldoExtraAcordo"
                        confirmaExclusaoTodos.BtnSimAcao = "removerFornecedoresPorSaldo"
                        confirmaExclusaoTodos.Mensagem = Constantes.EXCLUI_FORNECEDOR_SEM_SALDO_EXTRA_ACORDO
                        confirmaExclusaoTodos.Objeto = Constantes.FrnSaldo.SemSaldoAcordoExtra
                    Case "semSaldoAcordoSaldoExtra"
                        confirmaExclusaoTodos.BtnSimAcao = "removerFornecedoresPorSaldo"
                        confirmaExclusaoTodos.Mensagem = Constantes.EXCLUI_FORNECEDOR_SEM_SALDO_ACORDO_EXTRA_ACORDO
                        confirmaExclusaoTodos.Objeto = Constantes.FrnSaldo.SemSaldoAcordoSaldoExtra
                End Select
                Return PartialView("~/Views/Shared/_ModalConfirmacaoGenerico.vbhtml", confirmaExclusaoTodos)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Remove fornecedores do grid de fornecedores do modal por filtro
        ''' </summary>
        ''' <param name="codFrn"></param>
        ''' <param name="codCelula"></param>
        ''' <param name="codDiretoria"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RemoverFornecedores(ByVal ListCodigos As String) As PartialViewResult
            Try
                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")

                If listaTempFornecedores Is Nothing Or listaTempFornecedores.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                Else

                    Dim result() As String = ListCodigos.Split("|")

                    For Each codigo In result
                        Dim deleted As RelacaoAcaoComercialxFornecedoresViewModel = listaTempFornecedores.Where(Function(f) f.CODFRN = Convert.ToDecimal(codigo)).FirstOrDefault()

                        If (Not deleted Is Nothing) Then
                            listaTempFornecedores.Remove(deleted)
                        End If
                    Next

                    Session("listaTempFornecedores") = listaTempFornecedores
                    Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
                End If

                Dim gridInserirFornecedor = GetGridInserirFornecedoresModal()
                gridInserirFornecedor.ListPaged = PaginarLista(listaTempFornecedores)
                gridInserirFornecedor.TotalCount = listaTempFornecedores.Count
                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridInserirFornecedor.vbhtml", gridInserirFornecedor)

            Catch ex As Exception
                Danger(ex)
                Return Nothing

            End Try
        End Function

        ''' <summary>
        ''' Remove todos os fornecedores do grid de fornecedores do modal
        ''' </summary>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RemoverTodosFornecedores() As PartialViewResult
            Try
                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")
                listaTempFornecedores = New List(Of RelacaoAcaoComercialxFornecedoresViewModel)
                Session("listaTempFornecedores") = listaTempFornecedores

                Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)

                Dim gridInserirFornecedor = GetGridInserirFornecedoresModal()
                gridInserirFornecedor.ListPaged = PaginarLista(listaTempFornecedores)
                gridInserirFornecedor.TotalCount = listaTempFornecedores.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridInserirFornecedor.vbhtml", gridInserirFornecedor)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Remove todos os fornecedores selecionados do grid de fornecedores do modal
        ''' </summary>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RemoverFornecedoresSelecionados(itensCheckados As List(Of Decimal)) As PartialViewResult
            Try
                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")
                listaTempFornecedores = listaTempFornecedores.Where(Function(f) Not itensCheckados.Contains(f.CODFRN)).ToList()
                Session("listaTempFornecedores") = listaTempFornecedores

                Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)

                Dim gridInserirFornecedor = GetGridInserirFornecedoresModal()
                gridInserirFornecedor.ListPaged = PaginarLista(listaTempFornecedores)
                gridInserirFornecedor.TotalCount = listaTempFornecedores.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridInserirFornecedor.vbhtml", gridInserirFornecedor)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Remove os fornecedores do grid de fornecedores do modal de acordo com os saldos
        ''' </summary>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RemoverFornecedorPorSaldo(codTpSaldo As Integer) As PartialViewResult
            Try
                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")
                Dim countIni = listaTempFornecedores.Count

                Select Case codTpSaldo
                    Case Constantes.FrnSaldo.SemDados
                        listaTempFornecedores = listaTempFornecedores.Where(Function(f) Not f.SemDados).ToList()
                    Case Constantes.FrnSaldo.SemSaldoAcordo
                        listaTempFornecedores = listaTempFornecedores.Where(Function(f) Not f.SemSaldoAcordo).ToList()
                    Case Constantes.FrnSaldo.SemSaldoAcordoExtra
                        listaTempFornecedores = listaTempFornecedores.Where(Function(f) Not f.SemSaldoAcordoExtra).ToList()
                    Case Constantes.FrnSaldo.SemSaldoAcordoSaldoExtra
                        listaTempFornecedores = listaTempFornecedores.Where(Function(f) Not f.SemSaldoAcordoSaldoExtra).ToList()
                End Select

                If (countIni <> listaTempFornecedores.Count) Then
                    Session("listaTempFornecedores") = listaTempFornecedores
                    Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
                Else
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                Dim gridInserirFornecedor = GetGridInserirFornecedoresModal()
                gridInserirFornecedor.ListPaged = PaginarLista(listaTempFornecedores)
                gridInserirFornecedor.TotalCount = listaTempFornecedores.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridInserirFornecedor.vbhtml", gridInserirFornecedor)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Verifica se um fornecedor ja existe em uma lista
        ''' </summary>
        ''' <param name="lista"></param>
        ''' <param name="codFrn"></param>
        ''' <returns></returns>
        Public Function FornecedorExistente(lista As List(Of RelacaoAcaoComercialxFornecedoresViewModel), codFrn As Decimal) As Boolean
            If lista IsNot Nothing AndAlso lista.Count > 0 Then
                For i = 0 To lista.Count - 1
                    If (lista(i).CODFRN = codFrn) Then
                        Return True
                    End If
                Next
            End If
            Return False
        End Function

#End Region

#End Region

#Region "Metodos chamados a partir da tela de acao"

        ''' <summary>
        ''' Atualiza (paginacao e ordenacao) grid de fornecedores da tela de criar/editar acao mercadologica
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridFornecedores(dadosPesquisa As GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel)) As PartialViewResult
            Try
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoModel.StateGridFornecedores.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                Dim gridConf = PaginarEAtualizarGrid(acaoModel.Fornecedores, dadosPesquisa.Page, dadosPesquisa.Filter, acaoModel.StateGridFornecedores)
                acaoModel.StateGridFornecedores = gridConf
                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridFornecedor.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Abre o modal de edição de fornecedor
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function EditarFornecedor(ByVal objeto As Decimal) As PartialViewResult
            Try
                Dim acao As AcaoComercialViewModel = Session.Item("acaoModel")

                'Validar exibição dos campos Tipo Fornecedor e Percentual Participação
                If (IsNothing(Session.Item("INDEVTSMA"))) Then
                    Dim bo As New TipoDeEventoBO
                    Dim tipoEvento = bo.SelectT(New VO.TipoDeEvento With {.TIPEVTACOCMC = acao.TIPEVTACOCMC})
                    ViewBag.INDEVTSMA = If(tipoEvento.INDEVTSMA = 1, True, False)
                    Session.Item("INDEVTSMA") = tipoEvento.INDEVTSMA
                Else
                    ViewBag.INDEVTSMA = If(Session.Item("INDEVTSMA") = "1", True, False)
                End If

                Dim fornecedor = acao.Fornecedores.Find(Function(f) f.CODFRN = objeto)

                ViewBag.listaEmpenho = boEmpenho.ListT(New VO.DestinoDescontoPorBonificacaoPedidoCompra())
                ViewBag.listaBU = boBU.ListT(New VO.UnidadeEstrategicaDeNegocio())
                ViewBag.ListaTipoFornecedor = SelecionaTipoFornecedor(fornecedor.TIPFRNACOCMC)
                fornecedor.TIPDES = fornecedor.TIPDSNDSCBNFEXACTT & " - " & fornecedor.DESDSNDSCBNFEXACTT
                fornecedor.IndPreAcordo = acao.IndPreAcordo

                Session("listaTemEditarpFornecedores") = fornecedor

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_ModalFornecedor.vbhtml", fornecedor)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Remove um fornecedor do grid de fornecedores da tela de ação
        ''' </summary>
        ''' <param name="codFrn"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RemoverFornecedor(ByVal codFrn As Decimal) As PartialViewResult
            Try
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                Dim novaListaFornecedores = acaoModel.Fornecedores.Where(Function(f) f.CODFRN <> codFrn).ToList()
                acaoModel.Fornecedores = novaListaFornecedores
                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)

                acaoModel.StateGridFornecedores.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                Dim gridConf = PaginarEAtualizarGrid(acaoModel.Fornecedores, acaoModel.StateGridFornecedores)
                acaoModel.StateGridFornecedores = gridConf
                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_GridFornecedor.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Confirmacao para exclusao de fornecedor do grid de fornecedores da tela de acao
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function ConfirmaRemoverFornecedor(ByVal objeto As Decimal) As PartialViewResult
            Try
                Dim modelConfirmacao = New ModalConfirmacaoViewModel(objeto)
                modelConfirmacao.Mensagem = "Tem certeza que deseja excluir o fornecedor da ação?"
                modelConfirmacao.BtnSimAcao = "removeFornecedor"
                Return PartialView("~/Views/Shared/_ModalConfirmacaoGenerico.vbhtml", modelConfirmacao)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Abre o modal de inserir fornecedor na acao mercadologica
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function InserirFornecedor(ByVal objeto As RelacaoAcaoComercialxFornecedoresViewModel) As PartialViewResult
            Try
                Dim gridInserirFornecedor = GetGridInserirFornecedoresModal()
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                Session("listaTempFornecedores") = acaoModel.Fornecedores.ListCopy()
                gridInserirFornecedor.ListPaged = PaginarLista(acaoModel.Fornecedores)
                gridInserirFornecedor.TotalCount = acaoModel.Fornecedores.Count
                ViewBag.gridFornecedores = gridInserirFornecedor
                objeto.EmpenhoExaFrn = acaoModel.EmpenhoExa
                objeto.IndPreAcordo = acaoModel.IndPreAcordo


                ViewBag.listaEmpenho = boEmpenho.ListT(New VO.DestinoDescontoPorBonificacaoPedidoCompra())
                ViewBag.listaBU = boBU.ListT(New VO.UnidadeEstrategicaDeNegocio())

                ViewBag.listaFornecedor = CarregarDropFornecedor()
                ViewBag.ListaTipoFornecedor = CarregarDropTipoFornecedor(objeto.IndPreAcordo)
                ViewBag.ListaDropFornecedor = BuscaFornecedoresLista(Nothing)
                ViewBag.ListaDropFornecedorEdl = BuscaFornecedoresEdlLista(Nothing)
                ViewBag.ListaRelacaoEquipeDeComprasxGerenteNegocioServCompras = boRelacaoEquipeDeComprasxGerenteNegocioServCompras.ListT(New VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras()).OrderBy(Function(model) model.DESDIVCMP)
                ViewBag.ListaDiretoriaCompra = boDiretoriaCompra.ListT(New VO.DiretoriaCompra()).OrderBy(Function(model) model.DESDRTCMP)
                ViewBag.INDEVTSMA = If(Session.Item("INDEVTSMA") = "1", True, False)
                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_ModalInserirFornecedor.vbhtml", objeto)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Public Function DefinirTipoRateio() As PartialViewResult
            Try

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_ModalRateioVerba.vbhtml")

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        <HttpPost()>
        Public Function AssociaBeneficioFornecedor(ByVal objeto As String) As PartialViewResult
            Try

                Dim listaTempFornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel) = Session.Item("listaTempFornecedores")
                'objeto.CODFRN = listaTempFornecedores.Item(0).CODFRN
                'objeto.NOMFRN = listaTempFornecedores.Item(0).NOMFRN

                'ViewBag.gridAssociaBeneficioFornecedores = listaTempFornecedores
                ViewBag.gridAssociaBeneficioFornecedores = GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel).EmptyGrid()

                Dim item As New RelacaoAcaoComercialxFornecedoresViewModel With {.CODFRN = objeto}



                Return PartialView("~/Views/AcaoComercial/Patrocinador/Fornecedor/_ModalAssociaBeneficioFornecedor.vbhtml", item)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

#End Region

    End Class
End Namespace