﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList

Namespace Controllers

    <Authorize(Roles:="12,17,14")>
    Public Class CarimboController
        Inherits AbstractController

        Private boCarimbos As New RelacaoAcaoxFornecedorxCarimboBO()
        Private boRelacaoEquipeDeComprasxGerenteNegocioServCompras As New RelacaoEquipeDeComprasxGerenteNegocioServComprasBO

        'Property Cache As ICacheProvider = New DefaultCacheProvider()

        ''' <summary>
        ''' Abre o modal de inserir carimbo
        ''' </summary>
        ''' <param name="codAcao"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function InserirCarimbo(Optional codAcao As Decimal? = Nothing) As PartialViewResult

            Try
                ViewBag.filtroFornecedorCarimbo = Constantes.Fornecedores
                ViewBag.filtroCelulaCarimbo = boRelacaoEquipeDeComprasxGerenteNegocioServCompras.ListT(New VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras()).OrderBy(Function(model) model.DESDIVCMP)

                ViewBag.filtroCarimbos = New List(Of SelectListItem)()
                ViewBag.filtroCarimbos.Add(New SelectListItem() With {.Text = "Carimbo", .Value = 1})
                ViewBag.filtroCarimbos.Add(New SelectListItem() With {.Text = "Célula", .Value = 2})
                ViewBag.filtroCarimbos.Add(New SelectListItem() With {.Text = "Fornecedor", .Value = 3})

                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                Dim carimbos As New List(Of RelacaoAcaoxFornecedorxCarimboViewModel)
                carimbos = acaoModel.Carimbos.ListCopy()
                ViewBag.GridCarimbosSelecionados = GetGridCarimbosSelecionadosModal()
                ViewBag.GridCarimbosSelecionados.ListPaged = PaginarLista(carimbos)
                ViewBag.GridCarimbosSelecionados.TotalCount = carimbos.Count
                Session("listaTempCarimbosSelecionados") = carimbos
                Session("listaTempCarimbo") = New List(Of RelacaoAcaoxFornecedorxCarimboViewModel)

                ViewBag.GridCarimboModal = GetGridCarimboModal()

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Carimbo/_ModalInserirCarimbo.vbhtml", New RelacaoAcaoxFornecedorxCarimboViewModel)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Abre o modal de edição de fornecedor
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function EditarCarimbo(ByVal objeto As Decimal) As PartialViewResult
            Try
                Dim acao As AcaoComercialViewModel = Session.Item("acaoModel")
                Dim carimbo = acao.Carimbos.Find(Function(f) f.CODMCOVBAFRN = objeto)

                Session.Item("listaTempEditarCarimbo") = carimbo

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Carimbo/_ModalEditarCarimbo.vbhtml", carimbo)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        ''' <summary>
        ''' Salva as alteracoes de edição de fornecedor na lista
        ''' </summary>
        ''' <param name="fornecedor"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function AlterarCarimbo(ByVal carimbo As RelacaoAcaoxFornecedorxCarimboViewModel)
            Try
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                Dim carimboLista = acaoModel.Carimbos.Find(Function(f) f.CODMCOVBAFRN = carimbo.CODMCOVBAFRN)

                If (carimbo.VALOR_DISPONIVEL < carimbo.VLRUTZACOCMC) Then
                    Warning(Constantes.VALOR_UTILIZADO_MAIOR_QUE_DISPONIVEL)
                Else
                    carimboLista.VLRUTZACOCMC = carimbo.VLRUTZACOCMC

                    Session.Item("listaTempEditarCarimbo") = carimboLista

                    acaoModel.StateGridCarimbos.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                    Dim gridConf = PaginarEAtualizarGrid(acaoModel.Carimbos, acaoModel.StateGridCarimbos)
                    acaoModel.StateGridCarimbos = gridConf

                    Return PartialView("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimbo.vbhtml", gridConf)
                End If

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Filtrar carimbos de acordo com fornecedor, codigo do carimbo ou codigo da celula
        ''' </summary>
        ''' <param name="codFrn"></param>
        ''' <param name="codCelula"></param>
        ''' <param name="codCarimbo"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function FiltrarCarimbos(Optional ByVal codFrn As Decimal? = Nothing, Optional ByVal codCelula As Decimal? = Nothing, Optional ByVal codCarimbo As Decimal? = Nothing) As PartialViewResult
            Try
                Dim carimbos = boCarimbos.BuscarCarimbosPorFiltro(codFrn, codCelula, codCarimbo)
                If carimbos Is Nothing Or carimbos.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    Return Nothing
                End If

                Dim carimbosLista = Mapper.Map(Of List(Of VO.RelacaoAcaoxFornecedorxCarimbo), List(Of RelacaoAcaoxFornecedorxCarimboViewModel))(carimbos)
                Session("listaTempCarimbo") = carimbosLista

                Dim gridConf = GetGridCarimboModal()
                gridConf.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT5
                gridConf.ListPaged = PaginarLista(carimbosLista, gridConf.Page)
                gridConf.TotalCount = carimbosLista.Count
                Return PartialView("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimboModal.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function


        ''' <summary>
        ''' Selecionar carimbos checkados e adicionar na lista de carimbos selecionados
        ''' </summary>
        ''' <param name="itensCheckados"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function SelecionarCarimbos(ByVal itensCheckados As List(Of Decimal)) As PartialViewResult
            Try
                Dim listaTempCarimbosSelecionados As List(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Session.Item("listaTempCarimbosSelecionados")
                Dim listaTempCarimbo As List(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Session.Item("listaTempCarimbo")

                'Verifica se o pedido ja foi inserido
                For Each item In itensCheckados
                    If CarimboExistente(listaTempCarimbosSelecionados, item) Then
                        Warning(Constantes.CARIMBO_EXISTENTE)
                        Return Nothing
                    End If
                Next

                Dim carimbosParaAdd = listaTempCarimbo.Where(Function(f) itensCheckados.Contains(f.CODMCOVBAFRN)).ToList()
                listaTempCarimbosSelecionados.AddRange(carimbosParaAdd)

                Dim gridConf = GetGridCarimbosSelecionadosModal()
                gridConf.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT5
                gridConf.ListPaged = PaginarLista(listaTempCarimbosSelecionados, gridConf.Page)
                gridConf.TotalCount = listaTempCarimbosSelecionados.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimbosSelecionadosModal.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Remover carimbos checkados da lista de carimbos selecionados
        ''' </summary>
        ''' <param name="itensCheckados"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RemoverCarimbos(ByVal itensCheckados As List(Of Decimal)) As PartialViewResult
            Try
                Dim listaTempCarimbosSelecionados As List(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Session.Item("listaTempCarimbosSelecionados")
                listaTempCarimbosSelecionados = listaTempCarimbosSelecionados.Where(Function(f) Not itensCheckados.Contains(f.CODMCOVBAFRN)).ToList()

                Session("listaTempCarimbosSelecionados") = listaTempCarimbosSelecionados

                Dim gridConf = GetGridCarimbosSelecionadosModal()
                gridConf.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT5
                gridConf.ListPaged = PaginarLista(listaTempCarimbosSelecionados, gridConf.Page)
                gridConf.TotalCount = listaTempCarimbosSelecionados.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimbosSelecionadosModal.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Remover carimbo do grid de carimbos da tela de acao
        ''' </summary>
        ''' <param name="codCrb"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function RemoverCarimbo(ByVal codCrb As Decimal) As PartialViewResult
            Try
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoModel.Carimbos = acaoModel.Carimbos.Where(Function(f) f.CODMCOVBAFRN <> codCrb).ToList()

                Dim gridConf = GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid("gridCarimbos", "AtualizarGridCarimbos", "Carimbo")
                gridConf.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                gridConf.ListPaged = PaginarLista(acaoModel.Carimbos, gridConf.Page)
                gridConf.TotalCount = acaoModel.Carimbos.Count
                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)

                'Atualiza grid da pagina de edicao da acao
                Return PartialView("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimbo.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Confirmacao para exclusao de carimbo carimbo do grid de carimbos da tela de acao
        ''' </summary>
        ''' <param name="objeto"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function ConfirmaRemoverCarimbo(ByVal objeto As Decimal) As PartialViewResult
            Try
                Dim modelConfirmacao = New ModalConfirmacaoViewModel(objeto)
                modelConfirmacao.Mensagem = "Tem certeza que deseja excluir o carimbo da ação?"
                modelConfirmacao.BtnSimAcao = "removeCarimbo"
                Return PartialView("~/Views/Shared/_ModalConfirmacaoGenerico.vbhtml", modelConfirmacao)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Atualiza o valor do atributo Valor Utilizado de um pedido selecionado
        ''' </summary>
        <HttpPost()>
        Public Sub AtualizaValorUlitizadoCampoCarimbo(ByVal valores As List(Of Decimal))
            Try
                Dim codCarimbo = valores.FirstOrDefault
                Dim valorUtilizado = valores.LastOrDefault

                Dim listaTempCarimbosSelecionados As List(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Session.Item("listaTempCarimbosSelecionados")
                listaTempCarimbosSelecionados.Where(Function(f) f.CODMCOVBAFRN = codCarimbo).ToList().FirstOrDefault().VLRUTZACOCMC = valorUtilizado
            Catch ex As Exception
                Danger(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Concluir adicao e edicao de carimbos selecionados
        ''' Valida se todos os carimbos tem valor utilizado
        ''' </summary>
        ''' <returns></returns>
        Public Function ValidarCarimbos() As PartialViewResult
            Try
                Dim listaTempCarimbosSelecionados As List(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Session.Item("listaTempCarimbosSelecionados")

                If (listaTempCarimbosSelecionados.Find(Function(f) f.VLRUTZACOCMC = Nothing) IsNot Nothing) Then
                    Warning(Constantes.INFORME_VALOR_UTILIZADO_CARIMBO)
                    Return Nothing
                End If

                If (listaTempCarimbosSelecionados.Find(Function(f) f.VLRUTZACOCMC > f.VALOR_DISPONIVEL) IsNot Nothing) Then
                    Warning(Constantes.VALOR_UTILIZADO_MAIOR_QUE_DISPONIVEL)
                    Return Nothing
                End If

                Dim gridConf = GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid("gridCarimbos", "AtualizarGridCarimbos", "Carimbo")
                gridConf.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
                gridConf.ListPaged = PaginarLista(listaTempCarimbosSelecionados, gridConf.Page)
                gridConf.TotalCount = listaTempCarimbosSelecionados.Count

                'Salva no cache e exclui lista temporaria do cache
                Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
                acaoModel.Carimbos = listaTempCarimbosSelecionados
                'Cache.Invalidate("listaTempCarimbosSelecionados")

                'Atualiza grid da pagina de edicao da acao
                Return PartialView("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimbo.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Verifica se um pedido ja existe em uma lista
        ''' </summary>
        ''' <param name="lista"></param>
        ''' <param name="codCar"></param>
        ''' <returns></returns>
        Public Function CarimboExistente(lista As List(Of RelacaoAcaoxFornecedorxCarimboViewModel), codCar As Decimal) As Boolean
            If lista IsNot Nothing AndAlso lista.Count > 0 Then
                For i = 0 To lista.Count - 1
                    If (lista(i).CODMCOVBAFRN = codCar) Then
                        Return True
                    End If
                Next
            End If
            Return False
        End Function

        ''' <summary>
        ''' Retorna um grid vazio com as configuracoes de grid de carimbos selecionados
        ''' </summary>
        ''' <returns></returns>
        Function GetGridCarimbosSelecionadosModal() As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)
            Return GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid("gridCarimbosSelecionadosModal", "AtualizarGridCarimbosSelecionados", "Carimbo")
        End Function

        ''' <summary>
        ''' Retorna um grid vazio com as configuracoes de grid de carimbos do modal
        ''' </summary>
        ''' <returns></returns>
        Function GetGridCarimboModal() As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)
            Return GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel).EmptyGrid("gridCarimboModal", "AtualizarGridCarimboModal", "Carimbo")
        End Function

        ''' <summary>
        ''' Atualiza o grid de carimbos da tela
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridCarimbos(dadosPesquisa As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)) As PartialViewResult

            Dim acaoModel As AcaoComercialViewModel = Session.Item("acaoModel")
            acaoModel.StateGridCarimbos.Page.PageSize = Constantes.PAGE_SIZE_DEFAULT1000
            Dim gridConf = PaginarEAtualizarGrid(acaoModel.Carimbos, dadosPesquisa.Page, dadosPesquisa.Filter, acaoModel.StateGridCarimbos)
            acaoModel.StateGridCarimbos = gridConf

            Return PartialView("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimbo.vbhtml", gridConf)

        End Function

        ''' <summary>
        ''' Atualiza o grid de carimbos da busca por carimbos no modal de insercao de carimbo
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridCarimboModal(dadosPesquisa As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)) As PartialViewResult
            Try
                Dim listaTempCarimbo As List(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Session.Item("listaTempCarimbo")
                If (listaTempCarimbo Is Nothing) Then
                    listaTempCarimbo = New List(Of RelacaoAcaoxFornecedorxCarimboViewModel)
                End If
                Dim gridConf = PaginarEAtualizarGrid(listaTempCarimbo, dadosPesquisa.Page, dadosPesquisa.Filter, GetGridCarimboModal)
                gridConf.TotalCount = listaTempCarimbo.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimboModal.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Atualiza o grid de carimbos selecionados na tela de inserir carimbo
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function AtualizarGridCarimbosSelecionados(dadosPesquisa As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)) As PartialViewResult
            Try
                Dim listaTempCarimbosSelecionados As List(Of RelacaoAcaoxFornecedorxCarimboViewModel) = Session.Item("listaTempCarimbosSelecionados")
                Dim gridConf = PaginarEAtualizarGrid(listaTempCarimbosSelecionados, dadosPesquisa.Page, dadosPesquisa.Filter, GetGridCarimbosSelecionadosModal)
                gridConf.TotalCount = listaTempCarimbosSelecionados.Count

                Return PartialView("~/Views/AcaoComercial/Patrocinador/Carimbo/_GridCarimbosSelecionadosModal.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function
    End Class
End Namespace