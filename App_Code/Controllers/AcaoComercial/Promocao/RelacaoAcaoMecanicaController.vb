﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class RelacaoAcaoMecanicaController
        Inherits AbstractController

        Private relacaoAcaoMecanicaBO As New RelacaoAcaoMecanicaBO()
        Private formaDeCalculoBo As New FormaDeCalculoBO()
        Private tipoCriterioBO As New TipoCriterioBO()

        Public Sub New()

        End Sub

        Function Index() As RedirectResult
            Return New RedirectResult("Home")
        End Function

        <HttpPost()>
        Function EditMecanicaPromocao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()

                Dim obj As VO.RelacaoAcaoMecanica = BO.SelectEditMecanicaGerais(CODACOCMC, NUMSEQRGRACOCMC)

                Dim objGerais As RelacaoAcaoMecanicaViewModel = Mapper.Map(Of VO.RelacaoAcaoMecanica, RelacaoAcaoMecanicaViewModel)(obj)

                CarregaDropCreateEditRelacaoAcaoMecanica("")

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_EditMecanicaPromocao.vbhtml", objGerais)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        'Metodo Edit Geral
        <HttpPost()>
        Function CriarMecanicaPromocao(ByVal id As Decimal) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim objGerais As RelacaoAcaoMecanicaViewModel = New RelacaoAcaoMecanicaViewModel() With {
                    .CODACOCMC = id,
                    .NUMSEQRGRACOCMC = 0,
                    .INDPTCTOTMIXPMC = 0
                    }

                CarregaDropCreateEditRelacaoAcaoMecanica("")

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_EditMecanicaPromocao.vbhtml", objGerais)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo Edit Widget Filiais
        <HttpGet()>
        Function EditMecanicaPromocaoWidgetFiliais(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO As RelacaoAcaoMecanicaFilialBO = New RelacaoAcaoMecanicaFilialBO()

                Dim list As List(Of VO.RelacaoAcaoMecanicaFilial) = BO.ListarFiliais(CODACOCMC, NUMSEQRGRACOCMC)

                Dim filiaisList As List(Of RelacaoAcaoMecanicaFilialViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaFilial), List(Of RelacaoAcaoMecanicaFilialViewModel))(list)

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetMecanicaPromocaoFiliais.vbhtml", filiaisList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo Carrega Drop
        Sub CarregaDropCreateEditRelacaoAcaoMecanica(ByVal DESEQCCALCTRPMC As String)
            'Carrega Drop Tipo Promoção
            Dim TipoPromoList As List(Of TipoPromocaoViewModel) = New List(Of TipoPromocaoViewModel)

            TipoPromoList.Add(New TipoPromocaoViewModel(0, "Normal"))
            TipoPromoList.Add(New TipoPromocaoViewModel(1, "Combinada"))
            TipoPromoList.Add(New TipoPromocaoViewModel(2, "Faixas Multiplas"))
            ViewBag.ListaTipoPromoca = TipoPromoList
            '------------------------------------------------------------------------------------------------------------
            'Carrega Drop Forma de Calculo
            Dim formaDeCalculoList As List(Of FormaDeCalculoViewModel) = New List(Of FormaDeCalculoViewModel)

            ViewBag.formaDeCalculo = formaDeCalculoBo.CarregaDropFormaDeCalculo(DESEQCCALCTRPMC)

            '------------------------------------------------------------------------------------------------------------
            'Carrega tipo de Premio

            ViewBag.tipoPremio = ListaTipoPremio()

            '------------------------------------------------------------------------------------------------------------
            'Carrega Drop Critério

            ViewBag.criterio = tipoCriterioBO.CarregaDropTipoCriterio("", 0, 4)

            '------------------------------------------------------------------------------------------------------------
            'Carrega Drop Critério
            Dim tipoCriterioList As List(Of RelacaoAcaoMecanicaViewModel) = New List(Of RelacaoAcaoMecanicaViewModel)

            ViewBag.tipoCriterioSimplesList = tipoCriterioList
        End Sub

        Public Shared Function ListaTipoPremio() As List(Of SelectListItem)
            Dim listarTipoPremio As New List(Of SelectListItem)()
            listarTipoPremio.Add(New SelectListItem() With {.Text = "0 - Brinde", .Value = 0})
            listarTipoPremio.Add(New SelectListItem() With {.Text = "1 - Percentual Cash Back ", .Value = 1})
            listarTipoPremio.Add(New SelectListItem() With {.Text = "2 - Valor Cash Back", .Value = 2})
            Return listarTipoPremio
        End Function


        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal relacaoAcaoMecanicaViewModel As RelacaoAcaoMecanicaViewModel) As ActionResult
            Try
                If (ModelState.IsValid) Then
                    Dim relacaoAcaoMecanica = Mapper.Map(Of RelacaoAcaoMecanicaViewModel, VO.RelacaoAcaoMecanica)(relacaoAcaoMecanicaViewModel)

                    If (relacaoAcaoMecanicaViewModel.RELACAOACAOMECANICAREGISTROSLIST IsNot Nothing) Then
                        relacaoAcaoMecanica.RelacaoAcaoMecanicaCriterioDeApuracaoList = Mapper.Map(Of List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel), List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao))(relacaoAcaoMecanicaViewModel.RELACAOACAOMECANICAREGISTROSLIST)
                    ElseIf (relacaoAcaoMecanicaViewModel.RELACAOACAOMECANICAREGISTROSCOMBINADALIST IsNot Nothing) Then
                        relacaoAcaoMecanica.RelacaoAcaoMecanicaCriterioDeApuracaoList = Mapper.Map(Of List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel), List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao))(relacaoAcaoMecanicaViewModel.RELACAOACAOMECANICAREGISTROSCOMBINADALIST)
                    ElseIf relacaoAcaoMecanicaViewModel.RELACAOACAOMECANICAREGISTROSFAIXALIST IsNot Nothing Then
                        relacaoAcaoMecanica.RelacaoAcaoMecanicaCriterioDeApuracaoList = Mapper.Map(Of List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel), List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao))(relacaoAcaoMecanicaViewModel.RELACAOACAOMECANICAREGISTROSFAIXALIST)
                    End If


                    Dim CODACOCMC = relacaoAcaoMecanicaViewModel.CODACOCMC
                    Dim NUMSEQRGRACOCMC = relacaoAcaoMecanicaViewModel.NUMSEQRGRACOCMC

                    Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
                    relacaoAcaoMecanica = BO.Save(CODACOCMC, NUMSEQRGRACOCMC, relacaoAcaoMecanica)

                    relacaoAcaoMecanicaViewModel = Mapper.Map(Of VO.RelacaoAcaoMecanica, RelacaoAcaoMecanicaViewModel)(relacaoAcaoMecanica)

                    Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                    ControlaVisibilidadeComponentes(acaoCache)

                    Return Json(relacaoAcaoMecanicaViewModel)
                End If

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        'Editar Grid de Registros do Tipo Normal
        <HttpGet()>
        Function EditMecanicaPromocaoGridRegistros(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal) As PartialViewResult
            Try
                Dim BOCriterioDeApuracao = New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
                Dim list As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao) = BOCriterioDeApuracao.EditRelacaoAcaoMecanicaCriterioDeApuracaoList(CODACOCMC, NUMSEQRGRACOCMC, TIPPMC)
                Dim listViewModel As List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao), List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel))(list)

                Dim objViewModel As New RelacaoAcaoMecanicaViewModel() With {
                    .TIPPMC = TIPPMC,
                    .RELACAOACAOMECANICAREGISTROSLIST = listViewModel
                }
                If (Not listViewModel.FirstOrDefault() Is Nothing) Then
                    objViewModel.QDEPTOFXACTRPMC = listViewModel.First().QDEPTOFXACTRPMC
                    objViewModel.TIPEQCCALCTRPMC = listViewModel.First().TIPEQCCALCTRPMC
                    objViewModel.CODCTRPMC = listViewModel.First().CODCTRPMC
                    objViewModel.INDTIPPTOPMC = listViewModel.First().INDTIPPTOPMC
                    objViewModel.VLRLIMMAXCTR = listViewModel.First().VLRLIMMAXCTR

                End If

                If (listViewModel.Count <> 1) Then

                    listViewModel.Add(New RelacaoAcaoMecanicaCriterioDeApuracaoViewModel() With {
                                .CODACOCMC = CODACOCMC,
                                .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC,
                                .NUMSEQCTRACOCMC = 1,
                                .TIPEDENIVPRMPMC = 0,
                                .CODCTRPMC = 2
                        })
                End If

                CarregaDropCreateEditRelacaoAcaoMecanica("")

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_GridRegistros.vbhtml", objViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        'Editar Grid de Registros do Tipo Combinadas
        <HttpGet()>
        Function EditMecanicaPromocaoGridRegistrosCombinada(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal QDEGRPMIXPMC As Decimal, ByVal TIPPMC As Decimal) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BOCriterioDeApuracao = New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
                Dim list As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao) = BOCriterioDeApuracao.EditRelacaoAcaoMecanicaCriterioDeApuracaoList(CODACOCMC, NUMSEQRGRACOCMC, TIPPMC)

                Dim listViewModel As List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao), List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel))(list)


                Dim objViewModel = New RelacaoAcaoMecanicaViewModel() With {
                    .TIPPMC = TIPPMC,
                    .RELACAOACAOMECANICAREGISTROSCOMBINADALIST = listViewModel
                }
                If (Not listViewModel.FirstOrDefault() Is Nothing) Then
                    objViewModel.QDEPTOFXACTRPMC = listViewModel.First().QDEPTOFXACTRPMC
                    objViewModel.TIPEQCCALCTRPMC = listViewModel.First().TIPEQCCALCTRPMC
                    objViewModel.CODCTRPMC = listViewModel.First().CODCTRPMC
                    objViewModel.INDTIPPTOPMC = listViewModel.First().INDTIPPTOPMC
                    objViewModel.VLRLIMMAXCTR = listViewModel.First().VLRLIMMAXCTR
                End If

                If (listViewModel.Count > QDEGRPMIXPMC) Then 'Diminuiu a quantidade de faixas/Grupos
                    objViewModel.RELACAOACAOMECANICAREGISTROSCOMBINADALIST = listViewModel.Take(QDEGRPMIXPMC).ToList()
                ElseIf (listViewModel.Count < QDEGRPMIXPMC) Then ' Aumentou a quantidade de faixas/Grupos
                    Dim index As Integer = listViewModel.Count
                    While (index < QDEGRPMIXPMC)
                        index = index + 1
                        listViewModel.Add(New RelacaoAcaoMecanicaCriterioDeApuracaoViewModel() With {
                                .CODACOCMC = CODACOCMC,
                                .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC,
                                .NUMSEQCTRACOCMC = index,
                                .TIPEDENIVPRMPMC = index,
                                .CODCTRPMC = 2
                        })
                    End While
                End If

                ControlaVisibilidadeComponentes(acaoCache)

                CarregaDropCreateEditRelacaoAcaoMecanica("")

                Return PartialView("~/Views/AcaoComercial/Mecanica/_GridRegistrosCombinada.vbhtml", objViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Editar Grid de Registros do Tipo Faixas Multiplas
        <HttpGet()>
        Function EditMecanicaPromocaoGridRegistrosFaixa(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal QDEFXAPMC As Decimal, ByVal TIPPMC As Decimal) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BOCriterioDeApuracao = New RelacaoAcaoMecanicaCriterioDeApuracaoBO()

                Dim list As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao) = BOCriterioDeApuracao.EditRelacaoAcaoMecanicaCriterioDeApuracaoList(CODACOCMC, NUMSEQRGRACOCMC, TIPPMC)
                Dim listViewModel As List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao), List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel))(list)

                Dim objViewModel As RelacaoAcaoMecanicaViewModel = New RelacaoAcaoMecanicaViewModel() With {
                    .TIPPMC = TIPPMC,
                    .RELACAOACAOMECANICAREGISTROSFAIXALIST = listViewModel
                }
                If (Not listViewModel.FirstOrDefault() Is Nothing) Then
                    objViewModel.QDEPTOFXACTRPMC = listViewModel.First().QDEPTOFXACTRPMC
                    objViewModel.CODCTRPMC = listViewModel.First().CODCTRPMC
                    'objViewModel.TIPEQCCALCTRPMC = listViewModel.First().TIPEQCCALCTRPMC
                    objViewModel.INDTIPPTOPMC = listViewModel.First().INDTIPPTOPMC
                    objViewModel.VLRLIMMAXCTR = listViewModel.First().VLRLIMMAXCTR
                End If
                objViewModel.TIPEQCCALCTRPMC = 2

                If (listViewModel.Count > QDEFXAPMC) Then 'Diminuiu a quantidade de faixas/Grupos
                    objViewModel.RELACAOACAOMECANICAREGISTROSFAIXALIST = listViewModel.Take(QDEFXAPMC).ToList()
                ElseIf (listViewModel.Count < QDEFXAPMC) Then ' Aumentou a quantidade de faixas/Grupos
                    Dim index As Integer = listViewModel.Count
                    While (index < QDEFXAPMC)
                        index = index + 1
                        listViewModel.Add(New RelacaoAcaoMecanicaCriterioDeApuracaoViewModel() With {
                                .CODACOCMC = CODACOCMC,
                                .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC,
                                .NUMSEQCTRACOCMC = index,
                                .TIPEDENIVPRMPMC = index,
                                .CODCTRPMC = 2
                        })
                    End While
                End If

                ControlaVisibilidadeComponentes(acaoCache)

                CarregaDropCreateEditRelacaoAcaoMecanica("")

                If (objViewModel.RELACAOACAOMECANICAREGISTROSFAIXALIST.Count > 0) Then
                    objViewModel.RELACAOACAOMECANICAREGISTROSFAIXALIST.Last().VLRLIMMAXFXACTRPMC = 9999999999
                End If

                Return PartialView("~/Views/AcaoComercial/Mecanica/_GridRegistrosFaixa.vbhtml", objViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function EditMecanicaPromocaoWidgetPublicoAlvo(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO As RelacaoAcaoMecanicaPublicoAlvoBO = New RelacaoAcaoMecanicaPublicoAlvoBO()

                Dim obj As VO.RelacaoAcaoMecanicaPublicoAlvo = BO.SelectCountPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC)

                Dim objPublicoAlvo As RelacaoAcaoMecanicaPublicoAlvoViewModel = Mapper.Map(Of VO.RelacaoAcaoMecanicaPublicoAlvo, RelacaoAcaoMecanicaPublicoAlvoViewModel)(obj)

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetMecanicaPromocaoPublicoAlvo.vbhtml", objPublicoAlvo)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function EditMecanicaPromocaoWidgetMix(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal, ByVal INDPTCTOTMIXPMC As Decimal) As PartialViewResult
            Try

                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                Dim obj As VO.RelacaoAcaoMecanicaMix = BO.SelectCountMix(CODACOCMC, NUMSEQRGRACOCMC)

                Dim objMix As RelacaoAcaoMecanicaMixViewModel = Mapper.Map(Of VO.RelacaoAcaoMecanicaMix, RelacaoAcaoMecanicaMixViewModel)(obj)


                'Controle Visibilidade se Participação Mix Total Sim ou Não
                ViewBag.ParticipacaoMixTotal = INDPTCTOTMIXPMC
                ViewBag.TipoPromocao = TIPPMC


                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                ViewBag.INDEVTSMA = acaoCache.INDEVTSMA

                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetMecanicaPromocaoMix.vbhtml", objMix)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function EditMecanicaPromocaoWidgetClientesExcludentes(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO As RelacaoAcaoMecanicaClientesExcludentesBO = New RelacaoAcaoMecanicaClientesExcludentesBO()

                Dim obj As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes) = BO.ListT(New VO.RelacaoAcaoMecanicaClientesExcludentes With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

                Dim objClientesExcludentes As List(Of RelacaoAcaoMecanicaClientesExcludentesViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaClientesExcludentes), List(Of RelacaoAcaoMecanicaClientesExcludentesViewModel))(obj)

                ControlaVisibilidadeComponentes(acaoCache)

                If (objClientesExcludentes.Any()) Then
                    ViewBag.NenhumClienteExcludenteSelecionado = False
                Else
                    ViewBag.NenhumClienteExcludenteSelecionado = True
                End If

                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetMecanicaPromocaoClientesExcludentes.vbhtml", objClientesExcludentes)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo Deletar dados
        <HttpPost()>
        Function DeleteT(ByVal relacaoAcaoMecanicaViewModel As RelacaoAcaoMecanicaViewModel) As ActionResult
            Try
                Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()

                Dim relacaoAcaoMecanica = Mapper.Map(Of RelacaoAcaoMecanicaViewModel, VO.RelacaoAcaoMecanica)(relacaoAcaoMecanicaViewModel)

                BO.DeleteRelacaoAcaoMecanica(relacaoAcaoMecanica)

                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function

        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            ViewBag.PermiteEdicaoMecanica = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)

        End Sub




        <HttpPost()>
        Function ModalClonarMecanica(ByVal id As Decimal) As PartialViewResult
            Try

                Dim dadosPesquisa As New GridSettings(Of RelacaoAcaoMecanicaViewModel) With {
                    .Filter = New RelacaoAcaoMecanicaViewModel(),
                    .Page = New PageSettings() With {
                        .OrderBy = "CODACOCMCD",
                        .PageNumber = 1,
                        .PageSize = 100
                    }
                }


                Dim relacaoViewModel As New List(Of RelacaoAcaoMecanicaViewModel)()
                Dim gridConf = PaginarEAtualizarGrid(relacaoViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)


                gridConf.ActionName = "ClonarMecanicaResultados"
                gridConf.ControllerName = "RelacaoAcaoMecanica"


                Dim TipoPromoList As List(Of TipoPromocaoViewModel) = New List(Of TipoPromocaoViewModel)

                TipoPromoList.Add(New TipoPromocaoViewModel(Nothing, "Todos"))
                TipoPromoList.Add(New TipoPromocaoViewModel(0, "Normal"))
                TipoPromoList.Add(New TipoPromocaoViewModel(1, "Combinada"))
                TipoPromoList.Add(New TipoPromocaoViewModel(2, "Faixas Multiplas"))
                ViewBag.ListaTipoPromoca = TipoPromoList

                Return PartialView("~/Views/AcaoComercial/Mecanica/RelacaoAcaoMecanica/_ClonarMecanica.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        <HttpPost()>
        Public Function ClonarMecanicaResultados(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaViewModel)) As PartialViewResult
            Try
                Dim filtro As VO.RelacaoAcaoMecanica = Mapper.Map(Of RelacaoAcaoMecanicaViewModel, VO.RelacaoAcaoMecanica)(dadosPesquisa.Filter)

                Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
                Dim relacao As List(Of VO.RelacaoAcaoMecanica) = BO.PesquisarAcaoMecanica(filtro)

                Dim relacaoViewModel As List(Of RelacaoAcaoMecanicaViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanica), List(Of RelacaoAcaoMecanicaViewModel))(relacao)


                If (dadosPesquisa.Page.OrderBy Is Nothing) Then
                    dadosPesquisa.Page.OrderBy = "CODACOCMCD"
                End If

                Dim gridConf = PaginarEAtualizarGrid(relacaoViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If
                gridConf.ActionName = "ClonarMecanicaResultados"
                gridConf.ControllerName = "RelacaoAcaoMecanica"
                Return PartialView("~/Views/AcaoComercial/Mecanica/RelacaoAcaoMecanica/_ClonarMecanicaResultadosView.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        <HttpPost()>
        Public Function ClonarMecanica(ByVal CODACOCMC_Copy As Decimal, ByVal NUMSEQRGRACOCMC_Copy As Decimal, ByVal CODACOCMC_Paste As Decimal) As ActionResult
            Try

                Dim acaoMecanicaBO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()

                Dim acaoMecanica As VO.RelacaoAcaoMecanica = acaoMecanicaBO.ClonarMecanica(CODACOCMC_Copy, NUMSEQRGRACOCMC_Copy, CODACOCMC_Paste)

                Dim relacaoAcaoMecanicaViewModel As RelacaoAcaoMecanicaViewModel = Mapper.Map(Of VO.RelacaoAcaoMecanica, RelacaoAcaoMecanicaViewModel)(acaoMecanica)

                Return Json(relacaoAcaoMecanicaViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

    End Class

End Namespace
