﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class RelacaoAcaoMecanicaPublicoAlvoNegocioClienteController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoNegocioCliente(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO = New RelacaoAcaoMecanicaPublicoAlvoNegocioClienteBO()
                Dim relacaoAcaoMecanicaPublicoAlvoNegocioClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente) = BO.ListarNegocioCliente(CODACOCMC, NUMSEQRGRACOCMC)
                Dim objRelacaoAcaoMecanicaPublicoAlvoNegocioClienteList As List(Of RelacaoAcaoMecanicaPublicoAlvoNegocioClienteViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente), List(Of RelacaoAcaoMecanicaPublicoAlvoNegocioClienteViewModel))(relacaoAcaoMecanicaPublicoAlvoNegocioClienteList)

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalNegocioCliente.vbhtml", objRelacaoAcaoMecanicaPublicoAlvoNegocioClienteList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal obj As List(Of RelacaoAcaoMecanicaPublicoAlvoNegocioClienteViewModel)) As PartialViewResult
            Dim BO As RelacaoAcaoMecanicaPublicoAlvoNegocioClienteBO = New RelacaoAcaoMecanicaPublicoAlvoNegocioClienteBO()
            Try
                If (ModelState.IsValid) Then
                    Dim relacaoAcaoMecanicaPublicoAlvoNegocioClienteList = Mapper.Map(Of List(Of RelacaoAcaoMecanicaPublicoAlvoNegocioClienteViewModel), List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente))(obj)

                    BO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, relacaoAcaoMecanicaPublicoAlvoNegocioClienteList)

                    Return Nothing
                End If


                Throw New WebFaultException(HttpStatusCode.BadRequest)
                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function

        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            ViewBag.PermiteEdicaoMecanicaPublicoAlvoNegocioCliente = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)

        End Sub
    End Class
End Namespace
