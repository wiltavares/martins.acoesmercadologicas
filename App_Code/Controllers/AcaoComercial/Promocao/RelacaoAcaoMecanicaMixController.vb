﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports System.ServiceModel.Web
Imports System.Net
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    <RoutePrefix(“RelacaoAcaoMecanicaMix”)>
    Public Class RelacaoAcaoMecanicaMixController
        Inherits AbstractController

        Function Index() As RedirectResult
            Return New RedirectResult("Home")
        End Function
#Region "Mix Grupo Mercadoria"
        <HttpPost()>
        Public Function EditMecanicaPromocaoMixGrupoMercadoria(obj As VO.RelacaoAcaoMecanicaMix) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                Dim CODACOCMC = obj.CODACOCMC
                Dim NUMSEQRGRACOCMC = obj.NUMSEQRGRACOCMC

                Dim GrupoMercadoriaList As List(Of VO.RelacaoAcaoMecanicaMix) = BO.EditMixGrupoMercadoria(CODACOCMC, NUMSEQRGRACOCMC)
                Dim GrupoMercadoriaViewModelList As List(Of RelacaoAcaoMecanicaMixViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaMix), List(Of RelacaoAcaoMecanicaMixViewModel))(GrupoMercadoriaList)

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalMixGrupoMercadoria.vbhtml", GrupoMercadoriaViewModelList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados Grupo Mercadoria
        <HttpPost()>
        Function SaveMixGrupoMercadoria(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal obj As List(Of RelacaoAcaoMecanicaMixViewModel)) As ActionResult
            Dim relacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

            Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

            Dim relacaoAcaoMecanicaMixGrupoMercadoria = Mapper.Map(Of List(Of RelacaoAcaoMecanicaMixViewModel), List(Of VO.RelacaoAcaoMecanicaMix))(obj)

            BO.InsertListMixGrupoMercadoria(CODACOCMC, NUMSEQRGRACOCMC, relacaoAcaoMecanicaMixGrupoMercadoria)

            Return Nothing

        End Function
#End Region

#Region "Mix Familia Mercadoria"
        <HttpPost()>
        Public Function EditMecanicaPromocaoMixFamiliaMercadoria() As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim obj As RelacaoAcaoMecanicaMixViewModel = New RelacaoAcaoMecanicaMixViewModel()

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalMixFamiliaMercadoria.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function CarregarWidgetLoadPesquisaMixFamiliaMercadoria(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                'atribui no view bag as configs de edição/visualizacao
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)


                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of RelacaoAcaoMecanicaMixViewModel)
                gridConf.Filter = New RelacaoAcaoMecanicaMixViewModel()
                gridConf.Filter.CODACOCMC = CODACOCMC
                gridConf.Filter.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                gridConf.ActionName = "PesquisaMixFamiliaMercadoria"
                gridConf.ResultID = "gridItensPesquisaMixFamiliaMercadoria"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "DESFMLMERC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of RelacaoAcaoMecanicaMixViewModel), gridConf.Page)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetLoadPesquisaMixFamiliaMercadoria.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMixFamiliaMercadoria(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaMixViewModel)) As PartialViewResult
            Try
                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                Dim filtro As VO.RelacaoAcaoMecanicaMix = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.RelacaoAcaoMecanicaMix) = BO.PesquisaMixFamiliaMercadoria(filtro)
                Dim viewModeList As List(Of RelacaoAcaoMecanicaMixViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaMix), List(Of RelacaoAcaoMecanicaMixViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaMixViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaMixFamiliaMercadoria"
                dadosPesquisa.PaginationID = "gridItensPesquisaMixFamiliaMercadoria"
                dadosPesquisa.ActionName = "PesquisaMixFamiliaMercadoria"
                dadosPesquisa.ControllerName = "RelacaoAcaoMecanicaMix"


                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_GridPesquisaMixFamiliaMercadoria.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function CarregarWidgetMixFamiliaMercadoriaAdicionados(VO As VO.RelacaoAcaoMecanicaMix) As PartialViewResult
            Try
                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim list As List(Of VO.RelacaoAcaoMecanicaMix) = BO.ListarMixFamiliaMercadoriaAdicionados(VO)
                'ViewBag.ItensLoadPesquisaMixGrupoMercadoria = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaMix), List(Of RelacaoAcaoMecanicaMixViewModel))(list)

                Dim objViewModel As RelacaoAcaoMecanicaMixViewModel = New RelacaoAcaoMecanicaMixViewModel()
                objViewModel.ListMixFamiliaMercadoria = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaMix), List(Of RelacaoAcaoMecanicaMixViewModel))(list)

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetMixFamiliaMercadoria.vbhtml", objViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Adiciona o resultado da pesquisa ao mix.
        <HttpPost()>
        Function AddPesquisaMixFamiliaMercadoria(filtro As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult
            Try
                Dim filtroVO As VO.RelacaoAcaoMecanicaMix = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(filtro)

                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()
                BO.AddPesquisaMixFamiliaMercadoria(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados Mix Familia Mercadoria
        <HttpPost()>
        Function SaveMixFamiliaMercadoria(ByVal obj As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult
            Dim BO = New RelacaoAcaoMecanicaMixBO()

            Dim objFamiliaMercadoriaExiste As VO.RelacaoAcaoMecanicaMix = BO.SelectT(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = obj.CODACOCMC, .NUMSEQRGRACOCMC = obj.NUMSEQRGRACOCMC, .CODFMLMER = obj.CODFMLMER})

            If (objFamiliaMercadoriaExiste IsNot Nothing) Then
                Warning(Constantes.REGISTRO_DUPLICADO)
                Return Nothing
            End If

            Dim FamiliaMercadoria As VO.RelacaoAcaoMecanicaMix = BO.SelectMaxNumSeqMixAcoCmcFamiliaMercadoria(obj.CODACOCMC, obj.NUMSEQRGRACOCMC)

            obj.NUMSEQMIXACOCMC = FamiliaMercadoria.NUMSEQMIXACOCMC

            Dim relacaoAcaoMecanicMixFamiliaMercadoria = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(obj)

            BO.InsertT(relacaoAcaoMecanicMixFamiliaMercadoria)

            Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

            Return Nothing

        End Function

        'Deletar Itens Mix Familia Mercadoria
        <HttpPost()>
        Function DeleteMixFamiliaMercadoria(ByVal obj As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult
            Dim BO = New RelacaoAcaoMecanicaMixBO()

            If (obj.CODACOCMC Is Nothing Or obj.NUMSEQRGRACOCMC Is Nothing) Then
                Warning("Campos obrigatórios não informados")
                Return Nothing
            End If


            Dim relacaoAcaoMecanicaMixFamiliaMercadoria = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(obj)
            BO.DeleteMixFamiliaMercadoria(relacaoAcaoMecanicaMixFamiliaMercadoria)
            Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
            Return Nothing

        End Function

        'Importar Planila Execel
        <HttpPost()>
        Public Function ImportarXlsMixFamiliaMercadoria(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal QDEGRPMIXPMC As Decimal) As String
            Dim relacaoAcaoMecanicaMixBO As New RelacaoAcaoMecanicaMixBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseMixFamiliaMercadoria"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseMixFamiliaMercadoria"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = relacaoAcaoMecanicaMixBO.ImportarExelMixFamiliaMercadoria(savedFileName, CODACOCMC, NUMSEQRGRACOCMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseMixFamiliaMercadoria"
            Else
                Return "TrueMixFamiliaMercadoria"
            End If
        End Function
#End Region

#Region "Mix Classe Mercadoria"
        <HttpPost()>
        Public Function EditMecanicaPromocaoMixClasseMercadoria() As PartialViewResult
            Try
                'Controle de visibilidade de tela
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Dim obj As RelacaoAcaoMecanicaMixViewModel = New RelacaoAcaoMecanicaMixViewModel()

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixClasseMercadoria/_ModalMixClasseMercadoria.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function CarregarWidgetLoadPesquisaMixClasseMercadoria(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                'atribui no view bag as configs de edição/visualizacao
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)


                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of RelacaoAcaoMecanicaMixViewModel)
                gridConf.Filter = New RelacaoAcaoMecanicaMixViewModel()
                gridConf.Filter.CODACOCMC = CODACOCMC
                gridConf.Filter.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                gridConf.ActionName = "PesquisaMixClasseMercadoria"
                gridConf.ResultID = "gridItensPesquisaMixClasseMercadoria"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "DESCLSMERC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of RelacaoAcaoMecanicaMixViewModel), gridConf.Page)

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixClasseMercadoria/_WidgetLoadPesquisaMixClasseMercadoria.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMixClasseMercadoria(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaMixViewModel)) As PartialViewResult
            Try
                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                Dim filtro As VO.RelacaoAcaoMecanicaMix = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.RelacaoAcaoMecanicaMix) = BO.PesquisaMixClasseMercadoria(filtro)
                Dim viewModeList As List(Of RelacaoAcaoMecanicaMixViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaMix), List(Of RelacaoAcaoMecanicaMixViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaMixViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaMixClasseMercadoria"
                dadosPesquisa.PaginationID = "gridItensPesquisaMixClasseMercadoria"
                dadosPesquisa.ActionName = "PesquisaMixClasseMercadoria"
                dadosPesquisa.ControllerName = "RelacaoAcaoMecanicaMix"

                'Controle de visibilidade de tela
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixClasseMercadoria/_GridPesquisaMixClasseMercadoria.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function CarregarWidgetMixClasseMercadoriaAdicionados(VO As VO.RelacaoAcaoMecanicaMix) As PartialViewResult
            Try
                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                'Controle de visibilidade de tela
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Dim list As List(Of VO.RelacaoAcaoMecanicaMix) = BO.ListarMixClasseMercadoriaAdicionados(VO)
                Dim listViewModel As List(Of RelacaoAcaoMecanicaMixViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaMix), List(Of RelacaoAcaoMecanicaMixViewModel))(list)

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixClasseMercadoria/_WidgetMixClasseMercadoria.vbhtml", listViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Adiciona o resultado da pesquisa ao mix.
        <HttpPost()>
        Function AddPesquisaMixClasseMercadoria(filtro As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult
            Try
                Dim filtroVO As VO.RelacaoAcaoMecanicaMix = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(filtro)

                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()
                BO.AddPesquisaMixClasseMercadoria(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados Mix Classe Mercadoria
        <HttpPost()>
        Function SaveMixClasseMercadoria(ByVal obj As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult
            Dim BO = New RelacaoAcaoMecanicaMixBO()

            Dim objClasseMercadoriaExiste As VO.RelacaoAcaoMecanicaMix = BO.SelectT(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = obj.CODACOCMC, .NUMSEQRGRACOCMC = obj.NUMSEQRGRACOCMC, .CODCLSMER = obj.CODCLSMER})

            If (objClasseMercadoriaExiste IsNot Nothing) Then
                Warning(Constantes.REGISTRO_DUPLICADO)
                Return Nothing
            End If

            'Obter max NUMSEQMIXACOCMC
            Dim objClasseMercadoria As VO.RelacaoAcaoMecanicaMix = BO.SelectMaxNumSeqMixAcoCmc(obj.CODACOCMC, obj.NUMSEQRGRACOCMC)
            obj.NUMSEQMIXACOCMC = objClasseMercadoria.NUMSEQMIXACOCMC

            Dim relacaoAcaoMecanicMixClasseMercadoria = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(obj)

            BO.InsertT(relacaoAcaoMecanicMixClasseMercadoria)

            Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

            Return Nothing

        End Function

        'Deletar Itens Mix Classe Mercadoria
        <HttpPost()>
        Function DeleteMixClasseMercadoria(ByVal obj As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult

            If (obj.CODACOCMC Is Nothing Or obj.NUMSEQRGRACOCMC Is Nothing) Then
                Warning("Campos obrigatórios não informados")
                Return Nothing
            End If


            Dim BO = New RelacaoAcaoMecanicaMixBO()

            Dim relacaoAcaoMecanicaMixClasseMercadoria = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(obj)
            BO.DeleteMixClasseMercadoria(relacaoAcaoMecanicaMixClasseMercadoria)
            Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
            Return Nothing

        End Function

        'Importar Planila Execel
        <HttpPost()>
        Public Function ImportarXlsMixClasseMercadoria(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal QDEGRPMIXPMC As Decimal) As String
            Dim relacaoAcaoMecanicaMixBO As New RelacaoAcaoMecanicaMixBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseMixClasseMercadoria"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseMixClasseMercadoria"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = relacaoAcaoMecanicaMixBO.ImportarExelMixClasseMercadoria(savedFileName, CODACOCMC, NUMSEQRGRACOCMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseMixClasseMercadoria"
            Else
                Return "TrueMixClasseMercadoria"
            End If
        End Function
#End Region

#Region "Mix Mercadoria"
        <HttpPost()>
        Public Function EditMecanicaPromocaoMixMercadoria() As PartialViewResult
            Try
                'Controle de visibilidade de tela
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Dim obj As RelacaoAcaoMecanicaMixViewModel = New RelacaoAcaoMecanicaMixViewModel()

                If (acaoCache.Fornecedores IsNot Nothing AndAlso acaoCache.Fornecedores.Any()) Then
                    obj.TIPFRNACOCMC = acaoCache.Fornecedores.FirstOrDefault().TIPFRNACOCMC
                End If

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixMercadoria/_ModalMixMercadoria.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function CarregarWidgetLoadPesquisaMixMercadoria(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal, ByVal QTDFXAGRP As Decimal) As PartialViewResult
            Try

                'atribui no view bag as configs de edição/visualizacao
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)


                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of RelacaoAcaoMecanicaMixViewModel)
                gridConf.Filter = New RelacaoAcaoMecanicaMixViewModel()
                gridConf.Filter.CODACOCMC = CODACOCMC
                gridConf.Filter.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                gridConf.ActionName = "PesquisaMixMercadoria"
                gridConf.ResultID = "gridItensPesquisaMixMercadoria"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "DESMERC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of RelacaoAcaoMecanicaMixViewModel), gridConf.Page)

                If (acaoCache.Fornecedores IsNot Nothing AndAlso acaoCache.Fornecedores.Any()) Then
                    gridConf.Filter.TIPFRNACOCMC = acaoCache.Fornecedores.FirstOrDefault().TIPFRNACOCMC
                End If

                ViewBag.TIPPMC = TIPPMC
                If (TIPPMC = 1) Then
                    ViewBag.QTDFXAGRP = QTDFXAGRP
                Else
                    ViewBag.QTDFXAGRP = 0
                End If

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixMercadoria/_WidgetLoadPesquisaMixMercadoria.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMixMercadoria(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaMixViewModel)) As PartialViewResult
            Try
                If Not ValidaFiltroMixMercadoria(dadosPesquisa.Filter) Then
                    Return Nothing
                End If
                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                Dim filtro As VO.RelacaoAcaoMecanicaMix = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.RelacaoAcaoMecanicaMix)
                If (filtro.TIPFRNACOCMC = 1) Then
                    voList = BO.PesquisaMixMercadoriaEdl(filtro)
                Else
                    voList = BO.PesquisaMixMercadoria(filtro)
                End If

                Dim viewModeList As List(Of RelacaoAcaoMecanicaMixViewModel)
                viewModeList = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaMix), List(Of RelacaoAcaoMecanicaMixViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaMixViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaMixMercadoria"
                dadosPesquisa.PaginationID = "gridItensPesquisaMixMercadoria"
                dadosPesquisa.ActionName = "PesquisaMixMercadoria"
                dadosPesquisa.ControllerName = "RelacaoAcaoMecanicaMix"

                'Controle de visibilidade de tela
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixMercadoria/_GridPesquisaMixMercadoria.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function CarregarWidgetMixMercadoriaAdicionados(VO As VO.RelacaoAcaoMecanicaMix) As PartialViewResult
            Try
                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                'Controle de visibilidade de tela
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                If (acaoCache.Fornecedores IsNot Nothing AndAlso acaoCache.Fornecedores.Any()) Then
                    VO.TIPFRNACOCMC = acaoCache.Fornecedores.FirstOrDefault().TIPFRNACOCMC
                End If

                Dim list As List(Of VO.RelacaoAcaoMecanicaMix)
                If (VO.TIPFRNACOCMC = 1) Then
                    list = BO.ListarMixMercadoriaEdlAdicionados(VO)
                Else
                    list = BO.ListarMixMercadoriaAdicionados(VO)
                End If

                Dim listViewModel As List(Of RelacaoAcaoMecanicaMixViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaMix), List(Of RelacaoAcaoMecanicaMixViewModel))(list)


                Return PartialView("~/Views/AcaoComercial/Mecanica/MixMercadoria/_WidgetMixMercadoria.vbhtml", listViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Adiciona o resultado da pesquisa ao mix.
        <HttpPost()>
        Function AddPesquisaMixMercadoria(filtro As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult
            Try

                If Not ValidaFiltroMixMercadoria(filtro) Then
                    Return Nothing
                End If

                Dim filtroVO As VO.RelacaoAcaoMecanicaMix = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(filtro)

                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()
                BO.AddPesquisaMixMercadoria(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados Mix Mercadoria
        <HttpPost()>
        Function SaveMixMercadoria(ByVal obj As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult
            Dim BO = New RelacaoAcaoMecanicaMixBO()

            Dim objMercadoriaExiste As VO.RelacaoAcaoMecanicaMix = BO.SelectT(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = obj.CODACOCMC, .NUMSEQRGRACOCMC = obj.NUMSEQRGRACOCMC, .CODMER = obj.CODMER})

            If (objMercadoriaExiste IsNot Nothing) Then
                Warning(Constantes.REGISTRO_DUPLICADO)
                Return Nothing
            End If

            'Obter max NUMSEQMIXACOCMC
            Dim objMercadoria As VO.RelacaoAcaoMecanicaMix = BO.SelectMaxNumSeqMixAcoCmc(obj.CODACOCMC, obj.NUMSEQRGRACOCMC)
            obj.NUMSEQMIXACOCMC = objMercadoria.NUMSEQMIXACOCMC

            Dim relacaoAcaoMecanicMixMercadoria = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(obj)

            BO.InsertT(relacaoAcaoMecanicMixMercadoria)
            Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

            Return Nothing
        End Function

        'Deletar Itens Mix Mercadoria
        <HttpPost()>
        Function DeleteMixMercadoria(ByVal obj As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult

            If (obj.CODACOCMC Is Nothing Or obj.NUMSEQRGRACOCMC Is Nothing) Then
                Warning("Campos obrigatórios não informados")
                Return Nothing
            End If


            Dim BO = New RelacaoAcaoMecanicaMixBO()

            Dim relacaoAcaoMecanicaMixMercadoria = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(obj)
            BO.DeleteMixMercadoria(relacaoAcaoMecanicaMixMercadoria)
            Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
            Return Nothing

        End Function

        'Importar Planila Execel
        <HttpPost()>
        Public Function ImportarXlsMixMercadoria(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal QDEGRPMIXPMC As Decimal) As String
            Dim relacaoAcaoMecanicaMixBO As New RelacaoAcaoMecanicaMixBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseMixMercadoria"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseMixMercadoria"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = relacaoAcaoMecanicaMixBO.ImportarExelMixMercadoria(savedFileName, CODACOCMC, NUMSEQRGRACOCMC, QDEGRPMIXPMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseMixMercadoria"
            Else
                Return "TrueMixMercadoria"
            End If
        End Function

        Private Function ValidaFiltroMixMercadoria(ByRef filtro As RelacaoAcaoMecanicaMixViewModel) As Boolean

            If filtro.CODMER Is Nothing And filtro.DESMER Is Nothing And
                filtro.CODGRPMER Is Nothing And filtro.DESGRPMER Is Nothing And
                filtro.CODFMLMER Is Nothing And filtro.DESFMLMER Is Nothing And
                filtro.CODCLSMER Is Nothing And filtro.DESCLSMER Is Nothing And
                filtro.CODFRN Is Nothing And filtro.NOMFRN Is Nothing Then
                Warning("Informe pelo menos um filtro")
                Return False
            End If

            If Not filtro.DESMER Is Nothing Then
                If (filtro.DESMER.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome da mercadoria")
                    Return False
                End If
            End If
            If Not filtro.DESGRPMER Is Nothing Then
                If (filtro.DESGRPMER.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do grupo de mercadoria")
                    Return False
                End If
            End If
            If Not filtro.DESFMLMER Is Nothing Then
                If (filtro.DESFMLMER.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome da familia de mercadoria")
                    Return False
                End If
            End If
            If Not filtro.DESCLSMER Is Nothing Then
                If (filtro.DESCLSMER.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome da classe de mercadoria")
                    Return False
                End If
            End If
            If Not filtro.NOMFRN Is Nothing Then
                If (filtro.NOMFRN.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do fornecedor")
                    Return False
                End If
            End If
            Return True
        End Function


        <HttpGet()>
        Function CarregarWidgetLoadPesquisaMixMercadoriaPorPromocao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal, ByVal QTDFXAGRP As Decimal) As PartialViewResult
            Try

                'atribui no view bag as configs de edição/visualizacao
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)


                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of RelacaoAcaoMecanicaMixViewModel)
                gridConf.Filter = New RelacaoAcaoMecanicaMixViewModel()
                gridConf.Filter.CODACOCMC = CODACOCMC
                gridConf.Filter.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                gridConf.ActionName = "PesquisaMixMercadoria"
                gridConf.ResultID = "gridItensPesquisaMixMercadoria"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "DESMERC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of RelacaoAcaoMecanicaMixViewModel), gridConf.Page)

                ViewBag.TIPPMC = TIPPMC
                If (TIPPMC = 1) Then
                    ViewBag.QTDFXAGRP = QTDFXAGRP
                Else
                    ViewBag.QTDFXAGRP = 0
                End If

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixMercadoria/PesquisaPorPromocao/_WidgetLoadPesquisaMixMercadoriaPorPromocao.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMixMercadoriaPorPromocao(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaMixViewModel)) As PartialViewResult
            Try
                If dadosPesquisa.Filter.CODPMC Is Nothing Then
                    Warning("Informe o codigo da promocao")
                    Return Nothing
                End If
                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                'Dim filtro As VO.RelacaoAcaoMecanicaMix = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.RelacaoAcaoMecanicaMix) = BO.PesquisaMixMercadoriaPorPromocao(dadosPesquisa.Filter.CODPMC, dadosPesquisa.Filter.CODACOCMC, dadosPesquisa.Filter.NUMSEQRGRACOCMC)
                Dim viewModeList As List(Of RelacaoAcaoMecanicaMixViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaMix), List(Of RelacaoAcaoMecanicaMixViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaMixViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaMixMercadoriaPorPromocao"
                dadosPesquisa.PaginationID = "gridItensPesquisaMixMercadoriaPorPromocao"
                dadosPesquisa.ActionName = "PesquisaMixMercadoriaPorPromocao"
                dadosPesquisa.ControllerName = "RelacaoAcaoMecanicaMix"

                'Controle de visibilidade de tela
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixMercadoria/PesquisaPorPromocao/_GridPesquisaMixMercadoriaPorPromocao.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Adiciona o resultado da pesquisa por promocão ao mix.
        <HttpPost()>
        Function AddPesquisaMixMercadoriaPorPromocao(filtro As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult
            Try

                If filtro.CODPMC Is Nothing Then
                    Warning("Informe o código da promoção")
                    Return Nothing
                End If

                'Dim filtroVO As VO.RelacaoAcaoMecanicaMix = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(filtro)

                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                BO.AddPesquisaMixMercadoriaPorPromocao(filtro.CODPMC, filtro.CODACOCMC, filtro.NUMSEQRGRACOCMC, filtro.CODGRPMIXMERPMC)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados Mix Mercadoria por promoção
        <HttpPost()>
        Function SaveMixMercadoriaPorPromocao(ByVal obj As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult
            Try
                Dim BO = New RelacaoAcaoMecanicaMixBO()

                Dim objMercadoriaExiste As VO.RelacaoAcaoMecanicaMix = BO.SelectT(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = obj.CODACOCMC, .NUMSEQRGRACOCMC = obj.NUMSEQRGRACOCMC, .CODMER = obj.CODMER})

                If (objMercadoriaExiste IsNot Nothing) Then
                    Warning(Constantes.REGISTRO_DUPLICADO)
                    Return Nothing
                End If

                'Obter max NUMSEQMIXACOCMC
                Dim objMercadoria As VO.RelacaoAcaoMecanicaMix = BO.SelectMaxNumSeqMixAcoCmc(obj.CODACOCMC, obj.NUMSEQRGRACOCMC)
                obj.NUMSEQMIXACOCMC = objMercadoria.NUMSEQMIXACOCMC

                Dim relacaoAcaoMecanicMixMercadoria = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(obj)

                BO.InsertT(relacaoAcaoMecanicMixMercadoria)
                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


#End Region

#Region "Mix Fornecedor"
        <HttpPost()>
        Public Function EditMecanicaPromocaoMixFornecedor() As PartialViewResult
            Try
                'Controle de visibilidade de tela
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Dim obj As RelacaoAcaoMecanicaMixViewModel = New RelacaoAcaoMecanicaMixViewModel()

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixFornecedor/_ModalMixFornecedor.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function CarregarWidgetLoadPesquisaMixFornecedor(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                'atribui no view bag as configs de edição/visualizacao
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)


                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of RelacaoAcaoMecanicaMixViewModel)
                gridConf.Filter = New RelacaoAcaoMecanicaMixViewModel()
                gridConf.Filter.CODACOCMC = CODACOCMC
                gridConf.Filter.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                gridConf.ActionName = "PesquisaMixFornecedor"
                gridConf.ResultID = "gridItensPesquisaMixFornecedor"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMFRNC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of RelacaoAcaoMecanicaMixViewModel), gridConf.Page)

                Dim boRelacaoEquipeDeComprasxGerenteNegocioServCompras As New RelacaoEquipeDeComprasxGerenteNegocioServComprasBO
                ViewBag.ListaRelacaoEquipeDeComprasxGerenteNegocioServCompras = boRelacaoEquipeDeComprasxGerenteNegocioServCompras.ListT(New VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras()).OrderBy(Function(model) model.DESDIVCMP)

                Dim boDiretoriaCompra As New DiretoriaCompraBO
                ViewBag.ListaDiretoriaCompra = boDiretoriaCompra.ListT(New VO.DiretoriaCompra()).OrderBy(Function(model) model.DESDRTCMP)

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixFornecedor/_WidgetLoadPesquisaMixFornecedor.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMixFornecedor(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaMixViewModel)) As PartialViewResult
            Try
                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                Dim filtro As VO.RelacaoAcaoMecanicaMix = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.RelacaoAcaoMecanicaMix) = BO.PesquisaMixFornecedor(filtro)
                Dim viewModeList As List(Of RelacaoAcaoMecanicaMixViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaMix), List(Of RelacaoAcaoMecanicaMixViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaMixViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "gridItensPesquisaMixFornecedor"
                dadosPesquisa.PaginationID = "gridItensPesquisaMixFornecedor"
                dadosPesquisa.ActionName = "PesquisaMixFornecedor"
                dadosPesquisa.ControllerName = "RelacaoAcaoMecanicaMix"

                'Controle de visibilidade de tela
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixFornecedor/_GridPesquisaMixFornecedor.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function CarregarWidgetMixFornecedorAdicionados(VO As VO.RelacaoAcaoMecanicaMix) As PartialViewResult
            Try
                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()

                'Controle de visibilidade de tela
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Dim list As List(Of VO.RelacaoAcaoMecanicaMix) = BO.ListarMixFornecedorAdicionados(VO)
                Dim listViewModel As List(Of RelacaoAcaoMecanicaMixViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaMix), List(Of RelacaoAcaoMecanicaMixViewModel))(list)

                Return PartialView("~/Views/AcaoComercial/Mecanica/MixFornecedor/_WidgetMixFornecedor.vbhtml", listViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Adiciona o resultado da pesquisa ao mix.
        <HttpPost()>
        Function AddPesquisaMixFornecedor(filtro As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult
            Try
                Dim filtroVO As VO.RelacaoAcaoMecanicaMix = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(filtro)

                Dim BO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()
                BO.AddPesquisaMixFornecedor(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados Mix Classe Mercadoria
        <HttpPost()>
        Function SaveMixFornecedor(ByVal obj As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult
            Dim BO = New RelacaoAcaoMecanicaMixBO()

            Dim objFornecedorExiste As VO.RelacaoAcaoMecanicaMix = BO.SelectT(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = obj.CODACOCMC, .NUMSEQRGRACOCMC = obj.NUMSEQRGRACOCMC, .CODFRN = obj.CODFRN})

            If (objFornecedorExiste IsNot Nothing) Then
                Warning(Constantes.REGISTRO_DUPLICADO)
                Return Nothing
            End If

            'Obter max NUMSEQMIXACOCMC
            Dim objFornecedor As VO.RelacaoAcaoMecanicaMix = BO.SelectMaxNumSeqMixAcoCmc(obj.CODACOCMC, obj.NUMSEQRGRACOCMC)
            obj.NUMSEQMIXACOCMC = objFornecedor.NUMSEQMIXACOCMC

            Dim relacaoAcaoMecanicMixFornecedor = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(obj)

            BO.InsertT(relacaoAcaoMecanicMixFornecedor)

            Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

            Return Nothing

        End Function

        'Deletar Itens Mix Classe Mercadoria
        <HttpPost()>
        Function DeleteMixFornecedor(ByVal obj As RelacaoAcaoMecanicaMixViewModel) As PartialViewResult

            If (obj.CODACOCMC Is Nothing Or obj.NUMSEQRGRACOCMC Is Nothing) Then
                Warning("Campos obrigatórios não informados")
                Return Nothing
            End If


            Dim BO = New RelacaoAcaoMecanicaMixBO()

            Dim relacaoAcaoMecanicaMixFornecedor = Mapper.Map(Of RelacaoAcaoMecanicaMixViewModel, VO.RelacaoAcaoMecanicaMix)(obj)
            BO.DeleteMixFornecedor(relacaoAcaoMecanicaMixFornecedor)
            Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
            Return Nothing

        End Function

        'Importar Planila Execel
        <HttpPost()>
        Public Function ImportarXlsMixFornecedor(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal QDEGRPMIXPMC As Decimal) As String
            Dim relacaoAcaoMecanicaMixBO As New RelacaoAcaoMecanicaMixBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseMixFornecedor"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseMixFornecedor"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = relacaoAcaoMecanicaMixBO.ImportarExelMixFornecedor(savedFileName, CODACOCMC, NUMSEQRGRACOCMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseMixFornecedor"
            Else
                Return "TrueMixFornecedor"
            End If
        End Function
#End Region

        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            ViewBag.PermiteEdicaoMecanicaMixGrupoMercadoria = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)
            ViewBag.PermiteEdicaoMecanicaMixFamiliaMercadoria = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)
            ViewBag.PermiteEdicaoMecanicaMixClasseMercadoria = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)
            ViewBag.PermiteEdicaoMecanicaMixMercadoria = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)
            ViewBag.PermiteEdicaoMecanicaMixFornecedor = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)

        End Sub

    End Class

End Namespace
