﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoSegmentoCliente(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO As RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteBO()
                Dim relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente) = BO.ListaTipoSegmentoCliente(CODACOCMC, NUMSEQRGRACOCMC)
                Dim objRelacaoAcaoMecanicaPublicoAlvoSegmentoClienteList As List(Of RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente), List(Of RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteViewModel))(relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList)

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalSegmentoCliente.vbhtml", objRelacaoAcaoMecanicaPublicoAlvoSegmentoClienteList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal obj As List(Of RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteViewModel)) As PartialViewResult
            Dim BO As RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteBO()
            Try
                If (ModelState.IsValid) Then
                    Dim relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList = Mapper.Map(Of List(Of RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteViewModel), List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente))(obj)

                    BO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList)

                    Return Nothing
                End If


                Throw New WebFaultException(HttpStatusCode.BadRequest)
                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function

        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            ViewBag.PermiteEdicaoMecanicaPublicoAlvoSeguimentoCliente = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)

        End Sub
    End Class
End Namespace
