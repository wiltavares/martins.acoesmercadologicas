﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class RelacaoAcaoMecanicaPublicoAlvoRcaVendedorController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoRcaVendedor() As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim obj As RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel()

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalRcaVendedor.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function EditMecanicaPromocaoWidgetLoadPesquisaRcaVendedor(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try
                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel)
                gridConf.Filter = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel()
                gridConf.Filter.CODACOCMC = CODACOCMC
                gridConf.Filter.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                gridConf.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaRcaVendedor"
                gridConf.ResultID = "divGridPesquisaPublicoAlvoRcaVendedor"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMREPC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel), gridConf.Page)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetLoadPesquisaRcaVendedor.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMecanicaPromocaoWidgetLoadPesquisaRcaVendedor(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel)) As PartialViewResult
            Try
                Dim BO = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO()

                Dim filtro As VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor = Mapper.Map(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel, VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) = BO.BuscarListaRcaVendedor(filtro)
                Dim viewModeList As List(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor), List(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "divGridPesquisaPublicoAlvoRcaVendedor"
                dadosPesquisa.PaginationID = "divGridPesquisaPublicoAlvoRcaVendedor"
                dadosPesquisa.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaRcaVendedor"
                dadosPesquisa.ControllerName = "RelacaoAcaoMecanicaPublicoAlvoRcaVendedor"

                Return PartialView("~/Views/AcaoComercial/Mecanica/_GridPesquisaPublicoAlvoRcaVendedor.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function ListarPublicoAlvoRcaVendedor(obj As RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO()

                Dim objPublicoAlvoRcaVendedor As VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor = Mapper.Map(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel, VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)(obj)

                Dim objPublicoAlvoRcaVendedorList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) = BO.ListarPublicoAlvoRcaVendedor(objPublicoAlvoRcaVendedor)

                Dim publicoAlvoRcaVendedorList As List(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor), List(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel))(objPublicoAlvoRcaVendedorList)

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetPublicoAlvoRcaVendedor.vbhtml", publicoAlvoRcaVendedorList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal obj As RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel) As PartialViewResult
            Dim relacaoAcaoMecanicaPublicoAlvoRcaVendedorBO = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO()

            If (ModelState.IsValid) Then
                Dim relacaoAcaoMecanicaPublicoAlvoRcaVendedor = Mapper.Map(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel, VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)(obj)
                'relacaoAcaoMecanica.DesMecanica = "AI                  "
                relacaoAcaoMecanicaPublicoAlvoRcaVendedorBO.InsertT(relacaoAcaoMecanicaPublicoAlvoRcaVendedor)
                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                Return Nothing
            End If


            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function

        <HttpPost()>
        Function Delete(ByVal obj As RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel) As PartialViewResult
            Dim relacaoAcaoMecanicaPublicoAlvoRcaVendedorBO = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO()

            If (obj.CODACOCMC Is Nothing Or obj.NUMSEQRGRACOCMC Is Nothing) Then
                Warning("Campos obrigatórios não informados")
                Return Nothing
            End If

            If (ModelState.IsValid) Then
                Dim relacaoAcaoMecanicaPublicoAlvoRcaVendedor = Mapper.Map(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel, VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)(obj)
                'relacaoAcaoMecanica.DesMecanica = "AI                  "
                relacaoAcaoMecanicaPublicoAlvoRcaVendedorBO.DeleteT(relacaoAcaoMecanicaPublicoAlvoRcaVendedor)
                Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
                Return Nothing
            End If


            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function

        'Adiciona o resultado da pesquisa
        <HttpPost()>
        Function AddPesquisa(filtro As RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel) As PartialViewResult
            Try
                Dim filtroVO As VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor = Mapper.Map(Of RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel, VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)(filtro)

                Dim BO As RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO()
                BO.AddPesquisa(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            ViewBag.PermiteEdicaoMecanicaPublicoAlvoRcaVendedor = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)

        End Sub

        <HttpPost()>
        Public Function ImportarXlsRcaVendedor(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal QDEGRPMIXPMC As Decimal) As String
            Dim relacaoAcaoMecanicaPublicoAlvoRcaVendedorBO As New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseRcaVendedor"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseRcaVendedor"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = relacaoAcaoMecanicaPublicoAlvoRcaVendedorBO.ImportarExelRcaVendedor(savedFileName, CODACOCMC, NUMSEQRGRACOCMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseRcaVendedor"
            Else
                Return "TrueRcaVendedor"
            End If
        End Function

    End Class
End Namespace
