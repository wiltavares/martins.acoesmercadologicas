﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class RelacaoAcaoMecanicaPublicoAlvoGerenteVendasController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoGerenteVendas(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO = New RelacaoAcaoMecanicaPublicoAlvoGerenteVendasBO()
                Dim relacaoAcaoMecanicaPublicoAlvoGerenteVendasList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas) = BO.ListarGerenteVendas(CODACOCMC, NUMSEQRGRACOCMC)
                relacaoAcaoMecanicaPublicoAlvoGerenteVendasList.ForEach(Sub(obj) obj.NOMGER = obj.NOMGER.TrimEnd())

                Dim objRelacaoAcaoMecanicaPublicoAlvoGerenteVendasList As List(Of RelacaoAcaoMecanicaPublicoAlvoGerenteVendasViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas), List(Of RelacaoAcaoMecanicaPublicoAlvoGerenteVendasViewModel))(relacaoAcaoMecanicaPublicoAlvoGerenteVendasList)

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalGerenteVendas.vbhtml", objRelacaoAcaoMecanicaPublicoAlvoGerenteVendasList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal obj As List(Of RelacaoAcaoMecanicaPublicoAlvoGerenteVendasViewModel)) As PartialViewResult
            Dim BO As RelacaoAcaoMecanicaPublicoAlvoGerenteVendasBO = New RelacaoAcaoMecanicaPublicoAlvoGerenteVendasBO()
            Try
                If (ModelState.IsValid) Then
                    Dim relacaoAcaoMecanicaPublicoAlvoGerenteVendasList = Mapper.Map(Of List(Of RelacaoAcaoMecanicaPublicoAlvoGerenteVendasViewModel), List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas))(obj)

                    BO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, relacaoAcaoMecanicaPublicoAlvoGerenteVendasList)

                    Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                    Return Nothing
                End If


                Throw New WebFaultException(HttpStatusCode.BadRequest)
                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function

        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            ViewBag.PermiteEdicaoMecanicaPublicoAlvoGerenteVendas = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)

        End Sub
    End Class
End Namespace
