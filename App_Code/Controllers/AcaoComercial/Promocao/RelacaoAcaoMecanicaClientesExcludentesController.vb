﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class RelacaoAcaoMecanicaClientesExcludentesController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoClientesExcludentes() As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim obj As RelacaoAcaoMecanicaClientesExcludentesViewModel = New RelacaoAcaoMecanicaClientesExcludentesViewModel()

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalClientesExcludentes.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function EditMecanicaPromocaoWidgetLoadPesquisaClientesExcludentes(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of RelacaoAcaoMecanicaClientesExcludentesViewModel)
                gridConf.Filter = New RelacaoAcaoMecanicaClientesExcludentesViewModel()
                gridConf.Filter.CODACOCMC = CODACOCMC
                gridConf.Filter.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                gridConf.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaCliente"
                gridConf.ResultID = "divGridItensPesquisaClientesExcludentes"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMCLIC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of RelacaoAcaoMecanicaClientesExcludentesViewModel), gridConf.Page)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetLoadPesquisaClientesExcludentes.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMecanicaPromocaoWidgetLoadPesquisaCliente(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaClientesExcludentesViewModel)) As PartialViewResult
            Try
                If Not ValidaFiltroClienteExcludente(dadosPesquisa.Filter) Then
                    Return Nothing
                End If

                Dim BO = New RelacaoAcaoMecanicaClientesExcludentesBO()
                Dim filtro As VO.RelacaoAcaoMecanicaClientesExcludentes = Mapper.Map(Of RelacaoAcaoMecanicaClientesExcludentesViewModel, VO.RelacaoAcaoMecanicaClientesExcludentes)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes) = BO.BuscarListarCliente(filtro)
                Dim viewModeList As List(Of RelacaoAcaoMecanicaClientesExcludentesViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaClientesExcludentes), List(Of RelacaoAcaoMecanicaClientesExcludentesViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaClientesExcludentesViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count


                dadosPesquisa.ResultID = "divGridItensPesquisaClientesExcludentes"
                dadosPesquisa.PaginationID = "divGridItensPesquisaClientesExcludentes"
                dadosPesquisa.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaCliente"
                dadosPesquisa.ControllerName = "RelacaoAcaoMecanicaClientesExcludentes"

                Return PartialView("~/Views/AcaoComercial/Mecanica/_GridPesquisaClientesExcludentes.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function ListarClientesExcludentes(obj As RelacaoAcaoMecanicaClientesExcludentesViewModel) As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)


                Dim objClientesExcludentes As VO.RelacaoAcaoMecanicaClientesExcludentes = Mapper.Map(Of RelacaoAcaoMecanicaClientesExcludentesViewModel, VO.RelacaoAcaoMecanicaClientesExcludentes)(obj)

                Dim BO = New RelacaoAcaoMecanicaClientesExcludentesBO()
                Dim objClientesExcludentesList As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes) = BO.ListarClientesExcludentes(objClientesExcludentes)
                Dim clientesExcludentesList As List(Of RelacaoAcaoMecanicaClientesExcludentesViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaClientesExcludentes), List(Of RelacaoAcaoMecanicaClientesExcludentesViewModel))(objClientesExcludentesList)


                'objeto da pesquisa.
                Dim gridConf = New GridSettings(Of RelacaoAcaoMecanicaClientesExcludentesViewModel)
                gridConf.Filter = obj
                gridConf.ActionName = "PesquisaClientesExcludentes"
                gridConf.ResultID = "divGridClientesExcludentes"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMCLIC"
                gridConf.Page.PageSize = 10


                gridConf.ListPaged = PaginarLista(clientesExcludentesList, gridConf.Page)
                gridConf.TotalCount = clientesExcludentesList.Count


                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetClientesExcludentes.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function


        <HttpPost()>
        Function PesquisaClientesExcludentes(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaClientesExcludentesViewModel)) As PartialViewResult
            Try


                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")
                ControlaVisibilidadeComponentes(acaoCache)

                Dim objClientesExcludentes As VO.RelacaoAcaoMecanicaClientesExcludentes = Mapper.Map(Of RelacaoAcaoMecanicaClientesExcludentesViewModel, VO.RelacaoAcaoMecanicaClientesExcludentes)(dadosPesquisa.Filter)

                Dim BO = New RelacaoAcaoMecanicaClientesExcludentesBO()
                Dim objClientesExcludentesList As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes) = BO.ListarClientesExcludentes(objClientesExcludentes)
                Dim publicoClientesExcludentesList As List(Of RelacaoAcaoMecanicaClientesExcludentesViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaClientesExcludentes), List(Of RelacaoAcaoMecanicaClientesExcludentesViewModel))(objClientesExcludentesList)


                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaClientesExcludentesViewModel)(publicoClientesExcludentesList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = publicoClientesExcludentesList.Count
                dadosPesquisa.PaginationID = "divGridClientesExcludentes"
                dadosPesquisa.ActionName = "PesquisaClientesExcludentes"
                dadosPesquisa.ResultID = "divGridClientesExcludentes"
                dadosPesquisa.ControllerName = "RelacaoAcaoMecanicaClientesExcludentes"

                Return PartialView("~/Views/AcaoComercial/Mecanica/_GridClientesExcludentes.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal obj As RelacaoAcaoMecanicaClientesExcludentesViewModel) As PartialViewResult
            Dim relacaoAcaoMecanicaClientesExcludentesBO = New RelacaoAcaoMecanicaClientesExcludentesBO()

            If (ModelState.IsValid) Then
                Dim relacaoAcaoMecanicaClientesExcludentes = Mapper.Map(Of RelacaoAcaoMecanicaClientesExcludentesViewModel, VO.RelacaoAcaoMecanicaClientesExcludentes)(obj)
                'relacaoAcaoMecanica.DesMecanica = "AI                  "
                relacaoAcaoMecanicaClientesExcludentesBO.InsertT(relacaoAcaoMecanicaClientesExcludentes)
                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                Return Nothing
            End If


            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function

        <HttpPost()>
        Function Delete(ByVal obj As RelacaoAcaoMecanicaClientesExcludentesViewModel) As PartialViewResult
            Dim relacaoAcaoMecanicaClientesExcludentesBO = New RelacaoAcaoMecanicaClientesExcludentesBO()

            If (obj.CODACOCMC Is Nothing Or obj.NUMSEQRGRACOCMC Is Nothing) Then
                Warning("Campos obrigatórios não informados")
                Return Nothing
            End If

            If (ModelState.IsValid) Then
                Dim relacaoAcaoMecanicaClientesExcludentes = Mapper.Map(Of RelacaoAcaoMecanicaClientesExcludentesViewModel, VO.RelacaoAcaoMecanicaClientesExcludentes)(obj)
                'relacaoAcaoMecanica.DesMecanica = "AI                  "
                relacaoAcaoMecanicaClientesExcludentesBO.DeleteT(relacaoAcaoMecanicaClientesExcludentes)
                Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
                Return Nothing
            End If


            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function


        'Adiciona o resultado da pesquisa
        <HttpPost()>
        Function AddPesquisa(filtro As RelacaoAcaoMecanicaClientesExcludentesViewModel) As PartialViewResult
            Try
                Dim filtroVO As VO.RelacaoAcaoMecanicaClientesExcludentes = Mapper.Map(Of RelacaoAcaoMecanicaClientesExcludentesViewModel, VO.RelacaoAcaoMecanicaClientesExcludentes)(filtro)

                If Not ValidaFiltroClienteExcludente(filtro) Then
                    Return Nothing
                End If

                Dim BO As RelacaoAcaoMecanicaClientesExcludentesBO = New RelacaoAcaoMecanicaClientesExcludentesBO()
                BO.AddPesquisa(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Private Function ValidaFiltroClienteExcludente(ByRef filtro As RelacaoAcaoMecanicaClientesExcludentesViewModel) As Boolean

            If filtro.CODCLI Is Nothing And filtro.NOMCLI Is Nothing Then
                Warning("Informe pelo menos um filtro")
                Return False
            End If

            If Not filtro.NOMCLI Is Nothing Then
                If (filtro.NOMCLI.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do cliente")
                    Return False
                End If
            End If

            Return True
        End Function

        Private Function ValidaFiltroCliente(ByRef filtro As RelacaoAcaoMecanicaPublicoAlvoClienteViewModel) As Boolean

            If filtro.CODCLI Is Nothing And filtro.NOMCLI Is Nothing And
                filtro.CODREP Is Nothing And filtro.NOMREP Is Nothing And
                filtro.CODSUP Is Nothing And filtro.NOMSUP Is Nothing Then  'filtro.CODGER Is Nothing And filtro.NOMGER Is Nothing And
                Warning("Informe pelo menos um filtro")
                Return False
            End If

            If Not filtro.NOMCLI Is Nothing Then
                If (filtro.NOMCLI.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do cliente")
                    Return False
                End If
            End If
            If Not filtro.NOMREP Is Nothing Then
                If (filtro.NOMREP.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do representante vendas")
                    Return False
                End If
            End If
            'If Not filtro.NOMGER Is Nothing Then
            '    If (filtro.NOMGER.Length < 3) Then
            '        Warning("Informe pelo menos 3 caracteres no filtro de nome do gerente de vendas")
            '        Return False
            '    End If
            'End If
            If Not filtro.NOMSUP Is Nothing Then
                If (filtro.NOMSUP.Length < 3) Then
                    Warning("Informe pelo menos 3 caracteres no filtro de nome do gerente de mercado")
                    Return False
                End If
            End If

            Return True
        End Function


        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            ViewBag.PermiteEdicaoMecanicaClientesExcludentes = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)

        End Sub

        <HttpPost()>
        Public Function ImportarXlsClientesExcludentes(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As String
            Dim relacaoAcaoMecanicaClientesExcludentesBO As New RelacaoAcaoMecanicaClientesExcludentesBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseCliente"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseCliente"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = relacaoAcaoMecanicaClientesExcludentesBO.ImportarExelClientesExcludentes(savedFileName, CODACOCMC, NUMSEQRGRACOCMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseCliente"
            Else
                Return "TrueCliente"
            End If
        End Function

        '<HttpPost()>
        'Public Function ImportarXlsClientesExcludentes(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As String
        '    Try


        '        Dim relacaoAcaoMecanicaClientesExcludentesBO As New RelacaoAcaoMecanicaClientesExcludentesBO()

        '        If (Request.Files.Count <= 0) Then
        '            Warning("Selecione um arquivo para importar!")
        '            Return "FalseCliente"
        '        End If

        '        Dim savedFileName As String = ""
        '        If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
        '            Warning("Não foi possivel abrir o arquivo!")
        '            Return "FalseCliente"
        '        End If
        '        For Each item As String In Request.Files

        '            Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
        '            If (hpf.ContentLength = 0) Then
        '                Continue For
        '            End If

        '            Dim keybAuth As New Renci.SshNet.KeyboardInteractiveAuthenticationMethod(ConfigurationManager.AppSettings("usuarioDatastage"))
        '            AddHandler keybAuth.AuthenticationPrompt, AddressOf HandleKeyEvent

        '            Dim connectionInfo As New Renci.SshNet.ConnectionInfo(ConfigurationManager.AppSettings("urlDatastage"),
        '                                    ConfigurationManager.AppSettings("usuarioDatastage"),
        '                                    keybAuth)

        '            Dim filename As String = String.Format("mecanica_promo_cli{0}", Date.Now.ToString("yyyyMMddHHmmss"))
        '            Using client As New Renci.SshNet.SftpClient(connectionInfo)
        '                client.Connect()
        '                client.UploadFile(hpf.InputStream, String.Format("/geral/Sistemas/Arquivos_rest/mecanica_promo_cli/waiting/{0}.xls", filename))


        '                Dim atributes = client.GetAttributes(String.Format("/geral/Sistemas/Arquivos_rest/mecanica_promo_cli/waiting/{0}.xls", filename))

        '                atributes.OwnerCanExecute = True
        '                atributes.OwnerCanRead = True
        '                atributes.OwnerCanWrite = True
        '                atributes.OthersCanExecute = True
        '                atributes.OthersCanRead = True
        '                atributes.OthersCanWrite = True
        '                atributes.GroupCanExecute = True
        '                atributes.GroupCanRead = True
        '                atributes.GroupCanWrite = True

        '                client.SetAttributes(String.Format("/geral/Sistemas/Arquivos_rest/mecanica_promo_cli/waiting/{0}.xls", filename), atributes)
        '            End Using

        '            Dim codfnc As String = ControleAcesso.CodFnc()
        '            Dim nomacsusrsis As String = System.Web.HttpContext.Current.User.Identity.Name
        '            Dim Url = String.Format("https://{0}:9446/wisd-rest2/ProcessaArquivo/Tabela/RLCACOCMCRGRCLI?codacocmc={1}&numseqrgracocmc={2}&nomarqrcb={3}&nomacsusrsis={4}&codfnc={5}&datslc={6}%20{7}", ConfigurationManager.AppSettings("urlDatastage"), CODACOCMC, NUMSEQRGRACOCMC, filename, nomacsusrsis, codfnc, Date.Now.ToString("yyyy-MM-dd"), Date.Now.ToString("HH:mm:ss"))


        '            Dim httpClient As New System.Net.Http.HttpClient()
        '            Dim responsex = httpClient.GetAsync(Url)
        '            responsex.Wait()


        '            Dim log As New LogDataStageBO()
        '            Dim logErros = log.ListaParaGrid(New VO.LogDataStage() With {.NOMARQRCB = filename, .FLGSTAPCS = 0})

        '            Dim strLog As New StringBuilder
        '            If (logErros.Count > 0) Then
        '                strLog.AppendLine("As seguintes linhas não foram importadas:")
        '                For Each linha In logErros
        '                    strLog.Append(linha.CODLNHARQDTS.Substring(0, linha.CODLNHARQDTS.IndexOf("#"))).Append(" Motivo: ").Append(linha.DESMTVSTA).AppendLine()
        '                Next
        '            End If

        '            If (responsex.Result.IsSuccessStatusCode) Then
        '                Dim Content = responsex.Result.Content.ReadAsStringAsync()
        '                Content.Wait()

        '                'Dim res = Content.Result
        '                If (logErros.Count = 0) Then
        '                    AddAlert(AlertStyle.Success, "Importação concluida com sucesso", Nothing, True, False)
        '                Else
        '                    AddAlert(AlertStyle.Success, "Importação concluida com sucesso porem alguns algumas linhas não foram importadas, verifique", Nothing, True, False)
        '                    AddAlert(AlertStyle.Warning, strLog.ToString().Replace(vbCrLf, "<br/>"), Nothing, True, False)
        '                End If

        '                Return "TrueCliente"
        '            Else
        '                Dim Content = responsex.Result.Content.ReadAsStringAsync()
        '                Content.Wait()

        '                AddAlert(AlertStyle.Danger, "Importação concluida com erro, verifique se o arquivo esta conforme o template", Nothing, True, False)

        '                If (logErros.Count > 0) Then
        '                    AddAlert(AlertStyle.Warning, strLog.ToString().Replace(vbCrLf, "<br/>"), Nothing, True, False)
        '                End If
        '                Return "FalseCliente"
        '            End If
        '        Next

        '        Return "FalseCliente"
        '    Catch ex As Exception
        '        Danger(ex)
        '        Return "FalseCliente"
        '    End Try
        'End Function
        Sub HandleKeyEvent(ByVal sender As Object, ByVal e As Renci.SshNet.Common.AuthenticationPromptEventArgs)
            For Each prompt As Renci.SshNet.Common.AuthenticationPrompt In e.Prompts
                If prompt.Request.IndexOf("Password:", StringComparison.InvariantCultureIgnoreCase) <> -1 Then
                    prompt.Response = ConfigurationManager.AppSettings("senhaDatastage")
                End If
            Next
        End Sub


    End Class
End Namespace
