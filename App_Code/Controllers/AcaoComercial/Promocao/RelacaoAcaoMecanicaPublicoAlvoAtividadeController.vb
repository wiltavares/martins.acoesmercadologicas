﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class RelacaoAcaoMecanicaPublicoAlvoAtividadeController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoAtividade(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO = New RelacaoAcaoMecanicaPublicoAlvoAtividadeBO()
                Dim relacaoAcaoMecanicaPublicoAlvoAtividadeList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade) = BO.ListarAtividade(CODACOCMC, NUMSEQRGRACOCMC)
                Dim objRelacaoAcaoMecanicaPublicoAlvoAtividadeList As List(Of RelacaoAcaoMecanicaPublicoAlvoAtividadeViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade), List(Of RelacaoAcaoMecanicaPublicoAlvoAtividadeViewModel))(relacaoAcaoMecanicaPublicoAlvoAtividadeList)

                Dim tipoAtividadeList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade) = BO.BuscarTipoAtividade()
                Dim objTipoAtividadeList As List(Of RelacaoAcaoMecanicaPublicoAlvoAtividadeViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade), List(Of RelacaoAcaoMecanicaPublicoAlvoAtividadeViewModel))(tipoAtividadeList)
                ViewBag.TipoAtividade = objTipoAtividadeList

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalAtividade.vbhtml", objRelacaoAcaoMecanicaPublicoAlvoAtividadeList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal obj As List(Of RelacaoAcaoMecanicaPublicoAlvoAtividadeViewModel)) As ActionResult
            Dim BO As RelacaoAcaoMecanicaPublicoAlvoAtividadeBO = New RelacaoAcaoMecanicaPublicoAlvoAtividadeBO()
            Try
                If (ModelState.IsValid) Then
                    Dim atividadeList = Mapper.Map(Of List(Of RelacaoAcaoMecanicaPublicoAlvoAtividadeViewModel), List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade))(obj)

                    BO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, atividadeList)

                    'Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                    Return Nothing
                End If

                Throw New WebFaultException(HttpStatusCode.BadRequest)
                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function

        <HttpPost>
        Function BuscarAtividadePorBU(ByVal obj As List(Of RelacaoAcaoMecanicaPublicoAlvoAtividadeViewModel)) As JsonResult

            Dim BO As RelacaoAcaoMecanicaPublicoAlvoAtividadeBO = New RelacaoAcaoMecanicaPublicoAlvoAtividadeBO()

            Dim objeto As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade) = Mapper.Map(Of List(Of RelacaoAcaoMecanicaPublicoAlvoAtividadeViewModel), List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade))(obj)

            Dim list As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade) = BO.BuscarAtividadePorBU(objeto)
            Dim listViewModel As List(Of RelacaoAcaoMecanicaPublicoAlvoAtividadeViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade), List(Of RelacaoAcaoMecanicaPublicoAlvoAtividadeViewModel))(list)

            Return Json(listViewModel, JsonRequestBehavior.AllowGet)
        End Function

        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            ViewBag.PermiteEdicaoMecanicaPublicoAlvoAtividade = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)

        End Sub

    End Class
End Namespace
