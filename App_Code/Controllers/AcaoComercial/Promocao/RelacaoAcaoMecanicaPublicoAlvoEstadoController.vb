﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class RelacaoAcaoMecanicaPublicoAlvoEstadoController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoEstadoUniao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO = New RelacaoAcaoMecanicaPublicoAlvoEstadoBO()

                Dim relacaoAcaoMecanicaPublicoAlvoEstadoList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado) = BO.ListarEstadoUniao(CODACOCMC, NUMSEQRGRACOCMC)

                Dim objRelacaoAcaoMecanicaPublicoAlvoEstadoList As List(Of RelacaoAcaoMecanicaPublicoAlvoEstadoViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado), List(Of RelacaoAcaoMecanicaPublicoAlvoEstadoViewModel))(relacaoAcaoMecanicaPublicoAlvoEstadoList)

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalEstado.vbhtml", objRelacaoAcaoMecanicaPublicoAlvoEstadoList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal obj As List(Of RelacaoAcaoMecanicaPublicoAlvoEstadoViewModel)) As ActionResult
            Dim relacaoAcaoMecanicaPublicoAlvoEstadoBO = New RelacaoAcaoMecanicaPublicoAlvoEstadoBO()
            Try
                Dim BO As RelacaoAcaoMecanicaPublicoAlvoEstadoBO = New RelacaoAcaoMecanicaPublicoAlvoEstadoBO()
                If (ModelState.IsValid) Then
                    Dim relacaoAcaoMecanicaPublicoAlvoEstado = Mapper.Map(Of List(Of RelacaoAcaoMecanicaPublicoAlvoEstadoViewModel), List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado))(obj)

                    BO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, relacaoAcaoMecanicaPublicoAlvoEstado)

                    Return Nothing
                End If

                Throw New WebFaultException(HttpStatusCode.BadRequest)
                Return Nothing
            Catch
                Throw New WebFaultException(HttpStatusCode.InternalServerError)
                Return Nothing
            End Try
        End Function

        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            ViewBag.PermiteEdicaoMecanicaPublicoAlvoEstado = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)

        End Sub
    End Class
End Namespace

