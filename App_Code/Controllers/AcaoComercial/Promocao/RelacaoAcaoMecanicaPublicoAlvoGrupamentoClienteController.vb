﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoGrupamentoCliente(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try

                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO()
                Dim relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente) = BO.ListarGrupamentoCliente(CODACOCMC, NUMSEQRGRACOCMC)
                Dim objRelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList As List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente), List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel))(relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList)

                Dim tipoGrupoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente) = BO.BuscarTipoGrupamentoCliente()
                Dim objTipoGrupoClienteList As List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente), List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel))(tipoGrupoClienteList)
                ViewBag.TipoGrupoCliente = objTipoGrupoClienteList

                ControlaVisibilidadeComponentes(acaoCache)

                Dim dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel) = New GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel)()

                dadosPesquisa.Filter = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel()
                dadosPesquisa.Filter.CODACOCMC = CODACOCMC
                dadosPesquisa.Filter.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                dadosPesquisa.Page = New PageSettings()
                dadosPesquisa.Page.OrderBy = "CODGRPCLIC"
                dadosPesquisa.Page.PageSize = objRelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList.Count
                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel)(objRelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = objRelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList.Count
                dadosPesquisa.ResultID = "divGridModalGrupamentoCliente"
                dadosPesquisa.ActionName = "PesquisaMecanicaPromocaoPublicoAlvoGrupamentoCliente"

                Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalGrupamentoCliente.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Pesquisar as Mercadorias - Paginação
        <HttpPost()>
        Function PesquisaMecanicaPromocaoPublicoAlvoGrupamentoCliente(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel)) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO()
                Dim relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente) = BO.ListarGrupamentoCliente(dadosPesquisa.Filter.CODACOCMC, dadosPesquisa.Filter.NUMSEQRGRACOCMC)
                Dim objRelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList As List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente), List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel))(relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList)

                Dim tipoGrupoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente) = BO.BuscarTipoGrupamentoCliente()
                Dim objTipoGrupoClienteList As List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente), List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel))(tipoGrupoClienteList)
                ViewBag.TipoGrupoCliente = objTipoGrupoClienteList

                ControlaVisibilidadeComponentes(acaoCache)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel)(objRelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = objRelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList.Count
                dadosPesquisa.ResultID = "divGridModalGrupamentoCliente"
                dadosPesquisa.PaginationID = "divGridModalGrupamentoCliente"
                dadosPesquisa.ActionName = "PesquisaMecanicaPromocaoPublicoAlvoGrupamentoCliente"
                dadosPesquisa.ControllerName = "RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente"

                Return PartialView("~/Views/AcaoComercial/Mecanica/_GridGrupamentoCliente.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal obj As List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel)) As ActionResult
            Dim relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO()
            Try
                Dim BO As RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO()
                If (ModelState.IsValid) Then
                    Dim relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList = Mapper.Map(Of List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel), List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente))(obj)

                    BO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList)

                    Success("Alterações realiadas com sucesso")
                    Return Nothing
                End If

                Success("Verifique os campos obrigatórios")
                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost>
        Function BuscarGrupamentoClientePorBU(ByVal obj As List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel)) As JsonResult
            Try

                Dim BO As RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO()

                Dim objeto As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente) = Mapper.Map(Of List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel), List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente))(obj)

                Dim list As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente) = BO.BuscarGrupamentoClientePorBU(objeto)
                Dim listViewModel As List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente), List(Of RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteViewModel))(list)

                Return Json(listViewModel, JsonRequestBehavior.AllowGet)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            ViewBag.PermiteEdicaoMecanicaPublicoAlvoGrupamentoCliente = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)

        End Sub

    End Class
End Namespace
