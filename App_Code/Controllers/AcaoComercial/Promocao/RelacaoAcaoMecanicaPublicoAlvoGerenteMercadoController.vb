﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList
Imports System.Data
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports System.ServiceModel.Web
Imports System.Net
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="12,17,14,31,32,33,40")>
    Public Class RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoController
        Inherits AbstractController

        <HttpPost()>
        Public Function EditMecanicaPromocaoPublicoAlvoGerenteMercado() As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim obj As RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel = New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel()

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_ModalGerenteMercado.vbhtml", obj)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpGet()>
        Function EditMecanicaPromocaoWidgetLoadPesquisaGerenteMercado(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As PartialViewResult
            Try
                ' objeto da pesquisa.
                Dim gridConf = New GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel)
                gridConf.Filter = New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel()
                gridConf.Filter.CODACOCMC = CODACOCMC
                gridConf.Filter.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                gridConf.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaGerenteMercado"
                gridConf.ResultID = "divGridPesquisaPublicoAlvoGerenteMercado"
                gridConf.Page = New PageSettings()
                gridConf.Page.OrderBy = "NOMSUPC"
                gridConf.Page.PageSize = 10
                gridConf.ListPaged = PaginarLista(New List(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel), gridConf.Page)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetLoadPesquisaGerenteMercado.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function PesquisaMecanicaPromocaoWidgetLoadPesquisaGerenteMercado(dadosPesquisa As GridSettings(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel)) As PartialViewResult
            Try
                Dim BO = New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO()

                Dim filtro As VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado = Mapper.Map(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel, VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)(dadosPesquisa.Filter)

                Dim voList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado) = BO.BuscarListaGerenteMercado(filtro)
                Dim viewModeList As List(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado), List(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel))(voList)

                dadosPesquisa.ListPaged = Grid.PaginarLista(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel)(viewModeList, dadosPesquisa.Page)
                dadosPesquisa.TotalCount = viewModeList.Count
                dadosPesquisa.ResultID = "divGridPesquisaPublicoAlvoGerenteMercado"
                dadosPesquisa.PaginationID = "divGridPesquisaPublicoAlvoGerenteMercado"
                dadosPesquisa.ActionName = "PesquisaMecanicaPromocaoWidgetLoadPesquisaGerenteMercado"
                dadosPesquisa.ControllerName = "RelacaoAcaoMecanicaPublicoAlvoGerenteMercado"

                Return PartialView("~/Views/AcaoComercial/Mecanica/_GridPesquisaPublicoAlvoGerenteMercado.vbhtml", dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function ListarPublicoAlvoGerenteMercado(obj As RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel) As PartialViewResult
            Try
                Dim acaoCache As AcaoComercialViewModel = Session.Item("acaoModel")

                Dim BO = New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO()

                Dim objPublicoAlvoGerenteMercado As VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado = Mapper.Map(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel, VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)(obj)

                Dim objPublicoAlvoGerenteMercadoList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado) = BO.ListarPublicoAlvoGerenteMercado(objPublicoAlvoGerenteMercado)

                Dim publicoAlvoGerenteMercadoList As List(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel) = Mapper.Map(Of List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado), List(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel))(objPublicoAlvoGerenteMercadoList)

                ControlaVisibilidadeComponentes(acaoCache)

                Return PartialView("~/Views/AcaoComercial/Mecanica/_WidgetPublicoAlvoGerenteMercado.vbhtml", publicoAlvoGerenteMercadoList)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Metodo salvar dados
        <HttpPost()>
        Function Save(ByVal obj As RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel) As PartialViewResult
            Dim relacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO = New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO()

            If (ModelState.IsValid) Then
                Dim relacaoAcaoMecanicaPublicoAlvoGerenteMercado = Mapper.Map(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel, VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)(obj)
                'relacaoAcaoMecanica.DesMecanica = "AI                  "
                relacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO.InsertT(relacaoAcaoMecanicaPublicoAlvoGerenteMercado)
                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                Return Nothing
            End If


            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function

        <HttpPost()>
        Function Delete(ByVal obj As RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel) As PartialViewResult
            Dim relacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO = New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO()

            If (obj.CODACOCMC Is Nothing Or obj.NUMSEQRGRACOCMC Is Nothing) Then
                Warning("Campos obrigatórios não informados")
                Return Nothing
            End If


            If (ModelState.IsValid) Then
                Dim relacaoAcaoMecanicaPublicoAlvoGerenteMercado = Mapper.Map(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel, VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)(obj)
                'relacaoAcaoMecanica.DesMecanica = "AI                  "
                relacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO.DeleteT(relacaoAcaoMecanicaPublicoAlvoGerenteMercado)
                Success(Constantes.REGISTROS_EXCLUIDOS_SUCESSO)
                Return Nothing
            End If


            Throw New WebFaultException(HttpStatusCode.BadRequest)
            Return Nothing

        End Function

        'Adiciona o resultado da pesquisa
        <HttpPost()>
        Function AddPesquisa(filtro As RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel) As PartialViewResult
            Try
                Dim filtroVO As VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado = Mapper.Map(Of RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel, VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)(filtro)

                Dim BO As RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO = New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO()
                BO.AddPesquisa(filtroVO)

                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return Nothing
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function
        Public Sub ControlaVisibilidadeComponentes(acao As AcaoComercialViewModel)

            ViewBag.PermiteEdicaoMecanicaPublicoAlvoGerenteMercado = (Not acao.CODSTAAPVACOCMC Is Nothing And (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) And acao.CODFNCCRIACOCMC = ControleAcesso.CodFnc)

        End Sub

        <HttpPost()>
        Public Function ImportarXlsGerenteMercado(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal QDEGRPMIXPMC As Decimal) As String
            Dim relacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO As New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO()

            If (Request.Files.Count <= 0) Then
                Warning("Selecione um arquivo para importar!")
                Return "FalseGerenteMercado"
            End If

            Dim savedFileName As String = ""
            If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                Warning("Não foi possivel abrir o arquivo!")
                Return "FalseGerenteMercado"
            End If
            For Each item As String In Request.Files

                Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                If (hpf.ContentLength = 0) Then
                    Continue For
                End If

                Dim dataDoArquivo = Date.Now
                Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                Dim index As Integer = nomeArquivo.IndexOf(".xls")
                nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                savedFileName = Path.Combine(caminho, nomeArquivo)
                hpf.SaveAs(savedFileName)
            Next

            Dim result = relacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO.ImportarExelGerenteMercado(savedFileName, CODACOCMC, NUMSEQRGRACOCMC)

            If (Not String.IsNullOrEmpty(result)) Then
                Warning(result.Replace(vbCrLf, "<br/>"))
                Return "FalseGerenteMercado"
            Else
                Return "TrueGerenteMercado"
            End If
        End Function


    End Class
End Namespace
