﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList
Imports Martins.AcoesMercadologicas.Core
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="10")>
    Public Class ParametrosController
        Inherits AbstractController

        Private parametrosBO As New ParamentroSistemaDeGestaoDeAcaoComercialBO()
        Private fornecedorBO As New FornecedoresBO()
        Private filialBO As New FilialEmpresaCorporativoBO()

        Function Index(objeto As ParamentroSistemaDeGestaoDeAcaoComercialViewModel) As ViewResult
            Try

                'busca os dados de parametro para carregar os campos ja preenchidos na tela
                Dim parametro As IEnumerable(Of ParamentroSistemaDeGestaoDeAcaoComercialViewModel) = Mapper.Map(Of List(Of VO.ParamentroSistemaDeGestaoDeAcaoComercial), List(Of ParamentroSistemaDeGestaoDeAcaoComercialViewModel))(parametrosBO.ListT(New VO.ParamentroSistemaDeGestaoDeAcaoComercial))

                carregaDropFornecedores(parametro.FirstOrDefault.CODFRNMKT) 'carrega o combo de fornecedores com o fornecedor que esta vinculado ao Parametro
                Dim gridConsulta = GridSettings(Of FilialEmpresaCorporativoViewModel).EmptyGrid("resultadosView", "", "Parametros")

                gridConsulta = PesquisarFiliaisCodigoCidade(gridConsulta) 'chama função de pesquisa trazendo a lista de Relacao Filial Codigo Cidade
                gridConsulta.ActionName = "Pesquisar" 'indica que a paginação é com outra função que retorna um tipo PartialViewl
                gridConsulta.ControllerName = "Parametros"

                parametro.FirstOrDefault().list = gridConsulta

                Return View(parametro.FirstOrDefault())

                'Return View(gridConsulta) 'retorna o griSettings com a consulta e os filtros atribuidos
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Function PesquisarFiliaisCodigoCidade(dadosPesquisa As GridSettings(Of FilialEmpresaCorporativoViewModel)) As GridSettings(Of FilialEmpresaCorporativoViewModel)
            Try
                Dim filialList As List(Of VO.FilialEmpresaCorporativo) = filialBO.ListRelacaoFilialCodigoCidade()
                Dim lilialListViewModel As List(Of FilialEmpresaCorporativoViewModel) = Mapper.Map(Of List(Of VO.FilialEmpresaCorporativo), List(Of FilialEmpresaCorporativoViewModel))(filialList)

                Dim gridConf = PaginarEAtualizarGrid(lilialListViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridConf.TotalCount = 0 Then Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)

                Return gridConf
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        'Pesquisar as Filiais - Paginação
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of FilialEmpresaCorporativoViewModel)) As PartialViewResult
            Try
                Dim filialList As List(Of VO.FilialEmpresaCorporativo) = filialBO.ListRelacaoFilialCodigoCidade()
                Dim lilialListViewModel As List(Of FilialEmpresaCorporativoViewModel) = Mapper.Map(Of List(Of VO.FilialEmpresaCorporativo), List(Of FilialEmpresaCorporativoViewModel))(filialList)

                Dim gridConf = PaginarEAtualizarGrid(lilialListViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridConf.TotalCount = 0 Then Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)

                Return PartialView("~/Views/Parametros/_ResultadosView.vbhtml", gridConf)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Private Sub carregaDropFornecedores(ByVal codFrn As Decimal?)
            Try

                ViewBag.fornecedorAcao = Constantes.FornecedoresCodFrn(codFrn)
            Catch ex As Exception
                Danger(ex)
            End Try
        End Sub

        Function Edit(paramViewlModel As ParamentroSistemaDeGestaoDeAcaoComercialViewModel) As ActionResult

            'busca os dados da ação comercial e mostra na tela - funcao feita para ser usada quando o usuario vier redirecionado da tela de Consulta Saldo Ação
            Try
                'busca os dados da acção comercial e mostra na tela - funcao feita para ser usada quando o usuario vier redirecionado da tela de Consulta Saldo Ação
                Dim dadosPesquisaBD = parametrosBO.BuscarParametroPorId(paramViewlModel.CODEMP)

                'atribui os paraemtros da tela para o OBJETO que vai par ao banco
                dadosPesquisaBD.VLRMAXACOCMC = IIf(String.IsNullOrEmpty(paramViewlModel.VLRMAXACOCMC), 0, paramViewlModel.VLRMAXACOCMC)
                dadosPesquisaBD.VLRMNMITERSU = IIf(String.IsNullOrEmpty(paramViewlModel.VLRMNMITERSU), 0, paramViewlModel.VLRMNMITERSU)
                dadosPesquisaBD.VLRTXAADMFRNACOCMC = IIf(String.IsNullOrEmpty(paramViewlModel.VLRTXAADMFRNACOCMC), 0, paramViewlModel.VLRTXAADMFRNACOCMC)
                dadosPesquisaBD.PERCALENCFOLPGT = IIf(String.IsNullOrEmpty(paramViewlModel.PERCALENCFOLPGT), 0, paramViewlModel.PERCALENCFOLPGT)
                dadosPesquisaBD.VLRTXAADMBDEACOCMC = IIf(String.IsNullOrEmpty(paramViewlModel.VLRTXAADMBDEACOCMC), 0, paramViewlModel.VLRTXAADMBDEACOCMC)
                dadosPesquisaBD.PERMRGADIBDEPMC = IIf(String.IsNullOrEmpty(paramViewlModel.PERMRGADIBDEPMC), 0, paramViewlModel.PERMRGADIBDEPMC)


                'se nao colocar fornecedor o sistema insere o fornecedor anterior (da mesma forma que a tela no sistema antigo)
                If IsNothing(paramViewlModel.CODFRNMKT) = False Then dadosPesquisaBD.CODFRNMKT = paramViewlModel.CODFRNMKT

                parametrosBO.UpdateT(dadosPesquisaBD) 'altera registro no banco

                Success(Constantes.REGISTRO_ALTERADO_SUCESSO)

                Return View(paramViewlModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

#Region "Funções Controler Filial"

        ''' <summary>
        ''' Carrega pagina princial
        ''' </summary>
        ''' <param name="CodFilEmp"></param>
        ''' <returns></returns>
        Function IndexFilial(Optional ByVal CodFilEmp As Decimal? = 0) As ViewResult
            Try
                'busca a filial pelo codigo
                'Dim filialList = Mapper.Map(Of List(Of VO.FilialEmpresaCorporativo), List(Of FilialEmpresaCorporativoViewModel))(filialBO.ListT(New VO.FilialEmpresaCorporativo)).Where(Function(f) f.CODFILEMP = CodFilEmp).FirstOrDefault()

                Dim filialList = filialBO.ListT(New VO.FilialEmpresaCorporativo).Where(Function(f) f.CODFILEMP = CodFilEmp).FirstOrDefault()

                If (filialList IsNot Nothing) Then
                    filialList.NOMFILEMP = filialList.NOMFILEMP.Trim()
                    carregaDropCidades(filialList.CODCIDPCOACOCMC) 'carrega o combo de cidades com a cidade que esta vinculado ao Parametro

                    Dim filialEmpresaCorporativoViewModel = Mapper.Map(Of VO.FilialEmpresaCorporativo, FilialEmpresaCorporativoViewModel)(filialList)
                    filialEmpresaCorporativoViewModel.CODCID = filialList.CODCIDPCOACOCMC

                    Return View(filialEmpresaCorporativoViewModel)
                Else
                    Return Nothing
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        <HttpPost()>
        Function EditFilial(ByVal filialEmpresaCorporativoViewModel As FilialEmpresaCorporativoViewModel) As ActionResult
            Try
                Dim filialEmpresaCorporativo = Mapper.Map(Of FilialEmpresaCorporativoViewModel, VO.FilialEmpresaCorporativo)(filialEmpresaCorporativoViewModel)

                If filialEmpresaCorporativo.CODCIDPCOACOCMC.HasValue AndAlso filialEmpresaCorporativo.CODCIDPCOACOCMC.Value > 0 Then

                    'If paramViewlModel.CODCIDPCOACOCMC.HasValue AndAlso paramViewlModel.CODCIDPCOACOCMC.Value > 0 Then

                    Dim codFil As Decimal = Replace(filialEmpresaCorporativoViewModel.CODFILEMP, ",", String.Empty)
                    Dim codCid As Decimal = Replace(filialEmpresaCorporativoViewModel.CODCIDPCOACOCMC, ",", String.Empty)

                    'chama função que altera registro no banco
                    filialBO.atualizaCodigoCidade(codFil, codCid)
                    Success(Constantes.REGISTRO_ALTERADO_SUCESSO)
                Else
                    Information(Constantes.INFORME_CODIGO_CIDADE_CORRETAMENTE)
                End If
                Return View(filialEmpresaCorporativoViewModel)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Private Sub carregaDropCidades(ByVal codCid As Decimal?)
            ViewBag.cidades = BuscarCidades(codCid)
        End Sub

        ''' <summary>
        ''' Busca fornecedores (se passar o codigo retorna um só, se o parametro for null retorna todos os fornecedores)
        ''' </summary>
        ''' <param name="codCid"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function BuscarCidades(ByVal codCid As Decimal?) As List(Of CidadeViewModel)
            Try
                Dim cidadeBO As New CidadeBO()

                'veririca se ira filtrar por codido da cidade
                Dim ListCid As List(Of VO.Cidade) = IIf(codCid IsNot Nothing And codCid > 0, cidadeBO.ListT(New VO.Cidade With {.CODCID = codCid}), cidadeBO.ListT(New VO.Cidade))
                Dim cidades As List(Of CidadeViewModel) = Mapper.Map(Of List(Of VO.Cidade), List(Of CidadeViewModel))(ListCid).ToList() 'converte a lista para View Model

                If cidades Is Nothing Or cidades.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    Return Nothing
                End If

                Return cidades

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Busca fornecedores (se passar o codigo retorna um só, se o parametro for null retorna todos os fornecedores)
        ''' </summary>
        ''' <param name="codCid"></param>
        ''' <returns></returns>
        <HttpPost()>
        Public Function BuscarDropCidades(ByVal codCid As Decimal?) As JsonResult
            Try
                Dim cidadeBO As New CidadeBO()

                'veririca se ira filtrar por codido da cidade
                Dim ListCid As List(Of VO.Cidade) = IIf(codCid IsNot Nothing And codCid > 0, cidadeBO.ListT(New VO.Cidade With {.CODCID = codCid}), cidadeBO.ListT(New VO.Cidade))
                Dim cidades As List(Of CidadeViewModel) = Mapper.Map(Of List(Of VO.Cidade), List(Of CidadeViewModel))(ListCid).ToList() 'converte a lista para View Model

                If cidades Is Nothing Or cidades.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    Return Nothing
                End If

                Dim listaCidades = cidades.Select(Function(x) New With {.CODCID = x.CODCID, .NOMCID = x.NOMCID}).ToList()

                Return Json(listaCidades, JsonRequestBehavior.AllowGet)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

#End Region

    End Class

End Namespace
