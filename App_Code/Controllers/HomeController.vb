﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core.Controllers

Namespace Controllers
    Public Class HomeController
        Inherits AbstractController

        Function Index() As ActionResult

            'Session("IsMobile") = Util.IsMobile(Request)
            Return View()
        End Function

        Function About() As ActionResult
            ViewBag.Message = "Your application description page."
            Return View()
        End Function

        Function Contact() As ActionResult
            ViewBag.Message = "Your contact page."
            Return View()
        End Function

    End Class
End Namespace