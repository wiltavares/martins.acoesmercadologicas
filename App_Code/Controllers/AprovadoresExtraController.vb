﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Core
Imports PagedList

<Authorize(Roles:="16")>
Public Class AprovadoresExtraController
    Inherits AbstractController
    Private aprovadoresExtraBO As New AprovadoresExtraBO
    Private relacaoTipoEventoxAprovadoresExtraBO As New RelacaoTipoEventoxAprovadoresExtraBO
    Private tipoDeAprovadoresDoFluxoDeAprovacaoBO As New TipoDeAprovadoresDoFluxoDeAprovacaoBO
    Private funcionarioBO As New FuncionarioBO

    ' GET: /AprovadoresExtra

#Region "Modal Funcionários"
    <HttpPost()>
    Function AbrirModalFuncionario(objeto As String) As PartialViewResult
        Try
            Dim dados As New VO.Funcionario
            dados.NOMFNC = objeto

            Dim gridConf As New GridSettings(Of FuncionarioViewModel)

            If (dados.NOMFNC <> "") Then
                Dim grid = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(funcionarioBO.BuscaFuncionarios(dados))
                gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT})

                gridConf.ResultID = "resultadosFuncionarios"
                gridConf.PaginationID = "resultadosFuncionarios"
                gridConf.ActionName = "PesquisarDadosFuncionario"
                gridConf.ControllerName = "AprovadoresExtra"
                gridConf.Filter = New FuncionarioViewModel With {.NOMFNC = objeto}

                If grid.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If
            Else
                Information(Constantes.FILTRO_OBRIGATORIO)
                gridConf = GridSettings(Of FuncionarioViewModel).EmptyGrid()
            End If

            Return PartialView("~/Views/AprovadoresExtra/_ModalBuscaFuncionarios.vbhtml", gridConf)
        Catch ex As Exception
            Danger(ex)
            Return Nothing
        End Try
    End Function

    <HttpPost()>
    Function PesquisarDadosFuncionario(dadosPesquisa As GridSettings(Of FuncionarioViewModel)) As PartialViewResult
        Try
            Dim funcionarios As VO.Funcionario = Mapper.Map(Of FuncionarioViewModel, VO.Funcionario)(dadosPesquisa.Filter)
            Dim listFuncionarios As List(Of VO.Funcionario) = funcionarioBO.BuscaFuncionarios(funcionarios)
            Dim funcionarioViewModel As List(Of FuncionarioViewModel) = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(listFuncionarios)

            Dim gridConf = PaginarEAtualizarGrid(funcionarioViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
            If gridConf.TotalCount = 0 Then
                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
            End If

            gridConf.ResultID = "resultadosFuncionarios"
            gridConf.PaginationID = "resultadosFuncionarios"
            gridConf.ActionName = "PesquisarDadosFuncionario"
            gridConf.ControllerName = "AprovadoresExtra"
            gridConf.Filter = dadosPesquisa.Filter

            Return PartialView("~/Views/AprovadoresExtra/_ResultadosViewFuncionarios.vbhtml", gridConf)

        Catch ex As Exception
            Danger(ex)
            Return Nothing
        End Try
    End Function

    <HttpPost()>
    Function BuscaFuncionariosAprovadores(ByVal codfuncionario As Decimal?, ByVal nomfuncionario As String) As PartialViewResult
        Try
            Dim dados As New VO.Funcionario
            dados.CODFNC = codfuncionario
            dados.NOMFNC = nomfuncionario

            Dim grid = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(funcionarioBO.BuscaFuncionarios(dados))

            Dim gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT})

            If (codfuncionario <> 0 Or nomfuncionario <> "") Then
                If grid.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If
            Else
                Information(Constantes.FILTRO_OBRIGATORIO)
                gridConf = GridSettings(Of FuncionarioViewModel).EmptyGrid()
            End If

            gridConf.ResultID = "resultadosFuncionarios"
            gridConf.PaginationID = "resultadosFuncionarios"
            gridConf.ActionName = "PesquisarDadosFuncionario"
            gridConf.ControllerName = "AprovadoresExtra"
            gridConf.Filter = New FuncionarioViewModel With {.CODFNC = codfuncionario, .NOMFNC = nomfuncionario}

            Return PartialView("~/Views/AprovadoresExtra/_ResultadosViewFuncionarios.vbhtml", gridConf)

        Catch ex As Exception
            Danger(ex)
            Return Nothing
        End Try
    End Function

#End Region

#Region "Cadastro de Aprovadores Extra"
    Function Index() As ViewResult
        CarregaTipoEvento()
        CarregaTipoAprovador()
        ViewBag.ListaTipoAprovadorSubstituido = New List(Of VO.TipoDeAprovadoresDoFluxoDeAprovacao)

        Return View(GridSettings(Of AprovadoresExtraViewModel).EmptyGrid())
    End Function

    <HttpPost()>
    Function Pesquisar(dadosPesquisa As GridSettings(Of AprovadoresExtraViewModel)) As PartialViewResult
        Try
            Dim aprovadoresExtra As VO.AprovadoresExtra = Mapper.Map(Of AprovadoresExtraViewModel, VO.AprovadoresExtra)(dadosPesquisa.Filter)
            Dim aprovadoresExtraList As List(Of VO.AprovadoresExtra) = aprovadoresExtraBO.CarregaGrid(aprovadoresExtra)
            Dim aprovadoresExtraViewModel As List(Of AprovadoresExtraViewModel) = Mapper.Map(Of List(Of VO.AprovadoresExtra), List(Of AprovadoresExtraViewModel))(aprovadoresExtraList)

            Dim gridConf = PaginarEAtualizarGrid(aprovadoresExtraViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)

            If (dadosPesquisa.Filter.NOMFNC <> "" Or dadosPesquisa.Filter.TIPEVTACOCMC <> 0 Or dadosPesquisa.Filter.CODTIPAPV <> 0 Or dadosPesquisa.Filter.CODEDEAPV <> 0) Then
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If
            Else
                Information(Constantes.FILTRO_OBRIGATORIO)
                gridConf = GridSettings(Of AprovadoresExtraViewModel).EmptyGrid()
            End If

            Return PartialView("~/Views/AprovadoresExtra/_ResultadosView.vbhtml", gridConf)
        Catch ex As Exception
            Danger(ex)
            Return Nothing
        End Try
    End Function

    <HttpPost()>
    Function PesquisarRegistrosInseridos(ByVal codTipoEvento As Decimal, ByVal codAprovSub As Decimal, ByVal codAprovExtra As Decimal, ByVal codFuncionario As Decimal) As PartialViewResult
        Try
            Dim aprovadoresExtra As New VO.AprovadoresExtra
            aprovadoresExtra.TIPEVTACOCMC = codTipoEvento
            aprovadoresExtra.CODTIPAPV = codAprovSub
            aprovadoresExtra.CODEDEAPV = codAprovExtra
            aprovadoresExtra.CODEDEAPVEXA = codFuncionario

            Dim aprovadoresExtraList As List(Of VO.AprovadoresExtra) = aprovadoresExtraBO.CarregaRegistrosInseridos(aprovadoresExtra)
            Dim aprovadoresExtraViewModel As List(Of AprovadoresExtraViewModel) = Mapper.Map(Of List(Of VO.AprovadoresExtra), List(Of AprovadoresExtraViewModel))(aprovadoresExtraList)

            Dim gridConf As GridSettings(Of AprovadoresExtraViewModel) = PaginarEAtualizarGrid(aprovadoresExtraViewModel, New PageSettings, New AprovadoresExtraViewModel())

            Return PartialView("~/Views/AprovadoresExtra/_ResultadosView.vbhtml", gridConf)
        Catch ex As Exception
            Danger(ex)
            Return Nothing
        End Try
    End Function

    <HttpPost()>
    Function IncluirRegistros(ByVal codTipoEvento As Decimal, ByVal codAprovSub As Decimal, ByVal codAprovExtra As Decimal, ByVal codFuncionario As Decimal) As PartialViewResult
        Try
            aprovadoresExtraBO.InsereRegistrosAprovadores(codTipoEvento, codAprovSub, codAprovExtra, codFuncionario)
            Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
            Return PesquisarRegistrosInseridos(codTipoEvento, codAprovSub, codAprovExtra, codFuncionario)

        Catch ex As Exception
            Danger(ex)
            Return Nothing
        End Try
    End Function

    Function Delete(ByVal id As String, dadosPesquisa As GridSettings(Of AprovadoresExtraViewModel)) As PartialViewResult
        Try
            Dim dados As IEnumerable(Of String) = Split(id, ":")

            Dim filtro As New VO.AprovadoresExtra()
            filtro.TIPEVTACOCMC = CDec(dados(0))
            filtro.CODTIPAPV = CDec(dados(1))
            filtro.CODEDEAPV = CDec(dados(2))

            aprovadoresExtraBO.DeleteT(filtro)
            Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)

            Return Pesquisar(dadosPesquisa)
        Catch ex As Exception
            Danger(ex)
            Return Nothing
        End Try
    End Function

    <HttpPost()>
    Function CarregaSubstituido(ByVal codTipoAprovador As Decimal?) As JsonResult
        Try
           
        Dim retorno As IEnumerable(Of AprovadoresExtraViewModel)
        retorno = Mapper.Map(Of List(Of VO.AprovadoresExtra), List(Of AprovadoresExtraViewModel))(aprovadoresExtraBO.CarregaAprovadorSubstituido(codTipoAprovador))

            Return Json(retorno, JsonRequestBehavior.AllowGet)

        Catch ex As Exception
            Danger(ex)
            Return Nothing
        End Try

    End Function

    Public Sub CarregaTipoEvento()
        Dim tipoevento As List(Of VO.RelacaoTipoEventoxAprovadoresExtra) = relacaoTipoEventoxAprovadoresExtraBO.ListT(New VO.RelacaoTipoEventoxAprovadoresExtra())
        ViewBag.ListaTipoEvento = tipoevento
    End Sub

    Public Sub CarregaTipoAprovador()
        Dim tipoaprovador As List(Of VO.TipoDeAprovadoresDoFluxoDeAprovacao) = tipoDeAprovadoresDoFluxoDeAprovacaoBO.CarregaTipoAprovador()
        ViewBag.ListaTipoAprovador = tipoaprovador
    End Sub

    Public Sub CarregaFuncionarios()
        Dim funcionarios As List(Of VO.Funcionario) = funcionarioBO.ListT(New VO.Funcionario())
        ViewBag.ListaFuncionarios = funcionarios
    End Sub

    Public Sub CarregaAprovadorSubstituido(ByVal CodTipoAprovador As Decimal?)

        Dim aprovadoresSubst As List(Of VO.AprovadoresExtra) = aprovadoresExtraBO.CarregaAprovadorSubstituido(CodTipoAprovador)
        ViewBag.ListaTipoAprovadorSubstituido = aprovadoresSubst
    End Sub

#End Region

End Class