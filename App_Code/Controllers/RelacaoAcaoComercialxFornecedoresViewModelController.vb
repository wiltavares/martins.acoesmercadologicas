﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
'Imports Martins.AcoesMercadologicas.VO
Imports PagedList

Namespace Controllers
    <Authorize(Roles:="12,17,14")>
    Public Class RelacaoAcaoComercialxFornecedoresViewModelController
        Inherits AbstractController

        ' GET: RelacaoAcaoComercialxFornecedoresViewModel
        Function Index() As ActionResult
            Return View()
        End Function

    End Class
End Namespace