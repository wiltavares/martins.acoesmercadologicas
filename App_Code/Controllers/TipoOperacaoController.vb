﻿Imports System.Web.Mvc
Imports AutoMapper
Imports PagedList

Namespace Controllers
    <Authorize(Roles:="8")>
    Public Class TipoOperacaoController
        Inherits AbstractController
        Private tipoOperacaoBO As New OperacaoFiscalBO()
        ''' <summary>
        ''' Carrega a tela de consulta
        ''' </summary>
        ''' <returns></returns>
        Function Index() As ViewResult
            Return View(GridSettings(Of TipoOperacaoViewModel).EmptyGrid())
        End Function


        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of TipoOperacaoViewModel)) As PartialViewResult
            Try

            Dim tipo As VO.OperacaoFiscal = Mapper.Map(Of TipoOperacaoViewModel, VO.OperacaoFiscal)(dadosPesquisa.Filter)
            Dim tipoOperacao As List(Of VO.OperacaoFiscal) = tipoOperacaoBO.ListT(tipo)
                Dim tipoOperacaoViewModel As List(Of TipoOperacaoViewModel) = Mapper.Map(Of List(Of VO.OperacaoFiscal), List(Of TipoOperacaoViewModel))(tipoOperacao)

            Dim gridConf = PaginarEAtualizarGrid(tipoOperacaoViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
            If gridConf.TotalCount = 0 Then
                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
            End If
                Return PartialView("~/Views/TipoOperacao/_ResultadosView.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function


        Function Create() As ActionResult
            ViewBag.titulo = "Incluir Tipo Operacao"

            Dim retorno As TipoOperacaoViewModel = New TipoOperacaoViewModel()
            Dim maior As IEnumerable(Of VO.OperacaoFiscal) = tipoOperacaoBO.ListT(New VO.OperacaoFiscal())
            retorno.CODOPEFSCDSNACOCMC = maior.Max(Function(f) f.CODOPEFSCDSNACOCMC) + 1

            retorno.CODOPEFSCDSNACOCMC = Nothing
            retorno.INDTIPOPEDSNACOCMC = 0
            retorno.INDUNDESRNGCACOCMC = 0
            retorno.INDBDEOPEDSNACOCMC = 0
            retorno.INDTIPOPEICT = 0
            retorno.INDTIPOPEVLRMON = 0
            retorno.INDGRCITEENCFOLPGT = 0
            retorno.INDGRCREQAUTITERSU = 0
            retorno.INDTIPOPEACOCMCAUT = 0
            retorno.INDTIPOPEREQVGM = 0
            retorno.INDTIPOPEREQEVT = 0
            retorno.INDTIPOPESLCPGT = 0
            retorno.INDPTCCLIOPEDSN = 0

            Return View(retorno)
        End Function

        Function CreateEdit(Optional ByVal id As Integer? = Nothing) As ActionResult
            If id Is Nothing Then
                ViewBag.acao = "Save"
                Return Create()
            Else
                ViewBag.acao = "Alterar"
                Return Edit(id)
            End If
        End Function

        Function Edit(ByVal id As Integer) As ActionResult
            Dim filtro As New VO.OperacaoFiscal()
            filtro.CODOPEFSCDSNACOCMC = id

            Dim tipoOperacao = tipoOperacaoBO.SelectT(filtro)
            Dim tipoOperacaoViewModel = Mapper.Map(Of VO.OperacaoFiscal, TipoOperacaoViewModel)(tipoOperacao)

            Return View(tipoOperacaoViewModel)
        End Function

        <HttpPost()>
        Function Save(ByVal tipoOperacaoViewModel As TipoOperacaoViewModel) As ActionResult
            Try
                If (Not ModelState.IsValid) Then
                    Return View("CreateEdit", tipoOperacaoViewModel)
                End If

                Dim tipoOperacao = Mapper.Map(Of TipoOperacaoViewModel, VO.OperacaoFiscal)(tipoOperacaoViewModel)

                tipoOperacaoBO.InsertT(tipoOperacao)
                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)


                Return RedirectToAction("Index")
            Catch ex As Exception
                Danger(ex)
                Return View("CreateEdit", tipoOperacaoViewModel)
            End Try
        End Function

        <HttpPost()>
        Function Alterar(ByVal tipoOperacaoViewModel As TipoOperacaoViewModel) As ActionResult
            Try
                If (Not ModelState.IsValid) Then
                    Return View("CreateEdit", tipoOperacaoViewModel)
                End If

                Dim tipoOperacao = Mapper.Map(Of TipoOperacaoViewModel, VO.OperacaoFiscal)(tipoOperacaoViewModel)

                tipoOperacaoBO.UpdateT(tipoOperacao)
                Success(Constantes.REGISTRO_ALTERADO_SUCESSO)

                Return RedirectToAction("Index")
            Catch ex As Exception
                Danger(ex)
                Return View("CreateEdit", tipoOperacaoViewModel)
            End Try
        End Function


        Function Delete(ByVal id As Decimal, dadosPesquisa As GridSettings(Of TipoOperacaoViewModel)) As PartialViewResult
            Try
                Dim filtro As New VO.OperacaoFiscal()
                filtro.CODOPEFSCDSNACOCMC = id
                tipoOperacaoBO.DeleteT(filtro)
                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                Return Pesquisar(dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Pesquisar(dadosPesquisa)
            End Try
        End Function

    End Class
End Namespace