﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList

Namespace Controllers
    <Authorize(Roles:="6")>
    Public Class TipoEventoController
        Inherits AbstractController
        Private tipoEventoBO As New TipoDeEventoBO()
        Private ContContabil As New ContaContabilBO()

        ''' <summary>
        ''' Carrega a tela de consulta
        ''' </summary>
        ''' <returns></returns>
        Function Index(dadosPesquisa As GridSettings(Of TipoEventoViewModel)) As ViewResult
            CarregaDrops()

            Return View(GridSettings(Of TipoEventoViewModel).EmptyGrid())
        End Function



        ''' <summary>
        ''' Carrega listas de dropdown das telas de cadastro e pesquisa
        ''' </summary>
        Public Sub CarregaDrops()

            'Dim contacontabil As List(Of VO.ContaContabil) = ContContabil.ListT(New VO.ContaContabil())
            'ViewBag.CODCNTCTB = contacontabil.Select(Function(item) New SelectListItem() With {.Text = item.DESCNTCTB, .Value = item.CODCNTCTB}).OrderBy(Function(f) f.Text).ToList()

            Dim contacontabil As List(Of VO.ContaContabil) = ContContabil.ListT(New VO.ContaContabil())
            ViewBag.ListaContaContabil = contacontabil

        End Sub

        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of TipoEventoViewModel)) As PartialViewResult
            Try
                Dim tipo As VO.TipoDeEvento = Mapper.Map(Of TipoEventoViewModel, VO.TipoDeEvento)(dadosPesquisa.Filter)

                Dim tipoEvento As List(Of VO.TipoDeEvento) = tipoEventoBO.buscaDadosTipoEvento(tipo)

                Dim tipoEventoViewModel As List(Of TipoEventoViewModel) = Mapper.Map(Of List(Of VO.TipoDeEvento), List(Of TipoEventoViewModel))(tipoEvento)

                Dim gridConf = PaginarEAtualizarGrid(tipoEventoViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If
                Return PartialView("~/Views/TipoEvento/_ResultadosView.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        Function Create() As ActionResult

            Dim retorno As TipoEventoViewModel = New TipoEventoViewModel()
            Dim maior As IEnumerable(Of VO.TipoDeEvento) = tipoEventoBO.ListT(New VO.TipoDeEvento())
            retorno.TIPEVTACOCMC = maior.Max(Function(f) f.TIPEVTACOCMC) + 1

            retorno.INDVLDRGRICT = 0
            retorno.INDPREARDPMC = 1
            retorno.INDCPHBTB = 0
            retorno.INDEVTSMA = 0

            CarregaDrops()
            Return View(retorno)
        End Function

        Function CreateEdit(Optional ByVal id As Integer? = Nothing) As ActionResult

            If id Is Nothing Then
                ViewBag.acao = "Save"
                Return Create()
            Else
                ViewBag.acao = "Alterar"
                Return Edit(id)
            End If
        End Function

        Function Edit(ByVal id As Integer) As ActionResult
            CarregaDrops()
            Dim filtro As New VO.TipoDeEvento()
            filtro.TIPEVTACOCMC = id

            Dim tipoEvento = tipoEventoBO.SelectT(filtro)
            Dim tipoEventoViewModel = Mapper.Map(Of VO.TipoDeEvento, TipoEventoViewModel)(tipoEvento)

            Return View(tipoEventoViewModel)
        End Function

        <HttpPost()>
        Function Save(ByVal tipoEventoViewModel As TipoEventoViewModel) As ActionResult
            Try
                CarregaDrops()
                If (Not ModelState.IsValid) Then
                    Return View("CreateEdit", tipoEventoViewModel)
                End If

                Dim tipoEvento = Mapper.Map(Of TipoEventoViewModel, VO.TipoDeEvento)(tipoEventoViewModel)

                tipoEventoBO.InsertT(tipoEvento)
                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                Return RedirectToAction("Index")
            Catch ex As Exception
                Danger(ex)
                Return View("CreateEdit", tipoEventoViewModel)
            End Try
        End Function


        <HttpPost()>
        Function Alterar(ByVal tipoEventoViewModel As TipoEventoViewModel) As ActionResult
            Try
                CarregaDrops()
                If (Not ModelState.IsValid) Then
                    Return View("CreateEdit", tipoEventoViewModel)
                End If

                Dim tipoEvento = Mapper.Map(Of TipoEventoViewModel, VO.TipoDeEvento)(tipoEventoViewModel)
                tipoEventoBO.UpdateT(tipoEvento)
                Success(Constantes.REGISTRO_ALTERADO_SUCESSO)

                Return RedirectToAction("Index")
            Catch ex As Exception
                Danger(ex)
                Return View("CreateEdit", tipoEventoViewModel)
            End Try
        End Function


        ''' <summary>
        ''' Deleta um registro
        ''' </summary>
        ''' <param name="id"></param>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Delete(ByVal id As Decimal, dadosPesquisa As GridSettings(Of TipoEventoViewModel)) As PartialViewResult
            Try
                Dim filtro As New VO.TipoDeEvento()
                filtro.TIPEVTACOCMC = id
                tipoEventoBO.DeleteT(filtro)
                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                Return Pesquisar(dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Pesquisar(dadosPesquisa)
            End Try
        End Function

    End Class
End Namespace