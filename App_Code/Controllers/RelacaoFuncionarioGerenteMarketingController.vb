﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList

Namespace Controllers
    <Authorize(Roles:="11")>
    Public Class RelacaoFuncionarioGerenteMarketingController
        Inherits AbstractController
        Private funcionarioBO As New FuncionarioBO

#Region "Metodos Modal Funcionario"
        <HttpPost()>
        Function AbrirModalFuncionario(objeto As String) As PartialViewResult
            Try

                Dim dados As New VO.Funcionario

                dados.NOMFNC = objeto

                Dim dadosPesquisa As New GridSettings(Of FuncionarioViewModel)
                dadosPesquisa.Filter = New FuncionarioViewModel()

                Dim grid = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(funcionarioBO.BuscaFuncionarios(dados))

                Dim gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT})

                gridConf.ResultID = "resultadosFuncionarios"
                gridConf.PaginationID = "resultadosFuncionarios"
                gridConf.ActionName = "PesquisarDadosFuncionario"
                gridConf.ControllerName = "RelacaoFuncionarioGerenteMarketing"
                gridConf.Filter = New FuncionarioViewModel With {.NOMFNC = objeto}

                If grid.Count = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                Return PartialView("~/Views/RelacaoFuncionarioGerenteMarketing/_ModalBuscaFuncionarios.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        <HttpPost()>
        Function PesquisarDadosFuncionario(dadosPesquisa As GridSettings(Of FuncionarioViewModel)) As PartialViewResult
            Try


                Dim funcionarioBO As New FuncionarioBO

                Dim funcionarios As VO.Funcionario = Mapper.Map(Of FuncionarioViewModel, VO.Funcionario)(dadosPesquisa.Filter)
                Dim listFuncionarios As List(Of VO.Funcionario) = funcionarioBO.BuscaFuncionarios(funcionarios)
                Dim funcionarioViewModel As List(Of FuncionarioViewModel) = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(listFuncionarios)

                Dim gridConf = PaginarEAtualizarGrid(funcionarioViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)

                If gridConf.TotalCount = 0 Then
                    Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                End If

                gridConf.ResultID = "resultadosFuncionarios"
                gridConf.PaginationID = "resultadosFuncionarios"
                gridConf.ActionName = "PesquisarDadosFuncionario"
                gridConf.ControllerName = "RelacaoFuncionarioGerenteMarketing"
                gridConf.Filter = dadosPesquisa.Filter

                Return PartialView("~/Views/RelacaoFuncionarioGerenteMarketing/_ResultadosViewFuncionarios.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function


        <HttpPost()>
        Function BuscaFuncionarios(ByVal codfuncionario As Decimal?, ByVal nomfuncionario As String) As PartialViewResult
            Try

                Dim dados As New VO.Funcionario
                dados.CODFNC = codfuncionario
                dados.NOMFNC = nomfuncionario

                Dim grid = Mapper.Map(Of List(Of VO.Funcionario), List(Of FuncionarioViewModel))(funcionarioBO.BuscaFuncionarios(dados))

                Dim gridConf = PaginarEAtualizarGrid(grid, New PageSettings With {.PageNumber = Constantes.PAGE_NUMBER_DEFAULT, .PageSize = Constantes.PAGE_SIZE_DEFAULT})

                If (codfuncionario <> 0 Or nomfuncionario <> "") Then
                    If grid.Count = 0 Then
                        Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    End If
                Else
                    Information(Constantes.FILTRO_OBRIGATORIO)
                    gridConf = GridSettings(Of FuncionarioViewModel).EmptyGrid()
                End If

                gridConf.ResultID = "resultadosFuncionarios"
                gridConf.PaginationID = "resultadosFuncionarios"
                gridConf.ActionName = "PesquisarDadosFuncionario"
                gridConf.ControllerName = "RelacaoFuncionarioGerenteMarketing"
                gridConf.Filter = New FuncionarioViewModel With {.CODFNC = codfuncionario, .NOMFNC = nomfuncionario}

                Return PartialView("~/Views/RelacaoFuncionarioGerenteMarketing/_ResultadosViewFuncionarios.vbhtml", gridConf)


            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

#End Region

#Region "Metodos Relacao Funcionario Gerente MRTK"

        ''' <summary>
        ''' Carrega a tela de consulta
        ''' </summary>
        ''' <returns></returns>
        Function Index() As ViewResult
            Try
                Dim gridConsulta = GridSettings(Of RelacaoFuncionarioGerenteMarketingViewModel).EmptyGrid()

                Return View(gridConsulta)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Consulta registros a partir do filtro da pesquisa
        ''' </summary>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of RelacaoFuncionarioGerenteMarketingViewModel)) As PartialViewResult
            Try
                Dim RelacaoFuncionarioGerenteMarketing As VO.RelacaoFuncionarioGerenteMarketing = Mapper.Map(Of RelacaoFuncionarioGerenteMarketingViewModel, VO.RelacaoFuncionarioGerenteMarketing)(dadosPesquisa.Filter)

                If (RelacaoFuncionarioGerenteMarketing.CODFUNC_EMITENTE > 0 OrElse RelacaoFuncionarioGerenteMarketing.CODFUNC_GER_MTK > 0) Then

                    Dim RelacaoFuncGerBO As New RelacaoFuncionarioGerenteMarketingBO()

                    Dim RelacaoFuncGer As List(Of VO.RelacaoFuncionarioGerenteMarketing) = RelacaoFuncGerBO.ListFuncionarioGerenteMrkt(RelacaoFuncionarioGerenteMarketing.CODFUNC_EMITENTE, RelacaoFuncionarioGerenteMarketing.CODFUNC_GER_MTK)

                    Dim relacaoFuncionarioGerenteMarketingViewModel As List(Of RelacaoFuncionarioGerenteMarketingViewModel) = Mapper.Map(Of List(Of VO.RelacaoFuncionarioGerenteMarketing), List(Of RelacaoFuncionarioGerenteMarketingViewModel))(RelacaoFuncGer)

                    Dim gridConf = PaginarEAtualizarGrid(relacaoFuncionarioGerenteMarketingViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
                    If gridConf.TotalCount = 0 Then
                        Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
                    End If

                    Return PartialView("~/Views/RelacaoFuncionarioGerenteMarketing/_ResultadosView.vbhtml", gridConf)
                Else
                    Information(Constantes.INFORME_FILTRO)
                    Return Nothing
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function



        ''' <summary>
        ''' Carrega a tela para editar um registro
        ''' </summary>
        ''' <returns></returns>
        <HttpPost()>
        Function Adicionar(codFuncEmiente As Decimal?, codFuncGerMtk As Decimal?) As PartialViewResult
            Try
                If codFuncEmiente > 0 AndAlso codFuncGerMtk > 0 Then

                    Dim RelacaoFuncGerBO As New RelacaoFuncionarioGerenteMarketingBO()

                    Dim dadosPesquisa = GridSettings(Of RelacaoFuncionarioGerenteMarketingViewModel).EmptyGrid("resultadosView", "Pesquisar", "RelacaoFuncionarioGerenteMarketing")

                    dadosPesquisa.Filter.CODFUNC_EMITENTE = codFuncEmiente
                    dadosPesquisa.Filter.CODFUNC_GER_MTK = codFuncGerMtk

                    RelacaoFuncGerBO.deleteRelacaoFornGerMrkt(codFuncEmiente, codFuncGerMtk) 'apaga o vinculo existente
                    RelacaoFuncGerBO.insereRelacaoFornGerMrkt(codFuncEmiente, codFuncGerMtk) 'insere novo vinculo

                    Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)

                    Return Pesquisar(dadosPesquisa) 'carrega a consulta novamente
                Else
                    Information(Constantes.INFORME_FILTRO)
                    Return Nothing
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Carrega a tela para editar um registro
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        Function Apagar(codFuncGerMtk As Decimal, codFuncEmiente As Decimal) As PartialViewResult
            Try
                If codFuncEmiente > 0 AndAlso codFuncGerMtk > 0 Then

                    Dim dadosPesquisa = GridSettings(Of RelacaoFuncionarioGerenteMarketingViewModel).EmptyGrid("resultadosView", "Pesquisar", "RelacaoFuncionarioGerenteMarketing")

                    ' dadosPesquisa.Filter.CODFUNC_EMITENTE = codFuncEmiente
                    dadosPesquisa.Filter.CODFUNC_GER_MTK = codFuncGerMtk

                    Dim RelacaoFuncGerBO As New RelacaoFuncionarioGerenteMarketingBO()

                    RelacaoFuncGerBO.deleteRelacaoFornGerMrkt(codFuncEmiente, codFuncGerMtk) 'apaga o vinculo existente

                    Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                    Return Pesquisar(dadosPesquisa) 'carrega a consulta novamente
                Else
                    Information(Constantes.INFORME_FILTRO)
                    Return Nothing
                End If

            Catch ex As Exception
                Danger(ex.Message)
                Return Nothing
            End Try
        End Function

#End Region

    End Class

End Namespace