﻿Imports System.Web.Mvc

Namespace Controllers
    Public Class GenericErrorController
        Inherits Controller

        ' GET: GenericError
        Function Index() As ActionResult
            Return View(ViewData.Model)
        End Function

    End Class
End Namespace