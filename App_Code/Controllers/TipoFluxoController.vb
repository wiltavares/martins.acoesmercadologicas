﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList

Namespace Controllers
    <Authorize(Roles:="7")>
    Public Class TipoFluxoController
        Inherits AbstractController

        Private tipoFluxoBO As New TipoDeFluxoBO()

        ''' <summary>
        ''' Carrega a tela de consulta
        ''' </summary>
        ''' <returns></returns>
        Function Index() As ViewResult
            Return View(GridSettings(Of TipoFluxoViewModel).EmptyGrid())
        End Function

        ''' <summary>
        ''' Carrega a tela para criar ou editar um registro
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        Function CreateEdit(Optional ByVal id As Integer? = Nothing) As ActionResult
            If id Is Nothing Then
                ViewBag.acao = "Salvar"
                Return Create()
            Else
                ViewBag.acao = "Alterar"
                Return Edit(id)
            End If
        End Function

        ''' <summary>
        ''' Carrega a tela para criar um registro
        ''' </summary>
        ''' <returns></returns>
        Function Create() As ActionResult
            Dim retorno As TipoFluxoViewModel = New TipoFluxoViewModel()
            Dim maior As IEnumerable(Of VO.TipoDeFluxo) = tipoFluxoBO.ListT(New VO.TipoDeFluxo())
            retorno.TIPFLUAPVACOCMC = maior.Max(Function(f) f.TIPFLUAPVACOCMC) + 1

            Return View(retorno)
        End Function

        ''' <summary>
        ''' Carrega a tela para editar um registro
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        Function Edit(ByVal id As Integer) As ActionResult

            Dim filtro As New VO.TipoDeFluxo()
            filtro.TIPFLUAPVACOCMC = id
            Dim tipoFluxo = tipoFluxoBO.SelectT(filtro)
            'Dim codigo As Integer
            'codigo = CInt(tipoFluxo.TIPFLUAPVACOCMC)

            Dim tipoFluxoModel = Mapper.Map(Of VO.TipoDeFluxo, TipoFluxoViewModel)(tipoFluxo)

            Return View(tipoFluxoModel)
        End Function


        ''' <summary>
        ''' Consulta registros a partir do filtro da pesquisa
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of TipoFluxoViewModel)) As PartialViewResult
            Try

            
            Dim filtro As VO.TipoDeFluxo = Mapper.Map(Of TipoFluxoViewModel, VO.TipoDeFluxo)(dadosPesquisa.Filter)
            Dim listaTipoFluxo As List(Of VO.TipoDeFluxo) = tipoFluxoBO.ListT(filtro)
            Dim listaTipoFluxoModel As List(Of TipoFluxoViewModel) = Mapper.Map(Of List(Of VO.TipoDeFluxo), List(Of TipoFluxoViewModel))(listaTipoFluxo)

            Dim gridAtualizado = PaginarEAtualizarGrid(listaTipoFluxoModel, dadosPesquisa.Page, dadosPesquisa.Filter)
            If gridAtualizado.TotalCount = 0 Then
                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
            End If
            Return PartialView("~/Views/TipoFluxo/_ResultadosView.vbhtml", gridAtualizado)
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Salva (insere) um registro
        ''' Sucesso: Redireciona para index
        ''' Fracasso: Permanece na tela com a mensagem de erro
        ''' </summary>
        ''' <param name="tipoFluxoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Salvar(ByVal tipoFluxoViewModel As TipoFluxoViewModel) As ActionResult
            Try
                If (Not ModelState.IsValid) Then
                    Return View("CreateEdit", tipoFluxoViewModel)
                End If

                Dim tipoFluxo = Mapper.Map(Of TipoFluxoViewModel, VO.TipoDeFluxo)(tipoFluxoViewModel)

                tipoFluxoBO.InsertT(tipoFluxo)
                Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)


                Return RedirectToAction("Index")
            Catch ex As Exception
                Danger(ex)
                Return View("CreateEdit", tipoFluxoViewModel)
            End Try
        End Function


        ''' <summary>
        ''' Atualiza um registro
        ''' Sucesso: Redireciona para index
        ''' Fracasso: Permanece na tela com a mensagem de erro
        ''' </summary>
        ''' <param name="tipoFluxoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Alterar(ByVal tipoFluxoViewModel As TipoFluxoViewModel) As ActionResult
            Try
                If (Not ModelState.IsValid) Then
                    Return View("CreateEdit", tipoFluxoViewModel)
                End If

                Dim tipoFluxo = Mapper.Map(Of TipoFluxoViewModel, VO.TipoDeFluxo)(tipoFluxoViewModel)

                tipoFluxoBO.UpdateT(tipoFluxo)
                Success(Constantes.REGISTRO_ALTERADO_SUCESSO)

                Return RedirectToAction("Index")
            Catch ex As Exception
                Danger(ex)
                Return View("CreateEdit", tipoFluxoViewModel)
            End Try
        End Function

        ''' <summary>
        ''' Deleta um registro e atualiza o grid
        ''' </summary>
        ''' <param name="id"></param>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        Function Deletar(ByVal id As Decimal, dadosPesquisa As GridSettings(Of TipoFluxoViewModel)) As PartialViewResult
            Try
                Dim filtro As New VO.TipoDeFluxo()
                filtro.TIPFLUAPVACOCMC = id
                tipoFluxoBO.DeleteT(filtro)
                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                Return Pesquisar(dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Pesquisar(dadosPesquisa)
            End Try
        End Function

    End Class
End Namespace