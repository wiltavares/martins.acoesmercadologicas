﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Martins.AcoesMercadologicas.Core.Controllers
Imports PagedList
Imports Martins.AcoesMercadologicas.Core
Imports System.IO

Namespace Controllers
    <Authorize(Roles:="9")>
    Public Class PlanoDeMarketingController
        Inherits AbstractController

        Private boPlano As New PlanoDeMarketingBO()
        Private eventoBO As New EventoBO()
        Private boTipoEvento As New TipoDeEventoBO()
        Private boUnidadeEstrategica As New UnidadeEstrategicaDeNegocioBO()

        ''' <summary>
        ''' Carrega pagina princial
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        Function Index(dadosPesquisa As GridSettings(Of PlanoDeMarketingViewModel)) As ViewResult

            CarregaDrops()

            dadosPesquisa.Filter = New PlanoDeMarketingViewModel()
            dadosPesquisa.Filter.ANOMESREFDAT = Date.Now
            Dim planos As IEnumerable(Of PlanoDeMarketingViewModel) = Mapper.Map(Of List(Of VO.PlanoDeMarketing), List(Of PlanoDeMarketingViewModel))(New List(Of VO.PlanoDeMarketing))
            Dim gridConf = New GridSettings(Of PlanoDeMarketingViewModel)(dadosPesquisa.Filter, planos.ToPagedList(Constantes.PAGE_NUMBER_DEFAULT, Constantes.PAGE_SIZE_DEFAULT))
            gridConf.Filter.ANOMESREFDAT = Date.Now
            Return View(gridConf)
        End Function

        ''' <summary>
        ''' procura registros pelo filtro dado
        ''' </summary>
        ''' <param name="dadosPesquisa"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Pesquisar(dadosPesquisa As GridSettings(Of PlanoDeMarketingViewModel)) As PartialViewResult
            Try

            
            Dim plano As VO.PlanoDeMarketing = Mapper.Map(Of PlanoDeMarketingViewModel, VO.PlanoDeMarketing)(dadosPesquisa.Filter)
            Dim planos As List(Of VO.PlanoDeMarketing) = boPlano.ListPlanoDeMarketing(plano)
            Dim planosViewModel As List(Of PlanoDeMarketingViewModel) = Mapper.Map(Of List(Of VO.PlanoDeMarketing), List(Of PlanoDeMarketingViewModel))(planos)

            Dim gridConf = PaginarEAtualizarGrid(planosViewModel, dadosPesquisa.Page, dadosPesquisa.Filter)
            If gridConf.TotalCount = 0 Then
                Information(Constantes.NENHUM_REGISTRO_ENCONTRADO)
            End If
                Return PartialView("~/Views/PlanoDeMarketing/_ResultadosView.vbhtml", gridConf)

            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Função Para decidir se a ação é de criação ou edição
        ''' </summary>
        ''' <returns></returns>
        Function CreateEdit(Optional ByVal TIPEVTACOCMC As Integer? = Nothing, Optional ByVal CODEVTACOCMC As Integer? = Nothing,
                            Optional ByVal ANOMESREF As Integer? = Nothing, Optional ByVal CODUNDESRNGC As Integer? = Nothing) As ActionResult
            Try
                If TIPEVTACOCMC Is Nothing OrElse TIPEVTACOCMC = 0 Then
                    Return Create()
                Else
                    Return Edit(TIPEVTACOCMC, CODEVTACOCMC, ANOMESREF, CODUNDESRNGC)
                End If
            Catch ex As Exception
                Danger(ex)
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Salvar novo registro
        ''' </summary>
        ''' <param name="planoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Save(ByVal planoViewModel As PlanoDeMarketingViewModel) As ActionResult
            Try

                CarregaDrops()
                If (Not IsNothing(planoViewModel.TIPEVTACOCMC)) Then 'previne que drop de eventos carregue sem ser selecionado tipo de eventos
                    Dim evento As List(Of EventoViewModel) = Mapper.Map(Of List(Of VO.Evento), List(Of EventoViewModel))(eventoBO.ListT(New VO.Evento()))
                    ViewBag.evento = evento.Where(Function(m) m.TIPEVTACOCMC = planoViewModel.TIPEVTACOCMC).OrderBy(Function(f) f.CODEVTACOCMC)
                Else
                    Dim evento As New List(Of EventoViewModel)
                    ViewBag.evento = evento
                End If

                If (IsNothing(planoViewModel.VLRRSVPLNMKT)) Then
                    planoViewModel.VLRRSVPLNMKT = 0
                End If
                If (IsNothing(planoViewModel.VLRUTZPLNMKT)) Then
                    planoViewModel.VLRUTZPLNMKT = 0
                End If
                If (IsNothing(planoViewModel.VLROCDCTTPLNMKT)) Then
                    planoViewModel.VLROCDCTTPLNMKT = 0
                End If
                If (IsNothing(planoViewModel.VLROCDEXACTTPLNMKT)) Then
                    planoViewModel.VLROCDEXACTTPLNMKT = 0
                End If
                If (IsNothing(planoViewModel.VLROCDPLNMKT)) Then
                    planoViewModel.VLROCDPLNMKT = 0
                End If


                If (Not ModelState.IsValid) Then
                    Return View("CreateEdit", planoViewModel)
                End If

                Dim plano = Mapper.Map(Of PlanoDeMarketingViewModel, VO.PlanoDeMarketing)(planoViewModel)

                Dim acao As IEnumerable(Of String) = Split(Request.RawUrl, "/")


                If acao(acao.Count - 1) = "Save" Then
                    boPlano.InsertT(plano)
                    Success(Constantes.REGISTRO_INCLUIDO_SUCESSO)
                End If

                Return RedirectToAction("Index")
            Catch ex As Exception

                If Split(ex.Message, ":")(0) = "ORA-00001" Then
                    Warning(Constantes.REGISTRO_DUPLICADO)
                Else
                    Danger(ex)
                End If

                Return View("CreateEdit", planoViewModel)
            End Try
        End Function


        ''' <summary>
        ''' Atualizar registro
        ''' </summary>
        ''' <param name="planoViewModel"></param>
        ''' <returns></returns>
        <HttpPost()>
        Function Alterar(ByVal planoViewModel As PlanoDeMarketingViewModel) As ActionResult
            Try

                CarregaDrops()
                If (IsNothing(planoViewModel.VLRRSVPLNMKT)) Then
                    planoViewModel.VLRRSVPLNMKT = 0
                End If
                If (IsNothing(planoViewModel.VLRUTZPLNMKT)) Then
                    planoViewModel.VLRUTZPLNMKT = 0
                End If
                If (IsNothing(planoViewModel.VLROCDCTTPLNMKT)) Then
                    planoViewModel.VLROCDCTTPLNMKT = 0
                End If
                If (IsNothing(planoViewModel.VLROCDEXACTTPLNMKT)) Then
                    planoViewModel.VLROCDEXACTTPLNMKT = 0
                End If
                If (IsNothing(planoViewModel.VLROCDPLNMKT)) Then
                    planoViewModel.VLROCDPLNMKT = 0
                End If


                If (Not ModelState.IsValid) Then
                    Return View("CreateEdit", planoViewModel)
                End If

                Dim plano = Mapper.Map(Of PlanoDeMarketingViewModel, VO.PlanoDeMarketing)(planoViewModel)


                boPlano.UpdateT(plano)
                Success(Constantes.REGISTRO_ALTERADO_SUCESSO)

                Return RedirectToAction("Index")
            Catch ex As Exception

                If Split(ex.Message, ":")(0) = "ORA-00001" Then
                    Warning(Constantes.REGISTRO_DUPLICADO)
                Else
                    Danger(ex)
                End If

                Return View("CreateEdit", planoViewModel)
            End Try
        End Function

        ''' <summary>
        ''' Criar novo registro
        ''' </summary>
        ''' <returns></returns>
        Function Create() As ActionResult

            Dim tiposEvento As List(Of TipoEventoViewModel) = Mapper.Map(Of List(Of VO.TipoDeEvento), List(Of TipoEventoViewModel))(boTipoEvento.ListT(New VO.TipoDeEvento()))
            ViewBag.tipoEvento = tiposEvento

            Dim unidade As List(Of UnidadeEstrategicaDeNegocioViewModel) = Mapper.Map(Of List(Of VO.UnidadeEstrategicaDeNegocio), List(Of UnidadeEstrategicaDeNegocioViewModel))(boUnidadeEstrategica.ListT(New VO.UnidadeEstrategicaDeNegocio()))
            ViewBag.unidade = unidade

            Dim evento As New List(Of EventoViewModel)
            ViewBag.evento = evento

            Dim retorno As PlanoDeMarketingViewModel = New PlanoDeMarketingViewModel()
            retorno.ANOMESREFDAT = Date.Now
            Return View(retorno)
        End Function

        ''' <summary>
        ''' Editar resgistro existente
        ''' </summary>
        ''' <returns></returns>
        Function Edit(Optional ByVal TIPEVTACOCMC As Integer? = Nothing, Optional ByVal CODEVTACOCMC As Integer? = Nothing,
                            Optional ByVal ANOMESREF As Integer? = Nothing, Optional ByVal CODUNDESRNGC As Integer? = Nothing) As ActionResult
            CarregaDrops()

            Dim filtro As New VO.PlanoDeMarketing()
            filtro.TIPEVTACOCMC = TIPEVTACOCMC
            filtro.CODEVTACOCMC = CODEVTACOCMC
            filtro.ANOMESREF = ANOMESREF
            filtro.CODUNDESRNGC = CODUNDESRNGC

            Dim plano = boPlano.ListPlanoDeMarketing(filtro).FirstOrDefault()
            Dim planoViewModel = Mapper.Map(Of VO.PlanoDeMarketing, PlanoDeMarketingViewModel)(plano)

            Return View(planoViewModel)
        End Function


        Function Delete(ByVal id As String, dadosPesquisa As GridSettings(Of PlanoDeMarketingViewModel)) As PartialViewResult
            Try
                Dim dados As IEnumerable(Of String) = Split(id, ":")

                Dim filtro As New VO.PlanoDeMarketing()
                filtro.TIPEVTACOCMC = CDec(dados(0))
                filtro.CODEVTACOCMC = CDec(dados(1))
                filtro.ANOMESREF = CDec(dados(2))
                filtro.CODUNDESRNGC = CDec(dados(3))

                boPlano.DeleteT(filtro)
                Success(Constantes.REGISTRO_EXCLUIDO_SUCESSO)
                Return Pesquisar(dadosPesquisa)
            Catch ex As Exception
                Danger(ex)
                Return Pesquisar(dadosPesquisa)
            End Try
        End Function


        ''' <summary>
        ''' função auxiliar para carregar os drops de nescessarios
        ''' </summary>
        Private Sub CarregaDrops()

            Dim tiposEvento As List(Of TipoEventoViewModel) = Mapper.Map(Of List(Of VO.TipoDeEvento), List(Of TipoEventoViewModel))(boTipoEvento.ListT(New VO.TipoDeEvento()))
            ViewBag.tipoEvento = tiposEvento.OrderBy(Function(f) f.TIPEVTACOCMC)

            Dim evento As List(Of EventoViewModel) = Mapper.Map(Of List(Of VO.Evento), List(Of EventoViewModel))(eventoBO.ListT(New VO.Evento()))
            ViewBag.evento = evento.OrderBy(Function(f) f.CODEVTACOCMC)

            Dim unidade As List(Of UnidadeEstrategicaDeNegocioViewModel) = Mapper.Map(Of List(Of VO.UnidadeEstrategicaDeNegocio), List(Of UnidadeEstrategicaDeNegocioViewModel))(boUnidadeEstrategica.ListT(New VO.UnidadeEstrategicaDeNegocio()))
            ViewBag.unidade = unidade.OrderBy(Function(f) f.CODUNDESRNGC)
            ImportarPlanilha()
        End Sub

        <HttpPost()>
        Public Function ImportarPlanilha() As PartialViewResult
            Dim erros As List(Of String) = New List(Of String)
            Try
                Dim savedFileName As String = ""
                If IsNothing(Request.Files) OrElse Request.Files.Count = 0 Then
                    erros.Add("Não foi possivel abrir o arquivo!")
                End If
                For Each item As String In Request.Files

                    Dim hpf As HttpPostedFileBase = TryCast(Request.Files(0), HttpPostedFileBase)
                    If (hpf.ContentLength = 0) Then
                        Continue For
                    End If

                    Dim dataDoArquivo = Date.Now
                    Dim caminho As String = System.Web.Hosting.HostingEnvironment.MapPath("~/Arquivos/Temp")
                    Dim nomeArquivo As String = Path.GetFileName(hpf.FileName)
                    Dim index As Integer = nomeArquivo.IndexOf(".xls")
                    nomeArquivo = nomeArquivo.Substring(0, index) + dataDoArquivo.ToString.Replace("/", "-").Replace(" ", "-").Replace(":", "-") + nomeArquivo.Substring(index, nomeArquivo.Length - index)
                    savedFileName = Path.Combine(caminho, nomeArquivo)
                    hpf.SaveAs(savedFileName)
                    'My.Computer.Network.UploadFile("C:\\TFSBASE\\Martins\\AcoesMercadologicas\\testeplanodemarketing2.xls", "D:\\Temp\\bla.xls")

                    erros = boPlano.ImportarExelPlanoDeMarketing(savedFileName)
                Next
                Return PartialView("~/Views/PlanoDeMarketing/_ModalErrosImportacao.vbhtml", erros)
            Catch ex As Exception
                If ex.Message.Contains("Não foi possível localizar uma parte do caminho") Then
                    erros.Add("Não foi possível salvar o arquivo no servidor pois não foi possível localizar uma parte do caminho, favor entrar em contato com o administrador do sistema.")
                    Return PartialView("~/Views/PlanoDeMarketing/_ModalErrosImportacao.vbhtml", erros)
                End If
                Danger(ex)
            End Try
            Return Nothing
        End Function

        <HttpPost()>
        Public Function AbrirModarImport() As PartialViewResult
            Return PartialView("~/Views/PlanoDeMarketing/_ModalImport.vbhtml")
        End Function

    End Class
End Namespace