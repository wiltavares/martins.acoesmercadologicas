﻿Imports Core
Imports System.Collections.Generic

Public Class SimulaFundingBO
    Inherits BOBase(Of VO.SimulaFunding)


    Public Sub New()
        MyBase.daoBase = New SimulaFundingDAO()
    End Sub

    Public Function BuscaItensSimulaFunding(vo As VO.SimulaFunding) As List(Of VO.SimulaFunding)
        Return DirectCast(daoBase, SimulaFundingDAO).BuscaItensSimulaFunding(vo)
    End Function

End Class
