﻿Imports Core
Imports NPOI.HSSF.Extractor
Imports NPOI.HSSF.UserModel

Public Class RelacaoAcaoMecanicaClientesExcludentesBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaClientesExcludentes)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaClientesExcludentesDAO()
    End Sub

    Public Function ListarClientesExcludentes(obj As VO.RelacaoAcaoMecanicaClientesExcludentes) As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaClientesExcludentesDAO).ListarClientesExcludentes(obj)
    End Function

    Public Function BuscarListarCliente(obj As VO.RelacaoAcaoMecanicaClientesExcludentes) As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaClientesExcludentesDAO).BuscarListarCliente(obj)
    End Function

    Public Sub AddPesquisa(ByRef filtro As VO.RelacaoAcaoMecanicaClientesExcludentes)
        Dim voList As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes) = Me.BuscarListarCliente(filtro)

        If (voList.Count > 30000) Then
            Throw New Exception(String.Format("Esta consulta resuta em {0} registros, somente e permitido inserir 30000 por vez. Verifique os filtros", voList.Count))
        End If

        Dim clientesExcludentesAdicionados As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes) = Me.ListarClientesExcludentes(New VO.RelacaoAcaoMecanicaClientesExcludentes With {.CODACOCMC = filtro.CODACOCMC, .NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC})
        For Each novoClienteExcludente In voList
            If (clientesExcludentesAdicionados.Where(Function(f) f.CODCLI = novoClienteExcludente.CODCLI).Count() = 0) Then
                If (Not (novoClienteExcludente.CODCLI = 0)) Then
                    novoClienteExcludente.CODACOCMC = filtro.CODACOCMC
                    novoClienteExcludente.NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC
                    InsertT(novoClienteExcludente)
                End If
            End If
        Next
    End Sub

    Public Function ImportarExelClientesExcludentes(path As String, ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As String
        Try

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes) = ListaExcel(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            Dim erros As New StringBuilder()

            Dim clienteAdicionado As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes) = Me.ListarClientesExcludentes(New VO.RelacaoAcaoMecanicaClientesExcludentes With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

            If IsNothing(dados) Or dados.Count = 0 Then
                erros.AppendLine("Este arquivo não possui uma relação de codigos de clientes para importar")
            ElseIf dados.Count > 30000 Then
                erros.AppendLine("Este arquivo possui mais de 30000 codigos de clientes para importar")
            Else
                For Each novoCliente In dados

                    Dim relacaoAcaoMecanicaClientesExcludentesDAO As New RelacaoAcaoMecanicaClientesExcludentesDAO()
                    Dim verificaSeClienteExiste As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes) = relacaoAcaoMecanicaClientesExcludentesDAO.VerificaSeClienteExiste(novoCliente.CODCLI)
                    If (verificaSeClienteExiste.Count <= 0) Then
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).AppendLine(" Não foi encontrado cliente com este código")
                        Continue For
                    End If

                    Dim verificaClienteJaAdicionado As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes) = clienteAdicionado.Where(Function(f) f.CODCLI = novoCliente.CODCLI).ToList
                    If (verificaClienteJaAdicionado.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).AppendLine(" Cliente já adicionado.")
                        Continue For
                    End If


                    Dim verificaClientePublicadoXLS As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes) = dados.Where(Function(f) f.CODCLI = novoCliente.CODCLI AndAlso f.CODACOCMC IsNot Nothing).ToList
                    If (verificaClientePublicadoXLS.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).AppendLine(" Cliente duplicado na planilha.")
                        Continue For
                    End If

                    Try
                        novoCliente.CODACOCMC = CODACOCMC
                        novoCliente.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                        InsertT(novoCliente)
                    Catch ex As Exception
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                        Continue For
                    End Try
                Next
            End If
            Return erros.ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ListaExcel(path As String, ByRef erros As String) As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes)
        Dim clientesExcludentesImportados As New List(Of VO.RelacaoAcaoMecanicaClientesExcludentes)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (Not (alinha.Count = 1 Or alinha.Count = 2)) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 1 coluna de dados sendo a coluna ""Codigo do Cliente"" opcionalmente voce pode informar a coluna ""Nome do Cliente"", esta informação é apenas informativa", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo do Cliente") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo do Cliente""", NumLinha)
                    Return Nothing
                End If
                If (alinha.Count = 2 AndAlso alinha(1).Replace("""", "") <> "Nome do Cliente") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Nome do Cliente""", NumLinha)
                    Return Nothing
                End If

            Else ' Linhas de dados
                Dim novoItem As New VO.RelacaoAcaoMecanicaClientesExcludentes
                Dim CODCLI As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODCLI)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo do Cliente"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If
                novoItem.CODCLI = CODCLI

                If (alinha.Count = 2) Then
                    novoItem.NOMCLI = alinha(1).Replace("""", "")
                End If

                clientesExcludentesImportados.Add(novoItem)
            End If
            NumLinha += 1
        Next

        Return clientesExcludentesImportados
    End Function
End Class
