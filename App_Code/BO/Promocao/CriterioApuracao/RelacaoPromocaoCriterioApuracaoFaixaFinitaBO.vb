﻿Imports Core

Public Class RelacaoPromocaoCriterioApuracaoFaixaFinitaBO
    Inherits BOBase(Of VO.RelacaoPromocaoCriterioApuracaoFaixaFinita)

    Public Sub New()
        MyBase.daoBase = New RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO()
    End Sub

    Public Function BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(faixaFinita As VO.RelacaoPromocaoCriterioApuracaoFaixaFinita) As List(Of VO.RelacaoPromocaoCriterioApuracaoFaixaFinita)
        Return DirectCast(daoBase, RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO).BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(faixaFinita)
    End Function
End Class
