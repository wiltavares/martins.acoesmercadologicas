﻿Imports Core

Public Class RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO
    Inherits BOBase(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva)

    Public Sub New()
        MyBase.daoBase = New RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO()
    End Sub

    Public Function BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(escalaProgessiva As VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva) As List(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva)
        Return DirectCast(daoBase, RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO).BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(escalaProgessiva)
    End Function
End Class
