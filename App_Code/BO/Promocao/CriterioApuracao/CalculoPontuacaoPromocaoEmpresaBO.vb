﻿Imports Core

Public Class CalculoPontuacaoPromocaoEmpresaBO
    Inherits BOBase(Of VO.CalculoPontuacaoPromocaoEmpresa)

    Public Sub New()
        MyBase.daoBase = New CalculoPontuacaoPromocaoEmpresaDAO()
    End Sub

    Public Function ListCalculoPontuacaoPromocaoEmpresa() As List(Of VO.CalculoPontuacaoPromocaoEmpresa)
        Return DirectCast(daoBase, CalculoPontuacaoPromocaoEmpresaDAO).ListCalculoPontuacaoPromocaoEmpresa()
    End Function
End Class
