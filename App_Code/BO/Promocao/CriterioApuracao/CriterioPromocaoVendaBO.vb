﻿Imports Core

Public Class CriterioPromocaoVendaBO
    Inherits BOBase(Of VO.CriterioPromocaoVenda)

    Public Sub New()
        MyBase.daoBase = New CriterioPromocaoVendaDAO()
    End Sub

    Public Function BuscarCriterioPromocaoVenda() As List(Of VO.CriterioPromocaoVenda)
        Return DirectCast(daoBase, CriterioPromocaoVendaDAO).BuscarCriterioPromocaoVenda()
    End Function
End Class
