﻿Imports Core

Public Class AbastecimentoBO
    Inherits BOBase(Of VO.Abastecimento)

    Public Sub New()
        MyBase.daoBase = New AbastecimentoDAO()
    End Sub

    Public Function Pesquisa(vo As VO.Abastecimento) As List(Of VO.Abastecimento)
        Return DirectCast(daoBase, AbastecimentoDAO).Pesquisa(vo)
    End Function
    Public Function PesquisaAbastecimento(vo As VO.Abastecimento) As List(Of VO.Abastecimento)
        Return DirectCast(daoBase, AbastecimentoDAO).PesquisaAbastecimento(vo)
    End Function

    Public Sub AbastecerPromocao(vos As List(Of VO.Abastecimento))

        Try
            daoBase.connector.BeginTransaction()

            vos.ForEach(Sub(obj) Abastecer(obj))


            'vos.ForEach(Sub(obj) Abastecer(obj.INDTIPISRMERPMC, obj.CODPMC, obj.CODACOCMC, obj.CODMER, obj.QDEMERMAXPRMPMC, obj.QDEMERISRPMC, obj.QDEMERVNDPRMPMC))

            daoBase.connector.Commit()
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

    End Sub


    Private Sub Abastecer(ByVal abastecimento As VO.Abastecimento)

        Try
            ' Recupera os dados da promoção
            Dim promocaoDAO As New PromocaoDAO()
            Dim promocao As VO.Promocao = promocaoDAO.SelectT(New VO.Promocao With {.CODPMC = abastecimento.CODPMC, .TIPEDEPUBALVPMC = 4})
            If (promocao Is Nothing) Then
                Throw New Exception("Promoção não encontrada no cadastro de promoções!")
            End If
            If (promocao.TIPSITPMC = "E") Then
                Throw New Exception("Promoção Encerrada")
            End If
            abastecimento.CODFILEMP = promocao.CODFILEMP


            ' Recupera os dados da ação comercial informada
            Dim acaoComercialDAO As New AcaoComercialDAO()
            Dim acaoInformada = acaoComercialDAO.BuscarAcaoComercialPorId(abastecimento.CODACOCMC)
            If (acaoInformada Is Nothing) Then
                Throw New Exception("Ação Comercial informada não encontrada.")
            End If

            ' Caso a ação comercial informada não seja a ação comercial da promoção verifica se são complementares  e seus tipos
            If (promocao.CODACOCMC <> acaoInformada.CODACOCMC) Then
                Dim acaoPromocao = acaoComercialDAO.BuscarAcaoComercialPorId(promocao.CODACOCMC)
                If (Not (acaoPromocao.CODACOCMC = abastecimento.CODACOCMC Or acaoInformada.CODACOCMCORI = acaoPromocao.CODACOCMC)) Then
                    Throw New Exception("Ação Comercial informada não é a Ação principal desta Promoção nem suas complementares!")
                End If
                If (acaoPromocao.TIPACOMCD <> acaoInformada.TIPACOMCD) Then
                    Throw New Exception("O tipo da ação comercial informada é diferente do tipo da ação comercial desta promoção.")
                End If

                If (acaoInformada.CODSTAAPVACOCMC <> 5) Then
                    Throw New Exception("A ação comercial informada não esta aprovada.")
                End If
            End If

            'verifia se o item informado esta cadastrado como brinde para a promocao
            Dim promocaobrindeBO As New PromocaoBrindeBO()
            Dim brinde = promocaobrindeBO.SelectT(New VO.PromocaoBrinde With {.CODPMC = abastecimento.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC, .CODPRMPMC = abastecimento.CODMER, .TIPEDENIVPRMPMC = abastecimento.TIPEDENIVPRMPMC})

            If (brinde Is Nothing) Then
                Throw New Exception("Brinde não encontrado na promoção")
            ElseIf (Not brinde.DATECSPRD Is Nothing) Then
                Throw New Exception(String.Format("Brinde excluido da promoção em {0}", brinde.DATECSPRD))
            End If

            ' Calcula o valor
            ObtemValorMercadoria(abastecimento, acaoInformada, promocao)

            ' Verifica o saldo da mercadoria.
            VerificaEstoqueMercadoria(abastecimento)

            ' Verifica se apos o abastecimento/desabastecimento o saldo nao vai ficar menor que a quantidade vendida. (abastecimento - desabastecimento + valor a ser abastecidoou desabastecido)
            VerificaSaldoAbastecimentos(abastecimento)

            'Verifica se o item está cadastrado para a ação comercial como item principal ou item substituto
            VerificaItemAcaoComercial(abastecimento, acaoInformada)

            ' Se for abastecimento e a ação comercial for pré-acordo vigência verifica o saldo da ação comercial
            If (abastecimento.INDTIPISRMERPMC = 0 AndAlso acaoInformada.TIPACOMCD <> TipoAcao.PreAcordoVigencia) Then
                VerificaSaldoAcaoComercial(abastecimento)
            End If

            Insert(abastecimento)


            ' insere o abastecimento
        Catch ex As Exception
            Throw New Exception(String.Format("{0} - {1}:<br/> {2}", abastecimento.CODMER, abastecimento.DESMER, ex.Message))
        End Try
    End Sub


    Private Sub VerificaEstoqueMercadoria(ByRef abastecimento As VO.Abastecimento)

        Dim mercadoriaEmpresaQuantidadeDAO As New MercadoriaEmpresaQuantidadeDAO()
        Dim qtdsMer = mercadoriaEmpresaQuantidadeDAO.SelectPorMercadoriaFilial(abastecimento.CODMER, abastecimento.CODFILEMP)

        If (qtdsMer Is Nothing) Then
            Throw New Exception("Não foi encontrato informações de estoque para este item!")
        End If

        Dim ETQSLD As Decimal = qtdsMer.QDEETQMER + qtdsMer.QDEMERENRPND + qtdsMer.QDEMERENRESP + qtdsMer.QDEMERATPVND - qtdsMer.QDEMERVNDTMK - qtdsMer.QDEMERSAIESP - qtdsMer.QDEMERVNDBLC - qtdsMer.QDEMERSUSVND - qtdsMer.QDEETQMERMIX + qtdsMer.QDEMERENRTRN - qtdsMer.QDEMERSAITRN - qtdsMer.QDEMERRSVLPT - qtdsMer.QDEMERRSVVNDATP - qtdsMer.QDEMERSUSVNDDPS - qtdsMer.QDEETQRSVMIX - qtdsMer.QDEETQRSVKIT - (qtdsMer.QDEMERMAXPMC - qtdsMer.QDEMERVNDPMC)

        If (abastecimento.INDTIPISRMERPMC = 0 AndAlso (abastecimento.QDEMERISRPMC > ETQSLD)) Then
            Throw New Exception("O estoque de brindes não pode ser maior que o estoque total!")
        End If

    End Sub

    Private Sub VerificaItemAcaoComercial(ByRef abastecimento As VO.Abastecimento, ByVal acaoInformada As VO.AcaoComercial)

        ' [Regra] Verifica se o item está cadastrado para a ação comercial como item principal ou item substituto
        Dim DAORelacaoAcaoComItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoDAO()
        Dim resultAcaoItem = DAORelacaoAcaoComItemOrcamento.SelectRelacaoAcaoComercialEItensDeOrcamento(acaoInformada.CODACOCMC, abastecimento.CODMER).FirstOrDefault()

        If (resultAcaoItem IsNot Nothing) Then
            abastecimento.NUMLNHITEOCDACOCMC = resultAcaoItem.NUMLNHITEOCDACOCMC
            abastecimento.CODMER_PRINCIPAL = abastecimento.CODMER
        Else

            'Se encontrou registro não precisa fazer a segunda validação
            'Se não encontrou registros faz a segunda validação.

            Dim DAORelacaoItemPrincipalSubistituto As New RelacaoItemPrincipalItemSubstitutoAcaoComercialDAO()
            Dim resultAcaoItemSubstituto = DAORelacaoItemPrincipalSubistituto.SelectRelacaoItemPrincipalEItemSubstitutoDaAcaoComercial(acaoInformada.CODACOCMC, abastecimento.CODMER).FirstOrDefault()

            If (resultAcaoItemSubstituto Is Nothing) Then
                Throw New Exception("Não há relação de item principal com item substituto da ação comercial")
            End If

            abastecimento.NUMLNHITEOCDACOCMC = resultAcaoItemSubstituto.NUMLNHITEOCDACOCMC
            abastecimento.CODMER_PRINCIPAL = resultAcaoItemSubstituto.CODMER
        End If
    End Sub

    Private Sub ObtemValorMercadoria(ByRef abastecimento As VO.Abastecimento, ByVal acaoInformada As VO.AcaoComercial, ByVal promocao As VO.Promocao)

        ' [Regra] De acordo com o tipo da ação comercial informada verifica o valor do brinde 
        ' [Regra]       se:
        ' [Regra]    a.Pré-Acordo(5 = Vigência, 6 = Valor)
        ' [Regra]         i.Busca o valor com base na tabela T0138776 (RelacaoAcaoComercialPorItemOrcamento)
        ' [Regra]        ii.Caso não obtenha o valor unitário da mercadoria apresenta mensagem de erro
        ' [Regra]    b.Senão
        ' [Regra]        i.Busca o valor com base na tabela T0130856 (MovimentoQuinzenalPrecoMercadoria)
        ' [Regra]       ii.Caso não obtenha o valor unitário da mercadoria apresenta mensagem de erro

        Dim VLR_MERCADORIA As Decimal
        Dim DAORelacaoAcaoComItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoDAO()
        If (acaoInformada.TIPACOMCD = TipoAcao.PreAcordoVigencia OrElse acaoInformada.TIPACOMCD = TipoAcao.PreAcordoValor) Then

            Dim result = DAORelacaoAcaoComItemOrcamento.ObterValorTotalDeMercadoriaPreAcordo(acaoInformada.CODACOCMC, abastecimento.CODMER)
            If (result Is Nothing OrElse result.Count = 0) Then
                Throw New Exception("Não encontrado valor unitário da mercadoria!")
            End If
            VLR_MERCADORIA = result(0).VLRTOTMERISRPMC
        Else
            Dim DAOMovimentoQuinzenal As New MovimentoQuinzenalPrecoMercadoriaDAO()
            Dim result = DAOMovimentoQuinzenal.ObterValorTotalDeMercadoriaInseridaNaPromocao(promocao.CODFILEMP, abastecimento.CODMER)
            If (result Is Nothing OrElse result.Count = 0) Then
                Throw New Exception("Não encontrado valor unitário da mercadoria!")
            End If
            VLR_MERCADORIA = result(0).VLRTOTMERISRPMC
        End If

        ' [Regra] Verifica se a mercadoria não está com valor unitário zerado
        If (VLR_MERCADORIA = 0) Then
            Throw New Exception("Valor unitário da mercadoria igual a zero(0)!")
        End If

        abastecimento.VLRTOTMERISRPMC = VLR_MERCADORIA * abastecimento.QDEMERISRPMC
    End Sub

    Private Sub VerificaSaldoAcaoComercial(ByRef abastecimento As VO.Abastecimento)

        'Obtem o valor orçado e o valor utilizado noa ação comercial.
        Dim RelacaoAcaoComercialPorOperacaoDeDestinoDAO As New RelacaoAcaoComercialPorOperacaoDeDestinoDAO
        Dim valorOrcadoEUtilizado = RelacaoAcaoComercialPorOperacaoDeDestinoDAO.SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(abastecimento.CODACOCMC)
        If (valorOrcadoEUtilizado Is Nothing) Then
            Throw New Exception("Não Existe Ação Comercial!")
        End If
        Dim saldoRelacPorOperacaoDestino As Decimal = valorOrcadoEUtilizado.VLROPEFSCDSNACOCMC - valorOrcadoEUtilizado.VLRUTZOPEDSNACOCMC


        'obtem o saldo da mercadoria (Item orçamento)
        Dim DAORelacaoAcaoComItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoDAO()
        Dim valorOrcadoEUtilizadoItemOrcamento = DAORelacaoAcaoComItemOrcamento.SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(abastecimento.CODACOCMC, abastecimento.CODMER)
        If (valorOrcadoEUtilizadoItemOrcamento Is Nothing) Then
            Throw New Exception("Não Existe Ação Comercial!")
        End If
        Dim saldoPorItemOrcamento As Decimal = valorOrcadoEUtilizadoItemOrcamento.VLRITEOCDACOCMC - valorOrcadoEUtilizadoItemOrcamento.VLRUTZOPEDSNACOCMC


        Dim minSaldo = Math.Min(saldoRelacPorOperacaoDestino, saldoPorItemOrcamento)
        If ((minSaldo - abastecimento.VLRTOTMERISRPMC) < 0) Then
            Throw New Exception(String.Format("Saldo insuficiente, disponível: {0}", minSaldo))
        End If
    End Sub

    Private Sub Insert(ByRef abastecimento As VO.Abastecimento)

        abastecimento.DATGRCRLC = DateTime.Now.Date
        abastecimento.CODFNCGRCPMC = ControleAcesso.CodFnc()
        abastecimento.INDISRVLDMERPMC = 1


        ' Busca a proxima sequência
        Dim proxNumSeq = DirectCast(daoBase, AbastecimentoDAO).GetSelectProximoNumSeqIsrQdeMerPmc(abastecimento.CODACOCMC, abastecimento.CODPMC, abastecimento.CODMER)
        abastecimento.NUMSEQISRQDEMERPMC = proxNumSeq.NUMSEQISRQDEMERPMC

        ' Insere o Abastecimento/Desabastecimento
        DirectCast(daoBase, AbastecimentoDAO).InsertT(abastecimento) 'MRT.T0153553


        ' Update na relação ação comercial
        Dim relacaoAcaoComercialPorOperacaoDeDestinoDAO As New RelacaoAcaoComercialPorOperacaoDeDestinoDAO ' MRT.T0138008 
        relacaoAcaoComercialPorOperacaoDeDestinoDAO.UpdateAbastecerPromocao(abastecimento.CODACOCMC, abastecimento.INDTIPISRMERPMC, abastecimento.VLRTOTMERISRPMC)


        ' Dim CODPRMPMC As Decimal = 0
        ' Atualiza o item orçamento da ação
        Dim DAORelacaoAcaoComItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoDAO()
        'Dim relacaoAcaoComItemOrcamentoVO = DAORelacaoAcaoComItemOrcamento.SelectAbastecerPromocao(abastecimento.CODACOCMC, abastecimento.CODPRMPMC)
        DAORelacaoAcaoComItemOrcamento.UpdateAbastecerPromocao(abastecimento.CODACOCMC, abastecimento.NUMLNHITEOCDACOCMC, abastecimento.INDTIPISRMERPMC, abastecimento.VLRTOTMERISRPMC) 'MRT.T0138776 

        ' Insere o Historico de utilização do item
        Dim relacUtilizacaoItem As New VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem With {
                                                       .CODACOCMC = abastecimento.CODACOCMC,
                                                       .NUMLNHITEOCDACOCMC = abastecimento.NUMLNHITEOCDACOCMC,
                                                       .QDEUTZITEOCDACOCMC = 1,
                                                       .VLRUTZITEOCDACOCMC = abastecimento.VLRTOTMERISRPMC,
                                                       .CODFNC = ControleAcesso.CodFnc(),
                                                       .INDTIPISRUTZITEOCD = abastecimento.INDTIPISRMERPMC
                                                       }
        If (abastecimento.INDTIPISRMERPMC = 0) Then
            relacUtilizacaoItem.DESUTZITEOCDACOCMC = "REFERENTE INSERÇÃO BRINDES NA PROMOÇÃO"
        Else
            relacUtilizacaoItem.DESUTZITEOCDACOCMC = "REFERENTE EXCLUSÃO BRINDES NA PROMOÇÃO"
        End If
        Dim DAORelacaoItemUtilizado As New RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemDAO
        DAORelacaoItemUtilizado.InsereLogEstornoManual(relacUtilizacaoItem, abastecimento.CODPMC) 'MRT.T0158718 


        Dim mercadoriaEmpresaQuantidadeDAO As New MercadoriaEmpresaQuantidadeDAO()
        mercadoriaEmpresaQuantidadeDAO.UpdateAbastecerPromocao(CODEMP:=abastecimento.CODFILEMP, CODMER:=abastecimento.CODMER, INDTIPISRMERPMC:=abastecimento.INDTIPISRMERPMC, QDEMERISRPMC:=abastecimento.QDEMERISRPMC)


        Dim promocaoBrindeDAO As New PromocaoBrindeDAO()
        promocaoBrindeDAO.UpdateAbastecerPromocao(TIPEDEPUBALVPMC:=4, CODPMC:=abastecimento.CODPMC, TIPEDENIVPRMPMC:=abastecimento.TIPEDENIVPRMPMC, CODPRMPMC:=abastecimento.CODMER, INDTIPISRMERPMC:=abastecimento.INDTIPISRMERPMC, QDEMERISRPMC:=abastecimento.QDEMERISRPMC)

    End Sub

    Public Function HistoricoPromocaoItem(ByVal CODPMC As Decimal, ByVal CODMER As Decimal) As List(Of VO.Abastecimento)
        Return DirectCast(daoBase, AbastecimentoDAO).HistoricoPromocaoItem(CODPMC, CODMER)
    End Function

    Private Sub VerificaSaldoAbastecimentos(ByRef abastecimento As VO.Abastecimento)


        If (abastecimento.INDTIPISRMERPMC = 1) Then  ' 1 = desabastecer

            Dim saldo As VO.Abastecimento = DirectCast(daoBase, AbastecimentoDAO).BuscaSaldoAbastecimento(abastecimento.CODPMC, abastecimento.CODMER)

            If (saldo Is Nothing) Then
                Throw New Exception("Operação abortada, Não existe abastecimento para este item!")
            End If

            If (saldo.QDEMERISRPMC - abastecimento.QDEMERISRPMC < 0) Then
                Throw New Exception(String.Format("Operação abortada, a quantidade não pode ser negativa ({0})", saldo.QDEMERISRPMC - abastecimento.QDEMERISRPMC))
            End If

            Dim promocaoBrindeDAO As New PromocaoBrindeDAO()
            Dim promocaoBrinde As VO.PromocaoBrinde = promocaoBrindeDAO.GetSelectPremioPorNivelPremiacao(abastecimento.CODPMC, abastecimento.CODMER, abastecimento.TIPEDENIVPRMPMC, 4).FirstOrDefault()
            If (promocaoBrinde Is Nothing) Then
                Throw New Exception("Promoção não encontrada no cadastro de Prêmios (T0108044).")
            End If

            If saldo.QDEMERISRPMC - abastecimento.QDEMERISRPMC < promocaoBrinde.QDEMERVNDPRMPMC Then
                Throw New Exception(String.Format("Operação abortada, quantidade ficaria inferior ao vendido ({0})", promocaoBrinde.QDEMERVNDPRMPMC))
            End If

        End If
    End Sub

    'Private Sub Abastecer(ByVal tipo As Decimal, ByVal CODPMC As Decimal, ByVal CODACOCMC As Decimal, ByVal CODMER As Decimal, Optional ByVal qdeMerMaxPrmPmc As Decimal = 0, Optional ByVal qdeMerIsrPmc As Decimal = 0, Optional ByVal QdeMerVndPrmPmc As Decimal = 0)

    '    ' [Regra] 1-	Verifica se a quantidade irá ficar negativa

    '    If (tipo = 0) Then 'Inserir
    '        qdeMerMaxPrmPmc = qdeMerMaxPrmPmc + qdeMerIsrPmc
    '    ElseIf (tipo = 1) Then 'Excluir
    '        qdeMerMaxPrmPmc = qdeMerMaxPrmPmc - qdeMerIsrPmc
    '    End If

    '    ' [Regra] 2-	Verifica se a quantidade é inferior a quantidade vendida
    '    If qdeMerMaxPrmPmc < 0 Then
    '        Throw New Exception("Operação abortada, a quantidade não pode ser negativa")
    '    End If

    '    If qdeMerMaxPrmPmc < QdeMerVndPrmPmc Then
    '        Throw New Exception("Operação abortada, quantidade ficaria inferior ao vendido")
    '    End If


    '    ' [Regra] 3-	Verifica se a ação comercial informada é a ação comercial principal ou se é uma ação complementar da ação principal.
    '    ' [Regra] 4-	Verifica se a ação comercial está aprovada.
    '    ' [Regra] 5-	Verifica se o tipo da ação informada é o mesmo da ação principal.

    '    ' Recupera os dados da promoção
    '    Dim promocaoDAO As New PromocaoDAO()
    '    Dim promocao As VO.Promocao = promocaoDAO.SelectT(New VO.Promocao With {.CODPMC = CODPMC})
    '    If (promocao Is Nothing) Then
    '        Throw New Exception("Promoção não encontrada no cadastro de promoções!")
    '    End If

    '    ' Recupera os dados da ação comercial informada
    '    Dim acaoComercialDAO As New AcaoComercialDAO()
    '    Dim acaoInformada = acaoComercialDAO.BuscarAcaoComercialPorId(CODACOCMC)
    '    If (acaoInformada Is Nothing) Then
    '        Throw New Exception("Ação Comercial informada não encontrada.")
    '    End If


    '    ' Caso a ação comercial informada não seja a ação comercial da promoção verifica se são complementares  e seus tipos
    '    If (promocao.CODACOCMC <> acaoInformada.CODACOCMC) Then
    '        Dim acaoPromocao = acaoComercialDAO.BuscarAcaoComercialPorId(promocao.CODACOCMC)
    '        If (Not (acaoPromocao.CODACOCMC = CODACOCMC Or acaoInformada.CODACOCMCORI = acaoPromocao.CODACOCMC)) Then
    '            Throw New Exception("Ação Comercial informada não é a Ação principal desta Promoção nem suas complementares ou não está aprovada!")
    '        End If
    '        If (acaoPromocao.TIPACOMCD <> acaoInformada.TIPACOMCD) Then
    '            Throw New Exception("O tipo da ação comercial informada é diferente do tipo da ação comercial desta promoção.")
    '            'Mostar o tipo da ACAO digitada e Mostrar o Tipo da ACAO Existe
    '        End If
    '    End If


    '    ' [Regra] 6-	De acordo com o tipo da ação comercial informada verifica o valor do brinde 
    '    ' [Regra]       se:
    '    ' [Regra]    a.Pré-Acordo(5 = Vigência, 6 = Valor)
    '    ' [Regra]         i.Busca o valor com base na tabela T0138776 (RelacaoAcaoComercialPorItemOrcamento)
    '    ' [Regra]        ii.Caso não obtenha o valor unitário da mercadoria apresenta mensagem de erro
    '    ' [Regra]    b.Senão
    '    ' [Regra]        i.Busca o valor com base na tabela T0130856 (MovimentoQuinzenalPrecoMercadoria)
    '    ' [Regra]       ii.Caso não obtenha o valor unitário da mercadoria apresenta mensagem de erro

    '    Dim VLRTOTMERISRPMC As Decimal
    '    Dim DAORelacaoAcaoComItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoDAO()
    '    If (acaoInformada.TIPACOMCD = TipoAcao.PreAcordoVigencia OrElse acaoInformada.TIPACOMCD = TipoAcao.PreAcordoValor) Then
    '        'ObterValorTotalDeMercadoriaPreAcordo
    '        Dim result = DAORelacaoAcaoComItemOrcamento.ObterValorTotalDeMercadoriaPreAcordo(acaoInformada.CODACOCMC, CODMER)
    '        If (result Is Nothing OrElse result.Count = 0) Then
    '            Throw New Exception("Não encontrado valor unitário da mercadoria!")
    '        End If
    '        VLRTOTMERISRPMC = result(0).VLRTOTMERISRPMC
    '    Else
    '        'ObterValorTotalDeMercadoriaInseridaNaPromocao
    '        Dim DAOMovimentoQuinzenal As New MovimentoQuinzenalPrecoMercadoriaDAO()
    '        Dim result = DAOMovimentoQuinzenal.ObterValorTotalDeMercadoriaInseridaNaPromocao(promocao.CODFILEMP, CODMER)
    '        If (result Is Nothing OrElse result.Count = 0) Then
    '            Throw New Exception("Não encontrado valor unitário da mercadoria!")
    '        End If
    '        VLRTOTMERISRPMC = result(0).VLRTOTMERISRPMC
    '    End If

    '    ' [Regra] 7-	Verifica se a mercadoria não está com valor unitário zerado
    '    If (VLRTOTMERISRPMC = 0) Then
    '        Throw New Exception("Valor unitário da mercadoria igual a zero(0)!")
    '    End If

    '    VLRTOTMERISRPMC = VLRTOTMERISRPMC * qdeMerIsrPmc


    '    ' [Regra] 8-	Verifica se o item está cadastrado para a ação comercial como item principal ou item substituto
    '    Dim resultAcaoItem = DAORelacaoAcaoComItemOrcamento.SelectRelacaoAcaoComercialEItensDeOrcamento(acaoInformada.CODACOCMC, CODMER)
    '    Dim DAORelacaoItemPrincipalSubistituto As New RelacaoItemPrincipalItemSubstitutoAcaoComercialDAO()

    '    If (resultAcaoItem Is Nothing OrElse resultAcaoItem.Count = 0) Then
    '        'Se encontrou registro não precisa fazer a segunda validação
    '        'Se não encontrou registros faz a segunda validação.

    '        Dim resultAcaoItemSubstituto = DAORelacaoItemPrincipalSubistituto.SelectRelacaoItemPrincipalEItemSubstitutoDaAcaoComercial(acaoInformada.CODACOCMC, CODMER)

    '        If (resultAcaoItemSubstituto Is Nothing OrElse resultAcaoItemSubstituto.Count = 0) Then
    '            Throw New Exception("Não há relação de item principal com item substituto da ação comercial")
    '        End If
    '    End If







    '    ' [Regra] 9-	Verifica se o valor não ultrapassa o saldo da ação.
    '    '               Esta validação só ocorre ao abastecer. Quando desabastece não é necessário validar.

    '    'Faz validação se o valor não ultrapassa o saldo disponível na ação
    '    'Existe validação somente para inserção, para exclusão a regra foi 
    '    'colocada na classe
    '    Dim RelacaoAcaoComercialPorOperacaoDeDestinoDAO As New RelacaoAcaoComercialPorOperacaoDeDestinoDAO
    '    If (tipo = 0) Then 'Inserção

    '        'Obtem o saldo disponível (ação comercial)
    '        Dim objSaldoAcaoT0138008 = RelacaoAcaoComercialPorOperacaoDeDestinoDAO.SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(CODACOCMC)

    '        If (objSaldoAcaoT0138008 Is Nothing) Then
    '            Throw New Exception("Não Existe Ação Comercial!")
    '        End If

    '        Dim valorSaldoT0138008 As Decimal = objSaldoAcaoT0138008.VLROPEFSCDSNACOCMC - objSaldoAcaoT0138008.VLRUTZOPEDSNACOCMC

    '        'obtem o saldo da mercadoria (Item orçamento)
    '        Dim objSaldoAcaoT0138776 = DAORelacaoAcaoComItemOrcamento.SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(acaoInformada.CODACOCMC, CODMER)

    '        If (objSaldoAcaoT0138776 Is Nothing) Then
    '            Throw New Exception("Não Existe Ação Comercial!")
    '        End If

    '        Dim valorSaldoT0138776 As Decimal = objSaldoAcaoT0138776.VLRITEOCDACOCMC - objSaldoAcaoT0138776.VLRUTZOPEDSNACOCMC

    '        Dim minSaldo = Math.Min(valorSaldoT0138008, valorSaldoT0138776)

    '        If ((minSaldo - VLRTOTMERISRPMC) < 0 AndAlso Not (acaoInformada.TIPACOMCD = 5)) Then
    '            Throw New Exception(String.Format("Saldo insuficiente, disponível: {0}", minSaldo))
    '        End If
    '    End If

    '    ' [Regra] 10-	Verifica se o item está cadastrado na promoção
    '    Dim promocaoBrindeDAO As New PromocaoBrindeDAO()
    '    Dim PremioPorNivelPremiacao = promocaoBrindeDAO.GetSelectPremioPorNivelPremiacao(CODPMC, CODMER, 0, 4).FirstOrDefault()
    '    If (PremioPorNivelPremiacao Is Nothing) Then
    '        Throw New Exception("Promoção não econtrada no cadastro de Prêmios (T0108044).")
    '    End If


    '    ' [Regra] 11-	Verifica se existe registro na tabela T0138008 (RelacaoAcaoComercialPorOperacaoDeDestino) se não existir aborta a operação.
    '    ' [Regra] 12-	Update na tabela T0138008 (RelacaoAcaoComercialPorOperacaoDeDestino) coluna VLRUTZOPEDSNACOCMC


    '    Dim linhaT0138008 As VO.RelacaoAcaoComercialPorOperacaoDeDestino = RelacaoAcaoComercialPorOperacaoDeDestinoDAO.SelectT(New VO.RelacaoAcaoComercialPorOperacaoDeDestino With {.CODACOCMC = CODACOCMC, .CODOPEFSCDSNACOCMC = 2})
    '    If (linhaT0138008 Is Nothing) Then
    '        Throw New Exception("Não econtrado registro na tabela de saldo (T0138008)")
    '    End If
    '    If (tipo = 0) Then
    '        'Inserir
    '        linhaT0138008.VLRUTZOPEDSNACOCMC = linhaT0138008.VLRUTZOPEDSNACOCMC + VLRTOTMERISRPMC
    '    ElseIf (tipo = 1) Then
    '        'Excluir
    '        linhaT0138008.VLRUTZOPEDSNACOCMC = linhaT0138008.VLRUTZOPEDSNACOCMC - VLRTOTMERISRPMC
    '    End If
    '    RelacaoAcaoComercialPorOperacaoDeDestinoDAO.UpdateT(linhaT0138008)


    '    ' [Regra] 13-	Verifica na tabela MRT.T0138776 (RelacaoAcaoComercialPorItemOrcamento) se tem saldo para a mercadoria

    '    Dim resultAcaoItemSubstituto2 = DAORelacaoItemPrincipalSubistituto.SelectRelacaoItemPrincipalEItemSubstitutoDaAcaoComercial(acaoInformada.CODACOCMC, PremioPorNivelPremiacao.CODPRMPMC)
    '    Dim CODPRMPMC As Decimal

    '    If (resultAcaoItemSubstituto2 IsNot Nothing AndAlso resultAcaoItemSubstituto2.Count > 0) Then
    '        'Se encontrou item substituto, utiliza esse código para atualizar o saldo
    '        CODPRMPMC = resultAcaoItemSubstituto2(0).CODMER
    '    Else
    '        'Se não encontrou item substituto, utiliza o código da mercadoria principal
    '        CODPRMPMC = PremioPorNivelPremiacao.CODPRMPMC
    '    End If

    '    Dim relacoesAcaoComercialItensDeOrcamento = DAORelacaoAcaoComItemOrcamento.ListT(New VO.RelacaoAcaoComercialPorItemOrcamento With {.CODACOCMC = CODACOCMC, .CODMER = CODPRMPMC})

    '    'Filtrar somente os registros com .CODOPEFSCDSNACOCMC = 2 ou 4

    '    Dim relacaoAcaoComercialItensDeOrcamento As VO.RelacaoAcaoComercialPorItemOrcamento = relacoesAcaoComercialItensDeOrcamento.Where(Function(f) f.CODOPEFSCDSNACOCMC = 4 Or f.CODOPEFSCDSNACOCMC = 2).FirstOrDefault()

    '    If (relacaoAcaoComercialItensDeOrcamento Is Nothing) Then
    '        Throw New Exception("Não econtrado registro na tabela de saldo (T0138776).")
    '    End If

    '    ' [Regra] 14-	Atualiza o saldo na tabela MRT.T0138776 (RelacaoAcaoComercialPorItemOrcamento)
    '    If (tipo = 0) Then
    '        'Inserir
    '        relacaoAcaoComercialItensDeOrcamento.VLRUTZOPEDSNACOCMC = relacaoAcaoComercialItensDeOrcamento.VLRUTZOPEDSNACOCMC + VLRTOTMERISRPMC
    '    ElseIf (tipo = 1) Then
    '        'Excluir
    '        relacaoAcaoComercialItensDeOrcamento.VLRUTZOPEDSNACOCMC = relacaoAcaoComercialItensDeOrcamento.VLRUTZOPEDSNACOCMC - VLRTOTMERISRPMC
    '    End If

    '    DAORelacaoAcaoComItemOrcamento.UpdateT(relacaoAcaoComercialItensDeOrcamento)



    '    ' [Regra] 15-	Insere na tabela T0158718 (RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem)

    '    Dim DESUTZITEOCDACOCMC As String = ""
    '    If (tipo = 0) Then 'Inserir
    '        DESUTZITEOCDACOCMC = "Referente inserção brindes na promoção"
    '    ElseIf (tipo = 1) Then 'Excluir
    '        DESUTZITEOCDACOCMC = "Referente Exclusão brindes na promoção"
    '    End If


    '    Dim DAORelacaoItemUtilizado As New RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemDAO
    '    DAORelacaoItemUtilizado.InsereLogEstornoManual(New VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem With {
    '                                                   .CODACOCMC = CODACOCMC,
    '                                                   .NUMLNHITEOCDACOCMC = relacaoAcaoComercialItensDeOrcamento.NUMLNHITEOCDACOCMC,
    '                                                   .DESUTZITEOCDACOCMC = DESUTZITEOCDACOCMC.ToUpper(),
    '                                                   .QDEUTZITEOCDACOCMC = 1,
    '                                                   .VLRUTZITEOCDACOCMC = VLRTOTMERISRPMC,
    '                                                   .CODFNC = ControleAcesso.CodFnc(),
    '                                                   .INDTIPISRUTZITEOCD = tipo
    '                                                   }, CODPMC)



    '    ' [Regra] 16-	Se for um abastecimento verifica o saldo da ação comercial
    '    If (tipo = 0) Then 'Inserir
    '        VerificarSaldoAcaoComercial(tipo, CODPMC, CODACOCMC, CODMER, acaoInformada, DAORelacaoAcaoComItemOrcamento, RelacaoAcaoComercialPorOperacaoDeDestinoDAO)
    '    End If

    '    ' [Regra] 17-	Insere na tabela T0153553 (Abastecimento)
    '    Dim proxNumSeq = DirectCast(daoBase, AbastecimentoDAO).GetSelectProximoNumSeqIsrQdeMerPmc(CODACOCMC, CODPMC, CODMER)

    '    If (proxNumSeq Is Nothing) Then
    '        Throw New Exception("Não foi possível pegar proximo numero de sequencia.")
    '    End If

    '    'Inserindo
    '    Dim objT0153553 As New VO.Abastecimento With {
    '        .CODACOCMC = CODACOCMC,
    '        .CODPMC = CODPMC,
    '        .CODMER = CODPRMPMC,
    '        .NUMSEQISRQDEMERPMC = proxNumSeq.NUMSEQISRQDEMERPMC,
    '        .QDEMERISRPMC = qdeMerIsrPmc,
    '        .VLRTOTMERISRPMC = VLRTOTMERISRPMC,
    '        .DATGRCRLC = DateTime.Now,
    '        .CODFNCGRCPMC = ControleAcesso.CodFnc(),
    '        .INDISRVLDMERPMC = 1,
    '        .INDTIPISRMERPMC = tipo
    '    }
    '    DirectCast(daoBase, AbastecimentoDAO).InsertT(objT0153553)


    '    ' [Regra] 18-	Após persistir os dados faz uma conferência.
    '    ' [Regra]    a.Verifica o Saldo (abastecimento- desabastecimentos) para a combinação Item/AçãoComercial/Promoção. Caso a quantidade ou o valor desabastecido seja maior que a abastecida faz rollback. Tabela T0153553 (Abastecimento).

    '    Dim dsInserido As VO.Abastecimento = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 0, .INDISRVLDMERPMC = -1, .CODPMC = CODPMC, .CODMER = CODMER})
    '    Dim qtdInserido As Decimal = 0
    '    Dim valTotInserido As Decimal = 0
    '    If (dsInserido IsNot Nothing) Then
    '        qtdInserido = dsInserido.QDEMERISRPMC
    '        valTotInserido = dsInserido.VLRTOTMERISRPMC
    '    End If

    '    Dim dsExcluido As VO.Abastecimento = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 1, .INDISRVLDMERPMC = -1, .CODPMC = CODPMC, .CODMER = CODMER})
    '    Dim qtdExcluido As Decimal = 0
    '    Dim valTotExcluido As Decimal = 0
    '    If (dsExcluido IsNot Nothing) Then
    '        qtdExcluido = dsExcluido.QDEMERISRPMC
    '        valTotExcluido = dsExcluido.VLRTOTMERISRPMC
    '    End If

    '    If ((qtdInserido - qtdExcluido) < 0) Then
    '        Throw New Exception(String.Format("Quantidade inválida para mercadoria: {0}", CODMER))
    '    End If

    '    If ((valTotInserido - valTotExcluido) < 0) Then
    '        Throw New Exception(String.Format("Valor inválida para mercadoria: {0}", CODMER))
    '    End If

    '    If (qtdInserido < qtdExcluido) Then
    '        Throw New Exception("Mercadoria excluida.")
    '    End If

    '    ' [Regra] 18-	Após persistir os dados faz uma conferência.
    '    ' [Regra]    b.Verifica se a quantidade vendida e maior que o Saldo (abastecimento - desabastecimentos) para a combinação Item/Promoção. Caso a quantidade vendia seja maior que o saldo faz rollback. Tabelas T0153553 (Abastecimento).  MRT.T0108044 (PromocaoBrinde)

    '    ' não passa a ação comercial como filtro.
    '    dsInserido = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = 0, .INDTIPISRMERPMC = 0, .INDISRVLDMERPMC = -1, .CODPMC = CODPMC, .CODMER = CODMER})
    '    qtdInserido = 0
    '    valTotInserido = 0
    '    If (dsInserido IsNot Nothing) Then
    '        qtdInserido = dsInserido.QDEMERISRPMC
    '        valTotInserido = dsInserido.VLRTOTMERISRPMC
    '    End If
    '    ' não passa a ação comercial como filtro.
    '    dsExcluido = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = 0, .INDTIPISRMERPMC = 1, .INDISRVLDMERPMC = -1, .CODPMC = CODPMC, .CODMER = CODMER})
    '    qtdExcluido = 0
    '    valTotExcluido = 0
    '    If (dsExcluido IsNot Nothing) Then
    '        qtdExcluido = dsExcluido.QDEMERISRPMC
    '        valTotExcluido = dsExcluido.VLRTOTMERISRPMC
    '    End If

    '    Dim listPremioPorNivelPremiacao As List(Of VO.PromocaoBrinde) = promocaoBrindeDAO.ListaParaGrid(New VO.PromocaoBrinde() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = 4, .CODPRMPMC = CODMER})

    '    Dim quantidadeVendida As Decimal = 0
    '    If (listPremioPorNivelPremiacao IsNot Nothing AndAlso listPremioPorNivelPremiacao.Count > 0) Then
    '        quantidadeVendida = listPremioPorNivelPremiacao(0).QDEMERVNDPRMPMC
    '    End If

    '    If (quantidadeVendida > (qtdInserido - qtdExcluido)) Then
    '        Throw New Exception("Quantidade é menor do que foi vendido")
    '    End If

    'End Sub

    'Private Sub VerificarSaldoAcaoComercial(tipo As Decimal, CODPMC As Decimal, CODACOCMC As Decimal, CODMER As Decimal, acaoInformada As VO.AcaoComercial, DAORelacaoAcaoComItemOrcamento As RelacaoAcaoComercialPorItemOrcamentoDAO, RelacaoAcaoComercialPorOperacaoDeDestinoDAO As RelacaoAcaoComercialPorOperacaoDeDestinoDAO)

    '    Dim objSaldoAcaoT0138008Valid = RelacaoAcaoComercialPorOperacaoDeDestinoDAO.SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(CODACOCMC)

    '    If (objSaldoAcaoT0138008Valid Is Nothing) Then
    '        Throw New Exception("Não Existe Ação Comercial!")
    '    End If

    '    'Variavel usada no fonte antigo, quando a funcao era chamada sem ter persistido os dados
    '    'Como nesse caso já foi persistido, o valor da linha permanece 0, não alterando as somas
    '    'Pois o valor total das vendas já vai ter vindo na consulta anterior (já foi persistido o valor da linha inserida/excluida)
    '    Dim valorDaLinha As Decimal = 0

    '    Dim promocaoBrindeDAO As New PromocaoBrindeDAO
    '    If (tipo = 0) Then 'Inserir

    '        Dim dsInserido As VO.Abastecimento = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 0})
    '        Dim valInserido As Decimal = 0
    '        If (dsInserido IsNot Nothing) Then
    '            valInserido = dsInserido.VLRTOTMERISRPMC
    '        End If

    '        Dim dsExcluido As VO.Abastecimento = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 1})
    '        Dim valExcluido As Decimal = 0
    '        If (dsExcluido IsNot Nothing) Then
    '            valExcluido = dsExcluido.VLRTOTMERISRPMC
    '        End If


    '        Dim VLRTOTMERISRPMC_Inserido As Decimal
    '        VLRTOTMERISRPMC_Inserido = Decimal.Round(objSaldoAcaoT0138008Valid.VLRUTZOPEDSNACOCMC.GetValueOrDefault() + valorDaLinha + valInserido - valExcluido, 2)

    '        Dim VLROPEFSCDSNACOCMC As Decimal
    '        VLROPEFSCDSNACOCMC = objSaldoAcaoT0138008Valid.VLROPEFSCDSNACOCMC

    '        If ((VLRTOTMERISRPMC_Inserido > VLROPEFSCDSNACOCMC) AndAlso Not (acaoInformada.TIPACOMCD = 5)) Then
    '            Throw New Exception(String.Format("O Valor não pode ser maior que da operação! Valor inserido: {0} e valor operação: {1}", VLRTOTMERISRPMC_Inserido, VLROPEFSCDSNACOCMC))
    '        End If

    '    ElseIf (tipo = 1) Then 'Excluir

    '        Dim dsInserido As VO.Abastecimento = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 0})
    '        Dim valInserido As Decimal = 0
    '        If (dsInserido IsNot Nothing) Then
    '            valInserido = dsInserido.VLRTOTMERISRPMC
    '        End If

    '        Dim dsExcluido As VO.Abastecimento = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 1})
    '        Dim valExcluido As Decimal = 0
    '        If (dsExcluido IsNot Nothing) Then
    '            valExcluido = dsExcluido.VLRTOTMERISRPMC
    '        End If


    '        Dim dblVlrAux As Decimal = Decimal.Round(valInserido - valExcluido, 2)

    '        Dim dblVLRUTZ As Decimal = objSaldoAcaoT0138008Valid.VLROPEFSCDSNACOCMC.GetValueOrDefault() - objSaldoAcaoT0138008Valid.VLRUTZOPEDSNACOCMC.GetValueOrDefault() + dblVlrAux

    '        If (valExcluido < 0) Then
    '            Throw New Exception("O Valor Não pode ser maior que o existente!")
    '        End If


    '        'Obtem o valores (novamente, os parametros são outros diferentes dos da primeira chamada)
    '        dsInserido = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 0, .INDISRVLDMERPMC = -1, .CODPMC = CODPMC, .CODMER = CODMER})
    '        valInserido = 0
    '        If (dsInserido IsNot Nothing) Then
    '            valInserido = dsInserido.VLRTOTMERISRPMC
    '        End If

    '        dsExcluido = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 1, .INDISRVLDMERPMC = -1, .CODPMC = CODPMC, .CODMER = CODMER})
    '        valExcluido = 0
    '        If (dsExcluido IsNot Nothing) Then
    '            valExcluido = dsExcluido.VLRTOTMERISRPMC
    '        End If

    '        Dim QDEMERISRPMC_EXC As Decimal = valExcluido

    '        If (QDEMERISRPMC_EXC > valInserido) Then
    '            Throw New Exception("Não pode excluir quantidade maior do que foi cadastrado")
    '        End If

    '        dblVlrAux = Decimal.Round(valExcluido, 2)
    '        'Nao faz sentido, mas fonte está assim
    '        If (dblVlrAux > valInserido) Then
    '            Throw New Exception("Não pode excluir valor maior do que foi cadastrado")
    '        End If

    '    End If

    '    'Fim VerificarSaldoAcaoComercialT0138008
    '    'VerificarSaldoAcaoComercialT0138776

    '    'linhaSomaValorOperacaoEValorUtilizadoAcaoComercial
    '    Dim objSaldoAcaoT0138776Valida = DAORelacaoAcaoComItemOrcamento.SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(acaoInformada.CODACOCMC, CODMER)

    '    If (objSaldoAcaoT0138776Valida Is Nothing) Then
    '        Throw New Exception("Não Existe Ação Comercial!")
    '    End If

    '    valorDaLinha = 0

    '    If (tipo = 0) Then 'Inserir

    '        Dim dsInserido As VO.Abastecimento = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 0})
    '        Dim valInserido As Decimal = 0
    '        If (dsInserido IsNot Nothing) Then
    '            valInserido = dsInserido.VLRTOTMERISRPMC
    '        End If

    '        Dim dsExcluido As VO.Abastecimento = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 1})
    '        Dim valExcluido As Decimal = 0
    '        If (dsExcluido IsNot Nothing) Then
    '            valExcluido = dsExcluido.VLRTOTMERISRPMC
    '        End If


    '        Dim VLRTOTMERISRPMC_Inserido As Decimal
    '        VLRTOTMERISRPMC_Inserido = Decimal.Round(objSaldoAcaoT0138776Valida.VLRUTZOPEDSNACOCMC.GetValueOrDefault() + valorDaLinha + valInserido - valExcluido, 2)

    '        Dim VLRUTZOPEDSNACOCMC As Decimal
    '        VLRUTZOPEDSNACOCMC = objSaldoAcaoT0138776Valida.VLRITEOCDACOCMC

    '        If ((VLRTOTMERISRPMC_Inserido > VLRUTZOPEDSNACOCMC) AndAlso Not (acaoInformada.TIPACOMCD = 5)) Then
    '            Throw New Exception("O Valor não pode ser maior que da operação! Valor inserido: " + VLRTOTMERISRPMC_Inserido + " e valor operação: " + VLRUTZOPEDSNACOCMC)
    '        End If

    '    ElseIf (tipo = 1) Then 'Excluir

    '        Dim dsInserido As VO.Abastecimento = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 0})
    '        Dim valInserido As Decimal = 0
    '        If (dsInserido IsNot Nothing) Then
    '            valInserido = dsInserido.VLRTOTMERISRPMC
    '        End If

    '        Dim dsExcluido As VO.Abastecimento = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 1})
    '        Dim valExcluido As Decimal = 0
    '        If (dsExcluido IsNot Nothing) Then
    '            valExcluido = dsExcluido.VLRTOTMERISRPMC
    '        End If


    '        Dim dblVlrAux As Decimal = Decimal.Round(valInserido - valExcluido, 2)

    '        Dim dblVLRUTZ As Decimal = objSaldoAcaoT0138776Valida.VLRITEOCDACOCMC.GetValueOrDefault() - objSaldoAcaoT0138776Valida.VLRUTZOPEDSNACOCMC.GetValueOrDefault() + dblVlrAux

    '        If (valExcluido < 0) Then
    '            Throw New Exception("O Valor Não pode ser maior que o existente!")
    '        End If


    '        'Obtem o valores (novamente, os parametros são outros diferentes dos da primeira chamada)
    '        dsInserido = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 0, .INDISRVLDMERPMC = -1, .CODPMC = CODPMC, .CODMER = CODMER})
    '        valInserido = 0
    '        If (dsInserido IsNot Nothing) Then
    '            valInserido = dsInserido.VLRTOTMERISRPMC
    '        End If

    '        dsExcluido = DirectCast(daoBase, AbastecimentoDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(New VO.Abastecimento() With {.CODACOCMC = CODACOCMC, .INDTIPISRMERPMC = 1, .INDISRVLDMERPMC = -1, .CODPMC = CODPMC, .CODMER = CODMER})
    '        valExcluido = 0
    '        If (dsExcluido IsNot Nothing) Then
    '            valExcluido = dsExcluido.VLRTOTMERISRPMC
    '        End If

    '        Dim QDEMERISRPMC_EXC As Decimal = valExcluido

    '        If (QDEMERISRPMC_EXC > valInserido) Then
    '            Throw New Exception("Não pode excluir quantidade maior do que foi cadastrado")
    '        End If

    '        dblVlrAux = Decimal.Round(valExcluido, 2)
    '        'Nao faz sentido, mas fonte está assim
    '        If (dblVlrAux > valInserido) Then
    '            Throw New Exception("Não pode excluir valor maior do que foi cadastrado")
    '        End If

    '    End If

    '    'Fim VerificarSaldoAcaoComercialT0138776
    'End Sub


    Public Function SelectRelacaoAcaoComercialEPromocaoEBrinde(promocaoBrinde As VO.Abastecimento) As List(Of VO.Abastecimento)
        Return DirectCast(daoBase, AbastecimentoDAO).SelectRelacaoAcaoComercialEPromocaoEBrinde(promocaoBrinde)
    End Function
End Class
