﻿Imports AutoMapper
Imports Core

Public Class RelacaoAcaoMecanicaCriterioDeApuracaoBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaCriterioDeApuracaoDAO()
    End Sub

    Public Function EditRelacaoAcaoMecanicaCriterioDeApuracaoList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaCriterioDeApuracaoDAO).EditRelacaoAcaoMecanicaCriterioDeApuracaoList(CODACOCMC, NUMSEQRGRACOCMC, TIPPMC)
    End Function

    Public Function EditRelacaoAcaoMecanicaCriterioDeApuracao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal) As VO.RelacaoAcaoMecanicaCriterioDeApuracao
        Return DirectCast(daoBase, RelacaoAcaoMecanicaCriterioDeApuracaoDAO).EditRelacaoAcaoMecanicaCriterioDeApuracao(CODACOCMC, NUMSEQRGRACOCMC, TIPPMC)
    End Function

    Public Sub SaveList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal, ByVal CriterioDeApuracaoNormal As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao))

        MyBase.daoBase.DeleteT(New VO.RelacaoAcaoMecanicaCriterioDeApuracao() With {
            .CODACOCMC = CODACOCMC,
            .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
        })

        For i = 0 To CriterioDeApuracaoNormal.Count - 1
            CriterioDeApuracaoNormal(i).CODACOCMC = CODACOCMC
            CriterioDeApuracaoNormal(i).NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
            CriterioDeApuracaoNormal(i).NUMSEQCTRACOCMC = i + 1
            MyBase.daoBase.InsertT(CriterioDeApuracaoNormal(i))
        Next

    End Sub

    Public Function VerificaPromocaoTipoBrinde(ByVal CODACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao)

        Return DirectCast(daoBase, RelacaoAcaoMecanicaCriterioDeApuracaoDAO).VerificaPromocaoTipoBrinde(CODACOCMC)

    End Function


    'Public Sub InsertList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal relacaoAcaoMecanicaCriterioDeApuracaoList As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao))
    '    MyBase.daoBase.connector.BeginTransaction()
    '    Try

    '        Dim DAO As RelacaoAcaoMecanicaCriterioDeApuracaoDAO = New RelacaoAcaoMecanicaCriterioDeApuracaoDAO()

    '        Dim CriterioDeApuracaoSelecionadoListOLD As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao) = DAO.ListT(New VO.RelacaoAcaoMecanicaCriterioDeApuracao With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

    '        For Each item In relacaoAcaoMecanicaCriterioDeApuracaoList
    '            item.CODACOCMC = CODACOCMC
    '            item.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
    '            If Not Exists(CriterioDeApuracaoSelecionadoListOLD, item) Then
    '                MyBase.InsertT(item)
    '            End If

    '        Next

    '        For Each item In CriterioDeApuracaoSelecionadoListOLD
    '            If Not Exists(relacaoAcaoMecanicaCriterioDeApuracaoList, item) Then
    '                MyBase.DeleteT(item)
    '            End If

    '        Next

    '        MyBase.daoBase.connector.Commit()

    '    Catch ex As Exception
    '        MyBase.daoBase.connector.Rollback()
    '        Throw ex
    '    End Try
    'End Sub

    Private Function Exists(ByRef pList As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao), ByVal pObj As VO.RelacaoAcaoMecanicaCriterioDeApuracao) As Boolean
        Return pList.Where(Function(r) r.CODACOCMC = pObj.CODACOCMC And r.NUMSEQRGRACOCMC = pObj.NUMSEQRGRACOCMC And r.NUMSEQCTRACOCMC = pObj.NUMSEQCTRACOCMC And r.VLRLIMMNMFXACTRPMC = pObj.VLRLIMMNMFXACTRPMC And r.CODGRPMIXMERPMC = pObj.CODGRPMIXMERPMC).FirstOrDefault() IsNot Nothing
    End Function
End Class