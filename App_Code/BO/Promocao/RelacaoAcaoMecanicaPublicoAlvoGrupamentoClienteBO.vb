﻿Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteDAO()
    End Sub

    Public Function BuscarTipoGrupamentoCliente() As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteDAO).BuscarTipoGrupamentoCliente()
    End Function

    Public Function ListarGrupamentoCliente(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteDAO).ListarGrupamentoCliente(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Sub InsertList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteDAO = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteDAO()

            Dim GrupamentoClienteSelecionadoListOLD As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente) = DAO.ListarGrupamentoClienteSelecionadas(CODACOCMC, NUMSEQRGRACOCMC)

            For Each item In relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList
                item.CODACOCMC = CODACOCMC
                item.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                If Not Exists(GrupamentoClienteSelecionadoListOLD, item) Then
                    MyBase.InsertT(item)
                End If

            Next

            For Each item In GrupamentoClienteSelecionadoListOLD
                If Not Exists(relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList, item) Then
                    MyBase.DeleteT(item)
                End If

            Next

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Public Function BuscarGrupamentoClientePorBU(ByVal CODUNDESRNGC As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteDAO).BuscarGrupamentoClientePorBU(CODUNDESRNGC)
    End Function

    Private Function Exists(ByRef pList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente), ByVal pObj As VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente) As Boolean
        Return pList.Where(Function(r) r.CODACOCMC = pObj.CODACOCMC And r.NUMSEQRGRACOCMC = pObj.NUMSEQRGRACOCMC And r.CODGRPCLI = pObj.CODGRPCLI).FirstOrDefault() IsNot Nothing
    End Function
End Class
