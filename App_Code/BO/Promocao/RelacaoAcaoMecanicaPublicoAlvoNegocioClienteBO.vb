﻿Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoNegocioClienteBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaPublicoAlvoNegocioClienteDAO()
    End Sub

    Public Function ListarNegocioCliente(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoNegocioClienteDAO).ListarTipoNegocioCliente(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Sub InsertList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal relacaoAcaoMecanicaPublicoAlvoNegocioClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As RelacaoAcaoMecanicaPublicoAlvoNegocioClienteDAO = New RelacaoAcaoMecanicaPublicoAlvoNegocioClienteDAO()

            Dim NegocioClienteSelecionadoListOLD As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente) = DAO.ListarTipoNegocioClienteSelecionado(CODACOCMC, NUMSEQRGRACOCMC)

            For Each item In relacaoAcaoMecanicaPublicoAlvoNegocioClienteList
                item.CODACOCMC = CODACOCMC
                item.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                If Not Exists(NegocioClienteSelecionadoListOLD, item) Then
                    MyBase.InsertT(item)
                End If

            Next

            For Each item In NegocioClienteSelecionadoListOLD
                If Not Exists(relacaoAcaoMecanicaPublicoAlvoNegocioClienteList, item) Then
                    MyBase.DeleteT(item)
                End If

            Next

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Private Function Exists(ByRef pList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente), ByVal pObj As VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente) As Boolean
        Return pList.Where(Function(r) r.CODACOCMC = pObj.CODACOCMC And r.NUMSEQRGRACOCMC = pObj.NUMSEQRGRACOCMC And r.CODUNDESRNGC = pObj.CODUNDESRNGC).FirstOrDefault() IsNot Nothing
    End Function
End Class
