﻿Imports System.Data
Imports NPOI.HSSF.Extractor
Imports NPOI.HSSF.UserModel
Imports VO

Public Class RelacaoAcaoMecanicaMixBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaMix)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaMixDAO()
    End Sub


#Region "Mix Grupo Mercadoria"
    Public Function EditMixGrupoMercadoria(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).EditMixGrupoMercadoria(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Sub InsertListMixGrupoMercadoria(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal relacaoAcaoMecanicaMixGrupoMercadoriaList As List(Of VO.RelacaoAcaoMecanicaMix))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As RelacaoAcaoMecanicaMixDAO = New RelacaoAcaoMecanicaMixDAO()

            Dim GrupoMercadoriaSelecionadoListOLD As List(Of VO.RelacaoAcaoMecanicaMix) = DAO.ListarGrupoMercadoriaSelecionados(CODACOCMC, NUMSEQRGRACOCMC)

            For Each item In relacaoAcaoMecanicaMixGrupoMercadoriaList
                item.CODACOCMC = CODACOCMC
                item.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                If Not Exists(GrupoMercadoriaSelecionadoListOLD, item) Then

                    item.NUMSEQMIXACOCMC = DirectCast(MyBase.daoBase, RelacaoAcaoMecanicaMixDAO).SelectMaxNumSeqMixAcoCmc(CODACOCMC, NUMSEQRGRACOCMC).NUMSEQMIXACOCMC

                    MyBase.InsertT(item)
                End If

            Next

            For Each item In GrupoMercadoriaSelecionadoListOLD
                If Not Exists(relacaoAcaoMecanicaMixGrupoMercadoriaList, item) Then
                    MyBase.DeleteT(item)
                End If

            Next

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Private Function Exists(ByRef pList As List(Of VO.RelacaoAcaoMecanicaMix), ByVal pObj As VO.RelacaoAcaoMecanicaMix) As Boolean
        Return pList.Where(Function(r) r.CODACOCMC = pObj.CODACOCMC And r.NUMSEQRGRACOCMC = pObj.NUMSEQRGRACOCMC And r.CODGRPMER = pObj.CODGRPMER).FirstOrDefault() IsNot Nothing
    End Function

#End Region

#Region "Mix Familia Mercadoria"
    Public Function PesquisaMixFamiliaMercadoria(vo As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).PesquisaMixFamiliaMercadoria(vo)
    End Function

    Public Function ListarMixFamiliaMercadoriaAdicionados(vo As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).ListarMixFamiliaMercadoriaAdicionados(vo)
    End Function

    Public Function SelectMaxNumSeqMixAcoCmcFamiliaMercadoria(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As VO.RelacaoAcaoMecanicaMix
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).SelectMaxNumSeqMixAcoCmc(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Function ImportarExelMixFamiliaMercadoria(path As String, ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As String
        Try


            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.RelacaoAcaoMecanicaMix) = ListaExcelMixFamiliaMercadoria(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If


            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos familia de mercadoria para importar"
            End If

            Dim erros As New StringBuilder()

            Dim FamiliaMercadoriaAdicionadas As List(Of VO.RelacaoAcaoMecanicaMix) = Me.ListarMixFamiliaMercadoriaAdicionados(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

            For Each novaFamiliaMercadoria In dados



                'verificar se Família Mercadoria existe no cadastro de Família Mercadoria
                Dim relacaoAcaoMecanicaMixDAO As New RelacaoAcaoMecanicaMixDAO()
                Dim verificaSeFamiliaMercadoriaExiste As List(Of VO.RelacaoAcaoMecanicaMix) = relacaoAcaoMecanicaMixDAO.VerificarSeFamiliaMercadoriaEstaCadastrada(novaFamiliaMercadoria.CODFMLMER, novaFamiliaMercadoria.CODGRPMER)
                If (verificaSeFamiliaMercadoriaExiste.Count <= 0) Then
                    erros.Append("Linha ").Append(novaFamiliaMercadoria.CODFMLMER).Append("; ").Append(novaFamiliaMercadoria.CODGRPMER).Append(" Família Mercadoria não cadastrada.").AppendLine()
                    Continue For
                End If


                Dim verificaFamiliaMercadoriaJaAdicionada As List(Of VO.RelacaoAcaoMecanicaMix) = FamiliaMercadoriaAdicionadas.Where(Function(f) f.CODFMLMER = novaFamiliaMercadoria.CODFMLMER).ToList
                If (verificaFamiliaMercadoriaJaAdicionada.Count > 0) Then
                    erros.Append("Linha ").Append(novaFamiliaMercadoria.CODFMLMER).Append("; ").Append(novaFamiliaMercadoria.CODGRPMER).Append(" Família Mercadoria já adicionada.").AppendLine()
                    Continue For
                End If


                Dim verificaFamiliaMercadoriaDuplicadoXLS As List(Of VO.RelacaoAcaoMecanicaMix) = dados.Where(Function(f) f.CODFMLMER = novaFamiliaMercadoria.CODFMLMER AndAlso f.CODACOCMC IsNot Nothing).ToList
                If (verificaFamiliaMercadoriaDuplicadoXLS.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                    erros.Append("Linha ").Append(novaFamiliaMercadoria.CODFMLMER).Append("; ").Append(novaFamiliaMercadoria.CODGRPMER).Append(" Família Mercadoria já adicionado na planilha.").AppendLine()
                    Continue For
                End If

                Try
                    If (Not (novaFamiliaMercadoria.CODFMLMER = 0)) Then
                        'Obter max NUMSEQMIXACOCMC
                        Dim objFamiliaMercadoria As VO.RelacaoAcaoMecanicaMix = Me.SelectMaxNumSeqMixAcoCmc(CODACOCMC, NUMSEQRGRACOCMC)

                        'Atribuir dados no objeto
                        novaFamiliaMercadoria.NUMSEQMIXACOCMC = objFamiliaMercadoria.NUMSEQMIXACOCMC
                        novaFamiliaMercadoria.CODACOCMC = CODACOCMC
                        novaFamiliaMercadoria.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                        InsertT(novaFamiliaMercadoria)

                    End If
                Catch ex As Exception
                    erros.Append("Linha ").Append(novaFamiliaMercadoria.CODFMLMER).Append("; ").Append(novaFamiliaMercadoria.CODGRPMER).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                    Continue For
                End Try
            Next
            Return erros.ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function ListaExcelMixFamiliaMercadoria(path As String, ByRef erros As String) As List(Of VO.RelacaoAcaoMecanicaMix)
        Dim linhasImportadas As New List(Of VO.RelacaoAcaoMecanicaMix)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (alinha.Count <> 2) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 2 coluna de dados sendo ""Codigo da Familia da Mercadoria"" e ""Codigo do Grupo da Mercadoria""", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo da Familia da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Familia da Mercadoria""", NumLinha)
                    Return Nothing
                End If
                If (alinha(1).Replace("""", "") <> "Codigo do Grupo da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Codigo do Grupo da Mercadoria""", NumLinha)
                    Return Nothing
                End If

            Else ' Linhas de dados
                Dim novoLinha As New VO.RelacaoAcaoMecanicaMix
                Dim CODFMLMER As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODFMLMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Familia da Mercadoria"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If
                novoLinha.CODFMLMER = CODFMLMER

                Dim CODGRPMER As Decimal
                If (Not Decimal.TryParse(alinha(1).Replace("""", ""), CODGRPMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo do Grupo da Mercadoria"" válido", NumLinha, alinha(1))
                    Return Nothing
                End If
                novoLinha.CODGRPMER = CODGRPMER

                linhasImportadas.Add(novoLinha)
            End If
            NumLinha += 1
        Next

        Return linhasImportadas


    End Function

    Public Sub AddPesquisaMixFamiliaMercadoria(ByRef filtro As VO.RelacaoAcaoMecanicaMix)

        Dim voList As List(Of VO.RelacaoAcaoMecanicaMix) = Me.PesquisaMixFamiliaMercadoria(filtro)
        Dim mercadoriasAdicionadas As List(Of VO.RelacaoAcaoMecanicaMix) = Me.ListarMixFamiliaMercadoriaAdicionados(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = filtro.CODACOCMC, .NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC})
        For Each novaFamiliaMercadoria In voList.Select(Function(f) New VO.RelacaoAcaoMecanicaMix() With {.CODFMLMER = f.CODFMLMER, .CODGRPMER = f.CODGRPMER}).ToList()
            If (mercadoriasAdicionadas.Where(Function(f) f.CODFMLMER = novaFamiliaMercadoria.CODFMLMER And f.CODGRPMER = novaFamiliaMercadoria.CODGRPMER).Count() = 0) Then
                If (Not (novaFamiliaMercadoria.CODFMLMER = 0)) Then

                    Dim NUMSEQMIXACOCMC As Decimal? = Me.SelectMaxNumSeqMixAcoCmc(filtro.CODACOCMC, filtro.NUMSEQRGRACOCMC).NUMSEQMIXACOCMC

                    'Atribuir dados no objeto
                    novaFamiliaMercadoria.NUMSEQMIXACOCMC = NUMSEQMIXACOCMC
                    novaFamiliaMercadoria.CODACOCMC = filtro.CODACOCMC
                    novaFamiliaMercadoria.NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC

                    InsertT(novaFamiliaMercadoria)
                End If
            End If
        Next
    End Sub
#End Region

#Region "Mix Classe Mercadoria"
    Public Function PesquisaMixClasseMercadoria(vo As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).PesquisaMixClasseMercadoria(vo)
    End Function

    Public Function ListarMixClasseMercadoriaAdicionados(vo As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).ListarMixClasseMercadoriaAdicionados(vo)
    End Function

    Public Function ImportarExelMixClasseMercadoria(path As String, ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As String
        Try
            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.RelacaoAcaoMecanicaMix) = ListaExcelMixClasseMercadoria(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos classe de mercadoria para importar"
            End If


            Dim erros As New StringBuilder()

            Dim ClasseMercadoriaAdicionadas As List(Of VO.RelacaoAcaoMecanicaMix) = Me.ListarMixClasseMercadoriaAdicionados(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})


            For Each novaClasseMercadoria In dados

                'verificar se a classe de Mercadoria existe no cadastro de classe de Mercadoria
                Dim relacaoAcaoMecanicaMixDAO As New RelacaoAcaoMecanicaMixDAO()
                Dim verificaSeClasseMercadoriaExiste As List(Of VO.RelacaoAcaoMecanicaMix) = relacaoAcaoMecanicaMixDAO.VerificarSeClasseMercadoriaEstaCadastrada(novaClasseMercadoria.CODCLSMER, novaClasseMercadoria.CODGRPMER, novaClasseMercadoria.CODFMLMER)
                If (verificaSeClasseMercadoriaExiste.Count <= 0) Then
                    erros.Append("Linha ").Append(novaClasseMercadoria.CODCLSMER).Append("; ").Append(novaClasseMercadoria.CODGRPMER).Append("; ").Append(novaClasseMercadoria.CODFMLMER).Append(" Classe Mercadoria não cadastrada.").AppendLine()
                    Continue For
                End If

                Dim verificaClasseMercadoriaJaAdicionada As List(Of VO.RelacaoAcaoMecanicaMix) = ClasseMercadoriaAdicionadas.Where(Function(f) f.CODCLSMER = novaClasseMercadoria.CODCLSMER And f.CODGRPMER = novaClasseMercadoria.CODGRPMER And f.CODFMLMER = novaClasseMercadoria.CODFMLMER).ToList
                If (verificaClasseMercadoriaJaAdicionada.Count > 0) Then
                    erros.Append("Linha ").Append(novaClasseMercadoria.CODCLSMER).Append("; ").Append(novaClasseMercadoria.CODGRPMER).Append("; ").Append(novaClasseMercadoria.CODFMLMER).Append(" Classe Mercadoria já adicionada.").AppendLine()
                    Continue For
                End If


                Dim verificaClasseMercadoriaDuplicadoXLS As List(Of VO.RelacaoAcaoMecanicaMix) = dados.Where(Function(f) f.CODCLSMER = novaClasseMercadoria.CODCLSMER And f.CODGRPMER = novaClasseMercadoria.CODGRPMER And f.CODFMLMER = novaClasseMercadoria.CODFMLMER AndAlso f.CODACOCMC IsNot Nothing).ToList
                If (verificaClasseMercadoriaDuplicadoXLS.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                    erros.Append("Linha ").Append(novaClasseMercadoria.CODCLSMER).Append("; ").Append(novaClasseMercadoria.CODGRPMER).Append("; ").Append(novaClasseMercadoria.CODFMLMER).Append(" Classe Mercadoria já adicionado na planilha.").AppendLine()
                    Continue For
                End If

                Try
                    If (Not (novaClasseMercadoria.CODCLSMER = 0)) Then
                        'Obter max NUMSEQMIXACOCMC
                        Dim objClasseMercadoria As VO.RelacaoAcaoMecanicaMix = Me.SelectMaxNumSeqMixAcoCmc(CODACOCMC, NUMSEQRGRACOCMC)

                        'Atribuir dados no objeto
                        novaClasseMercadoria.NUMSEQMIXACOCMC = objClasseMercadoria.NUMSEQMIXACOCMC
                        novaClasseMercadoria.CODACOCMC = CODACOCMC
                        novaClasseMercadoria.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                        InsertT(novaClasseMercadoria)
                    End If
                Catch ex As Exception
                    erros.Append("Linha ").Append(novaClasseMercadoria.CODCLSMER).Append("; ").Append(novaClasseMercadoria.CODGRPMER).Append("; ").Append(novaClasseMercadoria.CODFMLMER).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                    Continue For
                End Try

            Next
            Return erros.ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function


    Public Function ListaExcelMixClasseMercadoria(path As String, ByRef erros As String) As List(Of VO.RelacaoAcaoMecanicaMix)
        Dim linhasImportadas As New List(Of VO.RelacaoAcaoMecanicaMix)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (alinha.Count <> 3) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 3 coluna de dados sendo ""Codigo da Classe da Mercadoria"", ""Codigo do Grupo da Mercadoria"" e  ""Codigo da Familia da Mercadoria""", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo da Classe da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Classe da Mercadoria""", NumLinha)
                    Return Nothing
                End If
                If (alinha(1).Replace("""", "") <> "Codigo do Grupo da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Codigo do Grupo da Mercadoria""", NumLinha)
                    Return Nothing
                End If
                If (alinha(2).Replace("""", "") <> "Codigo da Familia da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Codigo da Familia da Mercadoria""", NumLinha)
                    Return Nothing
                End If

            Else ' Linhas de dados
                Dim novoLinha As New VO.RelacaoAcaoMecanicaMix
                Dim CODCLSMER As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODCLSMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Classe da Mercadoria"" válida", NumLinha, alinha(0))
                    Return Nothing
                End If
                novoLinha.CODCLSMER = CODCLSMER

                Dim CODGRPMER As Decimal
                If (Not Decimal.TryParse(alinha(1).Replace("""", ""), CODGRPMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo do Grupo da Mercadoria"" válido", NumLinha, alinha(1))
                    Return Nothing
                End If
                novoLinha.CODGRPMER = CODGRPMER

                Dim CODFMLMER As Decimal
                If (Not Decimal.TryParse(alinha(2).Replace("""", ""), CODFMLMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Familia da Mercadoria"" válida", NumLinha, alinha(2))
                    Return Nothing
                End If
                novoLinha.CODFMLMER = CODFMLMER

                linhasImportadas.Add(novoLinha)
            End If
            NumLinha += 1
        Next

        Return linhasImportadas


    End Function


    Public Sub AddPesquisaMixClasseMercadoria(ByRef filtro As VO.RelacaoAcaoMecanicaMix)

        Dim voList As List(Of VO.RelacaoAcaoMecanicaMix) = Me.PesquisaMixClasseMercadoria(filtro)
        Dim classeMercadoriasAdicionadas As List(Of VO.RelacaoAcaoMecanicaMix) = Me.ListarMixClasseMercadoriaAdicionados(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = filtro.CODACOCMC, .NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC})
        For Each novaClasseMercadoria In voList.Select(Function(f) New VO.RelacaoAcaoMecanicaMix() With {.CODCLSMER = f.CODCLSMER, .CODGRPMER = f.CODGRPMER, .CODFMLMER = f.CODFMLMER}).ToList()
            If (classeMercadoriasAdicionadas.Where(Function(f) f.CODCLSMER = novaClasseMercadoria.CODCLSMER And f.CODGRPMER = novaClasseMercadoria.CODGRPMER And f.CODFMLMER = novaClasseMercadoria.CODFMLMER).Count() = 0) Then
                If (Not (novaClasseMercadoria.CODCLSMER = 0)) Then

                    Dim NUMSEQMIXACOCMC As Decimal? = Me.SelectMaxNumSeqMixAcoCmc(filtro.CODACOCMC, filtro.NUMSEQRGRACOCMC).NUMSEQMIXACOCMC

                    'Atribuir dados no objeto
                    novaClasseMercadoria.NUMSEQMIXACOCMC = NUMSEQMIXACOCMC
                    novaClasseMercadoria.CODACOCMC = filtro.CODACOCMC
                    novaClasseMercadoria.NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC

                    InsertT(novaClasseMercadoria)
                End If
            End If
        Next
    End Sub
#End Region

#Region "Mix Mercadoria"
    Public Function PesquisaMixMercadoria(vo As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).PesquisaMixMercadoria(vo)
    End Function
    Public Function PesquisaMixMercadoriaEdl(vo As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).PesquisaMixMercadoriaEdl(vo)
    End Function
    Public Function PesquisaMixMercadoriaPorPromocao(ByVal CODPMC As Decimal, ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).PesquisaMixMercadoriaPorPromocao(CODPMC, CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Function ListarMixMercadoriaAdicionados(vo As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).ListarMixMercadoriaAdicionados(vo)
    End Function

    Public Function ListarMixMercadoriaEdlAdicionados(vo As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).ListarMixMercadoriaEdlAdicionados(vo)
    End Function

    Public Function ImportarExelMixMercadoria(path As String, ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal QDEGRPMIXPMC As Decimal) As String
        Try

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.RelacaoAcaoMecanicaMix) = ListaExcelMixMercadoria(path, erroImportExcel, QDEGRPMIXPMC)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos de mercadoria para importar"
            End If

            If dados.Count > 30000 Then
                Return "Este arquivo possui mais de 30000 codigos de mercadoria para importar"
            End If

            Dim erros As New StringBuilder()

            Dim relacaoAcaoFornecedorBO As RelacaoAcaoComercialxFornecedoresBO = New RelacaoAcaoComercialxFornecedoresBO()
            Dim relacaoAcaoFornecedor As RelacaoAcaoComercialxFornecedores = relacaoAcaoFornecedorBO.BuscarFornecedoresDaAcao(CODACOCMC).FirstOrDefault()

            Dim MercadoriaAdicionadas As List(Of VO.RelacaoAcaoMecanicaMix) = If(relacaoAcaoFornecedor.TIPFRNACOCMC = 1,
                                                                                Me.ListarMixMercadoriaEdlAdicionados(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC}),
                                                                                Me.ListarMixMercadoriaAdicionados(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC}))

            For Each novaMercadoria In dados

                Dim verificaMercadoriaJaAdicionada As List(Of VO.RelacaoAcaoMecanicaMix) = MercadoriaAdicionadas.Where(Function(f) f.CODMER = novaMercadoria.CODMER).ToList

                If (relacaoAcaoFornecedor.TIPFRNACOCMC = 0) Then
                    'verificar se Mercadoria existe no cadastro de Mercadoria
                    Dim mercadoriaDAO As New MercadoriaDAO()
                    Dim verificaSeMercadoriaValida As VO.Mercadoria = mercadoriaDAO.SelectT(New VO.Mercadoria With {.CODMER = novaMercadoria.CODMER})

                    If (IsNothing(verificaSeMercadoriaValida)) Then
                        erros.Append("Linha ").Append(novaMercadoria.CODMER).Append(" Mercadoria inválida.").AppendLine()
                        Continue For
                    End If

                    If (verificaSeMercadoriaValida.DATDSTMER IsNot Nothing) Then
                        erros.Append("Linha ").Append(novaMercadoria.CODMER).Append(" Mercadoria inválida.").AppendLine()
                        Continue For
                    End If

                    Dim verificaSeMercadoriaExiste As List(Of VO.Mercadoria) = mercadoriaDAO.BuscaNomeMercadoria(novaMercadoria.CODMER)
                    If (verificaSeMercadoriaExiste.Count <= 0) Then
                        erros.Append("Linha ").Append(novaMercadoria.CODMER).Append(" Mercadoria não cadastrado.").AppendLine()
                        Continue For
                    End If
                End If

                If (verificaMercadoriaJaAdicionada.Count > 0) Then
                    erros.Append("Linha ").Append(novaMercadoria.CODMER).Append(" Mercadoria já adicionada.").AppendLine()
                    Continue For
                End If


                Dim verificaMercadoriaDuplicadoXLS As List(Of VO.RelacaoAcaoMecanicaMix) = dados.Where(Function(f) f.CODMER = novaMercadoria.CODMER AndAlso f.CODACOCMC IsNot Nothing).ToList
                If (verificaMercadoriaDuplicadoXLS.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                    erros.Append("Linha ").Append(novaMercadoria.CODMER).Append(" Mercadoria já adicionado na planilha.").AppendLine()
                    Continue For
                End If

                Try
                    If (Not (novaMercadoria.CODMER = 0)) Then
                        'Obter max NUMSEQMIXACOCMC
                        Dim objMercadoria As VO.RelacaoAcaoMecanicaMix = Me.SelectMaxNumSeqMixAcoCmc(CODACOCMC, NUMSEQRGRACOCMC)

                        'Atribuir dados no objeto
                        novaMercadoria.NUMSEQMIXACOCMC = objMercadoria.NUMSEQMIXACOCMC
                        novaMercadoria.CODACOCMC = CODACOCMC
                        novaMercadoria.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                        InsertT(novaMercadoria)
                    End If
                Catch ex As Exception
                    erros.Append("Linha ").Append(novaMercadoria.CODMER).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                    Continue For
                End Try

            Next
            Return erros.ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function ListaExcelMixMercadoria(path As String, ByRef erros As String, QDEGRPMIXPMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)
        Dim linhasImportadas As New List(Of VO.RelacaoAcaoMecanicaMix)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (QDEGRPMIXPMC > 0) Then
                If (alinha.Count <> 2) Then
                    erros = String.Format("Erro Linha {0}: O excel deve conter 2 colunas de dados sendo ""Codigo da Mercadoria"" e ""Grupo""", NumLinha)
                    Return Nothing
                End If
            Else
                If (Not (alinha.Count = 2 Or alinha.Count = 1)) Then
                    erros = String.Format("Erro Linha {0}: O excel deve conter 2 colunas de dados sendo ""Codigo da Mercadoria"" e ""Grupo""", NumLinha)
                    Return Nothing
                End If
            End If


            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (QDEGRPMIXPMC > 0) Then
                    If (alinha(0).Replace("""", "") <> "Codigo da Mercadoria" Or alinha(1).Replace("""", "") <> "Grupo") Then
                        erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Mercadoria"" e na segunda deve conter a coluna  e ""Grupo""", NumLinha)
                        Return Nothing
                    End If
                Else
                    If (alinha(0).Replace("""", "") <> "Codigo da Mercadoria") Then
                        erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Mercadoria""", NumLinha)
                        Return Nothing
                    End If

                End If
            Else ' Linhas de dados
                Dim novoLinha As New VO.RelacaoAcaoMecanicaMix
                If (QDEGRPMIXPMC > 0) Then
                    Dim CODMER As Decimal
                    If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODMER)) Then
                        erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Mercadoria"" válido", NumLinha, alinha(0))
                        Return Nothing
                    End If
                    novoLinha.CODMER = CODMER

                    Dim CODGRPMIXMERPMC As Decimal
                    If (Not Decimal.TryParse(alinha(1).Replace("""", ""), CODGRPMIXMERPMC)) Then
                        erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Grupo"" válido", NumLinha, alinha(1))
                        Return Nothing
                    End If
                    novoLinha.CODGRPMIXMERPMC = CODGRPMIXMERPMC

                Else
                    Dim CODMER As Decimal
                    If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODMER)) Then
                        erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Mercadoria"" válido", NumLinha, alinha(0))
                        Return Nothing
                    End If

                    novoLinha.CODMER = CODMER
                End If

                linhasImportadas.Add(novoLinha)
            End If
            NumLinha += 1
        Next

        Return linhasImportadas


    End Function

    Public Sub AddPesquisaMixMercadoria(ByRef filtro As VO.RelacaoAcaoMecanicaMix)

        Dim voList As List(Of VO.RelacaoAcaoMecanicaMix)
        If (filtro.TIPFRNACOCMC = 1) Then
            voList = Me.PesquisaMixMercadoriaEdl(filtro)
        Else
            voList = Me.PesquisaMixMercadoria(filtro)
        End If

        Dim mercadoriasAdicionadas As List(Of VO.RelacaoAcaoMecanicaMix) = Me.ListarMixMercadoriaAdicionados(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = filtro.CODACOCMC, .NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC})
        For Each novaMercadoria In voList.Select(Function(f) New VO.RelacaoAcaoMecanicaMix() With {.CODMER = f.CODMER}).ToList()
            If (mercadoriasAdicionadas.Where(Function(f) f.CODMER = novaMercadoria.CODMER).Count() = 0) Then
                If (Not (novaMercadoria.CODMER = 0)) Then

                    Dim NUMSEQMIXACOCMC As Decimal? = Me.SelectMaxNumSeqMixAcoCmc(filtro.CODACOCMC, filtro.NUMSEQRGRACOCMC).NUMSEQMIXACOCMC

                    'Atribuir dados no objeto
                    novaMercadoria.NUMSEQMIXACOCMC = NUMSEQMIXACOCMC
                    novaMercadoria.CODACOCMC = filtro.CODACOCMC
                    novaMercadoria.NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC
                    novaMercadoria.CODGRPMIXMERPMC = filtro.CODGRPMIXMERPMC

                    InsertT(novaMercadoria)
                End If
            End If
        Next
    End Sub

    Public Sub AddPesquisaMixMercadoriaPorPromocao(ByVal CODPMC As Decimal, ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal CODGRPMIXMERPMC As Nullable(Of Decimal))

        Dim voList As List(Of VO.RelacaoAcaoMecanicaMix) = Me.PesquisaMixMercadoriaPorPromocao(CODPMC, CODACOCMC, NUMSEQRGRACOCMC)
        Dim mercadoriasAdicionadas As List(Of VO.RelacaoAcaoMecanicaMix) = Me.ListarMixMercadoriaAdicionados(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})
        For Each novaMercadoria In voList.Select(Function(f) New VO.RelacaoAcaoMecanicaMix() With {.CODMER = f.CODMER}).ToList()
            If (mercadoriasAdicionadas.Where(Function(f) f.CODMER = novaMercadoria.CODMER).Count() = 0) Then
                If (Not (novaMercadoria.CODMER = 0)) Then

                    Dim NUMSEQMIXACOCMC As Decimal? = Me.SelectMaxNumSeqMixAcoCmc(CODACOCMC, NUMSEQRGRACOCMC).NUMSEQMIXACOCMC

                    'Atribuir dados no objeto
                    novaMercadoria.NUMSEQMIXACOCMC = NUMSEQMIXACOCMC
                    novaMercadoria.CODACOCMC = CODACOCMC
                    novaMercadoria.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                    novaMercadoria.CODGRPMIXMERPMC = CODGRPMIXMERPMC
                    InsertT(novaMercadoria)
                End If
            End If
        Next
    End Sub

#End Region

#Region "Mix Fornecedor"
    Public Function PesquisaMixFornecedor(vo As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).PesquisaMixFornecedor(vo)
    End Function

    Public Function ListarMixFornecedorAdicionados(vo As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).ListarMixFornecedorAdicionados(vo)
    End Function

    Public Function ImportarExelMixFornecedor(path As String, ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As String
        Try

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.RelacaoAcaoMecanicaMix) = ListaExcelMixFornecedor(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos de fornecedor para importar"
            End If

            Dim erros As New StringBuilder()

            Dim FornecedorAdicionado As List(Of VO.RelacaoAcaoMecanicaMix) = Me.ListarMixFornecedorAdicionados(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})


            For Each novoFornecedor In dados

                'verificar se Gerente Mercado existe no cadastro de Gerente de Mercado
                Dim fornecedoresDAO As New FornecedoresDAO()
                Dim verificaSeFornecedorExiste As List(Of VO.Fornecedores) = fornecedoresDAO.BuscarNomeCodigoFornecedores(novoFornecedor.CODFRN)
                If (verificaSeFornecedorExiste.Count <= 0) Then
                    erros.Append("Linha ").Append(novoFornecedor.CODFRN).Append(" Fornecedor não cadastrado.").AppendLine()
                    Continue For
                End If

                Dim verificaFornecedorJaAdicionado As List(Of VO.RelacaoAcaoMecanicaMix) = FornecedorAdicionado.Where(Function(f) f.CODFRN = novoFornecedor.CODFRN).ToList
                If (verificaFornecedorJaAdicionado.Count > 0) Then
                    erros.Append("Linha ").Append(novoFornecedor.CODFRN).Append(" Fornecedor já cadastrado.").AppendLine()
                    Continue For
                End If

                Dim verificaFornecedorDuplicadoXLS As List(Of VO.RelacaoAcaoMecanicaMix) = dados.Where(Function(f) f.CODFRN = novoFornecedor.CODFRN AndAlso f.CODACOCMC IsNot Nothing).ToList
                If (verificaFornecedorDuplicadoXLS.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                    erros.Append("Linha ").Append(novoFornecedor.CODFRN).Append(" Fornecedor já adicionado na planilha.").AppendLine()
                    Continue For
                End If

                Try
                    If (Not (novoFornecedor.CODFRN = 0)) Then

                        'Obter max NUMSEQMIXACOCMC
                        Dim objFornecedor As VO.RelacaoAcaoMecanicaMix = Me.SelectMaxNumSeqMixAcoCmc(CODACOCMC, NUMSEQRGRACOCMC)

                        'Atribuir dados no objeto
                        novoFornecedor.NUMSEQMIXACOCMC = objFornecedor.NUMSEQMIXACOCMC
                        novoFornecedor.CODACOCMC = CODACOCMC
                        novoFornecedor.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                        InsertT(novoFornecedor)

                    End If
                Catch ex As Exception
                    erros.Append("Linha ").Append(novoFornecedor.CODFRN).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                    Continue For
                End Try

            Next
            Return erros.ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function


    Public Function ListaExcelMixFornecedor(path As String, ByRef erros As String) As List(Of VO.RelacaoAcaoMecanicaMix)
        Dim linhasImportadas As New List(Of VO.RelacaoAcaoMecanicaMix)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (alinha.Count <> 1) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 1 coluna de dados sendo ""Codigo do Fornecedor""", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo do Fornecedor") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo do Fornecedor""", NumLinha)
                    Return Nothing
                End If
            Else ' Linhas de dados
                Dim novoLinha As New VO.RelacaoAcaoMecanicaMix
                Dim CODFRN As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODFRN)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo de Fornecedor"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If
                novoLinha.CODFRN = CODFRN

                linhasImportadas.Add(novoLinha)
            End If
            NumLinha += 1
        Next

        Return linhasImportadas

    End Function


    Public Sub AddPesquisaMixFornecedor(ByRef filtro As VO.RelacaoAcaoMecanicaMix)

        Dim voList As List(Of VO.RelacaoAcaoMecanicaMix) = Me.PesquisaMixFornecedor(filtro)
        Dim FornecedorAdicionado As List(Of VO.RelacaoAcaoMecanicaMix) = Me.ListarMixFornecedorAdicionados(New VO.RelacaoAcaoMecanicaMix With {.CODACOCMC = filtro.CODACOCMC, .NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC})
        For Each novoFornecedor In voList.Select(Function(f) New VO.RelacaoAcaoMecanicaMix() With {.CODFRN = f.CODFRN}).ToList()
            If (FornecedorAdicionado.Where(Function(f) f.CODFRN = novoFornecedor.CODFRN).Count() = 0) Then
                If (Not (novoFornecedor.CODFRN = 0)) Then
                    'Obter max NUMSEQMIXACOCMC
                    Dim objFornecedor As VO.RelacaoAcaoMecanicaMix = Me.SelectMaxNumSeqMixAcoCmc(filtro.CODACOCMC, filtro.NUMSEQRGRACOCMC)

                    'Atribuir dados no objeto
                    novoFornecedor.NUMSEQMIXACOCMC = objFornecedor.NUMSEQMIXACOCMC
                    novoFornecedor.CODACOCMC = filtro.CODACOCMC
                    novoFornecedor.NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC

                    InsertT(novoFornecedor)
                End If
            End If
        Next
    End Sub

#End Region


    'Public Function SelectCarregarDropGrupoMercadoria() As List(Of VO.RelacaoAcaoMecanicaMix)
    '    Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).SelectCarregarDropGrupoMercadoria()
    'End Function

    'Public Function SelectCarregarDropCategoria(ByVal CODGRPMER As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)
    '    Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).SelectCarregarDropCategoria(CODGRPMER)
    'End Function

    'Public Function SelectCarregarDropSubCategoria(ByVal CODGRPMER As Decimal, ByVal CODFMLMER As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)
    '    Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).SelectCarregarDropSubCategoria(CODGRPMER, CODFMLMER)
    'End Function

#Region "Count Mix Controle Botão 'Configurar' e 'Selecionar'"
    Public Function SelectCountMix(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As VO.RelacaoAcaoMecanicaMix
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).SelectCountMix(CODACOCMC, NUMSEQRGRACOCMC)
    End Function
#End Region



    Public Function SelectMaxNumSeqMixAcoCmc(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As VO.RelacaoAcaoMecanicaMix
        Return DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).SelectMaxNumSeqMixAcoCmc(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Friend Sub DeleteMixFornecedor(relacaoAcaoMecanicaMixFornecedor As RelacaoAcaoMecanicaMix)
        DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).DeleteMixFornecedor(relacaoAcaoMecanicaMixFornecedor)
    End Sub
    Friend Sub DeleteMixMercadoria(relacaoAcaoMecanicaMixFornecedor As RelacaoAcaoMecanicaMix)
        DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).DeleteMixMercadoria(relacaoAcaoMecanicaMixFornecedor)
    End Sub
    Friend Sub DeleteMixClasseMercadoria(relacaoAcaoMecanicaMixFornecedor As RelacaoAcaoMecanicaMix)
        DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).DeleteMixClasseMercadoria(relacaoAcaoMecanicaMixFornecedor)
    End Sub
    Friend Sub DeleteMixFamiliaMercadoria(relacaoAcaoMecanicaMixFornecedor As RelacaoAcaoMecanicaMix)
        DirectCast(daoBase, RelacaoAcaoMecanicaMixDAO).DeleteMixFamiliaMercadoria(relacaoAcaoMecanicaMixFornecedor)
    End Sub
End Class