﻿Imports Core

Public Class TipoCriterioBO
    Inherits BOBase(Of VO.TipoCriterio)

    Public Sub New()
        MyBase.daoBase = New TipoCriterioDAO()
    End Sub

    Public Function CarregaDropTipoCriterio(ByVal DESCTRPMC As String, ByVal CODCTRPMC As Decimal?, ByVal TIPEDEPUBALVPMC As Decimal?) As List(Of VO.TipoCriterio)
        Return DirectCast(daoBase, TipoCriterioDAO).CarregaDropTipoCriterio(DESCTRPMC, CODCTRPMC, TIPEDEPUBALVPMC)
    End Function
End Class
