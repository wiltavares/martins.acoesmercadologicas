﻿Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoAtividadeBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaPublicoAlvoAtividadeDAO()
    End Sub

    Public Function BuscarTipoAtividade() As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoAtividadeDAO).BuscarTipoAtividade()
    End Function

    Public Function ListarAtividade(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoAtividadeDAO).ListarAtividade(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Sub InsertList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal relacaoAcaoMecanicaPublicoAlvoAtividadeList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As RelacaoAcaoMecanicaPublicoAlvoAtividadeDAO = New RelacaoAcaoMecanicaPublicoAlvoAtividadeDAO()

            Dim AtividadeSelecionadoListOLD As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade) = DAO.ListarAtividadesSelecionadas(CODACOCMC, NUMSEQRGRACOCMC)

            For Each item In relacaoAcaoMecanicaPublicoAlvoAtividadeList
                item.CODACOCMC = CODACOCMC
                item.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                If Not Exists(AtividadeSelecionadoListOLD, item) Then
                    MyBase.InsertT(item)
                End If

            Next

            For Each item In AtividadeSelecionadoListOLD
                If Not Exists(relacaoAcaoMecanicaPublicoAlvoAtividadeList, item) Then
                    MyBase.DeleteT(item)
                End If

            Next

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Public Function BuscarAtividadePorBU(ByVal CODUNDESRNGC As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoAtividadeDAO).BuscarAtividadePorBU(CODUNDESRNGC)
    End Function

    Private Function Exists(ByRef pList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade), ByVal pObj As VO.RelacaoAcaoMecanicaPublicoAlvoAtividade) As Boolean
        Return pList.Where(Function(r) r.CODACOCMC = pObj.CODACOCMC And r.NUMSEQRGRACOCMC = pObj.NUMSEQRGRACOCMC And r.CODATI = pObj.CODATI).FirstOrDefault() IsNot Nothing
    End Function
End Class
