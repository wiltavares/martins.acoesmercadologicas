﻿Imports System.Data
Imports NPOI.HSSF.UserModel
Imports NPOI.HSSF.Extractor

Public Class PromocaoHistoricoSituacaoBO
    Inherits BOBase(Of VO.PromocaoHistoricoSituacao)

    Public Sub New()
        MyBase.daoBase = New PromocaoHistoricoSituacaoDAO()
    End Sub

    Public Overloads Function InsertT(vo As VO.PromocaoHistoricoSituacao, Optional startTransaction As Boolean = True) As VO.PromocaoHistoricoSituacao
        Dim last = MyBase.ListT(New VO.PromocaoHistoricoSituacao With {.CODPMC = vo.CODPMC}).OrderByDescending(Function(f) f.NUMSEQ).FirstOrDefault()
        If (last Is Nothing) Then
            vo.NUMSEQ = 1
        Else
            vo.NUMSEQ = last.NUMSEQ + 1
        End If

        Return MyBase.InsertT(vo, startTransaction)

    End Function

    Public Function ListHistorico(CODPMC As Decimal) As List(Of VO.PromocaoHistoricoSituacao)
        Return DirectCast(daoBase, PromocaoHistoricoSituacaoDAO).ListHistorico(CODPMC)
    End Function
End Class
