﻿Imports Core

Public Class PromocaoPublicoAlvoAtividadeBO
    Inherits BOBase(Of VO.PromocaoPublicoAlvoAtividade)

    Public Sub New()
        MyBase.daoBase = New PromocaoPublicoAlvoAtividadeDAO()
    End Sub

    Public Function BuscarTipoAtividade() As List(Of VO.PromocaoPublicoAlvoAtividade)
        Return DirectCast(daoBase, PromocaoPublicoAlvoAtividadeDAO).BuscarTipoAtividade()
    End Function

    Public Function ListarAtividade(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoAtividade)
        Return DirectCast(daoBase, PromocaoPublicoAlvoAtividadeDAO).ListarAtividade(CODPMC, TIPEDEPUBALVPMC)
    End Function

    Public Sub InsertList(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal PromocaoPublicoAlvoAtividadeList As List(Of VO.PromocaoPublicoAlvoAtividade))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As PromocaoPublicoAlvoAtividadeDAO = New PromocaoPublicoAlvoAtividadeDAO()

            Dim AtividadeSelecionadoListOLD As List(Of VO.PromocaoPublicoAlvoAtividade) = DAO.ListarAtividadesSelecionadas(CODPMC, TIPEDEPUBALVPMC)

            For Each item In PromocaoPublicoAlvoAtividadeList
                item.CODPMC = CODPMC
                item.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
                If Not Exists(AtividadeSelecionadoListOLD, item) Then
                    MyBase.InsertT(item, False)
                End If
            Next

            For Each item In AtividadeSelecionadoListOLD
                If Not Exists(PromocaoPublicoAlvoAtividadeList, item) Then
                    MyBase.DeleteT(item, False)
                End If
            Next


            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 4})
            If (PromocaoPublicoAlvoAtividadeList.Count = 0 AndAlso promocaoPublicoAlvoSelecao IsNot Nothing) Then
                promocaoPublicoAlvoSelecaoBO.DeleteT(promocaoPublicoAlvoSelecao, False)
            ElseIf (PromocaoPublicoAlvoAtividadeList.Count > 0 AndAlso promocaoPublicoAlvoSelecao Is Nothing) Then
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 4}, False)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Public Function BuscarAtividadePorBU(ByVal CODUNDESRNGC As List(Of VO.PromocaoPublicoAlvoAtividade)) As List(Of VO.PromocaoPublicoAlvoAtividade)
        Return DirectCast(daoBase, PromocaoPublicoAlvoAtividadeDAO).BuscarAtividadePorBU(CODUNDESRNGC)
    End Function

    Private Function Exists(ByRef pList As List(Of VO.PromocaoPublicoAlvoAtividade), ByVal pObj As VO.PromocaoPublicoAlvoAtividade) As Boolean
        Return pList.Where(Function(r) r.CODPMC = pObj.CODPMC And r.TIPEDEPUBALVPMC = pObj.TIPEDEPUBALVPMC And r.CODATI = pObj.CODATI).FirstOrDefault() IsNot Nothing
    End Function
End Class
