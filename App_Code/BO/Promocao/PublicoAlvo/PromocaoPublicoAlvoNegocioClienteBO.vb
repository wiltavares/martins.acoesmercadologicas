﻿Imports Core

Public Class PromocaoPublicoAlvoNegocioClienteBO
    Inherits BOBase(Of VO.PromocaoPublicoAlvoNegocioCliente)

    Public Sub New()
        MyBase.daoBase = New PromocaoPublicoAlvoNegocioClienteDAO()
    End Sub

    Public Function ListarNegocioCliente(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoNegocioCliente)
        Return DirectCast(daoBase, PromocaoPublicoAlvoNegocioClienteDAO).ListarTipoNegocioCliente(CODPMC, TIPEDEPUBALVPMC)
    End Function

    Public Sub InsertList(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal PromocaoPublicoAlvoNegocioClienteList As List(Of VO.PromocaoPublicoAlvoNegocioCliente))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As PromocaoPublicoAlvoNegocioClienteDAO = New PromocaoPublicoAlvoNegocioClienteDAO()

            Dim NegocioClienteSelecionadoListOLD As List(Of VO.PromocaoPublicoAlvoNegocioCliente) = DAO.ListarTipoNegocioClienteSelecionado(CODPMC, TIPEDEPUBALVPMC)

            For Each item In PromocaoPublicoAlvoNegocioClienteList
                item.CODPMC = CODPMC
                item.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
                If Not Exists(NegocioClienteSelecionadoListOLD, item) Then
                    MyBase.InsertT(item, False)
                End If
            Next

            For Each item In NegocioClienteSelecionadoListOLD
                If Not Exists(PromocaoPublicoAlvoNegocioClienteList, item) Then
                    MyBase.DeleteT(item, False)
                End If
            Next

            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 7})
            If (PromocaoPublicoAlvoNegocioClienteList.Count = 0 AndAlso promocaoPublicoAlvoSelecao IsNot Nothing) Then
                promocaoPublicoAlvoSelecaoBO.DeleteT(promocaoPublicoAlvoSelecao, False)
            ElseIf (PromocaoPublicoAlvoNegocioClienteList.Count > 0 AndAlso promocaoPublicoAlvoSelecao Is Nothing) Then
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 7}, False)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Private Function Exists(ByRef pList As List(Of VO.PromocaoPublicoAlvoNegocioCliente), ByVal pObj As VO.PromocaoPublicoAlvoNegocioCliente) As Boolean
        Return pList.Where(Function(r) r.CODPMC = pObj.CODPMC And r.TIPEDEPUBALVPMC = pObj.TIPEDEPUBALVPMC And r.CODUNDESRNGC = pObj.CODUNDESRNGC).FirstOrDefault() IsNot Nothing
    End Function
End Class
