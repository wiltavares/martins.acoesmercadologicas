﻿Imports Core

Public Class PromocaoPublicoAlvoGrupamentoClienteBO
    Inherits BOBase(Of VO.PromocaoPublicoAlvoGrupamentoCliente)

    Public Sub New()
        MyBase.daoBase = New PromocaoPublicoAlvoGrupamentoClienteDAO()
    End Sub

    Public Function BuscarTipoGrupamentoCliente() As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente)
        Return DirectCast(daoBase, PromocaoPublicoAlvoGrupamentoClienteDAO).BuscarTipoGrupamentoCliente()
    End Function

    Public Function ListarGrupamentoCliente(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente)
        Return DirectCast(daoBase, PromocaoPublicoAlvoGrupamentoClienteDAO).ListarGrupamentoCliente(CODPMC, TIPEDEPUBALVPMC)
    End Function

    Public Sub InsertList(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal PromocaoPublicoAlvoGrupamentoClienteList As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As PromocaoPublicoAlvoGrupamentoClienteDAO = New PromocaoPublicoAlvoGrupamentoClienteDAO()

            Dim GrupamentoClienteSelecionadoListOLD As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente) = DAO.ListarGrupamentoClienteSelecionadas(CODPMC, TIPEDEPUBALVPMC)

            For Each item In PromocaoPublicoAlvoGrupamentoClienteList
                item.CODPMC = CODPMC
                item.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
                If Not Exists(GrupamentoClienteSelecionadoListOLD, item) Then
                    MyBase.InsertT(item, False)
                End If
            Next

            For Each item In GrupamentoClienteSelecionadoListOLD
                If Not Exists(PromocaoPublicoAlvoGrupamentoClienteList, item) Then
                    MyBase.DeleteT(item, False)
                End If
            Next

            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 5})
            If (PromocaoPublicoAlvoGrupamentoClienteList.Count = 0 AndAlso promocaoPublicoAlvoSelecao IsNot Nothing) Then
                promocaoPublicoAlvoSelecaoBO.DeleteT(promocaoPublicoAlvoSelecao, False)
            ElseIf (PromocaoPublicoAlvoGrupamentoClienteList.Count > 0 AndAlso promocaoPublicoAlvoSelecao Is Nothing) Then
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 5}, False)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Public Function BuscarGrupamentoClientePorBU(ByVal CODUNDESRNGC As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente)) As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente)
        Return DirectCast(daoBase, PromocaoPublicoAlvoGrupamentoClienteDAO).BuscarGrupamentoClientePorBU(CODUNDESRNGC)
    End Function

    Private Function Exists(ByRef pList As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente), ByVal pObj As VO.PromocaoPublicoAlvoGrupamentoCliente) As Boolean
        Return pList.Where(Function(r) r.CODPMC = pObj.CODPMC And r.TIPEDEPUBALVPMC = pObj.TIPEDEPUBALVPMC And r.CODGRPCLI = pObj.CODGRPCLI).FirstOrDefault() IsNot Nothing
    End Function
End Class
