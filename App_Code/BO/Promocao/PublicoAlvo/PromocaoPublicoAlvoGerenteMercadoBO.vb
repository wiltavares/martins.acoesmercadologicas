﻿Imports Core
Imports NPOI.HSSF.Extractor
Imports NPOI.HSSF.UserModel

Public Class PromocaoPublicoAlvoGerenteMercadoBO
    Inherits BOBase(Of VO.PromocaoPublicoAlvoGerenteMercado)

    Public Sub New()
        MyBase.daoBase = New PromocaoPublicoAlvoGerenteMercadoDAO()
    End Sub

    Public Function BuscarListaGerenteMercado(vo As VO.PromocaoPublicoAlvoGerenteMercado) As List(Of VO.PromocaoPublicoAlvoGerenteMercado)
        Return DirectCast(daoBase, PromocaoPublicoAlvoGerenteMercadoDAO).ListarGerenteMercado(vo)
    End Function

    Public Function ListarPublicoAlvoGerenteMercado(vo As VO.PromocaoPublicoAlvoGerenteMercado) As List(Of VO.PromocaoPublicoAlvoGerenteMercado)
        Return DirectCast(daoBase, PromocaoPublicoAlvoGerenteMercadoDAO).ListarPublicoAlvoGerenteMercado(vo)
    End Function



    Public Sub Insert(promocaoPublicoAlvoGerenteMercado As VO.PromocaoPublicoAlvoGerenteMercado)
        If (DirectCast(daoBase, PromocaoPublicoAlvoGerenteMercadoDAO).SelectT(promocaoPublicoAlvoGerenteMercado) Is Nothing) Then
            MyBase.daoBase.connector.BeginTransaction()
            Try

                Me.InsertT(promocaoPublicoAlvoGerenteMercado, False)

                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocaoPublicoAlvoGerenteMercado.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoGerenteMercado.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 3})
                If (promocaoPublicoAlvoSelecao Is Nothing) Then
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocaoPublicoAlvoGerenteMercado.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoGerenteMercado.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 3}, False)
                End If

                MyBase.daoBase.connector.Commit()

            Catch ex As Exception
                MyBase.daoBase.connector.Rollback()
                Throw ex
            End Try
        End If
    End Sub


    Public Sub AddPesquisa(ByRef filtro As VO.PromocaoPublicoAlvoGerenteMercado)
        MyBase.daoBase.connector.BeginTransaction()
        Try
            Dim voList As List(Of VO.PromocaoPublicoAlvoGerenteMercado) = Me.BuscarListaGerenteMercado(filtro)
            Dim adicionados As List(Of VO.PromocaoPublicoAlvoGerenteMercado) = Me.ListarPublicoAlvoGerenteMercado(New VO.PromocaoPublicoAlvoGerenteMercado With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC})
            For Each newItem In voList
                If (adicionados.Where(Function(f) f.CODSUP = newItem.CODSUP).Count() = 0) Then
                    If (Not (newItem.CODSUP = 0)) Then
                        'Atribuir dados no objeto
                        newItem.CODPMC = filtro.CODPMC
                        newItem.TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC
                        InsertT(newItem, False)
                    End If
                End If
            Next


            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 3})
            If (promocaoPublicoAlvoSelecao Is Nothing) Then
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 3}, False)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Public Sub Delete(promocaoPublicoAlvoGerenteMercado As VO.PromocaoPublicoAlvoGerenteMercado)
        MyBase.daoBase.connector.BeginTransaction()
        Try
            DirectCast(daoBase, PromocaoPublicoAlvoGerenteMercadoDAO).Delete(promocaoPublicoAlvoGerenteMercado)

            If (promocaoPublicoAlvoGerenteMercado.CODSUP Is Nothing Or
                    Me.ListT(New VO.PromocaoPublicoAlvoGerenteMercado() With {.CODPMC = promocaoPublicoAlvoGerenteMercado.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoGerenteMercado.TIPEDEPUBALVPMC}).Where(Function(f) f.CODSUP <> promocaoPublicoAlvoGerenteMercado.CODSUP.GetValueOrDefault()).Count = 0) Then

                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.DeleteT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocaoPublicoAlvoGerenteMercado.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoGerenteMercado.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 3}, False)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub


    Public Function ImportarExelGerenteMercado(path As String, ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
        MyBase.daoBase.connector.BeginTransaction()
        Try
            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.PromocaoPublicoAlvoGerenteMercado) = ListaExcel(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos de gerentes de mercado para importar"
            End If



            Dim erros As New StringBuilder()

            Dim GerenteMercadoAdicionado As List(Of VO.PromocaoPublicoAlvoGerenteMercado) = Me.ListarPublicoAlvoGerenteMercado(New VO.PromocaoPublicoAlvoGerenteMercado With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})

            For Each novoGerenteMercado In dados

                Dim relacaoAcaoMecanicaPublicoAlvoGerenteMercadoDAO As New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoDAO()
                Dim verificaSeGerenteMercadoExiste As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado) = relacaoAcaoMecanicaPublicoAlvoGerenteMercadoDAO.VerificaSeGerenteMercadoExiste(novoGerenteMercado.CODSUP)
                If (verificaSeGerenteMercadoExiste.Count <= 0) Then
                    erros.Append("Linha ").Append(novoGerenteMercado.CODSUP).Append(" Gerente Mercado não cadastrado.").AppendLine()
                    Continue For
                End If

                'verificar se Gerente Mercado existe no cadastro de Gerente de Mercado
                Dim verificaGerenteMercadoJaAdicionado As List(Of VO.PromocaoPublicoAlvoGerenteMercado) = GerenteMercadoAdicionado.Where(Function(f) f.CODSUP = novoGerenteMercado.CODSUP).ToList
                If (verificaGerenteMercadoJaAdicionado.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                    erros.Append("Linha ").Append(novoGerenteMercado.CODSUP).Append(" Gerente Mercado já adicionado.").AppendLine()
                    Continue For
                End If

                Dim verificaGerenteMercadoDuplicadoXLS As List(Of VO.PromocaoPublicoAlvoGerenteMercado) = dados.Where(Function(f) f.CODSUP = novoGerenteMercado.CODSUP AndAlso f.CODPMC IsNot Nothing).ToList
                If (verificaGerenteMercadoDuplicadoXLS.Count > 0) Then
                    erros.Append("Linha ").Append(novoGerenteMercado.CODSUP).Append(" Gerente Mercado duplicado na planilha.").AppendLine()
                    Continue For
                End If

                Try
                    If (Not (novoGerenteMercado.CODSUP = 0)) Then
                        novoGerenteMercado.CODPMC = CODPMC
                        novoGerenteMercado.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
                        InsertT(novoGerenteMercado)
                    End If
                Catch ex As Exception
                    erros.Append("Linha ").Append(novoGerenteMercado.CODSUP).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                    Continue For
                End Try
            Next
            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 3})
            If (promocaoPublicoAlvoSelecao Is Nothing) Then
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 3}, False)
            End If

            MyBase.daoBase.connector.Commit()
            Return erros.ToString()
        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Return ex.Message
        End Try
    End Function

    Public Function ListaExcel(path As String, ByRef erros As String) As List(Of VO.PromocaoPublicoAlvoGerenteMercado)
        Dim linhasImportadas As New List(Of VO.PromocaoPublicoAlvoGerenteMercado)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (Not (alinha.Count = 1 Or alinha.Count = 2)) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 1 coluna de dados sendo a coluna ""Codigo do Gerente de Mercado"" opcionalmente voce pode informar a coluna ""Nome do Gerente de Mercado"", esta informação é apenas informativa", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo do Gerente de Mercado") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo do Gerente de Mercado""", NumLinha)
                    Return Nothing
                End If
                If (alinha.Count = 2 AndAlso alinha(1).Replace("""", "") <> "Nome do Gerente de Mercado") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Nome do Gerente de Mercado""", NumLinha)
                    Return Nothing
                End If

            Else ' Linhas de dados
                Dim novaLinha As New VO.PromocaoPublicoAlvoGerenteMercado
                Dim CODSUP As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODSUP)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo do Gerente de Mercado"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If
                novaLinha.CODSUP = CODSUP

                If (alinha.Count = 2) Then
                    novaLinha.NOMSUP = alinha(1).Replace("""", "")
                End If

                linhasImportadas.Add(novaLinha)
            End If
            NumLinha += 1
        Next

        Return linhasImportadas
    End Function

End Class
