﻿Imports Core

Public Class PromocaoPublicoAlvoSegmentoClienteBO
    Inherits BOBase(Of VO.PromocaoPublicoAlvoSegmentoCliente)

    Public Sub New()
        MyBase.daoBase = New PromocaoPublicoAlvoSegmentoClienteDAO()
    End Sub


    Public Function ListaTipoSegmentoCliente(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoSegmentoCliente)
        Return DirectCast(daoBase, PromocaoPublicoAlvoSegmentoClienteDAO).ListaTipoSegmentoCliente(CODPMC, TIPEDEPUBALVPMC)
    End Function

    Public Sub InsertList(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal PromocaoPublicoAlvoSegmentoClienteList As List(Of VO.PromocaoPublicoAlvoSegmentoCliente))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As PromocaoPublicoAlvoSegmentoClienteDAO = New PromocaoPublicoAlvoSegmentoClienteDAO()

            Dim SegmentoClienteSelecionadoListOLD As List(Of VO.PromocaoPublicoAlvoSegmentoCliente) = DAO.ListaTipoSegmentoClienteSelecionado(CODPMC, TIPEDEPUBALVPMC)

            For Each item In PromocaoPublicoAlvoSegmentoClienteList
                item.CODPMC = CODPMC
                item.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
                If Not Exists(SegmentoClienteSelecionadoListOLD, item) Then
                    MyBase.InsertT(item, False)
                End If
            Next

            For Each item In SegmentoClienteSelecionadoListOLD
                If Not Exists(PromocaoPublicoAlvoSegmentoClienteList, item) Then
                    MyBase.DeleteT(item, False)
                End If
            Next

            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 8})
            If (PromocaoPublicoAlvoSegmentoClienteList.Count = 0 AndAlso promocaoPublicoAlvoSelecao IsNot Nothing) Then
                promocaoPublicoAlvoSelecaoBO.DeleteT(promocaoPublicoAlvoSelecao, False)
            ElseIf (PromocaoPublicoAlvoSegmentoClienteList.Count > 0 AndAlso promocaoPublicoAlvoSelecao Is Nothing) Then
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 8}, False)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Private Function Exists(ByRef pList As List(Of VO.PromocaoPublicoAlvoSegmentoCliente), ByVal pObj As VO.PromocaoPublicoAlvoSegmentoCliente) As Boolean
        Return pList.Where(Function(r) r.CODPMC = pObj.CODPMC And r.TIPEDEPUBALVPMC = pObj.TIPEDEPUBALVPMC And r.CODSGMNGCCLI = pObj.CODSGMNGCCLI).FirstOrDefault() IsNot Nothing
    End Function
End Class
