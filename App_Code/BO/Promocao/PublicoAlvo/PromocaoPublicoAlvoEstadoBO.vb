﻿Imports AutoMapper
Imports Core

Public Class PromocaoPublicoAlvoEstadoBO
    Inherits BOBase(Of VO.PromocaoPublicoAlvoEstado)

    Public Sub New()
        MyBase.daoBase = New PromocaoPublicoAlvoEstadoDAO()
    End Sub

    Public Function ListarEstadoUniao(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoEstado)
        Return DirectCast(daoBase, PromocaoPublicoAlvoEstadoDAO).ListarEstadoUniao(CODPMC, TIPEDEPUBALVPMC)
    End Function

    Public Sub InsertList(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal PromocaoPublicoAlvoEstadoList As List(Of VO.PromocaoPublicoAlvoEstado))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As PromocaoPublicoAlvoEstadoDAO = New PromocaoPublicoAlvoEstadoDAO()

            Dim EstadoSelecionadoListOLD As List(Of VO.PromocaoPublicoAlvoEstado) = DAO.ListarEstadoUniaoSelecionados(CODPMC, TIPEDEPUBALVPMC)

            For Each item In PromocaoPublicoAlvoEstadoList
                item.CODPMC = CODPMC
                item.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
                If Not Exists(EstadoSelecionadoListOLD, item) Then
                    MyBase.InsertT(item, False)
                End If
            Next

            For Each item In EstadoSelecionadoListOLD
                If Not Exists(PromocaoPublicoAlvoEstadoList, item) Then
                    MyBase.DeleteT(item, False)
                End If
            Next

            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 1})
            If (PromocaoPublicoAlvoEstadoList.Count = 0 AndAlso promocaoPublicoAlvoSelecao IsNot Nothing) Then
                promocaoPublicoAlvoSelecaoBO.DeleteT(promocaoPublicoAlvoSelecao, False)
            ElseIf (PromocaoPublicoAlvoEstadoList.Count > 0 AndAlso promocaoPublicoAlvoSelecao Is Nothing) Then
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 1}, False)
            End If


            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Private Function Exists(ByRef pList As List(Of VO.PromocaoPublicoAlvoEstado), ByVal pObj As VO.PromocaoPublicoAlvoEstado) As Boolean
        Return pList.Where(Function(r) r.CODPMC = pObj.CODPMC And r.TIPEDEPUBALVPMC = pObj.TIPEDEPUBALVPMC And r.CODESTUNI = pObj.CODESTUNI).FirstOrDefault() IsNot Nothing
    End Function
End Class
