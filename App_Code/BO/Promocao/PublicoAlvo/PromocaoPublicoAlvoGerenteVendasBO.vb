﻿Imports AutoMapper
Imports Core

Public Class PromocaoPublicoAlvoGerenteVendasBO
    Inherits BOBase(Of VO.PromocaoPublicoAlvoGerenteVendas)

    Public Sub New()
        MyBase.daoBase = New PromocaoPublicoAlvoGerenteVendasDAO()
    End Sub

    Public Function ListarGerenteVendas(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoGerenteVendas)
        Return DirectCast(daoBase, PromocaoPublicoAlvoGerenteVendasDAO).ListarGerenteVendas(CODPMC, TIPEDEPUBALVPMC)
    End Function

    Public Sub InsertList(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal PromocaoPublicoAlvoGerenteVendasList As List(Of VO.PromocaoPublicoAlvoGerenteVendas))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As PromocaoPublicoAlvoGerenteVendasDAO = New PromocaoPublicoAlvoGerenteVendasDAO()

            Dim gerenteVendasSelecionadoListOLD As List(Of VO.PromocaoPublicoAlvoGerenteVendas) = DAO.ListarGerenteVendasSelecionados(CODPMC, TIPEDEPUBALVPMC)

            For Each item In PromocaoPublicoAlvoGerenteVendasList
                item.CODPMC = CODPMC
                item.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
                If Not Exists(gerenteVendasSelecionadoListOLD, item) Then
                    MyBase.InsertT(item, False)
                End If
            Next

            For Each item In gerenteVendasSelecionadoListOLD
                If Not Exists(PromocaoPublicoAlvoGerenteVendasList, item) Then
                    MyBase.DeleteT(item, False)
                End If
            Next

            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 2})
            If (PromocaoPublicoAlvoGerenteVendasList.Count = 0 AndAlso promocaoPublicoAlvoSelecao IsNot Nothing) Then
                promocaoPublicoAlvoSelecaoBO.DeleteT(promocaoPublicoAlvoSelecao, False)
            ElseIf (PromocaoPublicoAlvoGerenteVendasList.Count > 0 AndAlso promocaoPublicoAlvoSelecao Is Nothing) Then
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 2}, False)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Private Function Exists(ByRef pList As List(Of VO.PromocaoPublicoAlvoGerenteVendas), ByVal pObj As VO.PromocaoPublicoAlvoGerenteVendas) As Boolean
        Return pList.Where(Function(r) r.CODPMC = pObj.CODPMC And r.TIPEDEPUBALVPMC = pObj.TIPEDEPUBALVPMC And r.CODGER = pObj.CODGER).FirstOrDefault() IsNot Nothing
    End Function
End Class
