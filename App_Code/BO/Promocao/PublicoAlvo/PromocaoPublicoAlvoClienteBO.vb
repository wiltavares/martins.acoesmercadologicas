﻿Imports System.Runtime.CompilerServices
Imports Core
Imports NPOI.HSSF.Extractor
Imports NPOI.HSSF.UserModel

Public Class PromocaoPublicoAlvoClienteBO
    Inherits BOBase(Of VO.PromocaoPublicoAlvoCliente)

    Public Sub New()
        MyBase.daoBase = New PromocaoPublicoAlvoClienteDAO()
    End Sub

    Public Function BuscarListaCliente(vo As VO.PromocaoPublicoAlvoCliente) As List(Of VO.PromocaoPublicoAlvoCliente)
        Return DirectCast(daoBase, PromocaoPublicoAlvoClienteDAO).BuscarListaCliente(vo)
    End Function

    Public Function ListarPublicoAlvoCliente(vo As VO.PromocaoPublicoAlvoCliente) As List(Of VO.PromocaoPublicoAlvoCliente)
        Return DirectCast(daoBase, PromocaoPublicoAlvoClienteDAO).ListarPublicoAlvoCliente(vo)
    End Function


    Public Sub Insert(promocaoPublicoAlvoCliente As VO.PromocaoPublicoAlvoCliente)
        If (DirectCast(daoBase, PromocaoPublicoAlvoClienteDAO).SelectT(promocaoPublicoAlvoCliente) Is Nothing) Then
            MyBase.daoBase.connector.BeginTransaction()
            Try

                Me.InsertT(promocaoPublicoAlvoCliente, False)

                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocaoPublicoAlvoCliente.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoCliente.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 9})
                If (promocaoPublicoAlvoSelecao Is Nothing) Then
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocaoPublicoAlvoCliente.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoCliente.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 9}, False)
                End If

                MyBase.daoBase.connector.Commit()

            Catch ex As Exception
                MyBase.daoBase.connector.Rollback()
                Throw ex
            End Try
        End If
    End Sub



    Public Sub AddPesquisa(ByRef filtro As VO.PromocaoPublicoAlvoCliente)

        Dim voList As List(Of VO.PromocaoPublicoAlvoCliente) = Me.BuscarListaCliente(filtro)
        Dim adicionados As List(Of VO.PromocaoPublicoAlvoCliente) = Me.ListarPublicoAlvoCliente(New VO.PromocaoPublicoAlvoCliente With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC})

        If (voList.Count > 30000) Then
            Throw New Exception(String.Format("Esta consulta resuta em {0} registros, somente e permitido inserir 30000 por vez. Verifique os filtros", voList.Count))
        End If

        MyBase.daoBase.connector.BeginTransaction()
        Try


            For Each newItem In voList
                If (adicionados.Where(Function(f) f.CODCLI = newItem.CODCLI).Count() = 0) Then
                    If (Not (newItem.CODCLI = 0)) Then
                        'Atribuir dados no objeto
                        newItem.CODPMC = filtro.CODPMC
                        newItem.TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC
                        InsertT(newItem, False)
                    End If
                End If
            Next

            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 9})
            If (promocaoPublicoAlvoSelecao Is Nothing) Then
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 9}, False)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Public Sub Delete(promocaoPublicoAlvoCliente As VO.PromocaoPublicoAlvoCliente)
        MyBase.daoBase.connector.BeginTransaction()
        Try
            DirectCast(daoBase, PromocaoPublicoAlvoClienteDAO).Delete(promocaoPublicoAlvoCliente)

            If (promocaoPublicoAlvoCliente.CODCLI Is Nothing Or
                    Me.ListT(New VO.PromocaoPublicoAlvoCliente() With {.CODPMC = promocaoPublicoAlvoCliente.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoCliente.TIPEDEPUBALVPMC}).Where(Function(f) f.CODCLI <> promocaoPublicoAlvoCliente.CODCLI.GetValueOrDefault()).Count = 0) Then

                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.DeleteT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocaoPublicoAlvoCliente.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoCliente.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 9}, False)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub



    Public Function ImportarExelCliente(path As String, ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.PromocaoPublicoAlvoCliente) = ListaExcel(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            Dim erros As New StringBuilder()

            Dim clienteAdicionado As List(Of VO.PromocaoPublicoAlvoCliente) = Me.ListarPublicoAlvoCliente(New VO.PromocaoPublicoAlvoCliente With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})

            If IsNothing(dados) Or dados.Count = 0 Then
                erros.AppendLine("Este arquivo não possui uma relação de codigos de clientes para importar")
            ElseIf dados.Count > 30000 Then
                erros.AppendLine("Este arquivo possui mais de 30000 codigos de clientes para importar")
            Else

                For Each novoCliente In dados

                    Dim relacaoAcaoMecanicaPublicoAlvoClienteDAO As New RelacaoAcaoMecanicaPublicoAlvoClienteDAO()
                    Dim verificaSeClienteExiste As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente) = relacaoAcaoMecanicaPublicoAlvoClienteDAO.VerificaSeClienteExiste(novoCliente.CODCLI)
                    If (verificaSeClienteExiste.Count <= 0) Then
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).AppendLine(" Não foi encontrado cliente com este código")
                        Continue For
                    End If

                    Dim verificaClienteJaAdicionado As List(Of VO.PromocaoPublicoAlvoCliente) = clienteAdicionado.Where(Function(f) f.CODCLI = novoCliente.CODCLI).ToList
                    If (verificaClienteJaAdicionado.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).AppendLine(" Cliente já adicionado.")
                        Continue For
                    End If

                    Dim verificaClienteDuplicadoXLS As List(Of VO.PromocaoPublicoAlvoCliente) = dados.Where(Function(f) f.CODCLI = novoCliente.CODCLI AndAlso f.CODPMC IsNot Nothing).ToList
                    If (verificaClienteDuplicadoXLS.Count > 0) Then
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).AppendLine(" Cliente duplicado na planilha.")
                        Continue For
                    End If

                    Try
                        If (Not (novoCliente.CODCLI = 0)) Then
                            novoCliente.CODPMC = CODPMC
                            novoCliente.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
                            InsertT(novoCliente)
                        End If
                    Catch ex As Exception
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                        Continue For
                    End Try

                Next

                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 9})
                If (promocaoPublicoAlvoSelecao Is Nothing) Then
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 9}, False)
                End If

            End If
            MyBase.daoBase.connector.Commit()

            Return erros.ToString()
        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Return ex.Message
        End Try
    End Function


    Public Function ListaExcel(path As String, ByRef erros As String) As List(Of VO.PromocaoPublicoAlvoCliente)
        Dim mercadoriasImportadas As New List(Of VO.PromocaoPublicoAlvoCliente)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (Not (alinha.Count = 1 Or alinha.Count = 2)) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 1 coluna de dados sendo a coluna ""Codigo do Cliente"" opcionalmente voce pode informar a coluna ""Nome do Cliente"", esta informação é apenas informativa", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo do Cliente") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo do Cliente""", NumLinha)
                    Return Nothing
                End If
                If (alinha.Count = 2 AndAlso alinha(1).Replace("""", "") <> "Nome do Cliente") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Nome do Cliente""", NumLinha)
                    Return Nothing
                End If

            Else ' Linhas de dados
                Dim novoItem As New VO.PromocaoPublicoAlvoCliente
                Dim CODCLI As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODCLI)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo do Cliente"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If
                novoItem.CODCLI = CODCLI

                If (alinha.Count = 2) Then
                    novoItem.NOMCLI = alinha(1).Replace("""", "")
                End If

                mercadoriasImportadas.Add(novoItem)
            End If
            NumLinha += 1
        Next

        Return mercadoriasImportadas
    End Function

End Class
