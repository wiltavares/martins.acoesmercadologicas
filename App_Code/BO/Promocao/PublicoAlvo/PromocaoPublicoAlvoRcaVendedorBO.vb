﻿Imports Core
Imports NPOI.HSSF.Extractor
Imports NPOI.HSSF.UserModel

Public Class PromocaoPublicoAlvoRcaVendedorBO
    Inherits BOBase(Of VO.PromocaoPublicoAlvoRcaVendedor)

    Public Sub New()
        MyBase.daoBase = New PromocaoPublicoAlvoRcaVendedorDAO()
    End Sub

    Public Function BuscarListaRcaVendedor(vo As VO.PromocaoPublicoAlvoRcaVendedor) As List(Of VO.PromocaoPublicoAlvoRcaVendedor)
        Return DirectCast(daoBase, PromocaoPublicoAlvoRcaVendedorDAO).BuscarListaRcaVendedor(vo)
    End Function

    Public Function ListarPublicoAlvoRcaVendedor(vo As VO.PromocaoPublicoAlvoRcaVendedor) As List(Of VO.PromocaoPublicoAlvoRcaVendedor)
        Return DirectCast(daoBase, PromocaoPublicoAlvoRcaVendedorDAO).ListarPublicoAlvoRcaVendedor(vo)
    End Function




    Public Sub Insert(promocaoPublicoAlvoRcaVendedor As VO.PromocaoPublicoAlvoRcaVendedor)
        If (DirectCast(daoBase, PromocaoPublicoAlvoRcaVendedorDAO).SelectT(promocaoPublicoAlvoRcaVendedor) Is Nothing) Then
            MyBase.daoBase.connector.BeginTransaction()
            Try

                Me.InsertT(promocaoPublicoAlvoRcaVendedor, False)

                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocaoPublicoAlvoRcaVendedor.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoRcaVendedor.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 10})
                If (promocaoPublicoAlvoSelecao Is Nothing) Then
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocaoPublicoAlvoRcaVendedor.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoRcaVendedor.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 10}, False)
                End If

                MyBase.daoBase.connector.Commit()

            Catch ex As Exception
                MyBase.daoBase.connector.Rollback()
                Throw ex
            End Try
        End If
    End Sub


    Public Sub AddPesquisa(ByRef filtro As VO.PromocaoPublicoAlvoRcaVendedor)
        MyBase.daoBase.connector.BeginTransaction()
        Try
            Dim voList As List(Of VO.PromocaoPublicoAlvoRcaVendedor) = Me.BuscarListaRcaVendedor(filtro)
            Dim adicionados As List(Of VO.PromocaoPublicoAlvoRcaVendedor) = Me.ListarPublicoAlvoRcaVendedor(New VO.PromocaoPublicoAlvoRcaVendedor With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC})
            For Each newItem In voList
                If (adicionados.Where(Function(f) f.CODREP = newItem.CODREP).Count() = 0) Then
                    If (Not (newItem.CODREP = 0)) Then
                        'Atribuir dados no objeto
                        newItem.CODPMC = filtro.CODPMC
                        newItem.TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC
                        InsertT(newItem, False)
                    End If
                End If
            Next


            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 10})
            If (promocaoPublicoAlvoSelecao Is Nothing) Then
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 10}, False)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub


    Public Sub Delete(promocaoPublicoAlvoRcaVendedor As VO.PromocaoPublicoAlvoRcaVendedor)
        MyBase.daoBase.connector.BeginTransaction()
        Try
            DirectCast(daoBase, PromocaoPublicoAlvoRcaVendedorDAO).Delete(promocaoPublicoAlvoRcaVendedor)

            If (promocaoPublicoAlvoRcaVendedor.CODREP Is Nothing Or
                    Me.ListT(New VO.PromocaoPublicoAlvoRcaVendedor() With {.CODPMC = promocaoPublicoAlvoRcaVendedor.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoRcaVendedor.TIPEDEPUBALVPMC}).Where(Function(f) f.CODREP <> promocaoPublicoAlvoRcaVendedor.CODREP.GetValueOrDefault()).Count = 0) Then
                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.DeleteT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocaoPublicoAlvoRcaVendedor.CODPMC, .TIPEDEPUBALVPMC = promocaoPublicoAlvoRcaVendedor.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 10}, False)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub


    Public Function ImportarExelRcaVendedor(path As String, ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
        MyBase.daoBase.connector.BeginTransaction()
        Try
            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.PromocaoPublicoAlvoRcaVendedor) = ListaExcel(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos de RCA/Vendendores para importar"
            End If


            Dim erros As New StringBuilder()

            Dim RcaVendedorAdicionado As List(Of VO.PromocaoPublicoAlvoRcaVendedor) = Me.ListarPublicoAlvoRcaVendedor(New VO.PromocaoPublicoAlvoRcaVendedor With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})

            For Each novoRcaVendedor In dados

                Dim relacaoAcaoMecanicaPublicoAlvoRcaVendedorDAO As New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorDAO()
                Dim verificaSeRcaVendedorExiste As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) = relacaoAcaoMecanicaPublicoAlvoRcaVendedorDAO.VerificaSeRcaVendedorExiste(novoRcaVendedor.CODREP)
                If (verificaSeRcaVendedorExiste.Count <= 0) Then
                    erros.Append("Linha ").Append(novoRcaVendedor.CODREP).Append(": RCA/Vendedor não cadastrado.").AppendLine()
                    Continue For
                End If


                'verificar se cliente existe no cadastro de cliente
                Dim verificaRcaVendedorJaAdicionado As List(Of VO.PromocaoPublicoAlvoRcaVendedor) = RcaVendedorAdicionado.Where(Function(f) f.CODREP = novoRcaVendedor.CODREP).ToList
                If (verificaRcaVendedorJaAdicionado.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                    erros.Append("Linha ").Append(novoRcaVendedor.CODREP).Append(": RCA/Vendedor já adicionado.").AppendLine()
                    Continue For
                End If

                Dim verificaRcaVendedorDuplicadoXLS As List(Of VO.PromocaoPublicoAlvoRcaVendedor) = dados.Where(Function(f) f.CODREP = novoRcaVendedor.CODREP AndAlso f.CODPMC IsNot Nothing).ToList
                If (verificaRcaVendedorDuplicadoXLS.Count > 0) Then
                    erros.Append("Linha: ").Append(novoRcaVendedor.CODREP).Append(": RCA/Vendedor duplicado na planilha.").AppendLine()
                    Continue For
                End If

                Try
                    If (Not (novoRcaVendedor.CODREP = 0)) Then
                        novoRcaVendedor.CODPMC = CODPMC
                        novoRcaVendedor.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
                        InsertT(novoRcaVendedor)
                    End If
                Catch ex As Exception
                    erros.Append("Linha: ").Append(novoRcaVendedor.CODREP).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                    Continue For
                End Try

            Next


            Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
            Dim promocaoPublicoAlvoSelecao = promocaoPublicoAlvoSelecaoBO.SelectT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 10})
            If (promocaoPublicoAlvoSelecao Is Nothing) Then
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 10}, False)
            End If

            MyBase.daoBase.connector.Commit()

            Return erros.ToString()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Return ex.Message
        End Try
    End Function

    Public Function ListaExcel(path As String, ByRef erros As String) As List(Of VO.PromocaoPublicoAlvoRcaVendedor)
        Dim linhasImportadas As New List(Of VO.PromocaoPublicoAlvoRcaVendedor)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (Not (alinha.Count = 1 Or alinha.Count = 2)) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 1 coluna de dados sendo a coluna ""Codigo do RCA/Vendedor"" opcionalmente voce pode informar a coluna ""Nome do RCA/Vendedor"", esta informação é apenas informativa", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo do RCA/Vendedor") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo do RCA/Vendedor""", NumLinha)
                    Return Nothing
                End If
                If (alinha.Count = 2 AndAlso alinha(1).Replace("""", "") <> "Nome do RCA/Vendedor") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Nome do RCA/Vendedor""", NumLinha)
                    Return Nothing
                End If

            Else ' Linhas de dados
                Dim novaLinha As New VO.PromocaoPublicoAlvoRcaVendedor
                Dim CODREP As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODREP)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo do RCA/Vendedor"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If
                novaLinha.CODREP = CODREP

                If (alinha.Count = 2) Then
                    novaLinha.NOMREP = alinha(1).Replace("""", "")
                End If

                linhasImportadas.Add(novaLinha)
            End If
            NumLinha += 1
        Next

        Return linhasImportadas
    End Function


End Class
