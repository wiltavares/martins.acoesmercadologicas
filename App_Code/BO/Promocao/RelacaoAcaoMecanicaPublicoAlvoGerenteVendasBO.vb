﻿Imports AutoMapper
Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoGerenteVendasBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaPublicoAlvoGerenteVendasDAO()
    End Sub

    Public Function ListarGerenteVendas(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoGerenteVendasDAO).ListarGerenteVendas(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Sub InsertList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal relacaoAcaoMecanicaPublicoAlvoGerenteVendasList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As RelacaoAcaoMecanicaPublicoAlvoGerenteVendasDAO = New RelacaoAcaoMecanicaPublicoAlvoGerenteVendasDAO()

            Dim gerenteVendasSelecionadoListOLD As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas) = DAO.ListarGerenteVendasSelecionados(CODACOCMC, NUMSEQRGRACOCMC)

            For Each item In relacaoAcaoMecanicaPublicoAlvoGerenteVendasList
                item.CODACOCMC = CODACOCMC
                item.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                If Not Exists(gerenteVendasSelecionadoListOLD, item) Then
                    MyBase.InsertT(item)
                End If

            Next

            For Each item In gerenteVendasSelecionadoListOLD
                If Not Exists(relacaoAcaoMecanicaPublicoAlvoGerenteVendasList, item) Then
                    MyBase.DeleteT(item)
                End If

            Next

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Private Function Exists(ByRef pList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas), ByVal pObj As VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas) As Boolean
        Return pList.Where(Function(r) r.CODACOCMC = pObj.CODACOCMC And r.NUMSEQRGRACOCMC = pObj.NUMSEQRGRACOCMC And r.CODGER = pObj.CODGER).FirstOrDefault() IsNot Nothing
    End Function
End Class
