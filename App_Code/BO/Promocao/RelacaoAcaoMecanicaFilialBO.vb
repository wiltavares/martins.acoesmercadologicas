﻿Imports Core

Public Class RelacaoAcaoMecanicaFilialBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaFilial)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaFilialDAO()
    End Sub


    Public Function ListFiliaisRelacaoMecanica(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaFilial)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaFilialDAO).ListFiliaisRelacaoMecanica(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Function ListarFiliais(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaFilial)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaFilialDAO).ListarFiliais(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Sub SaveList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal relacaoAcaoMecanicaFiliaislList As List(Of VO.RelacaoAcaoMecanicaFilial))

        Dim DAO = New RelacaoAcaoMecanicaFilialDAO()

        Dim FiliaisSelecionadasListOLD As List(Of VO.RelacaoAcaoMecanicaFilial) = DAO.ListarFiliaisSelecionadas(CODACOCMC, NUMSEQRGRACOCMC)

        For Each item In relacaoAcaoMecanicaFiliaislList
            item.CODACOCMC = CODACOCMC
            item.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
            If Not Exists(FiliaisSelecionadasListOLD, item) Then
                MyBase.daoBase.InsertT(item)
            End If
        Next

        For Each item In FiliaisSelecionadasListOLD
            If Not Exists(relacaoAcaoMecanicaFiliaislList, item) Then
                MyBase.daoBase.DeleteT(item)
            End If

        Next


    End Sub

    Private Function Exists(ByRef pList As List(Of VO.RelacaoAcaoMecanicaFilial), ByVal pObj As VO.RelacaoAcaoMecanicaFilial) As Boolean
        Return pList.Where(Function(r) r.CODACOCMC = pObj.CODACOCMC And r.NUMSEQRGRACOCMC = pObj.NUMSEQRGRACOCMC And r.CODFILEMP = pObj.CODFILEMP).FirstOrDefault() IsNot Nothing
    End Function
End Class
