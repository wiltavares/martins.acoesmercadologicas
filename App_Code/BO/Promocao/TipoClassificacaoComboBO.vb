﻿Imports Core

Public Class TipoClassificacaoComboBO
    Inherits BOBase(Of VO.TipoClassificacaoCombo)

    Public Sub New()
        MyBase.daoBase = New TipoClassificacaocomboDAO()
    End Sub

    Public Function CarregaDropTipoClassificacaoCombo(ByVal DESTIPNIVCLFCPT As String, ByVal TIPNIVCLFCPT As Decimal?) As List(Of VO.TipoClassificacaoCombo)
        Return DirectCast(daoBase, TipoClassificacaocomboDAO).CarregaDropTipoClassificacaoCombo(DESTIPNIVCLFCPT, TIPNIVCLFCPT)
    End Function
End Class
