﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Core

Public Class PromocaoBO
    Inherits BOBase(Of VO.Promocao)

    Public Sub New()
        MyBase.daoBase = New PromocaoDAO()
    End Sub

    Public Function DataInicioSemana() As List(Of VO.Promocao)
        Return DirectCast(daoBase, PromocaoDAO).DataInicioSemana()
    End Function

    Public Function DataReferenciaEDataFimAgendaDiariaLivroPrecos(promocaoBrinde As VO.Promocao) As List(Of VO.Promocao)
        Return DirectCast(daoBase, PromocaoDAO).DataReferenciaEDataFimAgendaDiariaLivroPrecos(promocaoBrinde)
    End Function

    Public Function CarregaPromocao() As List(Of VO.Promocao)
        Return DirectCast(daoBase, PromocaoDAO).CarregaPromocao()
    End Function

    Public Function ListaParaGrid(promocao As VO.Promocao) As List(Of VO.Promocao)
        Return DirectCast(daoBase, PromocaoDAO).ListaParaGrid(promocao)
    End Function

    Public Function VerificaQtdMinVndMaiorQtdPmc(promocao As VO.Promocao) As List(Of VO.Promocao)
        Return DirectCast(daoBase, PromocaoDAO).ListSelectVerificaQtdMinVndMaiorQtdPmc(promocao)
    End Function

    Public Function VerificaQtdPmcNaoMult(promocao As VO.Promocao) As List(Of VO.Promocao)
        Return DirectCast(daoBase, PromocaoDAO).ListSelectVerificaQtdPmcNaoMult(promocao)
    End Function

    Public Function VerificaQtdPmcNaoMultBrinde(promocao As VO.Promocao) As List(Of VO.Promocao)
        Return DirectCast(daoBase, PromocaoDAO).ListSelectVerificaQtdPmcNaoMultBrinde(promocao)
    End Function

    'Public Function VerificaQtdMinVndMenorQtdPmcCriterio(promocao As VO.Promocao) As List(Of VO.Promocao)
    '    Return DirectCast(daoBase, PromocaoDAO).ListSelectVerificaQtdMinVndMenorQtdPmcCriterio(promocao)
    'End Function

    Public Function VerificaQtdMinVndMenorQtdPmcBrinde(promocao As VO.Promocao) As List(Of VO.Promocao)
        Return DirectCast(daoBase, PromocaoDAO).ListSelectVerificaQtdMinVndMenorQtdPmc(promocao)
    End Function

    'Public Function VerificaQtdPmcNaoMultiploCriterio(promocao As VO.Promocao) As List(Of VO.Promocao)
    '    Return DirectCast(daoBase, PromocaoDAO).ListSelectVerificaQtdPmcNaoMultiploCriterio(promocao)
    'End Function


    ''' <summary>
    ''' Consulta para liberar promoção (MIX)
    ''' retorna os itens do mix com quantidade de venda nao multiplo
    ''' </summary>
    ''' <param name="CODPMC"></param>
    ''' <returns></returns>
    Public Function ListaMIXQtdPmcNaoMult(CODPMC As Decimal) As List(Of VO.Promocao)
        Return DirectCast(daoBase, PromocaoDAO).ListaMIXQtdPmcNaoMult(CODPMC)
    End Function

    ''' <summary>
    ''' Consulta para liberar promoção (MIX)
    ''' retorna os itens do mix com quantidade de venda menor que a quantidade minima
    ''' </summary>
    ''' <param name="CODPMC"></param>
    ''' <returns></returns>
    Public Function ListaMIXQtdMinVndMaiorQtdPmc(CODPMC As Decimal) As List(Of VO.Promocao)
        Return DirectCast(daoBase, PromocaoDAO).ListaMIXQtdMinVndMaiorQtdPmc(CODPMC)
    End Function



    Public Function UpdatePromocao(promocao As VO.Promocao, criterioAdicionalAvaliacaoPromocao As VO.CriterioAdicionalAvaliacaoPromocao) As VO.Promocao
        Try
            daoBase.connector.BeginTransaction()

            'obtem a promoção da forma como esta no banco de dados antes da edição.
            Dim promocaoOld = Me.SelectT(New VO.Promocao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC})


            'AtualizaDatasetT0132115
            'Se foi selecionado critério adicional insere ou atualiza / Se nao deleta

            Dim criterioAdicionalAvaliacaoPromocaoBO = New CriterioAdicionalAvaliacaoPromocaoBO()
            Dim criterioAdicionalAvaliacaoPromocaoOld = criterioAdicionalAvaliacaoPromocaoBO.SelectT(New VO.CriterioAdicionalAvaliacaoPromocao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC})
            If (Not criterioAdicionalAvaliacaoPromocaoOld Is Nothing) Then
                'se critério adicional estiver marcado como true recebe valores caso contrario recebe 0
                If (promocao.INDCTRADIAVLPMC = 1) Then
                    criterioAdicionalAvaliacaoPromocaoOld.FTRACRCTRAVLPMC = criterioAdicionalAvaliacaoPromocao.FTRACRCTRAVLPMC
                    criterioAdicionalAvaliacaoPromocaoOld.VLRLIMMAXCTRAVLPMC = criterioAdicionalAvaliacaoPromocao.VLRLIMMAXCTRAVLPMC
                    criterioAdicionalAvaliacaoPromocaoOld.QDEMNMITEPEDPMC = criterioAdicionalAvaliacaoPromocao.QDEMNMITEPEDPMC
                    criterioAdicionalAvaliacaoPromocaoBO.UpdateT(criterioAdicionalAvaliacaoPromocaoOld)
                Else
                    'Remove entidade
                    criterioAdicionalAvaliacaoPromocaoBO.DeleteT(New VO.CriterioAdicionalAvaliacaoPromocao() With {.CODPMC = promocao.CODPMC})
                End If
            Else
                If (promocao.INDCTRADIAVLPMC = 1) Then
                    'Insere entidade
                    If (promocao.TIPPMC = 0 Or promocao.TIPPMC = 2) Then
                        criterioAdicionalAvaliacaoPromocaoBO.InsertT(New VO.CriterioAdicionalAvaliacaoPromocao() With {.CODPMC = promocao.CODPMC,
                                    .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC,
                                    .CODCTRAVLPMC = 1,
                                    .FTRACRCTRAVLPMC = criterioAdicionalAvaliacaoPromocao.FTRACRCTRAVLPMC,
                                    .VLRLIMMAXCTRAVLPMC = criterioAdicionalAvaliacaoPromocao.VLRLIMMAXCTRAVLPMC,
                                    .QDEMNMITEPEDPMC = criterioAdicionalAvaliacaoPromocao.QDEMNMITEPEDPMC
                                })
                    Else
                        criterioAdicionalAvaliacaoPromocaoBO.InsertT(New VO.CriterioAdicionalAvaliacaoPromocao() With {.CODPMC = promocao.CODPMC,
                                    .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC,
                                    .CODCTRAVLPMC = 1,
                                    .FTRACRCTRAVLPMC = 0,
                                    .VLRLIMMAXCTRAVLPMC = 0,
                                    .QDEMNMITEPEDPMC = 0
                                })
                    End If
                End If
            End If



            'Tratativa brindes adicionais
            'Se indicador de brindes adicionais for zero, deletar na tabela T0108044 todas as linhas que tem INDISRBDEADI = 1
            If (promocao.INDISRBDEADI = 0) Then
                Dim promocaoBrindeBO As New PromocaoBrindeBO
                Dim brindesAdicionais = promocaoBrindeBO.ListT(New VO.PromocaoBrinde() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC, .INDISRBDEADI = 1})
                brindesAdicionais.ForEach(Sub(obj) promocaoBrindeBO.RemoveBrinde(obj, False))
            End If

            'Tratativa Cobrança de Brinde
            'Se indicador de cobrança de brinde for alterado deleta os brindes existentes.
            If (promocao.INDCOBBDEPMC <> promocaoOld.INDCOBBDEPMC) Then
                Dim promocaoBrindeBO As New PromocaoBrindeBO
                Dim brindesAdicionais = promocaoBrindeBO.ListT(New VO.PromocaoBrinde() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC})
                brindesAdicionais.ForEach(Sub(obj) promocaoBrindeBO.RemoveBrinde(obj, False))
            End If

            'Valida Premios
            'Não valida pois não há alteração de TIPPMC

            promocao = daoBase.UpdateT(promocao)
            daoBase.connector.Commit()

            Return promocao
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

    End Function

    Public Function Validar(ByVal promocao As PromocaoViewModel) As String

        Dim promocaoBO As New PromocaoBO
        Dim acaoComercialBO As New AcaoComercialBO
        Dim filtro As New VO.Promocao()
        Dim FiltroAcaoComercial As New VO.AcaoComercial()
        filtro.CODPMC = promocao.CODPMC

        Dim promocaoValidar = promocaoBO.SelectT(filtro)
        FiltroAcaoComercial.CODACOCMC = promocaoValidar.CODACOCMC

        Dim acaoComercial = acaoComercialBO.SelectAcaoComercialParaPromocao(FiltroAcaoComercial)
        'Dim indicadorPromocaoLiberada As Boolean

        If (promocaoValidar IsNot Nothing) Then

            If promocaoValidar.TIPSITPMC = "E" Then
                Return "Promoção Encerrada!"
            End If
            'Data Inicio promoção
            If (promocao.FLGENVPMC <> " " And promocao.TIPSITPMC <> " ") Then
                If (promocao.DATINIPMC.ToString() = "") Then
                    Return "Data de Início da Promoção Inválida!"
                End If
                If ((promocao.DATINIPMC.Value.Date()) < Date.Now().Date) Then
                    Return "Data de Início não pode ser menor que a Data Atual!"
                End If
            End If

            'Data Término da promoção
            If (promocao.DATFIMPMC.ToString() = "") Then
                Return "Data de Término da Promoção Inválida !"
            End If
            Dim dtIniPmc As Date = If(promocao.TIPSITPMC = "I", promocao.DATINIPMC, promocaoValidar.DATINIPMC)
            If ((promocao.DATFIMPMC.Value.Date()) < dtIniPmc) Then
                Return "Data de Término da Promoção deve ser maior ou igual a Data de Início!"
            End If
            If ((promocao.DATFIMPMC.Value.Date()) < Date.Now().Date) Then
                Return "Data de Término da Promoção não pode ser menor que o dia atual!"
            End If
            ' Validar pela a Ação Comercial
            If (acaoComercial.TIPACOMCD = 5 Or acaoComercial.TIPACOMCD = 6) Then
                If ((promocao.DATFIMPMC.Value.Date()) > (acaoComercial.DATFIMACOCMC.Value.Date())) Then
                    Return "Data de Término da Promoção não pode maior que a data fim da ação comercial: " & CDate(acaoComercial.DATFIMACOCMC).ToString("dd/MM/yyyy")
                End If
            End If

            'Se foi selecionado critério adicional 
            If (promocao.INDCTRADIAVLPMC = 1) Then
                'Quantidade Mínima
                If (promocao.QDEMNMITEPEDPMC <= 0) Then
                    Return "A Quantidade Mínima deve ser maior que zero!"
                End If
            End If

            'Validar quantidade de grupos

            Select Case promocao.TIPPMC
                Case "1" 'Promoção do tipo Combinada
                    If (promocao.QDEGRPMIXPMC.GetValueOrDefault() <= 0) Then
                        Return "A Quantidade Mínima deve ser maior que zero!"
                    End If
                Case "2"
                    If (promocao.QDEFXAPMC.GetValueOrDefault() <= 0) Then
                        Return "A Quantidade de Faixas deve ser maior que zero!"
                    End If
            End Select
            'Validar Status Envio
            If (promocao.FLGENVPMC = Nothing) Then
                promocao.FLGENVPMC = ""
            End If
            If (promocao.FLGENVPMC.ToString() = String.Empty And promocaoValidar.FLGENVPMC.ToString() <> String.Empty) Then
                Return "Não é permitido alterar o status para enviado!"
            End If
            If (promocao.FLGENVPMC.ToString() <> String.Empty And promocaoValidar.FLGENVPMC.ToString() = String.Empty) Then
                Return "Foi alterado o status para enviado no banco!"
            End If
            If (promocao.FLGENVPMC.ToString() = " ") Then
                Return "Não é permitido incluir uma promoção com 'Status Envio' igual a 'Enviado'!"
            End If


            If (promocao.TIPPMC = 1 AndAlso promocao.QDEGRPMIXPMC = 0) Then
                Return "Quantidade de Grupos obrigatória!"
            End If
            If (promocao.TIPPMC = 1 AndAlso promocao.QDEGRPMIXPMC <= 1) Then
                Return "Quantidade de Grupos deve ser maior que 1!"
            End If
            If (promocao.TIPPMC = 2 AndAlso promocao.QDEFXAPMC = 0) Then
                Return "Quantidade de Faixas obrigatória!"
            End If
            If (promocao.TIPPMC = 2 AndAlso promocao.QDEFXAPMC <= 1) Then
                Return "Quantidade de Faixas deve ser maior que 1!"
            End If
            If (promocao.INDPTCTOTMIXPMC = 1 AndAlso promocao.TIPPMC = 1) Then
                Return "Não é possivel Participação Total de Mix e Tipo de Promoção Combinada!"
            End If

            If (promocao.INDPMCIMD = 1) Then
                If (IsNothing(promocao.HRAINIPMC) Or IsNothing(promocao.HRAFIMPMC)) Then
                    Return "Para Promoção Relâmpago é obrigatório informar Hr. Inicio e Hr. Fim."
                End If

                Dim dtIni As Date = If(promocao.TIPSITPMC = "I", promocao.DATINIPMC, promocaoValidar.DATINIPMC)
                If (DateDiff(DateInterval.Day, dtIni, promocao.DATFIMPMC.Value) > 3) Then
                    Return "Para Promoção Relâmpago, o campo Dt. de Fim não pode ter mais de 3 dias de diferença para o campo Dt. de Início."
                End If

                If (promocao.HRAINIPMC >= promocao.HRAFIMPMC) Then
                    Return "Hr. Fim precisa ser maior que Hr. Inicio."
                End If
            End If
        End If

            Return Nothing
    End Function

    Public Function ValidarPromocaoLiberada(ByVal promocao As PromocaoViewModel) As String

        Dim promocaoBO As New PromocaoBO
        Dim acaoComercialBO As New AcaoComercialBO
        Dim filtro As New VO.Promocao()
        Dim FiltroAcaoComercial As New VO.AcaoComercial()
        filtro.CODPMC = promocao.CODPMC

        Dim promocaoValidar = promocaoBO.SelectT(filtro)
        FiltroAcaoComercial.CODACOCMC = promocaoValidar.CODACOCMC

        Dim acaoComercial = acaoComercialBO.SelectAcaoComercialParaPromocao(FiltroAcaoComercial)
        'Dim indicadorPromocaoLiberada As Boolean

        If (promocaoValidar IsNot Nothing) Then

            If promocaoValidar.TIPSITPMC = "E" Then
                Return "Promoção Encerrada!"
            End If

            'Data Término da promoção
            If (promocao.DATFIMPMC.ToString() = "") Then
                Return "Data de Término da Promoção Inválida !"
            End If
            If ((promocao.DATFIMPMC.Value.Date()) < (promocaoValidar.DATINIPMC.Value.Date())) Then
                Return "Data de Término da Promoção deve ser maior ou igual a Data de Início!"
            End If
            If ((promocao.DATFIMPMC.Value.Date()) < Date.Now().Date) Then
                Return "Data de Término da Promoção não pode ser menor que o dia atual!"
            End If
            ' Validar pela a Ação Comercial
            If (acaoComercial.TIPACOMCD = 5 Or acaoComercial.TIPACOMCD = 6) Then
                If ((promocao.DATFIMPMC.Value.Date()) > (acaoComercial.DATFIMACOCMC.Value.Date())) Then
                    Return "Data de Término da Promoção não pode maior que a data fim da ação comercial: " & CDate(acaoComercial.DATFIMACOCMC).ToString("dd/MM/yyyy")
                End If
            End If

            'Se foi selecionado critério adicional 
            If (promocao.INDCTRADIAVLPMC = 1) Then
                'Quantidade Mínima
                If (promocao.QDEMNMITEPEDPMC <= 0) Then
                    Return "A Quantidade Mínima deve ser maior que zero!"
                End If
            End If

            'Validar quantidade de grupos

            Select Case promocao.TIPPMC
                Case "1" 'Promoção do tipo Combinada
                    If (promocao.QDEGRPMIXPMC.GetValueOrDefault() <= 0) Then
                        Return "A Quantidade Mínima deve ser maior que zero!"
                    End If
                Case "2"
                    If (promocao.QDEFXAPMC.GetValueOrDefault() <= 0) Then
                        Return "A Quantidade de Faixas deve ser maior que zero!"
                    End If
            End Select

        End If

        Return Nothing
    End Function

    Public Function LiberaCore(ByVal promocaoValidar As VO.Promocao) As List(Of VO.PromocaoBrinde)

        If (promocaoValidar.CODACOCMC Is Nothing) Then
            Throw New Exception("É obrigatório informar o Código da Ação Comercial!")
        End If

        If (promocaoValidar.CODACOCMC = 0) Then
            Throw New Exception("É obrigatório informar o Código da Ação Comercial!")
        End If

        If (promocaoValidar.FLGENVPMC.Trim() = "") Then
            Throw New Exception("Promoção já foi liberada!")
        End If

        If (promocaoValidar.DATINIPMC.GetValueOrDefault().Date < Date.Now.Date) Then
            Throw New Exception("Data de Início não pode ser menor que a Data Atual!")
        End If

        Dim boAcaoComercial As New AcaoComercialBO()
        Dim acao As VO.AcaoComercial = boAcaoComercial.BuscaAcaoPorId(promocaoValidar.CODACOCMC)

        If (acao Is Nothing) Then
            Throw New Exception("Ação Mercadologica não existe!")
        End If

        If (acao.CODSTAAPVACOCMC <> 5) Then
            Throw New Exception("Ação Mercadologica não aprovada")
        End If


        Try
            daoBase.connector.BeginTransaction()
            Dim BOrelacaoAcaoFornecedor As RelacaoAcaoComercialxFornecedoresBO = New RelacaoAcaoComercialxFornecedoresBO()
            Dim relacaoAcaoFornecedor = BOrelacaoAcaoFornecedor.BuscarFornecedoresDaAcao(promocaoValidar.CODACOCMC).FirstOrDefault()

            If (Not promocaoValidar.INDPTCTOTMIXPMC = 1) Then
                ' Atualiza o MIX da promoção conforme os filtros configurados na mecanica
                Dim promocaoMixVisualizarVigenteBO As New PromocaoMixVisualizarVigenteBO
                promocaoMixVisualizarVigenteBO.AtualizaMixPromocao(promocaoValidar.CODPMC, promocaoValidar.TIPEDEPUBALVPMC, promocaoValidar.TIPPMC, promocaoValidar.TIPSITPMC, relacaoAcaoFornecedor.TIPFRNACOCMC, False)
            End If
            'Iniciar Liberação
            'Excluir Cobranca de Brinde se nao existir cobranca na promocao T0161115
            If (promocaoValidar.INDCOBBDEPMC = 0) Then
                Dim premioValor As PromocaoBrindeDAO = New PromocaoBrindeDAO
                Dim lstPremioValor1 As List(Of VO.PromocaoBrinde) = premioValor.SelectRelacaoPromocaoEValorPremioSemMer(New VO.PromocaoBrinde() With {.CODPMC = promocaoValidar.CODPMC})

                If (lstPremioValor1 IsNot Nothing) Then
                    lstPremioValor1.ForEach(Sub(obj) premioValor.RemoveRelacaoPromocaoValor(New VO.PromocaoBrinde With {.CODPRMPMC = obj.CODPRMPMC, .CODPMC = promocaoValidar.CODPMC, .CODACOCMC = obj.CODACOCMC}))
                End If
            End If






            Dim msgErro = ""


            ' nao estava sendo usado comentei.
            'T0132107
            'Dim listRelacaoPromocaoEPremioAdicional As List(Of VO.PromocaoBrinde) = promocaoBrindeBO.SelectRelacaoPromocaoEPremioAdicional(New VO.PromocaoBrinde() With {.CODPMC = promocaoValidar.CODPMC, .TIPEDEPUBALVPMC = 4})


            Dim promocaoBrindeBO As New PromocaoBrindeBO
            'T0108044
            Dim listPremioPorNivelPremiacao As List(Of VO.PromocaoBrinde) = promocaoBrindeBO.ListaParaGrid(New VO.PromocaoBrinde() With {.CODPMC = promocaoValidar.CODPMC, .TIPEDEPUBALVPMC = 4})
            Dim EscalaProgressivaBO As New RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO()
            Dim FaixaFinitaBO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaBO()
            Dim EscalaProgressivaVO = EscalaProgressivaBO.BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva With {.CODPMC = promocaoValidar.CODPMC})
            Dim FaixaFinitaVO = FaixaFinitaBO.BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita With {.CODPMC = promocaoValidar.CODPMC})
            If (EscalaProgressivaVO.Count > 0 AndAlso EscalaProgressivaVO.Where(Function(r) r.INDTIPPTOPMC = 0).Count > 0 _
                Or FaixaFinitaVO.Count > 0 AndAlso FaixaFinitaVO.Where(Function(r) r.INDTIPPTOPMC = 0).Count > 0) Then

                If (listPremioPorNivelPremiacao Is Nothing OrElse listPremioPorNivelPremiacao.Count = 0) Then
                    If (msgErro <> "") Then
                        msgErro = msgErro + "#SPACE#"
                    End If

                    msgErro = msgErro + "Não existe brinde para promoção: " + promocaoValidar.CODPMC.ToString()
                End If

            End If

            If (promocaoValidar.FLGENVPMC Is Nothing) Then
                If (msgErro <> "") Then
                    msgErro = msgErro + "#SPACE#"
                End If

                msgErro = msgErro + "Promoção já foi enviada (código: " + promocaoValidar.CODPMC.ToString() + ")"

            ElseIf (promocaoValidar.FLGENVPMC = " ") Then
                If (msgErro <> "") Then
                    msgErro = msgErro + "#SPACE#"
                End If

                msgErro = msgErro + "Promoção já foi enviada (código: " + promocaoValidar.CODPMC.ToString() + ")"

            ElseIf (promocaoValidar.TIPSITPMC <> "I") Then
                If (msgErro <> "") Then
                    msgErro = msgErro + "#SPACE#"
                End If

                msgErro = msgErro + "Esta Promoção não pode ser Liberada!"
            End If


            Dim dataInicioSemanaList As List(Of VO.Promocao) = Me.DataInicioSemana()

            If (dataInicioSemanaList Is Nothing OrElse dataInicioSemanaList.Count = 0) Then
                Throw New Exception("Não foi encontrado registro pelo método que Obtem o dia do inicio da semana da tabela T0102321 com base na data atual")
            End If

            Dim dataRefeDataFimAgendaList As List(Of VO.Promocao) = Me.DataReferenciaEDataFimAgendaDiariaLivroPrecos(dataInicioSemanaList(0))

            If (dataRefeDataFimAgendaList Is Nothing OrElse dataRefeDataFimAgendaList.Count = 0) Then
                Throw New Exception("Não foi retornado registros pelo método busca as data de Referencia e Data Final da Agenda Diária do Livro")
            End If

            Dim listErroMercadoria As New List(Of VO.PromocaoBrinde)

            'If (promocaoValidar.DATINIPMC > dataRefeDataFimAgendaList(0).DATFIMVLDLIVPCO) Then
            '    'VerificarDadosVolateisDaMercadoria
            '    Dim listDadosVolateisMercadoria As List(Of VO.PromocaoBrinde) = promocaoBrindeBO.VerificarDadosVolateisDaMercadoria(New VO.PromocaoBrinde() With {.CODPMC = promocaoValidar.CODPMC, .TIPEDEPUBALVPMC = 4, .CODFILEMP = promocaoValidar.CODFILEMP})

            '    For Each mer In listDadosVolateisMercadoria

            '        Dim CODMER As Decimal = 0
            '        Dim DESMER As String = ""
            '        Dim TIPO As String = ""

            '        If (mer.CODMERCST Is Nothing OrElse mer.CODMERCST = 0) Then
            '            If (mer.CODMERPCP IsNot Nothing) Then
            '                CODMER = mer.CODMERPCP
            '            End If

            '            If (mer.DESMER IsNot Nothing) Then
            '                DESMER = mer.DESMER
            '            End If

            '            If (mer.FLGMER IsNot Nothing) Then
            '                TIPO = mer.FLGMER.Trim()
            '            End If

            '            listErroMercadoria.Add(New VO.PromocaoBrinde With {
            '                .TIPO = TIPO,
            '                .ERRO = "Mercadoria sem custo!",
            '                .CODPRMPMC = CODMER,
            '                .DESMER = DESMER
            '            })
            '        Else
            '            If (mer.CODMERMRG Is Nothing OrElse mer.CODMERMRG = 0) Then
            '                If (mer.CODMERPCP IsNot Nothing) Then
            '                    CODMER = mer.CODMERPCP
            '                End If

            '                If (mer.DESMER IsNot Nothing) Then
            '                    DESMER = mer.DESMER
            '                End If

            '                If (mer.FLGMER IsNot Nothing) Then
            '                    TIPO = mer.FLGMER.Trim()
            '                End If

            '                listErroMercadoria.Add(New VO.PromocaoBrinde With {
            '                    .TIPO = TIPO,
            '                    .ERRO = "Mercadoria sem Margem/Comissão!",
            '                    .CODPRMPMC = CODMER,
            '                    .DESMER = DESMER
            '                })
            '            End If
            '        End If
            '    Next
            'Else
            '    'VerificarHistoricoDiarioMercadoriaLivroPrecos
            '    Dim listHistoricoDiarioMercadoria As List(Of VO.PromocaoBrinde) = promocaoBrindeBO.VerificarHistoricoDiarioMercadoriaLivroPrecos(New VO.PromocaoBrinde() With {.CODPMC = promocaoValidar.CODPMC, .TIPEDEPUBALVPMC = 4, .CODFILEMP = promocaoValidar.CODFILEMP, .DATREFLIVPCO = dataRefeDataFimAgendaList(0).DATREFLIVPCO})

            '    For Each mer In listHistoricoDiarioMercadoria

            '        Dim CODMER As Decimal = 0
            '        Dim DESMER As String = ""
            '        Dim TIPO As String = ""

            '        If (mer.CODMERCST Is Nothing OrElse mer.CODMERCST = 0) Then
            '            If (mer.CODMERPCP IsNot Nothing) Then
            '                CODMER = mer.CODMERPCP
            '            End If

            '            If (mer.DESMER IsNot Nothing) Then
            '                DESMER = mer.DESMER
            '            End If

            '            If (mer.FLGMER IsNot Nothing) Then
            '                TIPO = mer.FLGMER.Trim()
            '            End If

            '            listErroMercadoria.Add(New VO.PromocaoBrinde With {
            '                    .TIPO = TIPO,
            '                    .ERRO = "Mercadoria sem custo!",
            '                    .CODPRMPMC = CODMER,
            '                    .DESMER = DESMER
            '             })
            '        End If
            '    Next
            'End If




            'T0153553




            'Dim lstCodFnc As List(Of Decimal) = (From c In listRelacaoAcaoComercialEPromocaoEBrinde Select (c.CODFNCGRCPMC.GetValueOrDefault())).ToList()

            'T0100361
            'Dim lstFuncionarios As List(Of VO.Funcionario) = funcionarioBO.GetSelectFuncionario(lstCodFnc)



            ' Verificações relacionadas ao abastecimento da promoção
            ' RelacaoAcaoMecanincaBO.VerificaPromocaoTipoBrinde(acaoComercial.CODACOCMC)

            Dim abastecimentoBO As New AbastecimentoBO
            Dim listAbastecimento As List(Of VO.Abastecimento) = abastecimentoBO.SelectRelacaoAcaoComercialEPromocaoEBrinde(New VO.Abastecimento() With {.CODPMC = promocaoValidar.CODPMC})
            If (EscalaProgressivaVO.Count > 0 AndAlso EscalaProgressivaVO.Where(Function(r) r.INDTIPPTOPMC = 0).Count > 0 _
                Or FaixaFinitaVO.Count > 0 AndAlso FaixaFinitaVO.Where(Function(r) r.INDTIPPTOPMC = 0).Count > 0) Then
                If (listAbastecimento.Count = 0) Then
                    Throw New Exception("É obrigatório abastecer os brindes antes de liberar a promoção.")
                End If

                'Varre as linha do premio
                'T0108044row
                For Each item As VO.PromocaoBrinde In listPremioPorNivelPremiacao
                    'Verifica se existe quantidade de premio T0153553
                    If (listAbastecimento.Where(Function(obj) obj.CODMER = item.CODPRMPMC).Count = 0) Then
                        If (msgErro <> "") Then
                            msgErro = msgErro + "#SPACE#"
                        End If
                        msgErro = msgErro + "É obrigatório informar a quantidade para o brinde: " + item.CODPRMPMC.ToString() + " - " + item.DESPRMPMC
                    End If
                Next


                ' Não estava usando comentei.
                ' Dim lstPremioValorJoin As List(Of VO.PromocaoBrinde) = promocaoBrindeBO.SelectRelacaoPromocaoEValorPremioJoin(New VO.PromocaoBrinde() With {.CODPMC = promocaoValidar.CODPMC})


                Dim lstPremioValor As List(Of VO.PromocaoBrinde) = promocaoBrindeBO.SelectRelacaoPromocaoEValorPremioSemMerAco(New VO.PromocaoBrinde() With {.CODPMC = promocaoValidar.CODPMC})
                If (promocaoValidar.INDCOBBDEPMC = 1) Then
                    If (lstPremioValor.Where(Function(obj) obj.VLRUNTLIQMER = 0).ToList().Count <> 0) Then
                        If (msgErro <> "") Then
                            msgErro = msgErro + "#SPACE#"
                        End If
                        msgErro = msgErro + "É obrigatório informar cobrança de brinde para todos itens"
                    End If
                Else
                    If (lstPremioValor IsNot Nothing AndAlso lstPremioValor.Count > 0) Then
                        Throw New Exception("Promoção marcado para não cobrar brinde entretanto existe(m) brinde(s) indicando cobrança")
                    End If
                End If

            End If


            'Validar também a join?

            'VerificarErrosCriteriosPromocao
            'Removido


            'quando é mix total pula a validação de mix
            If (Not promocaoValidar.INDPTCTOTMIXPMC = 1) Then

                Dim promocaoMixBO As PromocaoMixVisualizarVigenteBO = New PromocaoMixVisualizarVigenteBO()
                Dim lstRelacaoMercadoriaPromocao As List(Of VO.PromocaoMixVisualizarVigente)
                '+ de 30reg
                'T0107994
                If (relacaoAcaoFornecedor.TIPFRNACOCMC = 1) Then
                    lstRelacaoMercadoriaPromocao = promocaoMixBO.PesquisaItensVisualizarMixVigenteEdl(promocaoValidar.CODPMC, 4)
                Else
                    lstRelacaoMercadoriaPromocao = promocaoMixBO.PesquisaItensVisualizarMixVigente(promocaoValidar.CODPMC, 4)
                End If

                If (lstRelacaoMercadoriaPromocao Is Nothing) Then
                    lstRelacaoMercadoriaPromocao = New List(Of VO.PromocaoMixVisualizarVigente)
                End If

                For Each item As VO.PromocaoMixVisualizarVigente In lstRelacaoMercadoriaPromocao
                    If (item.CODGRPMIXMERPMC Is Nothing) Then
                        If (msgErro <> "") Then
                            msgErro = msgErro + "#SPACE#"
                        End If
                        msgErro = msgErro + "Existe(m) mercadoria(s) sem grupo definido"

                        Exit For
                    End If
                    If (item.CODGRPMIXMERPMC = 0) Then
                        If (msgErro <> "") Then
                            msgErro = msgErro + "#SPACE#"
                        End If
                        msgErro = msgErro + "Existe(m) mercadoria(s) sem grupo definido"

                        Exit For
                    End If
                Next



                Dim mixVigente = promocaoMixBO.MixVigentePromocao(promocaoValidar.CODPMC, promocaoValidar.TIPEDEPUBALVPMC)

                If (mixVigente Is Nothing OrElse mixVigente.Count = 0) Then
                    'Erro
                    Throw New Exception("O Mix da promoção não possui mercadorias associadas!")
                End If

                If (promocaoValidar.TIPPMC = 1) Then

                    Dim qtd = mixVigente.GroupBy(Function(f) f.CODGRPMIXMERPMC).Select(Function(g) g.Key).Count()
                    'Verifica os agrupamentos se promoção for do tipo: combo
                    If (promocaoValidar.QDEGRPMIXPMC <> mixVigente.GroupBy(Function(f) f.CODGRPMIXMERPMC).Select(Function(g) g.Key).Count()) Then
                        Throw New Exception("Existem Grupos de Mix sem mercadoria associada!")
                    End If

                    'VerificarSeAgrupamentoDePromocaoComboUltrapassouAFaixaDeVariacaoPermitida
                    Dim ValorMinimoVariacao As Decimal
                    Dim ValorMaximoVariacao As Decimal
                    Dim indicadorExisteGrupoAbaixoMinimo As Boolean
                    Dim indicadorExisteGrupoAcimaMaximo As Boolean

                    ValorMinimoVariacao = promocaoValidar.VLRBSECALGRPMIXPMC - (promocaoValidar.VLRBSECALGRPMIXPMC * (promocaoValidar.PERVARGRPMIXPMC / 100))
                    ValorMaximoVariacao = promocaoValidar.VLRBSECALGRPMIXPMC + (promocaoValidar.VLRBSECALGRPMIXPMC * (promocaoValidar.PERVARGRPMIXPMC / 100))

                    Dim ValorGrupo As Decimal
                    Dim VLRMEDITEGRPMIXPMC As Decimal
                    Dim QDEMNMITEGRPMIXPMC As Decimal

                    Dim lstRelacaoPromocaoESequenciaGrupoMix = promocaoMixBO.SelectRelacaoPromocaoSeqGrupoMix(promocaoValidar.CODPMC)

                    For Each item As VO.PromocaoMixVisualizarVigente In lstRelacaoPromocaoESequenciaGrupoMix
                        QDEMNMITEGRPMIXPMC = item.QDEMNMITEGRPMIXPMC
                        VLRMEDITEGRPMIXPMC = item.VLRMEDITEGRPMIXPMC
                        ValorGrupo = Decimal.Round(QDEMNMITEGRPMIXPMC * VLRMEDITEGRPMIXPMC, 2)

                        If ValorGrupo < ValorMinimoVariacao Then
                            indicadorExisteGrupoAbaixoMinimo = True
                        ElseIf ValorGrupo > ValorMaximoVariacao Then
                            indicadorExisteGrupoAcimaMaximo = True
                        End If
                    Next

                    If indicadorExisteGrupoAbaixoMinimo Then
                        If (msgErro <> "") Then
                            msgErro = msgErro + "#SPACE#"
                        End If
                        msgErro = msgErro + "Existe(m) grupo(s) com valor inferior a variação permitida"

                    End If
                    If indicadorExisteGrupoAcimaMaximo Then
                        If (msgErro <> "") Then
                            msgErro = msgErro + "#SPACE#"
                        End If
                        msgErro = msgErro + "Existe(m) grupo(s) com valor superior a variação permitida"

                    End If
                End If

                Dim msgErro2 = ""

                'VerificaQtdeMinima
                Dim lstVerificaQtdMin = Me.ListaMIXQtdMinVndMaiorQtdPmc(promocaoValidar.CODPMC)
                If (lstVerificaQtdMin.Count > 0) Then
                    For Each item In lstVerificaQtdMin
                        msgErro2 = msgErro2 + "<tr><td>" + item.CODMER.ToString() + "</td><td>" + item.QDEMIN.ToString() + "</td></tr>"
                    Next
                End If
                If (msgErro2 <> "") Then
                    Throw New Exception("Quantidade de venda não pode ser menor que a quantidade minima.: #SPACE# <Table Class='table table-striped'><thead><tr><td>Mercadoria</td><td>Quantidade</td></tr></thead><tbody>" + msgErro2 + "</tbody></table>")
                End If


                'VerificaQtdPmcNaoMultiploNaLiberacao
                Dim lstVerificaQtdPmcNaoMult = Me.ListaMIXQtdPmcNaoMult(promocaoValidar.CODPMC)
                If (lstVerificaQtdPmcNaoMult.Count > 0) Then
                    For Each item In lstVerificaQtdMin
                        msgErro2 = msgErro2 + "<tr><td>" + item.CODMER.ToString() + "</td><td>" + item.QDEMIN.ToString() + "</td></tr>"
                    Next
                End If
                If (msgErro2 <> "") Then
                    Throw New Exception("Intervalo deve ser múltiplo da Qtde. Venda Merc.: #SPACE# <Table Class='table table-striped'><thead><tr><td>Mercadoria</td><td>Quantidade</td></tr></thead><tbody>" + msgErro2 + "</tbody></table>")
                End If

                If (promocaoValidar.TIPPMC = 1) Then
                    For Each item As VO.PromocaoMixVisualizarVigente In lstRelacaoMercadoriaPromocao
                        If (item.CODGRPMIXMERPMC = 0) Then
                            Throw New Exception("Não informado grupo para mercadoria " + item.CODMER.ToString())
                        End If
                    Next
                End If

                ''Verifica faixa de quantidade de agrupamentos é multiplo da quantidade mínima de venda da mercadoria
                'If (promocaoValidar.TIPPMC = 1) Then
                '    Dim lstRelacaoPromocaoESequenciaGrupoMix = promocaoMixBO.SelectRelacaoPromocaoSeqGrupoMix(promocaoValidar.CODPMC)
                '    For Each item As VO.PromocaoMixVisualizarVigente In lstRelacaoPromocaoESequenciaGrupoMix

                '        'T0107994JoinT0100086
                '        Dim lstRelacaoMercadoriaEPromocao = promocaoMixBO.PesquisaItensVisualizarMixVigente(promocaoValidar.CODPMC, 4)
                '        If (lstRelacaoMercadoriaEPromocao.Count > 0) Then
                '            'ObterValoresUnitariosBrutoDeVendaDaMercadoria   codfilemp codmer-> lstrelacaomercadoria

                '            Dim lstCodMer As List(Of Decimal) = (From c In lstRelacaoMercadoriaEPromocao Select (c.CODMER.GetValueOrDefault())).ToList()

                '            Dim lstValorUnitarioVendaMer = promocaoMixBO.SelectValoresUnitariosBrutoDeVendaDaMercadoria(promocaoValidar.CODFILEMP, lstCodMer)
                '            Dim lstValorUnitarioVendaMer2 = promocaoMixBO.SelectValoresUnitariosBrutoDeVendaDaMercadoria2(promocaoValidar.CODFILEMP, lstCodMer)

                '            For Each itemValor As VO.PromocaoMixVisualizarVigente In lstValorUnitarioVendaMer2
                '                If (lstValorUnitarioVendaMer.Where(Function(obj) obj.CODMER = itemValor.CODMER).ToList().Count = 0) Then
                '                    lstValorUnitarioVendaMer.Add(itemValor)
                '                End If
                '            Next

                '            'Verifica todas as mercadorias que estão no grupo do primeiro for
                '            For Each linhaT0107994JoinT0100086 As VO.PromocaoMixVisualizarVigente In lstRelacaoMercadoriaEPromocao
                '                If (linhaT0107994JoinT0100086.NUMSEQGRPMIXPMC = item.NUMSEQGRPMIXPMC) Then
                '                    If (linhaT0107994JoinT0100086.CODGRPMERFRC = 0) Then
                '                        If (linhaT0107994JoinT0100086.QDEMNMVNDMER <> 0) Then
                '                            If ((item.QDEMNMITEGRPMIXPMC Mod linhaT0107994JoinT0100086.QDEMNMVNDMER) <> 0) Then
                '                                msgErro2 = msgErro2 + "<tr><td>" + linhaT0107994JoinT0100086.CODMER.ToString() + "</td><td>" + linhaT0107994JoinT0100086.QDEMNMVNDMER.ToString() + "</td></tr>"
                '                            End If
                '                        End If
                '                    Else
                '                        If (linhaT0107994JoinT0100086.QDEMNMVNDMER <> 0) Then
                '                            If ((item.QDEMNMITEGRPMIXPMC Mod linhaT0107994JoinT0100086.QDEMNMVNDMER) <> 0) Then
                '                                msgErro2 = msgErro2 + "<tr><td>" + linhaT0107994JoinT0100086.CODMER.ToString() + "</td><td>" + linhaT0107994JoinT0100086.QDEMNMVNDMER.ToString() + "</td></tr>"
                '                            End If
                '                        End If
                '                    End If
                '                End If
                '            Next


                '        End If


                '    Next

                '    'msgErro2 -> Intervalo deve ser múltiplo da Qtde. Venda Merc.: 
                '    'If (msgErro2 <> "") Then
                '    '    If (msgErro <> "") Then
                '    '        msgErro = msgErro + "#SPACE#"
                '    '    End If
                '    '    msgErro = msgErro + "Existe(m) grupo(s) com quantidade que não é múltiplo da venda da mercadoria"

                '    'End If

                '    If (msgErro2 <> "") Then
                '        msgErro2 = "<Table Class='table table-striped'><thead><tr><td>Mercadoria</td><td>Quantidade</td></tr></thead><tbody>" + msgErro2 + "</tbody></table>"
                '        Throw New Exception("Existe(m) grupo(s) com quantidade que não é múltiplo da venda da mercadoria: #SPACE#" + msgErro2)
                '    End If

                '    'ValidarSeFoiDefinidoGrupoEmPromocaoCombo lstRelacaoMercadoriaPromocao
                '    For Each item As VO.PromocaoMixVisualizarVigente In lstRelacaoMercadoriaPromocao
                '        If (item.CODGRPMIXMERPMC = 0) Then
                '            Throw New Exception("Não informado grupo para mercadoria " + item.CODMER.ToString())
                '        End If
                '    Next

                'ElseIf (promocaoValidar.TIPPMC = 0) Then
                '    'ObterParametrosFormulaCalculoPromocaoEscalaProgressiva
                '    'T0107951
                '    Dim RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO As New RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO
                '    Dim ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva = RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO.BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {.CODPMC = promocaoValidar.CODPMC})

                '    Dim msgErroFormCalc = ""
                '    Dim lstRelacaoMercadoriaEPromocao = promocaoMixBO.PesquisaItensVisualizarMixVigente(promocaoValidar.CODPMC, 4)
                '    For Each item As VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva In ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva

                '        If (item.CODCTRPMC > 0) Then
                '            If (item.CODCTRPMC <> 5) Then 'CODCTRPMC = CODCTRPMC_POR_VALOR (5)
                '                'Verificar se a quantidade informada é multiplo de venda na tabela de mercadoria
                '                'T0107994JoinT0100086
                '                If (lstRelacaoMercadoriaEPromocao.Count > 0) Then
                '                    For Each linhaT0107994JoinT0100086 As VO.PromocaoMixVisualizarVigente In lstRelacaoMercadoriaEPromocao
                '                        If (linhaT0107994JoinT0100086.CODGRPMERFRC = 0) Then
                '                            If (linhaT0107994JoinT0100086.QDEMNMVNDMER <> 0) Then
                '                                If ((item.TAMIVLFXAEQCCALCTR Mod linhaT0107994JoinT0100086.QDEMNMVNDMER) <> 0) Then
                '                                    msgErroFormCalc = msgErroFormCalc + "<tr><td>" + linhaT0107994JoinT0100086.CODMER.ToString() + "</td><td>" + linhaT0107994JoinT0100086.QDEMNMVNDMER.ToString() + "</td></tr>"
                '                                End If
                '                            End If
                '                        Else
                '                            If (linhaT0107994JoinT0100086.QDEMNMVNDMER <> 0) Then
                '                                If ((item.TAMIVLFXAEQCCALCTR Mod linhaT0107994JoinT0100086.QDEMNMVNDMER) <> 0) Then
                '                                    msgErroFormCalc = msgErroFormCalc + "<tr><td>" + linhaT0107994JoinT0100086.CODMER.ToString() + "</td><td>" + linhaT0107994JoinT0100086.QDEMNMVNDMER.ToString() + "</td></tr>"
                '                                End If
                '                            End If
                '                        End If
                '                    Next
                '                End If
                '            End If
                '        End If
                '    Next

                '    If (msgErroFormCalc <> "") Then
                '        msgErroFormCalc = "<Table Class='table table-striped'><thead><tr><td>Mercadoria</td><td>Quantidade</td></tr></thead><tbody>" + msgErroFormCalc + "</tbody></table>"
                '        Throw New Exception("Intervalo deve ser múltiplo da Qtde. Venda Merc.: #SPACE#" + msgErroFormCalc)
                '    End If

                'End If

                'Verificar Ação Comercial COMENTADO

                'Verifica as Mercadorias
                If (promocaoValidar.INDPTCTOTMIXPMC = 0) Then
                    'ValidarMercadoriaDaPromocao
                    If (lstRelacaoMercadoriaPromocao.Count = 0) Then
                        If (msgErro <> "") Then
                            msgErro = msgErro + "#SPACE#"
                        End If
                        msgErro = msgErro + "Não existem Mercadorias Cadastradas para a Promoção!"

                    End If
                End If

                If msgErro <> String.Empty Then
                    Throw New Exception(msgErro)
                End If


            End If

            'Verifica se existe parâmetros mix com item obrigatório e/ou quantidade minima
            Dim BOpromocaoMix As New PromocaoMixBO
            Dim parametrosMix = BOpromocaoMix.ListarParametrosMix(promocaoValidar.CODPMC, promocaoValidar.TIPSITPMC)
            If (parametrosMix IsNot Nothing AndAlso parametrosMix.Any(Function(f) f.QDEMNMMERMIX > 0 Or f.INDITEOBR = 1)) Then
                promocaoValidar.INDPMTMIXPMC = 1
            Else
                promocaoValidar.INDPMTMIXPMC = 0
            End If

            If (listErroMercadoria.Count = 0) Then
                Dim relacaoFuncionarioUserRacfAcountBO As New RelacaoFuncionarioUserRacfAcountBO
                Dim funcionario = relacaoFuncionarioUserRacfAcountBO.BuscaPorCodFnc(New VO.RelacaoFuncionarioUserRacfAcount With {.CODFNC = ControleAcesso.CodFnc()})

                If (funcionario Is Nothing) Then
                    Throw New Exception("Não foi possível localizar o funcionário que está alterando a promoção.")
                End If

                promocaoValidar.TIPSITPMC = " "
                promocaoValidar.NOMUSRSIS = funcionario.NOMUSRRCF
                promocaoValidar.DATHRAALTSITPMC = DateTime.Now

                Me.UpdateT(promocaoValidar)
                daoBase.connector.Commit()
            Else
                daoBase.connector.Rollback()
            End If

            Return listErroMercadoria

        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try
    End Function

    Public Function Liberar(ByVal codPmc As Decimal) As RetornoValidaQtd
        Try
            Dim promocaoValidar = Me.SelectT(New VO.Promocao() With {.CODPMC = codPmc})

            If (promocaoValidar Is Nothing) Then
                Return New RetornoValidaQtd With {.Erro = "Promoção não encontrada."}
            End If

            If promocaoValidar.TIPSITPMC = "E" Then
                Return New RetornoValidaQtd With {.Erro = "Promoção Encerrada!"}
            End If

            If (promocaoValidar.TIPSITPMC = " " Or promocaoValidar.TIPSITPMC = "") Then
                Return New RetornoValidaQtd With {.Erro = "Promoção já liberada!"}
            End If


            Dim QDEPTOG As Decimal

            Dim RelacaoPromocaoCriterioApuracaoFaixaFinitaBO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaBO()
            Dim ListRelacaoPromocaoCriterioApuracaoFaixaFinita = RelacaoPromocaoCriterioApuracaoFaixaFinitaBO.BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {.CODPMC = codPmc})

            If (ListRelacaoPromocaoCriterioApuracaoFaixaFinita.Count > 0) Then
                QDEPTOG = ListRelacaoPromocaoCriterioApuracaoFaixaFinita(0).QDEPTOFXACTRPMC
            Else
                Dim RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO As New RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO()
                Dim ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva As List(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva) = RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO.BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {.CODPMC = codPmc})
                If (ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva.Count > 0) Then
                    QDEPTOG = ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva(0).QDEPTOFXAEQCCALCTR
                Else
                    Return New RetornoValidaQtd With {.Erro = "Nenhum critério salvo."}
                End If
            End If


            'VerificaQtdeMinima
            'VerificaQtdMinVndMaiorQtdPmc
            Dim lstVerificaQtdMin = Me.VerificaQtdMinVndMaiorQtdPmc(New VO.Promocao With {.CODPMC = codPmc})
            If (lstVerificaQtdMin.Count > 0) Then
                Dim vm = Mapper.Map(Of List(Of VO.Promocao), List(Of PromocaoViewModel))(lstVerificaQtdMin)
                vm.ForEach(Sub(obj) obj.QDEPREMIOATINGIMENTO = QDEPTOG)

                Return New RetornoValidaQtd With {.Erro = "QtdVenda", .lstPromocaoErros = vm}
            End If

            'VerificaQtdPmcNaoMultiploNaLiberacao
            Dim lstVerificaQtdPmcNaoMult = Me.VerificaQtdPmcNaoMult(New VO.Promocao With {.CODPMC = codPmc})
            If (lstVerificaQtdPmcNaoMult.Count > 0) Then
                Dim vm = Mapper.Map(Of List(Of VO.Promocao), List(Of PromocaoViewModel))(lstVerificaQtdPmcNaoMult)
                vm.ForEach(Sub(obj) obj.QDEPREMIOATINGIMENTO = QDEPTOG)

                Return New RetornoValidaQtd With {.Erro = "QtdMult", .lstPromocaoErros = vm}
            End If

            Dim lstMercadoriaErro = Me.LiberaCore(promocaoValidar)

            If (lstMercadoriaErro.Count = 0) Then
                Return New RetornoValidaQtd With {.Erro = "SUCESSO"}
            Else
                Return New RetornoValidaQtd With {.Erro = "Foram encontrados erros nas seguintes mercadorias: ", .lstMercadoriaErros = lstMercadoriaErro}
            End If
        Catch ex As Exception

            Return New RetornoValidaQtd With {.Erro = ex.Message}
        End Try

    End Function

    Public Function GerarPromocaoMecanica(ByVal CODACOCMC As Decimal _
                        , ByVal NUMSEQRGRACOCMC As Decimal _
                        , ByVal filiais As List(Of Decimal) _
                        , ByVal promocao As VO.Promocao _
                        , ByVal criterioAdicionalAvaliacaoPromocao As VO.CriterioAdicionalAvaliacaoPromocao _
                        , ByVal brindes As List(Of VO.PromocaoBrinde) _
                        , ByVal comunicacao As List(Of VO.PromocaoComunicacao)) As List(Of VO.Promocao)

        Dim promocoes As New List(Of VO.Promocao)()

        Dim relacaoAcaoMecanicaFilialBO As New RelacaoAcaoMecanicaFilialBO()

        Dim filiaisAcaoMecanica = relacaoAcaoMecanicaFilialBO.ListFiliaisRelacaoMecanica(CODACOCMC, NUMSEQRGRACOCMC)

        For Each item In filiais
            Dim filial = filiaisAcaoMecanica.Where(Function(f) f.CODFILEMP = item).FirstOrDefault()
            If (filial Is Nothing) Then
                Throw New Exception(String.Format("Esta promoção nao esta prevista para a filial: {0}", item))
            End If
            If (filial.CODPMC IsNot Nothing) Then
                Throw New Exception(String.Format("Já foi gerado a promoção {0} para a filial {1} - {2}", filial.CODPMC, filial.CODFILEMP, filial.DESABVFILEMP))
            End If
        Next

        Dim relacaoAcaoMecanicaBO As New RelacaoAcaoMecanicaBO()
        Dim relacaoAcaoMecanicaVO As VO.RelacaoAcaoMecanica = relacaoAcaoMecanicaBO.SelectEditMecanicaGerais(CODACOCMC, NUMSEQRGRACOCMC)

        Dim relacaoAcaoMecanicaCriterioDeApuracaoBO As New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
        Dim relacaoAcaoMecanicaCriterioDeApuracaoList = relacaoAcaoMecanicaCriterioDeApuracaoBO.ListT(New VO.RelacaoAcaoMecanicaCriterioDeApuracao() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

        Dim relacaoAcaoMecanicaPublicoAlvoBO As New RelacaoAcaoMecanicaPublicoAlvoBO
        Dim RelacaoAcaoMecanicaPublicoAlvo = relacaoAcaoMecanicaPublicoAlvoBO.SelectCountPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC)

        'Se foi configurado por Região
        Dim listPublicoAlvoEstado As New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado)
        If (RelacaoAcaoMecanicaPublicoAlvo.INDESTADO) Then
            Dim bo As New RelacaoAcaoMecanicaPublicoAlvoEstadoBO
            listPublicoAlvoEstado = bo.ListT(New VO.RelacaoAcaoMecanicaPublicoAlvoEstado() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})
        End If
        'Se foi configurado por Atividade do cliente
        Dim listPublicoAlvoAtividade As New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)
        If (RelacaoAcaoMecanicaPublicoAlvo.INDATIVIDADE) Then
            Dim bo As New RelacaoAcaoMecanicaPublicoAlvoAtividadeBO
            listPublicoAlvoAtividade = bo.ListT(New VO.RelacaoAcaoMecanicaPublicoAlvoAtividade() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})
        End If
        'Se foi configurado por Grupamento de cliente
        Dim listPublicoAlvoGrupamentoCliente As New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)
        If (RelacaoAcaoMecanicaPublicoAlvo.INDGRUPAMENTOCLIENTE) Then
            Dim bo As New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO
            listPublicoAlvoGrupamentoCliente = bo.ListT(New VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})
        End If
        'Se foi configurado por Negocio do cliente
        Dim listPublicoAlvoNegocioCliente As New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente)
        If (RelacaoAcaoMecanicaPublicoAlvo.INDNEGOCIOCLIENTE) Then
            Dim bo As New RelacaoAcaoMecanicaPublicoAlvoNegocioClienteBO
            listPublicoAlvoNegocioCliente = bo.ListT(New VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})
        End If
        'Se foi configurado por Segmento do cliente
        Dim listPublicoAlvoSegmentoCliente As New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente)
        If (RelacaoAcaoMecanicaPublicoAlvo.INDSEGMENTOCLIENTE) Then
            Dim bo As New RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteBO
            listPublicoAlvoSegmentoCliente = bo.ListT(New VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})
        End If
        'Se foi configurado por cliente
        Dim listPublicoAlvoCliente As New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)
        If (RelacaoAcaoMecanicaPublicoAlvo.INDCLIENTE) Then
            Dim bo As New RelacaoAcaoMecanicaPublicoAlvoClienteBO
            listPublicoAlvoCliente = bo.ListT(New VO.RelacaoAcaoMecanicaPublicoAlvoCliente() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})
        End If
        'Se foi configurado por RCA/Vendedor - Força de vendas
        Dim listPublicoAlvoRcaVendedor As New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)
        If (RelacaoAcaoMecanicaPublicoAlvo.INDRCAVENDEDOR) Then
            Dim bo As New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO
            listPublicoAlvoRcaVendedor = bo.ListT(New VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})
        End If
        'Se foi configurado por Gerente de Vendas - Força de vendas
        Dim listPublicoAlvoGerenteVendas As New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas)
        If (RelacaoAcaoMecanicaPublicoAlvo.INDGERENTEVENDAS) Then
            Dim bo As New RelacaoAcaoMecanicaPublicoAlvoGerenteVendasBO
            listPublicoAlvoGerenteVendas = bo.ListT(New VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})
        End If
        'Se foi configurado por Gerente Mercado - Força de vendas
        Dim listPublicoAlvoGerenteMercado As New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)
        If (RelacaoAcaoMecanicaPublicoAlvo.INDGERENTEMERCADO) Then
            Dim bo As New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO
            listPublicoAlvoGerenteMercado = bo.ListT(New VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})
        End If

        Dim relacaoAcaoMecanicaClientesExcludentesBO As New RelacaoAcaoMecanicaClientesExcludentesBO
        Dim listRelacaoAcaoMecanicaClientesExcludentes = relacaoAcaoMecanicaClientesExcludentesBO.ListT(New VO.RelacaoAcaoMecanicaClientesExcludentes() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

        Dim relacaoAcaoMecanicaMixBO As New RelacaoAcaoMecanicaMixBO
        Dim listRelacaoAcaoMecanicaMix = relacaoAcaoMecanicaMixBO.ListT(New VO.RelacaoAcaoMecanicaMix() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})




        Dim relacaoFuncionarioUserRacfAcountBO As New RelacaoFuncionarioUserRacfAcountBO
        Dim funcionario = relacaoFuncionarioUserRacfAcountBO.BuscaPorCodFnc(New VO.RelacaoFuncionarioUserRacfAcount() With {.CODFNC = ControleAcesso.CodFnc()})

        If (funcionario Is Nothing) Then
            Throw New Exception("Não foi possível localizar o funcionário que está criando a promoção.")
        End If



        Dim promocaoBrindeBO As New PromocaoBrindeBO()
        Dim promocaoComunicacaoBO As New PromocaoComunicacaoBO()
        Dim promocaoMixBO As New PromocaoMixBO()
        Dim PromocaoClientesExcludentesBO As New PromocaoClientesExcludentesBO()

        daoBase.connector.BeginTransaction()
        Try
            For Each CODFILEMP In filiais
                Dim filial = filiaisAcaoMecanica.Where(Function(f) f.CODFILEMP = CODFILEMP).FirstOrDefault()

                Dim newPromocao As New VO.Promocao() With {
                    .TIPEDEPUBALVPMC = 4,
                    .CODFILEMP = CODFILEMP,
                    .NOMFILEMP = filial.DESABVFILEMP,
                    .CODEMP = 1,
                    .CODACOCMC = relacaoAcaoMecanicaVO.CODACOCMC,
                    .DESCMCPMC = relacaoAcaoMecanicaVO.DESRGRACOCMC,
                    .DESPMC = relacaoAcaoMecanicaVO.DESRGRACOCMC.Substring(0, IIf(relacaoAcaoMecanicaVO.DESRGRACOCMC.Length() > 60, 60, relacaoAcaoMecanicaVO.DESRGRACOCMC.Length())),
                    .TIPPMC = relacaoAcaoMecanicaVO.TIPPMC,
                    .QDEGRPMIXPMC = relacaoAcaoMecanicaVO.QDEGRPMIXPMC,
                    .QDEFXAPMC = relacaoAcaoMecanicaVO.QDEFXAPMC,
                    .INDPTCTOTMIXPMC = relacaoAcaoMecanicaVO.INDPTCTOTMIXPMC,
                    .CODUNDESRNGC = promocao.CODUNDESRNGC,
                    .NOMPMC = promocao.NOMPMC,
                    .DATINIPMC = promocao.DATINIPMC,
                    .DATFIMPMC = promocao.DATFIMPMC,
                    .INDPMCVNDSMP = promocao.INDPMCVNDSMP,
                    .INDPMCVLDAPLMVLCLI = promocao.INDPMCVLDAPLMVLCLI,
                    .INDCTRADIAVLPMC = promocao.INDCTRADIAVLPMC,
                    .INDCOBBDEPMC = promocao.INDCOBBDEPMC,
                    .INDISRBDEADI = promocao.INDISRBDEADI,
                    .QDEMAXBDECLI = promocao.QDEMAXBDECLI,
                    .FLGENVPMC = promocao.FLGENVPMC,
                    .TIPEQIVND = promocao.TIPEQIVND,
                    .DATHRAGRCPMC = DateTime.Now,
                    .CODFNCGRCPMC = ControleAcesso.CodFnc(),
                    .FLGSLCLSTPMC = "*",
                    .QDEPTOCOPVND = 0,
                    .TIPSITPMC = "I",
                    .NOMUSRSIS = funcionario.NOMUSRRCF.Trim(),
                    .INDPMCSISNVO = 1,
                    .INDBDEFRC = 0,
                    .VLRBSECALGRPMIXPMC = 0,
                    .PERVARGRPMIXPMC = 0,
                    .INDEVTSMA = promocao.INDEVTSMA,
                    .INDCPTFCH = promocao.INDCPTFCH,
                    .TIPNIVCLFCPT = promocao.TIPNIVCLFCPT,
                    .TIPNATVND = promocao.TIPNATVND,
                    .INDPMCIMD = promocao.INDPMCIMD,
                    .HRAINIPMC = promocao.HRAINIPMC,
                    .HRAFIMPMC = promocao.HRAFIMPMC,
                    .CODEVTBTB = promocao.CODEVTBTB
                }

                ' Obtem o proximo codigo (chave primaria)
                newPromocao.CODPMC = Me.GetNextCODPMC(newPromocao.TIPEDEPUBALVPMC)

                'Insere a promoção
                Me.InsertT(newPromocao, False)


                If (promocao.INDCTRADIAVLPMC = 1) Then
                    Dim criterioAdicionalAvaliacaoPromocaoBO As New CriterioAdicionalAvaliacaoPromocaoBO
                    'Insere entidade
                    If (promocao.TIPPMC = 0 Or promocao.TIPPMC = 2) Then
                        criterioAdicionalAvaliacaoPromocaoBO.InsertT(New VO.CriterioAdicionalAvaliacaoPromocao() With {
                                    .CODPMC = newPromocao.CODPMC,
                                    .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                                    .CODCTRAVLPMC = 1,
                                    .FTRACRCTRAVLPMC = criterioAdicionalAvaliacaoPromocao.FTRACRCTRAVLPMC,
                                    .VLRLIMMAXCTRAVLPMC = criterioAdicionalAvaliacaoPromocao.VLRLIMMAXCTRAVLPMC,
                                    .QDEMNMITEPEDPMC = criterioAdicionalAvaliacaoPromocao.QDEMNMITEPEDPMC
                                })
                    Else
                        criterioAdicionalAvaliacaoPromocaoBO.InsertT(New VO.CriterioAdicionalAvaliacaoPromocao() With {
                                    .CODPMC = newPromocao.CODPMC,
                                    .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                                    .CODCTRAVLPMC = 1,
                                    .FTRACRCTRAVLPMC = 0,
                                    .VLRLIMMAXCTRAVLPMC = 0,
                                    .QDEMNMITEPEDPMC = 0
                                })
                    End If
                End If



                'Insere o critéio de apuração
                For Each item In relacaoAcaoMecanicaCriterioDeApuracaoList
                    If (item.TIPEQCCALCTRPMC = 1) Then '1 = ESCALA PROGRESSIVA
                        Dim RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO As New RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO
                        RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO.InsertT(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {
                            .CODPMC = newPromocao.CODPMC,
                            .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                            .CODCTRPMC = item.CODCTRPMC,
                            .CODGRPMIXMERPMC = item.CODGRPMIXMERPMC,
                            .DESCTRMIXPMC = item.DESCTRMIXPMC,
                            .TAMIVLFXAEQCCALCTR = item.TAMIVLFXAEQCCALCTR,
                            .QDEPTOFXAEQCCALCTR = item.QDEPTOFXACTRPMC,
                            .VLRINIFXAEQCCALCTR = 0,
                            .INDTIPPTOPMC = item.INDTIPPTOPMC,
                            .VLRLIMMAXCTR = item.VLRLIMMAXCTR
                        })
                    ElseIf (item.TIPEQCCALCTRPMC = 2) Then '2 = FAIXAS FINITAS
                        Dim RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO
                        RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO.InsertT(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {
                            .CODPMC = newPromocao.CODPMC,
                            .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                            .CODCTRPMC = item.CODCTRPMC,
                            .CODGRPMIXMERPMC = item.CODGRPMIXMERPMC,
                            .DESCTRMIXPMC = item.DESCTRMIXPMC,
                            .QDEPTOFXACTRPMC = item.QDEPTOFXACTRPMC,
                            .TIPEDENIVPRMPMC = item.TIPEDENIVPRMPMC,
                            .VLRLIMMAXFXACTRPMC = item.VLRLIMMAXFXACTRPMC,
                            .VLRLIMMNMFXACTRPMC = item.VLRLIMMNMFXACTRPMC,
                            .INDTIPPTOPMC = item.INDTIPPTOPMC,
                            .VLRLIMMAXCTR = item.VLRLIMMAXCTR
                        })
                    End If
                Next


                'Insere os brindes
                For Each brinde In brindes
                    brinde.CODPMC = newPromocao.CODPMC
                    promocaoBrindeBO.InsertBrinde(brinde, False, newPromocao)
                Next

                'Se foi configurado por Região
                If (RelacaoAcaoMecanicaPublicoAlvo.INDESTADO) Then
                    Dim bo As New PromocaoPublicoAlvoEstadoBO
                    For Each item In listPublicoAlvoEstado
                        bo.InsertT(New VO.PromocaoPublicoAlvoEstado() With {
                            .CODPMC = newPromocao.CODPMC,
                            .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                            .CODESTUNI = item.CODESTUNI
                            }, False)
                    Next
                    Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = newPromocao.CODPMC, .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 1}, False)
                End If
                'Se foi configurado por Atividade do cliente
                If (RelacaoAcaoMecanicaPublicoAlvo.INDATIVIDADE) Then
                    Dim bo As New PromocaoPublicoAlvoAtividadeBO
                    For Each item In listPublicoAlvoAtividade
                        bo.InsertT(New VO.PromocaoPublicoAlvoAtividade() With {
                            .CODPMC = newPromocao.CODPMC,
                            .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                            .CODATI = item.CODATI
                            }, False)
                    Next
                    Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = newPromocao.CODPMC, .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 4}, False)
                End If
                'Se foi configurado por Grupamento de cliente
                If (RelacaoAcaoMecanicaPublicoAlvo.INDGRUPAMENTOCLIENTE) Then
                    Dim bo As New PromocaoPublicoAlvoGrupamentoClienteBO
                    For Each item In listPublicoAlvoGrupamentoCliente
                        bo.InsertT(New VO.PromocaoPublicoAlvoGrupamentoCliente() With {
                            .CODPMC = newPromocao.CODPMC,
                            .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                            .CODGRPCLI = item.CODGRPCLI
                            }, False)
                    Next
                    Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = newPromocao.CODPMC, .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 5}, False)
                End If
                'Se foi configurado por Negocio do cliente
                If (RelacaoAcaoMecanicaPublicoAlvo.INDNEGOCIOCLIENTE) Then
                    Dim bo As New PromocaoPublicoAlvoNegocioClienteBO
                    For Each item In listPublicoAlvoNegocioCliente
                        bo.InsertT(New VO.PromocaoPublicoAlvoNegocioCliente() With {
                            .CODPMC = newPromocao.CODPMC,
                            .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                            .CODUNDESRNGC = item.CODUNDESRNGC
                            }, False)
                    Next
                    Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = newPromocao.CODPMC, .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 7}, False)
                End If
                'Se foi configurado por Segmento do cliente
                If (RelacaoAcaoMecanicaPublicoAlvo.INDSEGMENTOCLIENTE) Then
                    Dim bo As New PromocaoPublicoAlvoSegmentoClienteBO
                    For Each item In listPublicoAlvoSegmentoCliente
                        bo.InsertT(New VO.PromocaoPublicoAlvoSegmentoCliente() With {
                            .CODPMC = newPromocao.CODPMC,
                            .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                            .CODSGMNGCCLI = item.CODSGMNGCCLI
                            }, False)
                    Next
                    Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = newPromocao.CODPMC, .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 8}, False)
                End If
                'Se foi configurado por cliente
                If (RelacaoAcaoMecanicaPublicoAlvo.INDCLIENTE) Then
                    Dim bo As New PromocaoPublicoAlvoClienteBO
                    For Each item In listPublicoAlvoCliente
                        bo.InsertT(New VO.PromocaoPublicoAlvoCliente() With {
                            .CODPMC = newPromocao.CODPMC,
                            .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                            .CODCLI = item.CODCLI
                            }, False)
                    Next
                    Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = newPromocao.CODPMC, .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 9}, False)
                End If
                'Se foi configurado por RCA/Vendedor - Força de vendas
                If (RelacaoAcaoMecanicaPublicoAlvo.INDRCAVENDEDOR) Then
                    Dim bo As New PromocaoPublicoAlvoRcaVendedorBO
                    For Each item In listPublicoAlvoRcaVendedor
                        bo.InsertT(New VO.PromocaoPublicoAlvoRcaVendedor() With {
                            .CODPMC = newPromocao.CODPMC,
                            .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                            .CODREP = item.CODREP
                            }, False)
                    Next
                    Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = newPromocao.CODPMC, .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 10}, False)
                End If
                'Se foi configurado por Gerente de Vendas - Força de vendas
                If (RelacaoAcaoMecanicaPublicoAlvo.INDGERENTEVENDAS) Then
                    Dim bo As New PromocaoPublicoAlvoGerenteVendasBO
                    For Each item In listPublicoAlvoGerenteVendas
                        bo.InsertT(New VO.PromocaoPublicoAlvoGerenteVendas() With {
                            .CODPMC = newPromocao.CODPMC,
                            .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                            .CODGER = item.CODGER
                            }, False)
                    Next
                    Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = newPromocao.CODPMC, .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 2}, False)
                End If
                'Se foi configurado por Gerente Mercado - Força de vendas
                If (RelacaoAcaoMecanicaPublicoAlvo.INDGERENTEMERCADO) Then
                    Dim bo As New PromocaoPublicoAlvoGerenteMercadoBO
                    For Each item In listPublicoAlvoGerenteMercado
                        bo.InsertT(New VO.PromocaoPublicoAlvoGerenteMercado() With {
                            .CODPMC = newPromocao.CODPMC,
                            .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC,
                            .CODSUP = item.CODSUP
                            }, False)
                    Next
                    Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                    promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = newPromocao.CODPMC, .TIPEDEPUBALVPMC = newPromocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 3}, False)
                End If

                'Insere o MIX
                For Each mix In listRelacaoAcaoMecanicaMix
                    promocaoMixBO.InsertT(New VO.PromocaoMix() With {
                    .CODPMC = newPromocao.CODPMC,
                    .CODGRPMER = mix.CODGRPMER,
                    .CODFMLMER = mix.CODFMLMER,
                    .CODCLSMER = mix.CODCLSMER,
                    .CODMER = mix.CODMER,
                    .CODFRN = mix.CODFRN,
                    .CODGRPMIXMERPMC = mix.CODGRPMIXMERPMC
                    }, False)
                Next
                ' Atualiza o MIX da promoção conforme os filtros configurados na mecanica
                ' Nao atualiza mais no momento da geração da promoção. Vai atualizar ao liberar a promoção
                'Dim promocaoMixVisualizarVigenteBO As New PromocaoMixVisualizarVigenteBO
                'promocaoMixVisualizarVigenteBO.AtualizaMixPromocao(newPromocao.CODPMC, newPromocao.TIPEDEPUBALVPMC, newPromocao.TIPPMC, False)

                For Each clienteExcludente In listRelacaoAcaoMecanicaClientesExcludentes
                    PromocaoClientesExcludentesBO.InsertT(New VO.PromocaoClientesExcludentes() With {
                    .CODPMC = newPromocao.CODPMC,
                    .CODCLI = clienteExcludente.CODCLI
                    }, False)
                Next

                'Insere as configurações de comunicação
                promocaoComunicacaoBO.SalvarComunicacao(newPromocao.CODPMC, comunicacao, False)


                'Atualiza a mecanica da ação
                filial.CODPMC = newPromocao.CODPMC
                relacaoAcaoMecanicaFilialBO.UpdateT(filial, False)

                promocoes.Add(newPromocao)
            Next

            daoBase.connector.Commit()
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

        Return promocoes
    End Function

    Function GetNextCODPMC(TIPEDEPUBALVPMC As Decimal) As Decimal
        'quando desativar o sistema antigo, voltar ao metodo GetCurrentCODPMC original que busca da tabela MRT.T0140126 (é so descomenta-lo e deletar este)
        Dim currentCODPMC As Decimal = DirectCast(daoBase, PromocaoDAO).GetCurrentCODPMC(TIPEDEPUBALVPMC)

        Dim existsPromocao As VO.Promocao
        Do
            currentCODPMC += 1
            If (currentCODPMC = 99999) Then
                currentCODPMC = 1
            End If

            existsPromocao = Me.SelectT(New VO.Promocao() With {.CODPMC = currentCODPMC})
        Loop Until (existsPromocao Is Nothing)

        'comentado para não atualizar mais o ultimo codigo em uso, o sistema antigo ira trabalhar de 1 a 9.999 e o novo de  1 a 99.999 
        'quando o sistema antigo for desativado ira voltar a usar a tabela MRT.T0140126 para controlar as sequencias. sendo necessario descomentar a linha abaixo
        'DirectCast(daoBase, PromocaoDAO).UpdateCurrentCODPMC(TIPEDEPUBALVPMC, currentCODPMC)

        Return currentCODPMC

    End Function

    Function AlterarDataFimPromocao(ByVal pDATFIMPMC As Date, ByVal CODPMCList As List(Of Decimal)) As String
        Dim Erros As New StringBuilder()
        'Validar se data fim nova e menor que data do dia.
        If (pDATFIMPMC < Date.Today) Then
            Erros.AppendLine("Data não pode ser menor que a data atual.")
            Return Erros.ToString()
        End If


        Dim promocoes As New List(Of VO.Promocao)

        For Each item In CODPMCList

            'Buscar promoção
            Dim promocao As VO.Promocao = daoBase.SelectT(New VO.Promocao() With {.CODPMC = item})


            If (promocao.TIPSITPMC <> " ") Then
                Erros.Append("Promoção ").Append(promocao.CODPMC).AppendLine(" não está liberada.")
                Continue For
            End If

            'Buscar Acao Comercial pelo codigo da promoção
            Dim acaoComercialBO As New AcaoComercialBO()
            Dim acaoComercial = acaoComercialBO.BuscaAcaoPorId(promocao.CODACOCMC)

            If (acaoComercial.TIPACOMCD = 5 Or acaoComercial.TIPACOMCD = 6) Then
                If (acaoComercial.ToString().Count() > 0) Then
                    If (pDATFIMPMC > acaoComercial.DATFIMACOCMC) Then
                        Erros.Append("Promoção ").Append(promocao.CODPMC).AppendLine(" não foi alterada, pois a data final solicitada é maior que a data fim da ação comercial (Pré acordo limitado por vigência ou limitado pelo valor).")
                        Continue For
                    Else
                        If promocao.QDEFXAPMC Is Nothing Then promocao.QDEFXAPMC = 0
                        If promocao.QDEMAXBDECLI Is Nothing Then promocao.QDEMAXBDECLI = 0
                        If promocao.INDISRBDEADI Is Nothing Then promocao.INDISRBDEADI = 0
                    End If
                End If
            Else
                If promocao.QDEFXAPMC Is Nothing Then promocao.QDEFXAPMC = 0
                If promocao.QDEMAXBDECLI Is Nothing Then promocao.QDEMAXBDECLI = 0
                If promocao.INDISRBDEADI Is Nothing Then promocao.INDISRBDEADI = 0

            End If

            promocao.DATFIMPMC = pDATFIMPMC
            promocoes.Add(promocao)
        Next

        If (Erros.ToString() <> String.Empty) Then
            Return Erros.ToString()
        Else
            daoBase.connector.BeginTransaction()
            Try
                For Each promocao In promocoes
                    Me.UpdateT(promocao)
                Next
                daoBase.connector.Commit()
            Catch ex As Exception
                daoBase.connector.Rollback()
                Throw ex
            End Try
            Return Nothing
        End If

    End Function


    Function Clonar(ByVal CODPMC As Decimal) As VO.Promocao


        Dim promocao As VO.Promocao = Me.SelectT(New VO.Promocao() With {.CODPMC = CODPMC})

        If (promocao Is Nothing) Then
            Throw New Exception("Promoção não encontrada!")
        End If


        Dim relacaoFuncionarioUserRacfAcountBO As New RelacaoFuncionarioUserRacfAcountBO
        Dim funcionario = relacaoFuncionarioUserRacfAcountBO.BuscaPorCodFnc(New VO.RelacaoFuncionarioUserRacfAcount() With {.CODFNC = ControleAcesso.CodFnc()})

        If (funcionario Is Nothing) Then
            Throw New Exception("Não foi possível localizar o funcionário que está criando a promoção.")
        End If

        Dim promocaoBrindeBO As New PromocaoBrindeBO
        Dim brindes As List(Of VO.PromocaoBrinde) = promocaoBrindeBO.ListT(New VO.PromocaoBrinde() With {.CODPMC = promocao.CODPMC})

        'Critério de apuração
        Dim relacaoPromocaoCriterioApuracaoFaixaFinitaBO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaBO
        Dim listRelacaoPromocaoCriterioApuracaoFaixaFinita As List(Of VO.RelacaoPromocaoCriterioApuracaoFaixaFinita) = relacaoPromocaoCriterioApuracaoFaixaFinitaBO.ListT(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {.CODPMC = promocao.CODPMC})

        Dim relacaoPromocaoCriterioApuracaoEscalaProgressivaBO As New RelacaoPromocaoCriterioApuracaoEscalaProgressivaBO
        Dim listRelacaoPromocaoCriterioApuracaoEscalaProgressiva As List(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva) = relacaoPromocaoCriterioApuracaoEscalaProgressivaBO.ListT(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {.CODPMC = promocao.CODPMC})


        'Se foi configurado por Região
        Dim promocaoPublicoAlvoEstadoBO As New PromocaoPublicoAlvoEstadoBO()
        Dim listPromocaoPublicoAlvoEstado As List(Of VO.PromocaoPublicoAlvoEstado) = promocaoPublicoAlvoEstadoBO.ListT(New VO.PromocaoPublicoAlvoEstado() With {.CODPMC = promocao.CODPMC})

        'Se foi configurado por Atividade do cliente
        Dim promocaoPublicoAlvoAtividadeBO As New PromocaoPublicoAlvoAtividadeBO()
        Dim listPromocaoPublicoAlvoAtividade As List(Of VO.PromocaoPublicoAlvoAtividade) = promocaoPublicoAlvoAtividadeBO.ListT(New VO.PromocaoPublicoAlvoAtividade() With {.CODPMC = promocao.CODPMC})

        'Se foi configurado por Grupamento de cliente
        Dim promocaoPublicoAlvoGrupamentoClienteBO As New PromocaoPublicoAlvoGrupamentoClienteBO()
        Dim listPromocaoPublicoAlvoGrupamentoCliente As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente) = promocaoPublicoAlvoGrupamentoClienteBO.ListT(New VO.PromocaoPublicoAlvoGrupamentoCliente() With {.CODPMC = promocao.CODPMC})

        'Se foi configurado por Negocio do cliente
        Dim promocaoPublicoAlvoNegocioClienteBO As New PromocaoPublicoAlvoNegocioClienteBO()
        Dim listPromocaoPublicoAlvoNegocioCliente As List(Of VO.PromocaoPublicoAlvoNegocioCliente) = promocaoPublicoAlvoNegocioClienteBO.ListT(New VO.PromocaoPublicoAlvoNegocioCliente() With {.CODPMC = promocao.CODPMC})

        'Se foi configurado por Segmento do cliente
        Dim promocaoPublicoAlvoSegmentoClienteBO As New PromocaoPublicoAlvoSegmentoClienteBO()
        Dim listPromocaoPublicoAlvoSegmentoCliente As List(Of VO.PromocaoPublicoAlvoSegmentoCliente) = promocaoPublicoAlvoSegmentoClienteBO.ListT(New VO.PromocaoPublicoAlvoSegmentoCliente() With {.CODPMC = promocao.CODPMC})

        'Se foi configurado por cliente
        Dim promocaoPublicoAlvoClienteBO As New PromocaoPublicoAlvoClienteBO()
        Dim listPromocaoPublicoAlvoCliente As List(Of VO.PromocaoPublicoAlvoCliente) = promocaoPublicoAlvoClienteBO.ListT(New VO.PromocaoPublicoAlvoCliente() With {.CODPMC = promocao.CODPMC})

        'Se foi configurado por RCA/Vendedor - Força de vendas
        Dim promocaoPublicoAlvoRcaVendedorBO As New PromocaoPublicoAlvoRcaVendedorBO()
        Dim listPromocaoPublicoAlvoRcaVendedor As List(Of VO.PromocaoPublicoAlvoRcaVendedor) = promocaoPublicoAlvoRcaVendedorBO.ListT(New VO.PromocaoPublicoAlvoRcaVendedor() With {.CODPMC = promocao.CODPMC})


        'Se foi configurado por Gerente de Vendas - Força de vendas
        Dim promocaoPublicoAlvoGerenteVendasBO As New PromocaoPublicoAlvoGerenteVendasBO()
        Dim listPromocaoPublicoAlvoGerenteVendas As List(Of VO.PromocaoPublicoAlvoGerenteVendas) = promocaoPublicoAlvoGerenteVendasBO.ListT(New VO.PromocaoPublicoAlvoGerenteVendas() With {.CODPMC = promocao.CODPMC})

        'Se foi configurado por Gerente Mercado - Força de vendas
        Dim promocaoPublicoAlvoGerenteMercadoBO As New PromocaoPublicoAlvoGerenteMercadoBO()
        Dim listPromocaoPublicoAlvoGerenteMercado As List(Of VO.PromocaoPublicoAlvoGerenteMercado) = promocaoPublicoAlvoGerenteMercadoBO.ListT(New VO.PromocaoPublicoAlvoGerenteMercado() With {.CODPMC = promocao.CODPMC})

        'CLIENTES EXCLUDENTES
        Dim promocaoClientesExcludentesBO As New PromocaoClientesExcludentesBO()
        Dim listPromocaoClientesExcludentes As List(Of VO.PromocaoClientesExcludentes) = promocaoClientesExcludentesBO.ListT(New VO.PromocaoClientesExcludentes() With {.CODPMC = promocao.CODPMC})

        'MIX
        Dim promocaoMixBO As New PromocaoMixBO()
        Dim listPromocaoMix As List(Of VO.PromocaoMix) = promocaoMixBO.ListT(New VO.PromocaoMix() With {.CODPMC = promocao.CODPMC})

        If (listPromocaoMix.Count = 0) Then
            listPromocaoMix = promocaoMixBO.CopiarMixVigentePromocao(promocao.CODPMC, promocao.TIPEDEPUBALVPMC)
        End If

        'Comunicação
        Dim promocaoComunicacaoBO As New PromocaoComunicacaoBO()
        Dim promocaoComunicacao As List(Of VO.PromocaoComunicacao) = promocaoComunicacaoBO.ListT(New VO.PromocaoComunicacao() With {.CODPMC = promocao.CODPMC})
        Dim item3 = promocaoComunicacao.Where(Function(f) f.TIPCMNPMC = 3).FirstOrDefault()
        If (Not item3 Is Nothing) Then
            Dim config = promocaoComunicacaoBO.ObterParametroPromocaoComunicacao(CODPMC).FirstOrDefault()
            If (Not config Is Nothing) Then
                item3.CODSMBDVGPMCLIVPCO = config.CODSMBDVGPMCLIVPCO
                item3.TIPDVGPMCLIVPCO = config.TIPDVGPMCLIVPCO
            End If
        End If

        'Criterio Adicional
        Dim criterioAdicionalAvaliacaoPromocaoBO As New CriterioAdicionalAvaliacaoPromocaoBO
        Dim criterioAdicionalAvaliacaoPromocao As New VO.CriterioAdicionalAvaliacaoPromocao()
        If (promocao.INDCTRADIAVLPMC = 1) Then
            criterioAdicionalAvaliacaoPromocao = criterioAdicionalAvaliacaoPromocaoBO.SelectT(New VO.CriterioAdicionalAvaliacaoPromocao() With {.CODPMC = promocao.CODPMC})
        End If


        daoBase.connector.BeginTransaction()
        Try
            ' Obtem o proximo codigo (chave primaria)
            promocao.CODFNCGRCPMC = ControleAcesso.CodFnc()
            promocao.TIPSITPMC = "I"
            promocao.CODPMC = Me.GetNextCODPMC(promocao.TIPEDEPUBALVPMC)
            promocao.DATHRAGRCPMC = DateTime.Now
            'Insere a promoção
            Me.InsertT(promocao, False)

            'Insere os brindes
            For Each brinde In brindes.Where(Function(f) f.DATECSPRD Is Nothing).ToList()
                brinde.CODPMC = promocao.CODPMC
                promocaoBrindeBO.InsertBrinde(brinde, False, promocao)
            Next

            'Insere o critéio de apuração

            For Each item In listRelacaoPromocaoCriterioApuracaoFaixaFinita
                item.CODPMC = promocao.CODPMC
                relacaoPromocaoCriterioApuracaoFaixaFinitaBO.InsertT(item, False)
            Next

            For Each item In listRelacaoPromocaoCriterioApuracaoEscalaProgressiva
                item.CODPMC = promocao.CODPMC
                relacaoPromocaoCriterioApuracaoEscalaProgressivaBO.InsertT(item, False)
            Next

            'Insere Criterio Adicional
            If (promocao.INDCTRADIAVLPMC = 1) Then
                criterioAdicionalAvaliacaoPromocao.CODPMC = promocao.CODPMC
                criterioAdicionalAvaliacaoPromocaoBO.InsertT(criterioAdicionalAvaliacaoPromocao, False)
            End If

            'Se foi configurado por Região
            For Each item In listPromocaoPublicoAlvoEstado
                item.CODPMC = promocao.CODPMC
                promocaoPublicoAlvoEstadoBO.InsertT(item, False)
            Next
            If (listPromocaoPublicoAlvoEstado.Count > 0) Then
                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 1}, False)
            End If

            'Se foi configurado por Atividade do cliente
            For Each item In listPromocaoPublicoAlvoAtividade
                item.CODPMC = promocao.CODPMC
                promocaoPublicoAlvoAtividadeBO.InsertT(item, False)
            Next
            If (listPromocaoPublicoAlvoAtividade.Count > 0) Then
                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 4}, False)
            End If

            'Se foi configurado por Grupamento de cliente
            For Each item In listPromocaoPublicoAlvoGrupamentoCliente
                item.CODPMC = promocao.CODPMC
                promocaoPublicoAlvoGrupamentoClienteBO.InsertT(item, False)
            Next
            If (listPromocaoPublicoAlvoGrupamentoCliente.Count > 0) Then
                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 5}, False)
            End If

            'Se foi configurado por Negocio do cliente
            For Each item In listPromocaoPublicoAlvoNegocioCliente
                item.CODPMC = promocao.CODPMC
                promocaoPublicoAlvoNegocioClienteBO.InsertT(item, False)
            Next
            If (listPromocaoPublicoAlvoNegocioCliente.Count > 0) Then
                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 7}, False)
            End If

            'Se foi configurado por Segmento do cliente
            For Each item In listPromocaoPublicoAlvoSegmentoCliente
                item.CODPMC = promocao.CODPMC
                promocaoPublicoAlvoSegmentoClienteBO.InsertT(item, False)
            Next
            If (listPromocaoPublicoAlvoSegmentoCliente.Count > 0) Then
                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 8}, False)
            End If

            'Se foi configurado por cliente
            For Each item In listPromocaoPublicoAlvoCliente
                item.CODPMC = promocao.CODPMC
                promocaoPublicoAlvoClienteBO.InsertT(item, False)
            Next
            If (listPromocaoPublicoAlvoCliente.Count > 0) Then
                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 9}, False)
            End If

            'Se foi configurado por RCA/Vendedor - Força de vendas
            For Each item In listPromocaoPublicoAlvoRcaVendedor
                item.CODPMC = promocao.CODPMC
                promocaoPublicoAlvoRcaVendedorBO.InsertT(item, False)
            Next
            If (listPromocaoPublicoAlvoRcaVendedor.Count > 0) Then
                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 10}, False)
            End If


            'Se foi configurado por Gerente de Vendas - Força de vendas
            For Each item In listPromocaoPublicoAlvoGerenteVendas
                item.CODPMC = promocao.CODPMC
                promocaoPublicoAlvoGerenteVendasBO.InsertT(item, False)
            Next
            If (listPromocaoPublicoAlvoGerenteVendas.Count > 0) Then
                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 2}, False)
            End If


            'Se foi configurado por Gerente Mercado - Força de vendas
            For Each item In listPromocaoPublicoAlvoGerenteMercado
                item.CODPMC = promocao.CODPMC
                promocaoPublicoAlvoGerenteMercadoBO.InsertT(item, False)
            Next
            If (listPromocaoPublicoAlvoGerenteMercado.Count > 0) Then
                Dim promocaoPublicoAlvoSelecaoBO As New PromocaoPublicoAlvoSelecaoBO()
                promocaoPublicoAlvoSelecaoBO.InsertT(New VO.PromocaoPublicoAlvoSelecao() With {.CODPMC = promocao.CODPMC, .TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC, .CODCTRSELPUBALVPMC = 3}, False)
            End If

            'Insere Clientes Excludentes
            For Each clientesExcludentes In listPromocaoClientesExcludentes
                clientesExcludentes.CODPMC = promocao.CODPMC
                promocaoClientesExcludentesBO.InsertT(clientesExcludentes, False)
            Next

            'Insere o MIX
            For Each mix In listPromocaoMix
                mix.CODPMC = promocao.CODPMC
                mix.INDVLDRGRMIXPMC = 0
                promocaoMixBO.InsertT(mix, False)
            Next
            '' Atualiza o MIX da promoção conforme os filtros configurados na mecanica
            'Não atualiza mais ao gerar a promoção, vai atualizar ao liberar a promoção
            'Dim promocaoMixVisualizarVigenteBO As New PromocaoMixVisualizarVigenteBO
            'promocaoMixVisualizarVigenteBO.AtualizaMixPromocao(promocao.CODPMC, promocao.TIPEDEPUBALVPMC, promocao.TIPPMC, False)

            'Insere as configurações de comunicação
            promocaoComunicacaoBO.SalvarComunicacao(promocao.CODPMC, promocaoComunicacao, False)


            ''Atualiza a mecanica da ação
            'filial.CODPMC = promocao.CODPMC
            'RelacaoAcaoMecanicaFilialBO.UpdateT(filial, False)


            daoBase.connector.Commit()

            Return promocao
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

        Return Nothing
    End Function



    Public Function ClonarPromocoesDaAcao(ByVal CODACOCMC As Decimal, ByRef mensagens As String) As List(Of VO.Promocao)


        Dim promocoes As New List(Of VO.Promocao)

        Dim acaoComercialBO As New AcaoComercialBO()
        Dim acaoComercialClone = acaoComercialBO.SelectT(New VO.AcaoComercial() With {.CODACOCMC = CODACOCMC})

        If (acaoComercialClone.CODACOCMCCPI Is Nothing OrElse acaoComercialClone.CODACOCMCCPI = 0) Then
            Throw New Exception("Esta ação comercial não é um clone, portando não é possivel criar a promoção usando o metodo de clone da Ação Comercial.")
        End If


        Dim relacaoAcaoMecanicaBO As New RelacaoAcaoMecanicaBO()
        Dim relacaoAcaoMecanicaClone = relacaoAcaoMecanicaBO.ListT(New VO.RelacaoAcaoMecanica() With {.CODACOCMC = CODACOCMC})

        Dim relacaoAcaoMecanicaFilialBO As New RelacaoAcaoMecanicaFilialBO()

        For Each item In relacaoAcaoMecanicaClone
            Dim filiaisClone = relacaoAcaoMecanicaFilialBO.ListT(New VO.RelacaoAcaoMecanicaFilial() With {.CODACOCMC = item.CODACOCMC, .NUMSEQRGRACOCMC = item.NUMSEQRGRACOCMC})

            For Each filial In filiaisClone
                If (filial.CODPMC Is Nothing) Then

                    Dim filialOriginal = relacaoAcaoMecanicaFilialBO.SelectT(New VO.RelacaoAcaoMecanicaFilial() With {.NUMSEQRGRACOCMC = filial.NUMSEQRGRACOCMC, .CODACOCMC = acaoComercialClone.CODACOCMCCPI, .CODFILEMP = filial.CODFILEMP})

                    If (filialOriginal Is Nothing) Or (filialOriginal.CODPMC Is Nothing) Then
                        mensagens += String.Format("Mecânica: {0} Filial: {1}: Não possui mecânica para esta filial na ação comercial original<br/>", item.DESRGRACOCMC, filial.CODFILEMP)
                    Else
                        Try
                            Dim clone = Me.Clonar(filialOriginal.CODPMC)
                            'atualiza o clone com o codigo da promoção
                            clone.CODACOCMC = CODACOCMC
                            MyBase.UpdateT(clone)

                            'Atualiza a relação acao mecanica com o codigo da promoçao gerada
                            filial.CODPMC = clone.CODPMC
                            relacaoAcaoMecanicaFilialBO.UpdateT(filial)

                            'adiciona a promoção na lista de promoções criadas
                            promocoes.Add(clone)
                        Catch ex As Exception
                            mensagens += String.Format("Mecânica: {0} Filial: {1}: {2}<br/>", item.DESRGRACOCMC, filial.CODFILEMP, ex.Message)
                        End Try
                    End If
                End If
            Next
        Next

        Return promocoes
    End Function



    Public Function ListarPromocaoPorAcaoMecanica(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.Promocao)
        Return DirectCast(daoBase, PromocaoDAO).ListarPromocaoPorAcaoMecanica(CODACOCMC, NUMSEQRGRACOCMC)
    End Function



    Public Function ReabrirPromocao(ByVal CODPMC As Decimal, ByRef mensagem As String) As VO.Promocao

        Dim promocao = Me.SelectT(New VO.Promocao With {.CODPMC = CODPMC})

        If promocao Is Nothing Then
            mensagem = "Promoção não encontrada"
            Return Nothing
        End If

        If promocao.TIPSITPMC = "E" Then
            mensagem = "Promoção Encerrada"
            Return Nothing
        End If

        If promocao.TIPSITPMC.Trim <> String.Empty Then
            mensagem = "É permitido abrir somente promoção Liberada"
            Return Nothing
        End If

        If promocao.DATINIPMC <= Date.Now Then
            mensagem = "É permitido abrir promoção com data de inicio posterior a data atual "
            Return Nothing
        End If

        promocao.TIPSITPMC = "I"

        Try
            daoBase.connector.BeginTransaction()

            Me.UpdateT(promocao, False)

            Dim promocaoMixBO As New PromocaoMixBO()
            promocaoMixBO.UpdateIndicadorValidadeRegraMIX(CODPMC, 0, False)

            Dim promocaoMixVisualizarVigenteBO As New PromocaoMixVisualizarVigenteBO()
            promocaoMixVisualizarVigenteBO.DeleteT(New VO.PromocaoMixVisualizarVigente() With {.CODPMC = CODPMC}, False)

            daoBase.connector.Commit()
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw (ex)
        End Try

        Return promocao

    End Function

    Function SuspenderPromocaoCashback(ByVal CODPMC As Decimal) As Boolean
        Try
            Dim promocao As VO.Promocao = daoBase.SelectT(New VO.Promocao() With {.CODPMC = CODPMC})
            promocao.TIPSITPMC = "S"

            daoBase.connector.BeginTransaction()
            Me.UpdateT(promocao)

            'Inserir Histórico
            Dim promocaoHistoricoSituacao As New PromocaoHistoricoSituacaoBO()
            promocaoHistoricoSituacao.InsertT(New VO.PromocaoHistoricoSituacao With {.CODPMC = CODPMC, .CODFNC = ControleAcesso.CodFnc(), .DATGRCHST = Date.Now, .TIPSITPMC = promocao.TIPSITPMC})

            daoBase.connector.Commit()
            Return True
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try
    End Function

    Function RetirarSuspensao(ByVal CODPMC As Decimal) As Boolean
        Try
            Dim promocao As VO.Promocao = daoBase.SelectT(New VO.Promocao() With {.CODPMC = CODPMC})
            promocao.TIPSITPMC = " "

            daoBase.connector.BeginTransaction()
            Me.UpdateT(promocao, False)

            'Inserir Histórico
            Dim promocaoHistoricoSituacao As New PromocaoHistoricoSituacaoBO()
            promocaoHistoricoSituacao.InsertT(New VO.PromocaoHistoricoSituacao With {.CODPMC = CODPMC, .CODFNC = ControleAcesso.CodFnc(), .DATGRCHST = Date.Now, .TIPSITPMC = promocao.TIPSITPMC})

            daoBase.connector.Commit()

            Return True
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try
    End Function

    Function ValidarParametrizacao(ByVal list As List(Of PromocaoParametrosMixViewModel), ByVal TIPFRNACOCMC As Decimal) As ValidacaoParametrosMixViewModel
        Dim result As ValidacaoParametrosMixViewModel = New ValidacaoParametrosMixViewModel()

        Try
            'Não é permitido mais que 12 itens obrigatorios na promoção.
            Dim promocaoMixBO As New PromocaoMixBO()
            Dim itensObrigatorios = promocaoMixBO.VerificaLimiteItensObrigatorios(list)
            If (itensObrigatorios > 12) Then
                result.Mensagem += "Não é permitido mais que 12 itens obrigatorios na promoção."
                Return result
            End If

            Dim parametros As List(Of VO.Promocao) = DirectCast(daoBase, PromocaoDAO).ListarPromocaoParaParametrizacao(list.FirstOrDefault().CODPMC)
            If (IsNothing(parametros) Or parametros.Count = 0) Then
                Return result
            End If

            For Each item In list
                Dim parametro As VO.Promocao = parametros.FirstOrDefault(Function(f) f.CODMER = item.CODMER)
                If (parametro IsNot Nothing) Then
                    If (TIPFRNACOCMC = 0 AndAlso item.INDITEOBR = 0) Then
                        Continue For
                    End If

                    If (item.QDEMNMMERMIX < parametro.QDEMNMVNDMER) Then
                        result.Mensagem += "Quantidade minima para a mercadoria " & item.CODMER & " deve ser maior ou igual a " & parametro.QDEMNMVNDMER & " " & Environment.NewLine
                        list = list.Where(Function(f) f.CODMER <> item.CODMER).ToList()
                        Continue For
                    End If

                    If (parametro.INDQDEMPLVNDMER = 1) Then
                        If (item.QDEMNMMERMIX Mod parametro.QDEMNMVNDMER > 0) Then
                            result.Mensagem += "Quantidade minima para a mercadoria " & item.CODMER & " deve ser multiplia de " & parametro.QDEMNMVNDMER & " " & Environment.NewLine
                            list = list.Where(Function(f) f.CODMER <> item.CODMER).ToList()
                            Continue For
                        End If

                        If (item.QDEMNMMERMIX Mod parametro.QDEMNMVNDMER > 0) Then
                            result.Mensagem += "Quantidade máxima para a mercadoria " & item.CODMER & " deve ser multiplia de " & parametro.QDEMNMVNDMER & " " & Environment.NewLine
                            list = list.Where(Function(f) f.CODMER <> item.CODMER).ToList()
                            Continue For
                        End If
                    End If
                End If
            Next

            result.List = list
            Return result
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

    End Function

    Function ValidarParametrizacaoCash(ByVal list As List(Of PromocaoParametrosMixViewModel), ByVal TIPFRNACOCMC As Decimal) As ValidacaoParametrosMixViewModel
        Dim result As ValidacaoParametrosMixViewModel = New ValidacaoParametrosMixViewModel()

        Try
            'Não é permitido mais que 12 itens obrigatorios na promoção.
            Dim promocaoMixBO As New PromocaoMixBO()
            Dim itensObrigatorios = promocaoMixBO.VerificaLimiteItensObrigatorios(list)
            If (itensObrigatorios > 12) Then
                result.Mensagem += "Não é permitido mais que 12 itens obrigatorios na promoção."
                Return result
            End If

            Dim parametros As List(Of VO.Promocao) = DirectCast(daoBase, PromocaoDAO).ListarPromocaoParaParametrizacao(list.FirstOrDefault().CODPMC)
            If (IsNothing(parametros) Or parametros.Count = 0) Then
                Return result
            End If

            For Each item In list
                Dim parametro As VO.Promocao = parametros.FirstOrDefault(Function(f) f.CODMER = item.CODMER)
                If (parametro IsNot Nothing) Then
                    If (TIPFRNACOCMC = 0 AndAlso item.INDITEOBR = 0) Then
                        Continue For
                    End If

                    If (item.QDEMNMMERMIX < parametro.QDEMNMVNDMER) Then
                        result.Mensagem += "Quantidade minima para a mercadoria " & item.CODMER & " deve ser maior ou igual a " & parametro.QDEMNMVNDMER & " " & Environment.NewLine
                        list = list.Where(Function(f) f.CODMER <> item.CODMER).ToList()
                        Continue For
                    End If

                    If (parametro.INDQDEMPLVNDMER = 1) Then
                        If (item.QDEMNMMERMIX Mod parametro.QDEMNMVNDMER > 0) Then
                            result.Mensagem += "Quantidade minima para a mercadoria " & item.CODMER & " deve ser multiplia de " & parametro.QDEMNMVNDMER & " " & Environment.NewLine
                            list = list.Where(Function(f) f.CODMER <> item.CODMER).ToList()
                            Continue For
                        End If

                        If (item.QDEMNMMERMIX Mod parametro.QDEMNMVNDMER > 0) Then
                            result.Mensagem += "Quantidade máxima para a mercadoria " & item.CODMER & " deve ser multiplia de " & parametro.QDEMNMVNDMER & " " & Environment.NewLine
                            list = list.Where(Function(f) f.CODMER <> item.CODMER).ToList()
                            Continue For
                        End If
                    End If
                End If
            Next

            result.List = list
            Return result
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

    End Function
    Public Function VerificaSeCashBack(CODPMC As Decimal) As VO.Promocao
        Return DirectCast(daoBase, PromocaoDAO).VerificaSeCashBack(CODPMC)
    End Function

End Class
