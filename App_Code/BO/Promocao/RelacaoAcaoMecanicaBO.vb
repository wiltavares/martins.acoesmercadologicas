﻿Imports Core
Imports VO

Public Class RelacaoAcaoMecanicaBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanica)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaDAO()
    End Sub

    Public Sub DeleteRelacaoAcaoMecanica(ByVal vo As VO.RelacaoAcaoMecanica)
        MyBase.daoBase.connector.BeginTransaction()
        Try
            Dim DAO As RelacaoAcaoMecanicaDAO = New RelacaoAcaoMecanicaDAO()

            DAO.DeleteRelacaoAcaoMecanica(vo)
            DAO.DeleteRelacaoAcaoMecanicaEstado(vo)
            DAO.DeleteRelacaoAcaoMecanicaAtividade(vo)
            DAO.DeleteRelacaoAcaoMecanicaGrupementoCliente(vo)
            DAO.DeleteRelacaoAcaoMecanicaNegocioCliente(vo)
            DAO.DeleteRelacaoAcaoMecanicaSeguimento(vo)
            DAO.DeleteRelacaoAcaoMecanicaCliente(vo)
            DAO.DeleteRelacaoAcaoMecanicaRcaVendedor(vo)
            DAO.DeleteRelacaoAcaoMecanicaGerenteVendas(vo)
            DAO.DeleteRelacaoAcaoMecanicaGerenteMercado(vo)
            DAO.DeleteRelacaoAcaoMecanicaMix(vo)
            DAO.DeleteRelacaoAcaoMecanicaCriterioDeApuracao(vo)

            MyBase.daoBase.connector.Commit()
        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
        End Try


    End Sub


    Public Function SelectEditMecanicaGerais(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As VO.RelacaoAcaoMecanica
        Return DirectCast(daoBase, RelacaoAcaoMecanicaDAO).SelectEditMecanicaGerais(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Function Save(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, vo As VO.RelacaoAcaoMecanica) As VO.RelacaoAcaoMecanica

        MyBase.daoBase.connector.BeginTransaction()
        Try
            vo.DESRGRACOCMC = vo.DESRGRACOCMC.Replace(vbCrLf, " ")
            Dim BOFilial As RelacaoAcaoMecanicaFilialBO = New RelacaoAcaoMecanicaFilialBO()
            If (NUMSEQRGRACOCMC = 0) Then
                NUMSEQRGRACOCMC = DirectCast(MyBase.daoBase, RelacaoAcaoMecanicaDAO).SelectMax(CODACOCMC).NUMSEQRGRACOCMC
                vo.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC

                MyBase.daoBase.InsertT(vo)
            Else
                MyBase.daoBase.UpdateT(vo)
            End If

            BOFilial.SaveList(CODACOCMC, NUMSEQRGRACOCMC, vo.RelacaoAcaoMecanicaFilialList)


            Dim BOCriterioDeApuracao = New RelacaoAcaoMecanicaCriterioDeApuracaoBO()

            Dim TIPPMC = vo.TIPPMC

            If (vo.RelacaoAcaoMecanicaCriterioDeApuracaoList IsNot Nothing) Then
                BOCriterioDeApuracao.SaveList(CODACOCMC, NUMSEQRGRACOCMC, TIPPMC, vo.RelacaoAcaoMecanicaCriterioDeApuracaoList)
            End If

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
        End Try

        Return vo

    End Function

    Public Function EditRelacaoAcaoMecanicaCriterioDeApuracaoList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanica)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaDAO).EditRelacaoAcaoMecanicaCriterioDeApuracaoList(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Function EditRelacaoAcaoMecanicaCriterioDeApuracao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As VO.RelacaoAcaoMecanica
        Return DirectCast(daoBase, RelacaoAcaoMecanicaDAO).EditRelacaoAcaoMecanicaCriterioDeApuracao(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Function ListPromocoesPendenteGeracao() As List(Of VO.RelacaoAcaoMecanica)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaDAO).ListPromocoesPendenteGeracao()
    End Function

    Public Function ListPromocoesPendenteGeracao(ByVal CODACOCMC As Decimal?, ByVal NOMACOCMC As String, ByVal DATINIACOCMC As DateTime?, ByVal DATFIMACOCMC As DateTime?, ByVal CODFNC As Decimal?) As List(Of VO.RelacaoAcaoMecanica)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaDAO).ListPromocoesPendenteGeracao(CODACOCMC, NOMACOCMC, DATINIACOCMC, DATFIMACOCMC, CODFNC)
    End Function


    Public Function PesquisarAcaoMecanica(ByVal filtro As VO.RelacaoAcaoMecanica) As List(Of VO.RelacaoAcaoMecanica)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaDAO).PesquisarAcaoMecanica(filtro)
    End Function

    Public Function ClonarMecanica(ByVal CODACOCMC_Copy As Decimal, ByVal NUMSEQRGRACOCMC_Copy As Decimal, ByVal CODACOCMC_Paste As Decimal) As RelacaoAcaoMecanica


        Dim acaoMecanica As VO.RelacaoAcaoMecanica = Me.SelectT(New VO.RelacaoAcaoMecanica() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})

        acaoMecanica.CODACOCMC = CODACOCMC_Paste
        acaoMecanica.NUMSEQRGRACOCMC = 0

        Dim filialBO As RelacaoAcaoMecanicaFilialBO = New RelacaoAcaoMecanicaFilialBO()
        acaoMecanica.RelacaoAcaoMecanicaFilialList = filialBO.ListT(New VO.RelacaoAcaoMecanicaFilial() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})
        For Each item In acaoMecanica.RelacaoAcaoMecanicaFilialList
            item.CODACOCMC = CODACOCMC_Paste
            item.NUMSEQRGRACOCMC = 0
            item.CODPMC = Nothing
        Next


        Dim criterioDeApuracaoBO = New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
        acaoMecanica.RelacaoAcaoMecanicaCriterioDeApuracaoList = criterioDeApuracaoBO.ListT(New VO.RelacaoAcaoMecanicaCriterioDeApuracao() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})
        For Each item In acaoMecanica.RelacaoAcaoMecanicaCriterioDeApuracaoList
            item.CODACOCMC = CODACOCMC_Paste
            item.NUMSEQRGRACOCMC = 0
        Next


        Dim mixBO = New RelacaoAcaoMecanicaMixBO()
        acaoMecanica.relacaoAcaoMecanicaMixList = mixBO.ListT(New RelacaoAcaoMecanicaMix() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})




        Dim publicoAlvoEstadoBO = New RelacaoAcaoMecanicaPublicoAlvoEstadoBO()
        Dim publicoAlvoEstado = publicoAlvoEstadoBO.ListT(New RelacaoAcaoMecanicaPublicoAlvoEstado() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})

        Dim publicoAlvoAtividadeBO = New RelacaoAcaoMecanicaPublicoAlvoAtividadeBO()
        Dim publicoAlvoAtividade = publicoAlvoAtividadeBO.ListT(New RelacaoAcaoMecanicaPublicoAlvoAtividade() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})

        Dim publicoAlvoGrupamentoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO()
        Dim publicoAlvoGrupamentoCliente = publicoAlvoGrupamentoClienteBO.ListT(New RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})

        Dim publicoAlvoNegocioClienteBO = New RelacaoAcaoMecanicaPublicoAlvoNegocioClienteBO()
        Dim publicoAlvoNegocioCliente = publicoAlvoNegocioClienteBO.ListT(New RelacaoAcaoMecanicaPublicoAlvoNegocioCliente() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})

        Dim publicoAlvoSegmentoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteBO()
        Dim publicoAlvoSegmentoCliente = publicoAlvoSegmentoClienteBO.ListT(New RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})

        Dim publicoAlvoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoClienteBO()
        Dim publicoAlvoCliente = publicoAlvoClienteBO.ListT(New RelacaoAcaoMecanicaPublicoAlvoCliente() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})

        Dim publicoAlvoRcaVendedorBO = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO()
        Dim publicoAlvoRcaVendedor = publicoAlvoRcaVendedorBO.ListT(New RelacaoAcaoMecanicaPublicoAlvoRcaVendedor() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})

        Dim publicoAlvoGerenteVendasBO = New RelacaoAcaoMecanicaPublicoAlvoGerenteVendasBO()
        Dim publicoAlvoGerenteVendas = publicoAlvoGerenteVendasBO.ListT(New RelacaoAcaoMecanicaPublicoAlvoGerenteVendas() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})

        Dim publicoAlvoGerenteMercadoBO = New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO()
        Dim publicoAlvoGerenteMercado = publicoAlvoGerenteMercadoBO.ListT(New RelacaoAcaoMecanicaPublicoAlvoGerenteMercado() With {.CODACOCMC = CODACOCMC_Copy, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC_Copy})


        MyBase.daoBase.connector.BeginTransaction()
        Try

            acaoMecanica.NUMSEQRGRACOCMC = DirectCast(MyBase.daoBase, RelacaoAcaoMecanicaDAO).SelectMax(CODACOCMC_Paste).NUMSEQRGRACOCMC

            MyBase.daoBase.InsertT(acaoMecanica)

            filialBO.SaveList(CODACOCMC_Paste, acaoMecanica.NUMSEQRGRACOCMC, acaoMecanica.RelacaoAcaoMecanicaFilialList)
            criterioDeApuracaoBO.SaveList(CODACOCMC_Paste, acaoMecanica.NUMSEQRGRACOCMC, acaoMecanica.TIPPMC, acaoMecanica.RelacaoAcaoMecanicaCriterioDeApuracaoList)

            For Each item In acaoMecanica.relacaoAcaoMecanicaMixList
                item.CODACOCMC = CODACOCMC_Paste
                item.NUMSEQRGRACOCMC = acaoMecanica.NUMSEQRGRACOCMC
                item.NUMSEQMIXACOCMC = mixBO.SelectMaxNumSeqMixAcoCmc(CODACOCMC_Paste, acaoMecanica.NUMSEQRGRACOCMC).NUMSEQMIXACOCMC
                mixBO.InsertT(item)
            Next

            For Each item In publicoAlvoEstado
                item.CODACOCMC = CODACOCMC_Paste
                item.NUMSEQRGRACOCMC = acaoMecanica.NUMSEQRGRACOCMC
                publicoAlvoEstadoBO.InsertT(item)
            Next

            For Each item In publicoAlvoAtividade
                item.CODACOCMC = CODACOCMC_Paste
                item.NUMSEQRGRACOCMC = acaoMecanica.NUMSEQRGRACOCMC
                publicoAlvoAtividadeBO.InsertT(item)
            Next

            For Each item In publicoAlvoGrupamentoCliente
                item.CODACOCMC = CODACOCMC_Paste
                item.NUMSEQRGRACOCMC = acaoMecanica.NUMSEQRGRACOCMC
                publicoAlvoGrupamentoClienteBO.InsertT(item)
            Next

            For Each item In publicoAlvoNegocioCliente
                item.CODACOCMC = CODACOCMC_Paste
                item.NUMSEQRGRACOCMC = acaoMecanica.NUMSEQRGRACOCMC
                publicoAlvoNegocioClienteBO.InsertT(item)
            Next

            For Each item In publicoAlvoSegmentoCliente
                item.CODACOCMC = CODACOCMC_Paste
                item.NUMSEQRGRACOCMC = acaoMecanica.NUMSEQRGRACOCMC
                publicoAlvoSegmentoClienteBO.InsertT(item)
            Next

            For Each item In publicoAlvoCliente
                item.CODACOCMC = CODACOCMC_Paste
                item.NUMSEQRGRACOCMC = acaoMecanica.NUMSEQRGRACOCMC
                publicoAlvoClienteBO.InsertT(item)
            Next

            For Each item In publicoAlvoRcaVendedor
                item.CODACOCMC = CODACOCMC_Paste
                item.NUMSEQRGRACOCMC = acaoMecanica.NUMSEQRGRACOCMC
                publicoAlvoRcaVendedorBO.InsertT(item)
            Next

            For Each item In publicoAlvoGerenteVendas
                item.CODACOCMC = CODACOCMC_Paste
                item.NUMSEQRGRACOCMC = acaoMecanica.NUMSEQRGRACOCMC
                publicoAlvoGerenteVendasBO.InsertT(item)
            Next

            For Each item In publicoAlvoGerenteMercado
                item.CODACOCMC = CODACOCMC_Paste
                item.NUMSEQRGRACOCMC = acaoMecanica.NUMSEQRGRACOCMC
                publicoAlvoGerenteMercadoBO.InsertT(item)
            Next

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
        End Try

        Return acaoMecanica
    End Function
End Class