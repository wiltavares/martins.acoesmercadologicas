﻿Imports AutoMapper
Imports Core

Public Class CriterioApuracaoBO
    Inherits BOBase(Of VO.CriterioApuracao)

    Public Sub New()
        MyBase.daoBase = New CriterioApuracaoDAO()
    End Sub
    ''' <summary>
    ''' Sistema Antigo : AtualizaDataSet
    ''' </summary>
    ''' <param name="criterioViewModel"></param>
    ''' <returns></returns>
    Public Function InserOrUpdate(criterioViewModel As PromocaoCriterioApuracaoViewModel, Optional startTransaction As Boolean = True) As Boolean
        If (startTransaction) Then
            daoBase.connector.BeginTransaction()
        End If
        Try
            If (criterioViewModel.CodFormaCalculo = "2") Then 'FAIXA FINITAS

                'AtualizaEscalaProgressivaT0107951

                'Dim RelacaoCriterioDAO As New RelacaoCriterioPromocaoPromocaoTipoEntidadeDAO
                Dim RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO
                Dim RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO As New RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO

                'Dim ListRelacaoCriterio As List(Of VO.RelacaoCriterioPromocaoPromocaoTipoEntidade) = RelacaoCriterioDAO.SelectRel(New VO.RelacaoCriterioPromocaoPromocaoTipoEntidade() With {.CODPMC = criterioViewModel.CODPMC, .TIPEDEPUBALVPMC = 4})

                Dim ListRelacaoPromocaoCriterioApuracaoFaixaFinita = RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO.BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {.CODPMC = criterioViewModel.CODPMC})
                Dim ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva = RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO.BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {.CODPMC = criterioViewModel.CODPMC})

                'Delete T0107960
                ListRelacaoPromocaoCriterioApuracaoFaixaFinita.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO.DeleteT(obj))

                'Delete T0107951
                ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO.DeleteT(obj))

                Dim parametrosCalculo As List(Of VO.RelacaoPromocaoCriterioApuracaoFaixaFinita)
                parametrosCalculo = Mapper.Map(Of List(Of RelacaoPromocaoCriterioApuracaoParamCalculoViewModel), List(Of VO.RelacaoPromocaoCriterioApuracaoFaixaFinita))(criterioViewModel.ParametrosCalculo)

                Dim QDEPTO As Decimal?
                Dim DefaultMaxVal As Decimal

                DefaultMaxVal = If(criterioViewModel.CODCTRPMC = 1, 9999999999.99, 9999999999)
                QDEPTO = criterioViewModel.QDEPTOFXACTRPMC

                If (criterioViewModel.Combinada) Then
                    parametrosCalculo.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO.InsertT(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {
                    .TIPEDEPUBALVPMC = 4,
                    .CODPMC = criterioViewModel.CODPMC,
                    .CODCTRPMC = obj.CODCTRPMC,
                    .VLRLIMMNMFXACTRPMC = obj.VLRLIMMNMFXACTRPMC,
                    .DESCTRMIXPMC = obj.DESCTRMIXPMC,
                    .VLRLIMMAXFXACTRPMC = DefaultMaxVal,
                    .TIPEDENIVPRMPMC = 1,
                    .CODGRPMIXMERPMC = obj.CODGRPMIXMERPMC,
                    .QDEPTOFXACTRPMC = QDEPTO
                    }))
                Else
                    parametrosCalculo.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO.InsertT(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {
                    .TIPEDEPUBALVPMC = 4,
                    .CODPMC = criterioViewModel.CODPMC,
                    .CODCTRPMC = If(obj.CODCTRPMC IsNot Nothing, obj.CODCTRPMC, criterioViewModel.CODCTRPMC),
                    .VLRLIMMNMFXACTRPMC = obj.VLRLIMMNMFXACTRPMC,
                    .DESCTRMIXPMC = " ",
                    .VLRLIMMAXFXACTRPMC = If(obj.VLRLIMMAXFXACTRPMC IsNot Nothing, obj.VLRLIMMAXFXACTRPMC, DefaultMaxVal),
                    .TIPEDENIVPRMPMC = obj.TIPEDENIVPRMPMC,
                    .CODGRPMIXMERPMC = 0,
                    .QDEPTOFXACTRPMC = QDEPTO
                    }))
                End If

                'AtualizaEscalaProgressivaT0107960





            ElseIf (criterioViewModel.CodFormaCalculo = "1") Then 'T0107960
                'AtualizaEscalaProgressivaT0107951

                'Dim RelacaoCriterioDAO As New RelacaoCriterioPromocaoPromocaoTipoEntidadeDAO
                Dim RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO As New RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO
                Dim RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO As New RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO

                'Dim ListRelacaoCriterio As List(Of VO.RelacaoCriterioPromocaoPromocaoTipoEntidade) = RelacaoCriterioDAO.SelectRel(New VO.RelacaoCriterioPromocaoPromocaoTipoEntidade() With {.CODPMC = criterioViewModel.CODPMC, .TIPEDEPUBALVPMC = 4})

                Dim ListRelacaoPromocaoCriterioApuracaoFaixaFinita = RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO.BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(New VO.RelacaoPromocaoCriterioApuracaoFaixaFinita() With {.CODPMC = criterioViewModel.CODPMC})
                Dim ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva = RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO.BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {.CODPMC = criterioViewModel.CODPMC})

                'Delete T0107951
                ListRelacaoPromocaoCriterioApuracaoEscalaProgressiva.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO.DeleteT(obj))

                'Delete T0107960
                ListRelacaoPromocaoCriterioApuracaoFaixaFinita.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO.DeleteT(obj))

                Dim parametrosCalculo As List(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva)
                parametrosCalculo = Mapper.Map(Of List(Of RelacaoPromocaoCriterioApuracaoParamCalculoViewModel), List(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva))(criterioViewModel.ParametrosCalculo)

                If (criterioViewModel.Combinada) Then
                    parametrosCalculo.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO.InsertT(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {
                    .TIPEDEPUBALVPMC = 4,
                    .CODPMC = criterioViewModel.CODPMC,
                    .CODGRPMIXMERPMC = obj.CODGRPMIXMERPMC,
                    .QDEPTOFXAEQCCALCTR = criterioViewModel.QDEPTOFXAEQCCALCTR,
                    .CODCTRPMC = obj.CODCTRPMC,
                    .VLRINIFXAEQCCALCTR = obj.VLRINIFXAEQCCALCTR,
                    .TAMIVLFXAEQCCALCTR = obj.TAMIVLFXAEQCCALCTR,
                    .DESCTRMIXPMC = obj.DESCTRMIXPMC
                    }))
                Else
                    parametrosCalculo.ForEach(Sub(obj) RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO.InsertT(New VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva() With {
                    .TIPEDEPUBALVPMC = 4,
                    .CODPMC = criterioViewModel.CODPMC,
                    .CODGRPMIXMERPMC = obj.CODGRPMIXMERPMC,
                    .QDEPTOFXAEQCCALCTR = criterioViewModel.QDEPTOFXAEQCCALCTR,
                    .CODCTRPMC = If(criterioViewModel.CODCTRPMC IsNot Nothing, criterioViewModel.CODCTRPMC, obj.CODCTRPMC),
                    .VLRINIFXAEQCCALCTR = obj.VLRINIFXAEQCCALCTR,
                    .TAMIVLFXAEQCCALCTR = obj.TAMIVLFXAEQCCALCTR,
                    .DESCTRMIXPMC = " "
                    }))
                End If
            End If




            If (startTransaction) Then
                daoBase.connector.Commit()
            End If

            Return True
        Catch ex As Exception
            If (startTransaction) Then
                daoBase.connector.Rollback()
            End If
            Throw ex
        End Try

    End Function

End Class
