﻿Imports Core
Imports NPOI.HSSF.Extractor
Imports NPOI.HSSF.UserModel
Imports VO

Public Class PromocaoMixBO
    Inherits BOBase(Of VO.PromocaoMix)

    Public Sub New()
        MyBase.daoBase = New PromocaoMixDAO()
    End Sub

    Public Overloads Function InsertT(vo As VO.PromocaoMix, Optional startTransaction As Boolean = True) As VO.PromocaoMix
        Try
            If startTransaction Then
                daoBase.connector.BeginTransaction()
            End If

            vo.NUMSEQMIXACOCMC = SelectNextNUMSEQMIXACOCMC(vo.CODPMC)
            vo = MyBase.InsertT(vo, startTransaction)

            If startTransaction Then
                daoBase.connector.Commit()
            End If

            Return vo
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try
    End Function

#Region "Mix Grupo Mercadoria"
    Public Function EditMixGrupoMercadoria(ByVal CODPMC As Decimal) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).EditMixGrupoMercadoria(CODPMC)
    End Function

    Public Sub InsertListMixGrupoMercadoria(ByVal CODPMC As Decimal, ByVal PromocaoMixGrupoMercadoriaList As List(Of VO.PromocaoMix))
        MyBase.daoBase.connector.BeginTransaction()
        Try


            Dim GrupoMercadoriaSelecionadoListOLD As List(Of VO.PromocaoMix) = DirectCast(daoBase, PromocaoMixDAO).ListarGrupoMercadoriaSelecionados(CODPMC)

            For Each item In PromocaoMixGrupoMercadoriaList
                item.CODPMC = CODPMC
                If (item.NUMSEQMIXACOCMC Is Nothing) Then
                    item.NUMSEQMIXACOCMC = Me.SelectNextNUMSEQMIXACOCMC(CODPMC)
                    MyBase.InsertT(item)
                End If

            Next

            For Each item In GrupoMercadoriaSelecionadoListOLD
                If Not Exists(PromocaoMixGrupoMercadoriaList, item) Then
                    MyBase.DeleteT(item)
                End If
            Next

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Private Function Exists(ByRef pList As List(Of VO.PromocaoMix), ByVal pObj As VO.PromocaoMix) As Boolean
        Return pList.Where(Function(r) r.CODPMC = pObj.CODPMC And r.NUMSEQMIXACOCMC = pObj.NUMSEQMIXACOCMC).FirstOrDefault() IsNot Nothing
    End Function

#End Region

#Region "Mix Familia Mercadoria"
    Public Function PesquisaMixFamiliaMercadoria(vo As VO.PromocaoMix) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).PesquisaMixFamiliaMercadoria(vo)
    End Function

    Public Function ListarMixFamiliaMercadoriaAdicionados(vo As VO.PromocaoMix) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).ListarMixFamiliaMercadoriaAdicionados(vo)
    End Function

    Private Function SelectNextNUMSEQMIXACOCMC(ByVal CODPMC As Decimal) As Decimal
        Return DirectCast(daoBase, PromocaoMixDAO).SelectNextNUMSEQMIXACOCMC(CODPMC)
    End Function

    Public Sub AddPesquisaMixFamiliaMercadoria(ByRef filtro As VO.PromocaoMix)
        Dim voList As List(Of VO.PromocaoMix) = Me.PesquisaMixFamiliaMercadoria(filtro)
        Dim mercadoriaAdicionado As List(Of VO.PromocaoMix) = Me.ListarMixFamiliaMercadoriaAdicionados(New VO.PromocaoMix With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC})
        For Each novaFamiliaMercadoria In voList.Select(Function(f) New VO.PromocaoMix() With {.CODFMLMER = f.CODFMLMER, .CODGRPMER = f.CODGRPMER}).ToList()
            If (mercadoriaAdicionado.Where(Function(f) f.CODFMLMER = novaFamiliaMercadoria.CODFMLMER And f.CODGRPMER = novaFamiliaMercadoria.CODGRPMER).Count() = 0) Then
                If (Not (novaFamiliaMercadoria.CODFMLMER = 0)) Then
                    'Atribuir dados no objeto
                    novaFamiliaMercadoria.NUMSEQMIXACOCMC = Me.SelectNextNUMSEQMIXACOCMC(filtro.CODPMC.Value)
                    novaFamiliaMercadoria.CODPMC = filtro.CODPMC
                    novaFamiliaMercadoria.TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC
                    InsertT(novaFamiliaMercadoria)
                End If
            End If
        Next
    End Sub

    Friend Sub DeleteMixFamiliaMercadoria(filtro As PromocaoMix)
        DirectCast(daoBase, PromocaoMixDAO).DeleteMixFamiliaMercadoria(filtro)
    End Sub

    Public Function ImportarExelMixFamiliaMercadoria(path As String, ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
        Try

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.PromocaoMix) = ListaExcelMixFamiliaMercadoria(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos familia de mercadoria para importar"
            End If

            Dim erros As New StringBuilder()

            Dim FamiliaMercadoriaAdicionadas As List(Of VO.PromocaoMix) = Me.ListarMixFamiliaMercadoriaAdicionados(New VO.PromocaoMix With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})


            For Each novaFamiliaMercadoria In dados

                'verificar se Família Mercadoria existe no cadastro de Família Mercadoria
                Dim relacaoAcaoMecanicaMixDAO As New RelacaoAcaoMecanicaMixDAO()
                Dim verificaSeFamiliaMercadoriaExiste As List(Of VO.RelacaoAcaoMecanicaMix) = relacaoAcaoMecanicaMixDAO.VerificarSeFamiliaMercadoriaEstaCadastrada(novaFamiliaMercadoria.CODFMLMER, novaFamiliaMercadoria.CODGRPMER)
                If (verificaSeFamiliaMercadoriaExiste.Count <= 0) Then
                    erros.Append("Linha ").Append(novaFamiliaMercadoria.CODFMLMER).Append("; ").Append(novaFamiliaMercadoria.CODGRPMER).Append(" Família Mercadoria não cadastrada.").AppendLine()
                    Continue For
                End If

                Dim verificaFamiliaMercadoriaJaAdicionada As List(Of VO.PromocaoMix) = FamiliaMercadoriaAdicionadas.Where(Function(f) f.CODFMLMER = novaFamiliaMercadoria.CODFMLMER And f.CODGRPMER = novaFamiliaMercadoria.CODGRPMER).ToList
                If (verificaFamiliaMercadoriaJaAdicionada.Count > 0) Then
                    erros.Append("Linha ").Append(novaFamiliaMercadoria.CODFMLMER).Append("; ").Append(novaFamiliaMercadoria.CODGRPMER).Append(" Família Mercadoria já adicionada.").AppendLine()
                    Continue For
                End If

                Dim verificaFamiliaMercadoriaDuplicadoXLS As List(Of VO.PromocaoMix) = dados.Where(Function(f) f.CODFMLMER = novaFamiliaMercadoria.CODFMLMER And f.CODGRPMER = novaFamiliaMercadoria.CODGRPMER AndAlso f.CODPMC IsNot Nothing).ToList
                If (verificaFamiliaMercadoriaDuplicadoXLS.Count > 0) Then
                    erros.Append("Linha ").Append(novaFamiliaMercadoria.CODFMLMER).Append("; ").Append(novaFamiliaMercadoria.CODGRPMER).AppendLine(" Família Mercadoria duplicado na planilha.")
                    Continue For
                End If

                Try
                    If (Not (novaFamiliaMercadoria.CODFMLMER = 0)) Then

                        'Atribuir dados no objeto
                        novaFamiliaMercadoria.NUMSEQMIXACOCMC = Me.SelectNextNUMSEQMIXACOCMC(CODPMC)
                        novaFamiliaMercadoria.CODPMC = CODPMC
                        novaFamiliaMercadoria.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                        InsertT(novaFamiliaMercadoria)
                    End If
                Catch ex As Exception
                    erros.Append("Linha ").Append(novaFamiliaMercadoria.CODFMLMER).Append("; ").Append(novaFamiliaMercadoria.CODGRPMER).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                    Continue For
                End Try

            Next
            Return erros.ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function ListaExcelMixFamiliaMercadoria(path As String, ByRef erros As String) As List(Of VO.PromocaoMix)
        Dim linhasImportadas As New List(Of VO.PromocaoMix)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (alinha.Count <> 2) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 2 coluna de dados sendo ""Codigo da Familia da Mercadoria"" e ""Codigo do Grupo da Mercadoria""", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo da Familia da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Familia da Mercadoria""", NumLinha)
                    Return Nothing
                End If
                If (alinha(1).Replace("""", "") <> "Codigo do Grupo da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Codigo do Grupo da Mercadoria""", NumLinha)
                    Return Nothing
                End If
            Else ' Linhas de dados
                Dim novoLinha As New VO.PromocaoMix
                Dim CODFMLMER As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODFMLMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Familia da Mercadoria"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If
                novoLinha.CODFMLMER = CODFMLMER

                Dim CODGRPMER As Decimal
                If (Not Decimal.TryParse(alinha(1).Replace("""", ""), CODGRPMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo do Grupo da Mercadoria"" válido", NumLinha, alinha(1))
                    Return Nothing
                End If
                novoLinha.CODGRPMER = CODGRPMER

                linhasImportadas.Add(novoLinha)
            End If
            NumLinha += 1
        Next

        Return linhasImportadas

    End Function

#End Region

#Region "Mix Classe Mercadoria"
    Public Function PesquisaMixClasseMercadoria(vo As VO.PromocaoMix) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).PesquisaMixClasseMercadoria(vo)
    End Function

    Public Function ListarMixClasseMercadoriaAdicionados(vo As VO.PromocaoMix) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).ListarMixClasseMercadoriaAdicionados(vo)
    End Function

    Public Sub AddPesquisaMixClasseMercadoria(ByRef filtro As VO.PromocaoMix)

        Dim voList As List(Of VO.PromocaoMix) = Me.PesquisaMixClasseMercadoria(filtro)
        Dim mercadoriaAdicionado As List(Of VO.PromocaoMix) = Me.ListarMixClasseMercadoriaAdicionados(New VO.PromocaoMix With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC})
        For Each novaClasseMercadoria In voList.Select(Function(f) New VO.PromocaoMix() With {.CODCLSMER = f.CODCLSMER, .CODGRPMER = f.CODGRPMER, .CODFMLMER = f.CODFMLMER}).ToList()
            If (mercadoriaAdicionado.Where(Function(f) f.CODCLSMER = novaClasseMercadoria.CODCLSMER And f.CODGRPMER = novaClasseMercadoria.CODGRPMER And f.CODFMLMER = novaClasseMercadoria.CODFMLMER).Count() = 0) Then
                If (Not (novaClasseMercadoria.CODCLSMER = 0)) Then
                    'Atribuir dados no objeto
                    novaClasseMercadoria.NUMSEQMIXACOCMC = Me.SelectNextNUMSEQMIXACOCMC(filtro.CODPMC.Value)
                    novaClasseMercadoria.CODPMC = filtro.CODPMC
                    novaClasseMercadoria.TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC
                    InsertT(novaClasseMercadoria)
                End If
            End If
        Next
    End Sub

    Friend Sub DeleteMixClasseMercadoria(filtro As PromocaoMix)
        DirectCast(daoBase, PromocaoMixDAO).DeleteMixClasseMercadoria(filtro)
    End Sub


    Public Function ImportarExelMixClasseMercadoria(path As String, ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
        Try

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.PromocaoMix) = ListaExcelMixClasseMercadoria(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos classe de mercadoria para importar"
            End If

            Dim erros As New StringBuilder()

            Dim ClasseMercadoriaAdicionadas As List(Of VO.PromocaoMix) = Me.ListarMixClasseMercadoriaAdicionados(New VO.PromocaoMix With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})


            For Each novaClasseMercadoria In dados

                'verificar se a classe de Mercadoria existe no cadastro de classe de Mercadoria
                Dim relacaoAcaoMecanicaMixDAO As New RelacaoAcaoMecanicaMixDAO()
                Dim verificaSeClasseMercadoriaExiste As List(Of VO.RelacaoAcaoMecanicaMix) = relacaoAcaoMecanicaMixDAO.VerificarSeClasseMercadoriaEstaCadastrada(novaClasseMercadoria.CODCLSMER, novaClasseMercadoria.CODGRPMER, novaClasseMercadoria.CODFMLMER)
                If (verificaSeClasseMercadoriaExiste.Count <= 0) Then
                    erros.Append("Linha ").Append(novaClasseMercadoria.CODCLSMER).Append("; ").Append(novaClasseMercadoria.CODGRPMER).Append("; ").Append(novaClasseMercadoria.CODFMLMER).Append(" Classe Mercadoria não cadastrada.").AppendLine()
                    Continue For
                End If

                Dim verificaClasseMercadoriaJaAdicionada As List(Of VO.PromocaoMix) = ClasseMercadoriaAdicionadas.Where(Function(f) f.CODCLSMER = novaClasseMercadoria.CODCLSMER And f.CODGRPMER = novaClasseMercadoria.CODGRPMER And f.CODFMLMER = novaClasseMercadoria.CODFMLMER).ToList
                If (verificaClasseMercadoriaJaAdicionada.Count > 0) Then
                    erros.Append("Linha ").Append(novaClasseMercadoria.CODCLSMER).Append("; ").Append(novaClasseMercadoria.CODGRPMER).Append("; ").Append(novaClasseMercadoria.CODFMLMER).Append(" Classe Mercadoria já adicionada.").AppendLine()
                    Continue For
                End If

                Dim verificaClasseMercadoriaDuplicadoXLS As List(Of VO.PromocaoMix) = dados.Where(Function(f) f.CODCLSMER = novaClasseMercadoria.CODCLSMER And f.CODGRPMER = novaClasseMercadoria.CODGRPMER And f.CODFMLMER = novaClasseMercadoria.CODFMLMER AndAlso f.CODPMC IsNot Nothing).ToList
                If (verificaClasseMercadoriaDuplicadoXLS.Count > 0) Then
                    erros.Append("Linha ").Append(novaClasseMercadoria.CODCLSMER).Append("; ").Append(novaClasseMercadoria.CODGRPMER).Append("; ").Append(novaClasseMercadoria.CODFMLMER).AppendLine(" Classe Mercadoria duplicado na planilha.")
                    Continue For
                End If

                Try
                    If (Not (novaClasseMercadoria.CODCLSMER = 0)) Then

                        'Atribuir dados no objeto
                        novaClasseMercadoria.NUMSEQMIXACOCMC = Me.SelectNextNUMSEQMIXACOCMC(CODPMC)
                        novaClasseMercadoria.CODPMC = CODPMC
                        novaClasseMercadoria.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                        InsertT(novaClasseMercadoria)
                    End If
                Catch ex As Exception
                    erros.Append("Linha ").Append(novaClasseMercadoria.CODCLSMER).Append("; ").Append(novaClasseMercadoria.CODGRPMER).Append("; ").Append(novaClasseMercadoria.CODFMLMER).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                    Continue For
                End Try

            Next
            Return erros.ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function ListaExcelMixClasseMercadoria(path As String, ByRef erros As String) As List(Of VO.PromocaoMix)
        Dim linhasImportadas As New List(Of VO.PromocaoMix)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (alinha.Count <> 3) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 3 coluna de dados sendo ""Codigo da Classe da Mercadoria"", ""Codigo do Grupo da Mercadoria"" e  ""Codigo da Familia da Mercadoria""", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo da Classe da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Classe da Mercadoria""", NumLinha)
                    Return Nothing
                End If
                If (alinha(1).Replace("""", "") <> "Codigo do Grupo da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Codigo do Grupo da Mercadoria""", NumLinha)
                    Return Nothing
                End If
                If (alinha(2).Replace("""", "") <> "Codigo da Familia da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Codigo da Familia da Mercadoria""", NumLinha)
                    Return Nothing
                End If
            Else ' Linhas de dados
                Dim novoLinha As New VO.PromocaoMix
                Dim CODCLSMER As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODCLSMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Classe da Mercadoria"" válida", NumLinha, alinha(0))
                    Return Nothing
                End If
                novoLinha.CODCLSMER = CODCLSMER

                Dim CODGRPMER As Decimal
                If (Not Decimal.TryParse(alinha(1).Replace("""", ""), CODGRPMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo do Grupo da Mercadoria"" válido", NumLinha, alinha(1))
                    Return Nothing
                End If
                novoLinha.CODGRPMER = CODGRPMER

                Dim CODFMLMER As Decimal
                If (Not Decimal.TryParse(alinha(2).Replace("""", ""), CODFMLMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Familia da Mercadoria"" válida", NumLinha, alinha(2))
                    Return Nothing
                End If
                novoLinha.CODFMLMER = CODFMLMER

                linhasImportadas.Add(novoLinha)
            End If
            NumLinha += 1
        Next

        Return linhasImportadas

    End Function

#End Region

#Region "Mix Mercadoria"
    Public Function PesquisaMixMercadoria(vo As VO.PromocaoMix) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).PesquisaMixMercadoria(vo)
    End Function

    Public Function PesquisaMixMercadoriaEdl(vo As VO.PromocaoMix) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).PesquisaMixMercadoriaEdl(vo)
    End Function

    Public Function PesquisaMixMercadoriaPorPromocao(ByVal CODPMCatual As Decimal, ByVal CODPMCtoFind As Decimal) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).PesquisaMixMercadoriaPorPromocao(CODPMCatual, CODPMCtoFind)
    End Function

    Public Function ListarMixMercadoriaAdicionados(vo As VO.PromocaoMix) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).ListarMixMercadoriaAdicionados(vo)
    End Function

    Public Function ListarMixMercadoriaAdicionadosEdl(vo As VO.PromocaoMix) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).ListarMixMercadoriaAdicionadosEdl(vo)
    End Function

    Public Function ListarMixMercadoriasParametros(CODPMC As Decimal, TIPSITPMC As String) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).ListarMixMercadoriasParametros(CODPMC, TIPSITPMC)
    End Function

    Public Function ListarMixMercadoriasParametrosEdl(CODPMC As Decimal, TIPSITPMC As String) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).ListarMixMercadoriasParametrosEdl(CODPMC, TIPSITPMC)
    End Function

    Public Sub AddPesquisaMixMercadoria(ByRef filtro As VO.PromocaoMix)

        Dim voList As List(Of VO.PromocaoMix) = Me.PesquisaMixMercadoria(filtro)
        Dim mercadoriaAdicionado As List(Of VO.PromocaoMix) = Me.ListarMixMercadoriaAdicionados(New VO.PromocaoMix With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC})
        For Each novaMercadoria In voList.Select(Function(f) New VO.PromocaoMix() With {.CODMER = f.CODMER}).ToList()
            If (mercadoriaAdicionado.Where(Function(f) f.CODMER = novaMercadoria.CODMER).Count() = 0) Then
                If (Not (novaMercadoria.CODMER = 0)) Then
                    'Atribuir dados no objeto
                    novaMercadoria.NUMSEQMIXACOCMC = Me.SelectNextNUMSEQMIXACOCMC(filtro.CODPMC.Value)
                    novaMercadoria.CODPMC = filtro.CODPMC
                    novaMercadoria.TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC
                    novaMercadoria.CODGRPMIXMERPMC = filtro.CODGRPMIXMERPMC
                    InsertT(novaMercadoria)
                End If
            End If
        Next
    End Sub

    Public Sub AddPesquisaMixMercadoriaPorPromocao(ByVal CODPMCatual As Decimal, ByVal CODPMCtoFind As Decimal, ByVal TIPEDEPUBALVPMC As Nullable(Of Decimal), ByVal CODGRPMIXMERPMC As Nullable(Of Decimal))

        Dim voList As List(Of VO.PromocaoMix) = Me.PesquisaMixMercadoriaPorPromocao(CODPMCatual, CODPMCtoFind)
        Dim mercadoriaAdicionado As List(Of VO.PromocaoMix) = Me.ListarMixMercadoriaAdicionados(New VO.PromocaoMix With {.CODPMC = CODPMCatual, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})
        For Each novaMercadoria In voList.Select(Function(f) New VO.PromocaoMix() With {.CODMER = f.CODMER}).ToList()
            If (mercadoriaAdicionado.Where(Function(f) f.CODMER = novaMercadoria.CODMER).Count() = 0) Then
                If (Not (novaMercadoria.CODMER = 0)) Then
                    'Atribuir dados no objeto
                    novaMercadoria.NUMSEQMIXACOCMC = Me.SelectNextNUMSEQMIXACOCMC(CODPMCatual)
                    novaMercadoria.CODPMC = CODPMCatual
                    novaMercadoria.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
                    novaMercadoria.CODGRPMIXMERPMC = CODGRPMIXMERPMC
                    InsertT(novaMercadoria)
                End If
            End If
        Next
    End Sub

    Friend Sub DeleteMixMercadoria(filtro As PromocaoMix)
        DirectCast(daoBase, PromocaoMixDAO).DeleteMixMercadoria(filtro)
    End Sub

    Public Function ImportarExelMixMercadoria(path As String, ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
        Try

            Dim promocaoDAO As New PromocaoDAO()
            Dim promocao As VO.Promocao = promocaoDAO.SelectT(New VO.Promocao() With {.CODPMC = CODPMC})

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.PromocaoMix) = ListaExcelMixMercadoria(path, erroImportExcel, promocao.QDEGRPMIXPMC)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos de mercadoria para importar"
            End If

            If dados.Count > 30000 Then
                Return "Este arquivo possui mais de 30000 codigos de clientes para importar"
            End If

            Dim erros As New StringBuilder()

            Dim relacaoAcaoFornecedorBO As RelacaoAcaoComercialxFornecedoresBO = New RelacaoAcaoComercialxFornecedoresBO()
            Dim relacaoAcaoFornecedor As RelacaoAcaoComercialxFornecedores = relacaoAcaoFornecedorBO.BuscarFornecedoresDaAcao(promocao.CODACOCMC).FirstOrDefault()

            Dim MercadoriaAdicionadas As List(Of VO.PromocaoMix) = If(relacaoAcaoFornecedor.TIPFRNACOCMC = 1,
                                                                                Me.ListarMixMercadoriaAdicionadosEdl(New VO.PromocaoMix With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC}),
                                                                                Me.ListarMixMercadoriaAdicionados(New VO.PromocaoMix With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC}))


            'Dim MercadoriaAdicionadas As List(Of VO.PromocaoMix) = Me.ListarMixMercadoriaAdicionados(New VO.PromocaoMix With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})

            For Each novaMercadoria In dados

                If (relacaoAcaoFornecedor.TIPFRNACOCMC = 0) Then
                    'verificar se Gerente Mercado existe no cadastro de Gerente de Mercado
                    Dim mercadoriaDAO As New MercadoriaDAO()
                    Dim verificaSeMercadoriaExiste As List(Of VO.Mercadoria) = mercadoriaDAO.BuscaNomeMercadoria(novaMercadoria.CODMER)
                    If (verificaSeMercadoriaExiste.Count <= 0) Then
                        erros.Append("Linha ").Append(novaMercadoria.CODMER).Append(" Mercadoria não cadastrado.").AppendLine()
                        Continue For
                    End If

                    If (promocao.QDEGRPMIXPMC > 0 And (novaMercadoria.CODGRPMIXMERPMC > promocao.QDEGRPMIXPMC Or novaMercadoria.CODGRPMIXMERPMC <= 0)) Then
                        erros.Append("Linha ").Append(novaMercadoria.CODGRPMIXMERPMC).Append(" Grupo inválido.").AppendLine()
                        Continue For
                    End If
                End If

                Dim verificaMercadoriaJaAdicionada As List(Of VO.PromocaoMix) = MercadoriaAdicionadas.Where(Function(f) f.CODMER = novaMercadoria.CODMER).ToList
                If (verificaMercadoriaJaAdicionada.Count > 0) Then
                    erros.Append("Linha ").Append(novaMercadoria.CODMER).Append(" Mercadoria já adicionada.").AppendLine()
                    Continue For
                End If

                Dim verificaMercadoriaDuplicadoXLS As List(Of VO.PromocaoMix) = dados.Where(Function(f) f.CODMER = novaMercadoria.CODMER AndAlso f.CODPMC IsNot Nothing).ToList
                If (verificaMercadoriaDuplicadoXLS.Count > 0) Then
                    erros.Append("Linha ").Append(novaMercadoria.CODMER).AppendLine(" Mercadoria duplicado na planilha.")
                    Continue For
                End If

                Try
                    If (Not (novaMercadoria.CODMER = 0)) Then

                        'Atribuir dados no objeto
                        novaMercadoria.NUMSEQMIXACOCMC = Me.SelectNextNUMSEQMIXACOCMC(CODPMC)
                        novaMercadoria.CODPMC = CODPMC
                        novaMercadoria.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                        InsertT(novaMercadoria)

                    End If
                Catch ex As Exception
                    erros.Append("Linha ").Append(novaMercadoria.CODMER).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                    Continue For
                End Try
            Next
            Return erros.ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function ListaExcelMixMercadoria(path As String, ByRef erros As String, QDEGRPMIXPMC As Decimal) As List(Of VO.PromocaoMix)
        Dim linhasImportadas As New List(Of VO.PromocaoMix)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (QDEGRPMIXPMC > 0) Then
                If (alinha.Count <> 2) Then
                    erros = String.Format("Erro Linha {0}: O excel deve conter 2 colunas de dados sendo ""Codigo da Mercadoria"" e ""Grupo""", NumLinha)
                    Return Nothing
                End If
            Else
                If (Not (alinha.Count = 2 Or alinha.Count = 1)) Then
                    erros = String.Format("Erro Linha {0}: O excel deve conter 2 colunas de dados sendo ""Codigo da Mercadoria"" e ""Grupo""", NumLinha)
                    Return Nothing
                End If
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (QDEGRPMIXPMC > 0) Then
                    If (alinha(0).Replace("""", "") <> "Codigo da Mercadoria" Or alinha(1).Replace("""", "") <> "Grupo") Then
                        erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Mercadoria"" e na segunda deve conter ""Grupo""", NumLinha)
                        Return Nothing
                    End If
                Else
                    If (alinha(0).Replace("""", "") <> "Codigo da Mercadoria") Then
                        erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Mercadoria""", NumLinha)
                        Return Nothing
                    End If
                End If

            Else ' Linhas de dados
                Dim novoLinha As New VO.PromocaoMix
                If (QDEGRPMIXPMC > 0) Then
                    Dim CODMER As Decimal
                    If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODMER)) Then
                        erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Mercadoria"" válido", NumLinha, alinha(0))
                        Return Nothing
                    End If

                    Dim CODGRPMIXMERPMC As Decimal
                    If (Not Decimal.TryParse(alinha(1).Replace("""", ""), CODGRPMIXMERPMC)) Then
                        erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Grupo"" válido", NumLinha, alinha(1))
                        Return Nothing
                    End If

                    novoLinha.CODMER = CODMER
                    novoLinha.CODGRPMIXMERPMC = CODGRPMIXMERPMC
                Else
                    Dim CODMER As Decimal
                    If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODMER)) Then
                        erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Mercadoria"" válido", NumLinha, alinha(0))
                        Return Nothing
                    End If

                    novoLinha.CODMER = CODMER
                End If
                linhasImportadas.Add(novoLinha)
            End If

            NumLinha += 1
        Next

        Return linhasImportadas

    End Function

    Public Sub AplicarParametrosMixMercadoria(ByVal list As List(Of PromocaoParametrosMixViewModel))
        Try
            daoBase.connector.BeginTransaction()
            For Each param In list
                Dim entity As VO.PromocaoMix = Me.SelectT(New VO.PromocaoMix With {.CODPMC = param.CODPMC, .CODMER = param.CODMER})
                entity.QDEMNMMERMIX = param.QDEMNMMERMIX
                entity.QDEMAXMERMIX = param.QDEMAXMERMIX
                entity.INDITEOBR = param.INDITEOBR
                entity.INDJRNSMA = param.INDJRNSMA

                Me.UpdateT(entity)
            Next
            daoBase.connector.Commit()
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw (ex)
        End Try
    End Sub

    Public Function ImportarExelMixParametrosMercadoria(path As String, ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
        Try

            Dim promocaoDAO As New PromocaoDAO()
            Dim promocao As VO.Promocao = promocaoDAO.SelectT(New VO.Promocao() With {.CODPMC = CODPMC})

            Dim relacaoAcaoFornecedorBO As RelacaoAcaoComercialxFornecedoresBO = New RelacaoAcaoComercialxFornecedoresBO()
            Dim relacaoAcaoFornecedor As RelacaoAcaoComercialxFornecedores = relacaoAcaoFornecedorBO.BuscarFornecedoresDaAcao(promocao.CODACOCMC).FirstOrDefault()

            Dim BO As PromocaoMixBO = New PromocaoMixBO()
            Dim mercadoriasDaPromocao As List(Of VO.PromocaoMix) = If(relacaoAcaoFornecedor.TIPFRNACOCMC = 1 _
                                                            , BO.ListarMixMercadoriasParametrosEdl(CODPMC, promocao.TIPSITPMC) _
                                                            , BO.ListarMixMercadoriasParametros(CODPMC, promocao.TIPSITPMC))

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of PromocaoParametrosMixViewModel) = ListaExcelMixParametrosMercadoria(path, erroImportExcel, mercadoriasDaPromocao, CODPMC)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos de mercadoria para importar"
            End If

            If dados.Count > 30000 Then
                Return "Este arquivo possui mais de 30000 codigos de clientes para importar"
            End If

            Dim BOpromocao As PromocaoBO = New PromocaoBO()
            Dim result As ValidacaoParametrosMixViewModel = BOpromocao.ValidarParametrizacao(dados, relacaoAcaoFornecedor.TIPFRNACOCMC)

            If (result.Mensagem <> String.Empty) Then
                Return result.Mensagem
            End If

            Dim BOpromocaoMix As PromocaoMixBO = New PromocaoMixBO()
            If (IsNothing(result.List)) Then
                BOpromocaoMix.AplicarParametrosMixMercadoria(dados)
            Else
                BOpromocaoMix.AplicarParametrosMixMercadoria(result.List)
            End If

            Return result.Mensagem
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function ListaExcelMixParametrosMercadoria(path As String, ByRef erros As String, mercadoriasDaPromocao As List(Of VO.PromocaoMix), ByVal CODPMC As Decimal) As List(Of PromocaoParametrosMixViewModel)
        Dim linhasImportadas As New List(Of PromocaoParametrosMixViewModel)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)

            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (alinha.Count <> 5) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 5 colunas de dados sendo ""Codigo da Mercadoria"", ""qde min"", ""qde max"", ""ite obr"" e ""jrn smart""", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Mercadoria""", NumLinha)
                End If

                If (alinha(1).Replace("""", "") <> "qde min") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""qde min""", NumLinha)
                End If

                If (alinha(2).Replace("""", "") <> "qde max") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na terceira coluna deve conter a coluna ""qde max""", NumLinha)
                End If

                If (alinha(3).Replace("""", "") <> "ite obr") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na quarta coluna deve conter a coluna ""ite obr""", NumLinha)
                End If

                If (alinha(4).Replace("""", "") <> "jrn smart") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na quinta coluna deve conter a coluna ""jrn smart""", NumLinha)
                End If

            Else ' Linhas de dados
                Dim novoLinha As New PromocaoParametrosMixViewModel
                Dim CODMER As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Mercadoria"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If

                If (mercadoriasDaPromocao.Where(Function(f) f.CODMER = CODMER).Count <= 0) Then
                    erros = String.Format("Erro Linha {0}: A mercadoria ""{1}"" não pertence a esta promoção ", NumLinha, alinha(0))
                    Return Nothing
                End If

                Dim QDEMNMMERMIX As Decimal
                If (Not Decimal.TryParse(alinha(1).Replace("""", ""), QDEMNMMERMIX)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""qde min"" válido", NumLinha, alinha(1))
                    Return Nothing
                End If

                Dim QDEMAXMERMIX As Decimal
                If (Not Decimal.TryParse(alinha(2).Replace("""", ""), QDEMAXMERMIX)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""qde max"" válido", NumLinha, alinha(2))
                    Return Nothing
                End If

                Dim INDITEOBR As Decimal
                If (alinha(3).ToUpper().Substring(0, 1) <> "S" AndAlso alinha(3).ToUpper().Substring(0, 1) <> "N") Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""ite obr"" válido", NumLinha, alinha(3))
                    Return Nothing
                Else
                    INDITEOBR = IIf(alinha(3).ToUpper().Substring(0, 1) = "S", 1, 0)
                End If

                Dim INDJRNSMA As Decimal
                If (alinha(4).ToUpper().Substring(0, 1) <> "S" AndAlso alinha(4).ToUpper().Substring(0, 1) <> "N") Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""jrn smart"" válido", NumLinha, alinha(4))
                    Return Nothing
                Else
                    INDJRNSMA = IIf(alinha(4).ToUpper().Substring(0, 1) = "S", 1, 0)
                End If

                novoLinha.CODPMC = CODPMC
                novoLinha.CODMER = CODMER
                novoLinha.QDEMNMMERMIX = QDEMNMMERMIX
                novoLinha.QDEMAXMERMIX = QDEMAXMERMIX
                novoLinha.INDITEOBR = INDITEOBR
                novoLinha.INDJRNSMA = INDJRNSMA

                linhasImportadas.Add(novoLinha)
            End If

            NumLinha += 1
        Next

        Return linhasImportadas

    End Function

    Public Function VerificaLimiteItensObrigatorios(ByVal list As List(Of PromocaoParametrosMixViewModel)) As Decimal
        Try
            Dim mix As List(Of VO.PromocaoMix) = DirectCast(daoBase, PromocaoMixDAO).ListPorPromocao(list.FirstOrDefault().CODPMC)
            If ((mix Is Nothing Or mix.Count = 0) AndAlso list.Count <= 12) Then
                Return 0
            End If

            For Each item In mix
                Dim newItem = list.FirstOrDefault(Function(f) f.CODMER = item.CODMER)
                If (newItem IsNot Nothing) Then
                    item.INDITEOBR = newItem.INDITEOBR
                End If
            Next

            Return mix.Where(Function(f) f.INDITEOBR = 1).Count
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ListarParametrosMix(CODPMC As Decimal, TIPSITPMC As String) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).ListarParametrosMix(CODPMC, TIPSITPMC)
    End Function

#Region "Parâmetros Mix Cash"
    Public Sub AplicarParametrosMixCashMercadoria(ByVal list As List(Of PromocaoParametrosMixViewModel))
        Try
            daoBase.connector.BeginTransaction()
            For Each param In list
                Dim entity As VO.PromocaoMix = Me.SelectT(New VO.PromocaoMix With {.CODPMC = param.CODPMC, .CODMER = param.CODMER})
                entity.QDEMNMMERMIX = param.QDEMNMMERMIX
                entity.INDITEOBR = param.INDITEOBR

                Me.UpdateT(entity)
            Next
            daoBase.connector.Commit()
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw (ex)
        End Try
    End Sub

    Public Function ImportarExelMixCashParametrosMercadoria(path As String, ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
        Try

            Dim promocaoDAO As New PromocaoDAO()
            Dim promocao As VO.Promocao = promocaoDAO.SelectT(New VO.Promocao() With {.CODPMC = CODPMC})

            Dim relacaoAcaoFornecedorBO As RelacaoAcaoComercialxFornecedoresBO = New RelacaoAcaoComercialxFornecedoresBO()
            Dim relacaoAcaoFornecedor As RelacaoAcaoComercialxFornecedores = relacaoAcaoFornecedorBO.BuscarFornecedoresDaAcao(promocao.CODACOCMC).FirstOrDefault()

            Dim BO As PromocaoMixBO = New PromocaoMixBO()
            Dim mercadoriasDaPromocao As List(Of VO.PromocaoMix) = If(relacaoAcaoFornecedor.TIPFRNACOCMC = 1 _
                                                            , BO.ListarMixMercadoriasParametrosEdl(CODPMC, promocao.TIPSITPMC) _
                                                            , BO.ListarMixMercadoriasParametros(CODPMC, promocao.TIPSITPMC))

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of PromocaoParametrosMixViewModel) = ListaExcelMixCashParametrosMercadoria(path, erroImportExcel, mercadoriasDaPromocao, CODPMC)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos de mercadoria para importar"
            End If

            If dados.Count > 30000 Then
                Return "Este arquivo possui mais de 30000 codigos de clientes para importar"
            End If

            Dim BOpromocao As PromocaoBO = New PromocaoBO()
            Dim result As ValidacaoParametrosMixViewModel = BOpromocao.ValidarParametrizacaoCash(dados, relacaoAcaoFornecedor.TIPFRNACOCMC)

            If (result.Mensagem <> String.Empty) Then
                Return result.Mensagem
            End If

            Dim BOpromocaoMix As PromocaoMixBO = New PromocaoMixBO()
            If (IsNothing(result.List)) Then
                BOpromocaoMix.AplicarParametrosMixCashMercadoria(dados)
            Else
                BOpromocaoMix.AplicarParametrosMixCashMercadoria(result.List)
            End If

            Return result.Mensagem
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function ListaExcelMixCashParametrosMercadoria(path As String, ByRef erros As String, mercadoriasDaPromocao As List(Of VO.PromocaoMix), ByVal CODPMC As Decimal) As List(Of PromocaoParametrosMixViewModel)
        Dim linhasImportadas As New List(Of PromocaoParametrosMixViewModel)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)

            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (alinha.Count <> 3) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 3 colunas de dados sendo ""Codigo da Mercadoria"", ""qde min"" e ""ite obr""", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Mercadoria""", NumLinha)
                End If

                If (alinha(1).Replace("""", "") <> "qde min") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""qde min""", NumLinha)
                End If

                If (alinha(2).Replace("""", "") <> "ite obr") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na quarta coluna deve conter a coluna ""ite obr""", NumLinha)
                End If

            Else ' Linhas de dados
                Dim novoLinha As New PromocaoParametrosMixViewModel
                Dim CODMER As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo da Mercadoria"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If

                If (mercadoriasDaPromocao.Where(Function(f) f.CODMER = CODMER).Count <= 0) Then
                    erros = String.Format("Erro Linha {0}: A mercadoria ""{1}"" não pertence a esta promoção ", NumLinha, alinha(0))
                    Return Nothing
                End If

                Dim QDEMNMMERMIX As Decimal
                If (Not Decimal.TryParse(alinha(1).Replace("""", ""), QDEMNMMERMIX)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""qde min"" válido", NumLinha, alinha(1))
                    Return Nothing
                End If

                Dim INDITEOBR As Decimal
                If (alinha(2).ToUpper().Substring(0, 1) <> "S" AndAlso alinha(2).ToUpper().Substring(0, 1) <> "N") Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""ite obr"" válido", NumLinha, alinha(2))
                    Return Nothing
                Else
                    INDITEOBR = IIf(alinha(2).ToUpper().Substring(0, 1) = "S", 1, 0)
                End If

                novoLinha.CODPMC = CODPMC
                novoLinha.CODMER = CODMER
                novoLinha.QDEMNMMERMIX = QDEMNMMERMIX
                novoLinha.INDITEOBR = INDITEOBR

                linhasImportadas.Add(novoLinha)
            End If

            NumLinha += 1
        Next

        Return linhasImportadas

    End Function

#End Region
#End Region

#Region "Mix Fornecedor"
    Public Function PesquisaMixFornecedor(vo As VO.PromocaoMix) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).PesquisaMixFornecedor(vo)
    End Function

    Public Function ListarMixFornecedorAdicionados(vo As VO.PromocaoMix) As List(Of VO.PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).ListarMixFornecedorAdicionados(vo)
    End Function


    Public Sub AddPesquisaMixFornecedor(ByRef filtro As VO.PromocaoMix)
        Dim voList As List(Of VO.PromocaoMix) = Me.PesquisaMixFornecedor(filtro)
        Dim FornecedorAdicionado As List(Of VO.PromocaoMix) = Me.ListarMixFornecedorAdicionados(New VO.PromocaoMix With {.CODPMC = filtro.CODPMC, .TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC})
        For Each novoFornecedor In voList.Select(Function(f) New VO.PromocaoMix() With {.CODFRN = f.CODFRN}).ToList()
            If (FornecedorAdicionado.Where(Function(f) f.CODFRN = novoFornecedor.CODFRN).Count() = 0) Then
                If (Not (novoFornecedor.CODFRN = 0)) Then
                    'Atribuir dados no objeto
                    novoFornecedor.NUMSEQMIXACOCMC = Me.SelectNextNUMSEQMIXACOCMC(filtro.CODPMC.Value)
                    novoFornecedor.CODPMC = filtro.CODPMC
                    novoFornecedor.TIPEDEPUBALVPMC = filtro.TIPEDEPUBALVPMC

                    InsertT(novoFornecedor)
                End If
            End If
        Next
    End Sub


    Friend Sub DeleteMixFornecedor(filtro As PromocaoMix)
        DirectCast(daoBase, PromocaoMixDAO).DeleteMixFornecedor(filtro)
    End Sub

    Public Function ImportarExelMixFornecedor(path As String, ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As String
        Try

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.PromocaoMix) = ListaExcelMixFornecedor(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos de fornecedor para importar"
            End If

            Dim erros As New StringBuilder()

            Dim FornecedorAdicionado As List(Of VO.PromocaoMix) = Me.ListarMixFornecedorAdicionados(New VO.PromocaoMix With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})


            For Each novoFornecedor In dados

                'verificar se Gerente Mercado existe no cadastro de Gerente de Mercado
                Dim fornecedoresDAO As New FornecedoresDAO()
                Dim verificaSeFornecedorExiste As List(Of VO.Fornecedores) = fornecedoresDAO.BuscarNomeCodigoFornecedores(novoFornecedor.CODFRN)
                If (verificaSeFornecedorExiste.Count <= 0) Then
                    erros.Append("Linha ").Append(novoFornecedor.CODFRN).Append(" Fornecedor não cadastrado.").AppendLine()
                    Continue For
                End If

                Dim verificaFornecedorJaAdicionado As List(Of VO.PromocaoMix) = FornecedorAdicionado.Where(Function(f) f.CODFRN = novoFornecedor.CODFRN).ToList
                If (verificaFornecedorJaAdicionado.Count > 0) Then
                    erros.Append("Linha ").Append(novoFornecedor.CODFRN).Append(" Fornecedor já cadastrado.").AppendLine()
                    Continue For
                End If

                Dim verificaFornecedorDuplicadoXLS As List(Of VO.PromocaoMix) = dados.Where(Function(f) f.CODFRN = novoFornecedor.CODFRN AndAlso f.CODPMC IsNot Nothing).ToList
                If (verificaFornecedorDuplicadoXLS.Count > 0) Then
                    erros.Append("Linha ").Append(novoFornecedor.CODFRN).AppendLine(" Fornecedor duplicado na planilha.")
                    Continue For
                End If

                Try
                    If (Not (novoFornecedor.CODFRN = 0)) Then

                        'Atribuir dados no objeto
                        novoFornecedor.NUMSEQMIXACOCMC = Me.SelectNextNUMSEQMIXACOCMC(CODPMC)
                        novoFornecedor.CODPMC = CODPMC
                        novoFornecedor.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC

                        InsertT(novoFornecedor)

                    End If
                Catch ex As Exception
                    erros.Append("Linha ").Append(novoFornecedor.CODFRN).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                    Continue For
                End Try
            Next
            Return erros.ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function


    Public Function ListaExcelMixFornecedor(path As String, ByRef erros As String) As List(Of VO.PromocaoMix)
        Dim linhasImportadas As New List(Of VO.PromocaoMix)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (alinha.Count <> 1) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 1 coluna de dados sendo ""Codigo do Fornecedor""", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo do Fornecedor") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo do Fornecedor""", NumLinha)
                    Return Nothing
                End If
            Else ' Linhas de dados
                Dim novoLinha As New VO.PromocaoMix
                Dim CODFRN As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODFRN)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo de Fornecedor"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If
                novoLinha.CODFRN = CODFRN

                linhasImportadas.Add(novoLinha)
            End If
            NumLinha += 1
        Next

        Return linhasImportadas

    End Function

#End Region


#Region "Count Mix Controle Botão 'Configurar' e 'Selecionar'"
    Public Function SelectCountMix(ByVal CODPMC As Decimal) As VO.PromocaoMix
        Return DirectCast(daoBase, PromocaoMixDAO).SelectCountMix(CODPMC)
    End Function


#End Region


    Public Function CopiarMixVigentePromocao(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of PromocaoMix)
        Return DirectCast(daoBase, PromocaoMixDAO).CopiarMixVigentePromocao(CODPMC, TIPEDEPUBALVPMC)
    End Function


    Public Sub UpdateIndicadorValidadeRegraMIX(CODPMC As Decimal, INDVLDRGRMIXPMC As Decimal, Optional startTransaction As Boolean = True)
        If (startTransaction) Then
            daoBase.connector.BeginTransaction()
        End If
        Try
            DirectCast(daoBase, PromocaoMixDAO).UpdateIndicadorValidadeRegraMIX(CODPMC, INDVLDRGRMIXPMC)

            If (startTransaction) Then
                daoBase.connector.Commit()
            End If
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw (ex)
        End Try
    End Sub
End Class