﻿Imports Core

Public Class PromocaoMixVisualizarVigenteBO
    Inherits BOBase(Of VO.PromocaoMixVisualizarVigente)

    Public Sub New()
        MyBase.daoBase = New PromocaoMixVisualizarVigenteDAO()
    End Sub

    Public Function PesquisaItensVisualizarMixVigente(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)
        Return DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).PesquisaItensVisualizarMixVigente(CODPMC, TIPEDEPUBALVPMC)
    End Function

    Public Function PesquisaItensVisualizarMixVigenteEdl(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)
        Return DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).PesquisaItensVisualizarMixVigenteEdl(CODPMC, TIPEDEPUBALVPMC)
    End Function

    Public Function PreVisualizarMixMecanicaAcao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)
        Return DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).PreVisualizarMixMecanicaAcao(CODACOCMC, NUMSEQRGRACOCMC, TIPPMC)
    End Function

    Public Function PreVisualizarMixMecanicaAcaoEdl(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)
        Return DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).PreVisualizarMixMecanicaAcaoEdl(CODACOCMC, NUMSEQRGRACOCMC, TIPPMC)
    End Function

    Public Function PreVisualizarMixPromocao(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal TIPPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)
        Return DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).PreVisualizarMixPromocao(CODPMC, TIPEDEPUBALVPMC, TIPPMC)
    End Function

    Public Function PreVisualizarMixPromocaoEdl(ByVal CODPMC As Decimal, ByVal TIPSITPMC As String) As List(Of VO.PromocaoMixVisualizarVigente)
        Return DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).PreVisualizarMixPromocaoEdl(CODPMC, TIPSITPMC)
    End Function

    Public Sub AtualizaMixPromocao(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal TIPPMC As Decimal, ByVal TIPSITPMC As String, Optional TIPFRNACOCMC As Decimal = 0, Optional startTransaction As Boolean = True)
        Dim lista As List(Of VO.PromocaoMixVisualizarVigente)
        If (TIPFRNACOCMC = 1) Then
            lista = Me.PreVisualizarMixPromocaoEdl(CODPMC, TIPSITPMC)
        Else
            lista = Me.PreVisualizarMixPromocao(CODPMC, TIPEDEPUBALVPMC, TIPPMC)
        End If

        If (lista Is Nothing) Then
            Throw New Exception("MIX não configurado para esta promoção")
        End If

        Dim listaOld As List(Of VO.PromocaoMixVisualizarVigente) = Me.ListT(New VO.PromocaoMixVisualizarVigente() With {.CODPMC = CODPMC, .TIPEDEPUBALVPMC = TIPEDEPUBALVPMC})

        If (startTransaction) Then
            daoBase.connector.BeginTransaction()
        End If
        Try

            Dim promocaoMixBO As New PromocaoMixBO()
            promocaoMixBO.UpdateIndicadorValidadeRegraMIX(CODPMC, 1, False)

            'For Each item In listaOld
            '    If (Not Me.Exists(lista, item)) Then
            '        Me.DeleteT(item, False)
            '    End If
            'Next

            For Each item In lista
                item.CODPMC = CODPMC
                item.TIPEDEPUBALVPMC = TIPEDEPUBALVPMC
                item.CODGRPMIXMERPMC = IIf(item.CODGRPMIXMERPMC.GetValueOrDefault() = 0, Convert.ToDecimal(1), item.CODGRPMIXMERPMC)
                item.QDEPTOMERPMC = 1
                item.VLRCNVUNDVNDDNMPMC = 1
                item.VLRCNVUNDVNDNUMPMC = 1
                item.NUMSEQGRPMIXPMC = 1
                item.VLRUNTPCOBRT = 1
                If (Not Me.Exists(listaOld, item)) Then
                    'DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).Inserir(item)
                    Me.InsertT(item, False)
                End If
            Next

            If (startTransaction) Then
                daoBase.connector.Commit()
            End If
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw (ex)
        End Try
    End Sub

    Function Exists(ByVal list As List(Of VO.PromocaoMixVisualizarVigente), ByVal obj As VO.PromocaoMixVisualizarVigente) As Boolean
        Return list.Where(Function(f) f.CODMER = obj.CODMER And f.CODGRPMIXMERPMC = obj.CODGRPMIXMERPMC).FirstOrDefault() IsNot Nothing
    End Function



    Public Function SelectValoresUnitariosBrutoDeVendaDaMercadoria(ByVal CODFILEMP As Decimal, ByVal lstMer As List(Of Decimal)) As List(Of VO.PromocaoMixVisualizarVigente)
        Return DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).SelectValoresUnitariosBrutoDeVendaDaMercadoria(CODFILEMP, lstMer)
    End Function

    Public Function SelectValoresUnitariosBrutoDeVendaDaMercadoria2(ByVal CODFILEMP As Decimal, ByVal lstMer As List(Of Decimal)) As List(Of VO.PromocaoMixVisualizarVigente)
        Return DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).SelectValoresUnitariosBrutoDeVendaDaMercadoria2(CODFILEMP, lstMer)
    End Function

    Public Function SelectValoresUnitariosBrutoDeVendaDaMercadoriaCusto(ByVal CODACOCMC As Decimal, ByVal lstMer As List(Of Decimal)) As List(Of VO.PromocaoMixVisualizarVigente)
        Return DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).SelectValoresUnitariosBrutoDeVendaDaMercadoriaCusto(CODACOCMC, lstMer)
    End Function

    Public Function MixVigentePromocao(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)
        Return DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).MixVigentePromocao(CODPMC, TIPEDEPUBALVPMC)
    End Function

    Public Function SelectRelacaoPromocaoSeqGrupoMix(ByVal CODPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)
        Return DirectCast(daoBase, PromocaoMixVisualizarVigenteDAO).SelectRelacaoPromocaoSeqGrupoMix(CODPMC)
    End Function


End Class