﻿Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvo)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaPublicoAlvoDAO()
    End Sub

    Public Function SelectCountPublicoAlvo(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As VO.RelacaoAcaoMecanicaPublicoAlvo
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoDAO).SelectCountPublicoAlvo(CODACOCMC, NUMSEQRGRACOCMC)
    End Function
End Class
