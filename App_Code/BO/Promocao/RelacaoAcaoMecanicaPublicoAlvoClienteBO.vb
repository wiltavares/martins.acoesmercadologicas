﻿Imports System.Data
Imports System.Data.OleDb
Imports Core
Imports NPOI.HSSF.Extractor
Imports NPOI.HSSF.UserModel

Public Class RelacaoAcaoMecanicaPublicoAlvoClienteBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaPublicoAlvoClienteDAO()
    End Sub

    'Dim stub As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente) = New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)

    'Public Overloads Function ListT(ByVal param As VO.RelacaoAcaoMecanicaPublicoAlvoCliente) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)


    '    Return stub.Where(Function(f) (param.CODACOCMC Is Nothing Or f.CODACOCMC = param.CODACOCMC) _
    '                            And (param.NUMSEQRGRACOCMC Is Nothing Or f.NUMSEQRGRACOCMC = param.NUMSEQRGRACOCMC) _
    '                            And (param.CODCLI Is Nothing Or f.CODCLI = param.CODCLI)).ToList()
    'End Function

    'Public Overloads Function SelectT(ByVal param As VO.RelacaoAcaoMecanicaPublicoAlvoCliente) As VO.RelacaoAcaoMecanicaPublicoAlvoCliente

    '    Return stub.Where(Function(f) f.CODACOCMC = param.CODACOCMC And f.NUMSEQRGRACOCMC = param.NUMSEQRGRACOCMC).FirstOrDefault()
    'End Function

    'Public Overloads Function InsertT(voList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente), Optional startTransaction As Boolean = True) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)
    '    For Each item In voList
    '        stub.Add(item)
    '    Next
    '    Return voList
    'End Function

    'Public Overloads Function UpdateT(vo As VO.RelacaoAcaoMecanicaPublicoAlvoCliente, Optional startTransaction As Boolean = True) As VO.RelacaoAcaoMecanicaPublicoAlvoCliente

    '    Dim old = SelectT(vo)
    '    stub.Remove(old)

    '    stub.Add(vo)
    '    Return vo
    'End Function

    'Public Overloads Sub DeleteT(vo As VO.RelacaoAcaoMecanicaPublicoAlvoCliente, Optional startTransaction As Boolean = True)

    '    Dim old = SelectT(vo)
    '    stub.Remove(old)

    'End Sub


    'Public Overloads Function InsertList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, voList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)

    '    MyBase.daoBase.connector.BeginTransaction()
    '    Try

    '        For Each item In voList
    '            item.CODACOCMC = CODACOCMC
    '            item.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
    '            MyBase.InsertT(item)
    '        Next

    '        MyBase.daoBase.connector.Commit()

    '    Catch ex As Exception
    '        MyBase.daoBase.connector.Rollback()
    '    End Try

    '    Return voList
    'End Function



    Public Function BuscarListaCliente(vo As VO.RelacaoAcaoMecanicaPublicoAlvoCliente) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoClienteDAO).BuscarListaCliente(vo)
    End Function

    Public Function ListarPublicoAlvoCliente(vo As VO.RelacaoAcaoMecanicaPublicoAlvoCliente) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoClienteDAO).ListarPublicoAlvoCliente(vo)
    End Function


    Public Sub AddPesquisa(ByRef filtro As VO.RelacaoAcaoMecanicaPublicoAlvoCliente)
        Dim voList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente) = Me.BuscarListaCliente(filtro)

        If (voList.Count > 30000) Then
            Throw New Exception(String.Format("Esta consulta resuta em {0} registros, somente e permitido inserir 30000 por vez. Verifique os filtros", voList.Count))
        End If

        Dim mercadoriasAdicionadas As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente) = Me.ListarPublicoAlvoCliente(New VO.RelacaoAcaoMecanicaPublicoAlvoCliente With {.CODACOCMC = filtro.CODACOCMC, .NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC})
        For Each novaFamiliaMercadoria In voList
            If (mercadoriasAdicionadas.Where(Function(f) f.CODCLI = novaFamiliaMercadoria.CODCLI).Count() = 0) Then
                If (Not (novaFamiliaMercadoria.CODCLI = 0)) Then
                    novaFamiliaMercadoria.CODACOCMC = filtro.CODACOCMC
                    novaFamiliaMercadoria.NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC
                    InsertT(novaFamiliaMercadoria)
                End If
            End If
        Next
    End Sub

    Public Function ImportarExelCliente(path As String, ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As String
        Try

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente) = ListaExcel(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            Dim erros As New StringBuilder()

            Dim clienteAdicionado As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente) = Me.ListarPublicoAlvoCliente(New VO.RelacaoAcaoMecanicaPublicoAlvoCliente With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

            If IsNothing(dados) Or dados.Count = 0 Then
                erros.AppendLine("Este arquivo não possui uma relação de codigos de clientes para importar")
            ElseIf dados.Count > 30000 Then
                erros.AppendLine("Este arquivo possui mais de 30000 codigos de clientes para importar")
            Else
                For Each novoCliente In dados

                    Dim relacaoAcaoMecanicaPublicoAlvoClienteDAO As New RelacaoAcaoMecanicaPublicoAlvoClienteDAO()
                    Dim verificaSeClienteExiste As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente) = relacaoAcaoMecanicaPublicoAlvoClienteDAO.VerificaSeClienteExiste(novoCliente.CODCLI)
                    If (verificaSeClienteExiste.Count <= 0) Then
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).AppendLine(" Não foi encontrado cliente com este código")
                        Continue For
                    End If

                    Dim verificaClienteJaAdicionado As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente) = clienteAdicionado.Where(Function(f) f.CODCLI = novoCliente.CODCLI).ToList
                    If (verificaClienteJaAdicionado.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).AppendLine(" Cliente já adicionado.")
                        Continue For
                    End If


                    Dim verificaClientePublicadoXLS As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente) = dados.Where(Function(f) f.CODCLI = novoCliente.CODCLI AndAlso f.CODACOCMC IsNot Nothing).ToList
                    If (verificaClientePublicadoXLS.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).AppendLine(" Cliente duplicado na planilha.")
                        Continue For
                    End If

                    Try
                        novoCliente.CODACOCMC = CODACOCMC
                        novoCliente.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                        InsertT(novoCliente)
                    Catch ex As Exception
                        erros.Append("Cliente: ").Append(novoCliente.CODCLI).Append(" - ").Append(novoCliente.NOMCLI).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                        Continue For
                    End Try
                Next
            End If
            Return erros.ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function ListaExcel(path As String, ByRef erros As String) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)
        Dim mercadoriasImportadas As New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (Not (alinha.Count = 1 Or alinha.Count = 2)) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 1 coluna de dados sendo a coluna ""Codigo do Cliente"" opcionalmente voce pode informar a coluna ""Nome do Cliente"", esta informação é apenas informativa", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo do Cliente") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo do Cliente""", NumLinha)
                    Return Nothing
                End If
                If (alinha.Count = 2 AndAlso alinha(1).Replace("""", "") <> "Nome do Cliente") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Nome do Cliente""", NumLinha)
                    Return Nothing
                End If

            Else ' Linhas de dados
                Dim novoItem As New VO.RelacaoAcaoMecanicaPublicoAlvoCliente
                Dim CODCLI As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODCLI)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo do Cliente"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If
                novoItem.CODCLI = CODCLI

                If (alinha.Count = 2) Then
                    novoItem.NOMCLI = alinha(1).Replace("""", "")
                End If

                mercadoriasImportadas.Add(novoItem)
            End If
            NumLinha += 1
        Next

        Return mercadoriasImportadas
    End Function


End Class
