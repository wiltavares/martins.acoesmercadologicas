﻿Imports System.Data
Imports NPOI.HSSF.UserModel
Imports NPOI.HSSF.Extractor

Public Class PromocaoBrindeHistoricoBO
    Inherits BOBase(Of VO.PromocaoBrindeHistorico)

    Public Sub New()
        MyBase.daoBase = New PromocaoBrindeHistoricoDAO()
    End Sub




    Public Overloads Function InsertT(vo As VO.PromocaoBrindeHistorico, Optional startTransaction As Boolean = True) As VO.PromocaoBrindeHistorico
        Dim last = MyBase.ListT(New VO.PromocaoBrindeHistorico With {.CODPMC = vo.CODPMC, .TIPEDENIVPRMPMC = vo.TIPEDENIVPRMPMC}).OrderByDescending(Function(f) f.NUMSEQ).FirstOrDefault()
        If (last Is Nothing) Then
            vo.NUMSEQ = 1
        Else
            vo.NUMSEQ = last.NUMSEQ + 1
        End If

        Return MyBase.InsertT(vo, startTransaction)

    End Function
    Public Function ListHistorico(TIPEDEPUBALVPMC As Decimal, CODPRMPMC As Decimal, CODPMC As Decimal, TIPEDENIVPRMPMC As Decimal) As List(Of VO.PromocaoBrindeHistorico)
        Return DirectCast(daoBase, PromocaoBrindeHistoricoDAO).ListHistorico(TIPEDEPUBALVPMC, CODPRMPMC, CODPMC, TIPEDENIVPRMPMC)
    End Function
End Class
