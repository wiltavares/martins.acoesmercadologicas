﻿Imports System.Data
Imports NPOI.HSSF.UserModel
Imports NPOI.HSSF.Extractor

Public Class PromocaoBrindeBO
    Inherits BOBase(Of VO.PromocaoBrinde)

    Public Sub New()
        MyBase.daoBase = New PromocaoBrindeDAO()
    End Sub

    Public Function VerificarDadosVolateisDaMercadoria(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)
        Return DirectCast(daoBase, PromocaoBrindeDAO).VerificarDadosVolateisDaMercadoria(promocaoBrinde)
    End Function
    Public Function VerificarHistoricoDiarioMercadoriaLivroPrecos(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)
        Return DirectCast(daoBase, PromocaoBrindeDAO).VerificarHistoricoDiarioMercadoriaLivroPrecos(promocaoBrinde)
    End Function

    'Public Function SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)
    '    Return DirectCast(daoBase, PromocaoBrindeDAO).SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(promocaoBrinde)
    'End Function
    Public Function ListaParaGrid(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)
        Return DirectCast(daoBase, PromocaoBrindeDAO).ListaParaGrid(promocaoBrinde)
    End Function

    Public Function CarregaPromocao() As List(Of VO.PromocaoBrinde)
        Return DirectCast(daoBase, PromocaoBrindeDAO).CarregaBrinde()
    End Function

    Public Function SelectRelacaoPromocaoEValorPremio(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)
        Return DirectCast(daoBase, PromocaoBrindeDAO).SelectRelacaoPromocaoEValorPremio(promocaoBrinde)
    End Function

    Public Function SelectRelacaoPromocaoEValorPremioSemMerAco(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)
        Return DirectCast(daoBase, PromocaoBrindeDAO).SelectRelacaoPromocaoEValorPremioSemMerAco(promocaoBrinde)
    End Function

    Public Function SelectRelacaoPromocaoEValorPremioJoin(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)
        Return DirectCast(daoBase, PromocaoBrindeDAO).SelectRelacaoPromocaoEValorPremioJoin(promocaoBrinde)
    End Function

    Public Function SelectRelacaoPromocaoEPremioAdicional(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)
        Return DirectCast(daoBase, PromocaoBrindeDAO).SelectRelacaoPromocaoEPremioAdicional(promocaoBrinde)
    End Function
    Public Function RemoveBrinde(ByVal brinde As VO.PromocaoBrinde, Optional startTransaction As Boolean = True) As String
        Dim promocao As New PromocaoBO

        If (startTransaction) Then
            daoBase.connector.BeginTransaction()
        End If

        Try
            DirectCast(MyBase.daoBase, PromocaoBrindeDAO).DeleteT(brinde)

            DirectCast(MyBase.daoBase, PromocaoBrindeDAO).RemoveRelacaoPromocaoValor(brinde)

            'Abastecimentos do brinde.
            Dim abastecimentoBO As New AbastecimentoBO
            Dim historico = abastecimentoBO.HistoricoPromocaoItem(brinde.CODPMC, brinde.CODPRMPMC)
            For Each item In historico
                abastecimentoBO.DeleteT(item, False)
            Next


            If (startTransaction) Then
                daoBase.connector.Commit()
            End If

            Return "Sucesso"
        Catch ex As Exception
            If (startTransaction) Then
                daoBase.connector.Rollback()
            End If
            Throw ex
        End Try

        Return Nothing
    End Function

    Public Function InsertBrinde(ByVal promocaoBrinde As VO.PromocaoBrinde, Optional startTransaction As Boolean = True, Optional promocaoVO As VO.Promocao = Nothing) As String

        If (promocaoVO Is Nothing) Then
            Dim promocao As New PromocaoBO
            promocaoVO = promocao.SelectT(New VO.Promocao() With {.CODPMC = promocaoBrinde.CODPMC})
        End If

        If ((promocaoBrinde.QDEBDEMPLCTRPMC Is Nothing) Or (promocaoBrinde.QDEBDEMPLCTRPMC = 0)) Then
            Throw New Exception("Informe a quantidade a ser inserida.")
        End If

        If promocaoVO Is Nothing Then
            Throw New Exception("Promoção não encontrada.")
        End If

        'ValidarProdutoVendidoFracionado feita antes do post de create

        If (promocaoVO.TIPSITPMC = "E") Then
            Throw New Exception("Promoção Encerrada")
        End If

        Dim listaPromocaoBrinde As List(Of VO.PromocaoBrinde) = ListaParaGrid(New VO.PromocaoBrinde() With {.CODPMC = promocaoVO.CODPMC})

        If (listaPromocaoBrinde.Exists(Function(x) x.CODPRMPMC = promocaoBrinde.CODPRMPMC)) Then
            Throw New Exception("Já existe este um premio com esta faixa no grid!")
        End If

        If (promocaoBrinde.PERMERRSVPRMPMC <= 0) Then
            Throw New Exception("Percentual de reserva tem que ser maior que zero")
        End If

        If (promocaoBrinde.PERMERRSVPRMPMC > 100) Then
            Throw New Exception("Percentual de reserva tem que ser menor que 100")
        End If

        'VerificaQtd
        Dim codPmc As Decimal? = promocaoVO.CODPMC
        Dim codMer As Decimal? = promocaoBrinde.CODPRMPMC
        Dim codFilEmp As Decimal? = promocaoVO.CODFILEMP

        If ((codPmc Is Nothing Or codPmc = 0) Or (codMer Is Nothing Or codMer = 0) Or (codFilEmp Is Nothing Or codFilEmp = 0)) Then
            Throw New Exception("Parâmetros incorretos.")
        End If
        Dim promocaoBO As PromocaoBO = New PromocaoBO

        Dim lstVerificaQtdMinVndMenorQtdPmc As List(Of VO.Promocao) = promocaoBO.VerificaQtdMinVndMenorQtdPmcBrinde(New VO.Promocao With {.CODPMC = codPmc, .CODMER = codMer, .CODFILEMP = codFilEmp})
        If (lstVerificaQtdMinVndMenorQtdPmc.Count > 0) Then
            If ((lstVerificaQtdMinVndMenorQtdPmc(0).QDEPMC * promocaoBrinde.QDEBDEMPLCTRPMC) < lstVerificaQtdMinVndMenorQtdPmc(0).QDEMIN) Then
                Throw New Exception("Mercadoria " + lstVerificaQtdMinVndMenorQtdPmc(0).CODMER.ToString() + " - " + lstVerificaQtdMinVndMenorQtdPmc(0).DESMER + ", com qtde minima venda (" + lstVerificaQtdMinVndMenorQtdPmc(0).QDEMIN.ToString() + ")" + " maior que qtde mecanica promocao (" + lstVerificaQtdMinVndMenorQtdPmc(0).QDEPMC.ToString() + ")")
            End If
        End If

        Dim lstVerificaQtdPmcNaoMultBrinde As List(Of VO.Promocao) = promocaoBO.VerificaQtdPmcNaoMultBrinde(New VO.Promocao With {.CODPMC = codPmc, .CODMER = codMer, .CODFILEMP = codFilEmp})
        If (lstVerificaQtdPmcNaoMultBrinde.Count > 0) Then
            Throw New Exception("Mercadoria " + lstVerificaQtdPmcNaoMultBrinde(0).CODMER.ToString() + " - " + lstVerificaQtdPmcNaoMultBrinde(0).DESMER + ", com qtde promocao (" + lstVerificaQtdPmcNaoMultBrinde(0).QDEPMC.ToString() + ")" + " não múltiplo da qtde minima venda (" + lstVerificaQtdPmcNaoMultBrinde(0).QDEMIN.ToString() + ")")
        End If

        'Verifica se existe o item na filial.
        Dim mercadoriaEmpresaQuantidadeDAO As New MercadoriaEmpresaQuantidadeDAO()
        Dim infoEstoque = mercadoriaEmpresaQuantidadeDAO.SelectPorMercadoriaFilial(promocaoBrinde.CODPRMPMC, codFilEmp)
        If (infoEstoque Is Nothing) Then
            Throw New Exception(String.Format("Não foi encontrato informações de estoque para o brinde {0} na filial {1}!", promocaoBrinde.CODPRMPMC, codFilEmp))
        End If

        'Verifica se está precificado
        Dim acaoBO As New AcaoComercialBO()
        Dim acao = acaoBO.BuscaAcaoPorId(promocaoVO.CODACOCMC)

        Dim MovimentoBO As New MovimentoQuinzenalPrecoMercadoriaBO()
        Dim a = MovimentoBO.ConsultaPrecoUnitarioItem(promocaoBrinde.CODPRMPMC, codFilEmp, acao.TIPACOMCD)
        If a.Count = 0 Or (Not a.Any(Function(x) x.VLRCSTUNTMER > 0)) Then
            Throw New Exception("Brinde não precificado nessa filial!")
        End If

        If (startTransaction) Then
            daoBase.connector.BeginTransaction()
        End If
        Try

            'Brinde rbtTipoBrinde - INDISRBDEADI 
            If (promocaoBrinde.INDISRBDEADI Is Nothing Or promocaoBrinde.INDISRBDEADI = 0) Then
                'Normal
                Dim VLRUNTFNDMERPMC As Decimal = 0

                Dim acaoComercialDAO As AcaoComercialDAO = New AcaoComercialDAO
                Dim acaoComercialObj As VO.AcaoComercial = acaoComercialDAO.SelectAcaoComercialParaPromocao(New VO.AcaoComercial() With {.CODACOCMC = promocaoVO.CODACOCMC})

                If (acaoComercialObj IsNot Nothing) Then
                    If (acaoComercialObj.TIPACOMCD IsNot Nothing) Then
                        If (acaoComercialObj.TIPACOMCD = 5 Or acaoComercialObj.TIPACOMCD = 6) Then
                            Dim acaoComercialFunding As List(Of VO.AcaoComercial) = acaoComercialDAO.SelectValorFunding(New VO.AcaoComercial() With {.CODACOCMC = promocaoVO.CODACOCMC, .CODMER = promocaoBrinde.CODPRMPMC})

                            If (acaoComercialFunding Is Nothing) Then
                                Throw New Exception("O item não é principal nem substituto nesta ação comercial.")
                            End If

                            If (acaoComercialFunding.Count = 0) Then
                                Throw New Exception("O item não é principal nem substituto nesta ação comercial.")
                            End If

                            If (acaoComercialFunding IsNot Nothing) Then
                                VLRUNTFNDMERPMC = acaoComercialFunding(0).VLRFUNDING
                            Else
                                Throw New Exception("O item não é principal nem substituto nesta ação comercial.")
                            End If
                        End If
                    End If
                Else
                    Throw New Exception("Ação comercial não encontrada.")
                End If


                promocaoBrinde.TIPEDEPUBALVPMC = promocaoVO.TIPEDEPUBALVPMC
                promocaoBrinde.INDISRBDEADI = 0
                promocaoBrinde.QDEMERMAXPRMPMC = 0
                promocaoBrinde.QDEMERVNDPRMPMC = 0
                promocaoBrinde.VLRPRMPMC = 1
                promocaoBrinde.FLGTRSMSGFIMPRMPMC = "S"
                promocaoBrinde.CODCNDPGT = 1
                promocaoBrinde.VLRUNTVNDPRMPMC = 1
                promocaoBrinde.INDACUBDEPMCFAT = 1
                promocaoBrinde.VLRUNTFNDMERPMC = VLRUNTFNDMERPMC

                'Atualiza promocaoBrindeUpdate
                'UPDATET

                MyBase.daoBase.InsertT(promocaoBrinde)

                'Me.InserirLinhaT0161115(linhaT0108044)
                Dim acaoComercialOrcamento As RelacaoAcaoComercialPorItemOrcamentoDAO = New RelacaoAcaoComercialPorItemOrcamentoDAO
                Dim acaoComercialOrcamentoObj As VO.RelacaoAcaoComercialPorItemOrcamento = acaoComercialOrcamento.SelectT(New VO.RelacaoAcaoComercialPorItemOrcamento() With {.CODACOCMC = promocaoVO.CODACOCMC, .CODCENCST = 0, .CODFILEMP = promocaoVO.CODFILEMP, .CODIDTTSC = 0, .CODITEOCDACOCMC = "", .CODMER = promocaoBrinde.CODPRMPMC, .CODOPEFSCDSNACOCMC = "2,4", .CODUNDESRNGCOPEDSN = 0, .DESFRMUTZITEACOCMC = "", .DESOBSCPR = "", .NUMLNHITEOCDACOCMC = 0})


                If (promocaoVO.INDCOBBDEPMC = 1) Then  ' Cobrança de brinde
                    Dim lstPremioValor As List(Of VO.PromocaoBrinde) = DirectCast(MyBase.daoBase, PromocaoBrindeDAO).SelectRelacaoPromocaoEValorPremio(New VO.PromocaoBrinde() With {.CODPRMPMC = promocaoBrinde.CODPRMPMC, .CODPMC = promocaoVO.CODPMC})

                    Dim linhaRelacaoPremio As VO.PromocaoBrinde

                    If (lstPremioValor IsNot Nothing) Then
                        If (lstPremioValor.Count > 0) Then
                            linhaRelacaoPremio = lstPremioValor(0)
                        Else
                            linhaRelacaoPremio = New VO.PromocaoBrinde
                        End If
                    Else
                        linhaRelacaoPremio = New VO.PromocaoBrinde
                    End If

                    linhaRelacaoPremio.CODPRMPMC = promocaoBrinde.CODPRMPMC
                    If (acaoComercialOrcamentoObj IsNot Nothing) Then
                        linhaRelacaoPremio.VLRUNTLIQMER = acaoComercialOrcamentoObj.VLRPTCCLIITEACOCMC
                    Else
                        linhaRelacaoPremio.VLRUNTLIQMER = 0
                    End If
                    linhaRelacaoPremio.CODPMC = promocaoVO.CODPMC
                    linhaRelacaoPremio.INDCOBIPRMRI = 0
                    linhaRelacaoPremio.CODACOCMC = acaoComercialObj.CODACOCMC


                    If (promocaoBrinde.RATEAR = True) Then
                        linhaRelacaoPremio.INDCOBIPRMRI = 1
                    Else
                        linhaRelacaoPremio.INDCOBIPRMRI = 0
                    End If

                    linhaRelacaoPremio.PERBFCBDE = promocaoBrinde.PERBFCBDE
                    linhaRelacaoPremio.VLRUNTLIQMER = promocaoBrinde.VLRUNTLIQMER


                    'Inserir linhaRelacaoPremio T0161115
                    If (lstPremioValor Is Nothing) Then
                        DirectCast(MyBase.daoBase, PromocaoBrindeDAO).InserirRelacaoPromocaoValor(linhaRelacaoPremio)
                    End If
                    'Else
                    '    linhaRelacaoPremio.PERBFCBDE = 0
                End If
            Else
                'Adicional
                Dim VLRUNTFNDMERPMC As Decimal = 0

                Dim acaoComercialDAO As AcaoComercialDAO = New AcaoComercialDAO
                Dim acaoComercialObj As VO.AcaoComercial = acaoComercialDAO.SelectAcaoComercialParaPromocao(New VO.AcaoComercial() With {.CODACOCMC = promocaoVO.CODACOCMC})

                If (acaoComercialObj IsNot Nothing) Then
                    If (acaoComercialObj.TIPACOMCD IsNot Nothing) Then
                        If (acaoComercialObj.TIPACOMCD = 5 Or acaoComercialObj.TIPACOMCD = 6) Then
                            Dim acaoComercialFunding As List(Of VO.AcaoComercial) = acaoComercialDAO.SelectValorFunding(New VO.AcaoComercial() With {.CODACOCMC = promocaoVO.CODACOCMC, .CODMER = promocaoBrinde.CODPRMPMC})

                            If (acaoComercialFunding Is Nothing) Then
                                Throw New Exception("O item não é principal nem substituto nesta ação comercial.")
                            End If

                            If (acaoComercialFunding.Count = 0) Then
                                Throw New Exception("O item não é principal nem substituto nesta ação comercial.")
                            End If

                            If (acaoComercialFunding IsNot Nothing) Then
                                VLRUNTFNDMERPMC = acaoComercialFunding(0).VLRFUNDING
                            Else
                                Throw New Exception("O item não é principal nem substituto nesta ação comercial.")
                            End If
                        End If
                    End If
                Else
                    Throw New Exception("Ação comercial não encontrada.")
                End If


                promocaoBrinde.TIPEDEPUBALVPMC = promocaoVO.TIPEDEPUBALVPMC
                promocaoBrinde.INDISRBDEADI = 1
                promocaoBrinde.QDEMERMAXPRMPMC = 0
                promocaoBrinde.QDEMERVNDPRMPMC = 0
                promocaoBrinde.VLRPRMPMC = 1
                promocaoBrinde.FLGTRSMSGFIMPRMPMC = "S"
                promocaoBrinde.CODCNDPGT = 1
                promocaoBrinde.VLRUNTVNDPRMPMC = 1
                promocaoBrinde.INDACUBDEPMCFAT = 1
                promocaoBrinde.VLRUNTFNDMERPMC = VLRUNTFNDMERPMC

                'Atualiza promocaoBrindeUpdate
                MyBase.daoBase.InsertT(promocaoBrinde)

                'Me.InserirLinhaT0161115(linhaT0108044)
                Dim acaoComercialOrcamento As RelacaoAcaoComercialPorItemOrcamentoDAO = New RelacaoAcaoComercialPorItemOrcamentoDAO
                Dim acaoComercialOrcamentoObj As VO.RelacaoAcaoComercialPorItemOrcamento = acaoComercialOrcamento.SelectT(New VO.RelacaoAcaoComercialPorItemOrcamento() With {.CODACOCMC = promocaoVO.CODACOCMC, .CODCENCST = 0, .CODFILEMP = promocaoVO.CODFILEMP, .CODIDTTSC = 0, .CODITEOCDACOCMC = "", .CODMER = promocaoBrinde.CODPRMPMC, .CODOPEFSCDSNACOCMC = "2,4", .CODUNDESRNGCOPEDSN = 0, .DESFRMUTZITEACOCMC = "", .DESOBSCPR = "", .NUMLNHITEOCDACOCMC = 0})

                Dim lstPremioValor As List(Of VO.PromocaoBrinde) = Me.SelectRelacaoPromocaoEValorPremio(New VO.PromocaoBrinde() With {.CODPRMPMC = promocaoBrinde.CODPRMPMC, .CODPMC = promocaoVO.CODPMC})

                Dim linhaRelacaoPremio As VO.PromocaoBrinde

                If (lstPremioValor IsNot Nothing) Then
                    If (lstPremioValor.Count > 0) Then
                        linhaRelacaoPremio = lstPremioValor(0)
                    Else
                        linhaRelacaoPremio = New VO.PromocaoBrinde
                    End If
                Else
                    linhaRelacaoPremio = New VO.PromocaoBrinde
                End If

                linhaRelacaoPremio.CODPRMPMC = promocaoBrinde.CODPRMPMC
                If (acaoComercialOrcamentoObj IsNot Nothing) Then
                    linhaRelacaoPremio.VLRUNTLIQMER = acaoComercialOrcamentoObj.VLRPTCCLIITEACOCMC
                Else
                    linhaRelacaoPremio.VLRUNTLIQMER = 0
                End If
                linhaRelacaoPremio.CODPMC = promocaoVO.CODPMC
                linhaRelacaoPremio.INDCOBIPRMRI = 0
                linhaRelacaoPremio.CODACOCMC = acaoComercialObj.CODACOCMC

                If (promocaoVO.INDCOBBDEPMC = 1) Then
                    If (promocaoBrinde.RATEAR = True) Then
                        linhaRelacaoPremio.INDCOBIPRMRI = 1
                    Else
                        linhaRelacaoPremio.INDCOBIPRMRI = 0
                    End If

                    linhaRelacaoPremio.PERBFCBDE = promocaoBrinde.PERBFCBDE
                    linhaRelacaoPremio.VLRUNTLIQMER = promocaoBrinde.VLRUNTLIQMER

                    'Inserir linhaRelacaoPremio T0161115
                    If (lstPremioValor Is Nothing) Then
                        DirectCast(daoBase, PromocaoBrindeDAO).InserirRelacaoPromocaoValor(linhaRelacaoPremio)
                    End If
                Else
                    linhaRelacaoPremio.PERBFCBDE = 0
                End If
            End If




            If (startTransaction) Then
                daoBase.connector.Commit()
            End If
        Catch ex As Exception
            If (startTransaction) Then
                daoBase.connector.Rollback()
            End If
            Throw ex
        End Try

        Return Nothing
    End Function


    'Public Overloads Function InsertT(vo As VO.PromocaoBrinde, Optional startTransaction As Boolean = True) As VO.PromocaoBrinde
    '    vo.CODPRMPMC = DirectCast(daoBase, PromocaoBrindeDAO).SelectMax(vo.CODPMC).CODPRMPMC
    '    Return MyBase.InsertT(vo, startTransaction)
    'End Function


    Public Function ImportarExelBrinde(path As String, ByVal CODPMC As Decimal) As List(Of String)
        Try
            Dim erros As New List(Of String)

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.PromocaoBrinde) = ListaExcel(path, erroImportExcel)

            If (erroImportExcel <> String.Empty) Then
                erros.Add(erroImportExcel)
                Return erros
            End If

            'Buscar Promoção pelo ID
            Dim promocaoBO As New PromocaoBO()
            Dim promocao As VO.Promocao = promocaoBO.SelectT(New VO.Promocao With {.CODPMC = CODPMC})

            Dim BrindesAdicionados As List(Of VO.PromocaoBrinde) = Me.ListaParaGrid(New VO.PromocaoBrinde With {.CODPMC = CODPMC})

            For Each novoBrinde In dados

                Dim mercadoriaBO As New MercadoriaBO()
                Dim Brinde As VO.Mercadoria = mercadoriaBO.SelectT(New VO.Mercadoria With {.CODMER = novoBrinde.CODPRMPMC})



                'verificar se Brinde existe no cadastro de Mercadoria
                If (Brinde Is Nothing) Then
                    erros.Add(String.Format("LINHA: {0};  Não foi encontrado Mercadoria com este código", novoBrinde.CODPRMPMC))
                    Continue For
                End If


                Dim verificaBrindeJaFoiAdicionado As List(Of VO.PromocaoBrinde) = BrindesAdicionados.Where(Function(f) f.CODPRMPMC = novoBrinde.CODPRMPMC).ToList
                If (verificaBrindeJaFoiAdicionado.Count > 0) Then 'verifica se o Brinde ja foi cadastrado
                    erros.Add(String.Format("LINHA: {0};  Brinde já adicionado", novoBrinde.CODPRMPMC))
                    Continue For
                End If

                If (verificaBrindeJaFoiAdicionado.Count <= 0) Then
                    If (Brinde IsNot Nothing) Then
                        If (Not (novoBrinde.CODPRMPMC = 0)) Then
                            novoBrinde.CODPMC = CODPMC
                            novoBrinde.CODACOCMC = promocao.CODACOCMC
                            novoBrinde.DESPRMPMC = Brinde.DESMER
                            novoBrinde.PERMERRSVPRMPMC = 100
                            InsertBrinde(novoBrinde)
                        End If
                    End If
                End If
            Next
            Return erros
        Catch ex As Exception
            Dim erros As New List(Of String)
            erros.Add(ex.Message)
            Return erros
        End Try
    End Function
    Public Function ListaExcel(path As String, ByRef erros As String) As List(Of VO.PromocaoBrinde)
        Dim mercadoriasImportadas As New List(Of VO.PromocaoBrinde)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)
            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If


            If (alinha.Count <> 4) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 4 colunas de dados dispostas na seguinte ordem ""Codigo da Mercadoria"", ""Múltiplo Brinde"", ""Tipo - (0 = Normal/ 1 = Adicional)"" e ""Faixa""", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Mercadoria""", NumLinha)
                    Return Nothing
                End If
                If (alinha(1).Replace("""", "") <> "Múltiplo Brinde") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Múltiplo Brinde""", NumLinha)
                    Return Nothing
                End If
                If (alinha(2).Replace("""", "") <> "Tipo - (0 = Normal/ 1 = Adicional)") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na terceira coluna deve conter a coluna ""Tipo - (0 = Normal/ 1 = Adicional)""", NumLinha)
                    Return Nothing
                End If
                If (alinha(3).Replace("""", "") <> "Faixa") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na quarta coluna deve conter a coluna ""Faixa""", NumLinha)
                    Return Nothing
                End If
            Else ' Linhas de dados
                Dim mercadoria As New VO.PromocaoBrinde
                Dim CODPRMPMC As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODPRMPMC)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo de Mercadoria"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If
                mercadoria.CODPRMPMC = CODPRMPMC

                Dim QDEBDEMPLCTRPMC As Decimal
                If (Not Decimal.TryParse(alinha(1).Replace("""", ""), QDEBDEMPLCTRPMC)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Múltiplo Brinde"" válido", NumLinha, alinha(1))
                    Return Nothing
                End If
                mercadoria.QDEBDEMPLCTRPMC = QDEBDEMPLCTRPMC


                Dim INDISRBDEADI As Decimal
                If (Not Decimal.TryParse(alinha(2).Replace("""", ""), INDISRBDEADI)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é uma ""Tipo - (0 = Normal/ 1 = Adicional)"" válido", NumLinha, alinha(2))
                    Return Nothing
                End If
                mercadoria.INDISRBDEADI = INDISRBDEADI

                Dim TIPEDENIVPRMPMC As Decimal
                If (Not Decimal.TryParse(alinha(3).Replace("""", ""), TIPEDENIVPRMPMC)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é uma ""Faixa"" válida", NumLinha, alinha(3))
                    Return Nothing
                End If
                mercadoria.TIPEDENIVPRMPMC = TIPEDENIVPRMPMC

                mercadoriasImportadas.Add(mercadoria)
            End If
            NumLinha += 1
        Next
        Return mercadoriasImportadas
    End Function



    Public Function RemoverBrindePromocaoLiberada(TIPEDEPUBALVPMC As Decimal, CODPMC As Decimal, TIPEDENIVPRMPMC As Decimal, CODPRMPMC As Decimal, DATECSPRD As DateTime, CODFNCECS As Decimal) As String

        'Dim promocaoBrindes = MyBase.daoBase.ListT(New VO.PromocaoBrinde() With {.CODPMC = CODPMC, .TIPEDENIVPRMPMC = TIPEDENIVPRMPMC, .DATECSPRD = Nothing})
        Dim promocaoBrindes = DirectCast(daoBase, PromocaoBrindeDAO).ListarBrindePromocaoNaoExcluido(CODPMC, TIPEDENIVPRMPMC)
        If (promocaoBrindes.Count() <= 1) Then
            If TIPEDENIVPRMPMC <> 0 Then
                Return "Impossivel excluir este item, promoção precisa ter pelo menos um brinde na faixa: " + TIPEDENIVPRMPMC.ToString()
            End If
            Return "Impossivel excluir este item, promoção precisa ter pelo menos um brinde!"
        End If

        Try
            daoBase.connector.BeginTransaction()
            DirectCast(daoBase, PromocaoBrindeDAO).RemoverBrindePromocaoLiberada(TIPEDEPUBALVPMC, CODPMC, TIPEDENIVPRMPMC, CODPRMPMC, DATECSPRD, CODFNCECS)

            Dim promocaoBrindeHistoricoBO As New PromocaoBrindeHistoricoBO()
            promocaoBrindeHistoricoBO.InsertT(New VO.PromocaoBrindeHistorico With {.CODPMC = CODPMC, .TIPEDENIVPRMPMC = TIPEDENIVPRMPMC, .CODMER = CODPRMPMC, .CODFNC = CODFNCECS, .DATGRCHST = DATECSPRD, .INDSITMER = 0})

            daoBase.connector.Commit()
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

        Return "Sucesso"
    End Function

    Public Function RetornaBrindePromocaoLiberada(TIPEDEPUBALVPMC As Decimal, CODPMC As Decimal, TIPEDENIVPRMPMC As Decimal, CODPRMPMC As Decimal, DATECSPRD As DateTime, CODFNCECS As Decimal) As String

        Try
            daoBase.connector.BeginTransaction()
            DirectCast(daoBase, PromocaoBrindeDAO).RetornaBrindePromocaoLiberada(TIPEDEPUBALVPMC, CODPMC, TIPEDENIVPRMPMC, CODPRMPMC)

            Dim promocaoBrindeHistoricoBO As New PromocaoBrindeHistoricoBO()
            promocaoBrindeHistoricoBO.InsertT(New VO.PromocaoBrindeHistorico With {.CODPMC = CODPMC, .TIPEDENIVPRMPMC = TIPEDENIVPRMPMC, .CODMER = CODPRMPMC, .CODFNC = CODFNCECS, .DATGRCHST = DATECSPRD, .INDSITMER = 1})

            daoBase.connector.Commit()
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

        Return "Sucesso"
    End Function


End Class
