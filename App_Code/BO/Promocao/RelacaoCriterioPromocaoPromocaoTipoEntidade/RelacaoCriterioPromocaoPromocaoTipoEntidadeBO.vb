﻿Imports Core

Public Class RelacaoCriterioPromocaoPromocaoTipoEntidadeBO
    Inherits BOBase(Of VO.RelacaoCriterioPromocaoPromocaoTipoEntidade)

    Public Sub New()
        MyBase.daoBase = New RelacaoCriterioPromocaoPromocaoTipoEntidadeDAO()
    End Sub
    Public Function CarregaRelacaoCriterio(obj As VO.RelacaoCriterioPromocaoPromocaoTipoEntidade) As List(Of VO.RelacaoCriterioPromocaoPromocaoTipoEntidade)
        Return DirectCast(daoBase, RelacaoCriterioPromocaoPromocaoTipoEntidadeDAO).CarregaRelacaoCriterio(obj)
    End Function
    Public Function SelectRel(obj As VO.RelacaoCriterioPromocaoPromocaoTipoEntidade) As List(Of VO.RelacaoCriterioPromocaoPromocaoTipoEntidade)
        Return DirectCast(daoBase, RelacaoCriterioPromocaoPromocaoTipoEntidadeDAO).SelectRel(obj)
    End Function
End Class
