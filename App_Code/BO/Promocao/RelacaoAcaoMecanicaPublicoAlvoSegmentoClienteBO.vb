﻿Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteDAO()
    End Sub


    Public Function ListaTipoSegmentoCliente(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteDAO).ListaTipoSegmentoCliente(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Sub InsertList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteDAO = New RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteDAO()

            Dim SegmentoClienteSelecionadoListOLD As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente) = DAO.ListaTipoSegmentoClienteSelecionado(CODACOCMC, NUMSEQRGRACOCMC)

            For Each item In relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList
                item.CODACOCMC = CODACOCMC
                item.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                If Not Exists(SegmentoClienteSelecionadoListOLD, item) Then
                    MyBase.InsertT(item)
                End If

            Next

            For Each item In SegmentoClienteSelecionadoListOLD
                If Not Exists(relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList, item) Then
                    MyBase.DeleteT(item)
                End If

            Next

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Private Function Exists(ByRef pList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente), ByVal pObj As VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente) As Boolean
        Return pList.Where(Function(r) r.CODACOCMC = pObj.CODACOCMC And r.NUMSEQRGRACOCMC = pObj.NUMSEQRGRACOCMC And r.CODSGMNGCCLI = pObj.CODSGMNGCCLI).FirstOrDefault() IsNot Nothing
    End Function
End Class
