﻿Imports AutoMapper
Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoEstadoBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaPublicoAlvoEstadoDAO()
    End Sub

    Public Function ListarEstadoUniao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoEstadoDAO).ListarEstadoUniao(CODACOCMC, NUMSEQRGRACOCMC)
    End Function

    Public Sub InsertList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal relacaoAcaoMecanicaPublicoAlvoEstadoList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado))
        MyBase.daoBase.connector.BeginTransaction()
        Try

            Dim DAO As RelacaoAcaoMecanicaPublicoAlvoEstadoDAO = New RelacaoAcaoMecanicaPublicoAlvoEstadoDAO()

            Dim EstadoSelecionadoListOLD As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado) = DAO.ListarEstadoUniaoSelecionados(CODACOCMC, NUMSEQRGRACOCMC)

            For Each item In relacaoAcaoMecanicaPublicoAlvoEstadoList
                item.CODACOCMC = CODACOCMC
                item.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                If Not Exists(EstadoSelecionadoListOLD, item) Then
                    MyBase.InsertT(item)
                End If

            Next

            For Each item In EstadoSelecionadoListOLD
                If Not Exists(relacaoAcaoMecanicaPublicoAlvoEstadoList, item) Then
                    MyBase.DeleteT(item)
                End If

            Next

            MyBase.daoBase.connector.Commit()

        Catch ex As Exception
            MyBase.daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Private Function Exists(ByRef pList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado), ByVal pObj As VO.RelacaoAcaoMecanicaPublicoAlvoEstado) As Boolean
        Return pList.Where(Function(r) r.CODACOCMC = pObj.CODACOCMC And r.NUMSEQRGRACOCMC = pObj.NUMSEQRGRACOCMC And r.CODESTUNI = pObj.CODESTUNI).FirstOrDefault() IsNot Nothing
    End Function
End Class
