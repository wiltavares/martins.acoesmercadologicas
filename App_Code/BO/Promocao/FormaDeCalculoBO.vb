﻿Imports Core

Public Class FormaDeCalculoBO
    Inherits BOBase(Of VO.FormaDeCalculo)

    Public Sub New()
        MyBase.daoBase = New FormaDeCalculoDAO()
    End Sub

    Public Function CarregaDropFormaDeCalculo(ByVal DESEQCCALCTRPMC As String) As List(Of VO.FormaDeCalculo)
        Return DirectCast(daoBase, FormaDeCalculoDAO).CarregaDropFormaDeCalculo(DESEQCCALCTRPMC)
    End Function
End Class