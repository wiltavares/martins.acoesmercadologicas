﻿Imports System.Data
Imports NPOI.HSSF.Extractor
Imports NPOI.HSSF.UserModel

Public Class RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO
    Inherits BOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorDAO()
    End Sub

    Public Function BuscarListaRcaVendedor(vo As VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoRcaVendedorDAO).BuscarListaRcaVendedor(vo)
    End Function

    Public Function ListarPublicoAlvoRcaVendedor(vo As VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)
        Return DirectCast(daoBase, RelacaoAcaoMecanicaPublicoAlvoRcaVendedorDAO).ListarPublicoAlvoRcaVendedor(vo)
    End Function

    Public Sub AddPesquisa(ByRef filtro As VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)
        Dim voList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) = Me.BuscarListaRcaVendedor(filtro)
        Dim mercadoriasAdicionadas As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) = Me.ListarPublicoAlvoRcaVendedor(New VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor With {.CODACOCMC = filtro.CODACOCMC, .NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC})
        For Each novaFamiliaMercadoria In voList
            If (mercadoriasAdicionadas.Where(Function(f) f.CODREP = novaFamiliaMercadoria.CODREP).Count() = 0) Then
                If (Not (novaFamiliaMercadoria.CODREP = 0)) Then
                    novaFamiliaMercadoria.CODACOCMC = filtro.CODACOCMC
                    novaFamiliaMercadoria.NUMSEQRGRACOCMC = filtro.NUMSEQRGRACOCMC
                    InsertT(novaFamiliaMercadoria)
                End If
            End If
        Next
    End Sub

    Public Function ImportarExelRcaVendedor(path As String, ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As String
        Try

            Dim erroImportExcel As String = String.Empty
            Dim dados As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) = ListaExcel(path, erroImportExcel)
            If (erroImportExcel <> String.Empty) Then
                Return erroImportExcel
            End If

            If dados.Count = 0 Then
                Return "Este arquivo não possui uma relação de códigos de RCA/Vendendores para importar"
            End If


            Dim erros As New StringBuilder()

            Dim RcaVendedorAdicionado As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) = Me.ListarPublicoAlvoRcaVendedor(New VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

            For Each novoRcaVendedor In dados

                Dim relacaoAcaoMecanicaPublicoAlvoRcaVendedorDAO As New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorDAO()
                Dim verificaSeRcaVendedorExiste As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) = relacaoAcaoMecanicaPublicoAlvoRcaVendedorDAO.VerificaSeRcaVendedorExiste(novoRcaVendedor.CODREP)
                If (verificaSeRcaVendedorExiste.Count <= 0) Then
                    erros.Append("Linha ").Append(novoRcaVendedor.CODREP).Append(": RCA/Vendedor não cadastrado.").AppendLine()
                    Continue For
                End If


                'verificar se cliente existe no cadastro de cliente
                Dim verificaRcaVendedorJaAdicionado As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) = RcaVendedorAdicionado.Where(Function(f) f.CODREP = novoRcaVendedor.CODREP).ToList
                If (verificaRcaVendedorJaAdicionado.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                    erros.Append("Linha ").Append(novoRcaVendedor.CODREP).Append(": RCA/Vendedor já adicionado.").AppendLine()
                    Continue For
                End If

                Dim verificaRcaVendedorDuplicadoXLS As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) = dados.Where(Function(f) f.CODREP = novoRcaVendedor.CODREP AndAlso f.CODACOCMC IsNot Nothing).ToList
                If (verificaRcaVendedorDuplicadoXLS.Count > 0) Then 'verifica se o tipo de evento ja foi cadastrado
                    erros.Append("Linha: ").Append(novoRcaVendedor.CODREP).Append(": RCA/Vendedor duplicado na planilha.").AppendLine()
                    Continue For
                End If

                Try
                    If (Not (novoRcaVendedor.CODREP = 0)) Then
                        novoRcaVendedor.CODACOCMC = CODACOCMC
                        novoRcaVendedor.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                        InsertT(novoRcaVendedor)
                    End If
                Catch ex As Exception
                    erros.Append("Linha: ").Append(novoRcaVendedor.CODREP).Append(" Erro ao inseri-lo: ").AppendLine(ex.Message)
                    Continue For
                End Try

            Next

            Return erros.ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ListaExcel(path As String, ByRef erros As String) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)
        Dim linhasImportadas As New List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)


            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (Not (alinha.Count = 1 Or alinha.Count = 2)) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 1 coluna de dados sendo a coluna ""Codigo do RCA/Vendedor"" opcionalmente voce pode informar a coluna ""Nome do RCA/Vendedor"", esta informação é apenas informativa", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo do RCA/Vendedor") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo do RCA/Vendedor""", NumLinha)
                    Return Nothing
                End If
                If (alinha.Count = 2 AndAlso alinha(1).Replace("""", "") <> "Nome do RCA/Vendedor") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Nome do RCA/Vendedor""", NumLinha)
                    Return Nothing
                End If

            Else ' Linhas de dados
                Dim novaLinha As New VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor
                Dim CODREP As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODREP)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo do RCA/Vendedor"" válido", NumLinha, alinha(0))
                    Return Nothing
                End If
                novaLinha.CODREP = CODREP

                If (alinha.Count = 2) Then
                    novaLinha.NOMREP = alinha(1).Replace("""", "")
                End If

                linhasImportadas.Add(novaLinha)
            End If
            NumLinha += 1
        Next

        Return linhasImportadas
    End Function


End Class
