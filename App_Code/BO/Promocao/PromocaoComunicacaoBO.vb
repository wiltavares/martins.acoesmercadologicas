﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Core

Public Class PromocaoComunicacaoBO
    Inherits BOBase(Of VO.PromocaoComunicacao)

    Public Function GetRelPromocaoComunicacao(codPmc As Decimal) As List(Of VO.PromocaoComunicacao)
        Return DirectCast(daoBase, PromocaoComunicacaoDAO).GetRelPromocaoComunicacao(codPmc)
    End Function

    Public Function ObterParametroPromocaoComunicacao(codPmc As Decimal) As List(Of VO.PromocaoComunicacao)
        Return DirectCast(daoBase, PromocaoComunicacaoDAO).ObterParametroPromocaoComunicacao(codPmc)
    End Function
    Public Function ObterOpcoesPromocaoComunicacao() As List(Of VO.PromocaoComunicacao)
        Return DirectCast(daoBase, PromocaoComunicacaoDAO).ObterOpcoesPromocaoComunicacao()
    End Function

    Public Function SalvarComunicacao(CODPMC As Decimal, param As List(Of VO.PromocaoComunicacao), Optional startTransaction As Boolean = True) As Object

        If (startTransaction) Then
            daoBase.connector.BeginTransaction()
        End If

        Try

            'Apaga Rel
            DirectCast(daoBase, PromocaoComunicacaoDAO).DeletarRelPmc(CODPMC)
            DirectCast(daoBase, PromocaoComunicacaoDAO).DeletarParamPmc(CODPMC)

            'Insere Rel
            For Each obj In param
                DirectCast(daoBase, PromocaoComunicacaoDAO).InserirRelPmc(CODPMC, obj.TIPCMNPMC)

                If (obj.TIPCMNPMC = 3) Then
                    DirectCast(daoBase, PromocaoComunicacaoDAO).InserirParamPmc(CODPMC, obj.TIPDVGPMCLIVPCO, obj.CODSMBDVGPMCLIVPCO.ToUpper())
                End If
            Next

            If (startTransaction) Then
                daoBase.connector.Commit()
            End If
        Catch ex As Exception
            If (startTransaction) Then
                daoBase.connector.Rollback()
            End If
            Throw ex
        End Try
        Return Nothing
    End Function


    Public Sub New()
        MyBase.daoBase = New PromocaoComunicacaoDAO()
    End Sub

End Class
