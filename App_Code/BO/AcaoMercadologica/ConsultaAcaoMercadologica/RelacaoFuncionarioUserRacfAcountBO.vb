﻿Imports Core
Public Class RelacaoFuncionarioUserRacfAcountBO
    Inherits BOBase(Of VO.RelacaoFuncionarioUserRacfAcount)

    Public Sub New()
        MyBase.daoBase = New RelacaoFuncionarioUserRacfAcountDAO()
    End Sub
    Public Function BuscaPorUsuarioRede(vo As VO.RelacaoFuncionarioUserRacfAcount) As List(Of VO.RelacaoFuncionarioUserRacfAcount)

        Return DirectCast(daoBase, RelacaoFuncionarioUserRacfAcountDAO).BuscaPorUsuarioRede(vo)

    End Function

    Public Function BuscaPorCodFnc(vo As VO.RelacaoFuncionarioUserRacfAcount) As VO.RelacaoFuncionarioUserRacfAcount

        Return DirectCast(daoBase, RelacaoFuncionarioUserRacfAcountDAO).BuscaPorCodFnc(vo)

    End Function
End Class
