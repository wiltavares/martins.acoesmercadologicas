﻿Imports Core
Imports System.Data.OleDb
Imports System.Data
Imports NPOI.HSSF.UserModel
Imports NPOI.HSSF.Extractor
Imports AutoMapper


Public Class RelacaoItemPrincipalItemSubstitutoAcaoComercialBO
    Inherits BOBase(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)

    Public Sub New()
        MyBase.daoBase = New RelacaoItemPrincipalItemSubstitutoAcaoComercialDAO()
    End Sub

    Public Function SelectRelacaoItemPrincipalEItemSubstitutoDaAcaoComercial(idAcao As Decimal?, codMer As Decimal?) As List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)

        Return DirectCast(daoBase, RelacaoItemPrincipalItemSubstitutoAcaoComercialDAO).SelectRelacaoItemPrincipalEItemSubstitutoDaAcaoComercial(idAcao, codMer)

    End Function

    Public Sub SalvarItensSubs(novos As List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO), velhos As List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO))
        Try
            daoBase.connector.BeginTransaction()

            For Each item In novos
                If (Not Exists(velhos, item)) Then
                    DirectCast(daoBase, RelacaoItemPrincipalItemSubstitutoAcaoComercialDAO).SalvarItensSubs(item)
                End If
            Next

            For Each item In velhos
                If (Not Exists(novos, item)) Then
                    DirectCast(daoBase, RelacaoItemPrincipalItemSubstitutoAcaoComercialDAO).DeleteT(item)
                End If
            Next
            daoBase.connector.Commit()
        Catch
            daoBase.connector.Rollback()
            Throw
        End Try
    End Sub


    Public Function Exists(listFind As List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO), objectFind As VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO) As Boolean
        Dim lista As List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO) = listFind.Where(Function(p) p.CODMERSBT = objectFind.CODMERSBT And p.CODACOCMC = objectFind.CODACOCMC And p.NUMLNHITEOCDACOCMC = objectFind.NUMLNHITEOCDACOCMC And p.CODMERSBT = objectFind.CODMERSBT).ToList
        If (IsNothing(lista) OrElse lista.Count = 0) Then
            Return False
        Else
            Return True
        End If
    End Function


    Public Function csnVlrAcoAtu(ByVal iCodAcoCmc As Integer) As List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)
        Return DirectCast(daoBase, RelacaoItemPrincipalItemSubstitutoAcaoComercialDAO).csnVlrAcoAtu(iCodAcoCmc)
    End Function



    Public Sub ImportarExelItemOrcamento(INDTIPOPEDSNACOCMC As Decimal, CODUNDESRNGCOPEDSN As Nullable(Of Decimal), CODCENCST As Decimal, DESFRMUTZITEACOCMC As String, acaoCache As AcaoComercialViewModel, path As String, ByRef erros As List(Of String))

        Dim mercadoriasExcel As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)
        Dim erroImportExcel As String = String.Empty
        mercadoriasExcel = ListaExcelItemOrcamento(path, erroImportExcel)
        If (erroImportExcel <> String.Empty) Then
            erros.Add(erroImportExcel)
            Return
        End If


        If (mercadoriasExcel.Count = 0) Then
            erros.Add(String.Format("Verifique se as colunas da planilha estão conforme o template disponibilizado. Não foi possivel ler os dados da planilha"))
            Return
        End If

        Dim boRelacaoItemPrincipalItemSubstitutoAcaoComercial As New RelacaoItemPrincipalItemSubstitutoAcaoComercialBO()

        Dim boRelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPreco As New RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoBO()

        Dim boRelacaoAcaoComercialPorItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoBO()

        Dim filialEmpresaCorporativoBO As New FilialEmpresaCorporativoBO()


        Dim operacaoFiscalBO As New OperacaoFiscalBO()
        Dim operacao = operacaoFiscalBO.ListT(New VO.OperacaoFiscal With {.CODOPEFSCDSNACOCMC = INDTIPOPEDSNACOCMC}).FirstOrDefault

        Dim tipoDeEventoBO As New TipoDeEventoBO
        Dim tipoEvento = tipoDeEventoBO.SelectT(New VO.TipoDeEvento With {.TIPEVTACOCMC = acaoCache.TIPEVTACOCMC})

        Dim count As Integer
        For Each linha In mercadoriasExcel

            linha.DESFRMUTZITEACOCMC = DESFRMUTZITEACOCMC            'Forma de Utilização:
            If (CODUNDESRNGCOPEDSN IsNot Nothing) Then
                linha.CODUNDESRNGCOPEDSN = CODUNDESRNGCOPEDSN            'BU
            Else
                linha.CODUNDESRNGCOPEDSN = 0
            End If
            linha.CODCENCST = CODCENCST                              'Centro de Custo:
            linha.CODOPEFSCDSNACOCMC = operacao.CODOPEFSCDSNACOCMC   'Tipo de Operação - Codigo 
            linha.DESOPEFSCDSNACOCMC = operacao.DESOPEFSCDSNACOCMC   'Tipo de Operação - Descricao
            linha.CODACOCMC = acaoCache.CODACOCMC                    'Código da ação comercial
            linha.DATUTZITEACOCMC = Date.Now.Date                    'Dt. de Utilização do Item:


            If IsNothing(linha.VLRPTCCLIITEACOCMC) Then              ' Valor pago pelo cliente: - Vem do excel
                linha.VLRPTCCLIITEACOCMC = 0 'valor participacao cliente cliente no item.
            End If


            linha.INDGRCREQAUTITERSU = operacao.INDGRCREQAUTITERSU
            If operacao.INDGRCREQAUTITERSU = 1 Then
                linha.CODSTAITERSU = 1
                linha.DESSTAITERSU = "Em Aberto"
            Else
                linha.CODSTAITERSU = 0
                linha.DESSTAITERSU = ""
            End If

            linha.INDBDEOPEDSNACOCMC = operacao.INDBDEOPEDSNACOCMC 'Indicador de brinde
            linha.INDTIPOPEDSNACOCMC = operacao.INDTIPOPEDSNACOCMC 'Indicador do tipo de operação
            linha.INDUNDESRNGCACOCMC = operacao.INDUNDESRNGCACOCMC 'Nome do tipo de operação

            linha.QDEUTZOPEDSNACOCMC = 0 'Quantidade Utilizada
            linha.VLRUTZOPEDSNACOCMC = 0 'Valor utilizado
            linha.INDITEREQRSUVLRMNM = 0 'Indicador de valor minimo do item
            linha.QDEDIACMPITEACOCMC = 0
            linha.NUMCGCEMPFRN = 0
            linha.DESCTTRSU = 0
            linha.NUMLNHCTTRSU = 0
            linha.IDTITERSU = 0
            linha.NUMCTTFRN = 0
            linha.CODIDTTSC = 0
            linha.IDTTERPGTITERSU = 0
            linha.IDTLCLEMPFRNITERSU = 0
            linha.IDTEMPFRNITERSU = 0

            If linha.INDITERSUFRNUNC Then
                linha.INDITERSUFRNUNC = 1
            Else
                linha.INDITERSUFRNUNC = 0
            End If

            If linha.INDITERSUARZPRVPGT Then
                linha.INDITERSUARZPRVPGT = 1
            Else
                linha.INDITERSUARZPRVPGT = 0
            End If

            linha.NUMLNHITEOCDACOCMC = acaoCache.ItensOrcamento.Max(Function(f) f.NUMLNHITEOCDACOCMC).GetValueOrDefault() + 1


            If IsNothing(linha.CODMER) Then
                erros.Add("Codigo da mercadoria não informado na planilha")
                Continue For
            End If

            Dim STRFRNACO As String
            If acaoCache.TIPACOMCD = 1 Then
                Dim fornecedores As List(Of Decimal?) = acaoCache.Fornecedores.Select(Function(f) f.CODFRN).ToList
                STRFRNACO = String.Join(",", fornecedores)
            ElseIf acaoCache.TIPACOMCD = 2 Then
                Dim fornecedores As List(Of Decimal?) = acaoCache.Carimbos.Select(Function(f) f.CODFRN).ToList
                STRFRNACO = String.Join(",", fornecedores)
            ElseIf acaoCache.TIPACOMCD = 1 Then
                Dim fornecedores As List(Of Decimal?) = acaoCache.Bonificacoes.Select(Function(f) f.CODFRN).ToList
                STRFRNACO = String.Join(",", fornecedores)
            Else
                Dim fornecedores As List(Of Decimal?) = acaoCache.Premiacao.Select(Function(f) f.CODFRN).ToList
                STRFRNACO = String.Join(",", fornecedores)
            End If

            Dim boMercadoria As New MercadoriaBO
            Dim mercadoria = boMercadoria.ConsultaItem(linha.CODMER, "", linha.CODFILEMP, operacao.INDBDEOPEDSNACOCMC, STRFRNACO).FirstOrDefault()
            If IsNothing(mercadoria) Then
                erros.Add(String.Format("Não foi encontrado mercadoria com o código {0} na base de dados", linha.CODMER))
                Continue For
            End If

            linha.CODITEOCDACOCMC = mercadoria.DESMER    ' Mercadoria - Descrição


            Dim filial = filialEmpresaCorporativoBO.ListT(New VO.FilialEmpresaCorporativo With {.CODFILEMP = linha.CODFILEMP}).FirstOrDefault
            If IsNothing(filial) Then
                erros.Add(String.Format("O Item {0} - {1} não foi importado pois nao existe filial com o codigo {2}", linha.CODMER, linha.CODITEOCDACOCMC, linha.CODFILEMP))
                Continue For
            End If
            linha.NOMFILEMP = filial.NOMFILEMP                       'Filial - Nome


            If IsNothing(linha.QDEITEOCDACOCMC) Then
                erros.Add(String.Format("Informe Quantidade a ser utilizada do item {0} - {1}", linha.CODMER, linha.CODITEOCDACOCMC))
                Continue For
            End If


            count = boRelacaoItemPrincipalItemSubstitutoAcaoComercial.ListT(New VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO With {.CODACOCMC = acaoCache.CODACOCMC, .CODMERSBT = linha.CODMER}).Count
            If count > 0 Then
                erros.Add(String.Format("O Item {0} - {1} já está cadastrado como item substituto nesta ação.", linha.CODMER, linha.CODITEOCDACOCMC))
                Continue For
            End If


            If acaoCache.ItensOrcamento.Count > 0 Then
                If (tipoEvento.INDCPHBTB = 1) Then ' verifica se for campanha b2b permite mercadorias
                    If (acaoCache.ItensOrcamento.Where(Function(f) f.CODMER = linha.CODMER And f.CODOPEFSCDSNACOCMC = INDTIPOPEDSNACOCMC And f.CODFILEMP = linha.CODFILEMP).FirstOrDefault() IsNot Nothing) Then
                        erros.Add(String.Format("O Item {0} - {1} já foi adicionado a esta Ação Comercial/Empresa!", linha.CODMER, linha.CODITEOCDACOCMC))
                        Continue For
                    End If
                Else
                    If (acaoCache.ItensOrcamento.Where(Function(f) f.CODMER = linha.CODMER And f.CODOPEFSCDSNACOCMC = INDTIPOPEDSNACOCMC).FirstOrDefault() IsNot Nothing) Then
                        erros.Add(String.Format("O Item {0} - {1} já foi adicionado a esta Ação Comercial!", linha.CODMER, linha.CODITEOCDACOCMC))
                        Continue For
                    End If
                End If
            End If

            If operacao.INDTIPOPEDSNACOCMC = 1 Then
                If boRelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPreco.ValidaProdutoFracionado(linha.CODFILEMP, linha.CODMER).Count <> 0 Then
                    erros.Add(String.Format("Atenção ! A mercadoria {0} - {1} que está sendo inserida é fracionada e pode ser fornecida pelo CAD há regiões atendidas por um CDR. Caso isto aconteça as regioes atendidas pelo CDR não conseguirão fazer bonificações, consumo ou promoções com esta mercadoria pelo CAD que abastece o CDR.", linha.CODMER, linha.CODITEOCDACOCMC))
                    Continue For
                End If
            End If

            Dim boMovimentoQuinzenal As New MovimentoQuinzenalPrecoMercadoriaBO
            Dim movimento As VO.MovimentoQuinzenalPrecoMercadoriaVO = boMovimentoQuinzenal.ConsultaPrecoUnitarioItem(linha.CODMER, linha.CODFILEMP, acaoCache.TIPACOMCD).FirstOrDefault()
            If movimento Is Nothing Then
                linha.VLRUNTITEOCDACOCMC = 0.0 'Valor Unitário do Item: 
            Else
                If Trim(movimento.FLGMERPMC) = "S" Then
                    linha.VLRUNTITEOCDACOCMC = movimento.VLRCSTUNTMER 'Valor Unitário do Item: 
                Else
                    'Se for Ação do tipo Pré-Acordo.
                    If (acaoCache.TIPACOMCD = TipoAcao.PreAcordoVigencia Or acaoCache.TIPACOMCD = TipoAcao.PreAcordoValor) Then
                        linha.VLRUNTITEOCDACOCMC = movimento.VLRCSTUNTMER 'Valor Unitário do Item: 
                    Else 'Se não for Ação do tipo Pré-Acordo.
                        linha.VLRUNTITEOCDACOCMC = movimento.VLRCSTUNTMER / 0.78 'Valor Unitário do Item: 
                    End If
                End If
            End If

            linha.VLRPTCFRNITEACOCMC = linha.VLRUNTITEOCDACOCMC - linha.VLRPTCCLIITEACOCMC   'Valor pago pelo fornecedor:





            If IsNothing(linha.VLRPTCFRNITEACOCMC) Then
                linha.VLRPTCFRNITEACOCMC = 0 'valor participacao fornecedor no item.
            End If


            If Not IsNothing(linha.INDGRCREQAUTITERSU) AndAlso linha.INDGRCREQAUTITERSU = 1 Then
                Dim Str As Object
                Dim StrAux As String

                StrAux = "01." & tipoEvento.CODCNTCTB & "." & CODCENCST & ".00.0143.000.00.000" 'IdtCntCtb
                Str = boRelacaoAcaoComercialPorItemOrcamento.VldGrpSgmCtb(StrAux)
                If IsNumeric(Str) Then
                    If CLng(Str) = 0 Then
                        erros.Add(String.Format("O Item {0} - {1} NÃO foi adicionado: Não foi possível efetuar a validação do seguinte segmento contábil porque o mesmo retornou zero. Favor contactar o analista responsável.", linha.CODMER, linha.CODITEOCDACOCMC))
                        Continue For
                    Else
                        linha.IDTCNTCTB = CInt(Str)
                    End If
                Else
                    erros.Add(String.Format("O Item {0} - {1} NÃO foi adicionado: Não foi possível efetuar a validação de segmento contábil.", linha.CODMER, linha.CODITEOCDACOCMC))
                    Continue For
                End If


                'Busca usuario cadastrado no iPROC    
                Dim boperPeopple As New perAllPeopleFBO()
                Dim usuario = boperPeopple.PsqCadFnc(ControleAcesso.CodFnc())
                If usuario Is Nothing OrElse usuario.Count = 0 Then
                    erros.Add(String.Format("O Item {0} - {1} NÃO foi adicionado: Usuário não está cadastrado no iPROC. Contate o analista!", linha.CODMER, linha.CODITEOCDACOCMC))
                    Continue For
                End If
            Else
                linha.IDTCNTCTB = 0
            End If


            boRelacaoAcaoComercialPorItemOrcamento.InserirRelacao(linha)

            acaoCache.ItensOrcamento.Add(Mapper.Map(Of VO.RelacaoAcaoComercialPorItemOrcamento, RelacaoAcaoComercialPorItemOrcamentoViewModel)(linha))

        Next
    End Sub
    Public Function ListaExcelItemOrcamento(path As String, ByRef erros As String) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)
        Dim mercadoriasImportadas As New List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Dim workbook As HSSFWorkbook = ManipulacaoArquivo.OpenSampleWorkbook(path)
        Dim extractor As ExcelExtractor = New ExcelExtractor(workbook)

        Dim arg() As String = {vbCrLf, vbLf}
        Dim NumLinha As Int32 = 0
        For Each linha As String In extractor.Text.Split(arg, StringSplitOptions.None)

            Dim alinha As String() = linha.Split(vbTab)

            If (NumLinha = 0) Then 'Aba da planilha
                NumLinha += 1
                Continue For
            End If

            If (String.IsNullOrWhiteSpace(linha)) Then 'linhas em branco
                NumLinha += 1
                Continue For
            End If

            If (alinha.Count <> 4) Then
                erros = String.Format("Erro Linha {0}: O excel deve conter 4 colunas de dados dispostas na seguinte ordem ""Codigo da Filial"", ""Codigo da Mercadoria"", ""Quantidade"" e ""Valor Pago Pelo Cliente"", sendo que o ""Valor Pago Pelo Cliente"" pode ser 0 (zero)", NumLinha)
                Return Nothing
            End If

            If (NumLinha = 1) Then ' cabeçalhos do excel
                If (alinha(0).Replace("""", "") <> "Codigo da Filial") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na primeira coluna deve conter a coluna ""Codigo da Filial""", NumLinha)
                    Return Nothing
                End If
                If (alinha(1).Replace("""", "") <> "Codigo da Mercadoria") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na segunda coluna deve conter a coluna ""Codigo da Mercadoria""", NumLinha)
                    Return Nothing
                End If
                If (alinha(2).Replace("""", "") <> "Quantidade") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na terceira coluna deve conter a coluna ""Quantidade""", NumLinha)
                    Return Nothing
                End If
                If (alinha(3).Replace("""", "") <> "Valor Pago Pelo Cliente") Then
                    erros = String.Format("Erro Linha {0}: Verifique a ordem das colunas do excel. Na quarta coluna deve conter a coluna ""Valor Pago Pelo Cliente""", NumLinha)
                    Return Nothing
                End If
            Else ' Linhas de dados
                Dim RelacaoItemPrincipalItemSubstitutoAcaoComercial As New VO.RelacaoAcaoComercialPorItemOrcamento
                Dim CODFILEMP As Decimal
                If (Not Decimal.TryParse(alinha(0).Replace("""", ""), CODFILEMP)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo de Filial"" valido", NumLinha, alinha(0))
                    Return Nothing
                End If
                RelacaoItemPrincipalItemSubstitutoAcaoComercial.CODFILEMP = CODFILEMP

                Dim CODMER As Decimal
                If (Not Decimal.TryParse(alinha(1).Replace("""", ""), CODMER)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Codigo de Mercadoria"" valida", NumLinha, alinha(1))
                    Return Nothing
                End If
                RelacaoItemPrincipalItemSubstitutoAcaoComercial.CODMER = CODMER

                Dim QDEITEOCDACOCMC As Decimal
                If (Not Decimal.TryParse(alinha(2).Replace("""", ""), QDEITEOCDACOCMC)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é uma ""Quantidade"" valida", NumLinha, alinha(2))
                    Return Nothing
                End If
                RelacaoItemPrincipalItemSubstitutoAcaoComercial.QDEITEOCDACOCMC = QDEITEOCDACOCMC

                Dim VLRPTCCLIITEACOCMC As Decimal
                If (Not Decimal.TryParse(alinha(3).Replace("""", ""), VLRPTCCLIITEACOCMC)) Then
                    erros = String.Format("Erro Linha {0}: o valor ""{1}"" Não é um ""Valor Pago Pelo Cliente"" valido", NumLinha, alinha(3))
                    Return Nothing
                End If
                RelacaoItemPrincipalItemSubstitutoAcaoComercial.VLRPTCCLIITEACOCMC = VLRPTCCLIITEACOCMC

                mercadoriasImportadas.Add(RelacaoItemPrincipalItemSubstitutoAcaoComercial)
            End If
            NumLinha += 1
        Next

        Return mercadoriasImportadas
    End Function
End Class

