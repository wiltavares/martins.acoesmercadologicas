﻿Imports Core
Imports System.Globalization
Imports System.Linq

Public Class ExtratoVerbaBO
    Inherits BOBase(Of VO.ExtratoVerba)

    Public Sub New()
        MyBase.daoBase = New ExtratoVerbaDAO()
    End Sub

    Public Function ListaSaldoFornecedor(dadosFiltro As VO.ExtratoVerba) As List(Of VO.ExtratoVerba)

        Return DirectCast(daoBase, ExtratoVerbaDAO).ListaSaldoFornecedor(dadosFiltro)
    End Function
    Public Function ListaExtratoFornecedor(dadosFiltro As VO.ExtratoVerba) As List(Of VO.ExtratoVerba)

        Return DirectCast(daoBase, ExtratoVerbaDAO).ListaExtratoFornecedor(dadosFiltro)
    End Function

    Public Function ListaSaldoEvento(dadosFiltro As VO.ExtratoVerba) As List(Of VO.ExtratoVerba)

        Return DirectCast(daoBase, ExtratoVerbaDAO).ListaSaldoEvento(dadosFiltro)
    End Function

End Class



