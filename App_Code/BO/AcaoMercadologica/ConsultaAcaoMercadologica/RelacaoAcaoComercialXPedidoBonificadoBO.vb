﻿Imports Core

Public Class RelacaoAcaoComercialXPedidoBonificadoBO
    Inherits BOBase(Of VO.RelacaoAcaoComercialXPedidoBonificado)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoComercialXPedidoBonificadoDAO()
    End Sub

    Private boFornecedores As New RelacaoAcaoComercialxFornecedoresBO

    Public Function ListarPedidosDeCompra(codFnc As Decimal?, numPedCmp As Decimal?) As List(Of VO.RelacaoAcaoComercialXPedidoBonificado)

        Return DirectCast(daoBase, RelacaoAcaoComercialXPedidoBonificadoDAO).ListarPedidosDeCompra(codFnc, numPedCmp)

    End Function

    ''' <summary>
    ''' Buscar pedido de compra pelo codigo do fornecedor e numero do pedido
    ''' </summary>
    ''' <param name="codFnc"></param>
    ''' <param name="numPedCmp"></param>
    ''' <returns></returns>
    Public Function BuscarPedidoDeCompra(codFnc As Decimal, numPedCmp As Decimal) As VO.RelacaoAcaoComercialXPedidoBonificado

        Return DirectCast(daoBase, RelacaoAcaoComercialXPedidoBonificadoDAO).ListarPedidosDeCompra(codFnc, numPedCmp).FirstOrDefault()

    End Function

    Public Function ListarPedidosBonificadosDaAcao(codCmc As Decimal) As List(Of VO.RelacaoAcaoComercialXPedidoBonificado)

        Return DirectCast(daoBase, RelacaoAcaoComercialXPedidoBonificadoDAO).ListarPedidosBonificadosDaAcao(codCmc)

    End Function

    ''' <summary>
    ''' Remove associacao de pedido bonificacao da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    Public Sub DeletarRelacaoPedidoBonificacaoDaAcao(codAcao As Integer)

        Dim filtro = New VO.RelacaoAcaoComercialXPedidoBonificado With {.CODACOCMC = codAcao}
        DeleteT(filtro, False)

    End Sub

    ''' <summary>
    ''' Inserir pedido bonificado na acao
    ''' </summary>
    ''' <param name="pedidoBonificado"></param>
    Public Function InserirRelacaoPedidoBonificacao(pedidoBonificado As VO.RelacaoAcaoComercialXPedidoBonificado)

        Return InsertT(pedidoBonificado, False)

    End Function

    ''' <summary>
    ''' Inserir pedidos bonificados na acao
    ''' </summary>
    ''' <param name="pedidosBonificados"></param>
    Public Sub InserirRelacaoPedidoBonificacaoNaAcao(idAcao As Decimal, pedidosBonificados As List(Of VO.RelacaoAcaoComercialXPedidoBonificado))

        For Each pedido In pedidosBonificados
            pedido.CODACOCMC = idAcao
            InsertT(pedido, False)
        Next

    End Sub

    ''' <summary>
    ''' InsDdoFrnPedidoBonificacao
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    Public Sub InserirDadosFornecedorPedidoBonificacao(acaoComercial As VO.AcaoComercial)

        Try

            acaoComercial.Fornecedores = New List(Of VO.RelacaoAcaoComercialxFornecedores)

            For Each pedido As VO.RelacaoAcaoComercialXPedidoBonificado In acaoComercial.Bonificacoes

                Dim valida = Not (acaoComercial.Fornecedores.Where(Function(f) f.CODFRN = pedido.CODFRN).Count > 0)

                If valida Then

                    Dim novoPedido As New VO.RelacaoAcaoComercialxFornecedores
                    novoPedido.CODACOCMC = acaoComercial.CODACOCMC
                    novoPedido.CODFRN = pedido.CODFRN
                    novoPedido.VLRPTCFRNACOCMC = acaoComercial.Bonificacoes.Sum(Function(f) f.VLRUTZACOCMC)
                    novoPedido.TIPDSNDSCBNF = acaoComercial.TIPDSNDSCBNF

                    novoPedido.TIPDSNDSCBNFEXACTT = 0
                    novoPedido.VLRTXAADMFRNACOCMC = 0
                    novoPedido.VLRSLDCNTCRRFRN = 0
                    novoPedido.VLRSLDDISACOFUTFRN = 0
                    novoPedido.VLRSLDDSNDSCACOCMC = 0
                    novoPedido.VLRTXAADMBDEACOCMC = 0
                    novoPedido.VLRUTZCTTACOCMC = 0
                    novoPedido.VLRUTZEXACTTACOCMC = 0
                    novoPedido.CODUNDESRNGC = 0
                    novoPedido.VLRSLDDSNCTTACOCMC = 0
                    novoPedido.DESBFCFRNACOCMC = " "
                    novoPedido.TIPDSNDSCBNFACR = 0
                    novoPedido.VLRSLDDSNACR = 0
                    novoPedido.VLRUTZRCTCSTMER = 0
                    novoPedido.DESSELFRN = " "

                    acaoComercial.Fornecedores.Add(novoPedido)
                    boFornecedores.InserirDadosFornecedoresBD(novoPedido)

                End If

            Next
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Sub EnviarAcoCmcBonificacao(acaoComercial As VO.AcaoComercial, ByVal icentivosHabilitado As Boolean, ByRef bBotaoPolitica As Boolean, ByVal benviar As Boolean, ByRef bDesabilitabotao As Integer, ByVal indPreAcordo As Boolean)
        Dim IndPol As Integer = 0
        Dim VlrTot As Decimal = 0
        Dim VlrTotOcd As Decimal = 0
        Dim codigoEvento As String = acaoComercial.CODEVTACOCMC
        Dim lstPmtPoliticaDivergente(0) As Object
        Dim itemfuncionario As VO.Funcionario
        Dim itemfuncionario2 As VO.Funcionario
        Dim boFuncionarios = New FuncionarioBO
        Dim boAcaoComercial As New AcaoComercialBO

        'atualiza o form de saldo do plano de marketing, calcula os saldos do plano de marketing (caso tenha algum fornecedor na grid)
        If acaoComercial.Bonificacoes.Count > 0 Then
            boAcaoComercial.AtualizaDadosPlanoDeMarketing(acaoComercial)
        End If

        For j As Integer = 0 To acaoComercial.Bonificacoes.Count - 1L
            If IsNothing(acaoComercial.Bonificacoes(j).VERBA_DISPONIVEL) Then
                Throw New MappedWarnException("É necessário informar o valor dos pedidos!")
                Exit For
            End If
        Next

        If boFornecedores.VerificaDadosAcao(acaoComercial, icentivosHabilitado, indPreAcordo) Then
            'Verifica se o valor orçado é igual ao valor gasto
            VlrTot = acaoComercial.ValorParticipacao 'valor de participação
            VlrTotOcd = acaoComercial.ValorTotal 'valor total orcamento


            'If bBotaoPolitica = True And acaoComercial.DESCSTA = "NOVA" Then
            '    'DelDdoAcoCmcPol(CInt(acaoComercial.CODACOCMC))
            '    boPoliticas.DeletarDadoAcaoComercialPoliticas(acaoComercial.CODACOCMC)
            '    'btPolAcoCmc.Visible = False
            'End If 'Botao politica

            'insere dados dos itens de orçamento
            If acaoComercial.ItensOrcamento.Count > 0 Then
                Dim gerente As New VO.Funcionario
                Dim codigo As Decimal
                If IsNothing(acaoComercial.confirmaEnviarGerente) Then
                    gerente = boFuncionarios.buscaGerenteDefault(acaoComercial.CODACOCMC, ControleAcesso.CodFnc())
                    If (gerente Is Nothing) Then
                        Throw New MappedWarnException("Não existe relacionamento entre o Funcionário x Gerente de Marketing! ")
                    Else
                        codigo = gerente.CODFNC
                    End If
                Else
                    codigo = acaoComercial.confirmaEnviarGerente
                End If
                itemfuncionario = boFuncionarios.selecionaGerente(codigo, "").FirstOrDefault()
                Dim funcfiltro As New VO.Funcionario
                funcfiltro.CODFNC = ControleAcesso.CodFnc()

                'itemfuncionario2 = boFuncionarios.SelectT(funcfiltro)

                'Busca o funcionário ou terceiro logado.
                itemfuncionario2 = boFuncionarios.BuscaFuncionariosETerceiros(funcfiltro)

                'Gera fluxo de aprovacao de acoes mercadologicas
                If Not boFornecedores.AtuFluApvAcoCmc(acaoComercial.CODACOCMC, acaoComercial.CODFNCCRIACOCMC,
                                                            itemfuncionario2.NOMFNC,
                                                            RelacaoAcaoComercialxFornecedoresBO.Programa.ACOES_MERCADOLOGICAS,
                                                            RelacaoAcaoComercialxFornecedoresBO.NumSeqAprovador.GER_CATEGORIA,
                                                            RelacaoAcaoComercialxFornecedoresBO.TipoUsuario.GER_CATEGORIA,
                                                            RelacaoAcaoComercialxFornecedoresBO.TipoUsuario.DIR_COMPRAS,
                                                            RelacaoAcaoComercialxFornecedoresBO.tipoFluxo.VALOR,
                                                            itemfuncionario.CODFNC,
                                                            itemfuncionario.NOMFNC,
                                                            indPreAcordo) Then
                    '
                    Throw New MappedWarnException("Erro: Os dados da ação foram gravados com sucesso, porém ocorreram erros na gravação do fluxo de aprovação. Por favor, tente enviar novamente esta ação, se essa mensagem persistir contacte o responsável.")
                Else
                    'Desabilita campos
                    bDesabilitabotao = 2
                End If
            End If
        End If
    End Sub

End Class

