﻿Imports Core

Public Class AcompanhamentoPreAcordoBO
    Inherits BOBase(Of VO.AcompanhamentoPreAcordo)

    Public Sub New()
        MyBase.daoBase = New AcompanhamentoPreAcordoDAO()
    End Sub

    Public Function ListaAcompanhamentoPreAcordo(dadosFiltro As VO.AcompanhamentoPreAcordo) As List(Of VO.AcompanhamentoPreAcordo)
        Return DirectCast(daoBase, AcompanhamentoPreAcordoDAO).ListaAcompanhamentoPreAcordo(dadosFiltro)
    End Function
    Public Function ListaAcompanhamentoPreAcordoCliente(dadosFiltro As VO.AcompanhamentoPreAcordo) As List(Of VO.AcompanhamentoPreAcordo)
        Return DirectCast(daoBase, AcompanhamentoPreAcordoDAO).ListaAcompanhamentoPreAcordoCliente(dadosFiltro)
    End Function

End Class
