﻿Imports Core
Public Class RelacaoAcaoComercialPorItemOrcamentoBO
    Inherits BOBase(Of VO.RelacaoAcaoComercialPorItemOrcamento)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoComercialPorItemOrcamentoDAO()
    End Sub

    Public Function ObterValorTotalDeMercadoriaPreAcordo(idAcao As Decimal?, codMer As Decimal?) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).ObterValorTotalDeMercadoriaPreAcordo(idAcao, codMer)

    End Function

    'Public Function SelectRelacaoAcaoComercialEItensDeOrcamento(idAcao As Decimal?, codMer As Decimal?) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

    '    Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).SelectRelacaoAcaoComercialEItensDeOrcamento(idAcao, codMer)

    'End Function
    'Public Function SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(idAcao As Decimal?, codMer As Decimal?) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

    '    Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(idAcao, codMer)

    'End Function

    Public Function BuscarItensOrcamentoDaAcao(idAcao As Decimal?) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).BuscarItensOrcamentoDaAcao(idAcao)

    End Function

    Public Function BuscarItensOrcamentoSubstituidosDaAcao(idAcao As Decimal?, markup As Decimal) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).BuscarItensOrcamentoSubstituidosDaAcao(idAcao, markup)

    End Function

    Public Function BuscarItensOrcamentoSubstituidosDaAcaoETR(idAcao As Decimal?) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).BuscarItensOrcamentoSubstituidosDaAcaoETR(idAcao)

    End Function

    ''' <summary>
    ''' Valida necessidade de canal de venda da acao
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function ValidaNecessidadeCanalVenda(idAcao As Decimal?) As Boolean
        Dim valor = DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).ValidaNecessidadeCanalVenda(idAcao)
        If valor > 0 Then
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' função temporaria para buscar centro de custo
    ''' </summary>
    ''' <returns></returns>
    Public Function BuscaCentrodeCusto(NomUsuario As String) As Integer
        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).BuscaCentrodeCusto(NomUsuario)
    End Function

    ''' <summary>
    ''' função de inserir diferente da padrao
    ''' </summary>
    ''' <param name="item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InserirRelacao(item As VO.RelacaoAcaoComercialPorItemOrcamento) As Integer
        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).InserirRelacao(item)
    End Function

    Public Function AtuDatUtzIte(mercadoria As VO.RelacaoAcaoComercialPorItemOrcamento)
        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).AtuDatUtzIte(mercadoria)
    End Function

    Public Function AtuDdoFrnIte(mercadoria As VO.RelacaoAcaoComercialPorItemOrcamento)
        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).AtuDdoFrnIte(mercadoria)
    End Function

    Public Function DelDdoAcoCmcIte(CodAco As Integer) As Integer
        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).DelDdoAcoCmcIte(CodAco)
    End Function

    Public Function InsDdoAcoCmcIteOcd(StrDdo As String, ItemOcd As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)) As Integer
        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).InsDdoAcoCmcIteOcd(StrDdo, ItemOcd)
    End Function

    Public Function IsrIteSubAco(CodAcoCmc As Integer,
                                 NumLnhIteOcdAcoCmc As Integer,
                                 CodMer As Integer,
                                 CodMerSbt As Integer,
                                 QdeIteOcdCmc As Integer,
                                 VlrUntIteOcdAcoCmc As Double) As Integer

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).IsrIteSubAco(CodAcoCmc, NumLnhIteOcdAcoCmc, CodMer, CodMerSbt, QdeIteOcdCmc, VlrUntIteOcdAcoCmc)

    End Function

    Public Function DelIteSubAco(CodAcoCmc As Integer,
                                 NumLnhIteOcdAcoCmc As Integer,
                                 CodMer As Integer) As Integer

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).DelIteSubAco(CodAcoCmc, NumLnhIteOcdAcoCmc, CodMer)
    End Function

    Public Function VldGrpSgmCtb(DesGrpSgmCtb As String)

        Dim CodUndNgc = DesGrpSgmCtb.Substring(0, DesGrpSgmCtb.IndexOf("."))


        Dim rs As Decimal = DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).RcpCodLivCtb(CodUndNgc)

        If Not IsNothing(rs) Then
            Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).VldGrpSgmCtb(rs, DesGrpSgmCtb)
        Else
            Return Nothing
        End If

    End Function

    Public Sub DeletarItem(item As VO.RelacaoAcaoComercialPorItemOrcamento, substituto As VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)
        Dim boRelacaoItemPrincipalItemSubstitutoAcaoComercial As New RelacaoItemPrincipalItemSubstitutoAcaoComercialBO

        Me.DeleteT(item)
        boRelacaoItemPrincipalItemSubstitutoAcaoComercial.DeleteT(substituto)
    End Sub

    Public Function cnsVlrMaxIdtRsu(CodAcoCmc As Integer) As Decimal
        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).cnsVlrMaxIdtRsu(CodAcoCmc)

    End Function

    Public Function AtlIdtRsu(ByVal item As VO.RelacaoAcaoComercialPorItemOrcamento) As Integer

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).AtlIdtRsu(item)
    End Function
    Public Function AltIndIteReqRsuVlrMnm(ByVal item As VO.RelacaoAcaoComercialPorItemOrcamento) As Integer

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).AltIndIteReqRsuVlrMnm(item)
    End Function

    Public Function csnReqRsuIteAcoMcd(ByVal iCodAcoCmc As Integer) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)
        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).csnReqRsuIteAcoMcd(iCodAcoCmc)
    End Function

    Public Function FunInsDdoOpeDsn(ByVal pCodAcoCmc As Integer) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)
        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).FunInsDdoOpeDsn(pCodAcoCmc)
    End Function


    ''' <summary>
    ''' Atualixa o Saldo do item Orçamento de acordo com o gasto
    ''' </summary>
    ''' <param name="VlrEstornar">Valor Unitário dos itens</param>
    ''' <param name="QtdEstornada">Quantidade de itens</param>
    ''' <param name="CODACOCMC">Codigo da ação</param>
    ''' <param name="NUMLNHITEOCDACOCMC">Codigo do item</param>
    ''' <returns>Quantidade de registros alterados</returns>
    Public Function AtualizaSaldoItemOrcamentoEstorno(VlrEstornar As Decimal, QtdEstornada As Integer, CODACOCMC As Integer, NUMLNHITEOCDACOCMC As Integer) As Integer
        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).AtualizaSaldoItemOrcamentoEstorno(VlrEstornar, QtdEstornada, CODACOCMC, NUMLNHITEOCDACOCMC)
    End Function


    ''' <summary>
    ''' Atualixa o Saldo do item Orçamento de acordo com o gasto
    ''' </summary>
    ''' <param name="VlrUtilizado">Valor Unitário dos itens</param>
    ''' <param name="QtdUtilizada">Quantidade de itens</param>
    ''' <param name="CODACOCMC">Codigo da ação</param>
    ''' <param name="NUMLNHITEOCDACOCMC">Codigo do item</param>
    ''' <returns>Quantidade de registros alterados</returns>
    Public Function AtualizaSaldoItemOrcamentoGasto(VlrUtilizado As Decimal, QtdUtilizada As Integer, CODACOCMC As Integer, NUMLNHITEOCDACOCMC As Integer) As Integer
        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).AtualizaSaldoItemOrcamentoGasto(VlrUtilizado, QtdUtilizada, CODACOCMC, NUMLNHITEOCDACOCMC)
    End Function


#Region "Estorno Manual"
    Public Function ConsultaValoresDisponiveis(idAcao As Decimal?, codOperacoes As String) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).ConsultaValoresDisponiveis(idAcao, codOperacoes)

    End Function

    Public Function AtualizaEstornoItensAcao(idAcao As Decimal?, codOperacoes As String) As Integer

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).AtualizaEstornoItensAcao(idAcao, codOperacoes)

    End Function

    Public Function AtualizaEstornoVlrAcao(idAcao As Decimal?, codOperacoes As String) As Integer

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).AtualizaEstornoVlrAcao(idAcao, codOperacoes)

    End Function

    Public Function AtualizaEstornoCarimbo(idCarimbo As Decimal?, vlrEstorno As Decimal?) As Integer

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).AtualizaEstornoCarimbo(idCarimbo, vlrEstorno)

    End Function


    Public Function ConsultaValoresDisponiveisDaAcao(idAcao As Decimal?) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Return DirectCast(daoBase, RelacaoAcaoComercialPorItemOrcamentoDAO).ConsultaValoresDisponiveisDaAcao(idAcao)

    End Function
#End Region

End Class
