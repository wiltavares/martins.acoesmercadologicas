﻿Imports Core
Public Class ParametroSistemaAdminitracaoVendaBO
    Inherits BOBase(Of VO.ParametroSistemaAdminitracaoVenda)

    Public Sub New()
        MyBase.daoBase = New ParametroSistemaAdminitracaoVendaDAO()
    End Sub

    Public Function ListarParametrosAcaoComercial() As List(Of VO.ParametroSistemaAdminitracaoVenda)

        Return DirectCast(daoBase, ParametroSistemaAdminitracaoVendaDAO).ListarParametrosAcaoComercial()

    End Function

    Public Function ListarParametrosAcaoComercialEmp() As VO.ParametroSistemaAdminitracaoVenda

        Return DirectCast(daoBase, ParametroSistemaAdminitracaoVendaDAO).ListarParametrosAcaoComercialEmp()

    End Function
    Public Function csnCodUndOpe() As VO.ParametroSistemaAdminitracaoVenda
        Return DirectCast(daoBase, ParametroSistemaAdminitracaoVendaDAO).csnCodUndOpe()
    End Function

End Class
