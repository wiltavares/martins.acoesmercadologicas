﻿Imports Core
Public Class RelacaoAcaoComercialxPremiacaoExclusivaBO
    Inherits BOBase(Of VO.RelacaoAcaoComercialxPremiacaoExclusiva)

    Private boAcaoComercial As AcaoComercialBO
    Private boRelacaoPlanoMarketing As RelacaoAcaoComercialxPlanoMarketingBO
    Private boMecanicaAcao As RelacaoAcaoComercialxMecanicaDaAcaoBO
    Private boIncentivo As RelacaoIncentivoxAcaoComercialBO
    Private boPoliticas As RelacaoAcaoComercialxPoliticaBO
    'Private boRelacaoComerForne As RelacaoAcaoComercialxFornecedoresBO
    Private boRalacaoFornCarimbo As RelacaoAcaoxFornecedorxCarimboBO
    Private boFornecedores As RelacaoAcaoComercialxFornecedoresBO

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoComercialxPremiacaoExclusivaDAO()
    End Sub


    ''' <summary>
    ''' Remove associacao de premiacoes da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    Public Sub DeletarRelacaoPremiacaoExclusivaDaAcao(codAcao As Integer)

        Dim filtro = New VO.RelacaoAcaoComercialxPremiacaoExclusiva With {.CODACOCMC = codAcao}
        DeleteT(filtro, False)

    End Sub

    ''' <summary>
    ''' Inserir premiacão na acao
    ''' </summary>
    ''' <param name="premiacao"></param>
    Public Function InserirRelacaoPremiacaoExclusiva(premiacao As VO.RelacaoAcaoComercialxPremiacaoExclusiva)

        Return InsertT(premiacao, False)

    End Function

    ''' <summary>
    ''' Inserir premiacoes exclusivas na acao
    ''' </summary>
    ''' <param name="premiacoesExclusivas"></param>
    Public Sub InserirRelacaoPremiacaoExclusivaNaAcao(idAcao As Decimal, premiacoesExclusivas As List(Of VO.RelacaoAcaoComercialxPremiacaoExclusiva))

        For Each premiacao In premiacoesExclusivas
            premiacao.CODACOCMC = idAcao
            InsertT(premiacao, False)
        Next

    End Sub

    ''' <summary>
    ''' InsDdoFrnPremiacao
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    Public Sub InserirDadosFornecedorPremiacaoExclusiva(acaoComercial As VO.AcaoComercial)

        Try

            acaoComercial.Fornecedores = New List(Of VO.RelacaoAcaoComercialxFornecedores)

            For Each premiacao As VO.RelacaoAcaoComercialxPremiacaoExclusiva In acaoComercial.Premiacao

                Dim valida = Not (acaoComercial.Fornecedores.Where(Function(f) f.CODFRN = premiacao.CODFRN).Count > 0)

                If valida Then

                    Dim novaPremiacao As New VO.RelacaoAcaoComercialxFornecedores
                    novaPremiacao.CODACOCMC = acaoComercial.CODACOCMC
                    novaPremiacao.CODFRN = premiacao.CODFRN
                    novaPremiacao.VLRPTCFRNACOCMC = acaoComercial.Premiacao.Sum(Function(f) f.VLRUTZACOCMC)
                    novaPremiacao.TIPDSNDSCBNF = acaoComercial.TIPDSNDSCBNF
                    novaPremiacao.TIPDSNDSCBNFEXACTT = 0
                    novaPremiacao.VLRTXAADMFRNACOCMC = 0
                    novaPremiacao.VLRSLDCNTCRRFRN = 0
                    novaPremiacao.VLRSLDDISACOFUTFRN = 0
                    novaPremiacao.VLRSLDDSNDSCACOCMC = 0
                    novaPremiacao.VLRTXAADMBDEACOCMC = 0
                    novaPremiacao.VLRUTZCTTACOCMC = 0
                    novaPremiacao.VLRUTZEXACTTACOCMC = 0
                    novaPremiacao.CODUNDESRNGC = 0
                    novaPremiacao.VLRSLDDSNCTTACOCMC = 0
                    novaPremiacao.DESBFCFRNACOCMC = " "
                    novaPremiacao.TIPDSNDSCBNFACR = 0
                    novaPremiacao.VLRSLDDSNACR = 0
                    novaPremiacao.VLRUTZRCTCSTMER = 0
                    novaPremiacao.DESSELFRN = " "

                    acaoComercial.Fornecedores.Add(novaPremiacao)
                    If (boFornecedores Is Nothing) Then
                        boFornecedores = New RelacaoAcaoComercialxFornecedoresBO()
                    End If
                    boFornecedores.InserirDadosFornecedoresBD(novaPremiacao)

                End If

            Next
        Catch ex As Exception
            Throw ex
        End Try


    End Sub

    ''' <summary>
    ''' Listar premiações da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    Public Function ListarPremiacaoExclusivaDaAcao(codAcao As Decimal)

        Return DirectCast(daoBase, RelacaoAcaoComercialxPremiacaoExclusivaDAO).ListarPremiacaoExclusivaDaAcao(codAcao)

    End Function

    Public Sub EnviarAcoCmcPremiacao(acaoComercial As VO.AcaoComercial, ByVal icentivosHabilitado As Boolean, ByRef bBotaoPolitica As Boolean, ByVal benviar As Boolean, ByRef bDesabilitabotao As Integer, ByVal indPreAcordo As Boolean)
        Dim IndPol As Integer = 0
        Dim VlrTot As Decimal = 0
        Dim VlrTotOcd As Decimal = 0
        Dim codigoEvento As String = acaoComercial.CODEVTACOCMC
        Dim lstPmtPoliticaDivergente(0) As Object
        Dim itemfuncionario As VO.Funcionario
        Dim itemfuncionario2 As VO.Funcionario
        Dim boFuncionarios = New FuncionarioBO

        boAcaoComercial = New AcaoComercialBO


        'atualiza o form de saldo do plano de marketing, calcula os saldos do plano de marketing (caso tenha algum fornecedor na grid)
        If acaoComercial.Premiacao.Count > 0 Then
            boAcaoComercial.AtualizaDadosPlanoDeMarketing(acaoComercial)
        End If

        For j As Integer = 0 To acaoComercial.Premiacao.Count - 1L
            If IsNothing(acaoComercial.Premiacao(j).VLRUTZACOCMC) Then
                Throw New MappedWarnException("É necessário informar o valor da premiação!")
                Exit For
            End If
        Next

        boFornecedores = New RelacaoAcaoComercialxFornecedoresBO
        If boFornecedores.VerificaDadosAcao(acaoComercial, icentivosHabilitado, indPreAcordo) Then

            'Verifica se o valor orçado é igual ao valor gasto
            VlrTot = acaoComercial.ValorParticipacao 'valor de participação
            VlrTotOcd = acaoComercial.ValorTotal 'valor total orcamento


            'If bBotaoPolitica = True And acaoComercial.DESCSTA = "NOVA" Then
            '    'DelDdoAcoCmcPol(CInt(acaoComercial.CODACOCMC))
            '    boPoliticas.DeletarDadoAcaoComercialPoliticas(acaoComercial.CODACOCMC)
            '    'btPolAcoCmc.Visible = False
            'End If 'Botao politica

            boPoliticas = New RelacaoAcaoComercialxPoliticaBO

            'boPoliticas.DeletarDadoAcaoComercialPoliticas(acaoComercial.CODACOCMC) 'Deleta as políticas da ação

            'boAcaoComercial.InserirAcaoComercial(acaoComercial, 1) 'InsDdoAcoCmc(1) 'Insere dados Da Ação Comercial na tabela mrt.t0118430 com fluxo normal

            'InserirDadosFornecedorPremiacaoExclusiva(acaoComercial)
            'boRalacaoFornCarimbo = New RelacaoAcaoxFornecedorxCarimboBO

            'boRalacaoFornCarimbo.AtuCadMcoVbaFrn(acaoComercial)

            'insere dados dos itens de orçamento
            If acaoComercial.ItensOrcamento.Count > 0 Then
                Dim gerente As New VO.Funcionario
                Dim codigo As Decimal
                If IsNothing(acaoComercial.confirmaEnviarGerente) Then
                    gerente = boFuncionarios.buscaGerenteDefault(acaoComercial.CODACOCMC, ControleAcesso.CodFnc())
                    If (gerente Is Nothing) Then
                        Throw New MappedWarnException("Não existe relacionamento entre o Funcionário x Gerente de Marketing! ")
                    Else
                        codigo = gerente.CODFNC
                    End If
                    codigo = boFuncionarios.buscaGerenteDefault(acaoComercial.CODACOCMC, ControleAcesso.CodFnc()).CODFNC
                Else
                    codigo = acaoComercial.confirmaEnviarGerente
                End If
                itemfuncionario = boFuncionarios.selecionaGerente(codigo, "").FirstOrDefault()
                Dim funcfiltro As New VO.Funcionario
                funcfiltro.CODFNC = ControleAcesso.CodFnc()

                ' itemfuncionario2 = boFuncionarios.SelectT(funcfiltro)
                'Busca o funcionário ou terceiro
                itemfuncionario2 = boFuncionarios.BuscaFuncionariosETerceiros(funcfiltro)

                'Gera fluxo de aprovacao de acoes mercadologicas
                If Not boFornecedores.AtuFluApvAcoCmc(acaoComercial.CODACOCMC, acaoComercial.CODFNCCRIACOCMC,
                                                            itemfuncionario2.NOMFNC,
                                                            RelacaoAcaoComercialxFornecedoresBO.Programa.ACOES_MERCADOLOGICAS,
                                                            RelacaoAcaoComercialxFornecedoresBO.NumSeqAprovador.GER_CATEGORIA,
                                                            RelacaoAcaoComercialxFornecedoresBO.TipoUsuario.GER_CATEGORIA,
                                                            RelacaoAcaoComercialxFornecedoresBO.TipoUsuario.DIR_COMPRAS,
                                                            RelacaoAcaoComercialxFornecedoresBO.tipoFluxo.VALOR,
                                                            itemfuncionario.CODFNC,
                                                            itemfuncionario.NOMFNC,
                                                            indPreAcordo) Then
                    '
                    Throw New MappedWarnException("Erro: Os dados da ação foram gravados com sucesso, porém ocorreram erros na gravação do fluxo de aprovação. Por favor, tente enviar novamente esta ação, se essa mensagem persistir contacte o responsável.")
                Else
                    'Desabilita campos
                    bDesabilitabotao = 2
                End If

            End If
            'InserirRelacaoPremiacaoExclusivaNaAcao(acaoComercial.CODACOCMC, acaoComercial.Premiacao)
        End If
    End Sub

End Class
