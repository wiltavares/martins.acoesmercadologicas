﻿Imports Core
Public Class RelacaoAcaoComercialxPlanoMarketingBO
    Inherits BOBase(Of VO.RelacaoAcaoComercialxPlanoMarketing)


    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoComercialxPlanoMarketingDAO()
    End Sub
    Public Function ListSaldoPlanoDeMarketingAcao(vo As VO.RelacaoAcaoComercialxPlanoMarketing, acao As VO.AcaoComercial) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
        If (acao.CODACOCMC Is Nothing) Then
            Return New List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
        End If
        Dim RelacaoAcaoComercialxPlanoMarketingList As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
        If (acao.CODSTAAPVACOCMC = 1 Or acao.CODSTAAPVACOCMC = 4) Then
            Dim strCodUndNgc As String = String.Join(",", acao.Fornecedores.Select(Function(x) x.CODUNDESRNGC.ToString()))
            If (String.IsNullOrEmpty(strCodUndNgc)) Then
                Return New List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
            End If
            vo.ANOMESREF = String.Concat(Date.Now.Year, Date.Now.Month.ToString().PadLeft(2, "0"))
            RelacaoAcaoComercialxPlanoMarketingList = DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).ListSaldoPlanoDeMarketingTipoEvento(vo, strCodUndNgc)
        Else
            RelacaoAcaoComercialxPlanoMarketingList = DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).ListSaldoPlanoDeMarketingAcao(vo)
        End If
        For Each item In From f In acao.Fornecedores Group By f.CODUNDESRNGC Into VlrAcao = Sum(f.VLRPTCFRNACOCMC + f.VLRTXAADMBDEACOCMC + f.VLRTXAADMFRNACOCMC) Select CODUNDESRNGC, VlrAcao
            For Each bu As VO.RelacaoAcaoComercialxPlanoMarketing In RelacaoAcaoComercialxPlanoMarketingList
                If (bu.CODUNDESRNGC = item.CODUNDESRNGC) Then
                    bu.VLRACAO = item.VlrAcao
                    bu.VLRESULTADOMENSAL = (bu.VLRSLDMNSPLNMKT.GetValueOrDefault() - item.VlrAcao)
                    bu.VLRESULTADOATUAL = (bu.VLRSLDATUPLNMKT.GetValueOrDefault() - item.VlrAcao)
                    bu.VLRESULTADOANUAL = (bu.VLRSLDANOPLNMKT.GetValueOrDefault() - item.VlrAcao)
                End If
            Next
        Next
        'Esconder colunas
        Return RelacaoAcaoComercialxPlanoMarketingList
    End Function

    Public Function BuscarPlanosMarketingDaAcao(codAcao As Integer) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        Return DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).BuscarPlanosMarketingDaAcao(codAcao)

    End Function

    Public Function BscDdoPlnMkt(TipEvt As Integer, CodEvt As Integer, datRef As Integer,
                                 ByVal CodUndNgc As Integer) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        Return DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).BscDdoPlnMkt(TipEvt, CodEvt, datRef, CodUndNgc)

    End Function


    ''' <summary>
    ''' BscDdoFrnPlnMktAco
    ''' Buscar os dados de plano de marketing relacionados a acao a partir de uma lista de fornecedores da acao
    ''' </summary>
    ''' <param name="fornecedoresDaAcao"></param>
    ''' <returns></returns>
    Public Function BuscarDadosFornecedorPlanoMarketing(fornecedoresDaAcao As List(Of VO.RelacaoAcaoComercialxFornecedores)) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        Dim listaPlanos As New List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        For Each item As VO.RelacaoAcaoComercialxFornecedores In fornecedoresDaAcao
            Dim obj = listaPlanos.Where(Function(f) f.CODUNDESRNGC = item.CODUNDESRNGC).FirstOrDefault()
            If (obj Is Nothing) Then
                obj = New VO.RelacaoAcaoComercialxPlanoMarketing() With {
                    .CODUNDESRNGC = item.CODUNDESRNGC,
                    .VLRRSVPLNMKT = item.VLRPTCFRNACOCMC.GetValueOrDefault() + item.VLRTXAADMFRNACOCMC.GetValueOrDefault() + item.VLRTXAADMBDEACOCMC.GetValueOrDefault()
                }

                listaPlanos.Add(obj)
            Else
                obj.VLRRSVPLNMKT = obj.VLRRSVPLNMKT + (item.VLRPTCFRNACOCMC.GetValueOrDefault() + item.VLRTXAADMFRNACOCMC.GetValueOrDefault() + item.VLRTXAADMBDEACOCMC.GetValueOrDefault())
            End If
        Next

        Return listaPlanos
    End Function

    ''' <summary>
    ''' BscDdoFrnPlnMktAco
    ''' Buscar os dados de plano de marketing relacionados aos fornecedores da acao
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function BuscarDadosFornecedorPlanoMarketing(idAcao As Decimal) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
        Return (DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).BuscarDadosFornecedorPlanoMarketing(idAcao))
    End Function

    ''' <summary>
    ''' InsDdoPlnMkt
    ''' Inserir dados do plano de marketing na acao
    ''' </summary>
    ''' <param name="IndEnvGrv">IndEnvGrv = 0 = para gravar, IndEnvGrv = 1 = para enviar</param>
    ''' <param name="acaoComercial"></param>
    Public Sub InserirDadosPlanoMarketingDaAcao(IndEnvGrv As Integer, acaoComercial As VO.AcaoComercial)
        Dim boPlanoMarketing As New PlanoDeMarketingBO

        'Data de referência
        Dim anoRef As String = Now.ToString("yyyyMM")

        If acaoComercial.CODEVTACOCMC <> 0 Then

            'Busca somatória dos fornecedores por BU
            Dim listaPlanos = BuscarDadosFornecedorPlanoMarketing(acaoComercial.Fornecedores)
            Dim Fornecedor = acaoComercial.Fornecedores.FirstOrDefault()

            For Each plano As VO.RelacaoAcaoComercialxPlanoMarketing In listaPlanos

                Dim novoPlano As VO.RelacaoAcaoComercialxPlanoMarketing = New VO.RelacaoAcaoComercialxPlanoMarketing
                novoPlano.CODACOCMC = acaoComercial.CODACOCMC 'codAcao
                novoPlano.CODEVTACOCMC = acaoComercial.CODEVTACOCMC ' tipoEvento
                novoPlano.TIPEVTACOCMC = acaoComercial.TIPEVTACOCMC ' codEvento
                novoPlano.ANOMESREF = anoRef
                novoPlano.VLRUTZPLNMKT = 0 '(VALOR UTILIZADO)
                novoPlano.VLRRSVPLNMKT = plano.VLRRSVPLNMKT '(VALOR RESERVADO)
                novoPlano.CODUNDESRNGC = If(Fornecedor.TIPFRNACOCMC = 1, 0, plano.CODUNDESRNGC) ' UndNgc (cod bu)

                novoPlano.VLRSLDMNSPLNMKT = 0 'Saldo mensal
                novoPlano.VLRSLDATUPLNMKT = 0 'Saldo atual
                novoPlano.VLRSLDANOPLNMKT = 0 'Saldo anual
                ' Se for fornecedores busca o saldo do plano de marketing
                ' busca BU's existentes para o plano de marketing
                If acaoComercial.Carimbos.Count = 0 AndAlso acaoComercial.Bonificacoes.Count = 0 AndAlso acaoComercial.Premiacao.Count = 0 Then

                    Dim planoSaldo = DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).ListSaldoPlanoDeMarketingTipoEvento(New VO.RelacaoAcaoComercialxPlanoMarketing() With {
                                                                                                                                                .ANOMESREF = novoPlano.ANOMESREF,
                                                                                                                                                .TIPEVTACOCMC = novoPlano.TIPEVTACOCMC,
                                                                                                                                                .CODEVTACOCMC = novoPlano.CODEVTACOCMC
                                                                                                                                                }, novoPlano.CODUNDESRNGC).FirstOrDefault()
                    If planoSaldo IsNot Nothing Then
                        novoPlano.VLRSLDMNSPLNMKT = planoSaldo.VLRSLDMNSPLNMKT.GetValueOrDefault() 'Saldo mensal
                        novoPlano.VLRSLDATUPLNMKT = planoSaldo.VLRSLDATUPLNMKT.GetValueOrDefault() 'Saldo atual
                        novoPlano.VLRSLDANOPLNMKT = planoSaldo.VLRSLDANOPLNMKT.GetValueOrDefault() 'Saldo anual
                    End If
                End If


                DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).InserirDadosAcaoPlanoMarketing(novoPlano)

                If IndEnvGrv = 1 Then ' se esta enviando a ação atualiza o plano de marketing do fornecedor
                    If boPlanoMarketing.SelectT(New VO.PlanoDeMarketing() With {
                                                                   .TIPEVTACOCMC = novoPlano.TIPEVTACOCMC,
                                                                   .CODEVTACOCMC = novoPlano.CODEVTACOCMC,
                                                                   .ANOMESREF = novoPlano.ANOMESREF,
                                                                   .CODUNDESRNGC = novoPlano.CODUNDESRNGC
                                                                }) IsNot Nothing Then
                        boPlanoMarketing.ProvisionaValorReservadoPlanoMarketing(TIPEVTACOCMC:=novoPlano.TIPEVTACOCMC _
                                                                              , CODEVTACOCMC:=novoPlano.CODEVTACOCMC _
                                                                              , ANOMESREF:=novoPlano.ANOMESREF _
                                                                              , CODUNDESRNGC:=novoPlano.CODUNDESRNGC _
                                                                              , VLRRSVPLNMKT:=novoPlano.VLRRSVPLNMKT)
                    Else
                        boPlanoMarketing.InserirPlanoDeMarketingDaAcao(novoPlano)
                    End If
                End If
            Next
        End If
    End Sub

    ''' <summary>
    ''' DelDdoAcoCmcPlnMkt
    ''' Deleta os planos de marketing da acao
    ''' </summary>
    ''' <param name="idAcao"></param>
    Public Sub DeletarPlanosMarketingDaAcao(idAcao As Integer)

        DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).DeletarPlanosMarketingDaAcao(idAcao)

    End Sub

    Public Function ListSaldoPlanoDeMarketingAcao(ByVal CODACOCMC As Decimal) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        Dim vo As New VO.RelacaoAcaoComercialxPlanoMarketing
        vo.CODACOCMC = CODACOCMC
        Return DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).ListSaldoPlanoDeMarketingAcao(vo)

    End Function

    Public Function BuscarPlanosMarketingDaAcaofluxo(codAcao As Integer) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        Return DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).BuscarPlanosMarketingDaAcaofluxo(codAcao)

    End Function

    Public Function BuscaPlanoMarketingProvisionadoAcao(ByVal codAcao As Decimal) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
        Return DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).BuscaPlanoMarketingProvisionadoAcao(codAcao)
    End Function

    Public Function ListarPlanoMktAcaoParaProvacao(CODACOCMC As Decimal) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
        Return DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).ListarPlanoMktAcaoParaProvacao(CODACOCMC)
    End Function
    Public Function UpdatePlanoMktAprovacaoAcao(CODACOCMC As Decimal, TIPEVTACOCMC As Decimal, CODEVTACOCMC As Decimal, CODUNDESRNGC As Decimal, ANOMESREF As String) As Integer
        Return DirectCast(daoBase, RelacaoAcaoComercialxPlanoMarketingDAO).UpdatePlanoMktAprovacaoAcao(CODACOCMC, TIPEVTACOCMC, CODEVTACOCMC, CODUNDESRNGC, ANOMESREF)
    End Function
End Class
