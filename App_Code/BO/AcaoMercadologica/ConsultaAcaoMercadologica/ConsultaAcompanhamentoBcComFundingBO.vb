﻿Imports Core
Imports System.Globalization
Imports System.Linq

Public Class ConsultaAcompanhamentoBcComFundingBO
    Inherits BOBase(Of VO.ConsultaAcompanhamentoBcComFunding)

    Public Sub New()
        MyBase.daoBase = New ConsultaAcompanhamentoBcComFundingDAO()
    End Sub

    Public Function Listar(dadosFiltro As VO.ConsultaAcompanhamentoBcComFunding) As List(Of VO.ConsultaAcompanhamentoBcComFunding)
        Return DirectCast(daoBase, ConsultaAcompanhamentoBcComFundingDAO).Listar(dadosFiltro)
    End Function
End Class



