﻿Imports Core
Public Class RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemBO
    Inherits BOBase(Of VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem)

    Public Sub New()
        MyBase.daoBase = New RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemDAO()
    End Sub

    Public Function InsereLogEstornoManual(item As VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem, codFrn As Decimal) As Integer

        Return DirectCast(daoBase, RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemDAO).InsereLogEstornoManual(item, codFrn)

    End Function

    Public Function FunCnsExpCod(codItem As Integer, codOpeAcoCmc As Integer) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Return DirectCast(daoBase, RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemDAO).FunCnsExpCod(codItem, codOpeAcoCmc)

    End Function

    Public Function FunAtuAct(CODACOCMC As Decimal, CODOPEACOCMC As Integer, CODTIPEXPCOD As String, VLRESTORNO As Double) As Integer

        Return DirectCast(daoBase, RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemDAO).FunAtuAct(CODACOCMC, CODOPEACOCMC, CODTIPEXPCOD, VLRESTORNO)

    End Function

    Public Function consultaGastoItem(codAcao As Decimal, linha As Decimal) As List(Of VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem)
        Return DirectCast(daoBase, RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemDAO).consultaGastoItem(codAcao, linha)
    End Function



    Public Function RealizaLancamentoUtilizacaoDoItem(obj As VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem) As VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem
        Dim bo As New RelacaoAcaoComercialPorItemOrcamentoBO()

        Dim item As VO.RelacaoAcaoComercialPorItemOrcamento = bo.BuscarItensOrcamentoDaAcao(obj.CODACOCMC).Where(Function(f) f.NUMLNHITEOCDACOCMC = obj.NUMLNHITEOCDACOCMC).FirstOrDefault()

        If (item Is Nothing) Then
            Throw New MappedWarnException("Erro ao obter o item do orçamento")
        End If

        'Se Gasto (O valor utilizado não pode ser maior que o saldo)
        If (obj.INDTIPISRUTZITEOCD = 0 And ((item.VLRUNTITEOCDACOCMC * item.QDEITEOCDACOCMC) - item.VLRUTZOPEDSNACOCMC) < (obj.VLRUTZITEOCDACOCMC * obj.QDEUTZITEOCDACOCMC)) Then
            Throw New MappedWarnException("Saldo Insuficiente")
        End If
        'Se Extorno (O valor a ser extornado não pode ser maior que o valor gasto
        If (obj.INDTIPISRUTZITEOCD = 1 And (obj.VLRUTZITEOCDACOCMC * obj.QDEUTZITEOCDACOCMC) > item.VLRUTZOPEDSNACOCMC) Then
            Throw New MappedWarnException("O valor a ser estornado é menor que o valor gasto!")
        End If


        Dim ultimo = consultaGastoItem(obj.CODACOCMC, obj.NUMLNHITEOCDACOCMC).OrderBy(Function(f) f.NUMSEQUTZITEOCD).LastOrDefault()
        If (ultimo Is Nothing) Then
            obj.NUMSEQUTZITEOCD = 1
        Else
            obj.NUMSEQUTZITEOCD = ultimo.NUMSEQUTZITEOCD + 1
        End If

        Try
            daoBase.connector.BeginTransaction()

            'Insere o Log
            InsertT(obj)

            'Atualiza o Saldo 
            If (obj.INDTIPISRUTZITEOCD = 0) Then
                bo.AtualizaSaldoItemOrcamentoGasto(obj.VLRUTZITEOCDACOCMC, obj.QDEUTZITEOCDACOCMC, obj.CODACOCMC, obj.NUMLNHITEOCDACOCMC)
            ElseIf (obj.INDTIPISRUTZITEOCD = 1) Then
                bo.AtualizaSaldoItemOrcamentoEstorno(obj.VLRUTZITEOCDACOCMC, obj.QDEUTZITEOCDACOCMC, obj.CODACOCMC, obj.NUMLNHITEOCDACOCMC)
            End If

            daoBase.connector.Commit()
        Catch
            daoBase.connector.Rollback()
            Throw
        End Try

        Return obj

    End Function

End Class
