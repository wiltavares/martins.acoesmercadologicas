﻿Imports AutoMapper
Imports Core
Imports System.Globalization
Imports System.Linq

Public Class AcaoComercialBO
    Inherits BOBase(Of VO.AcaoComercial)

    Private boRelacaoPlanoMarketing As New RelacaoAcaoComercialxPlanoMarketingBO
    Private boPlanoMarketing As New PlanoDeMarketingBO
    Private boFornecedores As New RelacaoAcaoComercialxFornecedoresBO
    Private boItensOrcamento As New RelacaoAcaoComercialPorItemOrcamentoBO
    Private boMecanicaAcao As New RelacaoAcaoComercialxMecanicaDaAcaoBO
    Private boFluxoAprovacao As New RelacaoAcaoComercialxFluxoAprovacaoBO
    Private boIncentivo As New RelacaoIncentivoxAcaoComercialBO
    Private boCarimbos As New RelacaoAcaoxFornecedorxCarimboBO
    Private boRelacaoItemUtilizado As New RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemBO
    Private boBonificoes As New RelacaoAcaoComercialXPedidoBonificadoBO
    Private boPremiacoes As New RelacaoAcaoComercialxPremiacaoExclusivaBO

    Public Sub New()
        MyBase.daoBase = New AcaoComercialDAO()
    End Sub

    ''' <summary>
    ''' bscDdoAcoCmc
    ''' Listar acoes de acordo com o filtro
    ''' </summary>
    ''' <param name="vo"></param>
    ''' <param name="codUsrRede"></param>
    ''' <returns></returns>
    Public Function ListarAcaoComercial(vo As VO.AcaoComercial, codUsrRede As Integer) As List(Of VO.AcaoComercial)

        If vo.CODSTAAPVACOCMC = Constantes.Status.MINHAS_APROVACOES Then

            vo.CODSTAAPVACOCMC = Constantes.Status.EM_APROVACAO
            Return DirectCast(daoBase, AcaoComercialDAO).ListarAcaoMercadologicaMinhasAprovacoes(vo, codUsrRede)
        End If
        Return DirectCast(daoBase, AcaoComercialDAO).ListarAcaoMercadologica(vo)

    End Function

    Public Function SelectValorFunding(acaoComercial As VO.AcaoComercial) As List(Of VO.AcaoComercial)
        Return DirectCast(daoBase, AcaoComercialDAO).SelectValorFunding(acaoComercial)
    End Function

    Public Function BuscaAcaoPorId(CODACOCMC As Decimal?) As VO.AcaoComercial
        Return DirectCast(daoBase, AcaoComercialDAO).BuscarAcaoComercialPorId(CODACOCMC)
    End Function
    ''' <summary>
    ''' BscDdoAcoNum
    ''' Buscar dados da acao comercial pelo id da acao e codigo do fornecedor usuario de rede
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function BuscarAcaoComercialPorId(idAcao As Integer, Optional codFnrUsrRede As Integer? = Nothing) As VO.AcaoComercial

        Dim daoAcao As AcaoComercialDAO = DirectCast(daoBase, AcaoComercialDAO)
        'Busca dados da ação - DdoAco
        Dim acaoComercial = daoAcao.BuscarAcaoComercialPorId(idAcao)
        If Not (acaoComercial Is Nothing) Then

            'Busca dados da regra da ação - DdoRgr
            acaoComercial.Mecanica = boMecanicaAcao.BuscarRegraMecanicaDaAcao(idAcao)

            'Busca informações do plano de marketing - DdoPlnMkt
            acaoComercial.PlanosDeMarketing = boRelacaoPlanoMarketing.BuscarPlanosMarketingDaAcao(idAcao)
            'Atribui informacoes de de evento
            If acaoComercial.PlanosDeMarketing.Count > 0 Then
                acaoComercial.TIPEVTACOCMC = acaoComercial.PlanosDeMarketing(0).TIPEVTACOCMC
                acaoComercial.CODEVTACOCMC = acaoComercial.PlanosDeMarketing(0).CODEVTACOCMC
                acaoComercial.TIPDSNDSCBNF = acaoComercial.PlanosDeMarketing(0).TIPDSNDSCBNF
                acaoComercial.INDVLDRGRICT = acaoComercial.PlanosDeMarketing(0).INDVLDRGRICT
                acaoComercial.EmpenhoExa = acaoComercial.TIPDSNDSCBNF & " - " & acaoComercial.PlanosDeMarketing(0).DESDSNDSCBNF
            End If

            acaoComercial.Carimbos = New List(Of VO.RelacaoAcaoxFornecedorxCarimbo)
            acaoComercial.Bonificacoes = New List(Of VO.RelacaoAcaoComercialXPedidoBonificado)
            acaoComercial.Premiacao = New List(Of VO.RelacaoAcaoComercialxPremiacaoExclusiva)
            acaoComercial.Fornecedores = New List(Of VO.RelacaoAcaoComercialxFornecedores)

            Select Case acaoComercial.TIPACOMCD.GetValueOrDefault()
                Case TipoAcao.Fornecedor, TipoAcao.PreAcordoVigencia, TipoAcao.PreAcordoValor
                    acaoComercial.Fornecedores = boFornecedores.BuscarFornecedoresDaAcao(idAcao)
                Case TipoAcao.Carimbo
                    acaoComercial.Carimbos = boCarimbos.ListarCarimbosDaAcaoComercial(idAcao)
                Case TipoAcao.PedidoBonificacao
                    acaoComercial.Bonificacoes = boBonificoes.ListarPedidosBonificadosDaAcao(idAcao)
                Case TipoAcao.Premiacao
                    acaoComercial.Premiacao = boPremiacoes.ListarPremiacaoExclusivaDaAcao(idAcao)
                Case Else
                    acaoComercial.Fornecedores = boFornecedores.BuscarFornecedoresDaAcao(idAcao)
            End Select

            'Busca os incentivos da acao
            acaoComercial.Incentivos = boIncentivo.BuscarIncentivos(idAcao)
            'Busca itens da ação - DdoIte
            acaoComercial.ItensOrcamento = boItensOrcamento.BuscarItensOrcamentoDaAcao(idAcao)
            'Busca itens substitutos da ação - DdoIteSub
            Dim boParametro As New ParamentroSistemaDeGestaoDeAcaoComercialBO()

            Dim markup As Decimal = boParametro.SelectT(New VO.ParamentroSistemaDeGestaoDeAcaoComercial()).PERMRGADIBDEPMC
            acaoComercial.ItensSubstituidos = boItensOrcamento.BuscarItensOrcamentoSubstituidosDaAcao(idAcao, markup)

            'Busca o Fluxo correspondente ao usuario
            If codFnrUsrRede IsNot Nothing And codFnrUsrRede <> 0 Then
                acaoComercial.FluxosAprovacao = boFluxoAprovacao.BuscaFluxosAprovacaoDaAcaoDoUsuario(idAcao, codFnrUsrRede)
            End If
        End If

        Return acaoComercial

    End Function

    Public Function BscDdoAcoNumEma(ByVal CODACOCMC As Decimal) As VO.AcaoComercial
        Return DirectCast(daoBase, AcaoComercialDAO).BscDdoAcoNumEma(CODACOCMC)
    End Function


    ''' <summary>
    '''  Salva acao comercial e seus grids
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    ''' <param name="_iniciatransacao"></param>
    ''' <param name="codAcaoAntigo"></param>
    ''' <param name="IndItemOrcamento"></param>
    ''' <param name="IndEnvGrv">IndEnvGrv = 0 = para gravar, IndEnvGrv = 1 = para enviar</param>
    ''' <returns></returns>
    Public Function SalvarAcaoComercial(acaoComercial As VO.AcaoComercial, ByVal _iniciatransacao As Boolean, Optional codAcaoAntigo As Decimal = 0, Optional IndItemOrcamento As Boolean = False, Optional IndEnvGrv As Integer = 0) As VO.AcaoComercial
        Try
            Dim daoAcao As AcaoComercialDAO = DirectCast(daoBase, AcaoComercialDAO)
            Dim boRelacaoAcaoComercialPorItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoBO()
            If _iniciatransacao Then
                daoBase.connector.BeginTransaction()
            End If

            Dim IndDspMrt As Integer ' indicador de DESPESAS MARTINS
            Dim IndErrAcoCmc = 0
            Dim tipoPatrocinador = 0
            Dim idAcao = acaoComercial.CODACOCMC

            ' Acao nova
            If acaoComercial.CODACOCMC Is Nothing Then
                'Busca o próximo código para a Ação
                acaoComercial.CODACOCMC = daoAcao.BuscarMaxCodAcaoComercial() + 1
                idAcao = acaoComercial.CODACOCMC
            Else
                ' Se nao for um registro novo exclui os dados antigos para a nova insercao
                DeletarAcaoComercial(idAcao)

                boFornecedores.DeletarDadosFornecedorDaAcao(idAcao)
                boCarimbos.DeletaRelacaoAcaoFornecedorCarimboDaAcao(idAcao)
                boPremiacoes.DeletarRelacaoPremiacaoExclusivaDaAcao(idAcao)
                boBonificoes.DeletarRelacaoPedidoBonificacaoDaAcao(idAcao)
                boRelacaoPlanoMarketing.DeletarPlanosMarketingDaAcao(idAcao)
                boIncentivo.DeletarIncentivoDaAcao(idAcao)
                'TODO: Deletar Itens Orcamento
            End If

            ' Insere dados da acao - InsDdoAcoCmc(0)
            InserirAcaoComercial(acaoComercial)
            acaoComercial.CODACOCMC = idAcao

            ' Insede dados da mecanica da acao
            If Not (acaoComercial.Mecanica Is Nothing) Then
                acaoComercial.Mecanica.CODACOCMC = idAcao
                boMecanicaAcao.InsereDadosMecanicaDaAcao(acaoComercial.Mecanica)
            End If

            ' Salva o patrocinador da acao
            Select Case acaoComercial.TIPACOMCD
                Case TipoAcao.Fornecedor, TipoAcao.PreAcordoVigencia, TipoAcao.PreAcordoValor
                    'Se o fornecedor for DESPESAS MARTINS ele precisa de um BU - a mensagem sera tratada na tela e não mais no codigo de inserir
                    If (acaoComercial.Fornecedores.Count = 0) Then
                        IndDspMrt = 0
                    Else
                        IndDspMrt = IIf(acaoComercial.Fornecedores.FirstOrDefault().CODFRN = 999999 AndAlso acaoComercial.Fornecedores.FirstOrDefault().CODUNDESRNGC IsNot Nothing, 0, 1)
                    End If

                    GravarComGridFornecedor(acaoComercial, IndEnvGrv)
                Case TipoAcao.Carimbo
                    GravarComGridCarimbo(acaoComercial)
                Case TipoAcao.Premiacao
                    GravarComGridPremiacaoExclusiva(acaoComercial)
                Case TipoAcao.PedidoBonificacao
                    GravarComGridPedidoBonificacao(acaoComercial)
                Case Else
            End Select

            'Insere Dados do Plano de Marketing da Açao Comercial
            boRelacaoPlanoMarketing.InserirDadosPlanoMarketingDaAcao(IndEnvGrv, acaoComercial)

            'Grava associação de Incentivo x Ação Comercial
            If acaoComercial.Incentivos IsNot Nothing AndAlso acaoComercial.Incentivos.Count > 0 Then
                'Atrela Incentivo a Ação Mercadologica que esta sendo cadastrada
                boIncentivo.InserirGrupoDeIncentivo(idAcao, acaoComercial.Incentivos)
            End If

            'Se for despesas martins e o BU não tiver sido escolhido - indDspMrt = 1
            'então não insere os dados dos itens
            If IndDspMrt <> 1 Then
                If acaoComercial.ItensOrcamento IsNot Nothing AndAlso acaoComercial.ItensOrcamento.Count >= 1 Then
                    'insere dados dos itens de orçamento
                    'InsDdoIteAcoCmc()
                Else
                    'Deleta dados dos itens do orçamento
                    'DelDdoAcoCmcIte(idAcao)
                End If
            End If

            If (IndItemOrcamento) Then
                Dim boRelacaoItemPrincipalItemSubstitutoAcaoComercial As New RelacaoItemPrincipalItemSubstitutoAcaoComercialBO
                For Each merc In acaoComercial.ItensOrcamento
                    merc.CODACOCMC = acaoComercial.CODACOCMC
                    boRelacaoAcaoComercialPorItemOrcamento.InsertT(merc)
                Next

                Dim filtroSubs As New VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO With {
                    .CODACOCMC = codAcaoAntigo
                }
                Dim itensSub As List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO) = boRelacaoItemPrincipalItemSubstitutoAcaoComercial.ListT(filtroSubs)
                For Each item In itensSub
                    item.CODACOCMC = acaoComercial.CODACOCMC
                    boRelacaoItemPrincipalItemSubstitutoAcaoComercial.InsertT(item)
                Next
            End If

            '-----------------------------------------------------------------------------------------------------------------------------------

            'Salvar Ação Mecanica
            Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
            If (acaoComercial.relacaoAcaoMecanicaList IsNot Nothing) Then
                For Each item In acaoComercial.relacaoAcaoMecanicaList
                    'Salvar dados Gerais / Filiais / Criterio de Apuração
                    item.CODACOCMC = idAcao
                    Dim acaoMecanica = BO.Save(idAcao, item.NUMSEQRGRACOCMC = 0, item)

                    Dim CODACOCMC = acaoMecanica.CODACOCMC
                    Dim NUMSEQRGRACOCMC = acaoMecanica.NUMSEQRGRACOCMC

                    'If (item.RelacaoAcaoMecanicaCriterioDeApuracaoList IsNot Nothing) Then
                    '    Dim relacaoAcaoMecanicaCriterioDeApuracaoBO As RelacaoAcaoMecanicaCriterioDeApuracaoBO = New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
                    '    relacaoAcaoMecanicaCriterioDeApuracaoBO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, item.RelacaoAcaoMecanicaCriterioDeApuracaoList)
                    'End If

                    'Salvar Publico Alvo
                    If (item.relacaoAcaoMecanicaPublicoAlvoEstadoList IsNot Nothing) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoEstadoBO As RelacaoAcaoMecanicaPublicoAlvoEstadoBO = New RelacaoAcaoMecanicaPublicoAlvoEstadoBO()
                        relacaoAcaoMecanicaPublicoAlvoEstadoBO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, item.relacaoAcaoMecanicaPublicoAlvoEstadoList)
                    End If

                    If (item.relacaoAcaoMecanicaPublicoAlvoAtividadeList IsNot Nothing) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoAtividadeBO As RelacaoAcaoMecanicaPublicoAlvoAtividadeBO = New RelacaoAcaoMecanicaPublicoAlvoAtividadeBO()
                        relacaoAcaoMecanicaPublicoAlvoAtividadeBO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, item.relacaoAcaoMecanicaPublicoAlvoAtividadeList)
                    End If

                    If (item.relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList IsNot Nothing) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO As RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO()
                        relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteBO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, item.relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList)
                    End If

                    If (item.relacaoAcaoMecanicaPublicoAlvoNegocioClienteList IsNot Nothing) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoNegocioClienteBO As RelacaoAcaoMecanicaPublicoAlvoNegocioClienteBO = New RelacaoAcaoMecanicaPublicoAlvoNegocioClienteBO()
                        relacaoAcaoMecanicaPublicoAlvoNegocioClienteBO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, item.relacaoAcaoMecanicaPublicoAlvoNegocioClienteList)
                    End If

                    If (item.relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList IsNot Nothing) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoSegmentoClienteBO As RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteBO()
                        relacaoAcaoMecanicaPublicoAlvoSegmentoClienteBO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, item.relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList)
                    End If
                    If (item.relacaoAcaoMecanicaPublicoAlvoClienteList IsNot Nothing) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoClienteBO As RelacaoAcaoMecanicaPublicoAlvoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoClienteBO()
                        For Each obj In item.relacaoAcaoMecanicaPublicoAlvoClienteList
                            obj.CODACOCMC = CODACOCMC
                            obj.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                            relacaoAcaoMecanicaPublicoAlvoClienteBO.InsertT(obj)
                        Next
                    End If

                    If (item.relacaoAcaoMecanicaPublicoAlvoRcaVendedorList IsNot Nothing) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoRcaVendedorBO As RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO()
                        For Each obj In item.relacaoAcaoMecanicaPublicoAlvoRcaVendedorList
                            obj.CODACOCMC = CODACOCMC
                            obj.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                            relacaoAcaoMecanicaPublicoAlvoRcaVendedorBO.InsertT(obj)
                        Next
                    End If

                    If (item.relacaoAcaoMecanicaPublicoAlvoGerenteVendasList IsNot Nothing) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoGerenteVendasBO As RelacaoAcaoMecanicaPublicoAlvoGerenteVendasBO = New RelacaoAcaoMecanicaPublicoAlvoGerenteVendasBO()
                        relacaoAcaoMecanicaPublicoAlvoGerenteVendasBO.InsertList(CODACOCMC, NUMSEQRGRACOCMC, item.relacaoAcaoMecanicaPublicoAlvoGerenteVendasList)
                    End If

                    If (item.relacaoAcaoMecanicaPublicoAlvoGerenteMercadoList IsNot Nothing) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO As RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO = New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO()
                        For Each obj In item.relacaoAcaoMecanicaPublicoAlvoGerenteMercadoList
                            obj.CODACOCMC = CODACOCMC
                            obj.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                            relacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO.InsertT(obj)
                        Next
                    End If

                    'Salvar Clientes Excludentes
                    Dim relacaoAcaoMecanicaClientesExcludentesBO As RelacaoAcaoMecanicaClientesExcludentesBO = New RelacaoAcaoMecanicaClientesExcludentesBO()
                    For Each obj In item.relacaoAcaoMecanicaClientesExcludentesList
                        obj.CODACOCMC = CODACOCMC
                        obj.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                        relacaoAcaoMecanicaClientesExcludentesBO.InsertT(obj)
                    Next

                    'Salvar Mix
                    Dim relacaoAcaoMecanicaMixBO As RelacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()
                    For Each obj In item.relacaoAcaoMecanicaMixList
                        obj.CODACOCMC = CODACOCMC
                        obj.NUMSEQRGRACOCMC = NUMSEQRGRACOCMC
                        relacaoAcaoMecanicaMixBO.InsertT(obj)
                    Next

                Next
            End If
            '-----------------------------------------------------------------------------------------------------------------------------------

            If _iniciatransacao Then
                daoBase.connector.Commit()
            End If



            Return acaoComercial
        Catch ex As Exception
            If _iniciatransacao Then
                daoBase.connector.Rollback()
            End If
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' BtEnviar_Click
    ''' Envia acao comercial e seus grids
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    ''' <returns></returns>
    Public Function EnviarAcaoComercial(acaoComercial As VO.AcaoComercial, ByVal icentivosHabilitado As Boolean, ByRef bBotaoPolitica As Boolean, ByVal benviar As Boolean, ByRef bDesabilitabotao As Integer, ByVal indPreAcordo As Boolean) As VO.AcaoComercial

        Try
            daoBase.connector.BeginTransaction()
            ' Salva o patrocinador da acao

            Select Case acaoComercial.TIPACOMCD
                Case TipoAcao.Fornecedor, TipoAcao.PreAcordoVigencia, TipoAcao.PreAcordoValor
                    boFornecedores.EnviarAcoCmcFornecedor(acaoComercial, icentivosHabilitado, bBotaoPolitica, benviar, bDesabilitabotao, indPreAcordo)
                Case TipoAcao.Carimbo
                    If boCarimbos.VerificaSaldo(acaoComercial.Carimbos) Then
                        boCarimbos.EnviarAcoCmcCarimbo(acaoComercial, icentivosHabilitado, bBotaoPolitica, benviar, bDesabilitabotao, indPreAcordo)
                    End If
                Case TipoAcao.Premiacao
                    boPremiacoes.EnviarAcoCmcPremiacao(acaoComercial, icentivosHabilitado, bBotaoPolitica, benviar, bDesabilitabotao, indPreAcordo)
                Case TipoAcao.PedidoBonificacao
                    boBonificoes.EnviarAcoCmcBonificacao(acaoComercial, icentivosHabilitado, bBotaoPolitica, benviar, bDesabilitabotao, indPreAcordo)
                Case Else
            End Select
            acaoComercial.CODSTAAPVACOCMC = 3

            SalvarAcaoComercial(acaoComercial, False, IndEnvGrv:=1)

            daoBase.connector.Commit()
            Return acaoComercial

        Catch 'ex As Exception
            daoBase.connector.Rollback()
            Throw 'ex
        End Try

    End Function

    ''' <summary>
    ''' BuscarMaxCodAcaoComercial
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuscarMaxCodAcaoComercial() As Decimal

        Return DirectCast(daoBase, AcaoComercialDAO).BuscarMaxCodAcaoComercial()

    End Function

    ''' <summary>
    ''' Busca saldo do fornecedor de marketing
    ''' </summary>
    ''' <param name="iCodFrn"></param>
    ''' <param name="iTipDsnDscBnf"></param>
    ''' <param name="iAnoMesRef"></param>
    ''' <returns></returns>
    Public Function BuscarSaldoFornecedorMarketing(iCodFrn As Long, iTipDsnDscBnf As Integer, iAnoMesRef As Integer) As List(Of VO.SaldoFornecedorMarketing)

        Return DirectCast(daoBase, AcaoComercialDAO).BuscarSaldoFornecedorMarketing(iCodFrn, iTipDsnDscBnf, iAnoMesRef)

    End Function

    ''' <summary>
    ''' Busca numero de reprovacoes e de politicas violadas da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <returns></returns>
    Public Function BuscarNumeroReprovacoesEPoliticas(codAcao As Integer) As VO.AcaoComercial

        Return DirectCast(daoBase, AcaoComercialDAO).BuscarNumeroReprovacoesEPoliticas(codAcao)

    End Function

    ''' <summary>
    '''  Salva os dados da acao quando o tipo de patrocinador é fornecedor
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    ''' <param name="IndEnvGrv">IndEnvGrv = 0 = para gravar, IndEnvGrv = 1 = para enviar</param>
    Private Sub GravarComGridFornecedor(acaoComercial As VO.AcaoComercial, IndEnvGrv As Integer)
        If acaoComercial.Fornecedores IsNot Nothing AndAlso acaoComercial.Fornecedores.Count > 0 Then
            'Insere dados dos patrocinadores da ação comercial
            boFornecedores.InserirDadosFornecedores(IndEnvGrv, acaoComercial)
        End If
    End Sub

    ''' <summary>
    ''' Salva os dados da acao quanto o tipo de patrocinador é carimbo
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    Private Sub GravarComGridCarimbo(acaoComercial As VO.AcaoComercial)

        If acaoComercial.Carimbos.Count > 0 Then
            boCarimbos.InserirDadosRelacaoFornecedorCarimbo(acaoComercial)
            'Insere dados dos carimbos na tabela de fornecedor acao
            boCarimbos.InserirDadosCarimbo(acaoComercial)
        End If

    End Sub

    ''' <summary>
    ''' Salva os dados da acao quando o tipo de patrocinador é premiacoes exclusivas
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    Private Sub GravarComGridPremiacaoExclusiva(acaoComercial As VO.AcaoComercial)
        If acaoComercial.Premiacao IsNot Nothing AndAlso acaoComercial.Premiacao.Count > 0 Then
            'Insere dados dos patrocinadores da ação comercial
            boPremiacoes.InserirRelacaoPremiacaoExclusivaNaAcao(acaoComercial.CODACOCMC, acaoComercial.Premiacao)
            'Insere dados das premiacoes na tabela de fornecedor acao
            boPremiacoes.InserirDadosFornecedorPremiacaoExclusiva(acaoComercial)
        End If
    End Sub

    ''' <summary>
    ''' Salva os dados da acao quando o tipo de patrocinador é pedidos bonificaçoes
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    Private Sub GravarComGridPedidoBonificacao(acaoComercial As VO.AcaoComercial)
        If acaoComercial.Bonificacoes IsNot Nothing AndAlso acaoComercial.Bonificacoes.Count > 0 Then
            'Insere dados dos patrocinadores da ação comercial
            boBonificoes.InserirRelacaoPedidoBonificacaoNaAcao(acaoComercial.CODACOCMC, acaoComercial.Bonificacoes)
            'Insere dados das bonificacoes na tabela de fornecedor acao
            boBonificoes.InserirDadosFornecedorPedidoBonificacao(acaoComercial)
        End If
    End Sub

    ''' <summary>
    ''' InsDdoAcoCmc
    ''' Insere os dados da acao comercial no banco
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    ''' <param name="tipoFluxo"></param>
    ''' <returns></returns>
    Public Function InserirAcaoComercial(acaoComercial As VO.AcaoComercial, Optional tipoFluxo As Integer = 0) As VO.AcaoComercial

        ' Se nao tem acao original coloca o valor 0
        acaoComercial.CODACOCMCORI = If(acaoComercial.CODACOCMCORI, 0)
        'Busca codigo do status
        If (IsNothing(acaoComercial.CODSTAAPVACOCMC) OrElse acaoComercial.CODSTAAPVACOCMC = 0) Then
            acaoComercial.CODSTAAPVACOCMC = 1
        Else
            acaoComercial.CODSTAAPVACOCMC = acaoComercial.CODSTAAPVACOCMC
        End If
        'acaoComercial.CODSTAAPVACOCMC = IIf(IsNothing(acaoComercial.CODSTAAPVACOCMC) OrElse acaoComercial.CODSTAAPVACOCMC = 0, 1, acaoComercial.CODSTAAPVACOCMC) 'bscCodStaAcoCmc()
        'Data de geracao da acao
        acaoComercial.DATGRCACOCMC = Now.Date
        'Calcula valor total
        acaoComercial.VLRTOTACOCMC = CalculaValorTotalAcaoComercial(acaoComercial)

        '001-01-01" quando status diferente de aprovada
        acaoComercial.DATAPVACOCMC = Date.MinValue
        ' 0 até enviar
        acaoComercial.TIPFLUAPVACOCMC = tipoFluxo
        ' Codigo do aprovador é 0 até enviar
        acaoComercial.CODFNCAPVATUACOCMC = 0
        ' Nao pode inserir null
        acaoComercial.DESOBJACOCMC = If(acaoComercial.DESOBJACOCMC, " ")


        If (acaoComercial.CODACOCMCCPI Is Nothing) Then
            acaoComercial.CODACOCMCCPI = 0
        End If

        'Associa tipo de desconto'
        If (acaoComercial.EmpenhoExa Is Nothing) Then
            acaoComercial.TIPDSNDSCBNF = 0
        Else
            acaoComercial.TIPDSNDSCBNF = CDec(acaoComercial.EmpenhoExa.Split(" - ")(0).Trim)
        End If

        acaoComercial.CODEVTBTB = If(acaoComercial.CODEVTBTB, 0)

        Return DirectCast(daoBase, AcaoComercialDAO).InserirAcaoComercial(acaoComercial)

    End Function

    ''' <summary>
    ''' DelDdoAcoCmc
    ''' Deleta uma acao comercial do banco
    ''' </summary>
    ''' <param name="idAcao"></param>
    Public Sub DeletarAcaoComercial(idAcao As Integer)

        DirectCast(daoBase, AcaoComercialDAO).DeletarAcaoComercial(idAcao)

    End Sub

    ''' <summary>
    ''' CalVlrTotAcoCmc
    ''' Calcula valor total da acao comercial
    ''' Participacao + Tx Adm + Tx Brinde
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    ''' <returns></returns>
    Public Function CalculaValorTotalAcaoComercial(acaoComercial As VO.AcaoComercial) As Decimal
        Return acaoComercial.VLRTOTOCDACOCMC + acaoComercial.VLRTOTTXAADMACOCMC + acaoComercial.VLRTOTBDEACOCMC
    End Function


    ''' <summary>
    ''' AtuDdoSldPlnMkt
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    Public Function AtualizaDadosPlanoDeMarketing(acaoComercial As VO.AcaoComercial) As List(Of VO.PlanoDeMarketing)
        Dim PlanodeMarkeing As New List(Of VO.PlanoDeMarketing)
        Dim itempplanomarqketing As VO.PlanoDeMarketing
        Dim vlartotdu As Decimal

        If acaoComercial.TIPEVTACOCMC IsNot Nothing And acaoComercial.CODEVTACOCMC IsNot Nothing And acaoComercial.Fornecedores.Count > 0 Then
            'apaga linhas da grid se já existir saldo calculado
            acaoComercial.PlanosDeMarketing = New List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

            ' Pega a data atual para saber qual a referencia
            Dim dataReferencia = Format(Now, "dd/MM/yyyy")
            dataReferencia = dataReferencia.Split("/"c)(2) & dataReferencia.Split("/"c)(1)

            Dim strCodigosBU = ""
            Dim strVirg = ""
            For Each fornecedor As VO.RelacaoAcaoComercialxFornecedores In acaoComercial.Fornecedores
                strCodigosBU = strCodigosBU & strVirg & fornecedor.CODUNDESRNGC
                strVirg = ","
            Next

            'Busca os saldos do plano de marketing
            Dim saldosPlanosMarketing = boPlanoMarketing.CalculaSaldoPlanoMarketingAcao(acaoComercial.TIPEVTACOCMC, acaoComercial.CODEVTACOCMC, dataReferencia, strCodigosBU)

            'Coloca a lista de saldos no modal de saldos do plano de marketing
            ' Monta o grid com algumas informacaoes calculadas 
            For Each itemsaldo In saldosPlanosMarketing
                itempplanomarqketing = New VO.PlanoDeMarketing With {
                    .CODUNDESRNGC = itemsaldo.CODUNDESRNGC,
                    .DESUNDESRNGC = itemsaldo.CODUNDESRNGC.ToString() + " - " + itemsaldo.DESUNDESRNGC,
                    .SLDMESPLNMKT = itemsaldo.SLDMESPLNMKT,
                    .SLDATUPLNMKT = itemsaldo.SLDATUPLNMKT,
                    .SLDANOPLNMKT = itemsaldo.SLDANOPLNMKT,
                    .VLROCDANOPLNMKT = itemsaldo.VLROCDANOPLNMKT,
                    .VLRGSTANOPLNMKT = itemsaldo.VLRGSTANOPLNMKT
                }

                vlartotdu = 0

                If acaoComercial.Fornecedores.Count > 0 Then
                    For Each itemforncedor In acaoComercial.Fornecedores
                        If itemforncedor.CODUNDESRNGC = itemsaldo.CODUNDESRNGC Then
                            'calculo do vlr utilizado na ação pelo BU
                            vlartotdu = vlartotdu + itemforncedor.VLRPTCFRNACOCMC + itemforncedor.VLRTXAADMFRNACOCMC + itemforncedor.VLRTXAADMBDEACOCMC

                        End If
                    Next

                    itempplanomarqketing.VLRUTZPLNMKT = vlartotdu
                End If

                PlanodeMarkeing.Add(itempplanomarqketing)
            Next
        End If

    End Function


#Region "Estorno"

    ''' <summary>
    ''' Buscs autorizacao para Estorno Manual
    ''' cnsAutEstorno
    ''' </summary>
    ''' <param name="codFnc"></param>
    ''' <returns></returns>
    Public Function ConsultaAutorizacaoEstorno(codFnc As Long) As Boolean

        Dim relacaoSistemaMenu = DirectCast(daoBase, AcaoComercialDAO).ConsultaAcaoMenuFuncionario(codFnc, 1, 19)

        If relacaoSistemaMenu IsNot Nothing AndAlso relacaoSistemaMenu.Count > 0 Then
            Return relacaoSistemaMenu(0).ACS = 1
        End If

        Return False

    End Function

    ''' <summary>
    ''' 'BscDdoEtrMan'
    ''' Busca dados para estornar
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function BuscaDadosParaEstorno(idAcao As Decimal) As VO.EstornoAcaoMercadologica

        Dim estorno = DirectCast(daoBase, AcaoComercialDAO).BuscarDadosAcaoEstorno(idAcao)

        If estorno.TIPACOMCD = 3 Then
            estorno.Fornecedores = boFornecedores.BuscarFornecedoresDaAcaoBonificacao(idAcao)
        Else
            estorno.Fornecedores = boFornecedores.BuscarFornecedoresDaAcaoETR(idAcao)
        End If

        estorno.ItensOrcamento = boItensOrcamento.BuscarItensOrcamentoSubstituidosDaAcaoETR(idAcao)

        If estorno.ItensOrcamento.Count > 0 Then
            For Each item In estorno.ItensOrcamento

                If (estorno.CODOPERACOES = "") Then
                    estorno.CODOPERACOES += item.CODOPEFSCDSNACOCMC
                Else
                    estorno.CODOPERACOES += "," + item.CODOPEFSCDSNACOCMC.ToString()
                End If

            Next
        End If

        Return estorno

    End Function

    ''' <summary>
    ''' recalcularTotValOpe
    ''' </summary>
    Public Function RecalcularTotalValorOperacoes(estorno As VO.EstornoAcaoMercadologica, itensChecados As List(Of Integer)) As VO.EstornoAcaoMercadologica
        Dim valorTotal = 0.0

        If itensChecados IsNot Nothing AndAlso itensChecados.Count > 0 Then
            For Each cod In itensChecados
                Dim item = estorno.ItensOrcamento.Find(Function(i) i.CODOPEFSCDSNACOCMC = cod)
                valorTotal += item.SALDO
            Next
        Else
            valorTotal = Nothing
        End If

        estorno.Fornecedores = RecalcularValorEstornoFornecedores(valorTotal, estorno.Fornecedores)

        'If Me.TabCarimbo.Visible Then
        '    recalculaValCrb(vlrTotOpe)
        'End If

        Return estorno

    End Function

    ''' <summary>
    ''' recalcularValFrn
    ''' </summary>
    ''' <param name="valorTotal"></param>
    ''' <param name="fornecedores"></param>
    ''' <returns></returns>
    Public Function RecalcularValorEstornoFornecedores(valorTotal As Decimal?, fornecedores As List(Of VO.RelacaoAcaoComercialxFornecedores)) As List(Of VO.RelacaoAcaoComercialxFornecedores)
        If valorTotal IsNot Nothing Then
            For Each fornecedor In fornecedores
                fornecedor.EstornoAcordo = valorTotal * fornecedor.PERCENTUAL / 100%
                'fornecedor.EstornoExaAcordo = valorTotal * fornecedor.PercPtc * fornecedor.PerExaAcordo
            Next
        Else
            For Each fornecedor In fornecedores
                fornecedor.EstornoAcordo = Nothing
                '  fornecedor.EstornoExaAcordo = Nothing
            Next
        End If
        Return fornecedores
    End Function

    ''' <summary>
    ''' FunIsrEtrMan
    ''' </summary>
    ''' <param name="estorno"></param>
    ''' <returns></returns>
    'Public Function InserirEstornoManual(estorno As VO.EstornoAcaoMercadologica, codUsrRede As Integer, usrRede As String) As Integer
    Public Function InserirEstornoManual(estorno As VO.EstornoAcaoMercadologica) As Integer
        '
        Try
            daoBase.connector.BeginTransaction()

            Dim iResultado As Integer
            Dim sDesUtzIteOcdAcoCmc As String = "Estorno Ac. Mercad. c.c Frn."

            ' sOpeEtrMan = itens selecionados para estornar

            If estorno.CODOPERACOES IsNot Nothing AndAlso estorno.CODOPERACOES <> "" AndAlso
                    estorno.ItensOrcamento IsNot Nothing AndAlso estorno.ItensOrcamento.Count > 0 Then

                iResultado = InserirLogEstornoManual(estorno, sDesUtzIteOcdAcoCmc)

                'Métodos que atualiza o valor do estorno
                boItensOrcamento.AtualizaEstornoItensAcao(estorno.CODACOCMC, estorno.CODOPERACOES)
                boItensOrcamento.AtualizaEstornoVlrAcao(estorno.CODACOCMC, estorno.CODOPERACOES)

                For Each fornecedor In estorno.Fornecedores
                    boItensOrcamento.AtualizaEstornoCarimbo(fornecedor.CODMCOVBAFRN, fornecedor.VALOR_ESTORNAR)
                Next

                'iResultado = FunChmPrccAtlCntCrrFrn(estorno)
                'iResultado = FunAtuIProc(estorno)

            End If

            '
            'iRstIsr = FunChmPrccAtlCntCrrFrn(CodAcoCmc,
            '                                         CodFnc,
            '                                         NomUsrSis,
            '                                         dsEtrMan,
            '                                         EmpExaAcc,
            '                                         "Estorno ac.mercad. " & CodAcoCmc.ToString() & " c.c frn.",
            '                                         58,
            '                                         oObeCsnDdo)
            '

            daoBase.connector.Commit()


            Return iResultado
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' FunIsrLogEtr
    ''' </summary>
    ''' <param name="estorno"></param>
    ''' <param name="sDesUtzIteOcdAcoCmc"></param>
    ''' <param name="carimbo"></param>
    ''' <returns></returns>
    Public Function InserirLogEstornoManual(estorno As VO.EstornoAcaoMercadologica,
                                 sDesUtzIteOcdAcoCmc As String, Optional carimbo As Boolean = False) As Integer

        'Regra de negócio vinculada as estruturas recebidas
        Dim QdeUtzIteOcdAcoCmc = 1
        Dim IndTipIsrUtzIteOcd = 0
        Dim sDesAuxUtzIteOcdAcoCmc = sDesUtzIteOcdAcoCmc

        Dim iIndFrn As Integer
        Dim valorSaldoItem As Double = 0
        Dim valorSaldoFrn As Double = 0

        Dim carimboOuFornecedor = If(True, estorno.Fornecedores, estorno.Carimbos)

        Dim iResult As Integer

        If carimboOuFornecedor.Count > 0 Then
            Dim itensOcdSld = boItensOrcamento.ConsultaValoresDisponiveis(estorno.CODACOCMC, estorno.CODOPERACOES)

            If itensOcdSld IsNot Nothing AndAlso itensOcdSld.Count > 0 Then

                For Each item In itensOcdSld
                    valorSaldoItem = (Math.Round(CDbl(item.SALDO * 100))) / 100

                    'While (valorSaldoItem > 1.0 And itensOcdSld.Count < iIndFrn)
                    'While (valorSaldoItem > 1.0) And iIndFrn <= itensOcdSld.Count

                    '    If valorSaldoFrn = 0 And iIndFrn <= itensOcdSld.Count Then
                    '        If Not bFlgExaAco Then
                    '            iIndTipAco = 0
                    '            valorSaldoFrn = carimboOuFornecedor(iIndFrn).EstornoAcordo
                    '            valorSaldoFrn = (Math.Round(valorSaldoFrn * 100)) / 100
                    '        Else
                    '            iIndTipAco = 1
                    '            valorSaldoFrn = carimboOuFornecedor(iIndFrn).EstornoExaAcordo
                    '            valorSaldoFrn = (Math.Round(valorSaldoFrn * 100)) / 100
                    '        End If
                    '    End If

                    'If valorSaldoFrn > 0 Then

                    Dim objetoItemUtilizado = New VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem With {
                        .CODACOCMC = estorno.CODACOCMC,
                        .NUMLNHITEOCDACOCMC = item.NUMLNHITEOCDACOCMC,
                        .QDEUTZITEOCDACOCMC = QdeUtzIteOcdAcoCmc,
                        .INDTIPISRUTZITEOCD = IndTipIsrUtzIteOcd,
                        .DESUTZITEOCDACOCMC = sDesAuxUtzIteOcdAcoCmc & " Ref Acordo",
                        .VLRUTZITEOCDACOCMC = valorSaldoItem,
                        .CODFNC = carimboOuFornecedor(iIndFrn).CODFRN
                    }

                    iResult = boRelacaoItemUtilizado.InsereLogEstornoManual(objetoItemUtilizado, carimboOuFornecedor(iIndFrn).CODFRN)

                    iIndFrn += 1
                    valorSaldoItem = 0

                    'If iIndTipAco = 0 Then
                    '    objetoItemUtilizado.DESUTZITEOCDACOCMC = sDesAuxUtzIteOcdAcoCmc & " Ref Acordo"
                    'Else
                    '    objetoItemUtilizado.DESUTZITEOCDACOCMC = sDesAuxUtzIteOcdAcoCmc & " Ref Exa Acordo"
                    'End If

                    'If valorSaldoFrn >= valorSaldoItem Then
                    '    objetoItemUtilizado.VLRUTZITEOCDACOCMC = valorSaldoItem

                    '    iResult = boRelacaoItemUtilizado.InsereLogEstornoManual(objetoItemUtilizado, carimboOuFornecedor(iIndFrn).CODFRN)

                    '    valorSaldoFrn = valorSaldoFrn - valorSaldoItem
                    '    valorSaldoFrn = (Math.Round(valorSaldoFrn * 100)) / 100
                    '    valorSaldoItem = 0

                    'ElseIf valorSaldoFrn < valorSaldoItem Then

                    '    objetoItemUtilizado.VLRUTZITEOCDACOCMC = valorSaldoFrn

                    '    iResult = boRelacaoItemUtilizado.InsereLogEstornoManual(objetoItemUtilizado, carimboOuFornecedor(iIndFrn).CODFRN)

                    '    valorSaldoItem = valorSaldoItem - valorSaldoFrn
                    '    valorSaldoItem = (Math.Round(valorSaldoItem * 100)) / 100
                    '    valorSaldoFrn = 0

                    'End If
                    'End If

                    '    If valorSaldoFrn = 0 Then
                    '        If Not bFlgExaAco Then
                    '            bFlgExaAco = True
                    '        Else
                    '            iIndFrn += 1
                    '            bFlgExaAco = False
                    '        End If
                    '    End If
                    'Else
                    '    If Not bFlgExaAco Then
                    '        bFlgExaAco = True
                    '    Else
                    '        iIndFrn += 1
                    '        bFlgExaAco = False
                    '    End If
                    'End If

                    'End While

                Next

            End If
        End If

        Return iResult
    End Function

    ''' <summary>
    ''' FunChmPrccAtlCntCrrFrn
    ''' CONSULTA BANCO EXTERNO
    ''' </summary>
    ''' <param name="estorno"></param>
    ''' <returns></returns>
    Public Function FunChmPrccAtlCntCrrFrn(estorno As VO.EstornoAcaoMercadologica) As Integer

        If Not IsNothing(estorno.Fornecedores) AndAlso
           estorno.Fornecedores.Count > 0 Then

            For i As Integer = 0 To estorno.Fornecedores.Count - 1
                If estorno.Fornecedores(i).EstornoAcordo > 0 Then
                    'OUTRO BANCO
                    'oObeCsnDdo.FunChmPrcAtlCntCrrFrn(Now,
                    '                              EmpExaAcc,
                    '                              dvEtr(i)("CodFrn"),
                    '                              "C",
                    '                              TipEmp,
                    '                              " ",
                    '                              " ",
                    '                              " ",
                    '                              " ",
                    '                              CDbl(dvEtr(i)("VlrEtrAco")),
                    '                              DesEtr & " Ref Acordo",
                    '                              " ",
                    '                              NomUsrSis,
                    '                              "PRCDJAtlCntCrrFrn")
                End If
                '
                If estorno.Fornecedores(i).EstornoExaAcordo > 0 Then
                    'OUTRO BANCO
                    'oObeCsnDdo.FunChmPrcAtlCntCrrFrn(Now,
                    '                              EmpExaAcc,
                    '                              dvEtr(i)("CodFrn"),
                    '                              "C",
                    '                              TipEmp,
                    '                              " ",
                    '                              " ",
                    '                              " ",
                    '                              " ",
                    '                              CDbl(dvEtr(i)("VlrEtrExaAco")),
                    '                              DesEtr & " Ref Exa Acordo",
                    '                              " ",
                    '                              NomUsrSis,
                    '                              "PRCDJAtlCntCrrFrn")
                End If
            Next
        End If
        Return 1
    End Function

    ''' <summary>
    ''' FunAtuIProc
    ''' </summary>
    ''' <param name="estorno"></param>
    ''' <returns></returns>
    Public Function FunAtuIProc(estorno As VO.EstornoAcaoMercadologica) As Integer
        '
        Dim iResultado As Integer
        'Regra de negócio vinculada as estruturas recebidas
        Dim QdeUtzIteOcdAcoCmc = 0
        Dim VlrUtzIteOcdAcoCmc = 1
        '
        Dim valorSaldoItem As Double
        Dim valorSaldoFrn As Double
        Dim iIndFrn As Integer
        Dim bFlgExaAco As Boolean
        Dim iIndTipAco As Integer

        If estorno.Fornecedores IsNot Nothing AndAlso estorno.Fornecedores.Count > 0 Then

            Dim itensOcdSld = boItensOrcamento.ConsultaValoresDisponiveis(estorno.CODACOCMC, estorno.CODOPERACOES)

            If itensOcdSld IsNot Nothing AndAlso itensOcdSld.Count > 0 Then

                For Each item In itensOcdSld
                    valorSaldoItem = item.SALDO

                    While valorSaldoItem > 1.0

                        If valorSaldoFrn = 0 Then
                            If Not bFlgExaAco Then
                                iIndTipAco = 0
                                valorSaldoFrn = estorno.Fornecedores(iIndFrn).EstornoAcordo
                            Else
                                iIndTipAco = 1
                                valorSaldoFrn = estorno.Fornecedores(iIndFrn).EstornoExaAcordo
                            End If
                        End If

                        If valorSaldoFrn > 0 Then

                            Dim itemUtilizado = New VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem

                            If valorSaldoFrn >= valorSaldoItem Then

                                itemUtilizado.VLRUTZITEOCDACOCMC = valorSaldoItem

                                If item.CODMER <> 0 Then
                                    If item.CODOPEFSCDSNACOCMC = 7 Or item.CODOPEFSCDSNACOCMC = 6 Or item.CODOPEFSCDSNACOCMC = 17 Then

                                        Dim dsExpCod = boRelacaoItemUtilizado.FunCnsExpCod(item.CODMER, item.CODOPEFSCDSNACOCMC)
                                        ' chechar coluna
                                        Dim type_expense_code = 1
                                        If dsExpCod IsNot Nothing AndAlso dsExpCod.Count > 0 Then
                                            iResultado = boRelacaoItemUtilizado.FunAtuAct(estorno.CODACOCMC, item.CODOPEFSCDSNACOCMC, type_expense_code, itemUtilizado.VLRUTZITEOCDACOCMC)
                                        End If
                                    End If
                                End If

                                valorSaldoFrn = valorSaldoFrn - valorSaldoItem
                                valorSaldoItem = 0
                            ElseIf valorSaldoFrn < valorSaldoItem Then

                                itemUtilizado.VLRUTZITEOCDACOCMC = valorSaldoFrn

                                If item.CODMER <> 0 Then
                                    If item.CODOPEFSCDSNACOCMC = 7 Or item.CODOPEFSCDSNACOCMC = 6 Or item.CODOPEFSCDSNACOCMC = 17 Then

                                        Dim dsExpCod = boRelacaoItemUtilizado.FunCnsExpCod(item.CODMER, item.CODOPEFSCDSNACOCMC)
                                        ' chechar coluna
                                        Dim type_expense_code = 1
                                        If dsExpCod IsNot Nothing AndAlso dsExpCod.Count > 0 Then
                                            iResultado = boRelacaoItemUtilizado.FunAtuAct(estorno.CODACOCMC, item.CODOPEFSCDSNACOCMC, type_expense_code, itemUtilizado.VLRUTZITEOCDACOCMC)
                                        End If
                                    End If
                                End If

                                valorSaldoItem = valorSaldoItem - valorSaldoFrn
                                valorSaldoFrn = 0

                            End If

                            If valorSaldoFrn = 0 Then
                                If Not bFlgExaAco Then
                                    bFlgExaAco = True
                                Else
                                    iIndFrn += 1
                                    bFlgExaAco = False
                                End If
                            End If
                        Else
                            If Not bFlgExaAco Then
                                bFlgExaAco = True
                            Else
                                iIndFrn += 1
                                bFlgExaAco = False
                            End If
                        End If
                    End While
                Next
            End If
        End If

        Return iResultado

    End Function

#End Region

#Region "Saldo Ação x Evento Em Aberto"

    ''' <summary>
    ''' bscDdoAcoCmc
    ''' Consulta Saldo Ação. Listar saldo acao acoes Vs evento em aberto
    ''' </summary>
    ''' <param name="vo"></param>
    ''' <returns></returns>
    Public Function ListarSaldoAcaoEventoEmAberto(vo As VO.AcaoComercial) As List(Of VO.AcaoComercial)

        Return DirectCast(daoBase, AcaoComercialDAO).ListarSaldoAcaoEventoEmAberto(vo)

    End Function

    ''' <summary>
    ''' bscDdoAcoCmc
    ''' Consulta Gasto por Evento.
    ''' </summary>
    ''' <param name="vo"></param>
    ''' <returns></returns>
    Public Function ListarGastoAcaoEvento(vo As VO.AcaoComercial) As List(Of VO.AcaoComercial)

        Return DirectCast(daoBase, AcaoComercialDAO).ListarGastoAcaoEvento(vo)

    End Function

#End Region

#Region "Saldo Ação x Fornecedor"

    ''' <summary>
    ''' bscDdoAcoCmc
    ''' Consulta Saldo Ação. Listar saldo acao acoes Vs evento em aberto
    ''' </summary>
    ''' <param name="vo"></param>
    ''' <returns></returns>
    Public Function ListarSaldoAcaoFornecedor(vo As VO.AcaoComercial) As List(Of VO.AcaoComercial)

        Return DirectCast(daoBase, AcaoComercialDAO).ListarSaldoAcaoFornecedor(vo)

    End Function

    ''' <summary>
    ''' ListarSaldoAcaoFornecedor
    ''' Consulta Fornecedores x Acao. Listar relacao de fornecedores com ação para carregar combo de fornecedores na modal 
    ''' </summary>
    ''' <returns></returns>
    Public Function listaRelacaoFornecedoresAcao(codFrn As Decimal?) As List(Of VO.AcaoComercial)

        Return DirectCast(daoBase, AcaoComercialDAO).listaRelacaoFornecedoresAcao(codFrn)

    End Function

    ''' <summary>
    ''' ListarSaldoAcaoFornecedor
    ''' Consulta Celulas x Acao Fornecedor. Listar relacao de celulas para carregar combo de fornecedores na modal 
    ''' </summary>
    ''' <param name="codFrn"></param>
    ''' <returns></returns>
    Public Function listaCelula(codFrn As Decimal?) As List(Of VO.AcaoComercial)

        Return DirectCast(daoBase, AcaoComercialDAO).listaCelula(codFrn)

    End Function

    Public Function AtuStaAcoCmc(ByVal CodAcoCmc As Decimal, ByVal intCodStaApv As Decimal) As Object

        Return DirectCast(daoBase, AcaoComercialDAO).AtuStaAcoCmc(CodAcoCmc, intCodStaApv)

    End Function


    ''' <summary>
    ''' função para atualizar valores do fornecedor em ações de pre acordo
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AtualizaValorFornecedorPreAcordo(acao As AcaoComercialViewModel)

        For Each fornecedor In acao.Fornecedores

            fornecedor.VLRUTZEXACTTACOCMC = 0
            fornecedor.VLRUTZEXACTTACOCMC = acao.ValorTotal
            'For Each item In acao.ItensOrcamento
            '    '   If item.CODFRNPCPMER = fornecedor.CODFRN Then
            '    fornecedor.VLRUTZEXACTTACOCMC += item.ValorTotal
            '    'fornecedor.VLRUTZEXACTTACOCMC += item.VLRUNTITEOCDACOCMC
            '    '  End If
            'Next
            CalculaValorDadosFornecedores(fornecedor)
        Next
    End Sub

    Private Sub CalculaValorDadosFornecedores(ByRef fornecedor As RelacaoAcaoComercialxFornecedoresViewModel)
        Dim VlrTot As Decimal = 0
        Dim TxaAdm As Decimal = 0
        fornecedor.VLRPTCFRNACOCMC = 0
        VlrTot = fornecedor.VLRUTZCTTACOCMC + fornecedor.VLRUTZEXACTTACOCMC + fornecedor.VLRUTZRCTCSTMER

        'Se for despesas martins não há taxa de adm. e o valor total é o valor da participação
        'Todo: buscar taxa de administrativa de parametro
        If (fornecedor.CODFRN <> Constantes.DESPESAS_MARTINS) Then
            TxaAdm = VlrTot * (0 / 100) 'Calcula a taxa de administração
        End If
        fornecedor.VLRPTCFRNACOCMC = VlrTot - TxaAdm  'Mostra o Valor total da Participação do fornecedor
        fornecedor.VLRTXAADMFRNACOCMC = TxaAdm
    End Sub

#End Region

#Region "Clone"

    Public Function CriarClone(ByVal codAcao As Decimal?, ByVal IndItemOrcamento As Boolean, ByVal IndMecanica As Boolean, ByVal CodFnc As Decimal) As VO.AcaoComercial
        Dim acao = BuscarAcaoComercialPorId(codAcao)
        If Not (acao Is Nothing) Then

            'Dim boRelacaoAcaoComercialPorItemOrcamento As New RelacaoAcaoComercialPorItemOrcamentoBO()
            'Dim boRelacaoItemPrincipalItemSubstitutoAcaoComercial As New RelacaoItemPrincipalItemSubstitutoAcaoComercialBO

            '-------------------------------------------------------------------------------------------------------------------------------------

            Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
            Dim relacaoAcaoMecanicaList As List(Of VO.RelacaoAcaoMecanica) = BO.ListT(New VO.RelacaoAcaoMecanica() With {.CODACOCMC = codAcao}).OrderBy(Function(r) r.NUMSEQRGRACOCMC).ToList()

            If (relacaoAcaoMecanicaList.Count > 0) Then

                acao.relacaoAcaoMecanicaList = New List(Of VO.RelacaoAcaoMecanica)()
                For Each item In relacaoAcaoMecanicaList
                    'Listar Dados Gerais da Mecanica
                    item = BO.SelectEditMecanicaGerais(item.CODACOCMC, item.NUMSEQRGRACOCMC)

                    'Listar Filiais Selecionadas por (CODACOCMC, NUMSEQRGRACOCMC)
                    Dim relacaoAcaoMecanicaFilialDAO = New RelacaoAcaoMecanicaFilialDAO()
                    item.RelacaoAcaoMecanicaFilialList = relacaoAcaoMecanicaFilialDAO.ListarFiliaisSelecionadas(item.CODACOCMC, item.NUMSEQRGRACOCMC)

                    'Verificar Tipo da Promoção para obter Criterio de Apuração
                    If (item.TIPPMC IsNot Nothing Or 0) Then
                        'Listar Dados Criterio de apuração se tipo da promoção for "NORMAL"
                        Dim BOCriterioDeApuracaoNormal = New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
                        item.RelacaoAcaoMecanicaCriterioDeApuracaoList = BOCriterioDeApuracaoNormal.EditRelacaoAcaoMecanicaCriterioDeApuracaoList(item.CODACOCMC, item.NUMSEQRGRACOCMC, item.TIPPMC)
                    ElseIf (item.TIPPMC IsNot Nothing Or 1) Then
                        'Listar Dados Criterio de apuração se tipo da promoção for "COMBINADA"
                        Dim BOCriterioDeApuracaoCombinada = New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
                        item.RelacaoAcaoMecanicaCriterioDeApuracaoList = BOCriterioDeApuracaoCombinada.EditRelacaoAcaoMecanicaCriterioDeApuracaoList(item.CODACOCMC, item.NUMSEQRGRACOCMC, item.TIPPMC)
                    ElseIf (item.TIPPMC IsNot Nothing Or 2) Then
                        'Listar Dados Criterio de apuração se tipo da promoção for "FAIXA"
                        Dim BOCriterioDeApuracaoFaixa = New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
                        item.RelacaoAcaoMecanicaCriterioDeApuracaoList = BOCriterioDeApuracaoFaixa.EditRelacaoAcaoMecanicaCriterioDeApuracaoList(item.CODACOCMC, item.NUMSEQRGRACOCMC, item.TIPPMC)
                    End If

                    'Publico Alvo
                    Dim relacaoAcaoMecanicaPublicoAlvoBO As RelacaoAcaoMecanicaPublicoAlvoBO = New RelacaoAcaoMecanicaPublicoAlvoBO()
                    Dim relacaoAcaoMecanicaPublicoAlvoObj As VO.RelacaoAcaoMecanicaPublicoAlvo = relacaoAcaoMecanicaPublicoAlvoBO.SelectCountPublicoAlvo(item.CODACOCMC, item.NUMSEQRGRACOCMC)


                    If (relacaoAcaoMecanicaPublicoAlvoObj.INDESTADO >= 1) Then
                        Dim DAO As RelacaoAcaoMecanicaPublicoAlvoEstadoDAO = New RelacaoAcaoMecanicaPublicoAlvoEstadoDAO()
                        item.relacaoAcaoMecanicaPublicoAlvoEstadoList = DAO.ListarEstadoUniaoSelecionados(item.CODACOCMC, item.NUMSEQRGRACOCMC)
                    End If

                    If (relacaoAcaoMecanicaPublicoAlvoObj.INDATIVIDADE >= 1) Then
                        Dim DAO As RelacaoAcaoMecanicaPublicoAlvoAtividadeDAO = New RelacaoAcaoMecanicaPublicoAlvoAtividadeDAO()
                        item.relacaoAcaoMecanicaPublicoAlvoAtividadeList = DAO.ListarAtividadesSelecionadas(item.CODACOCMC, item.NUMSEQRGRACOCMC)
                    End If

                    If (relacaoAcaoMecanicaPublicoAlvoObj.INDGRUPAMENTOCLIENTE >= 1) Then
                        Dim DAO As RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteDAO = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteDAO()
                        item.relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList = DAO.ListarGrupamentoClienteSelecionadas(item.CODACOCMC, item.NUMSEQRGRACOCMC)
                    End If

                    If (relacaoAcaoMecanicaPublicoAlvoObj.INDNEGOCIOCLIENTE) Then
                        Dim DAO As RelacaoAcaoMecanicaPublicoAlvoNegocioClienteDAO = New RelacaoAcaoMecanicaPublicoAlvoNegocioClienteDAO()
                        item.relacaoAcaoMecanicaPublicoAlvoNegocioClienteList = DAO.ListarTipoNegocioClienteSelecionado(item.CODACOCMC, item.NUMSEQRGRACOCMC)
                    End If

                    If (relacaoAcaoMecanicaPublicoAlvoObj.INDSEGMENTOCLIENTE) Then
                        Dim DAO As RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteDAO = New RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteDAO()
                        item.relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList = DAO.ListaTipoSegmentoClienteSelecionado(item.CODACOCMC, item.NUMSEQRGRACOCMC)
                    End If

                    If (relacaoAcaoMecanicaPublicoAlvoObj.INDCLIENTE) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoClienteBO = New RelacaoAcaoMecanicaPublicoAlvoClienteBO()
                        item.relacaoAcaoMecanicaPublicoAlvoClienteList = relacaoAcaoMecanicaPublicoAlvoClienteBO.ListarPublicoAlvoCliente(New VO.RelacaoAcaoMecanicaPublicoAlvoCliente() With {.CODACOCMC = item.CODACOCMC, .NUMSEQRGRACOCMC = item.NUMSEQRGRACOCMC})
                    End If

                    If (relacaoAcaoMecanicaPublicoAlvoObj.INDRCAVENDEDOR >= 1) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoRcaVendedorBO = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorBO()
                        item.relacaoAcaoMecanicaPublicoAlvoRcaVendedorList = relacaoAcaoMecanicaPublicoAlvoRcaVendedorBO.ListarPublicoAlvoRcaVendedor(New VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor() With {.CODACOCMC = item.CODACOCMC, .NUMSEQRGRACOCMC = item.NUMSEQRGRACOCMC})
                    End If

                    If (relacaoAcaoMecanicaPublicoAlvoObj.INDGERENTEVENDAS >= 1) Then
                        Dim DAO As RelacaoAcaoMecanicaPublicoAlvoGerenteVendasDAO = New RelacaoAcaoMecanicaPublicoAlvoGerenteVendasDAO()
                        item.relacaoAcaoMecanicaPublicoAlvoGerenteVendasList = DAO.ListarGerenteVendasSelecionados(item.CODACOCMC, item.NUMSEQRGRACOCMC)
                    End If

                    If (relacaoAcaoMecanicaPublicoAlvoObj.INDGERENTEMERCADO >= 1) Then
                        Dim relacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO = New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO()
                        item.relacaoAcaoMecanicaPublicoAlvoGerenteMercadoList = relacaoAcaoMecanicaPublicoAlvoGerenteMercadoBO.ListarPublicoAlvoGerenteMercado(New VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado() With {.CODACOCMC = item.CODACOCMC, .NUMSEQRGRACOCMC = item.NUMSEQRGRACOCMC})
                    End If

                    'Clientes Excludentes
                    Dim relacaoAcaoMecanicaClientesExcludentesBO = New RelacaoAcaoMecanicaClientesExcludentesBO()
                    item.relacaoAcaoMecanicaClientesExcludentesList = relacaoAcaoMecanicaClientesExcludentesBO.ListT(New VO.RelacaoAcaoMecanicaClientesExcludentes() With {.CODACOCMC = item.CODACOCMC, .NUMSEQRGRACOCMC = item.NUMSEQRGRACOCMC})

                    'Mix
                    Dim relacaoAcaoMecanicaMixBO = New RelacaoAcaoMecanicaMixBO()
                    item.relacaoAcaoMecanicaMixList = relacaoAcaoMecanicaMixBO.ListT(New VO.RelacaoAcaoMecanicaMix() With {.CODACOCMC = item.CODACOCMC, .NUMSEQRGRACOCMC = item.NUMSEQRGRACOCMC})

                    'adicionar item na lista
                    acao.relacaoAcaoMecanicaList.Add(item)
                Next





            End If

            '-------------------------------------------------------------------------------------------------------------------------------------
            acao.CODACOCMCCPI = codAcao
            acao.CODSTAAPVACOCMC = Constantes.Status.NOVA
            acao.CODACOCMC = Nothing
            acao.DATINIACOCMC = Date.Now
            acao.DATFIMACOCMC = Date.Now
            acao.CODFNCCRIACOCMC = CodFnc

            acao.Fornecedores = New List(Of VO.RelacaoAcaoComercialxFornecedores)
            acao.Carimbos = New List(Of VO.RelacaoAcaoxFornecedorxCarimbo)
            acao.Premiacao = New List(Of VO.RelacaoAcaoComercialxPremiacaoExclusiva)
            acao.Bonificacoes = New List(Of VO.RelacaoAcaoComercialXPedidoBonificado)



            If Not (IndItemOrcamento) Then
                acao.ItensOrcamento = New List(Of VO.RelacaoAcaoComercialPorItemOrcamento)
                acao.ItensSubstituidos = New List(Of VO.RelacaoAcaoComercialPorItemOrcamento)
            End If
            If Not (IndMecanica) Then
                acao.Mecanica = New VO.RelacaoAcaoComercialxMecanicaDaAcao()
            End If

            For Each item In acao.ItensOrcamento
                Dim novoValorUnitario = 0.0

                If item.INDTIPOPEDSNACOCMC = 1 Then
                    If item.CODFILEMP <> 0 Then
                        novoValorUnitario = ConsultaPrecoUnitarioItem(item.CODMER, item.CODFILEMP, acao)
                    End If
                Else
                    If Not (item.INDTIPOPEREQVGM = 1 Or item.INDTIPOPEREQEVT = 1 Or item.INDTIPOPESLCPGT = 1) Then
                        Dim boMercadoria = New MercadoriaBO()

                        Dim mercadoria As Object = New With {.CodUndNgc = 1, .CodFilEmp = "143", .CodUndOpe = "8",
                                                            .CodMerRsu = item.CODMER, .desmer = ""}

                        Dim retMer = Mapper.Map(Of List(Of VO.Mercadoria), List(Of MercadoriaViewModel))(boMercadoria.PsqIteCsm(mercadoria))

                        If (retMer.Count > 0) Then
                            novoValorUnitario = retMer(0).VLRPCO
                        End If
                    End If
                End If

                'Dim qtd = item.QDEITEOCDACOCMC
                'Dim novoValorTotal As Double = novoValorUnitario * qtd
                'Dim novoValorTotalRounded = Math.Round(novoValorTotal, 4)

                item.VLRUNTITEOCDACOCMC = CType(novoValorUnitario, Decimal)
                'item.VLRUNTITEOCDACOCMC = CType(novoValorTotalRounded, Decimal)

                If (item.VLRPTCCLIITEACOCMC <> 0 Or item.VLRPTCFRNITEACOCMC <> 0) Then

                    If (item.VLRPTCCLIITEACOCMC > novoValorUnitario) Then
                        item.VLRPTCCLIITEACOCMC = novoValorUnitario
                    End If

                    Dim novoValorFornecedor As Double = novoValorUnitario - item.VLRPTCCLIITEACOCMC
                    Dim novoValorFornecedorRounded As Double = Math.Round(novoValorFornecedor, 4)

                    item.VLRPTCFRNITEACOCMC = novoValorFornecedorRounded
                End If

                item.VLRUTZOPEDSNACOCMC = 0
            Next

            acao.CODACOCMC = SalvarAcaoComercial(acao, True, codAcao, IndItemOrcamento).CODACOCMC
        End If

        Return acao

    End Function

    Public Function ConsultaPrecoUnitarioItem(codMercadoria As Integer, codFilial As Integer, acao As VO.AcaoComercial) As Decimal
        Dim boMovimentoQuinzenal As New MovimentoQuinzenalPrecoMercadoriaBO

        Dim movimentos As List(Of MovimentoQuinzenalPrecoMercadoriaViewModel) = Mapper.Map(Of List(Of VO.MovimentoQuinzenalPrecoMercadoriaVO), List(Of MovimentoQuinzenalPrecoMercadoriaViewModel))(boMovimentoQuinzenal.ConsultaPrecoUnitarioItem(codMercadoria, codFilial, acao.TIPACOMCD))
        If movimentos.Count > 0 Then
            Dim ValIte = movimentos.FirstOrDefault.VLRCSTUNTMER
            If Trim(movimentos.FirstOrDefault.FLGMERPMC) = "S" Then
                Return ValIte
            Else
                If (acao.TIPACOMCD = TipoAcao.PreAcordoVigencia Or acao.TIPACOMCD = TipoAcao.PreAcordoValor) Then
                    ValIte = ValIte
                Else
                    ValIte = ValIte / 0.78
                End If
                Return ValIte
            End If
        Else
            Return 0.0
        End If
    End Function
#End Region

#Region "Fluxo"

    ''' <summary>
    ''' Buscar dados da acao comercial pelo id da acao e codigo do fornecedor usuario de rede
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function BuscarAcaoComercialfluxo(idAcao As Integer, Optional codFnrUsrRede As Integer? = Nothing) As VO.AcaoComercial

        Dim politicasBO As New PoliticasDaAcaoComercialBO
        Dim daoAcao As AcaoComercialDAO = DirectCast(daoBase, AcaoComercialDAO)
        'Busca dados da ação - DdoAco
        Dim acaoComercial = daoAcao.BuscarAcaoComercialPorId(idAcao)
        If Not (acaoComercial Is Nothing) Then

            'fazer buscar por id
            acaoComercial = BuscarAcaoComercialPorId(idAcao)

            'Busca politicas divergentes da ação
            acaoComercial.PoliticasDivergentes = politicasBO.ListPolitcasAcao(idAcao)


            'Busca informações do plano de marketing - DdoPlnMkt
            acaoComercial.PlanosDeMarketing = boRelacaoPlanoMarketing.BuscarPlanosMarketingDaAcaofluxo(idAcao)

            'Busca fornecedores da ação
            acaoComercial.Fornecedores = boFornecedores.BuscarFornecedoresDaAcaoFluxo(idAcao)

            'Busca os incentivos da acao
            acaoComercial.Incentivos = boIncentivo.BuscarIncentivosDaAcao(idAcao)

            'Busca o Fluxo            
            acaoComercial.FluxosAprovacao = boFluxoAprovacao.BuscaFluxosAprovacaoDaAcao(idAcao)
        End If
        Return acaoComercial

    End Function


    Public Sub FunAtuApvAcoCmc(ByVal dCodAcoCmc As Double,
                                    ByVal dCodFncApvAtuAcoCmc As Double,
                                    ByVal iStaAcoCmc As Short,
                                    ByVal CodFormPag As Decimal,
                                    ByVal NumDias As Decimal)
        If (CodFormPag <> 0) Then
            DirectCast(daoBase, AcaoComercialDAO).FunAtuApvAcoCmc(dCodAcoCmc, dCodFncApvAtuAcoCmc, iStaAcoCmc, CodFormPag, NumDias)
        End If
        'Se estiver sendo aprovada definitivamente, então grava os dados na T0138008
        If iStaAcoCmc = 5 Then
            FunAtuPlnMktApvAcoCmc(dCodAcoCmc)
            'Incentivo()
            FunInsDdoOpeDsn(dCodAcoCmc)
        End If
    End Sub
    Private Sub FunInsDdoOpeDsn(ByVal dCodAcoCmc As Double)
        Dim _RelacaoAcaoComercialPorOperacaoDeDestinoBO As New RelacaoAcaoComercialPorOperacaoDeDestinoBO
        Dim itensOrcamento As List(Of VO.RelacaoAcaoComercialPorItemOrcamento) = boItensOrcamento.FunInsDdoOpeDsn(dCodAcoCmc)

        If (itensOrcamento.Count > 0) Then

            For Each item In itensOrcamento
                If item.INDTIPOPEREQVGM = 1 Or item.INDTIPOPESLCPGT = 1 Or item.INDTIPOPEREQEVT = 1 Then

                    Dim filtro As New VO.MTS_RT_Type_Expenses_All()
                    filtro.TYPE_EXPENSE_ID = item.CODMER
                    If item.INDTIPOPEREQVGM = 1 Then
                        filtro.ATTRIBUTE1 = "AC"
                    End If
                    If item.INDTIPOPESLCPGT = 1 Then
                        filtro.ATTRIBUTE2 = "AC"
                    End If
                    If item.INDTIPOPEREQEVT = 1 Then
                        filtro.ATTRIBUTE3 = "AC"
                    End If


                    Dim mts_RT_Type_Expenses_AllBO As New MTS_RT_Type_Expenses_AllBO
                    Dim TYPE_EXPENSE_CODE As String = mts_RT_Type_Expenses_AllBO.SelectAttributes(filtro).TYPE_EXPENSE_CODE


                    Dim newobj As New VO.MTS_RT_Orc_Budge_Actions_All
                    newobj.ACTIONS_NUMBER = item.CODACOCMC
                    If item.INDTIPOPEREQVGM = 1 Then
                        newobj.ACTIONS_DESTINATION_TYPE = "CDP"
                    ElseIf item.INDTIPOPESLCPGT = 1 Then
                        newobj.ACTIONS_DESTINATION_TYPE = "SP"
                    ElseIf item.INDTIPOPEREQEVT = 1 Then
                        newobj.ACTIONS_DESTINATION_TYPE = "RE"
                    End If
                    newobj.TYPE_EXPENSE_CODE = TYPE_EXPENSE_CODE
                    newobj.BUDGE_AMOUNT_FORECASTED = item.VLRUNTITEOCDACOCMC


                    Dim mts_RT_Orc_Budge_Actions_AllBO As New MTS_RT_Orc_Budge_Actions_AllBO
                    mts_RT_Orc_Budge_Actions_AllBO.Insert(newobj)

                End If

                Dim newObjRelac = New VO.RelacaoAcaoComercialPorOperacaoDeDestino With {
                                                                    .CODACOCMC = item.CODACOCMC,
                                                                    .CODOPEFSCDSNACOCMC = item.CODOPEFSCDSNACOCMC,
                                                                    .VLROPEFSCDSNACOCMC = item.VLRUNTITEOCDACOCMC,
                                                                    .VLRUTZOPEDSNACOCMC = item.VLRUTZOPEDSNACOCMC,
                                                                    .QDEITEOPEDSNACOCMC = item.QDEITEOCDACOCMC,
                                                                    .QDEUTZOPEDSNACOCMC = item.QDEUTZOPEDSNACOCMC,
                                                                    .CODUNDESRNGCOPEDSN = item.CODUNDESRNGCOPEDSN
                                                                                    }

                _RelacaoAcaoComercialPorOperacaoDeDestinoBO.InsertUpdate(newObjRelac)
            Next
        End If

    End Sub

    ''' <summary>
    ''' Atualiza o Plano de marketing Quando a ação é aprovada
    ''' </summary>
    ''' <param name="CODACOCMC"></param>
    Public Sub FunAtuPlnMktApvAcoCmc(ByVal CODACOCMC As Decimal)
        Dim boRelacaoAcaoComercialxPlanoMarketing As New RelacaoAcaoComercialxPlanoMarketingBO()
        Dim boPlanoDeMarketing As New PlanoDeMarketingBO()

        Dim relacPlanoMktList As List(Of VO.RelacaoAcaoComercialxPlanoMarketing) = boRelacaoAcaoComercialxPlanoMarketing.ListarPlanoMktAcaoParaProvacao(CODACOCMC)

        If (relacPlanoMktList IsNot Nothing AndAlso relacPlanoMktList.Count > 0) Then

            'Verifica se a ação possui ação de acrescimo associada para os fornecedores da acao
            If (relacPlanoMktList.FirstOrDefault().QTD > 0) Then
                Dim acao As VO.AcaoComercial = Me.BuscaAcaoPorId(CODACOCMC)
                DirectCast(daoBase, AcaoComercialDAO).UpdateDtAprovacao(acao.CODACOCMC, acao.DATAPVACOCMC)
                DirectCast(daoBase, AcaoComercialDAO).UpdateDtIniVgr(acao.CODACOCMC, acao.DATAPVACOCMC, acao.DATAPVACOCMC.GetValueOrDefault().AddDays(1), acao.DATAPVACOCMC.GetValueOrDefault().AddDays(1).AddDays(DateDiff(DateInterval.Day, acao.DATINIACOCMC.GetValueOrDefault(), acao.DATFIMACOCMC.GetValueOrDefault())))
            End If

            For Each item As VO.RelacaoAcaoComercialxPlanoMarketing In relacPlanoMktList
                boRelacaoAcaoComercialxPlanoMarketing.UpdatePlanoMktAprovacaoAcao(item.CODACOCMC, item.TIPEVTACOCMC, item.CODEVTACOCMC, item.CODUNDESRNGC, item.ANOMESREF)
                boPlanoDeMarketing.UpdatePlanoMktAprovacaoAcao(item.CODACOCMC, item.TIPEVTACOCMC, item.CODEVTACOCMC, item.CODUNDESRNGC, item.ANOMESREF)
            Next
        End If
    End Sub

    Private Sub Incentivo()
        'Incentivo
        'sStrCmd = " SELECT DISTINCT B.CODICT, B.DATINICADICT, B.DATFIMCADICT, A.CODACOCMC, B.CODSTAICT, B.DATINIEFTICT, B.DATFIMEFTICT "
        'sStrCmd = sStrCmd & "  FROM MRT.T0163177 A, MRT.T0160461 B "
        'sStrCmd = sStrCmd & " WHERE B.CODICT = A.CODICT "
        'sStrCmd = sStrCmd & "   AND A.CODACOCMC = " & dCodAcoCmc
        ''
        ''Acesso ao Banco de Dados
        'oAcsDdo = New IAU013.UO_IAUAcsDdo(oObeCnx, sStrCmd)
        'oAcsDdo.ExcCmdSql(rsDdo)
        ''
        'If Not IsNothing(rsDdo) Then
        '    If rsDdo.Tables.Count > 0 Then
        '        If rsDdo.Tables(0).Rows.Count > 0 Then
        '            '
        '            Cont = 0
        '            '
        '            For Each drDdo As DataRow In rsDdo.Tables(0).Rows
        '                'OBTEM NUMPODREFAPUICT
        '                sStrCmd = "SELECT MIN(NUMPODREFAPUICT) ""NUMPODREFAPUICT"" "
        '                sStrCmd = sStrCmd & "  FROM MRT.T0161409 "
        '                sStrCmd = sStrCmd & " WHERE CODICT = " & drDdo("CODICT")
        '                '
        '                'Acesso ao Banco de Dados
        '                oAcsDdo = New IAU013.UO_IAUAcsDdo(oObeCnx, sStrCmd)
        '                oAcsDdo.ExcCmdSql(rsDdoIct)
        '                '
        '                If drDdo("CODSTAICT") = 2 Then
        '                    If CDate(drDdo("DATINICADICT")) < Today And Today < CDate(drDdo("DATFIMCADICT")) Then
        '                        DatIniEftIct = Format(Today, "yyyy-MM-dd")
        '                        DatFimEftIct = Format(drDdo("DATFIMCADICT"), "yyyy-MM-dd")
        '                        CodStaIct = 3
        '                    Else
        '                        '
        '                        DatIniEftIct = Format(drDdo("DATINICADICT"), "yyyy-MM-dd")
        '                        DatFimEftIct = Format(drDdo("DATFIMCADICT"), "yyyy-MM-dd")
        '                        '
        '                        If Today > CDate(drDdo("DATFIMCADICT")) Then
        '                            CodStaIct = 7
        '                        Else
        '                            CodStaIct = 3
        '                        End If
        '                    End If
        '                Else
        '                    CodStaIct = drDdo("CODSTAICT")
        '                    DatIniEftIct = Format(drDdo("DATINIEFTICT"), "yyyy-MM-dd")
        '                    DatFimEftIct = Format(drDdo("DATFIMEFTICT"), "yyyy-MM-dd")
        '                End If
        '                '
        '                'ATUALIZA TABELA T0160461
        '                sStrCmd = " UPDATE MRT.T0160461 "
        '                sStrCmd = sStrCmd & " SET CODSTAICT = " & CodStaIct
        '                sStrCmd = sStrCmd & " ,DATINIEFTICT = to_date('" & DatIniEftIct & "','yyyy-MM-dd')"
        '                sStrCmd = sStrCmd & " ,DATFIMEFTICT = to_date('" & DatFimEftIct & "','yyyy-MM-dd')"
        '                sStrCmd = sStrCmd & " WHERE CODICT = " & drDdo("CODICT")


        '                '
        '                'Acesso ao Banco de Dados
        '                oAcsDdo = New IAU013.UO_IAUAcsDdo(oObeCnx, sStrCmd)
        '                oAcsDdo.ExcCmdSql(iInt)
        '                '
        '                'ATUALIZA TABELA T0161409
        '                sStrCmd = " UPDATE MRT.T0161409 "
        '                sStrCmd = sStrCmd & " SET DATINIPODAPUICT = to_date('" & DatIniEftIct & "','yyyy-MM-dd')"
        '                sStrCmd = sStrCmd & " WHERE CODICT = " & drDdo("CODICT")
        '                sStrCmd = sStrCmd & " AND NUMPODREFAPUICT = " & rsDdoIct.Tables(0).Rows(0)("NUMPODREFAPUICT")
        '                '
        '                'Acesso ao Banco de Dados
        '                oAcsDdo = New IAU013.UO_IAUAcsDdo(oObeCnx, sStrCmd)
        '                oAcsDdo.ExcCmdSql(iInt)
        '                '
        '            Next
        '            '
        '        End If
        '    End If
        'End If
    End Sub


    Public Function AtuStaAcoCmc(ByVal dCodAcoCmc As Double,
                                ByVal iStaAcoCmc As Short) As Integer
        Return DirectCast(daoBase, AcaoComercialDAO).AtuStaAcoCmc(dCodAcoCmc, iStaAcoCmc)

    End Function

    Public Function PcdAtuAldRsvFrn(ByVal vCodIdtTipAco As Integer,
                                    ByVal vCodFrn As Integer,
                                    ByVal vCodAcoCmc As String,
                                    ByVal vTipDsnDscBnf As Integer,
                                    ByVal vVlrSldRsvFrn As Double,
                                    ByVal vFlgApvAcoCmc As String,
                                    ByVal CODUNI As String,
                                    ByVal sVlrTxaAdm As String,
                                    ByVal sVlrTxaBde As String) As Integer

        Return DirectCast(daoBase, AcaoComercialDAO).PcdAtuAldRsvFrn(vCodIdtTipAco, vCodFrn, vCodAcoCmc, vTipDsnDscBnf, vVlrSldRsvFrn, vFlgApvAcoCmc, CODUNI, sVlrTxaAdm, sVlrTxaBde)

    End Function

    Public Function AtuFormaPagNumDiasFuncGAC(ByVal CodAcao As Decimal,
                                              ByVal CodFormPag As Decimal,
                                              ByVal NumDias As Decimal) As Integer

        Return DirectCast(daoBase, AcaoComercialDAO).AtuFormaPagNumDiasFuncGAC(CodAcao, CodFormPag, NumDias)

    End Function

    ''' <summary>
    ''' Busca numero a acão comercial para a promoção
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    ''' <returns></returns>
    Public Function SelectAcaoComercialParaPromocao(acaoComercial As VO.AcaoComercial) As VO.AcaoComercial

        Return DirectCast(daoBase, AcaoComercialDAO).SelectAcaoComercialParaPromocao(acaoComercial)

    End Function

#End Region

End Class