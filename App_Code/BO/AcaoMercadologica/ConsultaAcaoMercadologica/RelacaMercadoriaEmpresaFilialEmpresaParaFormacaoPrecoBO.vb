﻿Imports Core

Public Class RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoBO
    Inherits BOBase(Of VO.RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoVO)

    Public Sub New()
        MyBase.daoBase = New RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoDAO()
    End Sub

    Public Function ValidaProdutoFracionado(codFilial As Decimal, codEmp As Decimal) As List(Of VO.RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoVO)

        Return DirectCast(daoBase, RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoDAO).ValidaProdutoFracionado(codFilial, codEmp)

    End Function

End Class
