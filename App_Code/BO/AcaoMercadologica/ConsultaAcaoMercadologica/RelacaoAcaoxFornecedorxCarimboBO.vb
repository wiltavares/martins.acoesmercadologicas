﻿Imports Core
Public Class RelacaoAcaoxFornecedorxCarimboBO
    Inherits BOBase(Of VO.RelacaoAcaoxFornecedorxCarimbo)

    Private boAcaoComercial As AcaoComercialBO
    Private boRelacaoPlanoMarketing As RelacaoAcaoComercialxPlanoMarketingBO
    Private boFornecedores As RelacaoAcaoComercialxFornecedoresBO
    Private boMecanicaAcao As RelacaoAcaoComercialxMecanicaDaAcaoBO
    Private boIncentivo As RelacaoIncentivoxAcaoComercialBO
    Private boPoliticas As RelacaoAcaoComercialxPoliticaBO
    'Private boRelacaoComerForne As RelacaoAcaoComercialxFornecedoresBO


    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoxFornecedorxCarimboDAO
    End Sub

    Public Function ListarCarimbosDaAcaoComercial(idAcao As Decimal?) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Return DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).ListarCarimbosDaAcaoComercial(idAcao)

    End Function

    Public Function BuscarCarimbosPorFiltro(codFrn As Decimal?, codCelula As Decimal?, codCarimbo As Decimal?, Optional nomFrn As String = Nothing) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Return DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).BuscarCarimbosPorFiltro(codFrn, codCelula, codCarimbo, nomFrn)

    End Function

    Public Function PesquisarCarimbos(codFrn As Decimal?, codCelula As Decimal?, codCarimbo As Decimal?, nomFrn As String, flSaldo As Boolean) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)
        ', flSaldo As Boolean
        Return DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).PesquisarCarimbos(codFrn, codCelula, codCarimbo, nomFrn, flSaldo)

    End Function

    Public Function ConsultaRelacaoAcaoFornecedorCarimbo(idAcao As Decimal?) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Return DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).ConsultaRelacaoAcaoFornecedorCarimboDaAcao(idAcao)

    End Function

    Public Function DeletaRelacaoAcaoFornecedorCarimboDaAcao(idAcao As Decimal?) As Integer

        Return DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).DeletaRelacaoAcaoFornecedorCarimboDaAcao(idAcao)

    End Function

    Public Function InsereRelacaoAcaoFornecedorCarimboDaAcao(idAcao As Decimal?, acaoComercial As VO.AcaoComercial) As Integer
        Dim str As String = ""
        Return DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).InsereRelacaoAcaoFornecedorCarimboDaAcao(idAcao, str, acaoComercial)

    End Function

    Public Function ConsultaEstornoCarimbo(idAcao As Decimal?) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Return DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).ConsultaEstornoCarimbo(idAcao)

    End Function

    Public Function BuscarCarimbos(codCarimbo As Decimal?, nomTIPEVTACOCMC As String, nomEVTACOCMC As String, nomOBJ As String) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Return DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).BuscarCarimbos(codCarimbo, nomTIPEVTACOCMC, nomEVTACOCMC, nomOBJ)

    End Function

    Public Function ListConsulta(vo As VO.RelacaoAcaoxFornecedorxCarimbo) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Return DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).ListConsulta(vo)

    End Function

    ''' <summary>
    ''' InsDdoFrnCarimbo
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    Public Sub InserirDadosCarimbo(acaoComercial As VO.AcaoComercial)

        Try

            acaoComercial.Fornecedores = New List(Of VO.RelacaoAcaoComercialxFornecedores)

            For Each carimbo As VO.RelacaoAcaoxFornecedorxCarimbo In acaoComercial.Carimbos

                Dim valida = Not (acaoComercial.Fornecedores.Where(Function(f) f.CODFRN = carimbo.CODFRN).Count > 0)

                If valida Then

                    Dim novoCarimbo As New VO.RelacaoAcaoComercialxFornecedores
                    novoCarimbo.CODACOCMC = acaoComercial.CODACOCMC
                    novoCarimbo.CODFRN = carimbo.CODFRN
                    novoCarimbo.VLRPTCFRNACOCMC = acaoComercial.Carimbos.Sum(Function(f) f.VLRUTZACOCMC)
                    novoCarimbo.TIPDSNDSCBNF = acaoComercial.TIPDSNDSCBNF

                    novoCarimbo.TIPDSNDSCBNFEXACTT = 0
                    novoCarimbo.VLRTXAADMFRNACOCMC = 0
                    novoCarimbo.VLRSLDCNTCRRFRN = 0
                    novoCarimbo.VLRSLDDISACOFUTFRN = 0
                    novoCarimbo.VLRSLDDSNDSCACOCMC = 0
                    novoCarimbo.VLRTXAADMBDEACOCMC = 0
                    novoCarimbo.VLRUTZCTTACOCMC = 0
                    novoCarimbo.VLRUTZEXACTTACOCMC = 0
                    novoCarimbo.CODUNDESRNGC = 0
                    novoCarimbo.VLRSLDDSNCTTACOCMC = 0
                    novoCarimbo.DESBFCFRNACOCMC = " "
                    novoCarimbo.TIPDSNDSCBNFACR = 0
                    novoCarimbo.VLRSLDDSNACR = 0
                    novoCarimbo.VLRUTZRCTCSTMER = 0
                    novoCarimbo.DESSELFRN = " "

                    acaoComercial.Fornecedores.Add(novoCarimbo)
                    If (boFornecedores Is Nothing) Then
                        boFornecedores = New RelacaoAcaoComercialxFornecedoresBO()
                    End If
                    boFornecedores.InserirDadosFornecedoresBD(novoCarimbo)

                End If

            Next
        Catch ex As Exception
            Throw ex
        End Try


    End Sub

    ''' <summary>
    ''' InsDdoRlcAcoFrnMco
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    Public Sub InserirDadosRelacaoFornecedorCarimbo(acaoComercial As VO.AcaoComercial)
        Try
            For Each carimbo As VO.RelacaoAcaoxFornecedorxCarimbo In acaoComercial.Carimbos
                carimbo.CODACOCMC = acaoComercial.CODACOCMC
                DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).InserirDadosRelacaoFornecedorCarimbo(carimbo)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Verifica saldo do carimbo
    ''' VrfSaldo()
    ''' </summary>
    ''' <param name="carimbos"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function VerificaSaldo(carimbos As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)) As Boolean

        For Each carimbo As VO.RelacaoAcaoxFornecedorxCarimbo In carimbos
            Dim consultaCarimbo = DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).BuscarCarimbosPorFiltro(carimbo.CODMCOVBAFRN, 0, 0, "")
            If consultaCarimbo IsNot Nothing AndAlso consultaCarimbo.Count > 0 Then
                If consultaCarimbo(0).VALOR_DISPONIVEL < consultaCarimbo(0).VLRUTZACOCMC Then
                    Throw New MappedWarnException("O saldo disponível no carimbo (" & carimbo.CODMCOVBAFRN & ") é insuficiente")
                End If
            End If
        Next
        Return True

    End Function


    Public Sub EnviarAcoCmcCarimbo(acaoComercial As VO.AcaoComercial, ByVal icentivosHabilitado As Boolean, ByRef bBotaoPolitica As Boolean, ByVal benviar As Boolean, ByRef bDesabilitabotao As Integer, ByVal indPreAcordo As Boolean)
        Dim IndPol As Integer = 0
        Dim VlrTot As Decimal = 0
        Dim VlrTotOcd As Decimal = 0
        Dim codigoEvento As String = acaoComercial.CODEVTACOCMC
        Dim lstPmtPoliticaDivergente(0) As Object

        Dim IndErrAcoCmc = 0

        Dim itemfuncionario As VO.Funcionario
        Dim itemfuncionario2 As VO.Funcionario

        Dim boFuncionarios = New FuncionarioBO


        'atualiza o form de saldo do plano de marketing, calcula os saldos do plano de marketing (caso tenha algum fornecedor na grid)
        'If acaoComercial.Carimbos.Count > 0 Then
        '    If (boAcaoComercial Is Nothing) Then
        '        boAcaoComercial = New AcaoComercialBO()
        '    End If
        '    boAcaoComercial.AtualizaDadosPlanoDeMarketing(acaoComercial)
        'End If

        For j As Integer = 0 To acaoComercial.Carimbos.Count - 1
            If IsNothing(acaoComercial.Carimbos(j).VALOR_DISPONIVEL) Then
                Throw New MappedWarnException("É necessário informar o valor a utilizar dos carimbos!")
                Exit For
            End If
        Next

        boFornecedores = New RelacaoAcaoComercialxFornecedoresBO
        If boFornecedores.VerificaDadosAcao(acaoComercial, icentivosHabilitado, indPreAcordo) Then

            'Verifica se o valor orçado é igual ao valor gasto
            VlrTot = acaoComercial.ValorParticipacao 'valor de participação
            VlrTotOcd = acaoComercial.ValorTotal 'valor total orcamento


            'If bBotaoPolitica = True And acaoComercial.DESCSTA = "NOVA" Then
            '    'DelDdoAcoCmcPol(CInt(acaoComercial.CODACOCMC))
            '    boPoliticas.DeletarDadoAcaoComercialPoliticas(acaoComercial.CODACOCMC)
            '    'btPolAcoCmc.Visible = False
            'End If 'Botao politica

            AtuCadMcoVbaFrn(acaoComercial)

            'insere dados dos itens de orçamento
            If acaoComercial.ItensOrcamento.Count > 0 Then
                Dim gerente As New VO.Funcionario
                Dim codigo As Decimal
                If IsNothing(acaoComercial.confirmaEnviarGerente) Then
                    gerente = boFuncionarios.buscaGerenteDefault(acaoComercial.CODACOCMC, ControleAcesso.CodFnc())
                    If (gerente Is Nothing) Then
                        Throw New MappedWarnException("Não existe relacionamento entre o Funcionário x Gerente de Marketing! ")
                    Else
                        codigo = gerente.CODFNC
                    End If
                Else
                    codigo = acaoComercial.confirmaEnviarGerente
                End If
                itemfuncionario = boFuncionarios.selecionaGerente(codigo, "").FirstOrDefault()
                Dim funcfiltro As New VO.Funcionario
                funcfiltro.CODFNC = ControleAcesso.CodFnc()

                'itemfuncionario2 = boFuncionarios.SelectT(funcfiltro)

                'Busca o funcionário ou terceiro logado.
                itemfuncionario2 = boFuncionarios.BuscaFuncionariosETerceiros(funcfiltro)

                'Gera fluxo de aprovacao de acoes mercadologicas
                If Not boFornecedores.AtuFluApvAcoCmc(acaoComercial.CODACOCMC, acaoComercial.CODFNCCRIACOCMC,
                                                            itemfuncionario2.NOMFNC,
                                                            RelacaoAcaoComercialxFornecedoresBO.Programa.ACOES_MERCADOLOGICAS,
                                                            RelacaoAcaoComercialxFornecedoresBO.NumSeqAprovador.GER_CATEGORIA,
                                                            RelacaoAcaoComercialxFornecedoresBO.TipoUsuario.GER_CATEGORIA,
                                                            RelacaoAcaoComercialxFornecedoresBO.TipoUsuario.DIR_COMPRAS,
                                                            RelacaoAcaoComercialxFornecedoresBO.tipoFluxo.VALOR,
                                                            itemfuncionario.CODFNC,
                                                            itemfuncionario.NOMFNC,
                                                            indPreAcordo) Then
                    '
                    Throw New MappedWarnException("Erro: Os dados da ação foram gravados com sucesso, porém ocorreram erros na gravação do fluxo de aprovação. Por favor, tente enviar novamente esta ação, se essa mensagem persistir contacte o responsável.")
                Else
                    'Desabilita campos
                    bDesabilitabotao = 2
                End If

            End If
            InsDdoRlcAcoFrnMco(acaoComercial)
        End If

    End Sub

    Public Sub AtuCadMcoVbaFrn(acaoComercial As VO.AcaoComercial)
        For Each item In acaoComercial.Carimbos
            DirectCast(daoBase, RelacaoAcaoxFornecedorxCarimboDAO).AtuCadMcoVbaFrn(item.CODMCOVBAFRN, item.VLRUTZACOCMC)
        Next

    End Sub


    Private Sub InsDdoRlcAcoFrnMco(acaoComercial As VO.AcaoComercial)

        Dim dst As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)
        'Dim oObeSvcRdeRmt As New wsSvcRmtDK.SvcRmt
        'Dim LstPmt1(0) As Object
        Dim aux As Integer
        'LstPmt1(0) = Integer.Parse(txtCodAcoCmc.Text)
        'dst = oObeSvcRdeRmt.ChmAsbRmtGrpRgt("CBU298", "BO_CsnAcoCmcCrb", "CnsRlcAcoFrnMco", LstPmt1)
        dst = ConsultaRelacaoAcaoFornecedorCarimbo(acaoComercial.CODACOCMC)
        'If dst.Tables.Count > 0 Then
        '    If dst.Tables(0).Rows.Count > 0 Then
        If dst.Count > 0 Then
            aux = DeletaRelacaoAcaoFornecedorCarimboDaAcao(acaoComercial.CODACOCMC)
            '        aux = oObeSvcRdeRmt.ChmAsbRmt("CBU298", "BO_CsnAcoCmcCrb", "DelRlcAcoFrnMco", LstPmt1)
            '    End If
            'End If
        End If
        'Dim str As String = ""

        'Dim LstPmt(1) As Object
        'str = dstCarimbo.GetXml
        'LstPmt(0) = Integer.Parse(txtCodAcoCmc.Text)
        'LstPmt(1) = str
        aux = InsereRelacaoAcaoFornecedorCarimboDaAcao(acaoComercial.CODACOCMC, acaoComercial)
        'aux = oObeSvcRdeRmt.ChmAsbRmt("CBU298", "BO_CsnAcoCmcCrb", "InsDdoRlcAcoFrnMco", LstPmt)

    End Sub

    Private Function AtuFluApvAcoCmc()
        Return False
    End Function

End Class
