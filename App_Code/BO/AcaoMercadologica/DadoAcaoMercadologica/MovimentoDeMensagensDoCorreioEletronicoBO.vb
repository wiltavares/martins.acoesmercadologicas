﻿Imports Core
Public Class MovimentoDeMensagensDoCorreioEletronicoBO
    Inherits BOBase(Of VO.MovimentoDeMensagensDoCorreioEletronico)

    Public Sub New()
        MyBase.daoBase = New MovimentoDeMensagensDoCorreioEletronicoDAO()
    End Sub

    Public Function CnsPrxNumSeqCreEtn(ByVal TIPMSGCREETN As Integer) As Decimal?
        Try
            Return DirectCast(daoBase, MovimentoDeMensagensDoCorreioEletronicoDAO).CnsPrxNumSeqCreEtn(TIPMSGCREETN)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function IsrMsgCreEtn(ByVal TipMsgCreEtn As Decimal, ByVal NumSeqMsgCreEtn As Decimal, ByVal DesAssCreEtn As String) As Integer
        Try
            Return DirectCast(daoBase, MovimentoDeMensagensDoCorreioEletronicoDAO).IsrMsgCreEtn(TipMsgCreEtn, NumSeqMsgCreEtn, DesAssCreEtn)
        Catch ex As Exception
            Throw ex
        End Try

    End Function



End Class
