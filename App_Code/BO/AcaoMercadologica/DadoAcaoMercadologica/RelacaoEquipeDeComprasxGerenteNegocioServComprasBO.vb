﻿Imports Core
Public Class RelacaoEquipeDeComprasxGerenteNegocioServComprasBO
    Inherits BOBase(Of VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras)

    Public Sub New()
        MyBase.daoBase = New RelacaoEquipeDeComprasxGerenteNegocioServComprasDAO()
    End Sub

    ''' <summary>
    ''' Lista DESCRICAO DIVISAO DE COMPRA
    ''' </summary>
    ''' <param name="vo"></param>
    ''' <returns></returns>
    Public Function ListDescricaoDivisaoDeCompra(vo As VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras) As List(Of VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras)


        Return DirectCast(daoBase, RelacaoEquipeDeComprasxGerenteNegocioServComprasDAO).ListDescricaoDivisaoDeCompra(vo)

    End Function

End Class
