﻿Imports Core
Public Class UnidadeEstrategicaDeNegocioBO
    Inherits BOBase(Of VO.UnidadeEstrategicaDeNegocio)

    Public Sub New()
        MyBase.daoBase = New UnidadeEstrategicaDeNegocioDAO()
    End Sub

    Public Function BuscaUnidadeNegocioFornecedor(codFrn As Long) As List(Of VO.UnidadeEstrategicaDeNegocio)

        Return DirectCast(daoBase, UnidadeEstrategicaDeNegocioDAO).BuscaUnidadeNegocioFornecedor(codFrn)

    End Function

    Public Function BuscaTodosCodNomUnidadeNegocio() As List(Of VO.UnidadeEstrategicaDeNegocio)
        Return DirectCast(daoBase, UnidadeEstrategicaDeNegocioDAO).BuscaTodosCodNomUnidadeNegocio()
    End Function
End Class
