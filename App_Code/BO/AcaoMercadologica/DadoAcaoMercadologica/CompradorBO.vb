﻿Imports Core
Public Class CompradorBO
    Inherits BOBase(Of VO.Comprador)

    Public Sub New()
        MyBase.daoBase = New CompradorDAO()
    End Sub

    Public Function ListCompradoresDiretoria() As List(Of VO.Comprador)
        Return DirectCast(daoBase, CompradorDAO).ListCompradoresDiretoria()
    End Function

End Class
