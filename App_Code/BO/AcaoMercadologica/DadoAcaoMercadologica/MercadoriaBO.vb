﻿Imports Core
Public Class MercadoriaBO
    Inherits BOBase(Of VO.Mercadoria)

    Public Sub New()
        MyBase.daoBase = New MercadoriaDAO()
    End Sub

    Public Function VerificaProdutoVendidoFracionado(merc As Decimal, filial As Decimal) As List(Of VO.Mercadoria)
        Return DirectCast(daoBase, MercadoriaDAO).VerificaProdutoVendidoFracionado(merc, filial)
    End Function

    Public Function ConsultaItem(CODMER As Decimal, DESMER As String, CODFILEMP As Decimal, INDBDEOPEDSNACOCMC As Decimal, STRFRNACO As String) As List(Of VO.Mercadoria)
        Return DirectCast(daoBase, MercadoriaDAO).ConsultaItem(CODMER, DESMER, CODFILEMP, INDBDEOPEDSNACOCMC, STRFRNACO)
    End Function

    Public Function ConsultaItemGastoGeral(merc As Object) As List(Of VO.Mercadoria)
        Return DirectCast(daoBase, MercadoriaDAO).ConsultaItemGastoGeral(merc)
    End Function

    Public Function PsqIteCsm(merc As Object) As List(Of VO.Mercadoria)
        Return DirectCast(daoBase, MercadoriaDAO).PsqIteCsm(merc)
    End Function


    Public Function BuscaItensOrcamento(vo As VO.Mercadoria) As List(Of VO.Mercadoria)
        Return DirectCast(daoBase, MercadoriaDAO).BuscaItensOrcamento(vo)
    End Function

    Public Function BuscaMercadorias(vo As VO.Mercadoria) As List(Of VO.Mercadoria)
        Return DirectCast(daoBase, MercadoriaDAO).BuscaMercadorias(vo)
    End Function



    Public Function ListaItensSubstitutos(CODACOCMC As Decimal, CODMER As Decimal, NUMLNHITEOCDACOCMC As Decimal) As List(Of VO.Mercadoria)
        Return DirectCast(daoBase, MercadoriaDAO).ListaItensSubstitutos(CODACOCMC, CODMER, NUMLNHITEOCDACOCMC)
    End Function
End Class
