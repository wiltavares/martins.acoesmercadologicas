﻿Imports Core
Public Class RelacaoDeLinhasDaMensagemDoCorreioEletronicoBO
    Inherits BOBase(Of VO.RelacaoDeLinhasDaMensagemDoCorreioEletronico)

    Public Sub New()
        MyBase.daoBase = New RelacaoDeLinhasDaMensagemDoCorreioEletronicoDAO()
    End Sub

    Public Sub IsrLnhCreEtn(ByVal TipMsgCreEtn As Decimal, ByVal NumSeqMsgCreEtn As Decimal, ByVal NumSeqLnhFisCreEnt As Decimal, ByVal DesTxtLnhFisCreEnt As String)
        Dim item As New VO.RelacaoDeLinhasDaMensagemDoCorreioEletronico

        item.TIPMSGCREETN = TipMsgCreEtn
        item.NUMSEQMSGCREETN = NumSeqMsgCreEtn
        item.NUMSEQLNHFISCREETN = NumSeqLnhFisCreEnt
        item.DESTXTLNHFISCREETN = DesTxtLnhFisCreEnt

        Try
            DirectCast(daoBase, RelacaoDeLinhasDaMensagemDoCorreioEletronicoDAO).IsrLnhCreEtn(item)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function IsrMsgLnh(ByVal TipMsgCreEtn As Integer, _
                             ByVal iPrxNumSeqCreEtn As Integer, _
                             ByVal MsgEnv As String) As Integer
        Dim iAuxIni As Integer
        Dim sPedacoMsg As String
        Dim iAux As Integer
        '
        'Looping para gravar de 120 em 120 caracteres no banco = 1 linha do email
        iAux = (MsgEnv.Length / 120)
        iAuxIni = 0
        '
        While iAux > 0
            '
            If MsgEnv.Length <= 120 Then
                sPedacoMsg = MsgEnv
            ElseIf MsgEnv.Length > ((iAuxIni * 120) + 120) Then
                sPedacoMsg = MsgEnv.Substring((iAuxIni * 120), 120)
            Else
                sPedacoMsg = MsgEnv.Substring((iAuxIni * 120), MsgEnv.Length - (iAuxIni * 120))
            End If
            '
            Me.IsrLnhCreEtn(TipMsgCreEtn, _
                            iPrxNumSeqCreEtn, _
                            iAuxIni + 1, _
                            sPedacoMsg)
            '
            iAuxIni += 1
            iAux -= 1
            '
        End While

    End Function


End Class
