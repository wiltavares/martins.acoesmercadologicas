﻿Imports Core
Imports System.Data

Public Class RelacaoAcaoComercialxFluxoAprovacaoBO
    Inherits BOBase(Of VO.RelacaoAcaoComercialxFluxoAprovacao)
    Enum IndicadorAprovacao
        Normal = 0
        Aprovacao = 1
        Parecer = 2
    End Enum

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoComercialxFluxoAprovacaoDAO()
    End Sub

    Public Function BuscaFluxosAprovacaoDaAcao(codAcao As Decimal) As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)

        Return DirectCast(daoBase, RelacaoAcaoComercialxFluxoAprovacaoDAO).BuscaFluxosAprovacaoDaAcao(codAcao)

    End Function

    Public Function BuscaFluxosAprovacaoDaAcaoDoUsuario(codAcao As Integer, codFnc As Integer) As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)

        Return DirectCast(daoBase, RelacaoAcaoComercialxFluxoAprovacaoDAO).BuscaFluxosAprovacaoDaAcaoDoUsuario(codAcao, codFnc)

    End Function

    Public Function BuscarMaxCodFluxo() As Decimal

        Return DirectCast(daoBase, RelacaoAcaoComercialxFluxoAprovacaoDAO).BuscarMaxCodFluxo()

    End Function

    Public Sub IserirCabecalhoFluxo(ByVal ListaFluxoAprovacao As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao), ByVal iNumPrxFluApv As Decimal)
        Try

            For Each itemfluxo In ListaFluxoAprovacao
                'Insere o fornecedor na tabela de relacionamento
                itemfluxo.NUMFLUAPV = iNumPrxFluApv
                DirectCast(daoBase, RelacaoAcaoComercialxFluxoAprovacaoDAO).IserirCabecalhoFluxo(itemfluxo)
            Next

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Function BuscaFluxosAprovacaoDaAcao(ByVal CodAcoCmc As Integer) As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)
        Try

            Return DirectCast(daoBase, RelacaoAcaoComercialxFluxoAprovacaoDAO).BuscaFluxosAprovacaoDaAcao(CodAcoCmc)

        Catch ex As Exception
            Throw ex
        End Try

    End Function


    Public Sub AtuFlgStaFluApvCmp(ByVal CodAcoCmc As Integer, _
                                       ByVal NumFluApv As Integer, _
                                       ByVal AcoApv As Integer)
        Try

            DirectCast(daoBase, RelacaoAcaoComercialxFluxoAprovacaoDAO).AtuFlgStaFluApvCmp(CodAcoCmc, NumFluApv, AcoApv)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    ''' <summary>
    ''' organizarAprovacao
    ''' Configura dados da aprovacao para controlar visibilidade dos botoes da tela
    ''' organizarAprovacao
    ''' </summary>
    ''' <param name="dsFluApv"></param>
    Public Function VerificarTipoDeAprovacao(ByVal dsFluApv As DataSet) As Integer
        'TODO: No sistema antigo o parâmetro é sempre NOTHING
        If dsFluApv IsNot Nothing Then
            If dsFluApv.Tables.Contains("DdoMhnApv") Then
                With dsFluApv.Tables("DdoMhnApv")
                    If .Rows.Count > 0 Then
                        If .Rows(0)("TIPSTAFLUAPV") = 3 Then
                            Return IndicadorAprovacao.Aprovacao
                        ElseIf .Rows(0)("TIPSTAFLUAPV") = 8 Then
                            Return IndicadorAprovacao.Parecer
                        Else
                            Return IndicadorAprovacao.Normal
                        End If
                    End If
                    Return IndicadorAprovacao.Normal
                End With
            Else
                Return IndicadorAprovacao.Normal
            End If
        End If
        Return IndicadorAprovacao.Normal

    End Function
End Class
