﻿Imports Core
Public Class DiretoriaCompraBO
    Inherits BOBase(Of VO.DiretoriaCompra)

    Public Sub New()
        MyBase.daoBase = New DiretoriaCompraDAO()
    End Sub

    ''' <summary>
    ''' Lista DESCRICAO DIVISAO DE COMPRA
    ''' </summary>
    ''' <returns></returns>
    Public Function ListDiretoriaCompra() As List(Of VO.DiretoriaCompra)

        Dim diretoriaCompra = New VO.DiretoriaCompra()
        'evento. = idTipoEvento
        Return DirectCast(daoBase, DiretoriaCompraDAO).ListDiretoriaCompra(diretoriaCompra)

    End Function
End Class
