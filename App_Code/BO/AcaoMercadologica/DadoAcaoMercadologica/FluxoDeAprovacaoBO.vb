﻿Imports Core
Imports System.Text
Imports System.Math
Imports System.Globalization

Public Class FluxoDeAprovacaoBO
    Inherits BOBase(Of VO.FluxoDeAprovacao)

    Private TipStaFluApv As Object = New With {.Criacao = 1,
                                               .Em_Aprovacao = 3,
                                               .Aprovado = 5,
                                               .Rejeitado = 4,
                                               .Pedido_Parecer = 6,
                                               .Resposta_Parecer = 8,
                                               .Aguardando_Aprovacao = 9,
                                               .Notificacao = 7}
    Private RelacaoFuncionarioUserRacfAcountBO As New RelacaoFuncionarioUserRacfAcountBO
    Private MovimentoDeMensagensDoCorreioEletronicoBO As New MovimentoDeMensagensDoCorreioEletronicoBO
    Private RelacaoDeEnderecosParaEnvioDoCorreioEletronicoBO As New RelacaoDeEnderecosParaEnvioDoCorreioEletronicoBO
    Private RelacaoAcaoComercialxFornecedoresBO As New RelacaoAcaoComercialxFornecedoresBO
    Private RelacaoAcaoComercialxFluxoAprovacaoBO As New RelacaoAcaoComercialxFluxoAprovacaoBO
    Private AcaoComercialBO As New AcaoComercialBO
    Private TipNivFluApv As Object = New With {
        .Ger_Marketing = 1,
        .Ger_Categoria = 2,
        .Dir_Compras = 3
    }



    Public Sub New()
        MyBase.daoBase = New FluxoDeAprovacaoDAO()
    End Sub

    Public Sub SalvarItensfluxo(ByVal ListaFluxoAprovacao As List(Of VO.FluxoDeAprovacao))
        Try
            For Each itemfluxo In ListaFluxoAprovacao
                DirectCast(daoBase, FluxoDeAprovacaoDAO).SalvarItensfluxo(itemfluxo)
            Next
        Catch
            Throw
        End Try

    End Sub

    Public Function CnsUsrNotEma(ByVal CodSisInf As Decimal, ByVal NumSeqApv As Decimal) As List(Of VO.FluxoDeAprovacao)

        Return DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrNotEma(CodSisInf, NumSeqApv)

    End Function


    Public Function buscaUltimoFluxoAprovacao() As Integer
        Return DirectCast(daoBase, FluxoDeAprovacaoDAO).buscaUltimoFluxoAprovacao()
    End Function


    Public Function BuscaFormaPagamento() As List(Of VO.FluxoDeAprovacao)
        Return DirectCast(daoBase, FluxoDeAprovacaoDAO).BuscaFormaPagamento()
    End Function


    Public Function BuscaFuncionarioGAC() As List(Of VO.FluxoDeAprovacao)

        Return DirectCast(daoBase, FluxoDeAprovacaoDAO).BuscaFuncionarioGAC()

    End Function

#Region "Aprovar/Parecer"
    Private Function PedidoParecer(ByVal CodSisInf As Integer,
                                    ByVal CodAcoCmc As Integer,
        ByVal NumFluApv As Integer,
        ByVal NumSeqFluApv As Integer,
        ByVal NumNivFluApv As Integer,
        ByVal CodFnc As Integer,
        ByVal NomFnc As String,
        ByVal CodFncArz As Integer,
        ByVal ObsApv As String,
        ByVal bFlgDlg As Boolean)
        'Atualiza o fluxo para conter as informacoes do pedido do parecer -> MRT.T0161591
        DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuPedOpnFluApv(CodSisInf,
                                   NumFluApv,
                                   NumSeqFluApv,
                                   TipStaFluApv.Pedido_Parecer,
                                   ObsApv,
                                   IIf(bFlgDlg, CodFnc, 0))

        'Insere os dados a espera da resposta de parecer -> MRT.T0161591
        DirectCast(daoBase, FluxoDeAprovacaoDAO).IsrPedOpnFluApv(CodSisInf,
                                   NumFluApv,
                                   NumNivFluApv,
                                   CodFncArz,
                                   TipStaFluApv.Resposta_Parecer,
                                   "Usuário: " & CodFnc.ToString() & " - " &
                                   NomFnc & " lhe pediu parecer. ",
                                   NumSeqFluApv)

        'Envio de Email para alerta de parecer
        Dim varVO As New VO.RelacaoFuncionarioUserRacfAcount
        varVO.CODFNC = CodFncArz
        Dim dsNomUsr As String = RelacaoFuncionarioUserRacfAcountBO.BuscaPorCodFnc(varVO).NOMUSRRCF

        If Not String.IsNullOrEmpty(dsNomUsr) Then
            PcdMsgAlaFlu(CodAcoCmc,
                         CodSisInf,
                         NumFluApv,
                         42,
                         CodFnc,
                         NomFnc,
                         dsNomUsr,
                         TipStaFluApv.Pedido_Parecer)
        End If
        'Fim de envio de email para alerta de parecer
    End Function

    Private Function RespostaParecer(ByVal CodSisInf As Integer,
                                    ByVal CodAcoCmc As Integer,
                                    ByVal NumFluApv As Integer,
                                    ByVal ObsApv As String,
                                     ByVal CodFnc As Integer,
                                    ByVal NomFnc As String,
                                    ByVal bFlgDlg As Boolean,
                                    ByVal NumSeqFluApv As Integer)

        'Atualiza a resposta de parecer -> MRT.T0161591
        DirectCast(daoBase, FluxoDeAprovacaoDAO).RspPedOpnFluApv(CodSisInf,
                                   NumFluApv,
                                   NumSeqFluApv,
                                   ObsApv,
                                   IIf(bFlgDlg, CodFnc, 0))

        'Atualiza os dados do emissor do pedido de parecer 
        'para novamente entrar em aprovacao. -> MRT.T0161591
        DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuEmiPedOpnFluApv(CodSisInf,
                                      NumFluApv,
                                      NumSeqFluApv)

        'Envio de Email para alerta de resposta de parecer
        Dim dsNomUsr As String = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEmaEsp(CodSisInf,
                                                    NumFluApv,
                                                    NumSeqFluApv,
                                                    0).FirstOrDefault.NOMUSRRCF

        If Not String.IsNullOrEmpty(dsNomUsr) Then
            PcdMsgAlaFlu(CodAcoCmc,
                         CodSisInf,
                         NumFluApv,
                         42,
                         CodFnc,
                         NomFnc,
                         dsNomUsr,
                         TipStaFluApv.Resposta_Parecer)


        End If
        'Fim de envio de email para alerta de parecer
    End Function

    Private Sub Rejeitado(ByVal CodSisInf As Integer,
                                    ByVal CodAcoCmc As Integer,
                                    ByVal NumFluApv As Integer,
                                    ByVal NumSeqFluApv As Integer,
                                    ByVal AcoApv As Integer,
                                    ByVal CodFnc As Integer,
                                    ByVal NomFnc As String,
                                    ByVal ObsApv As String,
                                    ByVal CodFormPag As Decimal,
                                    ByVal NumDias As Decimal,
                                    ByVal bFlgDlg As Boolean)

        'Atualiza o fluxo para estar o estado de rejeitado -> MRT.T0161591
        DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuFlgStaApvCmp(CodSisInf,
                                   NumFluApv,
                                   NumSeqFluApv,
                                   AcoApv,
                                   ObsApv,
                                   IIf(bFlgDlg, CodFnc, 0))

        'Envio de Email para alerta de resposta de parecer
        Dim dsNomUsr As String = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEmaEsp(CodSisInf,
                                              NumFluApv,
                                              0,
                                              0).FirstOrDefault.NOMUSRRCF

        If Not String.IsNullOrEmpty(dsNomUsr) Then
            PcdMsgAlaFlu(CodAcoCmc,
                         CodSisInf,
                         NumFluApv,
                         42,
                         CodFnc,
                         NomFnc,
                         dsNomUsr,
                         TipStaFluApv.Rejeitado)


        End If
        'Fim de envio de email para alerta de parecer


        'Atualiza o flag desse fluxo para reprovado
        RelacaoAcaoComercialxFluxoAprovacaoBO.AtuFlgStaFluApvCmp(CodAcoCmc, NumFluApv, TipStaFluApv.Rejeitado)

        'Atualizao o Status da ação para reprovado
        AcaoComercialBO.AtuStaAcoCmc(CodAcoCmc, TipStaFluApv.Rejeitado)


        'Chama a parte referente ao CBU300 - Inicio

        ' Insere o motivo da Rejeição  -- Chamada referente ao metodo 'FunInsDdoRpvAcoCmc' do ações antigo
        Dim boRelacaoAcaoComercialxRejeicao As New RelacaoAcaoComercialxRejeicaoBO
        boRelacaoAcaoComercialxRejeicao.FunInsDdoRpvAcoCmc(CodAcoCmc, TipStaFluApv.Rejeitado, CodFnc, ObsApv)

        ' Atualiza o Saldo reservado do Fornecedor -- Chamada referente ao metodo 'FunAtuSldRsvFrnRpv' do ações antigo
        Dim boMovimentoDiarioReservaDeSaldoFornecedor As New MovimentoDiarioReservaDeSaldoFornecedorBO()
        boMovimentoDiarioReservaDeSaldoFornecedor.DeleteT(New VO.MovimentoDiarioReservaDeSaldoFornecedor() With {.CODACOCMC = CodAcoCmc})

        ' Atualiza o plano de marketing  -- Chamada referente ao metodo 'FunAtuDdoPlnMktRpv' do ações antigo
        Dim boPlanoMarketing As New PlanoDeMarketingBO
        Dim boRelacaoAcaoComercialxPlanoMarketing As New RelacaoAcaoComercialxPlanoMarketingBO
        Dim relacPlanoMkt = boRelacaoAcaoComercialxPlanoMarketing.BuscaPlanoMarketingProvisionadoAcao(CodAcoCmc)
        For Each item As VO.RelacaoAcaoComercialxPlanoMarketing In relacPlanoMkt
            boPlanoMarketing.EstornaValorReservadoPlanoMarketing(TIPEVTACOCMC:=item.TIPEVTACOCMC _
                                                                           , CODEVTACOCMC:=item.CODEVTACOCMC _
                                                                           , ANOMESREF:=item.ANOMESREF _
                                                                           , CODUNDESRNGC:=item.CODUNDESRNGC _
                                                                           , VLRRSVPLNMKT:=item.VLRRSVPLNMKT)
        Next

        'Chama a parte referente ao CBU300 - Fim

    End Sub

    Private Sub Aprovacao(ByVal CodSisInf As Integer,
                                    ByVal CodAcoCmc As Integer,
                                    ByVal NumFluApv As Integer,
                                    ByVal NumSeqFluApv As Integer,
                                    ByVal NumNivFluApv As Integer,
                                    ByVal AcoApv As Integer,
                                    ByVal CodFnc As Integer,
                                    ByVal NomFnc As String,
                                    ByVal CodFncArz As Integer,
                                    ByVal ObsApv As String,
                                    ByVal bFlgDlg As Boolean,
                                    ByVal CodFormPag As Decimal,
                                    ByVal NumDias As Decimal,
                                    ByVal IndPreAcordo As Boolean)
        'Atualiza de em_aprovacao para aprovado o item do fluxo -> MRT.T0161591
        DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuFlgStaApvCmp(CodSisInf,
                                   NumFluApv,
                                   NumSeqFluApv,
                                   AcoApv,
                                   ObsApv,
                                   IIf(bFlgDlg, CodFnc, 0))

        Dim fluxoAtual As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao) = New List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)()
        fluxoAtual = RelacaoAcaoComercialxFluxoAprovacaoBO.BuscaFluxosAprovacaoDaAcao(CodAcoCmc)
        Dim alteracao = fluxoAtual.Where(Function(x) x.NUMSEQNIVAPV = 301).ToList
        For Each alt In alteracao
            alt.DATHRAAPVFLU = Date.Now
            alt.TIPSTAFLUAPV = AcoApv
            alt.DESOBSAPV = ObsApv
        Next

        Dim dsFluApv As List(Of VO.FluxoDeAprovacao)
        'Consulta o fluxo inteiro para analise de quem serão os proximos aprovadores -> 
        'MRT.T0161591, MRT.RLCACOCMCFLUAPV
        dsFluApv = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsFluApvAbt(CodAcoCmc)
        Dim dvFluApv As List(Of VO.FluxoDeAprovacao)
        Dim dvFluApvAux As List(Of VO.FluxoDeAprovacao)
        dvFluApv = New List(Of VO.FluxoDeAprovacao)
        dvFluApv = dsFluApv

        dvFluApvAux = New List(Of VO.FluxoDeAprovacao)
        dvFluApvAux = dsFluApv

        'dvFluApv.RowFilter = "NUMSEQNIVAPV LIKE " & NumNivFluApv.ToString().Chars(0) & "%"
        dvFluApv = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(NumNivFluApv.ToString().Chars(0)) And f.TIPSTAFLUAPV <> TipStaFluApv.Resposta_Parecer).ToList

        If dvFluApv.Count = 1 Then

            AprovacaoCount1(CodSisInf, CodAcoCmc, NumFluApv, NumSeqFluApv, NumNivFluApv, AcoApv, CodFnc, NomFnc, CodFncArz, ObsApv,
                               bFlgDlg, CodFormPag, NumDias, IndPreAcordo, dvFluApv, dsFluApv, dvFluApvAux, fluxoAtual)

        ElseIf dvFluApv.Count > 1 And IndPreAcordo Then

            'Implementação Jeferson Fluxo Pré Acordo
            AprovacaoPreAcordo(CodSisInf, CodAcoCmc, NumFluApv, NumSeqFluApv, NumNivFluApv, AcoApv, CodFnc, NomFnc, CodFncArz, ObsApv,
                               bFlgDlg, CodFormPag, NumDias, IndPreAcordo, dvFluApv, dsFluApv, dvFluApvAux)
        Else
            AprovacaoAcao(CodSisInf, CodAcoCmc, NumFluApv, NumSeqFluApv, NumNivFluApv, AcoApv, CodFnc, NomFnc, CodFncArz, ObsApv,
                   bFlgDlg, CodFormPag, NumDias, IndPreAcordo, dvFluApv, dsFluApv, dvFluApvAux)

        End If
    End Sub

    Private Sub AprovacaoPreAcordo(ByVal CodSisInf As Integer,
                                    ByVal CodAcoCmc As Integer,
                                    ByVal NumFluApv As Integer,
                                    ByVal NumSeqFluApv As Integer,
                                    ByVal NumNivFluApv As Integer,
                                    ByVal AcoApv As Integer,
                                    ByVal CodFnc As Integer,
                                    ByVal NomFnc As String,
                                    ByVal CodFncArz As Integer,
                                    ByVal ObsApv As String,
                                    ByVal bFlgDlg As Boolean,
                                    ByVal CodFormPag As Decimal,
                                    ByVal NumDias As Decimal,
                                    ByVal IndPreAcordo As Boolean,
                                    ByRef dvFluApv As List(Of VO.FluxoDeAprovacao),
                                    ByRef dsFluApv As List(Of VO.FluxoDeAprovacao),
                                    ByRef dvFluApvAux As List(Of VO.FluxoDeAprovacao))
        'dvFluApv = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(NumNivFluApv.ToString().Chars(0)) And f.NUMSEQNIVAPV > NumNivFluApv).ToList()

        'If dvFluApv.Count > 0 Then

        '    For idvFluApv As Integer = 0 To dvFluApv.Count - 1

        '        'Atualiza de aguardando_aprovacao para em_aprovado o nivel do fluxo -> 
        '        'MRT.T0161591()
        '        DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuFlgStaNumFluApvCmp(CodSisInf, _
        '                                         NumFluApv, _
        '                                         dvFluApv.Item(idvFluApv).NUMSEQFLUAPV, _
        '                                         TipStaFluApv.Em_Aprovacao)


        '        'Envio de Email para alerta de em_aprovacao
        '        Dim nomes As List(Of String)
        '        nomes = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEmaEsp(CodSisInf, _
        '                                              NumFluApv, _
        '                                     dvFluApv(idvFluApv).NUMSEQFLUAPV, _
        '                                              0).Select(Function(f) f.NOMUSRRCF).ToList


        '        If Not String.IsNullOrEmpty(dsNomUsr) Then
        '            For Each nome In nomes
        '                PcdMsgAlaFlu(CodAcoCmc, _
        '                             CodSisInf, _
        '                             NumFluApv, _
        '                             42, _
        '                             CodFnc, _
        '                             NomFnc, _
        '                             nome, _
        '                             TipStaFluApv.Aprovado)
        '            Next
        '        End If
        '        'Fim de envio de email para alerta de parecer

        '    Next

        'Else
        '    'Aprovacao final, Atualialização das tabelas -> 
        '    'MRT.RLCACOCMCFLUAPV, MRT.T0118430
        '    ApvAco(CodAcoCmc, _
        '           NumFluApv, _
        '           True, _
        '           CodFnc, _
        '           NomFnc, _
        '           ObsApv, _
        '           CodFormPag, _
        '           NumDias)



        'End If
        Dim mesmoNivel As List(Of VO.FluxoDeAprovacao) = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(NumNivFluApv.ToString().Chars(0)) _
                                                                                And f.NUMSEQFLUAPV <> NumSeqFluApv _
                                                                                And f.DATHRAAPVFLU = Nothing _
                                                                                And f.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao).ToList()

        If (mesmoNivel.Count > 0) Then
            dvFluApv = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(NumNivFluApv.ToString().Chars(0)) _
                                              And f.NUMSEQFLUAPV <> NumSeqFluApv _
                                              And f.DATHRAAPVFLU = Nothing _
                                              And f.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao).ToList()
        Else
            dvFluApv = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(NumNivFluApv.ToString().Chars(0)) _
                                              And f.NUMSEQFLUAPV <> NumSeqFluApv _
                                              And f.DATHRAAPVFLU = Nothing).ToList()
        End If


        If dvFluApv.Count > 0 Then

            For idvFluApv As Integer = 0 To dvFluApv.Count - 1

                'Atualiza de aguardando_aprovacao para em_aprovado o nivel do fluxo -> 
                'MRT.T0161591()
                DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuFlgStaNumFluApvCmp(CodSisInf,
                                                 NumFluApv,
                                                 dvFluApv.Item(idvFluApv).NUMSEQFLUAPV,
                                                 TipStaFluApv.Em_Aprovacao)


                'Envio de Email para alerta de em_aprovacao
                Dim nomes As List(Of String)
                nomes = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEmaEsp(CodSisInf,
                                                      NumFluApv,
                                             dvFluApv(idvFluApv).NUMSEQFLUAPV,
                                                      0).Select(Function(f) f.NOMUSRRCF).ToList


                If Not nomes Is Nothing Then
                    For Each nome In nomes
                        PcdMsgAlaFlu(CodAcoCmc,
                                     CodSisInf,
                                     NumFluApv,
                                     42,
                                     CodFnc,
                                     NomFnc,
                                     nome,
                                     TipStaFluApv.Aprovado)
                    Next
                End If
                'Fim de envio de email para alerta de parecer
            Next

        Else

            dvFluApvAux = dvFluApvAux.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith((CType(NumNivFluApv.ToString.Substring(0, 1), Integer) + 1))).ToList


            If ((CType(CStr(CInt(CStr(NumNivFluApv).Substring(0, 1)) + 1), Decimal) >= 3) And (dvFluApvAux.Count = 0)) Or ((CType(CStr(CInt(CStr(NumNivFluApv).Substring(0, 1)) + 1), Decimal) = 4)) Then

                'Aprovacao final, Atualialização das tabelas -> 
                'MRT.RLCACOCMCFLUAPV, MRT.T0118430
                ApvAco(CodAcoCmc,
                           NumFluApv,
                           CodFnc,
                           NomFnc,
                           ObsApv,
                           CodFormPag,
                           NumDias)

            Else

                'dvFluApv.RowFilter = "NUMSEQNIVAPV LIKE " & _
                '                     CStr(CInt(CStr(NumNivFluApv).Substring(0, 1)) + 1) & "%"

                dvFluApv = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(CStr(CInt(CStr(NumNivFluApv).Substring(0, 1)) + 1))).ToList

                If dvFluApv.Count = 0 Then

                    'dvFluApv.RowFilter = "NUMSEQNIVAPV LIKE " & _
                    'CStr(CInt(CStr(NumNivFluApv).Substring(0, 1)) + 2) & "%"
                    dvFluApv = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(CStr(CInt(CStr(NumNivFluApv).Substring(0, 1)) + 2))).ToList

                    If dvFluApv.Count > 0 Then

                        For idvFluApv As Integer = 0 To dvFluApv.Count - 1

                            'Atualiza de aguardando_aprovacao para em_aprovado o nivel do fluxo -> 
                            'MRT.T0161591()
                            DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuFlgStaNumFluApvCmp(CodSisInf,
                                                             NumFluApv,
                                                             dvFluApv.Item(idvFluApv).NUMSEQFLUAPV,
                                                             TipStaFluApv.Em_Aprovacao)


                            'Envio de Email para alerta de em_aprovacao
                            Dim nomes As List(Of String)
                            nomes = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEmaEsp(CodSisInf,
                                                                  NumFluApv,
                                                         dvFluApv(idvFluApv).NUMSEQFLUAPV,
                                                                  0).Select(Function(f) f.NOMUSRRCF).ToList


                            If Not nomes Is Nothing Then
                                For Each nome In nomes
                                    PcdMsgAlaFlu(CodAcoCmc,
                                                 CodSisInf,
                                                 NumFluApv,
                                                 42,
                                                 CodFnc,
                                                 NomFnc,
                                                 nome,
                                                 TipStaFluApv.Aprovado)
                                Next
                            End If
                            'Fim de envio de email para alerta de parecer

                        Next

                    Else
                        'Aprovacao final, Atualialização das tabelas -> 
                        'MRT.RLCACOCMCFLUAPV, MRT.T0118430
                        ApvAco(CodAcoCmc,
                               NumFluApv,
                               CodFnc,
                               NomFnc,
                               ObsApv,
                               CodFormPag,
                               NumDias)

                    End If

                Else
                    For idvFluApv As Integer = 0 To dvFluApv.Count - 1

                        'Atualiza de aguardando_aprovacao para em_aprovado o nivel do fluxo -> 
                        'MRT.T0161591()

                        DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuFlgStaNumFluApvCmp(CodSisInf,
                                                         NumFluApv,
                                                         dvFluApv.Item(idvFluApv).NUMSEQFLUAPV,
                                                         TipStaFluApv.Em_Aprovacao)

                        'Envio de Email para alerta de em_aprovacao
                        Dim nomes As List(Of String)

                        nomes = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEmaEsp(CodSisInf,
                                                                  NumFluApv,
                                                         dvFluApv.Item(idvFluApv).NUMSEQFLUAPV,
                                                                  0).Select(Function(f) f.NOMUSRRCF).ToList

                        If Not IsNothing(nomes) Then
                            If nomes.Count > 0 Then

                                For Each nome In nomes
                                    PcdMsgAlaFlu(CodAcoCmc,
                                                 CodSisInf,
                                                 NumFluApv,
                                                 42,
                                                 CodFnc,
                                                 NomFnc,
                                                 nome,
                                                 TipStaFluApv.Aprovado)
                                Next

                            End If
                        End If
                        'Fim de envio de email para aprovacao

                    Next
                End If

            End If

        End If

        ' Fim Implmentação do Fluxo de Pré Acordo 
    End Sub

    Private Sub AprovacaoAcao(ByVal CodSisInf As Integer,
                                    ByVal CodAcoCmc As Integer,
                                    ByVal NumFluApv As Integer,
                                    ByVal NumSeqFluApv As Integer,
                                    ByVal NumNivFluApv As Decimal,
                                    ByVal AcoApv As Integer,
                                    ByVal CodFnc As Integer,
                                    ByVal NomFnc As String,
                                    ByVal CodFncArz As Integer,
                                    ByVal ObsApv As String,
                                    ByVal bFlgDlg As Boolean,
                                    ByVal CodFormPag As Decimal,
                                    ByVal NumDias As Decimal,
                                    ByVal IndPreAcordo As Boolean,
                                    ByRef dvFluApv As List(Of VO.FluxoDeAprovacao),
                                    ByRef dsFluApv As List(Of VO.FluxoDeAprovacao),
                                    ByRef dvFluApvAux As List(Of VO.FluxoDeAprovacao))

        Select Case CInt(CStr(NumNivFluApv).Substring(0, 1))
            Case TipNivFluApv.Ger_Marketing

                Dim mesmoNivel As List(Of VO.FluxoDeAprovacao) = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(TipNivFluApv.Ger_Marketing) _
                                                                                    And f.NUMSEQNIVAPV <> NumNivFluApv _
                                                                                    And f.DATHRAAPVFLU = Nothing _
                                                                                    And f.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao).ToList()

                If (mesmoNivel.Count > 0) Then
                    Return
                Else
                    dvFluApv = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(TipNivFluApv.Ger_Marketing) And (f.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao Or f.TIPSTAFLUAPV = TipStaFluApv.Pedido_Parecer)).ToList
                End If


                If dvFluApv.Count = 0 Then
                    dvFluApv = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(TipNivFluApv.Ger_Categoria.ToString)).ToList

                    If dvFluApv.Count > 0 Then
                        For idvFluApv As Integer = 0 To dvFluApv.Count - 1

                            'Atualiza de aguardando_aprovacao para em_aprovado 
                            'o nivel do fluxo -> MRT.T0161591
                            DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuFlgStaNumFluApvCmp(CodSisInf,
                                                             NumFluApv,
                                                       dvFluApv.Item(idvFluApv).NUMSEQFLUAPV,
                                                         TipStaFluApv.Em_Aprovacao)


                            'Envio de Email para alerta de em_aprovacao
                            Dim nomes As List(Of String)
                            nomes = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEmaEsp(CodSisInf,
                                                              NumFluApv,
                                                     dvFluApv.Item(idvFluApv).NUMSEQFLUAPV,
                                                              0).Select(Function(f) f.NOMUSRRCF).ToList

                            If Not IsNothing(nomes) Then
                                If nomes.Count > 0 Then

                                    For Each drRow As String In nomes
                                        PcdMsgAlaFlu(CodAcoCmc,
                                                     CodSisInf,
                                                     NumFluApv,
                                                     42,
                                                     CodFnc,
                                                     NomFnc,
                                                     drRow,
                                                     TipStaFluApv.Aprovado)
                                    Next

                                End If
                            End If
                            'Fim de envio de email para alerta de parecer

                        Next
                    Else
                        dvFluApv = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(TipNivFluApv.Dir_Compras.ToString)).ToList

                        If dvFluApv.Count > 0 Then
                            For idvFluApv As Integer = 0 To dvFluApv.Count - 1

                                'Atualiza de aguardando_aprovacao para em_aprovado 
                                'o nivel do fluxo -> MRT.T0161591
                                DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuFlgStaNumFluApvCmp(CodSisInf,
                                                             NumFluApv,
                                                       dvFluApv.Item(idvFluApv).NUMSEQFLUAPV,
                                                         TipStaFluApv.Em_Aprovacao)

                                'Envio de Email para alerta de em_aprovacao
                                Dim nomes As List(Of String)
                                nomes = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEmaEsp(CodSisInf,
                                                           NumFluApv,
                                                  dvFluApv.Item(idvFluApv).NUMSEQFLUAPV,
                                                           0).Select(Function(f) f.NOMUSRRCF).ToList

                                If Not IsNothing(nomes) Then
                                    If nomes.Count > 0 Then

                                        For Each drRow In nomes
                                            PcdMsgAlaFlu(CodAcoCmc,
                                                         CodSisInf,
                                                         NumFluApv,
                                                         42,
                                                         CodFnc,
                                                         NomFnc,
                                                         drRow,
                                                         TipStaFluApv.Aprovado)
                                        Next

                                    End If
                                End If
                                'Fim de envio de email para alerta de parecer

                            Next
                        Else
                            'Aprovacao final, Atualialização das tabelas -> 
                            'MRT.RLCACOCMCFLUAPV, MRT.T0118430
                            ApvAco(CodAcoCmc,
                                   NumFluApv,
                                   CodFnc,
                                   NomFnc,
                                   ObsApv,
                                   CodFormPag,
                                   NumDias)
                        End If

                    End If

                End If

            Case TipNivFluApv.Ger_Categoria

                ' Verifica se tem mais alguem do mesmo nivel e gerencia que o aprovador atual
                Dim mesmoDiretoria As List(Of VO.FluxoDeAprovacao) = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV = NumNivFluApv _
                                                                                        And f.NUMSEQFLUAPV <> NumSeqFluApv _
                                                                                        And f.DATHRAAPVFLU = Nothing _
                                                                                        And f.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao).ToList()
                If (mesmoDiretoria.Count > 0) Then
                    Return
                End If

                ' Verifica se tem diretor aguardando aprovação
                Dim diretor As VO.FluxoDeAprovacao = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV = (NumNivFluApv + 100) _
                                                                        And f.TIPSTAFLUAPV = TipStaFluApv.Aguardando_Aprovacao).FirstOrDefault()
                If (Not diretor Is Nothing) Then
                    'Coloca o diretor em aprovação
                    'Atualiza de aguardando_aprovacao para em_aprovado 
                    DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuFlgStaNumFluApvCmp(CodSisInf, NumFluApv, diretor.NUMSEQFLUAPV, TipStaFluApv.Em_Aprovacao)

                    'Envio de Email para alerta de em_aprovacao
                    Dim nomes As List(Of String)
                    nomes = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEmaEsp(CodSisInf, NumFluApv, diretor.NUMSEQFLUAPV, 0).Select(Function(f) f.NOMUSRRCF).ToList

                    If Not IsNothing(nomes) Then
                        If nomes.Count > 0 Then
                            For Each drRow In nomes
                                PcdMsgAlaFlu(CodAcoCmc, CodSisInf, NumFluApv, 42, CodFnc, NomFnc, drRow, TipStaFluApv.Aprovado)
                            Next
                        End If
                    End If
                    'Fim de envio de email para alerta de parecer
                    Return
                End If

                ' Verifica se tem mais alguem de qualquer nivel pendente de aprovação (NUMSEQFLUAPV <> NumSeqFluApv =  Exceto o proprio)
                Dim qualquerOutroPendente As List(Of VO.FluxoDeAprovacao) = dsFluApv.Where(Function(f) f.NUMSEQFLUAPV <> NumSeqFluApv _
                                                                                                   And f.DATHRAAPVFLU = Nothing _
                                                                                                   And (f.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao _
                                                                                                        Or f.TIPSTAFLUAPV = TipStaFluApv.Pedido_Parecer _
                                                                                                        Or f.TIPSTAFLUAPV = TipStaFluApv.Aguardando_Aprovacao
                                                                                                        )).ToList()
                If (qualquerOutroPendente.Count > 0) Then
                    Return
                End If

                'Aprovacao final, Atualialização das tabelas -> 
                'MRT.RLCACOCMCFLUAPV, MRT.T0118430
                ApvAco(CodAcoCmc,
                       NumFluApv,
                       CodFnc,
                       NomFnc,
                       ObsApv,
                       CodFormPag,
                       NumDias)

            Case TipNivFluApv.Dir_Compras

                ' Verifica se tem mais alguem de qualquer nivel pendente de aprovação (NUMSEQFLUAPV <> NumSeqFluApv =  Exceto o proprio)
                Dim qualquerOutroPendente As List(Of VO.FluxoDeAprovacao) = dsFluApv.Where(Function(f) f.NUMSEQFLUAPV <> NumSeqFluApv _
                                                                                                   And f.DATHRAAPVFLU = Nothing _
                                                                                                   And (f.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao _
                                                                                                        Or f.TIPSTAFLUAPV = TipStaFluApv.Pedido_Parecer _
                                                                                                        Or f.TIPSTAFLUAPV = TipStaFluApv.Aguardando_Aprovacao
                                                                                                        )).ToList()
                If (qualquerOutroPendente.Count > 0) Then
                    Return
                End If

                'Aprovacao final, Atualialização das tabelas -> 
                'MRT.RLCACOCMCFLUAPV, MRT.T0118430
                ApvAco(CodAcoCmc,
                       NumFluApv,
                       CodFnc,
                       NomFnc,
                       ObsApv,
                       CodFormPag,
                       NumDias)

        End Select

    End Sub

    Private Sub AprovacaoCount1(ByVal CodSisInf As Integer,
                                    ByVal CodAcoCmc As Integer,
                                    ByVal NumFluApv As Integer,
                                    ByVal NumSeqFluApv As Integer,
                                    ByVal NumNivFluApv As Integer,
                                    ByVal AcoApv As Integer,
                                    ByVal CodFnc As Integer,
                                    ByVal NomFnc As String,
                                    ByVal CodFncArz As Integer,
                                    ByVal ObsApv As String,
                                    ByVal bFlgDlg As Boolean,
                                    ByVal CodFormPag As Decimal,
                                    ByVal NumDias As Decimal,
                                    ByVal IndPreAcordo As Boolean,
                                    ByRef dvFluApv As List(Of VO.FluxoDeAprovacao),
                                    ByRef dsFluApv As List(Of VO.FluxoDeAprovacao),
                                    ByRef dvFluApvAux As List(Of VO.FluxoDeAprovacao),
                                    ByVal fluxoAtual As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao))

        ' Verifica se tem mais alguem de qualquer nivel pendente de aprovação (NUMSEQFLUAPV <> NumSeqFluApv =  Exceto o proprio)
        Dim qualquerOutroPendente As List(Of VO.FluxoDeAprovacao) = dsFluApv.Where(Function(f) f.NUMSEQFLUAPV <> NumSeqFluApv _
                                                                                                   And f.DATHRAAPVFLU = Nothing _
                                                                                                   And (f.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao _
                                                                                                        Or f.TIPSTAFLUAPV = TipStaFluApv.Pedido_Parecer _
                                                                                                        Or f.TIPSTAFLUAPV = TipStaFluApv.Aguardando_Aprovacao
                                                                                                        )).ToList()

        'não tem mais ninguem para aprovar , entao efetiva a aprovação
        If (qualquerOutroPendente.Count = 0) Then
            'Aprovacao final, Atualialização das tabelas -> 
            'MRT.RLCACOCMCFLUAPV, MRT.T0118430
            ApvAco(CodAcoCmc,
                       NumFluApv,
                       CodFnc,
                       NomFnc,
                       ObsApv,
                       CodFormPag,
                       NumDias,
                       fluxoAtual)
            Return
        End If


        'Obtem os proximos aprovadores do proximo nivel.
        dvFluApv = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(CStr(CInt(CStr(NumNivFluApv).Substring(0, 1)) + 1))).ToList

        If dvFluApv.Count > 0 Then
            Dim acao = AcaoComercialBO.BuscaAcaoPorId(CodAcoCmc)
            For idvFluApv As Integer = 0 To dvFluApv.Count - 1
                If (dvFluApv.Item(idvFluApv).CODEDEAPV = acao.CODFNCCRIACOCMC) Then
                    Continue For
                End If
                'Atualiza de aguardando_aprovacao para em_aprovado o nivel do fluxo -> 
                'MRT.T0161591()

                DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuFlgStaNumFluApvCmp(CodSisInf,
                                                 NumFluApv,
                                                 dvFluApv.Item(idvFluApv).NUMSEQFLUAPV,
                                                 TipStaFluApv.Em_Aprovacao)

                'Envio de Email para alerta de em_aprovacao
                Dim nomes As List(Of String)

                nomes = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEmaEsp(CodSisInf,
                                                      NumFluApv,
                                             dvFluApv.Item(idvFluApv).NUMSEQFLUAPV,
                                                      0).Select(Function(f) f.NOMUSRRCF).ToList

                If Not nomes Is Nothing Then

                    For Each nome In nomes
                        PcdMsgAlaFlu(CodAcoCmc,
                                 CodSisInf,
                                 NumFluApv,
                                 42,
                                 CodFnc,
                                 NomFnc,
                                 nome,
                                 TipStaFluApv.Aprovado)
                    Next

                End If
                'Fim de envio de email para aprovacao
            Next
        Else  'Não tem aprovadores pendentes no proximo nível, então verifica no segundo nivel.
            dvFluApv = dsFluApv.Where(Function(f) f.NUMSEQNIVAPV.ToString.StartsWith(CStr(CInt(CStr(NumNivFluApv).Substring(0, 1)) + 2))).ToList

            If dvFluApv.Count > 0 Then

                For idvFluApv As Integer = 0 To dvFluApv.Count - 1

                    'Atualiza de aguardando_aprovacao para em_aprovado o nivel do fluxo -> 
                    'MRT.T0161591()
                    DirectCast(daoBase, FluxoDeAprovacaoDAO).AtuFlgStaNumFluApvCmp(CodSisInf,
                                                     NumFluApv,
                                                     dvFluApv.Item(idvFluApv).NUMSEQFLUAPV,
                                                     TipStaFluApv.Em_Aprovacao)


                    'Envio de Email para alerta de em_aprovacao
                    Dim nomes As List(Of String)
                    nomes = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEmaEsp(CodSisInf,
                                                          NumFluApv,
                                                 dvFluApv(idvFluApv).NUMSEQFLUAPV,
                                                          0).Select(Function(f) f.NOMUSRRCF).ToList

                    If Not nomes Is Nothing Then
                        For Each nome In nomes
                            PcdMsgAlaFlu(CodAcoCmc,
                                         CodSisInf,
                                         NumFluApv,
                                         42,
                                         CodFnc,
                                         NomFnc,
                                         nome,
                                         TipStaFluApv.Aprovado)
                        Next
                    End If
                    'Fim de envio de email para alerta de parecer
                Next
            End If
        End If
    End Sub

    Public Sub MovFluApvAcoCmc(ByVal CodSisInf As Integer,
                                    ByVal CodAcoCmc As Integer,
                                    ByVal NumFluApv As Integer,
                                    ByVal NumSeqFluApv As Integer,
                                    ByVal NumNivFluApv As Integer,
                                    ByVal AcoApv As Integer,
                                    ByVal CodFnc As Integer,
                                    ByVal NomFnc As String,
                                    ByVal CodFncArz As Integer,
                                    ByVal ObsApv As String,
                                    ByVal bFlgDlg As Boolean,
                                    ByVal CodFormPag As Decimal,
                                    ByVal NumDias As Decimal,
                                    ByVal IndPreAcordo As Boolean)
        Try
            daoBase.connector.BeginTransaction()

            If AcoApv = TipStaFluApv.Pedido_Parecer Then
                PedidoParecer(CodSisInf, CodAcoCmc, NumFluApv, NumSeqFluApv, NumNivFluApv, CodFnc, NomFnc, CodFncArz, ObsApv, bFlgDlg)
            ElseIf AcoApv = TipStaFluApv.Resposta_Parecer Then
                RespostaParecer(CodSisInf, CodAcoCmc, NumFluApv, ObsApv, CodFnc, NomFnc, bFlgDlg, NumSeqFluApv)
            ElseIf AcoApv = TipStaFluApv.Rejeitado Then
                Rejeitado(CodSisInf, CodAcoCmc, NumFluApv, NumSeqFluApv, AcoApv, CodFnc, NomFnc, ObsApv, CodFormPag, NumDias, bFlgDlg)
            Else
                If (CodFormPag <> 0) Then
                    AcaoComercialBO.AtuFormaPagNumDiasFuncGAC(CodAcoCmc, CodFormPag, NumDias)
                End If
                Aprovacao(CodSisInf, CodAcoCmc, NumFluApv, NumSeqFluApv, NumNivFluApv, AcoApv, CodFnc, NomFnc, CodFncArz, ObsApv, bFlgDlg, CodFormPag, NumDias, IndPreAcordo)
            End If
            daoBase.connector.Commit()

        Catch
            daoBase.connector.Rollback()
            Throw
        End Try
    End Sub

    Public Function PcdMsgAlaFlu(ByVal CodAcoCmc As Integer,
                                 ByVal CodSisInf As Integer,
                                 ByVal NumFluApv As Integer,
                                 ByVal TipMsgCreEtn As Integer,
                                 ByVal CodFnc As Integer,
                                 ByVal NomFnc As String,
                                 ByVal NomUsrRec As String,
                                 ByVal iApv As Integer) As Integer

        Dim iPrxNumSeqCreEtn As Integer
        Dim MsgEnv As String = ""
        Dim iNumSeqEndCreEtn As Integer
        Dim sPedacoMsg As String = ""
        Dim sMsgEnvAux As String = ""
        Dim iAuxIni As Integer
        Dim iAux As Single
        Dim dsUsrAla As List(Of VO.FluxoDeAprovacao)
        Dim sTitulo As String

        Dim tipAcao = AcaoComercialBO.BuscarAcaoComercialPorId(CodAcoCmc).TIPACOMCD

        Select Case tipAcao
            Case TipoAcao.PreAcordoValor, TipoAcao.PreAcordoVigencia
                Select Case iApv
                    Case TipStaFluApv.Aprovado
                        sTitulo = "Aviso de aprovação! Pré-Acordo Promocional: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Resposta_Parecer
                        sTitulo = "Aviso de resposta de parecer! Pré-Acordo Promocional: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Rejeitado
                        sTitulo = "Aviso de reprovação! Pré-Acordo Promocional: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Pedido_Parecer
                        sTitulo = "Aviso de pedido de parecer! Pré-Acordo Promocional: " & CodAcoCmc.ToString() & "."
                End Select
            Case Else
                Select Case iApv
                    Case TipStaFluApv.Aprovado
                        sTitulo = "Aviso de aprovação! Ação Mercadologica: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Resposta_Parecer
                        sTitulo = "Aviso de resposta de parecer! Ação Mercadologica: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Rejeitado
                        sTitulo = "Aviso de reprovação! Ação Mercadologica: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Pedido_Parecer
                        sTitulo = "Aviso de pedido de parecer! Ação Mercadologica: " & CodAcoCmc.ToString() & "."
                End Select
        End Select


        iPrxNumSeqCreEtn = CnsPrxNumSeqCreEtn(TipMsgCreEtn)

        If IsrMsgCreEtn(TipMsgCreEtn,
                          iPrxNumSeqCreEtn,
                          sTitulo) Then

            Dim filtro As VO.RelacaoFuncionarioUserRacfAcount = New VO.RelacaoFuncionarioUserRacfAcount()
            filtro.CODFNC = CodFnc
            Dim remetente = RelacaoFuncionarioUserRacfAcountBO.BuscaPorCodFnc(filtro)

            iNumSeqEndCreEtn += 1
            IsrEndCreEtn(TipMsgCreEtn,
                           iPrxNumSeqCreEtn,
                           1,
                           iNumSeqEndCreEtn,
                           remetente.NOMUSRRCF.Trim.ToLower & "@martins.com.br")

            If NomUsrRec = "" Then
                dsUsrAla = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrAlaEma(CodSisInf,
                                                   NumFluApv)

                iNumSeqEndCreEtn = 0

                If Not IsNothing(dsUsrAla) Then
                    If dsUsrAla.Count > 0 Then
                        For Each drRow In dsUsrAla
                            '
                            iNumSeqEndCreEtn += 1
                            IsrEndCreEtn(TipMsgCreEtn,
                                            iPrxNumSeqCreEtn,
                                            2,
                                            iNumSeqEndCreEtn,
                                            CStr(drRow.NOMUSRRCF).Trim().ToLower() & "@martins.com.br")

                        Next
                    End If
                End If
            Else
                iNumSeqEndCreEtn = 0
                iNumSeqEndCreEtn += 1
                IsrEndCreEtn(TipMsgCreEtn,
                                 iPrxNumSeqCreEtn,
                                 2,
                                 iNumSeqEndCreEtn,
                                 CStr(NomUsrRec).Trim().ToLower() & "@martins.com.br")
            End If

            '
            MsgEnv = MntMsgAlaFlu(CodAcoCmc,
                                  iApv, tipAcao)

            'Looping para gravar de 120 em 120 caracteres no banco = 1 linha do email
            iAux = (MsgEnv.Length / 120)
            iAuxIni = 0

            While iAux > 0

                If MsgEnv.Length <= 120 Then
                    sPedacoMsg = MsgEnv
                ElseIf MsgEnv.Length > ((iAuxIni * 120) + 120) Then
                    sPedacoMsg = MsgEnv.Substring((iAuxIni * 120), 120)
                Else
                    sPedacoMsg = MsgEnv.Substring((iAuxIni * 120), MsgEnv.Length - (iAuxIni * 120))
                End If

                IsrLnhCreEtn(TipMsgCreEtn,
                                iPrxNumSeqCreEtn,
                                iAuxIni + 1,
                                sPedacoMsg)

                iAuxIni += 1
                iAux -= 1
            End While
        End If

    End Function

    Public Function CnsPrxNumSeqCreEtn(ByVal TipMsgCreEtn As Integer) As Integer

        Dim dsPrxNumSeqCreEtn As Decimal
        Dim iPrxNumSeqCreEtn As Integer

        dsPrxNumSeqCreEtn = MovimentoDeMensagensDoCorreioEletronicoBO.CnsPrxNumSeqCreEtn(TipMsgCreEtn)

        If Not IsNothing(dsPrxNumSeqCreEtn) Then
            If dsPrxNumSeqCreEtn > 0 Then
                If Not IsDBNull(dsPrxNumSeqCreEtn) Then
                    iPrxNumSeqCreEtn = dsPrxNumSeqCreEtn
                Else
                    iPrxNumSeqCreEtn = 1
                End If
            Else
                iPrxNumSeqCreEtn = 1
            End If
        Else
            iPrxNumSeqCreEtn = 1
        End If

        Return iPrxNumSeqCreEtn

    End Function

    Public Function IsrMsgCreEtn(ByVal TipMsgCreEtn As Integer,
                                 ByVal NumSeqMsgCreEtn As Integer,
                                 ByVal DesAssCreEtn As String) As Integer

        Return MovimentoDeMensagensDoCorreioEletronicoBO.IsrMsgCreEtn(TipMsgCreEtn,
                                       NumSeqMsgCreEtn,
                                       DesAssCreEtn)

    End Function

    Public Sub IsrEndCreEtn(ByVal TipMsgCreEtn As Integer,
                                 ByVal NumSeqMsgCreEtn As Integer,
                                 ByVal TipEndCreEtn As Integer,
                                 ByVal NumSeqEndCreEtn As Integer,
                                 ByVal IdtEndCreEtn As String)

        RelacaoDeEnderecosParaEnvioDoCorreioEletronicoBO.IsrEndCreEtn(TipMsgCreEtn,
                                       NumSeqMsgCreEtn,
                                       TipEndCreEtn,
                                       NumSeqEndCreEtn,
                                       IdtEndCreEtn)

    End Sub

    Private Function MntMsgAlaFlu(ByVal CodAcoCmc As Integer,
                                  ByVal iApv As Integer, ByVal tipo As TipoAcao) As String

        Dim sMsg As StringBuilder

        sMsg = New StringBuilder

        If CodAcoCmc > 0 Then

            sMsg.Append("|----------------------------------------------------------------------------------------------------------------------|")
            Dim linha1 As String
            Select Case tipo
                Case TipoAcao.PreAcordoValor, TipoAcao.PreAcordoVigencia
                    linha1 = "| Mensagem automática para aviso do fluxo de aprovação do pré-acordo promocional: " & CodAcoCmc.ToString()
                Case Else
                    linha1 = "| Mensagem automática para aviso do fluxo de aprovação da ação mercadológica: " & CodAcoCmc.ToString()
            End Select

            'If CodAcoCmc.ToString().Length < 41 Then
            '    For i As Integer = 0 To (41 - CodAcoCmc.ToString.Length) - 1
            '        sMsg.Append(" ")
            '    Next
            'End If
            While linha1.Length < 119
                linha1 = linha1 & " "
            End While
            sMsg.Append(linha1)
            sMsg.Append("|")

            Select Case tipo
                Case TipoAcao.PreAcordoValor, TipoAcao.PreAcordoVigencia
                    Select Case iApv
                        Case TipStaFluApv.Aprovado
                            sMsg.Append("| O pré-acordo promocional está para SUA aprovação ou pedido de parecer.                                               |")
                        Case TipStaFluApv.Resposta_Parecer
                            sMsg.Append("| O pré-acordo promocional está para SUA aprovação ou pedido de parecer.                                               |")
                        Case TipStaFluApv.Rejeitado
                            sMsg.Append("| O pré-acordo promocional foi reprovada.                                                                              |")
                        Case TipStaFluApv.Pedido_Parecer
                            sMsg.Append("| O pré-acordo promocional está para SEU parecer.                                                                      |")
                    End Select
                Case Else
                    Select Case iApv
                        Case TipStaFluApv.Aprovado
                            sMsg.Append("| A ação está para SUA aprovação ou pedido de parecer.                                                                 |")
                        Case TipStaFluApv.Resposta_Parecer
                            sMsg.Append("| A ação está para SUA aprovação ou pedido de parecer.                                                                 |")
                        Case TipStaFluApv.Rejeitado
                            sMsg.Append("| A ação foi reprovada.                                                                                                |")
                        Case TipStaFluApv.Pedido_Parecer
                            sMsg.Append("| A ação está para SEU parecer.                                                                                        |")
                    End Select
            End Select

            Dim host As String = HttpContext.Current.Request.Url.Scheme & "://" & HttpContext.Current.Request.Url.Host
            If (Not IsNothing(HttpContext.Current.Request.Url.Port) AndAlso Not String.IsNullOrWhiteSpace(HttpContext.Current.Request.Url.Port)) Then
                host = host & ":" & HttpContext.Current.Request.Url.Port
            End If

            Dim link As String = ""

            Select Case tipo
                Case TipoAcao.PreAcordoValor
                    link = "| " & host & "/martins.acoesmercadologicas/AcaoComercial/PreAcordoCreateEditValor/" & CodAcoCmc
                Case TipoAcao.PreAcordoVigencia
                    link = "| " & host & "/martins.acoesmercadologicas/AcaoComercial/PreAcordoCreateEditVigencia/" & CodAcoCmc
                Case Else
                    link = "| " & host & "/martins.acoesmercadologicas/AcaoComercial/CreateEdit/" & CodAcoCmc
            End Select

            While link.Length < 119
                link = link & " "
            End While
            link = link & "|"

            sMsg.Append(link)
            sMsg.Append("|----------------------------------------------------------------------------------------------------------------------|")

        End If

        Return sMsg.ToString()


    End Function


    Public Sub IsrLnhCreEtn(ByVal TipMsgCreEtn As Decimal,
                                 ByVal NumSeqMsgCreEtn As Decimal,
                                 ByVal NumSeqLnhFisCreEnt As Decimal,
                                 ByVal DesTxtLnhFisCreEnt As String)

        Dim RelacaoDeLinhasDaMensagemDoCorreioEletronicoBO As New RelacaoDeLinhasDaMensagemDoCorreioEletronicoBO

        RelacaoDeLinhasDaMensagemDoCorreioEletronicoBO.IsrLnhCreEtn(TipMsgCreEtn,
                                       NumSeqMsgCreEtn,
                                       NumSeqLnhFisCreEnt,
                                       DesTxtLnhFisCreEnt)

    End Sub


    Private Function ApvAco(ByVal CodAcoCmc As Integer,
                            ByVal NumFluApv As Integer,
                            ByVal CodFncAtuAcoCmc As Integer,
                            ByVal NomFncAtuAcoCmc As String,
                            ByVal ObsApv As String,
                            ByVal CodFormPag As Decimal,
                            ByVal NumDias As Decimal,
                            Optional ByVal fluxoAtual As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao) = Nothing) As Boolean


        'Atualiza o flag desse fluxo para aprovado
        RelacaoAcaoComercialxFluxoAprovacaoBO.AtuFlgStaFluApvCmp(CodAcoCmc, NumFluApv, TipStaFluApv.Aprovado)


        'Atualiza o saldo reservado do fornecedor
        AtuAldRsvFrn(CodAcoCmc)

        'Atualiza o fluxo de aprovacao, plano de marketing e demais implicações
        AcaoComercialBO.AtuStaAcoCmc(CodAcoCmc, TipStaFluApv.Aprovado)

        'Chama a parte referente ao CBU300
        AcaoComercialBO.FunAtuApvAcoCmc(CodAcoCmc,
                                               CodFncAtuAcoCmc,
                                               TipStaFluApv.Aprovado,
                                               CodFormPag,
                                               NumDias)

        'Chama a parte refernte ao CBU345
        Dim tipAcao = AcaoComercialBO.BuscarAcaoComercialPorId(CodAcoCmc).TIPACOMCD
        GrvReqRsuIteAcoMcd(CodAcoCmc, CodFncAtuAcoCmc, tipAcao)

        'Grava a mensagem de email
        PcdMsgCreEtn(CodAcoCmc, 8, NumFluApv, 41, CodFncAtuAcoCmc, NomFncAtuAcoCmc, fluxoAtual)

        Return True

    End Function


    Private Sub AtuAldRsvFrn(ByVal CodAcoCmc As Integer)

        Dim vCodFrn As Integer
        Dim sEmpenhoCtt As String
        Dim sEmpenhoExaCtt As String
        Dim sEmpenhoCMV As String
        Dim sVlrSldRsvBnf As String
        Dim sFlgApvAcoCmc As String
        Dim sVlrTxaAdm As String
        Dim sVlrTxaBde As String

        Dim PercentualCtt As Double
        Dim PercentualExtraCtt As Double
        Dim Pos As Integer
        Dim dsDados As List(Of VO.RelacaoAcaoComercialxFornecedores)

        'Dim Ret As String

        sFlgApvAcoCmc = "S"

        dsDados = CnsSldRsvAcoCmc(CodAcoCmc)

        If Not IsNothing(dsDados) Then
            For Each drSldRsv In dsDados
                vCodFrn = drSldRsv.CODFRN

                If CLng(vCodFrn) <> 999999 Then

                    sEmpenhoCtt = Trim(drSldRsv.EmpenhoCtt)
                    sEmpenhoCMV = Trim(drSldRsv.EmpenhoCMV)

                    If CStr(drSldRsv.EmpenhoExaCtt).Trim() <> "" Then
                        Pos = InStr(drSldRsv.EmpenhoExaCtt, "-")

                        If Pos = 0 Then
                            sEmpenhoExaCtt = CStr(drSldRsv.EmpenhoExaCtt).Trim()
                        Else
                            sEmpenhoExaCtt = Trim(Left(drSldRsv.EmpenhoExaCtt, Pos - 1))
                        End If

                    End If

                    If drSldRsv.VlrAcordo > 0 Then

                        PercentualCtt = ((drSldRsv.VlrAcordo * 100) /
                        (drSldRsv.VlrAcordo + drSldRsv.VlrAcordoExa + drSldRsv.VlrReceitaCMV)) / 100

                        sVlrSldRsvBnf = Round((drSldRsv.VlrParticipacao.Value * PercentualCtt) +
                        drSldRsv.VlrTxaAdm.Value * PercentualCtt, 2)

                        sVlrTxaAdm = Round(drSldRsv.VlrTxaAdm.Value * PercentualCtt, 2)
                        sVlrTxaBde = Round(drSldRsv.VlrTxaBde.Value * PercentualCtt, 2)

                        AcaoComercialBO.PcdAtuAldRsvFrn(1,
                                                       vCodFrn,
                                                       CodAcoCmc,
                                                       sEmpenhoCtt,
                                                       sVlrSldRsvBnf,
                                                       sFlgApvAcoCmc,
                                                       "NULL",
                                                       sVlrTxaAdm,
                                                       sVlrTxaBde)

                    End If

                    If drSldRsv.VlrAcordoExa > 0 Then
                        PercentualExtraCtt = ((drSldRsv.VlrAcordoExa * 100) /
                                             (drSldRsv.VlrAcordo + drSldRsv.VlrAcordoExa + drSldRsv.VlrReceitaCMV)) / 100
                        sVlrSldRsvBnf = Round((drSldRsv.VlrParticipacao.Value * PercentualExtraCtt) +
                                        drSldRsv.VlrTxaAdm.Value * PercentualExtraCtt, 2)
                        sVlrTxaAdm = Round(drSldRsv.VlrTxaAdm.Value * PercentualExtraCtt, 2)
                        sVlrTxaBde = Round(drSldRsv.VlrTxaBde.Value * PercentualExtraCtt, 2)

                        AcaoComercialBO.PcdAtuAldRsvFrn(1,
                                                       vCodFrn,
                                                       CodAcoCmc,
                                                       sEmpenhoExaCtt,
                                                       sVlrSldRsvBnf,
                                                       sFlgApvAcoCmc,
                                                       "NULL",
                                                       sVlrTxaAdm,
                                                       sVlrTxaBde)

                    End If

                    If drSldRsv.VlrReceitaCMV > 0 Then
                        PercentualExtraCtt = ((drSldRsv.VlrReceitaCMV * 100) /
                                             (drSldRsv.VlrAcordo + drSldRsv.VlrAcordoExa + drSldRsv.VlrReceitaCMV)) / 100
                        sVlrSldRsvBnf = Round((drSldRsv.VlrParticipacao.Value * PercentualExtraCtt) +
                                        drSldRsv.VlrTxaAdm.Value * PercentualExtraCtt, 2)
                        sVlrTxaAdm = Round(drSldRsv.VlrTxaAdm.Value * PercentualExtraCtt, 2)
                        sVlrTxaBde = Round(drSldRsv.VlrTxaBde.Value * PercentualExtraCtt, 2)

                        AcaoComercialBO.PcdAtuAldRsvFrn(1,
                                                       vCodFrn,
                                                       CodAcoCmc,
                                                       sEmpenhoCMV,
                                                       sVlrSldRsvBnf,
                                                       sFlgApvAcoCmc,
                                                       "NULL",
                                                       sVlrTxaAdm,
                                                       sVlrTxaBde)

                    End If
                End If
            Next
        End If

    End Sub


    Public Function CnsSldRsvAcoCmc(ByVal CodAcoCmc As Integer) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim dsSldRsvAcoCmc As List(Of VO.RelacaoAcaoComercialxFornecedores)

        dsSldRsvAcoCmc = RelacaoAcaoComercialxFornecedoresBO.CnsSldRsvAcoCmc(CodAcoCmc)

        Return dsSldRsvAcoCmc

    End Function


    Public Function GrvReqRsuIteAcoMcd(ByVal iCodAcoCmc As Integer,
                                       ByVal CodFncAtuAcoCmc As String,
                                       ByVal tipo As TipoAcao) As Integer
        Dim strCodIdtTsc, CodAco As String
        Dim ValorMnmRsu, ValorAcoAtual As Double
        Dim ContIdtTsc, ContNumSeqIte As Integer
        Dim IDTEMPFRN, IDTLCLEMPFRN, IDTTERPGT As Integer
        Dim CodFnc, sCodIdtTsc As String
        Dim FlagEmail As Boolean
        Dim FlagFrnUnc As Char
        Dim CodUndOpe, QtdRegEft As Integer
        Dim dtsPsqCadFnc As List(Of VO.PER_ALL_PEOPLE_F)

        Dim sMsgAux As String = ""
        Dim NroSeqMsg As Integer = 0
        Dim TipMsgCreEtn As Integer = 22
        Dim CodSisInf As Integer = 8
        Dim sAux As String
        Dim cCulBra As New CultureInfo("pt-BR")
        Dim iAuxIni As Integer
        Dim iAux As Integer
        Dim sMecAux As String
        '
        Dim sIte As String
        Dim sVlr As String
        Dim sQde As String
        Dim sVlrTot As String
        Dim FlgIdtArzAtpPgt As String

        Dim _RelacaoDeLinhasDaMensagemDoCorreioEletronicoBO As New RelacaoDeLinhasDaMensagemDoCorreioEletronicoBO()
        Dim _RelacaoAcaoComercialPorItemOrcamentoBO As New RelacaoAcaoComercialPorItemOrcamentoBO()

        'Consulta todas as ações mercadologicas que serão usadas no processo
        Dim _AcoesItem As List(Of VO.RelacaoAcaoComercialPorItemOrcamento) = _RelacaoAcaoComercialPorItemOrcamentoBO.csnReqRsuIteAcoMcd(iCodAcoCmc)

        'Quantidade de registros processados
        QtdRegEft = _AcoesItem.Count

        If _AcoesItem.Count > 0 Then

            ' Inicializa Contadores
            ContNumSeqIte = 0
            Dim _ParametroSistemaAdminitracaoVendaBO As New ParametroSistemaAdminitracaoVendaBO()
            Dim _PoRequisitionsInterfaceAllBO As New PoRequisitionsInterfaceAllBO()
            Dim _perAllPeopleFBO As New perAllPeopleFBO()

            ' Pega Valor Minimo para geração da requisição
            ValorMnmRsu = _ParametroSistemaAdminitracaoVendaBO.ListarParametrosAcaoComercialEmp().VLRMNMITERSU
            CodUndOpe = _ParametroSistemaAdminitracaoVendaBO.csnCodUndOpe().CODUNDOPE

            CodAco = _AcoesItem.FirstOrDefault().CODACOCMC
            Dim _acao As VO.AcaoComercial = AcaoComercialBO.BuscaAcaoPorId(CodAco)
            ' Verifica número da último identicicador de transação gerado
            ContIdtTsc = _RelacaoAcaoComercialPorItemOrcamentoBO.cnsVlrMaxIdtRsu(CodAco)

            Dim _RelacaoItemPrincipalItemSubstitutoAcaoComercialBO As New RelacaoItemPrincipalItemSubstitutoAcaoComercialBO()
            ' Pega Valor da Primeira Acao Comercial
            ValorAcoAtual = _RelacaoItemPrincipalItemSubstitutoAcaoComercialBO.csnVlrAcoAtu(CodAco).FirstOrDefault().VLRUNTITEOCDACOCMC.GetValueOrDefault()

            For Each acaoItem As VO.RelacaoAcaoComercialPorItemOrcamento In _AcoesItem
                CodFnc = _acao.CODFNCCRIACOCMC
                dtsPsqCadFnc = _perAllPeopleFBO.PsqCadFnc(CodFnc)


                If dtsPsqCadFnc Is Nothing Then
                    Return -2
                Else
                    If dtsPsqCadFnc.Count = 0 Then
                        Return -2
                    End If
                End If
                strCodIdtTsc = String.Format("{0}{1}", acaoItem.CODACOCMC, Format(acaoItem.NUMLNHITEOCDACOCMC, "000"))

                If acaoItem.IDTEMPFRNITERSU <> 0 Then
                    FlagFrnUnc = "Y"
                    IDTEMPFRN = acaoItem.IDTEMPFRNITERSU
                    IDTLCLEMPFRN = acaoItem.IDTLCLEMPFRNITERSU
                    IDTTERPGT = acaoItem.IDTTERPGTITERSU
                Else
                    FlagFrnUnc = "N"
                    IDTEMPFRN = 0
                    IDTLCLEMPFRN = 0
                    IDTTERPGT = 0
                End If

                If CodAco <> acaoItem.CODACOCMC Then
                    CodAco = acaoItem.CODACOCMC
                    'ValorAcoAtual = _RelacaoItemPrincipalItemSubstitutoAcaoComercialBO.csnVlrAcoAtu(CodAco).FirstOrDefault().VLRUNTITEOCDACOCMC.GetValueOrDefault()
                    FlagEmail = False
                    ContIdtTsc = _RelacaoAcaoComercialPorItemOrcamentoBO.cnsVlrMaxIdtRsu(CodAco)
                    ContNumSeqIte = 0
                End If

                If (ValorAcoAtual > ValorMnmRsu) Or (IDTEMPFRN <> 0) Then
                    If IDTEMPFRN <> 0 Then
                        ContIdtTsc = ContIdtTsc + 1
                        ContNumSeqIte = 1
                    Else
                        ContNumSeqIte = ContNumSeqIte + 1
                    End If
                    '
                    sCodIdtTsc = CodAco.ToString() + Format(ContIdtTsc, "000")
                    '
                    If acaoItem.INDITERSUARZPRVPGT = 0 Then
                        FlgIdtArzAtpPgt = "N"
                    Else
                        FlgIdtArzAtpPgt = "Y"
                    End If
                    Dim USER_ID As Integer = _perAllPeopleFBO.CsnIdtUsu(CodFnc)

                    _PoRequisitionsInterfaceAllBO.IsrReqCmpMatCsm(USER_ID _
                                                                , CodAco _
                                                                , IDTLCLEMPFRN _
                                                                , FlgIdtArzAtpPgt _
                                                                , IDTTERPGT _
                                                                , acaoItem.NUMRMLTLF _
                                                                , acaoItem.QDEITEOCDACOCMC _
                                                                , acaoItem.VLRUNTITEOCDACOCMC _
                                                                , dtsPsqCadFnc.FirstOrDefault().PERSON_ID _
                                                                , acaoItem.DESOBSCPR _
                                                                , CInt(acaoItem.IDTITERSU) _
                                                                , CInt(acaoItem.IDTCNTCTB) _
                                                                , CInt(dtsPsqCadFnc.FirstOrDefault().INVENTORY_ORGANIZATION_ID) _
                                                                , CInt(dtsPsqCadFnc.FirstOrDefault().LOCATION_ID) _
                                                                , acaoItem.DATUTZITEACOCMC _
                                                                , CodUndOpe _
                                                                , CInt(acaoItem.NUMCTTFRN) _
                                                                , CInt(acaoItem.NUMLNHCTTRSU))

                    _RelacaoAcaoComercialPorItemOrcamentoBO.AtlIdtRsu(New VO.RelacaoAcaoComercialPorItemOrcamento With {
                                                                        .CODIDTTSC = CInt(sCodIdtTsc),
                                                                        .CODACOCMC = CInt(CodAco),
                                                                        .NUMLNHITEOCDACOCMC = acaoItem.NUMLNHITEOCDACOCMC
                                                                      })
                Else
                    If FlagEmail = False Then
                        '
                        Select Case tipo
                            Case TipoAcao.PreAcordoValor, TipoAcao.PreAcordoVigencia
                                sAux = "ITENS DE REQUISICAO INFERIOR AO VALOR MÍNIMO DO PRÉ-ACORDO PROMOCIONAL   " & CStr(CodAco) & " - " & _acao.NOMACOCMC.Trim() & " ".Trim()
                            Case Else
                                sAux = "ITENS DE REQUISICAO INFERIOR AO VALOR MÍNIMO AÇÃO   " & CStr(CodAco) & " - " & _acao.NOMACOCMC.Trim() & " ".Trim()
                        End Select
                        If sAux.Length > 80 Then
                            sAux = sAux.Substring(0, 80)
                        End If

                        NroSeqMsg = CnsPrxNumSeqCreEtn(TipMsgCreEtn)

                        If Me.IsrMsgCreEtn(TipMsgCreEtn, NroSeqMsg, sAux) Then
                            IsrEndCreEtn(TipMsgCreEtn, NroSeqMsg, 1, 1, LCase(_acao.NOMUSRRCF.Trim()) & "@martins.com.br")
                            IsrEndCreEtn(TipMsgCreEtn, NroSeqMsg, 2, 1, LCase(_acao.NOMUSRRCF.Trim()) & "@martins.com.br")
                        End If

                        Select Case tipo
                            Case TipoAcao.PreAcordoValor, TipoAcao.PreAcordoVigencia
                                sAux = "Segue abaixo os itens do pré-acordo promocional mercadológica "
                            Case Else
                                sAux = "Segue abaixo os itens da ação mercadológica "
                        End Select
                        sAux = sAux & CodAco.Trim() & " - " & _acao.NOMACOCMC.Trim() & " " &
                        "que não geraram requisição ao suprimentos devido  a  somatória do valor " &
                        "dos mesmos serem  inferior ao valor mínimo " & CDbl(ValorMnmRsu).ToString("c", cCulBra).Trim() & " Os mesmos deverão " &
                        "ser alterados no sistema de ações mercadológicas incluindo o fornecedor " &
                        "e a condição de pagto para serem resubmetidos. "
                        '
                        iAux = (sAux.Length / 120)
                        iAuxIni = 0
                        '
                        While iAux > 0
                            '
                            If sAux.Length <= 120 Then
                                sMecAux = sAux
                                sMsgAux &= sMecAux
                                If sMecAux.Length < 120 Then
                                    For i As Integer = 0 To (120 - sMecAux.Length) - 1
                                        sMsgAux &= " "
                                    Next
                                End If
                            ElseIf sAux.Length > ((iAuxIni * 120) + 120) Then
                                sMsgAux &= sAux.Substring((iAuxIni * 120), 120)
                            Else
                                sMecAux = sAux.Substring((iAuxIni * 120), sAux.Length - (iAuxIni * 120))
                                sMsgAux &= sMecAux
                                If sMecAux.Length < 120 Then
                                    For i As Integer = 0 To (120 - sMecAux.Length) - 1
                                        sMsgAux &= " "
                                    Next
                                End If
                            End If
                            '
                            iAuxIni += 1
                            iAux -= 1
                            '
                        End While
                        '
                        'Linha em branco
                        For i As Integer = 0 To 120 - 1
                            sMsgAux &= " "
                        Next
                        '
                        sMsgAux &= "+------------------------------------------------------------------------------+-------------+-----------+-------------+"
                        sMsgAux &= "| ITEM                                                                         | VALOR       | QTDE      | VALOR       |"
                        sMsgAux &= "| UNITARIO                                                                     | UNT.        |           | TOTAL       |"
                        sMsgAux &= "+------------------------------------------------------------------------------+-------------+-----------+-------------+"

                        FlagEmail = True
                    End If
                    ' Gera Items
                    '
                    sIte = acaoItem.CODITEOCDACOCMC.Trim()
                    sVlr = CDbl(acaoItem.VLRUNTITEOCDACOCMC).ToString("N", cCulBra).Trim()
                    sQde = acaoItem.QDEITEOCDACOCMC
                    sVlrTot = CDbl(CDbl(acaoItem.VLRUNTITEOCDACOCMC)) *
                              CDbl(acaoItem.QDEITEOCDACOCMC).ToString("N", cCulBra).Trim()
                    '
                    If sIte.Length > 78 Then
                        sMsgAux &= "|" & sIte.Substring(0, 78)
                    Else
                        sMsgAux &= "|" & sIte
                        If sIte.Length < 78 Then
                            For i As Integer = 0 To (78 - sIte.Length) - 1
                                sMsgAux &= " "
                            Next
                        End If
                    End If
                    '
                    If sVlr.Length > 13 Then
                        sMsgAux &= "|" & sVlr.Substring(0, 13)
                    Else
                        sMsgAux &= "|"
                        If sVlr.Length < 13 Then
                            For i As Integer = 0 To (13 - sVlr.Length) - 1
                                sMsgAux &= " "
                            Next
                        End If
                        sMsgAux &= sVlr
                    End If
                    '
                    If sQde.Length > 11 Then
                        sMsgAux &= "|" & sQde.Substring(0, 11)
                    Else
                        sMsgAux &= "|"
                        If sQde.Length < 11 Then
                            For i As Integer = 0 To (11 - sQde.Length) - 1
                                sMsgAux &= " "
                            Next
                        End If
                        sMsgAux &= sQde
                    End If
                    '
                    If sVlrTot.Length > 13 Then
                        sMsgAux &= "|" & sVlrTot.Substring(0, 13)
                    Else
                        sMsgAux &= "|"
                        If sVlrTot.Length < 13 Then
                            For i As Integer = 0 To (13 - sVlrTot.Length) - 1
                                sMsgAux &= " "
                            Next
                        End If
                        sMsgAux &= sVlrTot
                    End If
                    '
                    sMsgAux &= "|"
                    _RelacaoAcaoComercialPorItemOrcamentoBO.AltIndIteReqRsuVlrMnm(New VO.RelacaoAcaoComercialPorItemOrcamento With {.CODACOCMC = CInt(CodAco), .NUMLNHITEOCDACOCMC = acaoItem.NUMLNHITEOCDACOCMC})
                End If
            Next

            If FlagEmail = True Then
                sMsgAux &= "+------------------------------------------------------------------------------+-------------+-----------+-------------+"
                Select Case tipo
                    Case TipoAcao.PreAcordoValor, TipoAcao.PreAcordoVigencia
                        sAux = "TOTAL DO PRÉ-ACORDO PROMOCIONAL " & CDbl(ValorAcoAtual).ToString("N", cCulBra).Trim()
                    Case Else
                        sAux = "TOTAL DA AÇÃO " & CDbl(ValorAcoAtual).ToString("N", cCulBra).Trim()
                End Select
                If sAux.Length > 120 Then
                    sMsgAux &= sAux.Substring(0, 120)
                Else
                    sMsgAux &= sAux
                    If sAux.Length < 120 Then
                        For i As Integer = 0 To (120 - sAux.Length) - 1
                            sMsgAux &= " "
                        Next
                    End If
                End If
            End If
        End If

        If sMsgAux.Length > 0 Then
            _RelacaoDeLinhasDaMensagemDoCorreioEletronicoBO.IsrMsgLnh(TipMsgCreEtn,
                      NroSeqMsg,
                      sMsgAux)
        End If

        Return QtdRegEft 'Retorna Quantidade de registros processados
    End Function


    Public Function PcdMsgCreEtn(ByVal CodAcoCmc As Integer,
                                 ByVal CodSisInf As Integer,
                                 ByVal NumFluApv As Integer,
                                 ByVal TipMsgCreEtn As Integer,
                                 ByVal CodFnc As Integer,
                                 ByVal NomFnc As String,
                                 Optional ByVal fluxoAtual As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao) = Nothing) As Integer

        Dim iPrxNumSeqCreEtn As Integer
        Dim dsApvEma As List(Of VO.FluxoDeAprovacao)
        Dim MsgEnv As String = ""
        Dim iNumSeqEndCreEtn As Integer
        Dim sPedacoMsg As String = ""
        Dim sMsgEnvAux As String = ""
        Dim iAuxIni As Integer
        Dim iAux As Single


        iPrxNumSeqCreEtn = CnsPrxNumSeqCreEtn(TipMsgCreEtn)

        Dim msg = ""

        Dim acao = AcaoComercialBO.BuscaAcaoPorId(CodAcoCmc)
        If (acao.TIPACOMCD = TipoAcao.PreAcordoValor OrElse acao.TIPACOMCD = TipoAcao.PreAcordoVigencia) Then
            msg = "Aviso de aprovação final do Pré-Acordo Promocional - "
        Else
            msg = "Aviso de aprovação final da Ação Mercadológica - "
        End If

        If Me.IsrMsgCreEtn(TipMsgCreEtn,
                          iPrxNumSeqCreEtn,
                          msg & CodAcoCmc.ToString()) Then

            dsApvEma = CnsUsrNotEmaAntigo(CodSisInf, NumFluApv)

            If Not IsNothing(dsApvEma) Then

                iNumSeqEndCreEtn = 0

                If dsApvEma.Count > 0 Then
                    iNumSeqEndCreEtn += 1
                    IsrEndCreEtn(TipMsgCreEtn,
                                   iPrxNumSeqCreEtn,
                                   1,
                                   iNumSeqEndCreEtn,
                                   dsApvEma.FirstOrDefault.NOMUSRRCF.Trim().ToLower() & "@martins.com.br")


                    iNumSeqEndCreEtn = 0
                    For Each drApvEma In dsApvEma

                        iNumSeqEndCreEtn += 1
                        IsrEndCreEtn(TipMsgCreEtn,
                                        iPrxNumSeqCreEtn,
                                        2,
                                        iNumSeqEndCreEtn,
                                        CStr(drApvEma.NOMUSRRCF).Trim().ToLower() & "@martins.com.br")

                    Next

                    MsgEnv = MntMsgEma(CodAcoCmc, fluxoAtual)

                    'Looping para gravar de 120 em 120 caracteres no banco = 1 linha do email
                    iAux = (MsgEnv.Length / 120)
                    iAuxIni = 0


                    While iAux > 0

                        If MsgEnv.Length <= 120 Then
                            sPedacoMsg = MsgEnv
                        ElseIf MsgEnv.Length > ((iAuxIni * 120) + 120) Then
                            sPedacoMsg = MsgEnv.Substring((iAuxIni * 120), 120)
                        Else
                            sPedacoMsg = MsgEnv.Substring((iAuxIni * 120), MsgEnv.Length - (iAuxIni * 120))
                        End If

                        Me.IsrLnhCreEtn(TipMsgCreEtn,
                                        iPrxNumSeqCreEtn,
                                        iAuxIni + 1,
                                        sPedacoMsg)


                        iAuxIni += 1
                        iAux -= 1

                    End While

                End If

            End If

        End If

    End Function


    Public Function CnsUsrNotEmaAntigo(ByVal CodSisInf As Integer,
                                 ByVal NumSeqApv As Integer) As List(Of VO.FluxoDeAprovacao)

        Dim dsApvEma As List(Of VO.FluxoDeAprovacao)

        dsApvEma = DirectCast(daoBase, FluxoDeAprovacaoDAO).CnsUsrNotEma(CodSisInf, NumSeqApv)

        If IsNothing(dsApvEma) Then dsApvEma = New List(Of VO.FluxoDeAprovacao)

        Return dsApvEma
    End Function


    Private Function MntMsgEma(ByVal CodAcoCmc As Integer, fluxoAtual As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)) As String

        Dim sMsgCpa, sMsgRgrAcoCmc, sMsgPolAcoCmc As String
        Dim sMsgPti, sMsgSldPlnMrk, sMsgOcd, sMsgIct As String
        Dim sMsgFlu As String
        Dim sMsg As StringBuilder
        Dim dsDdoAco As VO.AcaoComercial
        Dim iAux As Integer

        dsDdoAco = BscDdoAcoEma(CodAcoCmc)

        If IsNothing(sMsg) Then
            sMsg = New StringBuilder
        End If

        If Not IsNothing(dsDdoAco) Then

            'Adição da capa da ação
            If Not IsNothing(dsDdoAco.CODACOCMC) Then
                sMsgCpa = MntMsgCab(dsDdoAco, iAux, dsDdoAco.TIPACOMCD)

                If sMsgCpa <> "" Then
                    iAux = 1
                    sMsg.Append(sMsgCpa)
                End If

            End If

            'Adição da mecanica da ação
            If Not IsNothing(dsDdoAco.Mecanica) Then
                sMsgRgrAcoCmc = MntMsgRgrAcoCmc(dsDdoAco.Mecanica, iAux)

                If sMsgRgrAcoCmc <> "" Then
                    iAux = 1
                    sMsg.Append(sMsgRgrAcoCmc)
                End If

            End If

            'Adição das politicas divergentes
            If Not IsNothing(dsDdoAco.PoliticasDivergentes) Then
                sMsgPolAcoCmc = MntMsgPolAcoCmc(dsDdoAco.PoliticasDivergentes, iAux)

                If sMsgPolAcoCmc <> "" Then
                    iAux = 1
                    sMsg.Append(sMsgPolAcoCmc)
                End If

            End If

            'Adição dos patrocinadores
            If Not IsNothing(dsDdoAco.Fornecedores) Then
                sMsgPti = MntMsgPti(dsDdoAco.Fornecedores, iAux)

                If sMsgPti <> "" Then
                    iAux = 1
                    sMsg.Append(sMsgPti)
                End If

            End If

            'Adição do saldo plano de marketing
            If Not IsNothing(dsDdoAco.PlanosDeMarketing) Then
                sMsgSldPlnMrk = MntMsgSldPlnMrk(dsDdoAco.PlanosDeMarketing, iAux, dsDdoAco.TIPACOMCD.Value)

                If sMsgPti <> "" Then
                    iAux = 1
                    sMsg.Append(sMsgSldPlnMrk)
                End If

            End If

            'Adição do orçamento
            If Not IsNothing(dsDdoAco.ItensOrcamento) Then
                sMsgOcd = MntMsgOcd(dsDdoAco.ItensOrcamento, iAux)

                If sMsgOcd <> "" Then
                    sMsg.Append(sMsgOcd)
                End If

            End If

            'Adição dos incentivos
            If Not IsNothing(dsDdoAco.Incentivos) Then
                sMsgIct = MntMsgIct(dsDdoAco.Incentivos, iAux)

                If sMsgIct <> "" Then
                    sMsg.Append(sMsgIct)
                End If

            End If

            'Adição do Fluxo de Aprovação
            If Not IsNothing(fluxoAtual) Then
                sMsgFlu = MntMsgFlu(fluxoAtual, iAux)

                If sMsgFlu <> "" Then
                    sMsg.Append(sMsgFlu)
                End If

            End If
        End If

        Return sMsg.ToString()



    End Function

    Private Function MntMsgFlu(ByVal dtFlu As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao),
                               ByVal iIndAux As Integer) As String
        Dim sMsgAux As StringBuilder
        Dim iAux As Integer = 0
        Dim sMsg As String
        Dim iTipStaAco As Integer
        Dim iAuxIni As Integer
        Dim iAuxNro As Single
        Dim sMecAux As String

        sMsgAux = New StringBuilder

        If iIndAux = 1 Then
            sMsgAux.Append("                                                                                                                        ")
        End If
        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
        sMsgAux.Append("| FLUXO                                                                                                                |")
        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")

        For Each drFlu In dtFlu
            Dim aprovou As Boolean = False
            If CInt(drFlu.NUMSEQNIVAPV) < 400 Then
                iAux += 1
                sMsg = ""
                iTipStaAco = drFlu.TIPSTAFLUAPV

                If CStr(drFlu.NUMSEQNIVAPV).Trim() = "0" Then
                    Select Case iTipStaAco
                        Case TipStaFluApv.Criacao
                            sMsg = iAux.ToString() & " - " & "ENVIADO PARA APROVAÇÃO POR " &
                            CStr(drFlu.NOMFNC).Trim().ToUpper() & " Dia " &
                            Format(drFlu.DATHRAFLUAPV, "dd/MM/yyyy HH : mm : ss")
                            aprovou = True
                    End Select
                Else
                    Select Case iTipStaAco
                        Case TipStaFluApv.Aprovado

                            If Not IsDBNull(drFlu.NOMFNCARZ) AndAlso Not IsNothing(drFlu.NOMFNCARZ) Then
                                sMsg = iAux.ToString() & " - " & "APROVADO POR " &
                                CStr(drFlu.NOMFNCARZ).Trim().ToUpper() & " NO LUGAR DE " &
                                CStr(drFlu.NOMFNC).Trim().ToUpper() & " " &
                                " DIA " &
                                Format(drFlu.DATHRAAPVFLU, "dd/MM/yyyy HH : mm : ss")
                                aprovou = True
                            Else
                                sMsg = iAux.ToString() & " - " & "APROVADO POR " &
                                CStr(drFlu.NOMFNC).Trim().ToUpper() & " DIA " &
                                Format(drFlu.DATHRAAPVFLU, "dd/MM/yyyy HH : mm : ss")
                                aprovou = True
                            End If

                        Case TipStaFluApv.Resposta_Parecer

                            If Not IsDBNull(drFlu.NOMFNCARZ) AndAlso Not IsNothing(drFlu.NOMFNCARZ) Then
                                sMsg = iAux.ToString() & " - " & "PARECER DADO POR " &
                                CStr(drFlu.NOMFNCARZ).Trim().ToUpper() & " NO LUGAR DE " &
                                CStr(drFlu.NOMFNC).Trim().ToUpper() & " PEDIDO POR " &
                                dtFlu.Where(Function(f) f.NUMSEQFLUAPV = f.NUMSEQFLUAPVPEDOPN).FirstOrDefault.NOMFNC.Trim().ToUpper() &
                                " DIA " &
                                Format(drFlu.DATHRAAPVFLU, "dd/MM/yyyy HH : mm : ss")
                                aprovou = True
                            Else
                                sMsg = iAux.ToString() & " - " & "PARECER DADO POR " &
                                CStr(drFlu.NOMFNC.Trim().ToUpper() & " PEDIDO POR " &
                                dtFlu.Where(Function(f) f.NUMSEQFLUAPV = f.NUMSEQFLUAPVPEDOPN).First.NOMFNC.Trim().ToUpper() &
                                " DIA " &
                                Format(drFlu.DATHRAAPVFLU, "dd/MM/yyyy HH : mm : ss"))
                                aprovou = True
                            End If

                    End Select
                End If

                If (aprovou) Then
                    If CStr(drFlu.NUMSEQNIVAPV).StartsWith("10") Then
                        sMsg &= " (GERENTE DE MARKETING)"
                    ElseIf CStr(drFlu.NUMSEQNIVAPV).StartsWith("20") Then
                        sMsg &= " (DIRETOR DE CATEGORIA)"
                    ElseIf CStr(drFlu.NUMSEQNIVAPV).StartsWith("30") Then
                        sMsg &= " (DIRETOR DE COMPRAS)"
                    End If
                Else
                    iAux -= 1
                End If


                iAuxNro = (sMsg.Length / 118)
                iAuxIni = 0

                While iAuxNro > 0
                    '
                    If sMsg.Length <= 118 Then
                        sMecAux = sMsg
                        sMsgAux.Append("|" & sMecAux)
                        If sMecAux.Length < 118 Then
                            For i As Integer = 0 To (118 - sMecAux.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                        sMsgAux.Append("|")
                    ElseIf sMsg.Length > ((iAuxIni * 118) + 118) Then
                        sMsgAux.Append("|" & sMsg.Substring((iAuxIni * 118), 118))
                        sMsgAux.Append("|")
                    Else
                        sMecAux = sMsg.Substring((iAuxIni * 118), sMsg.Length - (iAuxIni * 118))
                        sMsgAux.Append("|" & sMecAux)
                        If sMecAux.Length < 118 Then
                            For i As Integer = 0 To (118 - sMecAux.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                        sMsgAux.Append("|")
                    End If

                    iAuxIni += 1
                    iAuxNro -= 1

                End While
            End If
        Next

        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If
    End Function


    Private Function MntMsgIct(ByVal dtIct As List(Of VO.RelacaoIncentivoxAcaoComercial),
                               ByVal iIndAux As Integer) As String
        Dim sMsgAux As StringBuilder
        Dim sIct, sVal, sRsp, sMec, sMecAux As String
        Dim iAuxIni As Integer
        Dim iAux As Single


        sMsgAux = New StringBuilder

        If iIndAux = 1 Then
            sMsgAux.Append("                                                                                                                        ")
        End If
        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
        sMsgAux.Append("| DETALHAMENTO INCENTIVO                                                                                               |")
        sMsgAux.Append("+--------------------------------------+-----------------------+-----------------------------+-------------------------+")
        sMsgAux.Append("| INCENTIVO                            | VALIDADE              | RESPONSÁVEL                 | MECANICA INCENTIVO      |")
        sMsgAux.Append("+--------------------------------------+-----------------------+-----------------------------+-------------------------+")

        For Each drIct In dtIct

            sIct = CStr(drIct.CODICT).Trim() & " - " & CStr(drIct.DESICT).Trim()
            sVal = Format(drIct.DATINIEFTIC, "dd/MM/yyyy") & " - " & Format(drIct.DATFIMEFTICT, "dd/MM/yyyy")
            sRsp = CStr(drIct.CODUSRCADICT).Trim() & " - " & CStr(drIct.NOMFNC).Trim()
            sMec = CStr(drIct.DESRDCRGRICT).Trim()

            If sIct.Length > 38 Then
                sMsgAux.Append("|" & sIct.Substring(0, 38))
            Else
                sMsgAux.Append("|" & sIct)
                If sIct.Length < 38 Then
                    For i As Integer = 0 To (38 - sIct.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            If sVal.Length > 23 Then
                sMsgAux.Append("|" & sVal.Substring(0, 23))
            Else
                sMsgAux.Append("|" & sVal)
                If sVal.Length < 23 Then
                    For i As Integer = 0 To (23 - sVal.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            If sRsp.Length > 29 Then
                sMsgAux.Append("|" & sRsp.Substring(0, 29))
            Else
                sMsgAux.Append("|" & sRsp)
                If sRsp.Length < 29 Then
                    For i As Integer = 0 To (29 - sRsp.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            iAux = (sMec.Length / 25)
            iAuxIni = 0

            While iAux > 0

                If sMec.Length <= 25 Then
                    sMecAux = sMec
                    sMsgAux.Append("|" & sMec)
                    If sMecAux.Length < 25 Then
                        For i As Integer = 0 To (25 - sMecAux.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append("|")
                ElseIf sMec.Length > ((iAuxIni * 25) + 25) Then
                    sMsgAux.Append("|" & sMec.Substring((iAuxIni * 25), 25))
                    sMsgAux.Append("|")
                Else
                    sMecAux = sMec.Substring((iAuxIni * 25), sMec.Length - (iAuxIni * 25))
                    sMsgAux.Append("|" & sMecAux)
                    If sMecAux.Length < 25 Then
                        For i As Integer = 0 To (25 - sMecAux.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append("|")
                End If

                If iAux > 1 Then
                    sMsgAux.Append("|                                      |                       |                             ")
                End If

                iAuxIni += 1
                iAux -= 1

            End While

        Next

        sMsgAux.Append("+--------------------------------------+-------------------------------+---------------------+-------------------------+")

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If
    End Function

    Private Function MntMsgOcd(ByVal dtOcd As List(Of VO.RelacaoAcaoComercialPorItemOrcamento),
                               ByVal iIndAux As Integer) As String
        Dim sMsgAux As StringBuilder
        Dim sItem, sNumLin As String
        Dim sTipOpe, sVlrUni, sQde, sVlrTot As String
        Dim cCulBra As CultureInfo
        Dim dTotVlrOrc, dTotVlrBde, dVlrTotVlrCli As Double

        cCulBra = New CultureInfo("pt-BR")

        sMsgAux = New StringBuilder

        If iIndAux = 1 Then
            sMsgAux.Append("                                                                                                                        ")
        End If
        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
        sMsgAux.Append("| ORÇAMENTO                                                                                                            |")
        sMsgAux.Append("+--+----------------------------------------------+-----------------------------+--------------+--------+--------------+")
        sMsgAux.Append("|  | ITEM DE ORÇAMENTO                            | TIPO OPERAÇÃO               | VALOR UNIT.  | QUANT. | VALOR TOTAL  |")
        sMsgAux.Append("+--+----------------------------------------------+-----------------------------+--------------+--------+--------------+")

        For Each drOcd In dtOcd

            sNumLin = CStr(drOcd.NUMLNHITEOCDACOCMC).Trim()
            sItem = CStr(drOcd.CODITEOCDACOCMC).Trim()
            sTipOpe = CStr(drOcd.CODOPEFSCDSNACOCMC).Trim() & " - " &
            CStr(drOcd.DESOPEFSCDSNACOCMC).Trim()

            sVlrUni = CDbl(drOcd.VLRUNTITEOCDACOCMC).ToString("N", cCulBra).Trim()
            sQde = CDbl(drOcd.QDEITEOCDACOCMC).ToString("N", cCulBra).Trim()
            sVlrTot = CDbl(CDbl(drOcd.QDEITEOCDACOCMC) * CDbl(drOcd.VLRUNTITEOCDACOCMC)).ToString("N", cCulBra).Trim()

            If drOcd.INDBDEOPEDSNACOCMC = 1 Then
                dTotVlrBde += CDbl(drOcd.QDEITEOCDACOCMC) * CDbl(drOcd.VLRPTCFRNITEACOCMC)
            Else
                dTotVlrOrc += CDbl(drOcd.QDEITEOCDACOCMC) * CDbl(drOcd.VLRPTCFRNITEACOCMC)
            End If
            dVlrTotVlrCli += CDbl(drOcd.QDEITEOCDACOCMC) * CDbl(drOcd.VLRPTCCLIITEACOCMC)

            If sNumLin.Length > 2 Then
                sMsgAux.Append("|" & sNumLin.Substring(0, 2))
            Else
                sMsgAux.Append("|")
                If sNumLin.Length < 2 Then
                    For i As Integer = 0 To (2 - sNumLin.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sNumLin)
            End If

            If sItem.Length > 46 Then
                sMsgAux.Append("|" & sItem.Substring(0, 46))
            Else
                sMsgAux.Append("|" & sItem)
                If sItem.Length < 46 Then
                    For i As Integer = 0 To (46 - sItem.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            If sTipOpe.Length > 29 Then
                sMsgAux.Append("|" & sTipOpe.Substring(0, 29))
            Else
                sMsgAux.Append("|" & sTipOpe)
                If sTipOpe.Length < 29 Then
                    For i As Integer = 0 To (29 - sTipOpe.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            If sVlrUni.Length > 14 Then
                sMsgAux.Append("|" & sVlrUni.Substring(0, 14))
            Else
                sMsgAux.Append("|")
                If sVlrUni.Length < 14 Then
                    For i As Integer = 0 To (14 - sVlrUni.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sVlrUni)
            End If

            If sQde.Length > 8 Then
                sMsgAux.Append("|" & sQde.Substring(0, 8))
            Else
                sMsgAux.Append("|")
                If sQde.Length < 8 Then
                    For i As Integer = 0 To (8 - sQde.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sQde)
            End If

            If sVlrTot.Length > 14 Then
                sMsgAux.Append("|" & sVlrTot.Substring(0, 14))
            Else
                sMsgAux.Append("|")
                If sVlrTot.Length < 14 Then
                    For i As Integer = 0 To (14 - sVlrTot.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sVlrTot)
            End If

            sMsgAux.Append("|")

        Next

        sMsgAux.Append("+--+----------------------------------------------+-----------------------------+--------------+--------+--------------+")

        sVlrTot = "| TOTAL VLR CLIENTE " & dVlrTotVlrCli.ToString("N", cCulBra).Trim() &
                   "    TOTAL VLR ORÇAMENTO " & dTotVlrOrc.ToString("N", cCulBra).Trim() &
                   "    TOTAL VLR BRINDE " & dTotVlrBde.ToString("N", cCulBra).Trim()

        sMsgAux.Append("                                                                                                                        ")
        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")

        If sVlrTot.Length > 119 Then
            sMsgAux.Append(sVlrTot.Substring(0, 119))
        Else
            If sVlrTot.Length < 119 Then
                For i As Integer = 0 To (119 - sVlrTot.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
            sMsgAux.Append(sVlrTot)
        End If
        sMsgAux.Append("|")

        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If

    End Function

    Private Function MntMsgSldPlnMrk(ByVal dtSldPlnMrk As List(Of VO.RelacaoAcaoComercialxPlanoMarketing),
                                     ByVal iIndAux As Integer, ByVal tipAcao As Decimal) As String
        Dim sMsgAux As StringBuilder
        Dim sBU, sSldMns, sSldAtu, sSldAnu As String
        Dim sVlrAco, sRstMns, sRstAtu, sRstAnu As String
        Dim cCulBra As CultureInfo

        cCulBra = New CultureInfo("pt-BR")



        sMsgAux = New StringBuilder

        If iIndAux = 1 Then
            sMsgAux.Append("                                                                                                                        ")
        End If


        Select Case tipAcao
            Case TipoAcao.PreAcordoValor, TipoAcao.PreAcordoVigencia
                sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                sMsgAux.Append("| SALDO PLANO DE MARKETING                                                                                             |")
                sMsgAux.Append("+-------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+")
                sMsgAux.Append("| BU          | SALDO        | SALDO        | SALDO        | VALOR        | RESULTADO    | RESULTADO    | RESULTADO    |")
                sMsgAux.Append("|             | MENSAL       | ATUAL        | ANUAL        | PRÉ-ACORDO   | MENSAL       | ATUAL        | ANUAL        |")
                sMsgAux.Append("+-------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+")
            Case Else
                sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                sMsgAux.Append("| SALDO PLANO DE MARKETING                                                                                             |")
                sMsgAux.Append("+-------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+")
                sMsgAux.Append("| BU          | SALDO        | SALDO        | SALDO        | VALOR        | RESULTADO    | RESULTADO    | RESULTADO    |")
                sMsgAux.Append("|             | MENSAL       | ATUAL        | ANUAL        | AÇÃO         | MENSAL       | ATUAL        | ANUAL        |")
                sMsgAux.Append("+-------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+")
        End Select


        For Each drSldPlnMrk In dtSldPlnMrk

            If (IsNothing(drSldPlnMrk.VLRACAO)) Then
                drSldPlnMrk.VLRACAO = 0
            End If

            sBU = CStr(drSldPlnMrk.CODUNDESRNGC).Trim() & " - " &
                  CStr(drSldPlnMrk.DESUNDESRNGC).Trim()
            sSldMns = CDbl(drSldPlnMrk.VLRSLDMNSPLNMKT).ToString("N", cCulBra).Trim()
            sSldAtu = CDbl(drSldPlnMrk.VLRSLDATUPLNMKT).ToString("N", cCulBra).Trim()
            sSldAnu = CDbl(drSldPlnMrk.VLRSLDANOPLNMKT).ToString("N", cCulBra).Trim()
            sVlrAco = CDbl(drSldPlnMrk.VLRACAO).ToString("N", cCulBra).Trim()
            sRstMns = CDbl(CDbl(drSldPlnMrk.VLRSLDMNSPLNMKT) - CDbl(drSldPlnMrk.VLRACAO)).ToString("N", cCulBra).Trim()
            sRstAtu = CDbl(CDbl(drSldPlnMrk.VLRSLDATUPLNMKT) - CDbl(drSldPlnMrk.VLRACAO)).ToString("N", cCulBra).Trim()
            sRstAnu = CDbl(CDbl(drSldPlnMrk.VLRSLDANOPLNMKT) - CDbl(drSldPlnMrk.VLRACAO)).ToString("N", cCulBra).Trim()

            If sBU.Length > 13 Then
                sMsgAux.Append("|" & sBU.Substring(0, 13))
            Else
                sMsgAux.Append("|" & sBU)
                If sBU.Length < 13 Then
                    For i As Integer = 0 To (13 - sBU.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            If sSldMns.Length > 14 Then
                sMsgAux.Append("|" & sSldMns.Substring(0, 14))
            Else
                sMsgAux.Append("|")
                If sSldMns.Length < 14 Then
                    For i As Integer = 0 To (14 - sSldMns.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sSldMns)
            End If

            If sSldAtu.Length > 14 Then
                sMsgAux.Append("|" & sSldAtu.Substring(0, 14))
            Else
                sMsgAux.Append("|")
                If sSldAtu.Length < 14 Then
                    For i As Integer = 0 To (14 - sSldAtu.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sSldAtu)
            End If

            If sSldAnu.Length > 14 Then
                sMsgAux.Append("|" & sSldAnu.Substring(0, 14))
            Else
                sMsgAux.Append("|")
                If sSldAnu.Length < 14 Then
                    For i As Integer = 0 To (14 - sSldAnu.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sSldAnu)
            End If
            If sVlrAco.Length > 14 Then
                sMsgAux.Append("|" & sVlrAco.Substring(0, 14))
            Else
                sMsgAux.Append("|")
                If sVlrAco.Length < 14 Then
                    For i As Integer = 0 To (14 - sVlrAco.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sVlrAco)
            End If
            If sRstMns.Length > 14 Then
                sMsgAux.Append("|" & sRstMns.Substring(0, 14))
            Else
                sMsgAux.Append("|")
                If sRstMns.Length < 14 Then
                    For i As Integer = 0 To (14 - sRstMns.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sRstMns)
            End If

            If sRstAtu.Length > 14 Then
                sMsgAux.Append("|" & sRstAtu.Substring(0, 14))
            Else
                sMsgAux.Append("|")
                If sRstAtu.Length < 14 Then
                    For i As Integer = 0 To (14 - sRstAtu.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sRstAtu)
            End If

            If sRstAnu.Length > 14 Then
                sMsgAux.Append("|" & sRstAnu.Substring(0, 14))
            Else
                sMsgAux.Append("|")
                If sRstAnu.Length < 14 Then
                    For i As Integer = 0 To (14 - sRstAnu.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sRstAnu)
            End If
            sMsgAux.Append("|")

        Next

        sMsgAux.Append("+-------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+")


        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If
    End Function

    Private Function MntMsgPti(ByVal dtPti As List(Of VO.RelacaoAcaoComercialxFornecedores),
                               ByVal iIndAux As Integer) As String
        Dim sMsgAux As StringBuilder

        Dim sBU As String = ""
        Dim sFrn As String = ""
        Dim sEmpCtt As String = ""

        Dim sSldCtt As String = ""
        Dim sCtt As String = ""
        Dim sSldEmp As String = ""
        Dim sExt As String = ""
        Dim sPtc As String = ""
        Dim sTxaAdm As String = ""
        Dim sTxaBde As String = ""

        Dim dTotSldCtt As Double
        Dim dTotCtt As Double
        Dim dTotSldEmp As Double
        Dim dTotExt As Double
        Dim dTotPtc As Double
        Dim dTotTxaAdm As Double
        Dim dTotTxaBde As Double

        Dim sBfc As String = ""
        Dim cCulBra As CultureInfo


        cCulBra = New CultureInfo("pt-BR")


        sMsgAux = New StringBuilder
        '
        If iIndAux = 1 Then
            sMsgAux.Append("                                                                                                                        ")
        End If

        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
        sMsgAux.Append("| PATROCINADORES                                                                                                       |")
        sMsgAux.Append("+-------+---------------------+--------+------------+----------+----------+----------+-----------+----------+----------+")
        sMsgAux.Append("| BU    | FORNECEDOR          | EMP.   | SLD EMP.   | VR.      | SLD.     | VLR      | VLR       | TXA      | TXA      |")
        sMsgAux.Append("|       |                     | ACORDO | ACORDO     | ACORDO   | EMPENHO  | EXA ACC  | PART      | ADM      | BDE      |")
        sMsgAux.Append("+-------+---------------------+--------+------------+----------+----------+----------+-----------+----------+----------+")

        For Each drPti In dtPti
            'Preenchimento do BU
            If drPti.CODUNDESRNGC <> 0 Then
                sBU = CStr(drPti.CODUNDESRNGC).Trim() & "-" &
                                             CStr(drPti.DESUNDESRNGC).Trim()

                If sBU.Length > 7 Then
                    sMsgAux.Append("|" & sBU.Substring(0, 7))
                Else
                    sMsgAux.Append("|" & sBU)
                    If sBU.Length < 7 Then
                        For i As Integer = 0 To (7 - sBU.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
            End If


            'Preenchimento do fornecedor
            sFrn = CStr(drPti.CODFRN).Trim() & "-" &
            CStr(drPti.NOMFRN).Trim()
            '
            If sFrn.Length > 21 Then
                sMsgAux.Append("|" & sFrn.Substring(0, 21))
            Else
                sMsgAux.Append("|" & sFrn)
                If sFrn.Length < 21 Then
                    For i As Integer = 0 To (21 - sFrn.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            'Empenho do empenho ctt
            sEmpCtt = ""
            If Not IsDBNull(drPti.TIPDSNDSCBNF) Then
                If drPti.TIPDSNDSCBNF <> 0 Then
                    sEmpCtt = CStr(drPti.TIPDSNDSCBNF).Trim() & " - " &
                    CStr(drPti.DESDSNDSCBNF).Trim()

                    'Preenchimento Empenho Acordo
                    If sEmpCtt.Length > 8 Then
                        sMsgAux.Append("|" & sEmpCtt.Substring(0, 8))
                    Else
                        sMsgAux.Append("|" & sEmpCtt)
                    End If
                Else
                    sMsgAux.Append("|")
                End If
            Else
                sMsgAux.Append("|")
            End If
            '
            If sEmpCtt.Length < 8 Then
                For i As Integer = 0 To (8 - sEmpCtt.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If

            'Saldo do empenho acordo
            sSldCtt = ""
            If Not IsDBNull(drPti.VLRSLDDSNCTTACOCMC) Then
                sSldCtt = CDbl(drPti.VLRSLDDSNCTTACOCMC).ToString("N", cCulBra).Trim()
                dTotSldCtt += CDbl(drPti.VLRSLDDSNCTTACOCMC)
                '
                If sSldCtt.Length > 12 Then
                    sMsgAux.Append("|" & sSldCtt.Substring(0, 12))
                Else
                    sMsgAux.Append("|")
                    If sSldCtt.Length < 12 Then
                        For i As Integer = 0 To (12 - sSldCtt.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sSldCtt)
                End If
            Else
                sMsgAux.Append("|")
                If sSldCtt.Length < 12 Then
                    For i As Integer = 0 To (12 - sSldCtt.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            'Preenchimento Valor Acordo
            sCtt = ""
            If Not IsDBNull(drPti.VLRUTZCTTACOCMC) Then
                If drPti.VLRUTZCTTACOCMC <> 0 Then
                    sCtt = CDbl(drPti.VLRUTZCTTACOCMC).ToString("N", cCulBra).Trim()
                    dTotCtt += CDbl(drPti.VLRUTZCTTACOCMC)
                    '
                    If sCtt.Length > 10 Then
                        sMsgAux.Append("|" & sCtt.Substring(0, 10))
                    Else
                        sMsgAux.Append("|")
                        If sCtt.Length < 10 Then
                            For i As Integer = 0 To (10 - sCtt.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                        sMsgAux.Append(sCtt)
                    End If
                Else
                    sMsgAux.Append("|")
                    If sCtt.Length < 10 Then
                        For i As Integer = 0 To (10 - sCtt.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
            Else
                sMsgAux.Append("|")
                If sCtt.Length < 10 Then
                    For i As Integer = 0 To (10 - sCtt.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            'Preenchimento Saldo Empenho
            sSldEmp = ""
            sSldEmp = CDbl(drPti.VLRSLDDSNCTTACOCMC).ToString("N", cCulBra).Trim()
            dTotSldEmp += CDbl(drPti.VLRSLDDSNCTTACOCMC)

            If sSldEmp.Length > 10 Then
                sMsgAux.Append("|" & sSldEmp.Substring(0, 10))
            Else
                sMsgAux.Append("|")
                If sSldEmp.Length < 10 Then
                    For i As Integer = 0 To (10 - sSldEmp.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sSldEmp)
            End If

            'Valor extra contrato
            sExt = ""
            If Not IsDBNull(drPti.VLRUTZEXACTTACOCMC) Then
                If drPti.VLRUTZEXACTTACOCMC <> 0 Then
                    sExt = CDbl(drPti.VLRUTZEXACTTACOCMC).ToString("N", cCulBra).Trim()
                    dTotExt += CDbl(drPti.VLRUTZEXACTTACOCMC)
                    'Preenchimento Valor Extra Acordo
                    If sExt.Length > 10 Then
                        sMsgAux.Append("|" & sExt.Substring(0, 10))
                    Else
                        sMsgAux.Append("|")
                        If sExt.Length < 10 Then
                            For i As Integer = 0 To (10 - sExt.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                        sMsgAux.Append(sExt)
                    End If
                Else
                    sMsgAux.Append("|")
                    If sExt.Length < 10 Then
                        For i As Integer = 0 To (10 - sExt.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
            Else
                sMsgAux.Append("|")
                If sExt.Length < 13 Then
                    For i As Integer = 0 To (10 - sExt.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            'Valor Participacao
            sPtc = CDbl(drPti.VLRPTCFRNACOCMC).ToString("N", cCulBra).Trim()
            dTotPtc += CDbl(drPti.VLRPTCFRNACOCMC)
            If sPtc.Length > 11 Then
                sMsgAux.Append("|" & sPtc.Substring(0, 11))
            Else
                sMsgAux.Append("|")
                If sPtc.Length < 11 Then
                    For i As Integer = 0 To (11 - sPtc.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sPtc)
            End If

            'Valor da taxa de administração do fornecedor
            sTxaAdm = CDbl(drPti.VLRTXAADMFRNACOCMC).ToString("N", cCulBra).Trim()
            dTotTxaAdm += CDbl(drPti.VLRTXAADMFRNACOCMC)
            If sTxaAdm.Length > 10 Then
                sMsgAux.Append("|" & sTxaAdm.Substring(0, 10))
            Else
                sMsgAux.Append("|")
                If sTxaAdm.Length < 10 Then
                    For i As Integer = 0 To (10 - sTxaAdm.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sTxaAdm)
            End If

            'Valor da taxa de brinde 
            sTxaBde = CDbl(drPti.VLRTXAADMBDEACOCMC).ToString("N", cCulBra).Trim()
            dTotTxaBde += CDbl(drPti.VLRTXAADMBDEACOCMC)
            If sTxaBde.Length > 10 Then
                sMsgAux.Append("|" & sTxaBde.Substring(0, 10))
            Else
                sMsgAux.Append("|")
                If sTxaBde.Length < 10 Then
                    For i As Integer = 0 To (10 - sTxaBde.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sTxaBde)
            End If
            sMsgAux.Append("|")

        Next

        sMsgAux.Append("+-------+---------------------+--------+------------+----------+----------+----------+-----------+----------+----------+")
        sMsgAux.Append("+       |                     |        ")

        'Saldo do empenho acordo
        sSldCtt = dTotSldCtt.ToString("N", cCulBra).Trim()
        If sSldCtt.Length > 12 Then
            sMsgAux.Append("|" & sSldCtt.Substring(0, 12))
        Else
            sMsgAux.Append("|")
            If sSldCtt.Length < 12 Then
                For i As Integer = 0 To (12 - sSldCtt.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
            sMsgAux.Append(sSldCtt)
        End If

        'Preenchimento Valor Acordo
        sCtt = dTotCtt.ToString("N", cCulBra).Trim()
        If sCtt.Length > 10 Then
            sMsgAux.Append("|" & sCtt.Substring(0, 10))
        Else
            sMsgAux.Append("|")
            If sCtt.Length < 10 Then
                For i As Integer = 0 To (10 - sCtt.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
            sMsgAux.Append(sCtt)
        End If

        'Preenchimento Saldo Empenho
        sSldEmp = CDbl(dTotSldEmp).ToString("N", cCulBra).Trim()
        If sSldEmp.Length > 10 Then
            sMsgAux.Append("|" & sSldEmp.Substring(0, 10))
        Else
            sMsgAux.Append("|")
            If sSldEmp.Length < 10 Then
                For i As Integer = 0 To (10 - sSldEmp.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
            sMsgAux.Append(sSldEmp)
        End If

        'Valor extra contrato
        sExt = CDbl(dTotExt).ToString("N", cCulBra).Trim()
        If sExt.Length > 10 Then
            sMsgAux.Append("|" & sExt.Substring(0, 10))
        Else
            sMsgAux.Append("|")
            If sExt.Length < 10 Then
                For i As Integer = 0 To (10 - sExt.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
            sMsgAux.Append(sExt)
        End If

        'Valor Participacao
        sPtc = dTotPtc.ToString("N", cCulBra).Trim()
        If sPtc.Length > 11 Then
            sMsgAux.Append("|" & sPtc.Substring(0, 11))
        Else
            sMsgAux.Append("|")
            If sPtc.Length < 11 Then
                For i As Integer = 0 To (11 - sPtc.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
            sMsgAux.Append(sPtc)
        End If

        'Valor da taxa de administração do fornecedor
        sTxaAdm = dTotTxaAdm.ToString("N", cCulBra).Trim()
        If sTxaAdm.Length > 10 Then
            sMsgAux.Append("|" & sTxaAdm.Substring(0, 10))
        Else
            sMsgAux.Append("|")
            If sTxaAdm.Length < 10 Then
                For i As Integer = 0 To (10 - sTxaAdm.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
            sMsgAux.Append(sTxaAdm)
        End If

        'Valor da taxa de brinde 
        sTxaBde = dTotTxaBde.ToString("N", cCulBra).Trim()
        If sTxaBde.Length > 10 Then
            sMsgAux.Append("|" & sTxaBde.Substring(0, 10))
        Else
            sMsgAux.Append("|")
            If sTxaBde.Length < 10 Then
                For i As Integer = 0 To (10 - sTxaBde.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
            sMsgAux.Append(sTxaBde)
        End If
        sMsgAux.Append("|")

        sMsgAux.Append("+-------+---------------------+--------+------------+----------+----------+----------+-----------+----------+----------+")

        sMsgAux.Append("                                                                                                                        ")
        sMsgAux.Append("+------------+--------------------------------------------+------------------------------------------------------------+")
        sMsgAux.Append("| BU         | FORNECEDOR                                 |                  BENEFICIO DO FORNECEDOR                   |")
        sMsgAux.Append("+------------+--------------------------------------------+------------------------------------------------------------+")

        For Each drPti In dtPti
            If drPti.CODUNDESRNGC <> 0 Then
                sBU = CStr(drPti.CODUNDESRNGC).Trim() & " - " &
                                      CStr(drPti.DESUNDESRNGC).Trim()

                If sBU.Length > 12 Then
                    sMsgAux.Append("|" & sBU.Substring(0, 12))
                Else
                    sMsgAux.Append("|" & sBU)
                    If sBU.Length < 12 Then
                        For i As Integer = 0 To (12 - sBU.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
            End If


            'Preenchimento do fornecedor
            sFrn = CStr(drPti.CODFRN).Trim() & " - " &
                   CStr(drPti.NOMFRN).Trim()

            If sFrn.Length > 44 Then
                sMsgAux.Append("|" & sFrn.Substring(0, 44))
            Else
                sMsgAux.Append("|" & sFrn)
                If sFrn.Length < 44 Then
                    For i As Integer = 0 To (44 - sFrn.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            sBfc = CStr(drPti.DESBFCFRNACOCMC).Trim()

            If sBfc.Length > 60 Then
                sMsgAux.Append("|" & sBfc.Substring(0, 60))
            Else
                sMsgAux.Append("|" & sBfc)
                If sBfc.Length < 60 Then
                    For i As Integer = 0 To (60 - sBfc.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If
            sMsgAux.Append("|")

        Next

        sMsgAux.Append("+------------+--------------------------------------------+------------------------------------------------------------+")


        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If

    End Function

    Private Function MntMsgPolAcoCmc(ByVal dtPolAcoCmc As List(Of VO.PoliticasDaAcaoComercial),
                                     ByVal iIndAux As Integer) As String
        Dim sMsgAux As StringBuilder
        Dim sPol As String

        sMsgAux = New StringBuilder

        If iIndAux = 1 Then
            sMsgAux.Append("                                                                                                                        ")
        End If

        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
        sMsgAux.Append("| POLITICAS DIVERGENTES                                                                                                |")
        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")

        For Each drPolAcoCmc In dtPolAcoCmc
            sPol = CStr(drPolAcoCmc.DESPOLACOCMC).Trim()
            If sPol.Length > 118 Then
                sMsgAux.Append("|" & sPol.Substring(0, 118))
            Else
                sMsgAux.Append("|" & sPol)
                If sPol.Length < 118 Then
                    For i As Integer = 0 To (118 - sPol.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If

            sMsgAux.Append("|")
        Next

        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")


        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If
    End Function


    Public Function BscDdoAcoEma(ByVal CodAcoCmc As Integer)


        ''Busca capa da ação
        'oGrpDdoAux = FluxoDeAprovacaoDao.BscDdoAcoNumEma(CodAcoCmc)
        'oGrpDdoAux.Tables(0).TableName = "DdoCpa"
        'oGrpDdo.Tables.Add(oGrpDdoAux.Tables(0).Copy())

        ''Busca fornecedores da ação
        'oGrpDdoAux = FluxoDeAprovacaoDao.BscDdoFrn(CodAcoCmc)
        'oGrpDdoAux.Tables(0).TableName = "DdoFrn"
        'oGrpDdo.Tables.Add(oGrpDdoAux.Tables(0).Copy())

        ''Busca politicas divergentes da ação
        'oGrpDdoAux = FluxoDeAprovacaoDao.BscDdoPolAcoCmc(CodAcoCmc)
        'oGrpDdoAux.Tables(0).TableName = "DdoPol"
        'oGrpDdo.Tables.Add(oGrpDdoAux.Tables(0).Copy())

        ''Busca regras divergentes da ação
        'oGrpDdoAux = FluxoDeAprovacaoDao.BscRgrAcoCmc(CodAcoCmc)
        'oGrpDdoAux.Tables(0).TableName = "DdoRgr"
        'oGrpDdo.Tables.Add(oGrpDdoAux.Tables(0).Copy())

        ''Busca Saldo Plano de Marketing
        'oGrpDdoAux = FluxoDeAprovacaoDao.CalSldPlnMktAcoCmcEnv(CodAcoCmc)
        'oGrpDdoAux.Tables(0).TableName = "DdoSldPlnMrk"
        'oGrpDdo.Tables.Add(oGrpDdoAux.Tables(0).Copy())

        ''Busca Orçamento
        'oGrpDdoAux = FluxoDeAprovacaoDao.BscDdoIteOcdEma(CodAcoCmc)
        'oGrpDdoAux.Tables(0).TableName = "DdoOcd"
        'oGrpDdo.Tables.Add(oGrpDdoAux.Tables(0).Copy())

        ''Busca Incentivos
        'oGrpDdoAux = FluxoDeAprovacaoDao.BscDdoIctAco(CodAcoCmc)
        'oGrpDdoAux.Tables(0).TableName = "DdoIct"
        'oGrpDdo.Tables.Add(oGrpDdoAux.Tables(0).Copy())

        ''Busca Fluxo da ação
        'oGrpDdoAux = FluxoDeAprovacaoDao.CnsFluApvEma(CodAcoCmc)
        'oGrpDdoAux.Tables(0).TableName = "DdoFlu"
        'oGrpDdo.Tables.Add(oGrpDdoAux.Tables(0).Copy())


        'TODO: verificar se o BuscarAcaoComercialPorId tambem busca o fluxo da ação
        'Return AcaoComercialBO.BuscarAcaoComercialPorId(CodAcoCmc)
        Return AcaoComercialBO.BuscarAcaoComercialfluxo(CodAcoCmc)
    End Function


    Private Function MntMsgCab(ByVal dtCab As VO.AcaoComercial,
                               ByVal iIndAux As Integer,
                               ByVal tipo As TipoAcao) As String
        Dim sMsgAux As StringBuilder

        Dim sNome As String
        Dim sValidade As String
        Dim sComplementar As String
        Dim sAcaoPrincipal As String
        Dim sVlrTotAcao As String
        Dim sVlrTotOrcamento As String
        Dim sVlrTotTxaAdm As String
        Dim sVlrTotTxaBde As String
        Dim sTipEvento As String
        Dim sEvento As String
        Dim sEmpExaAco As String

        Dim cCulBra As New CultureInfo("pt-BR")

        With dtCab
            If Not IsNothing(dtCab) Then

                sNome = CStr(dtCab.NOMACOCMC).Trim()
                sValidade = Format(dtCab.DATINIACOCMC, "dd/MM/yyyy") & " - " &
                            Format(dtCab.DATFIMACOCMC, "dd/MM/yyyy")
                sComplementar = IIf(CStr(dtCab.INDACOCMCCPL).Trim() = "1", "SIM", "NÃO")
                sAcaoPrincipal = CStr(dtCab.CODACOCMCORI).Trim()
                sVlrTotAcao = CDbl(dtCab.VLRTOTACOCMC).ToString("c", cCulBra).Trim()
                sVlrTotOrcamento = CDbl(dtCab.VLRTOTOCDACOCMC).ToString("c", cCulBra).Trim()
                sVlrTotTxaAdm = CDbl(dtCab.VLRTOTTXAADMACOCMC).ToString("c", cCulBra).Trim()
                sVlrTotTxaBde = CDbl(dtCab.VLRTOTBDEACOCMC).ToString("c", cCulBra).Trim()

                If Not IsDBNull(dtCab.NOMTIPEVTACOCMC) AndAlso Not IsNothing(dtCab.NOMTIPEVTACOCMC) Then
                    sTipEvento = CStr(dtCab.TIPEVTACOCMC).Trim() & " - " &
                                 CStr(dtCab.NOMTIPEVTACOCMC).Trim()
                Else
                    sTipEvento = ""
                End If

                If Not IsDBNull(dtCab.NOMEVTACOCMC) AndAlso Not IsNothing(dtCab.NOMEVTACOCMC) Then
                    sEvento = CStr(dtCab.CODEVTACOCMC).Trim() & " - " &
                              CStr(dtCab.NOMEVTACOCMC).Trim()
                Else
                    sEvento = ""
                End If

                If Not IsDBNull(dtCab.DESDSNDSCBNF) AndAlso Not IsNothing(dtCab.DESDSNDSCBNF) Then
                    sEmpExaAco = CStr(dtCab.TIPDSNDSCBNF).Trim() & " - " &
                                 CStr(dtCab.DESDSNDSCBNF).Trim()
                Else
                    sEmpExaAco = ""
                End If


                sMsgAux = New StringBuilder

                'Estrutura comentada
                'sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                'sMsgAux.Append("| AÇÕES MERCADOLÓGICAS                                                                                                 |")
                'sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                'sMsgAux.Append("| AÇÃO                                                                                                                 |")
                'sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                'sMsgAux.Append("| NOME:                                               | VALIDADE:                                                      |")
                'sMsgAux.Append("| COMPLEMENTAR:                                       | AÇÃO PRINCIPAL:                                                |")
                'sMsgAux.Append("| VALOR TOTAL DA AÇÃO:                                | VALOR TOTAL ORÇAMENTO:                                         |")
                'sMsgAux.Append("| VALOR TOTAL TAX ADM:                                | VALOR TOTAL TAXA BRINDE:                                       |")
                'sMsgAux.Append("| TIPO DE EVENTO:                                     | EVENTO:                                                        |")
                'sMsgAux.Append("| EMPENHO EXA CTT:                                                                                                     |")
                'sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                'Fim de estrutura comentada

                If iIndAux = 1 Then
                    sMsgAux.Append("                                                                                                                        ")
                End If

                Select Case tipo
                    Case TipoAcao.PreAcordoValor, TipoAcao.PreAcordoVigencia
                        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                        sMsgAux.Append("| AÇÕES MERCADOLÓGICAS                                                                                                 |")
                        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                        sMsgAux.Append("| PRÉ-ACORDO PROMOCIONAL                                                                                               |")
                        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                    Case Else
                        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                        sMsgAux.Append("| AÇÕES MERCADOLÓGICAS                                                                                                 |")
                        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                        sMsgAux.Append("| AÇÃO                                                                                                                 |")
                        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                End Select


                'Primeira linha
                If sNome.Length > 46 Then
                    sMsgAux.Append("| NOME: " & sNome.Substring(0, 46))
                Else
                    sMsgAux.Append("| NOME: " & sNome)
                    If sNome.Length < 46 Then
                        For i As Integer = 0 To (46 - sNome.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If

                If sValidade.Length > 53 Then
                    sMsgAux.Append("| VALIDADE: " & sValidade.Substring(0, 53))
                Else
                    sMsgAux.Append("| VALIDADE: " & sValidade)
                    If sValidade.Length < 53 Then
                        For i As Integer = 0 To (53 - sValidade.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                sMsgAux.Append("|")

                'Segunda linha
                If sComplementar.Length > 38 Then
                    sMsgAux.Append("| COMPLEMENTAR: " & sComplementar.Substring(0, 38))
                Else
                    sMsgAux.Append("| COMPLEMENTAR: " & sComplementar)
                    If sComplementar.Length < 38 Then
                        For i As Integer = 0 To (38 - sComplementar.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                If sAcaoPrincipal.Length > 47 Then
                    sMsgAux.Append("| AÇÃO PRINCIPAL: " & sAcaoPrincipal.Substring(0, 47))
                Else
                    sMsgAux.Append("| AÇÃO PRINCIPAL: " & sAcaoPrincipal)
                    If sAcaoPrincipal.Length < 47 Then
                        For i As Integer = 0 To (47 - sAcaoPrincipal.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                sMsgAux.Append("|")

                'terceira linha
                If sVlrTotAcao.Length > 31 Then
                    sMsgAux.Append("| VALOR TOTAL DA AÇÃO: " & sVlrTotAcao.Substring(0, 31))
                Else
                    sMsgAux.Append("| VALOR TOTAL DA AÇÃO: " & sVlrTotAcao)
                    If sVlrTotAcao.Length < 31 Then
                        For i As Integer = 0 To (31 - sVlrTotAcao.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If

                If sVlrTotOrcamento.Length > 40 Then
                    sMsgAux.Append("| VALOR TOTAL ORÇAMENTO: " & sVlrTotOrcamento.Substring(0, 40))
                Else
                    sMsgAux.Append("| VALOR TOTAL ORÇAMENTO: " & sVlrTotOrcamento)
                    If sVlrTotOrcamento.Length < 40 Then
                        For i As Integer = 0 To (40 - sVlrTotOrcamento.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                sMsgAux.Append("|")

                'quarta linha
                If sVlrTotTxaAdm.Length > 31 Then
                    sMsgAux.Append("| VALOR TOTAL TAX ADM: " & sVlrTotTxaAdm.Substring(0, 31))
                Else
                    sMsgAux.Append("| VALOR TOTAL TAX ADM: " & sVlrTotTxaAdm)
                    If sVlrTotTxaAdm.Length < 31 Then
                        For i As Integer = 0 To (31 - sVlrTotTxaAdm.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If

                If sVlrTotTxaBde.Length > 38 Then
                    sMsgAux.Append("| VALOR TOTAL TAXA BRINDE: " & sVlrTotTxaBde.Substring(0, 38))
                Else
                    sMsgAux.Append("| VALOR TOTAL TAXA BRINDE: " & sVlrTotTxaBde)
                    If sVlrTotTxaBde.Length < 38 Then
                        For i As Integer = 0 To (38 - sVlrTotTxaBde.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                sMsgAux.Append("|")

                'quinta linha
                If sTipEvento.Length > 36 Then
                    sMsgAux.Append("| TIPO DE EVENTO: " & sTipEvento.Substring(0, 36))
                Else
                    sMsgAux.Append("| TIPO DE EVENTO: " & sTipEvento)
                    If sTipEvento.Length < 36 Then
                        For i As Integer = 0 To (36 - sTipEvento.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If

                If sEvento.Length > 55 Then
                    sMsgAux.Append("| EVENTO: " & sEvento.Substring(0, 55))
                Else
                    sMsgAux.Append("| EVENTO: " & sEvento)
                    If sEvento.Length < 55 Then
                        For i As Integer = 0 To (55 - sEvento.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                sMsgAux.Append("|")

                'sexta e ultima linha
                If sEmpExaAco.Length > 100 Then
                    sMsgAux.Append("| EMPENHO EXA CTT: " & sEmpExaAco.Substring(0, 100))
                Else
                    sMsgAux.Append("| EMPENHO EXA CTT: " & sEmpExaAco)
                    If sEmpExaAco.Length < 100 Then
                        For i As Integer = 0 To (100 - sEmpExaAco.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                sMsgAux.Append("|")

                sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")

            End If

        End With

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If
    End Function


    Private Function MntMsgRgrAcoCmc(ByVal dtRgrAcoCmc As VO.RelacaoAcaoComercialxMecanicaDaAcao,
                                     ByVal iIndAux As Integer) As String
        Dim sMsgAux As StringBuilder
        Dim sRgr As String

        Dim iAux As Integer
        Dim iAuxIni As Integer
        Dim sMecAux As String

        With dtRgrAcoCmc
            If Not IsNothing(dtRgrAcoCmc) Then

                sMsgAux = New StringBuilder

                If iIndAux = 1 Then
                    sMsgAux.Append("                                                                                                                        ")
                End If

                sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
                sMsgAux.Append("| MECÂNICA                                                                                                             |")
                sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")


                sRgr = CStr(dtRgrAcoCmc.DESRGRACOCMC).Trim()

                If sRgr.Length > 118 Then

                    iAux = (sRgr.Length / 118)
                    iAuxIni = 0

                    While iAux > 0

                        If sRgr.Length <= 118 Then
                            sMecAux = sRgr
                            sMsgAux.Append("|" & sRgr)
                            If sMecAux.Length < 118 Then
                                For i As Integer = 0 To (118 - sMecAux.Length) - 1
                                    sMsgAux.Append(" ")
                                Next
                            End If
                            sMsgAux.Append("|")
                        ElseIf sRgr.Length > ((iAuxIni * 118) + 118) Then
                            sMsgAux.Append("|" & sRgr.Substring((iAuxIni * 118), 118))
                            sMsgAux.Append("|")
                        Else
                            sMecAux = sRgr.Substring((iAuxIni * 118), sRgr.Length - (iAuxIni * 118))
                            sMsgAux.Append("|" & sMecAux)
                            If sMecAux.Length < 118 Then
                                For i As Integer = 0 To (118 - sMecAux.Length) - 1
                                    sMsgAux.Append(" ")
                                Next
                            End If
                            sMsgAux.Append("|")
                        End If

                        iAuxIni += 1
                        iAux -= 1

                    End While

                Else
                    sMsgAux.Append("|" & sRgr)
                    If sRgr.Length < 118 Then
                        For i As Integer = 0 To (118 - sRgr.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                sMsgAux.Append("|")

                sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")

            End If
        End With

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If

    End Function

#End Region


End Class
