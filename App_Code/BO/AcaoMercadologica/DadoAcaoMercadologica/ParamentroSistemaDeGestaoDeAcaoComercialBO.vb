﻿Imports Core
Public Class ParamentroSistemaDeGestaoDeAcaoComercialBO
    Inherits BOBase(Of VO.ParamentroSistemaDeGestaoDeAcaoComercial)

    Public Sub New()
        MyBase.daoBase = New ParamentroSistemaDeGestaoDeAcaoComercialDAO()

    End Sub

    Public Function BuscarParametroPorId(CODEMP As Decimal?)

        Return DirectCast(daoBase, ParamentroSistemaDeGestaoDeAcaoComercialDAO).BuscarParametroPorId(CODEMP)

    End Function

End Class




