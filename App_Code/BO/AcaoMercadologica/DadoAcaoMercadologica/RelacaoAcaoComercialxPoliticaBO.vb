﻿Imports Core
Public Class RelacaoAcaoComercialxPoliticaBO
    Inherits BOBase(Of VO.RelacaoAcaoComercialxPolitica)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoComercialxPoliticaDAO()
    End Sub

    Public Function DeletarDadoAcaoComercialPoliticas(CODACOCMC As Integer) As Integer

        Return DirectCast(daoBase, RelacaoAcaoComercialxPoliticaDAO).DeletarDadoAcaoComercialPoliticas(CODACOCMC)

    End Function

    Public Function InsereDadosPoliticaAcao(CODACOCMC As Integer, CODPOLACOCMC As Integer) As Integer

        Return DirectCast(daoBase, RelacaoAcaoComercialxPoliticaDAO).InsereDadosPoliticaAcao(CODACOCMC, CODPOLACOCMC)

    End Function

    Public Function ConsultaPoliticaDivergente(CODEVTACOCMC As Decimal) As List(Of VO.RelacaoAcaoComercialxPolitica)

        Return DirectCast(daoBase, RelacaoAcaoComercialxPoliticaDAO).ConsultaPoliticaDivergente(CODEVTACOCMC)

    End Function


End Class
