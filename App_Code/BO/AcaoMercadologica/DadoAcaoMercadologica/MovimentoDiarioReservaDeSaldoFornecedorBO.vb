﻿Imports Core
Public Class MovimentoDiarioReservaDeSaldoFornecedorBO
    Inherits BOBase(Of VO.MovimentoDiarioReservaDeSaldoFornecedor)

    Public Sub New()
        MyBase.daoBase = New MovimentoDiarioReservaDeSaldoFornecedorDAO()
    End Sub


    Public Sub DeletePorCodAcao(ByVal CODACOCMC As String)
        DirectCast(daoBase, MovimentoDiarioReservaDeSaldoFornecedorDAO).DeletePorCodAcao(CODACOCMC)
    End Sub

End Class
