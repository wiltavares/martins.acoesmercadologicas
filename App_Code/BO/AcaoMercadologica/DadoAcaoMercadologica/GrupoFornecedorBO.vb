﻿

Imports Core
Public Class GrupoFornecedorBO
    Inherits BOBase(Of VO.GrupoFornecedor)

    Public Sub New()
        MyBase.daoBase = New GrupoFornecedorDAO()
    End Sub


    Public Function ListLookup() As List(Of VO.GrupoFornecedor)
        Return DirectCast(daoBase, GrupoFornecedorDAO).ListLookup()
    End Function


End Class
