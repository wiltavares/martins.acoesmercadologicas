﻿Imports Core
Public Class RelacaoAcaoComercialPorOperacaoDeDestinoBO
    Inherits BOBase(Of VO.RelacaoAcaoComercialPorOperacaoDeDestino)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoComercialPorOperacaoDeDestinoDAO()
    End Sub

    Public Sub InsertUpdate(ByVal item As VO.RelacaoAcaoComercialPorOperacaoDeDestino)

        'Alterando os códigos
        If (item.CODOPEFSCDSNACOCMC = 4) Then 'se for promoção brinde (4)
            item.CODOPEFSCDSNACOCMC = 2 'transforma para promoção (2)
        ElseIf item.CODOPEFSCDSNACOCMC = 8 Then 'se for Bonificação brinde (8)
            item.CODOPEFSCDSNACOCMC = 1 'transforma para Bonificação (1)
        ElseIf item.CODOPEFSCDSNACOCMC = 9 Then 'se for Consumo brinde (9)
            item.CODOPEFSCDSNACOCMC = 3 'transforma para Consumo (3)
        End If

        'Verifica se existe ação
        Dim qtd As Integer = DirectCast(daoBase, RelacaoAcaoComercialPorOperacaoDeDestinoDAO).BuscaQtd(item.CODACOCMC, item.CODOPEFSCDSNACOCMC)

        If (qtd = 1) Then
            DirectCast(daoBase, RelacaoAcaoComercialPorOperacaoDeDestinoDAO).Update(item)
        Else
            DirectCast(daoBase, RelacaoAcaoComercialPorOperacaoDeDestinoDAO).Insert(item)
        End If

    End Sub


    Public Function BuscaQtd(ByVal iCodAcoCmc As Integer, ByVal CodOpeFsc As Integer) As Integer
        Return DirectCast(daoBase, RelacaoAcaoComercialPorOperacaoDeDestinoDAO).BuscaQtd(iCodAcoCmc, CodOpeFsc)
    End Function

    'Public Function SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(ByVal iCodAcoCmc As Integer) As List(Of VO.RelacaoAcaoComercialPorOperacaoDeDestino)
    '    Return DirectCast(daoBase, RelacaoAcaoComercialPorOperacaoDeDestinoDAO).SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(iCodAcoCmc)
    'End Function

End Class
