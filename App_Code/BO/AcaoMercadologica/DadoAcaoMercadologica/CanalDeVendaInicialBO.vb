﻿Imports Core
Public Class CanalDeVendaInicialBO
    Inherits BOBase(Of VO.CanalDeVendaInicial)

    Public Sub New()
        MyBase.daoBase = New CanalDeVendaInicialDAO()
    End Sub

    Public Function ListarCanalDeVenda() As List(Of VO.CanalDeVendaInicial)

        Return DirectCast(daoBase, CanalDeVendaInicialDAO).ListarCanalDeVenda()

    End Function
End Class
