﻿Imports Core
Public Class ObjetivoCarimbodeVerbadoFornecedorBO
    Inherits BOBase(Of VO.ObjetivoCarimbodeVerbadoFornecedor)

    Public Sub New()
        MyBase.daoBase = New ObjetivoCarimbodeVerbadoFornecedorDAO()
    End Sub

    Public Function ListObjetivos(vo As VO.ObjetivoCarimbodeVerbadoFornecedor) As List(Of VO.ObjetivoCarimbodeVerbadoFornecedor)

        Return DirectCast(DAOBase, ObjetivoCarimbodeVerbadoFornecedorDAO).ListObjetivos(vo)

    End Function


End Class
