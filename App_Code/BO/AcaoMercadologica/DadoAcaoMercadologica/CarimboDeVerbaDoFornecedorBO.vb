﻿Imports Core
Public Class CarimboDeVerbaDoFornecedorBO
    Inherits BOBase(Of VO.CarimboDeVerbaDoFornecedor)

    Public Sub New()
        MyBase.daoBase = New CarimboDeVerbaDoFornecedorDAO()
    End Sub

    Public Function RetornaCadMcoVbaFrn(lista As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)) As Integer
        Try

            Dim retorno As Integer
            daoBase.connector.BeginTransaction()
            For Each item In lista
                retorno = DirectCast(daoBase, CarimboDeVerbaDoFornecedorDAO).RetornaCadMcoVbaFrn(item.CODMCOVBAFRN, item.VLRUTZACOCMC)
            Next
            If (Not IsNothing(retorno)) Then
                daoBase.connector.Commit()
            End If
            Return retorno
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try



    End Function



End Class
