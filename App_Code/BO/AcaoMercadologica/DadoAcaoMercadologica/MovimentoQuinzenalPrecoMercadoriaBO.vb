﻿Imports System.Data.OleDb
Imports Core
Public Class MovimentoQuinzenalPrecoMercadoriaBO
    Inherits BOBase(Of VO.MovimentoQuinzenalPrecoMercadoriaVO)

    Public Sub New()
        MyBase.daoBase = New MovimentoQuinzenalPrecoMercadoriaDAO()
    End Sub

    ''' <summary>
    ''' busca dados com preço unitario do item selecionado
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultaPrecoUnitarioItem(codMercadoria As Integer, codFilial As Integer, ByVal tipAcao As Decimal) As List(Of VO.MovimentoQuinzenalPrecoMercadoriaVO)
        Return DirectCast(daoBase, MovimentoQuinzenalPrecoMercadoriaDAO).ConsultaPrecoUnitarioItem(codMercadoria, codFilial, tipAcao)
    End Function

    Public Function ObterValorTotalDeMercadoriaInseridaNaPromocao(codFilial As Integer, codMercadoria As Integer) As List(Of VO.MovimentoQuinzenalPrecoMercadoriaVO)
        Return DirectCast(daoBase, MovimentoQuinzenalPrecoMercadoriaDAO).ObterValorTotalDeMercadoriaInseridaNaPromocao(codFilial, codMercadoria)
    End Function


    ''' <summary>
    ''' busca dados com preço unitario do item selecionado
    ''' Este metodo so pode ser chamado se estiver no escopo de uma Thread
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultaPrecoUnitarioItemThread(codMercadoria As Integer, codFilial As Integer, ByVal TIPACOMCD As Decimal) As Decimal

        Dim movimentos = DirectCast(daoBase, MovimentoQuinzenalPrecoMercadoriaDAO).ConsultaPrecoUnitarioItemThread(codMercadoria, codFilial, TIPACOMCD)
        If movimentos.Count > 0 Then
            Dim ValIte = movimentos.FirstOrDefault.VLRCSTUNTMER
            If Trim(movimentos.FirstOrDefault.FLGMERPMC) = "S" Then
                Return ValIte
            Else
                'Se for Ação do tipo Pré-Acordo.
                If (TIPACOMCD = TipoAcao.PreAcordoVigencia Or TIPACOMCD = TipoAcao.PreAcordoValor) Then
                    Return ValIte
                Else 'Se não for Ação do tipo Pré-Acordo.
                    Return ValIte / 0.78
                End If
            End If
        Else
            Return 0.0
        End If

    End Function

End Class
