﻿Imports Core
Imports System.Text
Imports System.Data

Public Class RelacaoAcaoComercialxFornecedoresBO
    Inherits BOBase(Of VO.RelacaoAcaoComercialxFornecedores)

    'Private boAcaoComercial As New AcaoComercialBO
    Private boRelacaoPlanoMarketing As New RelacaoAcaoComercialxPlanoMarketingBO
    Private boFornecedores As RelacaoAcaoComercialxFornecedoresBO
    Private boMecanicaAcao As RelacaoAcaoComercialxMecanicaDaAcaoBO
    Private boIncentivo As RelacaoIncentivoxAcaoComercialBO
    'Private boFluxoAprovacao As RelacaoAcaoComercialxFluxoAprovacaoBO
    Private boPoliticas As New RelacaoAcaoComercialxPoliticaBO
    Private boEvento As New EventoBO
    Private boTipoEvento As TipoDeEventoBO()
    Private boItemOrcamento As RelacaoAcaoComercialPorItemOrcamentoBO
    Private boPrtGeraisIncentivoDeVendas As ParametrosGeraisDoIncentivoDeVendasBO
    Private boParametros As New ParametroSistemaAdminitracaoVendaBO
    Private boFuncionarios As FuncionarioBO
    Private boNoveosAprovacao As NiveisDoAprovadorDoFluxoPorSistemaBO
    Private boRelacaoAcaoxFluxoApro As RelacaoAcaoComercialxFluxoAprovacaoBO
    Private boFluxoAprovacao As FluxoDeAprovacaoBO
    Private boMovimentoMesnagem As MovimentoDeMensagensDoCorreioEletronicoBO
    Private borealcaoEndereco As RelacaoDeEnderecosParaEnvioDoCorreioEletronicoBO
    Private PoliticasDaAcaoComercial As PoliticasDaAcaoComercialBO
    Private RelacaoAcaoMecanica As RelacaoAcaoComercialxMecanicaDaAcaoBO
    Private RelacaoLinhasMSGCorreio As RelacaoDeLinhasDaMensagemDoCorreioEletronicoBO

    'Private boAcaoComercial2 As New AcaoComercialBO


    Private ReadOnly this As Object
    Private ReadOnly empenhoFornecedorMarketing As Integer = 78

    Public Enum TipoUsuario
        GER_MARKETING = 7
        GER_CATEGORIA = 5
        DIR_COMPRAS = 3
    End Enum

    Public Enum Programa
        ACOES_MERCADOLOGICAS = 8
    End Enum

    Public Enum tipoFluxo
        VALOR = 1
    End Enum
    '
    Public Enum NumSeqAprovador
        GER_CATEGORIA = 2
    End Enum

    Public Enum TipStaFluApv
        Criacao = 1
        Em_Aprovacao = 3
        Aprovado = 5
        Rejeitado = 4
        Pedido_Parecer = 6
        Resposta_Parecer = 8
        Aguardando_Aprovacao = 9
        Notificacao = 7
    End Enum

    Public Enum TipNivFluApv
        Ger_Marketing = 1
        Ger_Categoria = 2
        Dir_Compras = 3
    End Enum


    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoComercialxFornecedoresDAO()
    End Sub

    ''' <summary>
    ''' BscSldCttFrn
    ''' </summary>
    ''' <param name="CODFRN"></param>
    ''' <param name="TIPDSNDSCBNF"></param>
    ''' <param name="ANOREF"></param>
    ''' <returns></returns>
    Public Function BuscaValorSaldoAcordoFornecedor(CODFRN As Decimal, TIPDSNDSCBNF As Decimal, ANOREF As Decimal) As Decimal
        Try
            Dim daoFornecedor As RelacaoAcaoComercialxFornecedoresDAO = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO)
            Dim auxFrn = daoFornecedor.BscSldCttFrn(CODFRN, TIPDSNDSCBNF, ANOREF).FirstOrDefault
            If auxFrn IsNot Nothing Then
                Return auxFrn.VLRSLDDSP
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return 0

    End Function

    ''' <summary>
    ''' BscSldCntCrrFrn
    ''' </summary>
    ''' <param name="CODFRN"></param>
    ''' <param name="TIPDSNDSCBNF"></param>
    ''' <param name="ANOREF"></param>
    ''' <returns></returns>
    Public Function BuscaValorSaldoExaAcordoFornecedor(CODFRN As Decimal, TIPDSNDSCBNF As Decimal, ANOREF As Decimal) As Decimal
        Try
            Dim daoFornecedor As RelacaoAcaoComercialxFornecedoresDAO = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO)
            Dim auxFrn = daoFornecedor.BscSldCntCrrFrn(CODFRN, TIPDSNDSCBNF, ANOREF).FirstOrDefault
            If auxFrn IsNot Nothing Then
                Return auxFrn.VLRSLDDSP
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return 0


    End Function

    ''' <summary>
    ''' BscSldReceitaCMV
    ''' </summary>
    ''' <param name="CODFRN"></param>
    ''' <returns></returns>
    Public Function BuscaSaldoReceitaCMVFornecedor(CODFRN As Decimal) As Decimal
        Try
            Dim daoFornecedor As RelacaoAcaoComercialxFornecedoresDAO = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO)
            Dim auxFrn = daoFornecedor.BscSldReceitaCMV(CODFRN).FirstOrDefault
            If auxFrn IsNot Nothing Then
                Return auxFrn.VLRSLDDSP
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return 0

    End Function

    ''' <summary>
    ''' BscUndNgcFrn
    ''' </summary>
    ''' <param name="CODFRN"></param>
    ''' <returns></returns>
    Public Function BuscarUnicaUnidadeNegocioFornecedor(CODFRN As Decimal) As VO.RelacaoAcaoComercialxFornecedores

        Try
            Dim daoFornecedor As RelacaoAcaoComercialxFornecedoresDAO = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO)
            Dim auxFrn = daoFornecedor.BscUndNgcFrn(CODFRN).FirstOrDefault
            If auxFrn IsNot Nothing Then
                Return auxFrn
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return New VO.RelacaoAcaoComercialxFornecedores

    End Function

    ''' <summary>
    ''' BscUndNgcFrn
    ''' </summary>
    ''' <param name="CODFRN"></param>
    ''' <returns></returns>
    Public Function BscDesDsnDscBnfCtt(CODFRN As Decimal) As VO.RelacaoAcaoComercialxFornecedores
        Try
            Dim daoFornecedor As RelacaoAcaoComercialxFornecedoresDAO = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO)
            Dim auxFrn = daoFornecedor.BscDesDsnDscBnfCtt(CODFRN).FirstOrDefault
            If auxFrn IsNot Nothing Then
                Return auxFrn
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return New VO.RelacaoAcaoComercialxFornecedores

    End Function

    ''' <summary>
    ''' Busca os fornecedores da acao para montar o grid
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <param name="codStatusAcao"></param>
    ''' <param name="codBnfExaCtt"></param>
    ''' <returns></returns>
    Public Function BuscarFornecedoresDaAcao(idAcao As Decimal?, Optional codStatusAcao As Decimal? = Nothing, Optional codBnfExaCtt As Decimal? = Nothing) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Try
            Dim daoFornecedor As RelacaoAcaoComercialxFornecedoresDAO = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO)
            Dim listaRelacaoFornecedores = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BuscarFornecedoresDaAcao(idAcao)

            For Each fornecedor As VO.RelacaoAcaoComercialxFornecedores In listaRelacaoFornecedores

                Dim dat = Now
                Dim strAux = Format(dat, "dd/MM/yyyy")
                Dim strDat = strAux.Split("/")(2) & strAux.Split("/")(1)

                If codStatusAcao = Constantes.Status.NOVA Or codStatusAcao = Constantes.Status.REJEITADA Then
                    If fornecedor.CODFRN <> 999999 Then
                        'Valor do Saldo de acordo do fornecedor
                        fornecedor.VLRSLDDSNCTTACOCMC = BuscaValorSaldoAcordoFornecedor(fornecedor.CODFRN, fornecedor.TIPDSNDSCBNF, strDat)
                        'Saldo Conta Corrente do fornecedor
                        fornecedor.VLRSLDDSNDSCACOCMC = BuscaValorSaldoExaAcordoFornecedor(fornecedor.CODFRN, codBnfExaCtt, strDat)
                        fornecedor.VLRSLDCNTCRRFRN = fornecedor.VLRSLDDSNDSCACOCMC
                        'Valor de Saldo Receita CMV
                        fornecedor.VLRSLDDSNACR = BuscaSaldoReceitaCMVFornecedor(fornecedor.CODFRN)
                    End If
                End If

                If (fornecedor.TIPFRNACOCMC = 1) Then
                    Dim fornecedorEdl As Frn = Constantes.FornecedoresEdlCodFrn(fornecedor.CODFRN).FirstOrDefault()
                    fornecedor.NOMFRN = fornecedorEdl.NOMFRN
                End If

                Dim SldCtt As Double = If(fornecedor.VLRSLDDSNCTTACOCMC, 0)
                Dim SldCntCrr As Double = If(fornecedor.VLRSLDDSNDSCACOCMC, 0)
                Dim CMV As Double = If(fornecedor.VLRUTZRCTCSTMER, 0)
                Dim CMVaux As Double = If(fornecedor.VLRUTZRCTCSTMER, 0)
                Dim SldReceitaCMV As Double = If(fornecedor.VLRSLDDSNACR, 0)
                Dim txaBde As Double = If(fornecedor.VLRTXAADMBDEACOCMC, 0)

                Dim ctt As Double = If(fornecedor.VLRUTZCTTACOCMC, 0)
                Dim cttaux As Double = If(fornecedor.VLRUTZCTTACOCMC, 0)
                Dim ext As Double = If(fornecedor.VLRUTZEXACTTACOCMC, 0)
                Dim extaux As Double = If(fornecedor.VLRUTZEXACTTACOCMC, 0)

                CalTxaBde(CMV, CMVaux, SldReceitaCMV, SldCtt, SldCntCrr, txaBde, ctt, cttaux, ext, extaux)

                fornecedor.VLRUTZCTTACOCMC = cttaux
                fornecedor.VLRUTZEXACTTACOCMC = extaux

            Next

            Return listaRelacaoFornecedores
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function BuscarFornecedoresDaAcaoETR(id As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Return DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BuscarFornecedoresDaAcaoETR(id)

    End Function


    Public Function BuscarFornecedoresDaAcaoBonificacao(id As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Return DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BuscarFornecedoresDaAcaoBonificacao(id)

    End Function


    ''' <summary>
    ''' Inserir dados dos fornecedores da acao no banco de dados 
    ''' </summary>
    ''' <param name="IndEnvGrv">IndEnvGrv = 0 = para gravar, IndEnvGrv = 1 = para enviar</param>
    ''' <param name="acaoComercial"></param>
    Public Sub InserirDadosFornecedores(IndEnvGrv As Integer, acaoComercial As VO.AcaoComercial)

        Try
            Dim listaFornecedores = acaoComercial.Fornecedores

            Dim reservaSaldoExtraAcordoList As New List(Of VO.MovimentoDiarioReservaDeSaldoFornecedor)
            'Dim reservaSaldoReceitaCMVList As New List(Of VO.MovimentoDiarioReservaDeSaldoFornecedor)  ' So sera usado extra acordo

            For Each fornecedor As VO.RelacaoAcaoComercialxFornecedores In listaFornecedores

                Dim ResExtBde As Double = 0 'Resto da taxa de brinde a ser adicionado no valor Extra. 
                Dim ResCttBde As Double = 0 'Resto da taxa de brinde a ser adicionado no valor Contrato. 
                Dim ResCMV As Double = 0

                Dim SldCtt As Double = If(fornecedor.VLRSLDDSNCTTACOCMC, 0)
                Dim SldCntCrr As Double = If(fornecedor.VLRSLDDSNDSCACOCMC, 0)
                Dim CMV As Double = If(fornecedor.VLRUTZRCTCSTMER, 0)
                Dim CMVaux As Double = If(fornecedor.VLRUTZRCTCSTMER, 0)
                Dim SldMrg As Double = If(fornecedor.VLRSLDDSNACR, 0)
                Dim txaBde As Double = If(fornecedor.VLRTXAADMBDEACOCMC, 0)
                Dim ctt As Double = If(fornecedor.VLRUTZCTTACOCMC, 0)
                Dim cttaux As Double = If(fornecedor.VLRUTZCTTACOCMC, 0)
                Dim ext As Double = If(fornecedor.VLRUTZEXACTTACOCMC, 0)
                Dim extaux As Double = If(fornecedor.VLRUTZEXACTTACOCMC, 0)

                Dim empenhoExaAcao As String = acaoComercial.TIPDSNDSCBNF
                Dim empenhoFrnMkt As String = empenhoExaAcao

                ' Se existe valor na taxa de brinde
                If txaBde <> 0 Then
                    CalTxaBde(CMV, CMVaux, SldMrg, SldCtt, SldCntCrr, txaBde, ctt, cttaux, ext, extaux)
                    ResExtBde = extaux
                    ResCttBde = cttaux
                    ResCMV = CMVaux
                End If

                '0 Codigo da acao 0
                fornecedor.CODACOCMC = acaoComercial.CODACOCMC

                '1 Código do fornecedor
                'CODFRN

                '2 Valor da participação do fornecedor
                If empenhoFrnMkt = Constantes.EMPENHO_FORNECEDOR_MARKETING Then
                    fornecedor.VLRPTCFRNACOCMC = extaux
                End If

                '3 Valor da txa de adm do fornecedor 
                fornecedor.VLRTXAADMFRNACOCMC = If(fornecedor.VLRTXAADMFRNACOCMC, 0)

                '4 Saldo da conta corrente do fornecedor
                fornecedor.VLRSLDDSNDSCACOCMC = If(fornecedor.VLRSLDDSNDSCACOCMC, 0)
                fornecedor.VLRSLDCNTCRRFRN = fornecedor.VLRSLDDSNDSCACOCMC

                '5 Empenho do contrato
                fornecedor.TIPDSNDSCBNF = If(fornecedor.TIPDSNDSCBNF, 0)

                '6 Taxa de brinde  
                fornecedor.VLRTXAADMBDEACOCMC = If(fornecedor.VLRTXAADMBDEACOCMC, 0)

                '7 Valor utilizado do contrato 
                Dim valorUtilizadoContrato = ctt
                If IndEnvGrv = 1 Then
                    valorUtilizadoContrato += ResCttBde 'Alteração feita em 08-11-06 Douglas Otoni - acrescentar ResCttBde ao vlrctt
                End If
                fornecedor.VLRUTZCTTACOCMC = valorUtilizadoContrato

                '8 Valor utilizado extra contrato 
                Dim valorUtilizadoExtraContrato = ext
                If empenhoFrnMkt = Constantes.EMPENHO_FORNECEDOR_MARKETING Then
                    valorUtilizadoExtraContrato = extaux
                End If
                If IndEnvGrv = 1 Then
                    valorUtilizadoExtraContrato += ResExtBde 'Alteração feita em 08-11-06 Douglas Otoni - acrescentar ResExtBde ao vlrext
                End If
                fornecedor.VLRUTZEXACTTACOCMC = CDec(valorUtilizadoExtraContrato)

                '9 Empenho Extra Contrato
                fornecedor.TIPDSNDSCBNFEXACTT = empenhoExaAcao

                '10 BU
                'fornecedor.CODUNDESRNGC

                '11 Saldo de acordo do fornecedor
                fornecedor.VLRSLDDSNCTTACOCMC = If(fornecedor.VLRSLDDSNCTTACOCMC, 0)

                '12 Beneficio
                If (fornecedor.DESBFCFRNACOCMC Is Nothing) Then
                    fornecedor.DESBFCFRNACOCMC = "  "
                End If

                '13 Empenho Receita CMV
                Dim empenhoReceita As Decimal = 0
                Dim listSaldoReceita = BuscaSaldoReceitaCMV(fornecedor.CODFRN)
                If listSaldoReceita IsNot Nothing AndAlso listSaldoReceita.Count > 0 Then
                    empenhoReceita = listSaldoReceita.FirstOrDefault().TIPDSNDSCBNF
                    fornecedor.TIPDSNDSCBNFACR = empenhoReceita
                Else
                    fornecedor.TIPDSNDSCBNFACR = empenhoReceita
                End If

                '14 Saldo de acordo do fornecedor
                If acaoComercial.CODSTAAPVACOCMC = Constantes.Status.NOVA Then
                    fornecedor.VLRSLDDSNACR = 0
                Else
                    fornecedor.VLRSLDDSNACR = If(fornecedor.VLRSLDDSNACR, 0)
                End If

                '15 VALOR Utilizado Receita CMV
                fornecedor.VLRUTZRCTCSTMER = If(fornecedor.VLRUTZRCTCSTMER, 0)

                '16 DESCRICAO DA SELECAO DOS FORNECEDORES
                fornecedor.DESSELFRN = If(fornecedor.DESSELFRN, " ")

                If IndEnvGrv = 1 Then
                    If (fornecedor.CODFRN <> Constantes.DESPESAS_MARTINS And acaoComercial.TIPACOMCD = TipoAcao.Fornecedor) Then
                        If (fornecedor.TIPDSNDSCBNFEXACTT <> 0 And fornecedor.VLRUTZEXACTTACOCMC <> 0) Then
                            reservaSaldoExtraAcordoList.Add(New VO.MovimentoDiarioReservaDeSaldoFornecedor With {
                                        .CODFRN = fornecedor.CODFRN,
                                        .CODACOCMC = acaoComercial.CODACOCMC.GetValueOrDefault().ToString(),
                                        .TIPDSNDSCBNF = fornecedor.TIPDSNDSCBNFEXACTT,
                                        .VLRSLDRSVFRN = fornecedor.VLRUTZEXACTTACOCMC,
                                        .FLGAPVACOCMC = "N",
                                        .FLGLMTCNTCRR = " ",
                                        .TIPACOCMC = "C",
                                        .VLRACEACOCMC = 0,
                                        .VLRRLZACOCMC = 0,
                                        .DATAPVACOCMC = New DateTime
                            })
                        End If
                    End If
                End If

                'Insere o fornecedor na tabela de relacionamento
                DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).InserirDadosFornecedor(fornecedor)
            Next

            'Se estiver enviando então reserva o saldo do fornecedor 
            If IndEnvGrv = 1 Then

                Dim boMovimentoDiarioReservaDeSaldoFornecedor As New MovimentoDiarioReservaDeSaldoFornecedorBO()

                boMovimentoDiarioReservaDeSaldoFornecedor.DeletePorCodAcao(acaoComercial.CODACOCMC)

                For Each mov As VO.MovimentoDiarioReservaDeSaldoFornecedor In reservaSaldoExtraAcordoList
                    boMovimentoDiarioReservaDeSaldoFornecedor.InsertT(mov)
                Next

                ' Só sera usado extra acordo.
                'For Each mov As VO.MovimentoDiarioReservaDeSaldoFornecedor In reservaSaldoReceitaCMVList
                '    boMovimentoDiarioReservaDeSaldoFornecedor.InsertT(mov)
                'Next
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' DelDdoAcoCmcFrn
    ''' Deleta todos os dados dos fornecedores da acao
    ''' </summary>
    ''' <param name="idAcao"></param>
    Public Sub DeletarDadosFornecedorDaAcao(idAcao As Integer)

        DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).DeletarDadosFornecedorDaAcao(idAcao)

    End Sub


    ''' <summary>
    ''' Insere dados Fornecedores da Acao
    ''' </summary>
    ''' <param name="fornecedorAcao"></param>
    Public Sub InserirDadosFornecedoresBD(fornecedorAcao As VO.RelacaoAcaoComercialxFornecedores)

        DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).InserirDadosFornecedor(fornecedorAcao)

    End Sub

    ''' <summary>
    ''' CalRatFrn
    ''' </summary>
    ''' <param name="SldCtt"></param>
    ''' <param name="SldCntCrr"></param>
    ''' <param name="ValRatAux"></param>
    ''' <param name="ctt"></param>
    ''' <param name="ext"></param>
    Public Sub CalRatFrn(ByVal SldCtt As Double, ByVal SldCntCrr As Double, ByVal ValRatAux As Double,
                                  ByRef ctt As Double, ByRef ext As Double)

        Dim SldExaCttBde As Decimal = SldCntCrr - ext 'Saldo Extra Contrato Disponivel
        Dim SldCttBde As Decimal = SldCtt - ctt 'Saldo Contrato Disponivel

        If SldExaCttBde >= ValRatAux Then
            ext = ext + ValRatAux 'coloca bde na totalidade no ext
        ElseIf SldCttBde >= ValRatAux Then
            ctt = ctt + ValRatAux 'coloca bde na totalidade no ctt
        Else
            If SldExaCttBde > SldCttBde Then
                ext = SldExaCttBde
                ctt = (ValRatAux - SldExaCttBde)
            Else
                ctt = SldCttBde
                ext = (ValRatAux - SldCttBde)
            End If
        End If
    End Sub

    Public Sub CalTxaBde(ByVal CMV As Double, ByVal CMVaux As Double, ByVal SldReceitaCMV As Double, ByVal SldCtt As Double, ByVal SldCntCrr As Double,
                              ByVal txaBde As Double, ByVal ctt As Double, ByRef cttAux As Double, ByVal ext As Double, ByRef extaux As Double)

        Dim SldExaCttBde As Double = SldCntCrr - ext 'Saldo Extra Contrato Disponivel
        Dim SldCttBde As Double = SldCtt - ctt 'Saldo Contrato Disponivel
        Dim SldReceitaCMVBde As Double = SldReceitaCMV - CMV 'Saldo Receita CMV Disponivel
        txaBde -= SldReceitaCMV 'Utilizando Saldo de Margem para pagar taxa de brinde
        If txaBde < 0 Then txaBde = 0 'Se o valor ficar negativo (Saldo Maior que taxa) entao zerar o valor da taxa de brinde)

        If SldExaCttBde >= txaBde Then
            extaux = ext + txaBde 'coloca bde na totalidade no ext
        ElseIf SldCttBde >= txaBde Then
            cttAux = ctt + txaBde 'coloca bde na totalidade no ctt
        ElseIf SldReceitaCMVBde >= txaBde Then
            CMVaux = CMV + txaBde 'coloca bde na totalidade no ctt
        Else
            If SldExaCttBde + SldCttBde + SldReceitaCMVBde >= txaBde Then 'Verifico se o saldo (cx1 + cx2) é > que Brinde
                If SldExaCttBde > SldCttBde Then 'Verifico se a EXACTT é maior que CTT
                    'Se for, entao coloco tudo nesta caixa e coloco o restante na outra caixa
                    extaux = SldCntCrr
                    cttAux = Math.Abs(txaBde - SldExaCttBde) + cttAux
                ElseIf SldExaCttBde < SldCttBde Then 'Verifico se a CTT é maior que EXACTT
                    'Se for, entao coloco tudo nesta caixa e coloco o restante na outra caixa
                    cttAux = SldCtt
                    extaux = Math.Abs(txaBde - SldCttBde) + extaux
                ElseIf ((SldExaCttBde + SldCttBde) = txaBde) Then 'Se CTT + EXACTT = BRINDE
                    cttAux = SldCtt
                    extaux = SldCntCrr
                Else
                    If (Math.Abs(txaBde - SldCttBde) + extaux) <= SldCntCrr Then
                        cttAux = SldCtt
                        extaux = Math.Abs(txaBde - SldCttBde) + extaux
                    Else
                        extaux = SldCntCrr
                        cttAux = Math.Abs(txaBde - SldExaCttBde) + cttAux
                    End If
                End If
            End If
        End If
    End Sub

    Public Function BuscaFornecedor(codFrn As Decimal?) As VO.RelacaoAcaoComercialxFornecedores

        Return DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BuscaFornecedor(codFrn)

    End Function

    Public Function BuscaFornecedores(codFrn As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim BuscaListaFornecedores As New List(Of VO.RelacaoAcaoComercialxFornecedores)
        If codFrn = Constantes.DESPESAS_MARTINS Then
            BuscaListaFornecedores.Add(FornecedorDespesaMartins)
        Else
            BuscaListaFornecedores = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BuscaFornecedores(codFrn)
            If (codFrn Is Nothing) Then
                BuscaListaFornecedores.Add(FornecedorDespesaMartins)
            End If
        End If

        Return BuscaListaFornecedores

    End Function

    Public Function BuscaFornecedoresEdl(codFrn As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim BuscaListaFornecedores As New List(Of VO.RelacaoAcaoComercialxFornecedores)
        BuscaListaFornecedores = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BuscaFornecedoresEdl(codFrn)
        Return BuscaListaFornecedores

    End Function

    Private Function FornecedorDespesaMartins() As VO.RelacaoAcaoComercialxFornecedores
        Dim despesas As New VO.RelacaoAcaoComercialxFornecedores
        despesas.CODFRN = Constantes.DESPESAS_MARTINS
        despesas.NOMFRN = "DESPESAS MARTINS"
        despesas.TIPDSNDSCBNF = 20
        despesas.DESDSNDSCBNF = "PROMOCOES MARTINS"
        despesas.VLRUTZCTTACOCMC = 0
        despesas.VLRUTZEXACTTACOCMC = 0
        despesas.VLRPTCFRNACOCMC = 0
        despesas.VLRUTZRCTCSTMER = 0
        despesas.VLRTXAADMFRNACOCMC = 0
        despesas.VLRTXAADMBDEACOCMC = 0

        Return despesas
    End Function
    Public Function BuscaFornecedoresDaCelula(codCelula As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim BuscaFornecedoresCelula = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BuscaFornecedoresDaCelula(codCelula)

        Return BuscaFornecedoresCelula

    End Function

    Public Function BuscaFornecedoresDaDiretoria(codDiretoria As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim BuscaFornecedoresDiretoria = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BuscaFornecedoresDaDiretoria(codDiretoria)

        Return BuscaFornecedoresDiretoria

    End Function

    'Buscando BU
    Public Function BuscaUnidadeNegocioFornecedor(codFrn As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        'Buscando BU
        Dim NovaListaUnidadeNegocioFornecedor = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BscUndNgcFrn(codFrn)

        Return NovaListaUnidadeNegocioFornecedor

    End Function

    'Busca Empenho de Acordo para os fornecedores
    Public Function BuscaEmpenhoAcordoFornecedor(codFrn As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim NovaListaEmpenhoAcordoFornecedor = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BscDesDsnDscBnfCtt(codFrn)

        Return NovaListaEmpenhoAcordoFornecedor

    End Function

    'Busca saldo do empenho
    Public Function BuscaSaldoEmpenho(codFrn As Decimal?, iCodEmpExaCtt As Integer) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        'Obtendo o AnoMesRef
        Dim iAnoMesRef = Split(Format(Now, "dd/MM/yyyy"), "/")(2) & Split(Format(Now, "dd/MM/yyyy"), "/")(1)

        Dim NovaListaSaldoEmpenho = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BscSldCttFrn(codFrn, iCodEmpExaCtt, iAnoMesRef)

        Return NovaListaSaldoEmpenho

    End Function

    'Obtendo Saldo de Extra Contrato
    Public Function BuscaSaldoExtraContrato(codFrn As Decimal?, iCodEmpExaCtt As Integer) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        'Obtendo o AnoMesRef
        Dim iAnoMesRef = Split(Format(Now, "dd/MM/yyyy"), "/")(2) & Split(Format(Now, "dd/MM/yyyy"), "/")(1)

        Dim NovaListaSaldoExtraContrato = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BscSldCntCrrFrn(codFrn, iCodEmpExaCtt, iAnoMesRef)

        Return NovaListaSaldoExtraContrato

    End Function


    'Obtendo Saldo da Receita CMV
    ''' <summary>
    ''' BscSldReceitaCMV
    ''' </summary>
    ''' <param name="codFrn"></param>
    ''' <returns></returns>
    Public Function BuscaSaldoReceitaCMV(codFrn As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim NovaListaSaldoReceitaCMV = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BscSldReceitaCMV(codFrn)

        Return NovaListaSaldoReceitaCMV

    End Function

    'Public Function AdicionaFornecedoresFiltro(codFrn As Decimal?, codCelula As Decimal?, codDiretoria As Decimal?, iCodEmpExaCtt As Integer) As List(Of VO.RelacaoAcaoComercialxFornecedores)
    '    Dim NovaListaFornecedores = New List(Of VO.RelacaoAcaoComercialxFornecedores)
    '    Dim NovaListaBscDdoDtlFrn = New List(Of VO.RelacaoAcaoComercialxFornecedores)
    '    Dim ListaRetorno = New List(Of VO.RelacaoAcaoComercialxFornecedores)
    '    Dim sGrpCodFrn As String = ""

    '    If codFrn IsNot Nothing Then
    '        NovaListaFornecedores = BuscaFornecedores(codFrn)
    '    ElseIf codCelula IsNot Nothing Then
    '        NovaListaFornecedores = BuscaFornecedoresDaCelula(codCelula)
    '    ElseIf codDiretoria IsNot Nothing Then
    '        NovaListaFornecedores = BuscaFornecedoresDaDiretoria(codDiretoria)
    '    End If

    '    'For i As Integer = 0 To NovaListaFornecedores.Count - 1
    '    '    sGrpCodFrn += NovaListaFornecedores.Item(i).CODFRN.ToString().Trim() & ", "
    '    'Next

    '    'sGrpCodFrn = sGrpCodFrn.Substring(0, sGrpCodFrn.Length - 1)

    '    'If Not sGrpCodFrn Is Nothing Then
    '    '    If NovaListaFornecedores.Count <= 20 Then
    '    '        NovaListaBscDdoDtlFrn = BscDdoDtlFrn(sGrpCodFrn, iCodEmpExaCtt)
    '    '        For i As Integer = 0 To NovaListaBscDdoDtlFrn.Count - 1
    '    '            NovaListaBscDdoDtlFrn.Item(i).CODFRN = NovaListaFornecedores.Item(i).CODFRN
    '    '            NovaListaBscDdoDtlFrn.Item(i).NOMFRN = NovaListaFornecedores.Item(i).NOMFRN
    '    '        Next
    '    '        Return NovaListaBscDdoDtlFrn
    '    '    Else
    '    '        'Caso sejam varios fornecedores
    '    '        'If MsgBox("Deseja obter os dados (Saldos, BU e Empenhos de acordo) para todos os fornecedores inseridos ?" & vbNewLine & "Essa operação poderá demorar alguns instantes pois serão processados " & (sGrpCodFrn.Split(",").Length - 1) & " fornecedores.", "Ação Mercadológica", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
    '    '        If MsgBox("Deseja obter os dados (Saldos, BU e Empenhos de acordo) para todos os fornecedores inseridos ?" & vbNewLine & "Essa operação poderá demorar alguns instantes pois serão processados ", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
    '    '            NovaListaBscDdoDtlFrn = BscDdoDtlFrn(sGrpCodFrn, iCodEmpExaCtt)
    '    '            For i As Integer = 0 To NovaListaBscDdoDtlFrn.Count - 1
    '    '                NovaListaBscDdoDtlFrn.Item(i).CODFRN = NovaListaFornecedores.Item(i).CODFRN
    '    '                NovaListaBscDdoDtlFrn.Item(i).NOMFRN = NovaListaFornecedores.Item(i).NOMFRN
    '    '            Next
    '    '            Return NovaListaBscDdoDtlFrn
    '    '        End If
    '    '    End If
    '    'End If
    '    NovaListaFornecedores = BscDdoDtlFrn(NovaListaFornecedores, iCodEmpExaCtt)
    '    Return NovaListaFornecedores

    'End Function

    ''' <summary>
    ''' Busca os fornecedores a partir do filtro do modal de fornecedor
    ''' </summary>
    ''' <param name="codFrn"></param>
    ''' <param name="codCelula"></param>
    ''' <param name="codDiretoria"></param>
    ''' <param name="iCodEmpExaCtt"></param>
    ''' <returns></returns>
    Public Function BuscaFornecedoresFiltro(codFrn As Decimal?, codCelula As Decimal?, codDiretoria As Decimal?, iCodEmpExaCtt As Integer, indPreAcordo As Boolean, tipoFornecedor As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim NovaListaFornecedores = New List(Of VO.RelacaoAcaoComercialxFornecedores)
        Try
            If codFrn IsNot Nothing Then
                If (tipoFornecedor = 1) Then
                    NovaListaFornecedores = BuscaFornecedoresEdl(codFrn)
                Else
                    NovaListaFornecedores = BuscaFornecedores(codFrn)
                End If
            ElseIf codCelula IsNot Nothing Then
                NovaListaFornecedores = BuscaFornecedoresDaCelula(codCelula)
            ElseIf codDiretoria IsNot Nothing Then
                NovaListaFornecedores = BuscaFornecedoresDaDiretoria(codDiretoria)
            End If

            'TODO: LIMPAR GRID SE TIVER DESPESAS MARTINS

            'If NovaListaFornecedores.Count <= 20 Then
            NovaListaFornecedores = BscDdoDtlFrn(NovaListaFornecedores, iCodEmpExaCtt, indPreAcordo)
            Return NovaListaFornecedores
            'End If
        Catch ex As Exception
            Throw ex
        End Try
        Return NovaListaFornecedores

    End Function


    ''' <summary>
    ''' Completa dados dos fornecedores
    ''' </summary>
    ''' <param name="listaFornecedores"></param>
    ''' <param name="iCodEmpExaCtt"></param>
    ''' <returns></returns>
    Public Function BscDdoDtlFrn(listaFornecedores As List(Of VO.RelacaoAcaoComercialxFornecedores), iCodEmpExaCtt As Integer, indPreAcordo As Boolean) As List(Of VO.RelacaoAcaoComercialxFornecedores)
        Try
            'Obtendo o AnoMesRef
            Dim iAnoMesRef As Integer = Split(Format(Now, "dd/MM/yyyy"), "/")(2) & Split(Format(Now, "dd/MM/yyyy"), "/")(1)
            Dim fornecedorAux = New VO.RelacaoAcaoComercialxFornecedores
            For Each fornecedor As VO.RelacaoAcaoComercialxFornecedores In listaFornecedores

                If (fornecedor.CODFRN = Constantes.DESPESAS_MARTINS) Then
                    Dim boBU As New UnidadeEstrategicaDeNegocioBO
                    Dim Bus As VO.UnidadeEstrategicaDeNegocio = boBU.BuscaTodosCodNomUnidadeNegocio().OrderBy(Function(x) x.CODUNDESRNGC).FirstOrDefault()
                    fornecedor.CODUNDESRNGC = Bus.CODUNDESRNGC
                    fornecedor.DESUNDESRNGC = Bus.DESUNDESRNGC
                Else
                    'Buscando BU
                    fornecedorAux = BuscarUnicaUnidadeNegocioFornecedor(fornecedor.CODFRN)
                    fornecedor.CODUNDESRNGC = fornecedorAux.CODUNDESRNGC
                    fornecedor.DESUNDESRNGC = fornecedorAux.DESUNDESRNGC

                    'Busca Empenho de Acordo para os fornecedores do "Array"
                    fornecedorAux = BscDesDsnDscBnfCtt(fornecedor.CODFRN)
                    fornecedor.TIPDSNDSCBNF = fornecedorAux.TIPDSNDSCBNF
                    fornecedor.DESDSNDSCBNF = fornecedorAux.DESDSNDSCBNF

                    If (indPreAcordo = False) Then
                        If fornecedor.TIPDSNDSCBNF IsNot Nothing Then
                            fornecedor.VLRSLDDSNCTTACOCMC = BuscaValorSaldoAcordoFornecedor(fornecedor.CODFRN, fornecedor.TIPDSNDSCBNF, iAnoMesRef)
                        End If

                        'Obtendo Saldo de Extra Contrato
                        fornecedor.VLRSLDDSNDSCACOCMC = BuscaValorSaldoExaAcordoFornecedor(fornecedor.CODFRN, iCodEmpExaCtt, iAnoMesRef)

                        'Obtendo Saldo da Receita CMV
                        fornecedor.VLRSLDDSNACR = BuscaSaldoReceitaCMVFornecedor(fornecedor.CODFRN)
                    End If
                End If
            Next

            Return listaFornecedores
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function BscDdoDtlFrn(sGrpCodFrn As String, iCodEmpExaCtt As Integer) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim NovaListaBscDdoDtlFrn = New List(Of VO.RelacaoAcaoComercialxFornecedores)
        Dim dtsFrn, dtsUndNgcFrn, dtsBnfCtt, dtsSldCtt, dtsSldExaCtt, dsSldCMV As New List(Of VO.RelacaoAcaoComercialxFornecedores)
        Dim iAnoMesRef As Integer = 0
        Dim sCodFrn As String = ""

        'Obtendo o AnoMesRef
        iAnoMesRef = Split(Format(Now, "dd/MM/yyyy"), "/")(2) & Split(Format(Now, "dd/MM/yyyy"), "/")(1)

        For NroAux As Integer = 0 To (sGrpCodFrn.Split(",").Length - 1)

            Dim Ddo As New VO.RelacaoAcaoComercialxFornecedores

            'Incluindo o Código do fornecedor
            Ddo.CODFRN = sGrpCodFrn.Split(",")(NroAux)

            'Buscando BU
            dtsUndNgcFrn = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BscUndNgcFrn(sGrpCodFrn.Split(",")(NroAux))
            If dtsUndNgcFrn.Count > 0 Then
                'traz o BU como string concatenando o id dele com a descrição
                Ddo.CODUNDESRNGC = dtsUndNgcFrn.Item(0).CODUNDESRNGC
                Ddo.DESUNDESRNGC = Trim(dtsUndNgcFrn.Item(0).DESUNDESRNGC)
            Else
                Ddo.CODUNDESRNGC = Nothing
                Ddo.DESUNDESRNGC = ""
            End If

            'Busca Empenho de Acordo para os fornecedores do "Array"
            dtsBnfCtt = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BscDesDsnDscBnfCtt(sGrpCodFrn.Split(",")(NroAux))
            If dtsBnfCtt.Count > 0 Then
                'busca o Empenho como string concatenando o id dele com a descrição
                Ddo.TIPDSNDSCBNF = dtsBnfCtt.Item(0).TIPDSNDSCBNF
                Ddo.DESDSNDSCBNF = Trim(dtsBnfCtt.Item(0).DESDSNDSCBNF)

                'Busca saldo do empenho
                dtsSldCtt = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BscSldCttFrn(sGrpCodFrn.Split(",")(NroAux), dtsBnfCtt.Item(NroAux).TIPDSNDSCBNF, iAnoMesRef)
                If dtsSldCtt.Count > 0 Then
                    Ddo.VLRUTZCTTACOCMC = dtsSldCtt.Item(0).VLRSLDDSP
                Else
                    Ddo.VLRUTZCTTACOCMC = 0
                End If
            Else
                Ddo.TIPDSNDSCBNF = Nothing
                Ddo.DESDSNDSCBNF = ""
                Ddo.VLRUTZCTTACOCMC = 0
            End If

            'Obtendo Saldo de Extra Contrato
            dtsSldExaCtt = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BscSldCntCrrFrn(sGrpCodFrn.Split(",")(NroAux), iCodEmpExaCtt, iAnoMesRef)
            If dtsSldExaCtt.Count > 0 Then
                Ddo.VLRSLDDSNDSCACOCMC = dtsSldExaCtt.Item(0).VLRSLDDSP
            Else
                Ddo.VLRSLDDSNDSCACOCMC = 0
            End If

            'Obtendo Saldo da Receita CMV
            dsSldCMV = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BscSldReceitaCMV(sGrpCodFrn.Split(",")(NroAux))
            If dsSldCMV.Count > 0 Then
                Ddo.VLRUTZCTTACOCMC = dsSldCMV.Item(0).VLRSLDDSP
            Else
                Ddo.VLRUTZCTTACOCMC = 0
            End If

            dtsFrn.Add(Ddo)

        Next

        Return dtsFrn

    End Function

    Public Sub EnviarAcoCmcFornecedor(ByVal acaoComercial As VO.AcaoComercial, ByVal incentivosHabilitado As Boolean, ByRef bBotaoPolitica As Boolean, ByVal benviar As Boolean, ByVal bDesabilitabotao As Integer, ByVal indPreAcordo As Boolean)
        Dim IndPol As Integer = 0
        Dim VlrTot As Decimal = 0
        Dim VlrTotOcd As Decimal = 0
        Dim itemfuncionario As VO.Funcionario
        Dim itemfuncionario2 As VO.Funcionario
        Dim status As Integer = 1
        Dim boAcaoComercial As New AcaoComercialBO()
        boFuncionarios = New FuncionarioBO

        Try

            If VerificaTaxaBrinde(acaoComercial) Then

                If VerificaDadosAcao(acaoComercial, incentivosHabilitado, indPreAcordo) Then

                    'Verifica se o valor orçado é igual ao valor gasto
                    VlrTot = acaoComercial.ValorParticipacao 'valor de participação
                    VlrTotOcd = acaoComercial.ValorTotal 'valor total orcamento

                    If VlrTot <> VlrTotOcd Then
                        If VlrTot > VlrTotOcd Then
                            Throw New MappedWarnException("Valor da ação maior que o valor dos itens do orçamento.")
                        Else
                            Throw New MappedWarnException("Valor da ação menor que o valor dos itens do orçamento.")
                        End If
                    Else
                        If bBotaoPolitica = True And acaoComercial.DESCSTA = "NOVA" Then
                            boPoliticas.DeletarDadoAcaoComercialPoliticas(acaoComercial.CODACOCMC)
                            bBotaoPolitica = False
                        End If

                        If acaoComercial.CODACOCMC Is Nothing Then
                            'Busca o próximo código para a Ação
                            acaoComercial.CODACOCMC = boAcaoComercial.BuscarMaxCodAcaoComercial() + 1
                        End If

                        IndPol = InsDdoPol(acaoComercial)
                        If IndPol <> 0 Then

                            If benviar Then
                                If acaoComercial.ItensOrcamento.Count > 0 Then 'buscar itens oçamento para ve se ja foi inserido 
                                    Dim gerente As New VO.Funcionario
                                    Dim codigo As Decimal?
                                    If IsNothing(acaoComercial.confirmaEnviarGerente) Then
                                        gerente = boFuncionarios.buscaGerenteDefault(acaoComercial.CODACOCMC, ControleAcesso.CodFnc())
                                        If (gerente Is Nothing) Then
                                            Throw New MappedWarnException("Não existe relacionamento entre o Funcionário x Gerente de Marketing! ")
                                        Else
                                            codigo = gerente.CODFNC
                                        End If
                                    Else
                                        codigo = acaoComercial.confirmaEnviarGerente
                                    End If

                                    itemfuncionario = boFuncionarios.selecionaGerente(codigo, "").FirstOrDefault()
                                    Dim funcfiltro As New VO.Funcionario
                                    funcfiltro.CODFNC = ControleAcesso.CodFnc()

                                    'Busca o funcionário logado.
                                    ' itemfuncionario2 = boFuncionarios.SelectT(funcfiltro)

                                    'Busca o funcionário ou terceiro logado.
                                    itemfuncionario2 = boFuncionarios.BuscaFuncionariosETerceiros(funcfiltro)

                                    If Not AtuFluApvAcoCmc(acaoComercial.CODACOCMC,
                                                           acaoComercial.CODFNCCRIACOCMC,
                                                           itemfuncionario2.NOMFNC,
                                                           Programa.ACOES_MERCADOLOGICAS,
                                                           NumSeqAprovador.GER_CATEGORIA,
                                                           TipoUsuario.GER_CATEGORIA,
                                                           TipoUsuario.DIR_COMPRAS,
                                                           tipoFluxo.VALOR,
                                                           itemfuncionario.CODFNC,
                                                           itemfuncionario.NOMFNC,
                                                           indPreAcordo) Then

                                        Throw New MappedWarnException("Erro: Os dados da ação foram gravados com sucesso, porém ocorreram erros na gravação do fluxo de aprovação. Por favor, tente enviar novamente esta ação.")
                                    Else
                                        bDesabilitabotao = 1
                                    End If
                                End If                                '
                            Else
                                '
                                If acaoComercial.ItensOrcamento.Count > 0 Then
                                    ''Grava associação de Incentivo x Ação Comercial
                                    'If acaoComercial.INDVLDRGRICT = 1 And acaoComercial.Incentivos IsNot Nothing Then
                                    '    'Atrela Incentivo a Ação Mercadologica que esta sendo cadastrada
                                    '    boIncentivo.DeletarIncentivoDaAcao(acaoComercial.CODACOCMC)
                                    '    boIncentivo.InserirGrupoDeIncentivo(acaoComercial.CODACOCMC, acaoComercial.Incentivos)
                                    'Else
                                    '    'DelIctAcoCmc(acaoComercial.CODACOCMC))
                                    '    boIncentivo.DeletarIncentivoDaAcao(acaoComercial.CODACOCMC)
                                    'End If

                                    'btPolAcoCmc.Visible = True

                                End If 'MessageBox
                            End If
                        Else 'Políticas

                            'insere dados dos itens de orçamento
                            'If acaoComercial.ItensOrcamento.Count > 0 Then
                            Dim gerente As New VO.Funcionario
                                Dim codigo As Decimal?
                                If IsNothing(acaoComercial.confirmaEnviarGerente) Then
                                    gerente = boFuncionarios.buscaGerenteDefault(acaoComercial.CODACOCMC, ControleAcesso.CodFnc())
                                    If (gerente Is Nothing) Then
                                        Throw New MappedWarnException("Não existe relacionamento entre o Funcionário x Gerente de Marketing! ")
                                    Else
                                        codigo = gerente.CODFNC
                                    End If
                                Else
                                    codigo = acaoComercial.confirmaEnviarGerente
                                End If
                                itemfuncionario = boFuncionarios.selecionaGerente(codigo, "").FirstOrDefault()
                                Dim funcfiltro As New VO.Funcionario
                                funcfiltro.CODFNC = ControleAcesso.CodFnc()

                                'Busca o funcionário logado.
                                'itemfuncionario = boFuncionarios.SelectT(funcfiltro)

                                'Busca o funcionário ou terceiro logado.
                                itemfuncionario2 = boFuncionarios.BuscaFuncionariosETerceiros(funcfiltro)

                                If Not AtuFluApvAcoCmc(acaoComercial.CODACOCMC, acaoComercial.CODFNCCRIACOCMC,
                                                       itemfuncionario2.NOMFNC,
                                                       Programa.ACOES_MERCADOLOGICAS,
                                                       NumSeqAprovador.GER_CATEGORIA,
                                                       TipoUsuario.GER_CATEGORIA,
                                                       TipoUsuario.DIR_COMPRAS,
                                                       tipoFluxo.VALOR,
                                                       itemfuncionario.CODFNC,
                                                       itemfuncionario.NOMFNC,
                                                       indPreAcordo) Then

                                    Throw New MappedWarnException("Erro: Os dados da ação foram gravados com sucesso, porém ocorreram erros na gravação do fluxo de aprovação. Por favor, tente enviar novamente esta ação, se essa mensagem persistir contacte o responsável.")
                                Else
                                    bDesabilitabotao = 2                                    '
                                End If
                                'End If
                            End If 'Políticas
                    End If 'VlrOcd <> VlrAco
                End If 'Dados preenchidos
            End If

        Catch 'ex As Exception
            Throw 'ex
        End Try


    End Sub

    Function AtuFluApvAcoCmcOld(ByVal CodAcoCmc As Decimal, ByVal CodFncEmi As Decimal, ByVal NomUsrEmi As String, ByVal CodPrg As Programa, ByVal NumSeqNivApv As NumSeqAprovador,
                             ByVal CodTipApvGerCtg As TipoUsuario, ByVal CodTipApvDirCmp As TipoUsuario, ByVal TipIdtFluCmp As tipoFluxo, ByVal CodGerMrk As Decimal, ByVal NomGerMrk As String, ByVal indPreAcordo As Boolean) As Boolean

        Dim dtsCabFluxoAprovacao As New List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)
        Dim dtsFluxoAprovacao As New List(Of VO.FluxoDeAprovacao)
        Dim iNumPrxFluApv As Integer
        Dim bFlgAlaEma As Boolean = False
        Dim CodSisInf As Integer
        Dim NumFluApv As Integer
        Dim boAcaoComercial As New AcaoComercialBO()

        CosFlxApvAcoCmc(CodFncEmi,
                        CodAcoCmc,
                        CodPrg,
                        iNumPrxFluApv,
                        NumSeqNivApv,
                        CodTipApvGerCtg,
                        CodTipApvDirCmp,
                        TipIdtFluCmp,
                        CodGerMrk,
                        NomGerMrk,
                        dtsCabFluxoAprovacao,
                        dtsFluxoAprovacao,
                        indPreAcordo)


        boRelacaoAcaoxFluxoApro = New RelacaoAcaoComercialxFluxoAprovacaoBO
        iNumPrxFluApv = boRelacaoAcaoxFluxoApro.BuscarMaxCodFluxo()

        If dtsCabFluxoAprovacao.Count > 0 Then
            boRelacaoAcaoxFluxoApro.IserirCabecalhoFluxo(dtsCabFluxoAprovacao, iNumPrxFluApv)
        End If

        If dtsFluxoAprovacao.Count > 0 Then
            For Each item In dtsFluxoAprovacao
                If (item.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao) Or (item.TIPSTAFLUAPV = TipStaFluApv.Pedido_Parecer) Then
                    bFlgAlaEma = True
                End If
                CodSisInf = item.CODSISINF
                NumFluApv = item.NUMFLUAPV
            Next

            boFluxoAprovacao = New FluxoDeAprovacaoBO
            Dim x = 0
            Dim sequencia As Integer = iNumPrxFluApv
            For Each item In dtsFluxoAprovacao.OrderBy(Function(t) t.NUMSEQNIVAPV)
                item.NUMFLUAPV = sequencia
                item.NUMSEQFLUAPV = x
                NumFluApv = sequencia
                x = x + 1
            Next

            boFluxoAprovacao.SalvarItensfluxo(dtsFluxoAprovacao)
        End If

        Dim acao As VO.AcaoComercial = boAcaoComercial.BuscaAcaoPorId(CodAcoCmc)

        If bFlgAlaEma Then
            boFluxoAprovacao.PcdMsgAlaFlu(CodAcoCmc, CodSisInf, NumFluApv, 42, CodFncEmi, NomUsrEmi, "", TipStaFluApv.Aprovado)
        End If

        For Each itemcabflux In dtsCabFluxoAprovacao
            boAcaoComercial = New AcaoComercialBO
            If boAcaoComercial.AtuStaAcoCmc(itemcabflux.CODACOCMC.GetValueOrDefault(), itemcabflux.CODSTAAPVACOCMC.GetValueOrDefault()) = 0 Then
                'Throw New Exception("Erro na atualização do status da ação mercadolófica.")
            End If
        Next

        Return True

    End Function

    Function AtuFluApvAcoCmc(ByVal CodAcoCmc As Decimal, ByVal CodFncEmi As Decimal, ByVal NomUsrEmi As String, ByVal CodPrg As Programa, ByVal NumSeqNivApv As NumSeqAprovador,
                             ByVal CodTipApvGerCtg As TipoUsuario, ByVal CodTipApvDirCmp As TipoUsuario, ByVal TipIdtFluCmp As tipoFluxo, ByVal CodGerMrk As Decimal, ByVal NomGerMrk As String, ByVal indPreAcordo As Boolean) As Boolean

        Dim dtsCabFluxoAprovacao As New List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)
        Dim dtsFluxoAprovacao As New List(Of VO.FluxoDeAprovacao)
        Dim iNumPrxFluApv As Integer
        Dim bFlgAlaEma As Boolean = False
        Dim boAcaoComercial As New AcaoComercialBO()

        CosFlxApvAcoCmc(CodFncEmi,
                        CodAcoCmc,
                        CodPrg,
                        iNumPrxFluApv,
                        NumSeqNivApv,
                        CodTipApvGerCtg,
                        CodTipApvDirCmp,
                        TipIdtFluCmp,
                        CodGerMrk,
                        NomGerMrk,
                        dtsCabFluxoAprovacao,
                        dtsFluxoAprovacao,
                        indPreAcordo)


        boRelacaoAcaoxFluxoApro = New RelacaoAcaoComercialxFluxoAprovacaoBO
        iNumPrxFluApv = boRelacaoAcaoxFluxoApro.BuscarMaxCodFluxo()

        If dtsCabFluxoAprovacao.Count > 0 Then
            boRelacaoAcaoxFluxoApro.IserirCabecalhoFluxo(dtsCabFluxoAprovacao, iNumPrxFluApv)
        End If

        If dtsFluxoAprovacao.Count > 0 Then
            For Each item In dtsFluxoAprovacao
                If (item.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao) Or (item.TIPSTAFLUAPV = TipStaFluApv.Pedido_Parecer) Then
                    bFlgAlaEma = True
                End If
                item.NUMFLUAPV = iNumPrxFluApv
            Next

            boFluxoAprovacao = New FluxoDeAprovacaoBO
            boFluxoAprovacao.SalvarItensfluxo(dtsFluxoAprovacao)
        End If

        Dim acao As VO.AcaoComercial = boAcaoComercial.BuscaAcaoPorId(CodAcoCmc)

        If bFlgAlaEma Then
            boFluxoAprovacao.PcdMsgAlaFlu(CodAcoCmc, CodPrg, iNumPrxFluApv, 42, CodFncEmi, NomUsrEmi, "", TipStaFluApv.Aprovado)
        End If

        For Each itemcabflux In dtsCabFluxoAprovacao
            boAcaoComercial = New AcaoComercialBO
            If boAcaoComercial.AtuStaAcoCmc(itemcabflux.CODACOCMC.GetValueOrDefault(), itemcabflux.CODSTAAPVACOCMC.GetValueOrDefault()) = 0 Then
                'Throw New Exception("Erro na atualização do status da ação mercadolófica.")
            End If
        Next

        Return True

    End Function
    Public Function PcdMsgAlaFlu(ByVal CodAcoCmc As Decimal, ByVal CodSisInf As Decimal, ByVal NumFluApv As Decimal,
                                 ByVal TipMsgCreEtn As Decimal, ByVal CodFnc As Decimal, ByVal NomFnc As String,
                                 ByVal NomUsrRec As String, ByVal iApv As TipStaFluApv, ByVal tipAcao As TipoAcao) As Integer

        Dim sTitulo As String = ""
        Dim iPrxNumSeqCreEtn As Decimal?
        Dim dtFluxoAprovacao As List(Of VO.FluxoDeAprovacao)
        Dim iNumSeqEndCreEtn As Integer = 0
        Dim MsgEnv As String = ""
        Dim iMultiplo As Integer
        Dim sPedacoMsg As String = ""
        Dim sMsgEnvAux As String = ""
        Dim iAuxIni As Integer
        Dim iAux As Single

        Select Case tipAcao
            Case TipoAcao.PreAcordoValor
                Select Case iApv
                    Case TipStaFluApv.Aprovado
                        sTitulo = "Aviso de aprovação! Pré-Acordo Promocional Limitado por Valor: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Resposta_Parecer
                        sTitulo = "Aviso de resposta de parecer! Pré-Acordo Promocional Limitado por Valor: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Rejeitado
                        sTitulo = "Aviso de reprovação! Pré-Acordo Promocional Limitado por Valor: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Pedido_Parecer
                        sTitulo = "Aviso de pedido de parecer! Pré-Acordo Promocional Limitado por Valor: " & CodAcoCmc.ToString() & "."
                End Select
            Case TipoAcao.PreAcordoVigencia
                Select Case iApv
                    Case TipStaFluApv.Aprovado
                        sTitulo = "Aviso de aprovação! Pré-Acordo Promocional Limitado por Vigência: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Resposta_Parecer
                        sTitulo = "Aviso de resposta de parecer! Pré-Acordo Promocional Limitado por Vigência: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Rejeitado
                        sTitulo = "Aviso de reprovação! Pré-Acordo Promocional Limitado por Vigência: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Pedido_Parecer
                        sTitulo = "Aviso de pedido de parecer! Pré-Acordo Promocional Limitado por Vigência: " & CodAcoCmc.ToString() & "."
                End Select
            Case Else
                Select Case iApv
                    Case TipStaFluApv.Aprovado
                        sTitulo = "Aviso de aprovação! Ação Mercadologica: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Resposta_Parecer
                        sTitulo = "Aviso de resposta de parecer! Ação Mercadologica: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Rejeitado
                        sTitulo = "Aviso de reprovação! Ação Mercadologica: " & CodAcoCmc.ToString() & "."
                    Case TipStaFluApv.Pedido_Parecer
                        sTitulo = "Aviso de pedido de parecer! Ação Mercadologica: " & CodAcoCmc.ToString() & "."
                End Select
        End Select


        boMovimentoMesnagem = New MovimentoDeMensagensDoCorreioEletronicoBO
        iPrxNumSeqCreEtn = boMovimentoMesnagem.CnsPrxNumSeqCreEtn(TipMsgCreEtn)

        Dim corpo As String = ""

        Select Case tipAcao
            Case TipoAcao.PreAcordoValor
                corpo = "Aviso de aprovação final do pré-acordo promocional por valor: - "
            Case TipoAcao.PreAcordoVigencia
                corpo = "Aviso de aprovação final do pré-acordo promocional por vigência: - "
            Case Else
                corpo = "Aviso de aprovação final da ação mercadológica: - "
        End Select

        If boMovimentoMesnagem.IsrMsgCreEtn(TipMsgCreEtn, iPrxNumSeqCreEtn, corpo & CodAcoCmc.ToString()) > 0 Then
            dtFluxoAprovacao = boFluxoAprovacao.CnsUsrNotEma(CodSisInf, iPrxNumSeqCreEtn)
            If dtFluxoAprovacao.Count > 0 Then
                iNumSeqEndCreEtn += 1

                borealcaoEndereco = New RelacaoDeEnderecosParaEnvioDoCorreioEletronicoBO
                borealcaoEndereco.IsrEndCreEtn(TipMsgCreEtn, iPrxNumSeqCreEtn, 1, iNumSeqEndCreEtn, NomFnc.Trim().ToLower() & "@martins.com.br")
                iNumSeqEndCreEtn = 0
                For Each itemema In dtFluxoAprovacao
                    iNumSeqEndCreEtn += 1
                    borealcaoEndereco.IsrEndCreEtn(TipMsgCreEtn, iPrxNumSeqCreEtn, 2, iNumSeqEndCreEtn, itemema.NOMUSRRCF.Trim().ToLower() & "@martins.com.br")
                Next

                MsgEnv = MntMsgEma(CodAcoCmc)

                'Looping para gravar de 120 em 120 caracteres no banco = 1 linha do email
                iAux = (MsgEnv.Length / 120)
                iAuxIni = 0
                '
                '
                While iAux > 0
                    '
                    If MsgEnv.Length <= 120 Then
                        sPedacoMsg = MsgEnv
                    ElseIf MsgEnv.Length > ((iAuxIni * 120) + 120) Then
                        sPedacoMsg = MsgEnv.Substring((iAuxIni * 120), 120)
                    Else
                        sPedacoMsg = MsgEnv.Substring((iAuxIni * 120), MsgEnv.Length - (iAuxIni * 120))
                    End If
                    '
                    RelacaoLinhasMSGCorreio.IsrLnhCreEtn(TipMsgCreEtn, iPrxNumSeqCreEtn, iAuxIni + 1, sPedacoMsg)
                    '
                    iAuxIni += 1
                    iAux -= 1
                    '
                End While
            End If
        End If


        Return 1

    End Function

    Public Function MntMsgEma(ByVal CodAcoCmc As Decimal) As String
        Dim itemacao As VO.AcaoComercial
        Dim listFornecedores As New List(Of VO.RelacaoAcaoComercialxFornecedores)
        Dim listPoliticasDivergente As New List(Of VO.PoliticasDaAcaoComercial)
        Dim listamecanica As New VO.RelacaoAcaoComercialxMecanicaDaAcao
        Dim listaacaoplano As New List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
        Dim listItemOrcamento As New List(Of VO.RelacaoAcaoComercialPorItemOrcamento)
        Dim lisIcentivos As New List(Of VO.RelacaoIncentivoxAcaoComercial)
        Dim lisrealfluxoaprov As New List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)

        Dim sMsgCpa, sMsgRgrAcoCmc, sMsgPolAcoCmc As String
        Dim sMsgPti, sMsgSldPlnMrk, sMsgOcd, sMsgIct As String
        Dim sMsgFlu As String
        Dim sMsg As StringBuilder
        Dim iAux As Integer
        Dim boAcaoComercial As New AcaoComercialBO()

        itemacao = boAcaoComercial.BscDdoAcoNumEma(CodAcoCmc)

        sMsg = New StringBuilder

        If Not itemacao Is Nothing Then
            sMsgCpa = MntMsgCab(itemacao, iAux)

            If sMsgCpa <> "" Then
                iAux = 1
                sMsg.Append(sMsgCpa)
            End If
        End If

        listamecanica = RelacaoAcaoMecanica.BuscarRegraMecanicaDaAcao(CodAcoCmc)
        sMsgRgrAcoCmc = MntMsgRgrAcoCmc(listamecanica, iAux)
        If sMsgRgrAcoCmc <> "" Then
            iAux = 1
            sMsg.Append(sMsgRgrAcoCmc)
        End If

        listPoliticasDivergente = PoliticasDaAcaoComercial.ListPolitcasAcao(CodAcoCmc)
        sMsgPolAcoCmc = MntMsgPolAcoCmc(listPoliticasDivergente, iAux)
        '
        If sMsgPolAcoCmc <> "" Then
            iAux = 1
            sMsg.Append(sMsgPolAcoCmc)
        End If

        listFornecedores = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BuscarFornecedoresDaAcao(CodAcoCmc)
        sMsgPti = MntMsgPti(listFornecedores, iAux)
        If sMsgPti <> "" Then
            iAux = 1
            sMsg.Append(sMsgPti)
        End If

        listaacaoplano = boRelacaoPlanoMarketing.ListSaldoPlanoDeMarketingAcao(CodAcoCmc)
        sMsgSldPlnMrk = MntMsgSldPlnMrk(listaacaoplano, iAux)
        '
        If sMsgPti <> "" Then
            iAux = 1
            sMsg.Append(sMsgSldPlnMrk)
        End If

        listItemOrcamento = boItemOrcamento.BuscarItensOrcamentoDaAcao(CodAcoCmc)
        sMsgOcd = MntMsgOcd(listItemOrcamento, iAux)
        If sMsgOcd <> "" Then
            sMsg.Append(sMsgOcd)
        End If

        lisIcentivos = boIncentivo.BuscarIncentivosDaAcao(CodAcoCmc)
        sMsgIct = MntMsgIct(lisIcentivos, iAux)
        If sMsgIct <> "" Then
            sMsg.Append(sMsgIct)
        End If

        lisrealfluxoaprov = boRelacaoAcaoxFluxoApro.BuscaFluxosAprovacaoDaAcao(CodAcoCmc)
        sMsgFlu = MntMsgFlu(lisrealfluxoaprov, iAux)
        If sMsgFlu <> "" Then
            sMsg.Append(sMsgFlu)
        End If

        Return sMsg.ToString()

    End Function

    Private Function MntMsgFlu(ByVal lisrealfluxoaprov As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao), ByVal iIndAux As Integer) As String
        Dim sMsgAux As StringBuilder
        Dim iAux As Integer = 0
        Dim sMsg As String
        Dim iTipStaAco As TipStaFluApv
        Dim iAuxIni As Integer
        Dim iAuxNro As Single
        Dim sMecAux As String

        Dim itemfluxoaprovacao As New VO.RelacaoAcaoComercialxFluxoAprovacao

        If lisrealfluxoaprov.Count > 0 Then

            sMsgAux = New StringBuilder
            '
            If iIndAux = 1 Then
                sMsgAux.Append("                                                                                                                        ")
            End If
            '
            'Estrutura comentada
            'sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            'sMsgAux.Append("| FLUXO                                                                                                                |")
            'sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            'Fim de estrutura comentada
            '
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            sMsgAux.Append("| FLUXO                                                                                                                |")
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            '
            For Each item In lisrealfluxoaprov
                If CInt(item.NUMSEQNIVAPV) < 400 Then
                    iAux += 1
                    sMsg = ""
                    iTipStaAco = item.TIPSTAFLUAPV
                    '
                    If CStr(item.NUMSEQNIVAPV).Trim() = "0" Then
                        Select Case iTipStaAco
                            Case TipStaFluApv.Criacao
                                sMsg = iAux.ToString() & " - " & "ENVIADO PARA APROVAÇÃO POR " & item.NOMFNC.Trim().ToUpper() & " Dia: " & Format(item.DATHRAFLUAPV, "dd/MM/yyyy HH:mm:ss")
                        End Select
                    Else
                        Select Case iTipStaAco
                            Case TipStaFluApv.Aprovado
                                '
                                If Not IsDBNull(item.NOMFNCARZ) Then
                                    sMsg = iAux.ToString() & " - " & "APROVADO POR " &
                                    item.NOMFNCARZ.Trim().ToUpper() & " NO LUGAR DE " & item.NOMFNC.Trim().ToUpper() & " " & " DIA: " & Format(item.DATHRAAPVFLU, "dd/MM/yyyy HH:mm:ss")
                                Else
                                    sMsg = iAux.ToString() & " - " & "APROVADO POR " & item.NOMFNC.Trim().ToUpper() & " DIA: " & Format(item.DATHRAAPVFLU, "dd/MM/yyyy HH:mm:ss")
                                End If
                                '
                            Case TipStaFluApv.Resposta_Parecer
                                '
                                If Not IsDBNull(item.NOMFNCARZ) Then
                                    sMsg = iAux.ToString() & " - " & "PARECER DADO POR " & item.NOMFNCARZ.Trim().ToUpper() & " NO LUGAR DE " &
                                    item.NOMFNC.Trim().ToUpper() & " PEDIDO POR " & lisrealfluxoaprov.Where(Function(x) x.NUMSEQFLUAPV = x.NUMSEQFLUAPVPEDOPN).ToList().FirstOrDefault().NOMFNC.Trim().ToUpper() &
                                    " DIA: " &
                                    Format(item.DATHRAAPVFLU, "dd/MM/yyyy HH:mm:ss")
                                Else
                                    sMsg = iAux.ToString() & " - " & "PARECER DADO POR " &
                                    item.NOMFNC.Trim().ToUpper() & " PEDIDO POR " & lisrealfluxoaprov.Where(Function(x) x.NUMSEQFLUAPV = x.NUMSEQFLUAPVPEDOPN).ToList().FirstOrDefault().NOMFNC.Trim().ToUpper() &
                                    " DIA: " & Format(item.DATHRAAPVFLU, "dd/MM/yyyy HH:mm:ss")
                                End If
                                '
                        End Select
                    End If
                    '
                    If CStr(item.NUMSEQNIVAPV).StartsWith("10") Then
                        sMsg &= " (GERENTE DE MARKETING)"
                    ElseIf CStr(item.NUMSEQNIVAPV).StartsWith("20") Then
                        sMsg &= " (DIRETOR DE CATEGORIA)"
                    ElseIf CStr(item.NUMSEQNIVAPV).StartsWith("30") Then
                        sMsg &= " (DIRETOR DE COMPRAS)"
                    End If
                    '
                    iAuxNro = (sMsg.Length / 118)
                    iAuxIni = 0
                    '
                    While iAuxNro > 0
                        '
                        If sMsg.Length <= 118 Then
                            sMecAux = sMsg
                            sMsgAux.Append("|" & sMecAux)
                            If sMecAux.Length < 118 Then
                                For i As Integer = 0 To (118 - sMecAux.Length) - 1
                                    sMsgAux.Append(" ")
                                Next
                            End If
                            sMsgAux.Append("|")
                        ElseIf sMsg.Length > ((iAuxIni * 118) + 118) Then
                            sMsgAux.Append("|" & sMsg.Substring((iAuxIni * 118), 118))
                            sMsgAux.Append("|")
                        Else
                            sMecAux = sMsg.Substring((iAuxIni * 118), sMsg.Length - (iAuxIni * 118))
                            sMsgAux.Append("|" & sMecAux)
                            If sMecAux.Length < 118 Then
                                For i As Integer = 0 To (118 - sMecAux.Length) - 1
                                    sMsgAux.Append(" ")
                                Next
                            End If
                            sMsgAux.Append("|")
                        End If
                        '
                        iAuxIni += 1
                        iAuxNro -= 1
                        '
                    End While
                    '
                    'If sMsg.Length > 119 Then
                    '    sMsgAux.Append(sMsg.Substring(0, 119))
                    'Else
                    '    sMsgAux.Append(sMsg)
                    '    If sMsg.Length < 119 Then
                    '        For i As Integer = 0 To (119 - sMsg.Length) - 1
                    '            sMsgAux.Append(" ")
                    '        Next
                    '    End If
                    'End If
                    'sMsgAux.Append("|")
                End If
            Next
            '
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            '
        End If

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If

    End Function



    Private Function MntMsgIct(ByVal lisIcentivos As List(Of VO.RelacaoIncentivoxAcaoComercial), ByVal iIndAux As Integer) As String
        Dim sMsgAux As StringBuilder
        Dim sIct, sVal, sRsp, sMec, sMecAux As String
        Dim iAuxIni As Integer
        Dim iAux As Single


        If lisIcentivos.Count > 0 Then
            sMsgAux = New StringBuilder
            '
            If iIndAux = 1 Then
                sMsgAux.Append("                                                                                                                        ")
            End If
            '
            'Estrutura comentada
            'sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            'sMsgAux.Append("| DETALHAMENTO INCENTIVO                                                                                               |")
            'sMsgAux.Append("+--------------------------------------+-------------------------------+---------------------+-------------------------+")
            'sMsgAux.Append("| INCENTIVO                            | VALIDADE                      | RESPONSÁVEL         | MECANICA INCENTIVO      |")
            'sMsgAux.Append("+--------------------------------------+-------------------------------+---------------------+-------------------------+")
            'Fim de estrutura comentada
            '
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            sMsgAux.Append("| DETALHAMENTO INCENTIVO                                                                                               |")
            sMsgAux.Append("+--------------------------------------+-----------------------+-----------------------------+-------------------------+")
            sMsgAux.Append("| INCENTIVO                            | VALIDADE              | RESPONSÁVEL                 | MECANICA INCENTIVO      |")
            sMsgAux.Append("+--------------------------------------+-----------------------+-----------------------------+-------------------------+")
            '
            For Each item In lisIcentivos
                '
                sIct = CStr(item.CODICT).Trim() & " - " & item.DESICT.Trim()
                sVal = Format(item.DATFIMEFTICT.ToString(), "dd/MM/yyyy") & " - " & Format(item.DATFIMEFTICT.ToString(), "dd/MM/yyyy")
                sRsp = CStr(item.CODUSRCADICT).Trim() & " - " & item.NOMFNC.Trim()
                sMec = item.DESRDCRGRICT.Trim()
                '
                If sIct.Length > 38 Then
                    sMsgAux.Append("|" & sIct.Substring(0, 38))
                Else
                    sMsgAux.Append("|" & sIct)
                    If sIct.Length < 38 Then
                        For i As Integer = 0 To (38 - sIct.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                If sVal.Length > 23 Then
                    sMsgAux.Append("|" & sVal.Substring(0, 23))
                Else
                    sMsgAux.Append("|" & sVal)
                    If sVal.Length < 23 Then
                        For i As Integer = 0 To (23 - sVal.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                If sRsp.Length > 29 Then
                    sMsgAux.Append("|" & sRsp.Substring(0, 29))
                Else
                    sMsgAux.Append("|" & sRsp)
                    If sRsp.Length < 29 Then
                        For i As Integer = 0 To (29 - sRsp.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                iAux = (sMec.Length / 25)
                iAuxIni = 0
                '
                While iAux > 0
                    '
                    If sMec.Length <= 25 Then
                        sMecAux = sMec
                        sMsgAux.Append("|" & sMec)
                        If sMecAux.Length < 25 Then
                            For i As Integer = 0 To (25 - sMecAux.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                        sMsgAux.Append("|")
                    ElseIf sMec.Length > ((iAuxIni * 25) + 25) Then
                        sMsgAux.Append("|" & sMec.Substring((iAuxIni * 25), 25))
                        sMsgAux.Append("|")
                    Else
                        sMecAux = sMec.Substring((iAuxIni * 25), sMec.Length - (iAuxIni * 25))
                        sMsgAux.Append("|" & sMecAux)
                        If sMecAux.Length < 25 Then
                            For i As Integer = 0 To (25 - sMecAux.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                        sMsgAux.Append("|")
                    End If
                    '
                    If iAux > 1 Then
                        sMsgAux.Append("|                                      |                       |                             ")
                    End If
                    '
                    iAuxIni += 1
                    iAux -= 1
                    '
                End While
                '
            Next
            '
            sMsgAux.Append("+--------------------------------------+-------------------------------+---------------------+-------------------------+")

        End If

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If

    End Function

    Private Function MntMsgOcd(ByVal listaItem As List(Of VO.RelacaoAcaoComercialPorItemOrcamento), ByVal iIndAux As Integer) As String
        Dim sMsgAux As StringBuilder
        Dim sItem, sNumLin As String
        Dim sTipOpe, sVlrUni, sQde, sVlrTot As String
        Dim sTot As String
        Dim dTotVlrOrc, dTotVlrBde, dVlrTotVlrCli As Double


        If listaItem.Count > 0 Then

            sMsgAux = New StringBuilder
            '
            If iIndAux = 1 Then
                sMsgAux.Append("                                                                                                                        ")
            End If
            '
            'Estrutura comentada
            'sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            'sMsgAux.Append("| ORÇAMENTO                                                                                                            |")
            'sMsgAux.Append("+--+----------------------------------------------+-----------------------------+--------------+--------+--------------+")
            'sMsgAux.Append("|  | ITEM DE ORÇAMENTO                            | TIPO OPERAÇÃO               | VALOR UNIT.  | QUANT. | VALOR TOTAL  |")
            'sMsgAux.Append("+--+----------------------------------------------+-----------------------------+--------------+--------+--------------+")
            'Fim de estrutura comentada
            '
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            sMsgAux.Append("| ORÇAMENTO                                                                                                            |")
            sMsgAux.Append("+--+----------------------------------------------+-----------------------------+--------------+--------+--------------+")
            sMsgAux.Append("|  | ITEM DE ORÇAMENTO                            | TIPO OPERAÇÃO               | VALOR UNIT.  | QUANT. | VALOR TOTAL  |")
            sMsgAux.Append("+--+----------------------------------------------+-----------------------------+--------------+--------+--------------+")
            '
            For Each item In listaItem
                '
                sNumLin = CStr(item.NUMLNHITEOCDACOCMC).Trim()
                sItem = CStr(item.CODITEOCDACOCMC).Trim()
                sTipOpe = CStr(item.CODOPEFSCDSNACOCMC).Trim() & " - " & item.DESOPEFSCDSNACOCMC.Trim()
                '
                sVlrUni = CDbl(item.VLRUNTITEOCDACOCMC).ToString().Trim()
                sQde = CDbl(item.QDEITEOCDACOCMC).ToString("N").Trim()
                sVlrTot = CDbl(CDbl(item.QDEITEOCDACOCMC) * CDbl(item.VLRUNTITEOCDACOCMC)).ToString().Trim()
                '
                'Rodrigo 11/09/2007
                If item.INDBDEOPEDSNACOCMC = 1 Then
                    dTotVlrBde += CDbl(item.QDEITEOCDACOCMC) * CDbl(item.VLRPTCFRNITEACOCMC)
                Else
                    dTotVlrOrc += CDbl(item.QDEITEOCDACOCMC) * CDbl(item.VLRPTCFRNITEACOCMC)
                End If
                dVlrTotVlrCli += CDbl(item.QDEITEOCDACOCMC) * CDbl(item.VLRPTCCLIITEACOCMC)
                'Fim Rodrigo 11/09/2007
                '
                If sNumLin.Length > 2 Then
                    sMsgAux.Append("|" & sNumLin.Substring(0, 2))
                Else
                    sMsgAux.Append("|")
                    If sNumLin.Length < 2 Then
                        For i As Integer = 0 To (2 - sNumLin.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sNumLin)
                End If
                '
                If sItem.Length > 46 Then
                    sMsgAux.Append("|" & sItem.Substring(0, 46))
                Else
                    sMsgAux.Append("|" & sItem)
                    If sItem.Length < 46 Then
                        For i As Integer = 0 To (46 - sItem.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                If sTipOpe.Length > 29 Then
                    sMsgAux.Append("|" & sTipOpe.Substring(0, 29))
                Else
                    sMsgAux.Append("|" & sTipOpe)
                    If sTipOpe.Length < 29 Then
                        For i As Integer = 0 To (29 - sTipOpe.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                If sVlrUni.Length > 14 Then
                    sMsgAux.Append("|" & sVlrUni.Substring(0, 14))
                Else
                    sMsgAux.Append("|")
                    If sVlrUni.Length < 14 Then
                        For i As Integer = 0 To (14 - sVlrUni.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sVlrUni)
                End If
                '
                If sQde.Length > 8 Then
                    sMsgAux.Append("|" & sQde.Substring(0, 8))
                Else
                    sMsgAux.Append("|")
                    If sQde.Length < 8 Then
                        For i As Integer = 0 To (8 - sQde.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sQde)
                End If
                '
                If sVlrTot.Length > 14 Then
                    sMsgAux.Append("|" & sVlrTot.Substring(0, 14))
                Else
                    sMsgAux.Append("|")
                    If sVlrTot.Length < 14 Then
                        For i As Integer = 0 To (14 - sVlrTot.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sVlrTot)
                End If
                '
                sMsgAux.Append("|")
                '
            Next
            '
            sMsgAux.Append("+--+----------------------------------------------+-----------------------------+--------------+--------+--------------+")

            sVlrTot = "| TOTAL VLR CLIENTE: " & dVlrTotVlrCli.ToString().Trim() &
               "    TOTAL VLR ORÇAMENTO: " & dTotVlrOrc.ToString().Trim() &
               "    TOTAL VLR BRINDE: " & dTotVlrBde.ToString().Trim()
            '
            sMsgAux.Append("                                                                                                                        ")
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            '
            If sVlrTot.Length > 119 Then
                sMsgAux.Append(sVlrTot.Substring(0, 119))
            Else
                If sVlrTot.Length < 119 Then
                    For i As Integer = 0 To (119 - sVlrTot.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sVlrTot)
            End If
            sMsgAux.Append("|")
            '
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
        End If
        '
        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If

    End Function

    Private Function MntMsgSldPlnMrk(ByVal listaacaoplano As List(Of VO.RelacaoAcaoComercialxPlanoMarketing), ByVal iIndAux As Integer) As String
        Dim sMsgAux As StringBuilder
        Dim sBU, sSldMns, sSldAtu, sSldAnu As String
        Dim sVlrAco, sRstMns, sRstAtu, sRstAnu As String


        If listaacaoplano.Count > 0 Then

            sMsgAux = New StringBuilder
            '
            If iIndAux = 1 Then
                sMsgAux.Append("                                                                                                                        ")
            End If
            '
            'Estrutura comentada
            'sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            'sMsgAux.Append("| SALDO PLANO DE MARKETING                                                                                             |")
            'sMsgAux.Append("+-------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+")
            'sMsgAux.Append("| BU          | SALDO        | SALDO        | SALDO        | VALOR        | RESULTADO    | RESULTADO    | RESULTADO    |")
            'sMsgAux.Append("|             | MENSAL       | ATUAL        | ANUAL        | AÇÃO         | MENSAL       | ATUAL        | ANUAL        |")
            'sMsgAux.Append("+-------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+")
            'Fim de estrutura comentada
            '
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            sMsgAux.Append("| SALDO PLANO DE MARKETING                                                                                             |")
            sMsgAux.Append("+-------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+")
            sMsgAux.Append("| BU          | SALDO        | SALDO        | SALDO        | VALOR        | RESULTADO    | RESULTADO    | RESULTADO    |")
            sMsgAux.Append("|             | MENSAL       | ATUAL        | ANUAL        | AÇÃO         | MENSAL       | ATUAL        | ANUAL        |")
            sMsgAux.Append("+-------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+")
            '
            For Each item In listaacaoplano
                '
                sBU = CStr(item.CODUNDESRNGC).Trim() & " - " & item.DESUNDESRNGC.Trim()
                sSldMns = CDbl(item.VLRSLDMNSPLNMKT).ToString().Trim()
                sSldAtu = CDbl(item.VLRSLDATUPLNMKT).ToString().Trim()
                sSldAnu = CDbl(item.VLRSLDANOPLNMKT).ToString().Trim()
                sVlrAco = CDbl(item.VLRUTZPLNMKT).ToString().Trim()
                sRstMns = CDbl(CDbl(item.VLRSLDMNSPLNMKT) - CDbl(item.VLRUTZPLNMKT)).ToString().Trim()
                sRstAtu = CDbl(CDbl(item.VLRSLDATUPLNMKT) - CDbl(item.VLRUTZPLNMKT)).ToString().Trim()
                sRstAnu = CDbl(CDbl(item.VLRSLDANOPLNMKT) - CDbl(item.VLRUTZPLNMKT)).ToString().Trim()
                '
                If sBU.Length > 13 Then
                    sMsgAux.Append("|" & sBU.Substring(0, 13))
                Else
                    sMsgAux.Append("|" & sBU)
                    If sBU.Length < 13 Then
                        For i As Integer = 0 To (13 - sBU.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                If sSldMns.Length > 14 Then
                    sMsgAux.Append("|" & sSldMns.Substring(0, 14))
                Else
                    sMsgAux.Append("|")
                    If sSldMns.Length < 14 Then
                        For i As Integer = 0 To (14 - sSldMns.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sSldMns)
                End If
                '
                If sSldAtu.Length > 14 Then
                    sMsgAux.Append("|" & sSldAtu.Substring(0, 14))
                Else
                    sMsgAux.Append("|")
                    If sSldAtu.Length < 14 Then
                        For i As Integer = 0 To (14 - sSldAtu.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sSldAtu)
                End If
                '
                If sSldAnu.Length > 14 Then
                    sMsgAux.Append("|" & sSldAnu.Substring(0, 14))
                Else
                    sMsgAux.Append("|")
                    If sSldAnu.Length < 14 Then
                        For i As Integer = 0 To (14 - sSldAnu.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sSldAnu)
                End If
                '
                'Rodrigo 11/09/2007
                If sVlrAco.Length > 14 Then
                    sMsgAux.Append("|" & sVlrAco.Substring(0, 14))
                Else
                    sMsgAux.Append("|")
                    If sVlrAco.Length < 14 Then
                        For i As Integer = 0 To (14 - sVlrAco.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sVlrAco)
                End If
                'Fim Rodrigo 11/09/2007
                '
                If sRstMns.Length > 14 Then
                    sMsgAux.Append("|" & sRstMns.Substring(0, 14))
                Else
                    sMsgAux.Append("|")
                    If sRstMns.Length < 14 Then
                        For i As Integer = 0 To (14 - sRstMns.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sRstMns)
                End If
                '
                If sRstAtu.Length > 14 Then
                    sMsgAux.Append("|" & sRstAtu.Substring(0, 14))
                Else
                    sMsgAux.Append("|")
                    If sRstAtu.Length < 14 Then
                        For i As Integer = 0 To (14 - sRstAtu.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sRstAtu)
                End If
                '
                If sRstAnu.Length > 14 Then
                    sMsgAux.Append("|" & sRstAnu.Substring(0, 14))
                Else
                    sMsgAux.Append("|")
                    If sRstAnu.Length < 14 Then
                        For i As Integer = 0 To (14 - sRstAnu.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sRstAnu)
                End If
                sMsgAux.Append("|")
                '
            Next
            '
            sMsgAux.Append("+-------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+")

        End If

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If

    End Function


    Private Function MntMsgCab(ByVal dtCab As VO.AcaoComercial, ByVal iIndAux As Integer) As String

        Dim sMsgAux As StringBuilder
        '
        Dim sNome As String
        Dim sValidade As String
        Dim sComplementar As String
        Dim sAcaoPrincipal As String
        Dim sVlrTotAcao As String
        Dim sVlrTotOrcamento As String
        Dim sVlrTotTxaAdm As String
        Dim sVlrTotTxaBde As String
        Dim sTipEvento As String = ""
        Dim sEvento As String = ""
        Dim sEmpExaAco As String = ""

        sNome = dtCab.NOMACOCMC.Trim()
        sValidade = Format(dtCab.DATINIACOCMC, "dd/MM/yyyy") & " - " & Format(dtCab.DATFIMACOCMC, "dd/MM/yyyy")

        sComplementar = IIf(CStr(dtCab.INDACOCMCCPL).Trim() = "1", "SIM", "NÃO")
        sAcaoPrincipal = CStr(dtCab.CODACOCMCORI).Trim()
        sVlrTotAcao = CDbl(dtCab.VLRTOTACOCMC).ToString().Trim()
        sVlrTotOrcamento = CDbl(dtCab.VLRTOTOCDACOCMC).ToString().Trim()
        sVlrTotTxaAdm = CDbl(dtCab.VLRTOTTXAADMACOCMC).ToString().Trim()
        sVlrTotTxaBde = CDbl(dtCab.VLRTOTBDEACOCMC).ToString().Trim()
        '
        If Not IsDBNull(dtCab.NOMTIPEVTACOCMC) Then
            sTipEvento = CStr(dtCab.TIPEVTACOCMC).Trim() & " - " & dtCab.NOMTIPEVTACOCMC.Trim()
        End If

        If Not IsDBNull(dtCab.NOMEVTACOCMC) Then
            sEvento = CStr(dtCab.CODEVTACOCMC).Trim() & " - " & dtCab.NOMEVTACOCMC.Trim()
        End If
        '
        If Not IsDBNull(dtCab.DESDSNDSCBNF) Then
            sEmpExaAco = CStr(dtCab.TIPDSNDSCBNF).Trim() & " - " & CStr(dtCab.DESDSNDSCBNF).Trim()
        End If

        sMsgAux = New StringBuilder

        If iIndAux = 1 Then
            sMsgAux.Append("                                                                                                                        ")
        End If


        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
        sMsgAux.Append("| AÇÕES MERCADOLÓGICAS                                                                                                 |")
        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
        sMsgAux.Append("| AÇÃO                                                                                                                 |")
        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
        '
        'Primeira linha
        If sNome.Length > 46 Then
            sMsgAux.Append("| NOME: " & sNome.Substring(0, 46))
        Else
            sMsgAux.Append("| NOME: " & sNome)
            If sNome.Length < 46 Then
                For i As Integer = 0 To (46 - sNome.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
        End If
        '
        If sValidade.Length > 53 Then
            sMsgAux.Append("| VALIDADE: " & sValidade.Substring(0, 53))
        Else
            sMsgAux.Append("| VALIDADE: " & sValidade)
            If sValidade.Length < 53 Then
                For i As Integer = 0 To (53 - sValidade.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
        End If
        sMsgAux.Append("|")
        '
        'Segunda linha
        If sComplementar.Length > 38 Then
            sMsgAux.Append("| COMPLEMENTAR: " & sComplementar.Substring(0, 38))
        Else
            sMsgAux.Append("| COMPLEMENTAR: " & sComplementar)
            If sComplementar.Length < 38 Then
                For i As Integer = 0 To (38 - sComplementar.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
        End If
        '
        If sAcaoPrincipal.Length > 47 Then
            sMsgAux.Append("| AÇÃO PRINCIPAL: " & sAcaoPrincipal.Substring(0, 47))
        Else
            sMsgAux.Append("| AÇÃO PRINCIPAL: " & sAcaoPrincipal)
            If sAcaoPrincipal.Length < 47 Then
                For i As Integer = 0 To (47 - sAcaoPrincipal.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
        End If
        sMsgAux.Append("|")
        '
        'terceira linha
        If sVlrTotAcao.Length > 31 Then
            sMsgAux.Append("| VALOR TOTAL DA AÇÃO: " & sVlrTotAcao.Substring(0, 31))
        Else
            sMsgAux.Append("| VALOR TOTAL DA AÇÃO: " & sVlrTotAcao)
            If sVlrTotAcao.Length < 31 Then
                For i As Integer = 0 To (31 - sVlrTotAcao.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
        End If
        '
        If sVlrTotOrcamento.Length > 40 Then
            sMsgAux.Append("| VALOR TOTAL ORÇAMENTO: " & sVlrTotOrcamento.Substring(0, 40))
        Else
            sMsgAux.Append("| VALOR TOTAL ORÇAMENTO: " & sVlrTotOrcamento)
            If sVlrTotOrcamento.Length < 40 Then
                For i As Integer = 0 To (40 - sVlrTotOrcamento.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
        End If
        sMsgAux.Append("|")
        '
        'quarta linha
        If sVlrTotTxaAdm.Length > 31 Then
            sMsgAux.Append("| VALOR TOTAL TAX ADM: " & sVlrTotTxaAdm.Substring(0, 31))
        Else
            sMsgAux.Append("| VALOR TOTAL TAX ADM: " & sVlrTotTxaAdm)
            If sVlrTotTxaAdm.Length < 31 Then
                For i As Integer = 0 To (31 - sVlrTotTxaAdm.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
        End If
        '
        If sVlrTotTxaBde.Length > 38 Then
            sMsgAux.Append("| VALOR TOTAL TAXA BRINDE: " & sVlrTotTxaBde.Substring(0, 38))
        Else
            sMsgAux.Append("| VALOR TOTAL TAXA BRINDE: " & sVlrTotTxaBde)
            If sVlrTotTxaBde.Length < 38 Then
                For i As Integer = 0 To (38 - sVlrTotTxaBde.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
        End If
        sMsgAux.Append("|")
        '
        'quinta linha
        If sTipEvento.Length > 36 Then
            sMsgAux.Append("| TIPO DE EVENTO: " & sTipEvento.Substring(0, 36))
        Else
            sMsgAux.Append("| TIPO DE EVENTO: " & sTipEvento)
            If sTipEvento.Length < 36 Then
                For i As Integer = 0 To (36 - sTipEvento.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
        End If
        '
        If sEvento.Length > 55 Then
            sMsgAux.Append("| EVENTO: " & sEvento.Substring(0, 55))
        Else
            sMsgAux.Append("| EVENTO: " & sEvento)
            If sEvento.Length < 55 Then
                For i As Integer = 0 To (55 - sEvento.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
        End If
        sMsgAux.Append("|")
        '
        'sexta e ultima linha
        If sEmpExaAco.Length > 100 Then
            sMsgAux.Append("| EMPENHO EXA CTT: " & sEmpExaAco.Substring(0, 100))
        Else
            sMsgAux.Append("| EMPENHO EXA CTT: " & sEmpExaAco)
            If sEmpExaAco.Length < 100 Then
                For i As Integer = 0 To (100 - sEmpExaAco.Length) - 1
                    sMsgAux.Append(" ")
                Next
            End If
        End If
        sMsgAux.Append("|")
        '
        sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If


    End Function

    Public Function MntMsgRgrAcoCmc(ByVal listamecanica As VO.RelacaoAcaoComercialxMecanicaDaAcao, ByVal iIndAux As Integer)
        Dim sMsgAux As StringBuilder
        Dim sRgr As String
        '
        Dim iAux As Integer
        Dim iAuxIni As Integer
        Dim sMecAux As String


        If Not listamecanica Is Nothing Then

            sMsgAux = New StringBuilder
            '
            If iIndAux = 1 Then
                sMsgAux.Append("                                                                                                                        ")
            End If
            '
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            sMsgAux.Append("| MECÂNICA DA AÇÂO                                                                                                     |")
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")

            sRgr = listamecanica.DESRGRACOCMC.Trim()
            '
            If sRgr.Length > 118 Then
                '
                iAux = (sRgr.Length / 118)
                iAuxIni = 0
                '
                While iAux > 0
                    '
                    If sRgr.Length <= 118 Then
                        sMecAux = sRgr
                        sMsgAux.Append("|" & sRgr)
                        If sMecAux.Length < 118 Then
                            For i As Integer = 0 To (118 - sMecAux.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                        sMsgAux.Append("|")
                    ElseIf sRgr.Length > ((iAuxIni * 118) + 118) Then
                        sMsgAux.Append("|" & sRgr.Substring((iAuxIni * 118), 118))
                        sMsgAux.Append("|")
                    Else
                        sMecAux = sRgr.Substring((iAuxIni * 118), sRgr.Length - (iAuxIni * 118))
                        sMsgAux.Append("|" & sMecAux)
                        If sMecAux.Length < 118 Then
                            For i As Integer = 0 To (118 - sMecAux.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                        sMsgAux.Append("|")
                    End If
                    '
                    iAuxIni += 1
                    iAux -= 1
                    '
                End While
            Else
                sMsgAux.Append("|" & sRgr)
                If sRgr.Length < 118 Then
                    For i As Integer = 0 To (118 - sRgr.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
            End If
            '
            sMsgAux.Append("|")
            '
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")

        End If

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If

    End Function

    Public Function MntMsgPolAcoCmc(ByVal listPoliticas As List(Of VO.PoliticasDaAcaoComercial), ByVal iIndAux As Integer)
        Dim sMsgAux As StringBuilder
        Dim sPol As String

        If listPoliticas.Count > 0 Then

            sMsgAux = New StringBuilder
            '
            If iIndAux = 1 Then
                sMsgAux.Append("                                                                                                                        ")
            End If
            '
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            sMsgAux.Append("| POLITICAS DIVERGENTES                                                                                                |")
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            '
            For Each item In listPoliticas
                sPol = item.DESPOLACOCMC.Trim()
                If sPol.Length > 118 Then
                    sMsgAux.Append("|" & sPol.Substring(0, 118))
                Else
                    sMsgAux.Append("|" & sPol)
                    If sPol.Length < 118 Then
                        For i As Integer = 0 To (118 - sPol.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                sMsgAux.Append("|")                '
            Next

            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")

        End If

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If

    End Function
    Public Function MntMsgPti(ByVal listaforn As List(Of VO.RelacaoAcaoComercialxFornecedores), ByVal iIndAux As Integer)
        Dim sMsgAux As StringBuilder
        '
        Dim sBU As String = ""
        Dim sFrn As String = ""
        Dim sEmpCtt As String = ""
        '
        Dim sSldCtt As String = ""
        Dim sCtt As String = ""
        Dim sSldEmp As String = ""
        Dim sExt As String = ""
        Dim sPtc As String = ""
        Dim sTxaAdm As String = ""
        Dim sTxaBde As String = ""
        '
        Dim dTotSldCtt As Double
        Dim dTotCtt As Double
        Dim dTotSldEmp As Double
        Dim dTotExt As Double
        Dim dTotPtc As Double
        Dim dTotTxaAdm As Double
        Dim dTotTxaBde As Double
        '
        Dim sBfc As String = ""

        If listaforn.Count > 0 Then

            sMsgAux = New StringBuilder
            '
            If iIndAux = 1 Then
                sMsgAux.Append("                                                                                                                        ")
            End If
            '
            sMsgAux.Append("+----------------------------------------------------------------------------------------------------------------------+")
            sMsgAux.Append("| PATROCINADORES                                                                                                       |")
            sMsgAux.Append("+-------+---------------------+--------+------------+----------+----------+----------+-----------+----------+----------+")
            sMsgAux.Append("| BU    | FORNECEDOR          | EMP.   | SLD EMP.   | VR.      | SLD.     | VLR      | VLR       | TXA      | TXA      |")
            sMsgAux.Append("|       |                     | ACORDO | ACORDO     | ACORDO   | EMPENHO  | EXA ACC  | PART      | ADM      | BDE      |")
            sMsgAux.Append("+-------+---------------------+--------+------------+----------+----------+----------+-----------+----------+----------+")
            '
            For Each item In listaforn
                'Preenchimento do BU
                If item.CODUNDESRNGC <> 0 Then
                    sBU = item.CODUNDESRNGC.ToString().Trim() & "-" & item.DESUNDESRNGC.Trim()
                    '
                    If sBU.Length > 7 Then
                        sMsgAux.Append("|" & sBU.Substring(0, 7))
                    Else
                        sMsgAux.Append("|" & sBU)
                        If sBU.Length < 7 Then
                            For i As Integer = 0 To (7 - sBU.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                    End If
                End If

                '
                'Preenchimento do fornecedor
                sFrn = item.CODFRN.ToString().Trim() & "-" & item.NOMFRN.Trim()
                '
                If sFrn.Length > 21 Then
                    sMsgAux.Append("|" & sFrn.Substring(0, 21))
                Else
                    sMsgAux.Append("|" & sFrn)
                    If sFrn.Length < 21 Then
                        For i As Integer = 0 To (21 - sFrn.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                'Empenho do empenho ctt
                sEmpCtt = ""
                If Not IsDBNull(item.TIPDSNDSCBNF) Then
                    If item.TIPDSNDSCBNF <> 0 Then
                        sEmpCtt = item.TIPDSNDSCBNF.ToString().Trim() & " - " & item.DESDSNDSCBNF.Trim()
                        '
                        'Preenchimento Empenho Acordo
                        If sEmpCtt.Length > 8 Then
                            sMsgAux.Append("|" & sEmpCtt.Substring(0, 8))
                        Else
                            sMsgAux.Append("|" & sEmpCtt)
                        End If
                    Else
                        sMsgAux.Append("|")
                    End If
                Else
                    sMsgAux.Append("|")
                End If
                '
                If sEmpCtt.Length < 8 Then
                    For i As Integer = 0 To (8 - sEmpCtt.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                '
                'Saldo do empenho acordo
                sSldCtt = ""
                If Not IsDBNull(item.VLRSLDDSNCTTACOCMC) Then
                    sSldCtt = CDbl(item.VLRSLDDSNCTTACOCMC).ToString().Trim()
                    dTotSldCtt += CDbl(item.VLRSLDDSNCTTACOCMC)
                    '
                    If sSldCtt.Length > 12 Then
                        sMsgAux.Append("|" & sSldCtt.Substring(0, 12))
                    Else
                        sMsgAux.Append("|")
                        If sSldCtt.Length < 12 Then
                            For i As Integer = 0 To (12 - sSldCtt.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                        sMsgAux.Append(sSldCtt)
                    End If
                Else
                    sMsgAux.Append("|")
                    If sSldCtt.Length < 12 Then
                        For i As Integer = 0 To (12 - sSldCtt.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                'Preenchimento Valor Acordo
                sCtt = ""
                If Not IsDBNull(item.VLRUTZCTTACOCMC) Then
                    If item.VLRUTZCTTACOCMC <> 0 Then
                        sCtt = CDbl(item.VLRUTZCTTACOCMC).ToString().Trim()
                        dTotCtt += CDbl(item.VLRUTZCTTACOCMC)
                        '
                        If sCtt.Length > 10 Then
                            sMsgAux.Append("|" & sCtt.Substring(0, 10))
                        Else
                            sMsgAux.Append("|")
                            If sCtt.Length < 10 Then
                                For i As Integer = 0 To (10 - sCtt.Length) - 1
                                    sMsgAux.Append(" ")
                                Next
                            End If
                            sMsgAux.Append(sCtt)
                        End If
                    Else
                        sMsgAux.Append("|")
                        If sCtt.Length < 10 Then
                            For i As Integer = 0 To (10 - sCtt.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                    End If
                Else
                    sMsgAux.Append("|")
                    If sCtt.Length < 10 Then
                        For i As Integer = 0 To (10 - sCtt.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                'Preenchimento Saldo Empenho
                sSldEmp = ""
                sSldEmp = CDbl(item.VLRSLDDSNCTTACOCMC).ToString().Trim()
                dTotSldEmp += CDbl(item.VLRSLDDSNCTTACOCMC)
                '
                If sSldEmp.Length > 10 Then
                    sMsgAux.Append("|" & sSldEmp.Substring(0, 10))
                Else
                    sMsgAux.Append("|")
                    If sSldEmp.Length < 10 Then
                        For i As Integer = 0 To (10 - sSldEmp.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sSldEmp)
                End If
                '
                'Valor extra contrato
                sExt = ""
                If Not IsDBNull(item.VLRUTZEXACTTACOCMC) Then
                    If item.VLRUTZEXACTTACOCMC <> 0 Then
                        sExt = CDbl(item.VLRUTZEXACTTACOCMC).ToString().Trim()
                        dTotExt += CDbl(item.VLRUTZEXACTTACOCMC)
                        'Preenchimento Valor Extra Acordo
                        If sExt.Length > 10 Then
                            sMsgAux.Append("|" & sExt.Substring(0, 10))
                        Else
                            sMsgAux.Append("|")
                            If sExt.Length < 10 Then
                                For i As Integer = 0 To (10 - sExt.Length) - 1
                                    sMsgAux.Append(" ")
                                Next
                            End If
                            sMsgAux.Append(sExt)
                        End If
                    Else
                        sMsgAux.Append("|")
                        If sExt.Length < 10 Then
                            For i As Integer = 0 To (10 - sExt.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                    End If
                Else
                    sMsgAux.Append("|")
                    If sExt.Length < 13 Then
                        For i As Integer = 0 To (10 - sExt.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                'Valor Participacao
                sPtc = CDbl(item.VLRPTCFRNACOCMC).ToString().Trim()
                dTotPtc += CDbl(item.VLRPTCFRNACOCMC)
                If sPtc.Length > 11 Then
                    sMsgAux.Append("|" & sPtc.Substring(0, 11))
                Else
                    sMsgAux.Append("|")
                    If sPtc.Length < 11 Then
                        For i As Integer = 0 To (11 - sPtc.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sPtc)
                End If
                '
                'Valor da taxa de administração do fornecedor
                sTxaAdm = CDbl(item.VLRTXAADMFRNACOCMC).ToString().Trim()
                dTotTxaAdm += CDbl(item.VLRTXAADMFRNACOCMC)
                If sTxaAdm.Length > 10 Then
                    sMsgAux.Append("|" & sTxaAdm.Substring(0, 10))
                Else
                    sMsgAux.Append("|")
                    If sTxaAdm.Length < 10 Then
                        For i As Integer = 0 To (10 - sTxaAdm.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sTxaAdm)
                End If
                '
                'Valor da taxa de brinde 
                sTxaBde = CDbl(item.VLRTXAADMBDEACOCMC).ToString().Trim()
                dTotTxaBde += CDbl(item.VLRTXAADMBDEACOCMC)
                If sTxaBde.Length > 10 Then
                    sMsgAux.Append("|" & sTxaBde.Substring(0, 10))
                Else
                    sMsgAux.Append("|")
                    If sTxaBde.Length < 10 Then
                        For i As Integer = 0 To (10 - sTxaBde.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                    sMsgAux.Append(sTxaBde)
                End If
                sMsgAux.Append("|")
                '
            Next
            '
            sMsgAux.Append("+-------+---------------------+--------+------------+----------+----------+----------+-----------+----------+----------+")
            sMsgAux.Append("+       |                     |        ")
            '
            'Saldo do empenho acordo
            sSldCtt = dTotSldCtt.ToString().Trim()
            If sSldCtt.Length > 12 Then
                sMsgAux.Append("|" & sSldCtt.Substring(0, 12))
            Else
                sMsgAux.Append("|")
                If sSldCtt.Length < 12 Then
                    For i As Integer = 0 To (12 - sSldCtt.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sSldCtt)
            End If
            '
            'Preenchimento Valor Acordo
            sCtt = dTotCtt.ToString().Trim()
            If sCtt.Length > 10 Then
                sMsgAux.Append("|" & sCtt.Substring(0, 10))
            Else
                sMsgAux.Append("|")
                If sCtt.Length < 10 Then
                    For i As Integer = 0 To (10 - sCtt.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sCtt)
            End If
            '
            'Preenchimento Saldo Empenho
            sSldEmp = CDbl(dTotSldEmp).ToString().Trim()
            If sSldEmp.Length > 10 Then
                sMsgAux.Append("|" & sSldEmp.Substring(0, 10))
            Else
                sMsgAux.Append("|")
                If sSldEmp.Length < 10 Then
                    For i As Integer = 0 To (10 - sSldEmp.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sSldEmp)
            End If
            '
            'Valor extra contrato
            sExt = CDbl(dTotExt).ToString().Trim()
            If sExt.Length > 10 Then
                sMsgAux.Append("|" & sExt.Substring(0, 10))
            Else
                sMsgAux.Append("|")
                If sExt.Length < 10 Then
                    For i As Integer = 0 To (10 - sExt.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sExt)
            End If
            '
            'Valor Participacao
            sPtc = dTotPtc.ToString().Trim()
            If sPtc.Length > 11 Then
                sMsgAux.Append("|" & sPtc.Substring(0, 11))
            Else
                sMsgAux.Append("|")
                If sPtc.Length < 11 Then
                    For i As Integer = 0 To (11 - sPtc.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sPtc)
            End If
            '
            'Valor da taxa de administração do fornecedor
            sTxaAdm = dTotTxaAdm.ToString().Trim()
            If sTxaAdm.Length > 10 Then
                sMsgAux.Append("|" & sTxaAdm.Substring(0, 10))
            Else
                sMsgAux.Append("|")
                If sTxaAdm.Length < 10 Then
                    For i As Integer = 0 To (10 - sTxaAdm.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sTxaAdm)
            End If
            '
            'Valor da taxa de brinde 
            sTxaBde = dTotTxaBde.ToString().Trim()
            If sTxaBde.Length > 10 Then
                sMsgAux.Append("|" & sTxaBde.Substring(0, 10))
            Else
                sMsgAux.Append("|")
                If sTxaBde.Length < 10 Then
                    For i As Integer = 0 To (10 - sTxaBde.Length) - 1
                        sMsgAux.Append(" ")
                    Next
                End If
                sMsgAux.Append(sTxaBde)
            End If
            sMsgAux.Append("|")
            '
            sMsgAux.Append("+-------+---------------------+--------+------------+----------+----------+----------+-----------+----------+----------+")
            '
            sMsgAux.Append("                                                                                                                        ")
            sMsgAux.Append("+------------+--------------------------------------------+------------------------------------------------------------+")
            sMsgAux.Append("| BU         | FORNECEDOR                                 |                  BENEFICIO DO FORNECEDOR                   |")
            sMsgAux.Append("+------------+--------------------------------------------+------------------------------------------------------------+")
            '
            For Each item In listaforn
                If item.CODUNDESRNGC <> 0 Then
                    sBU = CStr(item.CODUNDESRNGC).Trim() & " - " & item.DESUNDESRNGC.Trim()
                    '
                    If sBU.Length > 12 Then
                        sMsgAux.Append("|" & sBU.Substring(0, 12))
                    Else
                        sMsgAux.Append("|" & sBU)
                        If sBU.Length < 12 Then
                            For i As Integer = 0 To (12 - sBU.Length) - 1
                                sMsgAux.Append(" ")
                            Next
                        End If
                    End If
                End If

                '
                'Preenchimento do fornecedor
                sFrn = CStr(item.CODFRN).Trim() & " - " & item.NOMFRN.Trim()
                '
                If sFrn.Length > 44 Then
                    sMsgAux.Append("|" & sFrn.Substring(0, 44))
                Else
                    sMsgAux.Append("|" & sFrn)
                    If sFrn.Length < 44 Then
                        For i As Integer = 0 To (44 - sFrn.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                '
                sBfc = item.DESBFCFRNACOCMC.Trim()
                '
                If sBfc.Length > 60 Then
                    sMsgAux.Append("|" & sBfc.Substring(0, 60))
                Else
                    sMsgAux.Append("|" & sBfc)
                    If sBfc.Length < 60 Then
                        For i As Integer = 0 To (60 - sBfc.Length) - 1
                            sMsgAux.Append(" ")
                        Next
                    End If
                End If
                sMsgAux.Append("|")
                '
            Next
            '
            sMsgAux.Append("+------------+--------------------------------------------+------------------------------------------------------------+")

        End If

        If Not IsNothing(sMsgAux) Then
            Return sMsgAux.ToString()
        Else
            Return ""
        End If



    End Function

    Public Function IniDtsGerMrk() As DataSet
        Dim dtGerMrk As New DataTable
        Dim dsGerMrk As New DataSet

        dsGerMrk.Tables.Add(dtGerMrk)
        dsGerMrk.Tables(0).Columns.Add("CODGERMARKETING", GetType(Integer))
        dsGerMrk.Tables(0).Columns.Add("NOMGERMARKETING", GetType(String))

        Return dsGerMrk
    End Function

    Public Sub CosFlxApvAcoCmcOld(ByVal CodFncEmi As Decimal, ByVal CodAcoCmc As Decimal, ByVal CodSisInf As Decimal, ByVal NumFluApv As Decimal, ByVal NumSeqNivApv As Decimal,
                                    ByVal CodTipApvGerCtg As Decimal, ByVal CodTipApvDirCmp As Decimal, ByVal TipIdtFluCmp As Decimal, ByVal CodGerMrk As Decimal, ByVal NomGerMrk As String,
                                    ByRef dtsCabFluxoAprovacao As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao),
                                    ByRef dtsFluxoAprovacao As List(Of VO.FluxoDeAprovacao), ByVal indPreAcordo As Boolean)

        Dim itemCabFluxoAprovacao As New VO.RelacaoAcaoComercialxFluxoAprovacao
        Dim itemFluxoAprovacao As New VO.FluxoDeAprovacao
        Dim listfuncionarios As New List(Of VO.Funcionario)
        Dim listNiveisAprovadores As New List(Of VO.NiveisDoAprovadorDoFluxoPorSistema)

        Dim listFuncGAC As New List(Of VO.FluxoDeAprovacao)


        Dim iNumAuxSeqFluApv, iNumAuxNivFluApv, iNumPcpNivFluApv, iNumAuxNivFluApvDirCmp, iNumAuxPcpPriNiv As Decimal
        'Dim  iNumRcpNivFluApvDirCmp As Decimal

        Dim dsGerMrkAux As DataSet
        Dim newDsGerMrkAuxRow As DataRow

        itemCabFluxoAprovacao.CODACOCMC = CodAcoCmc
        itemCabFluxoAprovacao.NUMFLUAPV = NumFluApv

        iNumAuxNivFluApv = 0
        itemFluxoAprovacao.CODSISINF = CodSisInf
        itemFluxoAprovacao.NUMFLUAPV = NumFluApv
        itemFluxoAprovacao.NUMSEQFLUAPV = iNumAuxSeqFluApv
        itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now
        itemFluxoAprovacao.NUMSEQNIVAPV = CStr(iNumPcpNivFluApv.ToString() + "0" + iNumAuxNivFluApv.ToString())
        itemFluxoAprovacao.CODEDEAPV = CodFncEmi
        itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Criacao
        itemFluxoAprovacao.DATHRAAPVFLU = Nothing
        itemFluxoAprovacao.DESOBSAPV = Nothing
        itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Autor da ação."
        itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
        itemFluxoAprovacao.CODEDEARZ = Nothing

        dtsFluxoAprovacao.Add(itemFluxoAprovacao)
        itemFluxoAprovacao = New VO.FluxoDeAprovacao

        'Somente quando for Pré-Acordo
        If (indPreAcordo = True) Then
            boFluxoAprovacao = New FluxoDeAprovacaoBO
            listFuncGAC = boFluxoAprovacao.BuscaFuncionarioGAC()

            If (listFuncGAC.Count > 0) Then
                For Each item In listFuncGAC
                    iNumAuxSeqFluApv += 1
                    iNumAuxNivFluApv = 1
                    iNumPcpNivFluApv = 1

                    itemFluxoAprovacao.CODSISINF = CodSisInf
                    itemFluxoAprovacao.NUMFLUAPV = NumFluApv
                    itemFluxoAprovacao.NUMSEQFLUAPV = iNumAuxSeqFluApv
                    itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now

                    'itemFluxoAprovacao.NUMSEQNIVAPV = "1" + "0" + "0"
                    itemFluxoAprovacao.NUMSEQNIVAPV = CStr(iNumPcpNivFluApv.ToString() + "0" + iNumAuxNivFluApv.ToString())
                    'itemFluxoAprovacao.NUMSEQNIVAPV = CStr(item.NUMSEQNIVAPV.ToString() + "0" + iNumAuxNivFluApv.ToString())
                    itemFluxoAprovacao.CODEDEAPV = item.CODFNCAPVFIX
                    itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao
                    itemFluxoAprovacao.DATHRAAPVFLU = Nothing
                    itemFluxoAprovacao.DESOBSAPV = Nothing
                    itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Aprovador Obrigatório - GAC"
                    itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
                    itemFluxoAprovacao.CODEDEARZ = Nothing

                    'itemFluxoAprovacao.CODTIPAPV = item.CODTIPAPV
                Next
            End If

            dtsFluxoAprovacao.Add(itemFluxoAprovacao)
            itemFluxoAprovacao = New VO.FluxoDeAprovacao
        End If

        If CodGerMrk <> 0 And NomGerMrk <> "" Then
            dsGerMrkAux = IniDtsGerMrk()
            newDsGerMrkAuxRow = dsGerMrkAux.Tables(0).NewRow
            newDsGerMrkAuxRow("CODGERMARKETING") = CodGerMrk
            newDsGerMrkAuxRow("NOMGERMARKETING") = NomGerMrk
            dsGerMrkAux.Tables(0).Rows.Add(newDsGerMrkAuxRow)
            dsGerMrkAux.AcceptChanges()

        Else
            listfuncionarios = boFuncionarios.CnsFluApvGerMrk(CodFncEmi, CodAcoCmc)
            dsGerMrkAux = IniDtsGerMrk()

            If listfuncionarios.Count > 0 Then
                For Each itemfuncionario In listfuncionarios
                    newDsGerMrkAuxRow = dsGerMrkAux.Tables(0).NewRow
                    newDsGerMrkAuxRow("CODGERMARKETING") = itemfuncionario.CODFNC
                    newDsGerMrkAuxRow("NOMGERMARKETING") = itemfuncionario.NOMFNC
                    dsGerMrkAux.Tables(0).Rows.Add(newDsGerMrkAuxRow)
                Next
                dsGerMrkAux.AcceptChanges()

            End If
        End If

        If Not (dsGerMrkAux Is Nothing) Then
            If dsGerMrkAux.Tables(0).Rows.Count > 0 Then
                If dsGerMrkAux.Tables(0).Rows.Count > 0 Then iNumPcpNivFluApv = 1
                ' iNumAuxNivFluApv = 0

                For Each itemGerMkt As DataRow In dsGerMrkAux.Tables(0).Rows
                    iNumAuxPcpPriNiv = 1

                    If dtsFluxoAprovacao.Where(Function(x) x.CODEDEAPV = Decimal.Parse(itemGerMkt("CODGERMARKETING"))).ToList().Count = 0 Then

                        'Incrementa a sequencia do fluxo
                        iNumAuxSeqFluApv += 1
                        iNumAuxNivFluApv += 1

                        itemFluxoAprovacao.CODSISINF = CodSisInf
                        itemFluxoAprovacao.NUMFLUAPV = NumFluApv
                        itemFluxoAprovacao.NUMSEQFLUAPV = iNumAuxSeqFluApv
                        itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now

                        If (indPreAcordo = True) Then
                            iNumPcpNivFluApv = 1
                        End If

                        itemFluxoAprovacao.NUMSEQNIVAPV = CStr(iNumPcpNivFluApv.ToString() + "0" + iNumAuxNivFluApv.ToString())
                        itemFluxoAprovacao.CODEDEAPV = Decimal.Parse(itemGerMkt("CODGERMARKETING"))

                        If (indPreAcordo = True) Then
                            itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Aguardando_Aprovacao
                        Else
                            itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao
                        End If

                        itemFluxoAprovacao.DATHRAAPVFLU = Nothing
                        itemFluxoAprovacao.DESOBSAPV = Nothing
                        itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Aprovador Obrigatório"
                        itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
                        itemFluxoAprovacao.CODEDEARZ = Nothing
                        dtsFluxoAprovacao.Add(itemFluxoAprovacao)
                        itemFluxoAprovacao = New VO.FluxoDeAprovacao

                    End If
                Next
            End If
        End If

        listfuncionarios.Clear()
        listfuncionarios = Nothing

        boFuncionarios = New FuncionarioBO
        If Not boFuncionarios.VldItePmc(CodAcoCmc) = 0 Then
            listfuncionarios = boFuncionarios.CnsFluApvGerCtgDirCmp(CodSisInf, CodAcoCmc, NumSeqNivApv, CodTipApvGerCtg, CodTipApvDirCmp, TipIdtFluCmp)
            If listfuncionarios.Count > 0 Then
                iNumPcpNivFluApv = 2
                For Each itemFuncionarios In listfuncionarios
                    iNumAuxNivFluApvDirCmp += 1
                    If iNumAuxPcpPriNiv = 0 Then iNumAuxPcpPriNiv = 2

                    If dtsFluxoAprovacao.Where(Function(x) x.CODEDEAPV = Decimal.Parse(itemFuncionarios.CODGERCATEGORIA.Value)).ToList().Count = 0 Then

                        iNumPcpNivFluApv = 2
                        itemFluxoAprovacao.CODSISINF = CodSisInf
                        itemFluxoAprovacao.NUMFLUAPV = NumFluApv
                        itemFluxoAprovacao.NUMSEQFLUAPV = iNumAuxSeqFluApv
                        itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now
                        itemFluxoAprovacao.NUMSEQNIVAPV = CStr(iNumPcpNivFluApv.ToString() + "0" + iNumAuxNivFluApvDirCmp.ToString())
                        itemFluxoAprovacao.CODEDEAPV = itemFuncionarios.CODGERCATEGORIA.Value

                        If iNumPcpNivFluApv = iNumAuxPcpPriNiv Then 'Se estiver aguardando gerente de marketing
                            itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao
                        Else ' Se forem os primeiros aprovadores
                            itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Aguardando_Aprovacao
                        End If

                        itemFluxoAprovacao.DATHRAAPVFLU = Nothing
                        itemFluxoAprovacao.DESOBSAPV = Nothing
                        itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Valor Alçada: " + itemFuncionarios.VLRALC.Value.ToString() + " Valor Total Fornecedores: " + itemFuncionarios.PTCDIRCATEGORIA.Value.ToString()
                        itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
                        itemFluxoAprovacao.CODEDEARZ = Nothing
                        dtsFluxoAprovacao.Add(itemFluxoAprovacao)
                        itemFluxoAprovacao = New VO.FluxoDeAprovacao

                        If itemFuncionarios.FLGAPVDIRCMP = 1 Then
                            If dtsFluxoAprovacao.Where(Function(x) x.CODEDEAPV = Decimal.Parse(itemFuncionarios.CODDIRCOMPRAS.Value)).ToList().Count = 0 Then
                                ' iNumRcpNivFluApvDirCmp += 1
                                ' iNumAuxNivFluApvDirCmp = iNumRcpNivFluApvDirCmp
                                iNumPcpNivFluApv = 3

                                itemFluxoAprovacao.CODSISINF = CodSisInf
                                itemFluxoAprovacao.NUMFLUAPV = NumFluApv
                                itemFluxoAprovacao.NUMSEQFLUAPV = -(iNumAuxSeqFluApv)
                                itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now
                                itemFluxoAprovacao.NUMSEQNIVAPV = CStr(iNumPcpNivFluApv.ToString() + "0" + iNumAuxNivFluApvDirCmp.ToString())
                                itemFluxoAprovacao.CODEDEAPV = itemFuncionarios.CODDIRCOMPRAS.Value

                                If iNumPcpNivFluApv = iNumAuxPcpPriNiv Then 'Se estiver aguardando gerente de marketing
                                    itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao
                                Else ' Se forem os primeiros aprovadores
                                    itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Aguardando_Aprovacao
                                End If

                                itemFluxoAprovacao.DATHRAAPVFLU = Nothing
                                itemFluxoAprovacao.DESOBSAPV = Nothing
                                itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Aprovador anterior com alçada inferior ao valor dos fornecedores"
                                itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
                                itemFluxoAprovacao.CODEDEARZ = Nothing
                                dtsFluxoAprovacao.Add(itemFluxoAprovacao)
                                itemFluxoAprovacao = New VO.FluxoDeAprovacao
                            Else
                                iNumAuxNivFluApvDirCmp = dtsFluxoAprovacao.Where(Function(x) x.CODEDEAPV = Decimal.Parse(itemFuncionarios.CODDIRCOMPRAS.Value)).ToList().Item(0).NUMSEQNIVAPV.Value
                            End If
                        End If


                    End If
                Next
            End If
        End If

        'Somente quando não for Pré-Acordo
        If (indPreAcordo = False) Then
            'TODO: comentado por dar loop infinito, verificar se o valor da variavel esta correto
            'iNumAuxSeqFluApv = AtuSeqApv(dtsFluxoAprovacao, iNumAuxSeqFluApv)

            'boNoveosAprovacao = New NiveisDoAprovadorDoFluxoPorSistemaBO

            'listNiveisAprovadores = boNoveosAprovacao.CnsApvNot(CodSisInf)
            'If listNiveisAprovadores.Count > 0 Then
            '    For Each itemNiviesAprovadores In listNiveisAprovadores
            '        iNumAuxSeqFluApv += 1

            '        iNumPcpNivFluApv = 4
            '        iNumAuxNivFluApvDirCmp = 1

            '        itemFluxoAprovacao.CODSISINF = CodSisInf
            '        itemFluxoAprovacao.NUMFLUAPV = NumFluApv
            '        itemFluxoAprovacao.NUMSEQFLUAPV = iNumAuxSeqFluApv
            '        itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now
            '        itemFluxoAprovacao.NUMSEQNIVAPV = CStr(iNumPcpNivFluApv.ToString() + CStr(0) + iNumAuxNivFluApvDirCmp.ToString())
            '        itemFluxoAprovacao.CODEDEAPV = itemNiviesAprovadores.CODFNCAPVFIX
            '        itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Notificacao
            '        itemFluxoAprovacao.DATHRAAPVFLU = Nothing
            '        itemFluxoAprovacao.DESOBSAPV = Nothing
            '        itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Usuário fixo a ser notificado."
            '        itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
            '        itemFluxoAprovacao.CODEDEARZ = Nothing
            '        dtsFluxoAprovacao.Add(itemFluxoAprovacao)
            '        itemFluxoAprovacao = New VO.FluxoDeAprovacao
            '    Next
            'End If
        End If

        'Se nao tem aprovador e sim somente o gerador do fluxo
        If dtsFluxoAprovacao.Count = 1 Then
            For Each item In dtsFluxoAprovacao
                item.TIPSTAFLUAPV = TipStaFluApv.Aprovado
                item.DATHRAAPVFLU = DateTime.Now
                item.DESOBSAPV = "Criado e Aprovado"
            Next
            '
            itemCabFluxoAprovacao.CODSTAAPVACOCMC = TipStaFluApv.Aprovado
            '
        Else
            itemCabFluxoAprovacao.CODSTAAPVACOCMC = TipStaFluApv.Em_Aprovacao
        End If
        dtsCabFluxoAprovacao.Add(itemCabFluxoAprovacao)

        'Throw New Exception("aborta tudo")
    End Sub


    Public Sub CosFlxApvAcoCmc(ByVal CodFncEmi As Decimal,
                               ByVal CodAcoCmc As Decimal,
                               ByVal CodSisInf As Decimal,
                               ByVal NumFluApv As Decimal,
                               ByVal NumSeqNivApv As Decimal,
                               ByVal CodTipApvGerCtg As Decimal,
                               ByVal CodTipApvDirCmp As Decimal,
                               ByVal TipIdtFluCmp As Decimal,
                               ByVal CodGerMrk As Decimal,
                               ByVal NomGerMrk As String,
                               ByRef dtsCabFluxoAprovacao As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao),
                               ByRef dtsFluxoAprovacao As List(Of VO.FluxoDeAprovacao),
                               ByVal indPreAcordo As Boolean)

        Dim itemCabFluxoAprovacao As New VO.RelacaoAcaoComercialxFluxoAprovacao

        Dim iNumAuxSeqFluApv As Integer = 0

        itemCabFluxoAprovacao.CODACOCMC = CodAcoCmc
        itemCabFluxoAprovacao.NUMFLUAPV = NumFluApv

        ' #INICIO - Nivel 000 - Autor da ação
        Using itemFluxoAprovacao As New VO.FluxoDeAprovacao

            itemFluxoAprovacao.CODSISINF = CodSisInf
            itemFluxoAprovacao.NUMFLUAPV = NumFluApv
            itemFluxoAprovacao.NUMSEQFLUAPV = iNumAuxSeqFluApv
            itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now
            itemFluxoAprovacao.NUMSEQNIVAPV = 0 'CStr("00" + iNumAuxNivFluApv.ToString())
            itemFluxoAprovacao.CODEDEAPV = CodFncEmi
            itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Criacao
            itemFluxoAprovacao.DATHRAAPVFLU = Nothing
            itemFluxoAprovacao.DESOBSAPV = Nothing
            itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Autor da ação."
            itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
            itemFluxoAprovacao.CODEDEARZ = Nothing

            dtsFluxoAprovacao.Add(itemFluxoAprovacao)
        End Using
        '==============================================================================================================================
        ' #FIM - Nivel 000 - Autor da ação
        '==============================================================================================================================

        ' #INICIO - Nivel 100 - 
        Dim iNumSeqNivApv100 As Integer = 0


        'Somente quando for Pré-Acordo
        If (indPreAcordo = True) Then
            boFluxoAprovacao = New FluxoDeAprovacaoBO
            Dim listFuncGAC As List(Of VO.FluxoDeAprovacao) = boFluxoAprovacao.BuscaFuncionarioGAC()

            If (listFuncGAC.Count > 0) Then
                For Each item In listFuncGAC
                    Using itemFluxoAprovacao As New VO.FluxoDeAprovacao

                        iNumAuxSeqFluApv += 1
                        iNumSeqNivApv100 += 1

                        itemFluxoAprovacao.CODSISINF = CodSisInf
                        itemFluxoAprovacao.NUMFLUAPV = NumFluApv
                        itemFluxoAprovacao.NUMSEQFLUAPV = iNumAuxSeqFluApv
                        itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now
                        itemFluxoAprovacao.NUMSEQNIVAPV = CStr("10" + iNumSeqNivApv100.ToString())
                        itemFluxoAprovacao.CODEDEAPV = item.CODFNCAPVFIX
                        itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao
                        itemFluxoAprovacao.DATHRAAPVFLU = Nothing
                        itemFluxoAprovacao.DESOBSAPV = Nothing
                        itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Aprovador Obrigatório - GAC"
                        itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
                        itemFluxoAprovacao.CODEDEARZ = Nothing
                        dtsFluxoAprovacao.Add(itemFluxoAprovacao)
                    End Using
                Next
            End If
        End If

        '
        Dim listFuncionariosNivel100 As New List(Of VO.Funcionario)
        If CodGerMrk <> 0 And NomGerMrk <> "" Then
            listFuncionariosNivel100.Add(New VO.Funcionario() With {.CODFNC = CodGerMrk, .NOMFNC = NomGerMrk})
        Else
            listFuncionariosNivel100 = boFuncionarios.CnsFluApvGerMrk(CodFncEmi, CodAcoCmc)
        End If

        Dim iNumAuxPcpPriNiv As Integer
        For Each itemGerMkt In listFuncionariosNivel100
            iNumAuxPcpPriNiv = 1

            If dtsFluxoAprovacao.Where(Function(x) x.CODEDEAPV = itemGerMkt.CODFNC).ToList().Count = 0 Then
                Using itemFluxoAprovacao As New VO.FluxoDeAprovacao

                    'Incrementa a sequencia do fluxo
                    iNumAuxSeqFluApv += 1
                    iNumSeqNivApv100 += 1

                    itemFluxoAprovacao.CODSISINF = CodSisInf
                    itemFluxoAprovacao.NUMFLUAPV = NumFluApv
                    itemFluxoAprovacao.NUMSEQFLUAPV = iNumAuxSeqFluApv
                    itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now
                    itemFluxoAprovacao.NUMSEQNIVAPV = CStr("10" + iNumSeqNivApv100.ToString())
                    itemFluxoAprovacao.CODEDEAPV = itemGerMkt.CODFNC
                    If (indPreAcordo = True) Then
                        itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Aguardando_Aprovacao
                    Else
                        itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Em_Aprovacao
                    End If

                    itemFluxoAprovacao.DATHRAAPVFLU = Nothing
                    itemFluxoAprovacao.DESOBSAPV = Nothing
                    itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Aprovador Obrigatório"
                    itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
                    itemFluxoAprovacao.CODEDEARZ = Nothing
                    dtsFluxoAprovacao.Add(itemFluxoAprovacao)
                End Using
            End If
        Next

        '==============================================================================================================================
        ' #FIM - Nivel 100 - 
        '==============================================================================================================================


        ' #INICIO - Nivel 200 e 300 - 

        ' Controla a sequencia 20X
        Dim iNumAuxNivFluApv200 As Integer = 0
        ' Controla a sequencia 30X
        Dim iNumAuxNivFluApv300 As Integer = 0

        boFuncionarios = New FuncionarioBO
        If Not boFuncionarios.VldItePmc(CodAcoCmc) = 0 Then

            Dim sequencia As Integer = 0
            Dim listFunApvGerDir As List(Of VO.Funcionario) = boFuncionarios.CnsFluApvGerCtgDirCmp(CodSisInf,
                                                                                                   CodAcoCmc,
                                                                                                   NumSeqNivApv,
                                                                                                   CodTipApvGerCtg,
                                                                                                   CodTipApvDirCmp,
                                                                                                   TipIdtFluCmp)

            Dim listFunApvDirCat As New List(Of VO.Funcionario)
            For Each diretor In listFunApvGerDir
                If (listFunApvDirCat.Where(Function(f) f.FLGAPVDIRCMP = diretor.FLGAPVDIRCMP And f.CODDIRCOMPRAS = diretor.CODDIRCOMPRAS).FirstOrDefault() Is Nothing) Then
                    listFunApvDirCat.Add(diretor)
                End If
            Next

            For Each diretor In listFunApvDirCat
                sequencia += 1

                Dim listGerCat As List(Of VO.Funcionario) = listFunApvGerDir.Where(Function(f) f.FLGAPVDIRCMP = diretor.FLGAPVDIRCMP And f.CODDIRCOMPRAS = diretor.CODDIRCOMPRAS).ToList()

                '------------------------------------------------------------------------------------------------------------------------------
                '  Nivel 200 - Gerentes
                '------------------------------------------------------------------------------------------------------------------------------
                For Each gerente In listGerCat
                    If (dtsFluxoAprovacao.Where(Function(f) f.CODEDEAPV = gerente.CODGERCATEGORIA.Value).FirstOrDefault() Is Nothing) Then
                        iNumAuxSeqFluApv += 1
                        Using itemFluxoAprovacao As New VO.FluxoDeAprovacao
                            itemFluxoAprovacao.CODSISINF = CodSisInf
                            itemFluxoAprovacao.NUMFLUAPV = NumFluApv
                            itemFluxoAprovacao.NUMSEQFLUAPV = iNumAuxSeqFluApv
                            itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now
                            itemFluxoAprovacao.NUMSEQNIVAPV = "20" + sequencia.ToString()
                            itemFluxoAprovacao.CODEDEAPV = gerente.CODGERCATEGORIA.Value
                            itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Aguardando_Aprovacao
                            itemFluxoAprovacao.DATHRAAPVFLU = Nothing
                            itemFluxoAprovacao.DESOBSAPV = Nothing
                            itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Valor Alçada: " + gerente.VLRALC.Value.ToString() + " Valor Total Fornecedores: " + gerente.PTCDIRCATEGORIA.Value.ToString()
                            itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
                            itemFluxoAprovacao.CODEDEARZ = Nothing
                            dtsFluxoAprovacao.Add(itemFluxoAprovacao)
                        End Using
                    End If
                Next

                '------------------------------------------------------------------------------------------------------------------------------
                '  Nivel 300 - Diretories
                '------------------------------------------------------------------------------------------------------------------------------
                If (diretor.FLGAPVDIRCMP <> 0) Then
                    iNumAuxSeqFluApv += 1
                    Using itemFluxoAprovacao As New VO.FluxoDeAprovacao
                        itemFluxoAprovacao.CODSISINF = CodSisInf
                        itemFluxoAprovacao.NUMFLUAPV = NumFluApv
                        itemFluxoAprovacao.NUMSEQFLUAPV = iNumAuxSeqFluApv
                        itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now
                        itemFluxoAprovacao.NUMSEQNIVAPV = "30" + sequencia.ToString()
                        itemFluxoAprovacao.CODEDEAPV = diretor.CODDIRCOMPRAS.Value
                        itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Aguardando_Aprovacao
                        itemFluxoAprovacao.DATHRAAPVFLU = Nothing
                        itemFluxoAprovacao.DESOBSAPV = Nothing
                        itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Aprovador anterior com alçada inferior ao valor dos fornecedores"
                        itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
                        itemFluxoAprovacao.CODEDEARZ = Nothing
                        dtsFluxoAprovacao.Add(itemFluxoAprovacao)
                    End Using
                End If
            Next
        End If

        '==============================================================================================================================
        ' #FIM -  Nivel 200 e 300 - 
        '==============================================================================================================================



        'Somente quando não for Pré-Acordo
        'If (indPreAcordo = False) Then
        '    TODO: comentado por dar loop infinito, verificar se o valor da variavel esta correto
        '    iNumAuxSeqFluApv = AtuSeqApv(dtsFluxoAprovacao, iNumAuxSeqFluApv)

        '    boNoveosAprovacao = New NiveisDoAprovadorDoFluxoPorSistemaBO

        '    listNiveisAprovadores = boNoveosAprovacao.CnsApvNot(CodSisInf)
        '    If listNiveisAprovadores.Count > 0 Then
        '        For Each itemNiviesAprovadores In listNiveisAprovadores
        '            iNumAuxSeqFluApv += 1

        '            iNumPcpNivFluApv = 4
        '            iNumAuxNivFluApvDirCmp = 1

        '            itemFluxoAprovacao.CODSISINF = CodSisInf
        '            itemFluxoAprovacao.NUMFLUAPV = NumFluApv
        '            itemFluxoAprovacao.NUMSEQFLUAPV = iNumAuxSeqFluApv
        '            itemFluxoAprovacao.DATHRAFLUAPV = DateTime.Now
        '            itemFluxoAprovacao.NUMSEQNIVAPV = CStr(iNumPcpNivFluApv.ToString() + CStr(0) + iNumAuxNivFluApvDirCmp.ToString())
        '            itemFluxoAprovacao.CODEDEAPV = itemNiviesAprovadores.CODFNCAPVFIX
        '            itemFluxoAprovacao.TIPSTAFLUAPV = TipStaFluApv.Notificacao
        '            itemFluxoAprovacao.DATHRAAPVFLU = Nothing
        '            itemFluxoAprovacao.DESOBSAPV = Nothing
        '            itemFluxoAprovacao.DESMTVAPVFLUACOCMC = "Usuário fixo a ser notificado."
        '            itemFluxoAprovacao.NUMSEQFLUAPVPEDOPN = Nothing
        '            itemFluxoAprovacao.CODEDEARZ = Nothing
        '            dtsFluxoAprovacao.Add(itemFluxoAprovacao)
        '            itemFluxoAprovacao = New VO.FluxoDeAprovacao
        '        Next
        '    End If
        'End If

        'Se nao tem aprovador e sim somente o gerador do fluxo
        If dtsFluxoAprovacao.Count = 1 Then
            For Each item In dtsFluxoAprovacao
                item.TIPSTAFLUAPV = TipStaFluApv.Aprovado
                item.DATHRAAPVFLU = DateTime.Now
                item.DESOBSAPV = "Criado e Aprovado"
            Next
            '
            itemCabFluxoAprovacao.CODSTAAPVACOCMC = TipStaFluApv.Aprovado
            '
        Else
            itemCabFluxoAprovacao.CODSTAAPVACOCMC = TipStaFluApv.Em_Aprovacao
        End If
        dtsCabFluxoAprovacao.Add(itemCabFluxoAprovacao)

    End Sub



    Private Function AtuSeqApv(ByRef listafluxoparovacao As List(Of VO.FluxoDeAprovacao), ByVal iNumAuxSeqFluApv As Integer) As Integer
        Dim listafluxoparovacaoAux As New List(Of VO.FluxoDeAprovacao)

        Try
            listafluxoparovacaoAux = listafluxoparovacao.Where(Function(X) X.NUMSEQFLUAPV < 0).ToList().OrderBy(Function(x) x.NUMSEQNIVAPV).ToList()
            For itemfluxoaprovacoa As Integer = 0 To listafluxoparovacao.Count - 1
                iNumAuxSeqFluApv += 1
                listafluxoparovacao.Item(itemfluxoaprovacoa).NUMSEQNIVAPV = iNumAuxSeqFluApv
                listafluxoparovacaoAux = listafluxoparovacaoAux.Where(Function(X) X.NUMSEQFLUAPV < 0).ToList()
                If listafluxoparovacaoAux.Count <> 0 Then
                    'iNumAuxSeqFluApv = AtuSeqApv(listafluxoparovacao, iNumAuxSeqFluApv)
                End If
                Exit For
            Next

            Return iNumAuxSeqFluApv
        Catch ex As Exception

        End Try
    End Function


    Function VerificaTaxaBrinde(ByVal acaoComercial As VO.AcaoComercial) As Boolean
        If Not (acaoComercial.CODACOCMC Is Nothing) Then
            Dim SldExaCttBde, SldCttBde As Decimal
            Dim daoFornecedor As RelacaoAcaoComercialxFornecedoresDAO = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO)

            Dim listaRelacaoFornecedores = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BuscarFornecedoresDaAcao(acaoComercial.CODACOCMC)

            For Each fornecedor As VO.RelacaoAcaoComercialxFornecedores In listaRelacaoFornecedores
                SldExaCttBde = fornecedor.VLRUTZEXACTTACOCMC  'Saldo Extra Contrato Disponivel
                SldCttBde = fornecedor.VLRSLDDSNACR  'Saldo Contrato Disponivel
                If Not (SldExaCttBde + SldCttBde >= acaoComercial.TaxaBrinde) Then
                    Throw New MappedWarnException("Taxa de Brinde do fornecedor " & fornecedor.CODFRN & " superior ao saldo disponivel!")
                    Return False
                End If
            Next
        End If
        Return True
    End Function

    Function VerificaDadosAcao(ByVal acaoComercial As VO.AcaoComercial, ByVal incentivosHabilitado As Boolean, ByVal indPreAcordo As Boolean) As Boolean

        Dim listParametrosGerais As New List(Of VO.ParametrosGeraisDoIncentivoDeVendas)
        Dim StrMessagem As StringBuilder
        Dim bExisteinconsitencia As Boolean = False

        If incentivosHabilitado Then
            If acaoComercial.Incentivos.Count > 0 Then
                For Each ItemIcentivos In acaoComercial.Incentivos
                    listParametrosGerais = boPrtGeraisIncentivoDeVendas.BuscarValidadoresIcentivos(ItemIcentivos.CODICT)

                    If listParametrosGerais.Count > 0 Then

                        StrMessagem.AppendLine("ATENÇÃO! INCONSISTÊNCIAS NO CADASTRO DE INCENTIVO DE CODIGO : " + ItemIcentivos.CODICT.ToString())

                        If listParametrosGerais.Item(0).QTDCADICENTIVOS = 0 Then
                            bExisteinconsitencia = True
                            StrMessagem.AppendLine("FALTA CADASTRO INCENTIVO OU INCENTIVO NAO LIBERADO") '0 - VALIDA CADASTRO INCENTIVO
                        End If

                        If listParametrosGerais.Item(0).QTDPUBLICOALVO = 0 Then
                            bExisteinconsitencia = True
                            StrMessagem.AppendLine("FALTA CADASTRO PUBLICO ALVO DO INCENTIVO") '1 - VALIDA PUBLICO ALVO
                        End If

                        If listParametrosGerais.Item(0).QTDMERCADORIAS = 0 Then
                            bExisteinconsitencia = True
                            StrMessagem.AppendLine("FALTA CADASTRO MERCADORIA DO INCENTIVO") '1 - VALIDA PUBLICO ALVO
                        End If

                        If listParametrosGerais.Item(0).QTDMETAS = 0 Then
                            bExisteinconsitencia = True
                            StrMessagem.AppendLine("FALTA CADASTRO METAS PARA O INCENTIVO") '3 - VALIDA METAS
                        End If

                        If listParametrosGerais.Item(0).QTDCRITERIOS = 0 Then
                            bExisteinconsitencia = True
                            StrMessagem.AppendLine("FALTA CADASTRO RELAÇÃO PÚBLICO ALVO X ABRANGÊNCIA ") '4 - VALIDA CRITÉRIOS
                        End If

                        If listParametrosGerais.Item(0).QTDCRITERIOSAPURACAO = 0 Then
                            bExisteinconsitencia = True
                            StrMessagem.AppendLine("FALTA CADASTRO CRITÉRIO PARA ALGUMA RELAÇÃO PÚBLICO ALVO X ABRANGÊNCIA") '5 - VALIDA CRITERIO APURACAO
                        End If

                        If listParametrosGerais.Item(0).QTDPREMIACAO = 0 Then
                            bExisteinconsitencia = True
                            StrMessagem.AppendLine("FALTA CADASTRO PREMIAÇÃO PARA ALGUMA RELAÇÃO PÚBLICO ALVO X ABRANGÊNCIA") '6 - VALIDA PREMIAÇÃO
                        End If
                    End If

                    listParametrosGerais.Clear()
                Next

                If StrMessagem.Length > 0 Then
                    Throw New MappedWarnException(StrMessagem.ToString())
                    Return False
                End If
            Else
                Throw New MappedWarnException("Para continuar é necessário selecionar o Incentivo que será atrelado a esta ação!")
                Return False
            End If
        End If

        If acaoComercial.Fornecedores.Count = 0 AndAlso acaoComercial.Carimbos.Count = 0 AndAlso acaoComercial.Premiacao.Count = 0 AndAlso acaoComercial.Bonificacoes.Count = 0 Then
            Throw New MappedWarnException("Patrocinadores da ação não escolhidos!")
            Return False
        ElseIf acaoComercial.Fornecedores.Count <> 0 Then
            If acaoComercial.Fornecedores.Item(0).CODFRN = 999999 Then
                If acaoComercial.Fornecedores.Item(0).CODUNDESRNGC Is Nothing Then
                    Throw New MappedWarnException("BU do fornecedor não escolhido!")
                    Return False
                ElseIf acaoComercial.Fornecedores.Item(0).VLRUTZEXACTTACOCMC Is Nothing Then
                    Throw New MappedWarnException("Valor extra acordo não digitado!")
                    Return False
                End If
            End If
        End If

        acaoComercial.EmpenhoExa.Split("-")(0).Trim()

        Dim codfrn As Decimal = 0
        Dim empexactt As Decimal = 0
        Dim SldValCtt As Decimal = 0
        Dim SldValExtCtt As Decimal = 0
        Dim anomes As String = Date.Now.ToString("yyyyMM")
        Dim Paramestros As List(Of VO.ParametroSistemaAdminitracaoVenda)
        Dim saldoForncedorMarketing As List(Of VO.SaldoFornecedorMarketing)
        Dim codigoFornecedorMarketing As Decimal
        Dim SldMrg As Decimal = 0

        Dim boAcaoComercial As New AcaoComercialBO()

        For Each itemfornecedor In acaoComercial.Fornecedores

            If Not itemfornecedor.CODFRN Is Nothing Then
                codfrn = itemfornecedor.CODFRN
            Else
                codfrn = 0
            End If

            If Not itemfornecedor.TIPDSNDSCBNF Is Nothing Then
                empexactt = itemfornecedor.TIPDSNDSCBNF
            Else
                empexactt = 0
            End If

            SldValCtt = BuscaValorSaldoAcordoFornecedor(codfrn, empexactt, anomes)
            If Integer.Parse(acaoComercial.EmpenhoExa.Split("-")(0).Trim()) = empenhoFornecedorMarketing Then

                Paramestros = boParametros.ListarParametrosAcaoComercial()
                If Paramestros.Count > 0 Then
                    codigoFornecedorMarketing = Paramestros.Item(0).CODFRNMKT.Value
                Else
                    Throw New MappedWarnException("Código do fornecedor de Marketing não encontrado!" & vbCrLf & "Contate o analista responsável.")
                    Return False
                End If

                saldoForncedorMarketing = boAcaoComercial.BuscarSaldoFornecedorMarketing(codfrn, Integer.Parse(acaoComercial.EmpenhoExa.Split("-")(0).Trim()), anomes)
                If saldoForncedorMarketing.Count > 0 Then
                    SldValExtCtt = saldoForncedorMarketing.Item(0).VLRSLDDSP.Value
                Else
                    SldValExtCtt = 0
                End If
            Else
                SldValExtCtt = BuscaValorSaldoExaAcordoFornecedor(codfrn, Integer.Parse(acaoComercial.EmpenhoExa.Split("-")(0).Trim()), anomes)
            End If

            If Not itemfornecedor.VLRSLDDSNACR Is Nothing Then
                SldMrg = itemfornecedor.VLRSLDDSNACR
            End If

            itemfornecedor.VLRSLDDSNCTTACOCMC = SldValCtt
            itemfornecedor.VLRSLDDSNDSCACOCMC = SldValExtCtt

            If ((SldValCtt + SldMrg) < itemfornecedor.VLRUTZCTTACOCMC) Then
                Throw New MappedWarnException("Atenção : O Fornecedor " + itemfornecedor.DESBFCFRNACOCMC + " tem o SALDO DO ACORDO insuficiente para efetivar esta ação. Efetue as correções necessárias e tente novamente.")
                Return False
            End If

            'If ((SldValCtt + SldMrg) < itemfornecedor.VLRUTZEXACTTACOCMC) Then
            '    Throw New MappedWarnException("Atenção : O Fornecedor " + itemfornecedor.DESBFCFRNACOCMC + " tem o SALDO DE EXTRA ACORDO insuficiente para efetivar esta ação. Efetue as correções necessárias e tente novamente.")
            '    Return False
            'End If
        Next

        If acaoComercial.ItensOrcamento.Count = 0 Then
            Dim RelacaoAcaoMecanincaBO As New RelacaoAcaoMecanicaCriterioDeApuracaoBO()
            Dim ListaCount = RelacaoAcaoMecanincaBO.VerificaPromocaoTipoBrinde(acaoComercial.CODACOCMC)
            If (ListaCount.Count() > 0) Then
                Throw New MappedWarnException("Itens do orçamento da ação não escolhidos!")
                Return False
            End If

        End If

        If (acaoComercial.INDVCLPMC = 1) Then '  1 = indica que a ação mercadologica tera mecanica vinculada a apromoção
            If (acaoComercial.INDACOCMCCPL = Constantes.NAO) Then
                Dim BO As RelacaoAcaoMecanicaBO = New RelacaoAcaoMecanicaBO()
                Dim relacaoAcaoMecanica As List(Of VO.RelacaoAcaoMecanica) = BO.ListT(New VO.RelacaoAcaoMecanica() With {.CODACOCMC = acaoComercial.CODACOCMC})
                If (relacaoAcaoMecanica.Count = 0) Then
                    Throw New MappedWarnException("Mecânica da ação não digitada!")
                    Return False
                End If

                For Each mecanica In relacaoAcaoMecanica
                    Dim relacaoAcaoMecanicaFilialBO As New RelacaoAcaoMecanicaFilialBO()
                    Dim filiais = relacaoAcaoMecanicaFilialBO.ListT(New VO.RelacaoAcaoMecanicaFilial() With {.CODACOCMC = mecanica.CODACOCMC, .NUMSEQRGRACOCMC = mecanica.NUMSEQRGRACOCMC})

                    If (filiais.Count = 0) Then
                        Throw New MappedWarnException(String.Format("Verifique a mecânica ""{0}"" Nenhuma filial selecionada!", mecanica.DESRGRACOCMC))
                        Return False
                    End If

                    If (mecanica.INDPTCTOTMIXPMC = 0) Then
                        Dim promocaoMixVisualizarVigenteBO As New PromocaoMixVisualizarVigenteBO()

                        Dim TIPFRNACOCMC As Nullable(Of Decimal)
                        If (acaoComercial.Fornecedores IsNot Nothing AndAlso acaoComercial.Fornecedores.Any()) Then
                            TIPFRNACOCMC = acaoComercial.Fornecedores.FirstOrDefault().TIPFRNACOCMC
                        End If
                        Dim mixPromocao As List(Of VO.PromocaoMixVisualizarVigente)

                        If (TIPFRNACOCMC = 1) Then
                            mixPromocao = promocaoMixVisualizarVigenteBO.PreVisualizarMixMecanicaAcaoEdl(mecanica.CODACOCMC, mecanica.NUMSEQRGRACOCMC, mecanica.TIPPMC)
                        Else
                            mixPromocao = promocaoMixVisualizarVigenteBO.PreVisualizarMixMecanicaAcao(mecanica.CODACOCMC, mecanica.NUMSEQRGRACOCMC, mecanica.TIPPMC)
                        End If

                        If (mixPromocao Is Nothing) Then
                                Throw New MappedWarnException(String.Format("Verifique a mecânica ""{0}"" MIX da promocao não configurado!", mecanica.DESRGRACOCMC))
                                Return False
                            End If
                            If (mixPromocao.Count = 0) Then
                                Throw New MappedWarnException(String.Format("Verifique a mecânica ""{0}"" a configuração de MIX não retorna nenhum item!", mecanica.DESRGRACOCMC))
                                Return False
                            End If
                        End If
                Next
            End If
        ElseIf Not (acaoComercial.Mecanica Is Nothing) Then
            If String.IsNullOrEmpty(acaoComercial.Mecanica.DESRGRACOCMC.Trim()) Then
                Throw New MappedWarnException("Mecânica da ação não digitada!")
                Return False
            End If
        Else
            Throw New MappedWarnException("Mecânica da ação não digitada!")
            Return False
        End If



        If acaoComercial.INDALTFRNACOCMC = 1 Then
            For Each itemorcamento In acaoComercial.ItensOrcamento
                If itemorcamento.INDTIPOPEDSNACOCMC <> 2 And itemorcamento.INDTIPOPEDSNACOCMC <> 4 Then
                    Throw New MappedWarnException("Ação complementar Despesas Martins possui itens com operação diferente de promoção!")
                    Return False
                End If
            Next
        End If

        If (indPreAcordo = True) Then
            Dim listFuncGAC As New List(Of VO.FluxoDeAprovacao)
            boFluxoAprovacao = New FluxoDeAprovacaoBO
            listFuncGAC = boFluxoAprovacao.BuscaFuncionarioGAC()

            If (listFuncGAC.Count = 0) Then
                Throw New MappedWarnException("Não existe Funcionário GAC para a Aprovação! ")
                Return False
            End If
        End If

        Return True

    End Function

    'VldAcoCmc : Valida Incentivos
    Public Function VldAcoCmc(acaoComercial As VO.AcaoComercial)
        ''ByVal CodIct As Integer
        'Dim oObeCnx As IAU013.UO_IAUCnxAcsDdo
        'Dim oObeCnsDdo As New DB_CBUAcoCmcIct
        'Dim oResVlc(6) As String
        'Dim oDs As DataSet
        'Dim oTbl As DataTable
        'Dim oCol1 As DataColumn
        'Dim oDtRow As DataRow

        'Try

        '    'Criando Programaticamente um Dataset para retornar os valores da validação
        '    oDs = New DataSet
        '    oTbl = New DataTable("0")
        '    oCol1 = New DataColumn("VldCadIct", Type.GetType("System.Int32"))
        '    oTbl.Columns.Add(oCol1)
        '    oCol1 = New DataColumn("VldPubAlv", Type.GetType("System.Int32"))
        '    oTbl.Columns.Add(oCol1)
        '    oCol1 = New DataColumn("VldMer", Type.GetType("System.Int32"))
        '    oTbl.Columns.Add(oCol1)
        '    oCol1 = New DataColumn("VldMdd", Type.GetType("System.Int32"))
        '    oTbl.Columns.Add(oCol1)
        '    oCol1 = New DataColumn("VldCtr", Type.GetType("System.Int32"))
        '    oTbl.Columns.Add(oCol1)
        '    oCol1 = New DataColumn("VldCtrApu", Type.GetType("System.Int32"))
        '    oTbl.Columns.Add(oCol1)
        '    oCol1 = New DataColumn("VldPrm", Type.GetType("System.Int32"))
        '    oTbl.Columns.Add(oCol1)
        '    oDs.Tables.Add(oTbl)

        '    oObeCnx = New IAU013.UO_IAUCnxAcsDdo("DB001", "AIADM")
        '    oObeCnx.IniTsc()
        '    oDtRow = oTbl.NewRow
        '    oDtRow("VldCadIct") = oObeCnsDdo.VldCadIct(CodIct, oObeCnx) 'VALIDA CADASTRO INCENTIVO
        '    oDtRow("VldPubAlv") = oObeCnsDdo.VldPubAlv(CodIct, oObeCnx) 'VALIDA PUBLICO ALVO
        '    oDtRow("VldMer") = oObeCnsDdo.VldMer(CodIct, oObeCnx)       'VALIDA MERCADORIAS
        '    oDtRow("VldMdd") = oObeCnsDdo.VldMdd(CodIct, oObeCnx)       'VALIDA METAS
        '    oDtRow("VldCtr") = oObeCnsDdo.VldCtr(CodIct, oObeCnx)       'VALIDA CRITÉRIOS
        '    oDtRow("VldCtrApu") = oObeCnsDdo.VldCtrApu(CodIct, oObeCnx) 'VALIDA CRITERIO APURACAO
        '    oDtRow("VldPrm") = oObeCnsDdo.VldPrm(CodIct, oObeCnx)       'VALIDA PREMIAÇÃO
        '    oTbl.Rows.Add(oDtRow)
        '    oObeCnx.FimTscSuc()

        '    Return oDs

        'Catch oEcc As Exception
        '    oObeCnx.FimTscErr()
        '    Dim oExp As New Exception("Funcao: CBU331.BO_CBUAcoCmcIct.VldAcoCmc " & vbNewLine & oEcc.Message)
        '    Publish(oExp)
        '    Throw oExp
        'Finally
        '    If Not IsNothing(oObeCnx) Then oObeCnx.Dispose()
        'End Try
        Return False
    End Function

    Function InsDdoIteAcoCmc() As Boolean

        'Dim oSvcRdeRmt As wsSvcRmtDK.SvcRmt
        'Dim aux As Integer = 1
        'Dim str As String
        'Dim cont As Integer
        'Dim rsl As Integer
        'Dim dat As String
        ''Rodrigo 16/07/2007
        'Dim LstPmt(2) As Object
        ''Fim Rodrigo 

        ''Dim arrCols(26) As String 'alterado tamanho vetor, de arrCols(24) para arrCols(26), 
        ''para acrescimo de cliente e fornecedor. 
        ''Criado por Daniel Souza(Ética Soluções Integrais)21/05/2007. 27º Inclusao.

        'Dim arrCols(30) As String 'Rodrigo 06/09/2007 Inclusao do Status do item de suprimento

        'Dim objDdo As Object
        'Dim DataRow As DataRowCollection
        'Dim rsDdo As New DataSet
        'Dim StrAuX As String
        'Dim Cmp As New CBU291.CmeFunCmu
        'Dim bFlgFncOra As Boolean

        'Try
        '    'If IndErrAcoCmc = 0 Then
        '    'Criando a tabela no dataSet
        '    rsDdo.Tables.Add("IteOcd")

        '    'adiciona colunas
        '    With rsDdo.Tables("IteOcd").Columns

        '        .Add("CodAcoCmc", GetType(Integer))
        '        .Add("NumLnh", GetType(Integer))
        '        .Add("DesIteOcd", GetType(String))
        '        .Add("QdeIteOcd", GetType(Integer))
        '        .Add("VlrUntIte", GetType(Decimal))
        '        .Add("DatUtz", GetType(String))
        '        .Add("FrmUtzIte", GetType(String))
        '        .Add("CodOpeFsc", GetType(Integer))
        '        .Add("CodIte", GetType(Integer))
        '        .Add("CodUndNgc", GetType(Integer))
        '        .Add("CodFilEmp", GetType(Integer))
        '        .Add("QdeDiaCmpIteAcoCmc", GetType(Integer))
        '        .Add("CodCenCst", GetType(Integer))
        '        .Add("NumCgcEmpFrn", GetType(String)) '
        '        .Add("IdtEmpFrnIteRsu", GetType(Integer)) '
        '        .Add("IdtLclEmpFrnIteRsu", GetType(Integer)) '
        '        .Add("IdtTerPgtIteRsu", GetType(Integer)) '
        '        .Add("DesObsCpr", GetType(String)) '17
        '        .Add("CodIdtTsc", GetType(String)) '18
        '        .Add("NumCttFrn", GetType(Integer)) '19 'NUMERO DO CONTRATO DE FORNECIMENTO
        '        .Add("IdtCntCtb", GetType(Integer)) '20
        '        .Add("IndIteReqRsuVlrMnm", GetType(Integer)) '21
        '        .Add("IdtIteRsu", GetType(Decimal)) '22 'IDENTIFICACAO ITEM RESSUPRIMENTO
        '        .Add("NumLnhCttRsu", GetType(Decimal)) '23 'NUMERO LINHA CONTRATO RESSUPRIMENTO
        '        .Add("DesCttRsu", GetType(String)) '24 'DESCRICAO CONTRATO RESSUPRIMENTO
        '        .Add("vlrCli", GetType(Decimal)) 'add coluna no dataSet referente à participacao de cliente. Criado por Daniel Souza(Ética Soluções Integrais)21/05/2007. 28º Inclusao.
        '        'Rodrigo 13/06/2007
        '        .Add("IndIteRsuFrnUnc", GetType(Integer))
        '        .Add("IndIteRsuArzPrvPgt", GetType(Integer))
        '        'Fim Rodrigo 13/09/2007
        '        .Add("vlrFrn", GetType(Decimal)) 'add coluna no dataSet referente à participacao de fornecedor. Criado por Daniel Souza(Ética Soluções Integrais)21/05/2007. 28º Inclusao.
        '        'Rodrigo 06/09/2007
        '        .Add("CodStaIteRsu", GetType(Integer))
        '        .Add("VlrUtz", GetType(Decimal))
        '        'Fim Rodrigo 06/09/2007
        '    End With

        '    cont = TabIteOcdAcoCmc.Rows.Count

        '    If cont = 1 Then
        '        Exit Function
        '    End If

        '    While aux < cont

        '        'código da ação
        '        objDdo = Integer.Parse(txtCodAcoCmc.Text)
        '        arrCols(0) = objDdo

        '        'número da linha
        '        objDdo = Integer.Parse(aux)
        '        arrCols(1) = objDdo

        '        'descrição do item do orçamento
        '        str = TabIteOcdAcoCmc.Item(aux, "IteOcd")
        '        StrAuX = Mid(str.Split("-")(1).Trim, 1, 49)
        '        '
        '        objDdo = Cmp.DelAcaTxt(StrAuX)
        '        arrCols(2) = objDdo

        '        'quantidade dos itens do orçamento
        '        objDdo = Integer.Parse(TabIteOcdAcoCmc.Item(aux, "QdeIteOcd"))
        '        arrCols(3) = objDdo

        '        'Valor unitário do item
        '        'Rodrigo Veloso 10/09/2007
        '        'objDdo = TabIteOcdAcoCmc.Item(aux, "VlrUnd")
        '        If Integer.Parse(TabIteOcdAcoCmc.Item(aux, "IndTipOpe")) = 1 Then
        '            objDdo = Convert.ToDecimal(TabIteOcdAcoCmc.Item(aux, "VlrUnd")).ToString("N2")
        '        Else
        '            objDdo = TabIteOcdAcoCmc.Item(aux, "VlrUnd")
        '        End If
        '        'Fim Rodrigo Veloso 10/09/2007
        '        arrCols(4) = objDdo

        '        'valor participacao cliente
        '        objDdo = TabIteOcdAcoCmc.Item(aux, "vlrCli")
        '        arrCols(25) = objDdo

        '        'Data de utilização
        '        If Not TabIteOcdAcoCmc.Item(aux, "datUtz") = "" Then
        '            dat = Format(CType(TabIteOcdAcoCmc.Item(aux, "datUtz"), Date), "d")
        '        Else
        '            dat = String.Empty
        '        End If

        '        If dat <> "" Then
        '            objDdo = dat.Split("/")(2) & "-" & dat.Split("/")(1) & "-" & dat.Split("/")(0)
        '        Else
        '            objDdo = "0001-01-01"
        '        End If
        '        arrCols(5) = objDdo

        '        'Forma de utilização
        '        objDdo = TabIteOcdAcoCmc.Item(aux, "FrmUtz").ToString
        '        arrCols(6) = objDdo

        '        'Código da operação
        '        objDdo = TabIteOcdAcoCmc.Item(aux, "TipOpe").split("-")(0)
        '        arrCols(7) = objDdo

        '        'código Item do orçamento
        '        str = TabIteOcdAcoCmc.Item(aux, "IteOcd")
        '        objDdo = Integer.Parse(str.Split("-")(0))
        '        arrCols(8) = objDdo

        '        'código da unidade de negócio
        '        str = TabIteOcdAcoCmc.Item(aux, "CodUndNgc")
        '        If str <> "" Then
        '            objDdo = str.Split("-")(0)
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(9) = objDdo

        '        'código da filial da empresa 
        '        str = TabIteOcdAcoCmc.Item(aux, "CodFilEmp")
        '        If str <> "" Then
        '            objDdo = str.Split("-")(0)
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(10) = objDdo

        '        'Quantidade de dias para a compra
        '        str = TabIteOcdAcoCmc.Item(aux, "QdeDiaCmpIteAcoCmc")
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(11) = objDdo

        '        'Codigo de centro de custo
        '        str = TabIteOcdAcoCmc.Item(aux, "CodCenCst")
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(12) = objDdo

        '        'Codigo da filial
        '        str = TabIteOcdAcoCmc.Item(aux, "CodFilCgcCpf") 'NumCgcEmpFrn
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(13) = objDdo

        '        'Codigo da empresa fornecedora
        '        str = TabIteOcdAcoCmc.Item(aux, "IdtEmpFrn") 'IdtEmpFrnIteRsu
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(14) = objDdo

        '        'Codigo de centro de custo
        '        str = TabIteOcdAcoCmc.Item(aux, "IdtLclEmpFrn") 'IdtLclEmpFrnIteRsu
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(15) = objDdo

        '        'Codigo da Condição de Pagamento
        '        str = TabIteOcdAcoCmc.Item(aux, "IdtTerPgt") 'IdtTerPgtIteRsu
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(16) = objDdo

        '        'Observação ao comprador de suprimentos
        '        str = TabIteOcdAcoCmc.Item(aux, "DesObsCpr") 'DesObsCpr
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = " "
        '        End If
        '        arrCols(17) = objDdo

        '        'CodIdtTsc
        '        str = TabIteOcdAcoCmc.Item(aux, "CodIdtTsc") 'CodIdtTsc
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(18) = objDdo

        '        'NumCttFrn
        '        str = TabIteOcdAcoCmc.Item(aux, "NumCttFrn") 'NumCttFrn
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(19) = objDdo

        '        'Combinação Contabil para requisição de bens de serviço
        '        'Rodrigo 06/09/2007
        '        'arrCols(20) = GrcIdtIteRsu(TabIteOcdAcoCmc.Item(aux, "CodCenCst"))
        '        If TabIteOcdAcoCmc.Item(aux, "IndGrcReqAutIteRsu") = 1 Then
        '            arrCols(20) = GrcIdtIteRsu(TabIteOcdAcoCmc.Item(aux, "CodCenCst"))

        '            If Not bFlgFncOra Then
        '                If Cmp Is Nothing Then Cmp = New CBU291.CmeFunCmu

        '                If Not Cmp.CnsUsrOra(FmrCBUPcp.CodUsrRde) Then
        '                    MessageBox.Show("Usuário não está cadastrado no iPROC. Contate o analista!", _
        '                                    "Item do orçamento", _
        '                                    MessageBoxButtons.OK)
        '                    Return True
        '                Else
        '                    bFlgFncOra = True
        '                End If
        '            End If
        '        Else
        '            arrCols(20) = 0
        '        End If
        '        'Fim Rodrigo

        '        'IndIteReqRsuVlrMnm
        '        str = TabIteOcdAcoCmc.Item(aux, "IndIteReqRsuVlrMnm") 'IndIteReqRsuVlrMnm
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(21) = objDdo

        '        'IdtIteRsu
        '        str = TabIteOcdAcoCmc.Item(aux, "IdtIteRsu") 'IdtIteRsu
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(22) = objDdo

        '        'NumLnhCttRsu
        '        str = TabIteOcdAcoCmc.Item(aux, "NumLnhCttRsu") 'NumLnhCttRsu
        '        '
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(23) = objDdo

        '        'DesCttRsu
        '        str = TabIteOcdAcoCmc.Item(aux, "DesCttRsu") 'DesCttRsu
        '        If str <> "" Then
        '            objDdo = str
        '        Else
        '            objDdo = 0
        '        End If
        '        arrCols(24) = objDdo


        '        'valor participacao cliente. Criado por Daniel Souza(Ética Soluções Integrais)21/05/2007. 29º Inclusao
        '        objDdo = TabIteOcdAcoCmc.Item(aux, "vlrCli")
        '        arrCols(25) = objDdo

        '        'Rodrigo 13/09/2007
        '        objDdo = TabIteOcdAcoCmc.Item(aux, "IndIteRsuFrnUnc")
        '        If objDdo Is Nothing Then
        '            objDdo = 0
        '        End If
        '        arrCols(26) = objDdo

        '        objDdo = TabIteOcdAcoCmc.Item(aux, "IndIteRsuArzPrvPgt")
        '        If objDdo Is Nothing Then
        '            objDdo = 0
        '        End If
        '        arrCols(27) = objDdo
        '        'Fim Rodrigo 13/09/2007

        '        'valor participacao fornecedor. Criado por Daniel Souza(Ética Soluções Integrais)21/05/2007. 29º Inclusao
        '        objDdo = TabIteOcdAcoCmc.Item(aux, "vlrFrn")
        '        arrCols(28) = objDdo

        '        'Rodrigo 06/09/2007
        '        objDdo = TabIteOcdAcoCmc.Item(aux, "CodStaIteRsu")
        '        arrCols(29) = objDdo
        '        'Fim Rodrigo 06/09/2007

        '        'Valor Utilizado
        '        objDdo = CType(TabIteOcdAcoCmc.Item(aux, "VlrUtz"), Decimal)
        '        arrCols(30) = objDdo

        '        'Adiciona a linha ao DataRowCollection
        '        DataRow = rsDdo.Tables("ITeOcd").Rows
        '        DataRow.Add(arrCols)

        '        'para aceitar as mudanças
        '        rsDdo.Tables("IteOcd").AcceptChanges()

        '        aux += 1
        '    End While

        '    'insere dados dos itens da ação
        '    str = rsDdo.GetXml
        '    LstPmt(0) = Integer.Parse(txtCodAcoCmc.Text)
        '    LstPmt(1) = str.ToString
        '    '
        '    'Rodrigo 16/07/2007
        '    If Not IsNothing(dsDdoItem) Then
        '        LstPmt(2) = Me.dsDdoItem.GetXml()
        '    Else
        '        LstPmt(2) = ""
        '    End If
        '    'Fim Rodrigo 
        '    '
        '    If IsNothing(oSvcRdeRmt) Then oSvcRdeRmt = New wsSvcRmtDK.SvcRmt 'Rodrigo
        '    rsl = oSvcRdeRmt.ChmAsbRmt("CBU301", "BO_CBUIteOcdAcoCmc", "InsDdoAcoCmcIteOcd", LstPmt)
        '    'End If
        'Catch ex As Exception
        '    IndErrAcoCmc = 1
        '    'ATuDdoAcoOcoErr() 
        '    LibSldRsv()
        '    MessageBox.Show("Erro ao gravar os itens da ação!" & vbNewLine & ex.Message, "InsDdoIteOcd", MessageBoxButtons.OK)

        '    Dim oExp As New Exception("Ações Mercadológicas - Funcao: CBU291.FmrAcoCmc.InsDdoIteAcoCmc " & vbNewLine & ex.ToString)
        '    oExp = Nothing
        'End Try

        Return False
    End Function

    Public Function InsDdoAcoCmcIteOcd(CodAco As Integer, StrDdo As String,
                                        StrDdoIteSbt As String)
        'ByVal CodAco As Integer, _
        'ByVal StrDdo As String, _
        'ByVal StrDdoIteSbt As String) As Integer

        'Dim oObeCnsDdo As New DB_CBUIteOcdAcoCmc
        Dim oObeCnx As List(Of VO.RelacaoAcaoComercialPorItemOrcamento) = Nothing
        Dim int As Integer
        ''
        Try

            '    'Instancia a Conexão
            '    oObeCnx = New IAU013.UO_IAUCnxAcsDdo("DB001", "AIADM")
            '    oObeCnx.IniTsc()

            '    'deleta todos os itens da ação
            '    int = oObeCnsDdo.DelDdoAcoCmcIte(CodAco, _
            '                                     oObeCnx)
            int = boItemOrcamento.DelDdoAcoCmcIte(CodAco)

            '    'insere itens da ação
            '    InsDdoAcoCmcIteOcd = oObeCnsDdo.InsDdoAcoCmcIteOcd(StrDdo, oObeCnx)
            InsDdoAcoCmcIteOcd = boItemOrcamento.InsDdoAcoCmcIteOcd(StrDdo, oObeCnx)

            '    AtuIteSubAco(CodAco, _
            '                 StrDdoIteSbt, _
            '                 oObeCnx, _
            '                 oObeCnsDdo)

            AtuIteSubAco(CodAco, StrDdoIteSbt)

            '    oObeCnx.FimTscSuc()
            '    '
        Catch oEcc As Exception
            Throw New MappedWarnException("Funcao: InsDdoAcoCmcIteOcd " & oEcc.Message)
            'Publish(oExp)
            'oObeCnx.FimTscErr()
            'Throw oExp
        Finally
            'If Not IsNothing(oObeCnx) Then oObeCnx.Dispose()
        End Try

    End Function

    Public Function AtuIteSubAco(ByVal CodAcoCmc As Integer,
                                 ByVal sXmlDdoIteSbt As String) As Integer

        Dim CodMer As Integer
        Dim NumLnhIteOcdAcoCmc As Integer
        Dim CodMerSbt As Integer
        Dim QdeIteOcdCmc As Integer
        Dim VlrUntIteOcdAcoCmc As Double

        'Optional ByVal oObeCnx As IAU013.UO_IAUCnxAcsDdo = Nothing, _
        'Optional ByVal oObeCnsDdo As DB_CBUIteOcdAcoCmc = Nothing) As Integer
        ''
        'Dim oObeLetCsn As System.IO.StringReader
        'Dim dsIteSbt As DataSet
        'Dim bFlgTransacao As Boolean
        ''
        Try
            If sXmlDdoIteSbt <> "" Then
                '        If IsNothing(oObeCnx) Then
                '            oObeCnx = New IAU013.UO_IAUCnxAcsDdo("DB001", "AIADM")
                '            oObeCnx.IniTsc()
                '            bFlgTransacao = True
                '        End If
                '        '
                '        If IsNothing(oObeCnsDdo) Then
                '            oObeCnsDdo = New DB_CBUIteOcdAcoCmc
                '        End If
                '        '
                '        If IsNothing(oObeLetCsn) Then
                '            oObeLetCsn = New System.IO.StringReader(sXmlDdoIteSbt)
                '            dsIteSbt = New DataSet
                '            dsIteSbt.ReadXml(oObeLetCsn)
                '        End If
                '        '
                '        Me.DelIteSubAco(CodAcoCmc, _
                '                        0, _
                '                        0, _
                '                        oObeCnx, _
                '                        oObeCnsDdo)
                boItemOrcamento.DelIteSubAco(CodAcoCmc, 0, 0)
                '        '
                '        If Not IsNothing(dsIteSbt) Then
                '            If dsIteSbt.Tables.Count > 0 Then
                '                '
                '                For Each drIteSbt As DataRow In dsIteSbt.Tables(0).Rows
                '                    '
                '                    With drIteSbt
                '                        '
                '                        Me.IsrIteSubAco(.Item("CodAcoCmc"), _
                '                                        .Item("NumLnhIteOcdAcoCmc"), _
                '                                        .Item("CodIteAco"), _
                '                                        .Item("Codigo"), _
                '                                        .Item("QdeIteOcdCmc"), _
                '                                        .Item("VlrUntIteOcdAcoCmc"), _
                '                                        oObeCnx, _
                '                                        oObeCnsDdo)
                boItemOrcamento.IsrIteSubAco(CodAcoCmc, NumLnhIteOcdAcoCmc, CodMer, CodMerSbt, QdeIteOcdCmc, VlrUntIteOcdAcoCmc)
                '                        '
                '                    End With
                '                    '
                '                Next
                '                '
                '            End If
                '        End If
                '        '
                '        If bFlgTransacao Then
                '            oObeCnx.FimTscSuc()
                '        End If
                '        '
            End If

        Catch oEcc As Exception
            Try
                'oObeCnx.FimTscErr()
            Catch ex As Exception

            End Try

            Throw New MappedWarnException("Funcao: AtuIteSubAco " & oEcc.Message)
            'Publish(oExp)
            'Throw oExp
        Finally
            'If Not IsNothing(oObeCnx) Then oObeCnx.Dispose()
            'If Not IsNothing(oObeCnsDdo) Then oObeCnsDdo = Nothing
        End Try
        Return False
    End Function

    Private Function GrcIdtIteRsu(ByVal CodCenCst As String) As Integer
        'Dim Str, StrAux As String
        'Dim TipEvt As String
        ''
        'Try
        '    If cbTipEvt.Text <> String.Empty Then
        '        TipEvt = BscDdoTipEvt(Trim(cbTipEvt.Text.Split("-")(0)))
        '    End If
        '    '
        '    '01.CODCNTCTB.CCUST.00.FLAL.000.00.000 = Formato (00.000000000.00000.00.0000.000.00.000)
        '    StrAux = "01." & TipEvt & "." & CodCenCst & ".00.0143.000.00.000" 'IdtCntCtb
        '    Str = FunVldGrpSgmCtb(StrAux)
        '    '
        '    If IsNumeric(Str) Then
        '        If CLng(Str) = 0 Then
        '            Throw New MappedWarnException("Não foi possível efetuar a validação do seguinte segmento contábil porque o mesmo retornou zero. Favor contactar o analista responsável.")
        '            Return 0
        '        Else
        '            Return CInt(Str)
        '        End If
        '    Else
        '        Throw New MappedWarnException("Não foi possível efetuar a validação do seguinte segmento contábil : " & StrAux & vbNewLine & vbNewLine & "Mensagem de erro : " & Str)
        '        Return 0
        '    End If
        '    '
        'Catch oEcc As Exception
        '    MsgBox(oEcc.ToString, _
        '        MsgBoxStyle.Critical, _
        '        Me.Text)
        'End Try
        Return False
    End Function

    Private Function FunVldGrpSgmCtb(ByVal GrpSgmCtb As String) As Object
        'Dim oSvcRdeRmt As wsSvcRmtDK.SvcRmt
        'Dim dtsAux As New DataSet
        'Dim ObjRetAux(0) As Object
        'Dim VarRetAux As Object
        ''
        'Try
        '    '
        '    ObjRetAux(0) = GrpSgmCtb
        '    '
        '    If IsNothing(oSvcRdeRmt) Then oSvcRdeRmt = New wsSvcRmtDK.SvcRmt 'Rodrigo
        '    VarRetAux = oSvcRdeRmt.ChmAsbRmt("cbu323", "BO_CBUCsn", "VldGrpSgmCtb", ObjRetAux)
        '    'dtsAux = oSvcRdeRmt.ChmAsbRmtGrpRgt("crb002", "BO_CBUCsn", "VldGrpSgmCtb", ObjRetAux)
        '    'VarRetAux = dtsAux.Tables(0).Rows(0).Item(0)

        '    If VarRetAux = "" Then
        '        Return 0
        '    ElseIf IsNumeric(VarRetAux) Then
        '        Return VarRetAux
        '    Else
        '        Return 0
        '    End If
        'Catch ex As Exception
        '    Return 0
        '    'MsgBox("Erro ao validar Grupo de seguimento contabil (GLB054)" & ex.ToString)
        'Finally
        '    If Not IsNothing(oSvcRdeRmt) Then oSvcRdeRmt.Dispose() 'Rodrigo
        'End Try
        Return False
    End Function

    Private Function AtuFluApvAcoCmc()
        Return False
    End Function

    Function VrfTipOpe(acaoComercial As VO.AcaoComercial)

        Dim indItePmc As Boolean = False
        'For Cont As Integer = 1 To (Me.TabIteOcdAcoCmc.Rows.Count - 1)
        For Cont As Integer = 1 To (acaoComercial.ItensOrcamento.Count - 1)
            If acaoComercial.ItensOrcamento(Cont).DESOPEFSCDSNACOCMC.Split("-")(0) = 2 Or acaoComercial.ItensOrcamento(Cont).DESOPEFSCDSNACOCMC.Split("-")(0) = 4 Then
                indItePmc = True
            End If
            'If (CType(Me.TabIteOcdAcoCmc.GetData(Cont, "TipOpe").Split("-")(0), Decimal) = 2) Or (CType(Me.TabIteOcdAcoCmc.GetData(Cont, "TipOpe").Split("-")(0), Decimal) = 4) Then
            '    indItePmc = True
            'End If
        Next
        Return indItePmc
    End Function

    Public Function InsDdoPol(acaoComercial As VO.AcaoComercial) As Integer
        Dim TipEvt As Integer
        Dim CodEvt As Integer
        Dim datRef As Integer
        Dim CodUnNgc As Integer
        Dim codUndNgc As Integer
        Dim aux As Integer
        Dim VlrTotAco As String
        Dim str As String
        Dim vlr As Decimal
        Dim data As Date
        'Dim rsDdoAux As DataSet
        Dim cnt As Integer
        Dim oRow As String = ""
        Dim CodFrn As Integer
        Dim i As Integer
        Dim rsDdo As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
        Dim VlrMaxAcoCmc As Decimal
        Dim IndErrAcoCmc As Decimal

        Try

            boPoliticas.DeletarDadoAcaoComercialPoliticas(acaoComercial.CODACOCMC)

            VlrTotAco = CalVlrTotAcoCmc(acaoComercial)
            VlrTotAco = VlrTotAco.Replace(".", ",")

            'Valor da ação superior ao valor máximo
            'busca valor máximo para as ações
            vlr = VlrMaxAcoCmc
            If Convert.ToDecimal(VlrTotAco) > vlr Then
                oRow = "1" + "|"
            End If

            Dim evento = boEvento.ConsultaDadosEventoPeloCodigo(acaoComercial.TIPEVTACOCMC)
            If evento.INDEVTADIACOCMC = 1 Then
                oRow = oRow + "2" + "|"
            End If

            'Despesas Martins adicionado 999999
            CodFrn = acaoComercial.Fornecedores(0).CODFRN   'TabFrnAcoCmc.GetData(1, "Frn").split("-")(0)
            If CodFrn = 999999 Then
                oRow = oRow + "3" + "|"
            End If

            ''Ações complementares
            If acaoComercial.INDACOCMCCPL = Constantes.SIM Then
                oRow = oRow + "5" + "|"
            End If

            'Valor do evento mensal negativo
            i = acaoComercial.PlanosDeMarketing.Count   'frmSldPlnMkt.TabPlnMkt.Rows.Count
            cnt = 0
            While cnt <= i - 1
                If Not (acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOMENSAL Is Nothing) Then
                    vlr = acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOMENSAL 'frmSldPlnMkt.TabPlnMkt.GetData(cnt, "RstMes") 'pega o valor para o BU
                    If vlr < 0 Then
                        oRow = oRow + "6" + "|"
                        cnt = i
                    End If
                End If
                cnt += 1
            End While

            'Valor do evento atual negativo
            cnt = 0
            While cnt <= i - 1
                If Not (acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOATUAL Is Nothing) Then
                    vlr = acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOATUAL 'frmSldPlnMkt.TabPlnMkt.GetData(cnt, "RstAtu")
                    If vlr < 0 Then
                        oRow = oRow + "7" + "|"
                        cnt = i
                    End If
                End If
                cnt += 1
            End While

            'Valor do evento anual negativo
            cnt = 0
            While cnt <= i - 1
                If Not (acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOATUAL Is Nothing) Then
                    vlr = acaoComercial.PlanosDeMarketing(cnt).VLRESULTADOANUAL 'frmSldPlnMkt.TabPlnMkt.GetData(cnt, "RstAnO")
                    If vlr < 0 Then
                        oRow = oRow + "8" + "|"
                        cnt = i
                    End If
                End If
                cnt += 1
            End While

            Dim VlrTot As Decimal
            Dim cod As String
            'Valor da taxa de administração menor que a estipulada
            cnt = 0
            i = acaoComercial.Fornecedores.Count 'TabFrnAcoCmc.Rows.Count  'número de linhas da tabela de fornecedores
            'percorre todas as linhas da tabela
            While cnt <= i - 1
                cod = acaoComercial.Fornecedores(cnt).CODFRN   'TabFrnAcoCmc.GetData(cnt, "Frn")
                'cod = cod.Split("-")(0)
                If Integer.Parse(cod) <> 999999 Then
                    vlr = acaoComercial.Fornecedores(cnt).VLRTOTTXAADMACOCMC  'TabFrnAcoCmc.GetData(cnt, "txaAdm") ' pega a taxa de administração que está na tabela
                    VlrTot = 0
                    'If Not (TabFrnAcoCmc.GetData(cnt, "ctt") Is Nothing) Then
                    If Not (acaoComercial.Fornecedores(cnt).VLRUTZCTTACOCMC Is Nothing) Then
                        'If Not (TabFrnAcoCmc.GetData(cnt, "ext") Is Nothing) Then
                        If Not (acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC Is Nothing) Then
                            VlrTot = (acaoComercial.Fornecedores(cnt).VLRUTZCTTACOCMC + acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC) * (acaoComercial.VLRTOTTXAADMACOCMC / 100) '(TabFrnAcoCmc.GetData(cnt, "ctt") + TabFrnAcoCmc.GetData(cnt, "ext")) * (VlrTxaAdm / 100)
                        Else
                            VlrTot = (acaoComercial.Fornecedores(cnt).VLRUTZCTTACOCMC) * (acaoComercial.VLRTOTTXAADMACOCMC / 100) '(TabFrnAcoCmc.GetData(cnt, "ctt")) * (VlrTxaAdm / 100)
                        End If
                    Else
                        'If Not (TabFrnAcoCmc.GetData(cnt, "ext") Is Nothing) Then
                        If Not (acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC Is Nothing) Then
                            VlrTot = (acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC) * (acaoComercial.VLRTOTTXAADMACOCMC / 100) 'TabFrnAcoCmc.GetData(cnt, "ext") * (VlrTxaAdm / 100)
                        End If
                    End If
                    'If CDbl(vlr.Round(vlr, 2)) < CDbl(VlrTot.Round(VlrTot, 2)) Then
                    If CDbl(vlr) < CDbl(VlrTot) Then
                        oRow = oRow + "9" + "|"
                        cnt = i
                    End If
                    cnt += 1
                Else
                    cnt = i
                End If
            End While

            Dim CntAux As Integer = 1
            Dim VlrAux As Decimal
            Dim ind As Integer = 0


            'Valor da taxa de brinde menor que a estipulada
            cnt = 0
            i = acaoComercial.ItensOrcamento.Count  'TabIteOcdAcoCmc.Rows.Count  'número de linhas da tabela de itens
            'TabIteOcdAcoCmc.BeginInit()
            While cnt <= i - 1
                acaoComercial.ItensOrcamento(cnt).IndUtzIte = 0 'TabIteOcdAcoCmc.SetData(cnt, "IndUtzIte", 0)
                cnt += 1
            End While

            cnt = 0
            'percorre todas as linhas da tabela de itens 
            While cnt <= i - 1

                'verifico se é brinde 
                'If TabIteOcdAcoCmc.GetData(cnt, "IndBde") = 1 And TabIteOcdAcoCmc.GetData(cnt, "IndUtzIte") = 0 Then
                If acaoComercial.ItensOrcamento(cnt).INDBDEOPEDSNACOCMC = 1 And acaoComercial.ItensOrcamento(cnt).IndUtzIte = 0 Then

                    'TabIteOcdAcoCmc.SetData(cnt, "IndUtzIte", 1) 'marco item como utilizado
                    'acaoComercial.ItensOrcamento(cnt).VLRUNTITEOCDACOCMC
                    VlrTot = (acaoComercial.ItensOrcamento(cnt).VLRUNTITEOCDACOCMC * acaoComercial.ItensOrcamento(cnt).QDEITEOCDACOCMC)
                    VlrTot = VlrTot * (acaoComercial.VLRTOTBDEACOCMC / 100) ' pega a taxa de brinde que está na tabela
                    CodFrn = acaoComercial.ItensOrcamento(cnt).CODFRNPCPMER 'pega o código do fornecedor da mercadoria

                    i = cnt + 1
                    'While i < TabIteOcdAcoCmc.Rows.Count - 1 'conta a partir do primeiro brinde
                    While i < acaoComercial.ItensOrcamento.Count - 1 'conta a partir do primeiro brinde
                        ' verifico se o código item é brinde e se for  verifico se o código do fornecedor é igual
                        If acaoComercial.ItensOrcamento(i).INDBDEOPEDSNACOCMC = 1 Then
                            'If acaoComercial.ItensOrcamento(i).CODFRNPCPMER = CodFrn And acaoComercial.ItensOrcamento(i)., "IndUtzIte") = 0 Then
                            If acaoComercial.ItensOrcamento(i).CODFRNPCPMER = CodFrn And acaoComercial.ItensOrcamento(cnt).IndUtzIte = 0 Then
                                'TabIteOcdAcoCmc.SetData(i, "IndUtzIte", 1)
                                acaoComercial.ItensOrcamento(cnt).IndUtzIte = 1
                                VlrAux = ((acaoComercial.ItensOrcamento(i).VLRUNTITEOCDACOCMC) * (acaoComercial.ItensOrcamento(i).QDEITEOCDACOCMC))
                                VlrAux = VlrAux * (acaoComercial.VLRTOTBDEACOCMC / 100) ' pega a taxa de brinde que está na tabela
                                VlrTot = VlrTot + VlrAux
                            End If
                        End If
                        i += 1
                    End While

                    'percorre a tabela de fornecedores 
                    CntAux = 0
                    While CntAux <= acaoComercial.Fornecedores.Count - 1
                        'verifico se o fornecedor é igual ao da mercadoria
                        If CodFrn = acaoComercial.Fornecedores(CntAux).CODFRN Then
                            vlr = acaoComercial.Fornecedores(CntAux).VLRTXAADMBDEACOCMC
                            If CDbl(vlr) < CDbl(VlrTot) Then
                                oRow = oRow + "10" + "|"
                                CntAux = acaoComercial.Fornecedores.Count
                            End If
                        End If
                        CntAux += 1
                    End While
                End If
                cnt += 1
            End While

            'PAGAMENTOS DIVERSOS
            cnt = 0
            i = acaoComercial.ItensOrcamento.Count   'TabIteOcdAcoCmc.Rows.Count  'número de linhas da tabela de itens
            'percorre todas as linhas da tabela de itens 
            While cnt <= i - 1
                str = acaoComercial.ItensOrcamento(cnt).DESOPEFSCDSNACOCMC.Trim()  'Trim(TabIteOcdAcoCmc.GetData(cnt, "TipOpe").Split("-")(1))
                If str.Equals("PAGAMENTOS DIVERSOS") = True Then
                    oRow = oRow + "14" + "|"
                    CntAux = i
                End If
                cnt += 1
            End While

            'VALOR DE ACORDO OU EXTRA ACORDO NAO ORCADO
            data = Now
            str = Format(data, "yyyy-MM-dd")
            datRef = Integer.Parse(str.Split("-")(0) & str.Split("-")(1))


            'busca dados do plano de marketing
            TipEvt = acaoComercial.TIPEVTACOCMC
            CodEvt = Integer.Parse(acaoComercial.EmpenhoExa(0).ToString().Split("-")(0))
            datRef = Integer.Parse(datRef)
            CodUnNgc = Integer.Parse(0)

            rsDdo = boRelacaoPlanoMarketing.BscDdoPlnMkt(TipEvt, CodEvt, datRef, CodUnNgc)
            i = acaoComercial.Fornecedores.Count - 1 'número de linhas de fornecedores

            'percorre as linhas do grid de fornecedores
            cnt = 0
            For cnt = 0 To i - 1
                codUndNgc = CInt(acaoComercial.Fornecedores(cnt).CODUNDESRNGC)
                CntAux = rsDdo.Count - 1
                aux = 0
                While aux < CntAux
                    If codUndNgc = rsDdo.Item(aux).CODUNDESRNGC Then
                        'verifico se foi digitado valor de contrato
                        If Not (acaoComercial.Fornecedores(cnt).VLRUTZCTTACOCMC Is Nothing) Then
                            If acaoComercial.Fornecedores(cnt).VLRUTZCTTACOCMC > 0 Then
                                If rsDdo.Item(aux).VLRSLDATUPLNMKT = 0 Then '("VlrOcdCttPlnMkt") 
                                    oRow = oRow + "13" + "|"
                                    aux = CntAux
                                    cnt = i
                                End If

                            End If
                        End If
                    End If
                    aux += 1
                End While
            Next

            cnt = 0
            For cnt = 0 To i - 1
                codUndNgc = acaoComercial.Fornecedores(cnt).CODUNDESRNGC
                CntAux = rsDdo.Count - 1
                aux = 0
                While aux <= CntAux
                    If codUndNgc = rsDdo.Item(aux).CODUNDESRNGC Then
                        'verifico se foi digitado valor de contrato
                        If Not (acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC Is Nothing) Then
                            If acaoComercial.Fornecedores(cnt).VLRUTZEXACTTACOCMC > 0 Then
                                If rsDdo.Item(aux).VLRSLDMNSPLNMKT = 0 Then '("VlrOcdExaCttPlnMkt")
                                    oRow = oRow + "12" + "|"
                                    aux = CntAux
                                    cnt = i
                                End If
                            End If
                        End If
                    End If
                    aux += 1
                End While
            Next

            'Verifica se há algum código de política infringida
            If oRow <> "" Then
                'Insere política infringida
                aux = InsDdoAcoCmcPol(acaoComercial, oRow)
            End If

            Return aux

        Catch ex As Exception
            IndErrAcoCmc = 1
            Throw New MappedWarnException("Erro ao gravar os dados da ação!" & acaoComercial.CODACOCMC & ex.Message)
        Finally
        End Try


    End Function

    Function InsDdoAcoCmcPol(acaoComercial As VO.AcaoComercial, oRow As String) As Integer

        Dim corrows As String() = oRow.Split("|")

        For Each codrow In corrows
            If (Not IsNothing(acaoComercial.CODACOCMC) AndAlso acaoComercial.CODACOCMC > 0 AndAlso
               Not String.IsNullOrWhiteSpace(codrow)) Then
                InsDdoAcoCmcPol = boPoliticas.DeletarDadoAcaoComercialPoliticas(acaoComercial.CODACOCMC)
                InsDdoAcoCmcPol = boPoliticas.InsereDadosPoliticaAcao(acaoComercial.CODACOCMC, Integer.Parse(codrow))
            End If
        Next

        Return InsDdoAcoCmcPol


    End Function

    Function CalVlrTotAcoCmc(acaoComercial As VO.AcaoComercial) As String

        Dim VlrTot As Double = 0
        Dim Str As String = ""

        Try
            If Not IsDBNull(acaoComercial.ValorParticipacao) And acaoComercial.ValorParticipacao <> 0 Then
                VlrTot = Convert.ToDecimal(acaoComercial.ValorParticipacao)
            End If

            If Not IsDBNull(acaoComercial.VLRTOTTXAADMACOCMC) Then
                VlrTot = VlrTot + acaoComercial.VLRTOTTXAADMACOCMC
            End If

            If Not IsDBNull(acaoComercial.VLRTOTBDEACOCMC) Then
                VlrTot = VlrTot + acaoComercial.VLRTOTBDEACOCMC
            End If
            Str = Convert.ToString(VlrTot)

            Return Str

        Catch ex As Exception
            Throw New MappedWarnException("CalVlrTotAcoCmc" & Str & ex.Message)
        End Try

    End Function

    Public Function CnsSldRsvAcoCmc(ByVal CodAcoCmc As Integer) As List(Of VO.RelacaoAcaoComercialxFornecedores)
        Return DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).CnsSldRsvAcoCmc(CodAcoCmc)
    End Function


    ''' <summary>
    ''' Busca os fornecedores da acao durante o fluxo
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <param name="codStatusAcao"></param>
    ''' <param name="codBnfExaCtt"></param>
    ''' <returns></returns>
    Public Function BuscarFornecedoresDaAcaoFluxo(idAcao As Decimal?, Optional codStatusAcao As Decimal? = Nothing, Optional codBnfExaCtt As Decimal? = Nothing) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Try
            Dim daoFornecedor As RelacaoAcaoComercialxFornecedoresDAO = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO)
            Dim listaRelacaoFornecedores = DirectCast(daoBase, RelacaoAcaoComercialxFornecedoresDAO).BuscarFornecedoresDaAcaoFluxo(idAcao)


            Return listaRelacaoFornecedores
        Catch
            Throw
        End Try

    End Function

End Class
