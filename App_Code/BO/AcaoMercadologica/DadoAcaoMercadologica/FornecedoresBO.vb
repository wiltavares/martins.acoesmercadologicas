﻿Imports Core
Public Class FornecedoresBO
    Inherits BOBase(Of VO.Fornecedores)

    Public Sub New()
        MyBase.daoBase = New FornecedoresDAO()
    End Sub

    Public Function BuscarFornecedorPorCodigo(codFrn) As VO.Fornecedores

        Dim fornecedor As New VO.Fornecedores
        fornecedor.CODFRN = codFrn

        Return DirectCast(daoBase, FornecedoresDAO).SelectT(fornecedor)

    End Function

    Public Function BuscarNomeCodigoFornecedorPorCodigo(codFrn) As VO.Fornecedores

        Return DirectCast(daoBase, FornecedoresDAO).BuscarNomeCodigoFornecedores(codFrn).FirstOrDefault()

    End Function

    Public Function BuscarNomeCodigoFornecedores(Optional codFrn As Decimal? = Nothing) As List(Of VO.Fornecedores)

        Return DirectCast(daoBase, FornecedoresDAO).BuscarNomeCodigoFornecedores(codFrn)

    End Function
End Class
