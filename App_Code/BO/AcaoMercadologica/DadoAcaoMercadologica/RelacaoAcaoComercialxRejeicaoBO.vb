﻿Imports Core
Public Class RelacaoAcaoComercialxRejeicaoBO
    Inherits BOBase(Of VO.RelacaoAcaoComercialxRejeicao)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoComercialxRejeicaoDAO()
    End Sub
    Public Function FunInsDdoRpvAcoCmc(ByVal dCodAcoCmc As Integer, _
                                   ByVal iStaAcoCmc As Integer, _
                                   ByVal dCodFncRpvAcoCmc As Integer, _
                                   ByVal sDesMtvRpvAcoCmc As String) As Integer
        Return DirectCast(daoBase, RelacaoAcaoComercialxRejeicaoDAO).FunInsDdoRpvAcoCmc(dCodAcoCmc, iStaAcoCmc, dCodFncRpvAcoCmc, sDesMtvRpvAcoCmc)

    End Function
End Class
