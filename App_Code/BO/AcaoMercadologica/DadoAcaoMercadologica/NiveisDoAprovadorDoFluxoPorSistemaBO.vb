﻿Imports Core
Public Class NiveisDoAprovadorDoFluxoPorSistemaBO
    Inherits BOBase(Of VO.NiveisDoAprovadorDoFluxoPorSistema)

    Public Sub New()
        MyBase.daoBase = New NiveisDoAprovadorDoFluxoPorSistemaDAO()
    End Sub

    Public Function CnsApvNot(ByVal CodSisInf As Decimal) As List(Of VO.NiveisDoAprovadorDoFluxoPorSistema)

        Return DirectCast(daoBase, NiveisDoAprovadorDoFluxoPorSistemaDAO).CnsApvNot(CodSisInf)

    End Function



End Class
