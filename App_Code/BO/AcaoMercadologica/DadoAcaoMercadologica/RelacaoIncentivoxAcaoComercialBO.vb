﻿Imports Core
Public Class RelacaoIncentivoxAcaoComercialBO
    Inherits BOBase(Of VO.RelacaoIncentivoxAcaoComercial)

    Public Sub New()
        MyBase.daoBase = New RelacaoIncentivoxAcaoComercialDAO()
    End Sub

    Public Function BuscarIncentivos(Optional codAcao As Integer? = Nothing, Optional codIct As Integer? = Nothing, Optional nomIct As String = Nothing) As List(Of VO.RelacaoIncentivoxAcaoComercial)

        Return DirectCast(daoBase, RelacaoIncentivoxAcaoComercialDAO).BuscarIncentivos(codAcao, codIct, nomIct)

    End Function

    ''' <summary>
    ''' IsrGrpIctAcoCmc
    ''' Insere grupo de incentivos e exclui os incentivos anteriores da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <param name="codGrupoIct"></param>
    Public Sub InserirGrupoDeIncentivo(codAcao As Integer, codGrupoIct As String)

        DeletarIncentivoDaAcao(codAcao)

        Dim incentivos = codGrupoIct.Split(","c)
        For Each codIncentivo As Integer In incentivos
            daoBase.InsertT(New VO.RelacaoIncentivoxAcaoComercial With {.CODACOCMC = codAcao, .CODICT = codIncentivo})
        Next

    End Sub

    ''' <summary>
    ''' IsrGrpIctAcoCmc
    ''' Insere grupo de incentivos e exclui os incentivos anteriores da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <param name="listaIncentivos"></param>
    Public Sub InserirGrupoDeIncentivo(codAcao As Integer, listaIncentivos As List(Of VO.RelacaoIncentivoxAcaoComercial))

        For Each incentivo As VO.RelacaoIncentivoxAcaoComercial In listaIncentivos
            incentivo.CODACOCMC = codAcao
            daoBase.InsertT(incentivo)
        Next

    End Sub

    Public Sub DeletarIncentivoDaAcao(codAcao As Integer)

        DirectCast(daoBase, RelacaoIncentivoxAcaoComercialDAO).DeletarIncentivoDaAcao(codAcao)

    End Sub

    Public Function BuscarIncentivosDaAcao(ByVal codAcao As Decimal) As List(Of VO.RelacaoIncentivoxAcaoComercial)

        Return DirectCast(daoBase, RelacaoIncentivoxAcaoComercialDAO).BuscarIncentivosDaAcao(codAcao)

    End Function


End Class
