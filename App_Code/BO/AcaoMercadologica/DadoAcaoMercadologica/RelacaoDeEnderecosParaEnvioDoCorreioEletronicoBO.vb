﻿Imports Core
Public Class RelacaoDeEnderecosParaEnvioDoCorreioEletronicoBO
    Inherits BOBase(Of VO.RelacaoDeEnderecosParaEnvioDoCorreioEletronico)

    Public Sub New()
        MyBase.daoBase = New RelacaoDeEnderecosParaEnvioDoCorreioEletronicoDAO()
    End Sub

    Public Sub IsrEndCreEtn(ByVal TipMsgCreEtn As Decimal, ByVal NumSeqMsgCreEtn As Decimal, ByVal TipEndCreEtn As Decimal, ByVal NumSeqEndCreEtn As Decimal, ByVal IdtEndCreEtn As String)
        Dim item As New VO.RelacaoDeEnderecosParaEnvioDoCorreioEletronico

        item.TIPMSGCREETN = TipMsgCreEtn
        item.NUMSEQMSGCREETN = NumSeqMsgCreEtn
        item.TIPENDCREETN = TipEndCreEtn
        item.NUMSEQENDCREETN = NumSeqEndCreEtn
        'TODO: tirar meu e-mail e colocar o que vem do parametro
        item.IDTENDCREETN = IdtEndCreEtn
        'item.IDTENDCREETN = "victor.rabelo@interacaotecnologia.com.br"

        Try
            DirectCast(daoBase, RelacaoDeEnderecosParaEnvioDoCorreioEletronicoDAO).IsrEndCreEtn(item)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
