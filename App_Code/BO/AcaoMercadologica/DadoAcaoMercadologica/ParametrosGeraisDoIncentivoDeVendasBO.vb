﻿Imports Core
Public Class ParametrosGeraisDoIncentivoDeVendasBO
    Inherits BOBase(Of VO.ParametrosGeraisDoIncentivoDeVendas)

    Public Sub New()
        MyBase.daoBase = New ParametrosGeraisDoIncentivoDeVendasDAO()
    End Sub

    Public Function BuscarIncentivos(Optional codIct As Integer? = Nothing, Optional nomIct As String = Nothing) As List(Of VO.ParametrosGeraisDoIncentivoDeVendas)

        Return DirectCast(daoBase, ParametrosGeraisDoIncentivoDeVendasDAO).BuscarIncentivos(codIct, nomIct)

    End Function

    Public Function BuscarValidadoresIcentivos(ByVal codIct As Integer) As List(Of VO.ParametrosGeraisDoIncentivoDeVendas)

        Return DirectCast(daoBase, ParametrosGeraisDoIncentivoDeVendasDAO).BuscarValidadoresIcentivos(codIct)

    End Function


End Class
