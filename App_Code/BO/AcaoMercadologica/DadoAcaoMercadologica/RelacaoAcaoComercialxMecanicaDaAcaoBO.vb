﻿Imports Core
Public Class RelacaoAcaoComercialxMecanicaDaAcaoBO
    Inherits BOBase(Of VO.RelacaoAcaoComercialxMecanicaDaAcao)

    Public Sub New()
        MyBase.daoBase = New RelacaoAcaoComercialxMecanicaDaAcaoDAO()
    End Sub

    Public Function BuscarRegraMecanicaDaAcao(codAcao As Integer) As VO.RelacaoAcaoComercialxMecanicaDaAcao

        Return DirectCast(daoBase, RelacaoAcaoComercialxMecanicaDaAcaoDAO).BuscarRegraMecanicaDaAcao(codAcao)

    End Function

    ''' <summary>
    ''' InsDdoRgrAcoCmc
    ''' Deleta mecanica existente e insere uma nova (atualiza ou só insere)
    ''' </summary>
    ''' <param name="mecanica"></param>
    Public Sub InsereDadosMecanicaDaAcao(mecanica As VO.RelacaoAcaoComercialxMecanicaDaAcao)

        Dim daoBaseMecanica = DirectCast(daoBase, RelacaoAcaoComercialxMecanicaDaAcaoDAO)
        'Deleta dados da mecânica da ação 
        daoBaseMecanica.DeleteT(New VO.RelacaoAcaoComercialxMecanicaDaAcao With {.CODACOCMC = mecanica.CODACOCMC})
        'Insere dados da mecânica da ação 
        If mecanica.DESRGRACOCMC IsNot Nothing AndAlso mecanica.DESRGRACOCMC <> "" Then
            daoBaseMecanica.InsertT(mecanica)
        End If

    End Sub
End Class
