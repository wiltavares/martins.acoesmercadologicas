﻿Imports Microsoft.VisualBasic

Public Class MTS_RT_Orc_Budge_Actions_AllBO
    Inherits BOBase(Of VO.MTS_RT_Orc_Budge_Actions_All)

    Public Sub New()
        MyBase.daoBase = New MTS_RT_Orc_Budge_Actions_AllDAO()
    End Sub

    Public Function Insert(item As VO.MTS_RT_Orc_Budge_Actions_All) As VO.MTS_RT_Orc_Budge_Actions_All
        Return DirectCast(daoBase, MTS_RT_Orc_Budge_Actions_AllDAO).Insert(item)
    End Function
End Class
