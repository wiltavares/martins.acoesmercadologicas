﻿Imports Microsoft.VisualBasic

Public Class MTS_RT_Type_Expenses_AllBO

    Inherits BOBase(Of VO.MTS_RT_Type_Expenses_All)

    Public Sub New()
        MyBase.daoBase = New MTS_RT_Type_Expenses_AllDAO()
    End Sub

    Public Function SelectAttributes(vo As VO.MTS_RT_Type_Expenses_All) As VO.MTS_RT_Type_Expenses_All
        Return DirectCast(daoBase, MTS_RT_Type_Expenses_AllDAO).SelectAttributes(vo)
    End Function

End Class
