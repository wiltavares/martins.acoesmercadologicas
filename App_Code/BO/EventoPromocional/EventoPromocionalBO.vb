﻿Imports Core
Public Class EventoPromocionalBO
    Inherits BOBase(Of VO.EventoPromocional)

    Public Sub New()
        MyBase.daoBase = New EventoPromocionalDAO()
    End Sub

    Public Function ListEventoPromocional() As List(Of VO.EventoPromocional)
        Return DirectCast(daoBase, EventoPromocionalDAO).ListEventoPromocional()
    End Function

End Class
