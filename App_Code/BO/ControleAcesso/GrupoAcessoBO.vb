﻿Imports Core

Public Class GrupoAcessoBO
    Inherits BOBase(Of VO.GrupoAcesso)

    Public Sub New()
        MyBase.daoBase = New GrupoAcessoDAO()
    End Sub

    Public Function BuscaGrupoAcesso(vo As VO.GrupoAcesso) As List(Of VO.GrupoAcesso)
        Return DirectCast(daoBase, GrupoAcessoDAO).BuscaGrupoAcesso(vo)
    End Function

    Public Function ListGruposAcesso(vo As VO.GrupoAcesso) As List(Of VO.GrupoAcesso)
        Return DirectCast(daoBase, GrupoAcessoDAO).ListGruposAcesso(vo)
    End Function


    Public Overloads Function GetNextCODGRPACS() As Decimal
        Return CType(daoBase, GrupoAcessoDAO).GetNextCODGRPACS()
    End Function
End Class
