﻿Imports Core

Public Class ControleAcessoBO
    Inherits BOBase(Of VO.ControleAcesso)

    Public Sub New()
        MyBase.daoBase = New ControleAcessoDAO()
    End Sub

    Public Function ControleAcessoUsuarioGrupoAcesso(grupos As String) As List(Of VO.ControleAcesso)
        Return DirectCast(daoBase, ControleAcessoDAO).ControleAcessoUsuarioGrupoAcesso(grupos)
    End Function


    Public Function ListarAcessosGrupo(codGrupo As Decimal) As List(Of VO.ControleAcesso)
        Return DirectCast(daoBase, ControleAcessoDAO).ListarAcessosGrupo(codGrupo)
    End Function


    Public Sub SalvaDados(List As List(Of VO.ControleAcesso), CODGRPACS As Decimal)
        Try

            Dim ListAcessoOld As List(Of VO.ControleAcesso) = ListT(New VO.ControleAcesso With {.CODGRPACS = CODGRPACS, .NOMSISINF = "AI                  "})

            daoBase.connector.BeginTransaction()

            For Each item As VO.ControleAcesso In List
                item.NOMSISINF = "AI                  "
                If (ListAcessoOld.Where(Function(m) m.CODMNU.Trim = item.CODMNU.Trim).Count() = 0) Then
                    daoBase.InsertT(item)
                End If
            Next
            For Each itemOld As VO.ControleAcesso In ListAcessoOld
                itemOld.NOMSISINF = "AI                  "
                If (List.Where(Function(m) m.CODMNU.Trim = itemOld.CODMNU.Trim).Count() = 0) Then
                    daoBase.DeleteT(itemOld)
                End If
            Next

            daoBase.connector.Commit()

        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

    End Sub

    'Public Function CarregaAcesso(vo As VO.ControleAcesso) As List(Of VO.ControleAcesso)

    '    Return DirectCast(daoBase, ControleAcessoDAO).CarregaAcesso(vo)
    'End Function


End Class
