﻿Imports Core

Public Class RelacaoDeSistemasxMenuBO
    Inherits BOBase(Of VO.RelacaoDeSistemasxMenu)

    Public Sub New()
        MyBase.daoBase = New RelacaoDeSistemasxMenuDAO()
    End Sub

    Public Function FunCsnMnuPcp(vo As VO.RelacaoDeSistemasxMenu, codUser As Integer) As List(Of VO.RelacaoDeSistemasxMenu)
        Return DirectCast(daoBase, RelacaoDeSistemasxMenuDAO).FunCsnMnuPcp(vo, codUser)

    End Function

End Class
