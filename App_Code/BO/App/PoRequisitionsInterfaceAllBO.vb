﻿Imports System.Data.OleDb
Imports Core
Public Class PoRequisitionsInterfaceAllBO
    Inherits BOBase(Of VO.PoRequisitionsInterfaceAll)

    Public Sub New()
        MyBase.daoBase = New PoRequisitionsInterfaceAllDAO()
    End Sub


    Public Sub IsrReqCmpMatCsm(ByVal IdtUsrCad As Integer, ByVal CodAcao As String, ByVal IdtLclFrn As String, ByVal FlgIdtArzAtpPgt As String,
                             ByVal IdtTerPgt As String,
                             ByVal NumRmlSlt As String,
                             ByVal QdePedIteReq As Decimal,
                             ByVal VlrPco As Decimal,
                             ByVal IdtFncEmt As Integer,
                             ByVal DesObsCpr As String,
                             ByVal IdtIteRsu As Integer,
                             ByVal IdtCntCtbReq As Integer,
                             ByVal IdtOrzOri As Integer,
                             ByVal IdtLclEnt As Integer,
                             ByVal DatCrc As Date,
                              ByVal CodUndOpe As Integer,
                             ByVal IdtCttRsu As Integer,
                             ByVal NumLnhCttRsu As Integer)
        'Obtendo a Razão Social da Empresa Fornecedora 
        If Trim(IdtLclFrn) = "0" Or Trim(IdtLclFrn) = "" Then
            IdtLclFrn = Nothing
        Else
            IdtLclFrn = String.Format("{0}-{1}", IdtLclFrn, PsqFrnCsm(IdtLclFrn))
        End If
        'Obtendo a Descrição da Forma de Pagamento
        If Trim(IdtTerPgt) = "0" Or Trim(IdtTerPgt) = "" Then
            IdtTerPgt = Nothing
        Else
            Dim _ApTermsBO As New ApTermsBO
            Dim terns = _ApTermsBO.LstTerPgt(New VO.ApTermsVO() With {.TERM_ID = IdtTerPgt}).FirstOrDefault()

            IdtTerPgt = String.Format("{0}-{1}", terns.TERM_ID, terns.DESCRIPTION)
        End If

        Dim PoRequisitionsInterfaceAll As New VO.PoRequisitionsInterfaceAll

        PoRequisitionsInterfaceAll.CREATION_DATE = DateTime.Now
        PoRequisitionsInterfaceAll.CREATED_BY = IdtUsrCad
        PoRequisitionsInterfaceAll.INTERFACE_SOURCE_CODE = "AÇÕES COMERCIAIS"
        PoRequisitionsInterfaceAll.SOURCE_TYPE_CODE = "VENDOR"
        PoRequisitionsInterfaceAll.REQUISITION_TYPE = "PURCHASE"
        PoRequisitionsInterfaceAll.DESTINATION_TYPE_CODE = "EXPENSE"
        PoRequisitionsInterfaceAll.QUANTITY = QdePedIteReq
        PoRequisitionsInterfaceAll.UNIT_PRICE = VlrPco
        PoRequisitionsInterfaceAll.AUTHORIZATION_STATUS = "APPROVED"
        PoRequisitionsInterfaceAll.BATCH_ID = CodAcao
        PoRequisitionsInterfaceAll.PREPARER_ID = IdtFncEmt
        PoRequisitionsInterfaceAll.AUTOSOURCE_FLAG = IIf(IdtCttRsu <> 0, "Y", "N")
        PoRequisitionsInterfaceAll.HEADER_DESCRIPTION = String.Format("AÇÃO: {0}", CodAcao)
        PoRequisitionsInterfaceAll.HEADER_ATTRIBUTE1 = "AÇÕES COMERCIAIS"
        PoRequisitionsInterfaceAll.HEADER_ATTRIBUTE2 = CodAcao
        PoRequisitionsInterfaceAll.JUSTIFICATION = String.Format("Atendimento da Ação Mercadológica {0}", CodAcao)
        PoRequisitionsInterfaceAll.NOTE_TO_BUYER = DesObsCpr
        PoRequisitionsInterfaceAll.ITEM_ID = IdtIteRsu
        PoRequisitionsInterfaceAll.CHARGE_ACCOUNT_ID = IdtCntCtbReq
        PoRequisitionsInterfaceAll.LINE_TYPE_ID = 1
        PoRequisitionsInterfaceAll.SOURCE_ORGANIZATION_ID = IdtOrzOri
        PoRequisitionsInterfaceAll.DESTINATION_ORGANIZATION_ID = IdtOrzOri
        PoRequisitionsInterfaceAll.DELIVER_TO_LOCATION_ID = IdtLclEnt
        PoRequisitionsInterfaceAll.DELIVER_TO_REQUESTOR_ID = IdtOrzOri
        PoRequisitionsInterfaceAll.LINE_ATTRIBUTE1 = IdtLclFrn
        PoRequisitionsInterfaceAll.LINE_ATTRIBUTE2 = ""
        'PoRequisitionsInterfaceAll.LINE_ATTRIBUTE3 =
        PoRequisitionsInterfaceAll.LINE_ATTRIBUTE4 = FlgIdtArzAtpPgt
        PoRequisitionsInterfaceAll.LINE_ATTRIBUTE5 = IdtTerPgt
        PoRequisitionsInterfaceAll.LINE_ATTRIBUTE6 = NumRmlSlt
        PoRequisitionsInterfaceAll.NEED_BY_DATE = DatCrc
        PoRequisitionsInterfaceAll.VARIANCE_ACCOUNT_ID = IdtCntCtbReq
        PoRequisitionsInterfaceAll.CURRENCY_CODE = "BRL"
        PoRequisitionsInterfaceAll.AUTOSOURCE_DOC_HEADER_ID = IIf(IdtCttRsu = 0, Nothing, IdtCttRsu)
        PoRequisitionsInterfaceAll.AUTOSOURCE_DOC_LINE_NUM = IIf(NumLnhCttRsu = 0, Nothing, NumLnhCttRsu)
        PoRequisitionsInterfaceAll.DOCUMENT_TYPE_CODE = "REQUISITION"
        PoRequisitionsInterfaceAll.ORG_ID = CodUndOpe

        DirectCast(daoBase, PoRequisitionsInterfaceAllDAO).Insert(PoRequisitionsInterfaceAll)

    End Sub
    Private Function PsqFrnCsm(VENDORSITEID As Decimal) As String
        Dim _PoVendorSitesAllBO As New PoVendorSitesAllBO
        Return _PoVendorSitesAllBO.PsqFrnCsm(VENDORSITEID).RAZSOCEMPFRN

    End Function

End Class
