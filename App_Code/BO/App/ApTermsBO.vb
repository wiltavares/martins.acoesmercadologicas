﻿Imports System.Data.OleDb
Imports Core
Public Class ApTermsBO
    Inherits BOBase(Of VO.ApTermsVO)

    Public Sub New()
        MyBase.daoBase = New ApTermsDAO()
    End Sub


    Public Function LstTerPgt(filtro As VO.ApTermsVO) As List(Of VO.ApTermsVO)

        Return DirectCast(daoBase, ApTermsDAO).LstTerPgt(filtro)

    End Function

    Public Function SelecionarPorId(TERM_ID As Decimal) As VO.ApTermsVO

        Return DirectCast(daoBase, ApTermsDAO).SelecionarPorId(TERM_ID)

    End Function

End Class
