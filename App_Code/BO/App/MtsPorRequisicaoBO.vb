﻿Imports System.Data.OleDb
Imports Core
Public Class MtsPorRequisicaoBO
    Inherits BOBase(Of VO.MtsPorRequisicaoV)

    Public Sub New()
        MyBase.daoBase = New MtsPorRequisicaoVDAO()
    End Sub


    Public Function CnsIteRsu(codMercadoria As Integer, codAcao As Integer) As List(Of VO.MtsPorRequisicaoV)

        Return DirectCast(daoBase, MtsPorRequisicaoVDAO).CnsIteRsu(codMercadoria, codAcao)

    End Function
End Class
