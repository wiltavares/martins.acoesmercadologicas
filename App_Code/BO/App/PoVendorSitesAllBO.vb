﻿Imports System.Data.OleDb
Imports Core
Public Class PoVendorSitesAllBO
    Inherits BOBase(Of VO.PoVendorSitesAllVO)

    Public Sub New()
        MyBase.daoBase = New PoVendorSitesAllDAO()
    End Sub


    Public Function PsqFrnCsmUndNgc(codUndNgc As Decimal, Optional numCgc As Decimal? = Nothing, Optional razaoSocial As String = Nothing) As List(Of VO.PoVendorSitesAllVO)

        Return DirectCast(daoBase, PoVendorSitesAllDAO).PsqFrnCsmUndNgc(codUndNgc, numCgc, razaoSocial)

    End Function
    Public Function PsqFrnCsm(VENDORSITEID As Decimal) As VO.PoVendorSitesAllVO
        Return DirectCast(daoBase, PoVendorSitesAllDAO).PsqFrnCsm(VENDORSITEID)
    End Function

End Class
