﻿Imports System.Data.OleDb
Imports Core
Public Class perAllPeopleFBO
    Inherits BOBase(Of VO.PER_ALL_PEOPLE_F)

    Public Sub New()
        MyBase.daoBase = New perAllPeopleFDAO()
    End Sub


    Public Function PsqCadFnc(CodFnc As Decimal) As List(Of VO.PER_ALL_PEOPLE_F)

        Return DirectCast(daoBase, perAllPeopleFDAO).PsqCadFnc(CodFnc)

    End Function
    Public Function CsnIdtUsu(CodFnc As String) As Integer
        Return DirectCast(daoBase, perAllPeopleFDAO).CsnIdtUsu(CodFnc)
    End Function
End Class
