﻿Imports System.Web.Mvc
Imports AutoMapper
Imports Core

Public Class LogDataStageBO
    Inherits BOBase(Of VO.LogDataStage)

    Public Sub New()
        MyBase.daoBase = New LogDataStageDAO()
    End Sub


    Public Function ListaParaGrid(logDataStage As VO.LogDataStage) As List(Of VO.LogDataStage)
        Return DirectCast(daoBase, LogDataStageDAO).ListaParaGrid(logDataStage)
    End Function
End Class
