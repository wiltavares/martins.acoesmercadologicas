﻿Imports System.Data.OleDb
Imports Core
Imports System.Data

Public Class PlanoDeMarketingBO
    Inherits BOBase(Of VO.PlanoDeMarketing)

    Public Sub New()
        MyBase.daoBase = New PlanoDeMarketingDAO()
    End Sub

    Public Function ListPlanoDeMarketing(vo As VO.PlanoDeMarketing) As List(Of VO.PlanoDeMarketing)

        Return DirectCast(daoBase, PlanoDeMarketingDAO).ListPlanoDeMarketing(vo)

    End Function
    Public Function ListPlanoDeMarketingAno(vo As VO.PlanoDeMarketing) As List(Of VO.PlanoDeMarketing)

        Return DirectCast(daoBase, PlanoDeMarketingDAO).ListPlanoDeMarketingAno(vo)

    End Function

    ''' <summary>
    ''' CalSldPlnMktAcoCmc
    ''' Calcula saldo do plano de marketing
    ''' </summary>
    ''' <param name="tipoEvento"></param>
    ''' <param name="codEvento"></param>
    ''' <param name="dataRef"></param>
    ''' <param name="codBUs"></param>
    ''' <returns></returns>
    Public Function CalculaSaldoPlanoMarketingAcao(tipoEvento As Integer, codEvento As Integer, dataRef As Integer, codBUs As String) As List(Of VO.PlanoDeMarketing)

        Return DirectCast(daoBase, PlanoDeMarketingDAO).CalculaSaldoPlanoMarketingAcao(tipoEvento, codEvento, dataRef, codBUs)

    End Function


    Public Function ListaExcel(path As String) As DataTable
        Try
            Dim conStr As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + "; Extended Properties='Excel 8.0;'"

            Dim dataExcel As DataTable = Nothing
            Using oleExcelConn As OleDbConnection = New OleDbConnection(conStr)
                Dim extensao As String = path.Split(".").ToList.Last

                oleExcelConn.Open()

                Dim dtExcelSchema As DataTable
                dtExcelSchema = oleExcelConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim nomeFolha As String = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
                Dim oleCommand As String = (Convert.ToString("SELECT * From [") & nomeFolha) + "]"

                Using oleExcelCmd As New OleDbCommand(oleCommand, oleExcelConn)
                    dataExcel = New DataTable()
                    dataExcel.Load(oleExcelCmd.ExecuteReader())
                End Using
            End Using

            Return dataExcel
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ImportarExelPlanoDeMarketing(path As String) As List(Of String)
        Try

            Dim eventoBO As New EventoBO()
            Dim boTipoEvento As New TipoDeEventoBO()
            Dim boUnidadeEstrategica As New UnidadeEstrategicaDeNegocioBO()

            Dim dados As DataTable = ListaExcel(path)
            Dim erros As List(Of String) = New List(Of String)

            Dim tiposEvento As List(Of VO.TipoDeEvento) = boTipoEvento.ListT(New VO.TipoDeEvento())
            Dim unidade As List(Of VO.UnidadeEstrategicaDeNegocio) = boUnidadeEstrategica.ListT(New VO.UnidadeEstrategicaDeNegocio())
            Dim evento As List(Of VO.Evento) = eventoBO.ListT(New VO.Evento())

            If IsNothing(dados) Then
                erros.Add("Não foi possível abrir o arquivo. Verifique se ele já está aberto por outra aplicação.")
            Else
                For Each linha In dados.Rows

                    If (IsDBNull(linha(0))) Then 'verifica se é campo vazio, mesmo que ultimo registro
                        Exit For
                    End If


                    Dim novoPlano As VO.PlanoDeMarketing = New VO.PlanoDeMarketing With {.TIPEVTACOCMC = CDec(linha("tipo de evento")),
                    .CODEVTACOCMC = CDec(linha("evento")), .CODUNDESRNGC = CDec(linha("codigo unidade de negocio")),
                    .VLROCDCTTPLNMKT = CDec(linha("valor orcado contrato")),
                    .VLROCDEXACTTPLNMKT = CDec(linha("valor orcado extra contato")), .ANOMESREF = CDec(linha("dta de referencia")),
                    .VLROCDPLNMKT = (CDec(linha("valor orcado contrato") + CDec(linha("valor orcado extra contato")))),
                    .VLRRSVPLNMKT = 0}

                    Dim verificaTipoEvento As List(Of VO.TipoDeEvento) = tiposEvento.Where(Function(f) f.TIPEVTACOCMC = novoPlano.TIPEVTACOCMC).ToList
                    Dim verificaUnidade As List(Of VO.UnidadeEstrategicaDeNegocio) = unidade.Where(Function(f) f.CODUNDESRNGC = novoPlano.CODUNDESRNGC).ToList
                    Dim verificaEvento As List(Of VO.Evento) = evento.Where(Function(f) f.CODEVTACOCMC = novoPlano.CODEVTACOCMC).ToList
                    Dim verificaRelacaoEventoTipoEvento As List(Of VO.Evento) = evento.Where(Function(f) f.TIPEVTACOCMC = novoPlano.TIPEVTACOCMC).ToList

                    'soma 2 no index de cada linha pois a contagem começa em 0 e a primeira linha no execel é o titulo de cada coluna,
                    'logo a primeira linha de informação deve ter numero 2
                    Dim erroLinha As String = "Linha " + (dados.Rows.IndexOf(linha) + 2).ToString + ":"
                    Dim temErro As Boolean = False

                    If (verificaTipoEvento.Count = 0) Then 'verifica se o tipo de evento ja foi cadastrado
                        temErro = True
                        erroLinha = erroLinha + (" tipo de evento inválido.")
                    End If
                    If (verificaUnidade.Count = 0) Then 'verifica se a unidade de negocio ja foi cadastrada
                        temErro = True
                        erroLinha = erroLinha + (" unidade de negócio inválida.")
                    End If
                    If (verificaEvento.Count = 0) Then 'verifica se o evento ja foi cadastrado
                        temErro = True
                        erroLinha = erroLinha + (" evento inválido.")
                    End If
                    If (verificaRelacaoEventoTipoEvento.Count = 0) Then 'verifica se o evento pertence ao determinado tipo de evento
                        temErro = True
                        erroLinha = erroLinha + (" evento não pertence ao tipo de evento selecionado.")
                    End If
                    If ((CInt(novoPlano.ANOMESREF) / 100) < Date.Now.Year) Then 'verifica se a data é do ano corrente ou posterior
                        temErro = True
                        erroLinha = erroLinha + (" data precisa ser do ano atual ou seguintes.")
                    End If
                    If ((CInt(novoPlano.ANOMESREF) Mod 100) < 1 OrElse (CInt(novoPlano.ANOMESREF) Mod 100) > 12) Then 'verifica se o mes é valido
                        temErro = True
                        erroLinha = erroLinha + (" mês inválido.")
                    End If

                    If temErro Then
                        erros.Add(erroLinha)
                    End If

                    If erros.Count = 0 Then
                        If (Not (novoPlano.TIPEVTACOCMC = 0 OrElse novoPlano.CODEVTACOCMC = 0)) Then
                            Try
                                InsertT(novoPlano)
                            Catch ex As Exception
                                If Split(ex.Message, ":")(0) = "ORA-00001" Then
                                    erros.Add("Linha " + (dados.Rows.IndexOf(linha) + 1).ToString + " , este plano já existe no banco de dados.")
                                End If
                            End Try
                        Else
                            erros.Add("Linha " + (dados.Rows.IndexOf(linha) + 1).ToString + " não possui Tipo de Evento ou Evento relacionado.")
                        End If
                    End If

                Next
            End If
            Return erros
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Inserir plano de marketing a partir da relaca acao comercial plano marketing
    ''' </summary>
    ''' <param name="planoMarketingAcao"></param>
    ''' <returns></returns>
    Public Function InserirPlanoDeMarketingDaAcao(planoMarketingAcao As VO.RelacaoAcaoComercialxPlanoMarketing) As Object

        Return DirectCast(daoBase, PlanoDeMarketingDAO).InserirPlanoDeMarketingDaAcao(planoMarketingAcao)

    End Function


    ''' <summary>
    ''' Realiza o provisionamento do valor da participação do fornecedor no plano de marketing quando a ação é enviada para aprovação
    ''' </summary>
    ''' <param name="TIPEVTACOCMC">Tipo de evento</param>
    ''' <param name="CODEVTACOCMC">Código do evento</param>
    ''' <param name="ANOMESREF">Ano Mes de referência</param>
    ''' <param name="CODUNDESRNGC">Código da unidade</param>
    ''' <param name="VLRRSVPLNMKT">Valor a ser provisionado no plano de marketing</param>
    Public Sub ProvisionaValorReservadoPlanoMarketing(ByVal TIPEVTACOCMC As Decimal, ByVal CODEVTACOCMC As Decimal, ByVal ANOMESREF As Decimal, ByVal CODUNDESRNGC As Decimal, ByVal VLRRSVPLNMKT As Decimal)
        DirectCast(daoBase, PlanoDeMarketingDAO).ProvisionaValorReservadoPlanoMarketing(TIPEVTACOCMC, CODEVTACOCMC, ANOMESREF, CODUNDESRNGC, VLRRSVPLNMKT)
    End Sub

    ''' <summary>
    ''' Realiza o estorno do valor provisionado no plano de marketing quando a ação e reprovada
    ''' </summary>
    ''' <param name="TIPEVTACOCMC">Tipo de evento</param>
    ''' <param name="CODEVTACOCMC">Código do evento</param>
    ''' <param name="ANOMESREF">Ano Mes de referência</param>
    ''' <param name="CODUNDESRNGC">Código da unidade</param>
    ''' <param name="VLRRSVPLNMKT">Valor a ser estornado no plano de marketing</param>
    Public Sub EstornaValorReservadoPlanoMarketing(ByVal TIPEVTACOCMC As Decimal, ByVal CODEVTACOCMC As Decimal, ByVal ANOMESREF As Decimal, ByVal CODUNDESRNGC As Decimal, ByVal VLRRSVPLNMKT As Decimal)
        DirectCast(daoBase, PlanoDeMarketingDAO).EstornaValorReservadoPlanoMarketing(TIPEVTACOCMC, CODEVTACOCMC, ANOMESREF, CODUNDESRNGC, VLRRSVPLNMKT)
    End Sub
    ''' <summary>
    ''' Atualiza o plano de marketing quando aprova a ação comercial
    ''' </summary>
    ''' <param name="CODACOCMC"></param>
    ''' <param name="TIPEVTACOCMC"></param>
    ''' <param name="CODEVTACOCMC"></param>
    ''' <param name="CODUNDESRNGC"></param>
    ''' <param name="ANOMESREF"></param>
    ''' <returns></returns>
    Public Function UpdatePlanoMktAprovacaoAcao(CODACOCMC As Decimal, TIPEVTACOCMC As Decimal, CODEVTACOCMC As Decimal, CODUNDESRNGC As Decimal, ANOMESREF As String) As Integer
        Return DirectCast(daoBase, PlanoDeMarketingDAO).UpdatePlanoMktAprovacaoAcao(CODACOCMC, TIPEVTACOCMC, CODEVTACOCMC, CODUNDESRNGC, ANOMESREF)
    End Function
End Class
