﻿Imports System.Data.OleDb
Imports Core
Public Class CentroCustoBO
    Inherits BOBase(Of VO.CentroCusto)

    Public Sub New()
        MyBase.daoBase = New CentroCustoDAO()
    End Sub

    Public Function BuscaCentrodeCustoFuncionario(codCentroCusto As Integer, desCentroCusto As String) As List(Of VO.CentroCusto)
        Return DirectCast(daoBase, CentroCustoDAO).BuscaCentrodeCustoFuncionario(codCentroCusto, desCentroCusto)
    End Function

End Class
