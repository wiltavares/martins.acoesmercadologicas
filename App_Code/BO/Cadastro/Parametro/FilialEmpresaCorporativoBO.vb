﻿Imports Core
Public Class FilialEmpresaCorporativoBO
    Inherits BOBase(Of VO.FilialEmpresaCorporativo)

    Public Sub New()
        MyBase.daoBase = New FilialEmpresaCorporativoDAO()
    End Sub

    Public Function BuscarCodNomFilial() As List(Of VO.FilialEmpresaCorporativo)

        Return DirectCast(daoBase, FilialEmpresaCorporativoDAO).BuscarCodNomFilial()

    End Function

    Public Function ListRelacaoFilialCodigoCidade() As List(Of VO.FilialEmpresaCorporativo)

        Return DirectCast(daoBase, FilialEmpresaCorporativoDAO).ListRelacaoFilialCodigoCidade()

    End Function

    Public Function ListFiliais() As List(Of VO.FilialEmpresaCorporativo)

        Return DirectCast(daoBase, FilialEmpresaCorporativoDAO).ListFiliais()
        Return Nothing

    End Function

    Public Sub atualizaCodigoCidade(CODFILEMP As Decimal?, COD_CID As Decimal?)
        Try
            daoBase.connector.BeginTransaction()

            DirectCast(daoBase, FilialEmpresaCorporativoDAO).atualizaCodigoCidade(CODFILEMP, COD_CID)
            daoBase.connector.Commit()

        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try

    End Sub
End Class
