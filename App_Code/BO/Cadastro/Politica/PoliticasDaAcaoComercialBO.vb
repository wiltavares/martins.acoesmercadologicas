﻿Imports Core
Public Class PoliticasDaAcaoComercialBO
    Inherits BOBase(Of VO.PoliticasDaAcaoComercial)

    Public Sub New()
        MyBase.daoBase = New PoliticasDaAcaoComercialDAO()
    End Sub

    Public Function ListPolitcasAcao(CODPOLACOCMC As Decimal) As List(Of VO.PoliticasDaAcaoComercial)

        Return DirectCast(daoBase, PoliticasDaAcaoComercialDAO).ListPoliticasAcao(CODPOLACOCMC)

    End Function

End Class
