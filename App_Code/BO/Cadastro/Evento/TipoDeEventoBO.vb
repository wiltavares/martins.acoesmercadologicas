﻿Imports Core

Public Class TipoDeEventoBO
    Inherits BOBase(Of VO.TipoDeEvento)

    Public Sub New()
        MyBase.daoBase = New TipoDeEventoDAO()
    End Sub

    Public Function buscaDadosTipoEvento(tipoEvento As VO.TipoDeEvento) As List(Of VO.TipoDeEvento)
        Return DirectCast(daoBase, TipoDeEventoDAO).buscaDadosTipoEvento(tipoEvento)
    End Function

    Public Function ListTipoDeEventos(vo As VO.TipoDeEvento) As List(Of VO.TipoDeEvento)
        Return DirectCast(daoBase, TipoDeEventoDAO).ListTipoDeEventos(vo)
    End Function

    Public Function ListTipoDeEventosPreAcordo(ByVal IndPreAcordo As Nullable(Of Boolean)) As List(Of VO.TipoDeEvento)
        Return DirectCast(daoBase, TipoDeEventoDAO).ListTipoDeEventosPreAcordo(IndPreAcordo)
    End Function

End Class
