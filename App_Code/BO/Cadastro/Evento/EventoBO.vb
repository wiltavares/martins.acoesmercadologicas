﻿Imports Core
Public Class EventoBO
    Inherits BOBase(Of VO.Evento)

    Public Sub New()
        MyBase.daoBase = New EventoDAO()
    End Sub

    Public Function ListEvento(vo As VO.Evento) As List(Of VO.Evento)

        Return DirectCast(daoBase, EventoDAO).ListEvento(vo)

    End Function

    ''' <summary>
    ''' Lista os eventos de um tipo de evento
    ''' </summary>
    ''' <param name="idTipoEvento"></param>
    ''' <returns></returns>
    Public Function ListEventoDoTipoEvento(idTipoEvento As Decimal?) As List(Of VO.Evento)

        Dim evento = New VO.Evento()
        evento.TIPEVTACOCMC = idTipoEvento
        Return DirectCast(daoBase, EventoDAO).ListEvento(evento)

    End Function

    Public Function ListEventoAno(vo As VO.Evento, Optional CodAcao As Decimal = 0) As List(Of VO.Evento)
        Return DirectCast(daoBase, EventoDAO).ListEventoAno(vo, CodAcao)
    End Function
    ''' <summary>
    ''' Consulta dados de um evento pelo id
    ''' </summary>
    ''' <param name="idTipoEvento"></param>
    ''' <returns></returns>
    Public Function ConsultaDadosEventoPeloCodigo(idTipoEvento As Decimal?) As VO.Evento

        Return DirectCast(daoBase, EventoDAO).ConsultaDadosEventoPeloCodigo(idTipoEvento)

    End Function

    Public Function ConsultaParaFluxo(acao As Decimal) As List(Of VO.Evento)

        Return DirectCast(daoBase, EventoDAO).ConsultaParaFluxo(acao)

    End Function


End Class
