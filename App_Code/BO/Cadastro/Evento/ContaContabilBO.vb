﻿Imports Core
Public Class ContaContabilBO
    Inherits BOBase(Of VO.ContaContabil)

    Public Sub New()
        MyBase.daoBase = New ContaContabilDAO()
    End Sub

    Public Function ListContaContabil(vo As VO.ContaContabil) As List(Of VO.ContaContabil)

        Return DirectCast(daoBase, ContaContabilDAO).ListContaContabil(vo)

    End Function

End Class
