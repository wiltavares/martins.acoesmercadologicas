﻿Imports Core
Public Class RelacaoFuncionarioGerenteMarketingBO
    Inherits BOBase(Of VO.RelacaoFuncionarioGerenteMarketing)

    Public Sub New()
        MyBase.daoBase = New RelacaoFuncionarioGerenteMarketingDAO()
    End Sub

    Public Function ListFuncionarioGerenteMrkt(CODFNCEMTACOCMC As Decimal?, CODFNCGERMKT As Decimal?) As List(Of VO.RelacaoFuncionarioGerenteMarketing)
        Return DirectCast(daoBase, RelacaoFuncionarioGerenteMarketingDAO).ListFuncionarioGerenteMrkt(CODFNCEMTACOCMC, CODFNCGERMKT)
    End Function

    Public Sub deleteRelacaoFornGerMrkt(CODFNCEMTACOCMC As Decimal, CODFNCGERMKT As Decimal)
        Try
            daoBase.connector.BeginTransaction()

            DirectCast(daoBase, RelacaoFuncionarioGerenteMarketingDAO).deleteRelacaoFornGerMrkt(CODFNCEMTACOCMC, CODFNCGERMKT)
            daoBase.connector.Commit()

        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

    Public Sub insereRelacaoFornGerMrkt(CODFNCEMTACOCMC As Decimal, CODFNCGERMKT As Decimal)
        Try
            daoBase.connector.BeginTransaction()

            DirectCast(daoBase, RelacaoFuncionarioGerenteMarketingDAO).insereRelacaoFornGerMrkt(CODFNCEMTACOCMC, CODFNCGERMKT)
            daoBase.connector.Commit()

        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub

End Class
