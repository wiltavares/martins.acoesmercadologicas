﻿Imports Core
Public Class FuncionarioBO
    Inherits BOBase(Of VO.Funcionario)

    Public Sub New()
        MyBase.daoBase = New FuncionarioDAO()
    End Sub

    Public Function ListFuncionarios(vo As VO.Funcionario) As List(Of VO.Funcionario)
        Return DirectCast(daoBase, FuncionarioDAO).ListFuncionario(vo)
    End Function

    Public Function ListFuncionariosPromocao(vo As VO.Funcionario) As List(Of VO.Funcionario)
        Return DirectCast(daoBase, FuncionarioDAO).ListFuncionarioPromocao(vo)
    End Function

    Public Function GetSelectFuncionario(lst As List(Of Decimal)) As List(Of VO.Funcionario)
        Return DirectCast(daoBase, FuncionarioDAO).GetSelectFuncionario(lst)
    End Function

    Public Function BuscaFuncionarios(vo As VO.Funcionario) As List(Of VO.Funcionario)
        Return DirectCast(daoBase, FuncionarioDAO).BuscaFuncionarios(vo)
    End Function
    Public Function ListFuncionariosFilial(vo As VO.Funcionario) As List(Of VO.Funcionario)

        Return DirectCast(daoBase, FuncionarioDAO).ListFuncionarioFilial(vo)

    End Function
    Public Function CnsFluApvGerMrk(ByVal CodFncEmtAcoCmc As Decimal, ByVal CodAcoCmc As Decimal) As List(Of VO.Funcionario)

        Return DirectCast(daoBase, FuncionarioDAO).CnsFluApvGerMrk(CodFncEmtAcoCmc, CodAcoCmc)

    End Function

    Public Function VldItePmc(ByVal CodAcoCmc As Decimal) As Decimal

        Return DirectCast(daoBase, FuncionarioDAO).VldItePmc(CodAcoCmc)

    End Function

    Public Function CnsFluApvGerCtgDirCmp(ByVal CodSisInf As Decimal, ByVal CodAcoCmc As Decimal, ByVal NumSeqNivApv As Decimal, ByVal CodTipApvGerCtg As Decimal, _
                                    ByVal CodTipApvDirCmp As Decimal, ByVal TipIdtCtnFluCmp As Decimal) As List(Of VO.Funcionario)

        Return DirectCast(daoBase, FuncionarioDAO).CnsFluApvGerCtgDirCmp(CodSisInf, CodAcoCmc, NumSeqNivApv, CodTipApvGerCtg, CodTipApvDirCmp, TipIdtCtnFluCmp)

    End Function


    Public Function selecionaGerente(codGerente As Decimal, nomGerente As String) As List(Of VO.Funcionario)

        Return DirectCast(daoBase, FuncionarioDAO).selecionaGerente(codGerente, nomGerente)
    End Function

    Public Function buscaGerenteDefault(acao As Integer, funcionario As Integer) As VO.Funcionario

        Return DirectCast(daoBase, FuncionarioDAO).buscaGerenteDefault(acao, funcionario)

    End Function

    Public Function BuscaFuncionariosETerceiros(vo As VO.Funcionario) As VO.Funcionario
        Return DirectCast(daoBase, FuncionarioDAO).BuscaFuncionariosETerceiros(vo)
    End Function
End Class
