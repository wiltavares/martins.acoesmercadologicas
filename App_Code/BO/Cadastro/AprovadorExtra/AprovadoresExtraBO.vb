﻿Imports Core

Public Class AprovadoresExtraBO
    Inherits BOBase(Of VO.AprovadoresExtra)

    Public Sub New()
        MyBase.daoBase = New AprovadoresExtraDAO()
    End Sub

    Public Function CarregaGrid(vo As VO.AprovadoresExtra) As List(Of VO.AprovadoresExtra)
        Return DirectCast(daoBase, AprovadoresExtraDAO).CarregaGrid(vo)
    End Function

    Public Function CarregaRegistrosInseridos(vo As VO.AprovadoresExtra) As List(Of VO.AprovadoresExtra)
        Return DirectCast(daoBase, AprovadoresExtraDAO).CarregaRegistrosInseridos(vo)
    End Function


    Public Function BuscaRegistrosAprovadores(ByVal codTipoEvento As Decimal, ByVal codAprovSub As Decimal, ByVal codAprovExtra As Decimal) As List(Of VO.AprovadoresExtra)
        Return DirectCast(daoBase, AprovadoresExtraDAO).BuscaRegistrosAprovadores(codTipoEvento, codAprovSub, codAprovExtra)
    End Function


    Public Function CarregaAprovadorSubstituido(ByVal CodTipoAprovador As Decimal?) As List(Of VO.AprovadoresExtra)
        Return DirectCast(daoBase, AprovadoresExtraDAO).CarregaAprovadorSubstituido(CodTipoAprovador)
    End Function

    Public Function DeletarRegistrosAprovadores(vo As VO.AprovadoresExtra)
        Try
            daoBase.connector.BeginTransaction()
            Return DirectCast(daoBase, AprovadoresExtraDAO).DeletarRegistrosAprovadores(vo)
            daoBase.connector.Commit()

        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try
    End Function

    Public Sub InsereRegistrosAprovadores(ByVal codTipoEvento As Decimal, ByVal codAprovSub As Decimal, ByVal codAprovExtra As Decimal, ByVal codFuncionario As Decimal)
        Try
            Dim registros As List(Of VO.AprovadoresExtra) = BuscaRegistrosAprovadores(codTipoEvento, codAprovSub, codAprovExtra)

            daoBase.connector.BeginTransaction()

            Dim campos As New VO.AprovadoresExtra
            campos.TIPEVTACOCMC = codTipoEvento
            campos.CODTIPAPV = codAprovSub
            campos.CODEDEAPV = codAprovExtra

            If registros.Count > 0 Then
                DirectCast(daoBase, AprovadoresExtraDAO).DeletarRegistrosAprovadores(campos)
                DirectCast(daoBase, AprovadoresExtraDAO).InsereRegistrosAprovadores(codTipoEvento, codAprovSub, codAprovExtra, codFuncionario)
            Else
                DirectCast(daoBase, AprovadoresExtraDAO).InsereRegistrosAprovadores(codTipoEvento, codAprovSub, codAprovExtra, codFuncionario)
            End If
            daoBase.connector.Commit()
        Catch ex As Exception
            daoBase.connector.Rollback()
            Throw ex
        End Try
    End Sub
End Class
