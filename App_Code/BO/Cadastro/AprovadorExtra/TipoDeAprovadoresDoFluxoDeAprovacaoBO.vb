﻿Imports Core
Public Class TipoDeAprovadoresDoFluxoDeAprovacaoBO
    Inherits BOBase(Of VO.TipoDeAprovadoresDoFluxoDeAprovacao)

    Public Sub New()
        MyBase.daoBase = New TipoDeAprovadoresDoFluxoDeAprovacaoDAO()
    End Sub

    Public Function CarregaTipoAprovador() As List(Of VO.TipoDeAprovadoresDoFluxoDeAprovacao)
        Return DirectCast(daoBase, TipoDeAprovadoresDoFluxoDeAprovacaoDAO).CarregaTipoAprovador()
    End Function


End Class
