﻿Namespace VO
    Public Class Funcionario

        Property CODFNC As Nullable(Of Decimal)
        Property NOMFNC As String
        Property CODDPT As Nullable(Of Decimal)
        Property CODSEC As Nullable(Of Decimal)
        Property CODEMP As Nullable(Of Decimal)
        Property DATADSFNC As DateTime
        Property DATDEMFNC As DateTime
        Property ENDFNC As String
        Property CODCID As Nullable(Of Decimal)
        Property CODBAIFNC As Nullable(Of Decimal)
        Property CODCEPFNC As Nullable(Of Decimal)
        Property NUMTLFFNC As String
        Property CODFILFNC As Nullable(Of Decimal)
        Property CODSEXFNC As String
        Property CODDRT As Nullable(Of Decimal)
        Property CODDIV As Nullable(Of Decimal)
        Property CODCENCST As String
        Property CODGRPPGTFNC As Nullable(Of Decimal)
        Property CODSETFNC As Nullable(Of Decimal)
        Property NUMCNTCRRFNC As String
        Property CODAGEBCOFNC As Nullable(Of Decimal)
        Property CODBCOFNC As Nullable(Of Decimal)
        Property CODCGRFNC As Nullable(Of Decimal)
        Property NUMCPFFNC As String
        Property CODSNDFNC As Nullable(Of Decimal)
        Property DATNSC As DateTime
        Property CODUNDNGC As Nullable(Of Decimal)
        Property CODFILEMP As Nullable(Of Decimal)
        Property CODUNDREG As Nullable(Of Decimal)
        Property CODSGMMCDCTB As Nullable(Of Decimal)
        Property NUMRMLTLF As String
        Property DESPSWFNCPAD As String
        Property DESPSWCDF As String
        Property DESENDCREETNFNC As String
        Property CODNIVFNC As Nullable(Of Decimal)
        Property INDTRBFNC As Nullable(Of Decimal)
        Property NUMCARCCP As String
        Property NUMCARRLP As String
        Property DATVLDCARCCP As DateTime
        Property DATVLDCARRLP As DateTime
        Property DATPRVDEMFNC As DateTime
        Property NUMCPF As String
        Property DATALTCADFNC As DateTime
        Property TIPFNC As String
        Property NOMFNCEXD As String
        Property INDEQIFNC As Nullable(Of Decimal)
        Property NUMENDFNC As String
        Property CODEDEOCDRDF As Nullable(Of Decimal)

        Property CODGRUP As Nullable(Of Decimal)

        ReadOnly Property CODIGO_NOME() As String
            Get
                Return CODFNC & " - " & NOMFNC
            End Get
        End Property

#Region "Campos Auxiliares"

        Property ACAO As Nullable(Of Decimal)
        Property CODGERCATEGORIA As Nullable(Of Decimal)
        Property NOMGERCATEGORIA As String
        Property CODDIRCOMPRAS As Nullable(Of Decimal)
        Property NOMDIRCOMPRAS As String
        Property FLGAPVDIRCMP As Integer
        Property VLRALC As Nullable(Of Decimal)
        Property PTCDIRCATEGORIA As Nullable(Of Decimal)


#End Region


    End Class
End Namespace