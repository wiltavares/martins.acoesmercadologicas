﻿Namespace VO
    Public Class RelacaoFuncionarioGerenteMarketing

        Property CODFNCGERMKT As Nullable(Of Decimal)
        Property CODFNCEMTACOCMC As Nullable(Of Decimal)

#Region "Campos auxiliares"
        Property CODFUNC_GER_MTK As Nullable(Of Decimal)
        Property NOMUNC_GER_MTK As String

        Property CODFUNC_EMITENTE As Nullable(Of Decimal)
        Property NOMFUNC_EMITENTE As String
#End Region

    End Class
End Namespace