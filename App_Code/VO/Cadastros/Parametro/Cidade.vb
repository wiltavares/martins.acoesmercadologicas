﻿Namespace VO
    Public Class Cidade
        Property CODCID As Nullable(Of Decimal)
        Property NOMCID As String
        Property CODESTUNI As String
        Property CODCENREG As Nullable(Of Decimal)
        Property CODREGPCO As Nullable(Of Decimal)
        Property FLGCLTIDCCSTDTBCID As String
        Property FLGCLTDDOCALPCOFRT As String
        Property VLRFRTKGSCID As Nullable(Of Decimal)
        Property CODSVCLCL As Nullable(Of Decimal)
        Property DATCADCID As Nullable(Of Date)
        Property TIPCID As Nullable(Of Decimal)
        Property QDEDIAPRZRCBPGTATP As Nullable(Of Decimal)
        Property CODCEP As Nullable(Of Decimal)
        Property FLGCIDCEPUNC As String
        Property CODCDA As Nullable(Of Decimal)
        Property QDEHBTCID As Nullable(Of Decimal)
        Property NUMKMSORI As Nullable(Of Decimal)
        Property CODMCRREGCID As Nullable(Of Decimal)
        Property IDCPTNPRICLS As Nullable(Of Decimal)
        Property IDCPTNSGNCLS As Nullable(Of Decimal)
        Property IDCPTNTCRCLS As Nullable(Of Decimal)
        Property IDCPTNQRTCLS As Nullable(Of Decimal)
        Property IDCPTNQNTCLS As Nullable(Of Decimal)
        Property IDCPTNTOTCLS As Nullable(Of Decimal)
        Property CODREGETAMCD As String
        Property CODPLOECO As Nullable(Of Decimal)
        Property VLRFRTMEOCUBMRB As Nullable(Of Decimal)
        Property VLRFRTKGSTCR As Nullable(Of Decimal)
        Property VLRFRTMEOCUBTCR As Nullable(Of Decimal)
        Property QDEDIALDTFRNMERMAX As Nullable(Of Decimal)
        Property CODTIPTRP As Nullable(Of Decimal)
        Property CODCMSMOT As Nullable(Of Decimal)
        Property PERALQIMPSVCREP As Nullable(Of Decimal)
        Property CODRAIENTCDA As Nullable(Of Decimal)
        Property CODARETLFDTN As Nullable(Of Decimal)
        Property CODCIDINUGEOETA As Nullable(Of Decimal)
        Property CODCEPINI As Nullable(Of Decimal)
        Property CODCEPFIM As Nullable(Of Decimal)
        Property VLRLNTCID As Nullable(Of Decimal)
        Property VLRLTTCID As Nullable(Of Decimal)
        Property INDTIPRTZCID As Nullable(Of Decimal)
        Property NOMCIDEXD As String
        Property CODCIDCRE As Nullable(Of Decimal)
        Property DATDSTCID As Nullable(Of Date)
        Property CODARENLS As Nullable(Of Decimal)
        Property CODMCRREGDTB As String
        Property QDEMEDDTNKMSCLI As Nullable(Of Decimal)
        Property CODCIDSIA As Nullable(Of Decimal)
        Property CODFNC As Nullable(Of Decimal)
        Property VLRLTTPCICID As Nullable(Of Decimal)
        Property VLRLNTPCICID As Nullable(Of Decimal)
        Property INDDTBTUBCID As Nullable(Of Decimal)
        Property CODGRPPTNPSEMCD As Nullable(Of Decimal)
        Property TIPLCLENTCRG As Nullable(Of Decimal)
    End Class
End Namespace