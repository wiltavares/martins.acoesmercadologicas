﻿Namespace VO
    Public Class TipoDeEvento

        Property TIPEVTACOCMC As Nullable(Of Decimal)
        Property NOMTIPEVTACOCMC As String
        Property CODCNTCTB As Nullable(Of Decimal)
        Property INDVLDRGRICT As Nullable(Of Decimal)
        Property INDPREARDPMC As Nullable(Of Decimal)
        Property INDCPHBTB As Nullable(Of Decimal)
        Property INDEVTSMA As Nullable(Of Decimal)

        ReadOnly Property CODIGO_NOME() As String
            Get
                Return TIPEVTACOCMC & " - " & NOMTIPEVTACOCMC
            End Get
        End Property

        ReadOnly Property DescDropTipeve() As String
            Get
                Return TIPEVTACOCMC & " - " & NOMTIPEVTACOCMC
            End Get
        End Property

    End Class
End Namespace