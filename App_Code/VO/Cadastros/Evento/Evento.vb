﻿Namespace VO
    Public Class Evento

        Property CODEVTACOCMC As Nullable(Of Decimal)
        Property NOMEVTACOCMC As String
        Property TIPEVTACOCMC As Nullable(Of Decimal)
        Property INDEVTADIACOCMC As Nullable(Of Decimal)
        Property TIPDSNDSCBNF As Nullable(Of Decimal)
        Property INDEVTMTDPOLDVR As Nullable(Of Decimal)
        Property INDVCLPMC As Nullable(Of Decimal)



        'Propriedades extras (não fazem parte da tabela)

        Property DESDSNDSCBNF As String
        Property NOMTIPEVTACOCMC As String

        ReadOnly Property CODIGO_NOME() As String
            Get
                Return CODEVTACOCMC & " - " & NOMEVTACOCMC
            End Get
        End Property

        'ReadOnly Property DescDropEve As String
        '    Get
        '        Return CODEVTACOCMC.ToString + " - " + NOMEVTACOCMC
        '    End Get
        'End Property



    End Class
End Namespace