﻿Namespace VO
    Public Class ContaContabil

        Property CODCNTCTB As Nullable(Of Decimal)
        Property NUMGRACNTCTB As Nullable(Of Decimal)
        Property DESCNTCTB As String
        Property DATCAD As DateTime
        Property DATDST As DateTime

    End Class
End Namespace