﻿Namespace VO
    Public Class OperacaoFiscal

        Property CODOPEFSCDSNACOCMC As Nullable(Of Decimal)
        Property DESOPEFSCDSNACOCMC As String
        Property INDTIPOPEDSNACOCMC As Nullable(Of Decimal)
        Property INDBDEOPEDSNACOCMC As Nullable(Of Decimal)
        Property INDUNDESRNGCACOCMC As Nullable(Of Decimal)
        Property INDGRCREQAUTITERSU As Nullable(Of Decimal)
        Property INDGRCITEENCFOLPGT As Nullable(Of Decimal)
        Property INDTIPOPEACOCMCAUT As Nullable(Of Decimal)
        Property INDTIPOPEICT As Nullable(Of Decimal)
        Property INDTIPOPEVLRMON As Nullable(Of Decimal)
        Property INDTIPOPEREQVGM As Nullable(Of Decimal)
        Property INDTIPOPEREQEVT As Nullable(Of Decimal)
        Property INDTIPOPESLCPGT As Nullable(Of Decimal)
        Property INDPTCCLIOPEDSN As Nullable(Of Decimal)
    End Class
End Namespace