﻿Namespace VO
    Public Class AprovadoresExtra

        Property TIPEVTACOCMC As Nullable(Of Decimal)
        Property CODTIPAPV As Nullable(Of Decimal)
        Property CODEDEAPV As Nullable(Of Decimal)
        Property CODEDEAPVEXA As Nullable(Of Decimal)

        Property CODFNC As Decimal?
        Property NOMFNC As String
        Property NOMTIPEVTACOCMC As String
        Property NOMUSREDEAPVEXT As String
        Property NOMUSREDEAPV As String
        Property DESTIPAPV As String
    End Class
End Namespace