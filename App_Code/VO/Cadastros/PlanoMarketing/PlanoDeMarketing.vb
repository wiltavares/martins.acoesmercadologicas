﻿Namespace VO
    Public Class PlanoDeMarketing

        Property TIPEVTACOCMC As Nullable(Of Decimal)
        Property CODEVTACOCMC As Nullable(Of Decimal)
        Property ANOMESREF As Nullable(Of Decimal)
        Property CODUNDESRNGC As Nullable(Of Decimal)
        Property VLROCDPLNMKT As Nullable(Of Decimal)
        Property VLRRSVPLNMKT As Nullable(Of Decimal)
        Property VLRUTZPLNMKT As Nullable(Of Decimal)
        Property VLROCDCTTPLNMKT As Nullable(Of Decimal)
        Property VLROCDEXACTTPLNMKT As Nullable(Of Decimal)

#Region "Campos Auxiliares"
        Property NOMTIPEVTACOCMC As String
        Property NOMEVTACOCMC As String
        Property DESUNDESRNGC As String
        Property DATA As String

        Property SLDMESPLNMKT As Nullable(Of Decimal)
        Property SLDATUPLNMKT As Nullable(Of Decimal)
        Property SLDANOPLNMKT As Nullable(Of Decimal)
        Property VLRGSTANOPLNMKT As Nullable(Of Decimal)
        Property VLROCDANOPLNMKT As Nullable(Of Decimal)

        Property VLRRSTMES As Nullable(Of Decimal)
        Property VLRRSTANO As Nullable(Of Decimal)
        Property VLRRSTATU As Nullable(Of Decimal)

#End Region
    End Class
End Namespace