﻿Imports Microsoft.VisualBasic
Namespace VO

    Public Class SimulaFunding
        Property CODFRN As Nullable(Of Decimal)
        Property DESFRN As String
        Property CODMER As Nullable(Of Decimal)
        Property DESMER As String
        Property PARTICIPACAO As Nullable(Of Decimal)
        Property QUANTIDADE As Nullable(Of Decimal)
        Property CODFILEMP As Nullable(Of Decimal)
        Property DESFILEMP As String
        Property VLRUNT As Nullable(Of Decimal)
    End Class
End Namespace