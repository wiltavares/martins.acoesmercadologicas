﻿Namespace VO
    Public Class EventoPromocional

        Property CODEVTBTB As Nullable(Of Decimal)
        Property DESEVTBTB As String
        Property DESDTLEVTBTB As String
        Property DESLNKIMGEVTBTB As String
        Property DATINIEVT As Nullable(Of Date)
        Property DATFIMEVT As Nullable(Of Date)
        Property CODFNCCAD As Nullable(Of Decimal)
        Property CODDES As String

    End Class
End Namespace