﻿Imports Core

Namespace VO
    Public Class RelacaoAcaoMecanicaPublicoAlvoEstado
        Property CODACOCMC As Nullable(Of Decimal)
        Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
        Property CODESTUNI As String
        Property NOMESTUNI As String
        Property CODDIVREG As String
    End Class
End Namespace