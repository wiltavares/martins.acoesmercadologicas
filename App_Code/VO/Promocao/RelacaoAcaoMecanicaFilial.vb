﻿Imports Microsoft.VisualBasic

Namespace VO
    Public Class RelacaoAcaoMecanicaFilial
        Property CODACOCMC As Nullable(Of Decimal)
        Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
        Property CODFILEMP As Nullable(Of Decimal)
        Property CODPMC As Nullable(Of Decimal)

        Property DATECSUSR As Nullable(Of DateTime)

        'Propriedade extra    
        Property DESRGRACOCMC As String
        Property DESABVFILEMP As String
    End Class
End Namespace
