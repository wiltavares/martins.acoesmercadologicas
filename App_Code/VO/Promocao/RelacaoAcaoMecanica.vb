﻿Imports Microsoft.VisualBasic

Namespace VO
    Public Class RelacaoAcaoMecanica


        Property CODACOCMC As Nullable(Of Decimal)
        Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
        Property DESRGRACOCMC As String
        Property TIPPMC As Nullable(Of Decimal)
        Property QDEGRPMIXPMC As Nullable(Of Decimal)
        Property QDEFXAPMC As Nullable(Of Decimal)
        Property INDPTCTOTMIXPMC As Nullable(Of Decimal)


        'Property FormaCalculo As String
        'Property CODCTRPMC As Nullable(Of Decimal)
        'Property QtdBrindesAting As Nullable(Of Decimal)

#Region "Property Extra"
        Property RelacaoAcaoMecanicaFilialList As List(Of RelacaoAcaoMecanicaFilial)
        Property RelacaoAcaoMecanicaCriterioDeApuracaoList As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao)
        Property VLRLIMMNMFXACTRPMC As Nullable(Of Decimal)
        Property QDEPTOFXACTRPMC As Nullable(Of Decimal)
        Property TIPEQCCALCTRPMC As Nullable(Of Decimal)
        Property CODCTRPMC As Nullable(Of Decimal)
        Property NUMSEQCTRACOCMC As Nullable(Of Decimal)
        Property CODGRPMIXMERPMC As Nullable(Of Decimal)

        Property NOMACOCMC As String
        Property CODSTAAPVACOCMC As Nullable(Of Decimal)
        Property NOMFNC As String
        Property TIPACOMCD As Nullable(Of Decimal)
        Property DATGRCACOCMC As Nullable(Of DateTime)
        Property DATINIACOCMC As Nullable(Of DateTime)
        Property DATFIMACOCMC As Nullable(Of DateTime)
        Property VLRTOTACOCMC As Nullable(Of Decimal)



        'CODIGO ACAO COMERCIAL COPIA (Ação que originou o clone)
        Property CODACOCMCCPI As Nullable(Of Decimal)

        Property relacaoAcaoMecanicaPublicoAlvoEstadoList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado)
        Property relacaoAcaoMecanicaPublicoAlvoAtividadeList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)
        Property relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)
        Property relacaoAcaoMecanicaPublicoAlvoNegocioClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente)
        Property relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente)
        Property relacaoAcaoMecanicaPublicoAlvoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)
        Property relacaoAcaoMecanicaPublicoAlvoRcaVendedorList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)
        Property relacaoAcaoMecanicaPublicoAlvoGerenteVendasList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas)
        Property relacaoAcaoMecanicaPublicoAlvoGerenteMercadoList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)
        Property relacaoAcaoMecanicaMixList As List(Of VO.RelacaoAcaoMecanicaMix)
        Property relacaoAcaoMecanicaClientesExcludentesList As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes)
#End Region


    End Class
End Namespace
