﻿Imports Core

Namespace VO
    Public Class PromocaoMix

        'CODIGO PROMOCAO DE VENDAS                                             
        Property CODPMC As Nullable(Of Decimal)

        'NUMERO SEQUENCIA MIX ACAO COMERCIAL                                   
        Property NUMSEQMIXACOCMC As Nullable(Of Decimal)

        'CODIGO GRUPO DA MERCADORIA                                            
        Property CODGRPMER As Nullable(Of Decimal)

        'CODIGO FAMILIA DA MERCADORIA                                          
        Property CODFMLMER As Nullable(Of Decimal)

        'CODIGO CLASSE DA MERCADORIA REPRESENTA A CATEGORIA                    
        Property CODCLSMER As Nullable(Of Decimal)

        'CODIGO MERCADORIA                                                     
        Property CODMER As Nullable(Of Decimal)

        'CODIGO FORNECEDOR                                                     
        Property CODFRN As Nullable(Of Decimal)

        'CODIGO GRUPO MIX MERCADORIA PROMOCAO                                  
        Property CODGRPMIXMERPMC As Nullable(Of Decimal)

        'INDICA VALIDAÇÃO REGRA DE MIX DA PROMOÇÃO                             
        Property INDVLDRGRMIXPMC As Nullable(Of Decimal)

        Property QDEMNMMERMIX As Nullable(Of Decimal)
        Property QDEMAXMERMIX As Nullable(Of Decimal)
        Property INDITEOBR As Nullable(Of Decimal)
        Property INDJRNSMA As Nullable(Of Decimal)


#Region "Propriedades Extra"
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
        Property DATDSTGRPMER As Nullable(Of Decimal)
        Property DESGRPMER As String
        Property DESFMLMER As String
        Property DESCLSMER As String
        Property DESMER As String
        Property NOMFRN As String
        Property INDGRUPOMERCADORIA As Nullable(Of Decimal)
        Property INDFAMILIAMERCADORIA As Nullable(Of Decimal)
        Property INDCLASSEMERCADORIA As Nullable(Of Decimal)
        Property INDMERCADORIA As Nullable(Of Decimal)
        Property INDFORNECEDOR As Nullable(Of Decimal)
        'Property ItensPesquisaFamiliaMercadoria As List(Of VO.PromocaoMix)
        'Property PromocaoMixGrupoMercadoria As List(Of PromocaoMixViewModel)

        Property CODDRTCMP As Nullable(Of Decimal)
        Property CODDIVCMP As Nullable(Of Decimal)
#End Region
    End Class
End Namespace

