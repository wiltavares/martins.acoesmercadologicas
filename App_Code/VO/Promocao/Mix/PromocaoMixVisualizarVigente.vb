﻿Imports Core

Namespace VO
    Public Class PromocaoMixVisualizarVigente
        Property CODGRPMIXMERPMC As Nullable(Of Decimal)
        Property CODMER As Nullable(Of Decimal)
        Property DESMER As String
        Property CODFRNPCPMER As Nullable(Of Decimal)
        Property CODPMC As Nullable(Of Decimal)
        Property QDEPTOMERPMC As Nullable(Of Decimal)
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
        Property VLRCNVUNDVNDDNMPMC As Nullable(Of Decimal)
        Property VLRCNVUNDVNDNUMPMC As Nullable(Of Decimal)
        Property QDEMNMVNDMER As Nullable(Of Decimal)
        Property QDEUNDVNDCXAFRN As Nullable(Of Decimal)
        Property CODGRPMERFRC As Nullable(Of Decimal)
        Property NUMSEQGRPMIXPMC As Nullable(Of Decimal)
        Property VLRUNTPCOBRT As Nullable(Of Decimal)
        Property QDEMNMITEGRPMIXPMC As Nullable(Of Decimal)
        Property VLRMEDITEGRPMIXPMC As Nullable(Of Decimal)
        Property CODACOCMC As Nullable(Of Decimal)
        Property NUMSEQRGRACOCMC As Nullable(Of Decimal)

        Property CODGRPMER As Nullable(Of Decimal)
        Property DESGRPMER As String
        Property CODFMLMER As Nullable(Of Decimal)
        Property DESFMLMER As String
        Property CODCLSMER As Nullable(Of Decimal)
        Property DESCLSMER As String
        Property NOMFRN As String

        Property QDEMNMMERMIX As Nullable(Of Decimal)
        Property QDEMAXMERMIX As Nullable(Of Decimal)
        Property INDITEOBR As Nullable(Of Decimal)
        Property INDJRNSMA As Nullable(Of Decimal)
    End Class
End Namespace

