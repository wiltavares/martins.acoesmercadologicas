﻿Imports Microsoft.VisualBasic
Namespace VO
    Public Class PromocaoHistoricoSituacao
        Property CODPMC As Nullable(Of Decimal)
        Property NUMSEQ As Nullable(Of Decimal)
        Property CODFNC As Nullable(Of Decimal)
        Property DATGRCHST As Nullable(Of DateTime)
        Property TIPSITPMC As String

        Property DESSITPMC As String
        Property NOMFNC As String
    End Class
End Namespace
