﻿Imports Core

Namespace VO
    Public Class Abastecimento
        Property CODACOCMC As Nullable(Of Decimal)
        Property CODPMC As Nullable(Of Decimal)
        Property CODMER As Nullable(Of Decimal)
        Property NUMSEQISRQDEMERPMC As Nullable(Of Decimal)
        Property QDEMERISRPMC As Nullable(Of Decimal)
        Property VLRTOTMERISRPMC As Nullable(Of Decimal)
        Property DATGRCRLC As Nullable(Of Date)
        Property CODFNCGRCPMC As Nullable(Of Decimal)
        Property INDISRVLDMERPMC As Nullable(Of Decimal)
        ''' <summary>
        '''  0 = Abastecer, 1 = Desabastecer
        ''' </summary>
        ''' <returns></returns>
        Property INDTIPISRMERPMC As Nullable(Of Decimal)





        'Auxiliares

        Property CODUNDESRNGC As Nullable(Of Decimal)
        Property NOMFILEMP As String
        Property CODCPR As Nullable(Of Decimal)
        Property NOMCPR As String
        Property CODFILEMP As Nullable(Of Decimal)
        Property DATINIPMC As Nullable(Of Date)
        Property DATFIMPMC As Nullable(Of Date)
        Property DESPMC As String
        Property NOMPMC As String
        Property CODPRMPMC As Nullable(Of Decimal)
        Property DESMER As String
        Property ETQBND As Nullable(Of Decimal)
        Property QDEMERVNDPRMPMC As Nullable(Of Decimal)
        Property QDEMERMAXPRMPMC As Nullable(Of Decimal)
        Property ETQSLD As Nullable(Of Decimal)
        Property QDEDIACOB As Nullable(Of Decimal)
        Property QDEDIARES As Nullable(Of Decimal)
        Property VALOR_BRINDE As Nullable(Of Decimal)
        Property CUSTO_BRINDE As Nullable(Of Decimal)
        Property MEDIA_SAIDA As Nullable(Of Decimal)
        Property TIPSITPMC As String
        Property CODFILEMPORIVBA As Nullable(Of Decimal)
        Property CODFNCCRIACOCMC As Nullable(Of Decimal)

        ''' <summary>
        ''' Numero do item na relação ação comercial X item Orçamento
        ''' Quando item substituto, o codigo sera o codigo do item principal.
        ''' </summary>
        ''' <returns></returns>
        Property NUMLNHITEOCDACOCMC As Nullable(Of Decimal)

        ''' <summary>
        ''' Codigo da mercadoria principal na relação ação comercial X item Orçamento
        ''' Quando for um item substituto, o codigo sera o codigo do item principal.
        ''' </summary>
        ''' <returns></returns>
        Property CODMER_PRINCIPAL As Nullable(Of Decimal)


        'use a propriedade INDTIPISRMERPMC.
        'Property tipo As Nullable(Of Decimal)
        Property NOMFNCGRCPMC As String


        Property NOMFRNMER As String
        Property CODFRNMER As Nullable(Of Decimal)


        Property TIPEDENIVPRMPMC As Nullable(Of Decimal)


        'Data de exclusão do brinde na promoção
        Property DATECSPRD As Nullable(Of DateTime)
    End Class
End Namespace