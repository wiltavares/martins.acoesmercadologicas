﻿Imports Core

Namespace VO
    Public Class RelacaoAcaoMecanicaClientesExcludentes

        Property CODACOCMC As Nullable(Of Decimal)
        Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
        Property CODCLI As Nullable(Of Decimal)
        Property NOMCLI As String
    End Class
End Namespace