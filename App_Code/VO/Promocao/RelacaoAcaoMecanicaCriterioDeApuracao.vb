﻿Imports Microsoft.VisualBasic

Namespace VO
    Public Class RelacaoAcaoMecanicaCriterioDeApuracao

        Property CODACOCMC As Nullable(Of Decimal)
        Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
        Property NUMSEQCTRACOCMC As Nullable(Of Decimal)
        Property CODCTRPMC As Nullable(Of Decimal)
        Property TIPEDENIVPRMPMC As Nullable(Of Decimal)
        Property VLRLIMMNMFXACTRPMC As Nullable(Of Decimal)
        Property VLRLIMMAXFXACTRPMC As Nullable(Of Decimal)
        Property QDEPTOFXACTRPMC As Nullable(Of Decimal)
        Property INDTIPPTOPMC As Nullable(Of Decimal)
        Property DESCTRMIXPMC As String
        Property CODGRPMIXMERPMC As Nullable(Of Decimal)
        Property TAMIVLFXAEQCCALCTR As Nullable(Of Decimal)
        Property TIPEQCCALCTRPMC As Nullable(Of Decimal)
        Property VLRLIMMAXCTR As Nullable(Of Decimal)

        'Propriedade Extra
        Property TIPPMC As Nullable(Of Decimal)

    End Class
End Namespace