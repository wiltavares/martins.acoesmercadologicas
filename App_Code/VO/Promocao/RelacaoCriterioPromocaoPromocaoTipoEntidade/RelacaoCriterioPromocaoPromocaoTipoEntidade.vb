﻿Imports Microsoft.VisualBasic

Namespace VO
    Public Class RelacaoCriterioPromocaoPromocaoTipoEntidade
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
        Property CODPMC As Nullable(Of Decimal)
        Property CODCTRPMC As Nullable(Of Decimal)
        Property VLRLIMMNMFXACTRPMC As Nullable(Of Decimal)
        Property VLRLIMMAXFXACTRPMC As Nullable(Of Decimal)
        Property QDEPTOFXACTRPMC As Nullable(Of Decimal)
        Property CODGRPMIXMERPMC As Nullable(Of Decimal)
        Property DESCTRMIXPMC As String
        Property TIPEDENIVPRMPMC As Nullable(Of Decimal)


        Property CODFTRCNDCTRPMC As Nullable(Of Decimal)
        Property VLRMNMFTRCNDCTRPMC As Nullable(Of Decimal)
        Property TIPEQCCALCTRPMC As Nullable(Of Decimal)
        Property FLGMNTCTRPMC As String

    End Class
End Namespace
