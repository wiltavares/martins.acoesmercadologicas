﻿Imports Core

Namespace VO
    Public Class RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente
        Property CODACOCMC As Nullable(Of Decimal)
        Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
        Property CODGRPCLI As Nullable(Of Decimal)
        Property DESGRPCLI As String
        Property CODUNDESRNGC As Nullable(Of Decimal)
        Property DESUNDESRNGC As String
    End Class
End Namespace

