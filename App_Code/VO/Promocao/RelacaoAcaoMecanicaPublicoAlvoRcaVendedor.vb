﻿Imports Core

Namespace VO
    Public Class RelacaoAcaoMecanicaPublicoAlvoRcaVendedor
        Property CODACOCMC As Nullable(Of Decimal)
        Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
        Property CODREP As Nullable(Of Decimal)

        'auxiliares
        Property NOMREP As String
        Property NOMSUP As String
        Property CODSUP As Nullable(Of Decimal)

    End Class
End Namespace

