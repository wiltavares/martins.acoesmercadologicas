﻿Imports Microsoft.VisualBasic

Namespace VO
    Public Class RelacaoPromocaoCriterioApuracaoEscalaProgressiva


        'Tipo Critério
        Property CODCTRPMC As Nullable(Of Decimal)

        'Mix
        Property CODGRPMIXMERPMC As Nullable(Of Decimal)

        Property CODPMC As Nullable(Of Decimal)

        'Desc Mecanica
        Property DESCTRMIXPMC As String

        'Qtd Premio Atingimento
        Property QDEPTOFXAEQCCALCTR As Nullable(Of Decimal)



        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)


        Property VLRINIFXAEQCCALCTR As Nullable(Of Decimal)

        'Critério
        Property TAMIVLFXAEQCCALCTR As Nullable(Of Decimal)

        Property INDTIPPTOPMC As Nullable(Of Decimal)
        Property VLRLIMMAXCTR As Nullable(Of Decimal)


    End Class
End Namespace
