﻿Imports Core

Namespace VO
    Public Class PromocaoPublicoAlvoGrupamentoCliente
        Property CODPMC As Nullable(Of Decimal)
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
        Property CODGRPCLI As Nullable(Of Decimal)
        Property DESGRPCLI As String
        Property CODUNDESRNGC As Nullable(Of Decimal)
        Property DESUNDESRNGC As String
    End Class
End Namespace

