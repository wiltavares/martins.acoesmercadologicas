﻿Imports Core

Namespace VO
    Public Class PromocaoPublicoAlvoEstado
        Property CODPMC As Nullable(Of Decimal)
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
        Property CODESTUNI As String
        Property NOMESTUNI As String
        Property CODDIVREG As String
    End Class
End Namespace