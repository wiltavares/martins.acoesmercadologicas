﻿Imports Core

Namespace VO
    Public Class PromocaoPublicoAlvoCliente
        Property CODPMC As Nullable(Of Decimal)
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
        Property CODCLI As Nullable(Of Decimal)
        Property NOMCLI As String

        Property NOMGER As String
        Property NOMSUP As String
        Property NOMREP As String



        Property CODREP As Nullable(Of Decimal)
        Property CODGER As Nullable(Of Decimal)
        Property CODSUP As Nullable(Of Decimal)
    End Class
End Namespace

