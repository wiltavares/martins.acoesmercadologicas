﻿Imports Core

Namespace VO
    Public Class PromocaoPublicoAlvoAtividade
        Property CODPMC As Nullable(Of Decimal)
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
        Property CODATI As Nullable(Of Decimal)
        Property NOMATI As String
        Property CODUNDESRNGC As Nullable(Of Decimal)
        Property DESUNDESRNGC As String
    End Class
End Namespace

