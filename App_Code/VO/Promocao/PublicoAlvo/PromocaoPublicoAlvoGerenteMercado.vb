﻿Imports Core

Namespace VO
    Public Class PromocaoPublicoAlvoGerenteMercado
        Property CODPMC As Nullable(Of Decimal)
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
        Property CODSUP As Nullable(Of Decimal)
        Property NOMSUP As String

        Property CODGER As Nullable(Of Decimal)
        Property NOMGER As String
    End Class
End Namespace

