﻿Imports Microsoft.VisualBasic
Namespace VO

    ' RELACAO PROMOCAO DE VENDA X CRITERIO DE SELECAO DE PUBLICO ALVO 
    Public Class PromocaoPublicoAlvoSelecao

        'TIPO DA ENTIDADE PUBLICO ALVO PROMOCAO
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)

        'CODIGO DA PROMOCAO DE VENDA
        Property CODPMC As Nullable(Of Decimal)

        'CODIGO DO CRITERIO DE SELECAO DO PUBLICO ALVO DA PROMOCAO DE VENDA
        Property CODCTRSELPUBALVPMC As Nullable(Of Decimal)

        'INDICA MANUTENCAO CRITERIO PUBLICO ALVO PROMOCAO
        Property FLGMNTCTRPUBALVPMC As String
    End Class
End Namespace
