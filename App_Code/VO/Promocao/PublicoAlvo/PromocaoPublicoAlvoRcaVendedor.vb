﻿Imports Core

Namespace VO
    Public Class PromocaoPublicoAlvoRcaVendedor
        Property CODPMC As Nullable(Of Decimal)
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
        Property CODREP As Nullable(Of Decimal)
        Property NOMREP As String

        Property NOMSUP As String
        Property CODSUP As Nullable(Of Decimal)
    End Class
End Namespace

