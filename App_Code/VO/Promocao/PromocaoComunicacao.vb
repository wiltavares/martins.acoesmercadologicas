﻿Imports Microsoft.VisualBasic

Namespace VO
    Public Class PromocaoComunicacao

        ''' <summary>
        '''  TIPO ENTIDADE DASEG WITH GRANT OPTIONO ALVO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)

        ''' <summary>
        '''  CODIGO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property CODPMC As Nullable(Of Decimal)

        ''' <summary>
        ''' TIPO DE COMUNICACAO DA PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property TIPCMNPMC As Nullable(Of Decimal)

        Property DESTIPCMNPMC As String
        Property DESTIPDVGPMCLIVPCO As String
        Property CODSMBDVGPMCLIVPCO As String
        Property TIPDVGPMCLIVPCO As Nullable(Of Decimal)


    End Class

End Namespace
