﻿Imports Core

Namespace VO
    Public Class RelacaoAcaoMecanicaMixPorPromocao
        Property CODACOCMC As Nullable(Of Decimal)
        Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
        Property NUMSEQMIXACOCMC As Nullable(Of Decimal)
        Property CODGRPMER As Nullable(Of Decimal)
        Property CODFMLMER As Nullable(Of Decimal)
        Property CODCLSMER As Nullable(Of Decimal)
        Property CODMER As Nullable(Of Decimal)
        Property CODFRN As Nullable(Of Decimal)
        Property CODGRPMIXMERPMC As Nullable(Of Decimal)
#Region "Propriedades Extra"
        Property DATDSTGRPMER As Nullable(Of Decimal)
        Property DESGRPMER As String
        Property DESFMLMER As String
        Property DESCLSMER As String
        Property DESMER As String
        Property NOMFRN As String
        Property INDGRUPOMERCADORIA As Nullable(Of Decimal)
        Property INDFAMILIAMERCADORIA As Nullable(Of Decimal)
        Property INDCLASSEMERCADORIA As Nullable(Of Decimal)
        Property INDMERCADORIA As Nullable(Of Decimal)
        Property INDFORNECEDOR As Nullable(Of Decimal)
        Property ItensPesquisaFamiliaMercadoria As List(Of VO.RelacaoAcaoMecanicaMix)
        Property relacaoAcaoMecanicaMixGrupoMercadoria As List(Of RelacaoAcaoMecanicaMixViewModel)


        Property CODDRTCMP As Nullable(Of Decimal)
        Property CODDIVCMP As Nullable(Of Decimal)
        Property CODPMC As Nullable(Of Decimal)
#End Region
    End Class
End Namespace

