﻿Imports Microsoft.VisualBasic

Namespace VO
    Public Class Promocao

        ''' <summary>
        '''  TIPO ENTIDADE DASEG WITH GRANT OPTIONO ALVO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)

        ''' <summary>
        '''  CODIGO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property CODPMC As Nullable(Of Decimal)

        ''' <summary>
        '''  DATA INICIO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property DATINIPMC As Nullable(Of Date)

        ''' <summary>
        '''  DATA FIM PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property DATFIMPMC As Nullable(Of Date)

        ''' <summary>
        '''  DESCRICAO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property DESPMC As String

        ''' <summary>
        '''  DATA HORA GERACAO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property DATHRAGRCPMC As Nullable(Of DateTime)

        ''' <summary>
        '''  CODIGO FUNCIONARIO GEROU PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property CODFNCGRCPMC As Nullable(Of Decimal)

        ''' <summary>
        '''  INDICA SE FOI SOLICITADO LISTAGEM PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property FLGSLCLSTPMC As String

        ''' <summary>
        '''  NOME PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property NOMPMC As String

        ''' <summary>
        '''  VALOR VERBA PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property VLRVBAPMC As Nullable(Of Decimal)

        ''' <summary>
        '''  QUANTIDADE PONTOS COMPETICAO VENDA
        ''' </summary>
        ''' <returns></returns>
        Property QDEPTOCOPVND As Nullable(Of Decimal)

        ''' <summary>
        '''  TIPO SITUACAO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property TIPSITPMC As String

        ''' <summary>
        '''  CODIGO FUNCIONARIO ALTEROU SITUACAO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property CODFNCALTSITPMC As Nullable(Of Decimal)

        ''' <summary>
        '''  DATA HORA ALTERACAO SITUACAO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property DATHRAALTSITPMC As Nullable(Of DateTime)

        ''' <summary>
        '''  DATA PROCESSAMENTO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property DATPCSPMC As Nullable(Of Date)

        ''' <summary>
        '''  TIPO APURACAO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property TIPAPUPMC As String

        ''' <summary>
        '''  CODIGO FUNCIONARIO ALTEROU PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property CODFNCALTPMC As Nullable(Of Decimal)

        ''' <summary>
        '''  DATA HORA ALTERACAO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property DATHRAALTPMC As Nullable(Of DateTime)

        ''' <summary>
        '''  CODIGO EMPRESA
        ''' </summary>
        ''' <returns></returns>
        Property CODEMP As Nullable(Of Decimal)

        ''' <summary>
        '''  CODIGO FILIAL EMPRESA
        ''' </summary>
        ''' <returns></returns>
        Property CODFILEMP As Nullable(Of Decimal)

        ''' <summary>
        '''  CODIGO DA ACAO COMERCIAL 
        ''' </summary>
        ''' <returns></returns>
        Property CODACOCMC As Nullable(Of Decimal)

        ''' <summary>
        '''  CODIGO DA PROMOCAO PRINCIPAL 
        ''' </summary>
        ''' <returns></returns>
        Property CODPMCPCP As Nullable(Of Decimal)

        ''' <summary>
        '''  QUANTIDADE BONIFICADA PROMOCAO CASADA 
        ''' </summary>
        ''' <returns></returns>
        Property QDEBNFPMCCAA As Nullable(Of Decimal)

        ''' <summary>
        '''  QUANTIDADE VENDIDA PROMOCAO CASADA 
        ''' </summary>
        ''' <returns></returns>
        Property QDEVNDPMCCAA As Nullable(Of Decimal)

        ''' <summary>
        '''  DATA INICIO DA PROMOCAO CASADA 
        ''' </summary>
        ''' <returns></returns>
        Property DATINIPMCCAA As Nullable(Of Date)

        ''' <summary>
        '''  DATA FINAL DA PROMOCAO CASADA 
        ''' </summary>
        ''' <returns></returns>
        Property DATFIMPMCCAA As Nullable(Of Date)

        ''' <summary>
        '''  FLAG DE CONTROLE DA PROMOCAO CASADA 
        ''' </summary>
        ''' <returns></returns>
        Property FLGCONPMCCAA As String

        ''' <summary>
        ''' FLAG ENVIO PROMOCAO 
        ''' </summary>
        ''' <returns></returns>
        Property FLGENVPMC As String

        ''' <summary>
        ''' NOME DO USUARIO DO SISTEMA 
        ''' </summary>
        ''' <returns></returns>
        Property NOMUSRSIS As String

        ''' <summary>
        ''' DESCRICAO COMERCIAL DA PROMOCAO 
        ''' </summary>
        ''' <returns></returns>
        Property DESCMCPMC As String

        ''' <summary>
        ''' INDICADOR CRITERIO ADICIONAL AVALIACAO PROMOCAO 
        ''' </summary>
        ''' <returns></returns>
        Property INDCTRADIAVLPMC As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICADOR PARTICIPACAO PROMOCAO COMERCIO ELETRONICO 
        ''' </summary>
        ''' <returns></returns>
        Property INDPTCPMCCMCETN As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICADOR PARTICIPACAO PROMOCAO TELEMARKETING 
        ''' </summary>
        ''' <returns></returns>
        Property INDPTCPMCTMK As Nullable(Of Decimal)

        ''' <summary>
        ''' CODIGO UNIDADE ESTRATEGICO NEGOCIO 
        ''' </summary>
        ''' <returns></returns>
        Property CODUNDESRNGC As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICADOR APURACAO INCENTIVO QUANTIDADE BRINDE 
        ''' </summary>
        ''' <returns></returns>
        Property INDAPUICTQDEBDE As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICA EXCLUSAO DOS ITENS DA PROMOCAO NO PEDIDO DE VENDA 
        ''' </summary>
        ''' <returns></returns>
        Property INDECSITEPMCPEDVND As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICA PREMIO UNICO VIGORACAO PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property INDPRMUNCVGRPMC As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICADOR COBRANCA DO BRINDE NA PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property INDCOBBDEPMC As Nullable(Of Decimal)

        ''' <summary>
        ''' TIPO DE PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property TIPPMC As Nullable(Of Decimal)

        ''' <summary>
        ''' VALOR BASE DE CALCULO PARA GRUPOS DO MIX DE PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property VLRBSECALGRPMIXPMC As Nullable(Of Decimal)

        ''' <summary>
        ''' PERCENTUAL DE VARIACAO DOS GRUPOS NO MIX DA PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property PERVARGRPMIXPMC As Nullable(Of Decimal)

        ''' <summary>
        ''' QUANTIDADE DE GRUPOS NO MIX DA PROMOCAO
        ''' </summary>
        ''' <returns></returns>
        Property QDEGRPMIXPMC As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICA PROMOCAO SISTEMA NOVO
        ''' </summary>
        ''' <returns></returns>
        Property INDPMCSISNVO As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICADOR DE BRINDE FRACIONADO
        ''' </summary>
        ''' <returns></returns>
        Property INDBDEFRC As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICA PARTICIPACAO DE TODO MIX NA PROMOCAO      
        ''' </summary>
        ''' <returns></returns>
        Property INDPTCTOTMIXPMC As Nullable(Of Decimal)

        ''' <summary>
        ''' QUANTIDADE MAXIMA DE BRINDE POR CLIENTE     
        ''' </summary>
        ''' <returns></returns>
        Property QDEMAXBDECLI As Nullable(Of Decimal)

        ''' <summary>
        ''' QUANTIDADE DE FAIXA DA PROMOCAO                                       
        ''' </summary>
        ''' <returns></returns>
        Property QDEFXAPMC As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICA QUE EXISTEM BRINDES ADICIONAIS NA PROMOCAO     
        ''' </summary>
        ''' <returns></returns>
        Property INDISRBDEADI As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICADOR DE STATUS DA PROMOCAO NO RELATORIO    
        ''' </summary>
        ''' <returns></returns>
        Property INDSTAPMCREL As Nullable(Of Decimal)

        ''' <summary>
        ''' Indica promoções que participarão da venda simplificada    
        ''' </summary>
        ''' <returns></returns>
        Property INDPMCVNDSMP As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICA PROMOCAO VALIDA NO APLICATIVO MOVEL DO CLIENTE
        ''' </summary>
        ''' <returns></returns>
        Property INDPMCVLDAPLMVLCLI As Nullable(Of Decimal)

        ''' <summary>
        ''' TIPO DA EQUIPE DE VENDA
        ''' </summary>
        ''' <returns></returns>
        Property TIPEQIVND As Nullable(Of Decimal)

        ''' <summary>
        ''' TIPO NATUREZA DA VENDA
        ''' </summary>
        ''' <returns></returns>
        Property TIPNATVND As Nullable(Of Decimal)

        ''' <summary>
        ''' INDICA PROMOCAO RELÂMPAGO
        ''' </summary>
        ''' <returns></returns>
        Property INDPMCIMD As Nullable(Of Decimal)

        ''' <summary>
        '''  DATA E HORA INICIO DA PROMOCAO RELÂMPAGO
        ''' </summary>
        ''' <returns></returns>
        Property HRAINIPMC As Nullable(Of Date)

        ''' <summary>
        '''  DATA E HORA FIM DA PROMOCAO RELÂMPAGO 
        ''' </summary>
        ''' <returns></returns>
        Property HRAFIMPMC As Nullable(Of Date)

        ''' <summary>
        ''' CODIGO EVENTO PROMOCIONAL
        ''' </summary>
        ''' <returns></returns>
        Property CODEVTBTB As Nullable(Of Decimal)



#Region "campos adicionais"

        Property NOMFILEMP As String

        'Propriedades para VerificaQtdMinVndMaiorQtdPmc
        Property CODMER As Nullable(Of Decimal)
        Property DESMER As String
        Property QDEMIN As Nullable(Of Decimal)
        Property QDEPMC As Nullable(Of Decimal)


        Property DATINISEMANA As Nullable(Of Date)
        Property DATREFLIVPCO As Nullable(Of Date)
        Property DATFIMVLDLIVPCO As Nullable(Of Date)

        Property DESUNDESRNGC As String

        'Propriedades para verificar se a prmoção é de CASHBACK
        Property INDTIPPTOPMC1 As Nullable(Of Decimal)
        Property INDTIPPTOPMC2 As Nullable(Of Decimal)

        Property INDHSTSITPMC As Nullable(Of Decimal)

        Property INDEVTSMA As Nullable(Of Decimal)
        Property INDCPTFCH As Nullable(Of Decimal)
        Property TIPNIVCLFCPT As Nullable(Of Decimal)
        Property QDEMNMVNDMER As Nullable(Of Decimal)
        Property INDQDEMPLVNDMER As Nullable(Of Decimal)
        Property INDPMTMIXPMC As Nullable(Of Decimal)

#End Region

    End Class

End Namespace
