﻿Imports Microsoft.VisualBasic
Namespace VO
    Public Class PromocaoBrindeHistorico
        Property CODFNC As Nullable(Of Decimal)
        Property DATGRCHST As Nullable(Of DateTime)
        Property CODPMC As Nullable(Of Decimal)
        Property TIPEDENIVPRMPMC As Nullable(Of Decimal)
        Property CODMER As Nullable(Of Decimal)
        Property NUMSEQ As Nullable(Of Decimal)
        Property INDSITMER As Nullable(Of Decimal)


        Property DESPRMPMC As String
        Property NOMFNC As String
    End Class
End Namespace
