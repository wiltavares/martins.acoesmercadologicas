﻿Imports Microsoft.VisualBasic
Namespace VO
    Public Class PromocaoBrinde
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
        Property CODPMC As Nullable(Of Decimal)
        Property TIPEDENIVPRMPMC As Nullable(Of Decimal)
        Property CODPRMPMC As Nullable(Of Decimal)
        Property QDEMERMAXPRMPMC As Nullable(Of Decimal)
        Property QDEMERVNDPRMPMC As Nullable(Of Decimal)
        Property PERMERRSVPRMPMC As Nullable(Of Decimal)
        Property VLRPRMPMC As Nullable(Of Decimal)
        Property DESPRMPMC As String
        Property FLGTRSMSGFIMPRMPMC As String
        Property INDISRBDEADI As Nullable(Of Decimal)
        Property CODCNDPGT As Nullable(Of Decimal)
        Property VLRUNTVNDPRMPMC As Nullable(Of Decimal)
        Property INDACUBDEPMCFAT As Nullable(Of Decimal)
        Property QDEBDEMPLCTRPMC As Nullable(Of Decimal)
        Property VLRUNTFNDMERPMC As Nullable(Of Decimal)

        Property DATECSPRD As Nullable(Of DateTime)
        Property CODFNCECS As Nullable(Of Decimal)
        Property NOMFNCECS As String

        Property SALDO As Nullable(Of Decimal)
        Property MEDIA As Nullable(Of Decimal)

        Property CUSTO As Nullable(Of Decimal)

        ' a propriedade CODPRMPMC e o codigo da mercadoria.
        'Property CODMER As Nullable(Of Decimal)
        Property DESMER As String


        Property CODACOCMC As Nullable(Of Decimal)
        Property INDCOBIPRMRI As Nullable(Of Decimal)
        Property VLRUNTLIQMER As Nullable(Of Decimal)
        Property PERBFCBDE As Nullable(Of Decimal)


        Property CODPMCADI As Nullable(Of Decimal)
        Property CODPRMADIPMC As Nullable(Of Decimal)
        Property QDEMERPRMADIPMC As Nullable(Of Decimal)

        Property CODFILEMP As Nullable(Of Decimal)
        Property FLGMER As String
        Property CODMERPCP As Nullable(Of Decimal)
        Property CODMERCST As Nullable(Of Decimal)
        Property CODMERMRG As Nullable(Of Decimal)
        Property CODGRPMIXMERPMC As Nullable(Of Decimal)
        Property ERRO As String
        Property TIPO As String
        Property DATREFLIVPCO As Nullable(Of Date)

        'Relacao Acao Comercial

        Property CODFNCGRCPMC As Nullable(Of Decimal)
        Property INDISRVLDMERPMC As Nullable(Of Decimal)
        Property INDTIPISRMERPMC As Nullable(Of Decimal)
        Property NUMSEQISRQDEMERPMC As Nullable(Of Decimal)
        Property QDEMERISRPMC As Nullable(Of Decimal)
        Property VLRTOTMERISRPMC As Nullable(Of Decimal)
        Property DATGRCRLC As Nullable(Of Date)


        Property RATEAR As Boolean

    End Class
End Namespace
