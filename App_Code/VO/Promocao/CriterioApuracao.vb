﻿Imports Microsoft.VisualBasic

Namespace VO
    'Referente tabela 
    Public Class CriterioApuracao
        Property CODACAO As Nullable(Of Decimal)
        Property SEQMECANICA As Nullable(Of Decimal)
        Property SEQCRITERIO As Nullable(Of Decimal)
        Property QDEPREMATINGIMENTO As Nullable(Of Decimal)
        Property FORMACALCULO As Nullable(Of Decimal)
        Property CODCTRPMC As Nullable(Of Decimal)
        Property VLRINICIAL As Nullable(Of Decimal)
        Property VLRFINAL As Nullable(Of Decimal)
        Property QTDPTSFAIXA As Nullable(Of Decimal)
        Property DESCTRMIXPMC As String
        Property CODGRPMIXMERPRM As Nullable(Of Decimal)
        Property TIPEDENIVPRMPMC As Nullable(Of Decimal)
    End Class
End Namespace

