﻿Imports Microsoft.VisualBasic

Namespace VO
    Public Class LogDataStage
        Property CODFNC As Nullable(Of Decimal) 'CODIGO FUNCIONARIO                                                    
        Property DATSLC As Nullable(Of DateTime) 'DATA QUE FOI FEITA A SOLICITACAO                                      
        Property CODLNHARQDTS As String 'Código gerado através de concatenaçao para identificar a linha        
        Property FLGSTAPCS As String 'FLAG STATUS PROCESSAMENTO                                             
        Property NOMARQRCB As String 'Nome Do Arquivo Recebido                                              
        Property NOMACSUSRSIS As String 'NOME ACESSO USUARIO SISTEMA                                           
        Property DESMTVSTA As String 'DESCRICAO MOTIVO STATUS                                               
        Property DATHRAINIPCS As Nullable(Of DateTime) 'DATA E HORA Do INICIO Do PROCESSAMENTO                                
        Property DATHRAFIMPCS As Nullable(Of DateTime) 'DATA E HORA Do FIM DE PROCESSAMENTO    

    End Class
End Namespace
