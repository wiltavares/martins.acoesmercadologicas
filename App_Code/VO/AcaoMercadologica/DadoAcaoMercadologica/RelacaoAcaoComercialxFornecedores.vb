﻿Namespace VO
    Public Class RelacaoAcaoComercialxFornecedores

        'CODIGO DA ACAO COMERCIAL
        Property CODACOCMC As Nullable(Of Decimal)
        'CODIGO FORNECEDOR
        Property CODFRN As Nullable(Of Decimal)

        'CODIGO UNIDADE ESTRATEGICA DE NEGOCIOS (BU)
        Property CODUNDESRNGC As Nullable(Of Decimal)

        'DESCRICAO DO BENEFICIO DO FORNECEDOR NA ACAO COMERCIAL
        Property DESBFCFRNACOCMC As String

        'DESCRICAO DA SELECAO DOS FORNECEDORES
        Property DESSELFRN As String

        'INDICADOR DE ATENDIMENTO DIRETO PELO FORNECEDOR (KEY ACCOUNT)
        Property INDATDDTOFRN As Nullable(Of Decimal)

        'TIPO DESTINO / DESCONTO BONIFICADO
        Property TIPDSNDSCBNF As Nullable(Of Decimal)

        'TIPO DESTINO DESCONTO BONIFICACAO DE ACRESCIMO
        Property TIPDSNDSCBNFACR As Nullable(Of Decimal)

        'TIPO EMPENHO EXTRA CONTRATO
        Property TIPDSNDSCBNFEXACTT As Nullable(Of Decimal)

        'VALOR PARTICIPACAO DO FORNECEDOR NA ACAO COMERCIAL
        Property VLRPTCFRNACOCMC As Nullable(Of Decimal)

        'VALOR DO SALDO CONTA CORRENTE DO FORNECEDOR
        Property VLRSLDCNTCRRFRN As Nullable(Of Decimal)

        'VALOR SALDO DISPONIVEL PARA ACOES FUTURAS DO FORNECEDOR
        Property VLRSLDDISACOFUTFRN As Nullable(Of Decimal)

        'VALOR SALDO DESTINO ACRESCIMO
        Property VLRSLDDSNACR As Nullable(Of Decimal)

        'VALOR SALDO EMPENHO CONTRATO ACAO COMERCIAL
        Property VLRSLDDSNCTTACOCMC As Nullable(Of Decimal)

        'VALOR SALDO DO DESTINO DE DESCONTO DA ACAO COMERCIAL
        Property VLRSLDDSNDSCACOCMC As Nullable(Of Decimal)

        'VALOR DA TAXA DE ADMINISTRACAO DO BRINDE DA ACAO COMERCIAL
        Property VLRTXAADMBDEACOCMC As Nullable(Of Decimal)

        'VALOR TAXA DE ADMINISTRACAO DO FORNECEDOR NA ACAO COMERCIAL
        Property VLRTXAADMFRNACOCMC As Nullable(Of Decimal)

        'VALOR UTILIZADO DO CONTRATO NA ACAO COMERCIAL
        Property VLRUTZCTTACOCMC As Nullable(Of Decimal)

        'VALOR UTILIZADO EXTRA CONTRATO NA ACAO COMERCIAL
        Property VLRUTZEXACTTACOCMC As Nullable(Of Decimal)

        'VALOR UTILIZADO RECEITA SOBRE CMV
        Property VLRUTZRCTCSTMER As Nullable(Of Decimal)



        Property VLR_TOTAL_ACAO As Nullable(Of Decimal)
        Property PERCENTUAL As Nullable(Of Decimal)
        Property VALOR_ESTORNAR As Nullable(Of Decimal)
        Property CODMCOVBAFRN As Nullable(Of Decimal)
        Property SALDO_ACAO As Nullable(Of Decimal)

        Property VLRUTZACOCMC As Nullable(Of Decimal)
        Property TIPFRNACOCMC As Nullable(Of Decimal)
        Property PERPTCFRNACOCMC As Nullable(Of Decimal)

#Region "Campos Auxiliares"

        Property VLRTOTACOCMC As Nullable(Of Decimal)
        Property VLRTOTTXAADMACOCMC As Nullable(Of Decimal)
        Property VLRTOTBDEACOCMC As Nullable(Of Decimal)
        Property CODEMP As Nullable(Of Decimal)
        Property NOMFRN As String
        Property DESDSNDSCBNF As String
        Property DESDSNDSCBNFEXACTT As String
        Property DESUNDESRNGC As String
        Property VLRSLDDSP As Nullable(Of Decimal)
        Property CODUNDESRNGC_DESUNDESRNGC As Object
        Property TIPDSNDSCBNF_DESDSNDSCBNF As Object

#End Region

#Region "Campos auxiliares Estorno"
        ReadOnly Property PercPtc As Decimal
            Get
                If (VLRTOTACOCMC IsNot Nothing AndAlso VLRTOTACOCMC <> 0) Then
                    Return VLRPTCFRNACOCMC / VLRTOTACOCMC
                End If

                Return 0
            End Get
        End Property

        ReadOnly Property PerTxaAdm As Decimal
            Get
                If (VLRPTCFRNACOCMC + VLRTXAADMFRNACOCMC + VLRTXAADMBDEACOCMC) <> 0 Then
                    Return (VLRTXAADMFRNACOCMC + VLRTXAADMBDEACOCMC) /
                             (VLRPTCFRNACOCMC + VLRTXAADMFRNACOCMC + VLRTXAADMBDEACOCMC)
                End If

                Return 0
            End Get
        End Property

        ReadOnly Property PerAcordo As Decimal
            Get
                If VLRPTCFRNACOCMC <> 0 Then
                    Return (VLRUTZCTTACOCMC - (VLRUTZCTTACOCMC * PerTxaAdm)) / VLRPTCFRNACOCMC
                End If

                Return 0
            End Get
        End Property

        ReadOnly Property PerExaAcordo As Decimal
            Get
                If VLRPTCFRNACOCMC <> 0 Then
                    Return (VLRUTZEXACTTACOCMC - (VLRUTZEXACTTACOCMC * PerTxaAdm)) / VLRPTCFRNACOCMC
                End If

                Return 0
            End Get
        End Property

        Property EstornoAcordo As Decimal?
        Property EstornoExaAcordo As Decimal?

#Region "Variaveis auxiliares para fluxo"
        'TODO: verificar CnsSldRsvAcoCmc , colunas com alias, remover para nao nao ficar coluna duplicada
        Property EmpenhoCtt As String
        Property EmpenhoCMV As String
        Property EmpenhoExaCtt As String
        Property VlrAcordo As Decimal?
        Property VlrAcordoExa As Decimal?
        Property VlrTxaAdm As Decimal?
        Property VlrReceitaCMV As Decimal?
        Property VlrParticipacao As Decimal?
        Property VlrTxaBde As Decimal?

#End Region
        ReadOnly Property CodNomFrn As String
            Get
                Return String.Format("{0} - {1}", CODFRN, NOMFRN)
            End Get
        End Property

#End Region

    End Class
End Namespace