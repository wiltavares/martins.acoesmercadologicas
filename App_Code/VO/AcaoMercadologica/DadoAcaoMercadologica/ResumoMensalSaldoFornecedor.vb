﻿Namespace VO
    Public Class ResumoMensalSaldoFornecedor

        Property ANOMESREF As Nullable(Of Decimal)
        Property CODFRN As Nullable(Of Decimal)
        Property TIPDSNDSCBNF As Nullable(Of Decimal)
        Property VLRCRDMESCRR As Nullable(Of Decimal)
        Property VLRDEBMESCRR As Nullable(Of Decimal)
        Property VLRJURMRA As Nullable(Of Decimal)
        Property VLRSLDMESANT As Nullable(Of Decimal)
        Property VLRSLDPMS As Nullable(Of Decimal)

    End Class
End Namespace
