﻿Namespace VO
    Public Class RelacaoPromessaxFormaDePagamentoxDestinoDaVerbaPrometida

        Property CODEMP As Nullable(Of Decimal)
        Property CODFILEMP As Nullable(Of Decimal)
        Property CODPMS As Nullable(Of Decimal)
        Property DATNGCPMS As Date
        Property DATPRVRCBPMS As Date
        Property INDASCARDFRNPMS As Nullable(Of Decimal)
        Property TIPDSNDSCBNF As Nullable(Of Decimal)
        Property TIPFRMDSCBNF As Nullable(Of Decimal)
        Property VLREFTPMS As Nullable(Of Decimal)
        Property VLRNGCPMS As Nullable(Of Decimal)
        Property VLRNGCPMSANTARR As Nullable(Of Decimal)
        Property VLRPDAPMS As Nullable(Of Decimal)
        Property VLRPGOPMS As Nullable(Of Decimal)
        Property VLRRCTPMS As Nullable(Of Decimal)

    End Class
End Namespace

