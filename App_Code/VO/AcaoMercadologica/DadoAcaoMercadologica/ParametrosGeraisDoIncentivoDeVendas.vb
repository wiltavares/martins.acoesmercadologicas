﻿Namespace VO
    Public Class ParametrosGeraisDoIncentivoDeVendas

        Property CODICT As Nullable(Of Decimal)
        Property CODSTAICT As Nullable(Of Decimal)
        Property CODTIPICT As Nullable(Of Decimal)
        Property CODUNDESRNGC As Nullable(Of Decimal)
        Property CODUSRCADICT As Nullable(Of Decimal)
        Property DATFIMCADICT As Date
        Property DATFIMEFTICT As Date
        Property DATINICADICT As Date
        Property DATINIEFTICT As Date
        Property DESDTLRGRICT As String
        Property DESICT As String
        Property DESRDCRGRICT As String
        Property INDACUPRMPODAPUICT As Nullable(Of Decimal)
        Property INDAPUPCLICT As Nullable(Of Decimal)
        Property INDAPUTOTICT As Nullable(Of Decimal)
        Property INDENVICTCPO As Nullable(Of Decimal)
        Property INDSTAICTREL As Nullable(Of Decimal)
        Property PERACRCLIATDICT As Nullable(Of Decimal)
        Property PERACRVNDICT As Nullable(Of Decimal)
        Property QDEPODAPUICT As Nullable(Of Decimal)
        Property QDEPODATGOBJAPUFIM As Nullable(Of Decimal)
        Property QDEPTOEQCMRT As Nullable(Of Decimal)

#Region "Campos Auxiliares"

        Property QTDCADICENTIVOS As Integer
        Property QTDPUBLICOALVO As Integer
        Property QTDMERCADORIAS As Integer
        Property QTDMETAS As Integer
        Property QTDCRITERIOS As Integer
        Property QTDCRITERIOSAPURACAO As Integer
        Property QTDPREMIACAO As Integer


#End Region



    End Class
End Namespace