﻿Namespace VO
    Public Class RelacaoDeEnderecosParaEnvioDoCorreioEletronico

        Property IDTENDCREETN As String
        Property NUMSEQENDCREETN As Nullable(Of Decimal)
        Property NUMSEQMSGCREETN As Nullable(Of Decimal)
        Property TIPENDCREETN As Nullable(Of Decimal)
        Property TIPMSGCREETN As Nullable(Of Decimal)

    End Class
End Namespace
