﻿Namespace VO
    Public Class CarimboDeVerbaDoFornecedor

        Property CODMCOVBAFRN As Nullable(Of Decimal)
        Property NUMPEDCMP As Nullable(Of Decimal)
        Property DATGRCMCOVBAFRN As Date
        Property CODFNCMCOVBAFRN As Nullable(Of Decimal)
        Property CODFRN As Nullable(Of Decimal)
        Property CODPMS As Nullable(Of Decimal)
        Property DATNGCPMS As Date
        Property INDSTAMCOVBAFRN As Nullable(Of Decimal)
        Property DESOBSMCOVBAFRN As String
        Property TIPOBJMCOVBAFRN As Nullable(Of Decimal)
        Property VLRUTZACOCMC As Nullable(Of Decimal)


#Region "Campos Auxiliares"

        'Property DESOBJMCOVBAFRN As String

#End Region
    End Class
End Namespace