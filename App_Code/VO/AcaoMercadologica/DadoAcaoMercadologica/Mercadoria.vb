﻿Namespace VO
    Public Class Mercadoria

        Property CODMER As Nullable(Of Decimal)
        Property CODEMP As Nullable(Of Decimal)
        Property DESMER As String
        Property DESUNDVNDMER As String
        Property CODSITMER As Nullable(Of Decimal)
        Property FLGDSTMER As String
        Property CODSBCMER As Nullable(Of Decimal)
        Property PESUNTMER As Nullable(Of Decimal)
        Property CODTBTICMMER As Nullable(Of Decimal)
        Property PERTBTIPIMER As Nullable(Of Decimal)
        Property DESCPLMER As String
        Property FLGMERCONGOV As String
        Property CODVARNVGMER As Nullable(Of Decimal)
        Property DATCADMER As Nullable(Of Date)
        Property DATDSTMER As Nullable(Of Date)
        Property CODACRCSTMER As Nullable(Of Decimal)
        Property FLGOPCCTS As String
        Property FLGOPCCMP As String
        Property FLGGARAUTETQ As String
        Property NUMFOLLIVPCO As Nullable(Of Decimal)
        Property PSOUNTMER As Nullable(Of Decimal)
        Property QDECXABSEPLT As Nullable(Of Decimal)
        Property QDECXAARAPLT As Nullable(Of Decimal)
        Property QDEUNDVNDCXAFRN As Nullable(Of Decimal)
        Property TIPMAP As Nullable(Of Decimal)
        Property FLGOPCDIVMAP As Nullable(Of Decimal)
        Property CODNRMPLTMER As Nullable(Of Decimal)
        Property VOLUNDVNDMER As Nullable(Of Decimal)
        Property TIPPDCMER As Nullable(Of Decimal)
        Property CODCLSMOVMER As Nullable(Of Decimal)
        Property FLGMERRNG As String
        Property QDEDIAATZMEDRCBMER As Nullable(Of Decimal)
        Property FLGMERTBTSFR As String
        Property CODCPRPCPMER As Nullable(Of Decimal)
        Property TIPMER As Nullable(Of Decimal)
        Property CODCLFIPIMER As Nullable(Of Decimal)
        Property CODFLXPCO As String
        Property TIPRTCFRTMER As String
        Property TIPATLRTCFRTMER As String
        Property NUMPODVLD As Nullable(Of Decimal)
        Property TIPPODVLD As String
        Property FLGUNDVNDMRTVOLEPD As String
        Property FLGUNDVNDFRNVOLEPD As String
        Property CODBRRUNDVNDMRT As Nullable(Of Decimal)
        Property TIPCODBRRUNDVNDMRT As String
        Property CODBRRUNDVNDFRN As Nullable(Of Decimal)
        Property TIPCODBRRUNDVNDFRN As String
        Property VLRCNVUNDVNDMER As Nullable(Of Decimal)
        Property PERMRGMNMFLXPCO As Nullable(Of Decimal)
        Property CODSMBSITMERFLXPCO As String
        Property FLGGRPCRGMER As String
        Property FTRAJTCSTDTB As Nullable(Of Decimal)
        Property PERMRGRSTFATMERMTD As Nullable(Of Decimal)
        Property TIPMERMTD As String
        Property CODTPIMERIPD As Nullable(Of Decimal)
        Property CODGRPMER As Nullable(Of Decimal)
        Property CODFMLMER As Nullable(Of Decimal)
        Property CODCLSMER As Nullable(Of Decimal)
        Property QDEPNDPED As Nullable(Of Decimal)
        Property CODCTGMER As Nullable(Of Decimal)
        Property CODPDAMER As String
        Property CODORIMER As Nullable(Of Decimal)
        Property CODPORMERPSC As Nullable(Of Decimal)
        Property CLFCRVABCMER As String
        Property POSCRVABCMER As Nullable(Of Decimal)
        Property CODGRPMERNCM As String
        Property VLRMNMSBTTBTNAC As Nullable(Of Decimal)
        Property FLGCONIDTUNDFBR As String
        Property CODGRPMERNCMCTB As String
        Property QDEDIAVLDMER As Nullable(Of Decimal)
        Property CODTABFRTDIFMOVMER As Nullable(Of Decimal)
        Property DESCMCMER As String
        Property DESCMCMERCCO As String
        Property INDRTNFTETBTSOC As Nullable(Of Decimal)
        Property CODFRNPCPMER As Nullable(Of Decimal)
        Property CODMERTCR As String
        Property QDEMNMVNDMER As Nullable(Of Decimal)
        Property INDQDEMPLVNDMER As Nullable(Of Decimal)
        Property INDMERPRL As Nullable(Of Decimal)
        Property VLRCMIMER As Nullable(Of Decimal)
        Property VLRARAMER As Nullable(Of Decimal)
        Property VLRPFNMER As Nullable(Of Decimal)
        Property INDCONMERTRPDPV As Nullable(Of Decimal)
        Property INDMEROFECRZ As Nullable(Of Decimal)
        Property DATALTCADMER As Date
        Property INDEBGMRTDPVMOV As Nullable(Of Decimal)
        Property INDEBGFRNDPVMOV As Nullable(Of Decimal)
        Property CODGRPMERFRC As Nullable(Of Decimal)
        Property CODBRRUNDVNDCSM As Nullable(Of Decimal)
        Property TIPCODBRRUNDVNDCSM As String
        Property INDREQNUMSERMER As Nullable(Of Decimal)
        Property DATRCDMER As Date
        Property QDEISNMNRUNDINT As Nullable(Of Decimal)
        Property QDEISNMERVEI As Nullable(Of Decimal)
        Property PSOLIQMNRUNDINT As Nullable(Of Decimal)
        Property TIPUNDMNRUNDINT As String
        Property CODLNHMER As Nullable(Of Decimal)
        Property QDEDIARMCPCO As Nullable(Of Decimal)
        Property DESMERMRT As String
        Property CODEBGMER As Nullable(Of Decimal)
        Property TIPIDTEMPASCACOCMC As Nullable(Of Decimal)
        Property CODMEREMPASCACOCMC As Nullable(Of Decimal)
        Property INDMERMPB As Nullable(Of Decimal)
        Property VLRMAXVNDMPB As Nullable(Of Decimal)
        Property PERTBTIMPIPD As Nullable(Of Decimal)
        Property DESECCTBTIMPIPD As String
        Property DESECCTBTIPI As String
        Property DESMERLNGETG As String
        Property INDORIMER As Nullable(Of Decimal)
        Property VLRMNMIPI As Nullable(Of Decimal)
        Property INDMERKIT As Nullable(Of Decimal)
        Property INDMERVNDRTCKIT As Nullable(Of Decimal)
        Property CODMRCMER As Nullable(Of Decimal)
        Property INDVNDENM As Nullable(Of Decimal)
        Property QDEUNDVNDCXAKIT As Nullable(Of Decimal)
        Property QDEVOL As Nullable(Of Decimal)
        Property INDORIMERFRN As Nullable(Of Decimal)
        Property NUMPORPRDIFR As String
        Property FLGECCTBT As String
        Property INDCRFINUNACMEO As Nullable(Of Decimal)
        Property FLGIDTDPVMVL As String
        Property CODEPCSBTTBT As Nullable(Of Decimal)


        Property TYPE_EXPENSE_ID As Nullable(Of Decimal)
        Property TYPE_EXPENSE_CODE As String
        Property DESCRIPTION As String
        Property IDTITE As Nullable(Of Decimal)
        Property VLRPCO As Nullable(Of Decimal)

    End Class
End Namespace