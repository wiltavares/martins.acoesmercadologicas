﻿Namespace VO
    Public Class MovimentoDiarioReservaDeSaldoFornecedor

        Property CODFRN As Nullable(Of Decimal)
        Property CODACOCMC As String
        Property TIPDSNDSCBNF As Nullable(Of Decimal)
        Property VLRSLDRSVFRN As Nullable(Of Decimal)
        Property FLGAPVACOCMC As String
        Property FLGLMTCNTCRR As String
        Property TIPACOCMC As String
        Property DATAPVACOCMC As Date
        Property VLRRLZACOCMC As Nullable(Of Decimal)
        Property DATACEACOCMC As Date
        Property VLRACEACOCMC As Nullable(Of Decimal)
        Property VLRACRCAPACOCMC As Nullable(Of Decimal)

    End Class
End Namespace