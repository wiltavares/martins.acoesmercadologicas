﻿Namespace VO
    Public Class NiveisDoAprovadorDoFluxoPorSistema

        Property CODSISINF As Nullable(Of Decimal)
        Property NUMSEQNIVAPV As Nullable(Of Decimal)
        Property CODTIPAPV As Nullable(Of Decimal)
        Property CODFNCAPVFIX As Nullable(Of Decimal)
        Property INDCTN As Nullable(Of Decimal)
        Property INDENVNOTAPV As Nullable(Of Decimal)

    End Class
End Namespace