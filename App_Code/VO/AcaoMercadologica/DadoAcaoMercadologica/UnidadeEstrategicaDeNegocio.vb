﻿Namespace VO
    Public Class UnidadeEstrategicaDeNegocio

        Property CODUNDESRNGC As Nullable(Of Decimal)
        Property DESUNDESRNGC As String
        Property CODFNC As Nullable(Of Decimal)
        Property DESABVUNDESRNGC As String
        Property VLRPTNMCD As Nullable(Of Decimal)
        Property DATDST As Date
        Property CODESLNGCEMP As Nullable(Of Decimal)
        Property QDECTGCLIOCD As Nullable(Of Decimal)

    End Class
End Namespace
