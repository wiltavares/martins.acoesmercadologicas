﻿Namespace VO
    Public Class ContratoDeFornecimento

        Property NUMCTTFRN As Nullable(Of Decimal)
        Property DATCADCTTFRN As Date
        Property DATINIPODVGRCTTFRN As Date
        Property DATVNCCTTFRN As Date
        Property TIPPODCTTFRN As Nullable(Of Decimal)
        Property QDEMESAPUPODVGRCTT As Nullable(Of Decimal)
        Property FLGPGCAUTCTTFRN As String
        Property QDEPGCEFTCTTFRN As Nullable(Of Decimal)
        Property CODFRN As Nullable(Of Decimal)
        Property DATDSTCTTFRN As Date
        Property FLGFIMCTTFRN As String
        Property FLGAPUCRSREFPODANT As String
        Property NUMNVOCTTFRN As Nullable(Of Decimal)
        Property TIPCTTFRN As Nullable(Of Decimal)
        Property QDEITECTLFRN As Nullable(Of Decimal)
        Property TIPFRMPGTCTTFRN As Nullable(Of Decimal)
        Property TIPFRNCTTFRN As Nullable(Of Decimal)
        Property TIPPGTNOTFSCCTTFRN As Nullable(Of Decimal)
        Property TIPABGTABPCOCTTFRN As Nullable(Of Decimal)
        Property TIPENCFINCTTFRN As Nullable(Of Decimal)
        Property NUMDIAPRZPGTCTTFRN As Nullable(Of Decimal)
        Property PERDSCFINCTTFRN As Nullable(Of Decimal)
        Property PERENCFINCTTFRN As Nullable(Of Decimal)
        Property DESOBSCNDPGTCTTFRN As String
        Property INDDIFALQICMCTTFRN As Nullable(Of Decimal)
        Property INDDIFCNLVNDCTTFRN As Nullable(Of Decimal)
        Property NUMDIAATLTABPCOFRN As Nullable(Of Decimal)
        Property DESOBSTABPCOCTTFRN As String
        Property INDTRCMERCTTFRN As Nullable(Of Decimal)
        Property DESOBSFRMPGTCTTFRN As String
        Property CODBCOCTTFRN As Nullable(Of Decimal)
        Property CODAGEBCOCTTFRN As Nullable(Of Decimal)
        Property CODCNTCRRBCOCTTFRN As String
        Property INDDSCCMCPETCTTFRN As Nullable(Of Decimal)
        Property NUMDIAVNCPRDCTTFRN As Nullable(Of Decimal)
        Property DESOBSDSCCMCPET As String
        Property DESLNHPRDCTTFRN As String
        Property TIPFRMDSCDVLMER As Nullable(Of Decimal)
        Property TIPFRTDVLMER As String
        Property DESOBSDVLMER As String
        Property INDAPUPISCTTFRN As Nullable(Of Decimal)
        Property INDAPUPISNCMCTTFRN As Nullable(Of Decimal)
        Property TIPUTZALCVBAFRN As Nullable(Of Decimal)
        Property DATALAPGCCTTFRN As Date

    End Class
End Namespace