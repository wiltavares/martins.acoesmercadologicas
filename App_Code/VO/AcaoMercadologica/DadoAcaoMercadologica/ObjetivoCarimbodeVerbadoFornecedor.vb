﻿Namespace VO
    Public Class ObjetivoCarimbodeVerbadoFornecedor


        Property TIPOBJMCOVBAFRN As Nullable(Of Decimal)
        Property DESOBJMCOVBAFRN As String

        ReadOnly Property DescDrop As String
            Get
                Return TIPOBJMCOVBAFRN & " - " & DESOBJMCOVBAFRN
            End Get
        End Property

    End Class
End Namespace