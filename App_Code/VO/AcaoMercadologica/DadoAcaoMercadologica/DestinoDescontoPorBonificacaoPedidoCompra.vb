﻿Namespace VO
    Public Class DestinoDescontoPorBonificacaoPedidoCompra

        Property TIPDSNDSCBNF As Nullable(Of Decimal)
        Property DESDSNDSCBNF As String
        Property FLGCALCST As String
        Property FLGCNTCRRFRN As String
        Property FLGACOMCD As String
        Property FLGUTZLCREMP As String
        Property FLGCTBDSNDSC As String
        Property IDTFLUACOCMC As String
        Property CODCCP As String
        Property CODCNTCTBGLM As String
        Property CODCENCST As String
        Property INDFLUCNCPEDCMP As Nullable(Of Decimal)
        Property INDTRNDSNDSCBNF As Nullable(Of Decimal)
        Property CODFLUAPVACOCMC As Nullable(Of Decimal)
        Property TIPALCVBAFRN As Nullable(Of Decimal)
        Property INDTIPDSNDSPFRN As Nullable(Of Decimal)
        Property FLGARDEMSCON As String
        Property INDTIPDSNRCTCSTMER As Nullable(Of Decimal)
        Property INDDSNVBAMCO As Nullable(Of Decimal)

    End Class
End Namespace