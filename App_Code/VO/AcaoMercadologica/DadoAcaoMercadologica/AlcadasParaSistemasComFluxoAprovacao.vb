﻿Namespace VO
    Public Class AlcadasParaSistemasComFluxoAprovacao

        Property CODSISINF As Nullable(Of Decimal)
        Property CODTIPAPV As Nullable(Of Decimal)
        Property CODEDEAPV As Nullable(Of Decimal)
        Property TIPIDTCTNFLUCMP As Nullable(Of Decimal)
        Property VLRCTN As Nullable(Of Decimal)

    End Class
End Namespace