﻿Namespace VO
    Public Class RelacaoAcaoComercialPorOperacaoDeDestino

        Property CODACOCMC As Nullable(Of Decimal)
        Property CODOPEFSCDSNACOCMC As Nullable(Of Decimal)
        Property CODUNDESRNGCOPEDSN As Nullable(Of Decimal)
        Property QDEITEOPEDSNACOCMC As Nullable(Of Decimal)
        Property QDEUTZOPEDSNACOCMC As Nullable(Of Decimal)
        Property VLROPEFSCDSNACOCMC As Nullable(Of Decimal)
        Property VLRUTZOPEDSNACOCMC As Nullable(Of Decimal)

    End Class
End Namespace