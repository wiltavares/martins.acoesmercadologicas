﻿Namespace VO
    Public Class ClausulaContratoDeFornecimento

        Property NUMCSLCTTFRN As Nullable(Of Decimal)
        Property DESCSLCTTFRN As String
        Property INDUTZCSLALCVBAFRN As Nullable(Of Decimal)
        Property TIPDSNDSCBNF As Nullable(Of Decimal)
        Property INDUTLVLRFXA As Nullable(Of Decimal)

    End Class

End Namespace