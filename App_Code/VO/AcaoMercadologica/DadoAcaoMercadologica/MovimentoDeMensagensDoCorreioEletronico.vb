﻿Namespace VO
    Public Class MovimentoDeMensagensDoCorreioEletronico

        Property TIPMSGCREETN As Nullable(Of Decimal)
        Property NUMSEQMSGCREETN As Nullable(Of Decimal)
        Property DATENVMSGCREETN As Date
        Property DESASSCREETN As String

    End Class
End Namespace