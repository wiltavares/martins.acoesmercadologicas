﻿Namespace VO
    Public Class RelacaoEquipeDeComprasxGerenteNegocioServCompras

        Property CODDIVCMP As Nullable(Of Decimal)
        Property CODDRTCMP As Nullable(Of Decimal)
        Property CODGERPRD As Nullable(Of Decimal)
        Property CODGRPMERFRC As Nullable(Of Decimal)
        Property DATDSTDIVCMP As Date
        Property DESDIVCMP As String
        Property PERRDCMRGBRTFOB As Nullable(Of Decimal)
        Property VLRMNMFATMER As Nullable(Of Decimal)

    End Class
End Namespace
