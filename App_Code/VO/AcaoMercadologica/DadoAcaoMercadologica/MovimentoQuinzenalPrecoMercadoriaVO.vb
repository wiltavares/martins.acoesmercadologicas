﻿Namespace VO
    Public Class MovimentoQuinzenalPrecoMercadoriaVO

        Property VLRVTAINTMER As Nullable(Of Decimal)
        Property VLRUNTRBTMERANT As Nullable(Of Decimal)
        Property VLRUNTRBTMER As Nullable(Of Decimal)
        Property VLRUNTPCOFBR As Nullable(Of Decimal)
        Property VLRUNTPCOBRTMTZ As Nullable(Of Decimal)
        Property VLRUNTPCOBRTFOB As Nullable(Of Decimal)
        Property VLRUNTPCOBRTCSM As Nullable(Of Decimal)
        Property VLRUNTPCOBRTANT As Nullable(Of Decimal)
        Property VLRUNTPCOBRT As Nullable(Of Decimal)
        Property VLRMEDVBAPCOMERANT As Nullable(Of Decimal)
        Property VLRMEDVBAPCOMER As Nullable(Of Decimal)
        Property VLRMEDBNFPCOMERANT As Nullable(Of Decimal)
        Property VLRMEDBNFPCOMER As Nullable(Of Decimal)
        Property VLRICMMEDSMRECCSFR As Nullable(Of Decimal)
        Property VLRICMMEDMERSMR As Nullable(Of Decimal)
        Property VLRFTRMCDPCL As Nullable(Of Decimal)
        Property VLRCSTUNTMERMTZ As Nullable(Of Decimal)
        Property VLRCSTUNTMERANT As Nullable(Of Decimal)
        Property VLRCSTUNTMER As Nullable(Of Decimal)
        Property VLRCSTPSORLCVND As Nullable(Of Decimal)
        Property TIPMER As Nullable(Of Decimal)
        Property TIPFILEMPSBTTBTPCO As Nullable(Of Decimal)
        Property QDEQTAMERLIVPCO As String
        Property PERTBTSOC As Nullable(Of Decimal)
        Property PERTBTIPIMER As Nullable(Of Decimal)
        Property PERTBTICMMER As Nullable(Of Decimal)
        Property PERSBTTBT As Nullable(Of Decimal)
        Property PERMRGMER As Nullable(Of Decimal)
        Property PERMRGCPLMER As Nullable(Of Decimal)
        Property PERMRGBRTMER As Nullable(Of Decimal)
        Property PERMRGADIMER As Nullable(Of Decimal)
        Property PERMAXCNSFLX As Nullable(Of Decimal)
        Property PERDSCFLXPCO As Nullable(Of Decimal)
        Property PERCSTDSP As Nullable(Of Decimal)
        Property PERCMSNORMER As Nullable(Of Decimal)
        Property NUMRLCCIDGIR As Nullable(Of Decimal)
        Property INDRTNFTETBTSOC As Nullable(Of Decimal)
        Property INDMERBLQVNDCPO As Nullable(Of Decimal)
        Property FTRUNTSBTTBTMER As Nullable(Of Decimal)
        Property FTRNVGMER As Nullable(Of Decimal)
        Property FTRAJTPSOCBAMER As Nullable(Of Decimal)
        Property FTRAJTCSTDTBMER As Nullable(Of Decimal)
        Property FLGUTZDIFICM As String
        Property FLGMERICTVND As String
        Property FLGMEREXVMIX As String
        Property FLGMERESGLIVPCO As String
        Property FLGMERCMSADI As String
        Property DATREFPCO As Nullable(Of Date)
        Property DATREFLIVPCO As Nullable(Of Date)
        Property CODTBTICMMER As Nullable(Of Decimal)
        Property CODSMBSITMER As String
        Property CODMER As Nullable(Of Decimal)
        Property CODFLXPCO As String
        Property CODFILEMPFAT As Nullable(Of Decimal)
        Property CODFILEMP As Nullable(Of Decimal)

        Property FLGMERPMC As String


        'Auxiliares
        Property VLRTOTMERISRPMC As Nullable(Of Decimal)
    End Class
End Namespace