﻿Namespace VO
    Public Class DiretoriaCompra

        Property CODDRTCMP As Nullable(Of Decimal)
        Property CODCPR As Nullable(Of Decimal)
        Property DESDRTCMP As String
        Property CODUNDESRNGC As Nullable(Of Decimal)
        Property CODDRT As Nullable(Of Decimal)
        Property DATDSTDRTCMP As Date

    End Class
End Namespace