﻿Namespace VO
    Public Class Fornecedores

        Property CODEMP As Nullable(Of Decimal)
        Property CODFRN As Nullable(Of Decimal)
        Property NOMFRN As String
        Property CODCPR As Nullable(Of Decimal)
        Property NUMTLFFRN As String
        Property ENDFRN As String
        Property CODCIDFRN As Nullable(Of Decimal)
        Property DATCADFRN As Date
        Property CODREPFRN As Nullable(Of Decimal)
        Property TIPFRTPADFRN As String
        Property DESNOMFRNCMNDDO As String
        Property FLGCMNDDOFRN As String
        Property QDEDIAATZPADFRN As Nullable(Of Decimal)
        Property TIPCALATZPAD As String
        Property NUMIDTTIPCALATZPAD As Nullable(Of Decimal)
        Property DATFCHANOFSC As Date
        Property CODGRPFRN As Nullable(Of Decimal)
        Property DATDSTDIVFRN As Date
        Property NUMSEQPCSENVINFFRN As Nullable(Of Decimal)
        Property NUMCGCFRNCMNDDOITX As String
        Property FLGCMNDDOITX As String
        Property CLFCRVABCFRN As String
        Property POSCRVABCFRN As Nullable(Of Decimal)
        Property FLGFRNFBR As String
        Property CODCSS As Nullable(Of Decimal)
        Property NOMFNTFRN As String
        Property INDATDDTOFRN As Nullable(Of Decimal)
        Property CODFRNPCPAPUARDFRN As Nullable(Of Decimal)
        Property NOMUSRCONCTOFRN As String
        Property IDTENDCREETNFINFRN As String
        Property CODBAI As Nullable(Of Decimal)
        Property CODCEP As Nullable(Of Decimal)
        Property CODIDTFRNBCOBRD As Nullable(Of Decimal)
        Property CODDIGVRFFRNBCOBRD As String
        Property TIPIDTEMPASCACOCMC As Nullable(Of Decimal)
        Property CODFRNEMPASCACOCMC As Nullable(Of Decimal)
        Property CODCRVABCFRNDRT As String
        Property NUMPOSCRVABCFRNDRT As Nullable(Of Decimal)
        Property NUMTLFATDCSM As String
        Property CODIDTFRNBCOBRA As Nullable(Of Decimal)
        Property CODDIGVRFBCOBRA As String
        Property TIPCLFFRN As Nullable(Of Decimal)
        Property INDFRNECCPGTSMN As Nullable(Of Decimal)
        Property CODPCSWMS As String
        Property DATHRAISRMOVITFWMS As Date
        Property PERARDFRNCSTMEDVND As Nullable(Of Decimal)
        Property PERARDFRNMCOPCO As Nullable(Of Decimal)
        Property DESPCDFRN As String
        Property CODGERPRD As Nullable(Of Decimal)
        Property INDECSVLRGACDTBATC As Nullable(Of Decimal)
        Property INDSUSDSCNOTFSCAUT As Nullable(Of Decimal)

    End Class
End Namespace