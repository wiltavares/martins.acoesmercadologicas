﻿Namespace VO
    Public Class RelacaoCttxClausulaxAbrangComRequisicaoDeAlocalcaoDasVerbas

        Property CODEDEABGMIX As Nullable(Of Decimal)
        Property CODREQALCVBAFRN As Nullable(Of Decimal)
        Property NUMCSLCTTFRN As Nullable(Of Decimal)
        Property NUMCTTFRN As Nullable(Of Decimal)
        Property TIPEDEABGMIX As Nullable(Of Decimal)

    End Class
End Namespace
