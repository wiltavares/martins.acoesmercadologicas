﻿Namespace VO
    Public Class RelacaoClausulaComContratoDeFornecimento

        Property DATDSTCSL As Date
        Property DATINIVGRCSLCTTFRN As Date
        Property DATREFAPUPODCSLCTT As Date
        Property DESOBSREFCSL As String
        Property FLGDSCAUTPED As String
        Property FLGGRCACOCMC As String
        Property INDCALGRPFRNASC As Nullable(Of Decimal)
        Property NUMCSLCTTFRN As Nullable(Of Decimal)
        Property NUMCTTFRN As Nullable(Of Decimal)
        Property NUMDIAVNCACOCMC As Nullable(Of Decimal)
        Property PERDISCNTCRRFRN As Nullable(Of Decimal)
        Property PERITEVARMIX As Nullable(Of Decimal)
        Property QDEITEVARMIX As Nullable(Of Decimal)
        Property QDEMESAPUMEDPOD As Nullable(Of Decimal)
        Property QDEPEDPODAPUCSL As Nullable(Of Decimal)
        Property TIPDSCPGTFVC As Nullable(Of Decimal)
        Property TIPDSNDSCBNFFRN As Nullable(Of Decimal)
        Property TIPDSNDSCBNFMRT As Nullable(Of Decimal)
        Property TIPFRMDSCBNF As Nullable(Of Decimal)
        Property TIPLIMCALPOD As Nullable(Of Decimal)
        Property TIPMOE As Nullable(Of Decimal)
        Property VLRLIMFATCALPOD As Nullable(Of Decimal)

    End Class
End Namespace
