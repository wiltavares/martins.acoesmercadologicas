﻿Namespace VO
    Public Class TipoDeAlocacaoDeVerbasDoFornecedor

        Property DESALCVBAFRN As String
        Property INDUTZVBACNTCRR As Nullable(Of Decimal)
        Property INDUTZVBAMKT As Nullable(Of Decimal)
        Property INDUTZVBAMRGCRB As Nullable(Of Decimal)
        Property INDUTZVBARST As Nullable(Of Decimal)
        Property TIPALCVBAFRN As Nullable(Of Decimal)
        Property TIPDSNDSCBNF As Nullable(Of Decimal)

    End Class
End Namespace