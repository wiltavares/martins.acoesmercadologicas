﻿Namespace VO
    Public Class MovimentoDiarioDePromessaSistema

        Property CODEMP As Nullable(Of Decimal)
        Property CODFILEMP As Nullable(Of Decimal)
        Property CODPMS As Nullable(Of Decimal)
        Property DATNGCPMS As Date
        Property CODSITPMS As Nullable(Of Decimal)
        Property NUMPEDCMP As Nullable(Of Decimal)
        Property CODFRN As Nullable(Of Decimal)
        Property NOMACSUSRSIS As String
        Property DESMSGUSR As String
        Property NOMCTOFRN As String
        Property NUMTLFCTOFRN As String
        Property DESCGRCTOFRN As String
        Property DATEFTPMS As Date
        Property DATCNCPED As Date
        Property NOMUSRCNCPED As String
        Property DATCADPMS As Date
        Property INDASCARDFRNPMS As Nullable(Of Decimal)
        Property INDTRNVLRARDCMCRCB As Nullable(Of Decimal)
        Property DESSTAARDCMC As String

    End Class
End Namespace