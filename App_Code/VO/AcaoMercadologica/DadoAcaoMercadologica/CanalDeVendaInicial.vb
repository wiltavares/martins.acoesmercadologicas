﻿Namespace VO
    Public Class CanalDeVendaInicial

        Property CODCNLVNDINI As Nullable(Of Decimal)
        Property DESCNLVND As String
        Property CODUSRCAD As Nullable(Of Decimal)
        Property DATHRACAD As DateTime  'TIMESTAMP(6)
        Property CODUSRALT As Nullable(Of Decimal)
        Property DATHRAALT As DateTime  'TIMESTAMP(6)
        Property CODUSRDST As Nullable(Of Decimal)
        Property DATHRADST As DateTime 'TIMESTAMP(6)
        Property TIPCNLVND As Nullable(Of Decimal)
        Property DESABVCNLVND As String

    End Class
End Namespace