﻿Namespace VO
    Public Class RelacaoComContratoDeFornecimentoeAbragenciaDoMix

        Property CODEDEABGMIX As Nullable(Of Decimal)
        Property FLGCALVLRRCBFXACRS As String
        Property FLGFXAAVLPER As String
        Property FLGMERECS As String
        Property FLGTRTMERNVO As String
        Property INDGRCBSEANT As Nullable(Of Decimal)
        Property INDINCICMAPUCTT As Nullable(Of Decimal)
        Property INDINCIPIAPUCTT As Nullable(Of Decimal)
        Property INDINCPISAPUCTT As Nullable(Of Decimal)
        Property INDINCSBTTBTAPUCTT As Nullable(Of Decimal)
        Property INDSUSAPUARDFRN As Nullable(Of Decimal)
        Property NUMCSLCTTFRN As Nullable(Of Decimal)
        Property NUMCTTFRN As Nullable(Of Decimal)
        Property PERFIXAPUVLRRCB As Nullable(Of Decimal)
        Property PERORIFIXAPUVLRRCB As Nullable(Of Decimal)
        Property TIPBSECALAPUIDCCRS As Nullable(Of Decimal)
        Property TIPBSECALAPUVLRRCB As Nullable(Of Decimal)
        Property TIPBSECALVLRFATMNM As Nullable(Of Decimal)
        Property TIPEDEABGMIX As Nullable(Of Decimal)
        Property VLRFATMNMCOBFRN As Nullable(Of Decimal)
        Property VLRFIXAPUVLRRCB As Nullable(Of Decimal)

    End Class
End Namespace
