﻿Namespace VO
    Public Class RequisicaoDeAlocacoaDeVerbasDoFornecedor

        Property CODFNCATUAPVALCVBA As Nullable(Of Decimal)
        Property CODFNCCADALCVBAFRN As Nullable(Of Decimal)
        Property CODREQALCVBAFRN As Nullable(Of Decimal)
        Property CODSTAALCVBAFRN As Nullable(Of Decimal)
        Property DATAPVALCVBAFRN As Date
        Property DATCADALCVBAFRN As Date

    End Class
End Namespace
