﻿Imports Microsoft.VisualBasic

Namespace VO
    Public Class CriterioAdicionalAvaliacaoPromocao
        Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
        Property CODPMC As Nullable(Of Decimal)
        Property CODCTRAVLPMC As Nullable(Of Decimal)
        Property FTRACRCTRAVLPMC As Nullable(Of Decimal)
        Property VLRLIMMAXCTRAVLPMC As Nullable(Of Decimal)
        Property QDEMNMITEPEDPMC As Nullable(Of Decimal)
    End Class

End Namespace
