﻿Namespace VO
    Public Class GestaoDaCarteiraDePedidosDeCompras

        Property CODEMP As Nullable(Of Decimal)
        Property CODCPR As Nullable(Of Decimal)
        Property CODFNC As Nullable(Of Decimal)
        Property DESSGLGERPRD As String
        Property CODGERPRD As Nullable(Of Decimal)
        Property CODFILEMP As Nullable(Of Decimal)
        Property TIPFUNCPR As Nullable(Of Decimal)
        Property DATDSTCPR As Date

    End Class
End Namespace