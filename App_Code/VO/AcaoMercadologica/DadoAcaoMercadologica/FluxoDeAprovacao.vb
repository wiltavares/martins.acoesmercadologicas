﻿Namespace VO
    Public Class FluxoDeAprovacao : Implements IDisposable


        Property CODSISINF As Nullable(Of Decimal)
        Property NUMFLUAPV As Nullable(Of Decimal)
        Property NUMSEQFLUAPV As Nullable(Of Decimal)
        Property DATHRAFLUAPV As DateTime 'TimeStamp
        Property NUMSEQNIVAPV As Nullable(Of Decimal)
        Property CODEDEAPV As Nullable(Of Decimal)
        Property TIPSTAFLUAPV As String
        Property DATHRAAPVFLU As DateTime 'TimeStamp
        Property DESOBSAPV As String
        Property DESMTVAPVFLUACOCMC As String
        Property NUMSEQFLUAPVPEDOPN As Nullable(Of Decimal)
        Property CODEDEARZ As Nullable(Of Decimal)


#Region "Campos auxiliares"

        Property NOMUSRRCF As String
        Property TIPFRMDSCBNF As Nullable(Of Decimal)
        Property DESFRMDSCBNF As String

        Property CODTIPAPV As Nullable(Of Decimal)
        Property CODFNCAPVFIX As Nullable(Of Decimal)



#End Region

        Public Sub Dispose() Implements IDisposable.Dispose
            GC.SuppressFinalize(Me)
        End Sub
    End Class
End Namespace