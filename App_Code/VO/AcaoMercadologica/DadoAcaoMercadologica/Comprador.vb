﻿Namespace VO
    Public Class Comprador

        Property CODCPR As Nullable(Of Decimal)
        Property NOMCPR As String
        Property CODGERPRD As Nullable(Of Decimal)
        Property DESSGLGERPRD As String
        Property DATDSTCPR As Date
    End Class
End Namespace