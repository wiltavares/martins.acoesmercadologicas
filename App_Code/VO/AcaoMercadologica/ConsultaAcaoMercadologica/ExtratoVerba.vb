﻿Namespace VO
    Public Class ExtratoVerba

        Property CODFRN As Nullable(Of Decimal)
        Property NOMFRN As String
        Property CODCPR As Nullable(Of Decimal)
        Property NOMCPR As String

        'CELULA
        Property CODDIVCMP As Nullable(Of Decimal)
        Property DESDIVCMP As String
        'DIRETORIA
        Property CODDRTCMP As Nullable(Of Decimal)
        Property DESDRTCMP As String

        Property TIPDSNDSCBNF As Nullable(Of Decimal)
        Property DESDSNDSCBNF As String
        Property VLRSLD As Nullable(Of Decimal)
        Property VLRSLDRSV As Nullable(Of Decimal)






        Property VLRMOV As Nullable(Of Decimal)


        Property TIPEVTACOCMC As Nullable(Of Decimal)
        Property NOMTIPEVTACOCMC As String
        Property CODEVTACOCMC As Nullable(Of Decimal)
        Property NOMEVTACOCMC As String
        Property CODACOCMC As Nullable(Of Decimal)
        Property NOMACOCMC As String
        Property DATAPVACOCMC As Nullable(Of DateTime)
        Property VLRPTCFRNACOCMC As Nullable(Of Decimal)


        Property DT_OCORRENCIA As Nullable(Of DateTime)
        Property IDTMOV As String
        Property TIPO_MOV As String






        Property Celula As Nullable(Of Decimal)
        Property CodEvento As Nullable(Of Decimal)
        Property NomeEvento As String
        Property TipoOperacao As String
        Property DataIni As Date
        Property DataFim As Date
        Property OBSERVACAO As String
        Property Cod_Filtro As Nullable(Of Decimal)
        Property CODGRPMERFRC As Nullable(Of Decimal)
        Property DATDSTDIVCMP As Date
        Property PERRDCMRGBRTFOB As Nullable(Of Decimal)
        Property VLRMNMFATMER As Nullable(Of Decimal)

    End Class
End Namespace
