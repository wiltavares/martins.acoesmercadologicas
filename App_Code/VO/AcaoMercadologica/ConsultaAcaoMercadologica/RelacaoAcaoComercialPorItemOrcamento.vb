﻿Namespace VO
    Public Class RelacaoAcaoComercialPorItemOrcamento

        Property CODACOCMC As Nullable(Of Decimal)
        Property NUMLNHITEOCDACOCMC As Nullable(Of Decimal)
        Property CODITEOCDACOCMC As String
        Property QDEITEOCDACOCMC As Nullable(Of Decimal)
        Property VLRUNTITEOCDACOCMC As Nullable(Of Decimal)
        Property DATUTZITEACOCMC As Nullable(Of Date)
        Property DESFRMUTZITEACOCMC As String
        Property CODOPEFSCDSNACOCMC As Nullable(Of Decimal)
        Property VLRUTZOPEDSNACOCMC As Nullable(Of Decimal)
        Property QDEUTZOPEDSNACOCMC As Nullable(Of Decimal)
        Property CODMER As Nullable(Of Decimal)
        Property CODUNDESRNGCOPEDSN As Nullable(Of Decimal)
        Property CODFILEMP As Nullable(Of Decimal)
        Property QDEDIACMPITEACOCMC As Nullable(Of Decimal)
        Property CODCENCST As Nullable(Of Decimal)
        Property NUMCGCEMPFRN As String
        Property IDTEMPFRNITERSU As Nullable(Of Decimal)
        Property IDTLCLEMPFRNITERSU As Nullable(Of Decimal)
        Property IDTTERPGTITERSU As Nullable(Of Decimal)
        Property CODIDTTSC As Nullable(Of Decimal)
        Property IDTCNTCTB As Nullable(Of Decimal)
        Property INDITEREQRSUVLRMNM As Nullable(Of Decimal)
        Property DESOBSCPR As String
        Property NUMCTTFRN As Nullable(Of Decimal)
        Property IDTITERSU As Nullable(Of Decimal)
        Property NUMLNHCTTRSU As Nullable(Of Decimal)
        Property DESCTTRSU As String
        Property VLRPTCCLIITEACOCMC As Nullable(Of Decimal)
        Property VLRPTCFRNITEACOCMC As Nullable(Of Decimal)
        Property CODSTAITERSU As Nullable(Of Decimal)
        Property INDITERSUARZPRVPGT As Nullable(Of Decimal)
        Property INDITERSUFRNUNC As Nullable(Of Decimal)

#Region "Campos Auxiliares"

        Property NOMFILEMP As String
        Property DESOPEFSCDSNACOCMC As String
        Property INDTIPOPEDSNACOCMC As Nullable(Of Decimal)
        Property INDBDEOPEDSNACOCMC As Nullable(Of Decimal)
        Property INDUNDESRNGCACOCMC As Nullable(Of Decimal)
        Property DESUNDESRNGC As String
        Property CODFRNPCPMER As Nullable(Of Decimal)
        Property INDGRCITEENCFOLPGT As Nullable(Of Decimal)
        Property INDGRCREQAUTITERSU As Nullable(Of Decimal)
        Property SALDO As Nullable(Of Decimal)
        Property DESSTAITERSU As String
        Property Checked As Boolean
        Property IndUtzIte As Nullable(Of Decimal)
        Property NUMRMLTLF As String

        Property VLRITEOCDACOCMC As Nullable(Of Decimal)

        Property INDTIPOPEREQVGM As Nullable(Of Decimal)
        Property INDTIPOPESLCPGT As Nullable(Of Decimal)
        Property INDTIPOPEREQEVT As Nullable(Of Decimal)


        Property VLRTOTMERISRPMC As Nullable(Of Decimal)

#End Region

    End Class
End Namespace

