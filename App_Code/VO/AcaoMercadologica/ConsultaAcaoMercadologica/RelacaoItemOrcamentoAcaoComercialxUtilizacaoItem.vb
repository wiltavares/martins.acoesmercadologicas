﻿Namespace VO
    Public Class RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem

        Property CODACOCMC As Nullable(Of Decimal)
        Property NUMLNHITEOCDACOCMC As Nullable(Of Decimal)
        Property NUMSEQUTZITEOCD As Nullable(Of Decimal)
        Property CODUTZITEOCDACOCMC As Nullable(Of Decimal)
        Property DESUTZITEOCDACOCMC As String
        Property QDEUTZITEOCDACOCMC As Nullable(Of Decimal)
        Property VLRUTZITEOCDACOCMC As Nullable(Of Decimal)
        Property CODFNC As Nullable(Of Decimal)
        Property DATGRCRLC As Date
        Property INDTIPISRUTZITEOCD As Nullable(Of Decimal)

        Property NOMFNC As String
    End Class
End Namespace