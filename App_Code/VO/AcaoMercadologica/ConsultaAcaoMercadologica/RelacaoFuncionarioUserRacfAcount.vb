﻿Namespace VO
    Public Class RelacaoFuncionarioUserRacfAcount

        Property CODFNC As Nullable(Of Decimal)
        Property NOMUSRRCF As String
        Property CODACC As String
        Property ENDUSROFF As String
        Property NOMUSRENDOFF As String
        Property FLGUSROFF As String
        Property FLGRCBMSGEQIVND As Nullable(Of Integer)
        Property TIPACSUSR As Nullable(Of Integer)

    End Class
End Namespace