﻿Namespace VO
    Public Class AcaoComercial

        Property CODACOCMC As Decimal?
        Property NOMACOCMC As String
        Property TIPDSNDSCBNF As Decimal?
        Property DATINIACOCMC As Date?
        Property DESOBJACOCMC As String
        Property INDACOCMCCPL As Decimal
        Property VLRTOTACOCMC As Decimal
        Property VLRTOTOCDACOCMC As Decimal
        Property VLRTOTTXAADMACOCMC As Decimal
        Property VLRTOTBDEACOCMC As Decimal
        Property CODFNCCRIACOCMC As Decimal?
        Property DATAPVACOCMC As Date?
        Property TIPFLUAPVACOCMC As Decimal?
        Property CODSTAAPVACOCMC As Decimal?
        Property CODFNCAPVATUACOCMC As Decimal?
        Property CODACOCMCORI As Decimal?
        Property DATFIMACOCMC As Date?
        Property DATGRCACOCMC As Date?
        Property INDALTFRNACOCMC As Decimal?
        Property CODCNLVNDINI As Decimal?
        Property TIPACOMCD As Decimal?
        Property CODFILEMPORIVBA As Nullable(Of Decimal)
        Property QDEDIARCBPMS As Integer
        Property TIPFRMDSCBNF As Integer
        Property DESCPHBTB As String
        Property INDVCLPMC As Nullable(Of Decimal)

        Property INDACOCMCBTBVLR As Nullable(Of Decimal)

        Property CODACOCMCCPI As Nullable(Of Decimal)

        Property TIPDSNDSPCBK As Decimal?
        Property CODEVTBTB As Nullable(Of Decimal)


#Region "Campos Auxiliares"
        Property DESCSTA As String
        Property NOMFNC As String
        Property NOMFNCAPVATU As String
        Property NOMUSRRCF As String
        Property CODEVTACOCMC As Nullable(Of Decimal)
        Property NOMEVTACOCMC As String
        Property TIPEVTACOCMC As Nullable(Of Decimal)
        Property NOMTIPEVTACOCMC As String
        Property INDVLDRGRICT As Decimal

        Property PlanosDeMarketing As List(Of RelacaoAcaoComercialxPlanoMarketing)
        Property Fornecedores As List(Of RelacaoAcaoComercialxFornecedores)
        Property ItensOrcamento As List(Of RelacaoAcaoComercialPorItemOrcamento)
        Property ItensSubstituidos As List(Of RelacaoAcaoComercialPorItemOrcamento)
        Property Carimbos As List(Of RelacaoAcaoxFornecedorxCarimbo)
        Property FluxosAprovacao As List(Of RelacaoAcaoComercialxFluxoAprovacao)
        Property Incentivos As List(Of RelacaoIncentivoxAcaoComercial)
        Property Bonificacoes As List(Of RelacaoAcaoComercialXPedidoBonificado)
        Property Premiacao As List(Of RelacaoAcaoComercialxPremiacaoExclusiva)
        Property Mecanica As RelacaoAcaoComercialxMecanicaDaAcao

        Property ValorTotal As Decimal
        Property SaldoOrcamento As Decimal
        Property ValorTotalBrinde As Decimal
        Property TaxaBrinde As Decimal
        Property ValorCliente As Decimal
        Property ValorParticipacao As Decimal
        Property SaldoReceitaCMV As Decimal
        Property TaxaAdmin As Decimal

        Property EmpenhoExa As String

        Property TotalReprovacao As Integer
        Property TotalPoliticas As Integer
        Property PoliticasDivergentes As List(Of PoliticasDaAcaoComercial)

        Property CODOPEFSCDSNACOCMC As Decimal?
        Property DESOPEFSCDSNACOCMC As String
        Property CODITE As String
        Property VLRORC As String
        Property VLRUTZ As String
        Property VLRSLD As String

        'Auxiliares para Conuslta Acao x Fornecedor
        Property CODFRN As String
        Property NOMFRN As String
        Property saldo As String
        Property ptcfrn As String
        Property SALDO_FORNECEDOR As String
        Property CODITEOCDACOCMC As String
        Property CODMER As Decimal

        'propriedades celula
        Property CODDIVCMP As Decimal?
        Property DESDIVCMP As String

        'propriedades COMPRADOR
        Property CODCPR As Decimal?
        Property NOMCPR As String

        'propriedades TIpo operacao
        Property CODOPE As Decimal?
        Property NOMOPE As String

        Property DESDSNDSCBNF As String

        Property confirmaEnviar1 As Nullable(Of Boolean)
        Property confirmaEnviar2 As Nullable(Of Boolean)
        Property confirmaEnviarGerente As Nullable(Of Decimal)

        Property IDTEMPFRN As Nullable(Of Decimal)
        Property NUMLNHITEOCDACOCMC As Nullable(Of Decimal)
        Property IDTLCLEMPFRN As Nullable(Of Decimal)
        Property IDTTERPGTITERSU As Nullable(Of Decimal)
        Property INDITERSUARZPRVPGT As Nullable(Of Decimal)

        'Campo código da filial
        Property CODFILEMP As Nullable(Of Decimal)
        Property QTD As Nullable(Of Decimal)
        Property CodUndEsrNgc As Nullable(Of Decimal)

        Property DESFRMDSCBNF As String


        Property VLRFUNDING As Decimal?


        Property VALOR_SALDO As Nullable(Of Decimal)

        Property relacaoAcaoMecanicaList As List(Of VO.RelacaoAcaoMecanica)

        Property INDEVTSMA As Nullable(Of Decimal)

#End Region

    End Class
End Namespace
