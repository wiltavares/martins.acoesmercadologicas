﻿Namespace VO
    Public Class AcompanhamentoPreAcordo

        Property DATREFOPE As Date?
        Property TIPOPEINIGRCHST As String
        Property CODEMP As Nullable(Of Decimal)
        Property CODFILEMP As Nullable(Of Decimal)
        Property CODACOCMC As Nullable(Of Decimal)
        Property CODPMC As Nullable(Of Decimal)
        Property CODMER As Nullable(Of Decimal)
        Property VLRUNTFNDMERPMC As Nullable(Of Decimal)
        Property QDEITEPMC As Nullable(Of Decimal)
        Property CODDIVCMP As Nullable(Of Decimal)
        Property CODFRN As Nullable(Of Decimal)
        Property CODFRNPTR As Nullable(Of Decimal)
        Property NOMFRNPTR As String
        Property CODCPR As Nullable(Of Decimal)
        Property CODITEOCDACOCMC As String

        Property IndOpcao As Nullable(Of Decimal)
        Property IndVisao As Nullable(Of Decimal)
        Property TipoOperacao As String

        Property NOMACOCMC As String
        Property NOMFILEMP As String
        Property DESMER As String
        Property NOMFRN As String
        Property DESPMC As String
        Property CODCLI As Decimal
        Property NOMCLI As String
        Property DATIPRNOTFSC As Date?
        Property NUMITEORI As Decimal
        Property NUMDOCOPE As Decimal
        Property DATINIACOCMC As Date?
        Property DATFIMACOCMC As Date?

        Property DataIniAux As String
        Property DataFimAux As String
        Property QuantidadeMerc As Nullable(Of Decimal)
        Property TotalGeral As Nullable(Of Decimal)

        Property INDCPHBTB As Nullable(Of Decimal)
    End Class
End Namespace