﻿Namespace VO
    Public Class RelacaoAcaoxFornecedorxCarimbo

        Property CODACOCMC As Nullable(Of Decimal)
        Property CODFRN As Nullable(Of Decimal)
        Property CODMCOVBAFRN As Nullable(Of Decimal)
        Property VLRUTZACOCMC As Nullable(Of Decimal)

#Region "Campos Auxiliares"

        Property DATGRCMCOVBAFRN As Date
        Property DATGRCMCO As Date
        Property DATAPVACOCMC As Date
        Property CODDIVCMP As Nullable(Of Decimal)
        Property DESDIVCMP As String
        Property CELULA As String
        Property COD_FILTRO As Nullable(Of Decimal)

        Property SALDO_CARIMBO As Nullable(Of Decimal)

        Property TIPEVTACOCMC As Nullable(Of Decimal)
        Property CODEVTACOCMC As String
        Property DESOBJMCOVBAFRN As String
        Property TIPOBJMCOVBAFRN As Nullable(Of Decimal)
        Property DESOBSMCOVBAFRN As String
        Property NOMACOCMC As String
        Property NOMTIPEVTACOCMC As String
        Property NOMEVTACOCMC As String

        Property OBJETIVO As String
        Property NOMFRN As String
        Property VALOR_DISPONIVEL As Decimal
        Property OBSERVACAO As String
        Property VLRPRVMCOVBAFRN As Nullable(Of Decimal)
        Property ACAO As String
        Property FORNECEDOR As String

        Property SALDO_ACAO As Nullable(Of Decimal)
        Property PERCENTUAL_CARIMBO As Nullable(Of Decimal)
        Property VALOR_ESTORNAR As Nullable(Of Decimal)
        Property VALOR_ESTORNAR_CARIMBO As Nullable(Of Decimal)
        Property VLR_TOTAL_ACAO As Nullable(Of Decimal)
        Property vlrrlzmcovbafrn As Nullable(Of Decimal)
        Property VLRMCO As Decimal
        Property VLRUTZ As Decimal






#End Region

    End Class
End Namespace