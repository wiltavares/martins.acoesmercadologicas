﻿Namespace VO
    Public Class SaldoFornecedorMarketing

        Property CODFRN As Nullable(Of Decimal)
        Property NOMFRN As String
        Property TIPDSNDSCBNF As Nullable(Of Decimal)
        Property VLRSLDDSP As Nullable(Of Decimal)
        Property DESDSNDSCBNF As String

    End Class
End Namespace