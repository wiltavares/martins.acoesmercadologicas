﻿Namespace VO
    Public Class ConsultaAcompanhamentoBcComFunding

        Property CODGRPFRN As Decimal?
        Property NOMGRPFRN As String
        Property DATREFOPE As Date?
        Property CODBFC As Decimal?
        Property DESBFC As String
        Property DATINIVLDBFC As Date?
        Property DATFIMVLDBFC As Date?
        Property CODFILEMP As Decimal?
        Property NOMFILEMP As String
        Property CODFRN As Decimal?
        Property NOMFRN As String
        Property CODMER As Decimal?
        Property DESMER As String
        Property PERBFC As Decimal?
        Property QDEITEBFC As Decimal?
        Property VLRTOTFNDBFC As Decimal?


        ' Auxiliares
        Property DATREFOPEINI As Date?
        Property DATREFOPEFIM As Date?
        Property TIPOPEINIGRCHST As String

    End Class
End Namespace