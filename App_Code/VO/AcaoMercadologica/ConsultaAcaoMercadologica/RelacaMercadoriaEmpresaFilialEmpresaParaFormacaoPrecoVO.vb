﻿Namespace VO
    Public Class RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoVO

        Property VLRCRBMRGMER As Nullable(Of Decimal)
        Property TIPMERMTD As String
        Property TIPMER As Nullable(Of Decimal)
        Property TIPCNSFLX As Nullable(Of Decimal)
        Property QDEDIAPRZFRN As Nullable(Of Decimal)
        Property PERMRGRSTFATMERMTD As Nullable(Of Decimal)
        Property PERMRGMNMFLXPCO As Nullable(Of Decimal)
        Property PERMRGMERORI As Nullable(Of Decimal)
        Property PERMRGMERNOR As Nullable(Of Decimal)
        Property PERMRGMERINF As Nullable(Of Decimal)
        Property PERMRGADIMER As Nullable(Of Decimal)
        Property PERMAXCNSFLX As Nullable(Of Decimal)
        Property PERCRBMRGMER As Nullable(Of Decimal)
        Property PERCMSORIMER As Nullable(Of Decimal)
        Property PERCMSNORMER As Nullable(Of Decimal)
        Property PERCMSINFMER As Nullable(Of Decimal)
        Property DATDSTMER As Nullable(Of Date)
        Property CODSMBSITMERFLXPCO As String
        Property CODSMBSITMER As String
        Property CODSITCMSADI As String
        Property CODPAPMER As Nullable(Of Decimal)
        Property CODMER As Nullable(Of Decimal)
        Property CODFLXPCO As String
        Property CODFILEMP As Nullable(Of Decimal)
        Property CODEMP As Nullable(Of Decimal)

    End Class
End Namespace