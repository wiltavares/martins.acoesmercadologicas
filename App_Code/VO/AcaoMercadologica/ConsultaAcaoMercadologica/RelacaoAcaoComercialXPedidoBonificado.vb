﻿Namespace VO
    Public Class RelacaoAcaoComercialXPedidoBonificado

        Property CODACOCMC As Decimal?
        Property CODFRN As Decimal?
        Property NUMPEDCMP As Decimal?
        Property VLRUTZACOCMC As Decimal

#Region "Campos auxiliares"
        Property NOMFRN As String
        Property DESOBSPEDCMP As String
        Property DATPEDCMP As Date
        Property VERBA_DISPONIVEL As Decimal
        Property COD_FILTRO As Integer
#End Region

    End Class
End Namespace