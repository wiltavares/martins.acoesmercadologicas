﻿Namespace VO
    Public Class EstornoAcaoMercadologica

        Property CODACOCMC As Decimal?
        Property NOMACOCMC As String
        Property EMPEXAACORDO As String
        Property CODOPERACOES As String
        Property VLRTOTACOCMC As Decimal?

        Property TIPACOMCD As Decimal?
        Property VALOR_SALDO As Nullable(Of Decimal)
        Property VALOR_ESTORNAR As Nullable(Of Decimal)
        Property CODMCOVBAFRN As Nullable(Of Decimal)

        Property Fornecedores As List(Of RelacaoAcaoComercialxFornecedores)
        Property Carimbos As List(Of RelacaoAcaoxFornecedorxCarimbo)
        Property ItensOrcamento As List(Of RelacaoAcaoComercialPorItemOrcamento)

    End Class
End Namespace