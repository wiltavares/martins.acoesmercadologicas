﻿Namespace VO
    Public Class RelacaoDeSistemasxMenu

        Private _NOMSISINF As String
        Property NOMSISINF As String
            Get
                If (String.IsNullOrEmpty(_NOMSISINF)) Then
                    Return "AI"
                Else
                    Return _NOMSISINF
                End If
            End Get
            Set(value As String)
                _NOMSISINF = value
            End Set
        End Property
        Property NOMOBEPRG As String
        Property INDABRMNUFLH As Nullable(Of Decimal)
        Property DESMNU As String
        Property DESLCLAPLMNU As String
        Property DESICNMNU As String
        Property CODMNUPAI As String
        Property CODMNU As String
        Property CODFILEMP As Nullable(Of Decimal)

        Property ACS As Nullable(Of Decimal)



    End Class
End Namespace