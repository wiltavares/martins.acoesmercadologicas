﻿Namespace VO
    Public Class PoRequisitionsInterfaceAll

        'Property TRANSACTION_ID As Nullable(Of Decimal)
        'Property PROCESS_FLAG As String
        'Property REQUEST_ID As Nullable(Of Decimal)
        'Property PROGRAM_ID As Nullable(Of Decimal)
        'Property PROGRAM_APPLICATION_ID As Nullable(Of Decimal)
        'Property PROGRAM_UPDATE_DATE As Date
        'Property LAST_UPDATED_BY As Nullable(Of Decimal)
        'Property LAST_UPDATE_DATE As Date
        'Property LAST_UPDATE_LOGIN As Nullable(Of Decimal)
        Property CREATION_DATE As Date
        Property CREATED_BY As Nullable(Of Decimal)
        Property INTERFACE_SOURCE_CODE As String
        'Property INTERFACE_SOURCE_LINE_ID As Nullable(Of Decimal)
        Property SOURCE_TYPE_CODE As String
        'Property REQUISITION_HEADER_ID As Nullable(Of Decimal)
        'Property REQUISITION_LINE_ID As Nullable(Of Decimal)
        'Property REQ_DISTRIBUTION_ID As Nullable(Of Decimal)
        Property REQUISITION_TYPE As String
        Property DESTINATION_TYPE_CODE As String
        'Property ITEM_DESCRIPTION As String
        Property QUANTITY As Nullable(Of Decimal)
        Property UNIT_PRICE As Nullable(Of Decimal)
        Property AUTHORIZATION_STATUS As String
        Property BATCH_ID As Nullable(Of Decimal)
        'Property GROUP_CODE As String
        'Property DELETE_ENABLED_FLAG As String
        'Property UPDATE_ENABLED_FLAG As String
        'Property APPROVER_ID As Nullable(Of Decimal)
        'Property APPROVER_NAME As String
        'Property APPROVAL_PATH_ID As Nullable(Of Decimal)
        'Property NOTE_TO_APPROVER As String
        Property PREPARER_ID As Nullable(Of Decimal)
        Property AUTOSOURCE_FLAG As String
        'Property REQ_Number_SEGMENT1 As String
        'Property REQ_Number_SEGMENT2 As String
        'Property REQ_Number_SEGMENT3 As String
        'Property REQ_Number_SEGMENT4 As String
        'Property REQ_Number_SEGMENT5 As String
        Property HEADER_DESCRIPTION As String
        'Property HEADER_ATTRIBUTE_CATEGORY As String
        Property HEADER_ATTRIBUTE1 As String
        Property HEADER_ATTRIBUTE2 As String
        'Property HEADER_ATTRIBUTE3 As String
        'Property HEADER_ATTRIBUTE4 As String
        'Property HEADER_ATTRIBUTE5 As String
        'Property HEADER_ATTRIBUTE6 As String
        'Property HEADER_ATTRIBUTE7 As String
        'Property HEADER_ATTRIBUTE8 As String
        'Property HEADER_ATTRIBUTE9 As String
        'Property HEADER_ATTRIBUTE10 As String
        'Property HEADER_ATTRIBUTE11 As String
        'Property HEADER_ATTRIBUTE12 As String
        'Property HEADER_ATTRIBUTE13 As String
        'Property HEADER_ATTRIBUTE14 As String
        'Property URGENT_FLAG As String
        'Property HEADER_ATTRIBUTE15 As String
        'Property RFQ_REQUIRED_FLAG As String
        Property JUSTIFICATION As String
        Property NOTE_TO_BUYER As String
        'Property NOTE_TO_RECEIVER As String
        Property ITEM_ID As Nullable(Of Decimal)
        'Property ITEM_SEGMENT1 As String
        'Property ITEM_SEGMENT2 As String
        'Property ITEM_SEGMENT3 As String
        'Property ITEM_SEGMENT4 As String
        'Property ITEM_SEGMENT5 As String
        'Property ITEM_SEGMENT6 As String
        'Property ITEM_SEGMENT7 As String
        'Property ITEM_SEGMENT8 As String
        'Property ITEM_SEGMENT9 As String
        'Property ITEM_SEGMENT10 As String
        'Property ITEM_SEGMENT11 As String
        'Property ITEM_SEGMENT12 As String
        'Property ITEM_SEGMENT13 As String
        'Property ITEM_SEGMENT14 As String
        'Property ITEM_SEGMENT15 As String
        'Property ITEM_SEGMENT16 As String
        'Property ITEM_SEGMENT17 As String
        'Property ITEM_SEGMENT18 As String
        'Property ITEM_SEGMENT19 As String
        'Property ITEM_SEGMENT20 As String
        'Property ITEM_REVISION As String
        Property CHARGE_ACCOUNT_ID As Nullable(Of Decimal)
        'Property CHARGE_ACCOUNT_SEGMENT1 As String
        'Property CHARGE_ACCOUNT_SEGMENT2 As String
        'Property CHARGE_ACCOUNT_SEGMENT3 As String
        'Property CHARGE_ACCOUNT_SEGMENT4 As String
        'Property CHARGE_ACCOUNT_SEGMENT5 As String
        'Property CHARGE_ACCOUNT_SEGMENT6 As String
        'Property CHARGE_ACCOUNT_SEGMENT7 As String
        'Property CHARGE_ACCOUNT_SEGMENT8 As String
        'Property CHARGE_ACCOUNT_SEGMENT9 As String
        'Property CHARGE_ACCOUNT_SEGMENT10 As String
        'Property CHARGE_ACCOUNT_SEGMENT11 As String
        'Property CHARGE_ACCOUNT_SEGMENT12 As String
        'Property CHARGE_ACCOUNT_SEGMENT13 As String
        'Property CHARGE_ACCOUNT_SEGMENT14 As String
        'Property CHARGE_ACCOUNT_SEGMENT15 As String
        'Property CHARGE_ACCOUNT_SEGMENT16 As String
        'Property CHARGE_ACCOUNT_SEGMENT17 As String
        'Property CHARGE_ACCOUNT_SEGMENT18 As String
        'Property CHARGE_ACCOUNT_SEGMENT19 As String
        'Property CHARGE_ACCOUNT_SEGMENT20 As String
        'Property CHARGE_ACCOUNT_SEGMENT21 As String
        'Property CHARGE_ACCOUNT_SEGMENT22 As String
        'Property CHARGE_ACCOUNT_SEGMENT23 As String
        'Property CHARGE_ACCOUNT_SEGMENT24 As String
        'Property CHARGE_ACCOUNT_SEGMENT25 As String
        'Property CHARGE_ACCOUNT_SEGMENT26 As String
        'Property CHARGE_ACCOUNT_SEGMENT27 As String
        'Property CHARGE_ACCOUNT_SEGMENT28 As String
        'Property CHARGE_ACCOUNT_SEGMENT29 As String
        'Property CHARGE_ACCOUNT_SEGMENT30 As String
        'Property CATEGORY_ID As Nullable(Of Decimal)
        'Property CATEGORY_SEGMENT1 As String
        'Property CATEGORY_SEGMENT2 As String
        'Property CATEGORY_SEGMENT3 As String
        'Property CATEGORY_SEGMENT4 As String
        'Property CATEGORY_SEGMENT5 As String
        'Property CATEGORY_SEGMENT6 As String
        'Property CATEGORY_SEGMENT7 As String
        'Property CATEGORY_SEGMENT8 As String
        'Property CATEGORY_SEGMENT9 As String
        'Property CATEGORY_SEGMENT10 As String
        'Property CATEGORY_SEGMENT11 As String
        'Property CATEGORY_SEGMENT12 As String
        'Property CATEGORY_SEGMENT13 As String
        'Property CATEGORY_SEGMENT14 As String
        'Property CATEGORY_SEGMENT15 As String
        'Property CATEGORY_SEGMENT16 As String
        'Property CATEGORY_SEGMENT17 As String
        'Property CATEGORY_SEGMENT18 As String
        'Property CATEGORY_SEGMENT19 As String
        'Property CATEGORY_SEGMENT20 As String
        'Property UNIT_OF_MEASURE As String
        'Property UOM_CODE As String
        Property LINE_TYPE_ID As Nullable(Of Decimal)
        'Property LINE_TYPE As String
        'Property UN_Number_ID As Nullable(Of Decimal)
        'Property UN_Number As String
        'Property HAZARD_CLASS_ID As Nullable(Of Decimal)
        'Property HAZARD_CLASS As String
        'Property MUST_USE_SUGG_VENDOR_FLAG As String
        'Property REFERENCE_NUM As String
        'Property WIP_ENTITY_ID As Nullable(Of Decimal)
        'Property WIP_LINE_ID As Nullable(Of Decimal)
        'Property WIP_OPERATION_SEQ_NUM As Nullable(Of Decimal)
        'Property WIP_RESOURCE_SEQ_NUM As Nullable(Of Decimal)
        'Property WIP_REPETITIVE_SCHEDULE_ID As Nullable(Of Decimal)
        'Property PROJECT_NUM As String
        'Property TASK_NUM As String
        'Property EXPENDITURE_TYPE As String
        Property SOURCE_ORGANIZATION_ID As Nullable(Of Decimal)
        'Property SOURCE_ORGANIZATION_CODE As String
        'Property SOURCE_SUBINVENTORY As String
        Property DESTINATION_ORGANIZATION_ID As Nullable(Of Decimal)
        'Property DESTINATION_ORGANIZATION_CODE As String
        'Property DESTINATION_SUBINVENTORY As String
        Property DELIVER_TO_LOCATION_ID As Nullable(Of Decimal)
        'Property DELIVER_TO_LOCATION_CODE As String
        Property DELIVER_TO_REQUESTOR_ID As Nullable(Of Decimal)
        'Property DELIVER_TO_REQUESTOR_NAME As String
        'Property SUGGESTED_BUYER_ID As Nullable(Of Decimal)
        'Property SUGGESTED_BUYER_NAME As String
        'Property SUGGESTED_VENDOR_NAME As String
        'Property SUGGESTED_VENDOR_ID As Nullable(Of Decimal)
        'Property SUGGESTED_VENDOR_SITE As String
        'Property SUGGESTED_VENDOR_SITE_ID As Nullable(Of Decimal)
        'Property SUGGESTED_VENDOR_CONTACT As String
        'Property SUGGESTED_VENDOR_CONTACT_ID As Nullable(Of Decimal)
        'Property SUGGESTED_VENDOR_PHONE As String
        'Property SUGGESTED_VENDOR_ITEM_NUM As String
        'Property LINE_ATTRIBUTE_CATEGORY As String
        Property LINE_ATTRIBUTE1 As String
        Property LINE_ATTRIBUTE2 As String
        Property LINE_ATTRIBUTE3 As String
        Property LINE_ATTRIBUTE4 As String
        Property LINE_ATTRIBUTE5 As String
        Property LINE_ATTRIBUTE6 As String
        'Property LINE_ATTRIBUTE7 As String
        'Property LINE_ATTRIBUTE8 As String
        'Property LINE_ATTRIBUTE9 As String
        'Property LINE_ATTRIBUTE10 As String
        'Property LINE_ATTRIBUTE11 As String
        'Property LINE_ATTRIBUTE12 As String
        'Property LINE_ATTRIBUTE13 As String
        'Property LINE_ATTRIBUTE14 As String
        'Property LINE_ATTRIBUTE15 As String
        Property NEED_BY_DATE As Date
        'Property NOTE1_ID As Nullable(Of Decimal)
        'Property NOTE2_ID As Nullable(Of Decimal)
        'Property NOTE3_ID As Nullable(Of Decimal)
        'Property NOTE4_ID As Nullable(Of Decimal)
        'Property NOTE5_ID As Nullable(Of Decimal)
        'Property NOTE6_ID As Nullable(Of Decimal)
        'Property NOTE7_ID As Nullable(Of Decimal)
        'Property NOTE8_ID As Nullable(Of Decimal)
        'Property NOTE9_ID As Nullable(Of Decimal)
        'Property NOTE10_ID As Nullable(Of Decimal)
        'Property NOTE1_TITLE As String
        'Property NOTE2_TITLE As String
        'Property NOTE3_TITLE As String
        'Property NOTE4_TITLE As String
        'Property NOTE5_TITLE As String
        'Property NOTE6_TITLE As String
        'Property NOTE7_TITLE As String
        'Property NOTE8_TITLE As String
        'Property NOTE9_TITLE As String
        'Property NOTE10_TITLE As String
        'Property GL_DATE As Date
        'Property DIST_ATTRIBUTE_CATEGORY As String
        'Property DISTRIBUTION_ATTRIBUTE1 As String
        'Property DISTRIBUTION_ATTRIBUTE2 As String
        'Property DISTRIBUTION_ATTRIBUTE3 As String
        'Property DISTRIBUTION_ATTRIBUTE4 As String
        'Property DISTRIBUTION_ATTRIBUTE5 As String
        'Property DISTRIBUTION_ATTRIBUTE6 As String
        'Property DISTRIBUTION_ATTRIBUTE7 As String
        'Property DISTRIBUTION_ATTRIBUTE8 As String
        'Property DISTRIBUTION_ATTRIBUTE9 As String
        'Property DISTRIBUTION_ATTRIBUTE10 As String
        'Property DISTRIBUTION_ATTRIBUTE11 As String
        'Property DISTRIBUTION_ATTRIBUTE12 As String
        'Property DISTRIBUTION_ATTRIBUTE13 As String
        'Property DISTRIBUTION_ATTRIBUTE14 As String
        'Property DISTRIBUTION_ATTRIBUTE15 As String
        'Property PREPARER_NAME As String
        'Property BOM_RESOURCE_ID As Nullable(Of Decimal)
        'Property ACCRUAL_ACCOUNT_ID As Nullable(Of Decimal)
        Property VARIANCE_ACCOUNT_ID As Nullable(Of Decimal)
        'Property BUDGET_ACCOUNT_ID As Nullable(Of Decimal)
        'Property USSGL_TRANSACTION_CODE As String
        'Property GOVERNMENT_CONTEXT As String
        Property CURRENCY_CODE As String
        'Property CURRENCY_UNIT_PRICE As Nullable(Of Decimal)
        'Property RATE As Nullable(Of Decimal)
        'Property RATE_DATE As Date
        'Property RATE_TYPE As String
        'Property PREVENT_ENCUMBRANCE_FLAG As String
        Property AUTOSOURCE_DOC_HEADER_ID As Nullable(Of Decimal)
        Property AUTOSOURCE_DOC_LINE_NUM As Nullable(Of Decimal)
        'Property PROJECT_ACCOUNTING_CONTEXT As String
        'Property EXPENDITURE_ORGANIZATION_ID As Nullable(Of Decimal)
        'Property PROJECT_ID As Nullable(Of Decimal)
        'Property TASK_ID As Nullable(Of Decimal)
        'Property EXPENDITURE_ITEM_DATE As Date
        Property DOCUMENT_TYPE_CODE As String
        Property ORG_ID As Nullable(Of Decimal)
        'Property TRANSACTION_REASON_CODE As String
        'Property ALLOCATION_TYPE As String
        'Property ALLOCATION_VALUE As Nullable(Of Decimal)
        'Property MULTI_DISTRIBUTIONS As String
        'Property REQ_DIST_SEQUENCE_ID As Nullable(Of Decimal)
        'Property KANBAN_CARD_ID As Nullable(Of Decimal)
        'Property EMERGENCY_PO_NUM As String
        'Property AWARD_ID As Nullable(Of Decimal)
        'Property END_ITEM_UNIT_NUMBER As String
        'Property TAX_CODE_ID As Nullable(Of Decimal)
        'Property OKE_CONTRACT_HEADER_ID As Nullable(Of Decimal)
        'Property OKE_CONTRACT_NUM As String
        'Property OKE_CONTRACT_VERSION_ID As Nullable(Of Decimal)
        'Property OKE_CONTRACT_LINE_ID As Nullable(Of Decimal)
        'Property OKE_CONTRACT_LINE_NUM As String
        'Property OKE_CONTRACT_DELIVERABLE_ID As Nullable(Of Decimal)
        'Property OKE_CONTRACT_DELIVERABLE_NUM As String
        'Property SECONDARY_UNIT_OF_MEASURE As String
        'Property SECONDARY_UOM_CODE As String
        'Property SECONDARY_QUANTITY As Nullable(Of Decimal)
        'Property PREFERRED_GRADE As String
        'Property VMI_FLAG As String
        'Property TAX_USER_OVERRIDE_FLAG As String
        'Property AMOUNT As Nullable(Of Decimal)
        'Property CURRENCY_AMOUNT As Nullable(Of Decimal)
        'Property SHIP_METHOD As String
        'Property ESTIMATED_PICKUP_DATE As Date
        'Property BASE_UNIT_PRICE As Nullable(Of Decimal)
        'Property NEGOTIATED_BY_PREPARER_FLAG As String
        'Property TAX_NAME As String
    End Class
End Namespace