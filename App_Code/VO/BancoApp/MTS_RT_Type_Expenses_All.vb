﻿Namespace VO
    Public Class MTS_RT_Type_Expenses_All

        Property TYPE_EXPENSE_ID As Nullable(Of Integer)
        Property TYPE_EXPENSE_CODE As String
        Property DESCRIPTION As String
        Property RESTRICTION_ADVANCING_FLAG As String
        Property INVENTORY_ITEM_ID As Nullable(Of Integer)
        Property ATTRIBUTE1 As String
        Property ATTRIBUTE2 As String
        Property ATTRIBUTE3 As String
        Property ATTRIBUTE4 As String
        Property ATTRIBUTE5 As String
        Property ATTRIBUTE6 As String
        Property ATTRIBUTE7 As String
        Property ATTRIBUTE8 As String
        Property ATTRIBUTE9 As String
        Property ATTRIBUTE10 As String
        Property ATTRIBUTE11 As String
        Property ATTRIBUTE12 As String
        Property ATTRIBUTE13 As String
        Property ATTRIBUTE14 As String
        Property ATTRIBUTE15 As String
        Property ORG_ID As Nullable(Of Integer)
        Property CREATED_BY As Nullable(Of Integer)
        Property CREATION_DATE As Nullable(Of DateTime)
        Property LAST_UPDATED_BY As Nullable(Of Integer)
        Property LAST_UPDATE_DATE As Nullable(Of DateTime)
        Property LAST_UPDATE_LOGIN As Nullable(Of Integer)
        Property RESTRICTION_CHECKING_FLAG As String
        Property INVOICING_FLAG As String
        Property REQUISITION_TYPE As String
        Property MANDATORY_POLICY_FLAG As String
        Property RESTRICTION_REQUEST_OFF_FLAG As String

    End Class
End Namespace
