﻿Namespace VO
    Public Class MTS_RT_Orc_Budge_Actions_All
        Property ACTIONS_ID As Nullable(Of Integer)
        Property LAST_UPDATED_BY As Nullable(Of Integer)
        Property LAST_UPDATE_DATE As Nullable(Of DateTime)
        Property CREATION_BY As Nullable(Of Integer)
        Property CREATION_DATE As Nullable(Of DateTime)
        Property ACTIONS_NUMBER As Nullable(Of Integer)
        Property ACTIONS_DESTINATION_TYPE As String
        Property TYPE_EXPENSE_CODE As String
        Property ACTIONS_STATUS_FLAG As String
        Property BUDGE_AMOUNT_FORECASTED As Nullable(Of Integer)
        Property BUDGE_AMOUNT_REALIZED As Nullable(Of Integer)
        Property ORG_ID As Nullable(Of Integer)
    End Class
End Namespace