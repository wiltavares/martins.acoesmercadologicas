﻿Imports Core
Imports VO
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Globalization

Public Class perAllPeopleFDAO
    Inherits DAOBase(Of VO.PER_ALL_PEOPLE_F)

    Public Sub New()
        daoConfiguration = New perAllPeopleFDAOConfiguration()
    End Sub


    Public Function PsqCadFnc(CodFnc As Decimal) As List(Of VO.PER_ALL_PEOPLE_F)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.PERSON_ID
                                          , A.EMPLOYEE_NUMBER 
                                          , A.FULL_NAME
                                          , C.LOCATION_ID
                                          , C.INVENTORY_ORGANIZATION_ID
                                        FROM APPS.PER_ALL_PEOPLE_F A
                                           , APPS.PER_ALL_ASSIGNMENTS_F B
                                           , APPS.HR_LOCATIONS_V C
                                       WHERE A.EMPLOYEE_NUMBER  = :CODFNC
                                         AND A.PERSON_ID = B.PERSON_ID
                                         AND B.LOCATION_ID = C.LOCATION_ID
                                         AND SYSDATE BETWEEN A.EFFECTIVE_START_DATE AND A.EFFECTIVE_END_DATE
                                         AND SYSDATE BETWEEN B.EFFECTIVE_START_DATE AND B.EFFECTIVE_END_DATE]]>.Value

        parameterMapper.Parameters.Add(":CODFNC", CodFnc.ToString)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql), BancoApp:=True)

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="CodFnc">'No banco app o campo e varchar. </param>
    ''' <returns></returns>
    Public Function CsnIdtUsu(CodFnc As String) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT FU.USER_ID
                                      FROM APPS.PER_ALL_PEOPLE_F PAPF, APPS.FND_USER FU  
                                     WHERE SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE AND 
                                           PAPF.EFFECTIVE_END_DATE                       
                                       AND PAPF.PERSON_ID = FU.EMPLOYEE_ID               
                                       AND PAPF.EMPLOYEE_NUMBER = :CODFNC]]>.Value

        parameterMapper.Parameters.Add(":CODFNC", CodFnc)
        Return connector.Execute(sql, parameterMapper, BancoApp:=True)

    End Function

End Class

<Table("apps.PER_ALL_PEOPLE_F")>
Public Class perAllPeopleFDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property PERSON_ID As Nullable(Of Decimal)
    <PrimaryKey>
    Property EFFECTIVE_START_DATE As Nullable(Of Date)
    <PrimaryKey>
    Property EFFECTIVE_END_DATE As Nullable(Of Decimal)
    Property BUSINESS_GROUP_ID As Nullable(Of Decimal)
    Property PERSON_TYPE_ID As Nullable(Of Decimal)
    Property LAST_NAME As String
    Property START_DATE As Nullable(Of Decimal)
    Property APPLICANT_NUMBER As String
    Property BACKGROUND_CHECK_STATUS As String
    Property BACKGROUND_DATE_CHECK As Nullable(Of Decimal)
    Property BLOOD_TYPE As String
    Property COMMENT_ID As Nullable(Of Decimal)
    Property CORRESPONDENCE_LANGUAGE As String
    Property CURRENT_APPLICANT_FLAG As String
    Property CURRENT_EMP_OR_APL_FLAG As String
    Property CURRENT_EMPLOYEE_FLAG As String
    Property DATE_EMPLOYEE_DATA_VERIFIED As Nullable(Of Decimal)
    Property DATE_OF_BIRTH As Nullable(Of Decimal)
    Property EMAIL_ADDRESS As String
    Property EMPLOYEE_NUMBER As String
    Property EXPENSE_CHECK_SEND_TO_ADDRESS As String
    Property FAST_PATH_EMPLOYEE As String
    Property FIRST_NAME As String
    Property FTE_CAPACITY As Nullable(Of Decimal)
    Property FULL_NAME As String
    Property HOLD_APPLICANT_DATE_UNTIL As Nullable(Of Decimal)
    Property HONORS As String
    Property INTERNAL_LOCATION As String
    Property KNOWN_AS As String
    Property LAST_MEDICAL_TEST_BY As String
    Property LAST_MEDICAL_TEST_DATE As Nullable(Of Decimal)
    Property MAILSTOP As String
    Property MARITAL_STATUS As String
    Property MIDDLE_NAMES As String
    Property NATIONALITY As String
    Property NATIONAL_IDENTIFIER As String
    Property OFFICE_NUMBER As String
    Property ON_MILITARY_SERVICE As String
    Property ORDER_NAME As String
    Property PRE_NAME_ADJUNCT As String
    Property PREVIOUS_LAST_NAME As String
    Property PROJECTED_START_DATE As Nullable(Of Decimal)
    Property REHIRE_AUTHORIZOR As String
    Property REHIRE_REASON As String
    Property REHIRE_RECOMMENDATION As String
    Property RESUME_EXISTS As String
    Property RESUME_LAST_UPDATED As Nullable(Of Decimal)
    Property REGISTERED_DISABLED_FLAG As String
    Property SECOND_PASSPORT_EXISTS As String
    Property SEX As String
    Property STUDENT_STATUS As String
    Property SUFFIX As String
    Property TITLE As String
    Property VENDOR_ID As Nullable(Of Decimal)
    Property WORK_SCHEDULE As String
    Property WORK_TELEPHONE As String
    Property COORD_BEN_MED_PLN_NO As String
    Property COORD_BEN_NO_CVG_FLAG As String
    Property DPDNT_ADOPTION_DATE As Nullable(Of Decimal)
    Property DPDNT_VLNTRY_SVCE_FLAG As String
    Property RECEIPT_OF_DEATH_CERT_DATE As Nullable(Of Decimal)
    Property USES_TOBACCO_FLAG As String
    Property BENEFIT_GROUP_ID As Nullable(Of Decimal)
    Property REQUEST_ID As Nullable(Of Decimal)
    Property PROGRAM_APPLICATION_ID As Nullable(Of Decimal)
    Property PROGRAM_ID As Nullable(Of Decimal)
    Property PROGRAM_UPDATE_DATE As Nullable(Of Decimal)
    Property ATTRIBUTE_CATEGORY As String
    Property ATTRIBUTE1 As String
    Property ATTRIBUTE2 As String
    Property ATTRIBUTE3 As String
    Property ATTRIBUTE4 As String
    Property ATTRIBUTE5 As String
    Property ATTRIBUTE6 As String
    Property ATTRIBUTE7 As String
    Property ATTRIBUTE8 As String
    Property ATTRIBUTE9 As String
    Property ATTRIBUTE10 As String
    Property ATTRIBUTE11 As String
    Property ATTRIBUTE12 As String
    Property ATTRIBUTE13 As String
    Property ATTRIBUTE14 As String
    Property ATTRIBUTE15 As String
    Property ATTRIBUTE16 As String
    Property ATTRIBUTE17 As String
    Property ATTRIBUTE18 As String
    Property ATTRIBUTE19 As String
    Property ATTRIBUTE20 As String
    Property ATTRIBUTE21 As String
    Property ATTRIBUTE22 As String
    Property ATTRIBUTE23 As String
    Property ATTRIBUTE24 As String
    Property ATTRIBUTE25 As String
    Property ATTRIBUTE26 As String
    Property ATTRIBUTE27 As String
    Property ATTRIBUTE28 As String
    Property ATTRIBUTE29 As String
    Property ATTRIBUTE30 As String
    Property LAST_UPDATE_DATE As Nullable(Of Decimal)
    Property LAST_UPDATED_BY As Nullable(Of Decimal)
    Property LAST_UPDATE_LOGIN As Nullable(Of Decimal)
    Property CREATED_BY As Nullable(Of Decimal)
    Property CREATION_DATE As Nullable(Of Decimal)
    Property PER_INFORMATION_CATEGORY As String
    Property PER_INFORMATION1 As String
    Property PER_INFORMATION2 As String
    Property PER_INFORMATION3 As String
    Property PER_INFORMATION4 As String
    Property PER_INFORMATION5 As String
    Property PER_INFORMATION6 As String
    Property PER_INFORMATION7 As String
    Property PER_INFORMATION8 As String
    Property PER_INFORMATION9 As String
    Property PER_INFORMATION10 As String
    Property PER_INFORMATION11 As String
    Property PER_INFORMATION12 As String
    Property PER_INFORMATION13 As String
    Property PER_INFORMATION14 As String
    Property PER_INFORMATION15 As String
    Property PER_INFORMATION16 As String
    Property PER_INFORMATION17 As String
    Property PER_INFORMATION18 As String
    Property PER_INFORMATION19 As String
    Property PER_INFORMATION20 As String
    Property PER_INFORMATION21 As String
    Property PER_INFORMATION22 As String
    Property PER_INFORMATION23 As String
    Property PER_INFORMATION24 As String
    Property PER_INFORMATION25 As String
    Property PER_INFORMATION26 As String
    Property PER_INFORMATION27 As String
    Property PER_INFORMATION28 As String
    Property PER_INFORMATION29 As String
    Property PER_INFORMATION30 As String
    Property OBJECT_VERSION_NUMBER As Nullable(Of Decimal)
    Property DATE_OF_DEATH As Nullable(Of Decimal)
    Property ORIGINAL_DATE_OF_HIRE As Nullable(Of Decimal)
    Property TOWN_OF_BIRTH As String
    Property REGION_OF_BIRTH As String
    Property COUNTRY_OF_BIRTH As String
    Property GLOBAL_PERSON_ID As String
    Property COORD_BEN_MED_PL_NAME As String
    Property COORD_BEN_MED_INSR_CRR_NAME As String
    Property COORD_BEN_MED_INSR_CRR_IDENT As String
    Property COORD_BEN_MED_EXT_ER As String
    Property COORD_BEN_MED_CVG_STRT_DT As Nullable(Of Decimal)
    Property COORD_BEN_MED_CVG_END_DT As Nullable(Of Decimal)
    Property PARTY_ID As Nullable(Of Decimal)
    Property NPW_NUMBER As String
    Property CURRENT_NPW_FLAG As String
    Property GLOBAL_NAME As String
    Property LOCAL_NAME As String

    <IgnoreMap>
    Property LOCATION_ID As Nullable(Of Decimal)
    <IgnoreMap>
    Property INVENTORY_ORGANIZATION_ID As Nullable(Of Decimal)

End Class
