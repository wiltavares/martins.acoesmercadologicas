﻿Imports Core
Imports VO
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Globalization

Public Class PoRequisitionsInterfaceAllDAO
    Inherits DAOBase(Of VO.PoRequisitionsInterfaceAll)

    Public Sub New()
        daoConfiguration = New PoRequisitionsInterfaceAllDAOConfiguration()
    End Sub
    Public Sub Insert(ByVal vo As VO.PoRequisitionsInterfaceAll)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[INSERT into apps.po_requisitions_interface_all
                                          ( CREATION_DATE
                                          , CREATED_BY
                                          , INTERFACE_SOURCE_CODE
                                          , HEADER_ATTRIBUTE2
                                          , LINE_ATTRIBUTE1
                                          , LINE_ATTRIBUTE2
                                          , LINE_ATTRIBUTE3
                                          , LINE_ATTRIBUTE4
                                          , LINE_ATTRIBUTE5
                                          , LINE_ATTRIBUTE6 
                                          , SOURCE_TYPE_CODE
                                          , REQUISITION_TYPE
                                          , DESTINATION_TYPE_CODE
                                          , QUANTITY
                                          , UNIT_PRICE
                                          , AUTHORIZATION_STATUS
                                          , PREPARER_ID
                                          , HEADER_DESCRIPTION
                                          , JUSTIFICATION
                                          , NOTE_TO_BUYER
                                          , ITEM_ID
                                          , CHARGE_ACCOUNT_ID
                                          , LINE_TYPE_ID
                                          , SOURCE_ORGANIZATION_ID
                                          , DESTINATION_ORGANIZATION_ID
                                          , DELIVER_TO_LOCATION_ID
                                          , DELIVER_TO_REQUESTOR_ID
                                          , NEED_BY_DATE
                                          , CURRENCY_CODE
                                          , DOCUMENT_TYPE_CODE
                                          , ORG_ID
                                          , AUTOSOURCE_DOC_HEADER_ID
                                          , AUTOSOURCE_DOC_LINE_NUM
                                          , HEADER_ATTRIBUTE1
                                          , VARIANCE_ACCOUNT_ID
                                          , AUTOSOURCE_FLAG
                                          , BATCH_ID
                                          )
                                    VALUES( :CREATION_DATE
                                          , :CREATED_BY
                                          , :INTERFACE_SOURCE_CODE
                                          , :HEADER_ATTRIBUTE2
                                          , :LINE_ATTRIBUTE1
                                          , :LINE_ATTRIBUTE2
                                          , :LINE_ATTRIBUTE3
                                          , :LINE_ATTRIBUTE4
                                          , :LINE_ATTRIBUTE5
                                          , :LINE_ATTRIBUTE6
                                          , :SOURCE_TYPE_CODE
                                          , :REQUISITION_TYPE
                                          , :DESTINATION_TYPE_CODE
                                          , :QUANTITY
                                          , :UNIT_PRICE
                                          , :AUTHORIZATION_STATUS
                                          , :PREPARER_ID
                                          , :HEADER_DESCRIPTION
                                          , :JUSTIFICATION
                                          , :NOTE_TO_BUYER
                                          , :ITEM_ID
                                          , :CHARGE_ACCOUNT_ID
                                          , :LINE_TYPE_ID
                                          , :SOURCE_ORGANIZATION_ID
                                          , :DESTINATION_ORGANIZATION_ID
                                          , :DELIVER_TO_LOCATION_ID
                                          , :DELIVER_TO_REQUESTOR_ID
                                          , :NEED_BY_DATE
                                          , :CURRENCY_CODE
                                          , :DOCUMENT_TYPE_CODE
                                          , :ORG_ID
                                          , :AUTOSOURCE_DOC_HEADER_ID	
                                          , :AUTOSOURCE_DOC_LINE_NUM	
                                          , :HEADER_ATTRIBUTE1
                                          , :VARIANCE_ACCOUNT_ID
                                          , :AUTOSOURCE_FLAG
                                          , :BATCH_ID)
                            ]]>.Value


        parameterMapper.Parameters.Add(":CREATION_DATE", vo.CREATION_DATE)
        parameterMapper.Parameters.Add(":CREATED_BY", vo.CREATED_BY)
        parameterMapper.Parameters.Add(":INTERFACE_SOURCE_CODE", vo.INTERFACE_SOURCE_CODE)
        parameterMapper.Parameters.Add(":HEADER_ATTRIBUTE2", vo.HEADER_ATTRIBUTE2)
        parameterMapper.Parameters.Add(":LINE_ATTRIBUTE1", vo.LINE_ATTRIBUTE1)
        parameterMapper.Parameters.Add(":LINE_ATTRIBUTE2", vo.LINE_ATTRIBUTE2)
        parameterMapper.Parameters.Add(":LINE_ATTRIBUTE3", vo.LINE_ATTRIBUTE3)
        parameterMapper.Parameters.Add(":LINE_ATTRIBUTE4", vo.LINE_ATTRIBUTE4)
        parameterMapper.Parameters.Add(":LINE_ATTRIBUTE5", vo.LINE_ATTRIBUTE5)
        parameterMapper.Parameters.Add(":LINE_ATTRIBUTE6", vo.LINE_ATTRIBUTE6)
        parameterMapper.Parameters.Add(":SOURCE_TYPE_CODE", vo.SOURCE_TYPE_CODE)
        parameterMapper.Parameters.Add(":REQUISITION_TYPE", vo.REQUISITION_TYPE)
        parameterMapper.Parameters.Add(":DESTINATION_TYPE_CODE", vo.DESTINATION_TYPE_CODE)
        parameterMapper.Parameters.Add(":QUANTITY", vo.QUANTITY)
        parameterMapper.Parameters.Add(":UNIT_PRICE", vo.UNIT_PRICE)
        parameterMapper.Parameters.Add(":AUTHORIZATION_STATUS", vo.AUTHORIZATION_STATUS)
        parameterMapper.Parameters.Add(":PREPARER_ID", vo.PREPARER_ID)
        parameterMapper.Parameters.Add(":HEADER_DESCRIPTION", vo.HEADER_DESCRIPTION)
        parameterMapper.Parameters.Add(":JUSTIFICATION", vo.JUSTIFICATION)
        parameterMapper.Parameters.Add(":NOTE_TO_BUYER", vo.NOTE_TO_BUYER)
        parameterMapper.Parameters.Add(":ITEM_ID", vo.ITEM_ID)
        parameterMapper.Parameters.Add(":CHARGE_ACCOUNT_ID", vo.CHARGE_ACCOUNT_ID)
        parameterMapper.Parameters.Add(":LINE_TYPE_ID", vo.LINE_TYPE_ID)
        parameterMapper.Parameters.Add(":SOURCE_ORGANIZATION_ID", vo.SOURCE_ORGANIZATION_ID)
        parameterMapper.Parameters.Add(":DESTINATION_ORGANIZATION_ID", vo.DESTINATION_ORGANIZATION_ID)
        parameterMapper.Parameters.Add(":DELIVER_TO_LOCATION_ID", vo.DELIVER_TO_LOCATION_ID)
        parameterMapper.Parameters.Add(":DELIVER_TO_REQUESTOR_ID", vo.DELIVER_TO_REQUESTOR_ID)
        parameterMapper.Parameters.Add(":NEED_BY_DATE", vo.NEED_BY_DATE)
        parameterMapper.Parameters.Add(":CURRENCY_CODE", vo.CURRENCY_CODE)
        parameterMapper.Parameters.Add(":DOCUMENT_TYPE_CODE", vo.DOCUMENT_TYPE_CODE)
        parameterMapper.Parameters.Add(":ORG_ID", vo.ORG_ID)
        parameterMapper.Parameters.Add(":AUTOSOURCE_DOC_HEADER_ID", vo.AUTOSOURCE_DOC_HEADER_ID)
        parameterMapper.Parameters.Add(":AUTOSOURCE_DOC_LINE_NUM", vo.AUTOSOURCE_DOC_LINE_NUM)
        parameterMapper.Parameters.Add(":HEADER_ATTRIBUTE1", vo.HEADER_ATTRIBUTE1)
        parameterMapper.Parameters.Add(":VARIANCE_ACCOUNT_ID", vo.VARIANCE_ACCOUNT_ID)
        parameterMapper.Parameters.Add(":AUTOSOURCE_FLAG", vo.AUTOSOURCE_FLAG)
        parameterMapper.Parameters.Add(":BATCH_ID", vo.BATCH_ID)

        connector.Execute(sql, parameterMapper, BancoApp:=True)
    End Sub
End Class

<Table("apps.po_requisitions_interface_all")>
Public Class PoRequisitionsInterfaceAllDAOConfiguration
    Inherits DAOConfiguration


End Class