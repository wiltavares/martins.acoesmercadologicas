﻿Imports Core
Imports VO
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Globalization
Public Class ApTermsDAO
    Inherits DAOBase(Of VO.ApTermsVO)

    Public Sub New()
        daoConfiguration = New ApTermsDAOConfiguration()
    End Sub

    Public Function LstTerPgt(filtro As VO.ApTermsVO) As List(Of VO.ApTermsVO)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT TERM_ID, DESCRIPTION 
                                     FROM APPS.AP_TERMS 
                                     WHERE (SYSDATE BETWEEN START_DATE_ACTIVE AND END_DATE_ACTIVE 
                                                OR (END_DATE_ACTIVE IS NULL AND SYSDATE >= START_DATE_ACTIVE))
                                        AND (:TERM_ID IS NULL OR TERM_ID = :TERM_ID)
                                        AND (:DESCRIPTION IS NULL OR UPPER(DESCRIPTION) LIKE UPPER(:DESCRIPTION)) ]]>.Value

        If (Not IsNothing(filtro.DESCRIPTION)) Then
            filtro.DESCRIPTION = "%" + filtro.DESCRIPTION + "%"
        End If

        parameterMapper.Parameters.Add(":TERM_ID", ValueDBProperty(filtro.TERM_ID))
        parameterMapper.Parameters.Add(":DESCRIPTION", ValueDBProperty(filtro.DESCRIPTION))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql), BancoApp:=True)

    End Function

    Public Function SelecionarPorId(TERM_ID As Decimal) As VO.ApTermsVO
        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT TERM_ID
                                          , DESCRIPTION 
                                       FROM APPS.AP_TERMS 
                                      WHERE SYSDATE BETWEEN START_DATE_ACTIVE AND END_DATE_ACTIVE 
                                           OR (END_DATE_ACTIVE IS NULL AND SYSDATE >= START_DATE_ACTIVE)
                                           AND TERM_ID =:TERM_ID
                                     ]]>.Value


        parameterMapper.Parameters.Add(":TERM_ID", ValueDBProperty(TERM_ID))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql), BancoApp:=True).FirstOrDefault()

    End Function

End Class

<Table("APPS.AP_TERMS")>
Public Class ApTermsDAOConfiguration
    Inherits DAOConfiguration

    Property ATTRIBUTE2 As String
    Property ATTRIBUTE3 As String
    Property ATTRIBUTE4 As String
    Property ATTRIBUTE5 As String
    Property ATTRIBUTE6 As String
    Property ATTRIBUTE7 As String
    Property ATTRIBUTE8 As String
    Property ATTRIBUTE9 As String
    Property ATTRIBUTE10 As String
    Property ATTRIBUTE11 As String
    Property ATTRIBUTE12 As String
    Property ATTRIBUTE13 As String
    Property ATTRIBUTE14 As String
    Property ATTRIBUTE15 As String
    Property NAME As String
    Property DESCRIPTION As String
    Property TERM_ID As Nullable(Of Decimal)
    Property LAST_UPDATE_DATE As Nullable(Of Date)
    Property LAST_UPDATED_BY As Nullable(Of Decimal)
    Property CREATION_DATE As Nullable(Of Date)
    Property CREATED_BY As Nullable(Of Decimal)
    Property LAST_UPDATE_LOGIN As Nullable(Of Decimal)
    Property ENABLED_FLAG As String
    Property DUE_CUTOFF_DAY As Nullable(Of Decimal)
    Property TYPE As String
    Property START_DATE_ACTIVE As Nullable(Of Date)
    Property END_DATE_ACTIVE As Nullable(Of Date)
    Property RANK As Nullable(Of Decimal)
    Property ATTRIBUTE_CATEGORY As String
    Property ATTRIBUTE1 As String

End Class