﻿Imports Core
Imports VO
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Globalization

Public Class MtsPorRequisicaoVDAO
    Inherits DAOBase(Of VO.MtsPorRequisicaoV)

    Public Sub New()
        daoConfiguration = New MtsPorRequisicaoVDAOConfiguration()
    End Sub


    ''' <summary>
    ''' retorna valores utilizados para preencher tela de suprimentos
    ''' </summary>
    ''' <param name="codMercadoria"></param>
    ''' <param name="codAcao"></param>
    ''' <returns></returns>
    Public Function CnsIteRsu(codMercadoria As Integer, codAcao As Integer) As List(Of VO.MtsPorRequisicaoV)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT EXTERNAL_SOURCE_ID
                                          , ITEM_NUM
                                          , REQUISITION_NUM
                                          , REQUISITION_TYPE
                                          , REQUISITION_APPROVED_STATUS
                                          , PURCHASE_NUM
                                          , PURCHASE_TYPE
                                          , PURCHASE_STATUS
                                          , VENDOR_NAME
                                          , PURCHASE_UNIT_PRICE
                                          , PURCHASE_QUANTITY
                                          , (PURCHASE_UNIT_PRICE  * PURCHASE_QUANTITY) as PURCHASE_TOTAL
                                          , PURCHASE_QUANTITY_RECEIVED
                                          , PURCHASE_QUANTITY_CANCELLED
                                          , INVOICE_NUM
                                          , OPERATION_STATUS
                                          , INVOICE_DATE
                                          , INVOICE_UNIT_PRICE
                                          , INVOICE_QUANTITY
                                          , (INVOICE_UNIT_PRICE * INVOICE_QUANTITY) as INVOICE_TOTAL 
                                      FROM apps.mts_por_requisitions_v
                                     WHERE EXTERNAL_SOURCE_ID = :SOURCEID 
                                       AND TO_NUMBER(ITEM_NUM) = :ITEMNUM]]>.Value

        parameterMapper.Parameters.Add(":SOURCEID", codAcao.ToString)
        parameterMapper.Parameters.Add(":ITEMNUM", codMercadoria)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql), BancoApp:=True)

    End Function

End Class


<Table("apps.mts_por_requisitions_v")>
Public Class MtsPorRequisicaoVDAOConfiguration
    Inherits DAOConfiguration

    Property INVOICE_QUANTITY As Nullable(Of Decimal)
    Property INVOICE_UNIT_PRICE As Nullable(Of Decimal)
    Property REQUISITION_HEADER_ID As Nullable(Of Decimal)
    Property REQUISITION_NUM As String
    Property REQUISITION_APPROVED_STATUS As String
    Property EXTERNAL_SOURCE_NAME As String
    Property EXTERNAL_SOURCE_ID As String
    Property ORG_ID As Nullable(Of Decimal)
    Property REQUISITION_TYPE As String
    Property REQUISITION_LINE_ID As Nullable(Of Decimal)
    Property REQUISITION_LINE_NUM As Nullable(Of Decimal)
    Property REQUISITION_LINE_CANCEL_FLAG As String
    Property ITEM_ID As Nullable(Of Decimal)
    Property ITEM_NUM As String
    Property ITEM_DESCRIPTION As String
    Property CONCATENATED_SEGMENTS As String
    Property REQUISITION_UNIT_PRICE As Nullable(Of Decimal)
    Property REQUISITION_QUANTITY As Nullable(Of Decimal)
    Property REQUISITION_QUANTITY_PURCHASE As Nullable(Of Decimal)
    Property PURCHASE_TYPE As String
    Property PO_HEADER_ID As Nullable(Of Decimal)
    Property PURCHASE_NUM As String
    Property PURCHASE_APPROVED_STATUS As String
    Property PURCHASE_STATUS As String
    Property PURCHASE_CANCEL_FLAG As String
    Property VENDOR_NUM As String
    Property VENDOR_NAME As String
    Property PO_LINE_ID As Nullable(Of Decimal)
    Property LINE_LOCATION_ID As Nullable(Of Decimal)
    Property PURCHASE_LINE_NUM As Nullable(Of Decimal)
    Property PURCHASE_LINE_CANCEL_FLAG As String
    Property PURCHASE_UNIT_PRICE As Nullable(Of Decimal)
    Property PURCHASE_QUANTITY As Nullable(Of Decimal)
    Property PURCHASE_QUANTITY_RECEIVED As Nullable(Of Decimal)
    Property PURCHASE_QUANTITY_CANCELLED As Nullable(Of Decimal)
    Property ORGANIZATION_ID As Nullable(Of Decimal)
    Property OPERATION_ID As Nullable(Of Decimal)
    Property OPERATION_STATUS As String
    Property INVOICE_NUM As Nullable(Of Decimal)
    Property INVOICE_DATE As Nullable(Of Date)

End Class