﻿
Public Class MTS_RT_Orc_Budge_Actions_AllDAO
    Inherits DAOBase(Of VO.MTS_RT_Orc_Budge_Actions_All)

    Public Sub New()
        daoConfiguration = New MTS_RT_Orc_Budge_Actions_AllDAOConfiguration()
    End Sub



    Private Function NEXTVAL() As Integer

        Dim sql As String = "select mts.mts_rt_orc_budge_actions_s.NEXTVAL FROM DUAL"

        Return Convert.ToInt32(connector.Execute(sql, Nothing, BancoApp:=True))

    End Function


    Public Function Insert(item As VO.MTS_RT_Orc_Budge_Actions_All) As VO.MTS_RT_Orc_Budge_Actions_All

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        item.ACTIONS_ID = Me.NEXTVAL()

        Dim sql As String = <![CDATA[INSERT into mts.mts_rt_orc_budge_actions_all 
                                          ( ACTIONS_ID
                                          , LAST_UPDATED_BY
                                          , LAST_UPDATE_DATE
                                          , CREATION_BY
                                          , CREATION_DATE
                                          , ACTIONS_NUMBER
                                          , ACTIONS_DESTINATION_TYPE
                                          , TYPE_EXPENSE_CODE
                                          , ACTIONS_STATUS_FLAG
                                          , BUDGE_AMOUNT_FORECASTED
                                          , BUDGE_AMOUNT_REALIZED
                                          , ORG_ID
                                          )
                                   VALUES ( :ACTIONS_ID
                                          , 2501
                                          , SYSDATE
                                          , 2501
                                          , SYSDATE
                                          , :ACTIONS_NUMBER
                                          , :ACTIONS_DESTINATION_TYPE
                                          , :TYPE_EXPENSE_CODE
                                          , 'APROVADO'
                                          , :BUDGE_AMOUNT_FORECASTED
                                          , 0
                                          , 8
                                         )]]>.Value

        parameterMapper.Parameters.Add(":ACTIONS_ID", ValueDBProperty(item.ACTIONS_ID))
        parameterMapper.Parameters.Add(":ACTIONS_NUMBER", ValueDBProperty(item.ACTIONS_NUMBER))
        parameterMapper.Parameters.Add(":ACTIONS_DESTINATION_TYPE", ValueDBProperty(item.ACTIONS_DESTINATION_TYPE))
        parameterMapper.Parameters.Add(":TYPE_EXPENSE_CODE", ValueDBProperty(item.TYPE_EXPENSE_CODE))
        parameterMapper.Parameters.Add(":BUDGE_AMOUNT_FORECASTED", ValueDBProperty(item.BUDGE_AMOUNT_FORECASTED))

        connector.Execute(sql, parameterMapper, BancoApp:=True)

        Return item

    End Function
End Class






<TableAttribute("mts.mts_rt_orc_budge_actions_all")>
Public Class MTS_RT_Orc_Budge_Actions_AllDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property ACTIONS_ID As Nullable(Of Integer)

End Class
