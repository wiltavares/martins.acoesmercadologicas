﻿



Public Class MTS_RT_Type_Expenses_AllDAO
    Inherits DAOBase(Of VO.MTS_RT_Type_Expenses_All)

    Public Sub New()
        daoConfiguration = New MTS_RT_Type_Expenses_AllDAOConfiguration()
    End Sub



    Public Function SelectAttributes(vo As VO.MTS_RT_Type_Expenses_All) As VO.MTS_RT_Type_Expenses_All

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT TYPE_EXPENSE_CODE 
                                FROM MTS.MTS_RT_TYPE_EXPENSES_ALL
                                WHERE TYPE_EXPENSE_ID = :TYPE_EXPENSE_ID
                                  AND (:ATTRIBUTE1 IS NULL OR  ATTRIBUTE1 = :ATTRIBUTE1)
                                  AND (:ATTRIBUTE2 IS NULL OR  ATTRIBUTE2 = :ATTRIBUTE2)
                                  AND (:ATTRIBUTE3 IS NULL OR  ATTRIBUTE3 = :ATTRIBUTE3)
                                ]]>.Value
        parameterMapper.Parameters.Add(":TYPE_EXPENSE_ID", ValueDBProperty(vo.TYPE_EXPENSE_ID))
        parameterMapper.Parameters.Add(":ATTRIBUTE1", ValueDBProperty(vo.ATTRIBUTE1))
        parameterMapper.Parameters.Add(":ATTRIBUTE2", ValueDBProperty(vo.ATTRIBUTE2))
        parameterMapper.Parameters.Add(":ATTRIBUTE3", ValueDBProperty(vo.ATTRIBUTE3))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql), BancoApp:=True).FirstOrDefault()

    End Function

End Class



<TableAttribute("mts.mts_rt_type_expenses_all")>
Public Class MTS_RT_Type_Expenses_AllDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property TYPE_EXPENSE_ID As Nullable(Of Integer)

End Class
