﻿Imports Core

Public Class LogDataStageDAO
    Inherits DAOBase(Of VO.LogDataStage)

    Public Sub New()
        daoConfiguration = New LogDataStageDAOConfiguration()
    End Sub


    Public Function ListaParaGrid(logDataStage As VO.LogDataStage) As List(Of VO.LogDataStage)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[SELECT HST.CODFNC
                                          , HST.DATSLC
                                          , HST.CODLNHARQDTS
                                          , HST.FLGSTAPCS
                                          , HST.NOMARQRCB
                                          , HST.NOMACSUSRSIS
                                          , HST.DESMTVSTA
                                          , HST.DATHRAINIPCS
                                          , HST.DATHRAFIMPCS
                                    FROM MRT.HSTTABLOGAPIDTS HST
                                    WHERE (:NOMARQRCB IS NULL OR HST.NOMARQRCB = :NOMARQRCB)
                                    AND (:FLGSTAPCS IS NULL OR HST.FLGSTAPCS = :FLGSTAPCS)
                                    ]]>.Value
        parameterMapper.Parameters.Add(":NOMARQRCB", ValueDBProperty(logDataStage.NOMARQRCB))
        parameterMapper.Parameters.Add(":FLGSTAPCS", ValueDBProperty(logDataStage.FLGSTAPCS))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFNC", "DATSLC", "CODLNHARQDTS", "FLGSTAPCS", "NOMARQRCB", "NOMACSUSRSIS", "DESMTVSTA", "DATHRAINIPCS", "DATHRAFIMPCS"}))
    End Function

End Class



<TableAttribute("MRT.HSTTABLOGAPIDTS")>
Public Class LogDataStageDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODFNC As Nullable(Of Decimal) 'CODIGO FUNCIONARIO                                                    
    <PrimaryKey>
    Property DATSLC As Nullable(Of DateTime) 'DATA QUE FOI FEITA A SOLICITACAO                                      
    <PrimaryKey>
    Property CODLNHARQDTS As String 'Código gerado através de concatenaçao para identificar a linha        
    Property FLGSTAPCS As String 'FLAG STATUS PROCESSAMENTO                                             
    Property NOMARQRCB As String 'Nome Do Arquivo Recebido                                              
    Property NOMACSUSRSIS As String 'NOME ACESSO USUARIO SISTEMA                                           
    Property DESMTVSTA As String 'DESCRICAO MOTIVO STATUS                                               
    Property DATHRAINIPCS As Nullable(Of DateTime) 'DATA E HORA Do INICIO Do PROCESSAMENTO                                
    Property DATHRAFIMPCS As Nullable(Of DateTime) 'DATA E HORA Do FIM DE PROCESSAMENTO                                   

End Class


