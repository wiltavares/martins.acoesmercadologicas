﻿Imports Core

Public Class RelacaoDeSistemasxMenuDAO
    Inherits DAOBase(Of VO.RelacaoDeSistemasxMenu)

    Public Sub New()
        daoConfiguration = New RelacaoDeSistemasxMenuDAOConfiguration()
    End Sub


    Public Function FunCsnMnuPcp(vo As VO.RelacaoDeSistemasxMenu, codUser As Integer) As List(Of VO.RelacaoDeSistemasxMenu)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[Select TO_NUMBER(MNU.CODMNU)
                                      ,MNU.DESMNU                                     
                                      ,TO_NUMBER(MNU.CODMNUPAI)                   
                                      ,MNU.NOMOBEPRG                                  
                                      ,MNU.CODFILEMP                                  
                                      ,CASE WHEN FNC.CODFNC IS NULL THEN 0 ELSE 1 END AS ACS
                                      FROM MRT.T0150830 MNU                                 
                                      LEFT JOIN MRT.T0149506 FNC ]]>.Value


        sql = sql + " ON FNC.CODFNC = :CODFNC"
        sql = sql + " AND FNC.NOMSISINF = MNU.NOMSISINF "
        sql = sql + " AND FNC.CODFILEMP = MNU.CODFILEMP"
        sql = sql + " AND FNC.CODMNU = MNU.CODMNU"
        sql = sql + " WHERE MNU.NOMSISINF = 'AI'"
        sql = sql + " AND MNU.CODFILEMP = :CODFILEMP"
        sql = sql + " AND MNU.CODMNU = MNU.CODMNUPAI"
        sql = sql + " ORDER BY 3, 1, 2"

        parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(vo.CODFILEMP))
        parameterMapper.Parameters.Add(":CODFNC", ValueDBProperty(codUser))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class

<TableAttribute("MRT.T0150830")>
Public Class RelacaoDeSistemasxMenuDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property NOMSISINF As String
    Property NOMOBEPRG As String
    Property INDABRMNUFLH As Nullable(Of Decimal)
    Property DESMNU As String
    Property DESLCLAPLMNU As String
    Property DESICNMNU As String
    Property CODMNUPAI As String
    <PrimaryKey>
    Property CODMNU As String
    <PrimaryKey>
    Property CODFILEMP As Nullable(Of Decimal)
    Property ACS As Nullable(Of Decimal)

End Class