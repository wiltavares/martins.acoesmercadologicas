﻿Imports Core
Public Class ControleAcessoDAO
    Inherits DAOBase(Of VO.ControleAcesso)

    Public Sub New()
        daoConfiguration = New ControleAcessoDAOConfiguration()
    End Sub

    Public Function ControleAcessoUsuarioGrupoAcesso(grupos As String) As List(Of VO.ControleAcesso)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String
        sql = <![CDATA[SELECT DISTINCT
                                  MN.CODMNU
                                , MN.DESMNU
                                , CASE WHEN MN.CODMNUPAI = MN.CODMNU THEN '0' ELSE MN.CODMNUPAI END AS CODMNUPAI
                                , CASE WHEN RLC.CODGRPACS IS NULL THEN 0 ELSE 1 END INDASSOCIADO
                             FROM MRT.T0150830 MN
                            INNER JOIN MRT.RLCGRPACSSISMNU RLC ON MN.CODMNU = RLC.CODMNU AND RLC.NOMSISINF = MN.NOMSISINF
                            INNER JOIN MRT.CADGRPACSSIS GRP ON GRP.CODGRPACS = RLC.CODGRPACS AND GRP.NOMSISINF = MN.NOMSISINF
                             WHERE MN.CODFILEMP = 1
                               AND MN.NOMSISINF = 'AI'
                               AND UPPER(GRP.DESGRPRDE) IN ({0})
                             ORDER BY MN.CODMNU]]>.Value

        sql = String.Format(sql, ("'" + grupos.Replace(";", "', '") + "'").ToUpper())
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function ListarAcessosGrupo(codGrupo As Decimal) As List(Of VO.ControleAcesso)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String
        sql = <![CDATA[SELECT DISTINCT
                              MN.CODMNU
                            , MN.DESMNU
                            , CASE WHEN MN.CODMNUPAI = MN.CODMNU THEN '0' ELSE MN.CODMNUPAI END AS CODMNUPAI
                            , CASE WHEN RLC.CODGRPACS IS NULL THEN 0 ELSE 1 END INDASSOCIADO
                         FROM MRT.T0150830 MN
                         LEFT JOIN MRT.RLCGRPACSSISMNU RLC ON MN.CODMNU = RLC.CODMNU AND RLC.NOMSISINF = MN.NOMSISINF  AND RLC.CODGRPACS = :CODGRPACS
                        WHERE MN.CODFILEMP = 1
                          AND MN.NOMSISINF = 'AI'
                        ORDER BY MN.CODMNU]]>.Value

        parameterMapper.Parameters.Add(":CODGRPACS", ValueDBProperty(codGrupo))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class

<TableAttribute("MRT.RLCGRPACSSISMNU")>
Public Class ControleAcessoDAOConfiguration
    Inherits DAOConfiguration

    Property CODGRPACS As Nullable(Of Decimal)
    Property NOMSISINF As String 
    Property CODMNU As String

    <IgnoreMap>
    Property INDASSOCIADO As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMFNC As String
    <IgnoreMap>
    Property NOMFILEMP As String
    <IgnoreMap>
    Property DESMNU As String
    <IgnoreMap>
    Property CODMNUPAI As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMOBEPRG As String
End Class

