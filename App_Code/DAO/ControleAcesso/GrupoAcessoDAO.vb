﻿Imports Core

Public Class GrupoAcessoDAO
    Inherits DAOBase(Of VO.GrupoAcesso)

    Public Sub New()
        daoConfiguration = New GrupoAcessoDAOConfiguration()
    End Sub


    Public Function BuscaGrupoAcesso(vo As VO.GrupoAcesso) As List(Of VO.GrupoAcesso)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT CODGRPACS 
                                       FROM MRT.CADGRPACSSIS 
                                      WHERE NOMSISINF = 'AI' 
                                        AND DESGRPRDE = :DESGRPRDE]]>.Value
        parameterMapper.Parameters.Add(":DESGRPRDE", ValueDBProperty(vo.DESGRPRDE.Trim))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function ListGruposAcesso(vo As VO.GrupoAcesso) As List(Of VO.GrupoAcesso)


        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                             CODGRPACS
                                           , NOMSISINF
                                           , DESGRPACS
                                           , DESGRPRDE      
                                     FROM MRT.CADGRPACSSIS 
                                    WHERE NOMSISINF = 'AI'
                                      AND (:DESGRPACS IS NULL OR DESGRPACS LIKE :DESGRPACS)
                                      AND (:CODGRPACS IS NULL OR CODGRPACS = :CODGRPACS)
                                      ORDER BY DESGRPACS ]]>.Value


        Dim nomGrupoAcesso = vo.DESGRPACS
        If (vo.DESGRPACS <> Nothing) Then
            nomGrupoAcesso = "%" + vo.DESGRPACS + "%"
        End If
        parameterMapper.Parameters.Add(":DESGRPACS", ValueDBProperty(nomGrupoAcesso))
        parameterMapper.Parameters.Add(":CODGRPACS", ValueDBProperty(vo.CODGRPACS))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function GetNextCODGRPACS() As Decimal
        Dim sql As String = <![CDATA[SELECT COALESCE(MAX(CODGRPACS), 0) + 1 AS CODGRPACS
                                       FROM MRT.CADGRPACSSIS 
                                      WHERE NOMSISINF = 'AI']]>.Value

        Return connector.SelectTList(sql, Nothing, RowMapperOnlyProperties(sql)).First().CODGRPACS
    End Function

    <TableAttribute("MRT.CADGRPACSSIS")>
    Public Class GrupoAcessoDAOConfiguration
        Inherits DAOConfiguration

        <PrimaryKey>
        Property NOMSISINF As String
        <PrimaryKey>
        Property CODGRPACS As Nullable(Of Decimal)
        Property DESGRPACS As String
        Property DESGRPRDE As String
        '  Property CODIGO_NOME As String

    End Class

End Class
