﻿Imports Core
Public Class EventoPromocionalDAO
    Inherits DAOBase(Of VO.EventoPromocional)

    Public Sub New()
        daoConfiguration = New EventoPromocionalDAODAOConfiguration()
    End Sub

    Public Function ListEventoPromocional() As List(Of VO.EventoPromocional)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODEVTBTB
                                        , DESEVTBTB
                                        , CODEVTBTB || ' - ' || DESEVTBTB AS CODDES
                                    FROM MRT.CADEVTBTB]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class

<TableAttribute("MRT.CADEVTBTB")>
Public Class EventoPromocionalDAODAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODEVTBTB As Nullable(Of Decimal)

    <IgnoreMap>
    Property CODDES As String

End Class
