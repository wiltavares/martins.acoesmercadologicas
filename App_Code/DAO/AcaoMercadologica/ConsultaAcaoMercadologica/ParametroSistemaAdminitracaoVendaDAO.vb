﻿Imports Core
Public Class ParametroSistemaAdminitracaoVendaDAO
    Inherits DAOBase(Of VO.ParametroSistemaAdminitracaoVenda)

    Public Sub New()
        daoConfiguration = New ParametroSistemaAdminitracaoVendaDAOConfiguration()
    End Sub

    ''' <summary>
    ''' BscPmtAcoCmc
    ''' Busca as taxas de adminstração e brinde da ação comercial
    ''' </summary>
    ''' <returns></returns>
    Public Function ListarParametrosAcaoComercial() As List(Of VO.ParametroSistemaAdminitracaoVenda)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT VLRMAXACOCMC, VLRTXAADMFRNACOCMC, 
                            VLRTXAADMBDEACOCMC, VLRMNMITERSU, 
                            PERCALENCFOLPGT, CODFRNMKT FROM MRT.T0101295]]>.Value

        Return connector.SelectTList(sql, Nothing, RowMapperOnlyProperties(sql))

    End Function

    Public Function ListarParametrosAcaoComercialEmp() As VO.ParametroSistemaAdminitracaoVenda

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT VLRMNMITERSU
                            FROM MRT.T0101295
                            WHERE CODEMP = 1]]>.Value

        Return connector.SelectTList(sql, Nothing, RowMapperOnlyProperties(sql)).FirstOrDefault()

    End Function


    Public Function csnCodUndOpe() As VO.ParametroSistemaAdminitracaoVenda

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODUNDOPE   
                                     FROM MRT.T0123469  
                                     WHERE CODUNDNGC = 1 ]]>.Value

        Return connector.SelectTList(sql, Nothing, RowMapperOnlyProperties(sql)).FirstOrDefault()

    End Function
End Class


<TableAttribute("MRT.T0101295")>
Public Class ParametroSistemaAdminitracaoVendaDAOConfiguration
    Inherits DAOConfiguration

End Class
