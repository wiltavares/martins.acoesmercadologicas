﻿Imports Core

Public Class RelacaoAcaoComercialXPedidoBonificadoDAO
    Inherits DAOBase(Of VO.RelacaoAcaoComercialXPedidoBonificado)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoComercialXPedidoBonificadoDAOConfiguration()
    End Sub

    ''' <summary>
    ''' Lista pedidos de compra do fornecedor
    ''' </summary>
    ''' <param name="codFnc"></param>
    ''' <param name="numPedCmp"></param>
    ''' <returns></returns>
    Public Function ListarPedidosDeCompra(codFnc As Decimal?, Optional numPedCmp As Decimal? = Nothing) As List(Of VO.RelacaoAcaoComercialXPedidoBonificado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.NUMPEDCMP, A.DATPEDCMP, A.CODFRN, A.DESOBSPEDCMP, D.NOMFRN,
                            SUM(B.QDEENTMERPEDCMP * B.VLRLIQITEPEDCMP) - A.VLRUTZACOCMC AS VERBA_DISPONIVEL
                            FROM MRT.T0105592 A
                            INNER JOIN MRT.T0105606 B ON A.NUMPEDCMP = B.NUMPEDCMP
                            INNER JOIN MRT.T0118104 C ON A.NUMIDTTIPPEDCMP = C.TIPPEDCMP
                            INNER JOIN MRT.T0100426 D ON A.CODFRN = D.CODFRN
                            WHERE C.TIPDSNDSCBNF = 99
                            AND A.DATPEDCMP >= TO_DATE('2016-01-01', 'YYYY-MM-DD')
                            AND A.CODSITPEDCMP IN ( 1, 2, 3)
                            AND (:CODFRN IS NULL OR A.CODFRN = :CODFRN)
                            AND (:NUMPEDCMP IS NULL OR A.NUMPEDCMP = :NUMPEDCMP)
                            GROUP BY A.NUMPEDCMP, A.DATPEDCMP, A.CODFRN, A.DESOBSPEDCMP, D.NOMFRN, A.VLRUTZACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(codFnc))
        parameterMapper.Parameters.Add(":NUMPEDCMP", ValueDBProperty(numPedCmp))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' Lista pedidos bonificados da acao
    ''' </summary>
    ''' <param name="CODACOCMC"></param>
    ''' <returns></returns>
    Public Function ListarPedidosBonificadosDaAcao(CODACOCMC As Decimal) As List(Of VO.RelacaoAcaoComercialXPedidoBonificado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT B.NUMPEDCMP, B.CODFRN, F.NOMFRN, P.DESOBSPEDCMP, B.VLRUTZACOCMC, SUM(D.QDEENTMERPEDCMP * D.VLRLIQITEPEDCMP) - P.VLRUTZACOCMC AS VERBA_DISPONIVEL 
                            FROM MRT.RLCACOCMCPEDCMP B
                            INNER JOIN MRT.T0105592 P ON P.NUMPEDCMP = B.NUMPEDCMP
                            INNER JOIN MRT.T0100426 F ON F.CODFRN = B.CODFRN
                            INNER JOIN MRT.T0105606 D ON B.NUMPEDCMP = D.NUMPEDCMP
                            WHERE B.CODACOCMC = :CODACOCMC
                            GROUP BY B.NUMPEDCMP, B.CODFRN, F.NOMFRN, P.DESOBSPEDCMP, B.VLRUTZACOCMC, P.VLRUTZACOCMC
                            ORDER BY B.NUMPEDCMP]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function
End Class


<TableAttribute("MRT.RLCACOCMCPEDCMP")>
Public Class RelacaoAcaoComercialXPedidoBonificadoDAOConfiguration
    Inherits DAOConfiguration

    <IgnoreMap>
    Property NOMFRN As String
    <IgnoreMap>
    Property DESOBSPEDCMP As String
    <IgnoreMap>
    Property DATPEDCMP As Date
    <IgnoreMap>
    Property VERBA_DISPONIVEL As Decimal
    <IgnoreMap>
    Property COD_FILTRO As Integer

End Class
