﻿Imports Core
Public Class RelacaoAcaoxFornecedorxCarimboDAO
    Inherits DAOBase(Of VO.RelacaoAcaoxFornecedorxCarimbo)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoxFornecedorxCarimboDAOConfiguration()
    End Sub

    ''' <summary>
    ''' CnsRlcAcoFrnMcoPesquisa
    ''' Busca a lista de relação de carimbos da acao comercial resgatando os nomes correspondentes (fornecedor e soma de valores)
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Public Function ListarCarimbosDaAcaoComercial(id As Decimal) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC, A.CODFRN, A.CODMCOVBAFRN, A.VLRUTZACOCMC, B.NOMFRN, 
                             SUM(ITEMCO.VLRPRVMCOVBAFRN) - MCO.VLRUTZACOCMC AS VALOR_DISPONIVEL, MCO.DESOBSMCOVBAFRN AS OBSERVACAO 
                             FROM MRT.RLCACOFRNMCO A  
                              INNER JOIN   MRT.CADMCOVBAFRN MCO  ON A.CODMCOVBAFRN = MCO.CODMCOVBAFRN 
                              INNER JOIN   MRT.T0100426 B ON A.CODFRN = B.CODFRN AND B.CODFRN = MCO.CODFRN 
                              INNER JOIN MRT.CADITEMCOVBAFRN ITEMCO ON MCO.CODMCOVBAFRN = ITEMCO.CODMCOVBAFRN  
                             WHERE A.CODACOCMC = :CODACOCMC
                              GROUP BY A.CODACOCMC, A.CODFRN, A.CODMCOVBAFRN, A.VLRUTZACOCMC, B.NOMFRN,  MCO.VLRUTZACOCMC, MCO.DESOBSMCOVBAFRN]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", id)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' CsnCrb
    ''' </summary>
    ''' <param name="codFrn"></param>
    ''' <param name="codCelula"></param>
    ''' <param name="codCarimbo"></param>
    ''' <param name="nomFrn"></param>
    ''' <returns></returns>
    Public Function BuscarCarimbosPorFiltro(codFrn As Decimal?, codCelula As Decimal?, codCarimbo As Decimal?, nomFrn As String) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT  
                                MCO.CODMCOVBAFRN,
                                MCO.DATGRCMCOVBAFRN, 
                                MCO.CODFRN, 
                                CEL.CODDIVCMP,
                                FRN.NOMFRN,
                                CEL.DESDIVCMP,
                                SUM(ITEMCO.VLRPRVMCOVBAFRN) VLRPRVMCOVBAFRN,
                                MCO.VLRUTZACOCMC VLRUTZACOCMC,
                                SUM(ITEMCO.VLRPRVMCOVBAFRN) - MCO.VLRUTZACOCMC AS VALOR_DISPONIVEL,
                                MCO.DESOBSMCOVBAFRN AS OBSERVACAO
                                FROM MRT.CADMCOVBAFRN MCO 
                                INNER JOIN MRT.CADITEMCOVBAFRN ITEMCO ON MCO.CODMCOVBAFRN = ITEMCO.CODMCOVBAFRN
                                                                     AND ITEMCO.TIPDSNDSCBNF = 99 
                                INNER JOIN MRT.T0100426 FRN ON FRN.CODFRN = MCO.CODFRN 
                                INNER JOIN MRT.T0113625 CPR ON CPR.CODCPR = FRN.CODCPR 
                                INNER JOIN MRT.T0118570 CEL ON CEL.CODGERPRD = CPR.CODGERPRD 
                             WHERE
                                MCO.INDSTAMCOVBAFRN IN (2, 3, 6, 7)
                                AND (:CODMCOVBAFRN IS NULL OR MCO.CODMCOVBAFRN = :CODMCOVBAFRN)
                                AND (:CODFRN IS NULL OR MCO.CODFRN  = :CODFRN)
                                AND (:CODDIVCMP IS NULL OR CEL.CODDIVCMP = :CODDIVCMP)
                                AND (:NOMFRN IS NULL OR FRN.NOMFRN LIKE :NOMFRN)
                            GROUP BY   
                                MCO.CODMCOVBAFRN,
                                MCO.DATGRCMCOVBAFRN, 
                                MCO.CODFRN,
                                FRN.NOMFRN,
                                CPR.CODCPR,
                                CPR.NOMCPR,
                                CEL.CODDIVCMP,
                                CEL.DESDIVCMP,
                                MCO.CODFNCMCOVBAFRN,
                                MCO.DESOBSMCOVBAFRN,
                                MCO.VLRUTZACOCMC
                            HAVING (SUM(ITEMCO.VLRPRVMCOVBAFRN) - MCO.VLRUTZACOCMC) > 0
                            ORDER BY 1 DESC]]>.Value


        If nomFrn IsNot Nothing Then
            nomFrn = "%" & nomFrn & "%"
        End If

        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(codFrn))
        parameterMapper.Parameters.Add(":CODDIVCMP", ValueDBProperty(codCelula))
        parameterMapper.Parameters.Add(":CODMCOVBAFRN", ValueDBProperty(codCarimbo))
        parameterMapper.Parameters.Add(":NOMFRN", ValueDBProperty(nomFrn))



        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' CsnSldCrb
    ''' </summary>
    ''' <param name="codFrn"></param>
    ''' <param name="codCelula"></param>
    ''' <param name="codCarimbo"></param>
    ''' <param name="nomFrn"></param>
    ''' <returns></returns>
    Public Function PesquisarCarimbos(codFrn As Decimal?, codCelula As Decimal?, codCarimbo As Decimal?, nomFrn As String, flSaldo As Boolean) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[with tab as (
                                          SELECT d.desdivcmp
                                               , c.codfrn  
                                               , c.nomfrn
                                               , a.codmcovbafrn
                                               , a.datgrcmcovbafrn as datgrcmco
                                               , a.DESOBSMCOVBAFRN
                                               , sum(b.vlrrlzmcovbafrn) as vlrmco
                                               , a.vlrutzacocmc as vlrutz
                                            from mrt.cadmcovbafrn a
                                            inner join mrt.caditemcovbafrn b on b.codmcovbafrn = a.codmcovbafrn
                                            inner join mrt.t0100426 c on c.codfrn = a.codfrn  
                                            inner join mrt.t0118570 d on d.codgerprd = c.codgerprd 
                                           AND (:CODDIVCMP IS NULL OR :CODDIVCMP = D.CODDIVCMP)
                                           AND (:CODFRN IS NULL OR :CODFRN = C.CODFRN)
                                           AND (:CODCRB IS NULL OR :CODCRB = A.CODMCOVBAFRN)
                                           GROUP BY d.desdivcmp
                                                  , c.codfrn
                                                  , c.nomfrn
                                                  , a.codmcovbafrn
                                                  , a.datgrcmcovbafrn
                                                  , a.DESOBSMCOVBAFRN
                                                  , a.vlrutzacocmc
                                             HAVING (SUM(b.vlrrlzmcovbafrn) > 0)
                                              )
                                        select desdivcmp as CELULA
                                             , codfrn || ' - ' || nomfrn AS FORNECEDOR
                                             , CODMCOVBAFRN 
                                             , DATGRCMCO 
                                             , DESOBSMCOVBAFRN
                                             , VLRMCO 
                                             , VLRUTZ 
                                             , VLRMCO - VLRUTZ AS SALDO_CARIMBO
                                          from tab 
                                           where (:flSaldo = 0 OR (vlrmco - vlrutz > 0))
                                         order by desdivcmp
                                                , CODMCOVBAFRN 
                                                , DESOBSMCOVBAFRN]]>.Value

        If nomFrn IsNot Nothing Then
            nomFrn = "%" & nomFrn & "%"
            parameterMapper.Parameters.Add(":NOMFRN", ValueDBProperty(nomFrn))
        End If


        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(codFrn))
        parameterMapper.Parameters.Add(":CODDIVCMP", ValueDBProperty(codCelula))
        parameterMapper.Parameters.Add(":CODCRB", ValueDBProperty(codCarimbo))
        parameterMapper.Parameters.Add(":flSaldo", ValueDBProperty(flSaldo))

        'where (:flSaldo = 0 OR (vlrmco - vlrutz > 0))


        Dim properties = {"CODMCOVBAFRN", "CELULA", "DATGRCMCO", "FORNECEDOR", "VLRMCO", "VLRUTZ", "SALDO_CARIMBO", "DESOBSMCOVBAFRN"}
        Return connector.SelectTList(sql, parameterMapper, DAORowMapper(Of VO.RelacaoAcaoxFornecedorxCarimbo).RowMapperOnlyProperties(properties))


    End Function

    ''' <summary>
    ''' CnsRlcAcoFrnMco
    ''' Busca a lista de relação de carimbos da acao comercial apenas na tabela principal
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>


    Public Function ConsultaRelacaoAcaoFornecedorCarimboDaAcao(idAcao As Decimal) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODACOCMC, CODFRN, CODMCOVBAFRN, VLRUTZACOCMC
                            FROM MRT.RLCACOFRNMCO
                            Where CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", idAcao)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


    ''' <summary>
    ''' DelRlcAcoFrnMco
    ''' Deleta a relacao acao fornecedor carimbo de uma acao
    ''' </summary>
    ''' <returns></returns>
    Public Function DeletaRelacaoAcaoFornecedorCarimboDaAcao(idAcao As Decimal) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE MRT.RLCACOFRNMCO WHERE CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", idAcao)

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function

    ''' <summary>
    ''' DelRlcAcoFrnMco
    ''' Deleta a relacao acao fornecedor carimbo de uma acao
    ''' </summary>
    ''' <returns></returns>
    Public Function InsereRelacaoAcaoFornecedorCarimboDaAcao(idAcao As Decimal, str As String, acaoComercial As VO.AcaoComercial) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim frn As Integer = 0
        Dim carimbo As Integer = 0
        Dim vlr As Decimal = 0
        Dim count As Integer = 0
        'pega o número de linhas do dataSet
        count = acaoComercial.Carimbos.Count

        For countAux = 0 To count - 1
            ' String de comando
            frn = acaoComercial.Carimbos(countAux).CODFRN
            carimbo = acaoComercial.Carimbos(countAux).CODMCOVBAFRN
            vlr = Decimal.Parse(acaoComercial.Carimbos(countAux).VLRUTZACOCMC)

            Dim sql As String = <![CDATA[ INSERT INTO MRT.RLCACOFRNMCO 
                                             (CODACOCMC,      
                                             CODFRN,   
                                             CODMCOVBAFRN,   
                                             VLRUTZACOCMC) 
                                             VALUES(:CODACOCMC, 
                                             :CODFRN, 
                                             :CODMCOVBAFRN, 
                                             :VLRUTZACOCMC)]]>.Value

            parameterMapper.Parameters.Add(":CODFRN", frn)
            parameterMapper.Parameters.Add(":CODMCOVBAFRN", carimbo)
            parameterMapper.Parameters.Add(":VLRUTZACOCMC", vlr)
            parameterMapper.Parameters.Add(":CODACOCMC", idAcao)

            Return connector.ExecuteScalar(sql, parameterMapper)
        Next

        Return Nothing

    End Function

    ''' <summary>
    ''' InsDdoRlcAcoFrnMco
    ''' Inserir Relacao Acao Fornecedor Carimbo
    ''' </summary>
    ''' <returns></returns>
    Public Function InserirDadosRelacaoFornecedorCarimbo(carimbo As VO.RelacaoAcaoxFornecedorxCarimbo) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.RLCACOFRNMCO (
                             CODACOCMC,
                             CODFRN,
                             CODMCOVBAFRN,
                             VLRUTZACOCMC)
                            VALUES( 
                             :CODACOCMC,
                             :CODFRN,
                             :CODMCOVBAFRN,
                             :VLRUTZACOCMC)]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", carimbo.CODACOCMC)
        parameterMapper.Parameters.Add(":CODFRN", carimbo.CODFRN)
        parameterMapper.Parameters.Add(":CODMCOVBAFRN", carimbo.CODMCOVBAFRN)
        parameterMapper.Parameters.Add(":VLRUTZACOCMC", carimbo.VLRUTZACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function

    ''' <summary>
    ''' DB_CsnAcoCmcCrb.CnsEstorno
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function ConsultaEstornoCarimbo(idAcao As Decimal) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT REL.CODFRN,
                            FRN.NOMFRN, REL.CODMCOVBAFRN, REL.VLRUTZACOCMC, 
                            ROUND(SUM(CASE WHEN VLRPTCCLIITEACOCMC = 0
                                THEN
                                QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC
                                ELSE
                                QDEITEOCDACOCMC * VLRPTCFRNITEACOCMC
                                END),2) AS VLR_TOTAL_ACAO,
                            ROUND(SUM((QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC) - VLRUTZOPEDSNACOCMC),2) AS SALDO_ACAO,
                            ROUND((CASE WHEN VLRPTCCLIITEACOCMC = 0
                                THEN
                                ROUND(REL.VLRUTZACOCMC / SUM(ACO.QDEITEOCDACOCMC * ACO.VLRUNTITEOCDACOCMC), 2) * 100
                                ELSE
                                ROUND(REL.VLRUTZACOCMC / SUM(ACO.QDEITEOCDACOCMC * VLRPTCFRNITEACOCMC), 2) * 100
                                END),2) AS PERCENTUAL_CARIMBO,
                            ROUND((SUM((QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC) - VLRUTZOPEDSNACOCMC))
                                * ROUND((REL.VLRUTZACOCMC / SUM(ACO.QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC)),2),2) AS VALOR_ESTORNAR_CARIMBO 
                            FROM MRT.T0138776 ACO                        
                            INNER JOIN MRT.RLCACOFRNMCO REL
                            ON REL.CODACOCMC = ACO.CODACOCMC
                            INNER JOIN MRT.T0100426 FRN
                            ON REL.CODFRN = FRN.CODFRN
                            INNER JOIN MRT.T0155573 OPE
                            ON OPE.CODOPEFSCDSNACOCMC = ACO.CODOPEFSCDSNACOCMC 	
                            AND OPE.INDBDEOPEDSNACOCMC <> 1                     
                            WHERE REL.CODACOCMC = :CODACOCMC
                            GROUP BY REL.CODFRN,
                            FRN.NOMFRN,
                            REL.CODMCOVBAFRN,
                            REL.VLRUTZACOCMC,
                            ACO.VLRPTCCLIITEACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", idAcao)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' InsDdoRlcAcoFrnMco
    ''' Inserir Relacao Acao Fornecedor Carimbo
    ''' </summary>
    ''' <returns></returns>
    Public Function AtuCadMcoVbaFrn(CODMCOVBAFRN As Integer, VLRUTZACOCMC As Decimal) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.CADMCOVBAFRN 
                                     SET   VLRUTZACOCMC= VLRUTZACOCMC + :VLRUTZACOCMC 
                                     WHERE CODMCOVBAFRN = :CODMCOVBAFRN]]>.Value

        parameterMapper.Parameters.Add(":CODMCOVBAFRN", CODMCOVBAFRN)
        parameterMapper.Parameters.Add(":VLRUTZACOCMC", VLRUTZACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function

    ''' <summary>
    ''' CsnExtCrbEvt-ConsultaExtratoCarimboEvento
    ''' </summary>
    ''' <param name="codCarimbo"></param>
    ''' <param name="NOMTIPEVTACOCMC"></param>
    ''' <param name="NOMEVTACOCMC"></param>
    ''' <param name="NOMOBJ"></param>
    ''' <returns></returns>
    Public Function BuscarCarimbos(codCarimbo As Decimal?, NOMTIPEVTACOCMC As String, NOMEVTACOCMC As String, NOMOBJ As String) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT  
                                            A.CODFRN || ' - ' ||  C.NOMFRN AS FORNECEDOR, 
                                            A.CODMCOVBAFRN ,
                                            D.DATGRCMCOVBAFRN ,
                                            F.NOMTIPEVTACOCMC ,
                                            G.NOMEVTACOCMC , 
                                            CASE WHEN H.TIPOBJMCOVBAFRN IS NULL THEN ' ' ELSE H.DESOBJMCOVBAFRN END AS OBJETIVO,
                                            D.DESOBSMCOVBAFRN AS OBSERVACAO,
                                            A.CODACOCMC || ' - ' || B.NOMACOCMC AS ACAO,
                                            B.DATAPVACOCMC ,
                                            A.VLRUTZACOCMC 
                                    FROM  MRT.RLCACOFRNMCO A
                                        INNER join MRT.T0100426 C on c.codfrn = a.codfrn
                                        INNER join MRT.CADMCOVBAFRN D on d.codmcovbafrn = a.codmcovbafrn
                                        left join MRT.CADOBJMCOVBAFRN H on h.tipobjmcovbafrn = d.tipobjmcovbafrn
                                        INNER join   MRT.T0118430 B on b.codacocmc = a.codacocmc
                                        INNER join MRT.T0155531 G on g.tipdsndscbnf = b.tipdsndscbnf
                                        INNER join  MRT.T0155522 F on g.tipevtacocmc = f.tipevtacocmc
                                        INNER join MRT.T0155581 E on e.codacocmc = a.codacocmc  
                                                                    AND F.TIPEVTACOCMC = E.TIPEVTACOCMC
                                                                    AND G.CODEVTACOCMC = E.CODEVTACOCMC
                                 Where   (:CODCRB IS NULL OR :CODCRB = A.CODMCOVBAFRN)
                                        AND (:NOMTIPEVTACOCMC IS NULL OR :NOMTIPEVTACOCMC = F.NOMTIPEVTACOCMC)
                                        AND (:NOMEVTACOCMC IS NULL OR :NOMEVTACOCMC = G.NOMEVTACOCMC)
                                        AND (:NOMOBJ IS NULL OR :NOMOBJ = H.DESOBJMCOVBAFRN)]]>.Value




        'If nomFrn IsNot Nothing Then
        '    nomFrn = "%" & nomFrn & "%"
        'End If

        parameterMapper.Parameters.Add(":CODMCOVBAFRN", ValueDBProperty(codCarimbo))
        parameterMapper.Parameters.Add(":NOMTIPEVTACOCMC", ValueDBProperty(NOMTIPEVTACOCMC))
        parameterMapper.Parameters.Add(":NOMEVTACOCMC", ValueDBProperty(NOMEVTACOCMC))
        parameterMapper.Parameters.Add(":DESOBJMCOVBAFRN", ValueDBProperty(NOMOBJ))





        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))


    End Function




    Public Function ListConsulta(vo As VO.RelacaoAcaoxFornecedorxCarimbo) As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT  A.CODFRN || ' - ' ||  C.NOMFRN AS FORNECEDOR, 
                                                A.CODMCOVBAFRN ,
                                                D.DATGRCMCOVBAFRN ,
                                                F.NOMTIPEVTACOCMC ,
                                                G.NOMEVTACOCMC , 
                                                CASE WHEN H.TIPOBJMCOVBAFRN IS NULL THEN ' ' ELSE H.DESOBJMCOVBAFRN END AS OBJETIVO,
                                                D.DESOBSMCOVBAFRN AS DESOBSMCOVBAFRN,
                                                A.CODACOCMC || ' - ' || B.NOMACOCMC AS ACAO,
                                                B.DATAPVACOCMC,
                                                A.VLRUTZACOCMC 

                                    FROM  MRT.RLCACOFRNMCO A
                                            INNER join MRT.T0100426 C on c.codfrn = a.codfrn
                                            INNER join MRT.CADMCOVBAFRN D on d.codmcovbafrn = a.codmcovbafrn
                                            left join MRT.CADOBJMCOVBAFRN H on h.tipobjmcovbafrn = d.tipobjmcovbafrn
                                            INNER join   MRT.T0118430 B on b.codacocmc = a.codacocmc
                                            INNER join MRT.T0155531 G on g.tipdsndscbnf = b.tipdsndscbnf
                                            INNER join  MRT.T0155522 F on g.tipevtacocmc = f.tipevtacocmc
                                            INNER join MRT.T0155581 E on e.codacocmc = a.codacocmc  
                                                                        AND F.TIPEVTACOCMC = E.TIPEVTACOCMC
                                                                        AND G.CODEVTACOCMC = E.CODEVTACOCMC

                                    Where   (:CODCRB IS NULL OR :CODCRB = A.CODMCOVBAFRN)
                                            AND (:NOMTIPEVTACOCMC IS NULL OR :NOMTIPEVTACOCMC = F.NOMTIPEVTACOCMC)
                                            AND (:NOMEVTACOCMC IS NULL OR :NOMEVTACOCMC = G.NOMEVTACOCMC)
                                            AND (:NOMOBJ IS NULL OR :NOMOBJ = H.DESOBJMCOVBAFRN)]]>.Value

        'Dim nomevtLike = "%" + vo.NOMEVTACOCMC + "%"

        parameterMapper.Parameters.Add(":NOMTIPEVTACOCMC", ValueDBProperty(vo.NOMTIPEVTACOCMC))
        parameterMapper.Parameters.Add(":NOMEVTACOCMC", ValueDBProperty(vo.NOMEVTACOCMC))
        parameterMapper.Parameters.Add(":NOMOBJ", ValueDBProperty(vo.DESOBJMCOVBAFRN))
        parameterMapper.Parameters.Add(":CODCRB", ValueDBProperty(vo.CODMCOVBAFRN))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class


<TableAttribute("MRT.RLCACOFRNMCO")>
Public Class RelacaoAcaoxFornecedorxCarimboDAOConfiguration
    Inherits DAOConfiguration

    <IgnoreMap>
    Property VLR_TOTAL_ACAO As Nullable(Of Decimal)

    <IgnoreMap>
    Property VALOR_ESTORNAR_CARIMBO As Nullable(Of Decimal)

    <IgnoreMap>
    Property PERCENTUAL_CARIMBO As Nullable(Of Decimal)

    <IgnoreMap>
    Property SALDO_ACAO As Nullable(Of Decimal)

    <IgnoreMap>
    Property VLRPRVMCOVBAFRN As Nullable(Of Decimal)

    <IgnoreMap>
    Property OBSERVACAO As String

    <IgnoreMap>
    Property VALOR_DISPONIVEL As Decimal

    <IgnoreMap>
    Property SALDO_CARIMBO As Decimal

    <IgnoreMap>
    Property NOMFRN As String

    <IgnoreMap>
    Property NOMEVTACOCMC As String

    <IgnoreMap>
    Property NOMTIPEVTACOCMC As String

    <IgnoreMap>
    Property DATGRCMCOVBAFRN As Date

    <IgnoreMap>
    Property DATAPVACOCMC As Date

    <IgnoreMap>
    Property CODDIVCMP As String

    <IgnoreMap>
    Property DESDIVCMP As String

    <IgnoreMap>
    Property COD_FILTRO As Nullable(Of Decimal)

    <IgnoreMap>
    Property TIPEVTACOCMC As Nullable(Of Decimal)

    <IgnoreMap>
    Property CODEVTACOCMC As String

    <IgnoreMap>
    Property DESOBJMCOVBAFRN As String

    <IgnoreMap>
    Property TIPOBJMCOVBAFRN As Nullable(Of Decimal)

    <IgnoreMap>
    Property DESOBSMCOVBAFRN As String

    <IgnoreMap>
    Property NOMACOCMC As String

    <IgnoreMap>
    Property FORNECEDOR As String

    <IgnoreMap>
    Property ACAO As String

    <IgnoreMap>
    Property OBJETIVO As String

    <IgnoreMap>
    Property vlrrlzmcovbafrn As Nullable(Of Decimal)




End Class
