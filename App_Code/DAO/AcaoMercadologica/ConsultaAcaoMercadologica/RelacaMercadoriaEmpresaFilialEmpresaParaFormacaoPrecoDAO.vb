﻿Imports Core
Imports VO
Public Class RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoDAO
    Inherits DAOBase(Of VO.RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoVO)

    Public Sub New()
        daoConfiguration = New RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoDAOConfiguration()
    End Sub


    Public Function ValidaProdutoFracionado(codFilial As Decimal, codEmp As Decimal) As List(Of VO.RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoVO)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[select distinct a.codmer
                                     from mrt.t0111770 a, mrt.t0100086 b, mrt.RLCFILPCPFILCPL c
                                     where a.codmer = :MER
                                     and a.codfilemp = :FIL
                                     and a.codemp = 1
                                     and b.codmer = a.codmer
                                     and b.codemp = 1
                                     and b.QDEUNDVNDCXAFRN > 1
                                     and b.QDEMNMVNDMER < b.QDEUNDVNDCXAFRN 
                                     and c.codfilempepdcpl = :FIL
                                     and c.DATDSTRLC is null]]>.Value

        parameterMapper.Parameters.Add(":MER", codEmp)
        parameterMapper.Parameters.Add(":FIL", codFilial)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class

<TableAttribute("MRT.T0111770")>
Public Class RelacaMercadoriaEmpresaFilialEmpresaParaFormacaoPrecoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODMER As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODFILEMP As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODEMP As Nullable(Of Decimal)

    Property VLRCRBMRGMER As Nullable(Of Decimal)
    Property TIPMERMTD As String
    Property TIPMER As Nullable(Of Decimal)
    Property TIPCNSFLX As Nullable(Of Decimal)
    Property QDEDIAPRZFRN As Nullable(Of Decimal)
    Property PERMRGRSTFATMERMTD As Nullable(Of Decimal)
    Property PERMRGMNMFLXPCO As Nullable(Of Decimal)
    Property PERMRGMERORI As Nullable(Of Decimal)
    Property PERMRGMERNOR As Nullable(Of Decimal)
    Property PERMRGMERINF As Nullable(Of Decimal)
    Property PERMRGADIMER As Nullable(Of Decimal)
    Property PERMAXCNSFLX As Nullable(Of Decimal)
    Property PERCRBMRGMER As Nullable(Of Decimal)
    Property PERCMSORIMER As Nullable(Of Decimal)
    Property PERCMSNORMER As Nullable(Of Decimal)
    Property PERCMSINFMER As Nullable(Of Decimal)
    Property DATDSTMER As Nullable(Of Date)
    Property CODSMBSITMERFLXPCO As String
    Property CODSMBSITMER As String
    Property CODSITCMSADI As String
    Property CODPAPMER As Nullable(Of Decimal)
    Property CODFLXPCO As String



End Class
