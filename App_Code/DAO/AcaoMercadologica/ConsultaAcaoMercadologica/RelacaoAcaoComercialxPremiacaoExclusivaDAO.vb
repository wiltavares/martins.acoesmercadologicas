﻿Imports Core
Public Class RelacaoAcaoComercialxPremiacaoExclusivaDAO
    Inherits DAOBase(Of VO.RelacaoAcaoComercialxPremiacaoExclusiva)
    Public Sub New()
        daoConfiguration = New PremiacaoExclusivaDAOConfiguration()
    End Sub

    ''' <summary>
    ''' Lista as premiacoes exclusivas da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <returns></returns>
    Public Function ListarPremiacaoExclusivaDaAcao(codAcao As Decimal) As List(Of VO.RelacaoAcaoComercialxPremiacaoExclusiva)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql = <![CDATA[SELECT P.CODFRN, P.DESPRMEXV, P.VLRUTZACOCMC, F.NOMFRN, P.CODACOCMC FROM MRT.RLCACOCMCPRMEXV P
                INNER JOIN MRT.T0100426 F ON F.CODFRN = P.CODFRN
                WHERE P.CODACOCMC = :CODACOCMC
                ORDER BY P.CODFRN]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function
End Class

<Table("MRT.RLCACOCMCPRMEXV")>
Public Class PremiacaoExclusivaDAOConfiguration
    Inherits DAOConfiguration

    <IgnoreMap>
    Property NOMFRN As String

End Class