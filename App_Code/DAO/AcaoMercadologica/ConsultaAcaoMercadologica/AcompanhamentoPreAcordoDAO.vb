﻿Imports Core
Imports VO
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Globalization
Imports System.Data.Common

Public Class AcompanhamentoPreAcordoDAO
    Inherits DAOBase(Of VO.AcompanhamentoPreAcordo)

    Public Sub New()
        daoConfiguration = New AcaoComercialDAOConfiguration()
    End Sub

    Public Function ListaAcompanhamentoPreAcordo(dadosFiltro As VO.AcompanhamentoPreAcordo) As List(Of VO.AcompanhamentoPreAcordo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                            Filial.CODFILEMP
                                          , Filial.NOMFILEMP
                                          , MER.CODMER
                                          , MER.DESMER
                                          , FRN.CODFRN
                                          , FRN.NOMFRN
                                          , RELFRN.CODFRN AS CODFRNPTR
                                          , RELFRNNOM.NOMFRN AS NOMFRNPTR
                                          , PRO.CODPMC
                                          , CASE 
                                            WHEN PRO.CODPMC IS NULL OR PRO.CODPMC = 0 THEN 'Bonificação' 
                                            ELSE PRO.DESPMC
                                            END AS DESPMC
                                          , MOV.CODACOCMC  
                                          , MAX(ACAO.NOMACOCMC) AS NOMACOCMC
                                          , SUM(MOV.VLRUNTFNDMERPMC) AS VLRUNTFNDMERPMC
                                          , SUM(MOV.QDEITEPMC) AS QDEITEPMC
                                          , MOV.DATREFOPE       
                                          , CASE WHEN ACAO.DESCPHBTB  <> ' ' THEN
                                                1
                                             ELSE 
                                                0
                                             END AS INDCPHBTB    
                                     FROM MRT.MOVVLRRLZFNDPMC MOV
                                       INNER JOIN MRT.T0118430 ACAO ON ACAO.CODACOCMC = MOV.CODACOCMC
                                        LEFT JOIN MRT.T0112963 Filial ON MOV.CODFILEMP = Filial.CODFILEMP
                                        LEFT JOIN MRT.T0100086 MER ON MOV.CODMER = MER.CODMER
                                        LEFT JOIN MRT.T0100426 FRN ON MER.CODFRNPCPMER = FRN.CODFRN
                                        LEFT JOIN MRT.T0107935 PRO ON MOV.CODPMC = PRO.CODPMC
                                        LEFT JOIN MRT.T0113625 COMP ON COMP.CODCPR = FRN.CODCPR
                                        LEFT JOIN MRT.T0118570 CEL ON CEL.CODGERPRD = COMP.CODGERPRD
                                        LEFT JOIN MRT.T0138768 RELFRN ON RELFRN.CODACOCMC = ACAO.CODACOCMC
                                        LEFT JOIN MRT.T0100426 RELFRNNOM ON RELFRN.CODFRN = RELFRNNOM.CODFRN
                                     WHERE MOV.TIPOPEINIGRCHST = :TIPOPEINIGRCHST
                                       AND (MOV.DATREFOPE BETWEEN :DATAINICIO AND :DATAFIM)
                                       AND (:INDCPHBTB = -1 
                                                OR (:INDCPHBTB = 1 AND ACAO.DESCPHBTB  <> ' ') 
                                                OR (:INDCPHBTB = 0 AND ACAO.DESCPHBTB  = ' ')
                                           )
                                           
                                    ]]>.Value
        'Tipo Operação
        parameterMapper.Parameters.Add(":TIPOPEINIGRCHST", dadosFiltro.TIPOPEINIGRCHST)

        'Data do Movimento
        parameterMapper.Parameters.Add(":DATAINICIO", dadosFiltro.DATINIACOCMC)
        parameterMapper.Parameters.Add(":DATAFIM", dadosFiltro.DATFIMACOCMC)

        'Pre-Acordo
        If (dadosFiltro.CODACOCMC <> 0) Then
            sql = sql + " AND MOV.CODACOCMC = :CODACOCMC "
            parameterMapper.Parameters.Add(":CODACOCMC", dadosFiltro.CODACOCMC)
        End If

        'Fornecedor
        If (dadosFiltro.CODFRN <> 0) Then
            sql = sql + " AND FRN.CODFRN = :CODFRN "
            parameterMapper.Parameters.Add(":CODFRN", dadosFiltro.CODFRN)
        End If

        'Fornecedor Patrocinador
        If (dadosFiltro.CODFRNPTR <> 0) Then
            sql = sql + " AND RELFRN.CODFRN = :CODFRNPTR "
            parameterMapper.Parameters.Add(":CODFRNPTR", dadosFiltro.CODFRNPTR)
        End If

        'Comprador
        If (dadosFiltro.CODCPR <> 0) Then
            sql = sql + " AND COMP.CODCPR = :CODCPR "
            parameterMapper.Parameters.Add(":CODCPR", dadosFiltro.CODCPR)
        End If

        'Celula
        If (dadosFiltro.CODDIVCMP <> 0) Then
            sql = sql + " AND CEL.CODDIVCMP = :CODDIVCMP "
            parameterMapper.Parameters.Add(":CODDIVCMP", dadosFiltro.CODDIVCMP)
        End If

        'Código da Mercadoria
        If (dadosFiltro.CODMER <> 0) Then
            sql = sql + " AND MER.CODMER = :CODMER "
            parameterMapper.Parameters.Add(":CODMER", dadosFiltro.CODMER)
        End If

        'Nome da Mercadoria
        If (dadosFiltro.DESMER <> "") Then
            dadosFiltro.DESMER = "%" + UCase(dadosFiltro.DESMER) + "%"
            sql = sql + " AND UPPER(MER.DESMER) LIKE :DESMER "
            parameterMapper.Parameters.Add(":DESMER", dadosFiltro.DESMER)
        End If

        'Código Promoção
        If (dadosFiltro.CODPMC <> 0) Then
            sql = sql + " AND PRO.CODPMC = :CODPMC "
            parameterMapper.Parameters.Add(":CODPMC", dadosFiltro.CODPMC)
        End If

        'Filial
        If (dadosFiltro.CODFILEMP <> 0) Then
            sql = sql + " AND Filial.CODFILEMP = :CODFILEMP "
            parameterMapper.Parameters.Add(":CODFILEMP", dadosFiltro.CODFILEMP)
        End If

        parameterMapper.Parameters.Add(":INDCPHBTB", dadosFiltro.INDCPHBTB)

        sql = sql + " GROUP BY Filial.CODFILEMP, Filial.NOMFILEMP, MER.CODMER, MER.DESMER"
        sql = sql + "       , FRN.CODFRN, FRN.NOMFRN, RELFRN.CODFRN, RELFRNNOM.NOMFRN, PRO.CODPMC, PRO.DESPMC, MOV.CODACOCMC,MOV.DATREFOPE "
        sql = sql + "       , CASE WHEN ACAO.DESCPHBTB  <> ' ' THEN "
        sql = sql + "         1 "
        sql = sql + "         ELSE "
        sql = sql + "         0 "
        sql = sql + "         END "
        sql = sql + "         ORDER BY DATREFOPE "

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function
    Public Function ListaAcompanhamentoPreAcordoCliente(dadosFiltro As VO.AcompanhamentoPreAcordo) As List(Of VO.AcompanhamentoPreAcordo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                            Filial.CODFILEMP
                                          , Filial.NOMFILEMP
                                          , MER.CODMER
                                          , MER.DESMER
                                          , FRN.CODFRN
                                          , FRN.NOMFRN 
                                          , RELFRN.CODFRN AS CODFRNPTR
                                          , RELFRNNOM.NOMFRN AS NOMFRNPTR
                                          , PRO.CODPMC
                                          , CASE 
                                            WHEN PRO.CODPMC IS NULL OR PRO.CODPMC = 0 THEN 'Bonificação' 
                                            ELSE PRO.DESPMC
                                            END AS DESPMC
                                          , MOV.CODACOCMC  
                                          , MAX(ACAO.NOMACOCMC) AS NOMACOCMC
                                          , CLI.CODCLI
                                          , CLI.NOMCLI
                                          , HST.DATIPRNOTFSC
                                          , HST.NUMITEORI
                                          , HST.NUMDOCOPE
                                          , SUM(HST.VLRUNTFNDMERPMC) AS VLRUNTFNDMERPMC
                                          , SUM(HST.QDEITEPMC) AS QDEITEPMC
                                          , MOV.DATREFOPE
                                          , CASE WHEN ACAO.DESCPHBTB  <> ' ' THEN
                                                1
                                             ELSE 
                                                0
                                             END AS INDCPHBTB         
                                     FROM MRT.MOVVLRRLZFNDPMC MOV
                                       INNER JOIN MRT.T0118430 ACAO ON ACAO.CODACOCMC = MOV.CODACOCMC
                                        LEFT JOIN MRT.T0112963 Filial ON MOV.CODFILEMP = Filial.CODFILEMP
                                        LEFT JOIN MRT.T0100086 MER ON MOV.CODMER = MER.CODMER
                                        LEFT JOIN MRT.T0100426 FRN ON MER.CODFRNPCPMER = FRN.CODFRN
                                        LEFT JOIN MRT.T0107935 PRO ON MOV.CODPMC = PRO.CODPMC
                                        LEFT JOIN MRT.HSTFNDMERBDE HST ON HST.CODACOCMC = ACAO.CODACOCMC
                                                                      AND HST.TIPOPEINIGRCHST = MOV.TIPOPEINIGRCHST
                                                                    AND HST.DATREFOPE = MOV.DATREFOPE
                                                                    AND HST.CODEMP = MOV.CODEMP
                                                                    AND HST.CODFILEMP = MOV.CODFILEMP
                                                                    AND HST.CODMER = MOV.CODMER
                                                                    AND MOV.CODPMC = HST.CODPMC
                                        LEFT JOIN MRT.T0100043 CLI ON CLI.CODCLI = HST.CODCLI
                                        LEFT JOIN MRT.T0113625 COMP ON COMP.CODCPR = FRN.CODCPR
                                        LEFT JOIN MRT.T0118570 CEL ON CEL.CODGERPRD = COMP.CODGERPRD
                                        LEFT JOIN MRT.T0138768 RELFRN ON RELFRN.CODACOCMC = ACAO.CODACOCMC
                                        LEFT JOIN MRT.T0100426 RELFRNNOM ON RELFRN.CODFRN = RELFRNNOM.CODFRN
                                     WHERE MOV.TIPOPEINIGRCHST = :TIPOPEINIGRCHST
                                       AND (MOV.DATREFOPE BETWEEN :DATAINICIO AND :DATAFIM)
                                       AND (:INDCPHBTB = -1 
                                                OR (:INDCPHBTB = 1 AND ACAO.DESCPHBTB  <> ' ') 
                                                OR (:INDCPHBTB = 0 AND ACAO.DESCPHBTB  = ' ')
                                           )
                                           
                                    ]]>.Value
        'Tipo Operação
        parameterMapper.Parameters.Add(":TIPOPEINIGRCHST", dadosFiltro.TIPOPEINIGRCHST)

        'Data do Movimento
        parameterMapper.Parameters.Add(":DATAINICIO", dadosFiltro.DATINIACOCMC)
        parameterMapper.Parameters.Add(":DATAFIM", dadosFiltro.DATFIMACOCMC)

        'Pre-Acordo
        If (dadosFiltro.CODACOCMC <> 0) Then
            sql = sql + " AND MOV.CODACOCMC = :CODACOCMC "
            parameterMapper.Parameters.Add(":CODACOCMC", dadosFiltro.CODACOCMC)
        End If

        'Fornecedor
        If (dadosFiltro.CODFRN <> 0) Then
            sql = sql + " AND FRN.CODFRN = :CODFRN "
            parameterMapper.Parameters.Add(":CODFRN", dadosFiltro.CODFRN)
        End If

        'Fornecedor Patrocinador
        If (dadosFiltro.CODFRNPTR <> 0) Then
            sql = sql + " AND RELFRN.CODFRN = :CODFRNPTR "
            parameterMapper.Parameters.Add(":CODFRNPTR", dadosFiltro.CODFRNPTR)
        End If

        'Comprador
        If (dadosFiltro.CODCPR <> 0) Then
            sql = sql + " AND COMP.CODCPR = :CODCPR "
            parameterMapper.Parameters.Add(":CODCPR", dadosFiltro.CODCPR)
        End If

        'Celula
        If (dadosFiltro.CODDIVCMP <> 0) Then
            sql = sql + " AND CEL.CODDIVCMP = :CODDIVCMP "
            parameterMapper.Parameters.Add(":CODDIVCMP", dadosFiltro.CODDIVCMP)
        End If

        'Código da Mercadoria
        If (dadosFiltro.CODMER <> 0) Then
            sql = sql + " AND MER.CODMER = :CODMER "
            parameterMapper.Parameters.Add(":CODMER", dadosFiltro.CODMER)
        End If

        'Nome da Mercadoria
        If (dadosFiltro.DESMER <> "") Then
            dadosFiltro.DESMER = "%" + UCase(dadosFiltro.DESMER) + "%"
            sql = sql + " AND UPPER(MER.DESMER) LIKE :DESMER "
            parameterMapper.Parameters.Add(":DESMER", dadosFiltro.DESMER)
        End If

        'Código Promoção
        If (dadosFiltro.CODPMC <> 0) Then
            sql = sql + " AND PRO.CODPMC = :CODPMC "
            parameterMapper.Parameters.Add(":CODPMC", dadosFiltro.CODPMC)
        End If

        'Filial
        If (dadosFiltro.CODFILEMP <> 0) Then
            sql = sql + " AND Filial.CODFILEMP = :CODFILEMP "
            parameterMapper.Parameters.Add(":CODFILEMP", dadosFiltro.CODFILEMP)
        End If


        parameterMapper.Parameters.Add(":INDCPHBTB", dadosFiltro.INDCPHBTB)

        sql = sql + " GROUP BY Filial.CODFILEMP, Filial.NOMFILEMP, MER.CODMER, MER.DESMER"
        sql = sql + "       , FRN.CODFRN, FRN.NOMFRN, RELFRN.CODFRN, RELFRNNOM.NOMFRN, PRO.CODPMC, PRO.DESPMC, MOV.CODACOCMC  "
        sql = sql + "       , CLI.CODCLI, CLI.NOMCLI, HST.DATIPRNOTFSC, HST.NUMITEORI, HST.NUMDOCOPE, MOV.DATREFOPE "
        sql = sql + "       , CASE WHEN ACAO.DESCPHBTB  <> ' ' THEN "
        sql = sql + "         1 "
        sql = sql + "         ELSE "
        sql = sql + "         0 "
        sql = sql + "         END "
        sql = sql + " ORDER BY DATREFOPE  "

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    <Table("MRT.MOVVLRRLZFNDPMC")>
    Public Class AcaoComercialDAOConfiguration
        Inherits DAOConfiguration

        <DateInsert>
       <[Readonly](False, True)>
        Property DATREFOPE As Date?

        <DateInsert>
        <[Readonly](False, True)>
        Property TIPOPEINIGRCHST As String

        <DateInsert>
        <[Readonly](True, False)>
        Property CODEMP As Nullable(Of Decimal)

        <DateInsert>
        <[Readonly](False, True)>
        Property CODFILEMP As Nullable(Of Decimal)

        <DateInsert>
        <[Readonly](False, True)>
        Property CODACOCMC As Nullable(Of Decimal)

        <DateInsert>
        <[Readonly](False, True)>
        Property CODPMC As Nullable(Of Decimal)

        <DateInsert>
        <[Readonly](False, True)>
        Property CODMER As Nullable(Of Decimal)

        <DateInsert>
        <[Readonly](False, True)>
        Property VLRUNTFNDMERPMC As Nullable(Of Decimal)

        <DateInsert>
        <[Readonly](False, True)>
        Property QDEITEPMC As Nullable(Of Decimal)


#Region "Campos Auxiliares"
        <IgnoreMap>
        Property INDCPHBTB As Nullable(Of Decimal)
        <IgnoreMap>
        Property CODFRNPTR As Nullable(Of Decimal)
        <IgnoreMap>
        Property NOMFRNPTR As Nullable(Of Decimal)
#End Region
    End Class
End Class

