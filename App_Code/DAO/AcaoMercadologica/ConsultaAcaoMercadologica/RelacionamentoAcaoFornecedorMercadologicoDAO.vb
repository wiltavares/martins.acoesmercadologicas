﻿Imports Core
Public Class RelacionamentoAcaoFornecedorMercadologicoDAO
    Inherits DAOBase(Of VO.RelacionamentoAcaoFornecedorMercadologico)

    Public Sub New()
        daoConfiguration = New RelacionamentoAcaoFornecedorMercadologicoDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.RLCACOFRNMCO")>
Public Class RelacionamentoAcaoFornecedorMercadologicoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODFRN As Nullable(Of Integer)

End Class


