﻿Imports Core
Imports VO
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Globalization
Imports System.Data.Common
Imports System.Data

Public Class AcaoComercialDAO
    Inherits DAOBase(Of VO.AcaoComercial)

    Public Sub New()
        daoConfiguration = New AcaoComercialDAOConfiguration()
    End Sub

    ''' <summary>
    ''' BscDdoAco
    ''' Consulta acoes mercadologicas de acordo com o filtro 
    ''' </summary>
    ''' <param name="vo"></param>
    ''' <returns></returns>
    Public Function ListarAcaoMercadologica(vo As VO.AcaoComercial) As List(Of VO.AcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        'vo.NOMACOCMC = If(vo.NOMACOCMC, "%" + vo.NOMACOCMC + "%")
        If Not vo.NOMACOCMC Is Nothing Then
            vo.NOMACOCMC = "%" + UCase(vo.NOMACOCMC) + "%"
        End If

        Dim sql As String = <![CDATA[ SELECT DISTINCT(A.CODACOCMC) As CODACOCMC,
                                    A.NOMACOCMC,
                                    A.TIPDSNDSCBNF,
                                    A.DATINIACOCMC,
                                    A.DESOBJACOCMC,
                                    A.INDACOCMCCPL,
                                    A.VLRTOTACOCMC,
                                    A.VLRTOTOCDACOCMC,
                                    A.VLRTOTTXAADMACOCMC,
                                    A.VLRTOTBDEACOCMC,
                                    A.CODFNCCRIACOCMC,
                                    COALESCE(C.NOMFNC, D.NOMANSTCR) AS NOMFNC, 
                                    A.DATAPVACOCMC,
                                    A.TIPFLUAPVACOCMC,
                                    A.CODSTAAPVACOCMC,
                                    A.DATFIMACOCMC,
                                    A.CODACOCMCORI,
                                    A.DATGRCACOCMC,
                                    B.TIPEVTACOCMC,
                                    B.CODEVTACOCMC,
                                    B.ANOMESREF,
                                    CODCNLVNDINI,
                                    A.TIPACOMCD,
                                    A.CODACOCMCCPI
                                    FROM MRT.T0118430  A
                                    LEFT JOIN MRT.T0155581 B
                                      ON A.CODACOCMC = B.CODACOCMC
                                    LEFT JOIN MRT.T0100361 C
                                      ON A.CODFNCCRIACOCMC = C.CODFNC
                                    LEFT JOIN MRT.CADANSINFTCR D 
                                      ON  A.CODFNCCRIACOCMC = D.CODANSTCR
                                    WHERE ( (:TIPACOMCD = 1 AND A.TIPACOMCD IN (1,2,3,4)) OR (:TIPACOMCD = 5 AND A.TIPACOMCD IN (5,6)))
                                    AND A.DATFIMACOCMC <> TO_DATE('0001-01-01','YYYY-MM-DD') AND
                                    (:CODACOCMC IS NULL OR :CODACOCMC = A.CODACOCMC)
                                     AND (:NOMACOCMC IS NULL OR UPPER(A.NOMACOCMC) LIKE :NOMACOCMC)
                                     AND (:DATINIACOCMC IS NULL OR :DATFIMACOCMC IS NULL OR A.DATINIACOCMC BETWEEN :DATINIACOCMC AND :DATFIMACOCMC)
                                     AND (:CODSTAAPVACOCMC IS NULL OR :CODSTAAPVACOCMC = A.CODSTAAPVACOCMC)
                                     AND (:CODFNCCRIACOCMC IS NULL OR :CODFNCCRIACOCMC = A.CODFNCCRIACOCMC)
                                     AND (:TIPEVTACOCMC IS NULL OR :TIPEVTACOCMC = B.TIPEVTACOCMC)
                                     AND (:CODEVTACOCMC IS NULL OR :CODEVTACOCMC = B.CODEVTACOCMC)
                                    ORDER BY A.CODACOCMC]]>.Value


        parameterMapper.Parameters.Add(":TIPACOMCD", ValueDBProperty(vo.TIPACOMCD))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NOMACOCMC", ValueDBProperty(vo.NOMACOCMC))
        parameterMapper.Parameters.Add(":CODSTAAPVACOCMC", ValueDBProperty(vo.CODSTAAPVACOCMC))
        parameterMapper.Parameters.Add(":DATINIACOCMC", ValueDBProperty(vo.DATINIACOCMC))
        parameterMapper.Parameters.Add(":DATFIMACOCMC", ValueDBProperty(vo.DATFIMACOCMC))
        parameterMapper.Parameters.Add(":CODFNCCRIACOCMC", ValueDBProperty(vo.CODFNCCRIACOCMC))
        parameterMapper.Parameters.Add(":TIPEVTACOCMC", ValueDBProperty(vo.TIPEVTACOCMC))
        parameterMapper.Parameters.Add(":CODEVTACOCMC", ValueDBProperty(vo.CODEVTACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' BscDdoAcoMhnApv
    ''' Listar acoes mercadologicas de acordo com o filtro quando o status é 7 (Minhas aprovacoes)
    ''' Passa o usuario de rede como parametro
    ''' </summary>
    ''' <param name="vo"></param>
    ''' <returns></returns>
    Public Function ListarAcaoMercadologicaMinhasAprovacoes(vo As VO.AcaoComercial, CodFncSis As Integer) As List(Of VO.AcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        'vo.NOMACOCMC = If(vo.NOMACOCMC, "%" + vo.NOMACOCMC + "%")
        If Not vo.NOMACOCMC Is Nothing Then
            vo.NOMACOCMC = "%" + UCase(vo.NOMACOCMC) + "%"
        End If

        Dim sql As String = <![CDATA[ SELECT DISTINCT (A.CODACOCMC) AS CODACOCMC
                                                    , A.NOMACOCMC
                                                    , A.TIPDSNDSCBNF
                                                    , A.DATINIACOCMC
                                                    , A.DESOBJACOCMC
                                                    , A.INDACOCMCCPL
                                                    , A.VLRTOTACOCMC
                                                    , A.VLRTOTOCDACOCMC
                                                    , A.VLRTOTTXAADMACOCMC
                                                    , A.VLRTOTBDEACOCMC
                                                    , A.CODFNCCRIACOCMC
                                                    , COALESCE(C.NOMFNC, D.NOMANSTCR) AS NOMFNC
                                                    , A.DATAPVACOCMC
                                                    , A.TIPFLUAPVACOCMC
                                                    , A.CODSTAAPVACOCMC
                                                    , A.DATFIMACOCMC
                                                    , A.CODACOCMCORI
                                                    , A.DATGRCACOCMC
                                                    , B.TIPEVTACOCMC
                                                    , B.CODEVTACOCMC
                                                    , B.ANOMESREF
                                                    , A.TIPACOMCD
                                                    , A.CODACOCMCCPI
                                                FROM MRT.T0118430  A
                                                    INNER JOIN MRT.RLCACOCMCFLUAPV E  ON (A.CODACOCMC = E.CODACOCMC AND E.CODSTAAPVACOCMC = A.CODSTAAPVACOCMC)                 
                                                    INNER JOIN MRT.T0161591 F ON (
                                                                                        E.NUMFLUAPV = F.NUMFLUAPV 
                                                                                AND (
                                                                                        F.TIPSTAFLUAPV  = '3' 
                                                                                        OR (
                                                                                            F.TIPSTAFLUAPV  = '8' 
                                                                                        AND F.DATHRAAPVFLU IS NULL
                                                                                        )
                                                                                    ) 
                                                                                AND (
                                                                                        F.CODEDEAPV IN (SELECT DISTINCT G.CODFNCAPVNGC FROM MRT.T0153350 G    
                                                                                                            WHERE G.CODFNCARZAPVNGC = :CODFNCSIS 
                                                                                                            AND G.DATDST IS NULL                                   
                                                                                                            AND SYSDATE BETWEEN G.DATINI AND G.DATFIM
                                                                                                        )
                                                                                        OR F.CODEDEAPV = :CODFNCSIS
                                                                                    )
                                                                                )  
                                                LEFT JOIN MRT.T0155581 B ON A.CODACOCMC = B.CODACOCMC
                                                LEFT JOIN MRT.T0100361 C ON A.CODFNCCRIACOCMC = C.CODFNC                           
                                                LEFT JOIN MRT.CADANSINFTCR D ON A.CODFNCCRIACOCMC = D.CODANSTCR
                                                WHERE (:TIPACOMCD IS NULL OR (:TIPACOMCD = 1 AND A.TIPACOMCD IN (1,2,3,4)) OR (:TIPACOMCD = 5 AND A.TIPACOMCD IN (5,6)))
                                                  AND (A.DATFIMACOCMC <> TO_DATE('0001-01-01','YYYY-MM-DD'))
                                                  AND (:CODACOCMC IS NULL OR :CODACOCMC = A.CODACOCMC)
                                                  AND (:NOMACOCMC IS NULL OR UPPER(A.NOMACOCMC) LIKE :NOMACOCMC)
                                                  AND (:DATINIACOCMC IS NULL OR :DATFIMACOCMC IS NULL OR A.DATINIACOCMC BETWEEN :DATINIACOCMC AND :DATFIMACOCMC)
                                                  AND (:CODSTAAPVACOCMC IS NULL OR :CODSTAAPVACOCMC = A.CODSTAAPVACOCMC)
                                                  AND (:CODFNCCRIACOCMC IS NULL OR :CODFNCCRIACOCMC = A.CODFNCCRIACOCMC)
                                                  AND (:TIPEVTACOCMC IS NULL OR :TIPEVTACOCMC = B.TIPEVTACOCMC)
                                                  AND (:CODEVTACOCMC IS NULL OR :CODEVTACOCMC = B.CODEVTACOCMC)
                                                ORDER BY A.CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":TIPACOMCD", ValueDBProperty(vo.TIPACOMCD))
        parameterMapper.Parameters.Add(":CODFNCSIS", CodFncSis)
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NOMACOCMC", ValueDBProperty(vo.NOMACOCMC))
        parameterMapper.Parameters.Add(":CODSTAAPVACOCMC", ValueDBProperty(vo.CODSTAAPVACOCMC))
        parameterMapper.Parameters.Add(":DATINIACOCMC", ValueDBProperty(vo.CODACOCMCORI))
        parameterMapper.Parameters.Add(":DATFIMACOCMC", ValueDBProperty(vo.CODACOCMCORI))
        parameterMapper.Parameters.Add(":CODFNCCRIACOCMC", ValueDBProperty(vo.CODFNCCRIACOCMC))
        parameterMapper.Parameters.Add(":TIPEVTACOCMC", ValueDBProperty(vo.TIPEVTACOCMC))
        parameterMapper.Parameters.Add(":CODEVTACOCMC", ValueDBProperty(vo.CODEVTACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))


    End Function

    ''' <summary>
    ''' Select Valor Funding da Acao
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    ''' <returns></returns>
    Public Function SelectValorFunding(acaoComercial As VO.AcaoComercial) As List(Of VO.AcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CASE WHEN B.FLGMERPMC <> 'S' AND A.VLRPTCCLIITEACOCMC = 0 
                                        THEN ROUND(A.VLRUNTITEOCDACOCMC, 4) 
                                        WHEN B.FLGMERPMC <> 'S' AND A.VLRPTCCLIITEACOCMC > 0 
                                        THEN ROUND(A.VLRPTCFRNITEACOCMC, 4) 
                                        ELSE 0 END AS VLRFUNDING 
                                        FROM MRT.T0138776 A, MRT.T0201350 B 
                                        WHERE A.CODMER = :CODMER
                                        AND A.CODACOCMC = :CODACOCMC
                                        AND B.CODMER = A.CODMER 
                                        UNION  
                                        SELECT CASE WHEN B.FLGMERPMC <> 'S' AND C.VLRPTCCLIITEACOCMC = 0 
                                                    THEN ROUND(A.VLRUNTITEOCDACOCMC, 4) 
                                                    WHEN B.FLGMERPMC <> 'S' AND C.VLRPTCCLIITEACOCMC > 0 
                                                    THEN ROUND((((A.VLRUNTITEOCDACOCMC * (( C.VLRPTCFRNITEACOCMC * 100) 
                                                         / C.VLRUNTITEOCDACOCMC )) / 100 )), 4) 
                                                    ELSE 0 END AS VLR_FUNDING 
                                        FROM MRT.RLCITEPCPITESBT A, MRT.T0201350 B,  MRT.T0138776 C 
                                        WHERE A.CODMERSBT = :CODMER
                                        AND A.CODACOCMC = :CODACOCMC
                                        AND B.CODMER = A.CODMER 
                                        AND C.CODACOCMC = A.CODACOCMC 
                                        AND C.NUMLNHITEOCDACOCMC = A.NUMLNHITEOCDACOCMC 
                                        AND C.CODMER = A.CODMER 
                        ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", acaoComercial.CODACOCMC)
        parameterMapper.Parameters.Add(":CODMER", acaoComercial.CODMER)
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"VLRFUNDING"}))

    End Function

    ''' <summary>
    ''' Busca os dados da acao comercial pelo id
    ''' </summary>
    ''' <param name="CODACOCMC"></param>
    ''' <returns></returns>
    Public Function BuscarAcaoComercialPorId(CODACOCMC As Decimal?) As VO.AcaoComercial

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC AS CODACOCMC
                                          , A.NOMACOCMC
                                          , A.DATINIACOCMC
                                          , A.INDACOCMCCPL
                                          , A.VLRTOTACOCMC
                                          , A.VLRTOTOCDACOCMC
                                          , A.VLRTOTTXAADMACOCMC
                                          , A.VLRTOTBDEACOCMC
                                          , A.CODFNCCRIACOCMC
                                          , COALESCE(I.NOMFNC, CADT.NOMANSTCR) AS NOMFNC
                                          , COALESCE(K.NOMUSRRCF, L.NOMUSRRCF) AS NOMUSRRCF
                                          , A.DATAPVACOCMC
                                          , A.TIPFLUAPVACOCMC
                                          , A.CODSTAAPVACOCMC
                                          , A.CODFNCAPVATUACOCMC
                                          , COALESCE(J.NOMFNC, CADTCR.NOMANSTCR) AS NOMFNCAPVATU
                                          , A.DATFIMACOCMC
                                          , A.CODACOCMCORI
                                          , A.DATGRCACOCMC
                                          , A.INDALTFRNACOCMC
                                          , A.CODCNLVNDINI
                                          , A.TIPACOMCD
                                          , A.CODFILEMPORIVBA
                                          , A.QDEDIARCBPMS
                                          , A.TIPFRMDSCBNF
                                          , F.DESFRMDSCBNF
                                          , A.DESCPHBTB
                                          , A.INDVCLPMC
                                          , A.INDACOCMCBTBVLR
                                          , A.CODACOCMCCPI
                                          , H.INDEVTSMA
                                          , A.TIPDSNDSPCBK
                                          , A.CODEVTBTB
                                     FROM MRT.T0118430              A     
                                       LEFT JOIN MRT.T0100361       I ON I.CODFNC = A.CODFNCCRIACOCMC
                                       LEFT JOIN MRT.CADANSINFTCR   CADT ON A.CODFNCCRIACOCMC = CADT.CODANSTCR         
                                       LEFT JOIN MRT.T0104596       K ON K.CODFNC = i.CODFNC
                                       LEFT JOIN MRT.T0104596       L ON CADT.CODANSTCR = L.CODFNC 
                                       LEFT JOIN MRT.T0100361       J ON J.CODFNC = A.CODFNCAPVATUACOCMC
                                       LEFT JOIN MRT.CADANSINFTCR   CADTCR ON A.CODFNCAPVATUACOCMC = CADTCR.CODANSTCR     
                                       LEFT JOIN MRT.T0113552       F on F.TIPFRMDSCBNF = A.TIPFRMDSCBNF
                                       LEFT JOIN MRT.T0155581       G ON A.CODACOCMC = G.CODACOCMC
                                       LEFT JOIN MRT.T0155522       H ON G.TIPEVTACOCMC = H.TIPEVTACOCMC
                                     WHERE A.CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql)).FirstOrDefault()

    End Function

    Public Function BscDdoAcoNumEma(ByVal CODACOCMC As Decimal) As VO.AcaoComercial

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC,                                           
                                            A.NOMACOCMC,                                           
                                            A.DATINIACOCMC,                                        
                                            B.TIPEVTACOCMC,                                        
                                            C.NOMTIPEVTACOCMC,                                     
                                            B.CODEVTACOCMC,                                        
                                            D.NOMEVTACOCMC,                                        
                                            A.INDACOCMCCPL,                                        
                                            A.VLRTOTACOCMC,                                        
                                            A.VLRTOTOCDACOCMC,                                     
                                            A.VLRTOTTXAADMACOCMC,                                  
                                            A.VLRTOTBDEACOCMC,                                     
                                            A.CODFNCCRIACOCMC,                                     
                                            COALESCE(I.NOMFNC, CADT.NOMANSTCR) AS NOMFNC,                                              
                                            K.NOMUSRRCF,                                           
                                            A.DATAPVACOCMC,                                        
                                            A.TIPFLUAPVACOCMC,                                     
                                            A.CODSTAAPVACOCMC,                                     
                                            A.CODFNCAPVATUACOCMC,                                  
                                            COALESCE(J.NOMFNC, CADTCR.NOMANSTCR) AS NOMFNCAPVATU,                              
                                            A.DATFIMACOCMC,                                        
                                            A.CODACOCMCORI,                                        
                                            A.DATGRCACOCMC,                                        
                                            D.TIPDSNDSCBNF,                                        
                                            F.DESDSNDSCBNF, CODCNLVNDINI                           
                                      FROM MRT.T0118430 A                                          
                                     INNER JOIN MRT.T0155581 B ON B.CODACOCMC = A.CODACOCMC                        
                                      LEFT JOIN MRT.T0155522 C ON C.TIPEVTACOCMC = B.TIPEVTACOCMC                  
                                      LEFT JOIN MRT.T0155531 D ON D.CODEVTACOCMC = B.CODEVTACOCMC                  
                                      LEFT JOIN MRT.T0109059 F ON F.TIPDSNDSCBNF = D.TIPDSNDSCBNF                  
                                      LEFT JOIN MRT.T0100361 I ON I.CODFNC = A.CODFNCCRIACOCMC
                                      LEFT JOIN MRT.CADANSINFTCR CADT ON A.CODFNCCRIACOCMC = CADT.CODANSTCR                     
                                      LEFT JOIN MRT.T0104596 K ON K.CODFNC = i.CODFNC                              
                                      LEFT JOIN MRT.T0100361 J ON J.CODFNC = A.CODFNCAPVATUACOCMC
                                      LEFT JOIN MRT.CADANSINFTCR CADTCR ON A.CODFNCAPVATUACOCMC = CADTCR.CODANSTCR
                                     WHERE A.CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql)).FirstOrDefault()

    End Function

    ''' <summary>
    ''' BuscarMaxCodAcaoComercial
    ''' </summary>
    ''' <returns></returns>
    Public Function BuscarMaxCodAcaoComercial() As Decimal?

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = "SELECT MAX(CODACOCMC) AS CODACOCMC FROM MRT.T0118430"

        Return connector.Execute(sql)

    End Function

    ''' <summary>
    '''  Calcula Saldo do Fornecedor de Marketing
    ''' </summary>
    ''' <param name="iCodFrn"></param>
    ''' <param name="iTipDsnDscBnf"></param>
    ''' <param name="iAnoMesRef"></param>
    ''' <returns></returns>
    Public Function BuscarSaldoFornecedorMarketing(iCodFrn As Long, iTipDsnDscBnf As Integer, iAnoMesRef As Integer) As List(Of VO.SaldoFornecedorMarketing)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[With SALDORESERVADO As (
                            Select D.TIPDSNDSCBNF, NVL(COALESCE(TRUNC(SUM(D.VLRSLDRSVFRN), 2), 0),0) As SLDRSV 
                            FROM MRT.T0123159 D, MRT.T0101295 B 
                            WHERE D.FLGAPVACOCMC = 'N'
                            And D.TIPDSNDSCBNF   = :iTipDsnDscBnf
                            AND D.CODFRN <> B.CODFRNMKT
                            And B.CODEMP = 1
                            GROUP BY D.TIPDSNDSCBNF),

                            SLDCNTCRR AS (  SELECT C.TIPDSNDSCBNF, NVL(sum(C.VLRSLDMESANT + C.VLRCRDMESCRR - C.VLRDEBMESCRR),0) AS SLDCNTCRR
                            FROM MRT.T0123078 C, MRT.T0101295 B  
                            WHERE C.TIPDSNDSCBNF = :iTipDsnDscBnf
                            AND C.ANOMESREF = :iAnoMesRef
                            And C.CODFRN <> B.CODFRNMKT
                            AND B.CODEMP = 1
                            And C.CODFRN > 0
                            GROUP BY C.TIPDSNDSCBNF),

                            SLDTOTFRN AS (
                            Select A.TIPDSNDSCBNF, SUM( A.SLDCNTCRR - NVL(B.SLDRSV,0) ) As SLDTOTFRN
                            FROM SLDCNTCRR A LEFT OUTER JOIN SALDORESERVADO B
                            On A.TIPDSNDSCBNF = B.TIPDSNDSCBNF
                            GROUP BY A.TIPDSNDSCBNF), 

                            SLDRSVMKT AS ( SELECT  D.CODFRN, D.TIPDSNDSCBNF, COALESCE(TRUNC(SUM(D.VLRSLDRSVFRN), 2), 0) AS SLDRSVMKT
                            FROM MRT.T0123159 D
                            WHERE D.FLGAPVACOCMC = 'N'
                            And D.CODFRN = :iCodFrn
                            AND D.TIPDSNDSCBNF = :iTipDsnDscBnf
                            GROUP BY D.CODFRN, D.TIPDSNDSCBNF),

                            SLDCNTCRRMKT AS ( SELECT A.CODFRN, A.TIPDSNDSCBNF, (A.VLRSLDMESANT + A.VLRCRDMESCRR - A.VLRDEBMESCRR) AS SLDCNTCRRMKT
                            FROM MRT.T0123078 A 
                            WHERE A.CODFRN = :iCodFrn   
                            AND A.TIPDSNDSCBNF = :iTipDsnDscBnf
                            And A.ANOMESREF = :iAnoMesRef),

                            SLDTOTMKT AS (  
                            SELECT A.CODFRN, A.TIPDSNDSCBNF, ( NVL(A.SLDCNTCRRMKT,0) - NVL(B.SLDRSVMKT,0) ) AS SLDTOTMKT
                            FROM SLDCNTCRRMKT A LEFT OUTER JOIN SLDRSVMKT B 
                            ON A.CODFRN = B.CODFRN             
                            And A.TIPDSNDSCBNF = B.TIPDSNDSCBNF)

                            Select B.CODFRN, C.NOMFRN, A.TIPDSNDSCBNF, D.DESDSNDSCBNF,
                            ( CASE WHEN B.SLDTOTMKT < 0 THEN A.SLDTOTFRN + B.SLDTOTMKT ELSE A.SLDTOTFRN - B.SLDTOTMKT END ) AS VLRSLDDSP
                            FROM SLDTOTFRN A, SLDTOTMKT B, MRT.T0100426 C, MRT.T0109059 D  
                            WHERE A.TIPDSNDSCBNF = B.TIPDSNDSCBNF 
                            And B.CODFRN = C.CODFRN
                            And A.TIPDSNDSCBNF = D.TIPDSNDSCBNF]]>.Value

        parameterMapper.Parameters.Add(":iCodFrn", iCodFrn)
        parameterMapper.Parameters.Add(":iTipDsnDscBnf", iTipDsnDscBnf)
        parameterMapper.Parameters.Add(":iAnoMesRef", iAnoMesRef)

        Dim properties = {"CODFRN", "NOMFRN", "TIPDSNDSCBNF", "DESDSNDSCBNF", "VLRSLDDSP"}
        Return connector.SelectTList(sql, parameterMapper, DAORowMapper(Of VO.SaldoFornecedorMarketing).RowMapperOnlyProperties(properties))

    End Function

    ''' <summary>
    ''' Busca numero de reprovacoes e politicas de uma acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <returns></returns>
    Public Function BuscarNumeroReprovacoesEPoliticas(codAcao As Integer) As VO.AcaoComercial

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = " SELECT count(CODACOCMC) AS TotalPoliticas FROM MRT.T0155808 WHERE CODACOCMC = :CODACOCMC "

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql)).FirstOrDefault()

    End Function

    ''' <summary>
    ''' InsDdoAcoCmc
    ''' Insere dados na tabela de acao comercial
    ''' </summary>
    ''' <param name="acaoComercial"></param>
    ''' <returns></returns>
    Public Function InserirAcaoComercial(acaoComercial As VO.AcaoComercial) As VO.AcaoComercial

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                  INSERT INTO MRT.T0118430
                      ( CODACOCMC,
                        NOMACOCMC,
                        TIPDSNDSCBNF,
                        DATINIACOCMC,
                        DESOBJACOCMC,
                        INDACOCMCCPL,
                        VLRTOTACOCMC,
                        VLRTOTOCDACOCMC,
                        VLRTOTTXAADMACOCMC,
                        VLRTOTBDEACOCMC,
                        CODFNCCRIACOCMC,
                        DATAPVACOCMC,
                        TIPFLUAPVACOCMC,
                        CODSTAAPVACOCMC,
                        CODFNCAPVATUACOCMC,
                        DATFIMACOCMC,
                        CODACOCMCORI,
                        DATGRCACOCMC,
                        INDALTFRNACOCMC,
                        TIPACOMCD, 
                        CODCNLVNDINI,
                        CODFILEMPORIVBA,
                        DESCPHBTB,
                        INDVCLPMC,
                        INDACOCMCBTBVLR,
                        CODACOCMCCPI,
                        TIPDSNDSPCBK,
                        CODEVTBTB)   
                VALUES( :CODACOCMC,
                        :NOMACOCMC,
                        :TIPDSNDSCBNF,
                        :DATINIACOCMC,
                        :DESOBJACOCMC,
                        :INDACOCMCCPL,
                        :VLRTOTACOCMC,
                        :VLRTOTOCDACOCMC,
                        :VLRTOTTXAADMACOCMC,
                        :VLRTOTBDEACOCMC,
                        :CODFNCCRIACOCMC,
                        :DATAPVACOCMC,
                        :TIPFLUAPVACOCMC,
                        :CODSTAAPVACOCMC,
                        :CODFNCAPVATUACOCMC,
                        :DATFIMACOCMC,
                        :CODACOCMCORI,
                        :DATGRCACOCMC,
                        :INDALTFRNACOCMC,
                        :TIPACOMCD, 
                        :CODCNLVNDINI,
                        :CODFILEMPORIVBA,
                        :DESCPHBTB,
                        :INDVCLPMC,
                        :INDACOCMCBTBVLR,
                        :CODACOCMCCPI,
                        :TIPDSNDSPCBK,
                        :CODEVTBTB)]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(acaoComercial.CODACOCMC))
        parameterMapper.Parameters.Add(":NOMACOCMC", ValueDBProperty(acaoComercial.NOMACOCMC))
        parameterMapper.Parameters.Add(":TIPDSNDSCBNF", ValueDBProperty(acaoComercial.TIPDSNDSCBNF))
        parameterMapper.Parameters.Add(":DATINIACOCMC", ValueDBProperty(acaoComercial.DATINIACOCMC))
        parameterMapper.Parameters.Add(":DESOBJACOCMC", ValueDBProperty(acaoComercial.DESOBJACOCMC))
        parameterMapper.Parameters.Add(":INDACOCMCCPL", ValueDBProperty(acaoComercial.INDACOCMCCPL))
        parameterMapper.Parameters.Add(":VLRTOTACOCMC", ValueDBProperty(acaoComercial.VLRTOTACOCMC))
        parameterMapper.Parameters.Add(":VLRTOTOCDACOCMC", ValueDBProperty(acaoComercial.VLRTOTOCDACOCMC))
        parameterMapper.Parameters.Add(":VLRTOTTXAADMACOCMC", ValueDBProperty(acaoComercial.VLRTOTTXAADMACOCMC))
        parameterMapper.Parameters.Add(":VLRTOTBDEACOCMC", ValueDBProperty(acaoComercial.VLRTOTBDEACOCMC))
        parameterMapper.Parameters.Add(":CODFNCCRIACOCMC", ValueDBProperty(acaoComercial.CODFNCCRIACOCMC))
        parameterMapper.Parameters.Add(":DATAPVACOCMC", ValueDBProperty(acaoComercial.DATAPVACOCMC))
        parameterMapper.Parameters.Add(":TIPFLUAPVACOCMC", ValueDBProperty(acaoComercial.TIPFLUAPVACOCMC))
        parameterMapper.Parameters.Add(":CODSTAAPVACOCMC", ValueDBProperty(acaoComercial.CODSTAAPVACOCMC))
        parameterMapper.Parameters.Add(":CODFNCAPVATUACOCMC", ValueDBProperty(acaoComercial.CODFNCAPVATUACOCMC))
        parameterMapper.Parameters.Add(":DATFIMACOCMC", ValueDBProperty(acaoComercial.DATFIMACOCMC))
        parameterMapper.Parameters.Add(":CODACOCMCORI", ValueDBProperty(acaoComercial.CODACOCMCORI))
        parameterMapper.Parameters.Add(":DATGRCACOCMC", ValueDBProperty(acaoComercial.DATGRCACOCMC))
        parameterMapper.Parameters.Add(":INDALTFRNACOCMC", ValueDBProperty(acaoComercial.INDALTFRNACOCMC))
        parameterMapper.Parameters.Add(":TIPACOMCD", ValueDBProperty(acaoComercial.TIPACOMCD))
        parameterMapper.Parameters.Add(":CODCNLVNDINI", ValueDBProperty(acaoComercial.CODCNLVNDINI))
        parameterMapper.Parameters.Add(":CODFILEMPORIVBA", ValueDBProperty(acaoComercial.CODFILEMPORIVBA))
        parameterMapper.Parameters.Add(":DESCPHBTB", ValueDBProperty(acaoComercial.DESCPHBTB))
        parameterMapper.Parameters.Add(":INDVCLPMC", ValueDBProperty(acaoComercial.INDVCLPMC))
        parameterMapper.Parameters.Add(":INDACOCMCBTBVLR", ValueDBProperty(acaoComercial.INDACOCMCBTBVLR))
        parameterMapper.Parameters.Add(":CODACOCMCCPI", ValueDBProperty(acaoComercial.CODACOCMCCPI))
        parameterMapper.Parameters.Add(":TIPDSNDSPCBK", ValueDBProperty(If(acaoComercial.TIPDSNDSPCBK Is Nothing, 0, acaoComercial.TIPDSNDSPCBK)))
        parameterMapper.Parameters.Add(":CODEVTBTB", ValueDBProperty(acaoComercial.CODEVTBTB))

        acaoComercial.CODACOCMC = connector.ExecuteScalar(sql, parameterMapper)

        Return acaoComercial

    End Function

    ''' <summary>
    ''' DelDdoAcoCmc
    ''' Deleta uma acao comercial do banco
    ''' </summary>
    ''' <param name="codAcao"></param>
    Public Sub DeletarAcaoComercial(codAcao As Integer)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = "DELETE FROM MRT.T0118430 WHERE CODACOCMC = :CODACOCMC"

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub

    ''' <summary>
    ''' FunCsnAcsSubMnu
    ''' </summary>
    ''' <param name="codFnc"></param>
    ''' <param name="codFil"></param>
    ''' <param name="codMnu"></param>
    ''' <returns></returns>
    Public Function ConsultaAcaoMenuFuncionario(codFnc As Long, codFil As Integer, codMnu As Integer) As List(Of RelacaoDeSistemasxMenu)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT MNU.CODMNU,
                            MNU.DESMNU, MNU.CODMNUPAI, MNU.NOMOBEPRG, MNU.CODFILEMP, 
                            CASE WHEN FNC.CODFNC IS NULL THEN 0 ELSE 1 END AS ACS
                             FROM MRT.T0150830 MNU                                      
                                  LEFT JOIN MRT.T0149506 FNC                            
                                  ON FNC.CODFNC       = :CODFNC                 
                                  AND FNC.NOMSISINF   = MNU.NOMSISINF                   
                                  AND FNC.CODMNU      = MNU.CODMNU                      
                                  AND MNU.CODFILEMP   = FNC.CODFILEMP                   
                             WHERE MNU.NOMSISINF      = 'AI'                            
                                  AND MNU.CODFILEMP   = :CODFILEMP
                                  And MNU.CODMNU     =  :CODMNU                 
                             ORDER BY 3, 1, 2 ]]>.Value

        parameterMapper.Parameters.Add(":CODFILEMP", codFil)
        parameterMapper.Parameters.Add(":CODFNC", codFnc)
        parameterMapper.Parameters.Add(":CODMNU", codMnu)

        Return connector.SelectTList(sql, parameterMapper, DAORowMapper(Of VO.RelacaoDeSistemasxMenu).RowMapperOnlyProperties(sql))
    End Function

    ''' <summary>
    ''' Busca os dados da ação que foi pedido o estorno
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function BuscarDadosAcaoEstorno(idAcao As Decimal) As VO.EstornoAcaoMercadologica

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                            A.CODACOCMC  
                                          , A.NOMACOCMC
                                          , A.TIPACOMCD
                                          , A.VLRTOTACOCMC
                                          , F.TIPDSNDSCBNF || ' - ' || Trim(f.DESDSNDSCBNF) AS EMPEXAACORDO          
                                          , TRUNC(SUM(CASE
                                                       WHEN G.VLRPTCCLIITEACOCMC > 0 AND G.VLRPTCCLIITEACOCMC <= G.VLRUNTITEOCDACOCMC THEN(G.VLRUNTITEOCDACOCMC * G.QDEITEOCDACOCMC)   
                                                            *(1 -VLRPTCCLIITEACOCMC / G.VLRUNTITEOCDACOCMC) - (G.VLRUTZOPEDSNACOCMC *(1 - (VLRPTCCLIITEACOCMC / G.VLRUNTITEOCDACOCMC))) 
                                                       WHEN G.VLRPTCCLIITEACOCMC > 0 AND G.VLRPTCCLIITEACOCMC > G.VLRUNTITEOCDACOCMC THEN 0
                                                       ELSE(G.VLRUNTITEOCDACOCMC * G.QDEITEOCDACOCMC) - G.VLRUTZOPEDSNACOCMC END), 2) AS VALOR_SALDO                           
                                     FROM MRT.T0118430 A     
                                      INNER JOIN MRT.T0155581 B ON B.CODACOCMC = A.CODACOCMC                  
                                       LEFT JOIN MRT.T0155531 D  ON D.CODEVTACOCMC = B.CODEVTACOCMC             
                                       LEFT JOIN MRT.T0109059 F  ON D.TIPDSNDSCBNF = F.TIPDSNDSCBNF    
                                      INNER JOIN MRT.T0138776 G ON G.CODACOCMC = A.CODACOCMC
                                     WHERE A.CODACOCMC = :CODACOCMC                                   
                                     GROUP BY A.CODACOCMC, A.NOMACOCMC, A.VLRTOTACOCMC, F.TIPDSNDSCBNF, F.DESDSNDSCBNF, A.TIPACOMCD]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", idAcao)
        Dim properties = {"CODACOCMC", "NOMACOCMC", "TIPACOMCD", "VLRTOTACOCMC", "EMPEXAACORDO", "VALOR_SALDO"}
        Return connector.SelectTList(sql, parameterMapper, DAORowMapper(Of VO.EstornoAcaoMercadologica).RowMapperOnlyProperties(properties)).FirstOrDefault()

    End Function

#Region "ConsultaSaldoAcaoFornecedor"

    ''' <summary>
    ''' BscDdoAco
    ''' Consulta Saldo Ação. Listar saldo acao acoes Vs evento em aberto
    ''' </summary>
    ''' <param name="vo"></param>
    ''' <returns></returns>
    Public Function ListarSaldoAcaoEventoEmAberto(vo As VO.AcaoComercial) As List(Of VO.AcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        'vo.NOMACOCMC = If(vo.NOMACOCMC, "%" + vo.NOMACOCMC + "%")
        If Not vo.NOMACOCMC Is Nothing Then
            vo.NOMACOCMC = "%" + UCase(vo.NOMACOCMC) + "%"
        End If

        Dim sql As String = <![CDATA[ SELECT A.CODACOCMC as CODACOCMC,
                                    B.CODOPEFSCDSNACOCMC as CODOPE,
                                    C.DESOPEFSCDSNACOCMC as NOMOPE, 
                                    A.NOMACOCMC,
                                    B.CODITEOCDACOCMC as CODITE,
                                    SUM(B.VLRUNTITEOCDACOCMC * B.QDEITEOCDACOCMC) as VLRORC, 
                                    SUM(B.VLRUTZOPEDSNACOCMC) as VLRUTZ
                                    ,TRUNC(SUM((B.VLRUNTITEOCDACOCMC * B.QDEITEOCDACOCMC) - B.VLRUTZOPEDSNACOCMC), 2) as VLRSLD
                                    FROM MRT.T0118430 A
                                    INNER JOIN MRT.T0155581 D ON A.CODACOCMC = D.CODACOCMC  
                                    AND (:CODEVTACOCMC IS NULL OR D.CODEVTACOCMC = :CODEVTACOCMC) 
                                   
                                    INNER JOIN MRT.T0138776 B ON A.CODACOCMC = B.CODACOCMC                                                                   
                                    AND (:CODOPE IS NULL OR B.CODOPEFSCDSNACOCMC = :CODOPE)

                                    INNER JOIN MRT.T0155573 C ON B.CODOPEFSCDSNACOCMC = C.CODOPEFSCDSNACOCMC                                     

                                    WHERE A.CODSTAAPVACOCMC = 5 
                                    AND (:DATINIACOCMC IS NULL OR :DATFIMACOCMC IS NULL OR A.DATINIACOCMC BETWEEN :DATINIACOCMC AND :DATFIMACOCMC)                               
                                       
                                    AND (:CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC) 
                                    AND (:NOMACOCMC IS NULL OR A.NOMACOCMC LIKE :NOMACOCMC) 
                                                                       
                                    GROUP BY A.CODACOCMC, B.CODOPEFSCDSNACOCMC, C.DESOPEFSCDSNACOCMC,  B.CODITEOCDACOCMC, A.NOMACOCMC
                                    HAVING SUM((B.VLRUNTITEOCDACOCMC * B.QDEITEOCDACOCMC) - B.VLRUTZOPEDSNACOCMC) > 0 
                                    ORDER BY A.CODACOCMC, B.CODOPEFSCDSNACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NOMACOCMC", ValueDBProperty(vo.NOMACOCMC))
        parameterMapper.Parameters.Add(":DATINIACOCMC", ValueDBProperty(vo.DATINIACOCMC))
        parameterMapper.Parameters.Add(":DATFIMACOCMC", ValueDBProperty(vo.DATFIMACOCMC))
        parameterMapper.Parameters.Add(":CODEVTACOCMC", ValueDBProperty(vo.CODEVTACOCMC))
        parameterMapper.Parameters.Add(":CODOPE", ValueDBProperty(vo.CODOPEFSCDSNACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

#End Region

#Region "ConsultaSaldoAcaoFornecedor"

    ''' <summary>
    ''' BscDdoAco
    ''' Consulta Saldo Ação. Listar saldo acao acoes Vs evento em aberto
    ''' </summary>
    ''' <param name="vo"></param>
    ''' <returns></returns>
    Public Function ListarSaldoAcaoFornecedor(vo As VO.AcaoComercial) As List(Of VO.AcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not vo.NOMACOCMC Is Nothing Then
            vo.NOMACOCMC = "%" + UCase(vo.NOMACOCMC) + "%"
        End If

        Dim sql As String = <![CDATA[ SELECT  CODCPR, 
                                      NOMCPR, 
                                      CODFRN, 
                                      NOMFRN, 
                                      VLRTOTOCDACOCMC, 
                                      VLRPTCFRNACOCMC, 
                                      CODACOCMC, 
                                      NOMACOCMC, 
                                      DATAPVACOCMC, 
                                      CODOPEFSCDSNACOCMC, 
                                      DESOPEFSCDSNACOCMC  , 
                                      CODMER, 
                                      CODITEOCDACOCMC,
                                     ptcfrn,
                                       saldo,
                                       SALDO_FORNECEDOR 
                                    FROM
                                    (

                                        with TAB as ( 
                                          SELECT G.CODCPR, 
                                      G.NOMCPR, 
                                      E.CODFRN, 
                                      F.NOMFRN, 
                                      c.VLRTOTOCDACOCMC, 
                                      E.VLRPTCFRNACOCMC, 
                                      A.CODACOCMC, 
                                      C.NOMACOCMC,
                                      C.DATAPVACOCMC, 

                                      A.CODOPEFSCDSNACOCMC, 
                                      D.DESOPEFSCDSNACOCMC, 
                                      NVL(A.CODMER, 0 ) AS CODMER, 
                                      A.CODITEOCDACOCMC,
                                      TRUNC( CASE  WHEN a.vlrptccliiteacocmc > 0 and a.vlrptccliiteacocmc <= a.vlruntiteocdacocmc 
                                                   THEN(a.vlruntiteocdacocmc * a.qdeiteocdacocmc) *(1 -vlrptccliiteacocmc / a.vlruntiteocdacocmc) - (a.vlrutzopedsnacocmc 
                                                        *(1 - (vlrptccliiteacocmc / a.vlruntiteocdacocmc))) 
                                                   WHEN a.vlrptccliiteacocmc > 0 and a.vlrptccliiteacocmc > a.vlruntiteocdacocmc THEN 0
                                                  ELSE(a.vlruntiteocdacocmc * a.qdeiteocdacocmc) -a.vlrutzopedsnacocmc 
                                             END,   2) AS SALDO 
                                , ''  as SALDO_FORNECEDOR
                               FROM mrt.t0138776 a 
                                    INNER JOIN mrt.t0118430 c ON a.codacocmc = c.codacocmc 
                                                          AND c.codstaapvacocmc= 5
                                    INNER JOIN mrt.t0138768 e on a.codacocmc = E.codacocmc	

                                    INNER JOIN MRT.T0155573 D ON A.codopefscdsnacocmc = D.codopefscdsnacocmc 
                                                              AND D.INDBDEOPEDSNACOCMC =  0 
                                    INNER JOIN MRT.T0100426 F ON F.CODFRN = E.CODFRN
                                    INNER JOIN MRT.T0113625 G ON G.CODCPR = F.CODCPR
                               WHERE a.codopefscdsnacocmc <> 5
                               and c.VLRTOTOCDACOCMC > 0
                               and e.vlrptcfrnacocmc > 0
                               and a.vlrptcfrniteacocmc > 0
                           
                             AND (:CODACOCMC IS NULL OR C.CODACOCMC = :CODACOCMC) 
                             AND (:NOMACOCMC IS NULL OR  UPPER(C.NOMACOCMC) LIKE :NOMACOCMC) 
                             AND (:CODFRN IS NULL OR E.CODFRN = :CODFRN)
                             AND (:CODCPR IS NULL OR G.CODCPR = :CODCPR)

                               )
                               select CODCPR, 
                                      NOMCPR, 
                                      CODFRN, 
                                      NOMFRN, 
                                      VLRTOTOCDACOCMC, 
                                      VLRPTCFRNACOCMC, 
                                      CODACOCMC, 
                                      NOMACOCMC, 
                                      DATAPVACOCMC, 
                                      CODOPEFSCDSNACOCMC, 
                                      DESOPEFSCDSNACOCMC  , 
                                      CODMER, 
                                      CODITEOCDACOCMC,
                                      trunc(to_char( ( ( vlrptcfrnacocmc * 100 ) / VLRTOTOCDACOCMC ), '99999999999D00'),2) as ptcfrn,
                                      trunc(saldo,2) as saldo,
                                      trunc(to_char( ( ( ( vlrptcfrnacocmc * 100 ) / VLRTOTOCDACOCMC ) * saldo / 100 ), '99999999999D00'),2)  as SALDO_FORNECEDOR     
                               from TAB      
                               where to_char( ( ( ( vlrptcfrnacocmc * 100 ) / VLRTOTOCDACOCMC ) * saldo / 100 ), '99999999999D00')  > 0
                                
                            ) 
                
                            WHERE ( :CODCPR IS NULL OR CODFRN IN    (
                                                                        SELECT B.codfrn 
                                                                        FROM MRT.T0113625 A
                                                                        INNER JOIN MRT.T0100426 B ON A.codcpr = B.codcpr
                                                                        WHERE A.codcpr = :CODCPR        
                                                                        AND B.datdstdivfrn is null
                                                                    )
                                )
                            AND (  :CODDIVCMP IS NULL OR CODFRN IN  (
                                                                        SELECT C.codfrn
                                                                        FROM MRT.T0118570 A
                                                                        INNER JOIN MRT.T0113625 B ON A.codgerprd = B.codgerprd   
                                                                        INNER JOIN MRT.T0100426 C ON B.codcpr = C.codcpr    
                                                                        WHERE A.coddivcmp = :CODDIVCMP      
                                                                        AND C.datdstdivfrn is null
                                                                    )
                                )
                                  ]]>.Value


        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NOMACOCMC", ValueDBProperty(vo.NOMACOCMC))
        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(vo.CODFRN))
        parameterMapper.Parameters.Add(":CODCPR", ValueDBProperty(vo.CODCPR))
        parameterMapper.Parameters.Add(":CODDIVCMP", ValueDBProperty(vo.CODDIVCMP))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' BscDdoAco
    ''' Consulta Celula Ação. Listar celulas para carregar o combo da Modal de Saldo Ação Fornecedor
    ''' </summary>
    ''' <param name="codCel"></param>
    ''' <returns></returns>
    Public Function listaCelula(codCel As Decimal?) As List(Of VO.AcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        'If Not VO.NOMACOCMC Is Nothing Then
        '    VO.NOMACOCMC = "%" + UCase(VO.NOMACOCMC) + "%"
        'End If

        Dim sql As String = <![CDATA[  SELECT * FROM MRT.T0118570
                                        WHERE (:CODDIVCMP IS NULL OR CODDIVCMP = :CODDIVCMP)                                      
                                        ORDER BY DESDIVCMP ASC     
                                  ]]>.Value

        parameterMapper.Parameters.Add(":CODDIVCMP", ValueDBProperty(codCel))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' BscDdoAco
    ''' Consulta Comprador Ação. Listar compradores para carregar o combo da Modal de Saldo Ação Fornecedor
    ''' </summary>
    ''' <param name="vo"></param>
    ''' <returns></returns>
    Public Function listaComprador(vo As VO.AcaoComercial) As List(Of VO.AcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not vo.NOMACOCMC Is Nothing Then
            vo.NOMACOCMC = "%" + UCase(vo.NOMACOCMC) + "%"
        End If

        Dim sql As String = <![CDATA[   SELECT * FROM MRT.T0113625 
                                         WHERE (:CODCPR IS NULL OR CODCPR = :CODDIVCMP) 
                                        AND (:NOMCPR IS NULL OR NOMCPR LIKE :NOMCPR)  
                                        ORDER BY NOMCPR ASC       
                                  ]]>.Value

        parameterMapper.Parameters.Add(":CODCPR", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NOMCPR", ValueDBProperty(vo.NOMACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' BscDdoAco
    ''' Consulta Fornecedores Ação. Listar fornecedores para carregar o combo da Modal de Saldo Ação Fornecedor
    ''' </summary>
    ''' <param name="codFrn"></param>
    ''' <returns></returns>
    Public Function listaRelacaoFornecedoresAcao(codFrn As Decimal?) As List(Of VO.AcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[   SELECT D.CODFRN, D.NOMFRN  
                                        FROM MRT.T0123183 A,   
                                        MRT.T0118570 B,
                                        MRT.T0113625 C,          
                                        MRT.T0100426 D  
                                        WHERE A.CODDRTCMP = B.CODDRTCMP 
                                        AND B.CODGERPRD = C.CODGERPRD 
                                        AND C.CODCPR = D.CODCPR       
                                        AND NVL(D.TIPIDTEMPASCACOCMC, 0) = 0                                        
                                        AND D.DATDSTDIVFRN IS NULL   
                                        AND (:CODFRN IS NULL OR D.CODFRN = :CODFRN) 
                                      --AND (:NOMFRN IS NULL OR D.NOMFRN LIKE :NOMFRN)  
                                        ORDER BY D.NOMFRN ASC]]>.Value

        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(codFrn))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function AtuStaAcoCmc(ByVal CodAcoCmc As Decimal, ByVal intCodStaApv As Decimal) As Object
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0118430 
                                        SET CODSTAAPVACOCMC = :CODSTAAPVACOCMC
                                          , DATAPVACOCMC = CASE WHEN :CODSTAAPVACOCMC = 5 THEN SYSDATE ELSE DATAPVACOCMC END
                                      WHERE CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODSTAAPVACOCMC", intCodStaApv)
        parameterMapper.Parameters.Add(":CODACOCMC", CodAcoCmc)

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function

#End Region

#Region "ConsultaGastoAcaoFornecedor"

    ''' <summary>
    ''' BscDdoAco
    ''' Consulta Saldo Ação. Listar saldo acao acoes Vs evento em aberto
    ''' </summary>
    ''' <param name="vo"></param>
    ''' <returns></returns>
    Public Function ListarGastoAcaoEvento(vo As VO.AcaoComercial) As List(Of VO.AcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[   SELECT A.CODACOCMC as CODACOCMC, 
                                            B.CODOPEFSCDSNACOCMC as CODOPE, 
                                            C.DESOPEFSCDSNACOCMC as NOMOPE, 
                                            B.CODITEOCDACOCMC as CODITE, 
                                            SUM(B.VLRUNTITEOCDACOCMC * B.QDEITEOCDACOCMC) as VLRORC, 
                                            SUM(B.VLRUTZOPEDSNACOCMC) as VLRUTZ, 
                                            TRUNC(SUM((B.VLRUNTITEOCDACOCMC * B.QDEITEOCDACOCMC) - B.VLRUTZOPEDSNACOCMC), 2) as VLRSLD 
                                            FROM MRT.T0118430 A 
                                            INNER JOIN MRT.T0155581 D ON A.CODACOCMC = D.CODACOCMC 
                                            INNER JOIN MRT.T0138776 B ON A.CODACOCMC = B.CODACOCMC 
                                            INNER JOIN MRT.T0155573 C ON B.CODOPEFSCDSNACOCMC = C.CODOPEFSCDSNACOCMC 
                                            WHERE (:CODEVTACOCMC IS NULL OR D.CODEVTACOCMC = :CODEVTACOCMC)
                                            GROUP BY A.CODACOCMC, B.CODOPEFSCDSNACOCMC, C.DESOPEFSCDSNACOCMC,  B.CODITEOCDACOCMC
                                            ORDER BY A.CODACOCMC, B.CODOPEFSCDSNACOCMC 
                                  ]]>.Value

        parameterMapper.Parameters.Add(":CODEVTACOCMC", ValueDBProperty(vo.CODEVTACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

#End Region

#Region "Fluxo"
    Public Function FunAtuApvAcoCmc(ByVal dCodAcoCmc As Double, _
                                    ByVal dCodFncApvAtuAcoCmc As Double, _
                                    ByVal iStaAcoCmc As Short, _
                                    ByVal CodFormPag As Decimal, _
                                    ByVal NumDias As Decimal) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[UPDATE MRT.T0118430
                                     SET CODFNCAPVATUACOCMC = :CODFNCAPVATUACOCMC
                                       , CODSTAAPVACOCMC    = :CODSTAAPVACOCMC
                                       , DATAPVACOCMC       = trunc(SYSDATE) 
                                       , TIPFRMDSCBNF       = :TIPFRMDSCBNF
                                       , QDEDIARCBPMS       = :QDEDIARCBPMS
                                     WHERE CODACOCMC  = :CODACOCMC ]]>.Value

        parameterMapper.Parameters.Add(":CODFNCAPVATUACOCMC", dCodFncApvAtuAcoCmc)
        parameterMapper.Parameters.Add(":CODSTAAPVACOCMC", iStaAcoCmc)
        parameterMapper.Parameters.Add(":CODACOCMC", dCodAcoCmc)
        parameterMapper.Parameters.Add(":TIPFRMDSCBNF", CodFormPag)
        parameterMapper.Parameters.Add(":QDEDIARCBPMS", NumDias)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function PcdAtuAldRsvFrn(ByVal vCodIdtTipAco As Integer, _
                                    ByVal vCodFrn As Integer, _
                                    ByVal vCodAcoCmc As String, _
                                    ByVal vTipDsnDscBnf As Integer, _
                                    ByVal vVlrSldRsvFrn As Double, _
                                    ByVal vFlgApvAcoCmc As String, _
                                    ByVal CodUni As String, _
                                    ByVal vVlrTxaAdm As Double, _
                                    ByVal vVlrTxaBde As Double) As Integer

        Dim parameterAndProperty As New ParameterAndProperty
        Dim parametersReturn As New Dictionary(Of String, Object)
        Dim sp As String = <![CDATA[MRT.PCTDJATLSLDRSVFRNONL.PRCDJAtlSldRsvFrnOnl]]>.Value

        With parameterAndProperty

            'Parametros de Entrada
            .Add("VCODIDTTIPACO", vCodIdtTipAco, Data.ParameterDirection.Input, Data.DbType.VarNumeric)
            .Add("VCODFRN", vCodFrn, Data.ParameterDirection.Input, Data.DbType.VarNumeric)
            .Add("VCODACOCMC", vCodAcoCmc, Data.ParameterDirection.Input, Data.DbType.AnsiStringFixedLength, 11)
            .Add("VTIPDSNDSCBNF", vTipDsnDscBnf, Data.ParameterDirection.Input, Data.DbType.VarNumeric)
            .Add("VLRSLDRSVFRN", vVlrSldRsvFrn, Data.ParameterDirection.Input, Data.DbType.VarNumeric)
            .Add("VFLGAPVACOCMC", vFlgApvAcoCmc, Data.ParameterDirection.Input, Data.DbType.AnsiStringFixedLength, 1)
            .Add("CODUNI", CodUni, Data.ParameterDirection.Input, Data.DbType.AnsiString)
            .Add("VVLRTXAADM", vVlrTxaAdm, Data.ParameterDirection.Input, Data.DbType.VarNumeric)
            .Add("VVLRTXABDE", vVlrTxaBde, Data.ParameterDirection.Input, Data.DbType.VarNumeric)

            'Parametros de Saída
            .Add("VCODERR", 0, Data.ParameterDirection.Output, Data.DbType.VarNumeric)
            .Add("VMSGERR", String.Empty, Data.ParameterDirection.Output, Data.DbType.AnsiStringFixedLength, 200)
            .Add("VRETORNO", 0, Data.ParameterDirection.Output, Data.DbType.VarNumeric) 'Seta valor false

        End With

        'Parametros que sera retornado da procedure
        parametersReturn.Add("VCODERR", String.Empty)
        parametersReturn.Add("VMSGERR", String.Empty)
        parametersReturn.Add("VRETORNO", String.Empty)


        'Executa e retorna Procedure
        connector.ExecuteStoredPrecedure(sp, parameterAndProperty.GetAll(), parametersReturn)

        Return parametersReturn("VRETORNO")
        'Dim retorno As Integer = 0
        'Return retorno

    End Function


    Public Function UpdateDtAprovacao(acao As Decimal, dtapv As DateTime) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0136560
                                    SET DATAPVACOCMC = :DATAPVACOCMC
                                    WHERE CODACOCMC = :CODACOCMC
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", acao)
        parameterMapper.Parameters.Add(":DATAPVACOCMC", dtapv)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function


    Public Function UpdateDtIniVgr(acao As Decimal, dtapv As DateTime, DatIniVgrDsc As DateTime, DatFimVgrDsc As DateTime) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0136331
                                        SET DATINIVGRDSC = :DATINIVGRDSC
                                          , DATFIMVGRDSC = :DATFIMVGRDSC
                                      WHERE TIPACOCMC = 3
                                        AND DATINIVGRDSC < :DATAPVACOCMC
                                        AND NUMREQACOCMC IN                                                         
                                            (SELECT B.NUMREQACOCMC                                                  
                                               FROM MRT.T0136560 A                                                  
                                               LEFT OUTER JOIN MRT.RLCACOACREDEUTZACR X ON A.CODACOCMC = X.CODACOACR
                                                                                  AND X.CODFRN IN (SELECT CODFRN
                                                                                                     FROM MRT.T0138768
                                                                                                    WHERE CODACOCMC = :CODACOCMC )
                                             , MRT.T0136331 B                                                        
                                              WHERE B.NUMREQACOCMC = A.NUMREQACOCMC                                 
                                                AND B.TIPACOCMC = 3                                                 
                                                AND A.DATAPVACOCMC IS NULL                                          
                                                AND A.DATRPVACOCMC IS NULL                                          
                                                AND A.DATLIBACOCMC IS NOT NULL                                      
                                                AND X.CODACOACR IS NULL                                             
                                                AND A.CODFRN IN (SELECT CODFRN 
                                                                   FROM MRT.T0138768 
                                                                  WHERE CODACOCMC =  :CODACOCMC )
                                            ) 
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", acao)
        parameterMapper.Parameters.Add(":DATAPVACOCMC", dtapv)
        parameterMapper.Parameters.Add(":DATINIVGRDSC", DatIniVgrDsc)
        parameterMapper.Parameters.Add(":DATFIMVGRDSC", DatFimVgrDsc)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function


    Public Function AtuFormaPagNumDiasFuncGAC(ByVal CodAcao As Decimal, _
                                              ByVal CodFormPag As Decimal, _
                                              ByVal NumDias As Decimal) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[UPDATE MRT.T0118430
                                     SET TIPFRMDSCBNF       = :TIPFRMDSCBNF
                                       , QDEDIARCBPMS       = :QDEDIARCBPMS
                                     WHERE CODACOCMC  = :CODACOCMC ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CodAcao)
        parameterMapper.Parameters.Add(":TIPFRMDSCBNF", CodFormPag)
        parameterMapper.Parameters.Add(":QDEDIARCBPMS", NumDias)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function
#End Region

    Public Function SelectAcaoComercialParaPromocao(acaoComercial As VO.AcaoComercial) As VO.AcaoComercial

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[ SELECT DATFIMACOCMC
                                            , DATINIACOCMC
                                            , CODACOCMC
                                            , NOMACOCMC 
                                            , TIPACOMCD
                                    FROM MRT.T0118430
                                    WHERE CODACOCMC = :CODACOCMC
                                     ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(acaoComercial.CODACOCMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"DATFIMACOCMC", "DATINIACOCMC", "CODACOCMC", "NOMACOCMC", "TIPACOMCD"})).FirstOrDefault()
    End Function
End Class

<Table("MRT.T0118430")>
Public Class AcaoComercialDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Decimal?
    <DateInsert>
    <[Readonly](False, True)>
    Property DATINIACOCMC As Date

    <DateInsert>
    <[Readonly](True, False)>
    Property DATAPVACOCMC As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATFIMACOCMC As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATGRCACOCMC As Date

    <IgnoreMap>
    Property INDVLDRGRICT As Decimal

    <IgnoreMap>
    Property Bonificacoes As List(Of RelacaoAcaoComercialXPedidoBonificado)

    <IgnoreMap>
    Property Premiacao As List(Of RelacaoAcaoComercialxPremiacaoExclusiva)

    <IgnoreMap>
    Property TotalReprovacao As Integer
#Region "Campos Auxiliares"
    <IgnoreMap>
    Property DESCSTA As String
    <IgnoreMap>
    Property NOMFNC As String
    <IgnoreMap>
    Property NOMFNCAPVATU As String
    <IgnoreMap>
    Property NOMUSRRCF As String
    <IgnoreMap>
    Property CODEVTACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMEVTACOCMC As String
    <IgnoreMap>
    Property TIPEVTACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMTIPEVTACOCMC As String

    <IgnoreMap>
    Property PlanosDeMarketing As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
    <IgnoreMap>
    Property Fornecedores As List(Of VO.RelacaoAcaoComercialxFornecedores)
    <IgnoreMap>
    Property ItensOrcamento As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)
    <IgnoreMap>
    Property ItensSubstituidos As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)
    <IgnoreMap>
    Property Carimbos As List(Of VO.RelacaoAcaoxFornecedorxCarimbo)
    <IgnoreMap>
    Property FluxosAprovacao As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)
    <IgnoreMap>
    Property Incentivos As List(Of VO.RelacaoIncentivoxAcaoComercial)
    <IgnoreMap>
    Property Mecanica As VO.RelacaoAcaoComercialxMecanicaDaAcao
    <IgnoreMap>
    Property PoliticasDivergentes As List(Of PoliticasDaAcaoComercial)

    <IgnoreMap>
    Property ValorTotal As Decimal
    <IgnoreMap>
    Property SaldoOrcamento As Decimal
    <IgnoreMap>
    Property ValorTotalBrinde As Decimal
    <IgnoreMap>
    Property TaxaBrinde As Decimal
    <IgnoreMap>
    Property ValorCliente As Decimal
    <IgnoreMap>
    Property ValorParticipacao As Decimal
    <IgnoreMap>
    Property SaldoReceitaCMV As Decimal
    <IgnoreMap>
    Property TaxaAdmin As Decimal
    <IgnoreMap>
    Property EmpenhoExa As String
    <IgnoreMap>
    Property confirmaEnviar1 As Nullable(Of Boolean)
    <IgnoreMap>
    Property confirmaEnviar2 As Nullable(Of Boolean)
    <IgnoreMap>
    Property confirmaEnviarGerente As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESDSNDSCBNF As String
    <IgnoreMap>
    Property CODOPEFSCDSNACOCMC As Decimal?
    <IgnoreMap>
    Property DESOPEFSCDSNACOCMC As String
    <IgnoreMap>
    Property CODITE As String
    <IgnoreMap>
    Property VLRORC As String
    <IgnoreMap>
    Property VLRUTZ As String
    <IgnoreMap>
    Property VLRSLD As String
    <IgnoreMap>
    Property CODFRN As String
    <IgnoreMap>
    Property NOMFRN As String
    <IgnoreMap>
    Property saldo As String
    <IgnoreMap>
    Property ptcfrn As String
    <IgnoreMap>
    Property SALDO_FORNECEDOR As String
    <IgnoreMap>
    Property CODITEOCDACOCMC As String
    <IgnoreMap>
    Property CODMER As Decimal
    <IgnoreMap>
    Property CODDIVCMP As Decimal?
    <IgnoreMap>
    Property DESDIVCMP As String
    <IgnoreMap>
    Property CODCPR As Decimal?
    <IgnoreMap>
    Property NOMCPR As String
    <IgnoreMap>
    Property CODOPE As Decimal?
    <IgnoreMap>
    Property NOMOPE As String
    <IgnoreMap>
    Property VLRFUNDING As Decimal?

    <IgnoreMap>
    Property IDTTERPGTITERSU As Decimal
    <IgnoreMap>
    Property IDTLCLEMPFRN As Nullable(Of Decimal)
    <IgnoreMap>
    Property NUMLNHITEOCDACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property IDTEMPFRN As Nullable(Of Decimal)


    <IgnoreMap>
    Property INDITERSUARZPRVPGT As Nullable(Of Decimal)

    'Campo código da filial
    <IgnoreMap>
    Property CODFILEMP As Nullable(Of Decimal)
    <IgnoreMap>
    Property QTD As Nullable(Of Decimal)
    <IgnoreMap>
    Property CodUndEsrNgc As Nullable(Of Decimal)

    <IgnoreMap>
    Property DESFRMDSCBNF As String
    <IgnoreMap>
    Property VALOR_SALDO As Nullable(Of Decimal)

    <IgnoreMap>
    Property relacaoAcaoMecanicaList As List(Of VO.RelacaoAcaoMecanica)


    <IgnoreMap>
    Property TotalPoliticas As Integer

    <IgnoreMap>
    Property INDEVTSMA As Nullable(Of Decimal)
#End Region
End Class
