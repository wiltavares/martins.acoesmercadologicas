﻿Imports Core
Imports VO

Public Class RelacaoAcaoComercialPorItemOrcamentoDAO
    Inherits DAOBase(Of VO.RelacaoAcaoComercialPorItemOrcamento)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoComercialPorItemOrcamentoDAOConfiguration()
    End Sub

    ''' <summary>
    ''' BscDdoIteOcd
    ''' Busca a lista de relação de itens de orcamento da acao comercial 
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Public Function BuscarItensOrcamentoDaAcao(id As Decimal) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC,                                    
                             A.NUMLNHITEOCDACOCMC,                                  
                             A.CODITEOCDACOCMC,                                     
                             A.QDEITEOCDACOCMC,                                     
                             A.VLRUNTITEOCDACOCMC,                                  
                             to_char(A.VLRUNTITEOCDACOCMC) as STRVLRUNTITEOCDACOCMC,
                             A.DATUTZITEACOCMC,                                     
                             A.DESFRMUTZITEACOCMC,                                  
                             A.CODOPEFSCDSNACOCMC,                                  
                             B.DESOPEFSCDSNACOCMC,                                  
                             B.INDTIPOPEDSNACOCMC,                                  
                             B.INDBDEOPEDSNACOCMC,                                  
                             B.INDUNDESRNGCACOCMC,                                  
                             A.VLRUTZOPEDSNACOCMC,                                  
                             to_char(A.VLRUTZOPEDSNACOCMC) as STRVLRUTZOPEDSNACOCMC,
                             A.QDEUTZOPEDSNACOCMC,                                  
                             A.CODMER,                                              
                             A.CODUNDESRNGCOPEDSN,                                  
                             R.DESUNDESRNGC,                                        
                             A.CODFILEMP,                                           
                             S.NOMFILEMP,                                           
                             O.CODFRNPCPMER,                                        
                             A.QDEDIACMPITEACOCMC,                                  
                             A.CODCENCST,                                           
                             A.NUMCGCEMPFRN,                                        
                             A.IDTEMPFRNITERSU,                                     
                             A.IDTLCLEMPFRNITERSU,                                  
                             A.IDTTERPGTITERSU,                                     
                             A.DESOBSCPR,                                           
                             A.NUMCTTFRN,                                           
                             A.IDTCNTCTB,                                           
                             A.INDITEREQRSUVLRMNM,                                  
                             D.INDGRCITEENCFOLPGT,                                  
                             A.NUMCTTFRN,                                           
                             A.IDTITERSU,                                           
                             A.NUMLNHCTTRSU,                                        
                             A.DESCTTRSU,                                           
                             A.VLRPTCCLIITEACOCMC,                                  
                             A.VLRPTCFRNITEACOCMC,                                  
                             A.CODIDTTSC,                                           
                             B.INDGRCREQAUTITERSU,                                  
                             NVL(E.CodStaIteRsu, 0) as CODSTAITERSU,              
                             NVL(E.DesStaIteRsu, ' ') as DESSTAITERSU,            
                             NVL(A.INDITERSUARZPRVPGT, 0) as INDITERSUARZPRVPGT,  
                             NVL(A.INDITERSUFRNUNC, 0) as INDITERSUFRNUNC   
                             FROM MRT.T0138776 A                                     
                                 LEFT JOIN MRT.T0155573 B                           
                                     ON B.CODOPEFSCDSNACOCMC = A.CODOPEFSCDSNACOCMC 
                                 LEFT JOIN MRT.T0138008 C                           
                                     ON A.CODACOCMC = C.CODACOCMC                   
                                     AND A.CODOPEFSCDSNACOCMC = C.CODOPEFSCDSNACOCMC
                                 LEFT JOIN MRT.T0100086 O                           
                                     ON O.CODMER = A.CODMER                         
                                 LEFT JOIN MRT.T0131275 R                           
                                     ON A.CODUNDESRNGCOPEDSN = R.CODUNDESRNGC       
                                 LEFT JOIN MRT.T0112963 S                           
                                     ON S.CODFILEMP = A.CODFILEMP                   
                                     AND S.CODEMP = 1                               
                                 LEFT JOIN MRT.T0155573 D                           
                                     ON A.CODOPEFSCDSNACOCMC = D.CODOPEFSCDSNACOCMC 
                                 LEFT JOIN MRT.CADSTAITERSUACOCMC E                 
                                     ON A.CodStaIteRsu = E.CodStaIteRsu   
                             WHERE                                                   
                               A.codacocmc = :CODACOCMC                         
                             ORDER BY                                                
                             A.NUMLNHITEOCDACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", id)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


    Public Function SelectAbastecerPromocao(CODACOCMC As Decimal, CODPRMPMC As Decimal) As RelacaoAcaoComercialPorItemOrcamento


        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODACOCMC
                                          , NUMLNHITEOCDACOCMC 
                                          , VLRUTZOPEDSNACOCMC
                                       FROM MRT.T0138776 
                                      WHERE CODACOCMC = :CODACOCMC
                                        AND CODMER = :CODPRMPMC
                                        AND CODOPEFSCDSNACOCMC in(4, 2) 
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)
        parameterMapper.Parameters.Add(":CODPRMPMC", CODPRMPMC) ' Mapea para o CODMER

        Return connector.ExecuteReader(Of VO.RelacaoAcaoComercialPorItemOrcamento)(sql, parameterMapper).FirstOrDefault()

    End Function

    Public Sub UpdateAbastecerPromocao(CODACOCMC As Decimal, NUMLNHITEOCDACOCMC As Decimal, INDTIPISRMERPMC As Decimal, VLRTOTMERISRPMC As Decimal)


        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0138776 
                                        SET VLRUTZOPEDSNACOCMC = (CASE WHEN :INDTIPISRMERPMC = 0 THEN 
                                                                          VLRUTZOPEDSNACOCMC + :VLRTOTMERISRPMC
                                                                       WHEN :INDTIPISRMERPMC = 1 THEN 
                                                                          VLRUTZOPEDSNACOCMC - :VLRTOTMERISRPMC                                           
                                                                      END )
                                      WHERE CODACOCMC = :CODACOCMC
                                        AND NUMLNHITEOCDACOCMC = :NUMLNHITEOCDACOCMC
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)
        parameterMapper.Parameters.Add(":NUMLNHITEOCDACOCMC", NUMLNHITEOCDACOCMC)
        parameterMapper.Parameters.Add(":INDTIPISRMERPMC", INDTIPISRMERPMC)
        parameterMapper.Parameters.Add(":VLRTOTMERISRPMC", VLRTOTMERISRPMC)

        connector.ExecuteScalar(sql, parameterMapper)

    End Sub


    ''' <summary>
    ''' BscDdoOpeEtr
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function BuscarItensOrcamentoSubstituidosDaAcaoETR(idAcao As Decimal?) As List(Of RelacaoAcaoComercialPorItemOrcamento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC,
                             A.CODOPEFSCDSNACOCMC,
                             B.DESOPEFSCDSNACOCMC,
                           TRUNC(SUM(CASE
                                     WHEN A.VLRPTCCLIITEACOCMC > 0 AND A.VLRPTCCLIITEACOCMC <= A.VLRUNTITEOCDACOCMC THEN(A.VLRUNTITEOCDACOCMC * A.QDEITEOCDACOCMC)   
                                         *(1 -VLRPTCCLIITEACOCMC / A.VLRUNTITEOCDACOCMC) - (A.VLRUTZOPEDSNACOCMC *(1 - (VLRPTCCLIITEACOCMC / A.VLRUNTITEOCDACOCMC))) 
                                     WHEN A.VLRPTCCLIITEACOCMC > 0 AND A.VLRPTCCLIITEACOCMC > A.VLRUNTITEOCDACOCMC THEN 0
                                     ELSE(A.VLRUNTITEOCDACOCMC * A.QDEITEOCDACOCMC) - A.VLRUTZOPEDSNACOCMC END),   2) AS SALDO
                            FROM   MRT.T0138776 A
                                   INNER JOIN MRT.T0155573 B ON A.CODOPEFSCDSNACOCMC = B.CODOPEFSCDSNACOCMC
                                   INNER JOIN MRT.T0118430 C ON A.CODACOCMC = C.CODACOCMC
                           AND C.CODSTAAPVACOCMC= 5
                            WHERE  A.CODACOCMC = :CODACOCMC
                            AND   (A.CODOPEFSCDSNACOCMC <> 5 OR(A.CODOPEFSCDSNACOCMC = 5
                                   AND A.CODSTAITERSU = 1
                                   AND C.DATAPVACOCMC > TO_DATE('2007-09-30',   'YYYY-MM-DD')))
                            AND EXISTS (SELECT C.CODACOCMC
                                        FROM MRT.T0138768 C
                                        WHERE C.CODACOCMC = A.CODACOCMC
                                        AND C.CODFRN <> 999999)
                            GROUP BY A.CODACOCMC,
                               A.CODOPEFSCDSNACOCMC,
                               B.DESOPEFSCDSNACOCMC
                            ORDER BY A.CODOPEFSCDSNACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", idAcao)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

        Throw New NotImplementedException()
    End Function

    ''' <summary>
    ''' CnsIteSubAco
    ''' Busca a lista de relação de itens de orcamento substituidos da acao comercial 
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Public Function BuscarItensOrcamentoSubstituidosDaAcao(id As Decimal, markup As Decimal) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT F.CODACOCMC,                           
                             F.CODMERSBT,
                             Z.DESMER AS DESMERSBT,
                             (CASE                                         
                               WHEN D.FLGMERPMC = 'S'                     
                               THEN A.VLRCSTUNTMER                          
                               ELSE (A.VLRCSTUNTMER / :MARKUP)                 
                              END) AS VLRUNTITESUB,         
                             G.CODMER AS CODITEACO,
                             G.NUMLNHITEOCDACOCMC,
                             G.VLRUNTITEOCDACOCMC,
                             G.QDEITEOCDACOCMC
                                FROM MRT.T0230856 A,
                               MRT.T0118392 B,
                               MRT.T0102640 C,
                               MRT.T0201350 D,
                               MRT.T0112963 E,
                               MRT.RLCITEPCPITESBT F,
                               MRT.T0138776 G,
                               MRT.T0100086 Z
                             WHERE C.CODEMP = 1
                             AND C.CODFILEMP = A.CODFILEMP
                             AND C.CODFILEMP = E.CODFILEMP
                             AND C.CODCID = E.CODCIDPCOACOCMC
                             AND C.DATREFALTCID  = B.DATREFLIVPCOVGR   
                             AND A.CODFILEMP = A.CODFILEMPFAT     
                             AND A.NUMRLCCIDGIR  = C.NUMRLCCIDGIR     
                             AND A.CODMER = F.CODMERSBT        
                             AND D.CODEMP = 1               
                             AND D.CODFILEMP = A.CODFILEMP 
                             AND D.CODMER = A.CODMER
                             AND B.CODEMP = 1 
                             AND F.CODACOCMC = :CODACOCMC
                             AND G.CODACOCMC = F.CODACOCMC  
                             AND G.NUMLNHITEOCDACOCMC = F.NUMLNHITEOCDACOCMC 
                             AND G.CODFILEMP = A.CODFILEMP      
                             AND Z.CODEMP = 1      
                             AND Z.CODMER = A.CODMER]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", id)
        If (markup <> 0) Then
            parameterMapper.Parameters.Add(":MARKUP", markup)
        Else
            parameterMapper.Parameters.Add(":MARKUP", 1)
        End If

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' Valida necessidade de canal de venda na acao
    ''' 1. True
    ''' 0. False
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function ValidaNecessidadeCanalVenda(idAcao As Decimal) As Decimal

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT SUM(CASE WHEN B.INDUNDESRNGCACOCMC = 1
                            THEN 1 ELSE 0 END) AS QDE_OPE_FSC 
                            FROM MRT.T0138776 A, MRT.T0155573 B 
                            WHERE A.CODOPEFSCDSNACOCMC = B.CODOPEFSCDSNACOCMC
                            AND A.CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", idAcao)

        Return connector.Execute(sql, parameterMapper)

    End Function

    ''' <summary>
    ''' função para buscar Centro de Custo
    ''' </summary>
    ''' <returns></returns>
    Public Function BuscaCentrodeCusto(NomUsuario As String) As Integer
        'Select para buscar o Centro de Custo por Usuário. 
        'O Analista determinou que para os Usuários Terceiros devem ter um Centro de Custo default. 
        'Exemplo: Código do Centro de Custo = 20210
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT COALESCE(B.CODCENCST, '20210') AS CODCENCST                          
                                     FROM MRT.T0104596 A
                                      LEFT JOIN MRT.T0100361 B ON A.CODFNC = B.CODFNC
                                      LEFT JOIN MRT.CADANSINFTCR C ON A.CODFNC = C.CODANSTCR                  
                                     WHERE UPPER(A.NomUsrRcf) = UPPER(:NomUsuario)]]>.Value
        parameterMapper.Parameters.Add(":NomUsuario", ValueDBProperty(NomUsuario))
        Dim retorno = connector.Execute(sql, parameterMapper)
        Return CInt(retorno)
    End Function

    ''' <summary>
    ''' função de inserção difernte da padrao
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InserirRelacao(item As VO.RelacaoAcaoComercialPorItemOrcamento) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.T0138776( CODACOCMC
                                                             , NUMLNHITEOCDACOCMC
                                                             , CODITEOCDACOCMC
                                                             , QDEITEOCDACOCMC
                                                             , VLRUNTITEOCDACOCMC
                                                             , DATUTZITEACOCMC
                                                             , DESFRMUTZITEACOCMC
                                                             , CODOPEFSCDSNACOCMC
                                                             , VLRUTZOPEDSNACOCMC
                                                             , QDEUTZOPEDSNACOCMC
                                                             , CODMER
                                                             , CODUNDESRNGCOPEDSN
                                                             , CODFILEMP
                                                             , QDEDIACMPITEACOCMC
                                                             , CODCENCST
                                                             , NUMCGCEMPFRN
                                                             , IDTEMPFRNITERSU
                                                             , IDTLCLEMPFRNITERSU
                                                             , IDTTERPGTITERSU
                                                             , DESOBSCPR
                                                             , CODIDTTSC
                                                             , NUMCTTFRN
                                                             , IDTCNTCTB
                                                             , INDITEREQRSUVLRMNM
                                                             , IDTITERSU
                                                             , NUMLNHCTTRSU
                                                             , VLRPTCCLIITEACOCMC
                                                             , VLRPTCFRNITEACOCMC
                                                             , INDITERSUFRNUNC
                                                             , INDITERSUARZPRVPGT
                                                             , DESCTTRSU
                                                             , CODSTAITERSU)
                                                      VALUES( :CODACOCMC
                                                            , :NUMLNHITEOCDACOCMC
                                                            , :CODITEOCDACOCMC
                                                            , :QDEITEOCDACOCMC
                                                            , CAST(:VLRUNTITEOCDACOCMC AS DECIMAL(18,2))
                                                            , TO_DATE(:DATUTZITEACOCMC,'DD-MM-YY')
                                                            , :DESFRMUTZITEACOCMC
                                                            , :CODOPEFSCDSNACOCMC
                                                            , :VLRUTZOPEDSNACOCMC
                                                            , 0
                                                            , :CODMER
                                                            , :CODUNDESRNGCOPEDSN
                                                            , :CODFILEMP
                                                            , :QDEDIACMPITEACOCMC
                                                            , :CODCENCST
                                                            , :NUMCGCEMPFRN
                                                            , :IDTEMPFRNITERSU
                                                            , :IDTLCLEMPFRNITERSU
                                                            , :IDTTERPGTITERSU
                                                            , :DESOBSCPR
                                                            , :CODIDTTSC
                                                            , :NUMCTTFRN
                                                            , :IDTCNTCTB
                                                            , 0
                                                            , :IDTITERSU
                                                            , :NUMLNHCTTRSU
                                                            , :VLRPTCCLIITEACOCMC
                                                            , CAST(:VLRPTCFRNITEACOCMC AS DECIMAL(18,2))
                                                            , :INDITERSUFRNUNC
                                                            , :INDITERSUARZPRVPGT
                                                            , :DESCTTRSU
                                                            , :CODSTAITERSU)
                                               ]]>.Value


        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(item.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMLNHITEOCDACOCMC", ValueDBProperty(item.NUMLNHITEOCDACOCMC))
        If item.CODITEOCDACOCMC = "" Then
            parameterMapper.Parameters.Add(":CODITEOCDACOCMC", ValueDBProperty(" "))
        Else
            parameterMapper.Parameters.Add(":CODITEOCDACOCMC", ValueDBProperty(item.CODITEOCDACOCMC))
        End If
        parameterMapper.Parameters.Add(":QDEITEOCDACOCMC", ValueDBProperty(item.QDEITEOCDACOCMC))
        parameterMapper.Parameters.Add(":VLRUNTITEOCDACOCMC", ValueDBProperty(Math.Round(item.VLRUNTITEOCDACOCMC.Value, 4, MidpointRounding.AwayFromZero)))
        parameterMapper.Parameters.Add(":DATUTZITEACOCMC", ValueDBProperty(item.DATUTZITEACOCMC))
        If item.DESFRMUTZITEACOCMC = "" Then
            parameterMapper.Parameters.Add(":DESFRMUTZITEACOCMC", ValueDBProperty(""))
        Else
            parameterMapper.Parameters.Add(":DESFRMUTZITEACOCMC", ValueDBProperty(item.DESFRMUTZITEACOCMC))
        End If
        parameterMapper.Parameters.Add(":CODOPEFSCDSNACOCMC", ValueDBProperty(item.CODOPEFSCDSNACOCMC))
        parameterMapper.Parameters.Add(":VLRUTZOPEDSNACOCMC", ValueDBProperty(item.VLRUTZOPEDSNACOCMC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(item.CODMER))
        parameterMapper.Parameters.Add(":CODUNDESRNGCOPEDSN", ValueDBProperty(item.CODUNDESRNGCOPEDSN))
        If item.INDTIPOPEDSNACOCMC = 2 Or item.INDTIPOPEDSNACOCMC = 4 Then
            parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty("1"))
        Else
            parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(item.CODFILEMP))
        End If
        parameterMapper.Parameters.Add(":QDEDIACMPITEACOCMC", ValueDBProperty(item.QDEDIACMPITEACOCMC))
        parameterMapper.Parameters.Add(":CODCENCST", ValueDBProperty(item.CODCENCST))
        If item.NUMCGCEMPFRN = "" Then
            parameterMapper.Parameters.Add(":NUMCGCEMPFRN", ValueDBProperty(" "))
        Else
            parameterMapper.Parameters.Add(":NUMCGCEMPFRN", ValueDBProperty(item.NUMCGCEMPFRN))
        End If
        parameterMapper.Parameters.Add(":IDTEMPFRNITERSU", ValueDBProperty(item.IDTEMPFRNITERSU))
        parameterMapper.Parameters.Add(":IDTLCLEMPFRNITERSU", ValueDBProperty(item.IDTLCLEMPFRNITERSU))
        parameterMapper.Parameters.Add(":IDTTERPGTITERSU", ValueDBProperty(item.IDTTERPGTITERSU))
        If (String.IsNullOrEmpty(item.DESOBSCPR)) Then
            parameterMapper.Parameters.Add(":DESOBSCPR", ValueDBProperty(" "))
        Else
            parameterMapper.Parameters.Add(":DESOBSCPR", ValueDBProperty(item.DESOBSCPR))
        End If
        parameterMapper.Parameters.Add(":CODIDTTSC", ValueDBProperty(item.CODIDTTSC))
        parameterMapper.Parameters.Add(":NUMCTTFRN", ValueDBProperty(item.NUMCTTFRN))
        If item.IDTCNTCTB Then
            parameterMapper.Parameters.Add(":IDTCNTCTB", ValueDBProperty(item.IDTCNTCTB))
        Else
            parameterMapper.Parameters.Add(":IDTCNTCTB", ValueDBProperty(0))
        End If
        parameterMapper.Parameters.Add(":IDTITERSU", ValueDBProperty(item.IDTITERSU))
        parameterMapper.Parameters.Add(":NUMLNHCTTRSU", ValueDBProperty(item.NUMLNHCTTRSU))
        If Not IsNothing(item.VLRPTCCLIITEACOCMC) Then
            If Not IsDBNull(item.VLRPTCCLIITEACOCMC) Then
                parameterMapper.Parameters.Add(":VLRPTCCLIITEACOCMC", ValueDBProperty(item.VLRPTCCLIITEACOCMC))
            Else
                parameterMapper.Parameters.Add(":VLRPTCCLIITEACOCMC", ValueDBProperty(0))
            End If
        Else
            parameterMapper.Parameters.Add(":VLRPTCCLIITEACOCMC", ValueDBProperty(0))
        End If
        If Not IsNothing(item.VLRPTCFRNITEACOCMC) Then
            If Not IsDBNull(item.VLRPTCFRNITEACOCMC) Then
                parameterMapper.Parameters.Add(":VLRPTCFRNITEACOCMC", ValueDBProperty(Math.Round(item.VLRPTCFRNITEACOCMC.Value, 4, MidpointRounding.AwayFromZero)))
            Else
                parameterMapper.Parameters.Add(":VLRPTCFRNITEACOCMC", ValueDBProperty(0))
            End If
        Else
            parameterMapper.Parameters.Add(":VLRPTCFRNITEACOCMC", ValueDBProperty(0))
        End If
        parameterMapper.Parameters.Add(":INDITERSUFRNUNC", ValueDBProperty(item.INDITERSUFRNUNC))
        parameterMapper.Parameters.Add(":INDITERSUARZPRVPGT", ValueDBProperty(item.INDITERSUARZPRVPGT))
        parameterMapper.Parameters.Add(":DESCTTRSU", ValueDBProperty(item.DESCTTRSU))
        parameterMapper.Parameters.Add(":CODSTAITERSU", ValueDBProperty(item.CODSTAITERSU))

        Dim retorno = connector.Execute(sql, parameterMapper)
        Return CInt(retorno)
    End Function


    Public Function ObterValorTotalDeMercadoriaPreAcordo(codAcao As Decimal, codMer As Decimal) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CASE WHEN A.VLRPTCCLIITEACOCMC = 0 THEN 
                                                      ROUND(A.VLRUNTITEOCDACOCMC, 4)
                                                 WHEN A.VLRPTCCLIITEACOCMC > 0 THEN
                                                      ROUND(A.VLRPTCFRNITEACOCMC, 4)
                                            END AS VLRTOTMERISRPMC
                                       FROM MRT.T0138776 A
                                      WHERE A.CODMER = :CODMER
                                        AND A.CODACOCMC = :CODACOCMC
                                     UNION 
                                     SELECT CASE WHEN C.VLRPTCCLIITEACOCMC = 0 THEN 
                                                      ROUND(A.VLRUNTITEOCDACOCMC, 4)
                                                 WHEN C.VLRPTCCLIITEACOCMC > 0 THEN 
                                                      ROUND((((A.VLRUNTITEOCDACOCMC * (( C.VLRPTCFRNITEACOCMC * 100) / C.VLRUNTITEOCDACOCMC )) / 100 )), 4)
                                            END AS VLRTOTMERISRPMC
                                       FROM MRT.RLCITEPCPITESBT A, MRT.T0138776 C
                                      WHERE A.CODMERSBT = :CODMER
                                        AND A.CODACOCMC = :CODACOCMC
                                        AND C.CODACOCMC = A.CODACOCMC
                                        AND C.NUMLNHITEOCDACOCMC = A.NUMLNHITEOCDACOCMC
                                        AND C.CODMER = A.CODMER]]>.Value

        parameterMapper.Parameters.Add(":CODMER", codMer)
        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"VLRTOTMERISRPMC"}))
    End Function


    Public Function SelectRelacaoAcaoComercialEItensDeOrcamento(codAcao As Decimal, codMer As Decimal) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                                 A.CODACOCMC, 
                                                 A.CODCENCST, 
                                                 A.CODFILEMP, 
                                                 A.CODIDTTSC, 
                                                 A.CODITEOCDACOCMC, 
                                                 A.CODMER, 
                                                 A.CODOPEFSCDSNACOCMC, 
                                                 A.CODUNDESRNGCOPEDSN, 
                                                 A.DATUTZITEACOCMC, 
                                                 A.DESCTTRSU, 
                                                 A.DESFRMUTZITEACOCMC, 
                                                 A.DESOBSCPR, 
                                                 A.IDTCNTCTB, 
                                                 A.IDTEMPFRNITERSU, 
                                                 A.IDTITERSU, 
                                                 A.IDTLCLEMPFRNITERSU, 
                                                 A.IDTTERPGTITERSU, 
                                                 A.INDITEREQRSUVLRMNM, 
                                                 A.NUMCGCEMPFRN, 
                                                 A.NUMCTTFRN, 
                                                 A.NUMLNHCTTRSU, 
                                                 A.NUMLNHITEOCDACOCMC, 
                                                 A.QDEDIACMPITEACOCMC, 
                                                 A.QDEITEOCDACOCMC, 
                                                 A.QDEUTZOPEDSNACOCMC, 
                                                 A.VLRPTCCLIITEACOCMC, 
                                                 A.VLRPTCFRNITEACOCMC, 
                                                 A.VLRUNTITEOCDACOCMC, 
                                                 A.VLRUTZOPEDSNACOCMC 
                                        FROM     MRT.T0138776 A, MRT.T0155573 B 
                                        WHERE    A.CODACOCMC = :CODACOCMC
                                                 AND A.CODOPEFSCDSNACOCMC = B.CODOPEFSCDSNACOCMC 
                                                 AND A.CODMER = :CODMER
                                                 AND B.INDTIPOPEDSNACOCMC = 1 
                                                 AND B.INDUNDESRNGCACOCMC = 0 ]]>.Value

        parameterMapper.Parameters.Add(":CODMER", codMer)
        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)

        Return connector.ExecuteReader(Of VO.RelacaoAcaoComercialPorItemOrcamento)(sql, parameterMapper)
        'Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(codAcoCmc As Decimal, codMer As Decimal) As VO.RelacaoAcaoComercialPorItemOrcamento

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT   SUM(A.VLRUNTITEOCDACOCMC * A.QDEITEOCDACOCMC) As VLRITEOCDACOCMC, 
                                              SUM(A.VLRUTZOPEDSNACOCMC) AS VLRUTZOPEDSNACOCMC 
                                       FROM     MRT.T0138776 A 
                                                LEFT JOIN MRT.RLCITEPCPITESBT B On B.CODACOCMC = A.CODACOCMC 
                                                          AND B.NUMLNHITEOCDACOCMC = A.NUMLNHITEOCDACOCMC 
                                                          AND B.CODMER = A.CODMER 
                                       WHERE    A.CODACOCMC= :CODACOCMC
                                                AND (A.CODMER= :CODMER OR B.CODMERSBT= :CODMER) 
                                                AND CODOPEFSCDSNACOCMC In(2,4) ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcoCmc)
        parameterMapper.Parameters.Add(":CODMER", codMer)

        Return connector.ExecuteReader(Of VO.RelacaoAcaoComercialPorItemOrcamento)(sql, parameterMapper).FirstOrDefault()
    End Function

#Region "Estorno Manual"
    ''' <summary>
    ''' FunCnsVlrDsp
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function ConsultaValoresDisponiveis(idAcao As Decimal?, codOperacoes As String) As List(Of RelacaoAcaoComercialPorItemOrcamento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC,
                               A.CODOPEFSCDSNACOCMC,
                               A.NUMLNHITEOCDACOCMC,
                               NVL(A.CODMER, 0) AS CODMER,
                               TRUNC(CASE
                                WHEN A.VLRPTCCLIITEACOCMC > 0 AND A.VLRPTCCLIITEACOCMC <= A.VLRUNTITEOCDACOCMC THEN(A.VLRUNTITEOCDACOCMC * A.QDEITEOCDACOCMC)    
                                    *(1 -VLRPTCCLIITEACOCMC / A.VLRUNTITEOCDACOCMC) - (A.VLRUTZOPEDSNACOCMC *(1 - (VLRPTCCLIITEACOCMC / A.VLRUNTITEOCDACOCMC))) 
                                WHEN A.VLRPTCCLIITEACOCMC > 0 AND A.VLRPTCCLIITEACOCMC > A.VLRUNTITEOCDACOCMC THEN 0
                                ELSE(A.VLRUNTITEOCDACOCMC * A.QDEITEOCDACOCMC) -A.VLRUTZOPEDSNACOCMC
                                END,   2) AS SALDO
                                FROM   MRT.T0138776 A
                                INNER JOIN MRT.T0118430 C ON A.CODACOCMC = C.CODACOCMC
                                                    AND C.CODSTAAPVACOCMC= 5
                                INNER JOIN MRT.T0155573 D ON A.CODOPEFSCDSNACOCMC = D.CODOPEFSCDSNACOCMC
                                                        AND D.INDBDEOPEDSNACOCMC =  0
                                WHERE  A.CODACOCMC = :CODACOCMC
                                AND A.CODOPEFSCDSNACOCMC IN ( :CODOPERACOES )
                                AND (A.CODOPEFSCDSNACOCMC <> 5 OR(A.CODOPEFSCDSNACOCMC = 5
                                AND A.CODSTAITERSU = 1
                                AND C.DATAPVACOCMC > TO_DATE('2007-09-30',   'YYYY-MM-DD')))
                                ORDER BY A.NUMLNHITEOCDACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", idAcao)
        parameterMapper.Parameters.Add(":CODOPERACOES", codOperacoes)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' FunCnsVlrDspAco
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function ConsultaValoresDisponiveisDaAcao(idAcao As Decimal?) As List(Of RelacaoAcaoComercialPorItemOrcamento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC as CODACOCMC,
                                   A.NUMLNHITEOCDACOCMC as NUMLNHITEOCDACOCMC,
                                   TRUNC((A.VLRUNTITEOCDACOCMC * A.QDEITEOCDACOCMC) - A.VLRUTZOPEDSNACOCMC, 2) as SALDO
                            From MRT.T0138776 A
                            WHERE A.CODACOCMC = :CODACOCMC
                            Order By A.NUMLNHITEOCDACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", idAcao)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    ''' <summary>
    ''' FunAtuEtrMan
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <param name="codOperacoes"></param>
    ''' <returns></returns>
    Public Function AtualizaEstornoItensAcao(idAcao As Decimal?, codOperacoes As String) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0138776 UP SET (VLRUTZOPEDSNACOCMC, DATUTZITEACOCMC) =
                             (SELECT TRUNC(CASE WHEN A.VLRUNTITEOCDACOCMC > 0 
                                THEN (A.VLRUNTITEOCDACOCMC * A.QDEITEOCDACOCMC)
                                Else (A.VLRUNTITEOCDACOCMC * A.QDEITEOCDACOCMC)
                                END, 2) as SALDO, SYSDATE 
                              FROM MRT.T0138776 A 
                                INNER JOIN MRT.T0118430 C ON A.CODACOCMC = C.CODACOCMC ]]>.Value

        sql += "              WHERE A.CODACOCMC = " + idAcao.ToString()
        sql += "                AND A.CODOPEFSCDSNACOCMC IN (" + codOperacoes + ") "

        sql += <![CDATA[        AND UP.CODACOCMC = A.CODACOCMC  
                                AND UP.NUMLNHITEOCDACOCMC = A.NUMLNHITEOCDACOCMC  
                                AND (A.CODOPEFSCDSNACOCMC <> 5 OR (A.CODOPEFSCDSNACOCMC = 5 AND A.CODSTAITERSU = 1 
                                AND  C.DATAPVACOCMC > TO_DATE('2007-09-30', 'YYYY-MM-DD')))) 
                                WHERE EXISTS
                                (SELECT TRUNC(A.QDEITEOCDACOCMC * A.VLRUNTITEOCDACOCMC, 2), 
                                 SYSDATE
                                 FROM MRT.T0138776 A 
                                  INNER JOIN MRT.T0118430 C ON A.CODACOCMC = C.CODACOCMC 
                                  INNER JOIN MRT.T0155573 D ON A.CODOPEFSCDSNACOCMC = D.CODOPEFSCDSNACOCMC 
                                                           AND D.INDBDEOPEDSNACOCMC =  0 ]]>.Value

        sql += "            WHERE A.CODACOCMC = " + idAcao.ToString()
        sql += "              AND A.CODOPEFSCDSNACOCMC IN (" + codOperacoes + ") "

        sql += <![CDATA[      AND UP.CODACOCMC = A.CODACOCMC  
                              AND UP.NUMLNHITEOCDACOCMC = A.NUMLNHITEOCDACOCMC  
                              AND (A.CODOPEFSCDSNACOCMC <> 5 OR (A.CODOPEFSCDSNACOCMC = 5 AND A.CODSTAITERSU = 1  
                              AND C.DATAPVACOCMC > TO_DATE('2007-09-30', 'YYYY-MM-DD'))))]]>.Value

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    ''' <summary>
    ''' Atualiza o valor do estorno da Ação
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <param name="codOperacoes"></param>
    ''' <returns></returns>
    Public Function AtualizaEstornoVlrAcao(idAcao As Decimal?, codOperacoes As String) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0138008 UP SET (VLRUTZOPEDSNACOCMC) =
                             (SELECT TRUNC(CASE WHEN A.VLRUNTITEOCDACOCMC > 0 
                                    THEN (A.VLRUNTITEOCDACOCMC * A.QDEITEOCDACOCMC)
                                    Else (A.VLRUNTITEOCDACOCMC * A.QDEITEOCDACOCMC)
                                    END, 2) as SALDO 
                              FROM MRT.T0138776 A 
                                INNER JOIN MRT.T0118430 C ON A.CODACOCMC = C.CODACOCMC ]]>.Value

        sql += "              WHERE A.CODACOCMC = " + idAcao.ToString()
        sql += "                AND A.CODOPEFSCDSNACOCMC IN (" + codOperacoes + ") "

        sql += <![CDATA[        AND UP.CODACOCMC = A.CODACOCMC  
                                AND UP.CODOPEFSCDSNACOCMC = A.CODOPEFSCDSNACOCMC
                                AND (A.CODOPEFSCDSNACOCMC <> 5 OR (A.CODOPEFSCDSNACOCMC = 5 AND A.CODSTAITERSU = 1  
                                AND  C.DATAPVACOCMC > TO_DATE('2007-09-30', 'YYYY-MM-DD')))) 
                                WHERE EXISTS
                             (SELECT TRUNC(A.QDEITEOCDACOCMC * A.VLRUNTITEOCDACOCMC, 2)
                              FROM MRT.T0138776 A 
                               INNER JOIN MRT.T0118430 C ON A.CODACOCMC = C.CODACOCMC 
                               INNER JOIN MRT.T0155573 D ON A.CODOPEFSCDSNACOCMC = D.CODOPEFSCDSNACOCMC 
                                                         AND D.INDBDEOPEDSNACOCMC =  0 ]]>.Value

        sql += "              WHERE A.CODACOCMC = " + idAcao.ToString()
        sql += "                AND A.CODOPEFSCDSNACOCMC IN (" + codOperacoes + ") "

        sql += <![CDATA[        AND UP.CODACOCMC = A.CODACOCMC 
                                AND UP.CODOPEFSCDSNACOCMC = A.CODOPEFSCDSNACOCMC
                                AND (A.CODOPEFSCDSNACOCMC <> 5 OR (A.CODOPEFSCDSNACOCMC = 5 AND A.CODSTAITERSU = 1  
                                AND  C.DATAPVACOCMC > TO_DATE('2007-09-30', 'YYYY-MM-DD'))))]]>.Value

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    ''' <summary>
    ''' Atualiza o valor do estorno do carimbo
    ''' </summary>
    ''' <param name="VLRUTZACOCMC"></param>
    ''' <param name="idCarimbo"></param>
    ''' <returns></returns>
    Public Function AtualizaEstornoCarimbo(idCarimbo As Decimal?, vlrEstorno As Decimal?) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.CADMCOVBAFRN 
                                     SET VLRUTZACOCMC = VLRUTZACOCMC - :VLRUTZACOCMC 
                                     WHERE CODMCOVBAFRN = :CODMCOVBAFRN ]]>.Value

        parameterMapper.Parameters.Add(":VLRUTZACOCMC", vlrEstorno)
        parameterMapper.Parameters.Add(":CODMCOVBAFRN", idCarimbo)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    ''' <summary>
    ''' função atualizar itens da acao
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AtuDatUtzIte(mercadoria As VO.RelacaoAcaoComercialPorItemOrcamento)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0138776
                                    SET DATUTZITEACOCMC = TO_DATE(:DATUTZITE,'YYYY-MM-DD')
                                    WHERE CODACOCMC = :INTCODACOCMC And NUMLNHITEOCDACOCMC = :INTNUMLNH]]>.Value


        parameterMapper.Parameters.Add(":DATUTZITE", CDate(mercadoria.DATUTZITEACOCMC).Date.ToString)
        parameterMapper.Parameters.Add(":INTCODACOCMC", mercadoria.CODACOCMC)
        parameterMapper.Parameters.Add(":INTNUMLNH", mercadoria.NUMLNHITEOCDACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function


    Public Function AtuDdoFrnIte(mercadoria As VO.RelacaoAcaoComercialPorItemOrcamento) As Integer
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0138776
            SET DATUTZITEACOCMC = TO_DATE(:DATUTZITE,'YYYY-MM-DD') 
                ,IDTTERPGTITERSU = :IDTTERPGT                            
                ,IDTLCLEMPFRNITERSU = :IDTLCLEMPFRNITERSU                   
                ,NUMCGCEMPFRN = :NUMCGCEMPFRN                    
                ,NUMCTTFRN = :NUMCTTFRN                            
                ,NUMLNHCTTRSU = :NUMLNHCTTRSU                         
                ,DESCTTRSU = :DESCTTRSU                         
                ,IDTEMPFRNITERSU = :IDTEMPFRNITERSU]]>.Value

        If "" <> mercadoria.DESCTTRSU & "/" & mercadoria.NUMLNHCTTRSU & " - " & mercadoria.NUMCGCEMPFRN Then
            sql = sql + " ,INDITEREQRSUVLRMNM   = 0 "
        End If
        sql = sql + " WHERE"
        sql = sql + "CODACOCMC = :INTCODACOCMC"
        sql = sql + " AND NUMLNHITEOCDACOCMC = :INTNUMLNH"

        parameterMapper.Parameters.Add(":DATUTZITE", mercadoria.DATUTZITEACOCMC)
        parameterMapper.Parameters.Add(":IDTTERPGTITERSU", mercadoria.IDTTERPGTITERSU)
        parameterMapper.Parameters.Add(":IDTLCLEMPFRNITERSU", mercadoria.IDTLCLEMPFRNITERSU)
        parameterMapper.Parameters.Add(":NUMCGCEMPFRN", mercadoria.NUMCGCEMPFRN)
        parameterMapper.Parameters.Add(":NUMCTTFRN", mercadoria.NUMCTTFRN)
        parameterMapper.Parameters.Add(":NUMLNHCTTRSU", mercadoria.NUMLNHCTTRSU)
        parameterMapper.Parameters.Add(":DESCTTRSU", mercadoria.DESCTTRSU)
        parameterMapper.Parameters.Add(":INTNUMLNH", mercadoria.NUMLNHITEOCDACOCMC)
        parameterMapper.Parameters.Add(":IDTEMPFRNITERSU", mercadoria.IDTEMPFRNITERSU)
        parameterMapper.Parameters.Add(":INTCODACOCMC", mercadoria.CODACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function


    ''' <summary>
    ''' função Deleta Dados da Ação na tabela RElação Ação Comercial x Itens 
    ''' do Orçamento (T0138776)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DelDdoAcoCmcIte(CodAco As Integer) As Integer
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE FROM MRT.T0138776
                                     WHERE CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CodAco)

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function

    ''' <summary>
    ''' função Insere
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InsDdoAcoCmcIteOcd(StrDdo As String, ItemOcd As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim count As Integer

        count = ItemOcd.Count  'oGrpDdo.Tables("IteOcd").Rows.Count

        For countAux = 0 To count - 1
            Dim sql As String = ""
            'Dim sql As String = <![CDATA[ INSERT INTO MRT.T0138776(
            '                              CODACOCMC,
            '                              NUMLNHITEOCDACOCMC,
            '                              CODITEOCDACOCMC,
            '                              QDEITEOCDACOCMC,
            '                              VLRUNTITEOCDACOCMC,
            '                              DATUTZITEACOCMC,
            '                              DESFRMUTZITEACOCMC,
            '                              CODOPEFSCDSNACOCMC,
            '                              VLRUTZOPEDSNACOCMC,
            '                              QDEUTZOPEDSNACOCMC,
            '                              CODMER,
            '                              CODUNDESRNGCOPEDSN,
            '                              CODFILEMP,
            '                              QDEDIACMPITEACOCMC,
            '                              CODCENCST,
            '                              NUMCGCEMPFRN,
            '                              IDTEMPFRNITERSU,
            '                              IDTLCLEMPFRNITERSU,
            '                              IDTTERPGTITERSU,
            '                              DESOBSCPR,
            '                              CODIDTTSC,
            '                              NUMCTTFRN,
            '                              IDTCNTCTB,
            '                              INDITEREQRSUVLRMNM,
            '                              IDTITERSU,
            '                              NUMLNHCTTRSU,
            '                              VLRPTCCLIITEACOCMC,
            '                              VLRPTCFRNITEACOCMC,
            '                              INDITERSUFRNUNC,
            '                              INDITERSUARZPRVPGT,
            '                              DESCTTRSU,
            '                              CODSTAITERSU)
            '                              VALUES(
            '                              ItemOcd.Item(countAux).CODACOCMC , 
            '                              ItemOcd.Item(countAux).NUMLNHITEOCDACOCMC, " ]]>.Value

            'If oGrpDdo.Tables("IteOcd").Rows(countAux)("DesIteOcd") = "" Then
            'sql += " ' ' , "
            'Else
            '    sCmdSql.Append("'" & oGrpDdo.Tables("IteOcd").Rows(countAux)("DesIteOcd") & "',
            'End If

            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("QdeIteOcd") & ",
            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("VlrUntIte") & ",
            'sCmdSql.Append("TO_DATE('" & oGrpDdo.Tables("IteOcd").Rows(countAux)("DatUtz") & "','YYYY-MM-DD'),

            'If oGrpDdo.Tables("IteOcd").Rows(countAux)("FrmUtzIte") = "" Then
            'sql += " ' ' , "
            'Else
            '    sCmdSql.Append("'" & oGrpDdo.Tables("IteOcd").Rows(countAux)("FrmUtzIte") & "', 
            'End If

            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("CodOpeFsc") & ",
            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("VlrUtz") & ",
            'sql += " 0 , "
            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("CodIte") & ",
            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("CodUndNgc") & ",

            'If CType(oGrpDdo.Tables("IteOcd").Rows(countAux)("CodOpeFsc"), Integer) = 2 Or _
            '   CType(oGrpDdo.Tables("IteOcd").Rows(countAux)("CodOpeFsc"), Integer) = 4 Then
            'sql += " 1 , "
            'Else
            '    sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("CodFilEmp") & ",
            'End If

            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("QdeDiaCmpIteAcoCmc") & ",
            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("CodCenCst") & ",
            'If oGrpDdo.Tables("IteOcd").Rows(countAux)("NumCgcEmpFrn") = "" Then
            'sql += " ' ' , "
            'Else
            '    sCmdSql.Append("'" & oGrpDdo.Tables("IteOcd").Rows(countAux)("NumCgcEmpFrn") & "',
            'End If
            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("IdtEmpFrnIteRsu") & ",
            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("IdtLclEmpFrnIteRsu") & ",
            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("IdtTerPgtIteRsu") & ",
            'If oGrpDdo.Tables("IteOcd").Rows(countAux)("DesObsCpr") = "" Then
            'sql += " ' ' , "
            'Else
            '    sCmdSql.Append("'" & oGrpDdo.Tables("IteOcd").Rows(countAux)("DesObsCpr") & "',
            'End If
            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("CodIdtTsc") & ",
            'sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("NumCttFrn") & ",

            'If oGrpDdo.Tables("IteOcd").Columns.Contains("IdtCntCtb") Then
            '    sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("IdtCntCtb") & ",
            'Else
            'sql += " 0 , "
            'End If

            'sql += " 0 , "
            'sql += " ItemOcd.Item(countAux).IDTITERSU , "
            'sql += " ItemOcd.Item(countAux).NUMLNHCTTRSU , "
            ''
            'If Not IsNothing(oGrpDdo.Tables("IteOcd").Rows(countAux)("vlrCli")) Then
            '    If Not IsDBNull(oGrpDdo.Tables("IteOcd").Rows(countAux)("vlrCli")) Then
            '        sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("vlrCli") & ",
            '    Else
            'sql += " 0 , "
            '    End If
            'Else
            'sql += " 0 , "
            'End If

            ''
            'If Not IsNothing(ItemOcd.Item(countAux).VLRPTCFRNITEACOCMC) Then
            '    If Not IsDBNull(oGrpDdo.Tables("IteOcd").Rows(countAux)("vlrFrn")) Then
            '        sCmdSql.Append(oGrpDdo.Tables("IteOcd").Rows(countAux)("vlrFrn") & ",
            '    Else
            'sql += " 0 , "
            '    End If
            'Else
            'sql += " 0 , "
            'End If

            'sql += " ItemOcd.Item(countAux).INDITERSUFRNUNC , "

            'sql += " ItemOcd.Item(countAux).INDITERSUARZPRVPGT , "
            '
            'sql += " ItemOcd.Item(countAux).DESCTTRSU  ,"
            '
            'sql += " ItemOcd.Item(countAux).CODSTAITERSU ) "

            'Remove dados da stringBuilder
            'sql.Remove(0, sql.Length)

        Next

        parameterMapper.Parameters.Add(":STRDDO", StrDdo)

        Return connector.ExecuteScalar(Sql, parameterMapper)
    End Function

    ''' <summary>
    ''' função Insere 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsrIteSubAco(CodAcoCmc As Integer,
                                 NumLnhIteOcdAcoCmc As Integer,
                                 CodMer As Integer,
                                 CodMerSbt As Integer,
                                 QdeIteOcdCmc As Integer,
                                 VlrUntIteOcdAcoCmc As Double) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.RLCITEPCPITESBT
                                     (CODACOCMC, NUMLNHITEOCDACOCMC, CODMER, CODMERSBT, QDEITEOCDACOCMC, VLRUNTITEOCDACOCMC)
                                     VALUES 
                                     (:CODACOCMC, :NUMLNHITEOCDACOCMC, :CODMER, :CODMERSBT, :QDEITEOCDCMC, :VLRUNTITEOCDACAOCMC)]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CodAcoCmc)
        parameterMapper.Parameters.Add(":NUMLNHITEOCDACOCMC", NumLnhIteOcdAcoCmc)
        parameterMapper.Parameters.Add(":CODMER", CodMer)
        parameterMapper.Parameters.Add(":CODMERSBT", CodMerSbt)
        parameterMapper.Parameters.Add(":QDEITEOCDCMC", QdeIteOcdCmc)
        parameterMapper.Parameters.Add(":VLRUNTITEOCDACAOCMC", VlrUntIteOcdAcoCmc)

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function

    ''' <summary>
    ''' função Deleta  
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DelIteSubAco(CodAcoCmc As Integer,
                                 NumLnhIteOcdAcoCmc As Integer,
                                 CodMer As Integer) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sqlWhere As String = ""
        Dim sql As String = <![CDATA[DELETE MRT.RLCITEPCPITESBT ]]>.Value

        sql += " WHERE "

        If CodAcoCmc <> 0 Then
            sqlWhere += "AND CODACOCMC = :CODACOCMC "
        End If

        If NumLnhIteOcdAcoCmc <> 0 Then
            sqlWhere += "AND NUMLNHITEOCDACOCMC = :NUMLNHITEOCDACOCMC"
        End If

        If CodMer <> 0 Then
            sqlWhere += "AND CODMER = :CODMER "
        End If

        sqlWhere = sqlWhere.Remove(0, 3)
        sql = sql & sqlWhere

        parameterMapper.Parameters.Add(":CODACOCMC", CodAcoCmc)
        parameterMapper.Parameters.Add(":NUMLNHITEOCDACOCMC", NumLnhIteOcdAcoCmc)
        parameterMapper.Parameters.Add(":CODMER", CodMer)

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function



    ''' <summary>
    ''' Atualixa o Saldo do item Orçamento de acordo com o gasto
    ''' </summary>
    ''' <param name="VlrUtilizado">Valor Unitário dos itens</param>
    ''' <param name="QtdUtilizada">Quantidade de itens</param>
    ''' <param name="CODACOCMC">Codigo da ação</param>
    ''' <param name="NUMLNHITEOCDACOCMC">Codigo do item</param>
    ''' <returns>Quantidade de registros alterados</returns>
    Public Function AtualizaSaldoItemOrcamentoGasto(VlrUtilizado As Decimal, QtdUtilizada As Integer, CODACOCMC As Integer, NUMLNHITEOCDACOCMC As Integer) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0138776 
                                        SET VLRUTZOPEDSNACOCMC = (VLRUTZOPEDSNACOCMC + (:VlrUtilizado * :QtdUtilizada))
                                          , QDEUTZOPEDSNACOCMC = (QDEUTZOPEDSNACOCMC + :QtdUtilizada)
                                      WHERE CODACOCMC = :CODACOCMC
                                        AND NUMLNHITEOCDACOCMC = :NUMLNHITEOCDACOCMC
                            ]]>.Value

        parameterMapper.Parameters.Add(":VlrUtilizado", VlrUtilizado)
        parameterMapper.Parameters.Add(":QtdUtilizada", QtdUtilizada)
        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)
        parameterMapper.Parameters.Add(":NUMLNHITEOCDACOCMC", NUMLNHITEOCDACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function


    ''' <summary>
    ''' Atualixa o Saldo do item Orçamento de acordo com o gasto
    ''' </summary>
    ''' <param name="VlrEstornar">Valor Unitário dos itens</param>
    ''' <param name="QtdEstornada">Quantidade de itens</param>
    ''' <param name="CODACOCMC">Codigo da ação</param>
    ''' <param name="NUMLNHITEOCDACOCMC">Codigo do item</param>
    ''' <returns></returns>
    Public Function AtualizaSaldoItemOrcamentoEstorno(VlrEstornar As Decimal, QtdEstornada As Integer, CODACOCMC As Integer, NUMLNHITEOCDACOCMC As Integer) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0138776 
                                        SET VLRUTZOPEDSNACOCMC = (VLRUTZOPEDSNACOCMC - (:VlrEstornar * :QtdEstornada))
                                          , QDEUTZOPEDSNACOCMC = (QDEUTZOPEDSNACOCMC - :QtdEstornada)
                                      WHERE CODACOCMC = :CODACOCMC
                                        AND NUMLNHITEOCDACOCMC = :NUMLNHITEOCDACOCMC
                            ]]>.Value

        parameterMapper.Parameters.Add(":VlrEstornar", VlrEstornar)
        parameterMapper.Parameters.Add(":QtdEstornada", QtdEstornada)
        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)
        parameterMapper.Parameters.Add(":NUMLNHITEOCDACOCMC", NUMLNHITEOCDACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

#End Region

    Private Function Sql() As String
        Throw New NotImplementedException
    End Function

#Region "Fluxo"

    Public Function VldGrpSgmCtb(ByVal CodPlnCntCtb As Decimal, ByVal DesGrpSgmCtb As String) As Object

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT (APPS.MTS_GL_CODE_COMBINATIONS_ACC.CHECK_AND_CREATE_CCID(P_APPL_SHORT_NAME      => 'SQLGL'
                                                                                          , P_KEY_FLEX_CODE        => 'GL#'
                                                                                          , P_FLEX_STRUCTURE_CODE  => :CODPLNCNTCTB
                                                                                          , P_CONC_SEGMENTS        => :DESGRPSGMCTB)) as RET 
                                       FROM DUAL]]>.Value

        parameterMapper.Parameters.Add(":CODPLNCNTCTB", CodPlnCntCtb)
        parameterMapper.Parameters.Add(":DESGRPSGMCTB", DesGrpSgmCtb)

        Return connector.ExecuteScalar(sql, parameterMapper, True)

    End Function

    Public Function RcpCodLivCtb(UndNgc As String) As Decimal
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CodLivCtb FROM MRT.T0123469 WHERE CodUndNgc = :UNDNGC AND DatDst is null]]>.Value
        parameterMapper.Parameters.Add(":UNDNGC", UndNgc)

        Dim result = connector.ExecuteScalar(sql, parameterMapper)

        If Not IsNothing(result) Then
            Return CDec(result)
        End If
        Return Nothing
    End Function

    Public Function cnsVlrMaxIdtRsu(ByVal iCodAcoCmc As Integer) As Decimal
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT (MAX(CODIDTTSC) + 1) AS CODIDTTSC
                                    FROM MRT.T0138776
                                    WHERE (CODACOCMC = :CODACOCMC) ]]>.Value
        parameterMapper.Parameters.Add(":CODACOCMC", iCodAcoCmc)

        Dim result = connector.ExecuteScalar(sql, parameterMapper)

        Dim tamanho = result.ToString().Length
        Dim sresult As String
        If tamanho > 3 Then
            sresult = Mid(CStr(result), result - 2, 3)
        Else
            sresult = CStr(result)
        End If
        result = CInt(sresult)

        If Not IsNothing(result) Then
            Return CDec(result)
        End If
        Return Nothing
    End Function



    Public Function AtlIdtRsu(ByVal item As VO.RelacaoAcaoComercialPorItemOrcamento) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[UPDATE MRT.T0138776
                                    SET CODIDTTSC = :CODIDTTSC
                                    WHERE CODACOCMC  = :CODACOCMC
                                    AND NUMLNHITEOCDACOCMC = :NUMLNHITEOCDACOCMC ]]>.Value


        parameterMapper.Parameters.Add(":CODIDTTSC", item.CODIDTTSC)
        parameterMapper.Parameters.Add(":CODACOCMC", item.CODACOCMC)
        parameterMapper.Parameters.Add(":NUMLNHITEOCDACOCMC", item.NUMLNHITEOCDACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function AltIndIteReqRsuVlrMnm(ByVal item As VO.RelacaoAcaoComercialPorItemOrcamento) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[UPDATE MRT.T0138776
                                    SET IndIteReqRsuVlrMnm = 1
                                    WHERE CODACOCMC  = :CODACOCMC
                                    AND NUMLNHITEOCDACOCMC = :NUMLNHITEOCDACOCMC ]]>.Value


        parameterMapper.Parameters.Add(":CODACOCMC", item.CODACOCMC)
        parameterMapper.Parameters.Add(":NUMLNHITEOCDACOCMC", item.NUMLNHITEOCDACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function csnReqRsuIteAcoMcd(ByVal iCodAcoCmc As Integer) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT B.CODACOCMC
                                                   , C.NUMRMLTLF
                                                   , B.NUMLNHITEOCDACOCMC
                                                   , B.CODITEOCDACOCMC
                                                   , B.CODMER IDTITE
                                                   , B.QDEITEOCDACOCMC
                                                   , B.CODCENCST
                                                   , B.VLRUNTITEOCDACOCMC
                                                   , B.IDTEMPFRNITERSU
                                                   , B.IDTLCLEMPFRNITERSU
                                                   , B.IDTTERPGTITERSU
                                                   , B.IDTCNTCTB
                                                   , B.DESOBSCPR
                                                   , (B.DATUTZITEACOCMC + B.QDEDIACMPITEACOCMC ) AS DATUTZITEACOCMC
                                                   , B.IDTITERSU
                                                   , B.NUMCTTFRN
                                                   , B.NUMLNHCTTRSU
                                                   , NVL(B.INDITERSUARZPRVPGT, 0) as INDITERSUARZPRVPGT   
                                                FROM MRT.T0118430 A
                                               INNER JOIN MRT.T0138776 B ON A.CODACOCMC = B.CODACOCMC
                                               INNER JOIN MRT.T0100361 C ON A.CODFNCCRIACOCMC = C.CODFNC  
                                               INNER JOIN MRT.T0155573 D ON B.CODOPEFSCDSNACOCMC = D.CODOPEFSCDSNACOCMC  
                                               INNER JOIN MRT.T0155581 E ON A.CODACOCMC = E.CODACOCMC  
                                               INNER JOIN MRT.T0155522 F ON E.TIPEVTACOCMC = F.TIPEVTACOCMC    
                                               INNER JOIN MRT.T0104596 G ON A.CODFNCCRIACOCMC = G.CODFNC   
                                               WHERE B.CODMER > 0  
                                                 AND B.CODIDTTSC = 0    
                                                 AND B.INDITEREQRSUVLRMNM = 0
                                                 --AND A.CODSTAAPVACOCMC = 5  -- o status da ação foi alterado mas nao foi realizado commit ainda  
                                                 AND D.INDGRCREQAUTITERSU = 1
                                                 AND (:CODACOCMC = 0 OR A.CODACOCMC = :CODACOCMC) 
                                               ORDER BY B.CODACOCMC
                                                      , B.IDTEMPFRNITERSU]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(iCodAcoCmc))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


    Public Function FunInsDdoOpeDsn(ByVal pCodAcoCmc As Integer) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODOPEFSCDSNACOCMC
                                          , SUM(A.QDEITEOCDACOCMC) AS QDEITEOCDACOCMC
                                          , SUM(A.QDEITEOCDACOCMC * A.VLRUNTITEOCDACOCMC) AS VLRUNTITEOCDACOCMC
                                          , A.CODUNDESRNGCOPEDSN
                                          , A.CODMER
                                          , A.CODACOCMC
                                          , B.INDTIPOPEREQVGM
                                          , B.INDTIPOPESLCPGT
                                          , B.INDTIPOPEREQEVT 
                                       FROM MRT.T0138776 A
                                       INNER JOIN MRT.T0155573 B ON A.CODOPEFSCDSNACOCMC = B.CODOPEFSCDSNACOCMC
                                       WHERE A.CODACOCMC = :CODACOCMC 
                                       GROUP BY A.CODOPEFSCDSNACOCMC
                                           , A.CODUNDESRNGCOPEDSN
                                           , A.CODMER
                                           , B.INDTIPOPEREQVGM
                                           , A.CODACOCMC
                                           , B.INDTIPOPESLCPGT
                                           , B.INDTIPOPEREQEVT
                                           ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(pCodAcoCmc))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function
#End Region

End Class


<TableAttribute("MRT.T0138776")>
Public Class RelacaoAcaoComercialPorItemOrcamentoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMLNHITEOCDACOCMC As Nullable(Of Decimal)

    <DateInsert>
    <[Readonly](False, True)>
    Property DATUTZITEACOCMC As Date

#Region "Campos Auxiliares"
    <IgnoreMap>
    Property DESOPEFSCDSNACOCMC As String
    <IgnoreMap>
    Property INDTIPOPEDSNACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDBDEOPEDSNACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDUNDESRNGCACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESUNDESRNGC As String
    <IgnoreMap>
    Property CODFRNPCPMER As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDGRCITEENCFOLPGT As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDGRCREQAUTITERSU As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESSTAITERSU As String
    <IgnoreMap>
    Property SALDO As Nullable(Of Decimal)
    <IgnoreMap>
    Property IndUtzIte As Nullable(Of Decimal)
    <IgnoreMap>
    Property Checked As Boolean
    <IgnoreMap>
    Property NOMFILEMP As String
    <IgnoreMap>
    Property NUMRMLTLF As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDTIPOPEREQVGM As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDTIPOPESLCPGT As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDTIPOPEREQEVT As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLRTOTMERISRPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLRITEOCDACOCMC As Nullable(Of Decimal)
#End Region

End Class
