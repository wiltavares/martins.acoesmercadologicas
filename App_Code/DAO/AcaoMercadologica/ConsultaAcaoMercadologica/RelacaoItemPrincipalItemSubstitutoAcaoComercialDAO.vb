﻿Imports Core
Imports VO

Public Class RelacaoItemPrincipalItemSubstitutoAcaoComercialDAO
    Inherits DAOBase(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)

    Public Sub New()
        daoConfiguration = New RelacaoItemPrincipalItemSubstitutoAcaoComercialDAOConfiguration()
    End Sub

    Public Function SelectRelacaoItemPrincipalEItemSubstitutoDaAcaoComercial(CODACOCMC As Decimal, CODMERSBT As Decimal) As List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                         A.CODACOCMC, 
                                         A.NUMLNHITEOCDACOCMC, 
                                         A.CODMER, 
                                         A.CODMERSBT, 
                                         A.QDEITEOCDACOCMC, 
                                         A.VLRUNTITEOCDACOCMC 
                                FROM     MRT.RLCITEPCPITESBT A, 
                                         MRT.T0138776 B, 
                                         MRT.T0155573 C 
                                WHERE    A.CODMERSBT = :CODMERSBT
                                         AND A.CODACOCMC = :CODACOCMC
                                         AND A.CODACOCMC = B.CODACOCMC  
                                         AND A.NUMLNHITEOCDACOCMC = B.NUMLNHITEOCDACOCMC 
                                         AND A.CODMER = B.CODMER 
                                         AND B.CODOPEFSCDSNACOCMC = C.CODOPEFSCDSNACOCMC 
                                         AND C.INDTIPOPEDSNACOCMC = 1 
                                         AND C.INDUNDESRNGCACOCMC = 0 ]]>.Value

        parameterMapper.Parameters.Add(":CODMERSBT", CODMERSBT) 'Codigo do item substituto
        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC) 'Codigo da ação  comercial

        Return connector.ExecuteReader(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)(sql, parameterMapper)
        'Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function SalvarItensSubs(item As VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.RLCITEPCPITESBT VALUES(
                                    :CODACAO,
                                    :NUMLINHA, 
                                    :CODMER,
                                    :CODMERSUB, 
                                    :QTEITEM, 
                                    :VLRITE)]]>.Value

        parameterMapper.Parameters.Add(":CODACAO", item.CODACOCMC)
        parameterMapper.Parameters.Add(":NUMLINHA", item.NUMLNHITEOCDACOCMC)
        parameterMapper.Parameters.Add(":CODMER", item.CODMER)
        parameterMapper.Parameters.Add(":CODMERSUB", item.CODMERSBT)
        parameterMapper.Parameters.Add(":QTEITEM", item.QDEITEOCDACOCMC)
        parameterMapper.Parameters.Add(":VLRITE", item.VLRUNTITEOCDACOCMC)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function csnVlrAcoAtu(ByVal iCodAcoCmc As Integer) As List(Of VO.RelacaoItemPrincipalItemSubstitutoAcaoComercialVO)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[  SELECT COALESCE(SUM(A.VLRUNTITEOCDACOCMC * A.QDEITEOCDACOCMC),0) VLRUNTITEOCDACOCMC
                                        FROM MRT.T0138776 A, MRT.T0155573 B
                                        WHERE
                                             B.INDGRCREQAUTITERSU = 1 
                                             AND A.CODOPEFSCDSNACOCMC = B.CODOPEFSCDSNACOCMC 
                                             AND A.CODACOCMC = :CODACOCMC
                                             AND A.CODIDTTSC = 0
                                             AND A.IDTEMPFRNITERSU = 0]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(iCodAcoCmc))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class

<TableAttribute("MRT.RLCITEPCPITESBT")>
Public Class RelacaoItemPrincipalItemSubstitutoAcaoComercialDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODMER As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMLNHITEOCDACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODMERSBT As Nullable(Of Decimal)

    Property VLRUNTITEOCDACOCMC As Nullable(Of Decimal)
    Property QDEITEOCDACOCMC As Nullable(Of Decimal)

End Class