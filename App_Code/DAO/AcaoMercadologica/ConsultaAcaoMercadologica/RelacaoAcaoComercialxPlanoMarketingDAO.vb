﻿Imports Core
Public Class RelacaoAcaoComercialxPlanoMarketingDAO
    Inherits DAOBase(Of VO.RelacaoAcaoComercialxPlanoMarketing)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoComercialxPlanoMarketingDAOConfiguration()
    End Sub

    Public Function ListSaldoPlanoDeMarketingAcao(vo As VO.RelacaoAcaoComercialxPlanoMarketing) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT A.CODUNDESRNGC
                                          , B.DESUNDESRNGC
                                          , A.VLRSLDMNSPLNMKT
                                          , A.VLRSLDATUPLNMKT
                                          , A.VLRSLDANOPLNMKT
                                          , A.VLRRSVPLNMKT
                                          , A.VLRUTZPLNMKT
                                       FROM MRT.T0155581 A
                                      INNER JOIN MRT.T0131275 B ON A.CODUNDESRNGC = B.CODUNDESRNGC
                                      WHERE A.CODACOCMC = :CODACOCMC ]]>.Value


        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function
    Public Function ListSaldoPlanoDeMarketingTipoEvento(vo As VO.RelacaoAcaoComercialxPlanoMarketing, strCodUndNgc As String) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()



        Dim sql As String = <![CDATA[SELECT A.CODUNDESRNGC
                                          , B.DESUNDESRNGC
                                          , (SELECT SUM( VLROCDPLNMKT - VLRRSVPLNMKT - VLRUTZPLNMKT)  
                                               FROM MRT.T0155549              
                                              WHERE CODUNDESRNGC = A.CODUNDESRNGC             
                                                AND AnoMesRef = :DatRef 
                                                AND TipEvtAcoCmc = :TipEvt
                                                AND CodEvtAcoCmc = :CodEvt
                                             ) AS VLRSLDMNSPLNMKT
                                          , (SELECT SUM( VLROCDPLNMKT -  VLRRSVPLNMKT - VLRUTZPLNMKT) 
                                               FROM MRT.T0155549              
                                              WHERE CODUNDESRNGC = A.CODUNDESRNGC             
                                                AND AnoMesRef between concat(:AnoRef,'01') And :DatRef
                                                AND TIPEVTACOCMC =  :TipEvt
                                                AND CODEVTACOCMC =  :CodEvt
                                            ) AS VLRSLDATUPLNMKT         
                                          , (SELECT SUM( VLROCDPLNMKT -  VLRRSVPLNMKT - VLRUTZPLNMKT)
                                               FROM MRT.T0155549              
                                              WHERE CODUNDESRNGC = A.CODUNDESRNGC
                                                AND ANOMESREF between concat(:AnoRef,'01') And concat(:AnoRef,'12')
                                                AND TIPEVTACOCMC =  :TipEvt
                                                AND CODEVTACOCMC =  :CodEvt
                                            ) AS VLRSLDANOPLNMKT     
                                      FROM MRT.T0155549 A
                                     INNER JOIN MRT.T0131275 B ON A.CODUNDESRNGC = B.CODUNDESRNGC
                                     WHERE A.CODUNDESRNGC IN ({0})
                                     GROUP BY A.CODUNDESRNGC
                                         , B.DESUNDESRNGC
                            ]]>.Value

        sql = String.Format(sql, strCodUndNgc.Replace("'", "''").Replace("-", ""))


        'Select não esta sendo utilizado
        '                   (SELECT SUM(  VLRRSVPLNMKT + VLRUTZPLNMKT)
        '       From MRT.T0155549              
        '            Where CODUNDESRNGC = A.CODUNDESRNGC
        'And ANOMESREF between concat(:AnoRef,'01') And concat(:AnoRef,'12')
        'And TIPEVTACOCMC =  : TipEvt
        'And CODEVTACOCMC =  :CodEvt
        '           ) AS VLTOTALGASTO,          

        '   (SELECT SUM(VLROCDPLNMKT)
        '       From MRT.T0155549              
        '            Where CODUNDESRNGC = A.CODUNDESRNGC
        'And ANOMESREF between concat(:AnoRef,'01') And concat(:AnoRef,'12')
        'And TIPEVTACOCMC =  : TipEvt
        'And CODEVTACOCMC =  :CodEvt
        '           ) AS VLTOTALORCADO     


        parameterMapper.Parameters.Add(":DatRef", ValueDBProperty(vo.ANOMESREF))
        parameterMapper.Parameters.Add(":TipEvt", ValueDBProperty(vo.TIPEVTACOCMC))
        parameterMapper.Parameters.Add(":CodEvt", ValueDBProperty(vo.CODEVTACOCMC))
        parameterMapper.Parameters.Add(":AnoRef", ValueDBProperty(vo.ANOMESREF.GetValueOrDefault().ToString().Substring(0, 4)))
        'parameterMapper.Parameters.Add(":strCodUndNgc", ValueDBProperty(strCodUndNgc))

        Dim colunas = {"CODUNDESRNGC", "DESUNDESRNGC", "VLRSLDMNSPLNMKT", "VLRSLDATUPLNMKT", "VLRSLDANOPLNMKT"}

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(colunas))

    End Function

    ''' <summary>
    ''' BscDdoPlnMkt
    ''' Buscar os planos de marketing da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <returns></returns>
    Public Function BuscarPlanosMarketingDaAcao(codAcao As Integer) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC,                               
                              B.TIPEVTACOCMC,
                              C.NOMTIPEVTACOCMC,
                              B.CODEVTACOCMC,
                              D.NOMEVTACOCMC,
                              B.ANOMESREF,
                              B.VLRUTZPLNMKT,
                              B.VLRRSVPLNMKT,
                              B.CODUNDESRNGC,
                              E.DESUNDESRNGC,
                              D.TIPDSNDSCBNF,
                              F.DESDSNDSCBNF,
                              C.INDVLDRGRICT
                              FROM MRT.T0118430 A                               
                             INNER JOIN MRT.T0155581 B                    
                                 ON B.CODACOCMC = A.CODACOCMC             
                             LEFT JOIN MRT.T0155522 C                     
                                 ON C.TIPEVTACOCMC = B.TIPEVTACOCMC       
                             LEFT JOIN MRT.T0155531 D                     
                                 ON D.CODEVTACOCMC = B.CODEVTACOCMC       
                             LEFT JOIN MRT.T0131275 E                     
                                 ON E.CODUNDESRNGC= B.CODUNDESRNGC        
                             LEFT JOIN MRT.T0109059 F                     
                                 ON D.TIPDSNDSCBNF = F.TIPDSNDSCBNF       
                            WHERE A.CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function BscDdoPlnMkt(TipEvt As Integer, CodEvt As Integer, datRef As Integer,
                                 CodUndNgc As Integer) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT   
                                     A.TipEvtAcoCmc,                                        
                                     B.NomTipEvtAcoCmc,                                     
                                     A.CodEvtAcoCmc,                                        
                                     c.nomevtacocmc,                                        
                                     A.CodUndEsrNgc,                                        
                                     d.DESundesrngc,                                        
                                     A.AnoMesRef,                                           
                                     A.VlrOcdPlnMkt,                                        
                                     to_char(A.VlrOcdPlnMkt) as strVlrOcdPlnMkt,            
                                     A.VlrUtzPlnMkt,                                        
                                     to_char(A.VlrUtzPlnMkt) as strVlrUtzPlnMkt,            
                                     A.VlrOcdCttPlnMkt,                                     
                                     to_char(A.VlrOcdCttPlnMkt) as strVlrOcdCttPlnMkt,      
                                     A.VlrOcdExaCttPlnMkt,                                  
                                     to_char(A.VlrOcdExaCttPlnMkt) as strVlrOcdExaCttPlnMkt,
                                     A.VlrRsvPlnMkt,                                        
                                     to_char(A.VlrRsvPlnMkt) as strVlrRsvPlnMkt             
                            FROM MRT.t0155549 A,                                            
                                     MRT.t0155522 B,                                        
                                     MRT.t0155531 C,                                        
                                     MRT.T0131275 D                                         
                            WHERE a.tipevtacocmc = b.tipevtacocmc and                       
                                     a.codevtacocmc = c.codevtacocmc and                    
                                     A.CODUNDESRNGC = d.codundesrngc]]>.Value

        'Tipo de  evento'
        If TipEvt <> 0 Then
            sql += " AND A.TipEvtAcoCmc = :TIPEVET "

            parameterMapper.Parameters.Add(":TIPEVET", TipEvt)
        End If

        'Código do evento'
        If CodEvt <> 0 Then
            sql += " AND A.CodEvtAcoCmc = :CODEVET "

            parameterMapper.Parameters.Add(":CODEVET", CodEvt)
        End If

        'Data de referência'
        If datRef <> 0 Then
            sql += " AND a.AnoMEsREf = :DATREF "

            parameterMapper.Parameters.Add(":DATREF", datRef)
        End If

        If CodUndNgc <> 0 Then
            sql += " AND A.CODUNDESRNGC = :CODUNDNGC "

            parameterMapper.Parameters.Add(":CODUNDNGC", CodUndNgc)
        End If

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' BscDdoFrnPlnMktAco
    ''' Buscar os dados de plano de marketing relacionados aos fornecedores da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <returns></returns>
    Public Function BuscarDadosFornecedorPlanoMarketing(codAcao As Decimal) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT(A.CODUNDESRNGC),
                            SUM(A.VLRPTCFRNACOCMC + A.VLRTXAADMFRNACOCMC + A.VLRTXAADMBDEACOCMC) AS VLRRSVPLNMKT
                                 FROM MRT.T0138768 A
                            WHERE A.CODACOCMC = :CODACOCMC
                            GROUP BY A.CODUNDESRNGC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODUNDESRNGC", "VLRRSVPLNMKT"}))
    End Function

    ''' <summary>
    ''' Deletar todos os planos de marketing da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <returns></returns>
    Public Function DeletarPlanosMarketingDaAcao(codAcao As Decimal) As Object
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE FROM MRT.T0155581
                                      WHERE CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)
        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    ''' <summary>
    ''' InsDdoAcoCmcPlnMkt
    ''' Insere Dados da Ação na tabela RElação Ação Comercial x Plano
    ''' de Marketing (T0155581)
    ''' </summary>
    ''' <param name="planoMarketing"></param>
    ''' <returns></returns>
    Public Function InserirDadosAcaoPlanoMarketing(planoMarketing As VO.RelacaoAcaoComercialxPlanoMarketing) As Object

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.T0155581
                                          ( CODACOCMC
                                          , TIPEVTACOCMC
                                          , CODEVTACOCMC
                                          , ANOMESREF
                                          , VLRUTZPLNMKT
                                          , VLRRSVPLNMKT
                                          , CODUNDESRNGC
                                          , VLRSLDMNSPLNMKT
                                          , VLRSLDATUPLNMKT
                                          , VLRSLDANOPLNMKT
                                          )
                                    VALUES( :CODACOCMC    
                                          , :TIPEVTACOCMC
                                          , :CODEVTACOCMC
                                          , :ANOMESREF
                                          , :VLRUTZPLNMKT
                                          , :VLRRSVPLNMKT
                                          , :CODUNDESRNGC
                                          , :VLRSLDMNSPLNMKT
                                          , :VLRSLDATUPLNMKT
                                          , :VLRSLDANOPLNMKT
                                          )]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", planoMarketing.CODACOCMC)
        parameterMapper.Parameters.Add(":TIPEVTACOCMC", planoMarketing.TIPEVTACOCMC)
        parameterMapper.Parameters.Add(":CODEVTACOCMC", planoMarketing.CODEVTACOCMC)
        parameterMapper.Parameters.Add(":ANOMESREF", planoMarketing.ANOMESREF)
        parameterMapper.Parameters.Add(":VLRUTZPLNMKT", planoMarketing.VLRUTZPLNMKT)
        parameterMapper.Parameters.Add(":VLRRSVPLNMKT", planoMarketing.VLRRSVPLNMKT)
        parameterMapper.Parameters.Add(":CODUNDESRNGC", planoMarketing.CODUNDESRNGC)
        parameterMapper.Parameters.Add(":VLRSLDMNSPLNMKT", planoMarketing.VLRSLDMNSPLNMKT)
        parameterMapper.Parameters.Add(":VLRSLDATUPLNMKT", planoMarketing.VLRSLDATUPLNMKT)
        parameterMapper.Parameters.Add(":VLRSLDANOPLNMKT", planoMarketing.VLRSLDANOPLNMKT)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function


    ''' <summary>
    ''' Buscar plano de marketing durante operação de fluxo
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <returns></returns>
    Public Function BuscarPlanosMarketingDaAcaofluxo(codAcao As Decimal) As Object
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODUNDESRNGC,                 
                                     B.DESUNDESRNGC,           
                                     VLRSLDMNSPLNMKT, 
                                     VLRSLDATUPLNMKT, 
                                     VLRSLDANOPLNMKT, 
                                     VLRRSVPLNMKT,    
                                     VLRUTZPLNMKT     
                                     FROM MRT.T0155581 A,                   
                                     MRT.T0131275 B                         
                                     WHERE A.CODUNDESRNGC = B.CODUNDESRNGC  
                                     AND A.CODACOCMC    = :CODACAO]]>.Value

        parameterMapper.Parameters.Add(":CODACAO", codAcao)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function BuscaPlanoMarketingProvisionadoAcao(ByVal codAcao As Decimal) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODUNDESRNGC
                                          , A.VLRRSVPLNMKT
                                          , A.TIPEVTACOCMC
                                          , A.CODEVTACOCMC
                                          , A.ANOMESREF
                                          , A.CODACOCMC
                                       FROM MRT.T0155581 A
                                      INNER JOIN MRT.T0155549 B ON A.TIPEVTACOCMC = B.TIPEVTACOCMC
                                                               AND A.CODEVTACOCMC = B.CODEVTACOCMC
                                                               AND A.CODUNDESRNGC = B.CODUNDESRNGC
                                                               AND A.ANOMESREF    = B.ANOMESREF
                                    WHERE CODACOCMC = :CODACOCMC
                              ]]>.Value



        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODUNDESRNGC", "VLRRSVPLNMKT", "TIPEVTACOCMC", "CODEVTACOCMC", "ANOMESREF", "CODACOCMC"}))
    End Function

    Public Function UpdatePlanoMktAprovacaoAcao(CODACOCMC As Decimal, TIPEVTACOCMC As Decimal, CODEVTACOCMC As Decimal, CODUNDESRNGC As Decimal, ANOMESREF As String) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0155581
                                        SET VLRUTZPLNMKT = VLRRSVPLNMKT 
                                          , VLRRSVPLNMKT = 0.00 
                                      WHERE CODACOCMC = :CODACOCMC
                                        AND TIPEVTACOCMC = :TIPEVTACOCMC
                                        AND CODEVTACOCMC = :CODEVTACOCMC
                                        AND CODUNDESRNGC = :CODUNDESRNGC
                                        AND ANOMESREF = :ANOMESREF]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)
        parameterMapper.Parameters.Add(":TIPEVTACOCMC", TIPEVTACOCMC)
        parameterMapper.Parameters.Add(":CODEVTACOCMC", CODEVTACOCMC)
        parameterMapper.Parameters.Add(":CODUNDESRNGC", CODUNDESRNGC)
        parameterMapper.Parameters.Add(":ANOMESREF", ANOMESREF)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function ListarPlanoMktAcaoParaProvacao(CODACOCMC As Decimal) As List(Of VO.RelacaoAcaoComercialxPlanoMarketing)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT B.CODACOCMC
                                          , B.TIPEVTACOCMC
                                          , B.CODEVTACOCMC
                                          , B.CODUNDESRNGC
                                          , B.ANOMESREF
                                          , (SELECT COUNT(*) 
                                               FROM MRT.RLCACOACREDEUTZACR 
                                               WHERE CODEDEUTZACOACR = :CODACOCMC 
                                          ) AS QTD 
                                       FROM MRT.T0118430 A 
                                       LEFT JOIN MRT.T0155581 B ON A.CODACOCMC = B.CODACOCMC 
                                      WHERE A.CODACOCMC =  :CODACOCMC
                                  ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "TIPEVTACOCMC", "CODEVTACOCMC", "CODUNDESRNGC", "ANOMESREF", "QTD"}))

    End Function

End Class


<TableAttribute("MRT.T0155581")>
Public Class RelacaoAcaoComercialxPlanoMarketingDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Integer

    <PrimaryKey>
    Property TIPEVTACOCMC As Integer

    <PrimaryKey>
    Property CODEVTACOCMC As Integer

    <PrimaryKey>
    Property CODUNDESRNGC As Integer


    <IgnoreMap>
    Property QTD As Nullable(Of Integer)
End Class
