﻿Imports Core
Imports VO
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Globalization
Imports System.Data.Common
Imports System.Data

Public Class ConsultaAcompanhamentoBcComFundingDAO
    Inherits DAOBase(Of VO.ConsultaAcompanhamentoBcComFunding)
    Public Sub New()
        daoConfiguration = New ConsultaAcompanhamentoBcComFundingDAOConfiguration()
    End Sub

    Public Function Listar(dadosFiltro As VO.ConsultaAcompanhamentoBcComFunding) As List(Of VO.ConsultaAcompanhamentoBcComFunding)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                        SELECT BFC.CODGRPFRN
                             , GRPFRN.NOMGRPFRN
                             , HST.DATREFOPE
                             , BFC.CODBFC
                             , BFC.DESBFC
                             , BFC.DATINIVLDBFC
                             , BFC.DATFIMVLDBFC
                             , Filial.CODFILEMP
                             , Filial.NOMFILEMP
                             , FRN.CODFRN
                             , FRN.NOMFRN 
                             , MER.CODMER
                             , MER.DESMER
                             , (CASE WHEN HST.PERMAXFNDITE > 0 
                                   THEN HST.PERMAXFNDITE
                                 ELSE 
                                   HST.PERDSCBFCFNDITE
                                 END
                               ) AS PERBFC
                             , SUM(HST.QDEITEBFC) AS QDEITEBFC
                             , SUM(HST.VLRUNTFNDMERBFC * HST.QDEITEBFC ) AS VLRTOTFNDBFC
                          FROM MRT.HSTFNDMERBFC HST
                         INNER JOIN MRT.T0140576 BFC ON BFC.CODBFC = HST.CODBFC AND BFC.INDBFCFND = 1
                          LEFT JOIN MRT.T0107579 GRPFRN ON GRPFRN.CODGRPFRN  = BFC.CODGRPFRN AND GRPFRN.CODEMP = 1 AND GRPFRN.CODFILEMP = 1 
                          LEFT JOIN MRT.T0112963 Filial ON Filial.CODFILEMP = HST.CODFILEMP 
                          LEFT JOIN MRT.T0100086 MER ON MER.CODMER = HST.CODMER
                          LEFT JOIN MRT.T0100426 FRN ON FRN.CODFRN = HST.CODFRN
                          LEFT JOIN MRT.T0100426 GRPFRN ON GRPFRN.CODGRPFRN = BFC.CODGRPFRN AND GRPFRN.CODFRN = HST.CODFRN
                          LEFT JOIN MRT.T0113625 COMP ON COMP.CODCPR = FRN.CODCPR
                          LEFT JOIN MRT.T0118570 CEL ON CEL.CODGERPRD = COMP.CODGERPRD
                         WHERE HST.TIPOPEINIGRCHST = :TIPOPEINIGRCHST
                           AND HST.DATREFOPE BETWEEN :DATREFOPEINI AND :DATREFOPEFIM
                           AND (:CODGRPFRN IS NULL OR BFC.CODGRPFRN = :CODGRPFRN)
                           AND (:CODFRN IS NULL OR HST.CODFRN  = :CODFRN) --:DIVISAO FORNECEDOR
                           AND (:CODBFC IS NULL OR BFC.CODBFC = :CODBFC) --:CODIGO BENEFICIO
                         GROUP BY BFC.CODGRPFRN 
                             , GRPFRN.NOMGRPFRN
                             , HST.DATREFOPE 
                             , BFC.CODBFC
                             , BFC.DESBFC
                             , BFC.DATINIVLDBFC
                             , BFC.DATFIMVLDBFC
                             , Filial.CODFILEMP
                             , Filial.NOMFILEMP
                             , FRN.CODFRN
                             , FRN.NOMFRN 
                             , MER.CODMER
                             , MER.DESMER
                             , (CASE WHEN HST.PERMAXFNDITE > 0 
                                    THEN HST.PERMAXFNDITE
                                  ELSE 
                                    HST.PERDSCBFCFNDITE
                                  END
                               ) 
                      ORDER BY BFC.CODGRPFRN
                             , FRN.CODFRN
                             , BFC.CODBFC
                             , HST.DATREFOPE

                         ]]>.Value


        parameterMapper.Parameters.Add(":TIPOPEINIGRCHST", ValueDBProperty(dadosFiltro.TIPOPEINIGRCHST))
        parameterMapper.Parameters.Add(":DATREFOPEINI", ValueDBProperty(dadosFiltro.DATREFOPEINI))
        parameterMapper.Parameters.Add(":DATREFOPEFIM", ValueDBProperty(dadosFiltro.DATREFOPEFIM))
        parameterMapper.Parameters.Add(":CODGRPFRN", ValueDBProperty(dadosFiltro.CODGRPFRN))
        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(dadosFiltro.CODFRN))
        parameterMapper.Parameters.Add(":CODBFC", ValueDBProperty(dadosFiltro.CODBFC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPFRN", "NOMGRPFRN", "DATREFOPE", "CODBFC", "DESBFC", "DATINIVLDBFC", "DATFIMVLDBFC", "CODFILEMP", "NOMFILEMP", "CODFRN", "NOMFRN", "CODMER", "DESMER", "PERBFC", "QDEITEBFC", "VLRTOTFNDBFC"}))
    End Function

End Class

<Table("")>
Public Class ConsultaAcompanhamentoBcComFundingDAOConfiguration
    Inherits DAOConfiguration

End Class