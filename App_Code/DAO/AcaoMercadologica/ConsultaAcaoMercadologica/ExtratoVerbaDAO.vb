﻿Imports Core
Imports VO
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Globalization
Imports System.Data.Common
Imports System.Data

Public Class ExtratoVerbaDAO
    Inherits DAOBase(Of VO.ExtratoVerba)

    Public Sub New()
        daoConfiguration = New ExtratoVerbaDAOConfiguration()
    End Sub

    Public Function ListaSaldoFornecedor(dadosFiltro As VO.ExtratoVerba) As List(Of VO.ExtratoVerba)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT C.CODFRN
                                          , FRN.NOMFRN
                                          , DIV.CODDIVCMP
                                          , DIV.DESDIVCMP
                                          , DIR.CODDRTCMP
                                          , DIR.DESDRTCMP
                                          , F.TIPDSNDSCBNF
                                          , F.DESDSNDSCBNF
                                          , C.VLRSLDMESANT + C.VLRCRDMESCRR - C.VLRDEBMESCRR  + COALESCE(PMS.VLRPMS, 0)  AS VLRSLD
                                          , COALESCE(RES.VLRSLDRSVFRN, 0) AS VLRSLDRSV
                                       FROM MRT.T0100426  FRN  -- Fornecedor
                                       LEFT JOIN MRT.T0118570 DIV ON DIV.CODGERPRD = FRN.CODGERPRD  -- CELULA
                                       LEFT JOIN MRT.T0123183 DIR ON DIR.CODDRTCMP = DIV.CODDRTCMP       -- DIRETORIA
                                      INNER JOIN MRT.T0123078 C ON FRN.CODFRN = C.CODFRN -- SALDO
                                      INNER JOIN MRT.T0109059 F ON F.TIPDSNDSCBNF = C.TIPDSNDSCBNF         -- EMPENHOS  
                                       LEFT JOIN (SELECT D.CODFRN, COALESCE(TRUNC(SUM(D.VLRSLDRSVFRN), 2), 0)   AS  VLRSLDRSVFRN                      
                                                    FROM MRT.T0123159 D 
                                                   WHERE D.FLGAPVACOCMC = 'N'
                                                     AND D.TIPDSNDSCBNF = :TIPDSNDSCBNF
                                                   GROUP BY D.CODFRN) RES ON RES.CODFRN = FRN.CODFRN   -- SALDO RESERVADO
                                       LEFT JOIN (SELECT  A.CODFRN
                                                          , TRUNC(COALESCE(SUM(                                                
                                                            CASE WHEN A.CODSITPMS = 0 OR A.CODSITPMS = 1 THEN                 
                                                                  (B.VLRNGCPMS) - (B.VLRPGOPMS) - ((B.VLRNGCPMS - B.VLRPGOPMS) * (J.PERJURSLDNEG + J.PERPIS) / 100)                          
                                                            ELSE                                                              
                                                                  (B.VLREFTPMS) - (B.VLRPGOPMS) - ((B.VLREFTPMS - B.VLRPGOPMS) * (J.PERJURSLDNEG + J.PERPIS) / 100)                
                                                            END),0),2) AS VLRPMS                                       
                                                      FROM MRT.T0118058 A
                                                     INNER JOIN MRT.T0118066 B ON B.CODEMP = A.CODEMP AND B.CODFILEMP = A.CODFILEMP AND B.CODPMS = A.CODPMS AND B.DATNGCPMS = A.DATNGCPMS       
                                                     INNER JOIN MRT.T0118074 J ON J.CODFILEMP = 1     
                                                     INNER JOIN MRT.T0109059 X on B.TIPDSNDSCBNF = X.TIPDSNDSCBNF
                                                  WHERE (A.INDASCARDFRNPMS IS NULL OR A.INDASCARDFRNPMS = 0)    
                                                    AND A.CODSITPMS IN (0, 1, 3)                         
                                                    AND B.TIPDSNDSCBNF =  :TIPDSNDSCBNF
                                                    AND X.FLGCTBDSNDSC = 'S'
                                                  GROUP BY A.CODFRN) PMS ON PMS.CODFRN = FRN.CODFRN
                                      WHERE F.FLGCTBDSNDSC = 'S' 
                                        AND C.TIPDSNDSCBNF = :TIPDSNDSCBNF
                                        AND C.ANOMESREF = :ANOMESREF
                                        AND (:CODFRN IS NULL OR C.CODFRN = :CODFRN )
                                        AND C.CODFRN IN(SELECT CPR.CODFRN FROM MRT.T0153541 CPR WHERE (:CODCPR IS NULL OR CPR.CODCPR = :CODCPR))
                                        AND (:CODDIVCMP IS NULL OR DIV.CODDIVCMP = :CODDIVCMP)               -- CELULA 
                                        AND (:CODDRTCMP IS NULL OR DIR.CODDRTCMP = :CODDRTCMP)               -- DIRETORIA
                         ]]>.Value


        parameterMapper.Parameters.Add(":TIPDSNDSCBNF", ValueDBProperty(dadosFiltro.TIPDSNDSCBNF))
        parameterMapper.Parameters.Add(":ANOMESREF", ValueDBProperty(DateTime.Now().ToString("yyyyMM")))
        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(dadosFiltro.CODFRN))
        parameterMapper.Parameters.Add(":CODCPR", ValueDBProperty(dadosFiltro.CODCPR))
        parameterMapper.Parameters.Add(":CODDIVCMP", ValueDBProperty(dadosFiltro.CODDIVCMP))
        parameterMapper.Parameters.Add(":CODDRTCMP", ValueDBProperty(dadosFiltro.CODDRTCMP))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFRN", "NOMFRN", "TIPDSNDSCBNF", "DESDSNDSCBNF", "VLRSLD", "VLRSLDRSV", "CODDIVCMP", "DESDIVCMP", "CODDRTCMP", "DESDRTCMP"}))

    End Function

    Public Function ListaExtratoFornecedor(dadosFiltro As VO.ExtratoVerba) As List(Of VO.ExtratoVerba)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[
                    SELECT FRN.CODFRN
                         , FRN.NOMFRN
                         , DIV.CODDIVCMP
                         , DIV.DESDIVCMP
                         , DIR.CODDRTCMP
                         , DIR.DESDRTCMP
                         , TMP.VLRMOV
                         , TMP.VLRSLDRSV
                         , TMP.DT_OCORRENCIA
                         , TMP.IDTMOV
                         , TMP.TIPO_MOV
                    FROM 
                    (
                        SELECT C.CODFRN
                             , C.VLRSLDMESANT AS VLRMOV 
                             , 0 AS VLRSLDRSV
                             , TO_DATE( C.ANOMESREF || '01', 'yyyyMMDD' )  AS DT_OCORRENCIA
                             , '' AS IDTMOV
                             , 'SALDO INICIAL' AS TIPO_MOV
                             , 1 AS ORDEM
                             , 1 AS SUB_ORDEM
                             , 1 AS CODFILEMP
                         FROM MRT.T0123078 C
                        WHERE C.TIPDSNDSCBNF = :TIPDSNDSCBNF
                     ------     AND C.ANOMESREF = :ANOMESREF    --  :TODO 
                          AND (TO_DATE( ANOMESREF || '01', 'yyyyMMDD' ) BETWEEN :DATAINI AND :DATAFIM)                   -- DATA DO SALDO MÊS

                    UNION ALL  -- PROMESSAS/VERBA
                            SELECT PMS.CODFRN
                                 , TRUNC(COALESCE(
                                      CASE WHEN PMS.CODSITPMS = 0 OR PMS.CODSITPMS = 1 THEN                 
                                            (B.VLRNGCPMS) - (B.VLRPGOPMS) - ((B.VLRNGCPMS - B.VLRPGOPMS) * (J.PERJURSLDNEG + J.PERPIS) / 100)                          
                                      ELSE
                                            (B.VLREFTPMS) - (B.VLRPGOPMS) - ((B.VLREFTPMS - B.VLRPGOPMS) * (J.PERJURSLDNEG + J.PERPIS) / 100)                
                                      END,0),2) AS VLRMOV
                                 , 0 AS VLRSLDRSV
                                 , B.DATPRVRCBPMS as DT_OCORRENCIA
                                 , CASE WHEN DEST.CODNGC IS NULL THEN 
                                  'ACORDO: ' || TO_CHAR(PMS.CODPMS)
                                  ELSE
                                  'NEG:' || DEST.CODNGC || ' ACORDO: ' || PMS.CODPMS
                                  END   AS IDTMOV
                                 , 'VERBA A RECEBER' AS TIPO_MOV
                                 , 1 AS ORDEM
                                 , 2 AS SUB_ORDEM
                                 , PMS.CODFILEMP
                              FROM  MRT.T0118058 PMS 
                             INNER JOIN MRT.T0118066 B ON B.CODEMP = PMS.CODEMP AND B.CODFILEMP = PMS.CODFILEMP AND B.CODPMS = PMS.CODPMS AND B.DATNGCPMS = PMS.DATNGCPMS 
                             INNER JOIN MRT.T0118074 J ON J.CODFILEMP = 1  
                             LEFT JOIN MRT.RLCNGCVBAFRNDSN DEST ON  PMS.CODPMS = DEST.CODPMS
                             WHERE PMS.CODSITPMS IN (0, 1, 3)        
                              AND (PMS.INDASCARDFRNPMS IS NULL OR PMS.INDASCARDFRNPMS = 0)    
                              AND B.TIPDSNDSCBNF =  :TIPDSNDSCBNF               
                        UNION ALL  -- ACORDOS RECEBIDOS 
                        SELECT RECB.CODFRN
                             , RECB.VLRLMTCTB as VLRMOV 
                             , 0 AS VLRSLDRSV
                             , RECB.DATREFLMT AS DT_OCORRENCIA
                             , CASE WHEN DEST.CODNGC IS NULL THEN 
                                  'ACORDO: ' || TO_CHAR(RECB.CODPMS)
                                  ELSE
                                  'NEG:' || DEST.CODNGC || ' ACORDO: ' || RECB.CODPMS
                                  END   AS IDTMOV
                             , 'VERBA RECEBIDA' as TIPO_MOV
                             , 1 AS ORDEM
                             , 2 AS SUB_ORDEM
                             , CODFILEMP
                          FROM MRT.T0123086 RECB
                          LEFT JOIN MRT.RLCNGCVBAFRNDSN DEST ON DEST.CODPMS = RECB.CODPMS
                         WHERE RECB.CODGDC = 'C'
                           AND RECB.TIPDSNDSCBNF =  :TIPDSNDSCBNF   
                           AND (RECB.DATREFLMT BETWEEN :DATAINI AND :DATAFIM)    -- DATA DE RECEBIMENTO

                    UNION ALL  -- AÇOES COMERCIAIS
                            SELECT PFRN.CODFRN
                                 , PFRN.VLRPTCFRNACOCMC AS VLRMOV 
                                 , 0 AS VLRSLDRSV
                                 , ACAO.DT_OCORRENCIA
                                 , 'AÇÃO:' || TO_CHAR(ACAO.CODACOCMC) AS IDTMOV
                                 , CASE WHEN ACAO.CODSTAAPVACOCMC = 5 THEN
                                      'AÇÃO APROVADA' 
                                    WHEN  ACAO.CODSTAAPVACOCMC = 3 THEN 
                                      'AÇÃO EM APROVAÇÃO' END
                                   AS TIPO_MOV
                                 , 1 AS ORDEM
                                 , 2 AS SUB_ORDEM
                                 , ACAO.CODFILEMPORIVBA AS CODFILEMP
                              FROM MRT.T0138768 PFRN   -- PARTICIPACAO DO FORNECEDOR NA AÇÃO
                             INNER JOIN(SELECT A.CODACOCMC
                                             ,  CASE WHEN A.CODSTAAPVACOCMC = 5 THEN
                                                    A.DATAPVACOCMC
                                                  WHEN  A.CODSTAAPVACOCMC = 3 THEN 
                                                    SYSDATE
                                                END AS DT_OCORRENCIA
                                             , A.NOMACOCMC                         
                                             , F.TIPDSNDSCBNF
                                             , F.DESDSNDSCBNF
                                             , A.CODFILEMPORIVBA
                                             , A.CODSTAAPVACOCMC
                                         FROM MRT.T0118430 A         -- AÇÂO                          
                                        INNER JOIN MRT.T0155581 B ON B.CODACOCMC = A.CODACOCMC        -- PLANO DE MARKETING DA AÇÃO        
                                         LEFT JOIN MRT.T0155522 C ON C.TIPEVTACOCMC = B.TIPEVTACOCMC  -- TIPO DE EVENTO    
                                         LEFT JOIN MRT.T0155531 D ON D.CODEVTACOCMC = B.CODEVTACOCMC  -- EVENTO
                                         LEFT JOIN MRT.T0131275 E ON E.CODUNDESRNGC= B.CODUNDESRNGC        
                                         LEFT JOIN MRT.T0109059 F ON D.TIPDSNDSCBNF = F.TIPDSNDSCBNF  -- EMPENHO              
                                        WHERE A.TIPACOMCD = 1   -- AÇÕES DE FORNECEDOR
                                          AND A.CODSTAAPVACOCMC IN(3, 5) -- ACAO (3)EM APROVACAO OU (5)APROVADA
                                          AND F.TIPDSNDSCBNF = :TIPDSNDSCBNF
                                        GROUP BY A.CODACOCMC  
                                            , A.NOMACOCMC
                                             ,  CASE WHEN A.CODSTAAPVACOCMC = 5 THEN
                                                  A.DATAPVACOCMC
                                                  WHEN  A.CODSTAAPVACOCMC = 3 THEN 
                                                    SYSDATE
                                                END
                                            , F.TIPDSNDSCBNF
                                            , F.DESDSNDSCBNF
                                            , A.CODFILEMPORIVBA
                                            , A.CODSTAAPVACOCMC
                                 ) ACAO ON PFRN.CODACOCMC = ACAO.CODACOCMC                      
                             WHERE (ACAO.DT_OCORRENCIA BETWEEN :DATAINI AND :DATAFIM)    -- DATA DE APROVAÇÃO DA AÇÃO 

                    UNION ALL  -- SALDO ATUAL
                                   SELECT FRN.CODFRN
                                        , C.VLRSLDMESANT + C.VLRCRDMESCRR - C.VLRDEBMESCRR  + COALESCE(PMS.VLRPMS, 0)  AS VLRMOV
                                        , COALESCE(RES.VLRSLDRSVFRN, 0) AS VLRSLDRSV
                                        , SYSDATE AS DT_OCORRENCIA
                                        , '' AS  IDTMOV
                                        , 'SALDO FINAL' TIP_MOV
                                        , 99 AS ORDEM
                                        , 1 AS SUB_ORDEM
                                        , 1 AS CODFILEMP
                                    FROM MRT.T0100426  FRN  -- Fornecedor
                                    INNER JOIN MRT.T0123078 C ON FRN.CODFRN = C.CODFRN -- Saldo
                                    INNER JOIN MRT.T0109059 F ON F.TIPDSNDSCBNF = C.TIPDSNDSCBNF         -- empenhos  
                                    LEFT JOIN (SELECT D.CODFRN, COALESCE(TRUNC(SUM(D.VLRSLDRSVFRN), 2), 0)   AS  VLRSLDRSVFRN                      
                                                FROM MRT.T0123159 D 
                                                WHERE D.FLGAPVACOCMC = 'N'
                                                    AND D.TIPDSNDSCBNF = :TIPDSNDSCBNF
                                                GROUP BY D.CODFRN) RES ON RES.CODFRN = FRN.CODFRN   -- SALDO RESERVADO
                                    LEFT JOIN (SELECT  A.CODFRN
                                                        , TRUNC(COALESCE(SUM(                                                
                                                        CASE WHEN A.CODSITPMS = 0 OR A.CODSITPMS = 1 THEN                 
                                                                (B.VLRNGCPMS) - (B.VLRPGOPMS) - ((B.VLRNGCPMS - B.VLRPGOPMS) * (J.PERJURSLDNEG + J.PERPIS) / 100)                          
                                                        ELSE                                                              
                                                                (B.VLREFTPMS) - (B.VLRPGOPMS) - ((B.VLREFTPMS - B.VLRPGOPMS) * (J.PERJURSLDNEG + J.PERPIS) / 100)                
                                                        END),0),2) AS VLRPMS                                       
                                                    FROM MRT.T0118058 A
                                                    INNER JOIN MRT.T0118066 B ON B.CODEMP = A.CODEMP AND B.CODFILEMP = A.CODFILEMP AND B.CODPMS = A.CODPMS AND B.DATNGCPMS = A.DATNGCPMS       
                                                    INNER JOIN MRT.T0118074 J ON J.CODFILEMP = 1     
                                                    INNER JOIN MRT.T0109059 X on B.TIPDSNDSCBNF = X.TIPDSNDSCBNF
                                                WHERE (A.INDASCARDFRNPMS IS NULL OR A.INDASCARDFRNPMS = 0)    
                                                AND A.CODSITPMS IN (0, 1, 3)                         
                                                AND B.TIPDSNDSCBNF =  :TIPDSNDSCBNF
                                                AND X.FLGCTBDSNDSC = 'S'
                                                GROUP BY A.CODFRN) PMS ON PMS.CODFRN = FRN.CODFRN
                                    WHERE  F.FLGCTBDSNDSC = 'S' 
                                    AND C.TIPDSNDSCBNF = :TIPDSNDSCBNF
                                    AND C.ANOMESREF = :ANOMESREF

                    ) TMP
                     INNER JOIN MRT.T0100426 FRN on FRN.CODFRN = TMP.CODFRN          -- FORNECEDOR
                      LEFT JOIN MRT.T0118570 DIV on DIV.CODGERPRD = FRN.CODGERPRD    -- CELULA
                      INNER JOIN MRT.T0123183 DIR ON DIR.CODDRTCMP = DIV.CODDRTCMP   -- DIRETORIA
                    WHERE (:CODFRN IS NULL OR FRN.CODFRN = :CODFRN)                  -- FORNECEDOR
                      AND FRN.CODFRN IN(SELECT CPR.CODFRN FROM MRT.T0153541 CPR WHERE (:CODCPR IS NULL OR CPR.CODCPR = :CODCPR))  -- COMPRADOR
                      AND (:CODDIVCMP IS NULL OR DIV.CODDIVCMP = :CODDIVCMP)         -- CELULA 
                      AND (:CODDRTCMP IS NULL OR DIR.CODDRTCMP = :CODDRTCMP)         -- DIRETORIA                   
                     ------ AND (TMP.DT_OCORRENCIA BETWEEN :DATAINI AND :DATAFIM)    -- DATA DA OCORRENCIA

                 ORDER BY  DIV.CODDIVCMP, DIR.CODDRTCMP, TMP.CODFRN, TMP.ORDEM, TMP.DT_OCORRENCIA, TMP.SUB_ORDEM

                            ]]>.Value


        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(dadosFiltro.CODFRN))
        parameterMapper.Parameters.Add(":CODCPR", ValueDBProperty(dadosFiltro.CODCPR))
        parameterMapper.Parameters.Add(":TIPDSNDSCBNF", ValueDBProperty(dadosFiltro.TIPDSNDSCBNF))
        parameterMapper.Parameters.Add(":CODDIVCMP", ValueDBProperty(dadosFiltro.CODDIVCMP))
        parameterMapper.Parameters.Add(":CODDRTCMP", ValueDBProperty(dadosFiltro.CODDRTCMP))
        parameterMapper.Parameters.Add(":DATAINI", ValueDBProperty(dadosFiltro.DataIni))
        parameterMapper.Parameters.Add(":DATAFIM", ValueDBProperty(dadosFiltro.DataFim))
        parameterMapper.Parameters.Add(":ANOMESREF", ValueDBProperty(DateTime.Now().ToString("yyyyMM")))
        'parameterMapper.Parameters.Add(":DATAATUAL", ValueDBProperty(DateTime.Now().Date))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFRN", "NOMFRN", "CODDIVCMP", "DESDIVCMP", "CODDRTCMP", "DESDRTCMP", "VLRMOV", "VLRSLDRSV", "DT_OCORRENCIA", "IDTMOV", "TIPO_MOV"}))
    End Function

    Public Function ListaSaldoEvento(dadosFiltro As VO.ExtratoVerba) As List(Of VO.ExtratoVerba)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[
                        SELECT ACAO.CODACOCMC  
                             , ACAO.NOMACOCMC
                             , ACAO.DATAPVACOCMC
                             , ACAO.TIPEVTACOCMC
                             , ACAO.NOMTIPEVTACOCMC
                             , ACAO.CODEVTACOCMC
                             , ACAO.NOMEVTACOCMC
                             , ACAO.TIPDSNDSCBNF
                             , ACAO.DESDSNDSCBNF
                             , PFRN.VLRPTCFRNACOCMC
                             , PFRN.CODFRN
                             , FRN.NOMFRN
                             , DIV.CODDIVCMP
                             , DIV.DESDIVCMP
                             , DIR.CODDRTCMP
                             , DIR.DESDRTCMP
                          FROM MRT.T0138768 PFRN 
                          INNER JOIN MRT.T0118430 ACO ON ACO.CODACOCMC  = PFRN.CODACOCMC
                          LEFT JOIN MRT.T0100426 FRN ON FRN.CODFRN = PFRN.CODFRN           -- FORNECEDOR
                          LEFT JOIN MRT.T0118570 DIV ON DIV.CODGERPRD = FRN.CODGERPRD      -- CELULA
                          INNER JOIN MRT.T0123183 DIR ON DIR.CODDRTCMP = DIV.CODDRTCMP     -- DIRETORIA
                         INNER JOIN(SELECT A.CODACOCMC    
                                         , A.DATAPVACOCMC
                                         , A.NOMACOCMC
                                         , C.TIPEVTACOCMC
                                         , C.NOMTIPEVTACOCMC
                                         , D.CODEVTACOCMC
                                         , D.NOMEVTACOCMC
                                         , F.TIPDSNDSCBNF
                                         , F.DESDSNDSCBNF
                                     FROM MRT.T0118430 A         -- AÇÂO                          
                                    INNER JOIN MRT.T0155581 B ON B.CODACOCMC = A.CODACOCMC        -- PLANO DE MARKETING DA AÇÃO        
                                     LEFT JOIN MRT.T0155522 C ON C.TIPEVTACOCMC = B.TIPEVTACOCMC  -- TIPO DE EVENTO    
                                     LEFT JOIN MRT.T0155531 D ON D.CODEVTACOCMC = B.CODEVTACOCMC  -- EVENTO
                                     LEFT JOIN MRT.T0131275 E ON E.CODUNDESRNGC= B.CODUNDESRNGC        
                                     LEFT JOIN MRT.T0109059 F ON D.TIPDSNDSCBNF = F.TIPDSNDSCBNF  -- EMPENHO              
                                    WHERE A.TIPACOMCD = 1   -- AÇÕES DE FORNECEDOR
                                      AND CODSTAAPVACOCMC = 5 -- ACAO APROVADA
                                    GROUP BY A.CODACOCMC  
                                        , A.NOMACOCMC
                                        , A.DATAPVACOCMC
                                        , C.TIPEVTACOCMC
                                        , C.NOMTIPEVTACOCMC
                                        , D.CODEVTACOCMC
                                        , D.NOMEVTACOCMC
                                        , F.TIPDSNDSCBNF
                                        , F.DESDSNDSCBNF
                              ) ACAO ON PFRN.CODACOCMC = ACAO.CODACOCMC
                          WHERE (:CODFRN IS NULL OR PFRN.CODFRN = :CODFRN)                          -- FORNECEDOR
                            AND PFRN.CODFRN IN(SELECT CPR.CODFRN FROM MRT.T0153541 CPR WHERE (:CODCPR IS NULL OR CPR.CODCPR = :CODCPR))  -- COMPRADOR
                            AND (:TIPEVTACOCMC IS NULL OR ACAO.TIPEVTACOCMC = :TIPEVTACOCMC)        -- TIPO EVENTO
                            AND (:CODEVTACOCMC IS NULL OR ACAO.CODEVTACOCMC = :CODEVTACOCMC)        -- EVENTO
                            AND (:TIPDSNDSCBNF IS NULL OR ACAO.TIPDSNDSCBNF = :TIPDSNDSCBNF)        -- EMPENHO
                            AND (:CODDIVCMP IS NULL OR DIV.CODDIVCMP = :CODDIVCMP)                  -- CELULA 
                            AND (:CODDRTCMP IS NULL OR DIR.CODDRTCMP = :CODDRTCMP)                  -- DIRETORIA
                            AND (ACAO.DATAPVACOCMC BETWEEN :DATAINI AND :DATAFIM)                   -- DATA DE APROVAÇÃO DA AÇÃO   
                   ]]>.Value

        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(dadosFiltro.CODFRN))
        parameterMapper.Parameters.Add(":CODCPR", ValueDBProperty(dadosFiltro.CODCPR))
        parameterMapper.Parameters.Add(":TIPEVTACOCMC", ValueDBProperty(dadosFiltro.TIPEVTACOCMC))
        parameterMapper.Parameters.Add(":CODEVTACOCMC", ValueDBProperty(dadosFiltro.CODEVTACOCMC))
        parameterMapper.Parameters.Add(":TIPDSNDSCBNF", ValueDBProperty(dadosFiltro.TIPDSNDSCBNF))
        parameterMapper.Parameters.Add(":CODDIVCMP", ValueDBProperty(dadosFiltro.CODDIVCMP))
        parameterMapper.Parameters.Add(":CODDRTCMP", ValueDBProperty(dadosFiltro.CODDRTCMP))
        parameterMapper.Parameters.Add(":DATAINI", ValueDBProperty(dadosFiltro.DataIni))
        parameterMapper.Parameters.Add(":DATAFIM", ValueDBProperty(dadosFiltro.DataFim))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NOMACOCMC", "DATAPVACOCMC", "TIPEVTACOCMC", "NOMTIPEVTACOCMC", "CODEVTACOCMC", "NOMEVTACOCMC", "TIPDSNDSCBNF", "DESDSNDSCBNF", "VLRPTCFRNACOCMC", "CODFRN", "NOMFRN", "CODDIVCMP", "DESDIVCMP", "CODDRTCMP", "DESDRTCMP"}))

    End Function

End Class


<Table("MRT.T0118430")>
Public Class ExtratoVerbaDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Decimal?


End Class