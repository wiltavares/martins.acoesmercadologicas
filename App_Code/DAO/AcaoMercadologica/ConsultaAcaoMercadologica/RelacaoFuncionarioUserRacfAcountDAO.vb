﻿Imports Core
Public Class RelacaoFuncionarioUserRacfAcountDAO
    Inherits DAOBase(Of VO.RelacaoFuncionarioUserRacfAcount)

    Public Sub New()
        daoConfiguration = New RelacaoFuncionarioUserRacfAcountDAOConfiguration()
    End Sub

    Public Function BuscaPorUsuarioRede(vo As VO.RelacaoFuncionarioUserRacfAcount) As List(Of VO.RelacaoFuncionarioUserRacfAcount)


        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT CODFNC
                             FROM MRT.T0104596
                             WHERE UPPER(TRIM(NOMUSRRCF)) = :NOMUSRRCF ]]>.Value

        parameterMapper.Parameters.Add(":NOMUSRRCF", ValueDBProperty(vo.NOMUSRRCF.ToUpper.Trim))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function BuscaPorCodFnc(vo As VO.RelacaoFuncionarioUserRacfAcount) As VO.RelacaoFuncionarioUserRacfAcount


        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT NOMUSRRCF
                             FROM MRT.T0104596
                             WHERE CODFNC = :CODFNC ]]>.Value

        parameterMapper.Parameters.Add(":CODFNC", ValueDBProperty(vo.CODFNC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql)).FirstOrDefault
    End Function

End Class


<TableAttribute("MRT.T0104596")>
Public Class RelacaoFuncionarioUserRacfAcountDAOConfiguration
    Inherits DAOConfiguration

    Property CODFNC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NOMUSRRCF As String

End Class
