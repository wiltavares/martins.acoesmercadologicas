﻿Imports Core
Public Class RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemDAO
    Inherits DAOBase(Of VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem)

    Public Sub New()
        daoConfiguration = New RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemDAOConfiguration()
    End Sub

#Region "Estorno Manual"
    ''' <summary>
    ''' FunIsrLogEtrMan
    ''' </summary>
    ''' <param name="item"></param>
    ''' <param name="codFrn"></param>
    ''' <returns></returns>
    Public Function InsereLogEstornoManual(item As VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem, codFrn As Decimal) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[ INSERT INTO MRT.T0158718 
                                                ( CODACOCMC
                                                , NUMLNHITEOCDACOCMC
                                                , NUMSEQUTZITEOCD
                                                , CODUTZITEOCDACOCMC
                                                , DESUTZITEOCDACOCMC
                                                , QDEUTZITEOCDACOCMC
                                                , VLRUTZITEOCDACOCMC
                                                , CODFNC
                                                , DATGRCRLC
                                                , INDTIPISRUTZITEOCD
                                                ) 
                                           SELECT :CODACOCMC
                                                , :NUMLNHITEOCDACOCMC
                                                ,  NVL(MAX(NUMSEQUTZITEOCD) + 1, 1)
                                                , :CODFRN
                                                , :DESUTZITEOCDACOCMC
                                                , :QDEUTZITEOCDACOCMC
                                                , :VLRUTZITEOCDACOCMC
                                                , :CODFNC
                                                , TRUNC(SYSDATE)
                                                , :INDTIPISRUTZITEOCD
                                             FROM MRT.T0158718 
                                            WHERE CODACOCMC = :CODACOCMC 
                                              AND NUMLNHITEOCDACOCMC = :NUMLNHITEOCDACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", item.CODACOCMC)
        parameterMapper.Parameters.Add(":NUMLNHITEOCDACOCMC", item.NUMLNHITEOCDACOCMC)
        parameterMapper.Parameters.Add(":CODFRN", codFrn)
        parameterMapper.Parameters.Add(":DESUTZITEOCDACOCMC", item.DESUTZITEOCDACOCMC)
        parameterMapper.Parameters.Add(":QDEUTZITEOCDACOCMC", item.QDEUTZITEOCDACOCMC)
        parameterMapper.Parameters.Add(":VLRUTZITEOCDACOCMC", item.VLRUTZITEOCDACOCMC)
        parameterMapper.Parameters.Add(":CODFNC", item.CODFNC)
        parameterMapper.Parameters.Add(":INDTIPISRUTZITEOCD", item.INDTIPISRUTZITEOCD)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    ''' <summary>
    ''' FunCnsExpCod
    ''' </summary>
    ''' <param name="codItem"></param>
    ''' <param name="codOpeAcoCmc"></param>
    ''' <returns></returns>
    Public Function FunCnsExpCod(codItem As Integer, codOpeAcoCmc As Integer) As List(Of VO.RelacaoAcaoComercialPorItemOrcamento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim atributo = ""

        Select Case codOpeAcoCmc
            Case 7
                atributo = "attribute1"
            Case 17
                atributo = "attribute2"
            Case 6
                atributo = "attribute3"
        End Select


        Dim sql As String = <![CDATA[SELECT TYPE_EXPENSE_CODE 
                                       FROM MTS.MTS_RT_TYPE_EXPENSES_ALL 
                                      WHERE :ATRIBUTO = 'AC' 
                                        AND type_expense_id = :CODITEM]]>.Value

        parameterMapper.Parameters.Add(":CODITEM", codItem)
        parameterMapper.Parameters.Add(":ATRIBUTO", atributo)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' FunAtuAct
    ''' </summary>
    ''' <param name="codAcoCmc"></param>
    ''' <param name="codOpeAcoCmc"></param>
    ''' <param name="codTipExpCod"></param>
    ''' <param name="vlrEstorno"></param>
    ''' <returns></returns>
    Public Function FunAtuAct(CODACOCMC As Decimal, CODOPEACOCMC As Integer, CODTIPEXPCOD As String, VLRESTORNO As Double) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim destination_type = ""

        Select Case CODOPEACOCMC
            Case 7
                destination_type = "CPD"
            Case 17
                destination_type = "SP"
            Case 6
                destination_type = "RE"
        End Select

        Dim sql As String = <![CDATA[UPDATE mts.mts_rt_orc_budge_actions_all 
                                        SET BUDGE_AMOUNT_FORECASTED = BUDGE_AMOUNT_FORECASTED - :VLRESTORNO
                                     WHERE ACTIONS_DESTINATION_TYPE = :DESTINATION_TYPE
                                       AND actions_number = :CODACOCMC 
                                       AND TYPE_EXPENSE_CODE = :CODTIPEXPCOD]]>.Value

        parameterMapper.Parameters.Add(":VLRESTORNO", VLRESTORNO)
        parameterMapper.Parameters.Add(":DESTINATION_TYPE", destination_type)
        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)
        parameterMapper.Parameters.Add(":CODTIPEXPCOD", CODTIPEXPCOD)

        Return connector.ExecuteScalar(sql, parameterMapper, True)
    End Function

#End Region

    ''' <summary>
    ''' função para modal de consulta gasto
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function consultaGastoItem(codAcao As Decimal, linha As Decimal) As List(Of VO.RelacaoItemOrcamentoAcaoComercialxUtilizacaoItem)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC,                           
	                                        A.NUMLNHITEOCDACOCMC,                      
	                                        A.NUMSEQUTZITEOCD,                         
	                                        A.CODUTZITEOCDACOCMC,                      
	                                        A.DESUTZITEOCDACOCMC,                      
	                                        A.QDEUTZITEOCDACOCMC,                      
	                                        A.VLRUTZITEOCDACOCMC,             
	                                        A.DATGRCRLC,                               
	                                        A.INDTIPISRUTZITEOCD,
                                            COALESCE(B.CODFNC, CADT.CODANSTCR) AS CODFNC,                              
	                                        COALESCE(B.NOMFNC, CADT.NOMANSTCR) AS NOMFNC
                                     FROM MRT.T0158718 a                         
                                       LEFT JOIN MRT.T0100361 B ON A.CODFNC = B.CODFNC
                                       LEFT JOIN MRT.CADANSINFTCR CADT ON A.CODFNC = CADT.CODANSTCR                
                                     WHERE CodAcoCmc = :CODACOCMC
	                                 AND NumLnhIteOcdAcoCmc = :NUMLNH
	                                 ORDER BY NUMSEQUTZITEOCD]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)
        parameterMapper.Parameters.Add(":NUMLNH", linha)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

End Class


<TableAttribute("MRT.T0158718")>
Public Class RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMLNHITEOCDACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMSEQUTZITEOCD As Nullable(Of Decimal)


    Property VLRUTZITEOCDACOCMC As Nullable(Of Decimal)
    Property QDEUTZITEOCDACOCMC As Nullable(Of Decimal)
    Property INDTIPISRUTZITEOCD As Nullable(Of Decimal)
    Property DESUTZITEOCDACOCMC As String
    Property DATGRCRLC As Nullable(Of Date)
    Property CODUTZITEOCDACOCMC As Nullable(Of Decimal)
    Property CODFNC As Nullable(Of Decimal)



    Property NOMFNC As String

End Class
