﻿Imports Core
Public Class RelacaoCttxClausulaxAbrangComRequisicaoDeAlocalcaoDasVerbasDAO
    Inherits DAOBase(Of VO.RelacaoCttxClausulaxAbrangComRequisicaoDeAlocalcaoDasVerbas)

    Public Sub New()
        daoConfiguration = New RelacaoCttxClausulaxAbrangComRequisicaoDeAlocalcaoDasVerbasDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0151829")>
Public Class RelacaoCttxClausulaxAbrangComRequisicaoDeAlocalcaoDasVerbasDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property NUMCTTFRN As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMCSLCTTFRN As Nullable(Of Decimal)

    <PrimaryKey>
    Property TIPEDEABGMIX As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODEDEABGMIX As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODREQALCVBAFRN As Nullable(Of Decimal)

End Class
