﻿Imports Core
Public Class RelacaoClausulaComContratoDeFornecimentoDAO
    Inherits DAOBase(Of VO.RelacaoClausulaComContratoDeFornecimento)

    Public Sub New()
        daoConfiguration = New RelacaoClausulaComContratoDeFornecimentoDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0124961")>
Public Class RelacaoClausulaComContratoDeFornecimentoDAOConfiguration
    Inherits DAOConfiguration

    <DateInsert>
    <[Readonly](False, True)>
    Property DATDSTCSL As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATINIVGRCSLCTTFRN As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATREFAPUPODCSLCTT As Date

End Class
