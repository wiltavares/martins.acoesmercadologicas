﻿Imports Core
Public Class RelacaoAcaoComercialxRejeicaoDAO
    Inherits DAOBase(Of VO.RelacaoAcaoComercialxRejeicao)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoComercialxRejeicaoDAOConfiguration()
    End Sub

#Region "Fluxo"
    Public Function FunInsDdoRpvAcoCmc(ByVal dCodAcoCmc As Integer, _
                                       ByVal iStaAcoCmc As Integer, _
                                       ByVal dCodFncRpvAcoCmc As Integer, _
                                       ByVal sDesMtvRpvAcoCmc As String) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[INSERT INTO MRT.T0155557
                                               ( CODACOCMC
                                               , DATHRARPVACOCMC
                                               , CODFNCRPVACOCMC
                                               , DESMTVRPVACOCMC
                                               , CODSTAAPVACOCMC
                                               )
                                         VALUES( :CODACOCMC
                                               , systimestamp
                                               , :CODFNCRPVACOCMC
                                               , :DESMTVRPVACOCMC
                                               , :CODSTAAPVACOCMC
                                               )
                             ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", dCodAcoCmc)
        parameterMapper.Parameters.Add(":CODFNCRPVACOCMC", dCodFncRpvAcoCmc)
        parameterMapper.Parameters.Add(":DESMTVRPVACOCMC", sDesMtvRpvAcoCmc)
        parameterMapper.Parameters.Add(":CODSTAAPVACOCMC", iStaAcoCmc)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function
#End Region
End Class


<TableAttribute("MRT.T0155557")>
Public Class RelacaoAcaoComercialxRejeicaoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    <DateInsert>
    <[Readonly](False, True)>
    Property DATHRARPVACOCMC As DateTime    'TIMESTAMP(6)

    <PrimaryKey>
    Property CODFNCRPVACOCMC As Nullable(Of Decimal)

End Class
