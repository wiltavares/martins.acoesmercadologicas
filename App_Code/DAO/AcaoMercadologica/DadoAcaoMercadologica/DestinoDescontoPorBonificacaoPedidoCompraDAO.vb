﻿Imports Core
Public Class DestinoDescontoPorBonificacaoPedidoCompraDAO
    Inherits DAOBase(Of VO.DestinoDescontoPorBonificacaoPedidoCompra)

    Public Sub New()
        daoConfiguration = New DestinoDescontoPorBonificacaoPedidoCompraDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0109059")>
Public Class DestinoDescontoPorBonificacaoPedidoCompraDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property TIPDSNDSCBNF As Integer

End Class
