﻿Imports Core
Public Class AlcadasParaSistemasComFluxoAprovacaoDAO
    Inherits DAOBase(Of VO.AlcadasParaSistemasComFluxoAprovacao)

    Public Sub New()
        daoConfiguration = New AlcadasParaSistemasComFluxoAprovacaoDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.CADCTNSISFLUAPV")>
Public Class AlcadasParaSistemasComFluxoAprovacaoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODSISINF As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODTIPAPV As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODEDEAPV As Nullable(Of Decimal)

    <PrimaryKey>
    Property TIPIDTCTNFLUCMP As Nullable(Of Decimal)

End Class
