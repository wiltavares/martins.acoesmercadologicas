﻿
Imports Core
Imports VO

Public Class GrupoFornecedorDAO
    Inherits DAOBase(Of VO.GrupoFornecedor)

    Public Sub New()
        daoConfiguration = New GrupoFornecedorDAOConfiguration()
    End Sub

    Public Function ListLookup() As List(Of GrupoFornecedor)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODGRPFRN
                                          , NOMGRPFRN 
                                       FROM MRT.T0107579
                                      WHERE CODEMP = 1 
                                        AND CODFILEMP = 1
                            ]]>.Value


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPFRN", "NOMGRPFRN"}))
    End Function
End Class


<TableAttribute("MRT.T0107579")>
Public Class GrupoFornecedorDAOConfiguration
    Inherits DAOConfiguration



End Class
