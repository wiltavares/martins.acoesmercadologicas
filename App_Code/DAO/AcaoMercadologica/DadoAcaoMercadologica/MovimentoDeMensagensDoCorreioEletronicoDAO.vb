﻿Imports Core
Public Class MovimentoDeMensagensDoCorreioEletronicoDAO
    Inherits DAOBase(Of VO.MovimentoDeMensagensDoCorreioEletronico)

    Public Sub New()
        daoConfiguration = New MovimentoDeMensagensDoCorreioEletronicoDAOConfiguration()
    End Sub

    Public Function CnsPrxNumSeqCreEtn(ByVal TIPMSGCREETN As Integer) As Decimal?

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = "SELECT COALESCE(MAX(NUMSEQMSGCREETN) + 1,0) AS NUMSEQMSGCREETN FROM MRT.T0134274 WHERE TIPMSGCREETN =" + TIPMSGCREETN.ToString

        Return connector.Execute(sql)
    End Function

    Public Function IsrMsgCreEtn(ByVal TipMsgCreEtn As Decimal, _
                                 ByVal NumSeqMsgCreEtn As Decimal, _
                                 ByVal DesAssCreEtn As String) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.T0134274  (TIPMSGCREETN,           
                                                                NUMSEQMSGCREETN,           
                                                                DESASSCREETN)
                                                         VALUES(:TIPMSGCREETN,
                                                                :NUMSEQMSGCREETN,
                                                                :DESASSCREETN)]]>.Value

        parameterMapper.Parameters.Add(":TIPMSGCREETN", TipMsgCreEtn)
        parameterMapper.Parameters.Add(":NUMSEQMSGCREETN", NumSeqMsgCreEtn)
        parameterMapper.Parameters.Add(":DESASSCREETN", DesAssCreEtn)

        Dim retorno = connector.Execute(sql, parameterMapper)
        Dim ret = If(CInt(retorno) = 0, True, False)

        Return ret

    End Function



End Class


<TableAttribute("MRT.T0134274")>
Public Class MovimentoDeMensagensDoCorreioEletronicoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property TIPMSGCREETN As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMSEQMSGCREETN As Nullable(Of Decimal)

    <DateInsert>
    <[Readonly](False, True)>
    Property DATENVMSGCREETN As Date




End Class
