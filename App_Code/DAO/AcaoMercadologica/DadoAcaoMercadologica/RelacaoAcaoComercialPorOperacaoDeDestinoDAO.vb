﻿Imports Core
Public Class RelacaoAcaoComercialPorOperacaoDeDestinoDAO
    Inherits DAOBase(Of VO.RelacaoAcaoComercialPorOperacaoDeDestino)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoComercialPorOperacaoDeDestinoDAOConfiguration()
    End Sub


    Public Function BuscaQtd(ByVal iCodAcoCmc As Integer, ByVal CodOpeFsc As Integer) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[  SELECT COUNT(1) AS CONT 
                                        FROM MRT.T0138008
                                        WHERE CODOPEFSCDSNACOCMC = :CODOPEFSCDSNACOCMC
                                        AND CODACOCMC = :CODACOCMC ]]>.Value

        parameterMapper.Parameters.Add(":CODOPEFSCDSNACOCMC", ValueDBProperty(CodOpeFsc))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(iCodAcoCmc))

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function
    Public Function Insert(ByVal item As VO.RelacaoAcaoComercialPorOperacaoDeDestino) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.T0138008( CODACOCMC
                                                             , CODOPEFSCDSNACOCMC
                                                             , VLROPEFSCDSNACOCMC
                                                             , QDEITEOPEDSNACOCMC
                                                             , VLRUTZOPEDSNACOCMC
                                                             , CODUNDESRNGCOPEDSN
                                                             , QDEUTZOPEDSNACOCMC
                                                             )
                                                       VALUES( :CODACOCMC
                                                             , :CODOPEFSCDSNACOCMC
                                                             , :VLROPEFSCDSNACOCMC 
                                                             ,:QDEITEOPEDSNACOCMC
                                                             , 0.00
                                                             , :CODUNDESRNGCOPEDSN
                                                             , 0)]]>.Value


        parameterMapper.Parameters.Add(":CODACOCMC", item.CODACOCMC)
        parameterMapper.Parameters.Add(":CODOPEFSCDSNACOCMC", item.CODOPEFSCDSNACOCMC)
        parameterMapper.Parameters.Add(":VLROPEFSCDSNACOCMC", item.VLROPEFSCDSNACOCMC)
        parameterMapper.Parameters.Add(":QDEITEOPEDSNACOCMC", item.QDEITEOPEDSNACOCMC)
        parameterMapper.Parameters.Add(":CODUNDESRNGCOPEDSN", item.CODUNDESRNGCOPEDSN)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function
    Public Function Update(ByVal item As VO.RelacaoAcaoComercialPorOperacaoDeDestino) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0138008 
                                        SET VLROPEFSCDSNACOCMC = VLROPEFSCDSNACOCMC + :VLROPEFSCDSNACOCMC
                                          , QDEITEOPEDSNACOCMC = QDEITEOPEDSNACOCMC + :QDEITEOPEDSNACOCMC
                                      WHERE CODACOCMC = :CODACOCMC
                                        AND CODOPEFSCDSNACOCMC = :CODOPEFSCDSNACOCMC]]>.Value


        parameterMapper.Parameters.Add(":VLROPEFSCDSNACOCMC", item.VLROPEFSCDSNACOCMC)
        parameterMapper.Parameters.Add(":QDEITEOPEDSNACOCMC", item.QDEITEOPEDSNACOCMC)
        parameterMapper.Parameters.Add(":CODACOCMC", item.CODACOCMC)
        parameterMapper.Parameters.Add(":CODOPEFSCDSNACOCMC", item.CODOPEFSCDSNACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function



    Public Sub UpdateAbastecerPromocao(CODACOCMC As Decimal, INDTIPISRMERPMC As Decimal, VLRTOTMERISRPMC As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[  UPDATE MRT.T0138008 
                                          SET VLRUTZOPEDSNACOCMC = (CASE WHEN :INDTIPISRMERPMC = 0 THEN 
                                                                         VLRUTZOPEDSNACOCMC + :VLRTOTMERISRPMC
                                                                     ELSE
                                                                         VLRUTZOPEDSNACOCMC - :VLRTOTMERISRPMC                                           
                                                                     END)
                                        WHERE CODACOCMC = :CODACOCMC
                                          AND CODOPEFSCDSNACOCMC = 2 
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)
        parameterMapper.Parameters.Add(":INDTIPISRMERPMC", INDTIPISRMERPMC)
        parameterMapper.Parameters.Add(":VLRTOTMERISRPMC", VLRTOTMERISRPMC)

        connector.ExecuteScalar(sql, parameterMapper)

    End Sub




    'Public Function FunInsDdoOpeDsnItens(ByVal iCodAcoCmc As Integer) As List(Of VO.RelacaoAcaoComercialPorOperacaoDeDestino)

    '    Dim parameterMapper As ParameterMapper = New ParameterMapper()
    '    '0 A.CodOpeFscDsnAcoCmc, 
    '    '1 SUM(A.QdeIteOcdAcoCmc) as QtdIteOpeDsn, 
    '    '2 SUM(A.QdeIteOcdAcoCmc * A.VlrUntIteOcdAcoCmc) as VlrTotOpeDsn, 
    '    '3 A.CodUndEsrNgcOpeDsn,
    '    '4 A.CodMer,
    '    '5 B.IndTipOpeReqVgm,
    '    '6 A.CodAcoCmc,
    '    '7 B.IndTipOpeSlcPgt,
    '    '8 B.IndTipOpeReqEvt
    '    Dim sql As String = <![CDATA[   SELECT 
    '                                    A.CODACOCMC,
    '                                    A.CODOPEFSCDSNACOCMC, 
    '                                    SUM(A.QdeIteOcdAcoCmc) as QDEITEOPEDSNACOCMC, 
    '                                    SUM(A.QdeIteOcdAcoCmc * A.VlrUntIteOcdAcoCmc) as VLROPEFSCDSNACOCMC, 
    '                                    A.CodUndEsrNgcOpeDsn AS CODUNDESRNGCOPEDSN
    '                                    --A.CodMer, 
    '                                    --B.IndTipOpeReqVgm, 
    '                                    --A.CodAcoCmc, 
    '                                    --B.IndTipOpeSlcPgt, 
    '                                    --B.IndTipOpeReqEvt 
    '                                 FROM MRT.T0138776 A
    '                                INNER JOIN MRT.T0155573 B ON A.CodOpeFscDsnAcoCmc = b.CodOpeFscDsnAcoCmc 
    '                                 WHERE A.CodAcoCmc =  :CODACOCMC
    '                                 GROUP BY A.CodOpeFscDsnAcoCmc, A.CodUndEsrNgcOpeDsn, A.CodMer, B.IndTipOpeReqVgm, A.CodAcoCmc, B.IndTipOpeSlcPgt, B.IndTipOpeReqEvt ]]>.Value

    '    parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(iCodAcoCmc))

    '    Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "CODOPEFSCDSNACOCMC", "QDEITEOPEDSNACOCMC", "VLROPEFSCDSNACOCMC", "CODUNDESRNGCOPEDSN"}))

    'End Function


    Public Function SelectSomaValorOperacaoEValorUtilizadoAcaoComercial(ByVal iCodAcoCmc As Integer) As VO.RelacaoAcaoComercialPorOperacaoDeDestino

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[  SELECT SUM(VLROPEFSCDSNACOCMC) As VLROPEFSCDSNACOCMC,
                                              SUM(VLRUTZOPEDSNACOCMC) AS VLRUTZOPEDSNACOCMC
                                         FROM MRT.T0138008 
                                        WHERE CODACOCMC = :CODACOCMC 
                                          AND CODOPEFSCDSNACOCMC  = 2]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(iCodAcoCmc))

        Return connector.ExecuteReader(Of VO.RelacaoAcaoComercialPorOperacaoDeDestino)(sql, parameterMapper).FirstOrDefault()

    End Function
End Class


<TableAttribute("MRT.T0138008")>
Public Class RelacaoAcaoComercialPorOperacaoDeDestinoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODOPEFSCDSNACOCMC As Nullable(Of Decimal)

End Class