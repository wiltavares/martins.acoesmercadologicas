﻿Imports Core
Public Class UnidadeEstrategicaDeNegocioDAO
    Inherits DAOBase(Of VO.UnidadeEstrategicaDeNegocio)

    Public Sub New()
        daoConfiguration = New UnidadeEstrategicaDeNegocioDAOConfiguration()
    End Sub

    Public Function BuscaUnidadeNegocioFornecedor(codFrn As Long) As List(Of VO.UnidadeEstrategicaDeNegocio)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT D.CODUNDESRNGC,
                            E.DESUNDESRNGC 
                            FROM MRT.T0100426 A,  
                            MRT.T0113625 B,  
                            MRT.T0118570 C,  
                            MRT.T0123183 D,  
                            MRT.T0131275 E    
                            WHERE A.CODFRN = :CODFRN
                             AND A.CODCPR = B.CODCPR
                             AND B.CODGERPRD = C.CODGERPRD
                             AND C.CODDRTCMP = D.CODDRTCMP
                             AND D.CODUNDESRNGC = E.CODUNDESRNGC]]>.Value

        parameterMapper.Parameters.Add(":CODFRN", codFrn)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


    Public Function BuscaTodosCodNomUnidadeNegocio() As List(Of VO.UnidadeEstrategicaDeNegocio)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODUNDESRNGC,
                             DESUNDESRNGC
        From MRT.T0131275]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

End Class


<TableAttribute("MRT.T0131275")>
Public Class UnidadeEstrategicaDeNegocioDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODUNDESRNGC As Nullable(Of Decimal)

End Class
