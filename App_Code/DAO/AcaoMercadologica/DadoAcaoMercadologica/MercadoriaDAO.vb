﻿Imports Core
Public Class MercadoriaDAO
    Inherits DAOBase(Of VO.Mercadoria)

    Public Sub New()
        daoConfiguration = New MercadoriaDAOConfiguration()
    End Sub

    Public Function ConsultaItem(CODMER As Decimal, DESMER As String, CODFILEMP As Decimal, INDBDEOPEDSNACOCMC As Decimal, STRFRNACO As String) As List(Of VO.Mercadoria)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODMER,A.DESMER,A.CODFRNPCPMER
                            FROM MRT.T0100086 A,
                                 MRT.T0201350 B
                            WHERE]]>.Value
        If Not String.IsNullOrWhiteSpace(DESMER) Then
            sql = sql + " upper(A.DESMER) Like upper(:DESMERC)"
            Dim nomeMercLike = "%" + DESMER + "%"

            parameterMapper.Parameters.Add(":DESMERC", ValueDBProperty(nomeMercLike))
        Else
            sql = sql + " A.CODMER= :CODMER"
            parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(CODMER))
        End If
        sql = sql + " AND A.CODEMP    = 1"
        sql = sql + " AND B.CODEMP    = 1"
        sql = sql + " And B.CODFILEMP = :CODFILEMP"
        sql = sql + " And B.CODMER = A.CODMER"

        If INDBDEOPEDSNACOCMC = 1 Then
            sql = sql + " AND B.FlgMerPmc = 'S'"
            If STRFRNACO.Split(",").Length > 0 And Not String.IsNullOrWhiteSpace(STRFRNACO) And STRFRNACO.Trim <> "999999" Then
                sql = sql + " And A.CODFRNPCPMER In (:STRFRNACO)"
                parameterMapper.Parameters.Add(":STRFRNACO", ValueDBProperty(STRFRNACO))
            End If
        Else
            sql = sql + " And B.FlgMerPmc <> 'S'"
        End If

        parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(CODFILEMP))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function ConsultaItemGastoGeral(merc As Object) As List(Of VO.Mercadoria)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT TYPE_EXPENSE_ID as CODMER, TYPE_EXPENSE_CODE, DESCRIPTION as DESMER
                                FROM MTS.MTS_RT_TYPE_EXPENSES_ALL
                                WHERE ORG_ID = :IORGIDT AND ATTRIBUTE]]>.Value + merc.icodatr.ToString + "  = :SDESATR AND"

        If merc.iCodIteGstGr > 0 Then
            sql = sql + " TYPE_EXPENSE_ID = :ICODITEGSTGR"
            parameterMapper.Parameters.Add(":ICODITEGSTGR", ValueDBProperty(merc.icoditegstgr))
        Else
            merc.sdesitegstgr = "%" + UCase(merc.sdesitegstgr) + "%"
            sql = sql + " UPPER(DESCRIPTION) LIKE  :SDESITEGSTGR"
            parameterMapper.Parameters.Add(":SDESITEGSTGR", ValueDBProperty(merc.sdesitegstgr))
        End If

        parameterMapper.Parameters.Add(":IORGIDT", ValueDBProperty(merc.iorgidt))
        parameterMapper.Parameters.Add(":SDESATR", ValueDBProperty(merc.sdesatr))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql), BancoApp:=True)

    End Function

    Public Function PsqIteCsm(merc As Object) As List(Of VO.Mercadoria)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sUndNgc As String = String.Format("{0:00}", Integer.Parse(merc.CodUndNgc))


        Dim sql As String = <![CDATA[select CODMERRSU as CODMER, DESMER, IDTEMPFRN as CODFRNPCPMER, IDTITE,VLRPCO  from(SELECT  DISTINCT D.SOURCE_TYPE TipPrc, 
                                        A.ORGANIZATION_ID IDTORZ, 
                                        B.ORGANIZATION_CODE CODORZ,
                                        B.ORGANIZATION_NAME NOMORZ, 
                                        A.INVENTORY_ITEM_ID IDTITE, 
                                        A.SEGMENT1 CODMERRSU, 
                                        A.DESCRIPTION DESMER, 
                                        A.PRIMARY_UOM_CODE DESUNDVNDMERRSU,
                                        C.SEGMENT2 CODCNTCTB, 
                                        A.INVENTORY_ITEM_FLAG FLGITEETQ, 
                                        NVL( D.UNIT_PRICE, 0 ) VLRPCO, 'NI' DESAMX, 0 QDEITE, 
                                        TRUNC( SYSDATE ) + ( NVL( A.PREPROCESSING_LEAD_TIME, 0 ) + NVL(A.FULL_LEAD_TIME, 0 ) + NVL( A.POSTPROCESSING_LEAD_TIME, 0 ) ) DATCRC, 
                                        D.po_header_id IdtCtt, 
                                        D.po_line_id IdtLnhCtt, 
                                        D.document_number NumCtt, 
                                        D.line_num NumLnhCtt, 
                                        G.VENDOR_NAME RazSocEmpFrn, 
                                        F.VENDOR_ID IdtEmpFrn, 
                                        F.VENDOR_SITE_ID IdtLclEmpFrn, 
                                        F.VENDOR_SITE_CODE CodFilCgcCpf, 
                                        DECODE( F.GLOBAL_ATTRIBUTE9, '1', F.GLOBAL_ATTRIBUTE10 || F.GLOBAL_ATTRIBUTE12, SUBSTR( F.GLOBAL_ATTRIBUTE10, 2, 8 ) || F.GLOBAL_ATTRIBUTE11 || F.GLOBAL_ATTRIBUTE12 ) NumCgcEmpFrn ,
                                        J.SEGMENT1  CODGRPMERNCM 
                                        FROM    APPS.MTL_SYSTEM_ITEMS_B           A, 
                                        APPS.ORG_ORGANIZATION_DEFINITIONS B , 
                                        APPS.ICX_CAT_ITEMS_CTX_HDRS_TLP          D , 
                                        APPS.HR_ALL_ORGANIZATION_UNITS    E , 
                                        APPS.GL_CODE_COMBINATIONS         C , 
                                        APPS.PO_VENDOR_SITES_ALL          F , 
                                        APPS.PO_VENDORS                   G , 
                                        APPS.MTL_CATEGORY_SETS            H , 
                                        APPS.MTL_ITEM_CATEGORIES          I , 
                                        APPS.MTL_CATEGORIES_B             J ,
                                        APPS.PO_HEADERS_ALL               L 
                                        WHERE   A.ORGANIZATION_ID       = B.ORGANIZATION_ID 
                                        AND     B.ORGANIZATION_CODE     = :CODFILEMP 
                                        AND     A.EXPENSE_ACCOUNT       = C.CODE_COMBINATION_ID 
                                        AND     A.INVENTORY_ITEM_ID     = D.INVENTORY_ITEM_ID 
                                        AND     D.SOURCE_TYPE            = 'BLANKET' 
                                        AND     D.PO_HEADER_ID = L.PO_HEADER_ID 
                                        AND     NVL(L.END_DATE,SYSDATE+1) >=SYSDATE 
                                        AND     A.ORGANIZATION_ID       = E.ORGANIZATION_ID 
                                        AND A.ORGANIZATION_ID   = I.ORGANIZATION_ID 
                                        AND A.INVENTORY_ITEM_ID = I.INVENTORY_ITEM_ID 
                                        AND I.CATEGORY_SET_ID   = H.CATEGORY_SET_ID 
                                        AND I.CATEGORY_ID       = J.CATEGORY_ID 
                                        AND H.STRUCTURE_ID      = J.STRUCTURE_ID 
                                        AND B.operating_unit    = D.org_id
                                        AND F.org_id            = D.org_id
                                        AND H.CATEGORY_SET_NAME = 'FISCAL_CLASSIFICATION']]>.Value




        If merc.CodFilEmp <> "CAD" Then
            sql = sql + " AND     E.ATTRIBUTE4            = :SUNDNGC "
            parameterMapper.Parameters.Add(":SUNDNGC", ValueDBProperty(sUndNgc))
        End If
        If Not IsNothing(merc.CodMerRsu) And merc.CodMerRsu > 0 Then
            sql = sql + " AND A.SEGMENT1 = :CODMERRSU"
            parameterMapper.Parameters.Add(":CODMERRSU", ValueDBProperty(merc.CodMerRsu))
        End If
        If Not IsNothing(merc.DesMer) And Len(merc.DesMer) > 0 Then
            merc.DesMer = "%" + merc.DesMer + "%"
            sql = sql + " AND UPPER(A.DESCRIPTION) LIKE UPPER(:DESMER)"
            parameterMapper.Parameters.Add(":DESMER", ValueDBProperty(merc.DesMer))

        End If

        sql = sql + " AND     D.ORG_ID = :CODUNDOPE"
        parameterMapper.Parameters.Add(":CODUNDOPE", ValueDBProperty(merc.CodUndOpe))
        sql = sql + " AND     D.SUPPLIER_SITE_ID      = F.VENDOR_SITE_ID "
        sql = sql + " AND     F.VENDOR_ID             = G.VENDOR_ID "
        sql = sql + " AND     D.po_header_id           > -2 "
        sql = sql + " AND     EXISTS ( SELECT NULL"
        sql = sql + "                    FROM APPS.ICX_CAT_ITEMS_CTX_HDRS_TLP          T5 "
        sql = sql + "                       , APPS.MTS_PO_ORGANIZATIONS_BLANKET T4 "
        sql = sql + "                   WHERE T5.po_header_id    = D.po_header_id "
        sql = sql + "                     AND T5.ORG_ID          = D.ORG_ID "
        sql = sql + "                     AND T4.ORGANIZATION_ID = A.ORGANIZATION_ID "
        sql = sql + "                     AND T4.PO_HEADER_ID    = D.po_header_id "
        sql = sql + "                     AND T4.ATTRIBUTED_FLAG = 'Y') "

        sql = sql + "         UNION ALL "

        sql = sql + " SELECT  DISTINCT D.SOURCE_TYPE TipPrc, "
        sql = sql + "         A.ORGANIZATION_ID IDTORZ, "
        sql = sql + "         B.ORGANIZATION_CODE CODORZ, "
        sql = sql + "         B.ORGANIZATION_NAME NOMORZ, "
        sql = sql + "         A.INVENTORY_ITEM_ID IDTITE, "
        sql = sql + "         A.SEGMENT1 CODMERRSU, "
        sql = sql + "         A.DESCRIPTION DESMER, "
        sql = sql + "         A.PRIMARY_UOM_CODE DESUNDVNDMERRSU, "
        sql = sql + "         C.SEGMENT2 CODCNTCTB, "
        sql = sql + "         A.INVENTORY_ITEM_FLAG FLGITEETQ, "
        sql = sql + "         NVL( D.UNIT_PRICE, 0 ) VLRPCO, "
        sql = sql + "         'NI' DESAMX, "
        sql = sql + "         0 QDEITE, "
        sql = sql + "         TRUNC( SYSDATE ) + ( NVL( A.PREPROCESSING_LEAD_TIME, 0 ) + NVL(A.FULL_LEAD_TIME, 0 ) + NVL( A.POSTPROCESSING_LEAD_TIME, 0 ) ) DATCRC, "
        sql = sql + "         D.supplier_id IdtCtt, "
        sql = sql + "         D.po_line_id IdtLnhCtt, "
        sql = sql + "         D.document_number NumCtt, "
        sql = sql + "         D.line_num NumLnhCtt, "
        sql = sql + "         '-', "
        sql = sql + "         NULL, "
        sql = sql + "         NULL, "
        sql = sql + "         NULL, "
        sql = sql + "         NULL , "
        sql = sql + "         H.SEGMENT1  CODGRPMERNCM"
        sql = sql + " FROM    APPS.MTL_SYSTEM_ITEMS_B           A, "
        sql = sql + "         APPS.ORG_ORGANIZATION_DEFINITIONS B, "
        sql = sql + "         APPS.ICX_CAT_ITEMS_CTX_HDRS_TLP          D, "
        sql = sql + "         APPS.HR_ALL_ORGANIZATION_UNITS    E, "
        sql = sql + "         APPS.GL_CODE_COMBINATIONS C ,"
        sql = sql + "         APPS.MTL_CATEGORY_SETS            F, "
        sql = sql + "         APPS.MTL_ITEM_CATEGORIES          G, "
        sql = sql + "         APPS.MTL_CATEGORIES_B             H "
        sql = sql + " WHERE   A.ORGANIZATION_ID       = B.ORGANIZATION_ID "
        sql = sql + " AND     B.ORGANIZATION_CODE     = :CODFILEMP"
        sql = sql + " AND     A.EXPENSE_ACCOUNT       = C.CODE_COMBINATION_ID "
        sql = sql + " AND     A.INVENTORY_ITEM_ID     = D.INVENTORY_ITEM_ID "
        sql = sql + " AND     D.SOURCE_TYPE            in ( 'PURCHASING_ITEM','MASTER_ITEM') "
        sql = sql + " AND     A.ORGANIZATION_ID       = E.ORGANIZATION_ID "
        sql = sql + " AND  A.ORGANIZATION_ID   = G.ORGANIZATION_ID"
        sql = sql + " AND A.INVENTORY_ITEM_ID = G.INVENTORY_ITEM_ID"
        sql = sql + " AND G.CATEGORY_SET_ID   = F.CATEGORY_SET_ID"
        sql = sql + " AND G.CATEGORY_ID       = H.CATEGORY_ID"
        sql = sql + " AND F.STRUCTURE_ID      = H.STRUCTURE_ID"
        sql = sql + " AND F.CATEGORY_SET_NAME = 'FISCAL_CLASSIFICATION'"
        sql = sql + " AND B.operating_unit    = d.org_id "

        If merc.CodFilEmp <> "CAD" Then
            sql = sql + " AND     E.ATTRIBUTE4= :SUNDNGC"
        End If
        If Not IsNothing(merc.CodMerRsu) And merc.CodMerRsu > 0 Then
            sql = sql + " AND A.SEGMENT1 = :CODMERRSU"
        End If
        If Not IsNothing(merc.DesMer) And Len(merc.DesMer) > 0 Then
            sql = sql + " AND UPPER(A.DESCRIPTION) LIKE UPPER(:DESMER)"
        End If

        sql = sql + "         AND     D.ORG_ID                = :CODUNDOPE"
        sql = sql + "         AND     D.po_line_id           = -2  "
        sql = sql + "         AND     NOT EXISTS ( SELECT NULL "
        sql = sql + "                                FROM APPS.ICX_CAT_ITEMS_CTX_HDRS_TLP   T5 "
        sql = sql + "                                   , APPS.MTS_PO_ORGANIZATIONS_BLANKET T4 "
        sql = sql + "                               WHERE T5.po_header_id = D.po_header_id "
        sql = sql + "                                 AND T5.ORG_ID          = D.ORG_ID "
        sql = sql + "                                 AND T4.ORGANIZATION_ID = A.ORGANIZATION_ID "
        sql = sql + "                                 AND T4.PO_HEADER_ID    = D.po_header_id "
        sql = sql + "                                 AND T4.ATTRIBUTED_FLAG = 'Y') "
        sql = sql + "                            ORDER BY 1, 2, 6) "


        parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(merc.CodFilEmp))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODFRNPCPMER", "IDTITE", "VLRPCO"}), BancoApp:=True)


    End Function

    Public Function VerificaProdutoVendidoFracionado(ByVal merc As Decimal, ByVal filial As Decimal) As List(Of VO.Mercadoria)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[select distinct a.codmer
                                    from mrt.t0111770 a
                                    , mrt.t0100086 b
                                    , mrt.RLCFILPCPFILCPL c
                                    where a.codmer = :CODMER
                                    and a.codfilemp = :CODFILEMP
                                    and a.codemp = 1
                                    and b.codmer = a.codmer
                                    and b.codemp = 1
                                    and b.QDEUNDVNDCXAFRN > 1
                                    and b.QDEMNMVNDMER < b.QDEUNDVNDCXAFRN
                                    and c.codfilempepdcpl = :CODFILEMP
                                    and c.DATDSTRLC is null]]>.Value

        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(merc))
        parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(filial))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function
    Public Function BuscaItensOrcamento(vo As VO.Mercadoria) As List(Of VO.Mercadoria)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODMER
                                          , A.DESMER
                                          , A.CODFRNPCPMER
                                       FROM MRT.T0100086 A
                                      WHERE CODEMP = 1  
                                        AND DATDSTMER IS NULL 
                                        AND 
                             ]]>.Value
        If Not String.IsNullOrWhiteSpace(vo.DESMER) Then
            sql = sql + " upper(A.DESMER) Like upper(:DESMERC)"
            Dim nomeMercLike = "%" + vo.DESMER + "%"
            parameterMapper.Parameters.Add(":DESMERC", ValueDBProperty(nomeMercLike))
        End If

        If (vo.CODMER > 0) Then
            sql = sql + " A.CODMER= :CODMER"
            parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(vo.CODMER))
        End If

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function BuscaMercadorias(vo As VO.Mercadoria) As List(Of VO.Mercadoria)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                              CODMER 
                                            , CODEMP 
                                            , DESMER 
                                            , DATDSTMER 
                                            , QDEPNDPED
                                            , DESUNDVNDMER 
                                            , CODSITMER 
                                            , FLGDSTMER 
                                            , CODSBCMER 
                                            , PESUNTMER 
                                            , CODTBTICMMER 
                                            , PERTBTIPIMER 
                                            , DESCPLMER 
                                            , FLGMERCONGOV 
                                            , CODVARNVGMER 
                                            , DATCADMER
                                     FROM MRT.T0100086  
                                     WHERE CODMER = :CODMER 
                                       AND CODEMP = :CODEMP
                                       AND DATDSTMER IS NULL]]>.Value

        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(vo.CODMER))
        parameterMapper.Parameters.Add(":CODEMP", ValueDBProperty(vo.CODEMP))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


    Public Function ListaItensSubstitutos(CODACOCMC As Decimal, CODMER As Decimal, NUMLNHITEOCDACOCMC As Decimal) As List(Of VO.Mercadoria)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[select mer.CODMER
                                        , mer.CODEMP
                                        , mer.DESMER
                                        , mer.DESUNDVNDMER
                                        , mer.CODSITMER
                                        , mer.FLGDSTMER
                                        , mer.CODSBCMER
                                        , mer.PESUNTMER
                                        , mer.CODTBTICMMER
                                        , mer.PERTBTIPIMER
                                        , mer.DESCPLMER
                                        , mer.FLGMERCONGOV
                                        , mer.CODVARNVGMER
                                        , mer.DATCADMER
                                        , mer.DATDSTMER
                                        , mer.CODACRCSTMER
                                        , mer.FLGOPCCTS
                                        , mer.FLGOPCCMP
                                        , mer.FLGGARAUTETQ
                                        , mer.NUMFOLLIVPCO
                                        , mer.PSOUNTMER
                                        , mer.QDECXABSEPLT
                                        , mer.QDECXAARAPLT
                                        , mer.QDEUNDVNDCXAFRN
                                        , mer.TIPMAP
                                        , mer.FLGOPCDIVMAP
                                        , mer.CODNRMPLTMER
                                        , mer.VOLUNDVNDMER
                                        , mer.TIPPDCMER
                                        , mer.CODCLSMOVMER
                                        , mer.FLGMERRNG
                                        , mer.QDEDIAATZMEDRCBMER
                                        , mer.FLGMERTBTSFR
                                        , mer.CODCPRPCPMER
                                        , mer.TIPMER
                                        , mer.CODCLFIPIMER
                                        , mer.CODFLXPCO
                                        , mer.TIPRTCFRTMER
                                        , mer.TIPATLRTCFRTMER
                                        , mer.NUMPODVLD
                                        , mer.TIPPODVLD
                                        , mer.FLGUNDVNDMRTVOLEPD
                                        , mer.FLGUNDVNDFRNVOLEPD
                                        , mer.CODBRRUNDVNDMRT
                                        , mer.TIPCODBRRUNDVNDMRT
                                        , mer.CODBRRUNDVNDFRN
                                        , mer.TIPCODBRRUNDVNDFRN
                                        , mer.VLRCNVUNDVNDMER
                                        , mer.PERMRGMNMFLXPCO
                                        , mer.CODSMBSITMERFLXPCO
                                        , mer.FLGGRPCRGMER
                                        , mer.FTRAJTCSTDTB
                                        , mer.PERMRGRSTFATMERMTD
                                        , mer.TIPMERMTD
                                        , mer.CODTPIMERIPD
                                        , mer.CODGRPMER
                                        , mer.CODFMLMER
                                        , mer.CODCLSMER
                                        , mer.QDEPNDPED
                                        , mer.CODCTGMER
                                        , mer.CODPDAMER
                                        , mer.CODORIMER
                                        , mer.CODPORMERPSC
                                        , mer.CLFCRVABCMER
                                        , mer.POSCRVABCMER
                                        , mer.CODGRPMERNCM
                                        , mer.VLRMNMSBTTBTNAC
                                        , mer.FLGCONIDTUNDFBR
                                        , mer.CODGRPMERNCMCTB
                                        , mer.QDEDIAVLDMER
                                        , mer.CODTABFRTDIFMOVMER
                                        , mer.DESCMCMER
                                        , mer.DESCMCMERCCO
                                        , mer.INDRTNFTETBTSOC
                                        , mer.CODFRNPCPMER
                                        , mer.CODMERTCR
                                        , mer.QDEMNMVNDMER
                                        , mer.INDQDEMPLVNDMER
                                        , mer.INDMERPRL
                                        , mer.VLRCMIMER
                                        , mer.VLRARAMER
                                        , mer.VLRPFNMER
                                        , mer.INDCONMERTRPDPV
                                        , mer.INDMEROFECRZ
                                        , mer.DATALTCADMER
                                        , mer.INDEBGMRTDPVMOV
                                        , mer.INDEBGFRNDPVMOV
                                        , mer.CODGRPMERFRC
                                        , mer.CODBRRUNDVNDCSM
                                        , mer.TIPCODBRRUNDVNDCSM
                                        , mer.INDREQNUMSERMER
                                        , mer.DATRCDMER
                                        , mer.QDEISNMNRUNDINT
                                        , mer.QDEISNMERVEI
                                        , mer.PSOLIQMNRUNDINT
                                        , mer.TIPUNDMNRUNDINT
                                        , mer.CODLNHMER
                                        , mer.QDEDIARMCPCO
                                        , mer.DESMERMRT
                                        , mer.CODEBGMER
                                        , mer.TIPIDTEMPASCACOCMC
                                        , mer.CODMEREMPASCACOCMC
                                        , mer.INDMERMPB
                                        , mer.VLRMAXVNDMPB
                                        , mer.PERTBTIMPIPD
                                        , mer.DESECCTBTIMPIPD
                                        , mer.DESECCTBTIPI
                                        , mer.DESMERLNGETG
                                        , mer.INDORIMER
                                        , mer.VLRMNMIPI
                                        , mer.INDMERKIT
                                        , mer.INDMERVNDRTCKIT
                                        , mer.CODMRCMER
                                        , mer.INDVNDENM
                                        , mer.QDEUNDVNDCXAKIT
                                        , mer.QDEVOL
                                        , mer.INDORIMERFRN
                                        , mer.NUMPORPRDIFR
                                        , mer.FLGECCTBT
                                        , mer.INDCRFINUNACMEO
                                        , mer.FLGIDTDPVMVL
                                        , mer.CODEPCSBTTBT
                                        from MRT.RLCITEPCPITESBT relac
                                        inner join  MRT.T0100086 mer on mer.CODMER = relac.CODMERSBT
                                        where relac.CODACOCMC = :CODACOCMC
                                          and relac.CODMER = :CODMER
                                          and relac.NUMLNHITEOCDACOCMC = :NUMLNHITEOCDACOCMC
                           ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(CODMER))
        parameterMapper.Parameters.Add(":NUMLNHITEOCDACOCMC", ValueDBProperty(NUMLNHITEOCDACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function


    Public Function BuscaNomeMercadoria(ByVal CODMER As Decimal) As List(Of VO.Mercadoria)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       SELECT  CODMER
                                              ,DESMER
                                        FROM MRT.T0100086
                                        WHERE (CODMER = :CODMER)                                      
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(CODMER))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER"}))
    End Function

End Class


<TableAttribute("MRT.T0100086")>
Public Class MercadoriaDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODMER As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODEMP As Nullable(Of Decimal)

    <DateInsert>
    <[Readonly](False, True)>
    Property DATCADMER As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATDSTMER As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATALTCADMER As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATRCDMER As Date

    Property DESMER As String
    Property DESUNDVNDMER As String
    Property CODSITMER As Nullable(Of Decimal)
    Property FLGDSTMER As String
    Property CODSBCMER As Nullable(Of Decimal)
    Property PESUNTMER As Nullable(Of Decimal)
    Property CODTBTICMMER As Nullable(Of Decimal)
    Property PERTBTIPIMER As Nullable(Of Decimal)
    Property DESCPLMER As String
    Property FLGMERCONGOV As String
    Property CODVARNVGMER As Nullable(Of Decimal)
    Property CODACRCSTMER As Nullable(Of Decimal)
    Property FLGOPCCTS As String
    Property FLGOPCCMP As String
    Property FLGGARAUTETQ As String
    Property NUMFOLLIVPCO As Nullable(Of Decimal)
    Property PSOUNTMER As Nullable(Of Decimal)
    Property QDECXABSEPLT As Nullable(Of Decimal)
    Property QDECXAARAPLT As Nullable(Of Decimal)
    Property QDEUNDVNDCXAFRN As Nullable(Of Decimal)
    Property TIPMAP As Nullable(Of Decimal)
    Property FLGOPCDIVMAP As Nullable(Of Decimal)
    Property CODNRMPLTMER As Nullable(Of Decimal)
    Property VOLUNDVNDMER As Nullable(Of Decimal)
    Property TIPPDCMER As Nullable(Of Decimal)
    Property CODCLSMOVMER As Nullable(Of Decimal)
    Property FLGMERRNG As String
    Property QDEDIAATZMEDRCBMER As Nullable(Of Decimal)
    Property FLGMERTBTSFR As String
    Property CODCPRPCPMER As Nullable(Of Decimal)
    Property TIPMER As Nullable(Of Decimal)
    Property CODCLFIPIMER As Nullable(Of Decimal)
    Property CODFLXPCO As String
    Property TIPRTCFRTMER As String
    Property TIPATLRTCFRTMER As String
    Property NUMPODVLD As Nullable(Of Decimal)
    Property TIPPODVLD As String
    Property FLGUNDVNDMRTVOLEPD As String
    Property FLGUNDVNDFRNVOLEPD As String
    Property CODBRRUNDVNDMRT As Nullable(Of Decimal)
    Property TIPCODBRRUNDVNDMRT As String
    Property CODBRRUNDVNDFRN As Nullable(Of Decimal)
    Property TIPCODBRRUNDVNDFRN As String
    Property VLRCNVUNDVNDMER As Nullable(Of Decimal)
    Property PERMRGMNMFLXPCO As Nullable(Of Decimal)
    Property CODSMBSITMERFLXPCO As String
    Property FLGGRPCRGMER As String
    Property FTRAJTCSTDTB As Nullable(Of Decimal)
    Property PERMRGRSTFATMERMTD As Nullable(Of Decimal)
    Property TIPMERMTD As String
    Property CODTPIMERIPD As Nullable(Of Decimal)
    Property CODGRPMER As Nullable(Of Decimal)
    Property CODFMLMER As Nullable(Of Decimal)
    Property CODCLSMER As Nullable(Of Decimal)
    Property QDEPNDPED As Nullable(Of Decimal)
    Property CODCTGMER As Nullable(Of Decimal)
    Property CODPDAMER As String
    Property CODORIMER As Nullable(Of Decimal)
    Property CODPORMERPSC As Nullable(Of Decimal)
    Property CLFCRVABCMER As String
    Property POSCRVABCMER As Nullable(Of Decimal)
    Property CODGRPMERNCM As String
    Property VLRMNMSBTTBTNAC As Nullable(Of Decimal)
    Property FLGCONIDTUNDFBR As String
    Property CODGRPMERNCMCTB As String
    Property QDEDIAVLDMER As Nullable(Of Decimal)
    Property CODTABFRTDIFMOVMER As Nullable(Of Decimal)
    Property DESCMCMER As String
    Property DESCMCMERCCO As String
    Property INDRTNFTETBTSOC As Nullable(Of Decimal)
    Property CODFRNPCPMER As Nullable(Of Decimal)
    Property CODMERTCR As String
    Property QDEMNMVNDMER As Nullable(Of Decimal)
    Property INDQDEMPLVNDMER As Nullable(Of Decimal)
    Property INDMERPRL As Nullable(Of Decimal)
    Property VLRCMIMER As Nullable(Of Decimal)
    Property VLRARAMER As Nullable(Of Decimal)
    Property VLRPFNMER As Nullable(Of Decimal)
    Property INDCONMERTRPDPV As Nullable(Of Decimal)
    Property INDMEROFECRZ As Nullable(Of Decimal)
    Property INDEBGMRTDPVMOV As Nullable(Of Decimal)
    Property INDEBGFRNDPVMOV As Nullable(Of Decimal)
    Property CODGRPMERFRC As Nullable(Of Decimal)
    Property CODBRRUNDVNDCSM As Nullable(Of Decimal)
    Property TIPCODBRRUNDVNDCSM As String
    Property INDREQNUMSERMER As Nullable(Of Decimal)
    Property QDEISNMNRUNDINT As Nullable(Of Decimal)
    Property QDEISNMERVEI As Nullable(Of Decimal)
    Property PSOLIQMNRUNDINT As Nullable(Of Decimal)
    Property TIPUNDMNRUNDINT As String
    Property CODLNHMER As Nullable(Of Decimal)
    Property QDEDIARMCPCO As Nullable(Of Decimal)
    Property DESMERMRT As String
    Property CODEBGMER As Nullable(Of Decimal)
    Property TIPIDTEMPASCACOCMC As Nullable(Of Decimal)
    Property CODMEREMPASCACOCMC As Nullable(Of Decimal)
    Property INDMERMPB As Nullable(Of Decimal)
    Property VLRMAXVNDMPB As Nullable(Of Decimal)
    Property PERTBTIMPIPD As Nullable(Of Decimal)
    Property DESECCTBTIMPIPD As String
    Property DESECCTBTIPI As String
    Property DESMERLNGETG As String
    Property INDORIMER As Nullable(Of Decimal)
    Property VLRMNMIPI As Nullable(Of Decimal)
    Property INDMERKIT As Nullable(Of Decimal)
    Property INDMERVNDRTCKIT As Nullable(Of Decimal)
    Property CODMRCMER As Nullable(Of Decimal)
    Property INDVNDENM As Nullable(Of Decimal)
    Property QDEUNDVNDCXAKIT As Nullable(Of Decimal)
    Property QDEVOL As Nullable(Of Decimal)
    Property INDORIMERFRN As Nullable(Of Decimal)
    Property NUMPORPRDIFR As String
    Property FLGECCTBT As String
    Property INDCRFINUNACMEO As Nullable(Of Decimal)
    Property FLGIDTDPVMVL As String
    Property CODEPCSBTTBT As Nullable(Of Decimal)

    <IgnoreMap()>
    Property TYPE_EXPENSE_ID As Nullable(Of Decimal)
    <IgnoreMap()>
    Property TYPE_EXPENSE_CODE As String
    <IgnoreMap()>
    Property DESCRIPTION As String
    <IgnoreMap()>
    Property IDTITE As Nullable(Of Decimal)
    <IgnoreMap()>
    Property VLRPCO As Nullable(Of Decimal)

End Class
