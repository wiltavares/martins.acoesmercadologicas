﻿Imports Core
Public Class CompradorDAO
    Inherits DAOBase(Of VO.Comprador)

    Public Sub New()
        daoConfiguration = New CompradorDAOConfiguration()
    End Sub




    Public Function ListCompradoresDiretoria() As List(Of VO.Comprador)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[
                    SELECT DISTINCT GER.CODCPR
                                  , GER.NOMCPR
                               FROM MRT.T0113625 COM
                               LEFT JOIN MRT.T0113625 GER ON COM.CODGERPRD = GER.CODCPR
                           ]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODCPR", "NOMCPR", "CODFNC"}))

    End Function



End Class


<TableAttribute("MRT.T0113625")>
Public Class CompradorDAOConfiguration
    Inherits DAOConfiguration

    <DateInsert>
    <[Readonly](False, True)>
    Property DATDSTCPR As Date

End Class