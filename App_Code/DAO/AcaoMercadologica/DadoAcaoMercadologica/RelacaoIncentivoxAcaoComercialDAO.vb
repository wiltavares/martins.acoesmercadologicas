﻿Imports Core
Public Class RelacaoIncentivoxAcaoComercialDAO
    Inherits DAOBase(Of VO.RelacaoIncentivoxAcaoComercial)

    Public Sub New()
        daoConfiguration = New RelacaoIncentivoxAcaoComercialDAOConfiguration()
    End Sub

    ''' <summary>
    ''' DB_CBUAcoCmcIct.CnsIctAcoCmc
    ''' Busca incentivos da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <param name="codIct"></param>
    ''' <returns></returns>
    Public Function BuscarIncentivos(Optional codAcao As Integer? = Nothing, Optional codIct As Integer? = Nothing, Optional nomIct As String = Nothing) As List(Of VO.RelacaoIncentivoxAcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        If (Not IsNothing(nomIct)) Then
            nomIct = "%" + nomIct + "%"
        End If

        Dim sql As String = <![CDATA[SELECT A.CODICT, A.CODACOCMC, UPPER(B.DESICT) AS DESICT
                            FROM MRT.T0163177 A, MRT.T0160461 B                       
                            WHERE A.CODICT = B.CODICT 
                            AND (:CODICT IS NULL OR A.CODICT = :CODICT)
                            AND (:DESICT IS NULL OR B.DESICT like :DESICT) 
                            AND (:CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(codAcao))
        parameterMapper.Parameters.Add(":CODICT", ValueDBProperty(codIct))
        parameterMapper.Parameters.Add(":DESICT", ValueDBProperty(nomIct))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' DelIctAcoCmc
    ''' Remove associacao de incentivos de uma acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    Public Sub DeletarIncentivoDaAcao(codAcao As Integer)

        Dim filtro = New VO.RelacaoIncentivoxAcaoComercial With {.CODACOCMC = codAcao}
        DeleteT(filtro)

    End Sub

    ''' <summary>
    ''' Inserir
    ''' Remove associacao de incentivos de uma acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    Public Sub InserirIncentivosNaAcao(codAcao As Integer, incentivos As List(Of VO.RelacaoIncentivoxAcaoComercial))

        For Each incentivo As VO.RelacaoIncentivoxAcaoComercial In incentivos
            incentivo.CODACOCMC = codAcao
            InsertT(incentivo)
        Next

    End Sub

    Public Function BuscarIncentivosDaAcao(ByVal codAcao As Decimal) As List(Of VO.RelacaoIncentivoxAcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                           A.CODICT, 
                                           B.DESICT, 
                                           B.DATINIEFTICT, 
                                           B.DATFIMEFTICT,
                                           B.CODUSRCADICT, 
                                           COALESCE(C.NOMFNC, CADT.NOMANSTCR) AS NOMFNC,
                                           B.DESRDCRGRICT 
                                     FROM MRT.T0163177 A 
                                       INNER JOIN MRT.T0160461 B ON A.CODICT = B.CODICT 
                                       INNER JOIN MRT.T0100361 C ON C.CODFNC = B.CODUSRCADICT 
                                       LEFT JOIN MRT.CADANSINFTCR CADT ON B.CODUSRCADICT = CADT.CODANSTCR 
                                     WHERE A.CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function



End Class


<TableAttribute("MRT.T0163177")>
Public Class RelacaoIncentivoxAcaoComercialDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODICT As Nullable(Of Decimal)

    <IgnoreMap>
    Property DESICT As String

End Class
