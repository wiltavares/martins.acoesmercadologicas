﻿Imports Core
Public Class RelacaoAcaoComercialxFluxoAprovacaoDAO
    Inherits DAOBase(Of VO.RelacaoAcaoComercialxFluxoAprovacao)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoComercialxFluxoAprovacaoDAOConfiguration()
    End Sub

    ''' <summary>
    ''' CnsFluApv
    ''' Busca fluxos de aprovacao da acao comercial
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <returns></returns>
    Public Function BuscaFluxosAprovacaoDaAcao(codAcao As Integer) As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                           A.CODSTAAPVACOCMC, 
                                           A.NUMFLUAPV,
                                           B.NUMSEQFLUAPV,
                                           B.DATHRAFLUAPV,
                                           B.NUMSEQNIVAPV,
                                           COALESCE(C.CODFNC, CADT.CODANSTCR) AS CODFNC,
                                           COALESCE(C.NOMFNC, CADT.NOMANSTCR) AS NOMFNC,
                                           B.TIPSTAFLUAPV,
                                           CASE B.NUMSEQFLUAPV WHEN 0 THEN B.DATHRAFLUAPV
                                             ELSE B.DATHRAAPVFLU END AS DATHRAAPVFLU, 
                                           B.DESOBSAPV,
                                           B.DESMTVAPVFLUACOCMC,
                                           B.NUMSEQFLUAPVPEDOPN,
                                           COALESCE(D.CODFNC, CADTCR.CODANSTCR) AS CODFNCARZ,
                                           COALESCE(D.NOMFNC, CADTCR.NOMANSTCR) AS NOMFNCARZ
                                     FROM  MRT.RLCACOCMCFLUAPV A
                                       INNER JOIN  MRT.T0161591 B ON (A.NUMFLUAPV = B.NUMFLUAPV And B.CODSISINF = 8)
                                       LEFT JOIN MRT.T0100361 C ON (B.CODEDEAPV = C.CODFNC)
                                       LEFT JOIN MRT.CADANSINFTCR CADT ON B.CODEDEAPV = CADT.CODANSTCR
                                       LEFT Join MRT.T0100361 D ON (B.CODEDEARZ = C.CODFNC)
                                       LEFT JOIN MRT.CADANSINFTCR CADTCR ON B.CODEDEAPV = CADTCR.CODANSTCR
                                     WHERE A.CODACOCMC = :CODACOCMC 
                                     ORDER BY A.NUMFLUAPV DESC, B.NUMSEQFLUAPV]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' CnsFluApvMnh
    ''' Busca fluxos de aprovacao da acao comercial
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <param name="codFnc"></param>
    ''' <returns></returns>
    Public Function BuscaFluxosAprovacaoDaAcaoDoUsuario(codAcao As Integer, codFnc As Integer) As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODSTAAPVACOCMC,
                             A.NUMFLUAPV,
                             B.NUMSEQFLUAPV,
                             B.CODEDEAPV,
                             B.DATHRAFLUAPV,
                             B.NUMSEQNIVAPV,
                             COALESCE(C.CODFNC, CADT.CODANSTCR) AS CODFNC,
                             COALESCE(C.NOMFNC, CADT.NOMANSTCR) AS NOMFNC,
                             B.TIPSTAFLUAPV
                             FROM MRT.RLCACOCMCFLUAPV A
                             INNER JOIN MRT.T0161591 B ON
                             (A.NUMFLUAPV = B.NUMFLUAPV AND (B.TIPSTAFLUAPV = '3' OR 
                             (B.TIPSTAFLUAPV = '8' AND B.DATHRAAPVFLU IS NULL)))
                             INNER JOIN MRT.T0100361 C ON (B.CODEDEAPV = C.CODFNC AND (C.CODFNC IN
                             (SELECT DISTINCT G.CODFNCAPVNGC FROM MRT.T0153350 G 
                             WHERE G.CODFNCARZAPVNGC = :CODFNC
                             AND G.DATDST IS NULL
                             AND SYSDATE BETWEEN G.DATINI AND G.DATFIM) OR
                             C.CODFNC = :CODFNC ))
                             LEFT JOIN MRT.CADANSINFTCR CADT ON B.CODEDEAPV = CADT.CODANSTCR
                             WHERE A.CODACOCMC = :CODACOCMC
                             AND A.CODSTAAPVACOCMC = 3 
                             ORDER BY A.NUMFLUAPV DESC, B.NUMSEQFLUAPV]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)
        parameterMapper.Parameters.Add(":CODFNC", codFnc)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function IserirCabecalhoFluxo(item As VO.RelacaoAcaoComercialxFluxoAprovacao)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.RLCACOCMCFLUAPV VALUES(
                                    :CODACOCMC,
                                    :NUMFLUAPV, 
                                    :CODEDEAPV)]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", item.CODACOCMC)
        parameterMapper.Parameters.Add(":NUMFLUAPV", item.NUMFLUAPV)
        parameterMapper.Parameters.Add(":CODEDEAPV", ValueDBProperty(item.CODSTAAPVACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function AtuFlgStaFluApvCmp(ByVal CodAcoCmc As Integer, _
                                       ByVal NumFluApv As Integer, _
                                       ByVal AcoApv As Integer) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.RLCACOCMCFLUAPV
                                    SET CODSTAAPVACOCMC = :AcoApv
                                     WHERE CODACOCMC = :CodAcoCmc
                                     AND NUMFLUAPV = : NumFluApv]]>.Value


        parameterMapper.Parameters.Add(":AcoApv", AcoApv)
        parameterMapper.Parameters.Add(":NumFluApv", NumFluApv)
        parameterMapper.Parameters.Add(":CodAcoCmc", CodAcoCmc)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function BuscarMaxCodFluxo() As Decimal?

        Dim sql As String = "SELECT NVL(MAX(NUMFLUAPV), 0) + 1 as NROPRXFLUAPV FROM MRT.RLCACOCMCFLUAPV "

        Return connector.Execute(sql)

    End Function

    Public Function BuscaFluxosAprovacaoDaAcaoFluxo(codAcao As Integer) As List(Of VO.RelacaoAcaoComercialxFluxoAprovacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[ SELECT 
                                            A.CODSTAAPVACOCMC, 
                                            A.NUMFLUAPV, 
                                            B.NUMSEQFLUAPV, 
                                            B.DATHRAFLUAPV, 
                                            TO_CHAR(B.NUMSEQNIVAPV) as NUMSEQNIVAPV, 
                                            COALESCE(C.CODFNC, CADT.CODANSTCR) AS CODFNC,
                                            COALESCE(C.NOMFNC, CADT.NOMANSTCR) AS NOMFNC,
                                            B.TIPSTAFLUAPV, 
                                            B.DATHRAAPVFLU, 
                                            B.DESOBSAPV, 
                                            B.DESMTVAPVFLUACOCMC, 
                                            B.NUMSEQFLUAPVPEDOPN, 
                                            COALESCE(D.CODFNC, CADTCR.CODANSTCR) AS CODFNCARZ, 
                                            COALESCE(D.NOMFNC, CADTCR.NOMANSTCR) AS NOMFNCARZ 
                                      FROM MRT.RLCACOCMCFLUAPV A 
                                        INNER JOIN  MRT.T0161591 B ON (A.NUMFLUAPV = B.NUMFLUAPV AND B.CODSISINF = 8) 
                                        LEFT JOIN MRT.T0100361 C ON (B.CODEDEAPV = C.CODFNC) 
                                        LEFT JOIN MRT.CADANSINFTCR CADT ON B.CODEDEAPV = CADT.CODANSTCR
                                        LEFT JOIN MRT.T0100361 D ON (B.CODEDEARZ = C.CODFNC) 
                                        LEFT JOIN MRT.CADANSINFTCR CADTCR ON B.CODEDEARZ = CADTCR.CODANSTCR 
                                      WHERE A.CODACOCMC =  :CODACAO
                                        AND A.CODSTAAPVACOCMC = 5 
                                      ORDER BY A.NUMFLUAPV DESC, B.NUMSEQFLUAPV]]>.Value

        parameterMapper.Parameters.Add(":CODACAO", codAcao)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class


<TableAttribute("MRT.RLCACOCMCFLUAPV")>
Public Class RelacaoAcaoComercialxFluxoAprovacaoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMFLUAPV As Nullable(Of Decimal)

    <IgnoreMap>
    Property NUMSEQFLUAPV As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODEDEAPV As Nullable(Of Decimal)
    <IgnoreMap>
    Property DATHRAFLUAPV As Date?
    <IgnoreMap>
    Property NUMSEQNIVAPV As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODFNC As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMFNC As String
    <IgnoreMap>
    Property TIPSTAFLUAPV As String

End Class

