﻿Imports Core
Public Class MovimentoQuinzenalPrecoMercadoriaDAO
    Inherits DAOBase(Of VO.MovimentoQuinzenalPrecoMercadoriaVO)

    Public Sub New()
        daoConfiguration = New MovimentoQuinzenalPrecoMercadoriaDAOConfiguration()
    End Sub

    Public Function ObterValorTotalDeMercadoriaInseridaNaPromocao(codFilemp As Decimal, codMer As Decimal) As List(Of VO.MovimentoQuinzenalPrecoMercadoriaVO)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[WITH VGRLIVRO3 AS 
                                      (SELECT A.CODFILEMP
                                            , A.CODFILEMPFAT
                                            , A.NUMRLCCIDGIR
                                            , A.DATREFLIVPCO
                                            , MAX(A.DATREFPCO) AS DATREFPCO
                                            , A.CODMER
                                            , D.FLGMERPMC 
                                        FROM  Mrt.T0130856 A 
                                            , MRT.T0118392 B 
                                            , MRT.T0102640 C 
                                            , MRT.T0201350 D 
                                            , MRT.T0112963 E 
                                        WHERE C.CODEMP = 1 
                                          AND C.CODFILEMP= :CODFILEMP
                                          AND C.CODCID    = E.CODCIDPCOACOCMC 
                                          AND C.DATREFALTCID = B.DATREFLIVPCOVGR 
                                          AND A.CODFILEMP= :CODFILEMP
                                          AND A.CODFILEMPFAT = :CODFILEMP
                                          AND A.NUMRLCCIDGIR = C.NUMRLCCIDGIR 
                                          AND A.DATREFLIVPCO = B.DATREFLIVPCOVGR 
                                          AND B.CODEMP = 1 
                                          AND A.CODMER= :CODMER
                                          AND D.CODEMP = 1 
                                          AND D.CODFILEMP = A.CODFILEMP 
                                          AND D.CODMER = A.CODMER 
                                          AND E.CODFILEMP = A.CODFILEMP 
                                        GROUP BY A.CODFILEMP
                                               , A.CODFILEMPFAT
                                               , A.NUMRLCCIDGIR
                                               , A.DATREFLIVPCO
                                               , A.CODMER
                                               , D.FLGMERPMC
                                      ) 
                                    SELECT TO_NUMBER(TO_CHAR( ( CASE C.FLGMERPMC WHEN 'S' THEN B.VLRCSTUNTMER ELSE B.VLRCSTUNTMER / 0.78 END ) ,'9999999999999D99')) AS VLRTOTMERISRPMC 
                                      FROM VGRLIVRO3 C 
                                     INNER JOIN MRT.T0130856 B ON B.CODFILEMP = C.CODFILEMP 
                                                              AND B.CODFILEMPFAT = C.CODFILEMPFAT 
                                                              AND B.NUMRLCCIDGIR = C.NUMRLCCIDGIR 
                                                              AND B.DATREFLIVPCO = C.DATREFLIVPCO 
                                                              AND B.DATREFPCO = C.DATREFPCO 
                                                              AND B.CODMER = C.CODMER
                                 ]]>.Value

        parameterMapper.Parameters.Add(":CODMER", codMer)
        parameterMapper.Parameters.Add(":CODFILEMP", codFilemp)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"VLRTOTMERISRPMC"}))
    End Function

    ''' <summary>
    ''' busca dados com preço unitario do item selecionado
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultaPrecoUnitarioItem(codMercadoria As Integer, codFilial As Integer, ByVal tipAcao As Decimal) As List(Of VO.MovimentoQuinzenalPrecoMercadoriaVO)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = ""
        If (tipAcao = TipoAcao.PreAcordoVigencia Or tipAcao = TipoAcao.PreAcordoValor) Then
            sql = <![CDATA[SELECT  
                                   ETQ.CODMER
                                 , ETQ.CODFILEMP
                                 , ETQ.FLGMERPMC
                                 , ETQ.VLRDIRCSTMEDEFT
                                 , MK.PERMRGADIBDEPMC
                                 , CASE WHEN FLGMERPMC <> 'S' 
                                    AND MK.PERMRGADIBDEPMC <> 0
                                    AND MER.INDRTNFTETBTSOC = 0
                                   THEN ROUND(ETQ.VLRDIRCSTMEDEFT/MK.PERMRGADIBDEPMC, 4) 
                                   WHEN FLGMERPMC <> 'S' 
                                     AND MK.PERMRGADIBDEPMC <> 0
                                     AND MER.INDRTNFTETBTSOC > 0
                                   THEN ROUND( (ETQ.VLRDIRCSTMEDEFT
                                               /MK.PERMRGADIBDEPMC )
                                               - (((ETQ.VLRDIRCSTMEDEFT
                                               /MK.PERMRGADIBDEPMC ) *
                                               ( A.PERPRGITGSOC + A.PERCRBFNMSEGSOC )
                                               / 100 ))
                                               , 4) 
                                   WHEN FLGMERPMC = 'S' 
                                   THEN ROUND(ETQ.VLRDIRCSTMEDEFT, 4) 
                                   END AS VLRCSTUNTMER 
                           FROM MRT.T0201350 ETQ 
                             INNER JOIN MRT.T0101295 MK 
                                      ON MK.CODEMP = 1 
                             INNER JOIN MRT.T0100086 MER 
                                     ON MER.CODEMP = 1
                                     AND MER.CODMER = ETQ.CODMER
                             INNER JOIN MRT.T0106726 A
                                     ON A.CODEMP = 1
                                     AND A.CODFILEMP = :CODFILIAL
                             INNER JOIN MRT.T0118392 B
                                     ON B.DATREFLIVPCOVGR = A.DATREFLIVPCO
                                     AND B.CODEMP = 1
                           WHERE ETQ.CODEMP = 1
                             AND ETQ.CODFILEMP = :CODFILIAL 
                             AND ETQ.CODMER = :CODMERC ]]>.Value
        Else
            sql = <![CDATA[WITH PcoUntMer as                              
                                            (SELECT A.CODFILEMP,                           
                                                     A.CODFILEMPFAT,                       
                                                     A.NUMRLCCIDGIR,                       
                                                     A.DATREFLIVPCO,                       
                                                     Max(A.DATREFPCO) as datrefpco,        
                                                     A.CODMER ,                            
                                                     D.FLGMERPMC                           
                                               FROM MRT.T0130856 A,                          
                                                  MRT.T0118392 B,                          
                                                  MRT.T0102640 C,                          
                                                  MRT.T0201350 D,                          
                                                  MRT.T0112963 E                           
                                             Where C.CODEMP = 1                            
                                                  AND C.CODFILEMP    = :CODFILIAL                    
                                                  AND C.CODFILEMP    = E.CODFILEMP         
                                                  AND C.CODCID       = E.CODCIDPCOACOCMC   
                                                  AND C.DATREFALTCID = B.DATREFLIVPCOVGR   
                                                  AND A.CODFILEMP    = :CODFILIAL                    
                                                  AND A.CODFILEMPFAT = :CODFILIAL                    
                                                  AND A.NUMRLCCIDGIR = C.NUMRLCCIDGIR      
                                                  AND A.DATREFLIVPCO = B.DATREFLIVPCOVGR   
                                                  AND B.CODEMP       = 1                   
                                                  AND A.CODMER       = :CODMERC                   
                                                  AND D.CODEMP       = 1                   
                                                  AND D.CODFILEMP    = A.CODFILEMP         
                                                  AND D.CODMER       = A.CODMER            
                                             Group by A.CODFILEMP,                         
                                                      A.CODFILEMPFAT,                      
                                                      A.NUMRLCCIDGIR,                      
                                                      A.DATREFLIVPCO,                      
                                                      A.CODMER,                            
                                                      D.FLGMERPMC)                         
                                             SELECT B.VLRCSTUNTMER,     
                                                    B.VLRCSTUNTMER,  
                                                    C.FLGMERPMC                            
                                             FROM PcoUntMer C                              
                                                  INNER JOIN MRT.T0130856 B                
                                                     ON B.CODFILEMP     = C.CODFILEMP      
                                                     AND B.CODFILEMPFAT = C.CODFILEMPFAT   
                                                     AND B.NUMRLCCIDGIR = C.NUMRLCCIDGIR   
                                                     AND B.DATREFLIVPCO = C.DATREFLIVPCO   
                                                     AND B.DATREFPCO = C.DATREFPCO
                                                     AND B.CODMER = C.CODMER]]>.Value
        End If

        parameterMapper.Parameters.Add(":CODMERC", codMercadoria)
        parameterMapper.Parameters.Add(":CODFILIAL", codFilial)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"VLRCSTUNTMER", "VLRCSTUNTMER", "FLGMERPMC"}))
    End Function


    ''' <summary>
    ''' busca dados com preço unitario do item selecionado
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultaPrecoUnitarioItemThread(codMercadoria As Integer, codFilial As Integer, ByVal TIPACOMCD As Decimal) As List(Of VO.MovimentoQuinzenalPrecoMercadoriaVO)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = ""
        If (TIPACOMCD = TipoAcao.PreAcordoVigencia Or TIPACOMCD = TipoAcao.PreAcordoValor) Then
            sql = <![CDATA[SELECT  
                                   ETQ.CODMER
                                 , ETQ.CODFILEMP
                                 , ETQ.FLGMERPMC
                                 , ETQ.VLRDIRCSTMEDEFT
                                 , MK.PERMRGADIBDEPMC
                                 , CASE WHEN FLGMERPMC <> 'S' 
                                    AND MK.PERMRGADIBDEPMC <> 0
                                    AND MER.INDRTNFTETBTSOC = 0
                                   THEN ROUND(ETQ.VLRDIRCSTMEDEFT/MK.PERMRGADIBDEPMC, 4) 
                                   WHEN FLGMERPMC <> 'S' 
                                     AND MK.PERMRGADIBDEPMC <> 0
                                     AND MER.INDRTNFTETBTSOC > 0
                                   THEN ROUND( (ETQ.VLRDIRCSTMEDEFT
                                               /MK.PERMRGADIBDEPMC )
                                               - (((ETQ.VLRDIRCSTMEDEFT
                                               /MK.PERMRGADIBDEPMC ) *
                                               ( A.PERPRGITGSOC + A.PERCRBFNMSEGSOC )
                                               / 100 ))
                                               , 4) 
                                   WHEN FLGMERPMC = 'S' 
                                   THEN ROUND(ETQ.VLRDIRCSTMEDEFT, 4) 
                                   END AS VLRCSTUNTMER 
                           FROM MRT.T0201350 ETQ 
                             INNER JOIN MRT.T0101295 MK 
                                      ON MK.CODEMP = 1 
                             INNER JOIN MRT.T0100086 MER 
                                     ON MER.CODEMP = 1
                                     AND MER.CODMER = ETQ.CODMER
                             INNER JOIN MRT.T0106726 A
                                     ON A.CODEMP = 1
                                     AND A.CODFILEMP = :CODFILIAL
                             INNER JOIN MRT.T0118392 B
                                     ON B.DATREFLIVPCOVGR = A.DATREFLIVPCO
                                     AND B.CODEMP = 1
                           WHERE ETQ.CODEMP = 1
                             AND ETQ.CODFILEMP = :CODFILIAL 
                             AND ETQ.CODMER = :CODMERC ]]>.Value
        Else
            sql = <![CDATA[WITH PcoUntMer as                              
                                            (SELECT A.CODFILEMP,                           
                                                     A.CODFILEMPFAT,                       
                                                     A.NUMRLCCIDGIR,                       
                                                     A.DATREFLIVPCO,                       
                                                     Max(A.DATREFPCO) as datrefpco,        
                                                     A.CODMER ,                            
                                                     D.FLGMERPMC                           
                                               FROM MRT.T0130856 A,                          
                                                  MRT.T0118392 B,                          
                                                  MRT.T0102640 C,                          
                                                  MRT.T0201350 D,                          
                                                  MRT.T0112963 E                           
                                             Where C.CODEMP = 1                            
                                                  AND C.CODFILEMP    = :CODFILIAL                    
                                                  AND C.CODFILEMP    = E.CODFILEMP         
                                                  AND C.CODCID       = E.CODCIDPCOACOCMC   
                                                  AND C.DATREFALTCID = B.DATREFLIVPCOVGR   
                                                  AND A.CODFILEMP    = :CODFILIAL                    
                                                  AND A.CODFILEMPFAT = :CODFILIAL                    
                                                  AND A.NUMRLCCIDGIR = C.NUMRLCCIDGIR      
                                                  AND A.DATREFLIVPCO = B.DATREFLIVPCOVGR   
                                                  AND B.CODEMP       = 1                   
                                                  AND A.CODMER       = :CODMERC                   
                                                  AND D.CODEMP       = 1                   
                                                  AND D.CODFILEMP    = A.CODFILEMP         
                                                  AND D.CODMER       = A.CODMER            
                                             Group by A.CODFILEMP,                         
                                                      A.CODFILEMPFAT,                      
                                                      A.NUMRLCCIDGIR,                      
                                                      A.DATREFLIVPCO,                      
                                                      A.CODMER,                            
                                                      D.FLGMERPMC)                         
                                             SELECT B.VLRCSTUNTMER,     
                                                    B.VLRCSTUNTMER,  
                                                    C.FLGMERPMC                            
                                             FROM PcoUntMer C                              
                                                  INNER JOIN MRT.T0130856 B                
                                                     ON B.CODFILEMP     = C.CODFILEMP      
                                                     AND B.CODFILEMPFAT = C.CODFILEMPFAT   
                                                     AND B.NUMRLCCIDGIR = C.NUMRLCCIDGIR   
                                                     AND B.DATREFLIVPCO = C.DATREFLIVPCO   
                                                     AND B.DATREFPCO = C.DATREFPCO
                                                     AND B.CODMER = C.CODMER]]>.Value
        End If

        parameterMapper.Parameters.Add(":CODMERC", codMercadoria)
        parameterMapper.Parameters.Add(":CODFILIAL", codFilial)

        Return connector.SelectTListThread(sql, parameterMapper, RowMapperOnlyProperties({"VLRCSTUNTMER", "VLRCSTUNTMER", "FLGMERPMC"}))
    End Function

    ''' <summary>
    ''' busca dados com preço unitario do item selecionado
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultaPrecoUnitarioItemPromocao(codMercadoria As Integer, codFilial As Integer, ByVal tipAcao As Decimal) As List(Of VO.MovimentoQuinzenalPrecoMercadoriaVO)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = ""
        If (tipAcao = TipoAcao.PreAcordoVigencia Or tipAcao = TipoAcao.PreAcordoValor) Then
            sql = <![CDATA[SELECT  
                                   ETQ.CODMER
                                 , ETQ.CODFILEMP
                                 , ETQ.FLGMERPMC
                                 , ETQ.VLRDIRCSTMEDEFT
                                 , MK.PERMRGADIBDEPMC
                                 , CASE WHEN FLGMERPMC <> 'S' 
                                    AND MK.PERMRGADIBDEPMC <> 0
                                    AND MER.INDRTNFTETBTSOC = 0
                                   THEN ROUND(ETQ.VLRDIRCSTMEDEFT/MK.PERMRGADIBDEPMC, 4) 
                                   WHEN FLGMERPMC <> 'S' 
                                     AND MK.PERMRGADIBDEPMC <> 0
                                     AND MER.INDRTNFTETBTSOC > 0
                                   THEN ROUND( (ETQ.VLRDIRCSTMEDEFT
                                               /MK.PERMRGADIBDEPMC )
                                               - (((ETQ.VLRDIRCSTMEDEFT
                                               /MK.PERMRGADIBDEPMC ) *
                                               ( A.PERPRGITGSOC + A.PERCRBFNMSEGSOC )
                                               / 100 ))
                                               , 4) 
                                   WHEN FLGMERPMC = 'S' 
                                   THEN ROUND(ETQ.VLRDIRCSTMEDEFT, 4) 
                                   END AS VLRCSTUNTMER 
                           FROM MRT.T0201350 ETQ 
                             INNER JOIN MRT.T0101295 MK 
                                      ON MK.CODEMP = 1 
                             INNER JOIN MRT.T0100086 MER 
                                     ON MER.CODEMP = 1
                                     AND MER.CODMER = ETQ.CODMER
                             INNER JOIN MRT.T0106726 A
                                     ON A.CODEMP = 1
                                     AND A.CODFILEMP = :CODFILIAL
                             INNER JOIN MRT.T0118392 B
                                     ON B.DATREFLIVPCOVGR = A.DATREFLIVPCO
                                     AND B.CODEMP = 1
                           WHERE ETQ.CODEMP = 1
                             AND ETQ.CODFILEMP = :CODFILIAL 
                             AND ETQ.CODMER = :CODMERC ]]>.Value
        Else
            sql = <![CDATA[WITH PcoUntMer as                              
                                            (SELECT A.CODFILEMP,                           
                                                     A.CODFILEMPFAT,                       
                                                     A.NUMRLCCIDGIR,                       
                                                     A.DATREFLIVPCO,                       
                                                     Max(A.DATREFPCO) as datrefpco,        
                                                     A.CODMER ,                            
                                                     D.FLGMERPMC                           
                                               FROM MRT.T0130856 A,                          
                                                  MRT.T0118392 B,                          
                                                  MRT.T0102640 C,                          
                                                  MRT.T0201350 D,                          
                                                  MRT.T0112963 E                           
                                             Where C.CODEMP = 1                            
                                                  AND C.CODFILEMP    = :CODFILIAL                    
                                                  AND C.CODFILEMP    = E.CODFILEMP         
                                                  AND C.CODCID       = E.CODCIDPCOACOCMC   
                                                  AND C.DATREFALTCID = B.DATREFLIVPCOVGR   
                                                  AND A.CODFILEMP    = :CODFILIAL                    
                                                  AND A.CODFILEMPFAT = :CODFILIAL                    
                                                  AND A.NUMRLCCIDGIR = C.NUMRLCCIDGIR      
                                                  AND A.DATREFLIVPCO = B.DATREFLIVPCOVGR   
                                                  AND B.CODEMP       = 1                   
                                                  AND A.CODMER       = :CODMERC                   
                                                  AND D.CODEMP       = 1                   
                                                  AND D.CODFILEMP    = A.CODFILEMP         
                                                  AND D.CODMER       = A.CODMER            
                                             Group by A.CODFILEMP,                         
                                                      A.CODFILEMPFAT,                      
                                                      A.NUMRLCCIDGIR,                      
                                                      A.DATREFLIVPCO,                      
                                                      A.CODMER,                            
                                                      D.FLGMERPMC)                         
                                             SELECT B.VLRCSTUNTMER,     
                                                    B.VLRCSTUNTMER,  
                                                    C.FLGMERPMC                            
                                             FROM PcoUntMer C                              
                                                  INNER JOIN MRT.T0130856 B                
                                                     ON B.CODFILEMP     = C.CODFILEMP      
                                                     AND B.CODFILEMPFAT = C.CODFILEMPFAT   
                                                     AND B.NUMRLCCIDGIR = C.NUMRLCCIDGIR   
                                                     AND B.DATREFLIVPCO = C.DATREFLIVPCO   
                                                     AND B.DATREFPCO = C.DATREFPCO
                                                     AND B.CODMER = C.CODMER]]>.Value
        End If

        parameterMapper.Parameters.Add(":CODMERC", codMercadoria)
        parameterMapper.Parameters.Add(":CODFILIAL", codFilial)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"VLRCSTUNTMER", "VLRCSTUNTMER", "FLGMERPMC"}))
    End Function

End Class

<TableAttribute("MRT.T0130856")>
Public Class MovimentoQuinzenalPrecoMercadoriaDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODFILEMPFAT As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODFILEMP As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMRLCCIDGIR As Nullable(Of Decimal)
    <PrimaryKey>
    Property DATREFPCO As Nullable(Of Date)
    <PrimaryKey>
    Property DATREFLIVPCO As Nullable(Of Date)
    <PrimaryKey>
    Property CODMER As Nullable(Of Decimal)
    Property VLRVTAINTMER As Nullable(Of Decimal)
    Property VLRUNTRBTMERANT As Nullable(Of Decimal)
    Property VLRUNTRBTMER As Nullable(Of Decimal)
    Property VLRUNTPCOFBR As Nullable(Of Decimal)
    Property VLRUNTPCOBRTMTZ As Nullable(Of Decimal)
    Property VLRUNTPCOBRTFOB As Nullable(Of Decimal)
    Property VLRUNTPCOBRTCSM As Nullable(Of Decimal)
    Property VLRUNTPCOBRTANT As Nullable(Of Decimal)
    Property VLRUNTPCOBRT As Nullable(Of Decimal)
    Property VLRMEDVBAPCOMERANT As Nullable(Of Decimal)
    Property VLRMEDVBAPCOMER As Nullable(Of Decimal)
    Property VLRMEDBNFPCOMERANT As Nullable(Of Decimal)
    Property VLRMEDBNFPCOMER As Nullable(Of Decimal)
    Property VLRICMMEDSMRECCSFR As Nullable(Of Decimal)
    Property VLRICMMEDMERSMR As Nullable(Of Decimal)
    Property VLRFTRMCDPCL As Nullable(Of Decimal)
    Property VLRCSTUNTMERMTZ As Nullable(Of Decimal)
    Property VLRCSTUNTMERANT As Nullable(Of Decimal)
    Property VLRCSTUNTMER As Nullable(Of Decimal)
    Property VLRCSTPSORLCVND As Nullable(Of Decimal)
    Property TIPMER As Nullable(Of Decimal)
    Property TIPFILEMPSBTTBTPCO As Nullable(Of Decimal)
    Property QDEQTAMERLIVPCO As String
    Property PERTBTSOC As Nullable(Of Decimal)
    Property PERTBTIPIMER As Nullable(Of Decimal)
    Property PERTBTICMMER As Nullable(Of Decimal)
    Property PERSBTTBT As Nullable(Of Decimal)
    Property PERMRGMER As Nullable(Of Decimal)
    Property PERMRGCPLMER As Nullable(Of Decimal)
    Property PERMRGBRTMER As Nullable(Of Decimal)
    Property PERMRGADIMER As Nullable(Of Decimal)
    Property PERMAXCNSFLX As Nullable(Of Decimal)
    Property PERDSCFLXPCO As Nullable(Of Decimal)
    Property PERCSTDSP As Nullable(Of Decimal)
    Property PERCMSNORMER As Nullable(Of Decimal)
    Property INDRTNFTETBTSOC As Nullable(Of Decimal)
    Property INDMERBLQVNDCPO As Nullable(Of Decimal)
    Property FTRUNTSBTTBTMER As Nullable(Of Decimal)
    Property FTRNVGMER As Nullable(Of Decimal)
    Property FTRAJTPSOCBAMER As Nullable(Of Decimal)
    Property FTRAJTCSTDTBMER As Nullable(Of Decimal)
    Property FLGUTZDIFICM As String
    Property FLGMERICTVND As String
    Property FLGMEREXVMIX As String
    Property FLGMERESGLIVPCO As String
    Property FLGMERCMSADI As String
    Property CODTBTICMMER As Nullable(Of Decimal)
    Property CODSMBSITMER As String
    Property CODFLXPCO As String

    Property FLGMERPMC As String

    'Auxiliares
    <IgnoreMap>
    Property VLRTOTMERISRPMC As Nullable(Of Decimal)
End Class
