﻿Imports Core
Public Class CarimboDeVerbaDoFornecedorDAO
    Inherits DAOBase(Of VO.CarimboDeVerbaDoFornecedor)

    Public Sub New()
        daoConfiguration = New CarimboDeVerbaDoFornecedorDAOConfiguration()
    End Sub


    Public Function RetornaCadMcoVbaFrn(ByVal CODMCOVBAFRN As Decimal, ByVal VLRUTZACOCMC As Decimal) As Integer



        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.CADMCOVBAFRN SET 
            VLRUTZACOCMC = VLRUTZACOCMC - :VLRUTZACOCMC
            WHERE CODMCOVBAFRN = :CODMCOVBAFRN]]>.Value

        parameterMapper.Parameters.Add(":CODMCOVBAFRN", CODMCOVBAFRN)
        parameterMapper.Parameters.Add(":VLRUTZACOCMC", VLRUTZACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function


End Class


<TableAttribute("MRT.CADMCOVBAFRN")>
Public Class CarimboDeVerbaDoFornecedorDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODMCOVBAFRN As Nullable(Of Decimal)

End Class

