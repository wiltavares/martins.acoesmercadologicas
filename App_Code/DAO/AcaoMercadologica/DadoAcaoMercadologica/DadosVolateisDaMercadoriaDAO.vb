﻿Imports Core
Public Class DadosVolateisDaMercadoriaDAO
    Inherits DAOBase(Of VO.DadosVolateisDaMercadoria)

    Public Sub New()
        daoConfiguration = New DadosVolateisDaMercadoriaDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0201350")>
Public Class DadosVolateisDaMercadoriaDAOConfiguration
    Inherits DAOConfiguration

    <DateInsert>
    <[Readonly](False, True)>
    Property DATULTMOVETQMER As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATULTRMCCST As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATINIMOVETQMER As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATULTFATMER As Date

End Class
