﻿Imports Core
Public Class RelacaoAcaoComercialxMecanicaDaAcaoDAO
    Inherits DAOBase(Of VO.RelacaoAcaoComercialxMecanicaDaAcao)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoComercialxMecanicaDaAcaoDAOConfiguration()
    End Sub
    ''' <summary>
    ''' BscRgrAcoCmc
    ''' Busca Regra de Mecanica da Acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <returns></returns>
    Public Function BuscarRegraMecanicaDaAcao(codAcao As Integer) As VO.RelacaoAcaoComercialxMecanicaDaAcao

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODACOCMC, DESRGRACOCMC 
                            FROM MRT.T0159412   
                            WHERE CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql)).FirstOrDefault()

    End Function

End Class


<TableAttribute("MRT.T0159412")>
Public Class RelacaoAcaoComercialxMecanicaDaAcaoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)

End Class