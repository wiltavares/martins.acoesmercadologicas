﻿Imports Core
Public Class RelacaoAcaoComercialxPoliticaDAO
    Inherits DAOBase(Of VO.RelacaoAcaoComercialxPolitica)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoComercialxPoliticaDAOConfiguration()
    End Sub


    ''' <summary>
    ''' DelDdoAcoCmc
    ''' Deleta Dado da Acao Comercial Politicas
    ''' </summary>
    ''' <param name="codAcao"></param>
    Public Function DeletarDadoAcaoComercialPoliticas(codAcao As Integer) As Integer
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = "DELETE FROM MRT.T0155808 WHERE CODACOCMC = :CODACOCMC "

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function


    ''' <summary>
    ''' DelDdoAcoCmc
    ''' Insere Dados da Politicas Acao Comercial
    ''' </summary>
    ''' <param name="codAcao"></param>
    Public Function InsereDadosPoliticaAcao(codAcao As Integer, codPolAcaoCmc As Integer) As Integer
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.T0155808 (CODACOCMC,CODPOLACOCMC)VALUES (:CODACOCMC,:CODPOLACOCMC)]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)
        parameterMapper.Parameters.Add(":CODPOLACOCMC", codPolAcaoCmc)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function ConsultaPoliticaDivergente(CODEVTACOCMC As Decimal) As List(Of VO.RelacaoAcaoComercialxPolitica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT INDEVTMTDPOLDVR FROM MRT.T0155531 WHERE CodEvtAcoCmc = :CODEVTACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODEVTACOCMC", CODEVTACOCMC)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class


<TableAttribute("MRT.T0155808")>
Public Class RelacaoAcaoComercialxPoliticaDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODPOLACOCMC As Nullable(Of Decimal)

End Class

