﻿Imports Core
Public Class CanalDeVendaInicialDAO
    Inherits DAOBase(Of VO.CanalDeVendaInicial)

    Public Sub New()
        daoConfiguration = New CanalDeVendaInicialDAOConfiguration()
    End Sub

    ''' <summary>
    ''' BscPmtAcoCmc
    ''' Busca as taxas de adminstração e brinde da ação comercial
    ''' </summary>
    ''' <returns></returns>
    Public Function ListarCanalDeVenda() As List(Of VO.CanalDeVendaInicial)

        Dim sql As String = <![CDATA[Select CODCNLVNDINI, CODCNLVNDINI || ' - ' || DESCNLVND AS DESCNLVND FROM MRT.CADCNLVNDINI
         WHERE DATHRADST Is NULL ORDER BY 1]]>.Value

        Return connector.SelectTList(sql, Nothing, RowMapperOnlyProperties(sql))

    End Function
End Class


<TableAttribute("MRT.CADCNLVNDINI")>
Public Class CanalDeVendaInicialDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODCNLVNDINI As Nullable(Of Decimal)

    <DateInsert>
    <[Readonly](False, True)>
    Property DATHRACAD As DateTime  'TIMESTAMP(6)

    <DateInsert>
    <[Readonly](False, True)>
    Property DATHRAALT As DateTime  'TIMESTAMP(6)

    <DateInsert>
    <[Readonly](False, True)>
    Property DATHRADST As DateTime 'TIMESTAMP(6)

End Class
