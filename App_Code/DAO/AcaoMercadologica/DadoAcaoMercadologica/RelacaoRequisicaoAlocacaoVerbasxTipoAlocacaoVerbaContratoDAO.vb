﻿Imports Core
Public Class RelacaoRequisicaoAlocacaoVerbasxTipoAlocacaoVerbaContratoDAO
    Inherits DAOBase(Of VO.RelacaoRequisicaoAlocacaoVerbasxTipoAlocacaoVerbaContrato)

    Public Sub New()
        daoConfiguration = New RelacaoRequisicaoAlocacaoVerbasxTipoAlocacaoVerbaContratoDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0151853")>
Public Class RelacaoRequisicaoAlocacaoVerbasxTipoAlocacaoVerbaContratoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODREQALCVBAFRN As Nullable(Of Decimal)

    <PrimaryKey>
    Property TIPALCVBAFRN As Nullable(Of Decimal)

    <DateInsert>
    <[Readonly](False, True)>
    Property DATEFTALCVBAFRN As Date

End Class
