﻿Imports Core
Public Class TipoDeAlocacaoDeVerbasDoFornecedorDAO
    Inherits DAOBase(Of VO.TipoDeAlocacaoDeVerbasDoFornecedor)

    Public Sub New()
        daoConfiguration = New TipoDeAlocacaoDeVerbasDoFornecedorDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0151799")>
Public Class TipoDeAlocacaoDeVerbasDoFornecedorDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property TIPALCVBAFRN As Nullable(Of Decimal)

End Class
