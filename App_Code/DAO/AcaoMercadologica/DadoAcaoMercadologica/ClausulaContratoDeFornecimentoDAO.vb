﻿Imports Core
Public Class ClausulaContratoDeFornecimentoDAO
    Inherits DAOBase(Of VO.ClausulaContratoDeFornecimento)

    Public Sub New()
        daoConfiguration = New ClausulaContratoDeFornecimentoDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0124953")>
Public Class ClausulaContratoDeFornecimentoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property NUMCSLCTTFRN As Nullable(Of Decimal)

End Class
