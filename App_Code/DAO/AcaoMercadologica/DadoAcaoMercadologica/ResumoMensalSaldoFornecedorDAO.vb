﻿Imports Core
Public Class ResumoMensalSaldoFornecedorDAO
    Inherits DAOBase(Of VO.ResumoMensalSaldoFornecedor)

    Public Sub New()
        daoConfiguration = New ResumoMensalSaldoFornecedorDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0123078")>
Public Class ResumoMensalSaldoFornecedorDAOConfiguration
    Inherits DAOConfiguration

End Class
