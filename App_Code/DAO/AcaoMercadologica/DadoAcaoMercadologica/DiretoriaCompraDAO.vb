﻿Imports Core
Public Class DiretoriaCompraDAO
    Inherits DAOBase(Of VO.DiretoriaCompra)

    Public Sub New()
        daoConfiguration = New DiretoriaCompraDAOConfiguration()
    End Sub

    Public Function ListDiretoriaCompra(vo As VO.DiretoriaCompra) As List(Of VO.DiretoriaCompra)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = "SELECT * FROM MRT.T0123183 ORDER BY DESDRTCMP ASC;"

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class


<TableAttribute("MRT.T0123183")>
Public Class DiretoriaCompraDAOConfiguration
    Inherits DAOConfiguration


    <DateInsert>
    <[Readonly](True, False)>
    Property DATDSTDRTCMP As Date

End Class
