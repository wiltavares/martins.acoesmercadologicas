﻿Imports Core
Public Class RelacaoGerenteMarketingxEmitenteAcaoMercadologicaDAO
    Inherits DAOBase(Of VO.RelacaoGerenteMarketingxEmitenteAcaoMercadologica)

    Public Sub New()
        daoConfiguration = New RelacaoGerenteMarketingxEmitenteAcaoMercadologicaDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.RLCGERMKTEMTACOCMC")>
Public Class RelacaoGerenteMarketingxEmitenteAcaoMercadologicaDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODFNCEMTACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODFNCGERMKT As Nullable(Of Decimal)

End Class
