﻿Imports Core
Public Class RelacaoPromessaxFormaDePagamentoxDestinoDaVerbaPrometidaDAO
    Inherits DAOBase(Of VO.RelacaoPromessaxFormaDePagamentoxDestinoDaVerbaPrometida)

    Public Sub New()
        daoConfiguration = New RelacaoPromessaxFormaDePagamentoxDestinoDaVerbaPrometidaDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0118066")>
Public Class RelacaoPromessaxFormaDePagamentoxDestinoDaVerbaPrometidaDAOConfiguration
    Inherits DAOConfiguration

    <DateInsert>
    <[Readonly](False, True)>
    Property DATNGCPMS As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATPRVRCBPMS As Date

End Class
