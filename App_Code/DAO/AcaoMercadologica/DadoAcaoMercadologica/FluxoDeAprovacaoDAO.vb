﻿Imports Core
Public Class FluxoDeAprovacaoDAO
    Inherits DAOBase(Of VO.FluxoDeAprovacao)

    Public Sub New()
        daoConfiguration = New FluxoDeAprovacaoDAOConfiguration()
    End Sub

    Public Function SalvarItensfluxo(item As VO.FluxoDeAprovacao)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim codigo As Integer
        If IsNothing(item.CODEDEARZ) Then
            codigo = 0
        Else
            codigo = item.CODEDEARZ.Value
        End If

        Dim sql As String = <![CDATA[INSERT INTO MRT.T0161591 (CODSISINF,           
                                                                           NUMFLUAPV,           
                                                                           NUMSEQFLUAPV,        
                                                                           DATHRAFLUAPV,        
                                                                           NUMSEQNIVAPV,        
                                                                           CODEDEAPV,           
                                                                           TIPSTAFLUAPV,        
                                                                           DESMTVAPVFLUACOCMC,  
                                                                           NUMSEQFLUAPVPEDOPN,  
                                                                           CODEDEARZ)
                                                                   VALUES(:CODSISINF,
                                                                          :NUMFLUAPV,
                                                                          :NUMSEQFLUAPV,
                                                                           SYSDATE,
                                                                          :NUMSEQNIVAPV,
                                                                          :CODEDEAPV,         
                                                                          :TIPSTAFLUAPV,      
                                                                          :DESMTVAPVFLUACOCMC,
                                                                          :NUMSEQFLUAPVPEDOPN,
                                                                          :CODEDEARZ)]]>.Value

        parameterMapper.Parameters.Add(":CODSISINF", item.CODSISINF)
        parameterMapper.Parameters.Add(":NUMFLUAPV", item.NUMFLUAPV)
        parameterMapper.Parameters.Add(":NUMSEQFLUAPV", item.NUMSEQFLUAPV)
        parameterMapper.Parameters.Add(":NUMSEQNIVAPV", item.NUMSEQNIVAPV)
        parameterMapper.Parameters.Add(":CODEDEAPV", item.CODEDEAPV)
        parameterMapper.Parameters.Add(":TIPSTAFLUAPV", ValueDBProperty(item.TIPSTAFLUAPV))
        parameterMapper.Parameters.Add(":DESMTVAPVFLUACOCMC", ValueDBProperty(item.DESMTVAPVFLUACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQFLUAPVPEDOPN", ValueDBProperty(item.NUMSEQFLUAPV))
        parameterMapper.Parameters.Add(":CODEDEARZ", ValueDBProperty(codigo))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function


    Public Function buscaUltimoFluxoAprovacao() As Integer
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[select max(NUMFLUAPV) as NUMFLUAPV from MRT.T0161591]]>.Value

        Dim properties = {"NUMFLUAPV"}
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(properties)).FirstOrDefault.NUMFLUAPV
    End Function


    Public Function BuscaFormaPagamento() As List(Of VO.FluxoDeAprovacao)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[ SELECT
                                            TIPFRMDSCBNF
                                          , DESFRMDSCBNF
                                      FROM MRT.T0113552
                                      WHERE TIPFRMDSCBNF IN(13,8)]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


    Public Function CnsUsrNotEma(ByVal CodSisInf As Decimal, ByVal NumSeqApv As Decimal) As List(Of VO.FluxoDeAprovacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT B.NOMUSRRCF AS NOMUSRRCF, NUMSEQFLUAPV 
                                       FROM MRT.T0161591 A, MRT.T0104596 B 
                                      WHERE A.CODSISINF = :CODSISINF 
                                        AND A.NUMFLUAPV = :NUMFLUAPV
                                        AND A.TIPSTAFLUAPV IN (1, 7) 
                                        AND A.CODEDEAPV = B.CODFNC]]>.Value

        parameterMapper.Parameters.Add(":CODSISINF", CodSisInf)
        parameterMapper.Parameters.Add(":NUMFLUAPV", NumSeqApv)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"NOMUSRRCF", "NUMSEQFLUAPV"}))

    End Function

    Public Function BuscaFuncionarioGAC() As List(Of VO.FluxoDeAprovacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT NUMSEQNIVAPV, CODTIPAPV, CODFNCAPVFIX 
                                     FROM MRT.T0161581 
                                     WHERE CODSISINF = 8
                                       AND NUMSEQNIVAPV = 5]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

#Region "Aprovar/Parecer"
    Public Function AtuPedOpnFluApv(ByVal CodSisInf As Integer, _
                                        ByVal NumFluApv As Integer, _
                                        ByVal NumSeqFluApv As Integer, _
                                        ByVal AcoApv As Integer, _
                                        ByVal ObsApv As String, _
                                        ByVal CodFnc As Integer) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If (CodFnc = 0) Then
            CodFnc = Nothing
        End If

        Dim sql As String = <![CDATA[UPDATE MRT.T0161591
                                    SET TIPSTAFLUAPV = :AcoApv
                                    , DESOBSAPV = :DESOBSAPV
                                    , NUMSEQFLUAPVPEDOPN = (SELECT MAX(NUMSEQFLUAPV) + 1 FROM MRT.T0161591 WHERE CODSISINF = :CodSisInf AND NUMFLUAPV = :NumFluApv)
                                    , CODEDEARZ = coalesce(:CodFnc, CODEDEARZ)
                                    WHERE CODSISINF = :CODSISINF
                                     AND NUMFLUAPV = :NUMFLUAPV
                                    AND NUMSEQFLUAPV = :NumSeqFluApv ]]>.Value

        If Not IsNothing(ObsApv) Then
            ObsApv = ObsApv.Trim().Replace("'", " ")
        End If

        parameterMapper.Parameters.Add(":AcoApv", AcoApv)
        parameterMapper.Parameters.Add(":DESOBSAPV", ObsApv)
        parameterMapper.Parameters.Add(":CodSisInf", CodSisInf)
        parameterMapper.Parameters.Add(":NumFluApv", NumFluApv)
        parameterMapper.Parameters.Add(":CodFnc", CodFnc)
        parameterMapper.Parameters.Add(":NumSeqFluApv", NumSeqFluApv)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function RspPedOpnFluApv(ByVal CodSisInf As Integer, _
                                       ByVal NumFluApv As Integer, _
                                       ByVal NumSeqFluApv As Integer, _
                                       ByVal ObsApv As String, _
                                       ByVal CodFnc As Integer) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If (CodFnc = 0) Then
            CodFnc = Nothing
        End If

        Dim sql As String = <![CDATA[UPDATE MRT.T0161591
                                    SET DATHRAAPVFLU = SYSDATE
                                    , DESOBSAPV = :DESOBSAPV
                                    , CODEDEARZ = :CodFnc
                                    WHERE CODSISINF = :CODSISINF
                                     AND NUMFLUAPV = :NUMFLUAPV
                                    AND NUMSEQFLUAPV = :NumSeqFluApv ]]>.Value

        If Not IsNothing(ObsApv) Then
            ObsApv = ObsApv.Trim().Replace("'", " ")
        End If

        parameterMapper.Parameters.Add(":DESOBSAPV", ObsApv)
        parameterMapper.Parameters.Add(":CodSisInf", CodSisInf)
        parameterMapper.Parameters.Add(":NumFluApv", NumFluApv)
        parameterMapper.Parameters.Add(":CodFnc", CodFnc)
        parameterMapper.Parameters.Add(":NumSeqFluApv", NumSeqFluApv)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function AtuEmiPedOpnFluApv(ByVal CodSisInf As Integer, _
                                   ByVal NumFluApv As Integer, _
                                   ByVal NumSeqFluApv As Integer) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0161591
                                    SET TIPSTAFLUAPV = '3'
                                    , NUMSEQFLUAPVPEDOPN = 0
                                    , CODEDEARZ = 0
                                    , DESOBSAPV = NULL
                                    WHERE CODSISINF = :CODSISINF
                                     AND NUMFLUAPV = :NUMFLUAPV
                                    AND NUMSEQFLUAPV = (SELECT MAX(NUMSEQFLUAPVPEDOPN) FROM MRT.T0161591 WHERE CODSISINF = :CodSisInf AND NUMFLUAPV = :NumFluApv AND NUMSEQFLUAPV = :NumSeqFluApv) ]]>.Value


        parameterMapper.Parameters.Add(":CodSisInf", CodSisInf)
        parameterMapper.Parameters.Add(":NumFluApv", NumFluApv)
        parameterMapper.Parameters.Add(":NumSeqFluApv", NumSeqFluApv)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function AtuFlgStaApvCmp(ByVal CodSisInf As Integer, _
                                    ByVal NumFluApv As Integer, _
                                    ByVal NumSeqFluApv As Integer, _
                                    ByVal AcoApv As Integer, _
                                    ByVal ObsApv As String, _
                                    ByVal CodFnc As Integer) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If (CodFnc = 0) Then
            CodFnc = Nothing
        End If

        Dim sql As String = <![CDATA[UPDATE MRT.T0161591
                                    SET DATHRAAPVFLU = SYSDATE
                                    , TIPSTAFLUAPV = :AcoApv
                                    , DESOBSAPV = :DESOBSAPV
                                    , CODEDEARZ = coalesce(:CodFnc, CODEDEARZ)
                                    WHERE CODSISINF = :CODSISINF
                                     AND NUMFLUAPV = :NUMFLUAPV
                                    AND NUMSEQFLUAPV = :NumSeqFluApv ]]>.Value

        If Not IsNothing(ObsApv) Then
            ObsApv = ObsApv.Trim().Replace("'", " ")
        End If

        parameterMapper.Parameters.Add(":AcoApv", AcoApv)
        parameterMapper.Parameters.Add(":DESOBSAPV", ObsApv)
        parameterMapper.Parameters.Add(":CodSisInf", CodSisInf)
        parameterMapper.Parameters.Add(":NumFluApv", NumFluApv)
        parameterMapper.Parameters.Add(":CodFnc", CodFnc)
        parameterMapper.Parameters.Add(":NumSeqFluApv", NumSeqFluApv)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function AtuFlgStaNumFluApvCmp(ByVal CodSisInf As Integer, _
                                          ByVal NumFluApv As Integer, _
                                          ByVal NumSeqFluApv As Integer, _
                                          ByVal AcoApv As Integer) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0161591
                                    SET TIPSTAFLUAPV = :AcoApv
                                    WHERE CODSISINF = :CODSISINF
                                     AND NUMFLUAPV = :NUMFLUAPV
                                    AND NUMSEQFLUAPV = :NumSeqFluApv 
                                    AND TIPSTAFLUAPV = '9']]>.Value


        parameterMapper.Parameters.Add(":AcoApv", AcoApv)
        parameterMapper.Parameters.Add(":CodSisInf", CodSisInf)
        parameterMapper.Parameters.Add(":NumFluApv", NumFluApv)
        parameterMapper.Parameters.Add(":NumSeqFluApv", NumSeqFluApv)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function IsrPedOpnFluApv(ByVal CodSisInf As Integer, _
                                    ByVal NumFluApv As Integer, _
                                    ByVal NumSeqNivApv As Integer, _
                                    ByVal CodEdeApv As Integer, _
                                    ByVal CodApv As Integer, _
                                    ByVal MtvApv As String, _
                                    ByVal NumSeqFluApvPedOpn As Integer) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.T0161591 (CODSISINF, NUMFLUAPV, NUMSEQFLUAPV, DATHRAFLUAPV, NUMSEQNIVAPV, CODEDEAPV,
                                                             TIPSTAFLUAPV, DATHRAAPVFLU, DESOBSAPV, DESMTVAPVFLUACOCMC, NUMSEQFLUAPVPEDOPN, CODEDEARZ)

                                select * from (SELECT  :CodSisInf as CodSisInf
                                            , :NumFluApv as NumFluApv
                                            , MAX(NUMSEQFLUAPV) + 1 as NUMSEQFLUAPV
                                            , DATHRAFLUAPV as DATHRAFLUAPV
                                            , :NumSeqNivApv as NumSeqNivApv
                                            , :CodEdeApv as CodEdeApv
                                            , :CodApv as CodApv
                                            , NULL as nulo1
                                            , NULL as nulo2
                                            , :MtvApv as MtvApv
                                            , :NumSeqFluApvPedOpn as NumSeqFluApvPedOpn
                                            , 0 as nulo3
                                            FROM MRT.T0161591                              
                                            WHERE CODSISINF = :CodSisInf
                                            AND NUMFLUAPV = :NumFluApv
                                            GROUP BY DATHRAFLUAPV 
                                            ORDER BY NUMSEQFLUAPV DESC) WHERE rownum = 1]]>.Value


        parameterMapper.Parameters.Add(":CodSisInf", CodSisInf)
        parameterMapper.Parameters.Add(":NumFluApv", NumFluApv)
        parameterMapper.Parameters.Add(":NumSeqNivApv", NumSeqNivApv)
        parameterMapper.Parameters.Add(":CodEdeApv", CodEdeApv)
        parameterMapper.Parameters.Add(":CodApv", CodApv)
        parameterMapper.Parameters.Add(":MtvApv", MtvApv)
        parameterMapper.Parameters.Add(":NumSeqFluApvPedOpn", NumSeqFluApvPedOpn)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    Public Function CnsUsrAlaEmaEsp(ByVal CodSisInf As Integer, _
                                    ByVal NumFluApv As Integer, _
                                    ByVal NumSeqFluApv As Integer, _
                                    ByVal NumNivFluApv As Integer) As List(Of VO.FluxoDeAprovacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim NumNivFluApvN As String
        If (NumNivFluApv = 0) Then
            NumNivFluApvN = Nothing
        ElseIf (Not NumNivFluApv = Nothing) Then
            NumNivFluApvN = String.Format("{0}%", NumNivFluApv)
        End If

        Dim sql As String = <![CDATA[SELECT B.NOMUSRRCF AS NOMUSRRCF
                                       FROM MRT.T0161591 A
                                        INNER JOIN MRT.T0104596 B ON A.CODEDEAPV = B.CODFNC
                                      WHERE A.CODSISINF = :CODSISINF 
                                        AND A.NUMFLUAPV = :NUMFLUAPV
                                        AND (:NumSeqFluApv IS NULL OR A.NUMSEQFLUAPVPEDOPN =  :NumSeqFluApv)
                                        AND (:NumNivFluApv IS NULL OR TO_CHAR(NUMSEQNIVAPV) LIKE :NumNivFluApv ) ]]>.Value

        parameterMapper.Parameters.Add(":CODSISINF", CodSisInf)
        parameterMapper.Parameters.Add(":NUMFLUAPV", NumFluApv)
        parameterMapper.Parameters.Add(":NumSeqFluApv", NumSeqFluApv)
        parameterMapper.Parameters.Add(":NumNivFluApv", ValueDBProperty(NumNivFluApvN))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function CnsFluApvAbt(ByVal CodAcoCmc As Integer) As List(Of VO.FluxoDeAprovacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        'A.CODSTAAPVACOCMC,
        '                            A.NUMFLUAPV,
        '                            B.NUMSEQFLUAPV,
        '                            B.DATHRAFLUAPV,
        '                            TO_CHAR(B.NUMSEQNIVAPV) as NUMSEQNIVAPV,
        '                            C.CODFNC,
        '                            C.NOMFNC,
        '                            B.TIPSTAFLUAPV,
        '                            B.DATHRAAPVFLU,
        '                            B.DESOBSAPV,
        '                            B.DESMTVAPVFLUACOCMC,
        '                            B.NUMSEQFLUAPVPEDOPN,
        '                            D.CODFNC as CODFNCARZ,
        '                            D.NOMFNC as NOMFNCARZ

        Dim sql As String = <![CDATA[SELECT B.NUMSEQFLUAPV,
                                    B.DATHRAFLUAPV,
                                    TO_CHAR(B.NUMSEQNIVAPV) as NUMSEQNIVAPV,
                                    B.CODEDEAPV,
                                    B.TIPSTAFLUAPV,
                                    B.DATHRAAPVFLU,
                                    B.DESOBSAPV,
                                    B.DESMTVAPVFLUACOCMC,
                                    B.NUMSEQFLUAPVPEDOPN,
                                    B.CODEDEARZ
                                    FROM  MRT.RLCACOCMCFLUAPV A
                                        INNER JOIN  MRT.T0161591 B ON (A.NUMFLUAPV = B.NUMFLUAPV AND B.CODSISINF = 8)
                                    WHERE A.CODACOCMC =  :CODACOCMC
                                    AND A.CODSTAAPVACOCMC = 3
                                    ORDER BY A.NUMFLUAPV DESC, B.NUMSEQFLUAPV ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CodAcoCmc)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function CnsUsrAlaEma(ByVal CodSisInf As Decimal, ByVal NumSeqApv As Decimal) As List(Of VO.FluxoDeAprovacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT B.NOMUSRRCF AS NOMUSRRCF, NUMSEQFLUAPV 
                                       FROM MRT.T0161591 A, MRT.T0104596 B 
                                      WHERE A.CODSISINF = :CODSISINF 
                                        AND A.NUMFLUAPV = :NUMFLUAPV
                                        AND A.TIPSTAFLUAPV IN (3,6) 
                                        AND A.CODEDEAPV = B.CODFNC]]>.Value

        parameterMapper.Parameters.Add(":CODSISINF", CodSisInf)
        parameterMapper.Parameters.Add(":NUMFLUAPV", NumSeqApv)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"NOMUSRRCF", "NUMSEQFLUAPV"}))

    End Function




#End Region

End Class

<TableAttribute("MRT.T0161591")>
Public Class FluxoDeAprovacaoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODSISINF As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMFLUAPV As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMSEQFLUAPV As Nullable(Of Decimal)

    Property DATHRAFLUAPV As DateTime 'TimeStamp

    Property DATHRAAPVFLU As DateTime 'TimeStamp

    Property NUMSEQNIVAPV As Nullable(Of Decimal)


    Property CODEDEAPV As Nullable(Of Decimal)

    Property TIPSTAFLUAPV As String

    Property DESOBSAPV As String

    Property DESMTVAPVFLUACOCMC As String

    Property NUMSEQFLUAPVPEDOPN As Nullable(Of Decimal)

    Property CODEDEARZ As Nullable(Of Decimal)


End Class
