﻿Imports Core
Imports VO

Public Class RelacaoAcaoComercialxFornecedoresDAO
    Inherits DAOBase(Of VO.RelacaoAcaoComercialxFornecedores)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoComercialxFornecedoresDAOConfiguration()
    End Sub

    ''' <summary>
    ''' InsDdoAcoCmcFrn
    ''' </summary>
    ''' <param name="fornecedor"></param>
    ''' <returns></returns>
    Public Function InserirDadosFornecedor(fornecedor As RelacaoAcaoComercialxFornecedores) As Object

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.T0138768(   
                                 CODACOCMC,         
                                 CODFRN,            
                                 INDATDDTOFRN,      
                                 VLRPTCFRNACOCMC,   
                                 VLRTXAADMFRNACOCMC,
                                 VLRSLDCNTCRRFRN,   
                                 VLRSLDDISACOFUTFRN,
                                 VLRSLDDSNDSCACOCMC,
                                 TIPDSNDSCBNF,      
                                 VLRTXAADMBDEACOCMC,
                                 VLRUTZCTTACOCMC,   
                                 VLRUTZEXACTTACOCMC,
                                 TIPDSNDSCBNFEXACTT,
                                 CODUNDESRNGC,      
                                 VLRSLDDSNCTTACOCMC,
                                 DESBFCFRNACOCMC,   
                                 TIPDSNDSCBNFACR,   
                                 VLRSLDDSNACR,      
                                 VLRUTZRCTCSTMER,
                                 DESSELFRN,
                                 TIPFRNACOCMC,
                                 PERPTCFRNACOCMC) 
                             VALUES( 
                                 :CODACOCMC,         
                                 :CODFRN,]]>.Value

        Dim indicadorFrn As String = "0,"
        If fornecedor.CODFRN <> 999999 And fornecedor.TIPFRNACOCMC <> 1 Then
            indicadorFrn = "(SELECT INDATDDTOFRN FROM MRT.T0100426 WHERE CODFRN = :CODFRN),"
        End If

        sql = sql + indicadorFrn + <![CDATA[ :VLRPTCFRNACOCMC,   
                                     :VLRTXAADMFRNACOCMC,
                                     :VLRSLDCNTCRRFRN,   
                                     0.00,
                                     :VLRSLDDSNDSCACOCMC,
                                     :TIPDSNDSCBNF,      
                                     :VLRTXAADMBDEACOCMC,
                                     :VLRUTZCTTACOCMC,   
                                     :VLRUTZEXACTTACOCMC,
                                     :TIPDSNDSCBNFEXACTT,
                                     :CODUNDESRNGC,      
                                     :VLRSLDDSNCTTACOCMC,
                                     :DESBFCFRNACOCMC,   
                                     :TIPDSNDSCBNFACR,   
                                     :VLRSLDDSNACR,      
                                     :VLRUTZRCTCSTMER, 
                                     :DESSELFRN,
                                     :TIPFRNACOCMC,
                                     :PERPTCFRNACOCMC)]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", fornecedor.CODACOCMC)
        parameterMapper.Parameters.Add(":CODFRN", fornecedor.CODFRN)
        parameterMapper.Parameters.Add(":VLRPTCFRNACOCMC", fornecedor.VLRPTCFRNACOCMC)
        parameterMapper.Parameters.Add(":VLRTXAADMFRNACOCMC", fornecedor.VLRTXAADMFRNACOCMC)
        parameterMapper.Parameters.Add(":VLRSLDCNTCRRFRN", fornecedor.VLRSLDCNTCRRFRN)
        parameterMapper.Parameters.Add(":VLRSLDDSNDSCACOCMC", fornecedor.VLRSLDDSNDSCACOCMC)
        parameterMapper.Parameters.Add(":TIPDSNDSCBNF", fornecedor.TIPDSNDSCBNF)
        parameterMapper.Parameters.Add(":VLRTXAADMBDEACOCMC", fornecedor.VLRTXAADMBDEACOCMC)
        parameterMapper.Parameters.Add(":VLRUTZCTTACOCMC", fornecedor.VLRUTZCTTACOCMC)
        parameterMapper.Parameters.Add(":VLRUTZEXACTTACOCMC", fornecedor.VLRUTZEXACTTACOCMC)
        parameterMapper.Parameters.Add(":TIPDSNDSCBNFEXACTT", fornecedor.TIPDSNDSCBNFEXACTT)
        parameterMapper.Parameters.Add(":CODUNDESRNGC", If(fornecedor.TIPFRNACOCMC = 1, 0, fornecedor.CODUNDESRNGC))
        parameterMapper.Parameters.Add(":VLRSLDDSNCTTACOCMC", fornecedor.VLRSLDDSNCTTACOCMC)
        parameterMapper.Parameters.Add(":DESBFCFRNACOCMC", fornecedor.DESBFCFRNACOCMC)
        parameterMapper.Parameters.Add(":TIPDSNDSCBNFACR", fornecedor.TIPDSNDSCBNFACR)
        parameterMapper.Parameters.Add(":VLRSLDDSNACR", fornecedor.VLRSLDDSNACR)
        parameterMapper.Parameters.Add(":VLRUTZRCTCSTMER", fornecedor.VLRUTZRCTCSTMER)
        parameterMapper.Parameters.Add(":DESSELFRN", fornecedor.DESSELFRN)
        parameterMapper.Parameters.Add(":TIPFRNACOCMC", fornecedor.TIPFRNACOCMC)
        parameterMapper.Parameters.Add(":PERPTCFRNACOCMC", fornecedor.PERPTCFRNACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    ''' <summary>
    ''' Deleta todos os dados dos fornecedores da acao
    ''' </summary>
    ''' <param name="codAcao"></param>
    ''' <returns></returns>
    Public Function DeletarDadosFornecedorDaAcao(codAcao As Decimal) As Object
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE FROM MRT.T0138768
                            WHERE CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", codAcao)
        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    ''' <summary>
    ''' BscDdoFrn
    ''' Busca a lista de relação de fornecedores da acao comercial 
    ''' </summary>
    ''' <param name="idAcao"></param>
    ''' <returns></returns>
    Public Function BuscarFornecedoresDaAcao(idAcao As Decimal) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC,                                                                                  
                                A.VLRTOTACOCMC,                                                                      
                                A.VLRTOTTXAADMACOCMC,                                                                
                                A.VLRTOTBDEACOCMC,                                                                   
                                E.CODFRN,                                                                            
                                (CASE WHEN E.CODFRN = 999999 THEN 'DESPESAS MARTINS' ELSE F.NOMFRN END) AS NOMFRN,   
                                F.CODEMP,                                                                            
                                E.INDATDDTOFRN,                                                                      
                                E.VLRPTCFRNACOCMC,                                                                   
                                E.VLRTXAADMFRNACOCMC,                                                                
                                E.VLRSLDCNTCRRFRN,                                                                   
                                E.VLRSLDDSNDSCACOCMC,                                                                
                                E.TIPDSNDSCBNF,                                                                      
                                G.DESDSNDSCBNF,                                                                      
                                E.VLRTXAADMBDEACOCMC,                                                                
                                E.VLRUTZCTTACOCMC,                                                                   
                                E.VLRUTZEXACTTACOCMC,                                                                
                                E.TIPDSNDSCBNFEXACTT,                                                                
                                H.DESDSNDSCBNF AS DESDSNDSCBNFEXACTT,                                                
                                E.CODUNDESRNGC,                                                                      
                                B.DESUNDESRNGC,                                                                      
                                E.DESBFCFRNACOCMC,                                                                   
                                E.TIPDSNDSCBNFACR,                                                                   
                                E.VLRUTZRCTCSTMER,                                                                   
                                E.VLRSLDDSNACR,                                                                      
                                E.DESSELFRN ,
                                E.TIPFRNACOCMC,
                                E.PERPTCFRNACOCMC
                             FROM MRT.T0118430 A                                                                     
                                inner JOIN MRT.T0138768 E                                                            
                                   ON E.CODACOCMC = A.CODACOCMC                                                      
                                LEFT JOIN MRT.T0100426 F                                                             
                                   ON F.CODFRN = E.CODFRN                                                            
                                LEFT JOIN MRT.T0109059 G                                                             
                                   ON E.TIPDSNDSCBNF = G.TIPDSNDSCBNF                                                
                                LEFT JOIN MRT.T0109059 H                                                             
                                   ON E.TIPDSNDSCBNFEXACTT = H.TIPDSNDSCBNF                                          
                                LEFT JOIN MRT.T0131275 B                                                             
                                   ON E.CODUNDESRNGC = B.CODUNDESRNGC
                             WHERE A.CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", idAcao)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function BuscaFornecedor(codFrn As Decimal?) As VO.RelacaoAcaoComercialxFornecedores

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT D.CODFRN, D.NOMFRN        
                             FROM MRT.T0123183 A,           
                             MRT.T0118570 B,            
                             MRT.T0113625 C,          
                             MRT.T0100426 D             
                             WHERE A.CODDRTCMP = B.CODDRTCMP 
                             AND B.CODGERPRD = C.CODGERPRD 
                             AND C.CODCPR = D.CODCPR       
                             AND NVL(D.TIPIDTEMPASCACOCMC, 0) = 0 
                             AND D.DATDSTDIVFRN IS NULL
                             And D.CODFRN = :CODFRN
                             ORDER BY D.NOMFRN ASC]]>.Value

        parameterMapper.Parameters.Add(":CODFRN", codFrn)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql)).FirstOrDefault()

    End Function

    ''' <summary>
    ''' Buscar Fornecedores para o grid pelo codFrn
    ''' </summary>
    ''' <param name="codFrn"></param>
    ''' <returns></returns>
    Public Function BuscaFornecedores(codFrn As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT D.CODFRN, D.NOMFRN,
                             0 AS VLRUTZCTTACOCMC, 0 AS VLRUTZEXACTTACOCMC, 0 AS VLRPTCFRNACOCMC,
                             0 AS VLRUTZCTTACOCMC, 0 AS VLRUTZRCTCSTMER, 0 AS VLRTXAADMFRNACOCMC,
                             0 AS VLRTXAADMBDEACOCMC
                             FROM MRT.T0123183 A,           
                             MRT.T0118570 B,            
                             MRT.T0113625 C,          
                             MRT.T0100426 D         
                             WHERE A.CODDRTCMP = B.CODDRTCMP 
                             AND B.CODGERPRD = C.CODGERPRD
                             AND C.CODCPR = D.CODCPR       
                             AND NVL(D.TIPIDTEMPASCACOCMC, 0) = 0 
                             AND D.DATDSTDIVFRN IS NULL]]>.Value
        If codFrn IsNot Nothing Then
            sql += " AND D.CODFRN = :CODFRN "
        Else
            sql += " AND UPPER(D.NOMFRN) Like '%%'"
        End If

        sql += " ORDER BY D.NOMFRN ASC"

        parameterMapper.Parameters.Add(":CODFRN", codFrn)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function BuscaFornecedoresEdl(codFrn As Decimal?) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT B.CODFRN
                                        , C.RAZSOCFRN AS NOMFRN
                                        , 0 AS VLRUTZCTTACOCMC
                                        , 0 AS VLRUTZEXACTTACOCMC
                                        , 0 AS VLRPTCFRNACOCMC
                                        , 0 AS VLRUTZCTTACOCMC 
                                        , 0 AS VLRUTZRCTCSTMER
                                        , 0 AS VLRTXAADMFRNACOCMC
                                        , 0 AS VLRTXAADMBDEACOCMC
                                    FROM MRT.CADPRDSMA A
                                    INNER JOIN MRT.RLCPRDFRNPLOSMA B ON B.CODPRD = A.CODPRD AND B.DATDST IS NULL
                                    INNER JOIN MRT.T0148968 C ON C.CODFRN = B.CODFRN AND C.DATDSTFRN IS NULL
                                    INNER JOIN MRT.RLCCNDPGTCPTBTB D ON D.CODFRN = B.CODFRN AND D.DATDST IS NULL
                                    WHERE A.DATDST IS NULL]]>.Value

        If codFrn IsNot Nothing Then
            sql += " AND B.CODFRN = :CODFRN "
        End If

        sql += " GROUP BY A.CODPRD, A.DESPRD, A.CODUNDVND, A.QDEUNDVNDMER, B.CODFRN,C.RAZSOCFRN,A.CODEANPRD ORDER BY C.RAZSOCFRN ASC"

        parameterMapper.Parameters.Add(":CODFRN", codFrn)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' Buscar Fornecedores para o grid pelo codigo da Celula
    ''' </summary>
    ''' <param name="codCelula"></param>
    ''' <returns></returns>
    Public Function BuscaFornecedoresDaCelula(codCelula As Decimal) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT D.CODFRN, D.NOMFRN,
                             0 AS VLRUTZCTTACOCMC, 0 AS VLRUTZEXACTTACOCMC, 0 AS VLRPTCFRNACOCMC,
                             0 AS VLRUTZCTTACOCMC, 0 AS VLRUTZRCTCSTMER, 0 AS VLRTXAADMFRNACOCMC,
                             0 AS VLRTXAADMBDEACOCMC           
                             FROM MRT.T0123183 A,           
                             MRT.T0118570 B,            
                             MRT.T0113625 C,          
                             MRT.T0100426 D
                             WHERE A.CODDRTCMP = B.CODDRTCMP
                             AND B.CODGERPRD = C.CODGERPRD 
                             AND C.CODCPR = D.CODCPR       
                             AND NVL(D.TIPIDTEMPASCACOCMC, 0) = 0 
                             AND D.DATDSTDIVFRN IS NULL     
                             AND B.CODDIVCMP = :CODDIVCMP
                             ORDER BY D.NOMFRN ASC]]>.Value

        parameterMapper.Parameters.Add(":CODDIVCMP", codCelula)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' Buscar fornecedores do grid pelo codigo da diretoria
    ''' </summary>
    ''' <param name="codDiretoria"></param>
    ''' <returns></returns>
    Public Function BuscaFornecedoresDaDiretoria(codDiretoria As Decimal) As List(Of VO.RelacaoAcaoComercialxFornecedores)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT D.CODFRN, D.NOMFRN,
                             0 AS VLRUTZCTTACOCMC, 0 AS VLRUTZEXACTTACOCMC, 0 AS VLRPTCFRNACOCMC,
                             0 AS VLRUTZCTTACOCMC, 0 AS VLRUTZRCTCSTMER, 0 AS VLRTXAADMFRNACOCMC,
                             0 AS VLRTXAADMBDEACOCMC        
                             FROM MRT.T0123183 A,           
                             MRT.T0118570 B,            
                             MRT.T0113625 C,          
                             MRT.T0100426 D             
                             WHERE A.CODDRTCMP = B.CODDRTCMP 
                             AND B.CODGERPRD = C.CODGERPRD 
                             AND C.CODCPR = D.CODCPR       
                             AND NVL(D.TIPIDTEMPASCACOCMC, 0) = 0 
                             AND D.DATDSTDIVFRN IS NULL     
                             AND A.CODDRTCMP = :CODDRTCMP
                             ORDER BY D.NOMFRN ASC]]>.Value

        parameterMapper.Parameters.Add(":CODDRTCMP", codDiretoria)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' BscDdoFrnEtr
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Public Function BuscarFornecedoresDaAcaoETR(id As Decimal?) As List(Of RelacaoAcaoComercialxFornecedores)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT 
                                           DISTINCT 
                                           REL.CODFRN, 																			
                                           FRN.NOMFRN,
                                           REL.CODMCOVBAFRN,
                                           REL.VLRUTZACOCMC,                                                          
                                           round(sum(case when VLRPTCCLIITEACOCMC = 0 then QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC                                                                   
                                           else QDEITEOCDACOCMC * vlrptcfrniteacocmc                                                                   
                                           end),2) AS VLR_TOTAL_ACAO,                                                                             
                                           ROUND(SUM((QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC) - VLRUTZOPEDSNACOCMC),2) AS SALDO_ACAO,               
                                           round((case when VLRPTCCLIITEACOCMC = 0                                                               
                                           then                                                                                                   
                                           ROUND(REL.VLRUTZACOCMC / sum(ACO.QDEITEOCDACOCMC * ACO.VLRUNTITEOCDACOCMC ),2) * 100                   
                                           else                                                                                                   
                                           ROUND(REL.VLRUTZACOCMC / sum(ACO.QDEITEOCDACOCMC * vlrptcfrniteacocmc),2) * 100                        
                                           end),2) AS PERCENTUAL,                                                                         
                                           ROUND((SUM((QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC) - VLRUTZOPEDSNACOCMC)) * ROUND((REL.VLRUTZACOCMC / SUM(ACO.QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC)),2),2) AS VALOR_ESTORNAR,
                                           E.VLRPTCFRNACOCMC,
                                           E.VLRTXAADMBDEACOCMC,                                                                 
                                           E.VLRUTZCTTACOCMC,
                                           E.VLRUTZEXACTTACOCMC + E.VLRUTZRCTCSTMER as VLRUTZEXACTTACOCMC,
                                           E.VLRSLDDSNACR,
                                           E.TIPDSNDSCBNF,
                                           E.TIPFRNACOCMC,
                                           E.PERPTCFRNACOCMC
                                     FROM MRT.T0138776 ACO                                                                                  
                                     INNER JOIN MRT.RLCACOFRNMCO REL ON REL.CODACOCMC = ACO.CODACOCMC                                                                       
                                     INNER JOIN MRT.T0100426 FRN ON REL.CODFRN = FRN.CODFRN                                                                             
                                     INNER JOIN MRT.T0155573 OPE ON OPE.CODOPEFSCDSNACOCMC = ACO.CODOPEFSCDSNACOCMC AND OPE.INDBDEOPEDSNACOCMC <> 1   
                                     INNER JOIN MRT.T0138768 E ON E.CODACOCMC = ACO.CODACOCMC
                                     WHERE REL.CODACOCMC = :CODACOCMC
                                     GROUP BY REL.CODFRN, FRN.NOMFRN, REL.CODMCOVBAFRN, REL.VLRUTZACOCMC,  ACO.VLRPTCCLIITEACOCMC, 
                                     E.VLRPTCFRNACOCMC, E.VLRTXAADMBDEACOCMC,   E.VLRUTZCTTACOCMC, E.VlrSldDsnAcr, E.VLRUTZEXACTTACOCMC, E.VLRUTZRCTCSTMER, E.TIPDSNDSCBNF]]>.Value


        parameterMapper.Parameters.Add(":CODACOCMC", id)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    ''' <summary>
    ''' BscDdoFrnEtr
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Public Function BuscarFornecedoresDaAcaoBonificacao(id As Decimal?) As List(Of RelacaoAcaoComercialxFornecedores)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT  DISTINCT 
                                             REL.CODFRN 																			
                                           , FRN.NOMFRN
                                           , REL.NUMPEDCMP
                                           , REL.VLRUTZACOCMC                                                          
                                           , ROUND(SUM(CASE WHEN VLRPTCCLIITEACOCMC = 0 then QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC                                                                   
                                             else QDEITEOCDACOCMC * vlrptcfrniteacocmc                                                                   
                                             end),2) AS VLR_TOTAL_ACAO                                                                             
                                           , ROUND(SUM((QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC) - VLRUTZOPEDSNACOCMC),2) AS SALDO_ACAO               
                                           , ROUND((CASE WHEN VLRPTCCLIITEACOCMC = 0                                                               
                                             THEN                                                                                                   
                                             ROUND(REL.VLRUTZACOCMC / sum(ACO.QDEITEOCDACOCMC * ACO.VLRUNTITEOCDACOCMC ),2) * 100                   
                                             ELSE                                                                                                   
                                             ROUND(REL.VLRUTZACOCMC / sum(ACO.QDEITEOCDACOCMC * vlrptcfrniteacocmc),2) * 100                        
                                             end),2) AS PERCENTUAL                                                                           
                                           , ROUND((SUM((QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC) - VLRUTZOPEDSNACOCMC)) * ROUND((REL.VLRUTZACOCMC / SUM(ACO.QDEITEOCDACOCMC * VLRUNTITEOCDACOCMC)),2),2) AS VALOR_ESTORNAR
                                           , E.VLRPTCFRNACOCMC
                                           , E.VLRTXAADMBDEACOCMC                             
                                           , E.VLRUTZCTTACOCMC
                                           , E.VLRUTZEXACTTACOCMC + E.VLRUTZRCTCSTMER as VLRUTZEXACTTACOCMC
                                           , E.VLRSLDDSNACR
                                           , E.TIPDSNDSCBNF
                                           , E.TIPFRNACOCMC
                                           , E.PERPTCFRNACOCMC
                                     FROM MRT.T0138776 ACO                                                                                                                                                    
                                     INNER JOIN MRT.RLCACOCMCPEDCMP REL ON REL.CODACOCMC = ACO.CODACOCMC
                                     INNER JOIN MRT.T0100426 FRN ON REL.CODFRN = FRN.CODFRN                                                                             
                                     INNER JOIN MRT.T0155573 OPE ON OPE.CODOPEFSCDSNACOCMC = ACO.CODOPEFSCDSNACOCMC AND OPE.INDBDEOPEDSNACOCMC <> 1   
                                     INNER JOIN MRT.T0138768 E ON E.CODACOCMC = ACO.CODACOCMC
                                     WHERE REL.CODACOCMC = :CODACOCMC
                                     GROUP BY REL.CODFRN, FRN.NOMFRN, REL.NUMPEDCMP, REL.VLRUTZACOCMC,  ACO.VLRPTCCLIITEACOCMC, E.VLRPTCFRNACOCMC, 
                                     E.VLRTXAADMBDEACOCMC, E.VLRUTZCTTACOCMC, E.VlrSldDsnAcr, E.VLRUTZEXACTTACOCMC, E.VLRUTZRCTCSTMER, E.TIPDSNDSCBNF]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", id)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function



    ''' <summary>
    ''' BscDdoFrnEtr
    ''' </summary>
    ''' <param name="CodFrn"></param>
    ''' <returns></returns>
    Public Function BscUndNgcFrn(CodFrn As Decimal?) As List(Of RelacaoAcaoComercialxFornecedores)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT D.CODUNDESRNGC,  E.DESUNDESRNGC                    
                            FROM MRT.T0100426 A,                     
                            MRT.T0113625 B,                     
                            MRT.T0118570 C,                     
                            MRT.T0123183 D,                     
                            MRT.T0131275 E                      
                            WHERE A.codfrn = :CODFRN
                            AND A.codcpr = B.codcpr            
                            AND B.codgerprd = C.codgerprd      
                            AND C.coddrtcmp = D.coddrtcmp      
                            AND D.codundesrngc = E.codundesrngc]]>.Value

        parameterMapper.Parameters.Add(":CODFRN", CodFrn)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODUNDESRNGC", "DESUNDESRNGC"}))
    End Function

    ''' <summary>
    ''' BscDdoFrnEtr
    ''' </summary>
    ''' <param name="CodFrn"></param>
    ''' <returns></returns>
    Public Function BscDesDsnDscBnfCtt(CodFrn As Decimal?) As List(Of RelacaoAcaoComercialxFornecedores)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT(I.TipDsnDscBnf) , K.DesDsnDscBnf                              
                            FROM   MRT.T0124945 B                              
                            INNER JOIN MRT.T0124961  C  ON C.NumCttFrn = B.NumCttFrn               
                            INNER JOIN  MRT.T0124996 D  ON   D.NUMCTTFRN = C.NUMCTTFRN  AND  D.NUMCSLCTTFRN = C.NUMCSLCTTFRN       
                            INNER JOIN  MRT.T0151829 E  ON   E.NUMCTTFRN = D.NUMCTTFRN  AND  E.NUMCSLCTTFRN = D.NUMCSLCTTFRN       
                                AND  E.TIPEDEABGMIX = D.TIPEDEABGMIX  AND  E.CODEDEABGMIX = D.CODEDEABGMIX  AND  E.TIPEDEABGMIX = 1                    
                            INNER JOIN  MRT.T0151810 F  ON   F.CODREQALCVBAFRN = E.CODREQALCVBAFRN  AND  F.CODSTAALCVBAFRN = 4                 
                            INNER JOIN  MRT.T0151853 G  ON   G.CODREQALCVBAFRN = F.CODREQALCVBAFRN 
                            INNER JOIN  MRT.T0151799 H  ON  H.TIPALCVBAFRN = G.TIPALCVBAFRN  AND H.INDUTZVBAMKT = 1                     
                            INNER JOIN  MRT.T0124953 I  ON  I.NUMCSLCTTFRN = E.NUMCSLCTTFRN  AND I.INDUTZCSLALCVBAFRN = 1               
                            INNER JOIN  MRT.t0118074 J  ON   J.CODFILEMP = 1                       
                            INNER JOIN  MRT.T0109059 K  ON   K.TIPDSNDSCBNF = I.TIPDSNDSCBNF 
                            WHERE B.CODFRN = :CODFRN ]]>.Value

        parameterMapper.Parameters.Add(":CODFRN", CodFrn)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"TIPDSNDSCBNF", "DESDSNDSCBNF"}))
    End Function

    ''' <summary>
    ''' BscDdoFrnEtr
    ''' </summary>
    ''' <param name="CodFrn"></param>
    ''' <param name="iTipDsnDscBnf"></param>
    ''' <param name="iAnoMesRef"></param>
    ''' <returns></returns>
    Public Function BscSldCttFrn(CodFrn As Decimal?, iTipDsnDscBnf As Integer,
                                       iAnoMesRef As Integer) As List(Of RelacaoAcaoComercialxFornecedores)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                            (
                                TRUNC(COALESCE(L.VLRSLDMESANT, 0) - COALESCE(L.VLRDBTMESCRR, 0) + COALESCE(L.VLRCRDMESCRR, 0),2) - 
                                (SELECT COALESCE(TRUNC(SUM(M.VLRSLDRSVFRN), 2), 0)                                                 
                                FROM MRT.T0123159 M
                                    WHERE M.FLGAPVACOCMC = 'N'
                                AND M.CODFRN = B.CODFRN
                                AND M.TIPDSNDSCBNF = K.TIPDSNDSCBNF) +                                                         
                            (SELECT TRUNC((COALESCE(SUM(CASE WHEN N.CODSITPMS = 0 OR N.CODSITPMS = 1 THEN                      
                                (O.VLRNGCPMS) - (O.VLRPGOPMS) - ((O.VLRNGCPMS - O.VLRPGOPMS)   
                                * (P.PERJURSLDNEG + P.PERPIS) / 100)                           
                            ELSE                                                              
                                (O.VLREFTPMS) - (O.VLRPGOPMS) - ((O.VLREFTPMS - O.VLRPGOPMS)   
                                * (P.PERJURSLDNEG + P.PERPIS) / 100)                           
                                END),0) * G.PerAlcVbaFrn / 100),2)                                    
                                FROM MRT.T0118058 N,                                              
                                    MRT.T0118066 O,                                              
                                    MRT.T0118074 P,                                              
                                    MRT.T0109059 Q                                               
                            WHERE (O.CODEMP = N.CODEMP)                                        
                                AND (N.INDASCARDFRNPMS IS NULL OR N.INDASCARDFRNPMS = 0)         
                                AND O.CODFILEMP = N.CODFILEMP                                    
                                AND O.CODPMS = N.CODPMS                        
                                AND O.DATNGCPMS = N.DATNGCPMS                  
                                AND P.CODFILEMP = 1                            
                                AND N.CODFRN = B.CODFRN                        
                                AND N.CODSITPMS IN (0, 1, 3)                   
                                AND O.TIPDSNDSCBNF = K.TIPDSNDSCBNF            
                                AND O.TIPDSNDSCBNF = Q.TIPDSNDSCBNF            
                                AND Q.FLGCTBDSNDSC = 'S')
                            ) VLRSLDDSP,          
                                        B.CODFRN                                       
                                FROM MRT.T0124945 B                                     
                            INNER JOIN MRT.T0124961 C                                
                                    ON C.NUMCTTFRN = B.NUMCTTFRN                       
                            INNER JOIN MRT.T0124996 D                                
                                    ON D.NUMCTTFRN = C.NUMCTTFRN                       
                                    AND D.NUMCSLCTTFRN = C.NUMCSLCTTFRN                 
                            INNER JOIN MRT.T0151829 E                                
                                    ON E.NUMCTTFRN = D.NUMCTTFRN                       
                                    AND E.NUMCSLCTTFRN = D.NUMCSLCTTFRN                 
                                    AND E.TIPEDEABGMIX = D.TIPEDEABGMIX                 
                                    AND E.CODEDEABGMIX = D.CODEDEABGMIX                 
                                    AND E.TIPEDEABGMIX = 1                              
                            INNER JOIN MRT.T0151810 F                                
                                    ON F.CODREQALCVBAFRN = E.CODREQALCVBAFRN   
                                    AND F.CODSTAALCVBAFRN = 4                   
                            INNER JOIN MRT.T0151853 G                        
                                    ON G.CODREQALCVBAFRN = F.CODREQALCVBAFRN   
                                    AND G.TIPALCVBAFRN = 2                
                            INNER JOIN MRT.T0151799 H                  
                                    ON H.TIPALCVBAFRN = G.TIPALCVBAFRN   
                                    AND H.INDUTZVBAMKT = 1                
                            INNER JOIN MRT.T0124953 I                  
                                    ON I.NUMCSLCTTFRN = E.NUMCSLCTTFRN   
                                    AND I.INDUTZCSLALCVBAFRN = 1          
                                    AND I.TIPDSNDSCBNF =  :ITIPDSNDSCBNF 
                            INNER Join MRT.t0118074 J                  
                                    On J.CODFILEMP = 1                   
                            INNER Join MRT.T0109059 K                  
                                    On K.TIPDSNDSCBNF = I.TIPDSNDSCBNF   
                                Left Join MRT.T0155271 L                  
                                    On B.CODFRN = L.CODFRN               
                                    And I.TIPDSNDSCBNF = L.TIPDSNDSCBNF   
                                    And G.TIPALCVBAFRN = L.TIPALCVBAFRN   
                                    And L.ANOMESREF =  :IANOMESREF 
                                    WHERE B.CODFRN = :CODFRN]]>.Value


        parameterMapper.Parameters.Add(":CODFRN", CodFrn)
        parameterMapper.Parameters.Add(":ITIPDSNDSCBNF", iTipDsnDscBnf)
        parameterMapper.Parameters.Add(":IANOMESREF", iAnoMesRef)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFRN", "VLRSLDDSNCTTACOCMC"}))

    End Function

    ''' <summary>
    ''' BscDdoFrnEtr
    ''' </summary>
    ''' <param name="CodFrn"></param>
    ''' <param name="iTipDsnDscBnf"></param>
    ''' <param name="iAnoMesRef"></param>
    ''' <returns></returns>
    Public Function BscSldCntCrrFrn(CodFrn As Decimal?, iTipDsnDscBnf As Integer,
                                       iAnoMesRef As Integer) As List(Of RelacaoAcaoComercialxFornecedores)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT C.CODFRN
                                          , FRN.NOMFRN
                                          , F.TIPDSNDSCBNF
                                          , F.DESDSNDSCBNF
                                          , (
                                                (C.VLRSLDMESANT + C.VLRCRDMESCRR - C.VLRDEBMESCRR) -                      
                                                (SELECT COALESCE(TRUNC(SUM(D.VLRSLDRSVFRN), 2), 0)                         
                                                    FROM MRT.T0123159 D 
                                                   WHERE D.FLGAPVACOCMC = 'N'
                                                     AND D.CODFRN = C.CODFRN 
                                                     AND D.TIPDSNDSCBNF = F.TIPDSNDSCBNF
                                                ) +                                
                                                (SELECT TRUNC(COALESCE(SUM(                                                
                                                        CASE WHEN A.CODSITPMS = 0 OR A.CODSITPMS = 1 THEN                 
                                                            (B.VLRNGCPMS) - (B.VLRPGOPMS) - ((B.VLRNGCPMS - B.VLRPGOPMS)  
                                                            * (J.PERJURSLDNEG + J.PERPIS) / 100)                          
                                                        ELSE                                                              
                                                            (B.VLREFTPMS) - (B.VLRPGOPMS) - ((B.VLREFTPMS - B.VLRPGOPMS)  
                                                            * (J.PERJURSLDNEG + J.PERPIS) / 100)                
                                                        END),0),2)                                              
                                                   FROM MRT.T0118058 A,
                                                        MRT.T0118066 B,
                                                        MRT.T0118074 J,
                                                        MRT.T0109059 X 
                                                  WHERE (B.CODEMP = A.CODEMP)                                   
                                                    AND B.CODFILEMP = A.CODFILEMP                        
                                                    AND B.CODPMS = A.CODPMS                              
                                                    AND B.DATNGCPMS = A.DATNGCPMS                        
                                                    AND (A.INDASCARDFRNPMS IS NULL OR A.INDASCARDFRNPMS = 0)    
                                                    AND J.CODFILEMP = 1                                  
                                                    AND A.CODFRN = C.CODFRN                              
                                                    AND A.CODSITPMS IN (0, 1, 3)                         
                                                    AND B.TIPDSNDSCBNF = C.TIPDSNDSCBNF                  
                                                    AND B.TIPDSNDSCBNF = X.TIPDSNDSCBNF                  
                                                    AND X.FLGCTBDSNDSC = 'S'
                                                 )
                                            ) AS VLRSLDDSP              
                                       FROM MRT.T0123078 C
                                      INNER JOIN MRT.T0100426 FRN ON FRN.CODFRN = C.CODFRN
                                      INNER JOIN MRT.T0109059 F ON F.TIPDSNDSCBNF = C.TIPDSNDSCBNF       
                                      WHERE C.CODFRN = :CODFRN
                                        AND C.TIPDSNDSCBNF = :iTipDsnDscBnf 
                                        AND C.ANOMESREF = :iAnoMesRef 
                                        AND F.FLGCTBDSNDSC = 'S'                                     
                                      ORDER BY C.CODFRN
                                             , F.TIPDSNDSCBNF]]>.Value

        parameterMapper.Parameters.Add(":CODFRN", CodFrn)
        parameterMapper.Parameters.Add(":iTipDsnDscBnf", iTipDsnDscBnf)
        parameterMapper.Parameters.Add(":iAnoMesRef", iAnoMesRef)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"VLRSLDDSP"}))

    End Function


    ''' <summary>
    ''' BscDdoFrnEtr
    ''' </summary>
    ''' <param name="CodFrn"></param>
    ''' <returns></returns>
    Public Function BscSldReceitaCMV(CodFrn As Decimal?) As List(Of RelacaoAcaoComercialxFornecedores)

        Dim ANOMESREF As String
        Dim strAux As String
        Dim dat As Date

        dat = Now
        strAux = Format(dat, "dd/MM/yyyy")
        ANOMESREF = strAux.Split("/")(2) & strAux.Split("/")(1)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT C.CODFRN, FRN.NOMFRN, F.TIPDSNDSCBNF, F.DESDSNDSCBNF,            
                   ((C.VLRSLDMESANT + C.VLRCRDMESCRR - C.VLRDEBMESCRR) -                     
                   (SELECT COALESCE(TRUNC(SUM(D.VLRSLDRSVFRN), 2), 0)                        
                       FROM MRT.T0123159 D WHERE D.FLGAPVACOCMC = 'N' AND D.CODFRN = C.CODFRN
                        AND D.TIPDSNDSCBNF = F.TIPDSNDSCBNF) +                               
                   (SELECT TRUNC(COALESCE(SUM(                                               
                            CASE WHEN A.CODSITPMS = 0 OR A.CODSITPMS = 1 THEN                
                                (B.VLRNGCPMS) - (B.VLRPGOPMS) - ((B.VLRNGCPMS - B.VLRPGOPMS) 
                                * (J.PERJURSLDNEG + J.PERPIS) / 100)                         
                            ELSE                                                             
                                (B.VLREFTPMS) - (B.VLRPGOPMS) - ((B.VLREFTPMS - B.VLRPGOPMS) 
                                * (J.PERJURSLDNEG + J.PERPIS) / 100)                         
                            END),0),2)                                                       
                       FROM MRT.T0118058 A,                                                  
                            MRT.T0118066 B,                                                  
                            MRT.T0118074 J,                                                  
                            MRT.T0109059 X                                                   
                      WHERE (B.CODEMP = A.CODEMP)                                            
                        AND (A.INDASCARDFRNPMS IS NULL OR A.INDASCARDFRNPMS = 0)             
                        AND B.CODFILEMP = A.CODFILEMP                                        
                        AND B.CODPMS = A.CODPMS                                              
                        AND B.DATNGCPMS = A.DATNGCPMS                                        
                        AND J.CODFILEMP = 1                                                  
                        AND A.CODFRN = C.CODFRN                                              
                        AND A.CODSITPMS IN (0, 1, 3)                                         
                        AND B.TIPDSNDSCBNF = C.TIPDSNDSCBNF                                  
                        AND B.TIPDSNDSCBNF = X.TIPDSNDSCBNF                                  
                        AND X.FLGCTBDSNDSC = 'S')) AS VLRSLDDSP                              
               FROM MRT.T0123078 C, MRT.T0100426 FRN, MRT.T0109059 F
               WHERE C.CODFRN = :CODFRN
                AND C.ANOMESREF  = :ANOMESREF
                AND FRN.CODFRN = C.CODFRN                                                    
                AND F.TIPDSNDSCBNF = C.TIPDSNDSCBNF                                          
                AND F.FLGCTBDSNDSC = 'S'                                                     
                AND F.INDTIPDSNRCTCSTMER = 1                                                 
            ORDER BY C.CODFRN, F.TIPDSNDSCBNF]]>.Value

        parameterMapper.Parameters.Add(":CODFRN", CodFrn)
        parameterMapper.Parameters.Add(":ANOMESREF", ANOMESREF)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"VLRSLDDSP", "CODFRN", "NOMFRN", "TIPDSNDSCBNF", "DESDSNDSCBNF"}))

    End Function

    Public Function CnsSldRsvAcoCmc(ByVal CodAcoCmc As Integer) As List(Of RelacaoAcaoComercialxFornecedores)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODFRN, 
                                            TIPDSNDSCBNF as EmpenhoCtt, 
                                            TIPDSNDSCBNFEXACTT as EmpenhoExaCtt, 
                                            TIPDSNDSCBNFACR AS EmpenhoCMV, 
                                            VLRUTZCTTACOCMC as VlrAcordo, 
                                            VLRUTZEXACTTACOCMC as VlrAcordoExa, 
                                            VLRUTZRCTCSTMER as VlrReceitaCMV, 
                                            VLRPTCFRNACOCMC as VlrParticipacao, 
                                            VLRTXAADMFRNACOCMC as VlrTxaAdm, 
                                            DESBFCFRNACOCMC, 
                                            VLRTXAADMBDEACOCMC as VlrTxaBde 
                                            FROM MRT.T0138768 A 
                                            WHERE CODACOCMC = :CodAcoCmc
                                            GROUP BY CODFRN, TIPDSNDSCBNF, TIPDSNDSCBNFEXACTT, TIPDSNDSCBNFACR, VLRUTZCTTACOCMC, 
                                            VLRUTZEXACTTACOCMC, VLRUTZRCTCSTMER, VLRPTCFRNACOCMC, VLRTXAADMFRNACOCMC, DESBFCFRNACOCMC, VLRTXAADMBDEACOCMC ]]>.Value


        parameterMapper.Parameters.Add(":CodAcoCmc", CodAcoCmc)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function BuscarFornecedoresDaAcaoFluxo(ByVal CodAcoCmc As Decimal) As List(Of RelacaoAcaoComercialxFornecedores)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC,                                                                 
                                     A.VLRTOTACOCMC,                                                                  
                                     A.VLRTOTTXAADMACOCMC,                                                           
                                     A.VLRTOTBDEACOCMC,                                                               
                                     E.CODFRN,                                                                           
                                     (Case WHEN e.codfrn = 999999 then 'DESPESAS MARTINS' 
                                      WHEN F.NOMFRN IS NULL then J.RAZSOCFRN ELSE F.NOMFRN END) AS NOMFRN,  
                                     F.CODEMP,                                                                           
                                     E.INDATDDTOFRN,                                                                     
                                     E.VLRPTCFRNACOCMC,                                                                  
                                     E.VLRPTCFRNACOCMC as strVlrPtcFrn,                                           
                                     E.VLRTXAADMFRNACOCMC,                                                               
                                     E.VLRSLDCNTCRRFRN,                                                                  
                                     E.VLRSLDDSNDSCACOCMC,                                                          
                                     E.TIPDSNDSCBNF,                                                                     
                                     G.DESDSNDSCBNF,                                                                     
                                     E.VLRTXAADMBDEACOCMC,                                                              
                                     E.VLRUTZCTTACOCMC,                                                                
                                     E.VLRUTZEXACTTACOCMC,                                                             
                                     E.TIPDSNDSCBNFEXACTT,                                                               
                                     H.DESDSNDSCBNF AS DESDSNDSCBNFEXACTT,                                               
                                     E.CODUNDESRNGC,                                                                     
                                     B.DESUNDESRNGC,                                                                     
                                     --to_char(E.VLRSLDDSNCTTACOCMC, '999999990D00') as VLRSLDDSNCTTACOCMC,                
                                     E.VLRSLDDSNCTTACOCMC,
                                     e.DESBFCFRNACOCMC,                                                                  
                                     e.TipDsnDscBnfAcr,                                                                  
                                     e.VlrUtzRctCstMer,                                                                  
                                     e.VlrSldDsnAcr,                                                                     
                                     e.DesSelFrn,
                                     e.tipfrnacocmc
                                     FROM MRT.T0118430 A                                                                
                                     inner JOIN MRT.T0138768 E                                                          
                                     ON E.CODACOCMC = A.CODACOCMC                                                       
                                     LEFT JOIN MRT.T0100426 F                                                           
                                     ON F.CODFRN = E.CODFRN                                                             
                                     LEFT JOIN MRT.T0109059 G                                                           
                                     ON E.TIPDSNDSCBNF = G.TIPDSNDSCBNF                                                 
                                     LEFT JOIN MRT.T0109059 H                                                           
                                     ON E.TIPDSNDSCBNFEXACTT = H.TIPDSNDSCBNF                                           
                                     LEFT JOIN MRT.T0131275 B                                                           
                                     ON E.CODUNDESRNGC = B.CODUNDESRNGC 
                                     LEFT JOIN MRT.T0148968 J ON J.CODFRN = E.CODFRN
                                     WHERE A.CODACOCMC = :CODACOCMC]]>.Value


        parameterMapper.Parameters.Add(":CODACOCMC", CodAcoCmc)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


End Class


<TableAttribute("MRT.T0138768")>
Public Class RelacaoAcaoComercialxFornecedoresDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODFRN As Nullable(Of Decimal)

    <IgnoreMap>
    Property VLRTOTACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLRTOTTXAADMACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLRTOTBDEACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODEMP As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESDSNDSCBNF As String
    <IgnoreMap>
    Property DESDSNDSCBNFEXACTT As String
    <IgnoreMap>
    Property DESUNDESRNGC As String
    <IgnoreMap>
    Property NOMFRN As String
    <IgnoreMap>
    Property CODNOMFRN As String

End Class


