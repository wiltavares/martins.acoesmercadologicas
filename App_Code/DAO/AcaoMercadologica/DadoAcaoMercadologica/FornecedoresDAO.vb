﻿Imports Core
Imports VO

Public Class FornecedoresDAO
    Inherits DAOBase(Of VO.Fornecedores)

    Public Sub New()
        daoConfiguration = New FornecedoresDAOConfiguration()
    End Sub

    Public Function BuscarNomeCodigoFornecedores(codFrn As Decimal?) As List(Of Fornecedores)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODFRN, NOMFRN    
                            FROM MRT.T0100426
                            WHERE (:CODFRN IS NULL OR CODFRN = :CODFRN)]]>.Value

        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(codFrn))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function
End Class


<TableAttribute("MRT.T0100426")>
Public Class FornecedoresDAOConfiguration
    Inherits DAOConfiguration

    <DateInsert>
    <[Readonly](False, True)>
    Property DATCADFRN As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATFCHANOFSC As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATDSTDIVFRN As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATHRAISRMOVITFWMS As Date

End Class
