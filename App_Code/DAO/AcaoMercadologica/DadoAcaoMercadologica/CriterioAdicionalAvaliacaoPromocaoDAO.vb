﻿Imports Core

Public Class CriterioAdicionalAvaliacaoPromocaoDAO
    Inherits DAOBase(Of VO.CriterioAdicionalAvaliacaoPromocao)

    Public Sub New()
        daoConfiguration = New CriterioAdicionalAvaliacaoPromocaoDAOConfiguration()
    End Sub



End Class

<TableAttribute("MRT.T0132115")>
Public Class CriterioAdicionalAvaliacaoPromocaoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)


End Class



