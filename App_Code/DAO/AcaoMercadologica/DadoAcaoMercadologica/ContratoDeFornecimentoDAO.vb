﻿Imports Core
Public Class ContratoDeFornecimentoDAO
    Inherits DAOBase(Of VO.ContratoDeFornecimento)

    Public Sub New()
        daoConfiguration = New ContratoDeFornecimentoDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0124945")>
Public Class ContratoDeFornecimentoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property NUMCTTFRN As Nullable(Of Decimal)


    <DateInsert>
    <[Readonly](False, True)>
    Property DATCADCTTFRN As Date


    <DateInsert>
    <[Readonly](False, True)>
    Property DATINIPODVGRCTTFRN As Date


    <DateInsert>
    <[Readonly](False, True)>
    Property DATVNCCTTFRN As Date


    <DateInsert>
    <[Readonly](False, True)>
    Property DATDSTCTTFRN As Date


    <DateInsert>
    <[Readonly](False, True)>
    Property DATALAPGCCTTFRN As Date

End Class
