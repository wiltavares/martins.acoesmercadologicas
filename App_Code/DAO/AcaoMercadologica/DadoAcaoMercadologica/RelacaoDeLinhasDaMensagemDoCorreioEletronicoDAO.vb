﻿Imports Core
Public Class RelacaoDeLinhasDaMensagemDoCorreioEletronicoDAO
    Inherits DAOBase(Of VO.RelacaoDeLinhasDaMensagemDoCorreioEletronico)

    Public Sub New()
        daoConfiguration = New RelacaoDeLinhasDaMensagemDoCorreioEletronicoDAOConfiguration()
    End Sub

    Public Function IsrLnhCreEtn(item As VO.RelacaoDeLinhasDaMensagemDoCorreioEletronico)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[ INSERT INTO MRT.T0134290 (TIPMSGCREETN, 
                                                                NUMSEQMSGCREETN, 
                                                                NUMSEQLNHFISCREETN, 
                                                                DESTXTLNHFISCREETN) 
                                                        VALUES (:TIPMSGCREETN,
                                                                :NUMSEQMSGCREETN,
                                                                :NUMSEQLNHFISCREETN,
                                                                :DESTXTLNHFISCREETN)]]>.Value

        parameterMapper.Parameters.Add(":TIPMSGCREETN", item.TIPMSGCREETN)
        parameterMapper.Parameters.Add(":NUMSEQMSGCREETN", item.NUMSEQMSGCREETN)
        parameterMapper.Parameters.Add(":NUMSEQLNHFISCREETN", item.NUMSEQLNHFISCREETN)
        parameterMapper.Parameters.Add(":DESTXTLNHFISCREETN", item.DESTXTLNHFISCREETN)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

End Class


<TableAttribute("MRT.T0134290")>
Public Class RelacaoDeLinhasDaMensagemDoCorreioEletronicoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property TIPMSGCREETN As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMSEQMSGCREETN As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMSEQLNHFISCREETN As Nullable(Of Decimal)

End Class