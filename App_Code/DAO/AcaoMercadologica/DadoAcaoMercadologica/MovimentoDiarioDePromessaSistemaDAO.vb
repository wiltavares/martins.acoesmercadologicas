﻿Imports Core
Public Class MovimentoDiarioDePromessaSistemaDAO
    Inherits DAOBase(Of VO.MovimentoDiarioDePromessaSistema)

    Public Sub New()
        daoConfiguration = New MovimentoDiarioDePromessaSistemaDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0118058")>
Public Class MovimentoDiarioDePromessaSistemaDAOConfiguration
    Inherits DAOConfiguration


    <DateInsert>
    <[Readonly](False, True)>
    Property DATNGCPMS As Date


    <DateInsert>
    <[Readonly](False, True)>
    Property DATEFTPMS As Date


    <DateInsert>
    <[Readonly](False, True)>
    Property DATCNCPED As Date


    <DateInsert>
    <[Readonly](False, True)>
    Property DATCADPMS As Date

End Class

