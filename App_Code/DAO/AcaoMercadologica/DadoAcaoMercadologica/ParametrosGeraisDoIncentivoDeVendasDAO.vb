﻿Imports Core
Public Class ParametrosGeraisDoIncentivoDeVendasDAO
    Inherits DAOBase(Of VO.ParametrosGeraisDoIncentivoDeVendas)

    Public Sub New()
        daoConfiguration = New ParametrosGeraisDoIncentivoDeVendasDAOConfiguration()
    End Sub
    ''' <summary>
    ''' busca dados dos incentivos baseado no id ou nome dele
    ''' </summary>
    ''' <param name="codIct"></param>
    ''' <param name="nomIct"></param>
    ''' <returns></returns>
    Public Function BuscarIncentivos(Optional codIct As Integer? = Nothing, Optional nomIct As String = Nothing) As List(Of VO.ParametrosGeraisDoIncentivoDeVendas)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        If (Not IsNothing(nomIct)) Then
            nomIct = "%" + nomIct + "%"
        End If

        Dim sql As String = <![CDATA[SELECT CODICT, DESICT
                            FROM MRT.T0160461                       
                            WHERE (:CODICT IS NULL OR CODICT = :CODICT)
                            AND (:DESICT IS NULL OR UPPER(DESICT) like UPPER(:DESICT))]]>.Value

        parameterMapper.Parameters.Add(":CODICT", ValueDBProperty(codIct))
        parameterMapper.Parameters.Add(":DESICT", ValueDBProperty(nomIct))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


    Public Function BuscarValidadoresIcentivos(ByVal codIct As Integer) As List(Of VO.ParametrosGeraisDoIncentivoDeVendas)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT (SELECT COUNT(CODICT) FROM MRT.T0160461 WHERE CODICT = :CODICT AND CODSTAICT IN (2,3,4,5)) AS QTDCADICENTIVOS,
                                    (SELECT COUNT(A.CODICT) FROM MRT.T0160461 D 
                                                            ,MRT.T0160666 A 
                                                            ,MRT.T0160542 B 
                                                            ,MRT.T0160518 C 
                                                            WHERE 
                                                            D.CODICT = :CODICT
                                                            AND D.CODSTAICT  IN (2,3,4,5) 
                                                            AND A.CODICT    = D.CODICT 
                                                            AND B.CODIDTNATPUBALV = A.CODIDTNATPUBALV 
                                                            AND C.CODTIPNATPUBALV = B.CODTIPNATPUBALV 
                                                            AND C.CODTIPNATPUBALV IN (1,2))  AS QTDPUBLICOALVO,              
                                    (SELECT COUNT(A.CODICT) FROM MRT.T0160461 A, MRT.T0160658 B 
                                                            WHERE A.CODICT = :CODICT 
                                                            AND A.CODSTAICT  IN (2,3,4,5) 
                                                            AND B.CODICT = A.CODICT) AS QTDMERCADORIAS,
                                    (SELECT COUNT(A.CODICT) FROM MRT.T0160461 A, MRT.T0160721 B  WHERE  A.CODICT = :CODICT AND A.CODSTAICT  IN (2,3,4,5) AND B.CODICT = A.CODICT) AS QTDMETAS,
                                    (SELECT COUNT(A.CODICT) FROM MRT.T0160461 A, MRT.T0160747 B WHERE A.CODICT = :CODICT AND A.CODSTAICT  IN (2,3,4,5)  AND B.CODICT = A.CODICT) AS QTDCRITERIOS, 
                                    (SELECT COUNT(A.CODICT) FROM MRT.T0160461 A 
                                                            INNER JOIN MRT.T0160747 B ON B.CODICT = A.CODICT 
                                                            LEFT OUTER JOIN MRT.T0160763 C ON C.CODICT = B.CODICT AND C.NUMSEQCTRAPUEDEICT = B.NUMSEQCTRAPUEDEICT 
                                                        WHERE A.CODICT = :CODICT  
                                                            AND A.CODSTAICT  IN (2,3,4,5) AND C.NUMSEQCTRAPUEDEICT IS NULL) AS QTDCRITERIOSAPURACAO,
                                    (SELECT COUNT(A.CODICT) FROM MRT.T0160461 A 
                                                            INNER JOIN MRT.T0160747 B ON B.CODICT = A.CODICT 
                                                            LEFT OUTER JOIN MRT.T0160828 C ON C.CODICT = B.CODICT AND C.NUMSEQCTRAPUEDEICT = B.NUMSEQCTRAPUEDEICT 
                                                            INNER JOIN MRT.T0160836 D ON D.CODICT = C.CODICT AND D.NUMSEQCTRAPUEDEICT = C.NUMSEQCTRAPUEDEICT AND D.NUMSEQPRMICT = C.NUMSEQPRMICT 
                                                            WHERE A.CODICT = :CODICT
                                                            AND A.CODSTAICT  IN (2,3,4,5) 
                                                            AND B.CODICT = A.CODICT 
                                                            AND D.NUMSEQCTRAPUEDEICT IS NULL) AS QTDPREMIACAO                               
                                    FROM DUAL]]>.Value

        parameterMapper.Parameters.Add(":CODICT", ValueDBProperty(codIct))

        Return connector.SelectTList(Of VO.ParametrosGeraisDoIncentivoDeVendas)(sql, parameterMapper, RowMapperOnlyProperties({"QTDCADICENTIVOS", "QTDPUBLICOALVO", "QTDMERCADORIAS", "QTDMETAS", "QTDCRITERIOS", "QTDCRITERIOSAPURACAO", "QTDPREMIACAO"}))

    End Function


End Class


<TableAttribute("MRT.T0160461")>
Public Class ParametrosGeraisDoIncentivoDeVendasDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODICT As Nullable(Of Decimal)

    <DateInsert>
    <[Readonly](False, True)>
    Property DATFIMCADICT As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATFIMEFTICT As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATINICADICT As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATINIEFTICT As Date

End Class
