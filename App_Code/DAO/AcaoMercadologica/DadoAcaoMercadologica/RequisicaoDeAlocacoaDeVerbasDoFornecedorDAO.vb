﻿Imports Core
Public Class RequisicaoDeAlocacoaDeVerbasDoFornecedorDAO
    Inherits DAOBase(Of VO.RequisicaoDeAlocacoaDeVerbasDoFornecedor)

    Public Sub New()
        daoConfiguration = New RequisicaoDeAlocacoaDeVerbasDoFornecedorDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0151810")>
Public Class RequisicaoDeAlocacoaDeVerbasDoFornecedorDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODREQALCVBAFRN As Nullable(Of Decimal)

    <DateInsert>
    <[Readonly](False, True)>
    Property DATAPVALCVBAFRN As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATCADALCVBAFRN As Date

End Class
