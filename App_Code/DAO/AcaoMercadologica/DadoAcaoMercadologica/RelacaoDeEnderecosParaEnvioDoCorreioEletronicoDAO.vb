﻿Imports Core
Public Class RelacaoDeEnderecosParaEnvioDoCorreioEletronicoDAO
    Inherits DAOBase(Of VO.RelacaoDeEnderecosParaEnvioDoCorreioEletronico)

    Public Sub New()
        daoConfiguration = New RelacaoDeEnderecosParaEnvioDoCorreioEletronicoDAOConfiguration()
    End Sub


    Public Function IsrEndCreEtn(item As VO.RelacaoDeEnderecosParaEnvioDoCorreioEletronico)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[ INSERT INTO MRT.T0134282 (TIPMSGCREETN,           
                                                                NUMSEQMSGCREETN,           
                                                                TIPENDCREETN,        
                                                                NUMSEQENDCREETN,        
                                                                IDTENDCREETN)
                                                        VALUES (:TIPMSGCREETN,
                                                                :NUMSEQMSGCREETN,
                                                                :TIPENDCREETN,
                                                                :NUMSEQENDCREETN,
                                                                :IDTENDCREETN)]]>.Value

        parameterMapper.Parameters.Add(":TIPMSGCREETN", item.TIPMSGCREETN)
        parameterMapper.Parameters.Add(":NUMSEQMSGCREETN", item.NUMSEQMSGCREETN)
        parameterMapper.Parameters.Add(":TIPENDCREETN", item.TIPENDCREETN)
        parameterMapper.Parameters.Add(":NUMSEQENDCREETN", item.NUMSEQENDCREETN)
        parameterMapper.Parameters.Add(":IDTENDCREETN", item.IDTENDCREETN)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function



End Class


<TableAttribute("MRT.T0134282")>
Public Class RelacaoDeEnderecosParaEnvioDoCorreioEletronicoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property TIPMSGCREETN As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMSEQMSGCREETN As Nullable(Of Decimal)

    <PrimaryKey>
    Property TIPENDCREETN As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMSEQENDCREETN As Nullable(Of Decimal)

End Class
