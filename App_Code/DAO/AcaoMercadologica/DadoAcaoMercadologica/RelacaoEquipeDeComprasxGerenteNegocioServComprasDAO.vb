﻿Imports Core
Public Class RelacaoEquipeDeComprasxGerenteNegocioServComprasDAO
    Inherits DAOBase(Of VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras)

    Public Sub New()
        daoConfiguration = New RelacaoEquipeDeComprasxGerenteNegocioServComprasDAOConfiguration()
    End Sub

    Public Function ListDescricaoDivisaoDeCompra(vo As VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras) As List(Of VO.RelacaoEquipeDeComprasxGerenteNegocioServCompras)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[ SELECT CODDIVCMP
                                           , DESDIVCMP
                                           , CODGERPRD
                                           , CODDRTCMP
                                           , CODGRPMERFRC 
                                           , VLRMNMFATMER 
                                           , DATDSTDIVCMP 
                                           , PERRDCMRGBRTFOB  
                                        FROM MRT.T0118570 
                                       ORDER BY DESDIVCMP ASC]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class


<TableAttribute("MRT.T0118570")>
Public Class RelacaoEquipeDeComprasxGerenteNegocioServComprasDAOConfiguration
    Inherits DAOConfiguration

    <DateInsert>
    <[Readonly](False, True)>
    Property DATDSTDIVCMP As Date

End Class
