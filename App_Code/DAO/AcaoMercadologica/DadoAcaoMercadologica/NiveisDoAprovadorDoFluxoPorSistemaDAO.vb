﻿Imports Core
Public Class NiveisDoAprovadorDoFluxoPorSistemaDAO
    Inherits DAOBase(Of VO.NiveisDoAprovadorDoFluxoPorSistema)

    Public Sub New()
        daoConfiguration = New NiveisDoAprovadorDoFluxoPorSistemaDAOConfiguration()
    End Sub

    Public Function CnsApvNot(ByVal CodSisInf As Decimal) As List(Of VO.NiveisDoAprovadorDoFluxoPorSistema)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT NUMSEQNIVAPV, CODTIPAPV, CODFNCAPVFIX 
                                       FROM MRT.T0161581 WHERE CODSISINF = :CODSISINF 
                                        AND INDENVNOTAPV = 1 AND INDCTN IS NULL AND NOT CODFNCAPVFIX IS NULL ]]>.Value

        parameterMapper.Parameters.Add(":CODSISINF", CodSisInf)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


End Class


<TableAttribute("MRT.T0161581")>
Public Class NiveisDoAprovadorDoFluxoPorSistemaDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODSISINF As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMSEQNIVAPV As Nullable(Of Decimal)

End Class
