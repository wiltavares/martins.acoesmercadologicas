﻿Imports Core
Public Class GestaoDaCarteiraDePedidosDeComprasDAO
    Inherits DAOBase(Of VO.GestaoDaCarteiraDePedidosDeCompras)

    Public Sub New()
        daoConfiguration = New GestaoDaCarteiraDePedidosDeComprasDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0100981")>
Public Class GestaoDaCarteiraDePedidosDeComprasDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODEMP As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODCPR As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODFNC As Nullable(Of Decimal)

    <DateInsert>
    <[Readonly](False, True)>
    Property DATDSTCPR As Date

End Class

