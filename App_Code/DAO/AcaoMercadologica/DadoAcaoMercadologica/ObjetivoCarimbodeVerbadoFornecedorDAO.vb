﻿Imports Core
Public Class ObjetivoCarimbodeVerbadoFornecedorDAO
    Inherits DAOBase(Of VO.ObjetivoCarimbodeVerbadoFornecedor)

    Public Sub New()
        daoConfiguration = New ObjetivoCarimbodeVerbadoFornecedorDAOConfiguration()
    End Sub

    Public Function ListObjetivos(vo As VO.ObjetivoCarimbodeVerbadoFornecedor) As List(Of VO.ObjetivoCarimbodeVerbadoFornecedor)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DESOBJMCOVBAFRN, TIPOBJMCOVBAFRN FROM MRT.CADOBJMCOVBAFRN ORDER BY TIPOBJMCOVBAFRN ASC]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function
End Class

<TableAttribute("MRT.CADOBJMCOVBAFRN")>
Public Class ObjetivoCarimbodeVerbadoFornecedorDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property TIPOBJMCOVBAFRN As Nullable(Of Decimal)

    <IgnoreMap>
    Property DescDrop As String

End Class