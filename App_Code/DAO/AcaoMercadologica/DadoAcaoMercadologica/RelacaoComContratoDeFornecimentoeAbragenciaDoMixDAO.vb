﻿Imports Core
Public Class RelacaoComContratoDeFornecimentoeAbragenciaDoMixDAO
    Inherits DAOBase(Of VO.RelacaoComContratoDeFornecimentoeAbragenciaDoMix)

    Public Sub New()
        daoConfiguration = New RelacaoComContratoDeFornecimentoeAbragenciaDoMixDAOConfiguration()
    End Sub
End Class


<TableAttribute("MRT.T0124996")>
Public Class RelacaoComContratoDeFornecimentoeAbragenciaDoMixDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property NUMCTTFRN As Nullable(Of Decimal)

    <PrimaryKey>
    Property NUMCSLCTTFRN As Nullable(Of Decimal)

    <PrimaryKey>
    Property TIPEDEABGMIX As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODEDEABGMIX As Nullable(Of Decimal)

End Class
