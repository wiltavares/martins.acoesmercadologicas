﻿Imports Core
Public Class MovimentoDiarioReservaDeSaldoFornecedorDAO
    Inherits DAOBase(Of VO.MovimentoDiarioReservaDeSaldoFornecedor)

    Public Sub New()
        daoConfiguration = New MovimentoDiarioReservaDeSaldoFornecedorDAOConfiguration()
    End Sub

    Public Function DeletePorCodAcao(ByVal CODACOCMC As String) As Integer
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE FROM MRT.T0123159  WHERE TRIM(CODACOCMC) = TRIM(:CODACOCMC)]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function

End Class


<TableAttribute("MRT.T0123159")>
Public Class MovimentoDiarioReservaDeSaldoFornecedorDAOConfiguration
    Inherits DAOConfiguration

    <DateInsert>
    <[Readonly](False, True)>
    Property DATAPVACOCMC As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATACEACOCMC As Date

End Class
