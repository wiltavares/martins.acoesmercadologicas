﻿Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoNegocioClienteDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaPublicoAlvoNegocioClienteDAOConfiguration()
    End Sub

    Public Function ListarTipoNegocioCliente(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT B.CODUNDESRNGC,
                                               B.DESUNDESRNGC,
                                               A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC

                                        FROM
                                        MRT.T0131275 B
                                        LEFT JOIN MRT.RLCACOCMCRGRNGC A  ON (A.CODUNDESRNGC = B.CODUNDESRNGC) AND A.CODACOCMC = :CODACOCMC 
                                                                                                              AND A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODUNDESRNGC", "DESUNDESRNGC", "CODACOCMC", "NUMSEQRGRACOCMC"}))
    End Function

    Public Function ListarTipoNegocioClienteSelecionado(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODACOCMC,
                                               NUMSEQRGRACOCMC,
                                               CODUNDESRNGC 
                                        FROM MRT.RLCACOCMCRGRNGC                                        
                                        WHERE (:CODACOCMC IS NULL OR :CODACOCMC = CODACOCMC)
                                        AND (:NUMSEQRGRACOCMC IS NULL OR :NUMSEQRGRACOCMC = NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "CODUNDESRNGC"}))
    End Function
End Class


<TableAttribute("MRT.RLCACOCMCRGRNGC")>
Public Class RelacaoAcaoMecanicaPublicoAlvoNegocioClienteDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODUNDESRNGC As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESUNDESRNGC As String
End Class