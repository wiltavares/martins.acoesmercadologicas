﻿Imports Microsoft.VisualBasic


Public Class MercadoriaEmpresaQuantidadeDAO
    Inherits DAOBase(Of VO.MercadoriaEmpresaQuantidade)

    Public Sub New()
        daoConfiguration = New MercadoriaEmpresaQuantidadeDAOConfiguration()
    End Sub


    Public Sub UpdateAbastecerPromocao(CODEMP As Decimal, CODMER As Decimal, INDTIPISRMERPMC As Decimal, QDEMERISRPMC As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[  UPDATE MRT.T0101350
                                          SET QDEMERMAXPMC  = (CASE WHEN :INDTIPISRMERPMC = 0 THEN 
                                                                        QDEMERMAXPMC + :QDEMERISRPMC
                                                                    ELSE
                                                                        QDEMERMAXPMC - :QDEMERISRPMC                                           
                                                                    END)
                                           ,  QDEMERRSVPMC  = (CASE WHEN :INDTIPISRMERPMC = 0 THEN 
                                                                        QDEMERRSVPMC + :QDEMERISRPMC
                                                                    ELSE
                                                                        QDEMERRSVPMC - :QDEMERISRPMC                                           
                                                                    END)
                                        WHERE CODEMP = :CODEMP
                                          AND CODMER = :CODMER
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODEMP", CODEMP)
        parameterMapper.Parameters.Add(":CODMER", CODMER)
        parameterMapper.Parameters.Add(":INDTIPISRMERPMC", INDTIPISRMERPMC)
        parameterMapper.Parameters.Add(":QDEMERISRPMC", QDEMERISRPMC)

        connector.ExecuteScalar(sql, parameterMapper)

    End Sub



    Public Function SelectPorMercadoriaFilial(CODMER As Decimal, CODFILEMP As Decimal) As VO.MercadoriaEmpresaQuantidade

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                 SELECT CODEMP
                      , CODMER
                      , QDEETQMER
                      , QDEMERVNDBLC
                      , QDEMERVNDTMK
                      , QDEMERENRPND
                      , QDEMERENRESP
                      , QDEMERSAIESP
                      , QDEMERATPVND
                      , QDEMERSUSVND
                      , QDEETQMERMIX
                      , QDEMERENRTRN
                      , QDEMERSAITRN
                      , QDEMERREFENRTRN
                      , QDEMERRSVPMC
                      , QDEMERVNDPMC
                      , QDEMERRSVLPT
                      , PERMERRSVPMC
                      , QDEMERMAXPMC
                      , QDEMERRSVVNDATP
                      , QDEMEDVNDMNSMER
                      , QDEMERSUSVNDDPS
                      , QDEETQRSVMIX
                      , QDEETQRSVMIXVNDTMK
                      , QDEETQRSVKIT
                      , QDEMEDVNDMERPOD
                      , QDEMERVNDENMRSVFAT
                      , QDEMERSUSVNDPCO
                      , QDEMERDSNSLD
                      , QDEMERVNDSLD
                      , QDERSVETQCMCETN
                   FROM MRT.T0101350
                  WHERE CODMER = :CODMER
                   AND CODEMP = :CODEMP
                     ]]>.Value

        parameterMapper.Parameters.Add(":CODMER", CODMER)
        parameterMapper.Parameters.Add(":CODEMP", CODFILEMP) 'esta tabela tem a coluna com o nome errado. Neste campo esta gravado a filial empresa

        Return connector.ExecuteReader(Of VO.MercadoriaEmpresaQuantidade)(sql, parameterMapper).FirstOrDefault()
    End Function



End Class

<TableAttribute("MRT.t0101350")>
Public Class MercadoriaEmpresaQuantidadeDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODEMP As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODMER As Nullable(Of Decimal)
End Class
