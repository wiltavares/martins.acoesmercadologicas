﻿Imports Core
Imports VO

Public Class AbastecimentoDAO
    Inherits DAOBase(Of VO.Abastecimento)

    Public Sub New()
        daoConfiguration = New AbastecimentoDAOConfiguration()
    End Sub

    Public Function Pesquisa(vo As VO.Abastecimento) As List(Of VO.Abastecimento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If (Not String.IsNullOrWhiteSpace(vo.NOMPMC)) Then
            vo.NOMPMC = "%" + vo.NOMPMC + "%"
        End If

        If (Not String.IsNullOrWhiteSpace(vo.NOMFRNMER)) Then
            vo.NOMFRNMER = "%" + vo.NOMFRNMER + "%"
        End If

        If (Not String.IsNullOrWhiteSpace(vo.DESMER)) Then
            vo.DESMER = "%" + vo.DESMER + "%"
        End If

        Dim sql As String = <![CDATA[
         SELECT A.CODUNDESRNGC AS CODUNDESRNGC  
              , A.CODFILEMP
              , H.NOMFILEMP
              , G.CODCPR
              , F.NOMFNC AS NOMCPR
              , A.CODPMC
              , A.CODACOCMC
              , A.DATINIPMC
              , A.DATFIMPMC
              , A.DESPMC
              , A.NOMPMC
              , B.CODPRMPMC as CODMER
              , B.TIPEDENIVPRMPMC
              , B.DATECSPRD
              , C.DESMER
              , (B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) AS ETQBND
              , B.QDEMERVNDPRMPMC
              , B.QDEMERMAXPRMPMC
              , (E.QDEETQMER  +  E.QDEMERENRPND  +  E.QDEMERENRESP +  E.QDEMERATPVND - E.QDEMERVNDTMK - E.QDEMERSAIESP - E.QDEMERVNDBLC - E.QDEMERSUSVND - E.QDEETQMERMIX +  E.QDEMERENRTRN - E.QDEMERSAITRN - E.QDEMERRSVLPT - E.QDEMERRSVVNDATP - E.QDEMERSUSVNDDPS -  E.QDEETQRSVMIX  - E.QDEETQRSVKIT  - ( E.QDEMERMAXPMC - E.QDEMERVNDPMC)) AS ETQSLD 
              , TRUNC((CASE WHEN TRUNC(TRUNC(SYSDATE) - A.DATINIPMC) = 0 OR TO_NUMBER(TO_CHAR((B.QDEMERVNDPRMPMC / TRUNC(TRUNC(SYSDATE) - A.DATINIPMC)), '999999999D999999')) = 0 THEN 
                          (
                              CASE WHEN ((B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) < (E.QDEETQMER - E.QDEMERVNDTMK)) THEN 
                                  (B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) 
                              ELSE 
                                  (E.QDEETQMER - E.QDEMERVNDTMK) 
                              END
                          ) 
                      ELSE 
                          (
                              CASE WHEN (( B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) < (E.QDEETQMER -  E.QDEMERVNDTMK)) THEN 
                                 ( B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) /  TO_NUMBER(TO_CHAR(( B.QDEMERVNDPRMPMC / TRUNC(TRUNC(SYSDATE ) - A.DATINIPMC)),  '999999999D999999')) 
                              ELSE 
                                 ( E.QDEETQMER - E.QDEMERVNDTMK  ) /  TO_NUMBER(TO_CHAR((B.QDEMERVNDPRMPMC / TRUNC(TRUNC(SYSDATE) -  A.DATINIPMC)),'999999999D999999'))
                              END
                          )
                      END))AS QDEDIACOB 
              , (A.DATFIMPMC ) - (TRUNC(SYSDATE) - 1) AS QDEDIARES 
              , NVL(BDE.VLRUNTLIQMER, 0) AS VALOR_BRINDE 
              , ROUND(ITE.VLRTOTMERISRPMC/ITE.QDEMERISRPMC, 2)  AS CUSTO_BRINDE 
              , TO_NUMBER( TO_CHAR(B.QDEMERVNDPRMPMC /  (SELECT CASE WHEN COUNT(DISTINCT CLD.DATREF) = 0 THEN 1 ELSE COUNT(DISTINCT CLD.DATREF) END
                                               FROM MRT.T0102321 CLD 
                                              WHERE CLD.DATREF BETWEEN A.DATINIPMC AND TRUNC(SYSDATE)  
                                                AND CLD.DATREF <= A.DATFIMPMC  
                                                AND CLD.NUMDIASMN NOT IN(6, 7)
                                                AND CLD.FLGFRDBCO = ' ' )
                        , '99990D99'
                 )) AS MEDIA_SAIDA  
             FROM MRT.T0107935 A  
             INNER JOIN MRT.T0108044 B  ON B.CODPMC = A.CODPMC AND B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC
             INNER JOIN MRT.T0100086 C ON C.CODMER = B.CODPRMPMC AND C.CODEMP = 1 
              LEFT JOIN MRT.T0100426 FRN ON C.CODFRNPCPMER = FRN.CODFRN
             INNER JOIN MRT.T0101350 E  ON E.CODEMP = A.CODFILEMP AND E.CODMER = C.CODMER 
             INNER JOIN MRT.T0100981 G  ON G.CODCPR = C.CODCPRPCPMER
             INNER JOIN MRT.T0100361 F ON F.CODFNC = G.CODFNC 	
             INNER JOIN MRT.T0112963 H  ON H.CODFILEMP = A.CODFILEMP
              LEFT JOIN MRT.T0161115 BDE ON BDE.CODPMC = A.CODPMC  AND BDE.CODACOCMC = A.CODACOCMC  AND BDE.CODMER = C.CODMER 
              LEFT JOIN (SELECT ITE.CODPMC
                              , ITE.CODMER
                              , ITE.VLRTOTMERISRPMC
                              , ITE.QDEMERISRPMC 
                           FROM MRT.T0153553 ITE 
                          INNER JOIN (SELECT XC.CODACOCMC
                                           , XC.CODPMC
                                           , XC.CODMER, MAX(XC.NUMSEQISRQDEMERPMC) AS NUMSEQISRQDEMERPMC  
                                        FROM MRT.T0107935 XA  
                                       INNER JOIN MRT.T0108044 XB  ON XB.CODPMC = XA.CODPMC AND XB.TIPEDEPUBALVPMC = XA.TIPEDEPUBALVPMC
                                       INNER JOIN MRT.T0153553 XC ON XC.CODPMC = XA.CODPMC  AND XC.CODMER = XB.CODPRMPMC  AND XC.INDTIPISRMERPMC = 0 AND XC.DATGRCRLC >= XA.DATINIPMC
                                       WHERE XA.TIPEDEPUBALVPMC = 4 
                                         AND XA.DATFIMPMC > = TRUNC (SYSDATE)
                                         AND XA.TIPSITPMC = ' '  
                                         AND XA.CODFILEMP > 0
                                         AND XC.INDTIPISRMERPMC = 0
                                      GROUP BY XC.CODACOCMC
                                             , XC.CODPMC
                                             , XC.CODMER
                                    )ULTIMO ON ULTIMO.CODACOCMC = ITE.CODACOCMC AND ULTIMO.CODPMC = ITE.CODPMC AND ULTIMO.CODMER = ITE.CODMER AND ULTIMO.NUMSEQISRQDEMERPMC = ITE.NUMSEQISRQDEMERPMC
                       ) ITE ON ITE.CODPMC = A.CODPMC  AND ITE.CODMER = B.CODPRMPMC 
             WHERE A.TIPEDEPUBALVPMC = 4 
               AND A.DATFIMPMC > = TRUNC ( SYSDATE  )
               AND A.TIPSITPMC = ' ' 	
               AND A.CODFILEMP > 0    
                    
               AND (:CODFILEMP IS NULL OR A.CODFILEMP = :CODFILEMP)
               AND (:NOMPMC IS NULL OR A.NOMPMC LIKE :NOMPMC)
               AND (:CODPMC  IS NULL OR A.CODPMC = :CODPMC )
               AND (:DATINIPMC IS NULL OR :DATFIMPMC IS NULL OR A.DATINIPMC BETWEEN :DATINIPMC AND :DATFIMPMC)
               AND (:CODUNDESRNGC IS NULL OR :CODUNDESRNGC = A.CODUNDESRNGC)
               AND (:CODFNCGRCPMC IS NULL OR :CODFNCGRCPMC = A.CODFNCGRCPMC)
               AND (:TIPSITPMC IS NULL OR :TIPSITPMC = A.TIPSITPMC)   
               AND (:QDEDIARES IS NULL OR ((A.DATFIMPMC ) - (TRUNC(SYSDATE) - 1)) <= :QDEDIARES)  
               AND (:CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
               AND (:CODFRNMER IS NULL OR FRN.CODFRN = :CODFRNMER)
               AND (:NOMFRNMER IS NULL OR UPPER(FRN.NOMFRN) LIKE :NOMFRNMER)  
               AND (:QDEDIACOB IS NULL OR (TRUNC((CASE WHEN TRUNC(TRUNC(SYSDATE) - A.DATINIPMC) = 0 OR TO_NUMBER(TO_CHAR((B.QDEMERVNDPRMPMC / TRUNC(TRUNC(SYSDATE) - A.DATINIPMC)), '999999999D999999')) = 0 THEN 
                          (
                              CASE WHEN ((B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) < (E.QDEETQMER - E.QDEMERVNDTMK)) THEN 
                                  (B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) 
                              ELSE 
                                  (E.QDEETQMER - E.QDEMERVNDTMK) 
                              END
                          ) 
                      ELSE 
                          (
                              CASE WHEN (( B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) < (E.QDEETQMER -  E.QDEMERVNDTMK)) THEN 
                                 ( B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) /  TO_NUMBER(TO_CHAR(( B.QDEMERVNDPRMPMC / TRUNC(TRUNC(SYSDATE ) - A.DATINIPMC)),  '999999999D999999')) 
                              ELSE 
                                 ( E.QDEETQMER - E.QDEMERVNDTMK  ) /  TO_NUMBER(TO_CHAR((B.QDEMERVNDPRMPMC / TRUNC(TRUNC(SYSDATE) -  A.DATINIPMC)),'999999999D999999'))
                              END
                          )
                      END))) <= :QDEDIACOB )
               AND (:CODPRMPMC IS NULL OR :CODPRMPMC = B.CODPRMPMC)
               AND (:DESMER IS NULL OR UPPER(C.DESMER) LIKE :DESMER)  
                            ]]>.Value


        parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(vo.CODFILEMP))
        parameterMapper.Parameters.Add(":NOMPMC", ValueDBProperty(vo.NOMPMC))
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(vo.CODPMC))
        parameterMapper.Parameters.Add(":DATINIPMC", ValueDBProperty(vo.DATINIPMC))
        parameterMapper.Parameters.Add(":DATFIMPMC", ValueDBProperty(vo.DATFIMPMC))
        parameterMapper.Parameters.Add(":CODUNDESRNGC", ValueDBProperty(vo.CODUNDESRNGC))
        parameterMapper.Parameters.Add(":TIPSITPMC", ValueDBProperty(vo.TIPSITPMC))
        parameterMapper.Parameters.Add(":CODFNCGRCPMC", ValueDBProperty(vo.CODFNCGRCPMC))
        parameterMapper.Parameters.Add(":QDEDIARES", ValueDBProperty(vo.QDEDIARES))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":QDEDIACOB", ValueDBProperty(vo.QDEDIACOB))
        parameterMapper.Parameters.Add(":CODFRNMER", ValueDBProperty(vo.CODFRNMER))
        parameterMapper.Parameters.Add(":NOMFRNMER", ValueDBProperty(vo.NOMFRNMER))
        parameterMapper.Parameters.Add(":CODPRMPMC", ValueDBProperty(vo.CODPRMPMC))
        parameterMapper.Parameters.Add(":DESMER", ValueDBProperty(vo.DESMER))







        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODUNDESRNGC", "CODFILEMP", "NOMFILEMP", "CODCPR", "NOMCPR", "CODPMC", "CODACOCMC", "DATINIPMC", "DATFIMPMC", "DESPMC", "NOMPMC", "CODMER", "TIPEDENIVPRMPMC", "DATECSPRD", "DESMER", "ETQBND", "QDEMERVNDPRMPMC", "QDEMERMAXPRMPMC", "ETQSLD", "QDEDIACOB", "QDEDIARES", "VALOR_BRINDE", "CUSTO_BRINDE", "MEDIA_SAIDA"}))
    End Function

    Friend Function HistoricoPromocaoItem(CODPMC As Decimal, CODMER As Decimal) As List(Of Abastecimento)
        Dim sql As String = <![CDATA[
                     SELECT A.CODACOCMC
                          , A.CODPMC
                          , A.CODMER
                          , A.NUMSEQISRQDEMERPMC
                          , A.QDEMERISRPMC
                          , A.VLRTOTMERISRPMC
                          , A.DATGRCRLC
                          , A.CODFNCGRCPMC
                          , A.INDISRVLDMERPMC
                          , A.INDTIPISRMERPMC
                          , B.NOMFNC AS NOMFNCGRCPMC
                        FROM MRT.T0153553 A
                        LEFT JOIN MRT.T0100361 B ON A.CODFNCGRCPMC = B.CODFNC
                        INNER JOIN MRT.T0107935 C ON A.CODPMC = C.CODPMC  AND  C.TIPEDEPUBALVPMC = 4 AND TRUNC(A.DATGRCRLC) >= TRUNC(C.DATHRAGRCPMC) 
                       WHERE A.CODPMC= :CODPMC
                         AND A.CODMER = :CODMER 
                       ORDER BY A.NUMSEQISRQDEMERPMC
                            ]]>.Value

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(CODMER))

        Return connector.ExecuteReader(Of VO.Abastecimento)(sql, parameterMapper)

    End Function

    Friend Function BuscaSaldoAbastecimento(CODPMC As Decimal, CODMER As Decimal) As Abastecimento


        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                  SELECT CODPMC
                       , CODMER
                     --, CODACOCMC
                       , SUM(CASE WHEN INDTIPISRMERPMC = 1 THEN QDEMERISRPMC * -1 WHEN INDTIPISRMERPMC = 0 THEN QDEMERISRPMC ELSE 0 END) AS QDEMERISRPMC
                    FROM MRT.T0153553
                   WHERE CODPMC= :CODPMC
                     AND CODMER = :CODMER
                   --AND CODACOCMC =  :CODACOCMC
                   GROUP BY CODPMC
                       , CODMER
                     --, CODACOCMC
                            ]]>.Value


        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(CODMER))
        'parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))


        Return connector.ExecuteReader(Of VO.Abastecimento)(sql, parameterMapper).FirstOrDefault()

    End Function

    Public Function PesquisaAbastecimento(vo As VO.Abastecimento) As List(Of VO.Abastecimento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
         SELECT A.CODUNDESRNGC AS CODUNDESRNGC  
              , A.CODFILEMP
              , H.NOMFILEMP
              , G.CODCPR
              , F.NOMFNC AS NOMCPR
              , A.CODPMC
              , A.CODACOCMC
              , A.DATINIPMC
              , A.DATFIMPMC
              , A.DESPMC
              , A.NOMPMC
              , B.CODPRMPMC as CODMER
              , B.TIPEDENIVPRMPMC
              , B.DATECSPRD
              , C.DESMER
              , (B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) AS ETQBND
              , B.QDEMERVNDPRMPMC
              , B.QDEMERMAXPRMPMC
              , (E.QDEETQMER  +  E.QDEMERENRPND  +  E.QDEMERENRESP +  E.QDEMERATPVND - E.QDEMERVNDTMK - E.QDEMERSAIESP - E.QDEMERVNDBLC - E.QDEMERSUSVND - E.QDEETQMERMIX +  E.QDEMERENRTRN - E.QDEMERSAITRN - E.QDEMERRSVLPT - E.QDEMERRSVVNDATP - E.QDEMERSUSVNDDPS -  E.QDEETQRSVMIX  - E.QDEETQRSVKIT  - ( E.QDEMERMAXPMC - E.QDEMERVNDPMC)) AS ETQSLD 
              , TRUNC((CASE WHEN TRUNC(TRUNC(SYSDATE) - A.DATINIPMC) = 0 OR TO_NUMBER(TO_CHAR((B.QDEMERVNDPRMPMC / TRUNC(TRUNC(SYSDATE) - A.DATINIPMC)), '999999999D999999')) = 0 THEN 
                          (
                              CASE WHEN ((B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) < (E.QDEETQMER - E.QDEMERVNDTMK)) THEN 
                                  (B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) 
                              ELSE 
                                  (E.QDEETQMER - E.QDEMERVNDTMK) 
                              END
                          ) 
                      ELSE 
                          (
                              CASE WHEN (( B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) < (E.QDEETQMER -  E.QDEMERVNDTMK)) THEN 
                                 ( B.QDEMERMAXPRMPMC - B.QDEMERVNDPRMPMC) /  TO_NUMBER(TO_CHAR(( B.QDEMERVNDPRMPMC / TRUNC(TRUNC(SYSDATE ) - A.DATINIPMC)),  '999999999D999999')) 
                              ELSE 
                                 ( E.QDEETQMER - E.QDEMERVNDTMK  ) /  TO_NUMBER(TO_CHAR((B.QDEMERVNDPRMPMC / TRUNC(TRUNC(SYSDATE) -  A.DATINIPMC)),'999999999D999999'))
                              END
                          )
                      END))AS QDEDIACOB 
              , (A.DATFIMPMC ) - (TRUNC(SYSDATE) - 1) AS QDEDIARES 
              , NVL(BDE.VLRUNTLIQMER, 0) AS VALOR_BRINDE 
              , ROUND(ITE.VLRTOTMERISRPMC/ITE.QDEMERISRPMC, 2)  AS CUSTO_BRINDE 
              , TO_NUMBER( TO_CHAR(B.QDEMERVNDPRMPMC /  (SELECT CASE WHEN COUNT(DISTINCT CLD.DATREF) = 0 THEN 1 ELSE COUNT(DISTINCT CLD.DATREF) END
                                               FROM MRT.T0102321 CLD 
                                              WHERE CLD.DATREF BETWEEN A.DATINIPMC AND TRUNC(SYSDATE)  
                                                AND CLD.DATREF <= A.DATFIMPMC  
                                                AND CLD.NUMDIASMN NOT IN(6, 7)
                                                AND CLD.FLGFRDBCO = ' ' )
                        , '99990D99'
                 )) AS MEDIA_SAIDA  
             FROM MRT.T0107935 A  
             INNER JOIN MRT.T0108044 B  ON B.CODPMC = A.CODPMC AND B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC
             INNER JOIN MRT.T0100086 C ON C.CODMER = B.CODPRMPMC AND C.CODEMP = 1 
             INNER JOIN MRT.T0101350 E  ON E.CODEMP = A.CODFILEMP AND E.CODMER = C.CODMER 
             INNER JOIN MRT.T0100981 G  ON G.CODCPR = C.CODCPRPCPMER
             INNER JOIN MRT.T0100361 F ON F.CODFNC = G.CODFNC 	
             INNER JOIN MRT.T0112963 H  ON H.CODFILEMP = A.CODFILEMP
              LEFT JOIN MRT.T0161115 BDE ON BDE.CODPMC = A.CODPMC  AND BDE.CODACOCMC = A.CODACOCMC  AND BDE.CODMER = C.CODMER 
              LEFT JOIN (SELECT ITE.CODPMC
                              , ITE.CODMER
                              , ITE.VLRTOTMERISRPMC
                              , ITE.QDEMERISRPMC 
                           FROM MRT.T0153553 ITE 
                          INNER JOIN (SELECT XC.CODACOCMC
                                           , XC.CODPMC
                                           , XC.CODMER, MAX(XC.NUMSEQISRQDEMERPMC) AS NUMSEQISRQDEMERPMC  
                                        FROM MRT.T0107935 XA  
                                       INNER JOIN MRT.T0108044 XB  ON XB.CODPMC = XA.CODPMC AND XB.TIPEDEPUBALVPMC = XA.TIPEDEPUBALVPMC
                                       INNER JOIN MRT.T0153553 XC ON XC.CODPMC = XA.CODPMC  AND XC.CODMER = XB.CODPRMPMC  AND XC.INDTIPISRMERPMC = 0 AND TRUNC(XC.DATGRCRLC) >= TRUNC(XA.DATHRAGRCPMC)
                                       WHERE XA.TIPEDEPUBALVPMC = 4 
                                         AND XA.DATFIMPMC > = TRUNC (SYSDATE)
                                         AND XA.TIPSITPMC = ' '  
                                         AND XA.CODFILEMP > 0
                                         AND XC.INDTIPISRMERPMC = 0
                                      GROUP BY XC.CODACOCMC
                                             , XC.CODPMC
                                             , XC.CODMER
                                    )ULTIMO ON ULTIMO.CODACOCMC = ITE.CODACOCMC AND ULTIMO.CODPMC = ITE.CODPMC AND ULTIMO.CODMER = ITE.CODMER AND ULTIMO.NUMSEQISRQDEMERPMC = ITE.NUMSEQISRQDEMERPMC
                       ) ITE ON ITE.CODPMC = A.CODPMC  AND ITE.CODMER = B.CODPRMPMC 
             WHERE A.TIPEDEPUBALVPMC = 4 
               AND A.CODFILEMP > 0    
               AND A.CODPMC = :CODPMC
                            ]]>.Value


        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(vo.CODPMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODUNDESRNGC", "CODFILEMP", "NOMFILEMP", "CODCPR", "NOMCPR", "CODPMC", "CODACOCMC", "DATINIPMC", "DATFIMPMC", "DESPMC", "NOMPMC", "CODMER", "TIPEDENIVPRMPMC", "DATECSPRD", "DESMER", "ETQBND", "QDEMERVNDPRMPMC", "QDEMERMAXPRMPMC", "ETQSLD", "QDEDIACOB", "QDEDIARES", "VALOR_BRINDE", "CUSTO_BRINDE", "MEDIA_SAIDA"}))
    End Function

    Public Function GetSelectProximoNumSeqIsrQdeMerPmc(ByVal CODACOCMC As Decimal, ByVal CODPMC As Decimal, ByVal CODMER As Decimal) As VO.Abastecimento

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT NVL(MAX( NVL(NUMSEQISRQDEMERPMC, 0)) + 1, 1) AS NUMSEQISRQDEMERPMC
                                          FROM MRT.T0153553 A
                                             , MRT.T0107935 B
                                         WHERE A.CODPMC = B.CODPMC
                                           AND B.TIPEDEPUBALVPMC = 4
                                           AND TRUNC(A.DATGRCRLC) >= TRUNC(B.DATHRAGRCPMC)
                                           AND (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                           AND (A.CODPMC IS NULL OR A.CODPMC = :CODPMC)
                                           AND (A.CODMER IS NULL OR A.CODMER = :CODMER)  
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(CODMER))

        Return connector.ExecuteReader(Of VO.Abastecimento)(sql, parameterMapper).FirstOrDefault()

    End Function

    Public Function SelectSomaDeQuantidadeEValorMercadoriaInseridaNaPromocao(promocaoBrinde As VO.Abastecimento) As VO.Abastecimento

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
            SELECT COALESCE(SUM(A.QDEMERISRPMC), 0) As QDEMERISRPMC
                 , COALESCE(SUM(A.VLRTOTMERISRPMC), 0) AS VLRTOTMERISRPMC 
              FROM MRT.T0153553 A
                 , MRT.T0107935 B 
             WHERE A.CODPMC = B.CODPMC 
               AND B.TIPEDEPUBALVPMC = 4 
               AND TRUNC(A.DATGRCRLC) >= TRUNC(B.DATHRAGRCPMC) 
               AND A.INDTIPISRMERPMC = :INDTIPISRMERPMC
                             ]]>.Value

        parameterMapper.Parameters.Add(":INDTIPISRMERPMC", ValueDBProperty(promocaoBrinde.INDTIPISRMERPMC))

        If (promocaoBrinde.CODACOCMC IsNot Nothing AndAlso promocaoBrinde.CODACOCMC > 0) Then
            sql = sql + " AND A.CODACOCMC= :CODACOCMC"
            parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(promocaoBrinde.CODACOCMC))
        End If

        If (promocaoBrinde.INDISRVLDMERPMC IsNot Nothing AndAlso promocaoBrinde.INDISRVLDMERPMC >= 0) Then
            sql = sql + " AND A.INDISRVLDMERPMC= :INDISRVLDMERPMC"
            parameterMapper.Parameters.Add(":INDISRVLDMERPMC", ValueDBProperty(promocaoBrinde.INDISRVLDMERPMC))
        End If

        If (promocaoBrinde.CODPMC IsNot Nothing AndAlso promocaoBrinde.CODPMC > 0) Then
            sql = sql + " AND A.CODPMC = :CODPMC"
            parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocaoBrinde.CODPMC))
        End If

        If (promocaoBrinde.CODMER IsNot Nothing AndAlso promocaoBrinde.CODMER > 0) Then
            sql = sql + " AND A.CODMER = :CODMER"
            parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(promocaoBrinde.CODMER))
        End If

        'Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"QDEMERISRPMC", "VLRTOTMERISRPMC"}))
        Return connector.ExecuteReader(Of VO.Abastecimento)(sql, parameterMapper).FirstOrDefault()
    End Function


    Public Function SelectRelacaoAcaoComercialEPromocaoEBrinde(promocaoBrinde As VO.Abastecimento) As List(Of VO.Abastecimento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
			                  SELECT A.CODACOCMC, 
                                     A.CODFNCGRCPMC,
                                     A.CODMER,
                                     A.CODPMC,
                                     A.DATGRCRLC, 
                                     A.INDISRVLDMERPMC, 
                                     A.INDTIPISRMERPMC, 
                                     A.NUMSEQISRQDEMERPMC,
                                     A.QDEMERISRPMC, 
                                     A.VLRTOTMERISRPMC 
                                FROM MRT.T0153553 A
                          INNER JOIN MRT.T0107935 B ON A.CODPMC = B.CODPMC AND TRUNC(A.DATGRCRLC) >= TRUNC(B.DATHRAGRCPMC)
                               WHERE B.TIPEDEPUBALVPMC = 4
	                             AND A.CODPMC = :CODPMC
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocaoBrinde.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "CODFNCGRCPMC", "CODMER", "CODPMC", "DATGRCRLC", "INDISRVLDMERPMC", "INDTIPISRMERPMC", "NUMSEQISRQDEMERPMC", "QDEMERISRPMC", "VLRTOTMERISRPMC"}))

    End Function
End Class


<TableAttribute("MRT.T0153553")>
Public Class AbastecimentoDAOConfiguration

    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODMER As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQISRQDEMERPMC As Nullable(Of Decimal)



    'Auxiliares

    <IgnoreMap>
    Property CODUNDESRNGC As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMFILEMP As String
    <IgnoreMap>
    Property CODCPR As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMCPR As String
    <IgnoreMap>
    Property CODFILEMP As Nullable(Of Decimal)
    <IgnoreMap>
    Property DATINIPMC As Nullable(Of Date)
    <IgnoreMap>
    Property DATFIMPMC As Nullable(Of Date)
    <IgnoreMap>
    Property DESPMC As String
    <IgnoreMap>
    Property NOMPMC As String
    <IgnoreMap>
    Property DESMER As String
    <IgnoreMap>
    Property ETQBND As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEMERVNDPRMPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEMERMAXPRMPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property ETQSLD As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEDIACOB As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEDIARES As Nullable(Of Decimal)
    <IgnoreMap>
    Property VALOR_BRINDE As Nullable(Of Decimal)
    <IgnoreMap>
    Property CUSTO_BRINDE As Nullable(Of Decimal)
    <IgnoreMap>
    Property MEDIA_SAIDA As Nullable(Of Decimal)
    <IgnoreMap>
    Property TIPSITPMC As String
    <IgnoreMap>
    Property CODFILEMPORIVBA As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODFNCCRIACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property NUMLNHITEOCDACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMFNCGRCPMC As String
    <IgnoreMap>
    Property CODMER_PRINCIPAL As String

    <IgnoreMap>
    Property TIPEDENIVPRMPMC As Nullable(Of Decimal)

    'Data de exclusão do brinde na promoção
    <IgnoreMap>
    Property DATECSPRD As Nullable(Of DateTime)

End Class