﻿Imports Core
Imports VO

Public Class RelacaoAcaoMecanicaPublicoAlvoRcaVendedorDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaPublicoAlvoRcaVendedorDAOConfiguration()
    End Sub

    Public Function BuscarListaRcaVendedor(vo As VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not vo.NOMSUP Is Nothing Then
            vo.NOMSUP = "%" + UCase(vo.NOMSUP) + "%"
        End If
        If Not vo.NOMREP Is Nothing Then
            vo.NOMREP = "%" + UCase(vo.NOMREP) + "%"
        End If

        Dim sql As String = <![CDATA[
                                       SELECT A.CODREP,
                                              A.NOMREP,
                                              C.NOMSUP,
                                              A.CODSUP
                                        FROM MRT.T0100116 A
                                        LEFT JOIN MRT.CADTIPEDEVND B ON B.INDEDEPTCICT = 1 
                                        LEFT JOIN MRT.T0100124 C ON C.CODSUP = A.CODSUP 
                                        LEFT JOIN MRT.RLCACOCMCRGRREP RACRR ON RACRR.CODREP = A.CODREP AND RACRR.CODACOCMC = :CODACOCMC AND RACRR.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                                        WHERE (A.DATDSTREP IS NULL)
                                        AND A.TIPEDEVND = B.TIPEDEVND
                                        AND (RACRR.CODREP IS NULL)
                                        AND (:NOMREP IS NULL OR UPPER(A.NOMREP) LIKE :NOMREP)                                        
                                        AND (:NOMSUP IS NULL OR UPPER(C.NOMSUP) LIKE :NOMSUP)
                                        AND (:CODSUP IS NULL OR A.CODSUP = :CODSUP)
                                        AND (:CODREP IS NULL OR A.CODREP = :CODREP)
                            ]]>.Value

        parameterMapper.Parameters.Add(":NOMSUP", ValueDBProperty(vo.NOMSUP))
        parameterMapper.Parameters.Add(":NOMREP", ValueDBProperty(vo.NOMREP))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":CODSUP", ValueDBProperty(vo.CODSUP))
        parameterMapper.Parameters.Add(":CODREP", ValueDBProperty(vo.CODREP))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function ListarPublicoAlvoRcaVendedor(vo As VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       SELECT  
                                                RACRR.CODACOCMC,
                                                RACRR.NUMSEQRGRACOCMC,
                                                RACRR.CODREP,
                                                C.NOMREP
                                        FROM MRT.RLCACOCMCRGRREP RACRR
                                        INNER JOIN MRT.T0100116 C ON (C.CODREP = RACRR.CODREP)
                                        WHERE (:CODACOCMC IS NULL OR :CODACOCMC = RACRR.CODACOCMC)
                                        AND (:NUMSEQRGRACOCMC IS NULL OR :NUMSEQRGRACOCMC = NUMSEQRGRACOCMC)

                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function


    Public Function VerificaSeRcaVendedorExiste(ByVal CODREP As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       SELECT CODREP,
                                              NOMREP
                                        FROM MRT.T0100116 A                                        
                                        WHERE (CODREP = :CODREP)                                        
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODREP", ValueDBProperty(CODREP))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function


End Class


<TableAttribute("MRT.RLCACOCMCRGRREP")>
Public Class RelacaoAcaoMecanicaPublicoAlvoRcaVendedorDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODREP As Nullable(Of Decimal)



    'auxiliares
    <IgnoreMap>
    Property NOMSUP As String
    <IgnoreMap>
    Property NOMREP As String
    <IgnoreMap>
    Property CODSUP As Nullable(Of Decimal)
    '<IgnoreMap>
    'Property NOMGER As String
End Class