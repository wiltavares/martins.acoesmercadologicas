﻿Imports Microsoft.VisualBasic

Public Class PromocaoClientesExcludentesDAO
    Inherits DAOBase(Of VO.PromocaoClientesExcludentes)

    Public Sub New()
        daoConfiguration = New PromocaoClientesExcludentesDAOConfiguration()
    End Sub

    Public Function BuscarListaCliente(vo As VO.PromocaoClientesExcludentes) As List(Of VO.PromocaoClientesExcludentes)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT D.CODCLI
                                         , D.NOMCLI
                                     FROM MRT.T0100116 A
                                     INNER JOIN MRT.T0133715 B ON (B.CODREP = A.CODREP)
                                     INNER JOIN MRT.T0133774 C ON (C.CODTETVND = B.CODTETVND)
                                     INNER JOIN MRT.T0100043 D ON (D.CODCLI = C.CODCLI)
                                                            AND D.DATDSTCLI IS NULL
                                     INNER JOIN MRT.T0100124 Y ON (Y.CODSUP = A.CODSUP)
                                                            AND Y.CODSUP <> 70001
                                      LEFT JOIN MRT.RLCPMCCLIECS RACRC ON RACRC.CODCLI = C.CODCLI AND RACRC.CODPMC = :CODPMC
                                     WHERE (RACRC.CODCLI IS NULL)]]>.Value

        'SQL concatenado pois ao passar os parametros com AND OR o tempo e resposta foi muito lento (perde os indices)
        If (Not vo.CODCLI Is Nothing) Then
            sql += " AND (D.CODCLI = : CODCLI)"
            parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(vo.CODCLI))
        End If
        If (Not vo.NOMCLI Is Nothing) Then
            vo.NOMCLI = "%" + UCase(vo.NOMCLI) + "%"
            sql += " AND (UPPER(D.NOMCLI) LIKE : NOMCLI)"
            parameterMapper.Parameters.Add(":NOMCLI", ValueDBProperty(vo.NOMCLI))
        End If

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(vo.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function ListarClientesExcludentes(vo As VO.PromocaoClientesExcludentes) As List(Of VO.PromocaoClientesExcludentes)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT  
                                              RACRC.CODPMC,
                                              RACRC.CODCLI,
                                              C.NOMCLI
                                         FROM MRT.RLCPMCCLIECS RACRC
                                        INNER JOIN MRT.T0100043 C ON (C.CODCLI = RACRC.CODCLI)
                                        WHERE (CODPMC IS NULL OR CODPMC = :CODPMC)]]>.Value

        If (Not vo.NOMCLI Is Nothing) Then
            vo.NOMCLI = "%" + UCase(vo.NOMCLI) + "%"
            sql += " AND (UPPER(C.NOMCLI) LIKE :NOMCLI)"
            parameterMapper.Parameters.Add(":NOMCLI", ValueDBProperty(vo.NOMCLI))
        End If

        If (Not vo.CODCLI Is Nothing) Then
            sql += " AND (RACRC.CODCLI = : CODCLI)"
            parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(vo.CODCLI))
        End If

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(vo.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function
End Class

<TableAttribute("MRT.RLCPMCCLIECS")>
Public Class PromocaoClientesExcludentesDAOConfiguration
    Inherits DAOConfiguration

    <IgnoreMap>
    Property NOMCLI As String
End Class