﻿Imports Core
Imports VO

Public Class RelacaoAcaoMecanicaMixDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaMix)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaMixDAOConfiguration()
    End Sub
#Region "Mix Grupo Mercadoria"

    Public Function EditMixGrupoMercadoria(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODGRPMER,
                                               A.DESGRPMER,
                                               B.CODACOCMC,
                                               B.NUMSEQRGRACOCMC,
                                               B.NUMSEQMIXACOCMC
                                        FROM MRT.T0100167 A
                                        LEFT JOIN MRT.RLCACOCMCRGRMIX B ON (B.CODGRPMER = A.CODGRPMER) AND B.CODACOCMC = :CODACOCMC 
                                                                                                       AND B.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                                                                                                       AND B.CODFMLMER IS NULL
                                                 WHERE A.DATDSTGRPMER IS NULL
                                                 ORDER BY A.DESGRPMER         
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPMER", "DESGRPMER", "CODACOCMC", "NUMSEQRGRACOCMC", "NUMSEQMIXACOCMC"}))
    End Function

    Public Function ListarGrupoMercadoriaSelecionados(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODACOCMC,
                                               NUMSEQRGRACOCMC,
                                               CODGRPMER 
                                        FROM MRT.RLCACOCMCRGRMIX                                        
                                        WHERE (:CODACOCMC IS NULL OR :CODACOCMC = CODACOCMC)
                                        AND (:NUMSEQRGRACOCMC IS NULL OR :NUMSEQRGRACOCMC = NUMSEQRGRACOCMC)
                                        AND (CODGRPMER IS NOT NULL)
                                        AND (CODFMLMER IS NULL)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "CODGRPMER"}))
    End Function
#End Region

#Region "Mix Familia Mercadoria"

    Public Function PesquisaMixFamiliaMercadoria(VO As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not VO.DESFMLMER Is Nothing Then
            VO.DESFMLMER = "%" + UCase(VO.DESFMLMER) + "%"
        End If

        If Not VO.DESGRPMER Is Nothing Then
            VO.DESGRPMER = "%" + UCase(VO.DESGRPMER) + "%"
        End If

        Dim sql As String = <![CDATA[
                                       SELECT A.CODFMLMER,
                                              A.CODGRPMER,
                                              A.DESFMLMER,
                                              C.DESGRPMER
                                         FROM MRT.T0100159 A
                                         LEFT JOIN MRT.RLCACOCMCRGRMIX B ON B.CODFMLMER = A.CODFMLMER AND B.CODGRPMER = A.CODGRPMER AND B.CODACOCMC = :CODACOCMC AND B.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC AND B.CODCLSMER IS NULL
                                         LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = A.CODGRPMER
                                        WHERE (A.DATDSTFMLMER IS NULL)
                                          AND (B.CODFMLMER IS NULL)
                                          AND (:DESFMLMER IS NULL OR UPPER(A.DESFMLMER) LIKE :DESFMLMER)    
                                          AND (:DESGRPMER IS NULL OR UPPER(C.DESGRPMER) LIKE :DESGRPMER)
                                          AND (:CODFMLMER IS NULL OR A.CODFMLMER = :CODFMLMER)
                                          AND (:CODGRPMER IS NULL OR A.CODGRPMER = :CODGRPMER)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":DESFMLMER", ValueDBProperty(VO.DESFMLMER))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(VO.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(VO.NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":DESGRPMER", ValueDBProperty(VO.DESGRPMER))
        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(VO.CODFMLMER))
        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(VO.CODGRPMER))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFMLMER", "CODGRPMER", "DESFMLMER", "DESGRPMER"}))
    End Function

    Public Function ListarMixFamiliaMercadoriaAdicionados(VO As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC,
                                               A.CODFMLMER,
                                               B.DESFMLMER,
                                               A.CODGRPMER,
                                               C.DESGRPMER,
                                               A.NUMSEQMIXACOCMC
                                        FROM MRT.RLCACOCMCRGRMIX A
                                        LEFT JOIN MRT.T0100159 B ON B.CODFMLMER = A.CODFMLMER AND A.CODGRPMER = B.CODGRPMER
                                        LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = A.CODGRPMER
                                        WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                          AND (A.NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                                          AND (A.CODFMLMER IS NOT NULL)
                                          AND (A.CODCLSMER IS NULL)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(VO.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(VO.NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFMLMER", "DESFMLMER", "NUMSEQRGRACOCMC", "CODACOCMC", "CODGRPMER", "DESGRPMER", "NUMSEQMIXACOCMC"}))
    End Function

    Public Function VerificarSeFamiliaMercadoriaEstaCadastrada(ByVal CODFMLMER As Decimal, ByVal CODGRPMER As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT  CODFMLMER
                                               ,DESFMLMER
                                        FROM MRT.T0100159
                                        WHERE   (CODFMLMER = :CODFMLMER)
                                          AND   (CODGRPMER = :CODGRPMER)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(CODFMLMER))
        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(CODGRPMER))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFMLMER", "DESFMLMER"}))
    End Function
#End Region

#Region "Mix Classe Mercadoria"

    Public Function PesquisaMixClasseMercadoria(VO As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not VO.DESCLSMER Is Nothing Then
            VO.DESCLSMER = "%" + UCase(VO.DESCLSMER) + "%"
        End If

        If Not VO.DESGRPMER Is Nothing Then
            VO.DESGRPMER = "%" + UCase(VO.DESGRPMER) + "%"
        End If

        If Not VO.DESFMLMER Is Nothing Then
            VO.DESFMLMER = "%" + UCase(VO.DESFMLMER) + "%"
        End If

        Dim sql As String = <![CDATA[
                                        SELECT A.CODCLSMER,
                                               A.CODGRPMER,
                                               A.CODFMLMER,
                                               A.DESCLSMER,
                                               C.DESGRPMER,
                                               D.DESFMLMER
                                        FROM MRT.T0100132 A
                                        LEFT JOIN MRT.RLCACOCMCRGRMIX B ON B.CODCLSMER = A.CODCLSMER AND B.CODGRPMER = A.CODGRPMER AND B.CODACOCMC = :CODACOCMC AND B.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC AND B.CODMER IS NULL
                                        LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = A.CODGRPMER
                                        LEFT JOIN MRT.T0100159 D ON D.CODFMLMER = A.CODFMLMER AND D.CODGRPMER = A.CODGRPMER
                                        WHERE (A.DATDSTCLSMER IS NULL)
                                          AND B.CODCLSMER IS NULL
                                          AND (:DESCLSMER IS NULL OR UPPER(A.DESCLSMER) LIKE :DESCLSMER)
                                          AND (:DESGRPMER IS NULL OR UPPER(C.DESGRPMER) LIKE :DESGRPMER)
                                          AND (:DESFMLMER IS NULL OR UPPER(D.DESFMLMER) LIKE :DESFMLMER)
                                          AND (:CODGRPMER IS NULL OR  A.CODGRPMER =  :CODGRPMER)
                                          AND (:CODFMLMER IS NULL OR  A.CODFMLMER =  :CODFMLMER)
                                          AND (:CODCLSMER IS NULL OR  A.CODCLSMER =  :CODCLSMER)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":DESCLSMER", ValueDBProperty(VO.DESCLSMER))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(VO.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(VO.NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":DESGRPMER", ValueDBProperty(VO.DESGRPMER))
        parameterMapper.Parameters.Add(":DESFMLMER", ValueDBProperty(VO.DESFMLMER))
        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(VO.CODGRPMER))
        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(VO.CODFMLMER))
        parameterMapper.Parameters.Add(":CODCLSMER", ValueDBProperty(VO.CODCLSMER))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODCLSMER", "DESCLSMER", "CODGRPMER", "DESGRPMER", "CODFMLMER", "DESFMLMER"}))
    End Function

    Public Function ListarMixClasseMercadoriaAdicionados(VO As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC,
                                               A.CODCLSMER,
                                               B.DESCLSMER,
                                               A.CODGRPMER,
                                               C.DESGRPMER,
                                               A.NUMSEQMIXACOCMC,
                                               A.CODFMLMER,
                                               D.DESFMLMER
                                        FROM MRT.RLCACOCMCRGRMIX A
                                        LEFT JOIN MRT.T0100132 B ON B.CODCLSMER = A.CODCLSMER AND A.CODGRPMER = B.CODGRPMER AND A.CODFMLMER = B.CODFMLMER
                                        LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = A.CODGRPMER
                                        LEFT JOIN MRT.T0100159 D ON D.CODFMLMER = A.CODFMLMER AND D.CODGRPMER = A.CODGRPMER
                                        WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                            AND (A.NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                                            AND (A.CODCLSMER IS NOT NULL)
                                            AND (A.CODMER IS NULL)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(VO.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(VO.NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODCLSMER", "DESCLSMER", "NUMSEQRGRACOCMC", "CODACOCMC", "CODGRPMER", "DESGRPMER", "NUMSEQMIXACOCMC", "DESFMLMER", "CODFMLMER"}))
    End Function

    Public Function VerificarSeClasseMercadoriaEstaCadastrada(ByVal CODCLSMER As Decimal, ByVal CODGRPMER As Decimal, ByVal CODFMLMER As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT  CODCLSMER
                                               ,DESCLSMER
                                        FROM MRT.T0100132
                                        WHERE (CODCLSMER = :CODCLSMER)
                                        AND   (CODGRPMER = :CODGRPMER)
                                        AND   (CODFMLMER = :CODFMLMER)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODCLSMER", ValueDBProperty(CODCLSMER))
        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(CODGRPMER))
        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(CODFMLMER))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODCLSMER", "DESCLSMER"}))
    End Function
#End Region

#Region "Mix Mercadoria"

    Public Function PesquisaMixMercadoria(VO As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not VO.DESMER Is Nothing Then
            VO.DESMER = "%" + UCase(VO.DESMER) + "%"
        End If

        If Not VO.DESGRPMER Is Nothing Then
            VO.DESGRPMER = "%" + UCase(VO.DESGRPMER) + "%"
        End If

        If Not VO.DESFMLMER Is Nothing Then
            VO.DESFMLMER = "%" + UCase(VO.DESFMLMER) + "%"
        End If

        If Not VO.DESCLSMER Is Nothing Then
            VO.DESCLSMER = "%" + UCase(VO.DESCLSMER) + "%"
        End If

        If Not VO.NOMFRN Is Nothing Then
            VO.NOMFRN = "%" + UCase(VO.NOMFRN) + "%"
        End If

        Dim sql As String = <![CDATA[
            SELECT A.CODMER
                 , A.DESMER
                 , A.CODGRPMER
                 , C.DESGRPMER
                 , A.CODFMLMER
                 , D.DESFMLMER
                 , A.CODCLSMER
                 , E.DESCLSMER
                 , frn.CODFRN
                 , frn.NOMFRN
                 , b.CODGRPMIXMERPMC
              FROM MRT.T0100086  A
              LEFT JOIN MRT.RLCACOCMCRGRMIX B ON B.CODMER = A.CODMER AND B.CODGRPMER IS NULL  AND B.CODACOCMC = :CODACOCMC AND B.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
              LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = A.CODGRPMER
              LEFT JOIN MRT.T0100159 D ON D.CODFMLMER = A.CODFMLMER AND D.CODGRPMER = A.CODGRPMER
              LEFT JOIN MRT.T0100132 E ON E.CODCLSMER = A.CODCLSMER AND E.CODFMLMER = A.CODFMLMER AND E.CODGRPMER = A.CODGRPMER
              LEFT JOIN MRT.T0100426 FRN ON A.CODFRNPCPMER = frn.CODFRN
             WHERE (A.DATDSTMER IS NULL)
               AND (B.CODMER IS NULL)
               AND (:DESMER IS NULL OR UPPER(A.DESMER) LIKE :DESMER)    
               AND (:DESGRPMER IS NULL OR UPPER(C.DESGRPMER) LIKE :DESGRPMER)  
               AND (:DESFMLMER IS NULL OR UPPER(D.DESFMLMER) LIKE :DESFMLMER)  
               AND (:DESCLSMER IS NULL OR UPPER(E.DESCLSMER) LIKE :DESCLSMER)  
               AND (:CODMER IS NULL OR  A.CODMER = :CODMER)
               AND (:CODGRPMER IS NULL OR  c.CODGRPMER =  :CODGRPMER)
               AND (:CODFMLMER IS NULL OR  D.CODFMLMER =  :CODFMLMER)
               AND (:CODCLSMER IS NULL OR  E.CODCLSMER =  :CODCLSMER)                                          
               AND (:CODFRN IS NULL OR  frn.CODFRN = :CODFRN)
               AND (:NOMFRN IS NULL OR UPPER(frn.NOMFRN) LIKE :NOMFRN)  
            ]]>.Value

        parameterMapper.Parameters.Add(":DESMER", ValueDBProperty(VO.DESMER))
        parameterMapper.Parameters.Add(":DESGRPMER", ValueDBProperty(VO.DESGRPMER))
        parameterMapper.Parameters.Add(":DESFMLMER", ValueDBProperty(VO.DESFMLMER))
        parameterMapper.Parameters.Add(":DESCLSMER", ValueDBProperty(VO.DESCLSMER))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(VO.CODMER))
        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(VO.CODGRPMER))
        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(VO.CODFMLMER))
        parameterMapper.Parameters.Add(":CODCLSMER", ValueDBProperty(VO.CODCLSMER))
        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(VO.CODFRN))
        parameterMapper.Parameters.Add(":NOMFRN", ValueDBProperty(VO.NOMFRN))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(VO.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(VO.NUMSEQRGRACOCMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODGRPMER", "DESGRPMER", "CODFMLMER", "DESFMLMER", "CODCLSMER", "DESCLSMER", "CODFRN", "NOMFRN", "CODGRPMIXMERPMC"}))
    End Function

    Public Function PesquisaMixMercadoriaEdl(VO As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not VO.DESMER Is Nothing Then
            VO.DESMER = "%" + UCase(VO.DESMER) + "%"
        End If

        If Not VO.NOMFRN Is Nothing Then
            VO.NOMFRN = "%" + UCase(VO.NOMFRN) + "%"
        End If

        Dim sql As String = <![CDATA[SELECT DISTINCT A.CODPRD AS CODMER
                                , A.DESPRD AS DESMER
                                , D.CODFRN
                                , C.RAZSOCFRN AS NOMFRN
                            FROM MRT.CADPRDSMA A
                            INNER JOIN MRT.RLCPRDFRNPLOSMA B ON B.CODPRD = A.CODPRD AND B.DATDST IS NULL
                            INNER JOIN MRT.T0148968 C ON C.CODFRN = B.CODFRN AND C.DATDSTFRN IS NULL
                            INNER JOIN MRT.RLCCNDPGTCPTBTB D ON D.CODFRN = B.CODFRN AND D.DATDST IS NULL
                            INNER JOIN MRT.T0138768 E ON E.CODFRN = B.CODFRN AND E.TIPFRNACOCMC = 1
                            WHERE E.CODACOCMC = :CODACOCMC
                            AND (:CODMER IS NULL OR  A.CODPRD = :CODMER)
                            AND (:DESMER IS NULL OR UPPER(A.DESPRD) LIKE :DESMER)
                            AND (:CODFRN IS NULL OR  D.CODFRN = :CODFRN)
                            AND (:NOMFRN IS NULL OR UPPER(C.RAZSOCFRN) LIKE :NOMFRN)]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(VO.CODACOCMC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(VO.CODMER))
        parameterMapper.Parameters.Add(":DESMER", ValueDBProperty(VO.DESMER))
        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(VO.CODFRN))
        parameterMapper.Parameters.Add(":NOMFRN", ValueDBProperty(VO.NOMFRN))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODFRN", "NOMFRN"}))
    End Function

    Public Function PesquisaMixMercadoriaPorPromocao(ByVal CODPMC As Decimal, ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
           SELECT promocaoMix.CODMER
                , mercadoria.DESMER
                , mercadoria.CODGRPMER
                , grupoMercadoria.DESGRPMER
            FROM MRT.T0107935 promocao
                INNER JOIN MRT.RLCPMCRGRMIX promocaoMix on promocaoMix.CODPMC = promocao.CODPMC
                INNER JOIN MRT.T0100086 mercadoria on mercadoria.CODMER = promocaoMix.CODMER
                INNER JOIN MRT.T0100167 grupoMercadoria on grupoMercadoria.CODGRPMER = mercadoria.CODGRPMER
                LEFT JOIN MRT.RLCACOCMCRGRMIX RelacaoAcaoMecanicaMix on RelacaoAcaoMecanicaMix.CODMER = mercadoria.CODMER
                  AND RelacaoAcaoMecanicaMix.CODACOCMC = :CODACOCMC
                  AND RelacaoAcaoMecanicaMix.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
            WHERE promocao.CODPMC = :CODPMC
                AND promocaoMix.CODMER IS NOT NULL 
                AND RelacaoAcaoMecanicaMix.CODMER IS NULL
            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODGRPMER", "DESGRPMER"}))
    End Function

    Public Function ListarMixMercadoriaAdicionados(VO As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC,
                                               A.CODMER,
                                               B.DESMER,
                                               B.CODGRPMER,
                                               C.DESGRPMER,
                                               A.NUMSEQMIXACOCMC,
                                               A.CODGRPMIXMERPMC
                                          FROM MRT.RLCACOCMCRGRMIX A
                                          LEFT JOIN MRT.T0100086 B ON B.CODMER = A.CODMER
                                          LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = B.CODGRPMER
                                          WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                          AND (A.NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                                          AND (A.CODMER IS NOT NULL)
                                        ORDER BY A.CODGRPMIXMERPMC
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(VO.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(VO.NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "NUMSEQRGRACOCMC", "CODACOCMC", "CODGRPMER", "DESGRPMER", "NUMSEQMIXACOCMC", "CODGRPMIXMERPMC"}))
    End Function

    Public Function ListarMixMercadoriaEdlAdicionados(VO As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC,
                                               A.CODMER,
                                               B.DESPRD AS DESMER,
                                               '' AS CODGRPMER,
                                               '' AS DESGRPMER,
                                               A.NUMSEQMIXACOCMC,
                                               A.CODGRPMIXMERPMC
                                          FROM MRT.RLCACOCMCRGRMIX A
                                          LEFT JOIN MRT.CADPRDSMA B ON B.CODPRD = A.CODMER
                                          --LEFT JOIN MRT.T0100086 B ON B.CODMER = A.CODMER
                                          --LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = B.CODGRPMER
                                          WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                          AND (A.NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                                          AND (A.CODMER IS NOT NULL)
                                        ORDER BY A.CODGRPMIXMERPMC
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(VO.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(VO.NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "NUMSEQRGRACOCMC", "CODACOCMC", "CODGRPMER", "DESGRPMER", "NUMSEQMIXACOCMC", "CODGRPMIXMERPMC"}))
    End Function
#End Region

#Region "Mix Fornecedor"

    Public Function PesquisaMixFornecedor(VO As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not VO.NOMFRN Is Nothing Then
            VO.NOMFRN = "%" + UCase(VO.NOMFRN) + "%"
        End If

        Dim sql As String = <![CDATA[
                    SELECT DISTINCT A.CODFRN
                                    ,A.NOMFRN
                            FROM MRT.T0100426  A
                            INNER JOIN MRT.T0113625 B ON  A.CODCPR = B.CODCPR
                            INNER JOIN MRT.T0118570 C ON B.CODGERPRD = C.CODGERPRD 
                            INNER JOIN MRT.T0123183 D ON C.CODDRTCMP = D.CODDRTCMP
                            LEFT JOIN MRT.RLCACOCMCRGRMIX X ON X.CODFRN = A.CODFRN AND X.CODACOCMC = :CODACOCMC AND X.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                            WHERE (A.DATDSTDIVFRN IS NULL)
                                AND (X.CODFRN IS NULL)
                                AND (:NOMFRN IS NULL OR UPPER(A.NOMFRN) LIKE :NOMFRN)
                                AND (:CODFRN IS NULL OR  A.CODFRN =  :CODFRN)
                                AND (:CODDIVCMP IS NULL OR C.CODDIVCMP = :CODDIVCMP)
                                AND (:CODDRTCMP IS NULL OR D.CODDRTCMP = :CODDRTCMP)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":NOMFRN", ValueDBProperty(VO.NOMFRN))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(VO.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(VO.NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(VO.CODFRN))

        parameterMapper.Parameters.Add(":CODDRTCMP", ValueDBProperty(VO.CODDRTCMP))
        parameterMapper.Parameters.Add(":CODDIVCMP", ValueDBProperty(VO.CODDIVCMP))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFRN", "NOMFRN"}))
    End Function

    Public Function ListarMixFornecedorAdicionados(VO As VO.RelacaoAcaoMecanicaMix) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT  A.CODACOCMC
                                               ,A.NUMSEQRGRACOCMC
                                               ,A.CODFRN
                                               ,B.NOMFRN
                                               ,A.NUMSEQMIXACOCMC
                                               ,A.CODGRPMIXMERPMC
                                        FROM MRT.RLCACOCMCRGRMIX A
                                        LEFT JOIN MRT.T0100426 B ON B.CODFRN = A.CODFRN
                                        WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                          AND (A.NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                                          AND (A.CODFRN IS NOT NULL)                                        
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(VO.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(VO.NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFRN", "NOMFRN", "NUMSEQMIXACOCMC", "CODGRPMIXMERPMC"}))
    End Function

    'Public Function VerificarSeFornecedorEstaCadastrado(ByVal CODFRN As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)

    '    Dim parameterMapper As ParameterMapper = New ParameterMapper()

    '    Dim sql As String = <![CDATA[
    '                                    SELECT  CODFRN
    '                                           ,NOMFRN
    '                                    FROM MRT.T0100426
    '                                    WHERE (CODFRN = :CODFRN)                                   
    '                                ]]>.Value

    '    parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(CODFRN))

    '    Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFRN", "NOMFRN"}))
    'End Function
#End Region

    Public Function SelectCarregarDropGrupoMercadoria() As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODGRPMER,
                                               DESGRPMER
                                        FROM MRT.T0100167 
                                                 WHERE DATDSTGRPMER IS NULL
                                                 ORDER BY DESGRPMER    
                                    ]]>.Value
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPMER", "DESGRPMER"}))
    End Function

    Public Function SelectCarregarDropCategoria(ByVal CODGRPMER As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT
                                                 CODFMLMER,
                                                 CODGRPMER,
                                                 DESFMLMER,
                                                 CODEMP,
                                                 CODCTGMOVMER,
                                                 DATDSTFMLMER
                                        FROM     MRT.T0100159
                                        WHERE  (CODGRPMER IS NULL OR CODGRPMER = :CODGRPMER)
                                                 AND DATDSTFMLMER IS NULL
                                        ORDER BY DESFMLMER
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(CODGRPMER))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFMLMER", "DESFMLMER"}))
    End Function

    Public Function SelectCarregarDropSubCategoria(ByVal CODGRPMER As Decimal, ByVal CODFMLMER As Decimal) As List(Of VO.RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT
                                                    CODCLSMER,
                                                    CODFMLMER,
                                                    DESCLSMER,
                                                    CODEMP,
                                                    CODGRPMER,
                                                    PERMRGMNMCTG,
                                                    PERMRGMAXCTG,
                                                    PERCMSMAXRLCMRGCTG,
                                                    CODCTGMOVMER,
                                                    DATDSTCLSMER,
                                                    CODPAPCTG,
                                                    INDCTGMPB,
                                                    VLRMAXVNDMPB
                                        FROM     MRT.T0100132
                                        WHERE    (CODGRPMER IS NULL OR CODGRPMER = :CODGRPMER)
                                             AND (CODFMLMER IS NULL OR CODFMLMER = :CODFMLMER)
                                             AND DATDSTCLSMER IS NULL
                                        ORDER BY DESCLSMER
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(CODGRPMER))
        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(CODFMLMER))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODCLSMER", "DESCLSMER"}))
    End Function

    'Selecionar maior NUMSEQMIXACOCMC
    Public Function SelectMaxNumSeqMixAcoCmc(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As VO.RelacaoAcaoMecanicaMix

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT NVL(MAX( NVL(NUMSEQMIXACOCMC, 0)) + 1, 1) AS NUMSEQMIXACOCMC
                                        FROM
                                        MRT.RLCACOCMCRGRMIX A 
                                        WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                          AND (A.NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"NUMSEQMIXACOCMC"})).FirstOrDefault()
    End Function

    Friend Sub DeleteMixFornecedor(relacaoAcaoMecanicaMixFornecedor As RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()



        Dim sql As String = <![CDATA[DELETE 
                                        FROM MRT.RLCACOCMCRGRMIX A
                                        WHERE A.CODACOCMC = :CODACOCMC
                                          AND A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                                          AND A.CODFRN IS NOT NULL
                                          AND (:NUMSEQMIXACOCMC IS NULL OR A.NUMSEQMIXACOCMC = :NUMSEQMIXACOCMC)
                                          AND (:CODFRN IS NULL OR A.CODFRN = :CODFRN)
                            ]]>.Value


        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQMIXACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.NUMSEQMIXACOCMC))
        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.CODFRN))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Friend Sub DeleteMixMercadoria(relacaoAcaoMecanicaMixFornecedor As RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()



        Dim sql As String = <![CDATA[DELETE 
                                        FROM MRT.RLCACOCMCRGRMIX A
                                        WHERE A.CODACOCMC = :CODACOCMC
                                          AND A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                                          AND A.CODMER IS NOT NULL
                                          AND (:NUMSEQMIXACOCMC IS NULL OR A.NUMSEQMIXACOCMC = :NUMSEQMIXACOCMC)
                                          AND (:CODMER IS NULL OR A.CODMER = :CODMER)
                            ]]>.Value


        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQMIXACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.NUMSEQMIXACOCMC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.CODMER))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Friend Sub DeleteMixClasseMercadoria(relacaoAcaoMecanicaMixFornecedor As RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()



        Dim sql As String = <![CDATA[DELETE 
                                        FROM MRT.RLCACOCMCRGRMIX A
                                        WHERE A.CODACOCMC = :CODACOCMC
                                          AND A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                                          AND A.CODCLSMER IS NOT NULL
                                          AND A.CODGRPMER IS NOT NULL
                                          AND (:NUMSEQMIXACOCMC IS NULL OR A.NUMSEQMIXACOCMC = :NUMSEQMIXACOCMC)
                                          AND (:CODCLSMER IS NULL OR A.CODCLSMER = :CODCLSMER)
                                          AND (:CODGRPMER IS NULL OR A.CODGRPMER = :CODGRPMER)
                            ]]>.Value


        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQMIXACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.NUMSEQMIXACOCMC))
        parameterMapper.Parameters.Add(":CODCLSMER", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.CODCLSMER))
        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.CODGRPMER))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Friend Sub DeleteMixFamiliaMercadoria(relacaoAcaoMecanicaMixFornecedor As RelacaoAcaoMecanicaMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()



        Dim sql As String = <![CDATA[DELETE 
                                        FROM MRT.RLCACOCMCRGRMIX A
                                        WHERE A.CODACOCMC = :CODACOCMC
                                          AND A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                                          AND A.CODFMLMER IS NOT NULL
                                          AND A.CODGRPMER IS NOT NULL
                                          AND (:NUMSEQMIXACOCMC IS NULL OR A.NUMSEQMIXACOCMC = :NUMSEQMIXACOCMC)
                                          AND (:CODFMLMER IS NULL OR A.CODFMLMER = :CODFMLMER)
                                          AND (:CODGRPMER IS NULL OR A.CODGRPMER = :CODGRPMER)
                            ]]>.Value


        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQMIXACOCMC", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.NUMSEQMIXACOCMC))
        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.CODFMLMER))
        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(relacaoAcaoMecanicaMixFornecedor.CODGRPMER))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

#Region "Count Mix Controle Botão 'Configurar' e 'Selecionar'"
    Public Function SelectCountMix(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As VO.RelacaoAcaoMecanicaMix

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       SELECT 
                                          (SELECT CASE WHEN COUNT(CODGRPMER) > 0 THEN 1 ELSE 0 END FROM MRT.RLCACOCMCRGRMIX WHERE CODACOCMC = :CODACOCMC AND NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC AND CODFMLMER IS NULL)  AS INDGRUPOMERCADORIA
                                        , (SELECT CASE WHEN COUNT(CODFMLMER) > 0 THEN 1 ELSE 0 END FROM MRT.RLCACOCMCRGRMIX WHERE CODACOCMC = :CODACOCMC AND NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC AND CODCLSMER IS NULL)  AS INDFAMILIAMERCADORIA
                                        , (SELECT CASE WHEN COUNT(CODCLSMER) > 0 THEN 1 ELSE 0 END FROM MRT.RLCACOCMCRGRMIX WHERE CODACOCMC = :CODACOCMC AND NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC AND CODMER IS NULL)  AS INDCLASSEMERCADORIA
                                        , (SELECT CASE WHEN COUNT(CODMER) > 0 THEN 1 ELSE 0 END FROM MRT.RLCACOCMCRGRMIX WHERE CODACOCMC = :CODACOCMC AND NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC AND CODFRN IS NULL)  AS INDMERCADORIA
                                        , (SELECT CASE WHEN COUNT(CODFRN) > 0 THEN 1 ELSE 0 END FROM MRT.RLCACOCMCRGRMIX WHERE CODACOCMC = :CODACOCMC AND NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)  AS INDFORNECEDOR
                                        FROM DUAL
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"INDGRUPOMERCADORIA", "INDFAMILIAMERCADORIA", "INDCLASSEMERCADORIA", "INDMERCADORIA", "INDFORNECEDOR"})).FirstOrDefault()

    End Function
#End Region
End Class

<TableAttribute("MRT.RLCACOCMCRGRMIX")>
Public Class RelacaoAcaoMecanicaMixDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQMIXACOCMC As Nullable(Of Decimal)
    Property CODGRPMER As Nullable(Of Decimal)
    Property CODFMLMER As Nullable(Of Decimal)
    Property CODCLSMER As Nullable(Of Decimal)
    Property CODMER As Nullable(Of Decimal)
    Property CODFRN As Nullable(Of Decimal)
    Property CODGRPMIXMERPMC As Nullable(Of Decimal)

    <IgnoreMap>
    Property DATDSTGRPMER As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESGRPMER As String
    <IgnoreMap>
    Property DESFMLMER As String
    <IgnoreMap>
    Property DESCLSMER As String
    <IgnoreMap>
    Property DESMER As String
    <IgnoreMap>
    Property NOMFRN As String
    <IgnoreMap>
    Property INDGRUPOMERCADORIA As Nullable(Of Boolean)
    <IgnoreMap>
    Property INDFAMILIAMERCADORIA As Nullable(Of Boolean)
    <IgnoreMap>
    Property INDCLASSEMERCADORIA As Nullable(Of Boolean)
    <IgnoreMap>
    Property INDMERCADORIA As Nullable(Of Boolean)
    <IgnoreMap>
    Property INDFORNECEDOR As Nullable(Of Boolean)
    <IgnoreMap>
    Property ListMixFamiliaMercadoria As List(Of RelacaoAcaoMecanicaMixViewModel)
    <IgnoreMap>
    Property relacaoAcaoMecanicaMixGrupoMercadoria As List(Of RelacaoAcaoMecanicaMixViewModel)
    <IgnoreMap>
    Property ItensPesquisaFamiliaMercadoria As List(Of VO.RelacaoAcaoMecanicaMix)


    <IgnoreMap>
    Property CODDRTCMP As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODDIVCMP As Nullable(Of Decimal)
    <IgnoreMap>
    Property TIPFRNACOCMC As Nullable(Of Decimal)
End Class

