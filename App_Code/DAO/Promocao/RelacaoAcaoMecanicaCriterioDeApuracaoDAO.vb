﻿Imports Core

Public Class RelacaoAcaoMecanicaCriterioDeApuracaoDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaCriterioDeApuracaoDaoConfiguration()
    End Sub

    Public Function EditRelacaoAcaoMecanicaCriterioDeApuracaoList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC,
                                               A.NUMSEQCTRACOCMC,
                                               A.CODCTRPMC,
                                               A.VLRLIMMNMFXACTRPMC,
                                               A.QDEPTOFXACTRPMC,
                                               A.CODGRPMIXMERPMC,
                                               A.TIPEQCCALCTRPMC,
                                               A.TIPEDENIVPRMPMC,
                                               A.VLRLIMMAXFXACTRPMC,
                                               A.DESCTRMIXPMC,
                                               A.TAMIVLFXAEQCCALCTR,
                                               A.INDTIPPTOPMC,
                                               A.VLRLIMMAXCTR,
                                               B.TIPPMC
                                        FROM MRT.RLCACOCMCRGRCTR A
                                        LEFT JOIN MRT.RLCACOCMCRGR B ON (A.CODACOCMC = B.CODACOCMC AND A.NUMSEQRGRACOCMC = B.NUMSEQRGRACOCMC)
                                        WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                        AND (A.NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                                        AND (B.TIPPMC IS NULL OR B.TIPPMC = :TIPPMC)
                                        ORDER BY A.TIPEDENIVPRMPMC ASC
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":TIPPMC", ValueDBProperty(TIPPMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "NUMSEQCTRACOCMC", "CODCTRPMC", "VLRLIMMNMFXACTRPMC", "QDEPTOFXACTRPMC", "CODGRPMIXMERPMC", "TIPEQCCALCTRPMC", "TIPEDENIVPRMPMC", "VLRLIMMAXFXACTRPMC", "DESCTRMIXPMC", "TAMIVLFXAEQCCALCTR", "INDTIPPTOPMC", "VLRLIMMAXCTR", "TIPPMC"}))
    End Function

    Public Function EditRelacaoAcaoMecanicaCriterioDeApuracao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal) As VO.RelacaoAcaoMecanicaCriterioDeApuracao

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC,
                                               A.NUMSEQCTRACOCMC,
                                               A.CODCTRPMC,
                                               A.VLRLIMMNMFXACTRPMC,
                                               A.QDEPTOFXACTRPMC,
                                               A.CODGRPMIXMERPMC,
                                               A.TIPEQCCALCTRPMC,
                                               A.TIPEDENIVPRMPMC,
                                               A.VLRLIMMAXFXACTRPMC,
                                               A.DESCTRMIXPMC,
                                               A.TAMIVLFXAEQCCALCTR,
                                               B.TIPPMC
                                        FROM MRT.RLCACOCMCRGRCTR A
                                        LEFT JOIN MRT.RLCACOCMCRGR B ON (A.CODACOCMC = B.CODACOCMC AND A.NUMSEQRGRACOCMC = B.NUMSEQRGRACOCMC)
                                        WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                        AND (A.NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                                        AND (B.TIPPMC IS NULL OR B.TIPPMC = :TIPPMC)

                                        
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":TIPPMC", ValueDBProperty(TIPPMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "NUMSEQCTRACOCMC", "CODCTRPMC", "VLRLIMMNMFXACTRPMC", "QDEPTOFXACTRPMC", "CODGRPMIXMERPMC", "TIPEQCCALCTRPMC", "TIPPMC", "TIPEDENIVPRMPMC", "VLRLIMMAXFXACTRPMC", "DESCTRMIXPMC", "TAMIVLFXAEQCCALCTR"})).FirstOrDefault()
    End Function


    Public Function VerificaPromocaoTipoBrinde(ByVal CODACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC,
                                               A.NUMSEQCTRACOCMC,
                                               A.CODCTRPMC,
                                               A.VLRLIMMNMFXACTRPMC,
                                               A.QDEPTOFXACTRPMC,
                                               A.CODGRPMIXMERPMC,
                                               A.TIPEQCCALCTRPMC,
                                               A.TIPEDENIVPRMPMC,
                                               A.VLRLIMMAXFXACTRPMC,
                                               A.DESCTRMIXPMC,
                                               A.TAMIVLFXAEQCCALCTR,
                                               A.INDTIPPTOPMC,
                                               A.VLRLIMMAXCTR
                                        FROM MRT.RLCACOCMCRGRCTR A
                                        WHERE (A.CODACOCMC = :CODACOCMC)
                                        AND A.INDTIPPTOPMC = 0

                                        
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "NUMSEQCTRACOCMC", "CODCTRPMC", "VLRLIMMNMFXACTRPMC", "QDEPTOFXACTRPMC", "CODGRPMIXMERPMC", "TIPEQCCALCTRPMC", "TIPEDENIVPRMPMC", "VLRLIMMAXFXACTRPMC", "DESCTRMIXPMC", "TAMIVLFXAEQCCALCTR", "INDTIPPTOPMC", "VLRLIMMAXCTR"})).ToList()
    End Function


End Class

<TableAttribute("MRT.RLCACOCMCRGRCTR")>
Public Class RelacaoAcaoMecanicaCriterioDeApuracaoDaoConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQCTRACOCMC As Nullable(Of Decimal)
    Property CODCTRPMC As Nullable(Of Decimal)
    Property TIPEDENIVPRMPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property VLRLIMMNMFXACTRPMC As Nullable(Of Decimal)
    Property VLRLIMMAXFXACTRPMC As Nullable(Of Decimal)
    Property QDEPTOFXACTRPMC As Nullable(Of Decimal)
    Property DESCTRMIXPMC As String
    <PrimaryKey>
    Property CODGRPMIXMERPMC As Nullable(Of Decimal)
    Property TAMIVLFXAEQCCALCTR As Nullable(Of Decimal)
    <IgnoreMap>
    Property TIPPMC As Nullable(Of Decimal)

End Class
