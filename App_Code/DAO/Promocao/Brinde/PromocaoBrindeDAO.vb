﻿Imports Core

Public Class PromocaoBrindeDAO
    Inherits DAOBase(Of VO.PromocaoBrinde)

    Public Sub New()
        daoConfiguration = New PromocaoBrindeDAOConfiguration()
    End Sub

    Public Function VerificarDadosVolateisDaMercadoria(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[WITH TABAUX AS
                                         (
                                         -- SELECT DISTINCT A.CODPMC
                                         --                      , A.CODFILEMP
                                         --                      , A.CODEMP
                                         --                      , B.CODMER
                                         --                      , 'MERCAD. PRINCIPAL' AS FLGMER
                                         --                      , B.CODGRPMIXMERPMC 
                                         -- FROM MRT.T0107935 A
                                         --    , MRT.T0107994 B
                                         -- WHERE A.CODPMC = :CODPMC
                                         -- AND    A.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                         -- AND    A.CODFILEMP = :CODFILEMP
                                         -- AND    A.CODEMP    = 1
                                         -- AND    B.CODPMC = A.CODPMC
                                         -- AND    B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC
                                         -- UNION
                                          SELECT DISTINCT A.CODPMC
                                                                , A.CODFILEMP
                                                                , A.CODEMP
                                                                , B.CODPRMPMC AS CODMER
                                                               , 'BRINDE' AS FLGMER, 0 AS CODGRPMIXMERPMC
                                          FROM MRT.T0107935 A
                                                 , MRT.T0108044 B
                                          WHERE A.CODPMC = :CODPMC
                                          AND    A.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                          AND    A.CODFILEMP = :CODFILEMP
                                          AND    A.CODEMP    = 1
                                          AND    B.CODPMC = A.CODPMC
                                          AND    B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC)
                                         SELECT A.CODPMC
                                               , A.CODFILEMP
                                               , A.FLGMER
                                               , COALESCE(X.DESMER,'') AS DESMER
                                               , A.CODMER  AS CODMERPCP
                                               , B.CODMER  AS CODMERCST
                                               , C.CODMER  AS CODMERMRG 
                                               , A.CODGRPMIXMERPMC 
                                         FROM TABAUX A
                                              LEFT OUTER JOIN MRT.T0201350 B ON
                                                  B.CODFILEMP = A.CODFILEMP
                                                  AND B.CODMER     = A.CODMER
                                                  AND B.CODEMP    = A.CODEMP
                                                  AND B.VLRCSTPSEUNTDIRMER > 0
                                                  AND B.CODCNLDTB = 0
                                              LEFT OUTER JOIN MRT.T0111770 C
                                                  ON  C.CODEMP     = A.CODEMP
                                                  AND C.CODFILEMP = A.CODFILEMP
                                                  AND C.CODMER      = A.CODMER
                                                  AND C.DATDSTMER IS NULL
                                              LEFT OUTER JOIN MRT.T0100086 X 
                                                  ON  X.CODMER = A.CODMER 
                                                  AND X.CODEMP = 1 
                                         ORDER BY A.FLGMER DESC  
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocaoBrinde.CODPMC))
        parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(promocaoBrinde.CODFILEMP))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(promocaoBrinde.TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "CODFILEMP", "FLGMER", "DESMER", "CODMERPCP", "CODMERCST", "CODMERMRG", "CODGRPMIXMERPMC"}))

    End Function

    Public Function VerificarHistoricoDiarioMercadoriaLivroPrecos(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
			                             WITH TABAUX AS
                                         (
                                          --SELECT DISTINCT A.CODPMC
                                          --                     , A.CODFILEMP
                                          --                     , A.CODEMP
                                          --                     , B.CODMER
                                          --                     , 'MERCAD. PRINCIPAL' AS FLGMER
                                          --FROM MRT.T0107935 A
                                          --   , MRT.T0107994 B
                                          --WHERE A.CODPMC = :CODPMC
                                          --AND    A.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                          --AND    A.CODFILEMP = :CODFILEMP
                                          --AND    A.CODEMP    = 1
                                          --AND    B.CODPMC = A.CODPMC
                                          --AND    B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC
                                          --UNION
                                          SELECT DISTINCT A.CODPMC
                                                                , A.CODFILEMP
                                                                , A.CODEMP
                                                                , B.CODPRMPMC AS CODMER
                                                               , 'BRINDE' AS FLGMER
                                          FROM MRT.T0107935 A
                                                 , MRT.T0108044 B
                                          WHERE A.CODPMC = :CODPMC
                                          AND    A.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                          AND    A.CODFILEMP = :CODFILEMP
                                          AND    A.CODEMP    = 1
                                          AND    B.CODPMC = A.CODPMC
                                          AND    B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC)
                                         SELECT A.CODPMC
                                               , A.CODFILEMP
                                               , A.FLGMER
                                               , COALESCE(X.DESMER,'') AS DESMER
                                               , A.CODMER  AS CODMERPCP
                                               , B.CODMER AS CODMERCST
                                         FROM TABAUX A 
                                         LEFT OUTER JOIN MRT.T0106696 B 
                                                    ON  B.CODFILEMP = A.CODFILEMP 
                                                    AND B.CODMER    = A.CODMER 
                                                    AND B.CODEMP    = A.CODEMP
                                                    AND B.DATREFLIVPCO >= to_date(:DATREFLIVPCO,'dd/MM/yyyy')
                                         LEFT OUTER JOIN MRT.T0100086 X 
                                           ON  X.CODMER = A.CODMER 
                                           AND X.CODEMP = 1 
                                         ORDER BY A.FLGMER DESC 
                                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocaoBrinde.CODPMC))
        parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(promocaoBrinde.CODFILEMP))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(promocaoBrinde.TIPEDEPUBALVPMC))
        parameterMapper.Parameters.Add(":DATREFLIVPCO", ValueDBProperty(promocaoBrinde.DATREFLIVPCO))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "CODFILEMP", "FLGMER", "DESMER", "CODMERPCP", "CODMERCST"}))

    End Function

    Public Function CarregaBrinde() As List(Of VO.PromocaoBrinde)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                            CODPMC
                                    FROM MRT.T0108044
                                    ]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function ListaParaGrid(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        If (String.IsNullOrWhiteSpace(promocaoBrinde.FLGTRSMSGFIMPRMPMC)) Then
            promocaoBrinde.FLGTRSMSGFIMPRMPMC = "%" + promocaoBrinde.FLGTRSMSGFIMPRMPMC + "%"
        End If
        If (String.IsNullOrWhiteSpace(promocaoBrinde.DESPRMPMC)) Then
            promocaoBrinde.DESPRMPMC = "%" + promocaoBrinde.DESPRMPMC + "%"
        End If
        Dim sql As String = <![CDATA[SELECT PRM.CODCNDPGT
                                           , PRM.CODPMC
                                           , PRM.CODPRMPMC
                                           , PRM.DESPRMPMC
                                           , PRM.FLGTRSMSGFIMPRMPMC
                                           , PRM.INDACUBDEPMCFAT
                                           , PRM.INDISRBDEADI
                                           , PRM.PERMERRSVPRMPMC
                                           , PRM.QDEMERVNDPRMPMC
                                           , PRM.QDEMERMAXPRMPMC
                                           , PRM.QDEMERVNDPRMPMC
                                           , PRM.TIPEDENIVPRMPMC
                                           , PRM.TIPEDEPUBALVPMC
                                           , PRM.VLRPRMPMC
                                           , PRM.VLRUNTFNDMERPMC
                                           , PRM.VLRUNTVNDPRMPMC
                                           , PRM.QDEBDEMPLCTRPMC
                                           , PRM.DATECSPRD
                                           , PRM.CODFNCECS
                                           , FNCDEL.NOMFNC AS NOMFNCECS
                                           , QDEMERMAXPRMPMC-QDEMERVNDPRMPMC AS SALDO
                                           , ROUND(PRM.QDEMERVNDPRMPMC/(CASE WHEN COUNT(DISTINCT CLD.DATREF) <> 0 THEN COUNT(DISTINCT CLD.DATREF) ELSE 1 END), 2) AS MEDIA
                                    FROM MRT.T0108044 PRM
                                          INNER JOIN MRT.T0107935 PMC  ON PRM.TIPEDEPUBALVPMC = PMC.TIPEDEPUBALVPMC AND PRM.CODPMC = PMC.CODPMC 
                                          LEFT JOIN MRT.T0102321 CLD ON (CLD.DATREF BETWEEN PMC.DATINIPMC AND SYSDATE AND CLD.DATREF <= PMC.DATFIMPMC) AND CLD.NUMDIASMN NOT IN (6,7) AND FLGFRDBCO = ' '
                                          LEFT JOIN  MRT.T0100361 FNCDEL  ON FNCDEL.CODFNC = PRM.CODFNCECS
                                    WHERE (:CODPMC  IS NULL OR PMC.CODPMC = :CODPMC )
                                        AND(:CODCNDPGT  IS NULL OR PRM.CODCNDPGT = :CODCNDPGT )
                                        AND (:CODPRMPMC  IS NULL OR PRM.CODPRMPMC = :CODPRMPMC )
                                        AND (:FLGTRSMSGFIMPRMPMC  IS NULL OR PRM.FLGTRSMSGFIMPRMPMC LIKE: FLGTRSMSGFIMPRMPMC )
                                        AND (:DESPRMPMC  IS NULL OR PRM.DESPRMPMC LIKE :DESPRMPMC )
                                        AND (:TIPEDENIVPRMPMC  IS NULL OR PRM.TIPEDENIVPRMPMC = :TIPEDENIVPRMPMC )
                                        AND (:TIPEDEPUBALVPMC  IS NULL OR PRM.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC )
                                  GROUP BY PRM.CODCNDPGT,
                                          PRM.CODPMC,
                                          PRM.CODPRMPMC,
                                          PRM.DESPRMPMC,
                                          PRM.FLGTRSMSGFIMPRMPMC,
                                          PRM.INDACUBDEPMCFAT,
                                          PRM.INDISRBDEADI,
                                          PRM.PERMERRSVPRMPMC,
                                          PRM.QDEMERMAXPRMPMC,
                                          PRM.QDEMERVNDPRMPMC,
                                          PRM.TIPEDENIVPRMPMC,
                                          PRM.TIPEDEPUBALVPMC,
                                          PRM.VLRPRMPMC,
                                          PRM.VLRUNTVNDPRMPMC,
                                          PRM.QDEBDEMPLCTRPMC,
                                          PRM.VLRUNTFNDMERPMC,
                                          PRM.DATECSPRD,
                                          PRM.CODFNCECS,
                                          FNCDEL.NOMFNC 
                                 ORDER BY PRM.CODPMC,  PRM.TIPEDENIVPRMPMC, PRM.DESPRMPMC
                           ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocaoBrinde.CODPMC))
        parameterMapper.Parameters.Add(":CODCNDPGT", ValueDBProperty(promocaoBrinde.CODCNDPGT))
        parameterMapper.Parameters.Add(":CODPRMPMC", ValueDBProperty(promocaoBrinde.CODPRMPMC))
        parameterMapper.Parameters.Add(":FLGTRSMSGFIMPRMPMC", ValueDBProperty(promocaoBrinde.FLGTRSMSGFIMPRMPMC))
        parameterMapper.Parameters.Add(":DESPRMPMC", ValueDBProperty(promocaoBrinde.DESPRMPMC))
        parameterMapper.Parameters.Add(":TIPEDENIVPRMPMC", ValueDBProperty(promocaoBrinde.TIPEDENIVPRMPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(promocaoBrinde.TIPEDEPUBALVPMC))
        'Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODCNDPGT", "CODPMC", "CODPRMPMC", "DESPRMPMC", "FLGTRSMSGFIMPRMPMC", "INDACUBDEPMCFAT", "INDISRBDEADI", "PERMERRSVPRMPMC", "QDEMERVNDPRMPMC", "QDEMERMAXPRMPMC", "QDEMERVNDPRMPMC", "TIPEDENIVPRMPMC", "TIPEDEPUBALVPMC", "VLRPRMPMC", "VLRUNTFNDMERPMC", "VLRUNTVNDPRMPMC", "QDEBDEMPLCTRPMC", "SALDO", "MEDIA", "VLRUNTFNDMERPMC", "DATECSPRD", "CODFNCECS", "NOMFNCECS"}))

        Return connector.ExecuteReader(Of VO.PromocaoBrinde)(sql, parameterMapper).ToList()
    End Function

    Public Function SelectRelacaoPromocaoEValorPremioSemMerAco(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODACOCMC, 
                                            CODMER as CODPRMPMC,
                                            CODPMC,
                                            INDCOBIPRMRI,
                                            VLRUNTLIQMER,
                                            PERBFCBDE
                                       FROM MRT.T0161115 
                                       WHERE CODPMC = :CODPMC
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocaoBrinde.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "CODPRMPMC", "CODPMC", "INDCOBIPRMRI", "VLRUNTLIQMER", "PERBFCBDE"}))
    End Function

    Public Function SelectRelacaoPromocaoEValorPremioJoin(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                             BDE.CODACOCMC, 
                                             MER.DESMER   , 
                                             BDE.CODMER AS CODPRMPMC, 
                                             BDE.CODPMC, 
                                             BDE.INDCOBIPRMRI, 
                                             BDE.VLRUNTLIQMER, 
                                             nvl(ITEACOCMC.VLRUNTITEOCDACOCMC, 0) As CUSTO, BDE.PERBFCBDE  
                                    FROM     MRT.T0161115 BDE 
                                    INNER JOIN MRT.T0100086 MER ON BDE.CODMER = MER.CODMER AND MER.CODEMP = 1  
                                    left JOIN MRT.T0138776 ITEACOCMC ON BDE.CODACOCMC = ITEACOCMC.CODACOCMC 
                                    AND ITEACOCMC.CODMER = BDE.CODMER 

                                    WHERE BDE.CODMER = MER.CODMER AND MER.CODEMP = 1 
			                        AND BDE.CODPMC= :CODPMC
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocaoBrinde.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "CODPRMPMC", "CODPMC", "INDCOBIPRMRI", "VLRUNTLIQMER", "PERBFCBDE", "CUSTO"}))
    End Function

    Public Function SelectRelacaoPromocaoEValorPremio(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If (promocaoBrinde.CODACOCMC Is Nothing) Then
            Return Nothing
        End If

        Dim sql As String = <![CDATA[SELECT CODACOCMC, 
                                            CODMER AS CODPRMPMC,
                                            CODPMC,
                                            INDCOBIPRMRI,
                                            VLRUNTLIQMER,
                                            PERBFCBDE
                                       FROM MRT.T0161115 
                                       WHERE CODACOCMC = :CODACOCMC
                                         AND CODPMC = :CODPMC
                                         AND CODMER = :CODPRMPMC
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocaoBrinde.CODPMC))

        parameterMapper.Parameters.Add(":CODPRMPMC", ValueDBProperty(promocaoBrinde.CODPRMPMC))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(promocaoBrinde.CODACOCMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "CODPRMPMC", "CODPMC", "INDCOBIPRMRI", "VLRUNTLIQMER", "PERBFCBDE"}))
    End Function

    Public Function SelectRelacaoPromocaoEPremioAdicional(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If (promocaoBrinde.CODACOCMC Is Nothing) Then
            Return Nothing
        End If

        Dim sql As String = <![CDATA[SELECT CODPMC, 
                                            CODPMCADI,
                                            CODPRMADIPMC,
                                            CODPRMPMC,
                                            QDEMERPRMADIPMC,
                                            TIPEDEPUBALVPMC
                                       FROM MRT.T0132107
                                       WHERE TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                         AND CODPMC = :CODPMC
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocaoBrinde.CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(promocaoBrinde.TIPEDEPUBALVPMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMCADI", "CODPRMADIPMC", "CODPMC", "CODPRMPMC", "QDEMERPRMADIPMC", "TIPEDEPUBALVPMC"}))
    End Function

    Public Function SelectRelacaoPromocaoEValorPremioSemMer(promocaoBrinde As VO.PromocaoBrinde) As List(Of VO.PromocaoBrinde)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If (promocaoBrinde.CODACOCMC Is Nothing) Then
            Return Nothing
        End If

        Dim sql As String = <![CDATA[SELECT CODACOCMC, 
                                            CODMER as CODPRMPMC,
                                            CODPMC,
                                            INDCOBIPRMRI,
                                            VLRUNTLIQMER,
                                            PERBFCBDE
                                       FROM MRT.T0161115 
                                       WHERE CODACOCMC = :CODACOCMC
                                         AND CODPMC = :CODPMC
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocaoBrinde.CODPMC))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(promocaoBrinde.CODACOCMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "CODPRMPMC", "CODPMC", "INDCOBIPRMRI", "VLRUNTLIQMER", "PERBFCBDE"}))
    End Function

    ''' <summary>
    ''' Inserir Relacao Promocao Valor e Premio
    ''' </summary>
    ''' <param name="promocaoBrinde"></param>
    ''' <returns></returns>
    Public Function InserirRelacaoPromocaoValor(promocaoBrinde As VO.PromocaoBrinde) As Object

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.T0161115(
                                            CODMER,
                                            VLRUNTLIQMER,
                                            CODPMC,
                                            INDCOBIPRMRI,
                                            CODACOCMC,
                                            PERBFCBDE)
                                    VALUES( :CODPRMPMC,
                                            :VLRUNTLIQMER,
                                            :CODPMC,
                                            :INDCOBIPRMRI,
                                            :CODACOCMC,
                                            :PERBFCBDE)]]>.Value

        parameterMapper.Parameters.Add(":CODPRMPMC", promocaoBrinde.CODPRMPMC)
        parameterMapper.Parameters.Add(":VLRUNTLIQMER", promocaoBrinde.VLRUNTLIQMER)
        parameterMapper.Parameters.Add(":PERBFCBDE", promocaoBrinde.PERBFCBDE)
        parameterMapper.Parameters.Add(":CODPMC", promocaoBrinde.CODPMC)
        parameterMapper.Parameters.Add(":INDCOBIPRMRI", promocaoBrinde.INDCOBIPRMRI)
        parameterMapper.Parameters.Add(":CODACOCMC", promocaoBrinde.CODACOCMC)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    ''' <summary>
    ''' Remove Relacao Promocao Valor e Premio
    ''' </summary>
    ''' <param name="promocaoBrinde"></param>
    ''' <returns></returns>
    Public Function RemoveRelacaoPromocaoValor(promocaoBrinde As VO.PromocaoBrinde) As Object

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE FROM MRT.T0161115
                                    WHERE CODMER = :CODPRMPMC
                                          AND CODPMC = :CODPMC]]>.Value

        parameterMapper.Parameters.Add(":CODPRMPMC", promocaoBrinde.CODPRMPMC)
        parameterMapper.Parameters.Add(":CODPMC", promocaoBrinde.CODPMC)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function



    'Public Function SelectMax(CODPMC As Decimal) As VO.PromocaoBrinde

    '    Dim parameterMapper As ParameterMapper = New ParameterMapper()

    '    Dim sql As String = <![CDATA[
    '                                    SELECT NVL(MAX( NVL(CODPRMPMC, 0)) + 1, 1) AS CODPRMPMC
    '                                    FROM
    '                                    MRT.T0108044 A 
    '                                    WHERE CODPMC = :CODPMC
    '                        ]]>.Value
    '    parameterMapper.Parameters.Add(":CODPMC", CODPMC)

    '    Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPRMPMC"})).FirstOrDefault()
    'End Function



    Public Function GetSelectPremioPorNivelPremiacao(ByVal CODPMC As Decimal, ByVal CODPRMPMC As Decimal, ByVal TIPEDENIVPRMPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoBrinde)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT
                                                 CODCNDPGT,
                                                 CODPMC,
                                                 CODPRMPMC,
                                                 DESPRMPMC,
                                                 FLGTRSMSGFIMPRMPMC,
                                                 INDACUBDEPMCFAT,
                                                 INDISRBDEADI,
                                                 PERMERRSVPRMPMC,
                                                 QDEMERMAXPRMPMC,
                                                 QDEMERVNDPRMPMC,
                                                 TIPEDENIVPRMPMC,
                                                 TIPEDEPUBALVPMC,
                                                 VLRPRMPMC,
                                                 VLRUNTVNDPRMPMC,
                                                 QDEBDEMPLCTRPMC,
                                                 VLRUNTFNDMERPMC
                                        FROM     MRT.T0108044
                                        WHERE    (CODPMC IS NULL OR CODPMC = :CODPMC)
                                                 AND (CODPRMPMC IS NULL OR CODPRMPMC = :CODPRMPMC)
                                                 AND (TIPEDENIVPRMPMC IS NULL OR TIPEDENIVPRMPMC = :TIPEDENIVPRMPMC)
                                                 AND (TIPEDEPUBALVPMC IS NULL OR TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)

                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":CODPRMPMC", ValueDBProperty(CODPRMPMC))
        parameterMapper.Parameters.Add(":TIPEDENIVPRMPMC", ValueDBProperty(TIPEDENIVPRMPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODCNDPGT", "CODPMC", "CODPRMPMC", "DESPRMPMC", "FLGTRSMSGFIMPRMPMC", "INDACUBDEPMCFAT", "INDISRBDEADI", "PERMERRSVPRMPMC", "QDEMERMAXPRMPMC", "QDEMERVNDPRMPMC", "TIPEDENIVPRMPMC", "TIPEDEPUBALVPMC", "VLRPRMPMC", "VLRUNTVNDPRMPMC", "QDEBDEMPLCTRPMC", "VLRUNTFNDMERPMC"}))

    End Function


    Public Sub UpdateAbastecerPromocao(TIPEDEPUBALVPMC As Decimal, CODPMC As Decimal, TIPEDENIVPRMPMC As Decimal, CODPRMPMC As Decimal, INDTIPISRMERPMC As Decimal, QDEMERISRPMC As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[  UPDATE MRT.T0108044
                                          SET QDEMERMAXPRMPMC  = (CASE WHEN :INDTIPISRMERPMC = 0 THEN 
                                                                        QDEMERMAXPRMPMC + :QDEMERISRPMC
                                                                    ELSE
                                                                        QDEMERMAXPRMPMC - :QDEMERISRPMC                                           
                                                                    END)
                                        WHERE TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                          AND CODPMC = :CODPMC
                                          AND TIPEDENIVPRMPMC = :TIPEDENIVPRMPMC
                                          AND CODPRMPMC = :CODPRMPMC
                            ]]>.Value

        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", TIPEDEPUBALVPMC)
        parameterMapper.Parameters.Add(":CODPMC", CODPMC)
        parameterMapper.Parameters.Add(":TIPEDENIVPRMPMC", TIPEDENIVPRMPMC)
        parameterMapper.Parameters.Add(":CODPRMPMC", CODPRMPMC)
        parameterMapper.Parameters.Add(":INDTIPISRMERPMC", INDTIPISRMERPMC)
        parameterMapper.Parameters.Add(":QDEMERISRPMC", QDEMERISRPMC)

        connector.ExecuteScalar(sql, parameterMapper)

    End Sub


    Public Sub RemoverBrindePromocaoLiberada(TIPEDEPUBALVPMC As Decimal, CODPMC As Decimal, TIPEDENIVPRMPMC As Decimal, CODPRMPMC As Decimal, DATECSPRD As DateTime, CODFNCECS As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[  UPDATE MRT.T0108044
                                          SET DATECSPRD  = :DATECSPRD
                                            , CODFNCECS = :CODFNCECS
                                        WHERE TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                          AND CODPMC = :CODPMC
                                          AND TIPEDENIVPRMPMC = :TIPEDENIVPRMPMC
                                          AND CODPRMPMC = :CODPRMPMC
                            ]]>.Value

        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", TIPEDEPUBALVPMC)
        parameterMapper.Parameters.Add(":CODPMC", CODPMC)
        parameterMapper.Parameters.Add(":TIPEDENIVPRMPMC", TIPEDENIVPRMPMC)
        parameterMapper.Parameters.Add(":CODPRMPMC", CODPRMPMC)
        parameterMapper.Parameters.Add(":DATECSPRD", DATECSPRD)
        parameterMapper.Parameters.Add(":CODFNCECS", CODFNCECS)

        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Sub RetornaBrindePromocaoLiberada(TIPEDEPUBALVPMC As Decimal, CODPMC As Decimal, TIPEDENIVPRMPMC As Decimal, CODPRMPMC As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[  UPDATE MRT.T0108044
                                          SET DATECSPRD = NULL
                                            , CODFNCECS = NULL
                                        WHERE TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                          AND CODPMC = :CODPMC
                                          AND TIPEDENIVPRMPMC = :TIPEDENIVPRMPMC
                                          AND CODPRMPMC = :CODPRMPMC
                            ]]>.Value

        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", TIPEDEPUBALVPMC)
        parameterMapper.Parameters.Add(":CODPMC", CODPMC)
        parameterMapper.Parameters.Add(":TIPEDENIVPRMPMC", TIPEDENIVPRMPMC)
        parameterMapper.Parameters.Add(":CODPRMPMC", CODPRMPMC)

        connector.ExecuteScalar(sql, parameterMapper)

    End Sub


    Public Function ListarBrindePromocaoNaoExcluido(CODPMC As Decimal, TIPEDENIVPRMPMC As Decimal) As List(Of VO.PromocaoBrinde)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[  SELECT  CODPMC
                                       FROM MRT.T0108044
                                       WHERE CODPMC = :CODPMC
                                          AND TIPEDENIVPRMPMC = :TIPEDENIVPRMPMC
                                          AND DATECSPRD IS NULL
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", CODPMC)
        parameterMapper.Parameters.Add(":TIPEDENIVPRMPMC", TIPEDENIVPRMPMC)


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC"}))
    End Function
End Class



<TableAttribute("MRT.T0108044")>
Public Class PromocaoBrindeDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)

    Property TIPEDEPUBALVPMC As String

    <IgnoreMap>
    Property SALDO As Nullable(Of Decimal)
    <IgnoreMap>
    Property MEDIA As Nullable(Of Decimal)
    <IgnoreMap>
    Property CUSTO As Nullable(Of Decimal)
    '<IgnoreMap>
    'Property CODMER As Nullable(Of Decimal)

    <IgnoreMap>
    Property CODACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDCOBIPRMRI As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLRUNTLIQMER As Nullable(Of Decimal)
    <IgnoreMap>
    Property PERBFCBDE As Nullable(Of Decimal)


    <IgnoreMap>
    Property CODPMCADI As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODPRMADIPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEMERPRMADIPMC As Nullable(Of Decimal)

    <IgnoreMap>
    Property CODFILEMP As Nullable(Of Decimal)
    <IgnoreMap>
    Property FLGMER As String
    <IgnoreMap>
    Property DESMER As String
    <IgnoreMap>
    Property CODMERPCP As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODMERCST As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODMERMRG As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODGRPMIXMERPMC As Nullable(Of Decimal)

    <IgnoreMap>
    Property ERRO As String
    <IgnoreMap>
    Property TIPO As String
    <IgnoreMap>
    Property DATREFLIVPCO As Nullable(Of Date)

    'Relacao Acao Comercial

    <IgnoreMap>
    Property CODFNCGRCPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDISRVLDMERPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDTIPISRMERPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property NUMSEQISRQDEMERPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEMERISRPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLRTOTMERISRPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property DATGRCRLC As Nullable(Of Date)
    <IgnoreMap>
    Property RATEAR As Boolean

    <IgnoreMap>
    Property NOMFNCECS As String
End Class


