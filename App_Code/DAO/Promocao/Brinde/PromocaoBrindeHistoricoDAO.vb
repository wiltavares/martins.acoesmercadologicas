﻿Imports Core

Public Class PromocaoBrindeHistoricoDAO
    Inherits DAOBase(Of VO.PromocaoBrindeHistorico)

    Public Sub New()
        daoConfiguration = New PromocaoBrindeHistoricoDAOConfiguration()
    End Sub


    Public Function ListHistorico(TIPEDEPUBALVPMC As Decimal, CODPRMPMC As Decimal, CODPMC As Decimal, TIPEDENIVPRMPMC As Decimal) As List(Of VO.PromocaoBrindeHistorico)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()



        Dim sql As String = <![CDATA[
            SELECT A.CODFNC
                 , B.NOMFNC 
                 , A.DATGRCHST
                 , A.CODPMC
                 , A.TIPEDENIVPRMPMC
                 , A.CODMER
                 , C.DESPRMPMC
                 , A.NUMSEQ
                 , A.INDSITMER   
              FROM MRT.HSTSITBDEPMC A
              LEFT JOIN MRT.T0100361 B ON A.CODFNC = B.CODFNC
              INNER JOIN MRT.T0108044 C ON C.CODPMC = A.CODPMC AND C.CODPMC = A.CODPMC 
              AND C.TIPEDENIVPRMPMC = A.TIPEDENIVPRMPMC AND C.CODPRMPMC = A.CODMER
             WHERE (C.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)
               AND (A.CODPMC = :CODPMC) 
               AND (A.TIPEDENIVPRMPMC = :TIPEDENIVPRMPMC) 
               AND (A.CODMER = :CODMER)
          ]]>.Value
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(CODPRMPMC))
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDENIVPRMPMC", ValueDBProperty(TIPEDENIVPRMPMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFNC", "NOMFNC", "DATGRCHST", "CODPMC", "TIPEDENIVPRMPMC", "CODMER", "DESPRMPMC", "NUMSEQ", "INDSITMER"}))


    End Function

End Class


<TableAttribute("MRT.HSTSITBDEPMC")>
Public Class PromocaoBrindeHistoricoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property TIPEDENIVPRMPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODMER As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQ As Nullable(Of Decimal)

    Property DATGRCHST As Nullable(Of DateTime)
    Property INDSITMER As Nullable(Of Decimal)
    Property CODFNC As Nullable(Of Decimal)


    <IgnoreMap>
    Property DESPRMPMC As String
    <IgnoreMap>
    Property NOMFNC As String
End Class


