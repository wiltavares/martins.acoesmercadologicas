﻿Imports Core

Public Class PromocaoPublicoAlvoSegmentoClienteDAO
    Inherits DAOBase(Of VO.PromocaoPublicoAlvoSegmentoCliente)

    Public Sub New()
        daoConfiguration = New PromocaoPublicoAlvoSegmentoClienteDAOConfiguration()
    End Sub

    Public Function ListaTipoSegmentoCliente(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoSegmentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT B.CODSGMNGCCLI,
                                               B.DESSGMNGCCLI,
                                               A.CODPMC,
                                               A.TIPEDEPUBALVPMC

                                        FROM
                                        MRT.CADSGMNGCCLI B
                                        LEFT JOIN MRT.RLCPMCSGMNGCCLI A  ON (A.CODSGMNGCCLI = B.CODSGMNGCCLI) AND A.CODPMC = :CODPMC 
                                                                                                              AND A.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODSGMNGCCLI", "DESSGMNGCCLI", "CODPMC", "TIPEDEPUBALVPMC"}))
    End Function

    Public Function ListaTipoSegmentoClienteSelecionado(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoSegmentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODPMC,
                                               TIPEDEPUBALVPMC,
                                               CODSGMNGCCLI 
                                        FROM MRT.RLCPMCSGMNGCCLI                                        
                                        WHERE (:CODPMC IS NULL OR :CODPMC = CODPMC)
                                        AND (:TIPEDEPUBALVPMC IS NULL OR :TIPEDEPUBALVPMC = TIPEDEPUBALVPMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "TIPEDEPUBALVPMC", "CODSGMNGCCLI"}))
    End Function

End Class


<TableAttribute("MRT.RLCPMCSGMNGCCLI")>
Public Class PromocaoPublicoAlvoSegmentoClienteDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODSGMNGCCLI As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESSGMNGCCLI As String
End Class