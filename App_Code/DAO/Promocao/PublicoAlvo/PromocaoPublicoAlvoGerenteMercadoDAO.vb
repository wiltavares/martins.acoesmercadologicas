﻿Imports Core
Imports VO

Public Class PromocaoPublicoAlvoGerenteMercadoDAO
    Inherits DAOBase(Of VO.PromocaoPublicoAlvoGerenteMercado)

    Public Sub New()
        daoConfiguration = New PromocaoPublicoAlvoGerenteMercadoDAOConfiguration()
    End Sub

    Public Function ListarGerenteMercado(vo As VO.PromocaoPublicoAlvoGerenteMercado) As List(Of VO.PromocaoPublicoAlvoGerenteMercado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not vo.NOMSUP Is Nothing Then
            vo.NOMSUP = "%" + UCase(vo.NOMSUP) + "%"
        End If
        If Not vo.NOMGER Is Nothing Then
            vo.NOMGER = "%" + UCase(vo.NOMGER) + "%"
        End If

        Dim sql As String = <![CDATA[
                                        SELECT
                                                A.CODSUP, 
                                                A.NOMSUP,
                                                A.CODGER,
                                                B.NOMGER
                                        FROM     MRT.T0100124 A
                                        LEFT JOIN MRT.T0100051 B ON (B.CODGER = A.CODGER)
                                        LEFT JOIN MRT.T0109687 RACRS ON RACRS.CODSUP = A.CODSUP AND RACRS.CODPMC = :CODPMC AND RACRS.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                        WHERE (:NOMSUP IS NULL OR UPPER(A.NOMSUP) LIKE :NOMSUP)
                                        AND (:NOMGER IS NULL OR UPPER(B.NOMGER) LIKE :NOMGER)
                                        AND (:CODSUP IS NULL OR A.CODSUP = :CODSUP)
                                        AND (:CODGER IS NULL OR A.CODGER = :CODGER)
                                        AND (RACRS.CODSUP IS NULL)           
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODSUP", ValueDBProperty(vo.CODSUP))
        parameterMapper.Parameters.Add(":NOMSUP", ValueDBProperty(vo.NOMSUP))
        parameterMapper.Parameters.Add(":CODGER", ValueDBProperty(vo.CODGER))
        parameterMapper.Parameters.Add(":NOMGER", ValueDBProperty(vo.NOMGER))
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(vo.CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(vo.TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODSUP", "NOMSUP"}))
    End Function



    Public Function ListarPublicoAlvoGerenteMercado(vo As VO.PromocaoPublicoAlvoGerenteMercado) As List(Of VO.PromocaoPublicoAlvoGerenteMercado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT 
                                                RACRS.CODPMC,
                                                RACRS.TIPEDEPUBALVPMC,
                                                RACRS.CODSUP,
                                                A.NOMSUP
                                        FROM MRT.T0109687 RACRS
                                        INNER JOIN MRT.T0100124 A ON (A.CODSUP = RACRS.CODSUP)
                                        WHERE (:CODPMC IS NULL OR :CODPMC = RACRS.CODPMC)
                                        AND (:TIPEDEPUBALVPMC IS NULL OR :TIPEDEPUBALVPMC = TIPEDEPUBALVPMC)

                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(vo.CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(vo.TIPEDEPUBALVPMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function


    Public Sub Delete(filtro As PromocaoPublicoAlvoGerenteMercado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE FROM MRT.T0109687  
                                      WHERE CODPMC = :CODPMC 
                                        AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC 
                                        AND (:CODSUP IS NULL OR CODSUP = :CODSUP)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(filtro.CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(filtro.TIPEDEPUBALVPMC))
        parameterMapper.Parameters.Add(":CODSUP", ValueDBProperty(filtro.CODSUP))

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub

End Class


<TableAttribute("MRT.T0109687")>
Public Class PromocaoPublicoAlvoGerenteMercadoDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODSUP As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMSUP As String
    <IgnoreMap>
    Property CODGER As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMGER As String
End Class