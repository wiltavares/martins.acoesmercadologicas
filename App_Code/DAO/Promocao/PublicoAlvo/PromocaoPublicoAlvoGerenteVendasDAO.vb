﻿Imports Core

Public Class PromocaoPublicoAlvoGerenteVendasDAO
    Inherits DAOBase(Of VO.PromocaoPublicoAlvoGerenteVendas)

    Public Sub New()
        daoConfiguration = New PromocaoPublicoAlvoGerenteVendasDAOConfiguration()
    End Sub

    Public Function ListarGerenteVendas(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoGerenteVendas)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                      SELECT B.CODGER,
                                             B.NOMGER,
                                             A.CODPMC,
                                             A.TIPEDEPUBALVPMC
                                        FROM MRT.T0100051 B
                                        LEFT JOIN MRT.T0109679 A  ON (A.CODGER = B.CODGER) AND A.CODPMC = :CODPMC 
                                                                                                  AND A.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                       ORDER BY B.CODGER
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGER", "NOMGER", "CODPMC", "TIPEDEPUBALVPMC"}))
    End Function

    Public Function ListarGerenteVendasSelecionados(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoGerenteVendas)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODPMC,
                                               TIPEDEPUBALVPMC,
                                               CODGER 
                                        FROM MRT.T0109679                                        
                                        WHERE (:CODPMC IS NULL OR :CODPMC = CODPMC)
                                        AND (:TIPEDEPUBALVPMC IS NULL OR :TIPEDEPUBALVPMC = TIPEDEPUBALVPMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "TIPEDEPUBALVPMC", "CODGER"}))
    End Function

End Class


<TableAttribute("MRT.T0109679")>
Public Class PromocaoPublicoAlvoGerenteVendasDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODGER As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMGER As String
End Class