﻿Imports Core

Public Class PromocaoPublicoAlvoNegocioClienteDAO
    Inherits DAOBase(Of VO.PromocaoPublicoAlvoNegocioCliente)

    Public Sub New()
        daoConfiguration = New PromocaoPublicoAlvoNegocioClienteDAOConfiguration()
    End Sub

    Public Function ListarTipoNegocioCliente(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoNegocioCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT B.CODUNDESRNGC,
                                               B.DESUNDESRNGC,
                                               A.CODPMC,
                                               A.TIPEDEPUBALVPMC
                                        FROM
                                        MRT.T0131275 B
                                        LEFT JOIN MRT.RLCPMCUNDESRNGC A  ON (A.CODUNDESRNGC = B.CODUNDESRNGC) AND A.CODPMC = :CODPMC 
                                                                                                              AND A.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODUNDESRNGC", "DESUNDESRNGC", "CODPMC", "TIPEDEPUBALVPMC"}))
    End Function

    Public Function ListarTipoNegocioClienteSelecionado(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoNegocioCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODPMC,
                                               TIPEDEPUBALVPMC,
                                               CODUNDESRNGC 
                                        FROM MRT.RLCPMCUNDESRNGC                                        
                                        WHERE (:CODPMC IS NULL OR :CODPMC = CODPMC)
                                          AND (:TIPEDEPUBALVPMC IS NULL OR :TIPEDEPUBALVPMC = TIPEDEPUBALVPMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "TIPEDEPUBALVPMC", "CODUNDESRNGC"}))
    End Function
End Class


<TableAttribute("MRT.RLCPMCUNDESRNGC")>
Public Class PromocaoPublicoAlvoNegocioClienteDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODUNDESRNGC As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESUNDESRNGC As String
End Class