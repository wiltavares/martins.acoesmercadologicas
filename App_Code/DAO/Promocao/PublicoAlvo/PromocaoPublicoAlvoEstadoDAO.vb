﻿Imports Core

Public Class PromocaoPublicoAlvoEstadoDAO
    Inherits DAOBase(Of VO.PromocaoPublicoAlvoEstado)

    Public Sub New()
        daoConfiguration = New PromocaoPublicoAlvoEstadoDAOConfiguration()
    End Sub

    Public Function ListarEstadoUniao(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoEstado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                          SELECT B.CODESTUNI,
                                                 B.NOMESTUNI,
                                                 A.CODPMC,                                                                                                  
                                                 A.TIPEDEPUBALVPMC,
                                                 B.CODDIVREG
                                            FROM
                                            MRT.T0100213 B
                                            LEFT JOIN MRT.T0109660 A  ON (A.CODESTUNI = B.CODESTUNI) AND A.CODPMC = :CODPMC 
                                                                                                     AND A.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC  
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODESTUNI", "NOMESTUNI", "CODPMC", "TIPEDEPUBALVPMC", "CODDIVREG"}))
    End Function

    Public Function ListarEstadoUniaoSelecionados(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoEstado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODPMC,
                                               TIPEDEPUBALVPMC,
                                               CODESTUNI 
                                        FROM MRT.T0109660                                        
                                        WHERE (:CODPMC IS NULL OR :CODPMC = CODPMC)
                                        AND (:TIPEDEPUBALVPMC IS NULL OR :TIPEDEPUBALVPMC = TIPEDEPUBALVPMC) 
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "TIPEDEPUBALVPMC", "CODESTUNI"}))
    End Function

End Class

<TableAttribute("MRT.T0109660")>
Public Class PromocaoPublicoAlvoEstadoDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODESTUNI As String
    <IgnoreMap>
    Property NOMESTUNI As String
    <IgnoreMap>
    Property CODDIVREG As String
End Class

