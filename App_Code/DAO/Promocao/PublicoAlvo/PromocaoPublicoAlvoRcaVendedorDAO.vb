﻿Imports Core
Imports VO

Public Class PromocaoPublicoAlvoRcaVendedorDAO
    Inherits DAOBase(Of VO.PromocaoPublicoAlvoRcaVendedor)

    Public Sub New()
        daoConfiguration = New PromocaoPublicoAlvoRcaVendedorDAOConfiguration()
    End Sub

    Public Function BuscarListaRcaVendedor(vo As VO.PromocaoPublicoAlvoRcaVendedor) As List(Of VO.PromocaoPublicoAlvoRcaVendedor)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not vo.NOMSUP Is Nothing Then
            vo.NOMSUP = "%" + UCase(vo.NOMSUP) + "%"
        End If
        If Not vo.NOMREP Is Nothing Then
            vo.NOMREP = "%" + UCase(vo.NOMREP) + "%"
        End If

        Dim sql As String = <![CDATA[
                                       SELECT A.CODREP,
                                              A.NOMREP,
                                              C.NOMSUP
                                        FROM MRT.T0100116 A
                                        LEFT JOIN MRT.CADTIPEDEVND B ON B.INDEDEPTCICT = 1 
                                        LEFT JOIN MRT.T0100124 C ON C.CODSUP = A.CODSUP 
                                        LEFT JOIN MRT.RLCPMCRCA RACRR ON RACRR.CODREP = A.CODREP AND RACRR.CODPMC = :CODPMC AND RACRR.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                        WHERE (A.DATDSTREP IS NULL)
                                        AND A.TIPEDEVND = B.TIPEDEVND
                                        AND (:NOMREP IS NULL OR UPPER(A.NOMREP) LIKE :NOMREP)                                        
                                        AND (:NOMSUP IS NULL OR UPPER(C.NOMSUP) LIKE :NOMSUP)
                                        AND (:CODSUP IS NULL OR A.CODSUP = :CODSUP)
                                        AND (:CODREP IS NULL OR A.CODREP = :CODREP)
                                        AND (RACRR.CODREP IS NULL)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODSUP", ValueDBProperty(vo.CODSUP))
        parameterMapper.Parameters.Add(":NOMSUP", ValueDBProperty(vo.NOMSUP))
        parameterMapper.Parameters.Add(":CODREP", ValueDBProperty(vo.CODREP))
        parameterMapper.Parameters.Add(":NOMREP", ValueDBProperty(vo.NOMREP))
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(vo.CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(vo.TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function ListarPublicoAlvoRcaVendedor(vo As VO.PromocaoPublicoAlvoRcaVendedor) As List(Of VO.PromocaoPublicoAlvoRcaVendedor)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       SELECT  
                                                RACRR.CODPMC,
                                                RACRR.TIPEDEPUBALVPMC,
                                                RACRR.CODREP,
                                                C.NOMREP
                                        FROM MRT.RLCPMCRCA RACRR
                                        LEFT JOIN MRT.T0100116 C ON (C.CODREP = RACRR.CODREP)
                                        WHERE (:CODPMC IS NULL OR :CODPMC = RACRR.CODPMC)
                                        AND (:TIPEDEPUBALVPMC IS NULL OR :TIPEDEPUBALVPMC = TIPEDEPUBALVPMC)

                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(vo.CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(vo.TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Sub Delete(filtro As PromocaoPublicoAlvoRcaVendedor)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE FROM MRT.RLCPMCRCA  
                                      WHERE CODPMC = :CODPMC 
                                        AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC 
                                        AND (:CODREP IS NULL OR CODREP = :CODREP)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(filtro.CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(filtro.TIPEDEPUBALVPMC))
        parameterMapper.Parameters.Add(":CODREP", ValueDBProperty(filtro.CODREP))

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub

End Class


<TableAttribute("MRT.RLCPMCRCA")>
Public Class PromocaoPublicoAlvoRcaVendedorDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODREP As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMREP As String
    <IgnoreMap>
    Property NOMGER As String
    <IgnoreMap>
    Property NOMSUP As String

    <IgnoreMap>
    Property CODSUP As Nullable(Of Decimal)

End Class