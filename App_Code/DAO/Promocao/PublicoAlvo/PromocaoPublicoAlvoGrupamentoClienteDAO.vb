﻿Imports Core

Public Class PromocaoPublicoAlvoGrupamentoClienteDAO
    Inherits DAOBase(Of VO.PromocaoPublicoAlvoGrupamentoCliente)

    Public Sub New()
        daoConfiguration = New PromocaoPublicoAlvoGrupamentoClienteDAOConfiguration()
    End Sub

    Public Function BuscarTipoGrupamentoCliente() As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT
                                                CODUNDESRNGC,
                                                DESUNDESRNGC
                                         FROM   MRT.T0131275
                            ]]>.Value
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODUNDESRNGC", "DESUNDESRNGC"}))
    End Function

    Public Function ListarGrupamentoCliente(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        
                                        SELECT B.CODGRPCLI,
                                               B.DESGRPCLI,
                                               A.CODPMC,
                                               A.TIPEDEPUBALVPMC

                                        FROM
                                        MRT.T0105347 B
                                        LEFT JOIN MRT.T0109717 A  ON (A.CODGRPCLI = B.CODGRPCLI) AND A.CODPMC = :CODPMC 
                                                                                                           AND A.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPCLI", "DESGRPCLI", "CODPMC", "TIPEDEPUBALVPMC"}))
    End Function

    Public Function ListarGrupamentoClienteSelecionadas(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODPMC,
                                               TIPEDEPUBALVPMC,
                                               CODGRPCLI 
                                        FROM MRT.T0109717                                        
                                        WHERE (:CODPMC IS NULL OR :CODPMC = CODPMC)
                                        AND (:TIPEDEPUBALVPMC IS NULL OR :TIPEDEPUBALVPMC = TIPEDEPUBALVPMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "TIPEDEPUBALVPMC", "CODGRPCLI"}))
    End Function

    Public Function BuscarGrupamentoClientePorBU(ByVal CODUNDESRNGC As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente)) As List(Of VO.PromocaoPublicoAlvoGrupamentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT   DISTINCT GRP.CODGRPCLI,
				                                          DES.DESGRPCLI,
                                                          DES.CODFILEMPEPD,
                                                          DES.CODSGMMCD,
                                                          DES.CODTRCINFGRPCLI,
                                                          DES.FLGACSVNDMIXGRP,
                                                          DES.FLGCALPRVETQ,
                                                          DES.FLGCONTRNGRP,
                                                          DES.FLGEMSLIVMIXGRP,
                                                          DES.FLGGRPCLIPLO,
                                                          DES.FLGIPRMERESGLIVGRP,
                                                          DES.FLGNAOUTZBLQQTA,
                                                          DES.FLGRCPAUTCTS,
                                                          DES.FLGTIPIPRLIVGRP,
                                                          DES.FLGUTZPCOESP,
                                                          DES.INDGRPCNDPGT,
                                                          DES.INDRTCCNDPGT,
                                                          DES.PERRDCVLRPEDMNM
                                                 FROM     MRT.T0100043 CLI
                                                          INNER JOIN MRT.T0100019 ATI  ON CLI.CODATI = ATI.CODATI
                                                          INNER JOIN MRT.T0114850 CNL ON CLI.CODCNL = CNL.CODCNL
                                                          INNER JOIN MRT.T0114893 BT  ON CNL.CODEQINGC = BT.CODEQINGC
                                                          INNER JOIN MRT.T0131275 BU ON BT.CODUNDESRNGC = BU.CODUNDESRNGC
                                                          INNER JOIN MRT.T0105398 GRP ON CLI.CODCLI = GRP.CODCLI
                                                          INNER JOIN MRT.T0105347 DES ON GRP.CODGRPCLI = DES.CODGRPCLI
                                                
                                    ]]>.Value

        Dim codigos = String.Empty
        For Each codigo In CODUNDESRNGC
            If (Not String.IsNullOrEmpty(codigos)) Then
                codigos += ", "
            End If
            codigos += codigo.CODUNDESRNGC.ToString()
        Next

        sql += " WHERE    BU.CODUNDESRNGC IN (" + codigos + ")"


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPCLI"}))
    End Function

End Class


<TableAttribute("MRT.T0109717")>
Public Class PromocaoPublicoAlvoGrupamentoClienteDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODGRPCLI As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESGRPCLI As String
    <IgnoreMap>
    Property CODUNDESRNGC As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESUNDESRNGC As String
End Class