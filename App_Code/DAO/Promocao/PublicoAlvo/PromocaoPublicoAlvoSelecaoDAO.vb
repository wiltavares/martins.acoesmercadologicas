﻿Imports Microsoft.VisualBasic

Public Class PromocaoPublicoAlvoSelecaoDAO
    Inherits DAOBase(Of VO.PromocaoPublicoAlvoSelecao)

    Public Sub New()
        daoConfiguration = New PromocaoPublicoAlvoSelecaoDAOConfiguration()
    End Sub

End Class

<TableAttribute("MRT.T0109652")>
Public Class PromocaoPublicoAlvoSelecaoDAOConfiguration
    Inherits DAOConfiguration
End Class