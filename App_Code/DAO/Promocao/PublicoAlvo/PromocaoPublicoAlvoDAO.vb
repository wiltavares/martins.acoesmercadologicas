﻿'Imports Core
'Imports VO

'Public Class PromocaoPublicoAlvoDAO
'    Inherits DAOBase(Of VO.PromocaoPublicoAlvo)

'    Public Sub New()
'        daoConfiguration = New PromocaoPublicoAlvoDAOConfiguration()
'    End Sub

'    Public Function SelectCountPublicoAlvo(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As VO.PromocaoPublicoAlvo

'        Dim parameterMapper As ParameterMapper = New ParameterMapper()

'        Dim sql As String = <![CDATA[
'                                       SELECT 
'                                              (SELECT CASE WHEN COUNT(CODESTUNI) > 0 THEN 1 ELSE 0 END FROM MRT.T0109660 WHERE CODPMC = :CODPMC AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)  AS INDESTADO
'                                            , (SELECT CASE WHEN COUNT(CODATI) > 0 THEN 1 ELSE 0 END FROM MRT.T0109709 WHERE CODPMC = :CODPMC AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)  AS INDATIVIDADE
'                                            , (SELECT CASE WHEN COUNT(CODGRPCLI) > 0 THEN 1 ELSE 0 END FROM MRT.T0109717 WHERE CODPMC = :CODPMC AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)  AS INDGRUPAMENTOCLIENTE
'                                            , (SELECT CASE WHEN COUNT(CODUNDESRNGC) > 0 THEN 1 ELSE 0 END FROM MRT.RLCPMCUNDESRNGC WHERE CODPMC = :CODPMC AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)  AS INDNEGOCIOCLIENTE
'                                            , (SELECT CASE WHEN COUNT(CODSGMNGCCLI) > 0 THEN 1 ELSE 0 END FROM MRT.RLCPMCSGMNGCCLI WHERE CODPMC = :CODPMC AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)  AS INDSEGMENTOCLIENTE 
'                                            , (SELECT CASE WHEN COUNT(CODCLI) > 0 THEN 1 ELSE 0 END FROM MRT.RLCPMCCLI WHERE CODPMC = :CODPMC AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)  AS INDCLIENTE 
'                                            , (SELECT CASE WHEN COUNT(CODREP) > 0 THEN 1 ELSE 0 END FROM MRT.RLCPMCRCA WHERE CODPMC = :CODPMC AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)  AS INDRCAVENDEDOR
'                                            , (SELECT CASE WHEN COUNT(CODGER) > 0 THEN 1 ELSE 0 END FROM MRT.T0109679 WHERE CODPMC = :CODPMC AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)  AS INDGERENTEVENDAS
'                                            , (SELECT CASE WHEN COUNT(CODSUP) > 0 THEN 1 ELSE 0 END FROM MRT.T0109687 WHERE CODPMC = :CODPMC AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)  AS INDGERENTEMERCADO
'                                        FROM DUAL

'                            ]]>.Value

'        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
'        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

'        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"INDESTADO", "INDATIVIDADE", "INDGRUPAMENTOCLIENTE", "INDNEGOCIOCLIENTE", "INDSEGMENTOCLIENTE", "INDCLIENTE", "INDRCAVENDEDOR", "INDGERENTEVENDAS", "INDGERENTEMERCADO"})).FirstOrDefault()

'    End Function
'End Class


'Public Class PromocaoPublicoAlvoDAOConfiguration
'    Inherits DAOConfiguration
'End Class