﻿Imports Core

Public Class PromocaoPublicoAlvoAtividadeDAO
    Inherits DAOBase(Of VO.PromocaoPublicoAlvoAtividade)

    Public Sub New()
        daoConfiguration = New PromocaoPublicoAlvoAtividadeDAOConfiguration()
    End Sub

    Public Function BuscarTipoAtividade() As List(Of VO.PromocaoPublicoAlvoAtividade)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT
                                                CODUNDESRNGC,
                                                DESUNDESRNGC
                                         FROM   MRT.T0131275
                            ]]>.Value
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODUNDESRNGC", "DESUNDESRNGC"}))
    End Function

    Public Function ListarAtividade(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoAtividade)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        
                                        SELECT B.CODATI,
                                               B.NOMATI,
                                               A.CODPMC,
                                               A.TIPEDEPUBALVPMC

                                        FROM
                                              MRT.T0100019 B
                                        LEFT JOIN MRT.T0109709 A  ON (A.CODATI = B.CODATI) AND A.CODPMC = :CODPMC 
                                                                                                  AND A.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC   
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODATI", "NOMATI", "CODPMC", "TIPEDEPUBALVPMC"}))
    End Function

    Public Function ListarAtividadesSelecionadas(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoPublicoAlvoAtividade)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODPMC,
                                               TIPEDEPUBALVPMC,
                                               CODATI 
                                        FROM MRT.T0109709                                        
                                        WHERE (:CODPMC IS NULL OR :CODPMC = CODPMC)
                                        AND (:TIPEDEPUBALVPMC IS NULL OR :TIPEDEPUBALVPMC = TIPEDEPUBALVPMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "TIPEDEPUBALVPMC", "CODATI"}))
    End Function

    Public Function BuscarAtividadePorBU(ByVal CODUNDESRNGC As List(Of VO.PromocaoPublicoAlvoAtividade)) As List(Of VO.PromocaoPublicoAlvoAtividade)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT   DISTINCT ATI.CODATI,
				                                          ATI.NOMATI,
                                                          ATI.CODCNL,
                                                          ATI.CODEMP,
                                                          ATI.CODEMPFRNINF,
                                                          ATI.CODGRPCLI,
                                                          ATI.CODGRPCLIATC,
                                                          ATI.DATULTALTCNL,
                                                          ATI.FLGPTDCADCLI,
                                                          ATI.FLGPTDCADCLIEVL,
                                                          ATI.FLGREQINFCPLATI,
                                                          ATI.FLGREQRPNTCN,
                                                          ATI.INDATIMPB,
                                                          ATI.INDOBRCADSCOCLI,
                                                          ATI.NUMCLICADATI,
                                                          ATI.QDEMESETBCLICAD,
                                                          ATI.QDEMESRVSCADCLI,
                                                          ATI.QDETPODSTCLIATI,
                                                          ATI.TIPINSESTREQ,
                                                   COUNT (CLI.CODCLI) AS CODCLI
                                                FROM     MRT.T0100043 CLI
                                                        INNER JOIN MRT.T0100019 ATI  ON CLI.CODATI = ATI.CODATI
                                                        INNER JOIN MRT.T0114850 CNL ON CLI.CODCNL = CNL.CODCNL
                                                        INNER JOIN MRT.T0114893 BT  ON CNL.CODEQINGC = BT.CODEQINGC
                                                        INNER JOIN MRT.T0131275 BU ON BT.CODUNDESRNGC = BU.CODUNDESRNGC
                                                        INNER JOIN MRT.T0105398 GRP ON CLI.CODCLI = GRP.CODCLI
         
                                    ]]>.Value
        Dim codigos = String.Empty
        For Each codigo In CODUNDESRNGC
            If (Not String.IsNullOrEmpty(codigos)) Then
                codigos += ", "
            End If
            codigos += codigo.CODUNDESRNGC.ToString()
        Next

        sql += " WHERE    BU.CODUNDESRNGC IN (" + codigos + ")"
        sql += "GROUP BY ATI.CODATI, ATI.NOMATI, ATI.CODCNL, "
        sql += "ATI.CODEMP, "
        sql += "ATI.CODEMPFRNINF, "
        sql += "ATI.CODGRPCLI, "
        sql += "ATI.CODGRPCLIATC, "
        sql += "ATI.DATULTALTCNL, "
        sql += "ATI.FLGPTDCADCLI, "
        sql += "ATI.FLGPTDCADCLIEVL, "
        sql += "ATI.FLGREQINFCPLATI, "
        sql += "ATI.FLGREQRPNTCN, "
        sql += "ATI.INDATIMPB, "
        sql += "ATI.INDOBRCADSCOCLI, "
        sql += "ATI.NUMCLICADATI, "
        sql += "ATI.QDEMESETBCLICAD, "
        sql += "ATI.QDEMESRVSCADCLI, "
        sql += "ATI.QDETPODSTCLIATI, "
        sql += "ATI.TIPINSESTREQ "
        sql += "HAVING COUNT(CLI.CODCLI) > 15"


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODATI"}))
    End Function

End Class


<TableAttribute("MRT.T0109709")>
Public Class PromocaoPublicoAlvoAtividadeDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODATI As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMATI As String
    <IgnoreMap>
    Property CODUNDESRNGC As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESUNDESRNGC As String
End Class