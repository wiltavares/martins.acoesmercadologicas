﻿Imports Core
Imports VO

Public Class PromocaoPublicoAlvoClienteDAO
    Inherits DAOBase(Of VO.PromocaoPublicoAlvoCliente)

    Public Sub New()
        daoConfiguration = New PromocaoPublicoAlvoClienteDAOConfiguration()
    End Sub

    Public Function BuscarListaCliente(vo As VO.PromocaoPublicoAlvoCliente) As List(Of VO.PromocaoPublicoAlvoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                SELECT DISTINCT D.CODCLI
                     , D.NOMCLI
                  FROM MRT.T0100116 A
                 INNER JOIN MRT.T0133715 B ON (B.CODREP = A.CODREP)
                 INNER JOIN MRT.T0133774 C ON (C.CODTETVND = B.CODTETVND)
                 INNER JOIN MRT.T0100043 D ON (D.CODCLI = C.CODCLI)
                                        AND D.DATDSTCLI IS NULL
                 INNER JOIN MRT.T0100124 Y ON (Y.CODSUP = A.CODSUP)
                                        AND Y.CODSUP <> 70001
                  LEFT JOIN MRT.RLCPMCCLI RACRC ON RACRC.CODCLI = C.CODCLI AND RACRC.CODPMC = :CODPMC AND RACRC.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                 WHERE (RACRC.CODCLI IS NULL)
                            ]]>.Value

        'SQL concatenado pois ao passar os parametros com AND OR o tempo e resposta foi muito lento (perde os indices)
        If (Not vo.CODCLI Is Nothing) Then
            sql += " AND (D.CODCLI = : CODCLI)"
            parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(vo.CODCLI))
        End If
        If (Not vo.NOMCLI Is Nothing) Then
            vo.NOMCLI = "%" + UCase(vo.NOMCLI) + "%"
            sql += " AND (UPPER(D.NOMCLI) LIKE : NOMCLI)"
            parameterMapper.Parameters.Add(":NOMCLI", ValueDBProperty(vo.NOMCLI))
        End If
        If (Not vo.CODREP Is Nothing) Then
            sql += " AND (A.CODREP = : CODREP)"
            parameterMapper.Parameters.Add(":CODREP", ValueDBProperty(vo.CODREP))
        End If
        If (Not vo.NOMREP Is Nothing) Then
            vo.NOMREP = "%" + UCase(vo.NOMREP) + "%"
            sql += " AND (UPPER(A.NOMREP) LIKE : NOMREP)"
            parameterMapper.Parameters.Add(":NOMREP", ValueDBProperty(vo.NOMREP))
        End If
        If (Not vo.CODSUP Is Nothing) Then
            sql += " AND (A.CODSUP = : CODSUP)"
            parameterMapper.Parameters.Add(":CODSUP", ValueDBProperty(vo.CODSUP))
        End If
        If (Not vo.NOMSUP Is Nothing) Then
            vo.NOMSUP = "%" + UCase(vo.NOMSUP) + "%"
            sql += " AND (UPPER(Y.NOMSUP) LIKE : NOMSUP)"
            parameterMapper.Parameters.Add(":NOMSUP", ValueDBProperty(vo.NOMSUP))
        End If
        'If (Not vo.CODGER Is Nothing) Then
        '    sql += " AND (Z.CODGER = : CODGER)"
        '    parameterMapper.Parameters.Add(":CODGER", ValueDBProperty(vo.CODGER))
        'End If
        'If (Not vo.NOMGER Is Nothing) Then
        '    vo.NOMGER = "%" + UCase(vo.NOMGER) + "%"
        '    sql += " AND (UPPER(Z.NOMGER) LIKE : NOMGER)"
        '    parameterMapper.Parameters.Add(":NOMGER", ValueDBProperty(vo.NOMGER))
        'End If


        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(vo.CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(vo.TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function ListarPublicoAlvoCliente(vo As VO.PromocaoPublicoAlvoCliente) As List(Of VO.PromocaoPublicoAlvoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       SELECT  
                                              RACRC.CODPMC,
                                              RACRC.TIPEDEPUBALVPMC,
                                              RACRC.CODCLI,
                                              C.NOMCLI
                                         FROM MRT.RLCPMCCLI RACRC
                                        INNER JOIN MRT.T0100043 C ON (C.CODCLI = RACRC.CODCLI)
                                        WHERE (CODPMC IS NULL OR CODPMC = :CODPMC)
                                          AND (TIPEDEPUBALVPMC IS NULL OR TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)
                            ]]>.Value

        If (Not vo.NOMCLI Is Nothing) Then
            vo.NOMCLI = "%" + UCase(vo.NOMCLI) + "%"
            sql += " AND (UPPER(C.NOMCLI) LIKE :NOMCLI)"
            parameterMapper.Parameters.Add(":NOMCLI", ValueDBProperty(vo.NOMCLI))
        End If

        If (Not vo.CODCLI Is Nothing) Then
            sql += " AND (RACRC.CODCLI = : CODCLI)"
            parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(vo.CODCLI))
        End If

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(vo.CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(vo.TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Sub Delete(filtro As PromocaoPublicoAlvoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE FROM MRT.RLCPMCCLI  
                                      WHERE CODPMC = :CODPMC 
                                        AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC 
                                        AND (:CODCLI IS NULL OR CODCLI = :CODCLI)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(filtro.CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(filtro.TIPEDEPUBALVPMC))
        parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(filtro.CODCLI))

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub
End Class



<TableAttribute("MRT.RLCPMCCLI")>
Public Class PromocaoPublicoAlvoClienteDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODCLI As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMCLI As String
    <IgnoreMap>
    Property NOMGER As String
    <IgnoreMap>
    Property NOMSUP As String
    <IgnoreMap>
    Property NOMREP As String


    <IgnoreMap>
    Property CODREP As Nullable(Of Decimal)

    <IgnoreMap>
    Property CODGER As Nullable(Of Decimal)

    <IgnoreMap>
    Property CODSUP As Nullable(Of Decimal)
End Class