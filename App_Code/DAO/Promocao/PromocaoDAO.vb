﻿Imports Core

Public Class PromocaoDAO
    Inherits DAOBase(Of VO.Promocao)

    Public Sub New()
        daoConfiguration = New PromocaoDAOConfiguration()
    End Sub

    Public Function DataInicioSemana() As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT SYSDATE - ( NUMDIASMN - 1 ) AS DATINISEMANA
                                       FROM MRT.T0102321
                                      WHERE DATREF = TRUNC(SYSDATE)
                                    ]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"DATINISEMANA"}))
    End Function

    Public Function DataReferenciaEDataFimAgendaDiariaLivroPrecos(promocaoBrinde As VO.Promocao) As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                SELECT TO_CHAR(MAX(DATREFLIVPCO),'YYYY-MM-DD') AS DATREFLIVPCO, 
                                       TO_CHAR(MAX(DATFIMVLDLIVPCO),'YYYY-MM-DD') AS DATFIMVLDLIVPCO 
                                  FROM MRT.T0101520
                                 WHERE DATREFLIVPCO <= TO_DATE(:DATAINISEMANA,'DD/MM/YYYY')
                                 ORDER BY DATREFLIVPCO DESC
                                ]]>.Value

        parameterMapper.Parameters.Add(":DATAINISEMANA", ValueDBProperty(promocaoBrinde.DATINISEMANA.GetValueOrDefault().ToString("dd/MM/yyyy")))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"DATREFLIVPCO", "DATFIMVLDLIVPCO"}))
    End Function


    Public Function CarregaPromocao() As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODPMC
                                          , DESPMC
                                    FROM MRT.T0107935
                                    ]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function ListaParaGrid(promocao As VO.Promocao) As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        If (Not String.IsNullOrWhiteSpace(promocao.NOMPMC)) Then
            promocao.NOMPMC = "%" + promocao.NOMPMC.ToUpper() + "%"
        End If
        Dim sql As String = <![CDATA[SELECT DISTINCT
	                                    A.CODPMC
	                                    , A.TIPEDEPUBALVPMC
	                                    , A.CODFILEMP
	                                    , B.NOMFILEMP
	                                    , A.NOMPMC
	                                    , A.TIPSITPMC 
	                                    , A.DATINIPMC
	                                    , A.DATFIMPMC
	                                    , C.DESUNDESRNGC
                                        , D.INDTIPPTOPMC AS INDTIPPTOPMC1
                                        , E.INDTIPPTOPMC AS INDTIPPTOPMC2
                                        , CASE WHEN F.CODPMC > 0 THEN 1 ELSE 0 END AS INDHSTSITPMC
                                    FROM MRT.T0107935       A
                                    INNER JOIN MRT.T0112963 B ON  A.CODFILEMP = B.CODFILEMP     
                                    INNER JOIN MRT.T0131275 C ON C.CODUNDESRNGC = A.CODUNDESRNGC
                                    LEFT JOIN MRT.T0107951  D ON A.CODPMC = D.CODPMC AND D.INDTIPPTOPMC in ( 1, 2 )
                                    LEFT JOIN MRT.T0107960  E ON A.CODPMC = E.CODPMC AND E.INDTIPPTOPMC in ( 1, 2 )
                                    LEFT JOIN MRT.HSTSITPMC F ON A.CODPMC = F.CODPMC AND F.NUMSEQ = 1
                                    WHERE (:CODFILEMP IS NULL OR B.CODFILEMP = :CODFILEMP)
                                    AND (:NOMPMC IS NULL OR UPPER(A.NOMPMC) LIKE :NOMPMC)
                                    AND (:CODPMC  IS NULL OR A.CODPMC = :CODPMC )
                                    AND (:DATINIPMC IS NULL OR :DATFIMPMC IS NULL OR A.DATINIPMC BETWEEN :DATINIPMC AND :DATFIMPMC)
                                    AND (:CODUNDESRNGC IS NULL OR :CODUNDESRNGC = A.CODUNDESRNGC)
                                    AND (:CODFNCGRCPMC IS NULL OR :CODFNCGRCPMC = A.CODFNCGRCPMC)
                                    AND (:TIPSITPMC IS NULL OR :TIPSITPMC = A.TIPSITPMC)
                                    AND (:CODACOCMC IS NULL OR :CODACOCMC = A.CODACOCMC)
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(promocao.CODFILEMP))
        parameterMapper.Parameters.Add(":NOMPMC", ValueDBProperty(promocao.NOMPMC))
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocao.CODPMC))
        parameterMapper.Parameters.Add(":DATINIPMC", ValueDBProperty(promocao.DATINIPMC))
        parameterMapper.Parameters.Add(":DATFIMPMC", ValueDBProperty(promocao.DATFIMPMC))
        parameterMapper.Parameters.Add(":CODUNDESRNGC", ValueDBProperty(promocao.CODUNDESRNGC))
        parameterMapper.Parameters.Add(":TIPSITPMC", ValueDBProperty(promocao.TIPSITPMC))
        parameterMapper.Parameters.Add(":CODFNCGRCPMC", ValueDBProperty(promocao.CODFNCGRCPMC))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(promocao.CODACOCMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "TIPEDEPUBALVPMC", "CODFILEMP", "NOMFILEMP", "NOMPMC", "TIPSITPMC", "DATINIPMC", "DATFIMPMC", "DESUNDESRNGC", "INDTIPPTOPMC1", "INDTIPPTOPMC2", "INDHSTSITPMC"}))
    End Function

    Public Function ListSelectVerificaQtdMinVndMaiorQtdPmc(promocao As VO.Promocao) As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[
           SELECT A.CODPMC 
                , D.CODMER 
                , D.DESMER 
                , E.QDEMNMVNDMER AS QDEMIN 
                , NVL(B.QDEPTOFXAEQCCALCTR * C.QDEBDEMPLCTRPMC, 0) AS QDEPMC 
             FROM MRT.T0107935 A 
            INNER JOIN MRT.T0107951 B  ON B.CODPMC = A.CODPMC  AND B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC 
            INNER JOIN MRT.T0108044 C  ON C.CODPMC = A.CODPMC 
            INNER JOIN MRT.T0100086 D  ON D.CODMER = C.CODPRMPMC  AND D.CODEMP = 1 
            INNER JOIN MRT.RLCMERFILQDEMNM E  ON  E.CODMER = D.CODMER  AND E.DATDST IS NULL  AND E.CODFILEMP = A.CODFILEMP 
            WHERE (A.CODPMC = :CODPMC )
              AND E.QDEMNMVNDMER > 0
              AND E.QDEMNMVNDMER > NVL(B.QDEPTOFXAEQCCALCTR * C.QDEBDEMPLCTRPMC, 0)
          UNION  
           SELECT A.CODPMC
                , D.CODMER
                , D.DESMER  
                , E.QDEMNMVNDMER AS QDEMIN
                , NVL(X.QDEPTOFXACTRPMC * C.QDEBDEMPLCTRPMC, 0) AS QDEPMC  
             FROM MRT.T0107935 A 
            INNER JOIN MRT.T0107960 X on X.CODPMC = A.CODPMC AND X.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC
            INNER JOIN MRT.T0108044 C  ON C.CODPMC = A.CODPMC
            INNER JOIN MRT.T0100086 D  ON D.CODMER = C.CODPRMPMC  AND D.CODEMP = 1 
            INNER JOIN MRT.RLCMERFILQDEMNM E ON  E.CODMER = D.CODMER AND E.DATDST IS NULL AND E.CODFILEMP = A.CODFILEMP 
            WHERE A.CODPMC = :CODPMC 
              AND E.QDEMNMVNDMER > 0
              AND E.QDEMNMVNDMER > NVL(X.QDEPTOFXACTRPMC * C.QDEBDEMPLCTRPMC, 0)
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocao.CODPMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "CODMER", "DESMER", "QDEMIN", "QDEPMC"}))
    End Function

    'Consulta para inserir brinde
    Public Function ListSelectVerificaQtdMinVndMenorQtdPmc(promocao As VO.Promocao) As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[
              SELECT DISTINCT A.CODPMC, 
                    D.CODMER,  
                    D.DESMER,  
                    E.QDEMNMVNDMER AS QDEMIN,  
                    B.QDEPTOFXAEQCCALCTR AS QDEPMC  
               FROM MRT.T0107935 A,  
                    MRT.T0107951 B,  
                    MRT.T0100086 D  
         INNER JOIN MRT.RLCMERFILQDEMNM E ON E.CODMER = D.CODMER  
                                              AND E.DATDST IS NULL  
                                              AND E.CODFILEMP = :CODFILEMP
              WHERE A.CODPMC = :CODPMC
                AND B.CODPMC = A.CODPMC  
                AND B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC   
                AND D.CODMER = :CODMER
                AND D.CODEMP = 1  
                AND E.QDEMNMVNDMER > 0
                AND E.QDEMNMVNDMER > B.QDEPTOFXAEQCCALCTR   
             UNION  
             SELECT DISTINCT  A.CODPMC,  
                    D.CODMER,  
                    D.DESMER,  
                    E.QDEMNMVNDMER AS QDEMIN,  
                    X.QDEPTOFXACTRPMC AS QDEPMC  
               FROM MRT.T0107935 A,  
                    MRT.T0107960 X,  
                    MRT.T0100086 D  
         INNER JOIN MRT.RLCMERFILQDEMNM E ON  E.CODMER = D.CODMER  
                                              AND E.DATDST IS NULL  
                                              AND E.CODFILEMP = :CODFILEMP
              WHERE A.CODPMC = :CODPMC
                AND X.CODPMC = A.CODPMC  
                AND X.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC   
                AND D.CODMER = :CODMER
                AND D.CODEMP = 1  
                AND E.QDEMNMVNDMER > 0
                AND E.QDEMNMVNDMER > X.QDEPTOFXACTRPMC
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocao.CODPMC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(promocao.CODMER))
        parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(promocao.CODFILEMP))
        'Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "CODMER", "DESMER", "QDEMIN", "QDEPMC"}))
        Return connector.ExecuteReader(Of VO.Promocao)(sql, parameterMapper)
    End Function

    Public Function ListSelectVerificaQtdPmcNaoMult(promocao As VO.Promocao) As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[
                   SELECT A.CODPMC 
                        , D.CODMER  
                        , D.DESMER 
                        , E.QDEMNMVNDMER AS QDEMIN 
                        , B.QDEPTOFXAEQCCALCTR AS QDE_PROMOCAO 
                     FROM MRT.T0107935 A 
                    INNER JOIN MRT.T0107951 B  ON B.CODPMC = A.CODPMC  AND B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC 
                    INNER JOIN MRT.T0108044 C  ON C.CODPMC = A.CODPMC 
                    INNER JOIN MRT.T0100086 D  ON D.CODMER = C.CODPRMPMC  AND D.CODEMP = 1 
                    INNER JOIN MRT.RLCMERFILQDEMNM E  ON  E.CODMER = D.CODMER  AND E.DATDST IS NULL  AND E.CODFILEMP = A.CODFILEMP 
                    WHERE (A.CODPMC = :CODPMC )
                      AND E.QDEMNMVNDMER > 0
                      AND E.QDEMNMVNDMER <= B.QDEPTOFXAEQCCALCTR  
                      AND MOD(B.QDEPTOFXAEQCCALCTR, E.QDEMNMVNDMER) > 0   
                  UNION  
                   SELECT A.CODPMC
                        , D.CODMER
                        , D.DESMER  
                        , E.QDEMNMVNDMER AS QDEMIN
                        , NVL(X.QDEPTOFXACTRPMC * C.QDEBDEMPLCTRPMC, 0) AS QDEPMC  
                     FROM MRT.T0107935 A 
                    INNER JOIN MRT.T0107960 X on X.CODPMC = A.CODPMC AND X.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC
                    INNER JOIN MRT.T0108044 C  ON C.CODPMC = A.CODPMC
                    INNER JOIN    MRT.T0100086 D  ON D.CODMER = C.CODPRMPMC  AND D.CODEMP = 1 
                    INNER JOIN MRT.RLCMERFILQDEMNM E ON  E.CODMER = D.CODMER AND E.DATDST IS NULL AND E.CODFILEMP = A.CODFILEMP 
                    WHERE A.CODPMC = :CODPMC 
                      AND E.QDEMNMVNDMER > 0
                      AND E.QDEMNMVNDMER <= X.QDEPTOFXACTRPMC   
              AND MOD(X.QDEPTOFXACTRPMC, E.QDEMNMVNDMER) > 0
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocao.CODPMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "CODMER", "DESMER", "QDEMIN"}))
    End Function

    'Consulta para inserir brinde
    Public Function ListSelectVerificaQtdPmcNaoMultBrinde(promocao As VO.Promocao) As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[
                          SELECT A.CODPMC, 
                                 D.CODMER, 
                                 D.DESMER, 
                                 E.QDEMNMVNDMER as QDEMIN, 
                                 B.QDEPTOFXAEQCCALCTR AS QDEPMC 
                            FROM MRT.T0107935 A, 
                                 MRT.T0107951 B, 
                                 MRT.T0100086 D 
                      INNER JOIN MRT.RLCMERFILQDEMNM E ON E.CODMER = D.CODMER  
                                                      AND E.DATDST IS NULL  
                                                      AND E.CODFILEMP = :CODFILEMP
                         WHERE A.CODPMC = :CODPMC
                           AND B.CODPMC = A.CODPMC 
                           AND B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC  
                           AND D.CODMER = :CODMER
                           AND D.CODEMP = 1                           
                           AND E.QDEMNMVNDMER > 0
                           AND E.QDEMNMVNDMER <= B.QDEPTOFXAEQCCALCTR  
                           AND MOD(B.QDEPTOFXAEQCCALCTR, E.QDEMNMVNDMER) > 0   
                    UNION   
                        SELECT A.CODPMC, 
                               D.CODMER, 
                               D.DESMER, 
                               E.QDEMNMVNDMER as QDEMIN, 
                               X.QDEPTOFXACTRPMC AS QDEPMC 
                          FROM MRT.T0107935 A, 
                               MRT.T0107960 X, 
                               MRT.T0100086 D 
                    INNER JOIN MRT.RLCMERFILQDEMNM E ON E.CODMER = D.CODMER  
                                                    AND E.DATDST IS NULL  
                                                    AND E.CODFILEMP = :CODFILEMP
                         WHERE A.CODPMC = :CODPMC
                           AND X.CODPMC = A.CODPMC 
                           AND X.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC  
                           AND D.CODMER = :CODMER
                           AND D.CODEMP = 1 
                           AND E.QDEMNMVNDMER > 0
                           AND E.QDEMNMVNDMER <= X.QDEPTOFXACTRPMC   
                           AND MOD(X.QDEPTOFXACTRPMC, E.QDEMNMVNDMER) > 0
                                        ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocao.CODPMC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(promocao.CODMER))
        parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(promocao.CODFILEMP))
        Return connector.ExecuteReader(Of VO.Promocao)(sql, parameterMapper)
    End Function

    'Public Function ListSelectVerificaQtdMinVndMenorQtdPmcCriterio(promocao As VO.Promocao) As List(Of VO.Promocao)

    '    Dim parameterMapper As ParameterMapper = New ParameterMapper()
    '    Dim sql As String = <![CDATA[SELECT DISTINCT  A.CODPMC, 
    '                                        D.CODMER, 
    '                                        D.DESMER, 
    '                                        NVL(E.QDEMNMVNDMER, D.QDEMNMVNDMER) AS QDEMIN, 
    '                                        B.QDEBDEMPLCTRPMC AS QDEPMC 
    '                                   FROM MRT.T0107935 A, 
    '                                        MRT.T0108044 B, 
    '                                        MRT.T0100086 D 
    '                                   LEFT JOIN MRT.RLCMERFILQDEMNM E 
    '                                        ON  E.CODMER = D.CODMER 
    '                                        AND E.DATDST IS NULL 
    '                                        AND E.CODFILEMP = :CODFILEMP
    '                                  WHERE A.CODPMC = :CODPMC
    '                                    AND B.CODPMC = A.CODPMC 
    '                                    AND B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC  
    '                                    AND D.CODEMP = 1 
    '                                    AND D.QDEMNMVNDMER > B.QDEBDEMPLCTRPMC
    '                                    AND D.CODMER = B.CODPRMPMC
    '                                ]]>.Value
    '    parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocao.CODPMC))
    '    parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(promocao.CODFILEMP))
    '    Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "CODMER", "DESMER", "QDEMIN", "QDEPMC"}))
    'End Function

    'Public Function ListSelectVerificaQtdPmcNaoMultiploCriterio(promocao As VO.Promocao) As List(Of VO.Promocao)

    '    Dim parameterMapper As ParameterMapper = New ParameterMapper()
    '    Dim sql As String = <![CDATA[SELECT A.CODPMC, 
    '                                        D.CODMER, 
    '                                        D.DESMER, 
    '                                        d.QDEMNMVNDMER as QDEMIN, 
    '                                        b.qdebdemplctrpmc AS QDEPMC 
    '                                   FROM MRT.T0107935 A, 
    '                                        MRT.T0108044 B, 
    '                                        MRT.T0100086 D 
    '                                  WHERE A.CODPMC = :CODPMC
    '                                        AND B.CODPMC = A.CODPMC 
    '                                        AND B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC  
    '                                        AND D.CODMER = B.Codprmpmc
    '                                        AND D.CODEMP = 1 
    '                                        AND D.INDQDEMPLVNDMER = 1 
    '                                        AND D.QDEMNMVNDMER <= B.qdebdemplctrpmc  
    '                                        AND MOD(B.qdebdemplctrpmc, D.QDEMNMVNDMER) > 0
    '                                ]]>.Value
    '    parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(promocao.CODPMC))
    '    Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "CODMER", "DESMER", "QDEMIN", "QDEPMC"}))
    'End Function


    '

    ''' <summary>
    ''' Consulta para liberar promoção (MIX)
    ''' retorna os itens do mix com quantidade de venda menor que a quantidade minima
    ''' </summary>
    ''' <param name="CODPMC"></param>
    ''' <returns></returns>
    Public Function ListaMIXQtdMinVndMaiorQtdPmc(CODPMC As Decimal) As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[
                              SELECT promocao.CODPMC 
                , Mercadoria.CODMER 
                , Mercadoria.DESMER 
                , E.QDEMNMVNDMER AS QDEMIN 
                , NVL(CriApuEscalaProgressiva.TAMIVLFXAEQCCALCTR  , 0) AS QDEPMC 
             FROM MRT.T0107935 promocao 
            INNER JOIN MRT.T0107951 CriApuEscalaProgressiva  ON CriApuEscalaProgressiva.CODPMC = promocao.CODPMC  
                                                            AND CriApuEscalaProgressiva.TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC 
            INNER JOIN MRT.T0107994 PromocaoMixVisualizarVigente  ON PromocaoMixVisualizarVigente.CODPMC = promocao.CODPMC 
                                                                  AND PromocaoMixVisualizarVigente.TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC 
                                                                  AND  (CASE WHEN PromocaoMixVisualizarVigente.CODGRPMIXMERPMC = 0 THEN 1
                                                                          ELSE PromocaoMixVisualizarVigente.CODGRPMIXMERPMC
                                                                          END) = CriApuEscalaProgressiva.CODGRPMIXMERPMC 
            INNER JOIN MRT.T0100086 Mercadoria  ON Mercadoria.CODMER = PromocaoMixVisualizarVigente.CODMER  
                                                AND Mercadoria.CODEMP = 1 
            INNER JOIN MRT.RLCMERFILQDEMNM E  ON  E.CODMER = Mercadoria.CODMER  
                                              AND E.DATDST IS NULL  
                                              AND E.CODFILEMP = promocao.CODFILEMP 
            WHERE (promocao.CODPMC = :CODPMC )
              AND E.QDEMNMVNDMER > 0
              AND E.QDEMNMVNDMER > NVL(CriApuEscalaProgressiva.TAMIVLFXAEQCCALCTR  , 0)
              AND CriApuEscalaProgressiva.CODCTRPMC = 2
          UNION  
           SELECT promocao.CODPMC
                , Mercadoria.CODMER
                , Mercadoria.DESMER  
                , E.QDEMNMVNDMER AS QDEMIN
                , NVL(PromoCriApuracaoFaixaFinita.VLRLIMMNMFXACTRPMC , 0) AS QDEPMC  
             FROM MRT.T0107935 promocao
            INNER JOIN MRT.T0107960 PromoCriApuracaoFaixaFinita ON PromoCriApuracaoFaixaFinita.CODPMC = promocao.CODPMC 
                                                                AND PromoCriApuracaoFaixaFinita.TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC
            INNER JOIN MRT.T0107994 PromocaoMixVisualizarVigente  ON PromocaoMixVisualizarVigente.CODPMC = promocao.CODPMC
                                                                  AND PromocaoMixVisualizarVigente.TIPEDEPUBALVPMC = promocao.TIPEDEPUBALVPMC 
                                                                  AND  (CASE WHEN PromocaoMixVisualizarVigente.CODGRPMIXMERPMC = 0 THEN 1
                                                                          ELSE PromocaoMixVisualizarVigente.CODGRPMIXMERPMC
                                                                          END) = PromoCriApuracaoFaixaFinita.CODGRPMIXMERPMC 
                                                                 -- AND PromocaoMixVisualizarVigente.CODGRPMIXMERPMC = PromoCriApuracaoFaixaFinita.CODGRPMIXMERPMC
            INNER JOIN MRT.T0100086 Mercadoria  ON Mercadoria.CODMER = PromocaoMixVisualizarVigente.CODMER  
                                                AND Mercadoria.CODEMP = 1 
            INNER JOIN MRT.RLCMERFILQDEMNM E ON  E.CODMER = Mercadoria.CODMER 
                                             AND E.DATDST IS NULL 
                                             AND E.CODFILEMP = promocao.CODFILEMP 
            WHERE promocao.CODPMC = :CODPMC
              AND E.QDEMNMVNDMER > 0
              AND E.QDEMNMVNDMER > NVL(PromoCriApuracaoFaixaFinita.VLRLIMMNMFXACTRPMC , 0)
              AND PromoCriApuracaoFaixaFinita.CODCTRPMC = 2                  
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        Return connector.ExecuteReader(Of VO.Promocao)(sql, parameterMapper).ToList()
    End Function

    ''' <summary>
    ''' Consulta para liberar promoção (MIX)
    ''' retorna os itens do mix com quantidade de venda nao multiplo
    ''' </summary>
    ''' <param name="CODPMC"></param>
    ''' <returns></returns>
    Public Function ListaMIXQtdPmcNaoMult(CODPMC As Decimal) As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[
               SELECT A.CODPMC 
                        , D.CODMER  
                        , D.DESMER 
                        , E.QDEMNMVNDMER AS QDEMIN 
                        , B.TAMIVLFXAEQCCALCTR  AS QDE_PROMOCAO 
                     FROM MRT.T0107935 A 
                    INNER JOIN MRT.T0107951 B  ON B.CODPMC = A.CODPMC  AND B.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC 
                    INNER JOIN MRT.T0107994 C  ON C.CODPMC = A.CODPMC 
                                              AND C.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC 
                                              AND (CASE WHEN C.CODGRPMIXMERPMC = 0 THEN 1
                                                  ELSE C.CODGRPMIXMERPMC
                                              END) = B.CODGRPMIXMERPMC
                    INNER JOIN MRT.T0100086 D  ON D.CODMER = C.CODMER  AND D.CODEMP = 1 
                    INNER JOIN MRT.RLCMERFILQDEMNM E  ON  E.CODMER = D.CODMER  AND E.DATDST IS NULL  AND E.CODFILEMP = A.CODFILEMP 
                    WHERE (A.CODPMC = :CODPMC )
                      AND E.QDEMNMVNDMER > 0
                      AND E.QDEMNMVNDMER <= B.TAMIVLFXAEQCCALCTR   
                      AND MOD(B.TAMIVLFXAEQCCALCTR , E.QDEMNMVNDMER) > 0   
                      AND B.CODCTRPMC = 2
                  UNION  
                   SELECT A.CODPMC
                        , D.CODMER
                        , D.DESMER  
                        , E.QDEMNMVNDMER AS QDEMIN
                        , NVL(X.VLRLIMMNMFXACTRPMC , 0) AS QDEPMC  
                     FROM MRT.T0107935 A 
                    INNER JOIN MRT.T0107960 X on X.CODPMC = A.CODPMC AND X.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC
                    INNER JOIN MRT.T0107994 C  ON C.CODPMC = A.CODPMC
                                              AND C.TIPEDEPUBALVPMC = A.TIPEDEPUBALVPMC 
                                              AND (CASE WHEN C.CODGRPMIXMERPMC = 0 THEN 1
                                                  ELSE C.CODGRPMIXMERPMC
                                              END) = X.CODGRPMIXMERPMC
                    INNER JOIN MRT.T0100086 D  ON D.CODMER = C.CODMER  AND D.CODEMP = 1 
                    INNER JOIN MRT.RLCMERFILQDEMNM E ON  E.CODMER = D.CODMER AND E.DATDST IS NULL AND E.CODFILEMP = A.CODFILEMP 
                    WHERE A.CODPMC = :CODPMC 
                      AND E.QDEMNMVNDMER > 0
                      AND E.QDEMNMVNDMER <= X.VLRLIMMNMFXACTRPMC    
                      AND MOD(X.VLRLIMMNMFXACTRPMC , E.QDEMNMVNDMER) > 0
                      AND X.CODCTRPMC = 2
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        Return connector.ExecuteReader(Of VO.Promocao)(sql, parameterMapper).ToList()
    End Function


    Public Function GetCurrentCODPMC(TIPEDEPUBALVPMC As Decimal) As Decimal

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT max(CODPMC) as CODPMC
                                       FROM MRT.T0107935
                                      WHERE TIPEDEPUBALVPMC= :TIPEDEPUBALVPMC
                            ]]>.Value
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Dim CODPMC As Decimal = connector.ExecuteReader(Of VO.Promocao)(sql, parameterMapper).FirstOrDefault().CODPMC

        Return CODPMC
    End Function
    'conforme regra do sistema antigo
    'Public Function GetCurrentCODPMC(TIPEDEPUBALVPMC As Decimal) As Decimal
    '
    '    Dim parameterMapper As ParameterMapper = New ParameterMapper()
    '
    '    Dim sql As String = <![CDATA[SELECT CODPMC
    '                                   FROM MRT.T0140126
    '                                  WHERE TIPEDEPUBALVPMC= :TIPEDEPUBALVPMC
    '                        ]]>.Value
    '    parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))
    '
    '    Dim CODPMC As Decimal = connector.ExecuteReader(Of VO.Promocao)(sql, parameterMapper).FirstOrDefault().CODPMC
    '
    '    Return CODPMC
    'End Function


    Public Sub UpdateCurrentCODPMC(TIPEDEPUBALVPMC As Decimal, CODPMC As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                               UPDATE MRT.T0140126
                                  SET CODPMC = :CODPMC
                                WHERE TIPEDEPUBALVPMC= :TIPEDEPUBALVPMC
                            ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        connector.ExecuteScalar(sql, parameterMapper)

    End Sub




    Public Function ListarPromocaoPorAcaoMecanica(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[SELECT 
                                            A.CODPMC
                                          , A.TIPEDEPUBALVPMC
                                          , A.CODFILEMP
                                          , B.NOMFILEMP
                                          , A.NOMPMC
                                          , A.TIPSITPMC 
                                          , A.DATINIPMC
                                          , A.DATFIMPMC
                                    FROM MRT.T0107935 A
                                    INNER JOIN MRT.T0112963 B ON  A.CODFILEMP = B.CODFILEMP     
                                    INNER JOIN MRT.RLCACOCMCRGRFIL R ON  R.CODPMC = A.CODPMC
                                    WHERE R.CODACOCMC = :CODACOCMC
                                    AND R.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "TIPEDEPUBALVPMC", "CODFILEMP", "NOMFILEMP", "NOMPMC", "TIPSITPMC", "DATINIPMC", "DATFIMPMC"}))
    End Function

    Public Function ListarPromocaoParaParametrizacao(ByVal CODPMC As Decimal) As List(Of VO.Promocao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[SELECT A.CODMER
                                        , C.QDEMNMVNDMER
                                        , C.INDQDEMPLVNDMER
                                    FROM MRT.RLCPMCRGRMIX A
                                    INNER JOIN MRT.T0107935 B ON B.CODPMC = A.CODPMC
                                    LEFT JOIN MRT.RLCMERFILQDEMNM C ON C.CODMER = A.CODMER AND C.CODFILEMP = B.CODFILEMP
                                    WHERE A.CODPMC = :CODPMC
                                    AND (C.QDEMNMVNDMER > 0 OR C.INDQDEMPLVNDMER = 1)]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "QDEMNMVNDMER", "INDQDEMPLVNDMER"}))
    End Function

    Public Function VerificaSeCashBack(ByVal CODPMC As Decimal) As VO.Promocao

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[SELECT
	                                    A.CODPMC
                                        , D.INDTIPPTOPMC AS INDTIPPTOPMC1
                                        , E.INDTIPPTOPMC AS INDTIPPTOPMC2
                                    FROM MRT.T0107935       A
                                    LEFT JOIN MRT.T0107951  D ON A.CODPMC = D.CODPMC AND D.INDTIPPTOPMC in ( 1, 2 )
                                    LEFT JOIN MRT.T0107960  E ON A.CODPMC = E.CODPMC AND E.INDTIPPTOPMC in ( 1, 2 )
                                    WHERE A.CODPMC = : CODPMC]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "INDTIPPTOPMC1", "INDTIPPTOPMC2"})).FirstOrDefault()
    End Function

End Class



<TableAttribute("MRT.T0107935")>
Public Class PromocaoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)

    <IgnoreMap>
    Property NOMFILEMP As String

    <IgnoreMap>
    Property CODMER As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESMER As String
    <IgnoreMap>
    Property QDEMIN As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEPMC As Nullable(Of Decimal)

    <IgnoreMap>
    Property DATINISEMANA As Nullable(Of Date)
    <IgnoreMap>
    Property DATREFLIVPCO As Nullable(Of Date)
    <IgnoreMap>
    Property DATFIMVLDLIVPCO As Nullable(Of Date)
    <IgnoreMap>
    Property DESUNDESRNGC As String

    <IgnoreMap>
    Property CODACOCMCCPI As Nullable(Of Decimal)

    <IgnoreMap>
    Property INDTIPPTOPMC1 As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDTIPPTOPMC2 As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDHSTSITPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEMNMVNDMER As Nullable(Of Decimal)
    <IgnoreMap>
    Property INDQDEMPLVNDMER As Nullable(Of Decimal)

End Class


