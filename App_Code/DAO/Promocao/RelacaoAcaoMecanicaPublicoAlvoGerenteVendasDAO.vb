﻿Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoGerenteVendasDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaPublicoAlvoGerenteVendasDAOConfiguration()
    End Sub

    Public Function ListarGerenteVendas(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                      SELECT B.CODGER,
                                             B.NOMGER,
                                             A.CODACOCMC,
                                             A.NUMSEQRGRACOCMC
                                        FROM MRT.T0100051 B
                                        LEFT JOIN MRT.RLCACOCMCRGRGER A  ON (A.CODGER = B.CODGER) AND A.CODACOCMC = :CODACOCMC 
                                                                                                  AND A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                                       ORDER BY B.CODGER
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGER", "NOMGER", "CODACOCMC", "NUMSEQRGRACOCMC"}))
    End Function

    Public Function ListarGerenteVendasSelecionados(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODACOCMC,
                                               NUMSEQRGRACOCMC,
                                               CODGER 
                                        FROM MRT.RLCACOCMCRGRGER                                        
                                        WHERE (:CODACOCMC IS NULL OR :CODACOCMC = CODACOCMC)
                                        AND (:NUMSEQRGRACOCMC IS NULL OR :NUMSEQRGRACOCMC = NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "CODGER"}))
    End Function

End Class


<TableAttribute("MRT.RLCACOCMCRGRGER")>
Public Class RelacaoAcaoMecanicaPublicoAlvoGerenteVendasDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODGER As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMGER As String
End Class