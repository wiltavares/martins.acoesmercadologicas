﻿Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteDAOConfiguration()
    End Sub

    Public Function ListaTipoSegmentoCliente(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT B.CODSGMNGCCLI,
                                               B.DESSGMNGCCLI,
                                               A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC

                                        FROM
                                        MRT.CADSGMNGCCLI B
                                        LEFT JOIN MRT.RLCACOCMCRGRSGM A  ON (A.CODSGMNGCCLI = B.CODSGMNGCCLI) AND A.CODACOCMC = :CODACOCMC 
                                                                                                              AND A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                                                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODSGMNGCCLI", "DESSGMNGCCLI", "CODACOCMC", "NUMSEQRGRACOCMC"}))
    End Function

    Public Function ListaTipoSegmentoClienteSelecionado(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODACOCMC,
                                               NUMSEQRGRACOCMC,
                                               CODSGMNGCCLI 
                                        FROM MRT.RLCACOCMCRGRSGM                                        
                                        WHERE (:CODACOCMC IS NULL OR :CODACOCMC = CODACOCMC)
                                        AND (:NUMSEQRGRACOCMC IS NULL OR :NUMSEQRGRACOCMC = NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "CODSGMNGCCLI"}))
    End Function

End Class


<TableAttribute("MRT.RLCACOCMCRGRSGM")>
Public Class RelacaoAcaoMecanicaPublicoAlvoSegmentoClienteDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODSGMNGCCLI As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESSGMNGCCLI As String
End Class