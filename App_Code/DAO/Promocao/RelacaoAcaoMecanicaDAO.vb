﻿Imports Core

Public Class RelacaoAcaoMecanicaDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanica)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaDAOConfiguration()
    End Sub

    Public Function SelectEditMecanicaGerais(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As VO.RelacaoAcaoMecanica

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC
                                          , A.NUMSEQRGRACOCMC
                                          , B.NOMACOCMC
                                          , B.CODSTAAPVACOCMC
                                          , COALESCE(C.NOMFNC, D.NOMANSTCR) AS NOMFNC
                                          , B.TIPACOMCD
                                          , B.DATGRCACOCMC
                                          , B.DATINIACOCMC
                                          , B.DATFIMACOCMC
                                          , B.VLRTOTACOCMC
                                          , A.TIPPMC
                                          , A.QDEFXAPMC
                                          , A.QDEGRPMIXPMC
                                          , A.INDPTCTOTMIXPMC
                                          , A.INDPTCTOTMIXPMC
                                          , A.DESRGRACOCMC
                                       FROM MRT.RLCACOCMCRGR A
                                       INNER JOIN MRT.T0118430 B ON A.CODACOCMC = B.CODACOCMC
                                        LEFT JOIN MRT.T0100361 C ON B.CODFNCCRIACOCMC = C.CODFNC                           
                                        LEFT JOIN MRT.CADANSINFTCR D ON B.CODFNCCRIACOCMC = D.CODANSTCR
                                     WHERE (A.CODACOCMC = :CODACOCMC)
                                       AND (A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "NOMACOCMC", "CODSTAAPVACOCMC", "NOMFNC", "TIPACOMCD", "DATGRCACOCMC", "DATINIACOCMC", "DATFIMACOCMC", "VLRTOTACOCMC", "TIPPMC", "QDEFXAPMC", "QDEGRPMIXPMC", "INDPTCTOTMIXPMC", "INDPTCTOTMIXPMC", "DESRGRACOCMC"})).FirstOrDefault()
    End Function

    Public Function SelectMax(ByVal CODACOCMC As Decimal) As VO.RelacaoAcaoMecanica

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT NVL(MAX( NVL(NUMSEQRGRACOCMC, 0)) + 1, 1) AS NUMSEQRGRACOCMC
                                        FROM
                                        MRT.RLCACOCMCRGR A 
                                        WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))



        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"NUMSEQRGRACOCMC"})).FirstOrDefault()
    End Function


    Public Overloads Sub DeleteRelacaoAcaoMecanica(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGR WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                                        ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaFilial(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGRFIL WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                                       ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaCriterio(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGRCTR WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)                                      
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaEstado(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[                                       
                                       DELETE FROM MRT.RLCACOCMCRGRESTUNI WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)                                                                                                                  
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaAtividade(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGRATI WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)                                       
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaGrupementoCliente(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[                                       
                                       DELETE FROM MRT.RLCACOCMCRGRGRPCLI WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)                                                                            
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaNegocioCliente(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGRNGC WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)                                                                           
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaSeguimento(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGRSGM WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)                                                                            
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaCliente(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGRCLI WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)                                                                            
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaRcaVendedor(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGRREP WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)                                                                             
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaGerenteMercado(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGRSUP WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)                                                                           
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaGerenteVendas(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGRGER WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaMix(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGRMIX WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) and (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)                                                                             
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Overloads Sub DeleteRelacaoAcaoMecanicaCriterioDeApuracao(ByVal vo As VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       DELETE FROM MRT.RLCACOCMCRGRCTR WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC) 
                                                                         AND (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        connector.ExecuteScalar(sql, parameterMapper)

    End Sub

    Public Function EditRelacaoAcaoMecanicaCriterioDeApuracao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As VO.RelacaoAcaoMecanica

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC,
                                               A.NUMSEQCTRACOCMC,
                                               A.CODCTRPMC,
                                               A.VLRLIMMNMFXACTRPMC,
                                               A.QDEPTOFXACTRPMC,
                                               A.CODGRPMIXMERPMC,
                                               A.TIPEQCCALCTRPMC,
                                               A.TIPEDENIVPRMPMC,
                                               A.VLRLIMMAXFXACTRPMC,
                                               A.DESCTRMIXPMC,
                                               A.TAMIVLFXAEQCCALCTR,
                                               B.TIPPMC
                                        FROM MRT.RLCACOCMCRGRCTR A
                                        LEFT JOIN MRT.RLCACOCMCRGR B ON (A.CODACOCMC = B.CODACOCMC AND A.NUMSEQRGRACOCMC = B.NUMSEQRGRACOCMC)
                                        WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                        AND (A.NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                                        
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "NUMSEQCTRACOCMC", "CODCTRPMC", "VLRLIMMNMFXACTRPMC", "QDEPTOFXACTRPMC", "CODGRPMIXMERPMC", "TIPEQCCALCTRPMC", "TIPPMC", "TIPEDENIVPRMPMC", "VLRLIMMAXFXACTRPMC", "DESCTRMIXPMC", "TAMIVLFXAEQCCALCTR"})).FirstOrDefault()
    End Function

    Public Function EditRelacaoAcaoMecanicaCriterioDeApuracaoList(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC,
                                               A.NUMSEQCTRACOCMC,
                                               A.CODCTRPMC,
                                               A.VLRLIMMNMFXACTRPMC,
                                               A.QDEPTOFXACTRPMC,
                                               A.CODGRPMIXMERPMC,
                                               A.TIPEQCCALCTRPMC,
                                               A.TIPEDENIVPRMPMC,
                                               A.VLRLIMMAXFXACTRPMC,
                                               A.DESCTRMIXPMC,
                                               A.TAMIVLFXAEQCCALCTR,
                                               B.TIPPMC
                                        FROM MRT.RLCACOCMCRGRCTR A
                                        LEFT JOIN MRT.RLCACOCMCRGR B ON (A.CODACOCMC = B.CODACOCMC AND A.NUMSEQRGRACOCMC = B.NUMSEQRGRACOCMC)
                                        WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                        AND (A.NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                                        
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "NUMSEQCTRACOCMC", "CODCTRPMC", "VLRLIMMNMFXACTRPMC", "QDEPTOFXACTRPMC", "CODGRPMIXMERPMC", "TIPEQCCALCTRPMC", "TIPPMC", "TIPEDENIVPRMPMC", "VLRLIMMAXFXACTRPMC", "DESCTRMIXPMC", "TAMIVLFXAEQCCALCTR"}))
    End Function


    Public Function ListPromocoesPendenteGeracao(ByVal CODACOCMC As Decimal?, ByVal NOMACOCMC As String, ByVal DATINIACOCMC As DateTime?, ByVal DATFIMACOCMC As DateTime?, ByVal CODFNC As Decimal?) As List(Of VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim nomeAcao As String = ""

        If (Not String.IsNullOrWhiteSpace(NOMACOCMC)) Then
            nomeAcao = "%" + NOMACOCMC + "%"
        End If

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC
                                          , A.NUMSEQRGRACOCMC
                                          , B.NOMACOCMC
                                          , B.CODSTAAPVACOCMC
                                          , COALESCE(C.NOMFNC, D.NOMANSTCR) AS NOMFNC
                                          , B.TIPACOMCD
                                          , B.DATGRCACOCMC
                                          , B.DATINIACOCMC
                                          , B.DATFIMACOCMC
                                          , B.VLRTOTACOCMC
                                          , A.TIPPMC
                                          , A.QDEFXAPMC
                                          , A.QDEGRPMIXPMC
                                          , A.INDPTCTOTMIXPMC
                                          , A.INDPTCTOTMIXPMC
                                          , A.DESRGRACOCMC
                                          , B.CODACOCMCCPI
                                       FROM MRT.RLCACOCMCRGR A
                                       INNER JOIN MRT.T0118430 B ON A.CODACOCMC = B.CODACOCMC
                                        LEFT JOIN MRT.T0100361 C ON B.CODFNCCRIACOCMC = C.CODFNC                           
                                        LEFT JOIN MRT.CADANSINFTCR D ON B.CODFNCCRIACOCMC = D.CODANSTCR
                                      WHERE  B.CODSTAAPVACOCMC  = 5
                                      AND EXISTS (
                                         SELECT X.CODACOCMC 
                                           FROM MRT.RLCACOCMCRGRFIL X 
                                          WHERE X.CODACOCMC = A.CODACOCMC 
                                            AND X.NUMSEQRGRACOCMC = A.NUMSEQRGRACOCMC
                                            AND X.CODPMC IS NULL
                                            AND x.DATECSUSR IS NULL
                                      )
                                      AND (:NOMACOCMC IS NULL OR B.NOMACOCMC LIKE :NOMACOCMC)
                                      AND (:CODACOCMC  IS NULL OR A.CODACOCMC = :CODACOCMC )
                                      AND (:DATINIACOCMC IS NULL OR :DATFIMACOCMC IS NULL OR B.DATINIACOCMC BETWEEN :DATINIACOCMC AND :DATFIMACOCMC)
                                      AND (:CODFNC IS NULL OR B.CODFNCCRIACOCMC = :CODFNC )
                                      ORDER BY A.CODACOCMC DESC , A.NUMSEQRGRACOCMC ASC
                            ]]>.Value

        parameterMapper.Parameters.Add(":NOMACOCMC", ValueDBProperty(nomeAcao))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":DATINIACOCMC", ValueDBProperty(DATINIACOCMC))
        parameterMapper.Parameters.Add(":DATFIMACOCMC", ValueDBProperty(DATFIMACOCMC))
        parameterMapper.Parameters.Add(":CODFNC", ValueDBProperty(CODFNC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "NOMACOCMC", "CODSTAAPVACOCMC", "NOMFNC", "TIPACOMCD", "DATGRCACOCMC", "DATINIACOCMC", "DATFIMACOCMC", "VLRTOTACOCMC", "TIPPMC", "QDEFXAPMC", "QDEGRPMIXPMC", "INDPTCTOTMIXPMC", "INDPTCTOTMIXPMC", "DESRGRACOCMC", "CODACOCMCCPI"}))
    End Function

    Public Function ListPromocoesPendenteGeracao() As List(Of VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC
                                          , A.NUMSEQRGRACOCMC
                                          , B.NOMACOCMC
                                          , B.CODSTAAPVACOCMC
                                          , COALESCE(C.NOMFNC, D.NOMANSTCR) AS NOMFNC
                                          , B.TIPACOMCD
                                          , B.DATGRCACOCMC
                                          , B.DATINIACOCMC
                                          , B.DATFIMACOCMC
                                          , B.VLRTOTACOCMC
                                          , A.TIPPMC
                                          , A.QDEFXAPMC
                                          , A.QDEGRPMIXPMC
                                          , A.INDPTCTOTMIXPMC
                                          , A.INDPTCTOTMIXPMC
                                          , A.DESRGRACOCMC
                                       FROM MRT.RLCACOCMCRGR A
                                       INNER JOIN MRT.T0118430 B ON A.CODACOCMC = B.CODACOCMC
                                        LEFT JOIN MRT.T0100361 C ON B.CODFNCCRIACOCMC = C.CODFNC                           
                                        LEFT JOIN MRT.CADANSINFTCR D ON B.CODFNCCRIACOCMC = D.CODANSTCR
                                      WHERE  B.CODSTAAPVACOCMC  = 5
                                      AND EXISTS (
                                         SELECT X.CODACOCMC
                                           FROM MRT.RLCACOCMCRGRFIL X 
                                          WHERE X.CODACOCMC = A.CODACOCMC 
                                            AND X.NUMSEQRGRACOCMC = A.NUMSEQRGRACOCMC
                                            AND X.CODPMC IS NULL
                                      )
                                      ORDER BY A.CODACOCMC DESC
                            ]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "NOMACOCMC", "CODSTAAPVACOCMC", "NOMFNC", "TIPACOMCD", "DATGRCACOCMC", "DATINIACOCMC", "DATFIMACOCMC", "VLRTOTACOCMC", "TIPPMC", "QDEFXAPMC", "QDEGRPMIXPMC", "INDPTCTOTMIXPMC", "INDPTCTOTMIXPMC", "DESRGRACOCMC"}))
    End Function


    Public Function PesquisarAcaoMecanica(ByVal filtro As VO.RelacaoAcaoMecanica) As List(Of VO.RelacaoAcaoMecanica)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If (Not String.IsNullOrWhiteSpace(filtro.NOMACOCMC)) Then
            filtro.NOMACOCMC = "%" + filtro.NOMACOCMC + "%"
        End If


        If (Not String.IsNullOrWhiteSpace(filtro.DESRGRACOCMC)) Then
            filtro.DESRGRACOCMC = "%" + filtro.DESRGRACOCMC + "%"
        End If

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC
                                          , A.NUMSEQRGRACOCMC
                                          , B.NOMACOCMC
                                          , A.DESRGRACOCMC
                                          , A.TIPPMC
                                          , A.QDEFXAPMC
                                          , A.QDEGRPMIXPMC
                                       FROM MRT.RLCACOCMCRGR A
                                       INNER JOIN MRT.T0118430 B ON A.CODACOCMC = B.CODACOCMC
                                        LEFT JOIN MRT.T0100361 C ON B.CODFNCCRIACOCMC = C.CODFNC
                                        LEFT JOIN MRT.CADANSINFTCR D ON B.CODFNCCRIACOCMC = D.CODANSTCR
                                     WHERE (:CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                                       AND (:NOMACOCMC IS NULL OR B.NOMACOCMC LIKE :NOMACOCMC)
                                       AND (:DESRGRACOCMC IS NULL OR A.DESRGRACOCMC LIKE :DESRGRACOCMC)
                                       AND (:TIPPMC IS NULL OR A.TIPPMC = :TIPPMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(filtro.CODACOCMC))
        parameterMapper.Parameters.Add(":NOMACOCMC", ValueDBProperty(filtro.NOMACOCMC))
        parameterMapper.Parameters.Add(":DESRGRACOCMC", ValueDBProperty(filtro.DESRGRACOCMC))
        parameterMapper.Parameters.Add(":TIPPMC", ValueDBProperty(filtro.TIPPMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "NOMACOCMC", "DESRGRACOCMC", "TIPPMC", "QDEFXAPMC", "QDEGRPMIXPMC"})).ToList()
    End Function

End Class

<TableAttribute("MRT.RLCACOCMCRGR")>
Public Class RelacaoAcaoMecanicaDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)

    Property DESRGRACOCMC As String

    Property TIPPMC As Nullable(Of Decimal)

    Property QDEGRPMIXPMC As Nullable(Of Decimal)

    Property QDEFXAPMC As Nullable(Of Decimal)

    Property INDPTCTOTMIXPMC As Nullable(Of Decimal)

#Region "Property Extra"
    <IgnoreMap>
    Property RelacaoAcaoMecanicaFilialList As List(Of Object)
    <IgnoreMap>
    Property RelacaoAcaoMecanicaCriterioDeApuracaoList As List(Of VO.RelacaoAcaoMecanicaCriterioDeApuracao)
    <IgnoreMap>
    Property RelacaoAcaoMecanicaRegistrosCombinadaList As List(Of Object)
    <IgnoreMap>
    Property RelacaoAcaoMecanicaRegistrosFaixaList As List(Of Object)
    <IgnoreMap>
    Property VLRLIMMNMFXACTRPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEPTOFXACTRPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property TIPEQCCALCTRPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODCTRPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property NUMSEQCTRACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODGRPMIXMERPMC As Nullable(Of Decimal)




    <IgnoreMap>
    Property NOMACOCMC As String
    <IgnoreMap>
    Property CODSTAAPVACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMFNC As String
    <IgnoreMap>
    Property TIPACOMCD As Nullable(Of Decimal)
    <IgnoreMap>
    Property DATGRCACOCMC As Nullable(Of DateTime)
    <IgnoreMap>
    Property DATINIACOCMC As Nullable(Of DateTime)
    <IgnoreMap>
    Property DATFIMACOCMC As Nullable(Of DateTime)
    <IgnoreMap>
    Property VLRTOTACOCMC As Nullable(Of Decimal)

    'CODIGO ACAO COMERCIAL COPIA (Ação que originou o clone)
    <IgnoreMap>
    Property CODACOCMCCPI As Nullable(Of Decimal)

    <IgnoreMap>
    Property relacaoAcaoMecanicaPublicoAlvoEstadoList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado)
    <IgnoreMap>
    Property relacaoAcaoMecanicaPublicoAlvoAtividadeList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)
    <IgnoreMap>
    Property relacaoAcaoMecanicaPublicoAlvoGrupamentoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)
    <IgnoreMap>
    Property relacaoAcaoMecanicaPublicoAlvoNegocioClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoNegocioCliente)
    <IgnoreMap>
    Property relacaoAcaoMecanicaPublicoAlvoSegmentoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoSegmentoCliente)
    <IgnoreMap>
    Property relacaoAcaoMecanicaPublicoAlvoClienteList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)
    <IgnoreMap>
    Property relacaoAcaoMecanicaPublicoAlvoRcaVendedorList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoRcaVendedor)
    <IgnoreMap>
    Property relacaoAcaoMecanicaPublicoAlvoGerenteVendasList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteVendas)
    <IgnoreMap>
    Property relacaoAcaoMecanicaPublicoAlvoGerenteMercadoList As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)
    <IgnoreMap>
    Property relacaoAcaoMecanicaMixList As List(Of VO.RelacaoAcaoMecanicaMix)
    <IgnoreMap>
    Property relacaoAcaoMecanicaClientesExcludentesList As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes)

#End Region
End Class

