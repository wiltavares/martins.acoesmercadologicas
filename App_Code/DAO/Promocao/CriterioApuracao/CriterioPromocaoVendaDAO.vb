﻿Imports Core

Public Class CriterioPromocaoVendaDAO
    Inherits DAOBase(Of VO.CriterioPromocaoVenda)

    Public Sub New()
        daoConfiguration = New CriterioPromocaoVendaDAOConfiguration()
    End Sub


    Public Function BuscarCriterioPromocaoVenda() As List(Of VO.CriterioPromocaoVenda)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[SELECT A.TIPEDEPUBALVPMC
                                            , A.CODCTRPMC
                                            , A.DESCTRPMC

                                    FROM MRT.T0107978 A
                                    ]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"TIPEDEPUBALVPMC", "CODCTRPMC", "DESCTRPMC"}))
    End Function

End Class

<TableAttribute("MRT.T0107978")>
Public Class CriterioPromocaoVendaDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODCTRPMC As Nullable(Of Decimal)
End Class
