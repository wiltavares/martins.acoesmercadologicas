﻿Imports Core

Public Class RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAO
    Inherits DAOBase(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva)

    Public Sub New()
        daoConfiguration = New RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAOConfiguration()
    End Sub


    Public Function BuscarRelacaoPromocaoCriterioApuracaoEscalaProgressiva(faixaProgressiva As VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva) As List(Of VO.RelacaoPromocaoCriterioApuracaoEscalaProgressiva)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[SELECT A.TIPEDEPUBALVPMC
                                            , A.CODPMC
                                            , A.CODCTRPMC
                                            , A.VLRINIFXAEQCCALCTR
                                            , A.TAMIVLFXAEQCCALCTR
                                            , A.QDEPTOFXAEQCCALCTR
                                            , A.CODGRPMIXMERPMC
                                            , A.DESCTRMIXPMC
                                            , A.INDTIPPTOPMC
                                            , A.VLRLIMMAXCTR
                                    FROM MRT.T0107951 A
                                    WHERE (:CODPMC  IS NULL OR A.CODPMC = :CODPMC )
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(faixaProgressiva.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

End Class

<TableAttribute("MRT.T0107951")>
Public Class RelacaoPromocaoCriterioApuracaoEscalaProgressivaDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
End Class
