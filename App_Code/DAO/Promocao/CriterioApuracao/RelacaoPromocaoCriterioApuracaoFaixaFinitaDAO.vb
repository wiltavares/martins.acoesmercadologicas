﻿Imports Core

Public Class RelacaoPromocaoCriterioApuracaoFaixaFinitaDAO
    Inherits DAOBase(Of VO.RelacaoPromocaoCriterioApuracaoFaixaFinita)

    Public Sub New()
        daoConfiguration = New RelacaoPromocaoCriterioApuracaoFaixaFinitaDAOConfiguration()
    End Sub


    Public Function BuscarRelacaoPromocaoCriterioApuracaoFaixaFinita(faixaFinita As VO.RelacaoPromocaoCriterioApuracaoFaixaFinita) As List(Of VO.RelacaoPromocaoCriterioApuracaoFaixaFinita)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[SELECT A.TIPEDEPUBALVPMC
                                            , A.CODPMC
                                            , A.CODCTRPMC
                                            , A.VLRLIMMNMFXACTRPMC
                                            , A.VLRLIMMAXFXACTRPMC
                                            , A.QDEPTOFXACTRPMC
                                            , A.CODGRPMIXMERPMC
                                            , A.DESCTRMIXPMC
                                            , A.TIPEDENIVPRMPMC
                                            , A.INDTIPPTOPMC 
                                            , A.VLRLIMMAXCTR
                                    FROM MRT.T0107960 A
                                    WHERE (:CODPMC  IS NULL OR A.CODPMC = :CODPMC )
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(faixaFinita.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

End Class

<TableAttribute("MRT.T0107960")>
Public Class RelacaoPromocaoCriterioApuracaoFaixaFinitaDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
End Class
