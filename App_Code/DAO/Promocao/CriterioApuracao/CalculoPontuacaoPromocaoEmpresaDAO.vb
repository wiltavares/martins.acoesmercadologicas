﻿Imports Core

Public Class CalculoPontuacaoPromocaoEmpresaDAO
    Inherits DAOBase(Of VO.CalculoPontuacaoPromocaoEmpresa)

    Public Sub New()
        daoConfiguration = New CalculoPontuacaoPromocaoEmpresaDAOConfiguration()
    End Sub


    Public Function ListCalculoPontuacaoPromocaoEmpresa() As List(Of VO.CalculoPontuacaoPromocaoEmpresa)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                            TIPEQCCALCTRPMC
                                            , DESEQCCALCTRPMC
                                     FROM MRT.T0107943 ]]>.Value
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class

<TableAttribute("MRT.T0107943")>
    Public Class CalculoPontuacaoPromocaoEmpresaDAOConfiguration
        Inherits DAOConfiguration

        <PrimaryKey>
        Property TIPEQCCALCTRPMC As Nullable(Of Decimal)
    End Class
