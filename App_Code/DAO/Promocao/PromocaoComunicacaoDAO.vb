﻿Imports Core
Imports VO

Public Class PromocaoComunicacaoDAO
    Inherits DAOBase(Of VO.PromocaoComunicacao)

    Public Sub New()
        daoConfiguration = New PromocaoComunicacaoDAOConfiguration()
    End Sub
    Public Function GetRelPromocaoComunicacao(codPmc As Decimal) As List(Of PromocaoComunicacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                              SELECT REL.CODPMC, 
                                     TIP.TIPCMNPMC, 
                                     TIP.TIPEDEPUBALVPMC, 
                                     TIP.DESTIPCMNPMC
                                FROM MRT.T0109610 TIP 
                                LEFT JOIN MRT.T0109628 REL
                                  ON TIP.TIPCMNPMC = REL.TIPCMNPMC AND TIP.TIPEDEPUBALVPMC = REL.TIPEDEPUBALVPMC AND REL.CODPMC = :CODPMC
                               WHERE TIP.TIPEDEPUBALVPMC = 4
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(codPmc))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "TIPCMNPMC", "DESTIPCMNPMC"}))

    End Function
    Public Function ObterParametroPromocaoComunicacao(codPmc As Decimal) As List(Of PromocaoComunicacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                               SELECT CODPMC, 
                                      CODSMBDVGPMCLIVPCO, 
                                      TIPDVGPMCLIVPCO
                                 FROM MRT.T0109636
                                WHERE CODPMC = :CODPMC
                                  AND TIPEDEPUBALVPMC = 4
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(codPmc))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "CODSMBDVGPMCLIVPCO", "TIPDVGPMCLIVPCO"}))

    End Function

    Public Function ObterOpcoesPromocaoComunicacao() As List(Of PromocaoComunicacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                              SELECT DESTIPDVGPMCLIVPCO, 
                                     TIPDVGPMCLIVPCO 
                                FROM MRT.T0110103 
                            ]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"DESTIPDVGPMCLIVPCO", "TIPDVGPMCLIVPCO"}))

    End Function

    Public Sub DeletarRelPmc(codPmc As Integer)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = "DELETE FROM MRT.T0109628 WHERE TIPEDEPUBALVPMC = 4 AND CODPMC = :CODPMC"

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(codPmc))

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub

    Public Sub DeletarParamPmc(codPmc As Integer)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = "DELETE FROM MRT.T0109636 WHERE TIPEDEPUBALVPMC = 4 AND CODPMC = :CODPMC "

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(codPmc))

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub

    Public Sub InserirRelPmc(codPmc As Integer, tipCmn As Integer)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = "INSERT INTO MRT.T0109628 (TIPEDEPUBALVPMC, CODPMC, TIPCMNPMC) VALUES (4, :CODPMC, :TIPCMNPMC)"

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(codPmc))
        parameterMapper.Parameters.Add(":TIPCMNPMC", ValueDBProperty(tipCmn))

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub



    Public Sub InserirParamPmc(codPmc As Integer, tipDvg As Integer, simbolo As String)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = "INSERT INTO MRT.T0109636 (TIPEDEPUBALVPMC, CODPMC, TIPDVGPMCLIVPCO, CODSMBDVGPMCLIVPCO) VALUES (4, :CODPMC, :TIPDVGPMCLIVPCO, :CODSMBDVGPMCLIVPCO)"

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(codPmc))
        parameterMapper.Parameters.Add(":TIPDVGPMCLIVPCO", ValueDBProperty(tipDvg))
        parameterMapper.Parameters.Add(":CODSMBDVGPMCLIVPCO", ValueDBProperty(simbolo))

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub


End Class



<TableAttribute("MRT.T0109628")>
Public Class PromocaoComunicacaoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)

    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)

    Property TIPCMNPMC As Nullable(Of Decimal)

    <IgnoreMap>
    Property DESTIPCMNPMC As String
    <IgnoreMap>
    Property CODSMBDVGPMCLIVPCO As String
    <IgnoreMap>
    Property TIPDVGPMCLIVPCO As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESTIPDVGPMCLIVPCO As String
End Class


