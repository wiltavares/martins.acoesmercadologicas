﻿Imports Core
Imports VO

Public Class RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoDAOConfiguration()
    End Sub

    Public Function ListarGerenteMercado(vo As VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not vo.NOMSUP Is Nothing Then
            vo.NOMSUP = "%" + UCase(vo.NOMSUP) + "%"
        End If
        If Not vo.NOMGER Is Nothing Then
            vo.NOMGER = "%" + UCase(vo.NOMGER) + "%"
        End If

        Dim sql As String = <![CDATA[
                                        SELECT
                                                A.CODSUP, 
                                                A.NOMSUP,
                                                A.CODGER,
                                                B.NOMGER
                                        FROM     MRT.T0100124 A
                                        LEFT JOIN MRT.T0100051 B ON (B.CODGER = A.CODGER)
                                        LEFT JOIN MRT.RLCACOCMCRGRSUP RACRS ON RACRS.CODSUP = A.CODSUP AND RACRS.CODACOCMC = :CODACOCMC AND RACRS.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                                        WHERE (:NOMSUP IS NULL OR UPPER(A.NOMSUP) LIKE :NOMSUP)
                                        AND (:NOMGER IS NULL OR UPPER(B.NOMGER) LIKE :NOMGER)
                                        AND (RACRS.CODSUP IS NULL)        
                                        AND (:CODSUP IS NULL OR A.CODSUP = :CODSUP)
                                        AND (:CODGER IS NULL OR A.CODGER = :CODGER)
                            ]]>.Value

        parameterMapper.Parameters.Add(":NOMSUP", ValueDBProperty(vo.NOMSUP))
        parameterMapper.Parameters.Add(":NOMGER", ValueDBProperty(vo.NOMGER))
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":CODSUP", ValueDBProperty(vo.CODSUP))
        parameterMapper.Parameters.Add(":CODGER", ValueDBProperty(vo.CODGER))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODSUP", "NOMSUP"}))
    End Function

    Public Function ListarPublicoAlvoGerenteMercado(vo As VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT 
                                                RACRS.CODACOCMC,
                                                RACRS.NUMSEQRGRACOCMC,
                                                RACRS.CODSUP,
                                                A.NOMSUP
                                        FROM MRT.RLCACOCMCRGRSUP RACRS
                                        INNER JOIN MRT.T0100124 A ON (A.CODSUP = RACRS.CODSUP)
                                        WHERE (:CODACOCMC IS NULL OR :CODACOCMC = RACRS.CODACOCMC)
                                        AND (:NUMSEQRGRACOCMC IS NULL OR :NUMSEQRGRACOCMC = NUMSEQRGRACOCMC)

                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function VerificaSeGerenteMercadoExiste(ByVal CODSUP As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGerenteMercado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT
                                                A.CODSUP, 
                                                A.NOMSUP,
                                                A.CODGER
                                        FROM     MRT.T0100124 A                                        
                                        WHERE (CODSUP = :CODSUP)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODSUP", ValueDBProperty(CODSUP))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODSUP", "NOMSUP"}))
    End Function

End Class


<TableAttribute("MRT.RLCACOCMCRGRSUP")>
Public Class RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODSUP As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMSUP As String
    <IgnoreMap>
    Property CODGER As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMGER As String
End Class