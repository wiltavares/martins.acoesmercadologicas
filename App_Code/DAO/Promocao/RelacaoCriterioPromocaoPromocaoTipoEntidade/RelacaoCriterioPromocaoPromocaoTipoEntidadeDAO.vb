﻿Imports Core

Public Class RelacaoCriterioPromocaoPromocaoTipoEntidadeDAO
    Inherits DAOBase(Of VO.RelacaoCriterioPromocaoPromocaoTipoEntidade)

    Public Sub New()
        daoConfiguration = New RelacaoCriterioPromocaoPromocaoTipoEntidadeDAOConfiguration()
    End Sub

    Public Function CarregaRelacaoCriterio(obj As VO.RelacaoCriterioPromocaoPromocaoTipoEntidade) As List(Of VO.RelacaoCriterioPromocaoPromocaoTipoEntidade)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[select TIPEDEPUBALVPMC
                                          , CODPMC
                                          , CODCTRPMC
                                          , VLRLIMMNMFXACTRPMC
                                          , VLRLIMMAXFXACTRPMC
                                          , QDEPTOFXACTRPMC
                                          , CODGRPMIXMERPMC
                                          , DESCTRMIXPMC
                                          , TIPEDENIVPRMPMC
                                    FROM MRT.T0107960
                                    WHERE (:CODPMC IS NULL OR CODPMC = :CODPMC)
                                     
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(obj.CODPMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function SelectRel(obj As VO.RelacaoCriterioPromocaoPromocaoTipoEntidade) As List(Of VO.RelacaoCriterioPromocaoPromocaoTipoEntidade)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[ SELECT
                                        CODCTRPMC,
                                        CODFTRCNDCTRPMC,
                                        CODPMC,
                                        FLGMNTCTRPMC,
                                        TIPEDEPUBALVPMC,
                                        TIPEQCCALCTRPMC,
                                        VLRMNMFTRCNDCTRPMC
                                        FROM     MRT.T0107986
                                        WHERE CODPMC = :CODPMC
                                        AND (:TIPEDEPUBALVPMC IS NOT NULL AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(obj.CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(obj.TIPEDEPUBALVPMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"VLRMNMFTRCNDCTRPMC", "TIPEQCCALCTRPMC", "TIPEDEPUBALVPMC", "CODCTRPMC", "CODFTRCNDCTRPMC", "CODPMC", "FLGMNTCTRPMC"}))
    End Function

End Class

<TableAttribute("MRT.T0107986")>
Public Class RelacaoCriterioPromocaoPromocaoTipoEntidadeDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
End Class