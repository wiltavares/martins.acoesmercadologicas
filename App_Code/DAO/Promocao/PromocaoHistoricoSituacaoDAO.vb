﻿Imports Core

Public Class PromocaoHistoricoSituacaoDAO
    Inherits DAOBase(Of VO.PromocaoHistoricoSituacao)

    Public Sub New()
        daoConfiguration = New PromocaoHistoricoSituacaoConfiguration()
    End Sub


    Public Function ListHistorico(CODPMC As Decimal) As List(Of VO.PromocaoHistoricoSituacao)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
            SELECT A.CODPMC
                 , A.NUMSEQ
                 , A.CODFNC
                 , A.DATGRCHST
                 , A.TIPSITPMC
                 , CASE WHEN A.TIPSITPMC = ' ' THEN 'Liberada' WHEN  A.TIPSITPMC = 'S' THEN 'Suspensa' ELSE '' END AS DESSITPMC
                 , B.NOMFNC 
            FROM MRT.HSTSITPMC A
            LEFT JOIN MRT.T0100361 B ON A.CODFNC = B.CODFNC
            WHERE (:CODPMC IS NULL OR A.CODPMC = :CODPMC)
            ORDER BY A.NUMSEQ
          ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "NUMSEQ", "CODFNC", "DATGRCHST", "TIPSITPMC", "DESSITPMC", "NOMFNC"}))

    End Function

End Class


<TableAttribute("MRT.HSTSITPMC")>
Public Class PromocaoHistoricoSituacaoConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQ As Nullable(Of Decimal)

    Property CODFNC As Nullable(Of Decimal)
    Property DATGRCHST As Nullable(Of DateTime)
    Property TIPSITPMC As String


    <IgnoreMap>
    Property DESSITPMC As String
    <IgnoreMap>
    Property NOMFNC As String
End Class


