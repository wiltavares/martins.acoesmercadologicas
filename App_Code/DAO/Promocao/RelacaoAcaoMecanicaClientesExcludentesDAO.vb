﻿Imports Core
Imports VO

Public Class RelacaoAcaoMecanicaClientesExcludentesDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaClientesExcludentes)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaClientesExcludentesDAOConfiguration()
    End Sub

    Public Function ListarClientesExcludentes(obj As VO.RelacaoAcaoMecanicaClientesExcludentes)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC
                                        , A.NUMSEQRGRACOCMC
                                        , A.CODCLI
                                        , B.NOMCLI
                                    FROM MRT.RLCACOCMCRGRCLIECS A
                                    INNER JOIN MRT.T0100043 B ON (B.CODCLI = A.CODCLI)
                                    WHERE (:CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC )
                                    AND (:NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC )
                                    AND (:CODCLI IS NULL OR A.CODCLI = :CODCLI )]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(obj.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(obj.NUMSEQRGRACOCMC))
        parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(obj.CODCLI))

        Return connector.ExecuteReader(Of VO.RelacaoAcaoMecanicaClientesExcludentes)(sql, parameterMapper).ToList()

    End Function

    Public Function BuscarListarCliente(obj As VO.RelacaoAcaoMecanicaClientesExcludentes)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT D.CODCLI
                                        , D.NOMCLI
                                     FROM MRT.T0100116 A
                                     INNER JOIN MRT.T0133715 B ON (B.CODREP = A.CODREP)
                                     INNER JOIN MRT.T0133774 C ON (C.CODTETVND = B.CODTETVND)
                                     INNER JOIN MRT.T0100043 D ON (D.CODCLI = C.CODCLI)
                                                            AND D.DATDSTCLI IS NULL
                                     INNER JOIN MRT.T0100124 Y ON (Y.CODSUP = A.CODSUP)
                                                            AND Y.CODSUP <> 70001

                                     LEFT JOIN MRT.RLCACOCMCRGRCLIECS RACRC ON RACRC.CODCLI = D.CODCLI 
                                        AND RACRC.CODACOCMC = :CODACOCMC AND RACRC.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC

                                     WHERE (RACRC.CODCLI IS NULL)]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(obj.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(obj.NUMSEQRGRACOCMC))

        If (Not obj.CODCLI Is Nothing) Then
            sql += " AND (D.CODCLI = :CODCLI)"
            parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(obj.CODCLI))
        End If
        If (Not obj.NOMCLI Is Nothing) Then
            obj.NOMCLI = "%" + UCase(obj.NOMCLI) + "%"
            sql += " AND (UPPER(D.NOMCLI) LIKE :NOMCLI)"
            parameterMapper.Parameters.Add(":NOMCLI", ValueDBProperty(obj.NOMCLI))
        End If

        Return connector.ExecuteReader(Of VO.RelacaoAcaoMecanicaClientesExcludentes)(sql, parameterMapper).ToList()

    End Function

    Public Function VerificaSeClienteExiste(ByVal CODCLI As Decimal) As List(Of VO.RelacaoAcaoMecanicaClientesExcludentes)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODCLI
                                              ,NOMCLI
                                        FROM MRT.T0100043                                   
                                        WHERE (CODCLI = :CODCLI)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(CODCLI))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function
End Class


<TableAttribute("MRT.RLCACOCMCRGRCLIECS")>
Public Class RelacaoAcaoMecanicaClientesExcludentesDAOConfiguration
    Inherits DAOConfiguration

    <IgnoreMap>
    Property NOMCLI As String
End Class