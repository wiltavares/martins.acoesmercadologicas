﻿Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteDAOConfiguration()
    End Sub

    Public Function BuscarTipoGrupamentoCliente() As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT
                                                CODUNDESRNGC,
                                                DESUNDESRNGC
                                         FROM   MRT.T0131275
                            ]]>.Value
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODUNDESRNGC", "DESUNDESRNGC"}))
    End Function

    Public Function ListarGrupamentoCliente(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        
                                        SELECT B.CODGRPCLI,
                                               B.DESGRPCLI,
                                               A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC

                                        FROM
                                        MRT.T0105347 B
                                        LEFT JOIN MRT.RLCACOCMCRGRGRPCLI A  ON (A.CODGRPCLI = B.CODGRPCLI) AND A.CODACOCMC = :CODACOCMC 
                                                                                                           AND A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPCLI", "DESGRPCLI", "CODACOCMC", "NUMSEQRGRACOCMC"}))
    End Function

    Public Function ListarGrupamentoClienteSelecionadas(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODACOCMC,
                                               NUMSEQRGRACOCMC,
                                               CODGRPCLI 
                                        FROM MRT.RLCACOCMCRGRGRPCLI                                        
                                        WHERE (:CODACOCMC IS NULL OR :CODACOCMC = CODACOCMC)
                                        AND (:NUMSEQRGRACOCMC IS NULL OR :NUMSEQRGRACOCMC = NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "CODGRPCLI"}))
    End Function

    Public Function BuscarGrupamentoClientePorBU(ByVal CODUNDESRNGC As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoGrupamentoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT   DISTINCT GRP.CODGRPCLI,
				                                          DES.DESGRPCLI,
                                                          DES.CODFILEMPEPD,
                                                          DES.CODSGMMCD,
                                                          DES.CODTRCINFGRPCLI,
                                                          DES.FLGACSVNDMIXGRP,
                                                          DES.FLGCALPRVETQ,
                                                          DES.FLGCONTRNGRP,
                                                          DES.FLGEMSLIVMIXGRP,
                                                          DES.FLGGRPCLIPLO,
                                                          DES.FLGIPRMERESGLIVGRP,
                                                          DES.FLGNAOUTZBLQQTA,
                                                          DES.FLGRCPAUTCTS,
                                                          DES.FLGTIPIPRLIVGRP,
                                                          DES.FLGUTZPCOESP,
                                                          DES.INDGRPCNDPGT,
                                                          DES.INDRTCCNDPGT,
                                                          DES.PERRDCVLRPEDMNM
                                                 FROM     MRT.T0100043 CLI
                                                          INNER JOIN MRT.T0100019 ATI  ON CLI.CODATI = ATI.CODATI
                                                          INNER JOIN MRT.T0114850 CNL ON CLI.CODCNL = CNL.CODCNL
                                                          INNER JOIN MRT.T0114893 BT  ON CNL.CODEQINGC = BT.CODEQINGC
                                                          INNER JOIN MRT.T0131275 BU ON BT.CODUNDESRNGC = BU.CODUNDESRNGC
                                                          INNER JOIN MRT.T0105398 GRP ON CLI.CODCLI = GRP.CODCLI
                                                          INNER JOIN MRT.T0105347 DES ON GRP.CODGRPCLI = DES.CODGRPCLI
                                                
                                    ]]>.Value

        Dim codigos = String.Empty
        For Each codigo In CODUNDESRNGC
            If (Not String.IsNullOrEmpty(codigos)) Then
                codigos += ", "
            End If
            codigos += codigo.CODUNDESRNGC.ToString()
        Next

        sql += " WHERE    BU.CODUNDESRNGC IN (" + codigos + ")"

        'parameterMapper.Parameters.Add(":CODUNDESRNGC", ValueDBProperty(CODUNDESRNGC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPCLI"}))
    End Function

End Class


<TableAttribute("MRT.RLCACOCMCRGRGRPCLI")>
Public Class RelacaoAcaoMecanicaPublicoAlvoGrupamentoClienteDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODGRPCLI As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESGRPCLI As String
    <IgnoreMap>
    Property CODUNDESRNGC As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESUNDESRNGC As String
End Class