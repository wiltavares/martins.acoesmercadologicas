﻿Imports Core
Public Class TipoClassificacaocomboDAO
    Inherits DAOBase(Of VO.TipoClassificacaoCombo)

    Public Sub New()
        daoConfiguration = New TipoCriterioDAOConfiguration()
    End Sub

    Public Function CarregaDropTipoClassificacaoCombo(ByVal DESTIPNIVCLFCPT As String, ByVal TIPNIVCLFCPT As Decimal?) As List(Of VO.TipoClassificacaoCombo)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        If Not DESTIPNIVCLFCPT Is Nothing Then
            DESTIPNIVCLFCPT = "%" + UCase(DESTIPNIVCLFCPT) + "%"
        End If

        Dim sql As String = <![CDATA[ SELECT TIPNIVCLFCPT
                                            , DESTIPNIVCLFCPT
                                        FROM MRT.CADTIPNIVCLFCPT]]>.Value

        'Se CODCTRPMC for diferente de 0 adiciona a condição no WHERE
        If (TIPNIVCLFCPT <> 0) Then
            sql = sql + "WHERE TIPNIVCLFCPT = :TIPNIVCLFCPT"
            parameterMapper.Parameters.Add(":TIPNIVCLFCPT", ValueDBProperty(TIPNIVCLFCPT))
        End If

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function
End Class

Public Class TipoClassificacaocomboDAOConfiguration
    Inherits DAOConfiguration
End Class


