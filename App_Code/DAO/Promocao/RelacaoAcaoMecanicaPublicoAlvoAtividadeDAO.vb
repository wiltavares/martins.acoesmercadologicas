﻿Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoAtividadeDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaPublicoAlvoAtividadeDAOConfiguration()
    End Sub

    Public Function BuscarTipoAtividade() As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT
                                                CODUNDESRNGC,
                                                DESUNDESRNGC
                                         FROM   MRT.T0131275
                            ]]>.Value
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODUNDESRNGC", "DESUNDESRNGC"}))
    End Function

    Public Function ListarAtividade(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        
                                        SELECT B.CODATI,
                                               B.NOMATI,
                                               A.CODACOCMC,
                                               A.NUMSEQRGRACOCMC

                                        FROM
                                              MRT.T0100019 B
                                        LEFT JOIN MRT.RLCACOCMCRGRATI A  ON (A.CODATI = B.CODATI) AND A.CODACOCMC = :CODACOCMC 
                                                                                                  AND A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC   
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODATI", "NOMATI", "CODACOCMC", "NUMSEQRGRACOCMC"}))
    End Function

    Public Function ListarAtividadesSelecionadas(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODACOCMC,
                                               NUMSEQRGRACOCMC,
                                               CODATI 
                                        FROM MRT.RLCACOCMCRGRATI                                        
                                        WHERE (:CODACOCMC IS NULL OR :CODACOCMC = CODACOCMC)
                                        AND (:NUMSEQRGRACOCMC IS NULL OR :NUMSEQRGRACOCMC = NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "CODATI"}))
    End Function

    Public Function BuscarAtividadePorBU(ByVal CODUNDESRNGC As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoAtividade)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT   DISTINCT ATI.CODATI,
				                                          ATI.NOMATI,
                                                          ATI.CODCNL,
                                                          ATI.CODEMP,
                                                          ATI.CODEMPFRNINF,
                                                          ATI.CODGRPCLI,
                                                          ATI.CODGRPCLIATC,
                                                          ATI.DATULTALTCNL,
                                                          ATI.FLGPTDCADCLI,
                                                          ATI.FLGPTDCADCLIEVL,
                                                          ATI.FLGREQINFCPLATI,
                                                          ATI.FLGREQRPNTCN,
                                                          ATI.INDATIMPB,
                                                          ATI.INDOBRCADSCOCLI,
                                                          ATI.NUMCLICADATI,
                                                          ATI.QDEMESETBCLICAD,
                                                          ATI.QDEMESRVSCADCLI,
                                                          ATI.QDETPODSTCLIATI,
                                                          ATI.TIPINSESTREQ,
                                                   COUNT (CLI.CODCLI) AS CODCLI
                                                FROM     MRT.T0100043 CLI
                                                        INNER JOIN MRT.T0100019 ATI  ON CLI.CODATI = ATI.CODATI
                                                        INNER JOIN MRT.T0114850 CNL ON CLI.CODCNL = CNL.CODCNL
                                                        INNER JOIN MRT.T0114893 BT  ON CNL.CODEQINGC = BT.CODEQINGC
                                                        INNER JOIN MRT.T0131275 BU ON BT.CODUNDESRNGC = BU.CODUNDESRNGC
                                                        INNER JOIN MRT.T0105398 GRP ON CLI.CODCLI = GRP.CODCLI
         
                                    ]]>.Value
        Dim codigos = String.Empty
        For Each codigo In CODUNDESRNGC
            If (Not String.IsNullOrEmpty(codigos)) Then
                codigos += ", "
            End If
            codigos += codigo.CODUNDESRNGC.ToString()
        Next

        sql += " WHERE    BU.CODUNDESRNGC IN (" + codigos + ")"
        sql += "GROUP BY ATI.CODATI, ATI.NOMATI, ATI.CODCNL, "
        sql += "ATI.CODEMP, "
        sql += "ATI.CODEMPFRNINF, "
        sql += "ATI.CODGRPCLI, "
        sql += "ATI.CODGRPCLIATC, "
        sql += "ATI.DATULTALTCNL, "
        sql += "ATI.FLGPTDCADCLI, "
        sql += "ATI.FLGPTDCADCLIEVL, "
        sql += "ATI.FLGREQINFCPLATI, "
        sql += "ATI.FLGREQRPNTCN, "
        sql += "ATI.INDATIMPB, "
        sql += "ATI.INDOBRCADSCOCLI, "
        sql += "ATI.NUMCLICADATI, "
        sql += "ATI.QDEMESETBCLICAD, "
        sql += "ATI.QDEMESRVSCADCLI, "
        sql += "ATI.QDETPODSTCLIATI, "
        sql += "ATI.TIPINSESTREQ "
        sql += "HAVING COUNT(CLI.CODCLI) > 15"


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODATI"}))
    End Function

End Class


<TableAttribute("MRT.RLCACOCMCRGRATI")>
Public Class RelacaoAcaoMecanicaPublicoAlvoAtividadeDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODATI As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMATI As String
    <IgnoreMap>
    Property CODUNDESRNGC As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESUNDESRNGC As String
End Class