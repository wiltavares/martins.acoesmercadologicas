﻿Imports Core
Imports VO

Public Class PromocaoMixDAO
    Inherits DAOBase(Of VO.PromocaoMix)

    Public Sub New()
        daoConfiguration = New PromocaoMixDAOConfiguration()
    End Sub

    Public Function ListPorPromocao(CODPMC As Decimal) As List(Of VO.PromocaoMix)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[
                                       SELECT CODPMC
                                            , NUMSEQMIXACOCMC
                                            , CODGRPMER
                                            , CODFMLMER
                                            , CODCLSMER
                                            , CODMER
                                            , CODFRN
                                            , CODGRPMIXMERPMC
                                            , INDVLDRGRMIXPMC
                                            , INDITEOBR
                                         FROM MRT.RLCPMCRGRMIX
                                        WHERE (CODPMC = :CODPMC)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))


        Return connector.ExecuteReader(Of VO.PromocaoMix)(sql, parameterMapper)
    End Function




#Region "Mix Grupo Mercadoria"

    Public Function EditMixGrupoMercadoria(ByVal CODPMC As Decimal) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                         SELECT A.CODGRPMER,
                                 A.DESGRPMER,
                                 B.CODPMC,
                                 B.NUMSEQMIXACOCMC,
                                 B.INDVLDRGRMIXPMC
                            FROM MRT.T0100167 A
                            LEFT JOIN MRT.RLCPMCRGRMIX B ON (B.CODGRPMER = A.CODGRPMER) AND B.CODPMC = :CODPMC 
                                                                                        AND B.CODFMLMER IS NULL
                           WHERE A.DATDSTGRPMER IS NULL
                           ORDER BY A.DESGRPMER         
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPMER", "DESGRPMER", "CODPMC", "NUMSEQMIXACOCMC", "INDVLDRGRMIXPMC"}))
    End Function

    Public Function ListarGrupoMercadoriaSelecionados(ByVal CODPMC As Decimal) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODPMC,
                                               NUMSEQMIXACOCMC,
                                               CODGRPMER,
                                               CODGRPMIXMERPMC,
                                               INDVLDRGRMIXPMC
                                        FROM MRT.RLCPMCRGRMIX                                        
                                        WHERE (:CODPMC IS NULL OR :CODPMC = CODPMC)
                                        AND (CODGRPMER IS NOT NULL)
                                        AND (CODFMLMER IS NULL)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "NUMSEQMIXACOCMC", "CODGRPMER", "CODGRPMIXMERPMC", "INDVLDRGRMIXPMC"}))
    End Function
#End Region

#Region "Mix Familia Mercadoria"

    Public Function PesquisaMixFamiliaMercadoria(VO As VO.PromocaoMix) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not VO.DESFMLMER Is Nothing Then
            VO.DESFMLMER = "%" + UCase(VO.DESFMLMER) + "%"
        End If

        If Not VO.DESGRPMER Is Nothing Then
            VO.DESGRPMER = "%" + UCase(VO.DESGRPMER) + "%"
        End If

        Dim sql As String = <![CDATA[
                                        SELECT A.CODFMLMER,
                                               A.CODGRPMER,
                                               A.DESFMLMER,
                                               C.DESGRPMER
                                        FROM MRT.T0100159 A
                                        LEFT JOIN MRT.RLCPMCRGRMIX B ON B.CODFMLMER = A.CODFMLMER AND B.CODGRPMER = A.CODGRPMER AND B.CODPMC = :CODPMC AND B.CODCLSMER IS NULL
                                        LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = A.CODGRPMER
                                        WHERE (:DESFMLMER IS NULL OR UPPER(A.DESFMLMER) LIKE :DESFMLMER)    
                                        AND (:CODFMLMER IS NULL OR A.CODFMLMER = :CODFMLMER)
                                        AND (:DESGRPMER IS NULL OR UPPER(C.DESGRPMER) LIKE :DESGRPMER)
                                        AND (:CODGRPMER IS NULL OR A.CODGRPMER = :CODGRPMER)
                                        AND (A.DATDSTFMLMER IS NULL)
                                        AND B.CODFMLMER IS NULL
                                    ]]>.Value

        parameterMapper.Parameters.Add(":DESFMLMER", ValueDBProperty(VO.DESFMLMER))
        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(VO.CODFMLMER))
        parameterMapper.Parameters.Add(":DESGRPMER", ValueDBProperty(VO.DESGRPMER))
        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(VO.CODGRPMER))
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(VO.CODPMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFMLMER", "CODGRPMER", "DESFMLMER", "DESGRPMER"}))
    End Function

    Friend Function CopiarMixVigentePromocao(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                               SELECT TIPEDEPUBALVPMC
                                    , CODPMC
                                    , 0 AS NUMSEQMIXACOCMC
                                    , CODMER
                                    , CODGRPMIXMERPMC
                                 FROM MRT.T0107994
                                WHERE CODPMC = :CODPMC
			                      AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                            ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"TIPEDEPUBALVPMC", "CODPMC", "NUMSEQMIXACOCMC", "CODMER", "CODGRPMIXMERPMC"}))
    End Function

    Public Function ListarMixFamiliaMercadoriaAdicionados(VO As VO.PromocaoMix) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODPMC,
                                               A.CODFMLMER,
                                               B.DESFMLMER,
                                               A.CODGRPMER,
                                               C.DESGRPMER,
                                               A.NUMSEQMIXACOCMC,
                                               A.INDVLDRGRMIXPMC
                                        FROM MRT.RLCPMCRGRMIX A
                                        LEFT JOIN MRT.T0100159 B ON B.CODFMLMER = A.CODFMLMER AND A.CODGRPMER = B.CODGRPMER
                                        LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = A.CODGRPMER
                                        WHERE (A.CODPMC IS NULL OR A.CODPMC = :CODPMC)
                                          AND (A.CODFMLMER IS NOT NULL)
                                          AND (A.CODCLSMER IS NULL)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(VO.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFMLMER", "DESFMLMER", "CODPMC", "CODGRPMER", "DESGRPMER", "NUMSEQMIXACOCMC", "INDVLDRGRMIXPMC"}))
    End Function

    Friend Sub DeleteMixFamiliaMercadoria(filtro As PromocaoMix)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE 
                                        FROM MRT.RLCPMCRGRMIX A
                                        WHERE A.CODPMC = :CODPMC
                                          AND (A.CODFMLMER IS NOT NULL AND A.CODCLSMER IS NULL)
                                          AND A.INDVLDRGRMIXPMC = 0
                                          AND (:CODFMLMER IS NULL OR A.CODFMLMER = :CODFMLMER)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(filtro.CODPMC))
        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(filtro.CODFMLMER))

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub

#End Region

#Region "Mix Classe Mercadoria"

    Public Function PesquisaMixClasseMercadoria(VO As VO.PromocaoMix) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not VO.DESCLSMER Is Nothing Then
            VO.DESCLSMER = "%" + UCase(VO.DESCLSMER) + "%"
        End If

        If Not VO.DESGRPMER Is Nothing Then
            VO.DESGRPMER = "%" + UCase(VO.DESGRPMER) + "%"
        End If

        If Not VO.DESFMLMER Is Nothing Then
            VO.DESFMLMER = "%" + UCase(VO.DESFMLMER) + "%"
        End If

        Dim sql As String = <![CDATA[
                                        SELECT A.CODCLSMER,
                                               A.CODGRPMER,
                                               A.CODFMLMER,
                                               A.DESCLSMER,
                                               C.DESGRPMER,
                                               D.DESFMLMER
                                        FROM MRT.T0100132 A
                                        LEFT JOIN MRT.RLCPMCRGRMIX B ON B.CODCLSMER = A.CODCLSMER AND B.CODGRPMER = A.CODGRPMER AND B.CODPMC = :CODPMC AND B.CODMER IS NULL
                                        LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = A.CODGRPMER
                                        LEFT JOIN MRT.T0100159 D ON D.CODFMLMER = A.CODFMLMER AND D.CODGRPMER = A.CODGRPMER
                                        WHERE (:DESCLSMER IS NULL OR UPPER(A.DESCLSMER) LIKE :DESCLSMER)
                                        AND (:CODCLSMER IS NULL OR A.CODCLSMER = :CODCLSMER)
                                        AND (:DESGRPMER IS NULL OR UPPER(C.DESGRPMER) LIKE :DESGRPMER)
                                        AND (:CODGRPMER IS NULL OR A.CODGRPMER = :CODGRPMER)
                                        AND (:DESFMLMER IS NULL OR UPPER(D.DESFMLMER) LIKE :DESFMLMER)
                                        AND (:CODFMLMER IS NULL OR A.CODFMLMER = :CODFMLMER)
                                        AND (A.DATDSTCLSMER IS NULL)
                                        AND B.CODCLSMER IS NULL
                                    ]]>.Value

        parameterMapper.Parameters.Add(":DESCLSMER", ValueDBProperty(VO.DESCLSMER))
        parameterMapper.Parameters.Add(":CODCLSMER", ValueDBProperty(VO.CODCLSMER))
        parameterMapper.Parameters.Add(":DESGRPMER", ValueDBProperty(VO.DESGRPMER))
        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(VO.CODGRPMER))
        parameterMapper.Parameters.Add(":DESFMLMER", ValueDBProperty(VO.DESFMLMER))
        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(VO.CODFMLMER))
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(VO.CODPMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODCLSMER", "DESCLSMER", "CODGRPMER", "DESGRPMER", "CODFMLMER", "DESFMLMER"}))
    End Function

    Public Function ListarMixClasseMercadoriaAdicionados(VO As VO.PromocaoMix) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODPMC,
                                               A.CODCLSMER,
                                               B.DESCLSMER,
                                               A.CODGRPMER,
                                               C.DESGRPMER,
                                               A.NUMSEQMIXACOCMC,
                                               A.CODFMLMER,
                                               D.DESFMLMER,
                                               A.INDVLDRGRMIXPMC
                                        FROM MRT.RLCPMCRGRMIX A
                                        LEFT JOIN MRT.T0100132 B ON B.CODCLSMER = A.CODCLSMER AND A.CODGRPMER = B.CODGRPMER AND A.CODFMLMER = B.CODFMLMER
                                        LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = A.CODGRPMER
                                        LEFT JOIN MRT.T0100159 D ON D.CODFMLMER = A.CODFMLMER AND D.CODGRPMER = A.CODGRPMER
                                        WHERE (A.CODPMC IS NULL OR A.CODPMC = :CODPMC)
                                            AND (A.CODCLSMER IS NOT NULL)
                                            AND (A.CODMER IS NULL)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(VO.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODCLSMER", "DESCLSMER", "CODPMC", "CODGRPMER", "DESGRPMER", "NUMSEQMIXACOCMC", "DESFMLMER", "CODFMLMER", "INDVLDRGRMIXPMC"}))
    End Function

    Friend Sub DeleteMixClasseMercadoria(filtro As PromocaoMix)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE 
                                        FROM MRT.RLCPMCRGRMIX A
                                        WHERE A.CODPMC = :CODPMC
                                          AND (A.CODCLSMER IS NOT NULL AND A.CODMER IS NULL)
                                          AND A.INDVLDRGRMIXPMC = 0
                                          AND (:CODCLSMER IS NULL OR A.CODCLSMER = :CODCLSMER)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(filtro.CODPMC))
        parameterMapper.Parameters.Add(":CODCLSMER", ValueDBProperty(filtro.CODCLSMER))

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub
#End Region

#Region "Mix Mercadoria"

    Public Function PesquisaMixMercadoria(VO As VO.PromocaoMix) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not VO.DESMER Is Nothing Then
            VO.DESMER = "%" + UCase(VO.DESMER) + "%"
        End If

        If Not VO.DESGRPMER Is Nothing Then
            VO.DESGRPMER = "%" + UCase(VO.DESGRPMER) + "%"
        End If

        If Not VO.DESFMLMER Is Nothing Then
            VO.DESFMLMER = "%" + UCase(VO.DESFMLMER) + "%"
        End If

        If Not VO.DESCLSMER Is Nothing Then
            VO.DESCLSMER = "%" + UCase(VO.DESCLSMER) + "%"
        End If

        If Not VO.NOMFRN Is Nothing Then
            VO.NOMFRN = "%" + UCase(VO.NOMFRN) + "%"
        End If

        Dim sql As String = <![CDATA[
                                      SELECT A.CODMER
                                           , A.DESMER
                                           , A.CODGRPMER
                                           , C.DESGRPMER
                                           , A.CODFMLMER
                                           , D.DESFMLMER
                                           , A.CODCLSMER
                                           , E.DESCLSMER
                                        FROM MRT.T0100086  A
                                        LEFT JOIN MRT.RLCPMCRGRMIX B ON B.CODMER = A.CODMER AND B.CODGRPMER IS NULL AND B.CODPMC = :CODPMC
                                        LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = A.CODGRPMER
                                        LEFT JOIN MRT.T0100159 D ON D.CODFMLMER = A.CODFMLMER AND D.CODGRPMER = A.CODGRPMER
                                        LEFT JOIN MRT.T0100132 E ON E.CODCLSMER = A.CODCLSMER AND E.CODFMLMER = A.CODFMLMER AND E.CODGRPMER = A.CODGRPMER
                                        LEFT JOIN MRT.T0100426 FRN ON A.CODFRNPCPMER = frn.CODFRN
                                       WHERE (:DESMER IS NULL OR UPPER(A.DESMER) LIKE :DESMER)    
                                         AND (:CODMER IS NULL OR A.CODMER = :CODMER)  
                                         AND (:DESGRPMER IS NULL OR UPPER(C.DESGRPMER) LIKE :DESGRPMER)  
                                         AND (:CODGRPMER IS NULL OR A.CODGRPMER = :CODGRPMER)  
                                         AND (:DESFMLMER IS NULL OR UPPER(D.DESFMLMER) LIKE :DESFMLMER)  
                                         AND (:CODFMLMER IS NULL OR A.CODFMLMER = :CODFMLMER)  
                                         AND (:DESCLSMER IS NULL OR UPPER(E.DESCLSMER) LIKE :DESCLSMER)  
                                         AND (:CODCLSMER IS NULL OR A.CODCLSMER = :CODCLSMER)  
                                         AND (:CODFRN IS NULL OR  frn.CODFRN = :CODFRN)
                                         AND  (:NOMFRN IS NULL OR UPPER(frn.NOMFRN) LIKE :NOMFRN)  
                                         AND (A.DATDSTMER IS NULL)
                                         AND (B.CODMER IS NULL)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":DESMER", ValueDBProperty(VO.DESMER))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(VO.CODMER))
        parameterMapper.Parameters.Add(":DESGRPMER", ValueDBProperty(VO.DESGRPMER))
        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(VO.CODGRPMER))
        parameterMapper.Parameters.Add(":DESFMLMER", ValueDBProperty(VO.DESFMLMER))
        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(VO.CODFMLMER))
        parameterMapper.Parameters.Add(":DESCLSMER", ValueDBProperty(VO.DESCLSMER))
        parameterMapper.Parameters.Add(":CODCLSMER", ValueDBProperty(VO.CODCLSMER))
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(VO.CODPMC))
        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(VO.CODFRN))
        parameterMapper.Parameters.Add(":NOMFRN", ValueDBProperty(VO.NOMFRN))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODGRPMER", "DESGRPMER", "CODFMLMER", "DESFMLMER", "CODCLSMER", "DESCLSMER"}))
    End Function

    Public Function PesquisaMixMercadoriaEdl(VO As VO.PromocaoMix) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not VO.DESMER Is Nothing Then
            VO.DESMER = "%" + UCase(VO.DESMER) + "%"
        End If

        If Not VO.NOMFRN Is Nothing Then
            VO.NOMFRN = "%" + UCase(VO.NOMFRN) + "%"
        End If

        Dim sql As String = <![CDATA[SELECT DISTINCT A.CODPRD AS CODMER
                                        , A.DESPRD AS DESMER
                                        , 0 AS CODGRPMER
                                        , ' ' AS DESGRPMER
                                        , 0 AS CODFMLMER
                                        , ' ' AS DESFMLMER
                                        , 0 AS CODCLSMER
                                        , ' ' AS DESCLSMER
                                    FROM MRT.CADPRDSMA A
                                    INNER JOIN MRT.RLCPRDFRNPLOSMA B ON B.CODPRD = A.CODPRD AND B.DATDST IS NULL
                                    INNER JOIN MRT.T0148968 C ON C.CODFRN = B.CODFRN AND C.DATDSTFRN IS NULL
                                    INNER JOIN MRT.RLCCNDPGTCPTBTB D ON D.CODFRN = B.CODFRN AND D.DATDST IS NULL
                                    INNER JOIN MRT.T0138768 E ON E.CODFRN = B.CODFRN AND E.TIPFRNACOCMC = 1
                                    WHERE C.CODFRN = :CODFRN
                                    AND (:NOMFRN IS NULL OR UPPER(C.RAZSOCFRN) LIKE :NOMFRN)  
                                    AND (:CODMER IS NULL OR A.CODPRD = :CODMER)
                                    AND (:DESMER IS NULL OR UPPER(A.DESPRD) LIKE :DESMER)]]>.Value

        parameterMapper.Parameters.Add(":DESMER", ValueDBProperty(VO.DESMER))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(VO.CODMER))
        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(VO.CODFRN))
        parameterMapper.Parameters.Add(":NOMFRN", ValueDBProperty(VO.NOMFRN))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODGRPMER", "DESGRPMER", "CODFMLMER", "DESFMLMER", "CODCLSMER", "DESCLSMER"}))
    End Function

    Public Function PesquisaMixMercadoriaPorPromocao(ByVal CODPMCatual As Decimal, ByVal CODPMCtoFind As Decimal) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                      SELECT promocaoMix.CODMER
                                          , mercadoria.DESMER
                                          , mercadoria.CODGRPMER
                                          , grupoMercadoria.DESGRPMER
                                      FROM MRT.T0107935 promocao
                                          INNER JOIN MRT.RLCPMCRGRMIX promocaoMix on promocaoMix.CODPMC = promocao.CODPMC
                                          INNER JOIN MRT.T0100086 mercadoria on mercadoria.CODMER = promocaoMix.CODMER
                                          INNER JOIN MRT.T0100167 grupoMercadoria on grupoMercadoria.CODGRPMER = mercadoria.CODGRPMER
                                            
                                      WHERE (promocao.CODPMC = :CODPMCtoFind)
                                          AND (promocaoMix.CODMER IS NOT NULL )
                                          AND (mercadoria.DATDSTMER IS NULL)
                                          AND NOT EXISTS (
                                            SELECT promocaoMix.CODMER
                                            FROM MRT.T0107935 promocao1
                                              INNER JOIN MRT.RLCPMCRGRMIX promocaoMix1 on promocaoMix1.CODPMC = promocao1.CODPMC
                                              INNER JOIN MRT.T0100086 mercadoria1 on mercadoria1.CODMER = promocaoMix1.CODMER                                
                                            WHERE promocao1.CODPMC = :CODPMCatual 
                                              AND promocaoMix1.CODMER IS NOT NULL 
                                              AND promocaoMix.CODMER = promocaoMix1.CODMER
                                          )
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMCtoFind", ValueDBProperty(CODPMCtoFind))
        parameterMapper.Parameters.Add(":CODPMCatual", ValueDBProperty(CODPMCatual))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODGRPMER", "DESGRPMER"}))
    End Function

    Public Function ListarMixMercadoriaAdicionados(VO As VO.PromocaoMix) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODPMC,
                                               A.CODMER,
                                               B.DESMER,
                                               B.CODGRPMER,
                                               C.DESGRPMER,
                                               A.NUMSEQMIXACOCMC,
                                               A.CODGRPMIXMERPMC,
                                               A.INDVLDRGRMIXPMC
                                        FROM MRT.RLCPMCRGRMIX A
                                        LEFT JOIN MRT.T0100086 B ON B.CODMER = A.CODMER
                                        LEFT JOIN MRT.T0100167 C ON C.CODGRPMER = B.CODGRPMER
                                        WHERE (A.CODPMC IS NULL OR A.CODPMC = :CODPMC)
                                          AND (A.CODMER IS NOT NULL) 
                                          ORDER BY A.CODGRPMIXMERPMC ,A.CODMER
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(VO.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODPMC", "CODGRPMER", "DESGRPMER", "NUMSEQMIXACOCMC", "CODGRPMIXMERPMC", "INDVLDRGRMIXPMC"}))
    End Function

    Public Function ListarMixMercadoriaAdicionadosEdl(VO As VO.PromocaoMix) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT A.CODPMC,
                                               A.CODMER,
                                               B.DESPRD AS DESMER,
                                               0 AS CODGRPMER,
                                               ' ' AS DESGRPMER,
                                               A.NUMSEQMIXACOCMC,
                                               A.CODGRPMIXMERPMC,
                                               A.INDVLDRGRMIXPMC
                                        FROM MRT.RLCPMCRGRMIX A
                                        LEFT JOIN MRT.CADPRDSMA B ON B.CODPRD = A.CODMER
                                        WHERE (A.CODPMC IS NULL OR A.CODPMC = :CODPMC)
                                          AND (A.CODMER IS NOT NULL) 
                                          ORDER BY A.CODGRPMIXMERPMC ,A.CODMER
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(VO.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODPMC", "CODGRPMER", "DESGRPMER", "NUMSEQMIXACOCMC", "CODGRPMIXMERPMC", "INDVLDRGRMIXPMC"}))
    End Function

    Public Function ListarMixMercadoriasParametros(ByVal CODPMC As Decimal, TIPSITPMC As String) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String
        If (TIPSITPMC = "I") Then
            sql = <![CDATA[SELECT A.CODMER
                                            , B.DESMER
                                            , A.QDEMNMMERMIX
                                            , A.QDEMAXMERMIX
                                            , A.INDITEOBR
                                            , A.INDJRNSMA 
                                        FROM MRT.RLCPMCRGRMIX A
                                        INNER JOIN MRT.T0100086 B ON B.CODEMP = 1 AND B.CODMER = A.CODMER
                                        WHERE CODPMC = :CODPMC]]>.Value
        Else
            sql = <![CDATA[SELECT A.CODMER
                                            , B.DESMER
                                            , A.QDEMNMMERMIX
                                            , A.QDEMAXMERMIX
                                            , A.INDITEOBR
                                            , A.INDJRNSMA 
                                        FROM MRT.T0107994 A
                                        INNER JOIN MRT.T0100086 B ON B.CODEMP = 1 AND B.CODMER = A.CODMER
                                        WHERE CODPMC = :CODPMC]]>.Value
        End If

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "QDEMNMMERMIX", "QDEMAXMERMIX", "INDITEOBR", "INDJRNSMA"}))
    End Function

    Public Function ListarMixMercadoriasParametrosEdl(ByVal CODPMC As Decimal, TIPSITPMC As String) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String

        If (TIPSITPMC = "I") Then
            sql = <![CDATA[SELECT A.CODMER
                            , B.DESPRD AS DESMER
                            , A.QDEMNMMERMIX
                            , A.QDEMAXMERMIX
                            , A.INDITEOBR
                            , A.INDJRNSMA 
                           FROM MRT.RLCPMCRGRMIX A
                           INNER JOIN MRT.CADPRDSMA B ON B.CODPRD = A.CODMER
                           WHERE CODPMC = :CODPMC]]>.Value
        Else
            sql = <![CDATA[SELECT A.CODMER
                            , B.DESPRD AS DESMER
                            , A.QDEMNMMERMIX
                            , A.QDEMAXMERMIX
                            , A.INDITEOBR
                            , A.INDJRNSMA 
                           FROM MRT.T0107994 A
                           INNER JOIN MRT.CADPRDSMA B ON B.CODPRD = A.CODMER
                           WHERE CODPMC = :CODPMC]]>.Value
        End If

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "QDEMNMMERMIX", "QDEMAXMERMIX", "INDITEOBR", "INDJRNSMA"}))
    End Function

    Public Function ListarParametrosMix(ByVal CODPMC As Decimal, TIPSITPMC As String) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String
        If (TIPSITPMC = "I") Then
            sql = <![CDATA[SELECT A.CODMER
                                            , A.QDEMNMMERMIX
                                            , A.QDEMAXMERMIX
                                            , A.INDITEOBR
                                            , A.INDJRNSMA 
                                        FROM MRT.RLCPMCRGRMIX A
                                        WHERE CODPMC = :CODPMC]]>.Value
        Else
            sql = <![CDATA[SELECT A.CODMER
                                            , A.QDEMNMMERMIX
                                            , A.QDEMAXMERMIX
                                            , A.INDITEOBR
                                            , A.INDJRNSMA 
                                        FROM MRT.T0107994 A
                                        WHERE CODPMC = :CODPMC]]>.Value
        End If

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "QDEMNMMERMIX", "QDEMAXMERMIX", "INDITEOBR", "INDJRNSMA"}))
    End Function

    Friend Sub DeleteMixMercadoria(filtro As PromocaoMix)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE 
                                        FROM MRT.RLCPMCRGRMIX A
                                        WHERE A.CODPMC = :CODPMC
                                          AND A.CODMER IS NOT NULL
                                          AND A.INDVLDRGRMIXPMC = 0
                                          AND (:CODMER IS NULL OR A.CODMER = :CODMER)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(filtro.CODPMC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(filtro.CODMER))

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub
#End Region

#Region "Mix Fornecedor"

    Public Function PesquisaMixFornecedor(VO As VO.PromocaoMix) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        If Not VO.NOMFRN Is Nothing Then
            VO.NOMFRN = "%" + UCase(VO.NOMFRN) + "%"
        End If

        Dim sql As String = <![CDATA[
                             SELECT DISTINCT A.CODFRN
                                           , A.NOMFRN
                                        FROM MRT.T0100426  A
                                       INNER JOIN MRT.T0113625 B ON  A.CODCPR = B.CODCPR
                                       INNER JOIN MRT.T0118570 C ON B.CODGERPRD = C.CODGERPRD 
                                       INNER JOIN MRT.T0123183 D ON C.CODDRTCMP = D.CODDRTCMP
                                        LEFT JOIN MRT.RLCPMCRGRMIX X ON X.CODFRN = A.CODFRN AND X.CODPMC = :CODPMC
                                       WHERE (A.DATDSTDIVFRN IS NULL)
                                         AND (:NOMFRN IS NULL OR UPPER(A.NOMFRN) LIKE :NOMFRN)
                                         AND (:CODFRN IS NULL OR A.CODFRN = :CODFRN)
                                         AND (:CODDIVCMP IS NULL OR C.CODDIVCMP = :CODDIVCMP)
                                         AND (:CODDRTCMP IS NULL OR D.CODDRTCMP = :CODDRTCMP)
                                         AND (X.CODFRN IS NULL)
                                    ]]>.Value

        parameterMapper.Parameters.Add(":NOMFRN", ValueDBProperty(VO.NOMFRN))
        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(VO.CODFRN))
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(VO.CODPMC))

        parameterMapper.Parameters.Add(":CODDRTCMP", ValueDBProperty(VO.CODDRTCMP))
        parameterMapper.Parameters.Add(":CODDIVCMP", ValueDBProperty(VO.CODDIVCMP))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFRN", "NOMFRN"}))
    End Function

    Public Function ListarMixFornecedorAdicionados(VO As VO.PromocaoMix) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT  A.CODPMC
                                               ,A.CODFRN
                                               ,B.NOMFRN
                                               ,A.NUMSEQMIXACOCMC
                                               ,A.INDVLDRGRMIXPMC
                                        FROM MRT.RLCPMCRGRMIX A
                                        LEFT JOIN MRT.T0100426 B ON B.CODFRN = A.CODFRN
                                        WHERE (A.CODPMC IS NULL OR A.CODPMC = :CODPMC)
                                          AND (A.CODFRN IS NOT NULL)                                        
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(VO.CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFRN", "NOMFRN", "NUMSEQMIXACOCMC", "INDVLDRGRMIXPMC"}))
    End Function

    Friend Sub DeleteMixFornecedor(filtro As PromocaoMix)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE 
                                        FROM MRT.RLCPMCRGRMIX A
                                        WHERE A.CODPMC = :CODPMC
                                          AND A.CODFRN IS NOT NULL
                                          AND A.INDVLDRGRMIXPMC = 0
                                          AND (:CODFRN IS NULL OR A.CODFRN = :CODFRN)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(filtro.CODPMC))
        parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(filtro.CODFRN))

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub

#End Region

    Public Function SelectCarregarDropGrupoMercadoria() As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODGRPMER,
                                               DESGRPMER
                                        FROM MRT.T0100167 
                                                 WHERE DATDSTGRPMER IS NULL
                                                 ORDER BY DESGRPMER    
                                    ]]>.Value
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPMER", "DESGRPMER"}))
    End Function

    Public Function SelectCarregarDropCategoria(ByVal CODGRPMER As Decimal) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT
                                                 CODFMLMER,
                                                 CODGRPMER,
                                                 DESFMLMER,
                                                 CODEMP,
                                                 CODCTGMOVMER,
                                                 DATDSTFMLMER
                                        FROM     MRT.T0100159
                                        WHERE  (CODGRPMER IS NULL OR CODGRPMER = :CODGRPMER)
                                                 AND DATDSTFMLMER IS NULL
                                        ORDER BY DESFMLMER
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(CODGRPMER))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFMLMER", "DESFMLMER"}))
    End Function

    Public Function SelectCarregarDropSubCategoria(ByVal CODGRPMER As Decimal, ByVal CODFMLMER As Decimal) As List(Of VO.PromocaoMix)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT
                                                    CODCLSMER,
                                                    CODFMLMER,
                                                    DESCLSMER,
                                                    CODEMP,
                                                    CODGRPMER,
                                                    PERMRGMNMCTG,
                                                    PERMRGMAXCTG,
                                                    PERCMSMAXRLCMRGCTG,
                                                    CODCTGMOVMER,
                                                    DATDSTCLSMER,
                                                    CODPAPCTG,
                                                    INDCTGMPB,
                                                    VLRMAXVNDMPB
                                        FROM     MRT.T0100132
                                        WHERE    (CODGRPMER IS NULL OR CODGRPMER = :CODGRPMER)
                                             AND (CODFMLMER IS NULL OR CODFMLMER = :CODFMLMER)
                                             AND DATDSTCLSMER IS NULL
                                        ORDER BY DESCLSMER
                                    ]]>.Value

        parameterMapper.Parameters.Add(":CODGRPMER", ValueDBProperty(CODGRPMER))
        parameterMapper.Parameters.Add(":CODFMLMER", ValueDBProperty(CODFMLMER))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODCLSMER", "DESCLSMER"}))
    End Function

    'Selecionar maior NUMSEQMIXACOCMC
    Public Function SelectNextNUMSEQMIXACOCMC(ByVal CODPMC As Decimal) As Decimal

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       SELECT NVL(MAX( NVL(NUMSEQMIXACOCMC, 0)) + 1, 1) AS NUMSEQMIXACOCMC
                                         FROM MRT.RLCPMCRGRMIX A 
                                        WHERE (A.CODPMC = :CODPMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))


        Return connector.ExecuteReader(Of VO.PromocaoMix)(sql, parameterMapper).First().NUMSEQMIXACOCMC
    End Function

#Region "Count Mix Controle Botão 'Configurar' e 'Selecionar'"
    Public Function SelectCountMix(ByVal CODPMC As Decimal) As VO.PromocaoMix

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       SELECT 
                                          (SELECT CASE WHEN COUNT(CODGRPMER) > 0 THEN 1 ELSE 0 END FROM MRT.RLCPMCRGRMIX WHERE CODPMC = :CODPMC AND CODFMLMER IS NULL)  AS INDGRUPOMERCADORIA
                                        , (SELECT CASE WHEN COUNT(CODFMLMER) > 0 THEN 1 ELSE 0 END FROM MRT.RLCPMCRGRMIX WHERE CODPMC = :CODPMC AND CODCLSMER IS NULL)  AS INDFAMILIAMERCADORIA
                                        , (SELECT CASE WHEN COUNT(CODCLSMER) > 0 THEN 1 ELSE 0 END FROM MRT.RLCPMCRGRMIX WHERE CODPMC = :CODPMC AND CODMER IS NULL)  AS INDCLASSEMERCADORIA
                                        , (SELECT CASE WHEN COUNT(CODMER) > 0 THEN 1 ELSE 0 END FROM MRT.RLCPMCRGRMIX WHERE CODPMC = :CODPMC AND CODFRN IS NULL)  AS INDMERCADORIA
                                        , (SELECT CASE WHEN COUNT(CODFRN) > 0 THEN 1 ELSE 0 END FROM MRT.RLCPMCRGRMIX WHERE CODPMC = :CODPMC)  AS INDFORNECEDOR
                                        FROM DUAL
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"INDGRUPOMERCADORIA", "INDFAMILIAMERCADORIA", "INDCLASSEMERCADORIA", "INDMERCADORIA", "INDFORNECEDOR"})).FirstOrDefault()

    End Function
#End Region


    Public Sub UpdateIndicadorValidadeRegraMIX(CODPMC As Decimal, INDVLDRGRMIXPMC As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.RLCPMCRGRMIX
                                        SET INDVLDRGRMIXPMC = :INDVLDRGRMIXPMC
                                       WHERE CODPMC = :CODPMC
                            ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", CODPMC)
        parameterMapper.Parameters.Add(":INDVLDRGRMIXPMC", INDVLDRGRMIXPMC)
        connector.ExecuteScalar(sql, parameterMapper)
    End Sub



End Class

<TableAttribute("MRT.RLCPMCRGRMIX")>
Public Class PromocaoMixDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODPMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQMIXACOCMC As Nullable(Of Decimal)
    Property CODGRPMER As Nullable(Of Decimal)
    Property CODFMLMER As Nullable(Of Decimal)
    Property CODCLSMER As Nullable(Of Decimal)
    Property CODMER As Nullable(Of Decimal)
    Property CODFRN As Nullable(Of Decimal)
    'INDICA VALIDAÇÃO REGRA DE MIX DA PROMOÇÃO                             
    Property INDVLDRGRMIXPMC As Nullable(Of Decimal)


    <IgnoreMap>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property DATDSTGRPMER As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESGRPMER As String
    <IgnoreMap>
    Property DESFMLMER As String
    <IgnoreMap>
    Property DESCLSMER As String
    <IgnoreMap>
    Property DESMER As String
    <IgnoreMap>
    Property NOMFRN As String
    <IgnoreMap>
    Property INDGRUPOMERCADORIA As Nullable(Of Boolean)
    <IgnoreMap>
    Property INDFAMILIAMERCADORIA As Nullable(Of Boolean)
    <IgnoreMap>
    Property INDCLASSEMERCADORIA As Nullable(Of Boolean)
    <IgnoreMap>
    Property INDMERCADORIA As Nullable(Of Boolean)
    <IgnoreMap>
    Property INDFORNECEDOR As Nullable(Of Boolean)
    <IgnoreMap>
    Property ListMixFamiliaMercadoria As List(Of PromocaoMixViewModel)
    <IgnoreMap>
    Property PromocaoMixGrupoMercadoria As List(Of PromocaoMixViewModel)

    <IgnoreMap>
    Property CODDRTCMP As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODDIVCMP As Nullable(Of Decimal)
End Class

