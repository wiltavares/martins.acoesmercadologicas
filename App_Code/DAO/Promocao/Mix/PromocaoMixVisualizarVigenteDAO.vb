﻿Imports Core

Public Class PromocaoMixVisualizarVigenteDAO
    Inherits DAOBase(Of VO.PromocaoMixVisualizarVigente)

    Public Sub New()
        daoConfiguration = New PromocaoMixVisualizarVigenteDAOConfiguration()
    End Sub

    Public Function SelectRelacaoPromocaoSeqGrupoMix(ByVal CODPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        
                                    SELECT 
                                             CODPMC,
                                             VLRMEDITEGRPMIXPMC, 
                                             NUMSEQGRPMIXPMC, 
                                             QDEMNMITEGRPMIXPMC 
                                    FROM     MRT.RLCPMCNUMSEQGRPMIX 
                                    WHERE    CODPMC = :CODPMC
                                            ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODPMC", "VLRMEDITEGRPMIXPMC", "NUMSEQGRPMIXPMC", "QDEMNMITEGRPMIXPMC"}))
    End Function

    Public Function SelectValoresUnitariosBrutoDeVendaDaMercadoria(ByVal CODFILEMP As Decimal, ByVal lstMer As List(Of Decimal)) As List(Of VO.PromocaoMixVisualizarVigente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                 
                                        SELECT   A.CODMER, 
                                                 A.VLRUNTPCOBRT 
                                        FROM     MRT.T0230856 A, 
                                                 MRT.T0102640 B, 
                                                 MRT.T0104227 C, 
                                                 MRT.T0118392 D 
                                        WHERE    D.CODEMP=1 
                                                 AND D.DATREFLIVPCOVGR=B.DATREFALTCID 
                                                 AND B.CODEMP=1 
                                                 AND B.CODFILEMP=A.CODFILEMP 
                                                 AND B.CODFILEMP=A.CODFILEMPFAT 
                                                 AND B.CODFILEMP=C.CODFILEMP 
                                                 AND B.CODCID=C.CODCIDFILEMP 
                                                 AND A.NUMRLCCIDGIR=B.NUMRLCCIDGIR 
                                                 AND CODMER IN (]]>.Value

        sql = sql + String.Join(",", lstMer)
        sql = sql + ")"

        If (CODFILEMP <> 0) Then
            sql = sql + " AND B.CODFILEMP= :CODFILEMP "

            parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(CODFILEMP))
        End If


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "VLRUNTPCOBRT"}))
    End Function

    Public Function SelectValoresUnitariosBrutoDeVendaDaMercadoria2(ByVal CODFILEMP As Decimal, ByVal lstMer As List(Of Decimal)) As List(Of VO.PromocaoMixVisualizarVigente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT   A.CODMER, 
                                                 A.VLRUNTPCOBRT 
                                        FROM     MRT.T0230856 A, 
                                                 MRT.T0102640 B, 
                                                 MRT.T0112963 C, 
                                                 MRT.T0118392 D 
                                        WHERE    D.CODEMP=1 
                                                 AND D.DATREFLIVPCOVGR=B.DATREFALTCID 
                                                 AND B.CODEMP=1 
                                                 AND B.CODFILEMP=A.CODFILEMP 
                                                 AND B.CODFILEMP=A.CODFILEMPFAT 
                                                 AND B.CODFILEMP=C.CODFILEMP 
                                                 AND B.CODCID=C.CODCIDPCOACOCMC 
                                                 AND A.NUMRLCCIDGIR=B.NUMRLCCIDGIR  
                                                 AND CODMER IN (]]>.Value

        sql = sql + String.Join(",", lstMer)
        sql = sql + ")"

        If (CODFILEMP <> 0) Then
            sql = sql + " AND B.CODFILEMP= :CODFILEMP "

            parameterMapper.Parameters.Add(":CODFILEMP", ValueDBProperty(CODFILEMP))
        End If

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "VLRUNTPCOBRT"}))
    End Function

    Public Function SelectValoresUnitariosBrutoDeVendaDaMercadoriaCusto(ByVal CODACOCMC As Decimal, ByVal lstMer As List(Of Decimal)) As List(Of VO.PromocaoMixVisualizarVigente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT nvl(ITEACOCMC.VLRUNTITEOCDACOCMC, 0) As VLRUNTPCOBRT,
                                        MER.CODMER AS CODMER
                                    FROM MRT.T0100086 MER, MRT.T0138776 ITEACOCMC 
                                    WHERE MER.CODEMP = 1 
                                      and ITEACOCMC.CODACOCMC = :CODACOCMC
                                      and ITEACOCMC.CODMER = MER.CODMER
                                      and MER.CODMER in (]]>.Value

        sql = sql + String.Join(",", lstMer)
        sql = sql + ")"

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "VLRUNTPCOBRT"}))
    End Function

    Public Function PesquisaItensVisualizarMixVigente(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT    
                                                 A.CODGRPMIXMERPMC,
                                                 A.CODMER,
                                                 B.DESMER,
                                                 B.CODFRNPCPMER,
                                                 A.CODPMC,
                                                 A.QDEPTOMERPMC,
                                                 A.TIPEDEPUBALVPMC,
                                                 A.VLRCNVUNDVNDDNMPMC,
                                                 A.VLRCNVUNDVNDNUMPMC,
                                                 B.QDEMNMVNDMER,
                                                 B.QDEUNDVNDCXAFRN,
                                                 B.CODGRPMERFRC,
                                                 A.NUMSEQGRPMIXPMC,
                                                 A.VLRUNTPCOBRT,
                                                 C.QDEMNMITEGRPMIXPMC,
                                                 C.VLRMEDITEGRPMIXPMC
                                        FROM     MRT.T0107994 A
                                                 INNER JOIN MRT.T0100086 B ON B.CODEMP = 1 AND A.CODMER = B.CODMER
                                                 LEFT  JOIN MRT.RLCPMCNUMSEQGRPMIX C ON C.CODPMC = A.CODPMC
                                                            AND A.NUMSEQGRPMIXPMC = C.NUMSEQGRPMIXPMC
                                                            WHERE (:CODPMC  IS NULL OR A.CODPMC = :CODPMC)
                                                            AND (:TIPEDEPUBALVPMC IS NULL OR A.TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC)
                                    ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.ExecuteReader(Of VO.PromocaoMixVisualizarVigente)(sql, parameterMapper)
        'Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPMIXMERPMC", "CODMER", "DESMER", "CODFRNPCPMER", "CODPMC", "QDEPTOMERPMC", "TIPEDEPUBALVPMC", "VLRCNVUNDVNDDNMPMC", "VLRCNVUNDVNDNUMPMC", "QDEMNMVNDMER", "QDEUNDVNDCXAFRN", "CODGRPMERFRC", "NUMSEQGRPMIXPMC", "VLRUNTPCOBRT", "QDEMNMITEGRPMIXPMC", "VLRMEDITEGRPMIXPMC"}))
    End Function

    Public Function PesquisaItensVisualizarMixVigenteEdl(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT A.CODMER
                                        , B.DESPRD AS DESMER
                                        , 0 AS CODGRPMER       
                                        , ' ' AS DESGRPMER
                                        , 0 AS CODFMLMER
                                        , ' ' AS DESFMLMER
                                        , 0 AS CODCLSMER
                                        , ' ' AS DESCLSMER
                                        , C.CODFRN AS CODFRNPCPMER
                                        , D.RAZSOCFRN AS NOMFRN
							            , A.CODGRPMIXMERPMC
                                        , A.QDEMNMMERMIX
                                        , A.QDEMAXMERMIX
                                        , A.INDITEOBR
                                        , A.INDJRNSMA 
                                       FROM MRT.T0107994 A
                                       INNER JOIN MRT.CADPRDSMA B ON B.CODPRD = A.CODMER
                                       INNER JOIN MRT.RLCPRDFRNPLOSMA C ON C.CODPRD = B.CODPRD AND C.DATDST IS NULL
                                       INNER JOIN MRT.T0148968 D ON D.CODFRN = C.CODFRN AND D.DATDSTFRN IS NULL
                                       INNER JOIN MRT.RLCCNDPGTCPTBTB E ON E.CODFRN = C.CODFRN AND E.DATDST IS NULL                           
                                       INNER JOIN MRT.T0107935 F ON F.CODPMC = A.CODPMC
                                       INNER JOIN MRT.T0138768 G ON G.CODFRN = D.CODFRN AND G.TIPFRNACOCMC = 1 AND G.CODACOCMC = F.CODACOCMC                           
                                       WHERE A.CODPMC = :CODPMC]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))

        Return connector.ExecuteReader(Of VO.PromocaoMixVisualizarVigente)(sql, parameterMapper)
        'Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODGRPMIXMERPMC", "CODMER", "DESMER", "CODFRNPCPMER", "CODPMC", "QDEPTOMERPMC", "TIPEDEPUBALVPMC", "VLRCNVUNDVNDDNMPMC", "VLRCNVUNDVNDNUMPMC", "QDEMNMVNDMER", "QDEUNDVNDCXAFRN", "CODGRPMERFRC", "NUMSEQGRPMIXPMC", "VLRUNTPCOBRT", "QDEMNMITEGRPMIXPMC", "VLRMEDITEGRPMIXPMC"}))
    End Function

    Public Function PreVisualizarMixMecanicaAcao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim relacaoAcaoMecanicaMixDAO As New RelacaoAcaoMecanicaMixDAO()

        Dim configList As List(Of VO.RelacaoAcaoMecanicaMix) = relacaoAcaoMecanicaMixDAO.ListT(New VO.RelacaoAcaoMecanicaMix() With {.CODACOCMC = CODACOCMC, .NUMSEQRGRACOCMC = NUMSEQRGRACOCMC})

        If (configList.Where(Function(f) f.CODGRPMER IsNot Nothing Or f.CODFMLMER IsNot Nothing Or f.CODCLSMER IsNot Nothing Or f.CODMER IsNot Nothing Or f.CODFRN IsNot Nothing).FirstOrDefault() IsNot Nothing) Then

            Dim sql As String
            If (TIPPMC = 1) Then
                sql = <![CDATA[
                                  SELECT A.CODMER,
                                         A.DESMER,
                                         A.CODGRPMER,       
                                         B1.DESGRPMER,
                                         A.CODFMLMER,
                                         B2.DESFMLMER,
                                         A.CODCLSMER,
                                         B3.DESCLSMER,
                                         A.CODFRNPCPMER,
                                         B4.NOMFRN,
                                         E.CODGRPMIXMERPMC
                                    FROM MRT.T0100086 A
                                    LEFT JOIN MRT.T0100167 B1 ON B1.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100159 B2 ON B2.CODFMLMER = A.CODFMLMER AND B2.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100132 B3 ON B3.CODCLSMER = A.CODCLSMER AND B3.CODFMLMER = A.CODFMLMER AND B3.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100426 B4 ON B4.CODFRN = A.CODFRNPCPMER 
                                                             INNER JOIN MRT.RLCACOCMCRGRMIX E ON E.CODMER = A.CODMER
                                                                 AND E.CODMER IS NOT NULL
                                                                 AND E.CODFRN IS NULL
                                                                 AND E.CODACOCMC = :CODACOCMC 
                                                                 AND E.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
 
                                    ]]>.Value
            Else
                sql = <![CDATA[
                                  SELECT A.CODMER,
                                         A.DESMER,
                                         A.CODGRPMER,       
                                         B1.DESGRPMER,
                                         A.CODFMLMER,
                                         B2.DESFMLMER,
                                         A.CODCLSMER,
                                         B3.DESCLSMER,
                                         A.CODFRNPCPMER,
                                         B4.NOMFRN,
                                         0 as CODGRPMIXMERPMC
                                    FROM MRT.T0100086 A
                                    LEFT JOIN MRT.T0100167 B1 ON B1.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100159 B2 ON B2.CODFMLMER = A.CODFMLMER AND B2.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100132 B3 ON B3.CODCLSMER = A.CODCLSMER AND B3.CODFMLMER = A.CODFMLMER AND B3.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100426 B4 ON B4.CODFRN = A.CODFRNPCPMER 

 
                                    ]]>.Value


                If (configList.Where(Function(f) f.CODGRPMER IsNot Nothing And f.CODFMLMER Is Nothing).FirstOrDefault() IsNot Nothing) Then
                    sql += <![CDATA[
                                  INNER JOIN  MRT.RLCACOCMCRGRMIX B ON B.CODGRPMER = A.CODGRPMER 
                                                              AND B.CODFMLMER IS NULL 
                                                              AND B.CODGRPMER IS NOT NULL 
                                                              AND B.CODACOCMC = :CODACOCMC
                                                              AND B.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC

                           ]]>.Value
                End If

                If (configList.Where(Function(f) f.CODGRPMER IsNot Nothing And f.CODFMLMER IsNot Nothing And f.CODCLSMER Is Nothing).FirstOrDefault() IsNot Nothing) Then
                    sql += <![CDATA[
                                 INNER JOIN  MRT.RLCACOCMCRGRMIX C ON C.CODGRPMER = A.CODGRPMER
                                                                  AND C.CODFMLMER = A.CODFMLMER
                                                                  AND C.CODFMLMER IS NOT NULL
                                                                  AND C.CODGRPMER IS NOT NULL 
                                                                  AND C.CODCLSMER IS NULL
                                                                  AND C.CODACOCMC = :CODACOCMC 
                                                                  AND C.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC

                           ]]>.Value
                End If

                If (configList.Where(Function(f) f.CODGRPMER IsNot Nothing And f.CODFMLMER IsNot Nothing And f.CODCLSMER IsNot Nothing And f.CODMER Is Nothing).FirstOrDefault() IsNot Nothing) Then
                    sql += <![CDATA[
                                 INNER JOIN  MRT.RLCACOCMCRGRMIX D ON D.CODGRPMER = A.CODGRPMER
                                                                  AND D.CODFMLMER = A.CODFMLMER 
                                                                  AND D.CODCLSMER = A.CODCLSMER
                                                                  AND D.CODFMLMER IS NOT NULL 
                                                                  AND D.CODGRPMER IS NOT NULL 
                                                                  AND D.CODCLSMER IS NOT NULL 
                                                                  AND D.CODMER IS NULL
                                                                  AND D.CODACOCMC = :CODACOCMC 
                                                                  AND D.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC

                           ]]>.Value
                End If

                If (configList.Where(Function(f) f.CODMER IsNot Nothing And f.CODFRN Is Nothing).FirstOrDefault() IsNot Nothing) Then
                    sql += <![CDATA[
                                 INNER JOIN MRT.RLCACOCMCRGRMIX E ON E.CODMER = A.CODMER
                                                                 AND E.CODMER IS NOT NULL
                                                                 AND E.CODFRN IS NULL
                                                                 AND E.CODACOCMC = :CODACOCMC 
                                                                 AND E.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC

                           ]]>.Value
                End If

                If (configList.Where(Function(f) f.CODFRN IsNot Nothing And f.CODMER Is Nothing).FirstOrDefault() IsNot Nothing) Then
                    sql += <![CDATA[
                                 INNER JOIN  MRT.RLCACOCMCRGRMIX F ON F.CODFRN = A.CODFRNPCPMER
                                                                  AND F.CODFRN IS NOT NULL 
                                                                  AND F.CODMER IS NULL
                                                                  AND F.CODACOCMC = :CODACOCMC 
                                                                  AND F.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC  
                                                                  
                           ]]>.Value
                End If
            End If



            sql += "      WHERE A.CODEMP = 1 AND A.DATDSTMER IS NULL"


            parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
            parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

            Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODGRPMER", "DESGRPMER", "CODFMLMER", "DESFMLMER", "CODCLSMER", "DESCLSMER", "CODFRNPCPMER", "NOMFRN", "CODGRPMIXMERPMC"}))
        Else
            Return Nothing
        End If

    End Function

    Public Function PreVisualizarMixMecanicaAcaoEdl(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal, ByVal TIPPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT A.CODPRD AS CODMER
                                , A.DESPRD AS DESMER
                                , D.CODFRN
                                , C.RAZSOCFRN AS NOMFRN
                                , 0 AS CODGRPMIXMERPMC
                                , 0 AS CODFRNPCPMER
                                , 0 AS CODPMC
                                , 0 AS QDEPTOMERPMC
                                , 0 AS TIPEDEPUBALVPMC
                                , 0 AS VLRCNVUNDVNDDNMPMC
                                , 0 AS QDEMNMVNDMER
                                , 0 AS QDEUNDVNDCXAFRN
                                , 0 AS CODGRPMERFRC
                                , 0 AS NUMSEQGRPMIXPMC
                                , 0 AS VLRUNTPCOBRT
                                , 0 AS QDEMNMITEGRPMIXPMC
                                , 0 AS VLRMEDITEGRPMIXPMC
                            FROM MRT.CADPRDSMA A
                            INNER JOIN MRT.RLCPRDFRNPLOSMA B ON B.CODPRD = A.CODPRD AND B.DATDST IS NULL
                            INNER JOIN MRT.T0148968 C ON C.CODFRN = B.CODFRN AND C.DATDSTFRN IS NULL
                            INNER JOIN MRT.RLCCNDPGTCPTBTB D ON D.CODFRN = B.CODFRN AND D.DATDST IS NULL
                            INNER JOIN MRT.T0138768 E ON E.CODFRN = B.CODFRN AND E.TIPFRNACOCMC = 1
                            INNER JOIN MRT.RLCACOCMCRGRMIX F ON F.CODMER = A.CODPRD
                                                                 AND F.CODMER IS NOT NULL
                                                                 AND F.CODFRN IS NULL
                                                                 AND F.CODACOCMC = :CODACOCMC 
                                                                 AND F.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                            WHERE E.CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODFRN", "NOMFRN", "CODGRPMIXMERPMC", "CODFRNPCPMER", "CODPMC", "QDEPTOMERPMC", "TIPEDEPUBALVPMC", "VLRCNVUNDVNDDNMPMC", "QDEMNMVNDMER", "QDEUNDVNDCXAFRN", "CODGRPMERFRC", "NUMSEQGRPMIXPMC", "VLRUNTPCOBRT", "QDEMNMITEGRPMIXPMC", "VLRMEDITEGRPMIXPMC"}))
    End Function

    Public Function PreVisualizarMixPromocao(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal, ByVal TIPPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)


        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim promocaoMixDAO As New PromocaoMixDAO()

        Dim configList As List(Of VO.PromocaoMix) = promocaoMixDAO.ListPorPromocao(CODPMC)

        If (configList.Where(Function(f) f.CODGRPMER IsNot Nothing Or f.CODFMLMER IsNot Nothing Or f.CODCLSMER IsNot Nothing Or f.CODMER IsNot Nothing Or f.CODFRN IsNot Nothing).FirstOrDefault() IsNot Nothing) Then


            Dim sql As String
            Dim extraColumns As String = ""

            If (TIPPMC = 1) Then
                sql = <![CDATA[
                                  SELECT A.CODMER,
                                         A.DESMER,
                                         A.CODGRPMER,       
                                         B1.DESGRPMER,
                                         A.CODFMLMER,
                                         B2.DESFMLMER,
                                         A.CODCLSMER,
                                         B3.DESCLSMER,
                                         A.CODFRNPCPMER,
                                         B4.NOMFRN,
                                         E.CODGRPMIXMERPMC,
                                         E.QDEMNMMERMIX,
                                         E.QDEMAXMERMIX,
                                         E.INDITEOBR,
                                         E.INDJRNSMA
                                    FROM MRT.T0100086 A
                                    LEFT JOIN MRT.T0100167 B1 ON B1.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100159 B2 ON B2.CODFMLMER = A.CODFMLMER AND B2.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100132 B3 ON B3.CODCLSMER = A.CODCLSMER AND B3.CODFMLMER = A.CODFMLMER AND B3.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100426 B4 ON B4.CODFRN = A.CODFRNPCPMER 
                                   INNER JOIN  MRT.RLCPMCRGRMIX E ON E.CODMER = A.CODMER
                                                               AND E.CODMER IS NOT NULL
                                                               AND E.CODFRN IS NULL
                                                               AND E.CODPMC = :CODPMC
                                    ]]>.Value
            Else





                sql = <![CDATA[
                                  SELECT A.CODMER,
                                         A.DESMER,
                                         A.CODGRPMER,       
                                         B1.DESGRPMER,
                                         A.CODFMLMER,
                                         B2.DESFMLMER,
                                         A.CODCLSMER,
                                         B3.DESCLSMER,
                                         A.CODFRNPCPMER,
                                         B4.NOMFRN,
                                         EXTRA_COLUMNS
                                    FROM MRT.T0100086 A
                                    LEFT JOIN MRT.T0100167 B1 ON B1.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100159 B2 ON B2.CODFMLMER = A.CODFMLMER AND B2.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100132 B3 ON B3.CODCLSMER = A.CODCLSMER AND B3.CODFMLMER = A.CODFMLMER AND B3.CODGRPMER = A.CODGRPMER
                                    LEFT JOIN MRT.T0100426 B4 ON B4.CODFRN = A.CODFRNPCPMER 
                                    ]]>.Value

                If (configList.Where(Function(f) f.CODGRPMER IsNot Nothing And f.CODFMLMER Is Nothing).FirstOrDefault() IsNot Nothing) Then
                    sql += <![CDATA[
                                 INNER JOIN  MRT.RLCPMCRGRMIX B ON B.CODGRPMER = A.CODGRPMER 
                                                               AND B.CODFMLMER IS NULL
                                                               AND B.CODGRPMER IS NOT NULL
                                                               AND B.CODPMC = :CODPMC
                           ]]>.Value

                    extraColumns += <![CDATA[
                                         B.CODGRPMIXMERPMC,
                                         B.QDEMNMMERMIX,
                                         B.QDEMAXMERMIX,
                                         B.INDITEOBR,
                                         B.INDJRNSMA
                                    ]]>.Value

                End If

                If (configList.Where(Function(f) f.CODGRPMER IsNot Nothing And f.CODFMLMER IsNot Nothing And f.CODCLSMER Is Nothing).FirstOrDefault() IsNot Nothing) Then
                    sql += <![CDATA[ 
                                INNER JOIN  MRT.RLCPMCRGRMIX C ON C.CODGRPMER = A.CODGRPMER
                                                              AND C.CODFMLMER = A.CODFMLMER
                                                              AND C.CODFMLMER IS NOT NULL
                                                              AND C.CODGRPMER IS NOT NULL
                                                              AND C.CODCLSMER IS NULL
                                                              AND C.CODPMC = :CODPMC
                           ]]>.Value

                    extraColumns += <![CDATA[
                                         C.CODGRPMIXMERPMC,
                                         C.QDEMNMMERMIX,
                                         C.QDEMAXMERMIX,
                                         C.INDITEOBR,
                                         C.INDJRNSMA
                                    ]]>.Value
                End If

                If (configList.Where(Function(f) f.CODGRPMER IsNot Nothing And f.CODFMLMER IsNot Nothing And f.CODCLSMER IsNot Nothing And f.CODMER Is Nothing).FirstOrDefault() IsNot Nothing) Then
                    sql += <![CDATA[ 
                                INNER JOIN  MRT.RLCPMCRGRMIX D ON D.CODGRPMER = A.CODGRPMER
                                                              AND D.CODFMLMER = A.CODFMLMER
                                                              AND  D.CODCLSMER = A.CODCLSMER
                                                              AND D.CODFMLMER IS NOT NULL
                                                              AND D.CODGRPMER IS NOT NULL
                                                              AND D.CODCLSMER IS NOT NULL
                                                              AND D.CODMER IS NULL
                                                              AND D.CODPMC = :CODPMC
                           ]]>.Value

                    extraColumns += <![CDATA[
                                         D.CODGRPMIXMERPMC,
                                         D.QDEMNMMERMIX,
                                         D.QDEMAXMERMIX,
                                         D.INDITEOBR,
                                         D.INDJRNSMA
                                    ]]>.Value
                End If

                If (configList.Where(Function(f) f.CODMER IsNot Nothing And f.CODFRN Is Nothing).FirstOrDefault() IsNot Nothing) Then
                    sql += <![CDATA[
                                 INNER JOIN  MRT.RLCPMCRGRMIX E ON E.CODMER = A.CODMER
                                                               AND E.CODMER IS NOT NULL
                                                               AND E.CODFRN IS NULL
                                                               AND E.CODPMC = :CODPMC
                           ]]>.Value

                    extraColumns += <![CDATA[
                                         E.CODGRPMIXMERPMC,
                                         E.QDEMNMMERMIX,
                                         E.QDEMAXMERMIX,
                                         E.INDITEOBR,
                                         E.INDJRNSMA
                                    ]]>.Value
                End If

                If (configList.Where(Function(f) f.CODFRN IsNot Nothing And f.CODMER Is Nothing).FirstOrDefault() IsNot Nothing) Then
                    sql += <![CDATA[
                                 INNER JOIN  MRT.RLCPMCRGRMIX F ON F.CODFRN = A.CODFRNPCPMER
                                                               AND F.CODFRN IS NOT NULL
                                                               AND F.CODMER IS NULL
                                                               AND F.CODPMC = :CODPMC
                           ]]>.Value

                    extraColumns += <![CDATA[
                                         F.CODGRPMIXMERPMC,
                                         F.QDEMNMMERMIX,
                                         F.QDEMAXMERMIX,
                                         F.INDITEOBR,
                                         F.INDJRNSMA
                                    ]]>.Value
                End If
            End If

            sql = Replace(sql, "EXTRA_COLUMNS", extraColumns)
            sql += "      WHERE A.CODEMP = 1 AND A.DATDSTMER IS NULL"
            parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))

            Return connector.ExecuteReader(Of VO.PromocaoMixVisualizarVigente)(sql, parameterMapper)
        Else
            Return Nothing
        End If

    End Function

    Public Function PreVisualizarMixPromocaoEdl(ByVal CODPMC As Decimal, ByVal TIPSITPMC As String) As List(Of VO.PromocaoMixVisualizarVigente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String

        sql = <![CDATA[SELECT DISTINCT A.CODMER
                            , B.DESPRD AS DESMER
                            , 0 AS CODGRPMER       
                            , ' ' AS DESGRPMER
                            , 0 AS CODFMLMER
                            , ' ' AS DESFMLMER
                            , 0 AS CODCLSMER
                            , ' ' AS DESCLSMER
                            , C.CODFRN AS CODFRNPCPMER
                            , D.RAZSOCFRN AS NOMFRN
							, 0 AS CODGRPMIXMERPMC
                            , A.QDEMNMMERMIX
                            , A.QDEMAXMERMIX
                            , A.INDITEOBR
                            , A.INDJRNSMA 
                           FROM MRT.RLCPMCRGRMIX A
                           INNER JOIN MRT.CADPRDSMA B ON B.CODPRD = A.CODMER
                           INNER JOIN MRT.RLCPRDFRNPLOSMA C ON C.CODPRD = B.CODPRD AND C.DATDST IS NULL
                           INNER JOIN MRT.T0148968 D ON D.CODFRN = C.CODFRN AND D.DATDSTFRN IS NULL
                           INNER JOIN MRT.RLCCNDPGTCPTBTB E ON E.CODFRN = C.CODFRN AND E.DATDST IS NULL
                           INNER JOIN MRT.T0107935 F ON F.CODPMC = A.CODPMC
                           INNER JOIN MRT.T0138768 G ON G.CODFRN = D.CODFRN AND G.TIPFRNACOCMC = 1 AND G.CODACOCMC = F.CODACOCMC
                           WHERE A.CODPMC = :CODPMC]]>.Value

        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODGRPMER", "DESGRPMER", "CODFMLMER", "DESFMLMER", "CODCLSMER", "DESCLSMER", "CODFRNPCPMER", "NOMFRN", "CODGRPMIXMERPMC", "QDEMNMMERMIX", "QDEMAXMERMIX", "INDITEOBR", "INDJRNSMA"}))
    End Function

    Public Function MixVigentePromocao(ByVal CODPMC As Decimal, ByVal TIPEDEPUBALVPMC As Decimal) As List(Of VO.PromocaoMixVisualizarVigente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                               SELECT TIPEDEPUBALVPMC
                                    , CODPMC
                                    , CODMER
                                    , CODGRPMIXMERPMC
                                    , QDEPTOMERPMC
                                    , VLRCNVUNDVNDNUMPMC
                                    , VLRCNVUNDVNDDNMPMC
                                    , NUMSEQGRPMIXPMC
                                    , VLRUNTPCOBRT
                                 FROM MRT.T0107994
                                WHERE CODPMC = :CODPMC
			                     AND TIPEDEPUBALVPMC = :TIPEDEPUBALVPMC
                                            ]]>.Value
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(CODPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))

        Return connector.ExecuteReader(Of VO.PromocaoMixVisualizarVigente)(sql, parameterMapper)
    End Function

    Public Function Inserir(ByVal item As VO.PromocaoMixVisualizarVigente) As VO.PromocaoMixVisualizarVigente

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.T0107994 (
                                        CODGRPMIXMERPMC
                                        , CODMER
                                        , CODPMC
                                        , QDEPTOMERPMC
                                        , TIPEDEPUBALVPMC
                                        , VLRCNVUNDVNDDNMPMC
                                        , VLRCNVUNDVNDNUMPMC
                                        , NUMSEQGRPMIXPMC
                                        , VLRUNTPCOBRT
                                        , QDEMNMMERMIX
                                        , QDEMAXMERMIX
                                        , INDITEOBR
                                        , INDJRNSMA) 
                                    VALUES (
                                        :CODGRPMIXMERPMC
                                        , :CODMER
                                        , :CODPMC
                                        , :QDEPTOMERPMC
                                        , :TIPEDEPUBALVPMC
                                        , :VLRCNVUNDVNDDNMPMC
                                        , :VLRCNVUNDVNDNUMPMC
                                        , :NUMSEQGRPMIXPMC
                                        , :VLRUNTPCOBRT
                                        , :QDEMNMMERMIX
                                        , :QDEMAXMERMIX
                                        , :INDITEOBR
                                        , :INDJRNSMA
                                     )]]>.Value

        parameterMapper.Parameters.Add(":CODGRPMIXMERPMC", ValueDBProperty(item.CODGRPMERFRC))
        parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(item.CODMER))
        parameterMapper.Parameters.Add(":CODPMC", ValueDBProperty(item.CODPMC))
        parameterMapper.Parameters.Add(":QDEPTOMERPMC", ValueDBProperty(item.QDEPTOMERPMC))
        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(item.TIPEDEPUBALVPMC))
        parameterMapper.Parameters.Add(":VLRCNVUNDVNDDNMPMC", ValueDBProperty(item.VLRCNVUNDVNDDNMPMC))
        parameterMapper.Parameters.Add(":VLRCNVUNDVNDNUMPMC", ValueDBProperty(item.VLRCNVUNDVNDNUMPMC))
        parameterMapper.Parameters.Add(":NUMSEQGRPMIXPMC", ValueDBProperty(item.NUMSEQGRPMIXPMC))
        parameterMapper.Parameters.Add(":VLRUNTPCOBRT", ValueDBProperty(item.VLRUNTPCOBRT))
        parameterMapper.Parameters.Add(":QDEMNMMERMIX", ValueDBProperty(item.QDEMNMMERMIX))
        parameterMapper.Parameters.Add(":QDEMAXMERMIX", ValueDBProperty(item.QDEMAXMERMIX))
        parameterMapper.Parameters.Add(":INDITEOBR", ValueDBProperty(item.INDITEOBR))
        parameterMapper.Parameters.Add(":INDJRNSMA", ValueDBProperty(item.INDJRNSMA))

        item.CODPMC = connector.ExecuteScalar(sql, parameterMapper)
        Return item
    End Function
End Class


<TableAttribute("MRT.T0107994")>
Public Class PromocaoMixVisualizarVigenteDAOConfiguration
    Inherits DAOConfiguration

    Property CODGRPMIXMERPMC As Nullable(Of Decimal)

    Property CODMER As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESMER As String
    <IgnoreMap>
    Property CODFRNPCPMER As Nullable(Of Decimal)

    Property CODPMC As Nullable(Of Decimal)

    Property QDEPTOMERPMC As Nullable(Of Decimal)

    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)

    Property VLRCNVUNDVNDDNMPMC As Nullable(Of Decimal)

    Property VLRCNVUNDVNDNUMPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEMNMVNDMER As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEUNDVNDCXAFRN As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODGRPMERFRC As Nullable(Of Decimal)

    Property NUMSEQGRPMIXPMC As Nullable(Of Decimal)

    Property VLRUNTPCOBRT As Nullable(Of Decimal)
    <IgnoreMap>
    Property QDEMNMITEGRPMIXPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLRMEDITEGRPMIXPMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODGRPMER As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESGRPMER As String
    <IgnoreMap>
    Property CODFMLMER As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESFMLMER As String
    <IgnoreMap>
    Property CODCLSMER As Nullable(Of Decimal)
    <IgnoreMap>
    Property DESCLSMER As String
    <IgnoreMap>
    Property NOMFRN As String
    Property QDEMNMMERMIX As Nullable(Of Decimal)
    Property QDEMAXMERMIX As Nullable(Of Decimal)
    Property INDITEOBR As Nullable(Of Decimal)
    Property INDJRNSMA As Nullable(Of Decimal)

    '<IgnoreMap>
    'Property QDEMNMMERMIX As Nullable(Of Decimal)
    '<IgnoreMap>
    'Property QDEMAXMERMIX As Nullable(Of Decimal)
    '<IgnoreMap>
    'Property INDITEOBR As Nullable(Of Decimal)
    '<IgnoreMap>
    'Property INDJRNSMA As Nullable(Of Decimal)
End Class

