﻿Imports Core
Imports VO

Public Class RelacaoAcaoMecanicaPublicoAlvoClienteDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaPublicoAlvoClienteDAOConfiguration()
    End Sub

    Public Function BuscarListaCliente(vo As VO.RelacaoAcaoMecanicaPublicoAlvoCliente) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
            SELECT DISTINCT D.CODCLI
                          , D.NOMCLI
                       FROM MRT.T0100116 A
                 INNER JOIN MRT.T0133715 B ON (B.CODREP = A.CODREP)
                 INNER JOIN MRT.T0133774 C ON (C.CODTETVND = B.CODTETVND)
                 INNER JOIN MRT.T0100043 D ON (D.CODCLI = C.CODCLI)
                                        AND D.DATDSTCLI IS NULL
                 INNER JOIN MRT.T0100124 Y ON (Y.CODSUP = A.CODSUP)
                                        AND Y.CODSUP <> 70001
                  LEFT JOIN MRT.RLCACOCMCRGRCLI RACRC 
                  ON RACRC.CODCLI = D.CODCLI 
                  AND RACRC.CODACOCMC = :CODACOCMC AND RACRC.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
                 WHERE (RACRC.CODCLI IS NULL)
                    ]]>.Value


        'SQL concatenado pois ao passar os parametros com AND OR o tempo e resposta foi muito lento (perde os indices)
        If (Not vo.CODCLI Is Nothing) Then
            sql += " AND (D.CODCLI = :CODCLI)"
            parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(vo.CODCLI))
        End If
        If (Not vo.NOMCLI Is Nothing) Then
            vo.NOMCLI = "%" + UCase(vo.NOMCLI) + "%"
            sql += " AND (UPPER(D.NOMCLI) LIKE :NOMCLI)"
            parameterMapper.Parameters.Add(":NOMCLI", ValueDBProperty(vo.NOMCLI))
        End If
        If (Not vo.CODREP Is Nothing) Then
            sql += " AND (A.CODREP = :CODREP)"
            parameterMapper.Parameters.Add(":CODREP", ValueDBProperty(vo.CODREP))
        End If
        If (Not vo.NOMREP Is Nothing) Then
            vo.NOMREP = "%" + UCase(vo.NOMREP) + "%"
            sql += " AND (UPPER(A.NOMREP) LIKE :NOMREP)"
            parameterMapper.Parameters.Add(":NOMREP", ValueDBProperty(vo.NOMREP))
        End If
        If (Not vo.CODSUP Is Nothing) Then
            sql += " AND (A.CODSUP = :CODSUP)"
            parameterMapper.Parameters.Add(":CODSUP", ValueDBProperty(vo.CODSUP))
        End If
        If (Not vo.NOMSUP Is Nothing) Then
            vo.NOMSUP = "%" + UCase(vo.NOMSUP) + "%"
            sql += " AND (UPPER(Y.NOMSUP) LIKE :NOMSUP)"
            parameterMapper.Parameters.Add(":NOMSUP", ValueDBProperty(vo.NOMSUP))
        End If
        'If (Not vo.CODGER Is Nothing) Then
        '    sql += " AND (Z.CODGER = :CODGER)"
        '    parameterMapper.Parameters.Add(":CODGER", ValueDBProperty(vo.CODGER))
        'End If
        'If (Not vo.NOMGER Is Nothing) Then
        '    vo.NOMGER = "%" + UCase(vo.NOMGER) + "%"
        '    sql += " AND (UPPER(Z.NOMGER) LIKE :NOMGER)"
        '    parameterMapper.Parameters.Add(":NOMGER", ValueDBProperty(vo.NOMGER))
        'End If


        'Dim sql As String = <![CDATA[
        '                               SELECT D.CODCLI
        '                                    , D.NOMCLI
        '                                    , A.CODREP
        '                                    , A.NOMREP
        '                                    , Y.CODSUP
        '                                    , Y.NOMSUP
        '                                 FROM MRT.T0100043 D 
        '                                INNER JOIN MRT.T0133774 C ON (D.CODCLI = C.CODCLI)
        '                                INNER JOIN MRT.T0133715 B ON (C.CODTETVND = B.CODTETVND)
        '                                INNER JOIN MRT.T0100116 A ON (B.CODREP = A.CODREP)
        '                                INNER JOIN MRT.T0100124 Y ON (Y.CODSUP = A.CODSUP)
        '                                 LEFT JOIN MRT.RLCACOCMCRGRCLI RACRC ON RACRC.CODCLI = C.CODCLI AND RACRC.CODACOCMC = :CODACOCMC AND RACRC.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC
        '                                WHERE (:NOMCLI IS NULL OR UPPER(D.NOMCLI) LIKE :NOMCLI)
        '                                  AND (:CODCLI IS NULL OR D.CODCLI = :CODCLI)
        '                                  AND (:CODREP IS NULL OR A.CODREP = :CODREP)
        '                                  AND (:NOMREP IS NULL OR UPPER(A.NOMREP) LIKE :NOMREP)
        '                                  AND (:CODSUP IS NULL OR A.CODSUP = :CODSUP)
        '                                  AND (:NOMSUP IS NULL OR UPPER(Y.NOMSUP) LIKE :NOMSUP)
        '                                  AND (RACRC.CODCLI IS NULL)
        '
        '                    ]]>.Value
        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))

        'parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(vo.CODCLI))
        'parameterMapper.Parameters.Add(":NOMCLI", ValueDBProperty(vo.NOMCLI))

        'parameterMapper.Parameters.Add(":CODREP", ValueDBProperty(vo.CODREP))
        'parameterMapper.Parameters.Add(":NOMREP", ValueDBProperty(vo.NOMREP))
        'parameterMapper.Parameters.Add(":NOMSUP", ValueDBProperty(vo.NOMSUP))
        'parameterMapper.Parameters.Add(":CODSUP", ValueDBProperty(vo.CODSUP))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function ListarPublicoAlvoCliente(vo As VO.RelacaoAcaoMecanicaPublicoAlvoCliente) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                       SELECT  
                                                RACRC.CODACOCMC,
                                                RACRC.NUMSEQRGRACOCMC,
                                                RACRC.CODCLI,
                                                C.NOMCLI
                                        FROM MRT.RLCACOCMCRGRCLI RACRC
                                        INNER JOIN MRT.T0100043 C ON (C.CODCLI = RACRC.CODCLI)
                                        WHERE (CODACOCMC IS NULL OR CODACOCMC = :CODACOCMC)
                                        AND (NUMSEQRGRACOCMC IS NULL OR NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(vo.NUMSEQRGRACOCMC))

        If (Not vo.NOMCLI Is Nothing) Then
            vo.NOMCLI = "%" + UCase(vo.NOMCLI) + "%"
            sql += " AND (UPPER(C.NOMCLI) LIKE :NOMCLI)"
            parameterMapper.Parameters.Add(":NOMCLI", ValueDBProperty(vo.NOMCLI))
        End If

        If (Not vo.CODCLI Is Nothing) Then
            sql += " AND (RACRC.CODCLI = : CODCLI)"
            parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(vo.CODCLI))
        End If

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Public Function VerificaSeClienteExiste(ByVal CODCLI As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoCliente)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODCLI
                                              ,NOMCLI
                                        FROM MRT.T0100043                                   
                                        WHERE (CODCLI = :CODCLI)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODCLI", ValueDBProperty(CODCLI))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function
End Class


<TableAttribute("MRT.RLCACOCMCRGRCLI")>
Public Class RelacaoAcaoMecanicaPublicoAlvoClienteDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODCLI As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMCLI As String
    <IgnoreMap>
    Property NOMGER As String
    <IgnoreMap>
    Property NOMSUP As String
    <IgnoreMap>
    Property NOMREP As String

    <IgnoreMap>
    Property CODREP As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODSUP As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODGER As Nullable(Of Decimal)
End Class