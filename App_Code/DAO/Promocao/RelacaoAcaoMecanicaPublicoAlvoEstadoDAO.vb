﻿Imports Core

Public Class RelacaoAcaoMecanicaPublicoAlvoEstadoDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaPublicoAlvoEstadoDAOConfiguration()
    End Sub

    Public Function ListarEstadoUniao(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                          SELECT B.CODESTUNI,
                                               B.NOMESTUNI,
                                               A.CODACOCMC,                                                                                                  
                                               A.NUMSEQRGRACOCMC,
                                               B.CODDIVREG
                                        FROM
                                        MRT.T0100213 B
                                        LEFT JOIN MRT.RLCACOCMCRGRESTUNI A  ON (A.CODESTUNI = B.CODESTUNI) AND A.CODACOCMC = :CODACOCMC 
                                                                                                           AND A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC   
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODESTUNI", "NOMESTUNI", "CODACOCMC", "NUMSEQRGRACOCMC", "CODDIVREG"}))
    End Function

    Public Function ListarEstadoUniaoSelecionados(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaPublicoAlvoEstado)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                                        SELECT CODACOCMC,
                                               NUMSEQRGRACOCMC,
                                               CODESTUNI 
                                        FROM MRT.RLCACOCMCRGRESTUNI                                        
                                        WHERE (:CODACOCMC IS NULL OR :CODACOCMC = CODACOCMC)
                                        AND (:NUMSEQRGRACOCMC IS NULL OR :NUMSEQRGRACOCMC = NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "CODESTUNI"}))
    End Function

End Class

<TableAttribute("MRT.RLCACOCMCRGRESTUNI")>
Public Class RelacaoAcaoMecanicaPublicoAlvoEstadoDAOConfiguration
    Inherits DAOConfiguration
    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODESTUNI As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMESTUNI As String
    <IgnoreMap>
    Property CODDIVREG As String
    <IgnoreMap>
    Property INDSEL As Boolean
End Class

