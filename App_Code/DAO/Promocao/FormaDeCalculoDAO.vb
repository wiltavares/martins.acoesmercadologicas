﻿Imports Core

Public Class FormaDeCalculoDAO
    Inherits DAOBase(Of VO.FormaDeCalculo)

    Public Sub New()
        daoConfiguration = New FormaDeCalculoDAOConfiguration()
    End Sub

    Public Function CarregaDropFormaDeCalculo(ByVal DESEQCCALCTRPMC As String) As List(Of VO.FormaDeCalculo)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        If Not DESEQCCALCTRPMC Is Nothing Then
            DESEQCCALCTRPMC = "%" + UCase(DESEQCCALCTRPMC) + "%"
        End If
        Dim sql As String = <![CDATA[ SELECT 
                                          TIPEQCCALCTRPMC,
                                          DESEQCCALCTRPMC
                                  FROM    MRT.T0107943
                                  WHERE ( :DESEQCCALCTRPMC IS NULL OR DESEQCCALCTRPMC LIKE :DESEQCCALCTRPMC)
                        ]]>.Value
        parameterMapper.Parameters.Add(":DESEQCCALCTRPMC", ValueDBProperty(DESEQCCALCTRPMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function
End Class


<TableAttribute("MRT.T0107943")>
Public Class FormaDeCalculoDAOConfiguration
    Inherits DAOConfiguration

End Class
