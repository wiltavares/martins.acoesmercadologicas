﻿Imports Core

Public Class CriterioApuracaoDAO
    Inherits DAOBase(Of VO.CriterioApuracao)

    Public Sub New()
        daoConfiguration = New CriterioApuracaoDaoConfiguration()
    End Sub
End Class

Public Class CriterioApuracaoDaoConfiguration
    Inherits DAOConfiguration
End Class
