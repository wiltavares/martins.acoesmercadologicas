﻿Imports Core
Public Class TipoCriterioDAO
    Inherits DAOBase(Of VO.TipoCriterio)

    Public Sub New()
        daoConfiguration = New TipoCriterioDAOConfiguration()
    End Sub

    Public Function CarregaDropTipoCriterio(ByVal DESCTRPMC As String, ByVal CODCTRPMC As Decimal?, ByVal TIPEDEPUBALVPMC As Decimal?) As List(Of VO.TipoCriterio)
        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        If Not DESCTRPMC Is Nothing Then
            DESCTRPMC = "%" + UCase(DESCTRPMC) + "%"
        End If

        Dim sql As String = <![CDATA[ SELECT 
                                          TIPEDEPUBALVPMC,
                                          CODCTRPMC,
                                          DESCTRPMC
                                  FROM    MRT.T0107978
                                  WHERE (TIPEDEPUBALVPMC IS NULL OR :TIPEDEPUBALVPMC = TIPEDEPUBALVPMC)                                  
                        ]]>.Value

        'Se CODCTRPMC for diferente de 0 adiciona a condição no WHERE
        If (CODCTRPMC <> 0) Then
            sql = sql + "AND CODCTRPMC = :CODCTRPMC"
            parameterMapper.Parameters.Add(":CODCTRPMC", ValueDBProperty(CODCTRPMC))
        End If

        parameterMapper.Parameters.Add(":TIPEDEPUBALVPMC", ValueDBProperty(TIPEDEPUBALVPMC))
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function
End Class

Public Class TipoCriterioDAOConfiguration
    Inherits DAOConfiguration
End Class


