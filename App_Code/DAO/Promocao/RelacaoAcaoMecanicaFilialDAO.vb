﻿Imports Core

Public Class RelacaoAcaoMecanicaFilialDAO
    Inherits DAOBase(Of VO.RelacaoAcaoMecanicaFilial)

    Public Sub New()
        daoConfiguration = New RelacaoAcaoMecanicaFilialDAOConfiguration()
    End Sub

    Public Function ListFiliaisRelacaoMecanica(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaFilial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                SELECT A.CODACOCMC,
                       A.NUMSEQRGRACOCMC,
                       A.CODFILEMP,
                       C.DESABVFILEMP,
                       A.CODPMC,
                       A.DATECSUSR
                  FROM MRT.RLCACOCMCRGRFIL A
                 INNER JOIN MRT.T0112963 C ON (C.CODFILEMP = A.CODFILEMP)
                 WHERE (A.CODACOCMC IS NULL OR A.CODACOCMC = :CODACOCMC)
                   AND (A.NUMSEQRGRACOCMC IS NULL OR A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "CODFILEMP", "DESABVFILEMP", "CODPMC", "DATECSUSR"}))
    End Function

    Public Function ListarFiliais(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaFilial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
            SELECT B.CODFILEMP,
                   B.DESABVFILEMP,
                   A.CODACOCMC,                                                                                                  
                   A.NUMSEQRGRACOCMC
              FROM MRT.T0112963 B 
              LEFT JOIN MRT.RLCACOCMCRGRFIL A  ON (A.CODFILEMP = B.CODFILEMP) AND A.CODACOCMC = :CODACOCMC 
                                                                              AND A.NUMSEQRGRACOCMC = :NUMSEQRGRACOCMC                                                                           
             WHERE DATDSTFILEMP IS NULL
               AND FLGFILEMPEPD LIKE '%*%'
               AND CODCIDPCOACOCMC > 0
             ORDER BY B.CODFILEMP
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFILEMP", "DESABVFILEMP", "CODACOCMC", "NUMSEQRGRACOCMC"}))
    End Function

    Public Function ListarFiliaisSelecionadas(ByVal CODACOCMC As Decimal, ByVal NUMSEQRGRACOCMC As Decimal) As List(Of VO.RelacaoAcaoMecanicaFilial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[
                SELECT CODACOCMC,
                       NUMSEQRGRACOCMC,
                       CODFILEMP 
                  FROM MRT.RLCACOCMCRGRFIL                                        
                 WHERE (:CODACOCMC IS NULL OR :CODACOCMC = CODACOCMC)
                   AND (:NUMSEQRGRACOCMC IS NULL OR :NUMSEQRGRACOCMC = NUMSEQRGRACOCMC)
                            ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(CODACOCMC))
        parameterMapper.Parameters.Add(":NUMSEQRGRACOCMC", ValueDBProperty(NUMSEQRGRACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODACOCMC", "NUMSEQRGRACOCMC", "CODFILEMP"}))
    End Function
End Class

<TableAttribute("MRT.RLCACOCMCRGRFIL")>
Public Class RelacaoAcaoMecanicaFilialDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <PrimaryKey>
    Property CODFILEMP As Nullable(Of Decimal)
    Property CODPMC As Nullable(Of Decimal)

    Property DATECSUSR As Nullable(Of DateTime)

    'Propriedade extra    
    <IgnoreMap>
    Property DESRGRACOCMC As String
    <IgnoreMap>
    Property DESABVFILEMP As String

End Class

