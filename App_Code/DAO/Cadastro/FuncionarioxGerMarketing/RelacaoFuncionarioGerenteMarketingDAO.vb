﻿Imports Core
Public Class RelacaoFuncionarioGerenteMarketingDAO
    Inherits DAOBase(Of VO.RelacaoFuncionarioGerenteMarketing)

    Public Sub New()
        daoConfiguration = New RelacaoFuncionarioGerenteMarketingDAOConfiguration()
    End Sub

    Public Function ListFuncionarioGerenteMrkt(CODFNCEMTACOCMC As Decimal?, CODFNCGERMKT As Decimal?) As List(Of VO.RelacaoFuncionarioGerenteMarketing)


        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT 
                                            S.CODFNCEMTACOCMC as CODFUNC_EMITENTE
                                          , S.CODFNCEMTACOCMC AS CODFNCEMTACOCMC                  
                                          , COALESCE(U.NomFnc, CADT.NOMANSTCR) AS NOMFUNC_EMITENTE            
                                          , S.CODFNCGERMKT AS CODFNCGERMKT
                                          , S.CODFNCGERMKT as CODFUNC_GER_MTK
                                          , COALESCE(Z.NomFnc, CADTCR.NOMANSTCR) AS NOMUNC_GER_MTK  
                                     FROM MRT.RLCGERMKTEMTACOCMC S 
                                       LEFT JOIN MRT.T0100361 U ON S.CODFNCEMTACOCMC = U.CodFnc
                                       LEFT JOIN MRT.CADANSINFTCR CADT ON S.CODFNCEMTACOCMC = CADT.CODANSTCR
                                       LEFT JOIN MRT.T0100361 Z ON S.CODFNCGERMKT = Z.CodFnc
                                       LEFT JOIN MRT.CADANSINFTCR CADTCR ON S.CODFNCGERMKT = CADTCR.CODANSTCR]]>.Value

        If (CODFNCEMTACOCMC > 0 And (CODFNCGERMKT = 0 Or CODFNCGERMKT Is Nothing)) Then
            sql = sql + " WHERE (S.CODFNCEMTACOCMC = :CODFNCEMTACOCMC)"
            parameterMapper.Parameters.Add(":CODFNCEMTACOCMC", CODFNCEMTACOCMC)
        ElseIf (CODFNCGERMKT > 0 And (CODFNCEMTACOCMC = 0 Or CODFNCEMTACOCMC Is Nothing)) Then
            sql = sql + " WHERE (S.CODFNCGERMKT = :CODFNCGERMKT)"
            parameterMapper.Parameters.Add(":CODFNCGERMKT", CODFNCGERMKT)
        Else
            sql = sql + " WHERE (S.CODFNCEMTACOCMC = :CODFNCEMTACOCMC)"
            sql = sql + "  AND (S.CODFNCGERMKT = :CODFNCGERMKT)"
            parameterMapper.Parameters.Add(":CODFNCEMTACOCMC", CODFNCEMTACOCMC)
            parameterMapper.Parameters.Add(":CODFNCGERMKT", CODFNCGERMKT)
        End If
        sql = sql + " ORDER BY COALESCE(U.NomFnc, CADT.NOMANSTCR) ASC"

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' CODFNCEMTACOCMC
    ''' CODFNCGERMKT
    ''' Apaga uma nova relação de funcionario com gerente mrkt
    ''' </summary>
    Public Sub deleteRelacaoFornGerMrkt(CODFNCEMTACOCMC As Decimal, CODFNCGERMKT As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE MRT.RLCGERMKTEMTACOCMC WHERE CODFNCEMTACOCMC = :CODFNCEMTACOCMC AND CODFNCGERMKT = :CODFNCGERMKT]]>.Value

        parameterMapper.Parameters.Add(":CODFNCEMTACOCMC", CODFNCEMTACOCMC)
        parameterMapper.Parameters.Add(":CODFNCGERMKT", CODFNCGERMKT)

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub

    'sCmdSql.Append(" DELETE MRT.RLCGERMKTEMTACOCMC ")
    '        sCmdSql.Append(" WHERE CODFNCEMTACOCMC = " & CodFnc.ToString() & " ")
    '        sCmdSql.Append(" AND CODFNCGERMKT = " & CodFncGerMrk.ToString() & " ")


    ''' <summary>
    ''' CODFNCEMTACOCMC
    ''' CODFNCGERMKT
    ''' Insere uma nova relação de funcionario com gerente mrkt
    ''' </summary>
    Public Sub insereRelacaoFornGerMrkt(CODFNCEMTACOCMC As Decimal, CODFNCGERMKT As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[ INSERT INTO MRT.RLCGERMKTEMTACOCMC VALUES( :CODFNCGERMKT, :CODFNCEMTACOCMC ) ]]>.Value

        parameterMapper.Parameters.Add(":CODFNCGERMKT", CODFNCGERMKT)
        parameterMapper.Parameters.Add(":CODFNCEMTACOCMC", CODFNCEMTACOCMC)

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub

    'sCmdSql.Append(" INSERT INTO MRT.RLCGERMKTEMTACOCMC ")
    '        sCmdSql.Append(" VALUES (" & CodFncGerMrk.ToString() & ", " & CodFnc.ToString() & ")")


    'Public Function DelRlcFncGerMrk(ByVal CodFnc As Integer, _
    '                                ByVal CodFncGerMrk As Integer, _
    '                                ByVal oObeCnx As IAU013.UO_IAUCnxAcsDdo) As Integer
    '    '
    '    Dim oObeAcsDdo As IAU013.UO_IAUAcsDdo
    '    Dim sCmdSql As StringBuilder
    '    '
    '    Try
    '        sCmdSql = New StringBuilder
    '        '
    '        sCmdSql.Append(" DELETE MRT.RLCGERMKTEMTACOCMC ")
    '        sCmdSql.Append(" WHERE CODFNCEMTACOCMC = " & CodFnc.ToString() & " ")
    '        sCmdSql.Append(" AND CODFNCGERMKT = " & CodFncGerMrk.ToString() & " ")
    '        '
    '        oObeAcsDdo = New IAU013.UO_IAUAcsDdo(oObeCnx, _
    '                                             sCmdSql.ToString)
    '        oObeAcsDdo.ExcCmdSql(DelRlcFncGerMrk)
    '        '
    '    Catch oEcc As Exception
    '        '
    '        MsgBox("Ocorreu um erro. QUERY: " & sCmdSql.ToString() & vbNewLine & oEcc.Message())
    '        Publish(oEcc)
    '        Throw
    '        '
    '    End Try
    'End Function
    ''

    'Public Function CnsFncGerMrk(ByVal CodFnc As Integer, _
    '                            ByVal CodFncGerMrk As Integer, _
    '                            ByVal oObeCnx As IAU013.UO_IAUCnxAcsDdo) As DataSet
    '    '
    '    Dim oObeAcsDdo As IAU013.UO_IAUAcsDdo
    '    Dim sCmdSql, sCmdWHERE, sCmdFlt As String
    '    '
    '    Try
    '        '
    '        sCmdSql = " SELECT DISTINCT S.CODFNCEMTACOCMC as CodEdeEmiAcoCmc, " & _
    '        " U.NomFnc as NomUsrEmiAcoCmc, " & _
    '        "S.CODFNCGERMKT as CodEdeGerMkt, Z.NomFnc as NomUsrGerMrk "
    '        '
    '        sCmdSql &= " FROM MRT.RLCGERMKTEMTACOCMC S "
    '        sCmdSql &= ", MRT.T0100361 U "
    '        sCmdSql &= ", MRT.T0100361 Z "
    '        '
    '        sCmdWHERE = " WHERE "
    '        '
    '        sCmdFlt &= "AND U.CodFnc = S.CODFNCEMTACOCMC "
    '        sCmdFlt &= "AND Z.CodFnc = S.CODFNCGERMKT "
    '        '
    '        If CodFnc <> 0 Then
    '            sCmdFlt &= "AND S.CODFNCEMTACOCMC = " & CodFnc.ToString() & " "
    '        End If
    '        '
    '        If CodFncGerMrk <> 0 Then
    '            sCmdFlt &= "AND S.CODFNCGERMKT = " & CodFncGerMrk.ToString() & " "
    '        End If
    '        '
    '        sCmdFlt = sCmdFlt.Remove(0, 3)
    '        '
    '        sCmdSql = sCmdSql & sCmdWHERE & sCmdFlt

    '        oObeAcsDdo = New IAU013.UO_IAUAcsDdo(oObeCnx, sCmdSql)
    '        oObeAcsDdo.ExcCmdSql(CnsFncGerMrk)
    '        '
    '    Catch oEcc As Exception
    '        '
    '        MsgBox("Ocorreu um erro. QUERY: " & sCmdSql.ToString() & vbNewLine & oEcc.Message())
    '        Publish(oEcc)
    '        Throw
    '        '
    '    End Try
    'End Function

End Class


<TableAttribute("MRT.RLCGERMKTEMTACOCMC")>
Public Class RelacaoFuncionarioGerenteMarketingDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODFNCGERMKT As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODFNCEMTACOCMC As Nullable(Of Decimal)

    <IgnoreMap>
    Property CODFUNC_GER_MTK As Nullable(Of Decimal)

    <IgnoreMap>
    Property NOMUNC_GER_MTK As String

    <IgnoreMap>
    Property CODFUNC_EMITENTE As Nullable(Of Decimal)

    <IgnoreMap>
    Property NOMFUNC_EMITENTE As String

End Class

