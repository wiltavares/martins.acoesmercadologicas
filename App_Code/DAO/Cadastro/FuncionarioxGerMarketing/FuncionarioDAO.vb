﻿Imports Core
Public Class FuncionarioDAO
    Inherits DAOBase(Of VO.Funcionario)

    Public Sub New()
        daoConfiguration = New FuncionarioDAOConfiguration()
    End Sub


    Public Function ListFuncionario(vo As VO.Funcionario) As List(Of VO.Funcionario)


        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT  
                                              COALESCE(C.CODFNC, CADT.CODANSTCR) AS CODFNC
                                            , COALESCE(C.NOMFNC, CADT.NOMANSTCR) AS NOMFNC
                                     FROM MRT.T0118430 A
                                       LEFT JOIN MRT.T0100361 C ON A.CODFNCCRIACOCMC = C.CODFNC 
                                       LEFT JOIN MRT.CADANSINFTCR CADT ON A.CODFNCCRIACOCMC = CADT.CODANSTCR   
                                     WHERE A.DATFIMACOCMC <> TO_DATE('0001-01-01','YYYY-MM-DD') 
                                     ORDER BY CODFNC]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function ListFuncionarioPromocao(vo As VO.Funcionario) As List(Of VO.Funcionario)


        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT  
                                              COALESCE(C.CODFNC, CADT.CODANSTCR) AS CODFNC
                                            , COALESCE(C.NOMFNC, CADT.NOMANSTCR) AS NOMFNC
                                     FROM mrt.T0107935 A
                                       LEFT JOIN MRT.T0100361 C ON A.CODFNCGRCPMC = C.CODFNC 
                                       LEFT JOIN MRT.CADANSINFTCR CADT ON A.CODFNCGRCPMC = CADT.CODANSTCR   
                                       WHERE A.CODFNCGRCPMC <> 0
                                     ORDER BY CODFNC]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function GetSelectFuncionario(lstCodFnc As List(Of Decimal)) As List(Of VO.Funcionario)


        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT   CODFNC, 
                                              NOMFNC 
                                       FROM  MRT.T0100361
                                      WHERE CODFNC IN (]]>.Value

        sql = sql + String.Join(",", lstCodFnc)
        sql = sql + ")"

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODFNC", "NOMFNC"}))

    End Function

    Public Function ListFuncionarioFilial(vo As VO.Funcionario) As List(Of VO.Funcionario)


        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT
                                            COALESCE(FNC.CODFNC, C.CODANSTCR) AS CODFNC
                                          , COALESCE(FNC.NOMFNC, C.NOMANSTCR) AS NOMFNC
                                          , FIL.CODFILEMP
                                          , FIL.NOMFILEMP
                                     FROM MRT.T0104596 A 
                                       LEFT JOIN MRT.T0100361 FNC ON A.CODFNC = FNC.CODFNC
                                                                  AND FNC.DATDEMFNC IS NULL
                                       LEFT JOIN MRT.CADANSINFTCR C ON A.CODFNC = C.CODANSTCR 
                                       LEFT JOIN MRT.T0112963 FIL ON FIL.CODFILEMP = 1 
                                                                  AND FIL.FLGFILEMPEPD = '*' 
                                                                  AND FIL.CODCENREG IS NOT NULL
                                     WHERE ( :NOMFNC IS NULL OR FNC.NOMFNC LIKE UPPER(:NOMFNC) OR C.NOMANSTCR LIKE UPPER(:NOMFNC))
                                       AND ( :CODFNC IS NULL OR A.CODFNC = :CODFNC)
                                     ORDER BY NOMFNC]]>.Value
        Dim nomfuncLike = ""
        If (vo.NOMFNC <> Nothing) Then
            nomfuncLike = "%" + vo.NOMFNC + "%"

        End If
        parameterMapper.Parameters.Add(":NOMFNC", ValueDBProperty(nomfuncLike))
        parameterMapper.Parameters.Add(":CODFNC", ValueDBProperty(vo.CODFNC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function BuscaFuncionarios(vo As VO.Funcionario) As List(Of VO.Funcionario)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT  
                                              COALESCE(FNC.CODFNC, C.CODANSTCR) AS CODFNC
                                            , COALESCE(FNC.NOMFNC, C.NOMANSTCR) AS NOMFNC
                                     FROM MRT.T0104596 A
                                       LEFT JOIN MRT.T0100361 FNC ON A.CODFNC = FNC.CODFNC
                                                                  AND FNC.DATDEMFNC IS NULL
                                       LEFT JOIN MRT.CADANSINFTCR C ON A.CODFNC = C.CODANSTCR]]>.Value

        Dim nomfuncLike = "%" + vo.NOMFNC + "%"
        If (vo.CODFNC <> 0 And vo.NOMFNC <> "") Then
            sql = sql + " WHERE (FNC.NOMFNC LIKE UPPER(:NOMFNC) OR C.NOMANSTCR LIKE UPPER(:NOMFNC))"
            sql = sql + " AND A.CODFNC = :CODFNC"
            sql = sql + " ORDER BY NOMFNC"
            parameterMapper.Parameters.Add(":CODFNC", ValueDBProperty(vo.CODFNC))
            parameterMapper.Parameters.Add(":NOMFNC", ValueDBProperty(nomfuncLike))
        ElseIf (vo.CODFNC <> 0) Then
            sql = sql + " WHERE A.CODFNC = :CODFNC"
            sql = sql + " ORDER BY NOMFNC"
            parameterMapper.Parameters.Add(":CODFNC", ValueDBProperty(vo.CODFNC))
        ElseIf (vo.NOMFNC <> "") Then
            sql = sql + " WHERE (FNC.NOMFNC LIKE UPPER(:NOMFNC) OR C.NOMANSTCR LIKE UPPER(:NOMFNC))"
            sql = sql + " ORDER BY NOMFNC"
            parameterMapper.Parameters.Add(":NOMFNC", ValueDBProperty(nomfuncLike))
        Else
            sql = sql + " WHERE (FNC.NOMFNC LIKE UPPER(:NOMFNC) OR C.NOMANSTCR LIKE UPPER(:NOMFNC))"
            sql = sql + " ORDER BY NOMFNC"
            parameterMapper.Parameters.Add(":NOMFNC", ValueDBProperty(nomfuncLike))
        End If
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function CnsFluApvGerMrk(ByVal CodFncEmtAcoCmc As Decimal, ByVal CodAcoCmc As Decimal) As List(Of VO.Funcionario)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT COALESCE(O.CODFNC, CADTCR.CODANSTCR, S.CODFNC, CADT.CODANSTCR) AS CODFNC 
                                          , COALESCE(O.NOMFNC, CADTCR.NOMANSTCR, S.NOMFNC, CADT.NOMANSTCR) AS NOMFNC 
                                       FROM MRT.RLCGERMKTEMTACOCMC A 
                                      INNER JOIN  MRT.T0100361 S ON (A.CodFncGerMkt = S.CodFnc) 
                                       LEFT JOIN MRT.CADANSINFTCR CADT ON A.CodFncGerMkt = CADT.CODANSTCR
                                       LEFT JOIN MRT.T0155581 L ON (L.CODACOCMC = :CODACOCMC) 
                                       LEFT JOIN MRT.RLCTIPEVTAPVEXA P ON (P.TIPEVTACOCMC = L.TIPEVTACOCMC AND P.CODTIPAPV = 7 AND S.CODFNC = P.CODEDEAPV) 
                                       LEFT JOIN MRT.T0100361 O ON (O.CODFNC = P.CODEDEAPVEXA) 
                                       LEFT JOIN MRT.CADANSINFTCR CADTCR ON P.CODEDEAPVEXA = CADTCR.CODANSTCR 
                                      WHERE A.CODFNCEMTACOCMC = :CODFNCEMTACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CodAcoCmc)
        parameterMapper.Parameters.Add(":CODFNCEMTACOCMC", CodFncEmtAcoCmc)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function VldItePmc(ByVal CodAcoCmc As Decimal) As Decimal?

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT COUNT(a.CODACOCMC) AS CODACOCMC 
                                       FROM  MRT.t0138776 a
                                     INNER JOIN  MRT.t0118430 b ON A.CODACOCMC = B.CODACOCMC
                                      WHERE ((A.CODOPEFSCDSNACOCMC NOT IN (2,4)
                                                  AND B.INDACOCMCCPL = 1
                                             ) OR B.INDACOCMCCPL = 0)
                                        AND A.CODACOCMC = :CODACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CodAcoCmc)
        Return connector.Execute(sql, parameterMapper)

    End Function

    Public Function CnsFluApvGerCtgDirCmp(ByVal CodSisInf As Decimal, ByVal CodAcoCmc As Decimal, ByVal NumSeqNivApv As Decimal, ByVal CodTipApvGerCtg As Decimal, _
                                    ByVal CodTipApvDirCmp As Decimal, ByVal TipIdtCtnFluCmp As Decimal) As List(Of VO.Funcionario)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODACOCMC as ACAO, 
                                            NVL(O.CODFNC, I.CODFNC) as CODGERCATEGORIA, 
                                            NVL(O.NOMFNC, I.NOMFNC) as NOMGERCATEGORIA, 
                                            NVL(N.CODFNC, S.CODFNC) as CODDIRCOMPRAS, 
                                            NVL(N.NOMFNC, S.NOMFNC) as NOMDIRCOMPRAS, 
                                            CASE WHEN NVL(Y.VLRCTN, 0) <= SUM(A.VLRPTCFRNACOCMC) THEN 1 ELSE 0 END as FLGAPVDIRCMP, 
                                            NVL(Y.VLRCTN, 0) as VLRALC, 
                                            SUM(NVL(A.VLRPTCFRNACOCMC, 0)) as PTCDIRCATEGORIA 
                                     FROM MRT.T0138768 A 
                                       INNER JOIN MRT.T0100426 B ON (A.CODFRN = B.CODFRN AND B.DATDSTDIVFRN IS NULL) 
                                       INNER JOIN MRT.T0113625 C ON (B.CODCPR = C.CODCPR AND C.DATDSTCPR IS NULL)
                                       INNER JOIN MRT.T0118430 Z ON (Z.CODACOCMC = A.CODACOCMC )
                                       INNER JOIN MRT.T0153541 X ON (B.CODFRN = X.CODFRN AND X.CODFILEMP = ( CASE WHEN Z.CODFILEMPORIVBA = 0 THEN 1 
                                                                                                             ELSE Z.CODFILEMPORIVBA END) AND X.INDCPRPCPFRN  = 1 )  
                                       INNER JOIN MRT.T0118570 D ON (D.CODGERPRD = C.CODGERPRD) 
                                       INNER JOIN MRT.T0123183 F ON (F.CODDRTCMP = D.CODDRTCMP) 
                                       INNER JOIN MRT.T0100981 K ON (K.CODEMP = 1 AND K.CODCPR = F.CODCPR) 
                                       INNER JOIN MRT.T0100361 S ON (S.CODFNC = K.CODFNC) 
                                       INNER JOIN MRT.T0100981 H ON (H.CODEMP = 1  AND H.CODCPR = X.CODCPR) 
                                       INNER JOIN MRT.T0100361 I ON (I.CODFNC = H.CODFNC) 
                                       INNER JOIN MRT.T0161581 V ON (V.CODSISINF = :CODSISINF AND V.NUMSEQNIVAPV = :NUMSEQNIVAPV AND V.CODTIPAPV = :CODTIPAPV) 
                                       LEFT JOIN MRT.CADCTNSISFLUAPV Y  ON ( Y.CODSISINF = V.CODSISINF 
                                                                        AND ( Y.CODTIPAPV IN ( CASE WHEN H.CODCPR = H.CODGERPRD THEN 5 ELSE 2 END ))
                                                                        AND   Y.TIPIDTCTNFLUCMP = :TIPIDTCTNFLUCMP 
                                                                        AND   Y.CODEDEAPV = I.CODFNC ) 
                                       INNER JOIN (SELECT DISTINCT X.TIPEVTACOCMC, X.CODACOCMC FROM MRT.T0155581 X WHERE X.CODACOCMC = :CODACOCMC) L  ON (A.CODACOCMC = L.CODACOCMC)
                                       LEFT JOIN MRT.RLCTIPEVTAPVEXA P ON (L.TIPEVTACOCMC = P.TIPEVTACOCMC AND P.CODTIPAPV IN ( 2, 5 ) AND I.CODFNC = P.CODEDEAPV) 
                                       LEFT JOIN MRT.T0100361 O ON (O.CODFNC = P.CODEDEAPVEXA) 
                                       LEFT JOIN MRT.RLCTIPEVTAPVEXA M ON (L.TIPEVTACOCMC = M.TIPEVTACOCMC AND M.CODTIPAPV = :CODTIPAPVDIR AND S.CODFNC = M.CODEDEAPV) 
                                       LEFT JOIN MRT.T0100361 N ON (N.CODFNC = M.CODEDEAPVEXA) 
                                     WHERE A.CODACOCMC = :CODACOCMC
                                     GROUP BY A.CODACOCMC, I.CODFNC, I.NOMFNC, O.CODFNC, O.NOMFNC, S.CODFNC, S.NOMFNC, N.CODFNC, N.NOMFNC, Y.VLRCTN 
                                     UNION 
                                     SELECT A.CODACOCMC as ACAO, 
                                            NVL(O.CODFNC, I.CODFNC) as CODGERCATEGORIA, 
                                            NVL(O.NOMFNC, I.NOMFNC) as NOMGERCATEGORIA, 
                                            NVL(N.CODFNC, S.CODFNC) as CODDIRCOMPRAS, 
                                            NVL(N.NOMFNC, S.NOMFNC) as NOMDIRCOMPRAS, 
                                            CASE WHEN NVL(Y.VLRCTN, 0) <= SUM(G.VLRUNTITEOCDACOCMC * G.QDEITEOCDACOCMC) THEN 1 ELSE 0 END as FLGAPVDIRCMP, 
                                            NVL(Y.VLRCTN, 0) as VLRALC, 
                                            SUM(NVL(G.VLRUNTITEOCDACOCMC * G.QDEITEOCDACOCMC, 0)) as PTCDIRCATEGORIA 
                                     FROM MRT.T0138768 A 
                                       INNER JOIN MRT.T0138776 G ON (A.CODACOCMC = G.CODACOCMC AND G.CODMER > 0) 
                                       INNER JOIN MRT.T0100086 E ON (G.CODMER = E.CODMER AND E.CODEMP = 1)
                                       INNER JOIN MRT.T0100426 B ON (E.CODFRNPCPMER =  B.CODFRN AND B.DATDSTDIVFRN IS NULL) 
                                       INNER JOIN MRT.T0113625 C ON (B.CODCPR = C.CODCPR AND C.DATDSTCPR IS NULL)
                                       INNER JOIN MRT.T0118430 Z ON (Z.CODACOCMC = A.CODACOCMC )
                                       INNER JOIN MRT.T0153541 X ON (B.CODFRN = X.CODFRN AND X.CODFILEMP = ( CASE WHEN Z.CODFILEMPORIVBA = 0 THEN 1
                                                                                                             ELSE Z.CODFILEMPORIVBA END) AND X.INDCPRPCPFRN  = 1 )
                                       INNER JOIN MRT.T0118570 D ON (D.CODGERPRD = C.CODGERPRD) 
                                       INNER JOIN MRT.T0123183 F ON (F.CODDRTCMP = D.CODDRTCMP) 
                                       INNER JOIN MRT.T0100981 K ON (K.CODEMP = 1 AND K.CODCPR = F.CODCPR) 
                                       INNER JOIN MRT.T0100361 S ON (S.CODFNC = K.CODFNC) 
                                       INNER JOIN MRT.T0100981 H ON (H.CODEMP = 1  AND H.CODCPR = X.CODCPR) 
                                       INNER JOIN MRT.T0100361 I ON (I.CODFNC = H.CODFNC) 
                                       INNER JOIN MRT.T0161581 V ON (V.CODSISINF = :CODSISINF AND V.NUMSEQNIVAPV = :NUMSEQNIVAPV  AND V.CODTIPAPV = :CODTIPAPV)
                                       LEFT JOIN MRT.CADCTNSISFLUAPV Y ON ( Y.CODSISINF = V.CODSISINF 
                                                                       AND ( Y.CODTIPAPV IN ( CASE WHEN H.CODCPR = H.CODGERPRD THEN 5 ELSE 2 END ))
                                                                       AND   Y.TIPIDTCTNFLUCMP = :TIPIDTCTNFLUCMP 
                                                                       AND   Y.CODEDEAPV = I.CODFNC ) 
                                       INNER JOIN (SELECT DISTINCT X.TIPEVTACOCMC, X.CODACOCMC FROM MRT.T0155581 X WHERE X.CODACOCMC = :CODACOCMC) L  ON (A.CODACOCMC = L.CODACOCMC)
                                       LEFT JOIN MRT.RLCTIPEVTAPVEXA P ON (L.TIPEVTACOCMC = P.TIPEVTACOCMC AND P.CODTIPAPV IN ( 2 , 5) AND I.CODFNC = P.CODEDEAPV)
                                       LEFT JOIN MRT.T0100361 O ON (O.CODFNC = P.CODEDEAPVEXA) 
                                       LEFT JOIN MRT.RLCTIPEVTAPVEXA M ON (L.TIPEVTACOCMC = M.TIPEVTACOCMC AND M.CODTIPAPV = :CODTIPAPVDIR AND S.CODFNC = M.CODEDEAPV) 
                                       LEFT JOIN MRT.T0100361 N ON (N.CODFNC = M.CODEDEAPVEXA) 
                                     WHERE A.CODACOCMC = :CODACOCMC
                                       AND A.CODFRN <> 999999 
                                     GROUP BY A.CODACOCMC, I.CODFNC, I.NOMFNC, O.CODFNC, O.NOMFNC, S.CODFNC, S.NOMFNC, N.CODFNC, N.NOMFNC, Y.VLRCTN
                                     ORDER BY ACAO, CODGERCATEGORIA, NOMGERCATEGORIA, CODDIRCOMPRAS, NOMDIRCOMPRAS ]]>.Value

        parameterMapper.Parameters.Add(":CODSISINF", CodSisInf)
        parameterMapper.Parameters.Add(":CODACOCMC", CodAcoCmc)
        parameterMapper.Parameters.Add(":NUMSEQNIVAPV", NumSeqNivApv)
        parameterMapper.Parameters.Add(":CODTIPAPV", CodTipApvGerCtg)
        parameterMapper.Parameters.Add(":CODTIPAPVDIR", CodTipApvDirCmp)
        parameterMapper.Parameters.Add(":TIPIDTCTNFLUCMP", TipIdtCtnFluCmp)

        Dim properties = {"ACAO", "CODGERCATEGORIA", "NOMGERCATEGORIA", "CODDIRCOMPRAS", "NOMDIRCOMPRAS", "FLGAPVDIRCMP", "VLRALC", "PTCDIRCATEGORIA"}
        Return connector.SelectTList(sql, parameterMapper, DAORowMapper(Of VO.Funcionario).RowMapperOnlyProperties(properties))

    End Function

    Public Function selecionaGerente(codGerente As Decimal, nomGerente As String) As List(Of VO.Funcionario)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT 
                                            COALESCE(a.CODFNC, CADT.CODANSTCR) AS CODFNC,
                                            COALESCE(a.NOMFNC, CADT.NOMANSTCR) AS NOMFNC
                                     FROM MRT.T0100361 a
                                      INNER JOIN MRT.RLCGERMKTEMTACOCMC b ON a.CODFNC = b.CODFNCGERMKT  
                                      LEFT JOIN MRT.CADANSINFTCR CADT ON a.CODFNC = CADT.CODANSTCR 
                                     WHERE A.DATDEMFNC IS NULL ]]>.Value

        If (Not IsNothing(codGerente) AndAlso codGerente <> 0) Then
            sql += " AND a.CODFNC = :CODGERENTE"
            parameterMapper.Parameters.Add(":CODGERENTE", ValueDBProperty(codGerente))
        End If
        If (Not String.IsNullOrWhiteSpace(nomGerente)) Then
            sql += "AND UPPER(a.NOMFNC) LIKE UPPER(:NOMGERENTE)"
            Dim nome = "%" + nomGerente + "%"
            parameterMapper.Parameters.Add(":NOMGERENTE", ValueDBProperty(nome))
        End If

        sql += <![CDATA[             UNION ALL
                                         SELECT DISTINCT
                                                COALESCE(FNC.CODFNC, CADT.CODANSTCR) AS CODFNC,
                                                COALESCE(FNC.NOMFNC, CADT.NOMANSTCR) AS NOMFNC
                                         FROM MRT.CADDPTMKT DPT 
                                           INNER JOIN MRT.T0100361 FNC ON TRIM(FNC.CODCENCST) = TRIM(TO_CHAR(DPT.CODCENCST)) 
                                           LEFT JOIN MRT.CADANSINFTCR CADT ON FNC.CODFNC = CADT.CODANSTCR 
                                         WHERE FNC.DATDEMFNC IS NULL 
                                           AND FNC.CODCGRFNC IN (102855, 102857, 102856, 105175)]]>.Value

        If (Not IsNothing(codGerente) AndAlso codGerente <> 0) Then
            sql += " AND FNC.CODFNC = :CODGERENTE"
        End If
        If (Not String.IsNullOrWhiteSpace(nomGerente)) Then
            sql += " AND UPPER(FNC.NOMFNC) LIKE UPPER(:NOMGERENTE)"
        End If

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))
    End Function

    Function buscaGerenteDefault(acao As Integer, funcionario As Integer) As VO.Funcionario

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                            COALESCE(O.CODFNC, CADTCR.CODANSTCR, S.CODFNC, CADT.CODANSTCR) AS CODFNC
                                          , COALESCE(O.NOMFNC, CADTCR.NOMANSTCR, S.NOMFNC, CADT.NOMANSTCR) AS NOMFNC 
                                     FROM MRT.RLCGERMKTEMTACOCMC A 
                                       INNER JOIN  MRT.T0100361 S ON (A.CODFNCGERMKT = S.CODFNC) 
                                       LEFT JOIN MRT.CADANSINFTCR CADT ON A.CODFNCGERMKT = CADT.CODANSTCR
                                       LEFT JOIN MRT.T0155581 L On (L.CODACOCMC = :CODACAO) 
                                       LEFT JOIN MRT.RLCTIPEVTAPVEXA P ON (P.TIPEVTACOCMC = L.TIPEVTACOCMC 
                                                                       AND P.CODTIPAPV = 7 
                                                                       AND S.CODFNC = P.CODEDEAPV) 
                                       LEFT JOIN MRT.T0100361 O ON (O.CODFNC = P.CODEDEAPVEXA) 
                                       LEFT JOIN MRT.CADANSINFTCR CADTCR ON P.CODEDEAPVEXA = CADTCR.CODANSTCR
                                     WHERE A.CODFNCEMTACOCMC = :CODFNC]]>.Value

        parameterMapper.Parameters.Add(":CODACAO", ValueDBProperty(acao))
        parameterMapper.Parameters.Add(":CODFNC", ValueDBProperty(funcionario))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql)).FirstOrDefault
    End Function

    Public Function BuscaFuncionariosETerceiros(vo As VO.Funcionario) As VO.Funcionario

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                               COALESCE(FNC.CODFNC, C.CODANSTCR) AS CODFNC
                                             , COALESCE(FNC.NOMFNC, C.NOMANSTCR) AS NOMFNC
                                     FROM MRT.T0104596 A
                                       LEFT JOIN MRT.T0100361 FNC ON A.CODFNC = FNC.CODFNC
                                                                  AND FNC.DATDEMFNC IS NULL
                                       LEFT JOIN MRT.CADANSINFTCR C ON A.CODFNC = C.CODANSTCR
                                       WHERE A.CODFNC = :CODFNC]]>.Value

        parameterMapper.Parameters.Add(":CODFNC", ValueDBProperty(vo.CODFNC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql)).FirstOrDefault

    End Function

End Class


<TableAttribute("MRT.T0100361")>
Public Class FuncionarioDAOConfiguration
    Inherits DAOConfiguration


    <DateInsert>
    <[Readonly](False, True)>
    Property DATADSFNC As DateTime

    <DateInsert>
    <[Readonly](False, True)>
    Property DATDEMFNC As DateTime

    <DateInsert>
    <[Readonly](False, True)>
    Property DATNSC As DateTime

    <DateInsert>
    <[Readonly](False, True)>
    Property DATVLDCARCCP As DateTime

    <DateInsert>
    <[Readonly](False, True)>
    Property DATVLDCARRLP As DateTime

    <DateInsert>
    <[Readonly](False, True)>
    Property DATPRVDEMFNC As DateTime

    <[Readonly](False, True)>
    Property NUMCPF As String

    <DateInsert>
    <[Readonly](False, True)>
    Property DATALTCADFNC As DateTime
    <IgnoreMap>
    Property CODIGO_NOME As String

    <IgnoreMap>
    Property PTCDIRCATEGORIA As Nullable(Of Decimal)

    <IgnoreMap>
    Property ACAO As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODGERCATEGORIA As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMGERCATEGORIA As String
    <IgnoreMap>
    Property CODDIRCOMPRAS As Nullable(Of Decimal)
    <IgnoreMap>
    Property NOMDIRCOMPRAS As String
    <IgnoreMap>
    Property FLGAPVDIRCMP As Integer
    <IgnoreMap>
    Property VLRALC As Nullable(Of Decimal)
End Class
