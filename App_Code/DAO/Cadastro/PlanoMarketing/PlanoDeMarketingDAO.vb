﻿Imports Core
Public Class PlanoDeMarketingDAO
    Inherits DAOBase(Of VO.PlanoDeMarketing)

    Public Sub New()
        daoConfiguration = New PlanoDeMarketingDAOConfiguration()
    End Sub

    ''' <summary>
    ''' Inserir plano de marketing a partir da relaca acao comercial plano marketing
    ''' </summary>
    ''' <param name="planoMarketing"></param>
    ''' <returns></returns>
    Public Function InserirPlanoDeMarketingDaAcao(planoMarketing As VO.RelacaoAcaoComercialxPlanoMarketing) As Object

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.T0155549(
                                            TIPEVTACOCMC,
                                            CODEVTACOCMC,
                                            ANOMESREF,
                                            CODUNDESRNGC,
                                            VLROCDPLNMKT,
                                            VLRRSVPLNMKT,
                                            VLRUTZPLNMKT,
                                            VLROCDCTTPLNMKT,
                                            VLROCDEXACTTPLNMKT)
                                    VALUES( :TIPEVTACOCMC,
                                            :CODEVTACOCMC,
                                            :ANOMESREF,
                                            :CODUNDESRNGC,
                                            0.0,
                                            :VLRRSVPLNMKT,
                                            0.0,
                                            0.0,
                                            0.0)]]>.Value

        parameterMapper.Parameters.Add(":TIPEVTACOCMC", planoMarketing.TIPEVTACOCMC)
        parameterMapper.Parameters.Add(":CODEVTACOCMC", planoMarketing.CODEVTACOCMC)
        parameterMapper.Parameters.Add(":ANOMESREF", planoMarketing.ANOMESREF)
        parameterMapper.Parameters.Add(":CODUNDESRNGC", planoMarketing.CODUNDESRNGC)
        parameterMapper.Parameters.Add(":VLRRSVPLNMKT", planoMarketing.VLRRSVPLNMKT)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

    ''' <summary>
    ''' Realiza o provisionamento do valor da participação do fornecedor no plano de marketing quando a ação é enviada para aprovação
    ''' </summary>
    ''' <param name="TIPEVTACOCMC">Tipo de evento</param>
    ''' <param name="CODEVTACOCMC">Código do evento</param>
    ''' <param name="ANOMESREF">Ano Mes de referência</param>
    ''' <param name="CODUNDESRNGC">Código da unidade</param>
    ''' <param name="VLRRSVPLNMKT">Valor a ser provisionado no plano de marketing</param>
    Public Sub ProvisionaValorReservadoPlanoMarketing(ByVal TIPEVTACOCMC As Decimal, ByVal CODEVTACOCMC As Decimal, ByVal ANOMESREF As Decimal, ByVal CODUNDESRNGC As Decimal, ByVal VLRRSVPLNMKT As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0155549 
                                        SET VLRRSVPLNMKT = VLRRSVPLNMKT +  :VLRRSVPLNMKT
                                      WHERE TIPEVTACOCMC = :TIPEVTACOCMC
                                        AND CODEVTACOCMC = :CODEVTACOCMC
                                        AND ANOMESREF  = :ANOMESREF
                                        AND CODUNDESRNGC  = :CODUNDESRNGC]]>.Value

        parameterMapper.Parameters.Add(":TIPEVTACOCMC", TIPEVTACOCMC)
        parameterMapper.Parameters.Add(":CODEVTACOCMC", CODEVTACOCMC)
        parameterMapper.Parameters.Add(":ANOMESREF", ANOMESREF)
        parameterMapper.Parameters.Add(":CODUNDESRNGC", CODUNDESRNGC)
        parameterMapper.Parameters.Add(":VLRRSVPLNMKT", VLRRSVPLNMKT)

        connector.ExecuteScalar(sql, parameterMapper)

    End Sub



    ''' <summary>
    ''' Realiza o estorno do valor provisionado no plano de marketing quando a ação e reprovada
    ''' </summary>
    ''' <param name="TIPEVTACOCMC">Tipo de evento</param>
    ''' <param name="CODEVTACOCMC">Código do evento</param>
    ''' <param name="ANOMESREF">Ano Mes de referência</param>
    ''' <param name="CODUNDESRNGC">Código da unidade</param>
    ''' <param name="VLRRSVPLNMKT">Valor a ser estornado no plano de marketing</param>
    Public Sub EstornaValorReservadoPlanoMarketing(ByVal TIPEVTACOCMC As Decimal, ByVal CODEVTACOCMC As Decimal, ByVal ANOMESREF As Decimal, ByVal CODUNDESRNGC As Decimal, ByVal VLRRSVPLNMKT As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0155549 
                                        SET VLRRSVPLNMKT = VLRRSVPLNMKT - :VLRRSVPLNMKT
                                      WHERE TIPEVTACOCMC = :TIPEVTACOCMC
                                        AND CODEVTACOCMC = :CODEVTACOCMC
                                        AND ANOMESREF  = :ANOMESREF
                                        AND CODUNDESRNGC  = :CODUNDESRNGC]]>.Value

        parameterMapper.Parameters.Add(":TIPEVTACOCMC", TIPEVTACOCMC)
        parameterMapper.Parameters.Add(":CODEVTACOCMC", CODEVTACOCMC)
        parameterMapper.Parameters.Add(":ANOMESREF", ANOMESREF)
        parameterMapper.Parameters.Add(":CODUNDESRNGC", CODUNDESRNGC)
        parameterMapper.Parameters.Add(":VLRRSVPLNMKT", VLRRSVPLNMKT)

        connector.ExecuteScalar(sql, parameterMapper)

    End Sub


    Public Function ListPlanoDeMarketing(vo As VO.PlanoDeMarketing) As List(Of VO.PlanoDeMarketing)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT A.TIPEVTACOCMC
                                          , B.NOMTIPEVTACOCMC
                                          , A.CODEVTACOCMC
                                          , C.NOMEVTACOCMC
                                          , A.CODUNDESRNGC
                                          , D.DESUNDESRNGC
                                          , A.ANOMESREF
                                          , A.VLROCDPLNMKT
                                          , A.VLRUTZPLNMKT
                                          , A.VLROCDCTTPLNMKT
                                          , A.VLROCDEXACTTPLNMKT
                                          , A.VLRRSVPLNMKT
                                       FROM MRT.T0155549 A
                                       LEFT JOIN MRT.T0155522 B ON A.TIPEVTACOCMC = B.TIPEVTACOCMC  
                                       LEFT JOIN MRT.T0155531 C ON A.CODEVTACOCMC = C.CODEVTACOCMC
                                       LEFT JOIN MRT.T0131275 D ON A.CODUNDESRNGC = D.CODUNDESRNGC   
                                      WHERE (:TIPEVTACOCMC IS NULL OR :TIPEVTACOCMC = A.TIPEVTACOCMC)
                                        AND (:CODEVTACOCMC IS NULL OR :CODEVTACOCMC = A.CODEVTACOCMC)
                                        AND (:ANOMESREF IS NULL OR :ANOMESREF = A.ANOMESREF)
                                        AND (:CODUNDESRNGC IS NULL OR :CODUNDESRNGC = A.CODUNDESRNGC)
                          ]]>.Value


        parameterMapper.Parameters.Add(":TIPEVTACOCMC", ValueDBProperty(vo.TIPEVTACOCMC))
        parameterMapper.Parameters.Add(":CODEVTACOCMC", ValueDBProperty(vo.CODEVTACOCMC))
        parameterMapper.Parameters.Add(":ANOMESREF", ValueDBProperty(vo.ANOMESREF))
        parameterMapper.Parameters.Add(":CODUNDESRNGC", ValueDBProperty(vo.CODUNDESRNGC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function ListPlanoDeMarketingAno(vo As VO.PlanoDeMarketing) As List(Of VO.PlanoDeMarketing)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[Select A.TIPEVTACOCMC
                                          , B.NOMTIPEVTACOCMC
                                          , A.CODEVTACOCMC
                                          , C.NOMEVTACOCMC
                                          , A.CODUNDESRNGC
                                          , D.DESUNDESRNGC
                                          , A.ANOMESREF
                                          , A.VLROCDPLNMKT
                                          , A.VLRUTZPLNMKT
                                          , A.VLROCDCTTPLNMKT
                                          , A.VLROCDEXACTTPLNMKT
                                          , A.VLRRSVPLNMKT
                                      FROM MRT.T0155549 A
                                      LEFT JOIN MRT.T0155522 B ON A.TIPEVTACOCMC = B.TIPEVTACOCMC  
                                      LEFT JOIN MRT.T0155531 C ON A.CODEVTACOCMC = C.CODEVTACOCMC
                                      LEFT JOIN MRT.T0131275 D ON A.CODUNDESRNGC = D.CODUNDESRNGC   
                                     WHERE (:TIPEVTACOCMC = A.TIPEVTACOCMC)
                                       AND (:CODEVTACOCMC = A.CODEVTACOCMC)
                                       AND (A.ANOMESREF BETWEEN CONCAT(:ANOMESREF,'01') AND CONCAT(:ANOMESREF,'12'))
                                ]]>.Value


        parameterMapper.Parameters.Add(":TIPEVTACOCMC", ValueDBProperty(vo.TIPEVTACOCMC))
        parameterMapper.Parameters.Add(":CODEVTACOCMC", ValueDBProperty(vo.CODEVTACOCMC))
        parameterMapper.Parameters.Add(":ANOMESREF", ValueDBProperty(vo.ANOMESREF))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    ''' <summary>
    ''' CalSldPlnMktAcoCmc
    ''' Calcula saldo do plano de marketing
    ''' </summary>
    ''' <param name="tipoEvento"></param>
    ''' <param name="codEvento"></param>
    ''' <param name="dataRef"></param>
    ''' <param name="codBUs"></param>
    ''' <returns></returns>
    Public Function CalculaSaldoPlanoMarketingAcao(tipoEvento As Integer, codEvento As Integer, dataRef As Integer, codBUs As String) As List(Of VO.PlanoDeMarketing)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim AnoReferencia As String = dataRef.ToString.Substring(0, 4)

        Dim sql As String = <![CDATA[SELECT A.CODUNDESRNGC,                                                                     
                            B.DESUNDESRNGC,
                            (SELECT SUM( VLROCDPLNMKT - VLRRSVPLNMKT - VLRUTZPLNMKT)
                                FROM MRT.T0155549                                                                 
                                 WHERE CODUNDESRNGC = A.CODUNDESRNGC
                                       AND ANOMESREF = :ANOMESREF
                                       AND TIPEVTACOCMC = :TIPEVTACOCMC
                                       AND CODEVTACOCMC = :CODEVTACOCMC
                                 ) AS SLDMESPLNMKT,                                                            
                            (SELECT SUM( VLROCDPLNMKT - VLRRSVPLNMKT - VLRUTZPLNMKT)                  
                                 FROM MRT.T0155549                                                                 
                                 WHERE CODUNDESRNGC = A.CODUNDESRNGC                                               
                                      AND ANOMESREF BETWEEN " & AnoReferencia & "01" & " AND  :ANOMESREF
                                      AND TIPEVTACOCMC = :TIPEVTACOCMC
                                      AND CODEVTACOCMC = :CODEVTACOCMC
                                ) AS SLDATUPLNMKT,                                                             
                            (SELECT SUM( VLROCDPLNMKT -  VLRRSVPLNMKT - VLRUTZPLNMKT)
                                FROM MRT.T0155549                                                                 
                                 WHERE CODUNDESRNGC = A.CODUNDESRNGC
                                      AND ANOMESREF BETWEEN " & AnoReferencia & "01" & " AND " & AnoReferencia & "12
                                      AND TIPEVTACOCMC = :TIPEVTACOCMC
                                      AND CODEVTACOCMC = :CODEVTACOCMC
                                ) AS SLDANOPLNMKT,                                                             
                            (SELECT SUM(  VLRRSVPLNMKT + VLRUTZPLNMKT)
                                FROM MRT.T0155549                                                                 
                                 WHERE CODUNDESRNGC = A.CODUNDESRNGC                                               
                                      AND ANOMESREF BETWEEN " & AnoReferencia & "01" & " AND " & AnoReferencia & "12
                                      AND TIPEVTACOCMC = :TIPEVTACOCMC
                                      AND CODEVTACOCMC = :CODEVTACOCMC
                                ) AS VLRGSTANOPLNMKT,                                                          
                            (SELECT SUM(VLROCDPLNMKT)
                                 FROM MRT.T0155549                                                                 
                                 WHERE CODUNDESRNGC = A.CODUNDESRNGC
                                      AND ANOMESREF BETWEEN " & AnoReferencia & "01" & " AND " & AnoReferencia & "12
                                      AND TIPEVTACOCMC =:TIPEVTACOCMC
                                      AND CODEVTACOCMC =:CODEVTACOCMC
                                ) AS VLROCDANOPLNMKT                                                           
                             FROM MRT.T0155549 A,
                                  MRT.T0131275 B
                             WHERE A.CODUNDESRNGC = B.CODUNDESRNGC
                                   AND A.CODUNDESRNGC IN ( :CODIGOSBUS )
                             GROUP BY A.CODUNDESRNGC,                                                                  
                                      B.DESUNDESRNGC ]]>.Value

        parameterMapper.Parameters.Add(":TIPEVTACOCMC", tipoEvento)
        parameterMapper.Parameters.Add(":CODEVTACOCMC", codEvento)
        parameterMapper.Parameters.Add(":ANOMESREF", dataRef)
        parameterMapper.Parameters.Add(":CODIGOSBUS", codBUs)

        Dim colunas = {"CODUNDESRNGC", "DESUNDESRNGC", "SLDMESPLNMKT", "SLDATUPLNMKT", "SLDANOPLNMKT", "VLRGSTANOPLNMKT", "VLROCDANOPLNMKT"}

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(colunas))

    End Function

    Public Function UpdatePlanoMktAprovacaoAcao(CODACOCMC As Decimal, TIPEVTACOCMC As Decimal, CODEVTACOCMC As Decimal, CODUNDESRNGC As Decimal, ANOMESREF As String) As Integer

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0155549 
                                        SET VLRUTZPLNMKT = (VLRUTZPLNMKT + (SELECT VLRUTZPLNMKT
                                                                              FROM MRT.T0155581 
                                                                             WHERE CODACOCMC = :CODACOCMC
                                                                               AND CODUNDESRNGC = :CODUNDESRNGC)
                                                            )
                                          , VLRRSVPLNMKT = VLRRSVPLNMKT - (SELECT VLRUTZPLNMKT
                                                                             FROM MRT.T0155581
                                                                            WHERE CODACOCMC = :CODACOCMC
                                                                              AND CODUNDESRNGC = :CODUNDESRNGC
                                                                           )
                                      WHERE TIPEVTACOCMC = :TIPEVTACOCMC
                                        AND CODEVTACOCMC = :CODEVTACOCMC
                                        AND CODUNDESRNGC = :CODUNDESRNGC
                                        AND ANOMESREF = :ANOMESREF ]]>.Value

        parameterMapper.Parameters.Add(":CODACOCMC", CODACOCMC)
        parameterMapper.Parameters.Add(":CODUNDESRNGC", CODUNDESRNGC)
        parameterMapper.Parameters.Add(":TIPEVTACOCMC", TIPEVTACOCMC)
        parameterMapper.Parameters.Add(":CODEVTACOCMC", CODEVTACOCMC)
        parameterMapper.Parameters.Add(":ANOMESREF", ANOMESREF)

        Return connector.ExecuteScalar(sql, parameterMapper)

    End Function

End Class

<TableAttribute("MRT.T0155549")>
Public Class PlanoDeMarketingDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property TIPEVTACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODEVTACOCMC As Nullable(Of Decimal)

    <PrimaryKey>
    Property ANOMESREF As Nullable(Of Decimal)

    <PrimaryKey>
    Property CODUNDESRNGC As Nullable(Of Decimal)

    <IgnoreMap>
    Property NOMTIPEVTACOCMC As String
    <IgnoreMap>
    Property NOMEVTACOCMC As String
    <IgnoreMap>
    Property DESUNDESRNGC As String
    <IgnoreMap>
    Property DATA As String
    <IgnoreMap>
    Property CODACOCMC As Nullable(Of Integer)
    <IgnoreMap>
    Property TIPDSNDSCBNF As Nullable(Of Integer)
    <IgnoreMap>
    Property DESDSNDSCBNF As String

    <IgnoreMap>
    Property SLDMESPLNMKT As Nullable(Of Decimal)
    <IgnoreMap>
    Property SLDATUPLNMKT As Nullable(Of Decimal)
    <IgnoreMap>
    Property SLDANOPLNMKT As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLRGSTANOPLNMKT As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLROCDANOPLNMKT As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLRRSTATU As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLRRSTANO As Nullable(Of Decimal)
    <IgnoreMap>
    Property VLRRSTMES As Nullable(Of Decimal)

End Class
