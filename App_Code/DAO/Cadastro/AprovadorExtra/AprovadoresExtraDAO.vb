﻿Imports Core
Public Class AprovadoresExtraDAO
    Inherits DAOBase(Of VO.AprovadoresExtra)

    Public Sub New()
        daoConfiguration = New AprovadoresExtraConfiguration()
    End Sub

    Public Function CarregaGrid(vo As VO.AprovadoresExtra) As List(Of VO.AprovadoresExtra)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT 
                                            S.TIPEVTACOCMC
                                          , E.NOMTIPEVTACOCMC
                                          , S.CODTIPAPV
                                          , T.DESTIPAPV
                                          , S.CODEDEAPV
                                          , COALESCE(U.NOMFNC, CADT.NOMANSTCR) AS NOMUSREDEAPV
                                          , S.CODEDEAPVEXA
                                          , COALESCE(Z.NOMFNC, CADTCR.NOMANSTCR) AS NOMFNC
                                     FROM MRT.RLCTIPEVTAPVEXA S 
                                       LEFT JOIN MRT.T0155522 E ON E.TipEvtAcoCmc = S.TIPEVTACOCMC
                                       LEFT JOIN MRT.T0161573 T ON T.CODTIPAPV = S.CODTIPAPV
                                       LEFT JOIN MRT.T0100361 U ON U.CODFNC = S.CODEDEAPV
                                       LEFT JOIN MRT.CADANSINFTCR CADT ON S.CODEDEAPV = CADT.CODANSTCR
                                       LEFT JOIN MRT.T0100361 Z ON Z.CODFNC = S.CODEDEAPVEXA
                                       LEFT JOIN MRT.CADANSINFTCR CADTCR ON S.CODEDEAPVEXA = CADTCR.CODANSTCR
                                     WHERE (:NOMFNC IS NULL OR UPPER(Z.NOMFNC) LIKE UPPER(:NOMFNC))
                                       AND (:TIPEVTACOCMC IS NULL OR S.TIPEVTACOCMC = :TIPEVTACOCMC)
                                       AND (:CODTIPAPV IS NULL OR S.CODTIPAPV = :CODTIPAPV)
                                       AND (:CODEDEAPVEXA IS NULL OR S.CODEDEAPVEXA = :CODEDEAPVEXA)]]>.Value

        Dim nomeFuncLike = "%" + Trim(vo.NOMFNC) + "%"
        parameterMapper.Parameters.Add(":TIPEVTACOCMC", ValueDBProperty(vo.TIPEVTACOCMC))
        parameterMapper.Parameters.Add(":CODTIPAPV", ValueDBProperty(vo.CODTIPAPV))
        parameterMapper.Parameters.Add(":CODEDEAPVEXA", ValueDBProperty(vo.CODEDEAPVEXA))
        parameterMapper.Parameters.Add(":NOMFNC", ValueDBProperty(nomeFuncLike))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function CarregaRegistrosInseridos(vo As VO.AprovadoresExtra) As List(Of VO.AprovadoresExtra)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT  
                                            S.TIPEVTACOCMC
                                          , E.NOMTIPEVTACOCMC
                                          , S.CODTIPAPV
                                          , T.DESTIPAPV
                                          , S.CODEDEAPV
                                          , COALESCE(U.NOMFNC, CADT.NOMANSTCR) AS NOMUSREDEAPV
                                          , S.CODEDEAPVEXA
                                          , COALESCE(Z.NOMFNC, CADTCR.NOMANSTCR) AS NOMFNC
                                     FROM MRT.RLCTIPEVTAPVEXA S 
                                       LEFT JOIN MRT.T0155522 E ON E.TipEvtAcoCmc = S.TIPEVTACOCMC
                                       LEFT JOIN MRT.T0161573 T ON T.CODTIPAPV = S.CODTIPAPV
                                       LEFT JOIN MRT.T0100361 U ON U.CODFNC = S.CODEDEAPV
                                       LEFT JOIN MRT.CADANSINFTCR CADT ON S.CODEDEAPV = CADT.CODANSTCR
                                       LEFT JOIN MRT.T0100361 Z ON Z.CODFNC = S.CODEDEAPVEXA
                                       LEFT JOIN MRT.CADANSINFTCR CADTCR ON S.CODEDEAPVEXA = CADTCR.CODANSTCR
                                     WHERE (S.TIPEVTACOCMC = :TIPEVTACOCMC)
                                       AND (S.CODTIPAPV = :CODTIPAPV)
                                       AND (S.CODEDEAPV = :CODEDEAPV)
                                       AND (S.CODEDEAPVEXA = :CODEDEAPVEXA)]]>.Value

        parameterMapper.Parameters.Add(":TIPEVTACOCMC", ValueDBProperty(vo.TIPEVTACOCMC))
        parameterMapper.Parameters.Add(":CODTIPAPV", ValueDBProperty(vo.CODTIPAPV))
        parameterMapper.Parameters.Add(":CODEDEAPV", ValueDBProperty(vo.CODEDEAPV))
        parameterMapper.Parameters.Add(":CODEDEAPVEXA", ValueDBProperty(vo.CODEDEAPVEXA))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


    Public Function CarregaAprovadorSubstituido(ByVal CodTipoAprovador As Decimal?) As List(Of VO.AprovadoresExtra)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim sql As String = <![CDATA[SELECT DISTINCT S.CODFNC, S.NOMFNC ]]>.Value

        Select Case CodTipoAprovador
            Case 3 'Dir Compras
                sql = sql + " FROM MRT.T0123183 F"
                sql = sql + " INNER JOIN MRT.T0100981 K ON (K.CODEMP = 1 AND K.CODCPR = F.CODCPR)"
                sql = sql + " INNER JOIN MRT.T0100361 S ON (S.CODFNC = K.CODFNC)"
                sql = sql + " LEFT JOIN MRT.CADANSINFTCR CADT ON (K.CODFNC = CADT.CODANSTCR)"
            Case 5 'Gerente de Categoria
                sql = sql + " FROM MRT.T0118570 D"
                sql = sql + " INNER JOIN MRT.T0100981 K ON (K.CODEMP = 1 AND K.CODCPR = D.CODGERPRD AND K.CODCPR = K.CODGERPRD)"
                sql = sql + " INNER JOIN MRT.T0100361 S ON (S.CODFNC = K.CODFNC)"
                sql = sql + " LEFT JOIN MRT.CADANSINFTCR CADT ON (K.CODFNC = CADT.CODANSTCR)"
            Case 7 'Gerente de Marketing
                sql = sql + " FROM MRT.RLCGERMKTEMTACOCMC A"
                sql = sql + " INNER JOIN MRT.T0100361 S ON (A.CODFNCGERMKT = S.CODFNC)"
                sql = sql + " LEFT JOIN MRT.CADANSINFTCR CADT ON (A.CODFNCGERMKT = CADT.CODANSTCR)"
        End Select
        sql = sql + " ORDER BY S.NOMFNC"

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function BuscaRegistrosAprovadores(ByVal codTipoEvento As Decimal, ByVal codAprovSub As Decimal, ByVal codAprovExtra As Decimal) As List(Of VO.AprovadoresExtra)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT * FROM MRT.RLCTIPEVTAPVEXA 
                                     WHERE TIPEVTACOCMC = :TIPEVTACOCMC
                                      AND CODTIPAPV = :CODTIPAPV
                                      AND CODEDEAPV = :CODEDEAPV]]>.Value

        parameterMapper.Parameters.Add(":TIPEVTACOCMC", ValueDBProperty(codTipoEvento))
        parameterMapper.Parameters.Add(":CODTIPAPV", ValueDBProperty(codAprovSub))
        parameterMapper.Parameters.Add(":CODEDEAPV", ValueDBProperty(codAprovExtra))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function DeletarRegistrosAprovadores(vo As VO.AprovadoresExtra)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[DELETE FROM MRT.RLCTIPEVTAPVEXA 
                                     WHERE TIPEVTACOCMC = :TIPEVTACOCMC
                                       AND CODTIPAPV = :CODTIPAPV
                                       AND CODEDEAPV = :CODEDEAPV]]>.Value

        parameterMapper.Parameters.Add(":TIPEVTACOCMC", ValueDBProperty(vo.TIPEVTACOCMC))
        parameterMapper.Parameters.Add(":CODTIPAPV", ValueDBProperty(vo.CODTIPAPV))
        parameterMapper.Parameters.Add(":CODEDEAPV", ValueDBProperty(vo.CODEDEAPV))

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function

    Public Function InsereRegistrosAprovadores(ByVal codTipoEvento As Decimal, ByVal codAprovSub As Decimal, ByVal codAprovExtra As Decimal, ByVal codFuncionario As Decimal)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[INSERT INTO MRT.RLCTIPEVTAPVEXA
                                    (TIPEVTACOCMC, CODTIPAPV, CODEDEAPV, CODEDEAPVEXA)
                                    VALUES
                                    (:TIPEVTACOCMC, :CODTIPAPV, :CODEDEAPV, :CODEDEAPVEXA)]]>.Value

        parameterMapper.Parameters.Add(":TIPEVTACOCMC", ValueDBProperty(codTipoEvento))
        parameterMapper.Parameters.Add(":CODTIPAPV", ValueDBProperty(codAprovSub))
        parameterMapper.Parameters.Add(":CODEDEAPV", ValueDBProperty(codAprovExtra))
        parameterMapper.Parameters.Add(":CODEDEAPVEXA", ValueDBProperty(codFuncionario))

        Return connector.ExecuteScalar(sql, parameterMapper)
    End Function

End Class

<TableAttribute("MRT.RLCTIPEVTAPVEXA")>
Public Class AprovadoresExtraConfiguration
    Inherits DAOConfiguration

    Property TIPEVTACOCMC As Nullable(Of Decimal)
    Property CODTIPAPV As Nullable(Of Decimal)
    Property CODEDEAPV As Nullable(Of Decimal)
    Property CODEDEAPVEXA As Nullable(Of Decimal)
End Class

