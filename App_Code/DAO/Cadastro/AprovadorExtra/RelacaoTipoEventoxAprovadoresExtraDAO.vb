﻿Imports Core
Public Class RelacaoTipoEventoxAprovadoresExtraDAO
    Inherits DAOBase(Of VO.RelacaoTipoEventoxAprovadoresExtra)

    Public Sub New()
        daoConfiguration = New RelacaoTipoEventoxAprovadoresExtraDAOConfiguration()
    End Sub
End Class


'<TableAttribute("MRT.RLCTIPEVTAPVEXA")>
<TableAttribute("MRT.T0155522")>
Public Class RelacaoTipoEventoxAprovadoresExtraDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property TIPEVTACOCMC As Nullable(Of Decimal)
    Property NOMTIPEVTACOCMC As String
    Property CODCNTCTB As Nullable(Of Decimal)
    Property INDVLDRGRICT As Nullable(Of Decimal)
End Class
