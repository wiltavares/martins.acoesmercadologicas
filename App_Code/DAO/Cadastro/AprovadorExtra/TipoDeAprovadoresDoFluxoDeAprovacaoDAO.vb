﻿Imports Core
Imports System.Text

Public Class TipoDeAprovadoresDoFluxoDeAprovacaoDAO
    Inherits DAOBase(Of VO.TipoDeAprovadoresDoFluxoDeAprovacao)

    Public Sub New()
        daoConfiguration = New TipoDeAprovadoresDoFluxoDeAprovacaoDAOConfiguration()
    End Sub

    Public Function CarregaTipoAprovador() As List(Of VO.TipoDeAprovadoresDoFluxoDeAprovacao)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT CODTIPAPV, DESTIPAPV 
                                    FROM MRT.T0161573 
                                    WHERE CODTIPAPV IN (3,5,7)
                                    ORDER BY CODTIPAPV]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function
End Class

<TableAttribute("MRT.T0161573")>
Public Class TipoDeAprovadoresDoFluxoDeAprovacaoDAOConfiguration
    Inherits DAOConfiguration
    Property CODTIPAPV As Nullable(Of Decimal)
    Property DESTIPAPV As String
End Class
