﻿Imports Core
Public Class CidadeDAO
    Inherits DAOBase(Of VO.Cidade)

    Public Sub New()
        daoConfiguration = New CidadeDAOConfiguration()
    End Sub

End Class

<TableAttribute("MRT.T0100035")>
Public Class CidadeDAOConfiguration
    Inherits DAOConfiguration

End Class