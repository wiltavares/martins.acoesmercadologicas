﻿Imports Core
Public Class FilialEmpresaCorporativoDAO
    Inherits DAOBase(Of VO.FilialEmpresaCorporativo)

    Public Sub New()
        daoConfiguration = New FilialEmpresaCorporativoDAOConfiguration()
    End Sub

    Public Function BuscarCodNomFilial() As List(Of VO.FilialEmpresaCorporativo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODFILEMP,A.NOMFILEMP
                             From MRT.T0112963 A       
                             Where A.CODEMP = 1
                             And A.DATDSTFILEMP Is NULL
                             And A.FLGFILEMPEPD = '*'  
                             ORDER BY A.CODFILEMP]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function ListRelacaoFilialCodigoCidade() As List(Of VO.FilialEmpresaCorporativo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODFILEMP,                         
                                        A.NOMFILEMP,                         
                                        A.CODCIDPCOACOCMC,                   
                                        b.NOMCID,
                                        A.CODBAIFILEMP                       
                                    FROM MRT.T0112963 A,                        
                                      MRT.T0100035 B                         
                                    WHERE A.CODEMP = 1                          
                                       AND A.DATDSTFILEMP IS NULL            
                                       AND A.FLGFILEMPEPD = '*'              
                                       AND B.CODCID  =   A.CODCIDPCOACOCMC   
                                       ORDER BY A.CODFILEMP   ]]>.Value
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function ListFiliais() As List(Of VO.FilialEmpresaCorporativo)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                            CODFILEMP 
                                          , NOMFILEMP
                                          , DESABVFILEMP
                                     FROM MRT.T0112963
                                     WHERE CODCIDPCOACOCMC > 0
                                     ORDER BY CODFILEMP]]>.Value
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


    ''' <summary>
    ''' CODFILEMP
    ''' COD_CID
    ''' Atualiza o codigo da cidade na tabela filial
    ''' </summary>
    ''' <returns></returns>
    Public Sub atualizaCodigoCidade(CODFILEMP As Decimal?, COD_CID As Decimal?)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[UPDATE MRT.T0112963 SET CODCIDPCOACOCMC = :COD_CID WHERE CODFILEMP = :CODFILEMP]]>.Value

        parameterMapper.Parameters.Add(":CODFILEMP", CODFILEMP)
        parameterMapper.Parameters.Add(":COD_CID", COD_CID)

        connector.ExecuteScalar(sql, parameterMapper)
    End Sub

End Class


<TableAttribute("MRT.T0112963")>
Public Class FilialEmpresaCorporativoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODEMP As Nullable(Of Decimal)

    <PrimaryKey>
    Property CCODFILEMP As Nullable(Of Decimal)

    <DateInsert>
    <[Readonly](False, True)>
    Property DATCADFILEMP As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATDSTFILEMP As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATABRFSC As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATBXAFSC As Date

    <DateInsert>
    <[Readonly](False, True)>
    Property DATATVPRSETRCMSFIL As Date

    <IgnoreMap>
    Property NOMCID As String

End Class
