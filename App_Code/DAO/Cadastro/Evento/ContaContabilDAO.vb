﻿Imports Core
Public Class ContaContabilDAO
    Inherits DAOBase(Of VO.ContaContabil)

    Public Sub New()
        daoConfiguration = New ContaContabilDAOConfiguration()
    End Sub


    Public Function ListContaContabil(vo As VO.ContaContabil) As List(Of VO.ContaContabil)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = "SELECT CODCNTCTB, DESCNTCTB from MRT.T0123566;"

        'parameterMapper.Parameters.Add(":CODACOCMC", ValueDBProperty(vo.CODACOCMC))
        'parameterMapper.Parameters.Add(":NOMACOCMC", ValueDBProperty(vo.NOMACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class

<TableAttribute("MRT.T0123566")>
Public Class ContaContabilDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODCNTCTB As Nullable(Of Integer)

End Class
