﻿Imports Core
Public Class TipoDeEventoDAO
    Inherits DAOBase(Of VO.TipoDeEvento)

    Public Sub New()
        daoConfiguration = New TipoDeEventoDAOConfiguration()
    End Sub

    Public Function buscaDadosTipoEvento(tipoEvento As VO.TipoDeEvento) As List(Of VO.TipoDeEvento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()
        Dim Ind As Decimal = 0

        Dim sql As String = <![CDATA[SELECT TIPEVTACOCMC, NOMTIPEVTACOCMC, CODCNTCTB, INDVLDRGRICT, INDPREARDPMC, INDCPHBTB, INDEVTSMA
                                    FROM MRT.T0155522]]>.Value
        If tipoEvento.TIPEVTACOCMC <> 0 Then
            sql = sql + " WHERE "
            sql = sql + " TIPEVTACOCMC = :TIPEVTACOCMC"
            Ind = 1
            parameterMapper.Parameters.Add(":TIPEVTACOCMC", ValueDBProperty(tipoEvento.TIPEVTACOCMC))
        End If

        'Nome do tipo de evento
        If tipoEvento.NOMTIPEVTACOCMC <> "" Then
            Dim nomevtLike = "%" + tipoEvento.NOMTIPEVTACOCMC + "%"

            If Ind = 0 Then
                sql = sql + " WHERE "
                sql = sql + " UPPER(NOMTIPEVTACOCMC) LIKE UPPER(:NOMTIPEVTACOCMC)"
                Ind = 1
            Else
                sql = sql + " AND UPPER(NOMTIPEVTACOCMC) LIKE UPPER(:NOMTIPEVTACOCMC)"
            End If
            parameterMapper.Parameters.Add(":NOMTIPEVTACOCMC", ValueDBProperty(nomevtLike))
        End If

        'Código da conta contábil
        If tipoEvento.CODCNTCTB <> 0 Then
            If Ind = 0 Then
                sql = sql + " WHERE "
                sql = sql + " CODCNTCTB = :CODCNTCTB"
                Ind = 1
            Else
                sql = sql + " AND CODCNTCTB = :CODCNTCTB"
            End If
            parameterMapper.Parameters.Add(":CODCNTCTB", ValueDBProperty(tipoEvento.CODCNTCTB))
        End If

        'Pré Acordo
        If tipoEvento.INDPREARDPMC <> 2 Then
           If Ind = 0 Then
                sql = sql + " WHERE "
                sql = sql + " INDPREARDPMC = :INDPREARDPMC"
            Else
                sql = sql + " AND INDPREARDPMC = :INDPREARDPMC"
            End If
            parameterMapper.Parameters.Add(":INDPREARDPMC", ValueDBProperty(tipoEvento.INDPREARDPMC))
        End If

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function ListTipoDeEventos(vo As VO.TipoDeEvento) As List(Of VO.TipoDeEvento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT TIPEVTACOCMC, NOMTIPEVTACOCMC, CODCNTCTB, INDVLDRGRICT, INDEVTSMA FROM MRT.T0155522 ORDER BY TIPEVTACOCMC ASC]]>.Value

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function ListTipoDeEventosPreAcordo(ByVal IndPreAcordo As Nullable(Of Boolean)) As List(Of VO.TipoDeEvento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT TIPEVTACOCMC
                                          , NOMTIPEVTACOCMC
                                          , CODCNTCTB
                                          , INDVLDRGRICT 
                                          , INDEVTSMA
                                       FROM MRT.T0155522
                             ]]>.Value

        If (IndPreAcordo = True) Then
            sql += " WHERE INDPREARDPMC = 1 "
        ElseIf (IndPreAcordo = False) Then
            sql += " WHERE INDPREARDPMC = 0 "
        End If

        sql += " ORDER BY TIPEVTACOCMC ASC"

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class



<TableAttribute("MRT.T0155522")>
Public Class TipoDeEventoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    <SelectMax>
    Property TIPEVTACOCMC As Nullable(Of Decimal)
    <IgnoreMap>
    Property CODIGO_NOME As String

    <IgnoreMap>
    Property DescDropTipeve() As String

End Class
