﻿Imports Core
Public Class EventoDAO
    Inherits DAOBase(Of VO.Evento)

    Public Sub New()
        daoConfiguration = New EventoDAOConfiguration()
    End Sub

    Public Function ListEvento(vo As VO.Evento) As List(Of VO.Evento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT A.CODEVTACOCMC
                                          , A.NOMEVTACOCMC
                                          , A.TIPEVTACOCMC
                                          , A.INDEVTADIACOCMC
                                          , A.TIPDSNDSCBNF
                                          , C.DESDSNDSCBNF
                                          , A.INDEVTMTDPOLDVR
                                          , B.NOMTIPEVTACOCMC
                                       FROM MRT.T0155531 A
                                       LEFT JOIN MRT.T0155522 B ON A.TIPEVTACOCMC = B.TIPEVTACOCMC
                                       LEFT JOIN MRT.T0109059 C ON A.TIPDSNDSCBNF= C.TIPDSNDSCBNF
                                      WHERE (:CODEVT IS NULL OR :CODEVT = CODEVTACOCMC)
                                        AND UPPER(NOMEVTACOCMC) LIKE UPPER(:NOMEVT)
                                        AND (:TIPEVT IS NULL OR :TIPEVT = A.TIPEVTACOCMC)
                          ]]>.Value

        Dim nomevtLike = "%" + vo.NOMEVTACOCMC + "%"

        parameterMapper.Parameters.Add(":CODEVT", ValueDBProperty(vo.CODEVTACOCMC))
        parameterMapper.Parameters.Add(":NOMEVT", ValueDBProperty(nomevtLike))
        parameterMapper.Parameters.Add(":TIPEVT", ValueDBProperty(vo.TIPEVTACOCMC))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function ListEventoAno(vo As VO.Evento, CodAcao As Decimal) As List(Of VO.Evento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()


        Dim sql As String = <![CDATA[SELECT DISTINCT(A.CodEvtAcoCmc) AS CODEVTACOCMC,
                                           B.NOMEVTACOCMC
                                        FROM MRT.T0155549 A
                                        INNER JOIN MRT.T0155531 B ON A.codevtacocmc = B.codevtacocmc
                                        WHERE A.TipEvtAcoCmc= :TIPEVT
                                        AND SUBSTR(A.AnoMesRef, 0, 4) = TO_CHAR(CURRENT_DATE,'YYYY') ]]>.Value

        If CodAcao <> 0 Then
            sql += <![CDATA[ UNION select b.codevtacocmc, b.nomevtacocmc 
                            from MRT.T0155581 pm
                            INNER JOIN MRT.T0155531 B ON PM.codevtacocmc = B.codevtacocmc
                            WHERE pm.codacocmc =: CodAcao ]]>.Value

            parameterMapper.Parameters.Add(":CodAcao", ValueDBProperty(CodAcao))
        End If

        parameterMapper.Parameters.Add(":TIPEVT", ValueDBProperty(vo.TIPEVTACOCMC))

            Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

    Public Function ConsultaDadosEventoPeloCodigo(idEvento As Decimal) As VO.Evento

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODEVTACOCMC
                                          , A.NOMEVTACOCMC
                                          , A.TIPEVTACOCMC
                                          , A.TIPDSNDSCBNF
                                          , B.DESDSNDSCBNF
                                          , A.INDEVTADIACOCMC
                                          , A.INDVCLPMC
                                       FROM MRT.T0155531 A
                                      INNER JOIN MRT.T0109059 B ON A.TIPDSNDSCBNF = B.TIPDSNDSCBNF
                                      WHERE CODEVTACOCMC= :CODEVTACOCMC
                                      ORDER BY A.CODEVTACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODEVTACOCMC", idEvento)
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql)).FirstOrDefault()

    End Function

    Public Function ConsultaParaFluxo(codAcao As Decimal) As List(Of VO.Evento)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT INDEVTMTDPOLDVR  
                                     FROM MRT.T0155531    
                                     WHERE CodEvtAcoCmc = :CODACAO]]>.Value

        parameterMapper.Parameters.Add(":CODACAO", codAcao)
        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class

<TableAttribute("MRT.T0155531")>
Public Class EventoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODEVTACOCMC As Nullable(Of Decimal)


    <IgnoreMap>
    Property NOMTIPEVTACOCMC As String
    <IgnoreMap>
    Property DESDSNDSCBNF As String
    <IgnoreMap>
    Property CODIGO_NOME As String


End Class
