﻿Imports Core
Public Class OperacaoFiscalDAO
    Inherits DAOBase(Of VO.OperacaoFiscal)

    Public Sub New()
        daoConfiguration = New OperacaoFiscalDAOConfiguration()
    End Sub

    ''' <summary>
    ''' BscDdoCpeOpeFsc
    ''' Busca todos os dados de todos os tipos de operação
    ''' </summary>
    ''' <returns></returns>
    Public Function ListarDadosOperacaoFiscal() As List(Of VO.OperacaoFiscal)

        Return ListT(New VO.OperacaoFiscal)

    End Function
End Class


<TableAttribute("MRT.T0155573")>
Public Class OperacaoFiscalDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    <SelectMax>
    Property CODOPEFSCDSNACOCMC As Nullable(Of Decimal)

End Class

