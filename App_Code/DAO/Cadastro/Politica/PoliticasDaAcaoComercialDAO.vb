﻿Imports Core
Public Class PoliticasDaAcaoComercialDAO
    Inherits DAOBase(Of VO.PoliticasDaAcaoComercial)

    Public Sub New()
        daoConfiguration = New PoliticasDaAcaoComercialDAOConfiguration()
    End Sub

    Public Function ListPoliticasAcao(CODPOLACOCMC As Decimal) As List(Of VO.PoliticasDaAcaoComercial)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT A.CODPOLACOCMC, 
                                    B.DESPOLACOCMC                      
                               FROM MRT.T0155808 A,
                                   MRT.T0155794 B                       
                              WHERE A.CODPOLACOCMC = B.CODPOLACOCMC 
                                AND CodAcoCmc = :CODPOLACOCMC
                              ORDER BY A.CODPOLACOCMC]]>.Value

        parameterMapper.Parameters.Add(":CODPOLACOCMC", CODPOLACOCMC)

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function

End Class

<TableAttribute("MRT.T0155794")>
Public Class PoliticasDaAcaoComercialDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    <SelectMax>
    Property CODPOLACOCMC As Integer?

End Class