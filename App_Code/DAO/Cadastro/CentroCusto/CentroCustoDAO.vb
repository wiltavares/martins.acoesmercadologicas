﻿Imports Core

Public Class CentroCustoDAO
    Inherits DAOBase(Of VO.CentroCusto)

    Public Sub New()
        daoConfiguration = New CentroCustoDAOConfiguration()
    End Sub

    Public Function BuscaCentrodeCustoFuncionario(codCentroCusto As Integer, desCentroCusto As String) As List(Of VO.CentroCusto)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT DISTINCT
                             B.CODCENCST, 
                             B.DESCENCST    
                             FROM           
                             MRT.T0100361 A,
                             MRT.T0123574 B 
                             WHERE                 
                             B.CODCENCST = to_number(RTrim(A.CODCENCST))]]>.Value
        If (Not IsNothing(codCentroCusto) AndAlso codCentroCusto <> 0) Then
            sql = sql + " And B.CODCENCST = :CODCENTROCUSTO"
            parameterMapper.Parameters.Add(":CODCENTROCUSTO", ValueDBProperty(codCentroCusto))
        End If
        If (Not String.IsNullOrEmpty(desCentroCusto)) Then
            desCentroCusto = "%" + desCentroCusto.Trim + "%"
            sql = sql + " And UPPER(B.DESCENCST) Like UPPER(:DESCENTROCUSTO)"
            parameterMapper.Parameters.Add(":DESCENTROCUSTO", ValueDBProperty(desCentroCusto))
        End If
        sql = sql + " ORDER BY B.DESCENCST ASC"


        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties(sql))

    End Function


End Class


<TableAttribute("MRT.T0123574")>
Public Class CentroCustoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    Property CODCENCST As Nullable(Of Decimal)
    Property DESCENCST As String
    Property DATDST As Nullable(Of Date)
    Property DATCAD As Nullable(Of Date)

End Class

