﻿Imports Core
Public Class TipoDeFluxoDAO
    Inherits DAOBase(Of VO.TipoDeFluxo)

    Public Sub New()
        daoConfiguration = New TipoDeFluxoDAOConfiguration()
    End Sub

End Class

<TableAttribute("MRT.T0155565")>
Public Class TipoDeFluxoDAOConfiguration
    Inherits DAOConfiguration

    <PrimaryKey>
    <SelectMax>
    Property TIPFLUAPVACOCMC As Nullable(Of Decimal)

End Class
