﻿Imports Microsoft.VisualBasic
Imports Core
Public Class SimulaFundingDAO
    Inherits DAOBase(Of VO.SimulaFunding)

    Public Sub New()
        daoConfiguration = New MercadoriaDAOConfiguration()
    End Sub

    Public Function BuscaItensSimulaFunding(vo As VO.SimulaFunding) As List(Of VO.SimulaFunding)

        Dim parameterMapper As ParameterMapper = New ParameterMapper()

        Dim sql As String = <![CDATA[SELECT 
                                            M.CODMER,
                                            M.DESMER,
                                            F.CODFRN,
                                            F.NOMFRN AS DESFRN,
                                            T.VLRCSTUNTMER as VLRUNT
                                     FROM MRT.T0100086 M
                                       INNER JOIN MRT.T0100426 F
                                               ON M.CODFRNPCPMER = F.CODFRN
                                       INNER JOIN ( SELECT ETQ.CODMER,
                                                           CASE WHEN FLGMERPMC <> 'S' 
                                                                AND MK.PERMRGADIBDEPMC <> 0
                                                                AND MER.INDRTNFTETBTSOC = 0
                                                           THEN ROUND(ETQ.VLRDIRCSTMEDEFT/MK.PERMRGADIBDEPMC, 4) 
                                                           WHEN FLGMERPMC <> 'S' 
                                                                AND MK.PERMRGADIBDEPMC <> 0
                                                                AND MER.INDRTNFTETBTSOC > 0
                                                           THEN ROUND( (ETQ.VLRDIRCSTMEDEFT
                                                                        /MK.PERMRGADIBDEPMC )
                                                                        - (((ETQ.VLRDIRCSTMEDEFT
                                                                        /MK.PERMRGADIBDEPMC ) *
                                                                        ( A.PERPRGITGSOC + A.PERCRBFNMSEGSOC )
                                                                        / 100 ))
                                                                        , 4) 
                                                           WHEN FLGMERPMC = 'S' 
                                                           THEN ROUND(ETQ.VLRDIRCSTMEDEFT, 4) 
                                                           END AS VLRCSTUNTMER
                                                    FROM MRT.T0201350 ETQ 
                                                      INNER JOIN MRT.T0101295 MK 
                                                              ON MK.CODEMP = 1 
                                                      INNER JOIN MRT.T0100086 MER 
                                                              ON MER.CODEMP = 1
                                                              AND MER.CODMER = ETQ.CODMER
                                                      INNER JOIN MRT.T0106726 A
                                                              ON A.CODEMP = 1
                                                              AND A.CODFILEMP = :CODFILIAL
                                                      INNER JOIN MRT.T0118392 B
                                                              ON B.DATREFLIVPCOVGR = A.DATREFLIVPCO
                                                              AND B.CODEMP = 1
                                                    WHERE ETQ.CODEMP = 1
                                                      AND ETQ.CODFILEMP = :CODFILIAL
                                                    ) T
                                               ON M.CODMER = T.CODMER
                                    WHERE M.CODEMP = 1 AND M.DATDSTMER IS NULL]]>.Value

        If (Not IsNothing(vo.CODFRN)) Then
            sql = sql + " AND :CODFRN = M.CODFRNPCPMER"
            parameterMapper.Parameters.Add(":CODFRN", ValueDBProperty(vo.CODFRN))
        End If
        If (Not IsNothing(vo.DESMER)) Then
            sql = sql + " AND UPPER(M.DESMER) Like UPPER(:DESMERC)"
            parameterMapper.Parameters.Add(":DESMERC", ValueDBProperty("%" + vo.DESMER.Trim() + "%"))
        End If
        If (Not IsNothing(vo.CODMER)) Then
            sql = sql + " And (:CODMER = M.CODMER)"
            parameterMapper.Parameters.Add(":CODMER", ValueDBProperty(vo.CODMER))
        End If

        parameterMapper.Parameters.Add(":CODFILIAL", ValueDBProperty(vo.CODFILEMP))

        Return connector.SelectTList(sql, parameterMapper, RowMapperOnlyProperties({"CODMER", "DESMER", "CODFRN", "DESFRN", "VLRUNT"}))

    End Function
End Class

Public Class SimulaFundingDAOConfiguration
    Inherits DAOConfiguration

    Property CODFRN As Nullable(Of Decimal)
    Property DESFRN As String
    Property CODMER As Nullable(Of Decimal)
    Property DESMER As String
    Property PARTICIPACAO As Nullable(Of Decimal)
    Property QUANTIDADE As Nullable(Of Decimal)
    Property VLRUNT As Nullable(Of Decimal)


End Class

