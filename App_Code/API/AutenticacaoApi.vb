﻿Imports Microsoft.VisualBasic
Imports Newtonsoft.Json
Imports System.Net
Imports RestSharp
Imports RestSharp.Authenticators
Imports Newtonsoft.Json.Linq

Public Class AutenticacaoApi

    Private Shared Sub SetaAlertaApi(response As IRestResponse, retorno As RetornoApi, url As String)
        Dim msg As String = url

        If Not String.IsNullOrEmpty(response.Content) Then
            msg &= " " + response.Content
        End If

        If Not String.IsNullOrEmpty(response.ErrorMessage) Then
            msg &= " " + response.ErrorMessage
        End If

        retorno.SetaAlerta(msg)
    End Sub

    Private Shared Function GetTokenAutenticacao() As RetornoApi
        Dim retorno As RetornoApi = New RetornoApi()
        Dim url As String = ""

        Try
            Dim dtExpiracaoToken As DateTime? = CType(HttpContext.Current.Session("DT_EXPIRA_TOKEN_AUTENTICACAO"), DateTime?)

            If IsNothing(dtExpiracaoToken) OrElse dtExpiracaoToken.Value <= DateTime.Now Then

                Dim urlApi As String = ConfigurationManager.AppSettings("UrlApiTokenAutenticacao")
                Dim authUser As String = ConfigurationManager.AppSettings("authUserAutenticacao")
                Dim authPassword As String = ConfigurationManager.AppSettings("authPasswordAutenticacao")

                url = urlApi
                Dim client As RestClient = New RestClient(url)
                client.Authenticator = New HttpBasicAuthenticator(authUser, authPassword)

                Dim request = New RestRequest()
                request.Method = Method.POST
                request.AddHeader("cache-control", "no-cache")
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded")
                request.AddParameter("grant_type", "client_credentials")

                Dim response = client.Execute(Of Token)(request)

                If response.StatusCode = HttpStatusCode.OK OrElse response.StatusCode = HttpStatusCode.Created Then

                    HttpContext.Current.Session("DT_EXPIRA_TOKEN_AUTENTICACAO") =
                        DateTime.Now.AddSeconds(Double.Parse(response.Data.ExpiresIn))

                    HttpContext.Current.Session("TOKEN_AUTENTICACAO") = response.Data.AccessToken
                Else
                    SetaAlertaApi(response, retorno, url)
                    Return retorno
                End If
            End If

            retorno.Resultado = CType(HttpContext.Current.Session("TOKEN_AUTENTICACAO"), String)

        Catch ex As Exception
            retorno.SetaExcecao(ex, url)
        End Try

        Return retorno
    End Function



    Public Shared Function AutenticaPOST(pSenha As String, pUsuario As String) As RetornoApi
        Dim retorno = New RetornoApi()
        Try
            Dim pr As New Dictionary(Of String, String)
            pr.Add("user", pSenha)
            pr.Add("password", pUsuario)

            retorno = Post("/Data/authMartinsAD", pr)
            If (Not retorno.Sucesso) Then
                retorno.Resultado = JObject.Parse(retorno.Resultado)
                Return retorno
            End If

            retorno.Resultado = JObject.Parse(retorno.Resultado)
        Catch ex As Exception
            retorno.SetaExcecao(ex)
        End Try
        Return retorno
    End Function

    Public Shared Function Post(rotaMetodo As String, param As Dictionary(Of String, String)) As RetornoApi

        Dim retorno = New RetornoApi()
        Dim url = ""
        Try
            Dim retornoToken As RetornoApi = GetTokenAutenticacao()
            If Not retornoToken.Sucesso Then
                Return retornoToken
            End If

            url = ConfigurationManager.AppSettings("urlApiAutenticacao") + rotaMetodo
            Dim client As RestClient = New RestClient(url)
            Dim request As RestRequest = New RestRequest(Method.POST)
            request.AddHeader("client_id", ConfigurationManager.AppSettings("authUserAutenticacao"))
            request.AddHeader("access_token", CType(retornoToken.Resultado, String))
            request.AddHeader("Content-Type", "application/json")
            request.AddParameter("application/json", SimpleJson.SerializeObject(param), ParameterType.RequestBody)

            Dim response As IRestResponse = client.Execute(request)

            If response.StatusCode = HttpStatusCode.OK Then
                retorno.Resultado = response.Content
                Return retorno
            Else
                SetaAlertaApi(response, retorno, url)
                Return retorno
            End If

        Catch ex As Exception
            retorno.SetaExcecao(ex, url)
            Return retorno
        End Try
    End Function

End Class
