﻿Imports Microsoft.VisualBasic
Imports Newtonsoft.Json
Imports System.Net
Imports RestSharp
Imports RestSharp.Authenticators

Public Class AprovaSIMApi

    Private Shared Sub SetaAlertaApi(response As IRestResponse, retorno As RetornoApi, url As String)
        Dim msg As String = url

        If Not String.IsNullOrEmpty(response.Content) Then
            msg &= " " + response.Content
        End If

        If Not String.IsNullOrEmpty(response.ErrorMessage) Then
            msg &= " " + response.ErrorMessage
        End If

        retorno.SetaAlerta(msg)
    End Sub

    Private Shared Function GetTokenAprovaSim() As RetornoApi
        Dim retorno As RetornoApi = New RetornoApi()
        Dim url As String = ""

        Try
            Dim dtExpiracaoToken As DateTime? = CType(HttpContext.Current.Session("DT_EXPIRA_TOKEN_APROVA_CREDITO"), DateTime?)

            If IsNothing(dtExpiracaoToken) OrElse dtExpiracaoToken.Value <= DateTime.Now Then

                Dim urlApi As String = ConfigurationManager.AppSettings("urlApiToken")
                Dim authUser As String = ConfigurationManager.AppSettings("authUserAprovaSim")
                Dim authPassword As String = ConfigurationManager.AppSettings("authPasswordAprovaSim")

                url = urlApi
                Dim client As RestClient = New RestClient(url)
                client.Authenticator = New HttpBasicAuthenticator(authUser, authPassword)

                Dim request = New RestRequest()
                request.Method = Method.POST
                request.AddHeader("cache-control", "no-cache")
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded")
                request.AddParameter("grant_type", "client_credentials")

                Dim response = client.Execute(Of Token)(request)

                If response.StatusCode = HttpStatusCode.OK OrElse response.StatusCode = HttpStatusCode.Created Then

                    HttpContext.Current.Session("DT_EXPIRA_TOKEN_APROVA_CREDITO") =
                        DateTime.Now.AddSeconds(Double.Parse(response.Data.ExpiresIn))

                    HttpContext.Current.Session("TOKEN_APROVA_CREDITO") = response.Data.AccessToken
                Else
                    SetaAlertaApi(response, retorno, url)
                    Return retorno
                End If
            End If

            retorno.Resultado = CType(HttpContext.Current.Session("TOKEN_APROVA_CREDITO"), String)

        Catch ex As Exception
            retorno.SetaExcecao(ex, url)
        End Try

        Return retorno
    End Function

    Public Shared Function GetSessionTimeout(IdNetwork As String, TokenAcessoUsuario As String) As RetornoApi
        Dim retorno = New RetornoApi()
        Try
            Dim codSistema As String = ConfigurationManager.AppSettings("CodSistema")
            Dim pr = New Dictionary(Of String, String)
            pr.Add("IdNetwork", IdNetwork)
            pr.Add("Token", TokenAcessoUsuario)
            pr.Add("IdSystem", codSistema)

            retorno = Post("/User/IsValidSession", pr)

            If Not retorno.Sucesso Then
                Return retorno
            End If

            Dim json = CType(retorno.Resultado, String)

            retorno.Resultado = JsonConvert.DeserializeObject(Of Boolean)(json)

            If IsNothing(retorno.Resultado) Then
                retorno.SetaAlerta("Dados  do usuário não encontrados.")
            End If

        Catch ex As Exception
            retorno.SetaExcecao(ex)
        End Try

        Return retorno
    End Function

    Public Shared Function Post(rotaMetodo As String, param As Dictionary(Of String, String)) As RetornoApi

        Dim retorno = New RetornoApi()
        Dim url = ""
        Try
            Dim retornoToken As RetornoApi = GetTokenAprovaSim()
            If Not retornoToken.Sucesso Then
                Return retornoToken
            End If

            url = ConfigurationManager.AppSettings("urlAprovaSIMApi") + rotaMetodo
            Dim client As RestClient = New RestClient(url)
            Dim request As RestRequest = New RestRequest(Method.POST)
            request.AddHeader("client_id", ConfigurationManager.AppSettings("authUserAprovaSim"))
            request.AddHeader("access_token", CType(retornoToken.Resultado, String))
            request.AddHeader("Content-Type", "application/json")
            request.AddParameter("application/json", SimpleJson.SerializeObject(param), ParameterType.RequestBody)

            Dim response As IRestResponse = client.Execute(request)

            If response.StatusCode = HttpStatusCode.OK Then
                retorno.Resultado = response.Content
                Return retorno
            Else
                SetaAlertaApi(response, retorno, url)
                Return retorno
            End If

        Catch ex As Exception
            retorno.SetaExcecao(ex, url)
            Return retorno
        End Try
    End Function

End Class

Public Class Token

    Public Property Scope As String
    Public Property TokenType As String
    Public Property ExpiresIn As String
    Public Property RefreshToken As String
    Public Property AccessToken As String
End Class

Public Class Enumeradores

    Public Enum TipoAlerta
        Alerta
        Sucesso
        Erro
        Informativo
    End Enum
End Class

Public Class RetornoApi

    Public Property Sucesso As Boolean
    Public Property Mensagem As String
    Public Property Resultado As Object
    Public Property TpAlerta As Enumeradores.TipoAlerta
    Public Property Stacktrace As String
    Public Property InnerException As String

    Public Sub New()
        Sucesso = True
        TpAlerta = Enumeradores.TipoAlerta.Sucesso
    End Sub

    Public Sub SetaAlerta(msg As String)
        Sucesso = False
        TpAlerta = Enumeradores.TipoAlerta.Alerta
        Mensagem = msg
    End Sub

    Public Sub SetaExcecao(ex As Exception, Optional mensagemPrefixo As String = Nothing)
        Sucesso = False
        TpAlerta = Enumeradores.TipoAlerta.Erro
        Mensagem = ex.Message
        Stacktrace = ex.StackTrace

        If Not IsNothing(ex.InnerException) Then
            InnerException = ex.InnerException.ToString
            Mensagem &= InnerException
        End If

        If Not IsNothing(mensagemPrefixo) Then
            Mensagem = mensagemPrefixo & Mensagem
        End If
    End Sub
End Class

