﻿Imports System.ComponentModel.DataAnnotations
Public Class TipoDeAprovadoresDoFluxoDeAprovacaoViewModel

    Property CODTIPAPV As Nullable(Of Decimal)
    Property DESTIPAPV As String
    Property CODFNC As Nullable(Of Decimal)
    Property NOMFNC As String

End Class
