﻿
Public Class DiretoriaCompraViewModel
    Property CODDRTCMP As Nullable(Of Decimal)
    Property CODCPR As Nullable(Of Decimal)
    Property DESDRTCMP As String
    Property CODUNDESRNGC As Nullable(Of Decimal)
    Property CODDRT As Nullable(Of Decimal)
    Property DATDSTDRTCMP As Date

    Public Property CODDESDRTCMP() As String
        Get
            Return String.Format("{0} - {1}", Me.CODDRTCMP, Me.DESDRTCMP)
        End Get
        Set(ByVal value As String)
        End Set
    End Property

End Class
