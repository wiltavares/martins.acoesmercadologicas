﻿Imports System.ComponentModel.DataAnnotations

Public Class PoliticasAcaoComercialViewModel

    <Key>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPOLACOCMC As Nullable(Of Decimal)

    <Required(ErrorMessage:="Preencha o campo Tipo de Fluxo")>
    <MaxLength(100, ErrorMessage:="Máximo: {1} caracteres")>
    Property DESPOLACOCMC As String

End Class
