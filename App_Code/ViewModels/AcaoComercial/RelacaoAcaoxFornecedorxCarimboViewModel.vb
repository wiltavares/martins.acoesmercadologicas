﻿Public Class RelacaoAcaoxFornecedorxCarimboViewModel

    'Friend Filter As RelacaoAcaoxFornecedorxCarimboViewModel

    Property CODACOCMC As Nullable(Of Decimal)
    Property CODFRN As Nullable(Of Decimal)
    Property CODMCOVBAFRN As Nullable(Of Decimal)
    Property VLRUTZACOCMC As Decimal

#Region "Campos Auxiliares"

    Property DATGRCMCOVBAFRN As Date
    Property DATGRCMCO As Date
    Property DATAPVACOCMC As Date
    Property CODDIVCMP As String
    Property DESDIVCMP As String
    Property CELULA As String

    Property COD_FILTRO As Nullable(Of Decimal)

    Property SALDO_CARIMBO As Decimal

    Property TIPEVTACOCMC As Nullable(Of Decimal)
    Property CODEVTACOCMC As String
    Property DESOBJMCOVBAFRN As String
    Property TIPOBJMCOVBAFRN As Nullable(Of Decimal)
    Property DESOBSMCOVBAFRN As String
    Property NOMACOCMC As String
    Property NOMTIPEVTACOCMC As String
    Property NOMEVTACOCMC As String
    Property OBJETIVO As String

    Property FORNECEDOR As String
    Property ACAO As String

    Property NOMFRN As String
    Property VALOR_DISPONIVEL As Decimal
    Property OBSERVACAO As String
    Property VLRPRVMCOVBAFRN As Nullable(Of Decimal)


    Property SALDO_ACAO As Nullable(Of Decimal)
    Property PERCENTUAL_CARIMBO As Nullable(Of Decimal)
    Property VALOR_ESTORNAR_CARIMBO As Nullable(Of Decimal)
    Property VALOR_ESTORNAR As Nullable(Of Decimal)
    Property vlrrlzmcovbafrn As Nullable(Of Decimal)
    Property VLRMCO As Decimal
    Property VLRUTZ As Decimal







    ReadOnly Property CODIGO_NOME() As String
        Get
            Return CODEVTACOCMC & " - " & NOMEVTACOCMC
        End Get
    End Property

    ReadOnly Property DescDropTipeve() As String
        Get
            Return TIPEVTACOCMC & " - " & NOMTIPEVTACOCMC
        End Get
    End Property

    ReadOnly Property DescDrop As String
        Get
            Return TIPOBJMCOVBAFRN & " - " & DESOBJMCOVBAFRN
        End Get
    End Property

    'ReadOnly Property DescDrop As String
    '    Get

    '        Return TIPOBJMCOVBAFRN.ToString + " - " + DESOBJMCOVBAFRN


    '    End Get
    'End Property

#End Region
End Class
