﻿Public Class RelacaoAcaoComercialxPlanoMarketingViewModel
    Property CODACOCMC As Nullable(Of Decimal)
    Property TIPEVTACOCMC As Nullable(Of Decimal)
    Property CODEVTACOCMC As Nullable(Of Decimal)
    Property CODUNDESRNGC As Nullable(Of Decimal)
    Property ANOMESREF As Nullable(Of Decimal)
    Property VLRUTZPLNMKT As Nullable(Of Decimal)
    Property VLRRSVPLNMKT As Nullable(Of Decimal)
    Property VLRSLDMNSPLNMKT As Nullable(Of Decimal)
    Property VLRSLDATUPLNMKT As Nullable(Of Decimal)
    Property VLRSLDANOPLNMKT As Nullable(Of Decimal)

#Region "Campos Auxiliares"

    Property NOMTIPEVTACOCMC As String
    Property NOMEVTACOCMC As String
    Property DESUNDESRNGC As String
    Property TIPDSNDSCBNF As Nullable(Of Decimal)
    Property DESDSNDSCBNF As String

    Property VLRACAO As Nullable(Of Decimal)
    Property VLRESULTADOMENSAL As Nullable(Of Decimal)
    Property VLRESULTADOATUAL As Nullable(Of Decimal)
    Property VLRESULTADOANUAL As Nullable(Of Decimal)
    Property VLTOTALORCADO As Nullable(Of Decimal)
    Property VLTOTALGASTO As Nullable(Of Decimal)
#End Region
End Class
