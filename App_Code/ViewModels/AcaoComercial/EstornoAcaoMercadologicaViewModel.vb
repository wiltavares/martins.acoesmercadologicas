﻿Imports Martins.AcoesMercadologicas.Core

Public Class EstornoAcaoMercadologicaViewModel

    Property CODACOCMC As Decimal?
    Property NOMACOCMC As String
    Property EMPEXAACORDO As String
    Property CODOPERACOES As String
    Property VLRTOTACOCMC As Decimal?

    '<DisplayFormat(DataFormatString:="{0:F2}", ApplyFormatInEditMode:=True)>
    <DisplayFormat(ApplyFormatInEditMode:=True, DataFormatString:="{0:D2}")>
    Property VALOR_SALDO As Nullable(Of Decimal)
    Property CODMCOVBAFRN As Nullable(Of Decimal)

    Property Fornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel)
    Property StateGridFornecedores As GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel)

    Property ItensOrcamento As List(Of RelacaoAcaoComercialPorItemOrcamentoViewModel)
    ReadOnly Property CODIGO_NOME As String
        Get
            Return CODACOCMC & " - " & NOMACOCMC
        End Get
    End Property

End Class
