﻿Imports System.ComponentModel.DataAnnotations
Public Class RelacaoAcaoComercialxPremiacaoExclusivaViewModel

    Property CODACOCMC As Nullable(Of Decimal)
    <Key>
    <Required(ErrorMessage:="Preencha o Código do Fornecedor.")>
    Property CODFRN As Nullable(Of Decimal)
    <Key>
    <Required(ErrorMessage:="Preencha o campo Valor.")>
    Property VLRUTZACOCMC As Nullable(Of Decimal)
    <Key>
    <Required(ErrorMessage:="Preencha o campo Descrição da Premiação.")>
    Property DESPRMEXV As String

    Property NOMFRN As String
    Property Novo As Boolean? = True

End Class
