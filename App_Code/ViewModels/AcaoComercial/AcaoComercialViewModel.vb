﻿Imports System.ComponentModel.DataAnnotations

Public Class AcaoComercialViewModel

    Public Sub New()
        Fornecedores = New List(Of RelacaoAcaoComercialxFornecedoresViewModel)
        Premiacao = New List(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel)
        Carimbos = New List(Of RelacaoAcaoxFornecedorxCarimboViewModel)
        Bonificacoes = New List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)
    End Sub

    'CÓDIGO DA AÇÃO COMERCIAL
    Property CODACOCMC As Decimal?

    'NOME DA ACAO COMERCIAL
    <Required(ErrorMessage:="Preencha o campo Nome")>
    <MaxLength(30, ErrorMessage:="Máximo {1} caracteres")>
    Property NOMACOCMC As String

    'TIPO DESTINO / DESCONTO BONIFICADO
    Property TIPDSNDSCBNF As Decimal?

    'DATA REFERENCIA (AAAA-MM-DD)
    <Required(ErrorMessage:="Preencha o campo Data Inicial")>
    Property DATINIACOCMC As Date?

    'DESCRICAO OBJETIVO ACAO COMERCIAL	
    Property DESOBJACOCMC As String

    'INDICADOR ACAO COMERCIAL COMPLEMENTAR
    Property INDACOCMCCPL As Decimal

    'VALOR TOTAL DA ACAO COMERCIAL
    Property VLRTOTACOCMC As Decimal

    'VALOR TOTAL ORCADO DA ACAO COMERCIAL
    Property VLRTOTOCDACOCMC As Decimal

    'VALOR TOTAL DA TAXA DE ADMINISTRACAO DA ACAO COMERCIAL
    Property VLRTOTTXAADMACOCMC As Decimal

    'VALOR TOTAL DE BRINDES DA ACAO COMERCIAL
    Property VLRTOTBDEACOCMC As Decimal

    'CODIGO DO FUNCIONARIO CRIADOR DA ACAO COMERCIAL
    Property CODFNCCRIACOCMC As Decimal?

    'DATA DE APROVACAO DA ACAO COMERCIAL
    Property DATAPVACOCMC As Date?

    'TIPO FLUXO APROVACAO ACAO COMERCIAL/TATICA
    Property TIPFLUAPVACOCMC As Decimal?

    'CODIGO STATUS APROVACAO DA ACAO COMERCIAL
    Property CODSTAAPVACOCMC As Decimal?

    'CODIGO FUNCIONARIO APROVADOR ATUAL DA ACAO COMERCIAL
    Property CODFNCAPVATUACOCMC As Decimal?

    'CODIGO DA ACAO COMERCIAL ORIGINAL
    <RequiredIf("INDACOCMCCPL", 1, ErrorMessage:="Preencha o campo Ação Principal")>
    <IntegerValidator()>
    Property CODACOCMCORI As Decimal?

    'DATA REFERENCIA (AAAA-MM-DD)
    <Required(ErrorMessage:="Preencha o campo Data Final")>
    Property DATFIMACOCMC As Date?

    'DATA DE GERACAO DA ACAO COMERCIAL
    Property DATGRCACOCMC As Date?

    'INDICADOR DE ALTERACAO DE FORNECEDOR DA ACAO COMERCIAL
    Property INDALTFRNACOCMC As Decimal?

    'CODIGO CANAL VENDA INICIAL

    '<RequiredIf("hCanalVenda", "true", ErrorMessage:="Preencha o campo Canal de Venda")>
    '<Required(ErrorMessage:="Preencha o campo Canal de Venda")>
    Property CODCNLVNDINI As Decimal?
    Property TIPACOMCD As Nullable(Of Decimal)

    Property VALOR_SALDO As Nullable(Of Decimal)
    <RequiredIf("INDACOCMCCPL", 0, ErrorMessage:="Preencha o campo Filial")>
    Property CODFILEMPORIVBA As Nullable(Of Decimal)


    'Observação da Campanha B2B
    <DataType(DataType.MultilineText)>
    Property DESCPHBTB As String

    Property INDACOCMCBTBVLR As Nullable(Of Decimal)


    Property INDVCLPMC As Nullable(Of Decimal)

    'CODIGO ACAO COMERCIAL COPIA (Ação que originou o clone)
    Property CODACOCMCCPI As Nullable(Of Decimal)

    Property TIPDSNDSPCBK As Nullable(Of Decimal)

    'CODIGO EVENTO PROMOCIONAL
    Property CODEVTBTB As Nullable(Of Decimal)


#Region "Campos Auxiliares"

    Public Property INDVCLPMCBool() As Boolean
        Get
            Return (INDVCLPMC.GetValueOrDefault() = 1)
        End Get
        Set(ByVal value As Boolean)
            If (value) Then
                INDVCLPMC = 1
            Else
                INDVCLPMC = 0
            End If
        End Set
    End Property



    'Descricao do status
    Property DESCSTA As String

    'NOME DO FUNCIONARIO CRIADOR DA ACAO COMERCIAL
    Property NOMFNC As String
    'NOME DO FUNCIONARIO APROVADOR DA ACAO COMERCIAL
    Property NOMFNCAPVATU As String
    'NOME USUARIO PARA EXECUCAO DO JOB
    Property NOMUSRRCF As String

    'Codigo do evento
    Property CODEVTACOCMC As Nullable(Of Decimal)
    'Nome do evento
    Property NOMEVTACOCMC As String

    'Codigo do tipo do evento
    Property TIPEVTACOCMC As Nullable(Of Decimal)

    'Nome do tipo do evento
    Property NOMTIPEVTACOCMC As String

    'Auxiliares para Conuslta Acao x Fornecedor
    Property CODFRN As String
    Property NOMFRN As String
    Property saldo As String
    Property ptcfrn As String
    Property SALDO_FORNECEDOR As String
    Property CODITEOCDACOCMC As String
    Property CODMER As String
    Property COD_FILTRO As Nullable(Of Decimal)

    'propriedades celula
    Property CODDIVCMP As Decimal?
    Property DESDIVCMP As String

    'propriedades COMPRADOR
    Property CODCPR As Decimal?
    Property NOMCPR As String

    'propriedades TIpo operacao
    Property CODOPE As Decimal?
    Property NOMOPE As String

    'campo auxilicar de data
    Property DataIniAux As String
    Property DataFimAux As String

    'Campo código da filial
    Property CODFILEMP As Nullable(Of Decimal)

    'Campo para idenficar se é pré-acordo
    'Property IndPreAcordo As Boolean
    'Campo para verificar qual é o tipo de pré-acordo
    'Property TipoPreAcordo As Nullable(Of Decimal)

#End Region

#Region "Campos Auxiliares Tela Cadastro/Edit"

    Property CODOPEFSCDSNACOCMC As Decimal? 'Codigo operacao fiscal
    Property VLRMAXACOCMC As Decimal
    Property VLRTXAADM As Decimal
    Property VLRTXABDE As Decimal
    Property EmpenhoExa As String

    Property DESOPEFSCDSNACOCMC As String 'NOME OPERACAO
    Property CODITE As String
    Property VLRORC As String
    Property VLRUTZ As String
    Property VLRSLD As String

    'Indicador de incentivo
    Property INDVLDRGRICT As Decimal

    Property CanalVendaVisible As Boolean

    Property Fornecedores As List(Of RelacaoAcaoComercialxFornecedoresViewModel)
    Property StateGridFornecedores As GridSettings(Of RelacaoAcaoComercialxFornecedoresViewModel)
    Property ItensOrcamento As List(Of RelacaoAcaoComercialPorItemOrcamentoViewModel)
    Property StateGridItensOrcamento As GridSettings(Of RelacaoAcaoComercialPorItemOrcamentoViewModel)
    Property Carimbos As List(Of RelacaoAcaoxFornecedorxCarimboViewModel)
    Property StateGridCarimbos As GridSettings(Of RelacaoAcaoxFornecedorxCarimboViewModel)

    Property PlanosDeMarketing As List(Of RelacaoAcaoComercialxPlanoMarketingViewModel)
    Property ItensSubstituidos As List(Of RelacaoAcaoComercialPorItemOrcamentoViewModel)
    Property FluxosAprovacao As List(Of RelacaoAcaoComercialxFluxoAprovacaoViewModel)
    Property Incentivos As List(Of RelacaoIncentivoxAcaoComercialViewModel)
    Property Mecanica As RelacaoAcaoComercialxMecanicaDaAcaoViewModel

    Property Bonificacoes As List(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)
    Property StateGridBonificacoes As GridSettings(Of RelacaoAcaoComercialXPedidoBonificadoViewModel)
    Property StateGridPremiacao As GridSettings(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel)
    Property Premiacao As List(Of RelacaoAcaoComercialxPremiacaoExclusivaViewModel)

    Property ValorTotal As Decimal
    Property SaldoOrcamento As Decimal
    Property ValorTotalBrinde As Decimal
    Property TaxaBrinde As Decimal
    Property ValorCliente As Decimal
    Property ValorParticipacao As Decimal
    Property SaldoReceitaCMV As Decimal
    Property TaxaAdmin As Decimal
    ReadOnly Property SaldoParticipacao As Decimal
        Get
            Return VLRTOTOCDACOCMC - ValorTotal
        End Get
    End Property
    Property Enviar As Boolean
    Property confirmaEnviar2 As Nullable(Of Boolean)
    Property confirmaEnviar1 As Nullable(Of Boolean)
    Property confirmaEnviarGerente As Nullable(Of Decimal)

    ReadOnly Property IndPreAcordo As Boolean?
        Get
            Return (TIPACOMCD = 5 Or TIPACOMCD = 6)
        End Get
    End Property
    Property DESFRMDSCBNF As String
    Property QDEDIARCBPMS As Integer
    Property TIPFRMDSCBNF As Integer


    Property relacaoAcaoMecanicaList As List(Of RelacaoAcaoMecanicaViewModel)

    Property INDEVTSMA As Nullable(Of Decimal)

    Property INDCASHBACK As Nullable(Of Decimal)

#End Region

End Class
