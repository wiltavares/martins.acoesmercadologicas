﻿Public Class DadosFluxoAcaoComercial
    Property aprovar As Boolean
    Property rejeitar As Boolean
    Property parecer As Boolean
    Property pedir As Boolean
    Property observacao As String
    Property funcionario As Integer
    Property nomFuncionario As String
    Property radio As Integer

#Region "Campos Auxiliares"
    Property TIPFRMDSCBNF As Nullable(Of Decimal)
    Property QDEDIARCBPMS As Nullable(Of Decimal)
    Property DESFRMDSCBNF As String
    Property IndPreAcordo As Boolean
    Property NUMSEQNIVAPV As Integer

#End Region

End Class
