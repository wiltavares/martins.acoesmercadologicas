﻿Imports System.ComponentModel.DataAnnotations
Public Class RelacaoAcaoComercialxFornecedoresViewModel
    ''' <summary>
    '''CODIGO DA ACAO COMERCIAL
    ''' </summary>
    ''' <returns></returns>
    Property CODACOCMC As Nullable(Of Decimal)
    ''' <summary>
    '''CODIGO FORNECEDOR
    ''' </summary>
    ''' <returns></returns>
    Property CODFRN As Nullable(Of Decimal)
    ''' <summary>
    '''CODIGO UNIDADE ESTRATEGICA DE NEGOCIOS (BU)
    ''' </summary>
    ''' <returns></returns>
    Property CODUNDESRNGC As Nullable(Of Decimal)
    ''' <summary>
    '''DESCRICAO DO BENEFICIO DO FORNECEDOR NA ACAO COMERCIAL
    ''' </summary>
    ''' <returns></returns>
    <Required(ErrorMessage:="Preencha o campo Benefício do Fornecedor")>
    Property DESBFCFRNACOCMC As String
    ''' <summary>
    '''DESCRICAO DA SELECAO DOS FORNECEDORES
    ''' </summary>
    ''' <returns></returns>
    Property DESSELFRN As String
    ''' <summary>
    '''INDICADOR DE ATENDIMENTO DIRETO PELO FORNECEDOR (KEY ACCOUNT)
    ''' </summary>
    ''' <returns></returns>
    Property INDATDDTOFRN As Nullable(Of Decimal)
    ''' <summary>
    '''TIPO DESTINO / DESCONTO BONIFICADO
    ''' </summary>
    ''' <returns></returns>
    Property TIPDSNDSCBNF As Nullable(Of Decimal)
    ''' <summary>
    '''TIPO DESTINO DESCONTO BONIFICACAO DE ACRESCIMO
    ''' </summary>
    ''' <returns></returns>
    Property TIPDSNDSCBNFACR As Nullable(Of Decimal)
    ''' <summary>
    '''TIPO EMPENHO EXTRA CONTRATO
    ''' </summary>
    ''' <returns></returns>
    Property TIPDSNDSCBNFEXACTT As Nullable(Of Decimal)

    ''' <summary>
    '''VALOR PARTICIPACAO DO FORNECEDOR NA ACAO COMERCIAL
    ''' </summary>
    ''' <returns></returns>
    Property VLRPTCFRNACOCMC As Nullable(Of Decimal)

    ''' <summary>
    '''VALOR DO SALDO CONTA CORRENTE DO FORNECEDOR
    ''' </summary>
    ''' <returns></returns>
    Property VLRSLDCNTCRRFRN As Nullable(Of Decimal)

    ''' <summary>
    '''VALOR SALDO DISPONIVEL PARA ACOES FUTURAS DO FORNECEDOR
    ''' </summary>
    ''' <returns></returns>
    Property VLRSLDDISACOFUTFRN As Nullable(Of Decimal)

    ''' <summary>
    ''' VALOR SALDO DESTINO ACRESCIMO
    ''' </summary>
    ''' <returns></returns>
    Property VLRSLDDSNACR As Nullable(Of Decimal)

    ''' <summary>
    '''VALOR SALDO EMPENHO CONTRATO ACAO COMERCIAL
    ''' </summary>
    ''' <returns></returns>
    Property VLRSLDDSNCTTACOCMC As Nullable(Of Decimal)

    ''' <summary>
    '''VALOR SALDO DO DESTINO DE DESCONTO DA ACAO COMERCIAL
    ''' </summary>
    ''' <returns></returns>
    Property VLRSLDDSNDSCACOCMC As Nullable(Of Decimal)

    ''' <summary>
    '''VALOR DA TAXA DE ADMINISTRACAO DO BRINDE DA ACAO COMERCIAL
    ''' </summary>
    ''' <returns></returns>
    Property VLRTXAADMBDEACOCMC As Nullable(Of Decimal)

    ''' <summary>
    '''VALOR TAXA DE ADMINISTRACAO DO FORNECEDOR NA ACAO COMERCIAL
    ''' </summary>
    ''' <returns></returns>
    Property VLRTXAADMFRNACOCMC As Nullable(Of Decimal)

    ''' <summary>
    '''VALOR UTILIZADO DO CONTRATO NA ACAO COMERCIAL
    ''' </summary>
    ''' <returns></returns>
    Property VLRUTZCTTACOCMC As Nullable(Of Decimal)

    ''' <summary>
    '''VALOR UTILIZADO EXTRA CONTRATO NA ACAO COMERCIAL
    ''' </summary>
    ''' <returns></returns>
    Property VLRUTZEXACTTACOCMC As Nullable(Of Decimal)

    ''' <summary>
    '''VALOR UTILIZADO RECEITA SOBRE CMV
    ''' </summary>
    ''' <returns></returns>
    Property VLRUTZRCTCSTMER As Nullable(Of Decimal)

    Property VLR_TOTAL_ACAO As Nullable(Of Decimal)
    Property PERCENTUAL As Nullable(Of Decimal)
    Property VALOR_ESTORNAR As Nullable(Of Decimal)
    Property CODMCOVBAFRN As Nullable(Of Decimal)
    Property SALDO_ACAO As Nullable(Of Decimal)

    Property VLRUTZACOCMC As Nullable(Of Decimal)
    Property TIPFRNACOCMC As Nullable(Of Decimal)
    Property PERPTCFRNACOCMC As Nullable(Of Decimal)

#Region "Campos Auxiliares"

    Property VLRTOTACOCMC As Nullable(Of Decimal)
    Property VLRTOTTXAADMACOCMC As Nullable(Of Decimal)
    Property VLRTOTBDEACOCMC As Nullable(Of Decimal)
    Property CODEMP As Nullable(Of Decimal)
    Property NOMFRN As String
    Property DESDSNDSCBNF As String
    Property DESDSNDSCBNFEXACTT As String
    Property DESUNDESRNGC As String
    Property TIPDES As String

    Property CODDIVCMP As Nullable(Of Decimal)
    Property DESDIVCMP As String
    Property CODDRTCMP As Nullable(Of Decimal)
    Property DESDRTCMP As String

    Property SldExaCttBde As Nullable(Of Decimal)
    Property SldCttBde As Nullable(Of Decimal)
    Property SldReceitaCMVBde As Nullable(Of Decimal)
    'Property extaux As Nullable(Of Decimal)
    'Property cttAux As Nullable(Of Decimal)
    'Property CMVaux As Nullable(Of Decimal)
    'Property VlrTot As Nullable(Of Decimal)
    'Property ValRat As Nullable(Of Decimal)
    'Property txtValRat As Nullable(Of Decimal)
    'Property sInserirBeneficioParaOsFornecedores As String
    Property EmpenhoExaFrn As String
    'Property QtdeTotalRateio As Integer


    Property IndPreAcordo As Boolean


    'Property rbtRatVlr As Integer

    ReadOnly Property CorTipoSaldoFrn As String
        Get
            Return RetornaCorTipoSaldoFornecedor()
        End Get
    End Property

    ReadOnly Property CodNomFrn As String
        Get
            Return String.Format("{0} - {1}", CODFRN, NOMFRN)
        End Get
    End Property

#Region "Campos auxiliares Estorno"
    ReadOnly Property PercPtc As Decimal
        Get
            Return PercPtc
        End Get
    End Property

    ReadOnly Property PerTxaAdm As Decimal
        Get
            Return PerTxaAdm
        End Get
    End Property

    ReadOnly Property PerAcordo As Decimal
        Get
            Return PerAcordo
        End Get
    End Property

    ReadOnly Property PerExaAcordo As Decimal
        Get
            Return PerExaAcordo
        End Get
    End Property

    Property EstornoAcordo As Decimal?
    Property EstornoExaAcordo As Decimal?

#End Region
#End Region

#Region "Tipo Saldo"
    Public Function SemDados() As Boolean
        Return CODUNDESRNGC Is Nothing AndAlso DESDSNDSCBNFEXACTT Is Nothing
    End Function

    Public Function SemSaldoAcordo() As Boolean
        Return VLRUTZCTTACOCMC Is Nothing Or VLRUTZCTTACOCMC <= 0
    End Function

    Public Function SemSaldoAcordoExtra() As Boolean
        Return VLRSLDDSNDSCACOCMC Is Nothing Or VLRSLDDSNDSCACOCMC <= 0
    End Function

    Public Function SemSaldoAcordoSaldoExtra() As Boolean
        Return SemSaldoAcordo() AndAlso SemSaldoAcordoExtra()
    End Function


    Public Function SaldoAcordoExtraInsuficiente() As Boolean
        Return VLRSLDDSNDSCACOCMC Is Nothing Or VLRSLDDSNDSCACOCMC < VLRUTZEXACTTACOCMC
    End Function

    Public Function RetornaTipoSaldoFornecedor() As Integer
        If SemDados() Then
            Return Constantes.FrnSaldo.SemDados
        End If
        If SemSaldoAcordoSaldoExtra() Then
            Return Constantes.FrnSaldo.SemDados
        End If
        If SemSaldoAcordo() Then
            Return Constantes.FrnSaldo.SemDados
        End If
        If SemSaldoAcordoExtra() Then
            Return Constantes.FrnSaldo.SemDados
        End If
        Return Nothing
    End Function

    Public Function RetornaCorTipoSaldoFornecedor() As String
        If SemDados() Then
            Return "gray-color"
        End If
        'If SemSaldoAcordoSaldoExtra() Then
        '    Return "red-color"
        'End If
        'If SemSaldoAcordo() Then
        '    Return "green-color"
        'End If
        If SemSaldoAcordoExtra() Then
            'Return "orange-color"
            Return "red-color"
        End If

        If SaldoAcordoExtraInsuficiente() Then
            Return "orange-color"
        End If
        'Return "black-color"
        Return "green-color"
    End Function
#End Region

End Class
