﻿Public Class RelacaoAcaoComercialxFluxoAprovacaoViewModel
    Property CODACOCMC As Nullable(Of Decimal)
    Property CODSTAAPVACOCMC As Nullable(Of Decimal)
    Property NUMFLUAPV As Nullable(Of Decimal)

#Region "Campos Auxiliares"
    Property NUMSEQFLUAPV As Nullable(Of Decimal)
    Property CODEDEAPV As Nullable(Of Decimal)
    Property DATHRAFLUAPV As Date?
    Property DATHRAAPVFLU As Date?
    Property NUMSEQNIVAPV As Nullable(Of Decimal)
    Property CODFNC As Nullable(Of Decimal)
    Property NOMFNC As String
    Property TIPSTAFLUAPV As String
    Property CODFNCARZ As Decimal
    Property NOMFNCARZ As String
    Property DESOBSAPV As String
    Property DESMTVAPVFLUACOCMC As String
    Property NUMSEQFLUAPVPEDOPN As Decimal?

    ReadOnly Property NOMTIPSTAFLUAPV As String
        Get
            Return Constantes.ConvertStatusFluxoAprovacao(TIPSTAFLUAPV)
        End Get
    End Property

#End Region

End Class
