﻿Imports System.ComponentModel.DataAnnotations
Public Class AcompanhamentoPreAcordoViewModel

    Property DATREFOPE As Date?
    Property TIPOPEINIGRCHST As String
    Property CODEMP As Nullable(Of Decimal)
    Property CODFILEMP As Nullable(Of Decimal)
    Property CODACOCMC As Nullable(Of Decimal)
    Property CODPMC As Nullable(Of Decimal)
    Property CODMER As Nullable(Of Decimal)
    Property VLRUNTFNDMERPMC As Nullable(Of Decimal)
    Property QDEITEPMC As Nullable(Of Decimal)
    Property CODDIVCMP As Nullable(Of Decimal)
    Property CODFRN As Nullable(Of Decimal)
    Property CODFRNPTR As Nullable(Of Decimal)
    Property NOMFRNPTR As String
    Property CODCPR As Nullable(Of Decimal)
    Property CODITEOCDACOCMC As String

    Property IndOpcao As Nullable(Of Decimal)
    Property IndVisao As Nullable(Of Decimal)
    Property INDCPHBTB As Nullable(Of Decimal)
    Property TipoOperacao As String

    Property NOMACOCMC As String
    Property NOMFILEMP As String
    Property DESMER As String
    Property NOMFRN As String
    Property DESPMC As String
    Property CODCLI As Decimal
    Property NOMCLI As String
    Property DATIPRNOTFSC As Date?
    Property NUMITEORI As Decimal
    Property NUMDOCOPE As Decimal

    <Required(ErrorMessage:="Preencha o campo Período Inicio")>
    Property DATINIACOCMC As Date?
    <Required(ErrorMessage:="Preencha o campo Período Fim")>
    Property DATFIMACOCMC As Date?

#Region "Campos Auxiliares"
    'campo auxilicar de data
    Property DataIniAux As String
    Property DataFimAux As String
    Property QuantidadeMerc As Decimal?
    Property TotalGeral As Decimal?
    Property CODFILEMPORIVBA As Nullable(Of Decimal)

    ReadOnly Property Total As Decimal
        Get
            If (IsNothing(QDEITEPMC) OrElse IsNothing(VLRUNTFNDMERPMC)) Then
                Return 0
            End If
            Return QDEITEPMC.Value * VLRUNTFNDMERPMC.Value
        End Get
    End Property
#End Region
End Class