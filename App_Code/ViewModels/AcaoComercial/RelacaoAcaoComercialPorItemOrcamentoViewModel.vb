﻿Imports System.ComponentModel.DataAnnotations

Public Class RelacaoAcaoComercialPorItemOrcamentoViewModel
    Property CODACOCMC As Nullable(Of Decimal)
    Property NUMLNHITEOCDACOCMC As Nullable(Of Decimal)
    Property CODITEOCDACOCMC As String
    <Required(ErrorMessage:="É preciso preencher a quantidade.")>
    <Range(1, 99999999999, ErrorMessage:="Favor preencher com um valor entre 1 e 99999999999")>
    Property QDEITEOCDACOCMC As Nullable(Of Decimal)
    Property VLRUNTITEOCDACOCMC As Decimal
    Property DATUTZITEACOCMC As Nullable(Of DateTime)
    Property DESFRMUTZITEACOCMC As String
    Property CODOPEFSCDSNACOCMC As Nullable(Of Decimal)
    Property VLRUTZOPEDSNACOCMC As Decimal
    Property QDEUTZOPEDSNACOCMC As Nullable(Of Decimal)
    <Required(ErrorMessage:="Favor escolher uma mercadoria.")>
    Property CODMER As Nullable(Of Decimal)
    Property CODUNDESRNGCOPEDSN As Nullable(Of Decimal)
    Property CODFILEMP As Nullable(Of Decimal)
    Property QDEDIACMPITEACOCMC As Nullable(Of Decimal)
    Property CODCENCST As Nullable(Of Decimal)
    Property NUMCGCEMPFRN As String
    Property IDTEMPFRNITERSU As Nullable(Of Decimal)
    Property IDTLCLEMPFRNITERSU As Nullable(Of Decimal)
    Property IDTTERPGTITERSU As Nullable(Of Decimal)
    Property CODIDTTSC As Nullable(Of Decimal)
    Property IDTCNTCTB As Nullable(Of Decimal)
    Property INDITEREQRSUVLRMNM As Nullable(Of Decimal)
    Property DESOBSCPR As String
    Property NUMCTTFRN As Nullable(Of Decimal)
    Property IDTITERSU As Nullable(Of Decimal)
    Property NUMLNHCTTRSU As Nullable(Of Decimal)
    Property DESCTTRSU As String
    Property VLRPTCCLIITEACOCMC As Decimal
    Property VLRPTCFRNITEACOCMC As Decimal
    Property CODSTAITERSU As Nullable(Of Decimal)
    Property INDITERSUARZPRVPGT As Nullable(Of Decimal)
    Property INDITERSUFRNUNC As Nullable(Of Decimal)

#Region "Campos Auxiliares"

    Property DESTERPGTITERSU As String
    Public NOMRAZSOCEMPFRN As String
    Public IDTCONCGCEMPFRN As String


    Property DESOPEFSCDSNACOCMC As String
    <Required(ErrorMessage:="Favor escolher um Tipo de Operação.")>
    Property INDTIPOPEDSNACOCMC As Nullable(Of Decimal)
    Property INDBDEOPEDSNACOCMC As Nullable(Of Decimal)
    Property INDUNDESRNGCACOCMC As Nullable(Of Decimal)
    Property DESUNDESRNGC As String
    Property CODFRNPCPMER As Nullable(Of Decimal)
    Property INDGRCITEENCFOLPGT As Nullable(Of Decimal)
    Property INDGRCREQAUTITERSU As Nullable(Of Decimal)
    Property DESSTAITERSU As String
    Property NOMFILEMP As String
    Property SALDO As Decimal
    Property DESCENCST As String
    Property IndUtzIte As Nullable(Of Decimal)
    Property Checked As Boolean
    Property NOMITEMORC As String
    Property CODMERITEMDROP As Nullable(Of Decimal)
    Property NOMITEMORCDROP As String

    ReadOnly Property ValorTotal As Decimal
        Get
            If QDEITEOCDACOCMC Is Nothing Then
                Return 0
            End If
            Return Math.Round((VLRUNTITEOCDACOCMC * QDEITEOCDACOCMC.Value), 4, MidpointRounding.AwayFromZero)
        End Get
    End Property

    ReadOnly Property SaldoItem As Decimal
        Get
            If (Not IsNothing(VLRUTZOPEDSNACOCMC)) Then
                Return ValorTotal - VLRUTZOPEDSNACOCMC
            Else
                Return ValorTotal
            End If
        End Get
    End Property

    Property AcaoComercial As AcaoComercialViewModel

    ReadOnly Property DescDropSuprimentos As String
        Get
            If (Not IsNothing(CODSTAITERSU) AndAlso CODSTAITERSU <> 0) Then
                Return CODSTAITERSU.ToString + " - " + DESSTAITERSU.ToString
            Else
                Return Nothing
            End If
        End Get
    End Property

    ReadOnly Property DescNomItemOrcDrop As String
        Get
            If (Not IsNothing(NOMITEMORCDROP)) Then
                Return CODMERITEMDROP.ToString + " - " + NOMITEMORCDROP
            Else
                Return NOMITEMORCDROP
            End If
        End Get
    End Property
#End Region
End Class
