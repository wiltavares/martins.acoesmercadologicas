﻿Imports System.ComponentModel.DataAnnotations

Public Class TipoFluxoViewModel

    <Key>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPFLUAPVACOCMC As Nullable(Of Decimal)
    <Required(ErrorMessage:="Preencha o campo Tipo de Fluxo")>
    <MaxLength(100, ErrorMessage:="Máximo: {1} caracteres")>
    Property DESFLUAPVACOCMC As String

End Class
