﻿Imports System.ComponentModel.DataAnnotations
Public Class AprovadoresExtraViewModel

    <Required(ErrorMessage:="Preencha o campo Tipo de Evento")>
    Property TIPEVTACOCMC As Nullable(Of Decimal)

    Property NOMTIPEVTACOCMC As String

    <Required(ErrorMessage:="Preencha o campo Tipo de Aprovador")>
    Property CODTIPAPV As Nullable(Of Decimal)

    Property DESTIPAPV As String
    Property CODEDEAPV As Nullable(Of Decimal)
    Property NOMUSREDEAPV As String
    Property CODEDEAPVEXA As Nullable(Of Decimal)
    Property NOMUSREDEAPVEXT As String
    Property CODCNTCTB As Nullable(Of Decimal)
    Property INDVLDRGRICT As Nullable(Of Decimal)


    <Required(ErrorMessage:="Preencha o campo  Aprovador Substítuido")>
    Property CODFNC As Nullable(Of Decimal)
    <Required(ErrorMessage:="Preencha o campo  Funcionário")>
    Property NOMFNC As String
End Class
