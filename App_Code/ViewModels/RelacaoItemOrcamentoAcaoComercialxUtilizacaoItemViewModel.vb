﻿Imports System.ComponentModel.DataAnnotations

Public Class RelacaoItemOrcamentoAcaoComercialxUtilizacaoItemViewModel
    Property CODACOCMC As Nullable(Of Decimal)

    Property NUMLNHITEOCDACOCMC As Nullable(Of Decimal)

    Property NUMSEQUTZITEOCD As Nullable(Of Decimal)


    Property VLRUTZITEOCDACOCMC As Nullable(Of Decimal)
    Property QDEUTZITEOCDACOCMC As Nullable(Of Decimal)
    Property INDTIPISRUTZITEOCD As Nullable(Of Decimal)
    Property DESUTZITEOCDACOCMC As String
    Property DATGRCRLC As Nullable(Of Date)
    Property CODUTZITEOCDACOCMC As Nullable(Of Decimal)
    Property CODFNC As Nullable(Of Decimal)



    Property NOMFNC As String
    ReadOnly Property TIPUTLZ As String
        Get
            If INDTIPISRUTZITEOCD = 0 Then
                Return "Gasto"
            Else
                Return "Estorno"
            End If
        End Get
    End Property

    ReadOnly Property VlrUtz As Nullable(Of Decimal)
        Get
            Return VLRUTZITEOCDACOCMC * QDEUTZITEOCDACOCMC
        End Get
    End Property
End Class
