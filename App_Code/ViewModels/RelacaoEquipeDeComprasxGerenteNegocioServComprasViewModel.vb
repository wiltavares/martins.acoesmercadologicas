﻿Public Class RelacaoEquipeDeComprasxGerenteNegocioServComprasViewModel

    Property CODDIVCMP As Nullable(Of Decimal)
    Property CODDRTCMP As Nullable(Of Decimal)
    Property CODGERPRD As Nullable(Of Decimal)
    Property CODGRPMERFRC As Nullable(Of Decimal)
    Property DATDSTDIVCMP As Date
    Property DESDIVCMP As String
    Property PERRDCMRGBRTFOB As Nullable(Of Decimal)
    Property VLRMNMFATMER As Nullable(Of Decimal)


    Public Property CODDESDIVCMP() As String
        Get
            Return String.Format("{0} - {1}", Me.CODDIVCMP, Me.DESDIVCMP)
        End Get
        Set(ByVal value As String)
        End Set
    End Property

End Class
