﻿
Public Class BeneficiarioViewModel

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODBFC As Decimal?

    Property DESBFC As String

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATINIVLDBFC As DateTime?

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATFIMVLDBFC As DateTime?


    Public Property CODDESBFC() As String
        Get
            Return String.Format("{0} - {1}", Me.CODBFC, Me.DESBFC)
        End Get
        Set(ByVal value As String)
        End Set
    End Property


End Class
