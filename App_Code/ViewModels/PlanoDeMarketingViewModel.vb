﻿Imports System.ComponentModel.DataAnnotations

Public Class PlanoDeMarketingViewModel

    <Key>
    <Required(ErrorMessage:="Preencha o campo Tipo de Evento")>
    Property TIPEVTACOCMC As Nullable(Of Decimal)

    <Key>
    <Required(ErrorMessage:="Preencha o campo Evento")>
    Property CODEVTACOCMC As Nullable(Of Decimal)

    <Key>
    <Required(ErrorMessage:="Preencha o Data de Referência")>
    Property ANOMESREF As Nullable(Of Decimal)

    <Key>
    <Required(ErrorMessage:="Preencha o campo Unidade de Negócio")>
    Property CODUNDESRNGC As Nullable(Of Decimal)

    Property VLROCDPLNMKT As Nullable(Of Decimal)

    Property VLRRSVPLNMKT As Nullable(Of Decimal)

    Property VLRUTZPLNMKT As Nullable(Of Decimal)

    Property VLROCDCTTPLNMKT As Nullable(Of Decimal)

    Property VLROCDEXACTTPLNMKT As Nullable(Of Decimal)


    Property NOMTIPEVTACOCMC As String
    Property NOMEVTACOCMC As String
    Property DESUNDESRNGC As String

    <Required(ErrorMessage:="Preencha o Data de Referência")>
    Public Property ANOMESREFDAT() As Nullable(Of DateTime)
        Get
            If (IsNothing(ANOMESREF) OrElse ANOMESREF = 0) Then
                Return Nothing
            Else
                Dim retorno As DateTime = New DateTime(CInt(ANOMESREF.ToString.Substring(0, 4)), CInt(ANOMESREF.ToString.Substring(4, 2)), Date.Now.Day)
                Return retorno
            End If
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            If Not IsNothing(value) Then
                ANOMESREF = CInt(value.Value.Year * 100 + value.Value.Month)
            End If

        End Set
    End Property

    Property SLDMESPLNMKT As Nullable(Of Decimal)
    Property SLDATUPLNMKT As Nullable(Of Decimal)
    Property SLDANOPLNMKT As Nullable(Of Decimal)
    Property VLRGSTANOPLNMKT As Nullable(Of Decimal)
    Property VLROCDANOPLNMKT As Nullable(Of Decimal)
End Class
