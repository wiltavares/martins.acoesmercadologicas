﻿Imports System.ComponentModel.DataAnnotations

Public Class GrupoAcessoViewModel
    Property NOMSISINF As String

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODGRPACS As Nullable(Of Decimal)

    Property DESGRPACS As String
    Property DESGRPRDE As String

    Property CODEMP As Nullable(Of Decimal)
    Property ControleAcessos As List(Of ControleAcessoViewModel)


    Property CodMenuCorrente As String
End Class
