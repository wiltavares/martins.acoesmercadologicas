﻿Imports System.ComponentModel.DataAnnotations

Public Class FilialEmpresaCorporativoViewModel

    Property CODEMP As Nullable(Of Decimal)
    Property CODFILEMP As Nullable(Of Decimal)
    Property NOMFILEMP As String
    Property ENDFILEMP As String
    Property CODBAIFILEMP As Nullable(Of Decimal)
    Property CODCPLBAIFILEMP As Nullable(Of Decimal)
    Property CODCEPFILEMP As Nullable(Of Decimal)
    Property NUMCGCFILEMP As String
    Property NUMINSESTFILEMP As String
    Property DATCADFILEMP As Date
    Property DATDSTFILEMP As Date
    Property FLGFILEMPEPD As String
    Property FLGFILEMPFAT As String
    Property FLGFILEMPMTZ As String
    Property FLGFILEMPCDA As String
    Property FLGFILEMPCENREG As String
    Property FLGFILEMPTRNMERRVD As String
    Property FLGFILEMPENRNOTFSC As String
    Property CODCENREG As Nullable(Of Decimal)
    Property CODFILEMPCTB As Nullable(Of Decimal)
    Property CODNACATIEMP As Nullable(Of Decimal)
    Property PERALQSEGACNTRB As Nullable(Of Decimal)
    Property NUMFNDPVDAPOSOCTCR As Nullable(Of Decimal)
    Property CODTCRPVDSOC As Nullable(Of Decimal)
    Property CODPGTGUAPVDSOC As Nullable(Of Decimal)
    Property VLRDNSMERREF As Nullable(Of Decimal)
    Property TIPFILEMPSBTTBT As Nullable(Of Decimal)
    Property TIPFILEMPSBTTBTPCO As Nullable(Of Decimal)
    Property TIPCONETQENRSAI As Nullable(Of Decimal)
    Property NUMINSMNC As String
    Property NUMAVR As String
    Property DATABRFSC As Date
    Property DATBXAFSC As Date
    Property CODFILEMPVNDDTO As Nullable(Of Decimal)
    Property CODEANFILEMP As String
    Property DESABVFILEMP As String
    Property INDFILATVPRSETRCMS As Nullable(Of Decimal)
    Property DATATVPRSETRCMSFIL As Date
    Property PERMAXVARALTCSTCAD As Nullable(Of Decimal)
    Property IDTULTGRPCRG As Nullable(Of Decimal)
    Property CODFILEMPTBT As Nullable(Of Decimal)
    Property NUMTLFFILEMPTBT As String
    Property NUMFAXFILEMPTBT As String
    Property CODCRFLICFUNPRDCON As String
    Property NUMSEQLIVFSCSVC As Nullable(Of Decimal)
    Property NUMPAGLIVFSCSVC As Nullable(Of Decimal)
    Property QDELIMPAGLIVFSCSVC As Nullable(Of Decimal)
    Property INDISNCRDCMB As Nullable(Of Decimal)
    Property INDCALPTOVNDFILEPD As Nullable(Of Decimal)
    Property CODCIDPCOACOCMC As Nullable(Of Decimal)
    Property NUMSEQCRGSEP As Nullable(Of Decimal)
    Property TIPFILARDTBT As Nullable(Of Decimal)
    Property CODTXTFILEMPEXT As String
    Property CODUNDNGC As Nullable(Of Decimal)
    Property CODUNDNGCADMEMP As Nullable(Of Decimal)
    Property NUMENDFILEMP As String
    Property VLRLTTENDFILEMP As Nullable(Of Decimal)
    Property VLRLNTENDFILEMP As Nullable(Of Decimal)
    Property CODHUB As Nullable(Of Decimal)
    Property FLGFILEMPARMGRL As String
    Property INDFILEPDOPEREG As Nullable(Of Decimal)
    Property CODFILEMPEMSNOTSVC As Nullable(Of Decimal)
    Property FLGFILEMPCMCETN As String
    Property INDFILDTB As Nullable(Of Decimal)
    Property INDFILEXVRCA As Nullable(Of Decimal)


    Property CODCID As Nullable(Of Decimal)
    Property NOMCID As String

    Public Property DROPDESC() As String
        Get
            Return CODFILEMP.ToString + " - " + NOMFILEMP
        End Get
        Set(ByVal value As String)
        End Set
    End Property

    ReadOnly Property CODFILIAL_NOMFILIAL() As String
        Get
            Return CODFILEMP & " - " & NOMFILEMP
        End Get
    End Property

End Class
