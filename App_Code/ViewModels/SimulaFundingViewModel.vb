﻿Imports Microsoft.VisualBasic

Public Class SimulaFundingViewModel

    Property CODFRN As Nullable(Of Decimal)
    Property DESFRN As String
    Property CODMER As Nullable(Of Decimal)
    Property DESMER As String
    Property PARTICIPACAO As Nullable(Of Decimal)
    Property QUANTIDADE As Nullable(Of Decimal)
    Property VLRUNT As Nullable(Of Decimal)
    Property CODFILEMP As Nullable(Of Decimal)
    Property DESFILEMP As String


    ReadOnly Property Total As Nullable(Of Decimal)
        Get
            If (IsNothing(QUANTIDADE) OrElse IsNothing(VLRUNT)) Then
                Return Nothing
            End If
            Return Math.Round((QUANTIDADE.Value * VLRUNT.Value), 4)
        End Get
    End Property

    ReadOnly Property TotalCliente As Nullable(Of Decimal)
        Get
            If (IsNothing(QUANTIDADE) OrElse IsNothing(VLRUNT) OrElse IsNothing(PARTICIPACAO)) Then
                Return Nothing
            End If
            Return Math.Round(Total.Value * (PARTICIPACAO.Value / 100), 4)
        End Get
    End Property

    ReadOnly Property UnitarioCliente As Nullable(Of Decimal)
        Get
            If (IsNothing(VLRUNT) OrElse IsNothing(PARTICIPACAO)) Then
                Return Nothing
            End If
            Return Math.Round(VLRUNT.Value * (PARTICIPACAO.Value / 100), 4)
        End Get
    End Property
    ReadOnly Property UnitarioCalculado As Nullable(Of Decimal)
        Get
            If (Not IsNothing(UnitarioCliente)) Then
                Return (VLRUNT - UnitarioCliente)
            Else
                Return VLRUNT
            End If
        End Get
    End Property

    ReadOnly Property TotalCalculado As Nullable(Of Decimal)
        Get
            If (Not IsNothing(TotalCliente)) Then
                Return (Total - TotalCliente)
            Else
                Return Total
            End If
        End Get
    End Property


End Class
