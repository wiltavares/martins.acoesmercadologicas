﻿Imports System.ComponentModel.DataAnnotations

Public Class RelacaoFuncionarioGerenteMarketingViewModel

    Property CODFNCGERMKT As Nullable(Of Decimal)
    Property CODFNCEMTACOCMC As Nullable(Of Decimal)

    Property CODFUNC_GER_MTK As Nullable(Of Decimal)
    Property NOMUNC_GER_MTK As String

    Property CODFUNC_EMITENTE As Nullable(Of Decimal)
    Property NOMFUNC_EMITENTE As String

End Class
