﻿Imports System.ComponentModel.DataAnnotations
Public Class EventoViewModel

    <Key>
    Property CODEVTACOCMC As Nullable(Of Decimal)

    <Required(ErrorMessage:="Preencha o campo Evento")>
    <MaxLength(30, ErrorMessage:="Máximo 30 caracteres")>
    <MinLength(2, ErrorMessage:="Mínimo 2 caracteres")>
    <WebDisplayName("Evento")>
    Property NOMEVTACOCMC As String

    <Required(ErrorMessage:="Preencha o campo Tipo Evento")>
    Property TIPEVTACOCMC As Nullable(Of Decimal)
    Property INDEVTADIACOCMC As Nullable(Of Decimal)

    <Required(ErrorMessage:="Preencha o campo Empenho")>
    Property TIPDSNDSCBNF As Nullable(Of Decimal)
    Property INDEVTMTDPOLDVR As Nullable(Of Decimal)

    <Required(ErrorMessage:="Preencha o campo Vinculação Promoção")>
    Property INDVCLPMC As Nullable(Of Decimal)


    Property NOMTIPEVTACOCMC As String
    Property DESDSNDSCBNF As String
    Property CODIGO_NOME As String

    Public Property INDEVTADIACOCMCDES() As String
        Get
            If INDEVTADIACOCMC = 0 Then
                Return "Não"
            Else
                Return "Sim"
            End If
        End Get
        Set(ByVal value As String)
            If value = "Sim" OrElse value = "1" Then
                INDEVTADIACOCMC = 1
            ElseIf value = "Não" OrElse value = "0"
                INDEVTADIACOCMC = 0
            Else
                INDEVTADIACOCMC = 2
            End If
        End Set
    End Property

    Public Property INDEVTMTDPOLDVRDES() As String
        Get
            If INDEVTMTDPOLDVR = 0 Then
                Return "Não"
            Else
                Return "Sim"
            End If
        End Get
        Set(ByVal value As String)
            If value = "Sim" OrElse value = "1" Then
                INDEVTMTDPOLDVR = 1
            ElseIf value = "Não" OrElse value = "0"
                INDEVTMTDPOLDVR = 0
            Else
                INDEVTMTDPOLDVR = 2
            End If
        End Set
    End Property


    Public Property INDVCLPMCDES() As String
        Get
            If INDVCLPMC = 0 Then
                Return "Não"
            Else
                Return "Sim"
            End If
        End Get
        Set(ByVal value As String)
            If value = "Sim" OrElse value = "1" Then
                INDVCLPMC = 1
            ElseIf value = "Não" OrElse value = "0" Then
                INDVCLPMC = 0
            Else
                INDVCLPMC = 2
            End If
        End Set
    End Property

    Public Property DROPDESC() As String
        Get
            Return CODEVTACOCMC.ToString + " - " + NOMEVTACOCMC
        End Get
        Set(ByVal value As String)
            'NOMEVTACOCMC = value.Split("-")(1)
            'CODEVTACOCMC = CDec(value.Split("-")(0))
        End Set
    End Property

End Class
