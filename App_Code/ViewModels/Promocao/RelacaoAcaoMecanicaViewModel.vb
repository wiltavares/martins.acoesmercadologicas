﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

Public Class RelacaoAcaoMecanicaViewModel
    Property CodCombo As Nullable(Of Decimal)
    Property NomCombo As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal _CodCombo As Decimal, ByVal _NomCombo As String)
        Me.CodCombo = _CodCombo
        Me.NomCombo = _NomCombo
    End Sub

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODACOCMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)

    <Required>
    <MaxLength(180, ErrorMessage:="Máximo {1} caracteres")>
    Property DESRGRACOCMC As String

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPPMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEGRPMIXPMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEFXAPMC As Nullable(Of Decimal)

    <Required>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDPTCTOTMIXPMC As Nullable(Of Decimal)

    'campo auxilicar de data
    Property DataIniAux As String
    Property DataFimAux As String

    'CODIGO DO FUNCIONARIO CRIADOR DA ACAO COMERCIAL
    Property CODFNCCRIACOCMC As Decimal?

    'Public Property INDPTCTOTMIXPMCDES() As String
    '    Get
    '        If INDPTCTOTMIXPMC = 1 Then
    '            Return "Sim"
    '        Else
    '            Return "Não"
    '        End If
    '    End Get
    '    Set(ByVal value As String)
    '        If value = "Sim" OrElse value = "1" Then
    '            INDPTCTOTMIXPMC = 1
    '        ElseIf value = "Não" OrElse value = "0" Then
    '            INDPTCTOTMIXPMC = 0
    '        End If
    '    End Set
    'End Property




















    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFILEMP As Nullable(Of Decimal)

    Property DESABVFILEMP As String

    Property CodFormaCalculo As Nullable(Of Decimal)
    Property QtdBrindesAting As Nullable(Of Decimal)


    Property RelacaoAcaoMecanicaFilialList As List(Of RelacaoAcaoMecanicaFilialViewModel)
    Property RELACAOACAOMECANICAREGISTROSLIST As List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel)
    Property RELACAOACAOMECANICAREGISTROSCOMBINADALIST As List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel)
    Property RELACAOACAOMECANICAREGISTROSFAIXALIST As List(Of RelacaoAcaoMecanicaCriterioDeApuracaoViewModel)
    Property RelacaoAcaoMecanicaPublicoAlvoEstadoList As List(Of RelacaoAcaoMecanicaPublicoAlvoEstadoViewModel)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDTIPPTOPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:D2}", ApplyFormatInEditMode:=True)>
    Property VLRLIMMAXCTR As Nullable(Of Decimal)


    <DisplayFormat(DataFormatString:="{0:D2}", ApplyFormatInEditMode:=True)>
    Property VLRLIMMNMFXACTRPMC As Nullable(Of Decimal)
    Property QDEPTOFXACTRPMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEQCCALCTRPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODCTRPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property NUMSEQCTRACOCMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODGRPMIXMERPMC As Nullable(Of Decimal)





    <DisplayName("Nome da Ação")>
    Property NOMACOCMC As String

    <DisplayName("Status da Ação")>
    Property CODSTAAPVACOCMC As Nullable(Of Decimal)

    <DisplayName("Nome do Emitente")>
    Property NOMFNC As String

    <DisplayName("Status Ação")>
    Property TIPACOMCD As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    <DisplayName("Geração da Ação")>
    Property DATGRCACOCMC As Nullable(Of DateTime)

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    <DisplayName("Validade Início")>
    Property DATINIACOCMC As Nullable(Of DateTime)

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    <DisplayName("Validade Fim")>
    Property DATFIMACOCMC As Nullable(Of DateTime)

    <DisplayFormat(DataFormatString:="{0:c2}", ApplyFormatInEditMode:=True)>
    <DisplayName("Total Valor Orçamento")>
    Property VLRTOTACOCMC As Nullable(Of Decimal)




    'CODIGO ACAO COMERCIAL COPIA (Ação que originou o clone)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODACOCMCCPI As Nullable(Of Decimal)
End Class

