﻿Imports System.ComponentModel.DataAnnotations

Public Class RelacaoCriterioPromocaoPromocaoTipoEntidadeViewModel
    ' TIPO DA ENTIDADE (REPRES., PEDIDO, CARGA, CLIENTE, ETC)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)

    ' CODIGO PROMOCAO DE VENDAS
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)

    ' CODIGO CRITERIO PROMOCAO DE VENDAS
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODCTRPMC As Nullable(Of Decimal)

    ' VALOR INFERIOR DA FAIXA DE PONTUACAO DE CRITERIOS DAS PROMOCOES
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property VLRLIMMNMFXACTRPMC As Nullable(Of Decimal)

    ' VALOR SUPERIOR DA FAIXA DE PONTUACAO DE CRITERIOS DAS PROMOCOES
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property VLRLIMMAXFXACTRPMC As Nullable(Of Decimal)

    ' QUANTIDADE PONTOS FAIXA DE PONTUACAO DOS CRITERIOS DAS PROMOCOES
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEPTOFXACTRPMC As Nullable(Of Decimal)

    'CODIGO GRUPO MIX MERCADORIA PROMOCAO                                  
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODGRPMIXMERPMC As Nullable(Of Decimal)


    'DESCRICAO CRITERIO DO MIX DA PROMOCAO                                 
    Property DESCTRMIXPMC As String


    'TIPO ENTIDADE NIVEL PREMIO PROMOCAO                                    
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEDENIVPRMPMC As Nullable(Of Decimal)

End Class
