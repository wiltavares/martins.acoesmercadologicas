﻿Imports System.ComponentModel.DataAnnotations

Public Class CalculoPontuacaoPromocaoEmpresaViewModel
    'TIPO DA EQUACAO DE CALCULO DE CRITERIO DA PROMOCAO DE VENDA
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEQCCALCTRPMC As Nullable(Of Decimal)

    'DESCRICAO DA EQUACAO DE CALCULO DE CRITERIO DA PROMOCAO DE VENDA
    Property DESEQCCALCTRPMC As String

End Class
