﻿Imports Microsoft.VisualBasic

Public Class PromocaoCriterioApuracaoViewModel
    Property CODPMC As Nullable(Of Decimal)
    Property ParametrosCalculo As List(Of RelacaoPromocaoCriterioApuracaoParamCalculoViewModel)

    Property CodFormaCalculo As Decimal
    Property QDEPTOFXAEQCCALCTR As Nullable(Of Decimal)
    Property QDEPTOFXACTRPMC As Nullable(Of Decimal)
    Property CODCTRPMC As Nullable(Of Decimal)
    Property INDTIPPTOPMC As Nullable(Of Decimal)
    Property VLRLIMMAXCTR As Nullable(Of Decimal)

    ReadOnly Property INDTIPPTOPMC_DESC As String
        Get
            If (INDTIPPTOPMC = 0) Then
                Return "0 - Brinde"
            ElseIf (INDTIPPTOPMC = 1) Then
                Return "1 - Percentual Cash Back"
            ElseIf (INDTIPPTOPMC = 2) Then
                Return "2 - Valor Cash Back"
            End If
            Return ""
        End Get
    End Property






#Region "Campos para validação"
    Property TIPSITPMC As String
    Property Combinada As Boolean
#End Region
End Class
