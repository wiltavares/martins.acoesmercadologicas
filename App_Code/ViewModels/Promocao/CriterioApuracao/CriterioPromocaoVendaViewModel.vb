﻿Imports System.ComponentModel.DataAnnotations

Public Class CriterioPromocaoVendaViewModel
    'TIPO DA ENTIDADE (REPRES., PEDIDO, CARGA, CLIENTE, ETC)
    '<DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)

    'CODIGO CRITERIO PROMOCAO
    '<DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODCTRPMC As Nullable(Of Decimal)

    'DESCRICAO CRITERIO PROMOCAO
    Property DESCTRPMC As String

End Class
