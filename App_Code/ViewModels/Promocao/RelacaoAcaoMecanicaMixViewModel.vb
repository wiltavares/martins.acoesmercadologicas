﻿Imports System.ComponentModel.DataAnnotations

Public Class RelacaoAcaoMecanicaMixViewModel
    Property CODACOCMC As Nullable(Of Decimal)
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    Property NUMSEQMIXACOCMC As Nullable(Of Decimal)
    <Display(Name:="Grupo Mercadoria")>
    Property CODGRPMER As Nullable(Of Decimal)
    <Display(Name:="Família Mercadoria")>
    Property CODFMLMER As Nullable(Of Decimal)
    <Display(Name:="Classe Mercadoria")>
    Property CODCLSMER As Nullable(Of Decimal)
    <Display(Name:="Mercadoria")>
    Property CODMER As Nullable(Of Decimal)
    <Display(Name:="Fornecedor")>
    Property CODFRN As Nullable(Of Decimal)

    <Display(Name:="Faixa")>
    Property CODGRPMIXMERPMC As Nullable(Of Decimal)
#Region "Propriedades Extra"
    Property DATDSTGRPMER As Nullable(Of Decimal)
    Property DESGRPMER As String
    Property DESFMLMER As String
    Property DESCLSMER As String
    Property DESMER As String
    Property NOMFRN As String
    Property INDGRUPOMERCADORIA As Nullable(Of Decimal)
    Property INDFAMILIAMERCADORIA As Nullable(Of Decimal)
    Property INDCLASSEMERCADORIA As Nullable(Of Decimal)
    Property INDMERCADORIA As Nullable(Of Decimal)
    Property INDFORNECEDOR As Nullable(Of Decimal)
    Property ListMixFamiliaMercadoria As List(Of RelacaoAcaoMecanicaMixViewModel)
    Property relacaoAcaoMecanicaMixGrupoMercadoria As List(Of RelacaoAcaoMecanicaMixViewModel)


    Property CODDRTCMP As Nullable(Of Decimal)
    Property CODDIVCMP As Nullable(Of Decimal)
    Property CODPMC As Nullable(Of Decimal)
    Property TIPFRNACOCMC As Nullable(Of Decimal)


#End Region
End Class
