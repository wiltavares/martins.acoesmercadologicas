﻿Imports Microsoft.VisualBasic

Public Class RelacaoAcaoMecanicaClientesExcludentesViewModel
    Property CODACOCMC As Nullable(Of Decimal)
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODCLI As Nullable(Of Decimal)
    Property NOMCLI As String
End Class
