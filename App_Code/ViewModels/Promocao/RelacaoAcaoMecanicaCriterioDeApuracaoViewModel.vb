﻿Imports Microsoft.VisualBasic

Public Class RelacaoAcaoMecanicaCriterioDeApuracaoViewModel

    Property CODACOCMC As Nullable(Of Decimal)
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    Property NUMSEQCTRACOCMC As Nullable(Of Decimal)
    Property CODCTRPMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEDENIVPRMPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F2}", ApplyFormatInEditMode:=True)>
    Property VLRLIMMNMFXACTRPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F2}", ApplyFormatInEditMode:=True)>
    Property VLRLIMMAXFXACTRPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDTIPPTOPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:D2}", ApplyFormatInEditMode:=True)>
    Property VLRLIMMAXCTR As Nullable(Of Decimal)
    Property QDEPTOFXACTRPMC As Nullable(Of Decimal)
    Property DESCTRMIXPMC As String
    Property CODGRPMIXMERPMC As Nullable(Of Decimal)
    Property TAMIVLFXAEQCCALCTR As Nullable(Of Decimal)
    Property TIPEQCCALCTRPMC As Nullable(Of Decimal)

    'Propriedade Extra
    Property TIPPMC As Nullable(Of Decimal)

End Class
