﻿Imports System.ComponentModel.DataAnnotations

Public Class PromocaoBrindeViewModel
    ' TIPO ENTIDADE DASEG WITH GRANT OPTIONO ALVO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)

    ' CODIGO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)

    ' TIPO ENTIDADE NIVEL PREMIACAO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEDENIVPRMPMC As Nullable(Of Decimal)

    ' CODIGO PREMIO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPRMPMC As Nullable(Of Decimal)

    ' QUANTIDADE MERCADORIA MAXIMA PREMIO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEMERMAXPRMPMC As Nullable(Of Decimal)

    ' QUANTIDADE MERCADORIA VENDIDA PREMIO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEMERVNDPRMPMC As Nullable(Of Decimal)

    ' PERCENTUAL MERCADORIA RESERVA PREMIO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property PERMERRSVPRMPMC As Nullable(Of Decimal)

    ' VALOR DO PREMIO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property VLRPRMPMC As Nullable(Of Decimal)

    ' DATA DE EXCLUSAO DO PRODUTO                                           
    Property DATECSPRD As Nullable(Of DateTime)

    ' CODIGO DO FUNCIONARIO QUE EXCLUIU                                     
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFNCECS As Nullable(Of Decimal)

    ' NOME DO FUNCIONARIO QUE EXCLUIU                                     
    Property NOMFNCECS As String

    ' DESCRICAO PREMIO PROMOCAO
    Property DESPRMPMC As String

    ' INDICA TRANSMISSAO MENSAGEM AO FINAL PREMIO PROMOCAO
    Property FLGTRSMSGFIMPRMPMC As String

    'INDICA QUE EXISTEM BRINDES ADICIONAIS NA PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDISRBDEADI As Nullable(Of Decimal)

    'CODIGO CONDICAO DE PAGAMENTO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODCNDPGT As Nullable(Of Decimal)


    'VALOR UNITARIO VENDA PREMIO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property VLRUNTVNDPRMPMC As Nullable(Of Decimal)

    'VALOR UNITARIO BRINDE
    <DisplayFormat(DataFormatString:="{0:C2}", ApplyFormatInEditMode:=True)>
    Property VLRUNTLIQMER As Nullable(Of Decimal)

    'INDICADOR DE ACUMULACAO DE BRINDES DA PROMOCAO NO FATURAMENTO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDACUBDEPMCFAT As Nullable(Of Decimal)

    'QUANTIDADE DE BRINDE A SER MULTIPLICADO PELO CRITERIO PROMOCAO        
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEBDEMPLCTRPMC As Nullable(Of Decimal)

    'VALOR UNITARIO FUNDING MERCADORIA PROMOCAO                            
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property VLRUNTFNDMERPMC As Nullable(Of Decimal)


    'PORCENTAGEM BENEFICIO BRINDE  
    <DisplayFormat(DataFormatString:="{0:F2}", ApplyFormatInEditMode:=True)>
    Property PERBFCBDE As Nullable(Of Decimal)

    'CUSTO VLRUNTPCOBRT
    <DisplayFormat(DataFormatString:="{0:F2}", ApplyFormatInEditMode:=True)>
    Property CUSTO As Nullable(Of Decimal)

    'PRECO MEDIO VENDA
    <DisplayFormat(DataFormatString:="{0:C2}", ApplyFormatInEditMode:=True)>
    Property VLRUNTPCOBRT As Nullable(Of Decimal)

    Property RATEAR As Boolean


#Region "Campos Adicionais"

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property SALDO As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property MEDIA As Nullable(Of Decimal)

    Property NOMPRMPMC As String

    'Codigo mercadoria
    '<DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    'Property CODMER As Nullable(Of Decimal)

    'Property CODITEOCDACOCMC As String

    Property CODACOCMC As Decimal
#End Region

End Class
