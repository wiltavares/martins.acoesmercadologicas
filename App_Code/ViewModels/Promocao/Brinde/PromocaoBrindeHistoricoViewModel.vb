﻿Imports Microsoft.VisualBasic
Public Class PromocaoBrindeHistoricoViewModel
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFNC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode:=True)>
    Property DATGRCHST As Nullable(Of DateTime)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEDENIVPRMPMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODMER As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property NUMSEQ As Nullable(Of Decimal)

    Property INDSITMER As Nullable(Of Decimal)


    Property DESPRMPMC As String
    Property NOMFNC As String
End Class
