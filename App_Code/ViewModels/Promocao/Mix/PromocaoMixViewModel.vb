﻿Imports System.ComponentModel.DataAnnotations

Public Class PromocaoMixViewModel
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)
    Property NUMSEQMIXACOCMC As Nullable(Of Decimal)
    <Display(Name:="Grupo Mercadoria")>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODGRPMER As Nullable(Of Decimal)
    <Display(Name:="Família Mercadoria")>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFMLMER As Nullable(Of Decimal)
    <Display(Name:="Classe Mercadoria")>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODCLSMER As Nullable(Of Decimal)
    <Display(Name:="Mercadoria")>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODMER As Nullable(Of Decimal)
    <Display(Name:="Fornecedor")>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFRN As Nullable(Of Decimal)

    <Display(Name:="Faixa")>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODGRPMIXMERPMC As Nullable(Of Decimal)


    <Display(Name:="Processado")>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDVLDRGRMIXPMC As Nullable(Of Decimal)

#Region "Propriedades Extra"
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    Property DATDSTGRPMER As Nullable(Of Decimal)
    Property DESGRPMER As String
    Property DESFMLMER As String
    Property DESCLSMER As String
    Property DESMER As String
    Property NOMFRN As String
    Property INDGRUPOMERCADORIA As Nullable(Of Decimal)
    Property INDFAMILIAMERCADORIA As Nullable(Of Decimal)
    Property INDCLASSEMERCADORIA As Nullable(Of Decimal)
    Property INDMERCADORIA As Nullable(Of Decimal)
    Property INDFORNECEDOR As Nullable(Of Decimal)
    Property ListMixFamiliaMercadoria As List(Of PromocaoMixViewModel)
    Property PromocaoMixGrupoMercadoria As List(Of PromocaoMixViewModel)


    Property CODDRTCMP As Nullable(Of Decimal)
    Property CODDIVCMP As Nullable(Of Decimal)

    Property CODPMCtoFind As Nullable(Of Decimal)

    Property QDEMNMMERMIX As Nullable(Of Decimal)
    Property QDEMAXMERMIX As Nullable(Of Decimal)
    Property INDITEOBR As Nullable(Of Decimal)
    Property INDJRNSMA As Nullable(Of Decimal)
    Property TIPFRNACOCMC As Nullable(Of Decimal)
    Property INDCPTFCH As Nullable(Of Decimal)
    Property TIPSITPMC As String

#End Region
End Class
