﻿Imports Microsoft.VisualBasic
Public Class PromocaoHistoricoSituacaoViewModel
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property NUMSEQ As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFNC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode:=True)>
    Property DATGRCHST As Nullable(Of DateTime)

    Property TIPSITPMC As String
    Property DESSITPMC As String
    Property NOMFNC As String
End Class
