﻿Imports System.ComponentModel.DataAnnotations

Public Class CriterioAdicionalAvaliacaoPromocaoViewModel
    'TIPO ENTIDADE PUBLICO ALVO PROMOCAO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)

    'CODIGO PROMOCAO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)

    'CODIGO CRITERIO AVALIAR PROMOCAO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODCTRAVLPMC As Nullable(Of Decimal)

    'FATOR ACRESCIMO CRITERIO AVALIAR PROMOCAO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property FTRACRCTRAVLPMC As Nullable(Of Decimal)

    'VALOR LIMITE MAXIMO CRITERIO AVALIAR PROMOCAO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property VLRLIMMAXCTRAVLPMC As Nullable(Of Decimal)

    'QUANTIDADE MINIMO ITEM PEDIDO PROMOCAO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEMNMITEPEDPMC As Nullable(Of Decimal)
End Class
