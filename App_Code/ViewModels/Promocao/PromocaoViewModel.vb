﻿Imports System.ComponentModel.DataAnnotations

Public Class PromocaoViewModel
    ' TIPO ENTIDADE DASEG WITH GRANT OPTIONO ALVO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)

    ' CODIGO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)

    ' DATA INICIO PROMOCAO
    <Required(ErrorMessage:="(*) Data de Início da Promoção Inválida!")>
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATINIPMC As Nullable(Of Date)

    ' DATA FIM PROMOCAO
    <Required(ErrorMessage:="(*) Data de Término da Promoção Inválida!")>
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATFIMPMC As Nullable(Of Date)

    ' DESCRICAO PROMOCAO
    Property DESPMC As String

    ' DATA HORA GERACAO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATHRAGRCPMC As Nullable(Of DateTime)

    ' CODIGO FUNCIONARIO GEROU PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFNCGRCPMC As Nullable(Of Decimal)

    ' INDICA SE FOI SOLICITADO LISTAGEM PROMOCAO
    Property FLGSLCLSTPMC As String

    ' NOME PROMOCAO
    <Required(ErrorMessage:="(*) Informe o Nome da Promoção!")>
    <MaxLength(25)>
    Property NOMPMC As String

    ' VALOR VERBA PROMOCAO
    <DisplayFormat(DataFormatString:="{0:C}", ApplyFormatInEditMode:=True)>
    Property VLRVBAPMC As Nullable(Of Decimal)

    ' QUANTIDADE PONTOS COMPETICAO VENDA
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEPTOCOPVND As Nullable(Of Decimal)

    ' TIPO SITUACAO PROMOCAO
    Property TIPSITPMC As String

    ' CODIGO FUNCIONARIO ALTEROU SITUACAO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFNCALTSITPMC As Nullable(Of Decimal)

    ' DATA HORA ALTERACAO SITUACAO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATHRAALTSITPMC As Nullable(Of DateTime)

    ' DATA PROCESSAMENTO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATPCSPMC As Nullable(Of Date)

    ' TIPO APURACAO PROMOCAO
    Property TIPAPUPMC As String

    ' CODIGO FUNCIONARIO ALTEROU PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFNCALTPMC As Nullable(Of Decimal)

    ' DATA HORA ALTERACAO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATHRAALTPMC As Nullable(Of DateTime)

    'CODIGO EMPRESA
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODEMP As Nullable(Of Decimal)

    ' CODIGO FILIAL EMPRESA
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFILEMP As Nullable(Of Decimal)

    ' CODIGO DA ACAO COMERCIAL 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODACOCMC As Nullable(Of Decimal)

    ' CODIGO DA PROMOCAO PRINCIPAL 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMCPCP As Nullable(Of Decimal)

    ' QUANTIDADE BONIFICADA PROMOCAO CASADA 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEBNFPMCCAA As Nullable(Of Decimal)

    ' QUANTIDADE VENDIDA PROMOCAO CASADA 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEVNDPMCCAA As Nullable(Of Decimal)

    ' DATA INICIO DA PROMOCAO CASADA 
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATINIPMCCAA As Nullable(Of Date)

    ' DATA FINAL DA PROMOCAO CASADA 
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATFIMPMCCAA As Nullable(Of Date)

    ' FLAG DE CONTROLE DA PROMOCAO CASADA 
    Property FLGCONPMCCAA As String

    'FLAG ENVIO PROMOCAO 
    Property FLGENVPMC As String

    'NOME DO USUARIO DO SISTEMA 
    Property NOMUSRSIS As String

    'DESCRICAO COMERCIAL DA PROMOCAO 
    ' <DataType(DataType.MultilineText = 3)>
    Property DESCMCPMC As String

    'INDICADOR CRITERIO ADICIONAL AVALIACAO PROMOCAO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDCTRADIAVLPMC As Decimal

    'INDICADOR PARTICIPACAO PROMOCAO COMERCIO ELETRONICO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDPTCPMCCMCETN As Nullable(Of Decimal)

    'INDICADOR PARTICIPACAO PROMOCAO TELEMARKETING 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDPTCPMCTMK As Nullable(Of Decimal)

    'CODIGO UNIDADE ESTRATEGICO NEGOCIO 
    <Required(ErrorMessage:="(*) Informe o BU responsável pela promoção!")>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODUNDESRNGC As Nullable(Of Decimal)

    'INDICADOR APURACAO INCENTIVO QUANTIDADE BRINDE 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDAPUICTQDEBDE As Nullable(Of Decimal)

    'INDICA EXCLUSAO DOS ITENS DA PROMOCAO NO PEDIDO DE VENDA 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDECSITEPMCPEDVND As Nullable(Of Decimal)

    'INDICA PREMIO UNICO VIGORACAO PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDPRMUNCVGRPMC As Nullable(Of Decimal)

    'INDICADOR COBRANCA DO BRINDE NA PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDCOBBDEPMC As Decimal

    'TIPO DE PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPPMC As Decimal

    'VALOR BASE DE CALCULO PARA GRUPOS DO MIX DE PROMOCAO
    <DisplayFormat(DataFormatString:="{0:C}", ApplyFormatInEditMode:=True)>
    Property VLRBSECALGRPMIXPMC As Nullable(Of Decimal)

    'PERCENTUAL DE VARIACAO DOS GRUPOS NO MIX DA PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property PERVARGRPMIXPMC As Nullable(Of Decimal)

    'QUANTIDADE DE GRUPOS NO MIX DA PROMOCAO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEGRPMIXPMC As Nullable(Of Decimal)

    'INDICA PROMOCAO SISTEMA NOVO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDPMCSISNVO As Nullable(Of Decimal)

    'INDICADOR DE BRINDE FRACIONADO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDBDEFRC As Nullable(Of Decimal)

    'INDICA PARTICIPACAO DE TODO MIX NA PROMOCAO                           
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDPTCTOTMIXPMC As Nullable(Of Decimal)

    'QUANTIDADE MAXIMA DE BRINDE POR CLIENTE                               
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEMAXBDECLI As Nullable(Of Decimal)

    'QUANTIDADE DE FAIXA DA PROMOCAO                                       
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEFXAPMC As Nullable(Of Decimal)

    'INDICA QUE EXISTEM BRINDES ADICIONAIS NA PROMOCAO                     
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDISRBDEADI As Decimal

    'INDICADOR DE STATUS DA PROMOCAO NO RELATORIO                          
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDSTAPMCREL As Nullable(Of Decimal)

    'Indica promoções que participarão da venda simplificada               
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDPMCVNDSMP As Nullable(Of Decimal)

    'INDICA PROMOCAO VALIDA NO APLICATIVO MOVEL DO CLIENTE                 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDPMCVLDAPLMVLCLI As Nullable(Of Decimal)

    'TIPO DA EQUIPE DE VENDA                                               
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEQIVND As Nullable(Of Decimal)

    'FATOR ACRESCIMO CRITERIO AVALIAR PROMOCAO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property FTRACRCTRAVLPMC As Nullable(Of Decimal)

    'VALOR LIMITE MAXIMO CRITERIO AVALIAR PROMOCAO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property VLRLIMMAXCTRAVLPMC As Nullable(Of Decimal)

    'QUANTIDADE MINIMO ITEM PEDIDO PROMOCAO 
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEMNMITEPEDPMC As Nullable(Of Decimal)

    'NOME DA FILIAL
    Property NOMFILEMP As String
    Property CODFILEMPORIVBA As Nullable(Of Decimal)
    Property DESUNDESRNGC As String

    'QTD PARA VALIDACAO
    Property CODMER As Nullable(Of Decimal)
    Property DESMER As String
    Property QDEMIN As Nullable(Of Decimal)
    Property QDEPMC As Nullable(Of Decimal)
    Property QDEPREMIOATINGIMENTO As Nullable(Of Decimal)

    ReadOnly Property CodNomProm As String
        Get
            Return String.Format("{0} - {1}", CODPMC, DESPMC)
        End Get
    End Property

    ReadOnly Property CodNomFil As String
        Get
            Return String.Format("{0} - {1}", CODFILEMP, NOMFILEMP)
        End Get
    End Property

    Public Property INDCTRADIAVLPMCBool As Boolean
        Get
            Return INDCTRADIAVLPMC = 1
        End Get
        Set(ByVal value As Boolean)
            If value Then
                INDCTRADIAVLPMC = 1
            Else
                INDCTRADIAVLPMC = 0
            End If

        End Set
    End Property

    'brinde Adicional
    Public Property INDISRBDEADIBool As Boolean
        Get
            Return INDISRBDEADI = 1
        End Get
        Set(ByVal value As Boolean)
            If value Then
                INDISRBDEADI = 1
            Else
                INDISRBDEADI = 0
            End If

        End Set
    End Property

    'Cobrança de Brinde:
    Public Property INDCOBBDEPMCBool As Boolean
        Get
            Return INDCOBBDEPMC = 1
        End Get
        Set(ByVal value As Boolean)
            If value Then
                INDCOBBDEPMC = 1
            Else
                INDCOBBDEPMC = 0
            End If

        End Set
    End Property


    'Tipo
    Public Property TIPPMCBoll As Boolean
        Get
            Return TIPPMC = 0
        End Get
        Set(ByVal value As Boolean)
            If value = 1 Then
                TIPPMC = 1
            Else
                TIPPMC = 0
            End If

        End Set
    End Property '

    'Situação
    ReadOnly Property TIPSITPMCBoll As Boolean
        Get
            Select Case TIPSITPMC
                Case "I"
                    Return 1
                Case Else
                    Return 0
            End Select
        End Get
    End Property

    ReadOnly Property DESSITPMC As String
        Get
            Select Case TIPSITPMC
                Case "I"
                    Return "À Liberar"
                Case "E"
                    Return "Encerrada"
                Case " ", ""
                    Return "Liberada"
                Case "S"
                    Return "Suspensa"
                Case Else
                    Return ""
            End Select
        End Get
    End Property



    ReadOnly Property DesTIPPMC As String
        Get
            Select Case TIPPMC
                Case 0
                    Return "Normal"
                Case 1
                    Return "Combinada"
                Case Else
                    Return "Faixas Multiplas"
            End Select
        End Get
    End Property

    'Propriedades para verificar se a prmoção é de CASHBACK
    Property INDTIPPTOPMC1 As Nullable(Of Decimal)
    Property INDTIPPTOPMC2 As Nullable(Of Decimal)
    Property INDTIPPTOPMC As Boolean
        Get
            Return (INDTIPPTOPMC1 IsNot Nothing OrElse INDTIPPTOPMC2 IsNot Nothing)
        End Get

        Set(value As Boolean)

        End Set
    End Property

    Property INDHSTSITPMC As Nullable(Of Decimal)

    Property INDEVTSMA As Nullable(Of Decimal)
    Property INDCPTFCH As Nullable(Of Decimal)
    Property TIPNIVCLFCPT As Nullable(Of Decimal)
    Property TIPFRNACOCMC As Nullable(Of Decimal)
    Property NOMTIPFRNACOCMC As String

    Property TIPNATVND As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDPMCIMD As Nullable(Of Decimal)

    Public Property INDPMCIMDBool As Boolean
        Get
            If (IsNothing(INDPMCIMD)) Then
                Return False
            End If

            Return INDPMCIMD = 1
        End Get
        Set(ByVal value As Boolean)
            If value Then
                INDPMCIMD = 1
            Else
                INDPMCIMD = 0
            End If

        End Set
    End Property

    <DisplayFormat(DataFormatString:="{0:hh:mm}", ApplyFormatInEditMode:=True)>
    Property HRAINIPMC As Nullable(Of Date)

    <DisplayFormat(DataFormatString:="{0:hh:mm}", ApplyFormatInEditMode:=True)>
    Property HRAFIMPMC As Nullable(Of Date)

    Property CODEVTBTB As Nullable(Of Decimal)
    Property NOMEVTBTB As String

End Class
