﻿Imports System.ComponentModel.DataAnnotations

Public Class RelacaoAcaoMecanicaPublicoAlvoGerenteMercadoViewModel
    Property CODACOCMC As Nullable(Of Decimal)
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    Property CODSUP As Nullable(Of Decimal)
    Property NOMSUP As String

    Property CODGER As Nullable(Of Decimal)
    Property NOMGER As String
End Class
