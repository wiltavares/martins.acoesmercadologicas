﻿Imports System.ComponentModel.DataAnnotations

Public Class RelacaoAcaoMecanicaFilialViewModel
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODACOCMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFILEMP As Nullable(Of Decimal)

    Property DESABVFILEMP As String

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)

    Public DATECSUSR As Nullable(Of DateTime)

    'Propriedade extra    
    Property DESRGRACOCMC As String
End Class
