﻿Imports System.ComponentModel.DataAnnotations

Public Class RelacaoAcaoMecanicaPublicoAlvoRcaVendedorViewModel
    Property CODACOCMC As Nullable(Of Decimal)
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    Property CODREP As Nullable(Of Decimal)
    Property NOMREP As String

    'Propriedades Extra
    Property CODOPCAOFILTRO As Nullable(Of Decimal)
    Property DESOPCAOFILTRO As String
    Property NOMSUP As String
    Property CODSUP As Nullable(Of Decimal)
End Class
