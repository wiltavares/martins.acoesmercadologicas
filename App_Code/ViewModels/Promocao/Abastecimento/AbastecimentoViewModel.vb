﻿Imports System.ComponentModel.DataAnnotations

Public Class AbastecimentoViewModel
    Property CODUNDESRNGC As Nullable(Of Decimal)
    Property NOMFILEMP As String
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODCPR As Nullable(Of Decimal)
    Property NOMCPR As String
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFILEMP As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODACOCMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATINIPMC As Nullable(Of Date)
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATFIMPMC As Nullable(Of Date)

    Property DESPMC As String
    Property NOMPMC As String
    'Property CODPRMPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODMER As Nullable(Of Decimal)
    Property CODPRMPMC As Nullable(Of Decimal)

    Property DESMER As String
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property ETQBND As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEMERVNDPRMPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEMERMAXPRMPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property ETQSLD As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEDIACOB As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEDIARES As Nullable(Of Decimal)
    Property VALOR_BRINDE As Nullable(Of Decimal)
    Property CUSTO_BRINDE As Nullable(Of Decimal)
    Property MEDIA_SAIDA As Nullable(Of Decimal)
    Property TIPSITPMC As String
    Property CODFILEMPORIVBA As Nullable(Of Decimal)
    Property CODFNCCRIACOCMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property NUMSEQISRQDEMERPMC As Nullable(Of Decimal)
    Property INDTIPISRMERPMC As Nullable(Of Decimal)




    'Auxiliares
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATGRCRLC As Nullable(Of Date)
    <DisplayFormat(DataFormatString:="{0:C2}", ApplyFormatInEditMode:=True)>
    Property VLRTOTMERISRPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFNCGRCPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property INDISRVLDMERPMC As Nullable(Of Decimal)

    Property NOMFNCGRCPMC As String

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property qdeMerIsrPmc As Nullable(Of Decimal)
    'Property tipo As Nullable(Of Decimal)


    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFRNMER As Nullable(Of Decimal)
    Property NOMFRNMER As String


    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEDENIVPRMPMC As Nullable(Of Decimal)


    'Data de exclusão do brinde na promoção
    Property DATECSPRD As Nullable(Of DateTime)

End Class
