﻿Imports Microsoft.VisualBasic

Public Class TipoCriterioViewModel
    'Referente tabela MRT.T0107978
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    Property CODCTRPMC As Nullable(Of Decimal)
    Property DESCTRPMC As String
End Class
