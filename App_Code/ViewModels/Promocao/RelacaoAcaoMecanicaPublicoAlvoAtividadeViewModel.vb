﻿Imports System.ComponentModel.DataAnnotations

Public Class RelacaoAcaoMecanicaPublicoAlvoAtividadeViewModel
    Property CODACOCMC As Nullable(Of Decimal)
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)
    Property CODATI As Nullable(Of Decimal)
    Property NOMATI As String
    Property CODUNDESRNGC As Nullable(Of Decimal)
    Property DESUNDESRNGC As String
End Class
