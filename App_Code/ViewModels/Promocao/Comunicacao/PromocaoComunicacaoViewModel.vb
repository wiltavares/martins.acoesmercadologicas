﻿Imports System.ComponentModel.DataAnnotations

Public Class PromocaoComunicacaoViewModel


    ''' <summary>
    '''  TIPO ENTIDADE DASEG WITH GRANT OPTIONO ALVO PROMOCAO
    ''' </summary>
    ''' <returns></returns>
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)

    ''' <summary>
    '''  CODIGO PROMOCAO
    ''' </summary>
    ''' <returns></returns>
    Property CODPMC As Nullable(Of Decimal)

    Property TIPCMNPMC As Nullable(Of Decimal)

    Property DESTIPCMNPMC As String
    Property DESTIPDVGPMCLIVPCO As String
    Property CODSMBDVGPMCLIVPCO As String
    Property TIPDVGPMCLIVPCO As Nullable(Of Decimal)

End Class
