﻿Imports System.ComponentModel.DataAnnotations

Public Class PromocaoComunicacaoSaveViewModel

    Property CODPMC As Nullable(Of Decimal)

    Property RelacaoComunicacao As List(Of PromocaoComunicacaoViewModel)

End Class
