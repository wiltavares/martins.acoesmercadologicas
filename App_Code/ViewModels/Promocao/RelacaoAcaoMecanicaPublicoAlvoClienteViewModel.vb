﻿Imports System.ComponentModel.DataAnnotations

Public Class RelacaoAcaoMecanicaPublicoAlvoClienteViewModel
    Property CODACOCMC As Nullable(Of Decimal)
    Property NUMSEQRGRACOCMC As Nullable(Of Decimal)

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODCLI As Nullable(Of Decimal)
    Property NOMCLI As String

    'Propriedades Extra
    Property CODOPCAOFILTRO As Nullable(Of Decimal)
    Property DESOPCAOFILTRO As String

    Property NOMREP As String
    Property NOMGER As String
    Property NOMSUP As String

    Property CODREP As Nullable(Of Decimal)
    Property CODSUP As Nullable(Of Decimal)
    Property CODGER As Nullable(Of Decimal)
End Class
