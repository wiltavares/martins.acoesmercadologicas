﻿Imports System.ComponentModel.DataAnnotations

Public Class PromocaoPublicoAlvoGerenteVendasViewModel
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODGER As Nullable(Of Decimal)
    Property NOMGER As String
End Class
