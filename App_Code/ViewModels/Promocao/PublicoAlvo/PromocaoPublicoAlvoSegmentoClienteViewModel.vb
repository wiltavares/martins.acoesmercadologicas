﻿Imports System.ComponentModel.DataAnnotations

Public Class PromocaoPublicoAlvoSegmentoClienteViewModel
    Property CODPMC As Nullable(Of Decimal)
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    Property CODSGMNGCCLI As Nullable(Of Decimal)
    Property DESSGMNGCCLI As String
End Class
