﻿Imports System.ComponentModel.DataAnnotations

Public Class PromocaoPublicoAlvoRcaVendedorViewModel
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODREP As Nullable(Of Decimal)
    Property NOMREP As String

    'Propriedades Extra
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODOPCAOFILTRO As Nullable(Of Decimal)
    Property DESOPCAOFILTRO As String

    Property NOMSUP As String
    Property CODSUP As Nullable(Of Decimal)
End Class
