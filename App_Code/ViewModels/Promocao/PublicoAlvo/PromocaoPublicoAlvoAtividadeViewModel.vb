﻿Imports System.ComponentModel.DataAnnotations

Public Class PromocaoPublicoAlvoAtividadeViewModel
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODPMC As Nullable(Of Decimal)
    Property TIPEDEPUBALVPMC As Nullable(Of Decimal)
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODATI As Nullable(Of Decimal)
    Property NOMATI As String
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODUNDESRNGC As Nullable(Of Decimal)
    Property DESUNDESRNGC As String
End Class
