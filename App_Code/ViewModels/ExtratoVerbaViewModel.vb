﻿Imports Microsoft.VisualBasic

Public Class ExtratoVerbaViewModel

    'Data Referencia
    <Required(ErrorMessage:="Preencha o campo Data Inicial")>
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DataIni As DateTime?

    'Data Referencia
    <Required(ErrorMessage:="Preencha o campo Data Final")>
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DataFim As DateTime?

    'PROPRIEDADES FORNECEDOR
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFRN As Nullable(Of Decimal)
    Property NOMFRN As String

    'PROPRIEDADES COMPRADOR
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODCPR As Nullable(Of Decimal)
    Property NOMCPR As String

    'CELULA
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODDIVCMP As Nullable(Of Decimal)
    Property DESDIVCMP As String

    'DIRETORIA
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODDRTCMP As Nullable(Of Decimal)
    Property DESDRTCMP As String

    'TIPO DE EVENTO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEVTACOCMC As Nullable(Of Decimal)
    Property NOMTIPEVTACOCMC As String

    'EVENTO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODEVTACOCMC As Nullable(Of Decimal)
    Property NOMEVTACOCMC As String

    'EMPENHO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPDSNDSCBNF As Nullable(Of Decimal)
    Property DESDSNDSCBNF As String

    'CODIGO DA AÇÃO
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODACOCMC As Nullable(Of Decimal)
    'NOME DA AÇÃO
    Property NOMACOCMC As String
    'DATA APROVAÇÃO DA AÇÃO
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATAPVACOCMC As Nullable(Of DateTime)

    'VALOR DA PARTICIPACAO DO FORNECEDOR NA VERBA
    <DisplayFormat(DataFormatString:="{0:C}")>
    Property VLRPTCFRNACOCMC As Nullable(Of Decimal)



    'VALOR DO SALDO
    <DisplayFormat(DataFormatString:="{0:C}")>
    Property VLRSLD As Nullable(Of Decimal)

    'VALOR DO SALDO RESERVADO
    <DisplayFormat(DataFormatString:="{0:C}")>
    Property VLRSLDRSV As Nullable(Of Decimal)

    'VALOR DO LANÇAMENTO NO EXTRATO
    <DisplayFormat(DataFormatString:="{0:C}")>
    Property VLRMOV As Nullable(Of Decimal)

    'DATA DO LANCAMENTO NO EXTRATO
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DT_OCORRENCIA As Nullable(Of DateTime)

    'IDENTIFICADO DA ORIGEM DO LANÇAMENTO
    Property IDTMOV As String
    'TIPO DO LANÇAMENTO
    Property TIPO_MOV As String




    '---------------------------------------------------------------------

    'Property OBSERVACAO As String

    'codigo filtro 1 fornecedor  - 2 evento
    'Property Cod_Filtro As Nullable(Of Decimal)

    'propriedades celula / descrição
    'Property DATGRCMCOVBAFRN As Date
    'Property DATGRCMCO As Date







    <Display()>
    Property TipoVisao As String
    Property TipoOperacao As String

End Class
