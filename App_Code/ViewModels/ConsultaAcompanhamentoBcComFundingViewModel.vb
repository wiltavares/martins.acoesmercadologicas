﻿
Public Class ConsultaAcompanhamentoBcComFundingViewModel

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODGRPFRN As Decimal?

    Property NOMGRPFRN As String

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATREFOPE As Date?

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODBFC As Decimal?

    Property DESBFC As String

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATINIVLDBFC As Date?

    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATFIMVLDBFC As Date?

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFILEMP As Decimal?
    Property NOMFILEMP As String

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODFRN As Decimal?
    Property NOMFRN As String

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODMER As Decimal?
    Property DESMER As String

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property PERBFC As Decimal?

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property QDEITEBFC As Decimal?

    <DisplayFormat(DataFormatString:="{0:C}")>
    Property VLRTOTFNDBFC As Decimal?


    ' Auxiliares
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATREFOPEINI As Date?
    <DisplayFormat(DataFormatString:="{0:dd/MM/yyyy}", ApplyFormatInEditMode:=True)>
    Property DATREFOPEFIM As Date?
    Property TIPOPEINIGRCHST As String

End Class
