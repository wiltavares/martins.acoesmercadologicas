﻿Imports Microsoft.VisualBasic

Public Class GrupoFornecedorViewModel

    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODGRPFRN As Nullable(Of Decimal)

    Property NOMGRPFRN As String

    Public Property CODNOMGRPFRN() As String
        Get
            Return String.Format("{0} - {1}", Me.CODGRPFRN, Me.NOMGRPFRN)
        End Get
        Set(ByVal value As String)
        End Set
    End Property

End Class
