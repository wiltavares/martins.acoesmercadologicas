﻿Imports System.ComponentModel.DataAnnotations
Public Class LoginViewModel


    <Required(ErrorMessage:="(*) Preencha o nome de usuário")>
    <MaxLength(150, ErrorMessage:="Máximo: {1} caracteres")>
    <MinLength(2, ErrorMessage:="Mínimo: {1} caracteres")>
    <WebDisplayName("Usuário")>
    Property Usuario As String

    <Required(ErrorMessage:="(*) Preencha a senha")>
    <MaxLength(150, ErrorMessage:="Máximo: {1} caracteres")>
    <MinLength(2, ErrorMessage:="Mínimo: {1} caracteres")>
    <WebDisplayName("Senha")>
    Property Senha As String

    <WebDisplayName("Lembrar-me")>
    Property Lembrar As Boolean
    Property Codigo As Integer
    Property Nome As String

End Class
