﻿Public Class UnidadeEstrategicaDeNegocioViewModel

    Property CODUNDESRNGC As Nullable(Of Decimal)
    Property DESUNDESRNGC As String
    Property CODFNC As Nullable(Of Decimal)
    Property DESABVUNDESRNGC As String
    Property VLRPTNMCD As Nullable(Of Decimal)
    Property DATDST As Date
    Property CODESLNGCEMP As Nullable(Of Decimal)
    Property QDECTGCLIOCD As Nullable(Of Decimal)

    Public Property DROPDESC() As String
        Get
            Return CODUNDESRNGC.ToString + " - " + DESUNDESRNGC
        End Get
        Set(ByVal value As String)
            DESUNDESRNGC = value.Split("-")(1)
            CODUNDESRNGC = CDec(value.Split("-")(0))
        End Set
    End Property

End Class
