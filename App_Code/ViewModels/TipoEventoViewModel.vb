﻿Imports System.ComponentModel.DataAnnotations
Public Class TipoEventoViewModel

    <Key>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property TIPEVTACOCMC As Nullable(Of Decimal)

    <Required(ErrorMessage:="Preencha o campo Tipo de Evento")>
    <MaxLength(30, ErrorMessage:="Máximo: {1} caracteres")>
    Property NOMTIPEVTACOCMC As String

    <Required(ErrorMessage:="Selecione Opção")>
    Property CODCNTCTB As Nullable(Of Decimal)

    Property INDVLDRGRICT As Nullable(Of Decimal)

    Property INDPREARDPMC As Nullable(Of Decimal)


    Property INDCPHBTB As Nullable(Of Decimal)

    Property INDEVTSMA As Nullable(Of Decimal)



    Public Property DROPDESC() As String
        Get
            Return TIPEVTACOCMC.ToString + " - " + NOMTIPEVTACOCMC
        End Get
        Set(ByVal value As String)
            'NOMTIPEVTACOCMC = value.Split("-")(1)
            'TIPEVTACOCMC = CDec(value.Split("-")(0))
        End Set
    End Property

    ReadOnly Property DescDropTireve As String
        Get
            Return TIPEVTACOCMC.ToString + " - " + NOMTIPEVTACOCMC
        End Get
    End Property


End Class
