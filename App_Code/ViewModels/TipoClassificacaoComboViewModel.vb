﻿Public Class TipoClassificacaoComboViewModel

    Property TIPNIVCLFCPT As Nullable(Of Decimal)
    Property DESTIPNIVCLFCPT As String
    Property CODFNCDST As Nullable(Of Decimal)
    Property DATDST As Nullable(Of DateTime)
    Property CODFNCALT As Nullable(Of Decimal)
    Property DATALT As Nullable(Of DateTime)
    Property VLRMPLTIPNIVCPT As Nullable(Of Decimal)

End Class
