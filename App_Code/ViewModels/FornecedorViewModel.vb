﻿Imports System.ComponentModel.DataAnnotations

Public Class FornecedorViewModel

    Property IDFornecedor As Integer
    Property nmFornecedor As String
    Property bu As String
    Property empenhoAcordo As String
    Property empenhoExtraAcordo As String
    Property vrSaldoAcordo As Decimal?
    Property vrAcordo As String
    Property vrSaldoExtraAcordo As Decimal?
    Property vrExtraAcordo As Decimal?
    Property vrSaldoReceita As Decimal?
    Property vrReceita As Decimal?
    Property txAdministracao As Decimal?
    Property txBrinde As Decimal?
    Property vrparticipacao As Decimal?

End Class
