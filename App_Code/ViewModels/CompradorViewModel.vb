﻿
Public Class CompradorViewModel


    Property CODCPR As Nullable(Of Decimal)
    Property NOMCPR As String
    Property CODGERPRD As Nullable(Of Decimal)
    Property DESSGLGERPRD As String
    Property DATDSTCPR As Date

    Public Property CODNOMCPR() As String
        Get
            Return String.Format("{0} - {1}", Me.CODCPR, Me.NOMCPR)
        End Get
        Set(ByVal value As String)
        End Set
    End Property
End Class
