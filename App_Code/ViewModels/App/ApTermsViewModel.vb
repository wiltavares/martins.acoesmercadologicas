﻿Public Class ApTermsViewModel


    Property ATTRIBUTE2 As String
    Property ATTRIBUTE3 As String
    Property ATTRIBUTE4 As String
    Property ATTRIBUTE5 As String
    Property ATTRIBUTE6 As String
    Property ATTRIBUTE7 As String
    Property ATTRIBUTE8 As String
    Property ATTRIBUTE9 As String
    Property ATTRIBUTE10 As String
    Property ATTRIBUTE11 As String
    Property ATTRIBUTE12 As String
    Property ATTRIBUTE13 As String
    Property ATTRIBUTE14 As String
    Property ATTRIBUTE15 As String
    Property NAME As String
    Property DESCRIPTION As String
    Property TERM_ID As Nullable(Of Decimal)
    Property LAST_UPDATE_DATE As Nullable(Of Date)
    Property LAST_UPDATED_BY As Nullable(Of Decimal)
    Property CREATION_DATE As Nullable(Of Date)
    Property CREATED_BY As Nullable(Of Decimal)
    Property LAST_UPDATE_LOGIN As Nullable(Of Decimal)
    Property ENABLED_FLAG As String
    Property DUE_CUTOFF_DAY As Nullable(Of Decimal)
    Property TYPE As String
    Property START_DATE_ACTIVE As Nullable(Of Date)
    Property END_DATE_ACTIVE As Nullable(Of Date)
    Property RANK As Nullable(Of Decimal)
    Property ATTRIBUTE_CATEGORY As String
    Property ATTRIBUTE1 As String

End Class
