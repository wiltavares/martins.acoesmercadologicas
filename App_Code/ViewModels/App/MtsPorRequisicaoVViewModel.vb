﻿Imports System.ComponentModel.DataAnnotations
Public Class MtsPorRequisicaoVViewModel

    Property INVOICE_QUANTITY As Nullable(Of Decimal)
    Property INVOICE_UNIT_PRICE As Nullable(Of Decimal)
    Property REQUISITION_HEADER_ID As Nullable(Of Decimal)
    Property REQUISITION_NUM As String
    Property REQUISITION_APPROVED_STATUS As String
    Property EXTERNAL_SOURCE_NAME As String
    Property EXTERNAL_SOURCE_ID As String
    Property ORG_ID As Nullable(Of Decimal)
    Property REQUISITION_TYPE As String
    Property REQUISITION_LINE_ID As Nullable(Of Decimal)
    Property REQUISITION_LINE_NUM As Nullable(Of Decimal)
    Property REQUISITION_LINE_CANCEL_FLAG As String
    Property ITEM_ID As Nullable(Of Decimal)
    Property ITEM_NUM As String
    Property ITEM_DESCRIPTION As String
    Property CONCATENATED_SEGMENTS As String
    Property REQUISITION_UNIT_PRICE As Nullable(Of Decimal)
    Property REQUISITION_QUANTITY As Nullable(Of Decimal)
    Property REQUISITION_QUANTITY_PURCHASE As Nullable(Of Decimal)
    Property PURCHASE_TYPE As String
    Property PO_HEADER_ID As Nullable(Of Decimal)
    Property PURCHASE_NUM As String
    Property PURCHASE_APPROVED_STATUS As String
    Property PURCHASE_STATUS As String
    Property PURCHASE_CANCEL_FLAG As String
    Property VENDOR_NUM As String
    Property VENDOR_NAME As String
    Property PO_LINE_ID As Nullable(Of Decimal)
    Property LINE_LOCATION_ID As Nullable(Of Decimal)
    Property PURCHASE_LINE_NUM As Nullable(Of Decimal)
    Property PURCHASE_LINE_CANCEL_FLAG As String
    Property PURCHASE_UNIT_PRICE As Nullable(Of Decimal)
    Property PURCHASE_QUANTITY As Nullable(Of Decimal)
    Property PURCHASE_QUANTITY_RECEIVED As Nullable(Of Decimal)
    Property PURCHASE_QUANTITY_CANCELLED As Nullable(Of Decimal)
    Property ORGANIZATION_ID As Nullable(Of Decimal)
    Property OPERATION_ID As Nullable(Of Decimal)
    Property OPERATION_STATUS As String
    Property INVOICE_NUM As Nullable(Of Decimal)
    Property INVOICE_DATE As Nullable(Of Date)

    ReadOnly Property PURCHASE_TOTAL
        Get
            Return PURCHASE_UNIT_PRICE * PURCHASE_QUANTITY
        End Get
    End Property

    ReadOnly Property INVOICE_TOTAL
        Get
            Return INVOICE_UNIT_PRICE * INVOICE_QUANTITY
        End Get
    End Property
End Class
