﻿Public Class PoVendorSitesAllViewModel


    Property AP_TAX_ROUNDING_RULE As String
    Property AUTO_TAX_CALC_FLAG As String
    Property AUTO_TAX_CALC_OVERRIDE As String
    Property AMOUNT_INCLUDES_TAX_FLAG As String
    Property OFFSET_VAT_CODE As String
    Property BANK_NUMBER As String
    Property BANK_BRANCH_TYPE As String
    Property OFFSET_TAX_FLAG As String
    Property REMITTANCE_EMAIL As String
    Property EDI_TRANSACTION_HANDLING As String
    Property EDI_PAYMENT_METHOD As String
    Property EDI_PAYMENT_FORMAT As String
    Property EDI_REMITTANCE_METHOD As String
    Property EDI_REMITTANCE_INSTRUCTION As String
    Property EXCLUSIVE_PAYMENT_FLAG As String
    Property CAGE_CODE As String
    Property LEGAL_BUSINESS_NAME As String
    Property DOING_BUS_AS_NAME As String
    Property DIVISION_NAME As String
    Property SMALL_BUSINESS_CODE As String
    Property CCR_COMMENTS As String
    Property DEBARMENT_START_DATE As Nullable(Of Date)
    Property DEBARMENT_END_DATE As Nullable(Of Date)
    Property ADDRESS_STYLE As String
    Property LANGUAGE As String
    Property PROVINCE As String
    Property COUNTRY As String
    Property AREA_COD As String
    Property PHONE As String
    Property CUSTOMER_NUM As String
    Property SHIP_TO_LOCATION_ID As Nullable(Of Decimal)
    Property BILL_TO_LOCATION_ID As Nullable(Of Decimal)
    Property SHIP_VIA_LOOKUP_CODE As String
    Property FREIGHT_TERMS_LOOKUP_CODE As String
    Property FOB_LOOKUP_CODE As String
    Property INACTIVE_DATE As Nullable(Of Date)
    Property FAX As String
    Property FAX_AREA_CODE As String
    Property TELEX As String
    Property TERMS_DATE_BASIS As String
    Property DISTRIBUTION_SET_ID As Nullable(Of Decimal)
    Property ACCTS_PAY_CODE_COMBINATION_ID As Nullable(Of Decimal)
    Property PREPAY_CODE_COMBINATION_ID As Nullable(Of Decimal)
    Property PAY_GROUP_LOOKUP_CODE As String
    Property PAYMENT_PRIORITY As Nullable(Of Decimal)
    Property TERMS_ID As Nullable(Of Decimal)
    Property INVOICE_AMOUNT_LIMIT As Nullable(Of Decimal)
    Property PAY_DATE_BASIS_LOOKUP_CODE As String
    Property ALWAYS_TAKE_DISC_FLAG As String
    Property INVOICE_CURRENCY_CODE As String
    Property PAYMENT_CURRENCY_CODE As String
    Property COUNTY As String
    Property VENDOR_SITE_ID As Nullable(Of Decimal)
    Property LAST_UPDATE_DATE As Nullable(Of Date)
    Property LAST_UPDATED_BY As Nullable(Of Decimal)
    Property VENDOR_ID As Nullable(Of Decimal)
    Property VENDOR_SITE_CODE As String
    Property VENDOR_SITE_CODE_ALT As String
    Property LAST_UPDATE_LOGIN As Nullable(Of Decimal)
    Property CREATION_DATE As Nullable(Of Date)
    Property CREATED_BY As Nullable(Of Decimal)
    Property PURCHASING_SITE_FLAG As String
    Property RFQ_ONLY_SITE_FLAG As String
    Property PAY_SITE_FLAG As String
    Property ATTENTION_AR_FLAG As String
    Property ADDRESS_LINE1 As String
    Property ADDRESS_LINE2 As String
    Property ADDRESS_LINE3 As String
    Property ADDRESS_LINES_AL As String
    Property CITY As String
    Property STATE As String
    Property ZIP As String
    Property HOLD_ALL_PAYMENTS_FLAG As String
    Property HOLD_FUTURE_PAYMENTS_FLAG As String
    Property HOLD_REASON As String
    Property HOLD_UNMATCHED_INVOICES_FLAG As String
    Property TAX_REPORTING_SITE_FLAG As String
    Property ATTRIBUTE_CATEGORY As String
    Property ATTRIBUTE1 As String
    Property ATTRIBUTE2 As String
    Property ATTRIBUTE3 As String
    Property ATTRIBUTE4 As String
    Property ATTRIBUTE5 As String
    Property ATTRIBUTE6 As String
    Property ATTRIBUTE7 As String
    Property ATTRIBUTE8 As String
    Property ATTRIBUTE9 As String
    Property ATTRIBUTE10 As String
    Property ATTRIBUTE11 As String
    Property ATTRIBUTE12 As String
    Property ATTRIBUTE13 As String
    Property ATTRIBUTE14 As String
    Property ATTRIBUTE15 As String
    Property REQUEST_ID As Nullable(Of Decimal)
    Property PROGRAM_APPLICATION_ID As Nullable(Of Decimal)
    Property PROGRAM_ID As Nullable(Of Decimal)
    Property PROGRAM_UPDATE_DATE As Nullable(Of Date)
    Property VALIDATION_NUMBER As Nullable(Of Decimal)
    Property EXCLUDE_FREIGHT_FROM_DISCOUNT As String
    Property BANK_CHARGE_BEARER As String
    Property ORG_ID As Nullable(Of Decimal)
    Property CHECK_DIGITS As String
    Property ADDRESS_LINE4 As String
    Property ALLOW_AWT_FLAG As String
    Property AWT_GROUP_ID As Nullable(Of Decimal)
    Property PAY_AWT_GROUP_ID As Nullable(Of Decimal)
    Property DEFAULT_PAY_SITE_ID As Nullable(Of Decimal)
    Property PAY_ON_CODE As String
    Property PAY_ON_RECEIPT_SUMMARY_CODE As String
    Property GLOBAL_ATTRIBUTE_CATEGORY As String
    Property GLOBAL_ATTRIBUTE1 As String
    Property GLOBAL_ATTRIBUTE2 As String
    Property GLOBAL_ATTRIBUTE3 As String
    Property GLOBAL_ATTRIBUTE4 As String
    Property GLOBAL_ATTRIBUTE5 As String
    Property GLOBAL_ATTRIBUTE6 As String
    Property GLOBAL_ATTRIBUTE7 As String
    Property GLOBAL_ATTRIBUTE8 As String
    Property GLOBAL_ATTRIBUTE9 As String
    Property GLOBAL_ATTRIBUTE10 As String
    Property GLOBAL_ATTRIBUTE11 As String
    Property GLOBAL_ATTRIBUTE12 As String
    Property GLOBAL_ATTRIBUTE13 As String
    Property GLOBAL_ATTRIBUTE14 As String
    Property GLOBAL_ATTRIBUTE15 As String
    Property GLOBAL_ATTRIBUTE16 As String
    Property GLOBAL_ATTRIBUTE17 As String
    Property GLOBAL_ATTRIBUTE18 As String
    Property GLOBAL_ATTRIBUTE19 As String
    Property GLOBAL_ATTRIBUTE20 As String
    Property TP_HEADER_ID As Nullable(Of Decimal)
    Property ECE_TP_LOCATION_CODE As String
    Property PCARD_SITE_FLAG As String
    Property MATCH_OPTION As String
    Property COUNTRY_OF_ORIGIN_CODE As String
    Property FUTURE_DATED_PAYMENT_CCID As Nullable(Of Decimal)
    Property CREATE_DEBIT_MEMO_FLAG As String
    Property SUPPLIER_NOTIF_METHOD As String
    Property EMAIL_ADDRESS As String
    Property PRIMARY_PAY_SITE_FLAG As String
    Property SHIPPING_CONTROL As String
    Property SELLING_COMPANY_IDENTIFIER As String
    Property GAPLESS_INV_NUM_FLAG As String
    Property DUNS_NUMBER As String
    Property LOCATION_ID As Nullable(Of Decimal)
    Property PARTY_SITE_ID As Nullable(Of Decimal)
    Property TOLERANCE_ID As Nullable(Of Decimal)
    Property SERVICES_TOLERANCE_ID As Nullable(Of Decimal)
    Property RETAINAGE_RATE As Nullable(Of Decimal)
    Property EDI_ID_NUMBER As String
    Property VAT_REGISTRATION_NUM As String
    Property LANGUAGE_CODE As String
    Property PAYMENT_METHOD_LOOKUP_CODE As String
    Property BANK_ACCOUNT_NAME As String
    Property BANK_ACCOUNT_NUM As String
    Property BANK_NUM As String
    Property BANK_ACCOUNT_TYPE As String
    Property CURRENT_CATALOG_NUM As String
    Property VAT_CODE As String



    Property IDTEMPFRN As Nullable(Of Decimal)
    Property NUMCGCCPF As String
    Property RAZSOCEMPFRN As String
    Property IDTLCLEMPFRN As Nullable(Of Decimal)
    Property CODFILCGCCPF As String
    Property NOMEMPFRN As String
    Property TIPSITJURPES As String
    Property NUMCGCEMPFRN As Nullable(Of Decimal)


End Class
