﻿Imports System.ComponentModel.DataAnnotations
Public Class TipoOperacaoViewModel

    <Key>
    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODOPEFSCDSNACOCMC As Nullable(Of Decimal)

    <Required(ErrorMessage:="Preencha o campo Tipo de Operação")>
    <MaxLength(50, ErrorMessage:="Máximo: {1} caracteres")>
    Property DESOPEFSCDSNACOCMC As String

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDTIPOPEDSNACOCMC As Nullable(Of Decimal)

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDBDEOPEDSNACOCMC As Nullable(Of Decimal)

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDUNDESRNGCACOCMC As Nullable(Of Decimal)

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDGRCREQAUTITERSU As Nullable(Of Decimal)

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDGRCITEENCFOLPGT As Nullable(Of Decimal)

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDTIPOPEACOCMCAUT As Nullable(Of Decimal)

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDTIPOPEICT As Nullable(Of Decimal)

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDTIPOPEVLRMON As Nullable(Of Decimal)

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDTIPOPEREQVGM As Nullable(Of Decimal)

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDTIPOPEREQEVT As Nullable(Of Decimal)

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDTIPOPESLCPGT As Nullable(Of Decimal)

    <Required(ErrorMessage:="Selecione uma Opção")>
    Property INDPTCCLIOPEDSN As Nullable(Of Decimal)


    Public Property DROPDESC() As String
        Get
            Return CODOPEFSCDSNACOCMC.ToString + " - " + DESOPEFSCDSNACOCMC
        End Get
        Set(ByVal value As String)
        End Set
    End Property
End Class
