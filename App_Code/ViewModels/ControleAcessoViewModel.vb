﻿Imports System.ComponentModel.DataAnnotations
Public Class ControleAcessoViewModel


    <DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    Property CODGRPACS As Nullable(Of Decimal)
    Property NOMFNC As String
    Property NOMFILEMP As String

    Property CODMNU As String
    Property NOMSISINF As String
    Property DESMNU As String
    Property CODMNUPAI As Nullable(Of Decimal)
    Property NOMOBEPRG As String
    Property INDASSOCIADO As Nullable(Of Decimal)

    '<DisplayFormat(DataFormatString:="{0:F0}", ApplyFormatInEditMode:=True)>
    'Property CODFNC As Nullable(Of Decimal)
    'Property NOMFNC As String
    'Property CODFILEMP As Nullable(Of Decimal)
    'Property NOMFILEMP As String

    'Property CODMNU As Nullable(Of Decimal)
    'Property DESMNU As String
    'Property CODMNUPAI As Nullable(Of Decimal)
    'Property NOMOBEPRG As String

    'Property INDASSOCIADO As Nullable(Of Decimal)

    'ReadOnly Property Associado() As Boolean
    '    Get
    '        Return (INDASSOCIADO = 1)
    '    End Get
    'End Property

    'Property DESGRPACS As String

End Class
